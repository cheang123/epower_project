﻿namespace EPower
{
    public enum TransferType {
        UPLOAD_USAGE = 1,
        DOWNLOD_USAGE = 2
    }

    public enum TransmissionOption
    {
        WIRE = 1,
        Internet = 2,
        GPRS = 3,
        SD = 4,
    }

    public enum RelayControl
    {
        Connect = 2,
        Disconnect = 3
    }
    
    public enum Unlimitted
    {
        INT = -1
    }
    public enum CashDrawer
    {
        Transfer=-1
    }
    public enum DeviceInfo
    {
        DeviceType,
        DeviceOemInfo,
        DeviceVersion,
        DeviceProcessorType,
        DeviceHardwareId
    } 
    public enum DeviceType
    { 
        Mobile=1,
        IR_READER=2,
        Metrologic=3,
        BlueStar=4,
        /// <summary>
        /// Portble Data Terminal
        /// </summary>
        PDT=5
    } 
    public enum CustomerStatus
    {
        Pending = 1,
        Active = 2,
        Blocked = 3,
        Closed = 4,
        Cancelled= 5
    }   

    public enum EmpPosition
    {
        Collector = 1,
        Cutter = 2,
        Biller = 3
    }

    public enum InvoiceStatus
    {
        Open = 1,//not yet pay
        Pay = 2,//pay some amount
        Cancel = 3,//cancel by user
        Close = 4,//the invoic is paid
        ForwardBill = 5 // Forward the service bill balance to When run bill
    }

    public enum AuditTrialGroup
    {
        Prepaid = 1,
        PostPaid = 2,
        Customer = 3,
        Billing = 4,
        Admin = 5,
        Security = 6,
        SystemLog = 8,
        SendUsage = 9,
        ReceiveUsage = 10
    }

    public enum CustomerOperation
    {
        BLOCK_CUSTOMER, 
        ACTIVATE_CUSTOMER
    }

    public enum FixChargeStatus
    {
        Open = 1,    //Not yet sent to billing
        Pay = 2,     //Already sent some to billing
        Cancel = 3,  //User cancel
        Close = 4    //Sent all to billing
    }

    public enum InvoiceItem
    {
        CAPACITY_CHARGE = -8,
        Prepayment = -7,
        Deposit = -6,
        Reconnect=-5,
        Reverse = -3,
        Adjustment =-1,
        Power = 1, 
        NewConnection = 3,
        PrepaidPower = 4
    }

    public enum DeviceStatus
    {
        Stock = 1,
        Used,
        Unavailable
    }

    public enum MeterStatus
    {
        Stock = 1,
        Used = 2,
        Unavailable = 3
    }

    public enum BoxStatus
    {
        Stock = 1,
        Used = 2,
        Unavailable = 3
    }

    public enum BreakerStatus
    {
        Stock = 1,
        Used = 2,
        Unavailable = 3
    }

    // SELECT UTILITY_NAME+'='+CONVERT(NVARCHAR,UTILITY_ID)+',' FROM TBL_UTILITY;
    public enum Utility
    {
        DEFAULT_CUT_OFF_DAY = 1,
        INVOICE_DUE_DATE = 2,
        CUT_OFF_AMOUNT = 3,
        MAX_DAY_TO_IGNORE_FIXED_AMOUNT = 16,
        BUYPOWER_HEADER = 17,
        BUYPOWER_DIGIT = 18,
        BUYPOWER_START_NUMBER = 19,
        BUYPOWER_DISPLAY_YEAR = 20,
        DEPOSIT_IS_PAID_AUTO = 21,
        DEPOSIT_IS_IGNORED = 22,
        ALARM_QTY = 23,
        VERSION = 24,
        MAX_DAY_TO_IGNORE_BILLING = 25,
        ENABLE_AUTO_MOVE_TO_NEXT_CUSTOMER = 26,
        ENABLE_AUTO_FILL_DUE_AMOUNT = 27,
        ENABLE_RECURRING_SERVICE = 28,
        KEEP_BILLING_REMAIN_AMOUNT = 29,
        ENABLE_BARCODE_COLLECTION = 30,
        ENABLE_MOBILE_COLLECTION = 31,
        SERVICE_URL = 32,
        UPDATE_URL = 33,
        ENABLE_BANK_DEPOSIT = 34,
        ENABLE_STOCK_MANGEMENT = 35,
        ENABLE_INCOME_EXPENSE = 36,
        ADJUST_POWER_PRODUCTION = 37,
        ENABLE_QUARTERLY_REPORT = 38,
        USE_ONLY_POWER_INVOICE_TO_BLOCK_CUSTOMER = 39,
        REPORT_INVOICE_LIST = 40,
        ENABLE_EDIT_PAYMENT_DATE = 41,
        EXE_SCHEDULE = 42,
        BANK_PAYMENT_ENABLE = 43,
        BANK_PAYMENT_CODE = 44,
        BANK_PAYMENT_ACCESS_KEY = 45,
        BANK_PAYMENT_CRYPTO_KEY = 46,
        BANK_PAYMENT_SERVICE_URL = 47,
        BANK_PAYMENT_VPN_USER = 48,
        BANK_PAYMENT_VPN_PASSWORD = 50,
        POSITION_IN_BOX_ENABLE = 51,
        ENABLE_ADJUST_OLD_INVOICE = 52,
        ENABLE_ALL_HISTORY_USAGE = 53,
        USAGE_SERVICE_URL = 54,
        AUTO_SEND_DATA_TO_BANK = 55,
        INVOICE_CONCURRENT_PRINTING = 56,
        BANK_PAYMENT_INCLUDE_DEPOSIT = 57,
        IR_MULTIPLE_DATABASE = 58,
        ENABLE_EDIT_ACCOUNT_CHART = 59,
        ENABLE_EDIT_FIXED_ASSET_CATEGORY = 60,
        ENABLE_ANNAUL_REPORT = 61,
        ENABLE_SERVICE_PENALTY = 62,
        ENABLE_ORDER_RELAY_METER = 63,
        ENABLE_PRICE_EXTRA_CHARGE = 64,
        AMR_SERVICE_URL = 65,
        AMR_ACCESS_KEY = 66,
        PREPAID_LIMITE_USAGE = 67,
        PREPAID_PRICE_CREDIT_USAGE = 68,
        ENABLE_DEPOSIT_DATE = 69,
        ENABLE_INVOICE_SERVICE_DATE = 70,
        EBILL_ENABLE = 71,
        EBILL_SERVICE_URL = 72,
        EBILL_SERVICE_USER = 73,
        EBILL_SERVICE_PASSWORD = 74,
        EBILL_SUPPLIER_CODE = 75,
        EBILL_SUPPLIER_SECRET_KEY = 76,
        EBILL_SUPPLIER_AUTO_UPLOAD = 77,
        DEFAULT_DAY_TO_RUN_REF = 78,
        WINCE_IR_VERSION = 79,
        ENABLE_ADJUST_BASED_PRICE_SUBSIDY = 80,
        NOTE_BANK_REGISTRATION = 81,
        ENABLE_PREPAYMENT_SERVICE = 82,
        IR_ADB_PATH = 83,
        IR_H203_ACTIVATE_CODE = 84,
        ENABLE_BANK_PAYMENT_LOG_CUSTOMER_BY_AREA = 85,
        ENABLE_B24 = 86,
        B24_SUPPLIER_API_URL = 87,
        B24_SUPPLIER_API_KEY = 88,
        B24_CUSTOMER_URL_PAYMENT = 89,
        B24_SUPPLIER_URL = 90,
        B24_EAC_SUPPLIER_API_URL = 91,
        B24_EAC_SUPPLIER_API_KEY = 92,
        B24_EAC_CUSTOMER_CODE = 93,
        EAC_APP_TABLE_NAME = 94,
        FTP_URL = 95,
        FTP_USER = 96,
        FTP_PWD = 97,
        EAC_BILLING_EMAIL=98,
        ACL_AGGREEMENT_EMAIL_ADDRESS=99,
        ENABLE_EDIT_INVOICE_ADJUSTMENT_OVER3=100,
        ENABLE_COVID19_SUBSIDIZED=101,
        E_FILLING_EXPORT_NUMBER = 102,
        POINTER_CONNECTION_STRING = 103,
        POINTER_COMPANY_ID = 104,
        MAXED_CUSTOMER_IN_CYCLE = 105,
        BACKUP_BEFORE_RUN_BILL = 106,
        BACKUP_BEFORE_REVERS_BILL = 107,
        EAC_MAIL_FOR_REPORT_STATISTIC = 108,
        UPDATE_SERVICE_URL = 109,
        ARAKAWA = 110,
        MAIL_SERVER = 111,
        HB02_SERVICE_URL = 112
    }

    public enum AccountType
    {
        Income=1,
        Expense=2
    }

    public enum CustomerType
    {
        UsedInProduction = -1,
        SellerMarket = -2
    }

    public enum DepositAction
    {
        AddDeposit = 1,
        AdjustDeposit = 2,
        RefundDeposit = 3,
        AppyPayment = 4
    }

    public enum PrepaymentAction
    {
        AddPrepayment = 1,
        ApplyPayment = 2,
        RefundPrepayment = 3,
        CancelPrepayment = 4
    }

    public enum MininumUsageType
    {
        MINIMUM_USAGE = 1,
        MINIMUM_AMOUNT = 2
    }

    public enum SendPeriod
    {
        DIALY=1,
        WEEKLY,
        MONTHLY,
    }

    public enum IRReaderEnergy
    {
        DefaultEnergy=0,
        DefaultConstant=-1,
        UpdateConstant=0
    }

    public enum TmpUsageStatus
    {

    }

    public enum StockType
    {
        None = 0,

        Stock = 1,
        Used = 2,
        Unavailable = 3
    }

    public enum StockTranType
    {
        /// <summary>
        /// Stock-In or Import Stock!
        /// Status : StockType.None to StockType.Stock
        /// </summary>
        StockIn = 1, 

        /// <summary>
        /// Stock-Out (Not the same Use).
        /// Status : StockType.Unavailble/StockType.Stock to StockTyp.None
        /// </summary>
        StockOut = 2,

        /// <summary>
        /// Stock was change status from stock to Unavaible
        /// Status : StockType.Stock to StockType.Unavaible
        /// </summary>
        Damage = 3,

        /// <summary>
        /// Process of use stock, change status from stock to used!
        /// Status : StockType.Stock to StockType.Use
        /// </summary>
        Use = 4,

        /// <summary>
        /// Adjust andy type of stock
        /// Status : StockType.None to StockType.X (where X is any stock type exclue StockType.NONE)
        /// </summary>
        Adjust = 5
    }

    public enum StockItemType
    {
        StockItem = 1, 
        Meter = 2, 
        Breaker = 3
    }



    public enum ItemType
    {
        OTHER = 0,
        SERVICE = 1,
        PREPAID_METER = 2,
        POSTPAID_METER = 3,
        BREAKER = 4,
        IR_READER = 5
    }

    public enum Sequence
    {
        Reverse = -2,
        Adjustment = -1,
        Invoice = 1,
        Receipt = 2,
        Customer = 3,
        Service = 4
    }
    public enum SequenceType
    {
        Invoice = 1,
        InvoiceService = 2,
        Prepaid = 3
    }

    public enum AccountConfig
    {
        CUSTOMER_TYPE_INCOME_ACCOUNTS=1,
        INVOICE_ITEM_INCOME_ACCOUNTS=2,
        FIX_ASSET_ASSET_ACCOUNTS = 3,
        FIX_ASSET_ACCUMULATED_DEPRECIATION_ACCOUNTS = 4,
        FIX_ASSET_DEPRECIATION_EXPENSE_ACCOUNTS = 5,
        CASH_ACCOUNTS = 6,
        EXPENSE_TRANS_ACCOUNTS = 7,
        INCOME_TRANS_ACCOUNTS = 8,
        PAYMENT_ACCOUNTS = 9,
        PAYMENT_DEFAULT_ACCOUNTS = 10,
        INCOME_DISPOSE_ACCOUNTS = 11,
        EXPENSE_MAINTENANCE_ACCOUNTS = 12, 

        POWER_PURCHASE_ACCOUNT=13,
        LIABILITY_ACCOUNT=14,
        EXPENSE_INTEREST=15,

        OTHER_INCOME_ACCOUNT = 16,

        DEPOSIT_ACCOUNT = 17,
        DEPOSIT_ACCOUNT_CASH = 18,

        PREPAYMENT_ACCOUNT = 19,
        PREPAYMENT_ACCOUNT_CASH = 20,

        RECEIVABLE_ACCOUNT = 21
    }

    public enum BankPaymentTypes
    {
        EXCEL = 1,
        ACLEDA = 2,
        SERVICE = 3
    }
    /// <summary>
    /// Result of method.
    /// </summary>
    public enum BankPaymentImportResult
    {
        /// <summary>
        /// Record or invoice that's not found in the system.
        /// </summary>
        RecordNotFound = 4,
        /// <summary>
        /// Record that input not correct (error pay_amount of pay_date).
        /// </summary>
        RecordError = 1,

        /// <summary>
        /// Record that process successfully.
        /// </summary>
        RecordSuccess = 2,
        /// <summary>
        /// Record that alareay paid.
        /// </summary>
        RecordPaid = 3,
    }
    public enum BankPaymentAction
    {
        EPW_UPDATE_INVOICE = 1,
        EPW_UPDATE_CUSTOMER = 2,
        EPW_GET_PAYMENT_DATA = 3,
        BNK_GET_CUSTOMER_DATA = 4,
        BNK_UPDATE_PAYMENT_DATA = 5
    }

    public enum FixAssetItemStatus
    {
        USING=1,
        STOP_USE=2
    }

    public enum DepreciationStatus
    {
        ReadyDepreciation=1,
        NotYet=2
    }
    // SELLECT 
    public enum LookUp
    {
        TRANSFORMER_TYPE=100,
        TRANSFORMER_POSITION=101,
        BUSINESS_DIVISION=102
    }

    public enum LoanStatus
    {
        LOAN_NOT_PAID=1,
        LOAN_PAID=2
    }

    public enum RationType
    {
        MV=1,
        LV=2
    }

    public enum ConnectionType
    {
        Normal=1,
        MVLicenseeTransfo = 2,
        MVOwnTransfo=3,
        MV=4
    }

    public enum Currency
    {
        KHR=1,
        USD=2,
        THB=3,
        VND=4
    }

    public enum TransfoPositonType
    {
        OUTDOOR = 10101,
        INDOOR = 10102
    }

    public enum MVCustomer
    {
        IndustryMVCustomer = 4,
        BusinessMVCustomer = 8,
        IndustryTOU_MVCustomer = 7,
        BusinessTOU_MVCustomer = 10,
        LicenseeMVCustomer = 11,
        MINING_INDUSTRY_MV= 26,
        MANUFACTURING_INDUSTRY_MV= 38,
        TEXTILE_INDUSTRY_MV= 50,
        AGRICULTURE_MV= 62,
        ACTIVITIES_MV= 74,
        AGRICULTURAL_MV= 86,
        BUSINESS_MV= 98,
        PUBLIC_ADMINISTRATION_MV= 110,
        OTHER_SERVICES_MV= 122,
        MINING_INDUSTRY_BUY_MV_7_TO_9= 29,
        MINING_INDUSTRY_BUY_MV_9_TO_7 = 32,
        MINING_INDUSTRY_BUY_MV_SUN= 35,
        MANUFACTURIING_INDUSTRY_BUY_MV_7_TO_9 = 41,
        MANUFACTURIING_INDUSTRY_BUY_MV_9_TO_7 = 44,
        MANUFACTURIING_INDUSTRY_BUY_MV_SUN= 47,
        TXTTILE_INDUSTRY_BUY_MV_7_TO_9 = 53,
        TXTTILE_INDUSTRY_BUY_MV_9_TO_7 = 56,
        TXTTILE_INDUSTRY_BUY_MV_SUN = 59,
        AGRICULTURE_BUY_MV_7_TO_9 = 65,
        AGRICULTURE_BUY_MV_9_TO_7 = 68,
        AGRICULTURE_BUY_MV_SUN = 71,
        ACTIVITIES_BUY_MV_7_TO_9= 77,
        ACTIVITIES_BUY_MV_9_TO_7 = 80,
        ACTIVITIES_BUY_MV_SUN= 83,
        AGRICULTURAL_BUY_MV_7_TO_9 = 89,
        AGRICULTURAL_BUY_MV_9_TO_7 = 92,
        AGRICULTURAL_BUY_MV_SUN = 95,
        BUSINESS_BUY_MV_7_TO_9= 101,
        BUSINESS_BUY_MV_9_TO_7 = 104,
        BUSINESS_BUY_MV_SUN = 107,
        PUBLIC_ADMINISTRATION_BUY_MV_7_TO_9 = 113,
        PUBLIC_ADMINISTRATION_BUY_MV_9_TO_7 = 116,
        PUBLIC_ADMINISTRATION_BUY_MV_SUN = 119,
        OTHER_SERVICES_BUY_MV_7_TO_9 = 125,
        OTHER_SERVICES_BUY_MV_9_TO_7 = 128,
        OTHER_SERVICES_BUY_MV_SUN = 131,
        AGRICULTURAL_WATER_9_TO_7=141,
        PUBLIC_ADMINISTRATION_BUY_MV_RIEL = 145
    }

    public enum LicenseType
    {
        LicenseGeneral=1,
        LicenseMarketDistributor=2
    }

    public enum IdentificationType
    {
        NationalID = -3,
        Passport = -2,
        VATIN = -1
    }

    public enum TaxCustomerType
    {
        Taxable = 1,
        TaxFree = 2,
        OverseasCompanies = 3
    }

    public enum ReasonType
    {
        ChangeMeter = 1,
        ChangeMetercode = 2,
        ChangeAmpere = 3
    }

    public enum AccountingConfig
    {
        JOURNAL_SALE = 1,
        JOURNAL_LOSS = 2,
        JOURNAL_CASH = 3,
        JOURNAL_GENERAL = 4,
        JOURNAL_ADJUSTMENT = 5,
        JOURNAL_DEPOSIT = 6,
        JOURNAL_PREPAYMENT = 7,
        TAX = 8
    }
    public enum ReferenceType
    {
        InvoiceBill = 1,
        InvoiceService = 2,
        Deposit = 3,
        Payment = 4,
        Adjustment = 5,
        Prepayment = 6
    }

    public enum TaxComputations
    {
        FIXED = 1,
        PERCENTAGE = 2,
        PERCENTAGE_INCLUDED_TAX = 3
    }

    public enum PaymentMethod
    {
        Bank = -1,
        Prepayment = -2,
    }
}
