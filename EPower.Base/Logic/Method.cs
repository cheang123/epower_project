﻿using Microsoft.Win32;
using SoftTech;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace EPower.Base.Logic
{
    public class Method
    {
        public static int METER_DIGIT = 12;
        public static int CUSTOMER_DIGIT = 6;
        public static int PIPE_METER_DIGIT = 14;
        #region Connection
        /// <summary>
        /// Get connection string
        /// </summary>
        /// <returns></returns>
        public static string GetConnectionString(string strCon)
        {
            return Cryption.Cryption.DecryptString(strCon);
        }

        #endregion Connection

        #region Deposit

        /// <summary>
        /// Get amount to deposit for each customer
        /// </summary>
        /// <param name="intCustomerID"></param>
        /// <returns></returns>
        public static decimal GetAmoutToDepsoit(int intCustomerID, int nCurrency)
        {
            decimal decReturn = 0;

            var preDeposit = (from d in DBDataContext.Db.TBL_DEPOSITs
                              from c in DBDataContext.Db.TBL_CUSTOMERs
                              where d.CUSTOMER_TYPE_ID == c.CUSTOMER_CONNECTION_TYPE_ID &&
                                    d.PHASE_ID == c.PHASE_ID &&
                                    d.AMPARE_ID == c.AMP_ID &&
                                    d.IS_ACTIVE == true &&
                                    c.CUSTOMER_ID == intCustomerID
                                    && d.CURRENCY_ID == nCurrency
                              select d.AMOUNT).FirstOrDefault();

            if (preDeposit != null)
            {
                return (decimal)preDeposit;
            }

            var objCus = (from c in DBDataContext.Db.TBL_CUSTOMERs
                          join ct in DBDataContext.Db.TBL_CUSTOMER_TYPEs on c.CUSTOMER_CONNECTION_TYPE_ID equals ct.CUSTOMER_TYPE_ID
                          join amp in DBDataContext.Db.TBL_AMPAREs on c.AMP_ID equals amp.AMPARE_ID
                          join ph in DBDataContext.Db.TBL_PHASEs on c.PHASE_ID equals ph.PHASE_ID
                          join pri in DBDataContext.Db.TBL_PRICEs on c.PRICE_ID equals pri.PRICE_ID
                          join pd in DBDataContext.Db.TBL_PRICE_DETAILs on pri.PRICE_ID equals pd.PRICE_ID
                          where c.CUSTOMER_ID == intCustomerID
                          select new
                          {
                              ct.DEPOSIT_USAGE,
                              amp.AMPARE_NAME,
                              ph.PHASE_NAME,
                              pd.PRICE
                          }).FirstOrDefault();
            if (objCus != null)
            {
                decReturn = DataHelper.NumberOnly(objCus.AMPARE_NAME) * DataHelper.NumberOnly(objCus.PHASE_NAME) *
                    objCus.DEPOSIT_USAGE * objCus.PRICE;

            }
            return decReturn;
        }

        public static decimal GetNewConnectionAmount(int intCustomerID, int nCurrencyId)
        {
            decimal decReturn = 0;
            TBL_CUSTOMER objCus = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == intCustomerID);

            if (objCus != null)
            {
                TBL_CONNECTION_FEE objDep = DBDataContext.Db.TBL_CONNECTION_FEEs.FirstOrDefault(row => row.AMPARE_ID == objCus.AMP_ID
                                                    && row.PHASE_ID == objCus.PHASE_ID && row.CURRENCY_ID == nCurrencyId && row.IS_ACTIVE);
                if (objDep != null)
                {
                    decReturn = objDep.NEW_CONECTION;
                }
            }

            return decReturn;
        }

        /// <summary>
        /// Get Customer Last Deposit Balance
        /// </summary>
        /// <param name="intCusId"></param>
        /// <returns></returns>
        public static decimal GetCustomerDeposit(int intCusId, out int intCurrencyId)
        {
            decimal decReturn = 0;
            intCurrencyId = 0;
            TBL_CUS_DEPOSIT objDeposit = DBDataContext.Db.TBL_CUS_DEPOSITs.Where(cd => cd.CUSTOMER_ID == intCusId && cd.IS_PAID).OrderByDescending(cd => cd.CUS_DEPOSIT_ID).FirstOrDefault();
            if (objDeposit != null)
            {
                decReturn = objDeposit.BALANCE;
                intCurrencyId = objDeposit.CURRENCY_ID;
            }
            return decReturn;
        }

        /// <summary>
        /// Get Customer Last Deposit Balance
        /// </summary>
        /// <param name="intCusId"></param>
        /// <returns></returns>
        public static decimal GetCustomerDeposit(int intCusId, int nCurrency)
        {
            decimal decReturn = 0;
            TBL_CUS_DEPOSIT objDeposit = DBDataContext.Db.TBL_CUS_DEPOSITs.Where(cd => cd.CUSTOMER_ID == intCusId && cd.IS_PAID && cd.CURRENCY_ID == nCurrency).OrderByDescending(cd => cd.CUS_DEPOSIT_ID).FirstOrDefault();
            if (objDeposit != null)
            {
                decReturn = objDeposit.BALANCE;
            }
            return decReturn;
        }

        public static TBL_CUS_DEPOSIT NewCustomerDeposit(int intCusId, decimal decAmount, decimal decBalance,
                        int intCurrencyId, DepositAction action,
                        string strNode, DateTime datDepositDate, bool blnIsPaid, DateTime refund_date)
        {
            var exchangeRate = new ExchangeRateLogic().findLastExchangeRate(datDepositDate, intCurrencyId);
            TBL_CUS_DEPOSIT objReturn = new TBL_CUS_DEPOSIT()
            {
                AMOUNT = decAmount,
                BALANCE = decBalance,
                CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                CREATE_ON = DBDataContext.Db.GetSystemDate(),
                DEPOSIT_DATE = datDepositDate,
                NOTE = strNode,
                CURRENCY_ID = intCurrencyId,
                CUSTOMER_ID = intCusId,
                DEPOSIT_ACTION_ID = (int)action,
                IS_PAID = blnIsPaid,
                REQUEST_REFUND_DATE = refund_date,
                EXCHANGE_RATE = exchangeRate.EXCHANGE_RATE,
                EXCHANGE_RATE_DATE = exchangeRate.CREATE_ON
            };
            if (Login.CurrentCashDrawer != null)
            {
                objReturn.USER_CASH_DRAWER_ID = Login.CurrentCashDrawer.USER_CASH_DRAWER_ID;
            }
            else
            {
                objReturn.USER_CASH_DRAWER_ID = 0;
            }

            return objReturn;
        }

        #endregion Deposit

        #region Invoice
        /// <summary>
        /// Create New Invoice object by setting default value
        /// </summary>
        /// <param name="blnIsService"></param>
        /// <param name="strInvoiceTitle"></param>
        /// <returns></returns>
        public static TBL_INVOICE NewInvoice(bool blnIsService, string strInvoiceTitle, int customerId)
        {
            Sequence sequence = Method.GetSequenceId(SequenceType.InvoiceService);
            TBL_INVOICE objInvoice = new TBL_INVOICE();
            objInvoice.RUN_ID = 0;
            objInvoice.CURRENCY_ID = 0;
            objInvoice.CUSTOMER_ID = customerId;
            objInvoice.CYCLE_ID = 0;
            objInvoice.INVOICE_DATE = new DateTime();
            objInvoice.DUE_DATE = new DateTime();
            objInvoice.INVOICE_MONTH = new DateTime();

            objInvoice.START_DATE = new DateTime();
            objInvoice.END_DATE = new DateTime();
            objInvoice.FORWARD_AMOUNT = 0;
            objInvoice.METER_CODE = "";
            objInvoice.START_USAGE = 0;
            objInvoice.END_USAGE = 0;

            objInvoice.TOTAL_USAGE = 0;
            objInvoice.INVOICE_STATUS = (int)InvoiceStatus.Open;
            objInvoice.TOTAL_AMOUNT = 0;
            objInvoice.PAID_AMOUNT = 0;
            objInvoice.SETTLE_AMOUNT = 0;
            objInvoice.IS_SERVICE_BILL = blnIsService;
            objInvoice.PRINT_COUNT = 0;
            objInvoice.DISCOUNT_AMOUNT = 0;
            objInvoice.DISCOUNT_AMOUNT_NAME = "";
            objInvoice.DISCOUNT_USAGE = 0;
            objInvoice.DISCOUNT_USAGE_NAME = "";
            objInvoice.INVOICE_TITLE = strInvoiceTitle;
            objInvoice.INVOICE_NO = GetNextSequence(sequence, true);

            objInvoice.ROW_DATE = DBDataContext.Db.GetSystemDate();
            objInvoice.CUSTOMER_CONNECTION_TYPE_ID = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == customerId).CUSTOMER_CONNECTION_TYPE_ID;
            return objInvoice;
        }

        public static decimal CalculateUsage(TBL_CUSTOMER objCustomer, decimal decMaxUsage, DateTime datRunMonth, out decimal decStartUsage, out decimal decEndUsage)
        {
            decimal decTotalUsageByInvoice = 0;
            //get total usage of each customer

            decStartUsage = 0;
            decEndUsage = 0;

            //select total usage from customer
            var lstUsage = from u in DBDataContext.Db.TBL_USAGEs
                           where
                           DBDataContext.Db.TBL_CUSTOMER_METERs.Where(x => x.METER_ID == u.METER_ID)
                           .Any(x => x.CUSTOMER_ID == objCustomer.CUSTOMER_ID)
                           && u.CUSTOMER_ID == objCustomer.CUSTOMER_ID
                           && u.USAGE_MONTH.Date == datRunMonth.Date
                           select u;

            //if customer have only one usage
            if (lstUsage.Count() == 1)
            {
                var usage = lstUsage.FirstOrDefault();
                //if the meter have renew usage cycle
                if (usage.IS_METER_RENEW_CYCLE)
                {
                    decTotalUsageByInvoice = decMaxUsage - usage.START_USAGE + usage.END_USAGE;
                }
                else
                {
                    decTotalUsageByInvoice = usage.END_USAGE - usage.START_USAGE;
                }
                decStartUsage = usage.START_USAGE;
                decEndUsage = usage.END_USAGE;
            }
            //if this customer chage meter during the month
            else if (lstUsage.Count() > 1)
            {
                decStartUsage = lstUsage.FirstOrDefault().START_USAGE;
                decEndUsage = lstUsage.OrderByDescending(x => x.USAGE_ID).FirstOrDefault().END_USAGE;
                decTotalUsageByInvoice = lstUsage.Sum(u => u.END_USAGE - u.START_USAGE);
            }
            return decTotalUsageByInvoice;
        }

        public static decimal CalculateTaxAmount(TBL_TAX tax, decimal amount, Currency currency)
        {
            decimal taxAmount = 0;
            if (tax != null)
            {
                switch (tax.COMPUTATION)
                {
                    case (int)TaxComputations.FIXED: taxAmount = tax.TAX_AMOUNT; break;
                    case (int)TaxComputations.PERCENTAGE: taxAmount = amount * (tax.TAX_AMOUNT / 100); break;
                    case (int)TaxComputations.PERCENTAGE_INCLUDED_TAX: taxAmount = amount - (amount / (1 + tax.TAX_AMOUNT / 100)); break;
                }
            }
            taxAmount = DataHelper.ParseToDecimal(taxAmount.ToString(currency.FORMAT));
            return taxAmount;
        }

        public static void AdjustInvoiceAmount(decimal adjustAmount, decimal adjustUsage, int intMeterId, TBL_INVOICE objInvoice, string strAdjustReason, int oldCurrencyId, int newCurrencyId, decimal oldusage)
        {
            DateTime now = DBDataContext.Db.GetSystemDate();
            var exchange = new ExchangeRateLogic().findLastExchangeRate(now, oldCurrencyId);
            var objDetail = DBDataContext.Db.TBL_INVOICE_DETAILs.FirstOrDefault(x => x.INVOICE_ID == objInvoice.INVOICE_ID);
            if (adjustAmount < 0)
            {
                exchange = new TBL_EXCHANGE_RATE() { CREATE_ON = objDetail.EXCHANGE_RATE_DATE, EXCHANGE_RATE = objDetail.EXCHANGE_RATE };
            }
            // Adjust reason with changing new Currency Id
            string str = newCurrencyId != oldCurrencyId ? "​​ (ប្តូររូបិយប័ណ្ណពី " + Lookup.GetCurrencyName(oldCurrencyId) + " ទៅ " + Lookup.GetCurrencyName(newCurrencyId) + ")" : "";

            // ADJUSTMENT --
            //Insert all adjust transaction to TBL_INVOICE_ADJUSTMENT
            TBL_INVOICE_ADJUSTMENT objInvoiceAdjust = new TBL_INVOICE_ADJUSTMENT()
            {
                INVOICE_ID = objInvoice.INVOICE_ID,
                CREATE_BY = SoftTech.Security.Logic.Login.CurrentLogin.LOGIN_NAME,
                CREATE_ON = now,
                USER_CASH_DRAWER_ID = Login.CurrentCashDrawer.USER_CASH_DRAWER_ID,
                ADJUST_REASON = strAdjustReason + str,
                BEFORE_ADJUST_AMOUNT = objInvoice.TOTAL_AMOUNT,
                ADJUST_AMOUNT = adjustAmount,
                METER_ID = intMeterId,
                BEFORE_ADJUST_USAGE = oldusage,
                ADJUST_USAGE = adjustUsage,
                EXCHANGE_RATE = exchange.EXCHANGE_RATE,
                EXCHANGE_RATE_DATE = exchange.CREATE_ON,
                TAX_ADJUST_AMOUNT = 0,
                INVOICE_ITEM_ID = objDetail.INVOICE_ITEM_ID
            };

            DBDataContext.Db.TBL_INVOICE_ADJUSTMENTs.InsertOnSubmit(objInvoiceAdjust);
            DBDataContext.Db.SubmitChanges();

            // INVOICE_DETAIL for ADJUSTMENT --
            TBL_INVOICE_DETAIL objInvoiceDetail = new TBL_INVOICE_DETAIL();
            objInvoiceDetail.INVOICE_ID = objInvoice.INVOICE_ID;
            objInvoiceDetail.REF_NO = GetNextSequence(Sequence.Adjustment, true);
            objInvoiceDetail.INVOICE_ITEM_ID = (int)InvoiceItem.Adjustment;
            objInvoiceDetail.AMOUNT = adjustAmount;
            objInvoiceDetail.CHARGE_DESCRIPTION = strAdjustReason + str;

            if (objInvoice.IS_SERVICE_BILL == false)
            {
                objInvoiceDetail.PRICE = DBDataContext.Db.TBL_INVOICE_DETAILs.Where(x => x.INVOICE_ID == objInvoice.INVOICE_ID && (x.INVOICE_ITEM_ID == 1 || x.INVOICE_ITEM_ID == -1)).OrderByDescending(x => x.INVOICE_DETAIL_ID).FirstOrDefault().PRICE;
            }
            else
            {
                objInvoiceDetail.PRICE = DBDataContext.Db.TBL_INVOICE_DETAILs.Where(x => x.INVOICE_ID == objInvoice.INVOICE_ID).OrderByDescending(x => x.INVOICE_DETAIL_ID).FirstOrDefault().PRICE;
            }

            objInvoiceDetail.EXCHANGE_RATE = exchange.EXCHANGE_RATE;
            objInvoiceDetail.EXCHANGE_RATE_DATE = exchange.CREATE_ON;
            objInvoiceDetail.TRAN_DATE = now;
            DBDataContext.Db.TBL_INVOICE_DETAILs.InsertOnSubmit(objInvoiceDetail);
            DBDataContext.Db.SubmitChanges();

            // UPDATE INVOICE -- 
            var objInvoiceTMP = DBDataContext.Db.TBL_INVOICEs.FirstOrDefault(row => row.INVOICE_ID == objInvoice.INVOICE_ID);
            objInvoice._CopyTo(objInvoiceTMP);
            objInvoiceTMP.TOTAL_AMOUNT = newCurrencyId != oldCurrencyId ? objInvoiceTMP.SETTLE_AMOUNT + objInvoiceDetail.AMOUNT : objInvoiceTMP.TOTAL_AMOUNT + objInvoiceDetail.AMOUNT;
            objInvoiceTMP.SETTLE_AMOUNT = UIHelper.Round(objInvoiceTMP.TOTAL_AMOUNT, objInvoice.CURRENCY_ID);
            objInvoiceTMP.INVOICE_STATUS = (objInvoiceTMP.SETTLE_AMOUNT == 0) ? (int)InvoiceStatus.Close : objInvoiceTMP.INVOICE_STATUS = (int)InvoiceStatus.Open;
            objInvoiceTMP.ROW_DATE = now; // update ROW_DATE for B24 Intergration
            DBDataContext.Db.SubmitChanges();
        }

        #endregion Invoice

        #region Utility
        public static Dictionary<Utility, string> utilities = null;
        public static Dictionary<Utility, string> Utilities
        {
            get
            {
                if (utilities == null)
                {
                    utilities = new Dictionary<Utility, string>();
                    var data = DBDataContext.Db.TBL_UTILITies.ToList();
                    foreach (TBL_UTILITY u in data)
                    {
                        utilities.Add((Utility)u.UTILITY_ID, u.UTILITY_VALUE);
                    }
                }
                return utilities;
            }
            set
            {
                utilities = value;
            }
        }
        public static string GetUtilityValue(Utility u)
        {
            return Utilities[u];
        }
        public static void SetUtilityValue(Utility u, string value)
        {
            Utilities[u] = value;
            DBDataContext.Db.TBL_UTILITies
                .FirstOrDefault(x => x.UTILITY_ID == (int)u)
                .UTILITY_VALUE = value;
            DBDataContext.Db.SubmitChanges();
        }
        #endregion Utility

        #region Billing Cycle
        public static DateTime GetNextBillingMonth(int cycleId)
        {
            DateTime datBillingMonth = UIHelper._DefaultDate;
            TBL_BILLING_CYCLE objCycle = DBDataContext.Db.TBL_BILLING_CYCLEs.FirstOrDefault(c => c.CYCLE_ID == cycleId);
            if (objCycle == null)
            {
                return datBillingMonth;
            }
            TBL_RUN_BILL objRun = (from br in DBDataContext.Db.TBL_RUN_BILLs
                                   where br.CYCLE_ID == objCycle.CYCLE_ID && br.IS_ACTIVE
                                   orderby br.RUN_ID descending
                                   select br).FirstOrDefault();
            //if not yet run in this cycle
            if (objRun == null)
            {
                // if not have any run.
                var lstCustomerRunned = from c in DBDataContext.Db.TBL_CUSTOMERs
                                        join i in DBDataContext.Db.TBL_INVOICEs on c.CUSTOMER_ID equals i.CUSTOMER_ID
                                        join r in DBDataContext.Db.TBL_RUN_BILLs on i.RUN_ID equals r.RUN_ID
                                        where c.BILLING_CYCLE_ID == objCycle.CYCLE_ID && r.IS_ACTIVE
                                        orderby r.BILLING_MONTH descending
                                        select new
                                        {
                                            r.BILLING_MONTH
                                        };
                if (lstCustomerRunned.Count() == 0)
                {
                    DateTime objActivate = (from b in DBDataContext.Db.TBL_BILLING_CYCLEs
                                            join c in DBDataContext.Db.TBL_CUSTOMERs on b.CYCLE_ID equals c.BILLING_CYCLE_ID
                                            join cm in DBDataContext.Db.TBL_CUSTOMER_METERs on c.CUSTOMER_ID equals cm.CUSTOMER_ID
                                            where b.CYCLE_ID == objCycle.CYCLE_ID
                                                 && cm.IS_ACTIVE
                                                 && (c.STATUS_ID == (int)CustomerStatus.Active || c.STATUS_ID == (int)CustomerStatus.Blocked)
                                            orderby c.ACTIVATE_DATE
                                            select c.ACTIVATE_DATE).FirstOrDefault();
                    DateTime datActivate = objActivate != new DateTime() ? (DateTime)objActivate : DBDataContext.Db.GetSystemDate();
                    datBillingMonth = new DateTime(datActivate.Year, datActivate.Month, 1);
                }
                else
                {
                    datBillingMonth = lstCustomerRunned.FirstOrDefault().BILLING_MONTH.AddMonths(1);
                }
            }
            else
            {
                datBillingMonth = objRun.BILLING_MONTH.AddMonths(1);
            }
            return datBillingMonth;
        }
        public static DateTime GetNextBillingMonth(int cycleId, ref DateTime startDate, ref DateTime endDate)
        {
            // get billing month.
            DateTime datBillingMonth = GetNextBillingMonth(cycleId);

            TBL_BILLING_CYCLE objCycle = DBDataContext.Db.TBL_BILLING_CYCLEs.FirstOrDefault(c => c.CYCLE_ID == cycleId);
            if (objCycle == null)
            {
                return datBillingMonth;
            }
            //if run end of month.
            if (objCycle.IS_END_MONTH)
            {
                startDate = new DateTime(datBillingMonth.Year, datBillingMonth.Month, 1);
                endDate = startDate.AddMonths(1).AddDays(-1);
            }
            else
            {
                startDate = new DateTime(datBillingMonth.Year, datBillingMonth.Month, objCycle.END_DAY).AddDays(1);
                endDate = new DateTime(startDate.AddMonths(1).Year, startDate.AddMonths(1).Month, objCycle.END_DAY);
            }
            return datBillingMonth;
        }
        #endregion Billinc Cycle

        #region None Usage
        public static int GetNoneUsageCustomer(int intBillingCycleId, DateTime datRuningMonth)
        {
            int MAX_DAY_TO_IGNORE_BILLING = DataHelper.ParseToInt(Utilities[Utility.MAX_DAY_TO_IGNORE_BILLING]);
            //select customer that not yet input usage
            var lstNoUsageCustomer = from c in DBDataContext.Db.TBL_CUSTOMERs
                                     join cusmeter in DBDataContext.Db.TBL_CUSTOMER_METERs on c.CUSTOMER_ID equals cusmeter.CUSTOMER_ID
                                     where c.BILLING_CYCLE_ID == intBillingCycleId && c.IS_POST_PAID
                                             && cusmeter.IS_ACTIVE
                                             && (c.STATUS_ID == (int)CustomerStatus.Active || c.STATUS_ID == (int)CustomerStatus.Blocked)
                                             && !(DBDataContext.Db.TBL_USAGEs.Where(u => u.USAGE_MONTH.Date == datRuningMonth.Date && u.COLLECTOR_ID != 0).Any(x => x.CUSTOMER_ID == c.CUSTOMER_ID && x.METER_ID == cusmeter.METER_ID))
                                             && (DBDataContext.Db.GetSystemDate() - c.ACTIVATE_DATE).TotalDays >= MAX_DAY_TO_IGNORE_BILLING
                                     select c.CUSTOMER_ID;
            return lstNoUsageCustomer.Count();
        }

        public static int GetNoneUsageCustomerPrepaid(int intBillingCycleId, DateTime datRuningMonth)
        {
            int MAX_DAY_TO_IGNORE_BILLING = DataHelper.ParseToInt(Utilities[Utility.MAX_DAY_TO_IGNORE_BILLING]);
            //select customer that not yet input usage
            var lstNoUsageCustomer = from c in DBDataContext.Db.TBL_CUSTOMERs
                                     join cusmeter in DBDataContext.Db.TBL_CUSTOMER_METERs on c.CUSTOMER_ID equals cusmeter.CUSTOMER_ID
                                     where c.BILLING_CYCLE_ID == intBillingCycleId && c.IS_POST_PAID == false
                                             && cusmeter.IS_ACTIVE
                                             && (c.STATUS_ID == (int)CustomerStatus.Active || c.STATUS_ID == (int)CustomerStatus.Blocked)
                                             && !(DBDataContext.Db.TBL_USAGEs.Where(u => u.USAGE_MONTH.Date == datRuningMonth.Date && u.COLLECTOR_ID != 0).Any(x => x.CUSTOMER_ID == c.CUSTOMER_ID && x.METER_ID == cusmeter.METER_ID))
                                             && (DBDataContext.Db.GetSystemDate() - c.ACTIVATE_DATE).TotalDays >= MAX_DAY_TO_IGNORE_BILLING
                                     select c.CUSTOMER_ID;
            return lstNoUsageCustomer.Count();
        }
        #endregion None Usage

        #region Unknown Meter

        /// <summary>
        /// Remove unknown meter from TBL_UNKNOWN_METER
        /// when activate new customer or change meter
        /// </summary>
        /// <param name="strMeterCode"></param>
        public static void RemoveUnknownMeter(string strMeterCode)
        {
            string strSQL = "DELETE FROM TBL_METER_UNKNOWN WHERE METER_CODE={0};";
            DBDataContext.Db.ExecuteCommand(strSQL, strMeterCode);
        }

        #endregion Unknown Meter


        public static string FormatMeterCode(string meterCode)
        {
            return meterCode.Replace(" ", "")
                            .PadLeft(METER_DIGIT, '0')
                            .ToUpper();
        }

        public static string FormatCustomerCode(string customerCode)
        {
            return customerCode.Replace(" ", "")
                               .PadLeft(CUSTOMER_DIGIT, '0')
                               .ToUpper();
        }
        public static string FormatPrice(decimal decValue, int currencyId)
        {
            string str = "";
            if (currencyId == 0 || currencyId == 1)
            {
                str = decValue.ToString("N0");
            }
            else
            {
                str = decValue.ToString("#,##0.#####");
            }
            return str;
        }
        /// <summary>
        /// Get Max number of each meter
        /// </summary>
        /// <param name="decStart"></param>
        /// <returns></returns>
        public static decimal GetMaxNumber(int decStart)
        {
            decimal decReturn = 0;
            string strStartUsage = decStart.ToString().Trim();

            int intDigit = strStartUsage.Length;
            string strMax = new string('9', intDigit);
            decReturn = decimal.Parse(strMax);

            return decReturn;
        }

        public static decimal GetTotalUsage(decimal startUsage, decimal endUsage, decimal multiplier, bool isMeterRenewCycle)
        {
            decimal total = 0;
            if (isMeterRenewCycle)
            {
                decimal maxUsage = GetMaxNumber((int)startUsage);
                total = (maxUsage - startUsage) + endUsage + 1;
            }
            else
            {
                total = endUsage - startUsage;
            }
            return Math.Floor(total * multiplier);
        }

        public static decimal GetTotalUsage(TBL_USAGE objUsage)
        {
            return GetTotalUsage(objUsage.START_USAGE, objUsage.END_USAGE, objUsage.MULTIPLIER, objUsage.IS_METER_RENEW_CYCLE);
        }

        public static string GetFILE_HEADER(ItemType type)
        {
            return GetRecordChecksum(type.ToString().ToUpper(), "EPF");
        }

        public static string GetMETER_REGCODE(string METER_CODE, bool IS_DIGITAL)
        {
            METER_CODE = FormatMeterCode(METER_CODE);
            //Declarations
            Byte[] originalBytes;
            Byte[] encodedBytes;
            MD5 md5;
            //Instantiate MD5CryptoServiceProvider, get bytes for original password and compute hash (encoded password)
            md5 = new MD5CryptoServiceProvider();
            originalBytes = UnicodeEncoding.Default.GetBytes("!" + METER_CODE + "+" + IS_DIGITAL.ToString().ToUpper()[0]);
            encodedBytes = md5.ComputeHash(originalBytes);
            //Convert encoded bytes back to a 'readable' string
            return BitConverter.ToString(encodedBytes).Replace("-", "");
        }

        public static string GetIR_REGCODE(string code)
        {
            return GetRecordChecksum(code, "READER");
        }

        public static string GetSM_REGCODE(DateTime activateDate, string areaCode, int month, bool lockMet, int cus, string server_cpu)
        {
            string conf = string.Concat(
                                activateDate.ToString("yyyyMMdd")
                               , areaCode
                               , cus
                               , month
                               , lockMet.ToString());

            string tmp = GetRecordChecksum(conf, server_cpu);

            string serial = string.Concat(tmp.Substring(0, 12),
                            month.ToString("X").PadLeft(2, '0')
                            , Convert.ToInt16(lockMet).ToString("X").PadLeft(2, '0')
                            , cus.ToString("X").PadLeft(4, '0'));

            for (int i = 5; i < serial.Length; i += 5)
            {
                serial = serial.Insert(i++, "-");
            }

            return serial;
        }

        public static string GetRecordChecksum(string value, string chk)
        {
            //Add more salt to encrypt
            value = value + "2&mp@$3Gh&a5&^$" + chk;
            MD5 md5 = new MD5CryptoServiceProvider(); // MD5 algorithm
            byte[] checksum = md5.ComputeHash(new UnicodeEncoding().GetBytes(value));
            return (BitConverter.ToString(checksum).Replace("-", string.Empty));
        }
        public static Sequence GetSequenceId(SequenceType seqType)
        {
            return (Sequence)DBDataContext.Db.TBL_SEQUENCE_TYPEs.FirstOrDefault(x => x.SEQUENCE_TYPE_ID == (int)seqType).SEQUENCE_ID;
        }

        public static string GetNextSequence(Sequence seq, bool update)
        {
            if (update)
            {
                return DBDataContext.Db.ExecuteQuery<string>($"EXEC dbo.NEXT_SEQUENCE @SEQUENCE_ID ={(int)seq}").FirstOrDefault();
            }

            string tmp = DBDataContext.Db.GET_SEQUENCE((int)seq, 0, DateTime.Now);
            if (update)
            {
                TBL_SEQUENCE objSeq = DBDataContext.Db.TBL_SEQUENCEs.FirstOrDefault(row => row.SEQUENCE_ID == (int)seq);
                //objSeq.Refresh();
                DateTime now = DBDataContext.Db.GetSystemDate();
                // by default increase by 1
                objSeq.VALUE += 1;
                // else reset to 1
                if (objSeq.FORMAT.Contains("{Y") && objSeq.DATE.Year != now.Year)
                {
                    objSeq.VALUE = 1;
                }
                if (objSeq.FORMAT.Contains("{M") && objSeq.DATE.Month != now.Month)
                {
                    objSeq.VALUE = 1;
                }
                objSeq.DATE = now;
                DBDataContext.Db.SubmitChanges();
            }
            return tmp;
        }

        public static CrystalReportHelper GetInvoiceReport(string invoice = "ReportInvoice.rpt")
        {
            return new CrystalReportHelper(invoice, "INVOICE_USAGE", "REPORT_INVOICE_HISTORY");
        }

        #region CheckSum File

        public static string CheckSumFile(string fileName)
        {
            using (var stream = new BufferedStream(File.OpenRead(fileName)))
            {
                SHA256Managed sha = new SHA256Managed();
                byte[] checksum = sha.ComputeHash(stream);
                return BitConverter.ToString(checksum).Replace("-", String.Empty).ToLower();
            }
        }

        public static void WriteFile(byte[] file, string fileName, string filePath)
        {
            FileStream fs = new FileStream(filePath + fileName, FileMode.Create);
            fs.Write(file, 0, file.Length);
            fs.Close();
        }
        #endregion

        #region Village
        public static List<TBL_LICENSE_VILLAGE> villageByCode(string villageCode)
        {
            var listId = ParseToStringVillageCode(villageCode);
            return villageByCode(listId);
        }
        public static List<TBL_LICENSE_VILLAGE> villageByCode(List<string> villageCode)
        {
            return DBDataContext.Db.TBL_LICENSE_VILLAGEs
                .Where(x => villageCode.Count > 0 && villageCode.Contains(x.VILLAGE_CODE))
                .OrderBy(x => x.VILLAGE_CODE).ToList();
        }
        public static List<string> ParseToStringVillageCode(string villageCode)
        {
            return villageCode.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }
        public static string ConcatVillageCode(IEnumerable<TBL_LICENSE_VILLAGE> villages)
        {
            return string.Join(",", villages.Select(x => x.VILLAGE_CODE.ToString()).ToArray());
        }
        public static string ConcatVillageName(IEnumerable<TBL_LICENSE_VILLAGE> villages)
        {
            var r = from lv in villages
                    join v in DBDataContext.Db.TLKP_VILLAGEs on lv.VILLAGE_CODE equals v.VILLAGE_CODE
                    select new
                    {
                        lv.VILLAGE_CODE,
                        v.VILLAGE_NAME
                    };
            return string.Join(",", r.Select(x => x.VILLAGE_NAME.ToString()).ToArray());

        }
        #endregion 

        public static void ChangeIEVersion()
        {
            int BrowserVer, RegVal;

            // get the installed IE version
            using (WebBrowser Wb = new WebBrowser())
                BrowserVer = Wb.Version.Major;

            // set the appropriate IE version
            if (BrowserVer >= 11)
                RegVal = 11001;
            else if (BrowserVer == 10)
                RegVal = 10001;
            else if (BrowserVer == 9)
                RegVal = 9999;
            else if (BrowserVer == 8)
                RegVal = 8888;
            else
                RegVal = 7000;

            // set the actual key
            using (RegistryKey Key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION", RegistryKeyPermissionCheck.ReadWriteSubTree))
                if (Key.GetValue(System.Diagnostics.Process.GetCurrentProcess().ProcessName + ".exe") == null)
                    Key.SetValue(System.Diagnostics.Process.GetCurrentProcess().ProcessName + ".exe", RegVal, RegistryValueKind.DWord);
        }

        public static void OpenFile(string _fileName, Binary attachment)
        {
            try
            {
                string fileName = System.IO.Path.Combine(Application.StartupPath + "\\TEMP", _fileName);
                using (FileStream stream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.Read))
                {
                    stream.Write(attachment.ToArray(), 0, attachment.Length);
                }
                // try opening newfile after complete  
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo(fileName);
                System.Diagnostics.Process.Start(startInfo);
            }
            catch { }
        }
        static readonly string[] SizeSuffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
        public static string SizeSuffix(Int64 value)
        {
            if (value < 0)
            {
                return "-" + SizeSuffix(-value);
            }
            if (value == 0)
            {
                return "0.0 bytes";
            }
            int mag = (int)Math.Log(value, 1024);
            decimal adjustedSize = (decimal)value / (1L << (mag * 10));

            return string.Format("{0:n1} {1}", adjustedSize, SizeSuffixes[mag]);
        }

        public static decimal CalculatorPowerKW(int customerID, decimal powerKW)
        {
            decimal amount​ = 0;
            TBL_CUSTOMER obj = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == customerID);
            TBL_PRICE currency = DBDataContext.Db.TBL_PRICEs.FirstOrDefault(x => x.PRICE_ID == obj.PRICE_ID);
            TBL_PRICE_DETAIL price = DBDataContext.Db.TBL_PRICE_DETAILs.OrderByDescending(x => x.PRICE_DETAIL_ID).FirstOrDefault(x => x.PRICE_ID == obj.PRICE_ID);
            if (obj.IS_MV == true)
            {
                if (powerKW >= 275 && powerKW <= 1000)
                {
                    amount = (powerKW * DataHelper.ParseToDecimal("0.6") * DataHelper.ParseToDecimal("0.9") * 10 * 25 * DataHelper.ParseToDecimal("0.5") * price.PRICE);
                }
                else if (powerKW >= 1001 && powerKW <= 3000)
                {
                    amount = (powerKW * DataHelper.ParseToDecimal("0.6") * DataHelper.ParseToDecimal("0.9") * 10 * 25 * DataHelper.ParseToDecimal("0.4") * price.PRICE);
                }
                else if (powerKW >= 3001 && powerKW <= 10000)
                {
                    amount = (powerKW * DataHelper.ParseToDecimal("0.6") * DataHelper.ParseToDecimal("0.9") * 10 * 25 * DataHelper.ParseToDecimal("0.3") * price.PRICE);
                }
            }
            else
            {
                if (powerKW >= 50 && powerKW <= 275)
                {
                    amount = (powerKW * DataHelper.ParseToDecimal("0.7") * DataHelper.ParseToDecimal("0.9") * 10 * 25 * DataHelper.ParseToDecimal("0.5") * price.PRICE);
                }
            }
            return amount;
        }
    }
}
