﻿using EPower.Base.Models;
using HB01.Domain.Enums;
using HB01.Domain.Models.Transactions;
using HB01.Helpers;
using HB01.Logics;
using SoftTech;
using SoftTech.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace EPower.Base.Logic
{
    internal class PushDepositLogic : Integration<Deposit>
    {
        public static PushDepositLogic Instance { get; } = new PushDepositLogic();

        public override List<Deposit> AvailableRecords(DateTime startDate, DateTime endDate)
        {
            var dAcc = AccountingLogic.Instance.AccountConfigId(AccountConfig.DEPOSIT_ACCOUNT).FirstOrDefault();
            var cAcc = AccountingLogic.Instance.AccountConfigId(AccountConfig.DEPOSIT_ACCOUNT_CASH).FirstOrDefault();
            if (dAcc == 0 || cAcc == 0)
            {
                return null;
            }

            var postedItems = from j in DBDataContext.Db.TBL_JOURNAL_ENTRies
                              join ja in DBDataContext.Db.TBL_JOURNAL_ENTRY_ACCOUNTs on j.JOURNAL_ENTRY_ID equals ja.JOURNAL_ENTRY_ID
                              where j.IS_ACTIVE && j.REFERENCE_TYPE_ID == (int)ReferenceType.Deposit
                              select new
                              {
                                  j.JOURNAL_ENTRY_ID,
                                  ja.REFERENCE_ID
                              };

            var q = from d in DBDataContext.Db.TBL_CUS_DEPOSITs
                    join c in DBDataContext.Db.TLKP_CURRENCies on d.CURRENCY_ID equals c.CURRENCY_ID
                    join j in postedItems on d.CUS_DEPOSIT_ID equals j.REFERENCE_ID into l
                    from je in l.DefaultIfEmpty()
                    where d.IS_PAID && d.DEPOSIT_DATE >= startDate && d.DEPOSIT_DATE <= endDate
                        && je.JOURNAL_ENTRY_ID == null
                    select new Deposit
                    {
                        DepositId = d.CUS_DEPOSIT_ID,
                        DepositAcc = dAcc,
                        CashAcc = cAcc,
                        Amount = d.AMOUNT,
                        CurrencyId = c.EXTERNAL_CURRENCY_ID,
                        DepositDate = d.DEPOSIT_DATE,
                        InvoiceExchangeRate = d.EXCHANGE_RATE,
                        PaymentExchangeRate = d.EXCHANGE_RATE
                    };

            return q.ToList();
        }

        public override List<TBL_JOURNAL_ENTRY_ACCOUNT> EntryItems(List<Deposit> list, int EntryId)
        {
            var items = (from l in list
                         select new
                         {
                             CASH = new TBL_JOURNAL_ENTRY_ACCOUNT
                             {
                                 JOURNAL_ENTRY_ID = EntryId,
                                 ACCOUNT_ID = l.CashAcc,
                                 AMOUNT = l.Amount,
                                 REFERENCE_ID = l.DepositId
                             },
                             DEPOSIT = new TBL_JOURNAL_ENTRY_ACCOUNT
                             {
                                 JOURNAL_ENTRY_ID = EntryId,
                                 ACCOUNT_ID = l.DepositAcc,
                                 AMOUNT = l.Amount,
                                 REFERENCE_ID = l.DepositId
                             }
                         }).ToList();
            return items.Select(x => x.DEPOSIT).Concat(items.Select(x => x.CASH)).ToList();
        }

        public override List<JournalEntryLine> EntryLines(List<Deposit> list)
        {
            var line = (from l in list
                        group l by new { l.CurrencyId, l.PaymentExchangeRate } into g
                        select new
                        {
                            DEPOSIT = new JournalEntryLine
                            {
                                AccountId = g.FirstOrDefault().DepositAcc,
                                Title = $"Deposit",
                                NetAmount = g.Sum(x => x.Amount) * g.Key.PaymentExchangeRate,
                                CurrencyId = g.Key.CurrencyId,
                                Active = true,
                                RefId = "",
                                CurrencyAmount = g.Sum(x => x.Amount),
                                ExchangeRate = g.Key.PaymentExchangeRate,
                                ExchangeRateId = -1
                            },
                            CASH = new JournalEntryLine
                            {
                                AccountId = g.FirstOrDefault().CashAcc,
                                Title = $"Cash",
                                NetAmount = g.Sum(x => x.Amount) * g.Key.PaymentExchangeRate,
                                CurrencyId = g.Key.CurrencyId,
                                Active = true,
                                RefId = "",
                                CurrencyAmount = g.Sum(x => x.Amount),
                                ExchangeRate = g.Key.PaymentExchangeRate,
                                ExchangeRateId = -1
                            },
                        }).ToList();
            return line.Select(x => x.DEPOSIT).Concat(line.Select(x => x.CASH)).ToList();
        }

        public override void PushJournal(DateTime startDate, DateTime endDate)
        {
            int k = 1;
            var currDate = DBDataContext.Db.GetSystemDate();
            var tran = (from t in AvailableRecords(startDate, endDate)
                        group t by new { DepositDate = t.DepositDate.Date, t.CurrencyId } into g
                        orderby g.Key.DepositDate
                        select new Transaction
                        {
                            Date = g.Key.DepositDate,
                            CurrencyId = g.Key.CurrencyId,
                            Transactions = g.ToList()
                        }).ToList();

            var journalId = AccountingLogic.Instance.Journal(AccountingConfig.JOURNAL_DEPOSIT).Id;

            foreach (var t in tran)
            {
                var tranOption = new TransactionOptions()
                {
                    IsolationLevel = IsolationLevel.ReadUncommitted,
                    Timeout = TimeSpan.MaxValue
                };
                TBL_JOURNAL_ENTRY afterUpdateJournal = new TBL_JOURNAL_ENTRY();
                TBL_JOURNAL_ENTRY beforeUpdateJournal = new TBL_JOURNAL_ENTRY();

                try
                {
                    using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required, tranOption))
                    {
                        if (t.Transactions.Count > 0)
                        {
                            Runner.Instance.Text = $"ចុះបញ្ជីប្រាក់កក់អតិថិជន {k++}/{tran.Count}";

                            //Sumit E-Power
                            var eJournal = new TBL_JOURNAL_ENTRY
                            {
                                REF_NO = "",
                                CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                                CREATE_ON = currDate,
                                ENTRY_DATE = t.Date,
                                IS_ACTIVE = true,
                                CURRENCY_ID = t.CurrencyId,
                                REFERENCE_TYPE_ID = (int)ReferenceType.Deposit,
                                HB01_JOURNAL_ID = 0,
                                NOTE = ""
                            };
                            DBDataContext.Db.TBL_JOURNAL_ENTRies.InsertOnSubmit(eJournal);
                            DBDataContext.Db.SubmitChanges();
                            var line = EntryItems(t.Transactions, eJournal.JOURNAL_ENTRY_ID);
                            DBDataContext.Db.BulkCopy(line._ToDataTable(), $"HB01.{nameof(TBL_JOURNAL_ENTRY_ACCOUNT)}");
                            DBDataContext.Db.SubmitChanges();
                            eJournal._CopyTo(afterUpdateJournal);
                            eJournal._CopyTo(beforeUpdateJournal);

                            //Submit Pointer
                            var journal = new JournalEntry()
                            {
                                CompanyId = Current.CompanyId,
                                Active = true,
                                CurrencyId = t.CurrencyId,
                                EntryDate = t.Date,
                                RefNo = "From E-Power",
                                JournalId = journalId,
                                SourceRefId = "CusDepositIds." + string.Join(",", t.Transactions.Select(x => x.DepositId).ToList()),
                                Posted = true,
                                Source = PostingSources.External,
                                Lines = EntryLines(t.Transactions),
                                Amount = t.Transactions.Sum(x => x.Amount)
                            };

                            var j = JournalEntryLogic.Instance.PostExternal(journal);
                            afterUpdateJournal.HB01_JOURNAL_ID = j.Id;
                            afterUpdateJournal.REF_NO = j.EntryNo;
                        }
                        transaction.Complete();
                    }
                    DBDataContext.Db.Update(beforeUpdateJournal, afterUpdateJournal);
                    DBDataContext.Db.SubmitChanges();
                }
                catch (Exception e)
                {
                    //MsgBox.ShowInformation(e.Message);
                }
            }
        }
    }
}
