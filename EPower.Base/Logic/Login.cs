﻿using System.Collections.Generic;
using System.Linq;
using SoftTech;

namespace EPower.Base.Logic
{
    public class Login
    {
        public static TBL_LOGIN CurrentLogin
        {
            get
            {
                return SoftTech.Security.Logic.Login.CurrentLogin;
            }
        }

        public static TBL_USER_CASH_DRAWER CurrentCashDrawer
        {
            get
            { 
                return DBDataContext.Db.TBL_USER_CASH_DRAWERs.FirstOrDefault(
                                        row => row.USER_ID == CurrentLogin.LOGIN_ID
                                        && row.IS_ACTIVE && row.IS_CLOSED==false);
            }
        } 

        public static IEnumerable<TBL_CASH_DRAWER> CashDrawersToOpen
        {
            get{
                return DBDataContext.Db.TBL_CASH_DRAWERs.Where(c=> c.IS_ACTIVE && 
                    !DBDataContext.Db.TBL_USER_CASH_DRAWERs.Where(uc=> uc.IS_CLOSED==false).Select(uc=>uc.CASH_DRAWER_ID).Contains(c.CASH_DRAWER_ID)
                    && DBDataContext.Db.TBL_LOGIN_CASH_DRAWERs.Where(l => l.LOGIN_ID == CurrentLogin.LOGIN_ID).Select(l => l.CASH_DRAWER_ID).Contains(c.CASH_DRAWER_ID));
            }
        }
    }
}
