﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using EPower.Base.Properties;
using SoftTech;
using SoftTech.Helper;

namespace EPower.Base.Logic
{
    public class Lookup
    {
        public static DataTable GetCurrencies()
        {
            return DBDataContext.Db.TLKP_CURRENCies.Select(x => new { x.CURRENCY_ID, x.CURRENCY_NAME })._ToDataTable();
        }

        public static DataTable GetReactiveRules()
        {
            return DBDataContext.Db.TBL_REACTIVE_RULEs.Where(x => x.IS_ACTIVE)._ToDataTable();
        }
        public static DataTable GetCustomerStatuses()
        {
            return DBDataContext.Db.TLKP_CUS_STATUS._ToDataTable();
        }
        public static DataTable GetCustomCustomerStatused()
        {
            string query = "SELECT CUS_STATUS_ID=-1,CUS_STATUS_NAME=N'"+Resources.ACTIVATE_AND_BLOCKED_CUSTOMER+"' UNION ALL SELECT CUS_STATUS_ID=-2,CUS_STATUS_NAME=N'"+Resources.CLOSE_AND_CANCEL_CUSTOMER+"' UNION ALL SELECT * FROM TLKP_CUS_STATUS";
            return DBDataContext.Db.ExecuteQuery<TLKP_CUS_STATUS>(query)._ToDataTable();
        }
        public static DataTable GetCustomerTypes()
        {
            return DBDataContext.Db.TBL_CUSTOMER_TYPEs.Where(t => t.IS_ACTIVE == true || t.CUSTOMER_TYPE_ID == (int)CustomerType.UsedInProduction)._ToDataTable();
        }
        public static DataTable GetBillingCycles()
        {
            return DBDataContext.Db.TBL_BILLING_CYCLEs.Where(row => row.IS_ACTIVE)._ToDataTable();
        }

        public static DataTable GetPrices()
        {
            var q = from p in DBDataContext.Db.TBL_PRICEs
                    join c in DBDataContext.Db.TLKP_CURRENCies on p.CURRENCY_ID equals c.CURRENCY_ID
                    where p.IS_ACTIVE
                    select new
                    {
                        p.PRICE_ID,
                        PRICE_NAME = p.PRICE_NAME +" ("+c.CURRENCY_SING+")"
                    };
            return q._ToDataTable();
        }

        public static DataTable GetAreas(){
            return DBDataContext.Db.TBL_AREAs.Where(x => x.IS_ACTIVE)
                                             .OrderBy(x => x.AREA_CODE)
                                             ._ToDataTable();
        }

        public static DataTable GetPoles(int areaId)
        {
            return DBDataContext.Db.TBL_POLEs.Where(x =>(areaId==0 || x.AREA_ID == areaId) && x.IS_ACTIVE)
                                             .Select(x => new { x.POLE_ID, x.POLE_CODE })
                                             .OrderBy(x => x.POLE_CODE)
                                             ._ToDataTable();
        }

        public static DataTable GetBoxes(int poleId)
        {
            return DBDataContext.Db.TBL_BOXes.Where(x => x.POLE_ID == poleId)
                                 .Select(x => new { x.BOX_ID, x.BOX_CODE })
                                 .OrderBy(x => x.BOX_CODE)
                                 ._ToDataTable();
        } 

        public static DataTable GetTransformers()
        {
            return DBDataContext.Db.TBL_TRANSFORMERs.Where(x => x.IS_ACTIVE)
                                                    .OrderBy(x => x.TRANSFORMER_CODE)
                                                    ._ToDataTable();
        }

        public static DataTable GetEmployeeCollectors()
        {
            IEnumerable<TBL_EMPLOYEE> lstCollector = from e in DBDataContext.Db.TBL_EMPLOYEEs
                                                     join p in DBDataContext.Db.TBL_EMPLOYEE_POSITIONs on e.EMPLOYEE_ID equals p.EMPLOYEE_ID
                                                     where p.POSITION_ID == (int)EmpPosition.Collector
                                                     && e.IS_ACTIVE
                                                     orderby e.EMPLOYEE_NAME
                                                     select e;
            return lstCollector._ToDataTable();
        } 

        public static DataTable GetEmployeeDistributers()
        {
            IEnumerable<TBL_EMPLOYEE> lstBiller = from e in DBDataContext.Db.TBL_EMPLOYEEs
                                                  join p in DBDataContext.Db.TBL_EMPLOYEE_POSITIONs on e.EMPLOYEE_ID equals p.EMPLOYEE_ID
                                                  where p.POSITION_ID == (int)EmpPosition.Biller
                                                  && e.IS_ACTIVE
                                                  orderby e.EMPLOYEE_NAME
                                                  select e;
            return lstBiller._ToDataTable();
        }

        public static DataTable GetEmployeeCutters()
        {
            var lstCutter =   from e in DBDataContext.Db.TBL_EMPLOYEEs
                              join p in DBDataContext.Db.TBL_EMPLOYEE_POSITIONs on e.EMPLOYEE_ID equals p.EMPLOYEE_ID
                              where p.POSITION_ID == (int)EmpPosition.Cutter
                              && e.IS_ACTIVE
                              orderby e.EMPLOYEE_NAME
                              select e;
            return lstCutter._ToDataTable();
        }

        public static DataTable GetPowerAmpare()
        {
            var tmp = DBDataContext.Db.TBL_AMPAREs
                                      .Where(x => x.IS_ACTIVE)
                                      .ToList()
                                      .OrderBy(x => Right( "0000" + x.AMPARE_NAME.Replace("A",""),4));
            return tmp._ToDataTable();
        }
        public static DataTable GetPowerPhases()
        {
            var tmp = DBDataContext.Db.TBL_PHASEs
                                      .Where(x => x.IS_ACTIVE)
                                      .ToList()
                                      .OrderBy(x=> x.PHASE_NAME);
            return tmp._ToDataTable();
        }
        public static DataTable GetPowerVoltage()
        {
            var tmp = DBDataContext.Db.TBL_VOLTAGEs
                                      .Where(x => x.IS_ACTIVE && x.VOLTAGE_ID>0)
                                      .ToList()
                                      .OrderBy(x => ParseVoltage( x.VOLTAGE_NAME));
            return tmp._ToDataTable();
        }
        public static DataTable GetPowerCapacity()
        {
            return (from cp in DBDataContext.Db.TBL_CAPACITies
                    where cp.IS_ACTIVE && cp.CAPACITY_ID>0
                    orderby cp.CAPACITY
                    select cp).ToList()
                    .Select(cp=>new {  cp.CAPACITY_ID, 
                                        CAPACITY = cp.CAPACITY.ToString()

                    })
                    ._ToDataTable();
        }

        public static DataTable GetTransfomerBrand()
        {
            return DBDataContext.Db.TBL_TRANSFORMER_BRANDs.Where(x => x.IS_ACTIVE)._ToDataTable();
        }
          
        public static DataTable GetSeals()
        {
            return DBDataContext.Db.TBL_SEALs.Where(row => row.IS_ACTIVE)._ToDataTable();
        }
        
        public static DataTable GetFixAssetStatus()
        {
            return DBDataContext.Db.TLKP_FIX_ASSET_STATUS._ToDataTable();
        }

        public static DataTable GetDepreciationStatus()
        {
            return DBDataContext.Db.TLKP_DEPRECIATION_STATUS._ToDataTable();
        }

        public static DataTable GetFuelType()
        {
            return DBDataContext.Db.TLKP_FUEL_TYPEs._ToDataTable();
        }

        public static DataTable GetFuelSource()
        {
            return DBDataContext.Db.TBL_FUEL_SOURCEs.Where(x => x.IS_ACTIVE)._ToDataTable();
        }

        public static DataTable GetDistribution()
        {
            return DBDataContext.Db.TLKP_DISTRIBUTIONs._ToDataTable();
        }

        public static DataTable GetLoanStatus()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("STATUS_ID", typeof(int));
            dt.Columns.Add("STATUS_NAME",typeof(string));
            dt.Rows.Add(0,Resources.ALL_TYPE);
            dt.Rows.Add(1,Resources.LOAN_NOT_PAID);
            dt.Rows.Add(2,Resources.LOAN_PAID); 
            return dt;
        }
        public static DataTable GetLookUpValue(int lookUpId)
        {
            var tmp = from l in DBDataContext.Db.TBL_LOOKUP_VALUEs
                      where l.LOOKUP_ID == lookUpId
                      select new
                      {
                          l.VALUE_ID,
                          l.VALUE_TEXT
                      };
            return tmp._ToDataTable();
        }

        public static DataTable GetConstant()
        {
            return DBDataContext.Db.TBL_CONSTANTs.Where(x => x.IS_ACTIVE)._ToDataTable();
        }
        public static decimal ParseAmpare(string value)
        {
            try
            {
                value = value.ToUpper();
                if (value.Contains("KA"))
                {
                    value = value.Replace("KA", "");
                    return DataHelper.ParseToDecimal(value) * 1000;
                }
                if (value.Contains("A"))
                {
                    value = value.Replace("A", "");
                    return DataHelper.ParseToDecimal(value);
                }
                return DataHelper.ParseToDecimal(value);
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public static decimal ParseVoltage(string value)
        {
            try
            {
                value = value.ToUpper();
                if (value.Contains("KV"))
                {
                    value = value.Replace("KV","");
                    return DataHelper.ParseToDecimal(value) * 1000; 
                }
                if (value.Contains("V")){
                    value = value.Replace("V","");
                    return DataHelper.ParseToDecimal(value);
                }
                return DataHelper.ParseToDecimal(value);
            }
            catch (Exception ex)
            {
                return 0;
            } 
        }

        public static decimal ParseCapacity(string value)
        {
            try
            {
                value = value.ToUpper();
                //if (value.Contains("KVA"))
                //{
                //    value = value.Replace("KV", "");
                //    return DataHelper.ParseToDecimal(value) * 1000;
                //}
                if (value.Contains("KVA"))
                {
                    value = value.Replace("KVA", "");
                    return DataHelper.ParseToDecimal(value);
                }
                return DataHelper.ParseToDecimal(value);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        private static string Right(string original, int numberCharacters)
        {
            return original.Substring(original.Length - numberCharacters);
        }

        public static DataTable GetConnectionType()
        {
            return DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs.OrderBy(x=>x.DESCRIPTION)._ToDataTable();
        }

        public static DataTable GetCustomerGroup()
        {
            return DBDataContext.Db.TLKP_CUSTOMER_GROUPs.OrderBy(x => x.DESCRIPTION)._ToDataTable();
        }

        public static DataTable GetBoxType()
        {
            return DBDataContext.Db.TLKP_BOX_TYPEs._ToDataTable();
        }
        public static string GetCurrencyName(int currencyId)
        {
            return DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.CURRENCY_ID == currencyId).CURRENCY_NAME;
        }
         
        public static DataTable GetBoxStatus()
        {
            return DBDataContext.Db.TLKP_BOX_SATUS._ToDataTable();
        }

        public static DataTable GetCustomerGroupType()
        {
            return DBDataContext.Db.TLKP_CUSTOMER_GROUPs.Where(x=>x.CUSTOMER_GROUP_ID !=3)._ToDataTable();
        }

        public static DataTable GetCashDrawer()
        {
            return DBDataContext.Db.TBL_CASH_DRAWERs.Where(x => x.IS_ACTIVE)._ToDataTable();
        }

        public static DataTable GetLicenseType()
        {
            return DBDataContext.Db.TBL_LICENSE_TYPEs.Where(x => x.IS_ACTIVE)._ToDataTable();
        }
        public static DataTable GetMeterType()
        {
            return DBDataContext.Db.TBL_METER_TYPEs.Where(x => x.IS_ACTIVE).Select(x => new { x.METER_TYPE_ID, x.METER_TYPE_CODE })._ToDataTable();
        }

        public static DataTable GetMeterStatus()
        {
            return DBDataContext.Db.TLKP_METER_STATUS.Select(x => new { x.STATUS_ID, x.STATUS_NAME })._ToDataTable();
        }

        public static DataTable GetVoltageLow()
        {
            var objlist = new List<int> { 2, 3 };
            var voltageLow = (from x in DBDataContext.Db.TBL_VOLTAGEs
                              where objlist.Contains(x.VOLTAGE_ID)
                              select x
                            );
            return voltageLow._ToDataTable();
        }

        public static DataTable GetVoltageMid()
        {
            var objlist = new List<int> { 1, 4, 5, 9 };
            var voltageMid = (from x in DBDataContext.Db.TBL_VOLTAGEs
                              where objlist.Contains(x.VOLTAGE_ID)
                              select x
                            );
            return voltageMid._ToDataTable();
        }

        public static DataTable GetVoltageHigh()
        {
            var objlist = new List<int> { 7, 8, 10 };
            var GetVoltageHigh = (from x in DBDataContext.Db.TBL_VOLTAGEs
                              where objlist.Contains(x.VOLTAGE_ID)
                              select x
                            );
            return GetVoltageHigh._ToDataTable();
        }

        public static DataTable GetPhase1_3()
        {
            var objlist = new List<int> { 1, 3 };
            var Phase1 = (from x in DBDataContext.Db.TBL_PHASEs
                          where objlist.Contains(x.PHASE_ID)
                          select x
                            );
            return Phase1._ToDataTable();
        }

        public static DataTable GetPhase1_2_3()
        {
            var Phase2 = (from x in DBDataContext.Db.TBL_PHASEs
                          select x
                            );
            return Phase2._ToDataTable();
        }

        public static DataTable GetPhase3()
        {
            var Phase3 = (from x in DBDataContext.Db.TBL_PHASEs
                          where x.PHASE_ID==3
                          select x
                            );
            return Phase3._ToDataTable();
        }

        public static DataTable GetInvoiceStatus()
        {
            var objlist = new List<int>() { 1, 4 };
            var _InvoiceStatus = (from x in DBDataContext.Db.TLKP_INVOICE_STATUS
                                  where objlist.Contains(x.INVOICE_STATUS_ID)
                                  select x
                                );
            return _InvoiceStatus._ToDataTable();
        }
    }
}
