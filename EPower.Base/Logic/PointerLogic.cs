﻿using DevExpress.XtraEditors;
using Dynamic.Helpers;
using HB01.Base.BaseModules;
using HB01.Base.Helpers;
using HB01.Base.Logics;
using HB01.Helpers.DevExpressCustomize;
using HB01.Interfaces;
using HB01.Interfaces.Security;
using HB01.Logics;
using HB01.Properties;
using HB01.Security.Models;
using Newtonsoft.Json;
using SoftTech;
using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading;

namespace EPower.Base.Logic
{
    public class PointerLogic
    {
        public static bool isConnectedPointer = false;
        public static string PointerPath = "";
        private static string[] _userLoggedIn = Settings.Default.USER_LOGGED_IN?.Cast<string>().ToArray() ?? new string[0];
        public static PointerLogic Instance { get; } = new PointerLogic();
        public void startHB01()
        {
            try
            {
                string connStr = Method.Utilities[Utility.POINTER_CONNECTION_STRING];
                int companyId = SoftTech.Helper.DataHelper.ParseToInt(Method.Utilities[Utility.POINTER_COMPANY_ID]);
                if (string.IsNullOrEmpty(connStr) || companyId == 0)
                {
                    return;
                }
                WindowsFormsSettings.LoadApplicationSettings();
                WindowsFormsSettings.DefaultFont = new System.Drawing.Font("Khmer Kep", 9);
                WindowsFormsSettings.DefaultLookAndFeel.SetSkinStyle("DevExpress Style");
                if (ShareAPI.Instance.AuthorizedUser == null)
                {
                    return;
                }
                var company = CompanyLogic.Instance.Find(companyId);
                Current.Company = company;
                Current.CurrencyId = company.CurrencyId;
                Current.CompanyId = company.Id;
                Current.Currencies = CompanyLogic.Instance.AvailableCurrencies(Current.CompanyId).ToList();
                Current.User = ShareAPI.Instance.AuthorizedUser;
                ShareAPI.Instance.CompanyId = company.Id;
                ShareAPI.Instance.Company = company;
                ShareAPI.Instance.CurrencyId = company?.CurrencyId ?? 0;
                isConnectedPointer = true; 

                //Method.Utilities[Utility.POINTER_COMPANY_ID] = Settings.Default.CURRENT_COMPANY_ID.ToString();
                //var ps = DBDataContext.Db.TBL_UTILITies.FirstOrDefault(x => x.UTILITY_ID == (int)Utility.POINTER_COMPANY_ID);
                //ps.UTILITY_VALUE = Settings.Default.CURRENT_COMPANY_ID.ToString();
                //DBDataContext.Db.SubmitChanges();
            }
            catch (Exception e)
            {
                isConnectedPointer = false;
                //  MsgBox.ShowInformation(string.Format(Resources.ConnectionProblem + "\n" + Resources.CheckAndTryAgain), Resources.ApplicationName);
            }
        }

        public static bool checkServices(bool isRequire)
        {
            bool _notAvailable = false;
            Settings.Default.SERVER_LINK_USER = Settings.Default.SERVER_LINK_USER ?? new HB01.Helper.ServerLinkConfigurationSetting();
            var allServerLink = Settings.Default.SERVER_LINK_USER;
            string PointerConnection = Method.Utilities[Utility.POINTER_CONNECTION_STRING];
            ServerLinkSetting selectedLink;
            try
            {
                if (!string.IsNullOrEmpty(PointerConnection))
                {
                    selectedLink = allServerLink?.ServerLinkSettings?.Items.FirstOrDefault(x => x.ConnectionEncrypt == PointerConnection);
                }
                else
                {
                    selectedLink = allServerLink.ServerLinkSettings[Settings.Default.USER_SERVER_LINK];
                    Method.Utilities[Utility.POINTER_CONNECTION_STRING] = selectedLink.ConnectionEncrypt;
                    var ps = DBDataContext.Db.TBL_UTILITies.FirstOrDefault(x => x.UTILITY_ID == (int)Utility.POINTER_CONNECTION_STRING);
                    ps.UTILITY_VALUE = selectedLink.ConnectionEncrypt;
                    DBDataContext.Db.SubmitChanges();
                }  
                if (selectedLink == null)
                {
                    var setting = Settings.Default.SERVER_LINK_USER.ServerLinkSettings;
                    var keyName = "EPower";
                    var st = setting?[keyName];
                    if (st == null)
                    {
                        Settings.Default.SERVER_LINK_USER.ServerLinkSettings[keyName] = new ServerLinkSetting()
                        {
                            Name = keyName,
                            ConnectionEncrypt = Method.Utilities[Utility.POINTER_CONNECTION_STRING],
                        };
                    }
                    Settings.Default.Save();
                    selectedLink = allServerLink.ServerLinkSettings.Items.FirstOrDefault(x => x.ConnectionEncrypt == Method.Utilities[Utility.POINTER_CONNECTION_STRING]);
                }
                if (string.IsNullOrEmpty(selectedLink.Name) && isRequire)
                {
                    var diag = new LinkConfiguration() { };
                    diag.ShowDialog();
                }
                //var selectedLink = allServerLink.ServerLinkSettings[selectedLink.Name];
                var appSettingjson = HB01.Domain.Helpers.Crypto.Decrypt(selectedLink.ConnectionEncrypt, HB01.Domain.Helpers.Crypto.Pwd);
                var appsetting = JsonConvert.DeserializeObject<LinkAppSetting>(appSettingjson);
                LocalVariable.ServerLinkName = selectedLink.Name;
                LocalVariable.ServerSetting = JsonConvert.DeserializeObject<AppSetting>(appsetting.Url);
                CheckServerAvaiable(LocalVariable.ServerSetting.SecurityApiUrl);
                CheckServerAvaiable(LocalVariable.ServerSetting.CoreApiUrl);
                CheckServerAvaiable(LocalVariable.ServerSetting.MediaApiUrl);
                authorize();
            }
            catch (Exception)
            {
                if (isRequire)
                {
                    var diag = new ErrorConnectionDialog(true) { };
                    if(diag.ShowDialog()== System.Windows.Forms.DialogResult.Cancel)
                    {
                        _notAvailable = true;
                    }
                }
            }
            return _notAvailable;
        }
        internal static void authorize()
        {
            if (ShareAPI.Instance.AuthorizedUser == null)
            {
                AuthenticateModel authenticateModel = Settings.Default.RemmeberUser;
                var authorizeModel = UserLogic.Instance.GetAuthorize(authenticateModel);
                if (authorizeModel != null)
                {
                    ShareAPI.Instance.AuthorizedUser = authorizeModel;
                    ShareAPI.Instance.UserName = authorizeModel?.Username ?? "";
                }
                else
                {
                    return;
                }
            }
        } 

        public static void ActivateResource(string language, bool needStart = false)
        {
            ResourceHelper.SecondaryResourceManager = Resources.ResourceManager;
            language = string.IsNullOrEmpty(language) ? "km-KH" : language;
            if (needStart)
            {
                language = "km-KH";
            }

            var cult = new CultureInfo(language);
            SetDefaultCulture(cult);
            if (cult.Name == "km-KH")
            {
                DevExpress.XtraEditors.Controls.Localizer.Active = new KHEditorsLocalizer();
                DevExpress.XtraGrid.Localization.GridLocalizer.Active = new KHGridLocalizer();
            }
        }
        public static void SetDefaultCulture(CultureInfo culture)
        {
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;
            Type type = typeof(CultureInfo);
            try
            {
                type.InvokeMember("s_userDefaultCulture",
                    BindingFlags.SetField | BindingFlags.NonPublic | BindingFlags.Static,
                    null,
                    culture,
                    new object[] { culture });

                type.InvokeMember("s_userDefaultUICulture",
                    BindingFlags.SetField | BindingFlags.NonPublic | BindingFlags.Static,
                    null,
                    culture,
                    new object[] { culture });
            }
            catch
            {
                // ignored
            }
        }
        public static void CheckServerAvaiable(string url)
        {
            try
            {
                var myRequest = (HttpWebRequest)WebRequest.Create(url);
                var response = (HttpWebResponse)myRequest.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    //  it's at least in some way responsive
                    //  but may be internally broken
                    //  as you could find out if you called one of the methods for real
                    Debug.Write(string.Format("{0} Available", url));
                }
                else
                {
                    var diag = new ErrorConnectionDialog(false) { };
                    diag.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(Resources.ServiceUnavailable, new Exception(Resources.ServiceUnavailable));
            }
        }
    }
}
