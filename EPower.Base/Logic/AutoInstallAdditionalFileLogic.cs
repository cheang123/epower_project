﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace EPower.Base.Logic
{
    public class AutoInstallAditionalFileLogic
    { 
        public static void DeployMSIFile(string fileName)
        { 
            var allPrograms = InstalledPrograms.GetInstalledPrograms();
            if (allPrograms.Contains("SAP Crystal Reports runtime engine for .NET Framework (32-bit)"))
            {
                return;
            }
            Process installerProcess = new Process();
            ProcessStartInfo processInfo = new ProcessStartInfo();
            processInfo.Arguments = @"/i  " + fileName;
            processInfo.FileName = "msiexec";
            installerProcess.StartInfo = processInfo;
            installerProcess.Start();
            installerProcess.WaitForExit();
        }

        public static void DeployREGFile(string fileName)
        {
            var isInstalled = false;
            using (RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Software\SAP BusinessObjects\Crystal Reports for .NET Framework 4.0\Crystal Reports\Export\PDF"))
            {
                if (key != null)
                {
                    var lagerFont = key.GetValue("ForceLargerFonts")?.ToString() ?? "0";
                    var UsePrecisePositioningForText = key.GetValue("UsePrecisePositioningForText")?.ToString() ?? "0";
                    var UseCustomEncoding = key.GetValue("UseCustomEncoding")?.ToString() ?? "0";
                    var TruncationAdjustment = key.GetValue("TruncationAdjustment")?.ToString() ?? "0";

                    if (lagerFont == "1" && UsePrecisePositioningForText == "1" && TruncationAdjustment == "1")
                    {
                        isInstalled = true;
                    }

                }
            }
            if (isInstalled)
            {
                return;
            }

            try
            {
                if (fileName != null)
                {
                    Process installerProcess = new Process();
                    ProcessStartInfo processInfo = new ProcessStartInfo();
                    processInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                    processInfo.FileName = fileName;
                    installerProcess.StartInfo = processInfo;
                    installerProcess.Start();
                    installerProcess.WaitForExit();
                }
            }
            catch (Exception)
            {


            }
        }

        public static void DeployMSIFileWithCommandline32(string fileName)
        {
            var allPrograms = InstalledPrograms.GetInstalledPrograms();
            if (allPrograms.Contains("SAP Crystal Reports runtime engine for .NET Framework (32-bit)"))
            {
                return;
            }

            if (fileName !=null)
            {
                Process installerProcess = new Process();
                ProcessStartInfo processInfo = new ProcessStartInfo();
                processInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                processInfo.FileName = fileName;
                installerProcess.StartInfo = processInfo;
                installerProcess.Start();
                installerProcess.WaitForExit();
            }
        }
        public static void DeployMSIFileWithCommandline64(string fileName)
        {
            var allPrograms = InstalledPrograms.GetInstalledPrograms();
            if (allPrograms.Contains("SAP Crystal Reports runtime engine for .NET Framework (64-bit)"))
            {
                return;
            }

            if (fileName != null)
            {
                Process installerProcess = new Process();
                ProcessStartInfo processInfo = new ProcessStartInfo();
                processInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                processInfo.FileName = fileName;
                installerProcess.StartInfo = processInfo;
                installerProcess.Start();
                installerProcess.WaitForExit();
            }
        }

        public static void DeployREGFileWithCommandline(string fileName)
        {
            var isInstalled = false;
            using (RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Software\SAP BusinessObjects\Crystal Reports for .NET Framework 4.0\Crystal Reports\Export\PDF"))
            {
                if (key != null)
                {
                    var lagerFont = key.GetValue("ForceLargerFonts")?.ToString() ?? "0";
                    var UsePrecisePositioningForText = key.GetValue("UsePrecisePositioningForText")?.ToString() ?? "0";
                    var UseCustomEncoding = key.GetValue("UseCustomEncoding")?.ToString() ?? "0";
                    var TruncationAdjustment = key.GetValue("TruncationAdjustment")?.ToString() ?? "0";

                    if (lagerFont == "1" && UsePrecisePositioningForText == "1" && TruncationAdjustment == "1")
                    {
                        isInstalled = true;
                    }

                }
            }
            if (isInstalled)
            {
                return;
            }

            if (fileName != null)
            {
                Process installerProcess = new Process();
                ProcessStartInfo processInfo = new ProcessStartInfo();
                processInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                processInfo.FileName = fileName;
                installerProcess.StartInfo = processInfo;
                installerProcess.Start();
                installerProcess.WaitForExit();
            }
        }
    }
    public static class InstalledPrograms
    {
        const string registry_key = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall";
         
        public static List<string> GetInstalledPrograms()
        {
            var result = new List<string>();
            result.AddRange(GetInstalledProgramsFromRegistry(RegistryView.Registry32));
            result.AddRange(GetInstalledProgramsFromRegistry(RegistryView.Registry64));
            return result;
        } 

        private static IEnumerable<string> GetInstalledProgramsFromRegistry(RegistryView registryView)
        {
            var result = new List<string>(); 
            using (RegistryKey key = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, registryView).OpenSubKey(registry_key))
            {
                foreach (string subkey_name in key.GetSubKeyNames())
                {
                    using (RegistryKey subkey = key.OpenSubKey(subkey_name))
                    {
                        if (IsProgramVisible(subkey))
                        {
                            result.Add((string)subkey.GetValue("DisplayName"));
                        }
                    }
                }
            } 
            return result;
        }

        private static bool IsProgramVisible(RegistryKey subkey)
        {
            var name = (string)subkey.GetValue("DisplayName");
            var releaseType = (string)subkey.GetValue("ReleaseType");
            //var unistallString = (string)subkey.GetValue("UninstallString");
            var systemComponent = subkey.GetValue("SystemComponent");
            var parentName = (string)subkey.GetValue("ParentDisplayName");

            return
                !string.IsNullOrEmpty(name)
                && string.IsNullOrEmpty(releaseType)
                && string.IsNullOrEmpty(parentName)
                && (systemComponent == null);
        }
    }
}