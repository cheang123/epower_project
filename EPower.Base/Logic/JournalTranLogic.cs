﻿using EPower.Base.Models;
using EPower.Base.Properties;
using HB01.Domain.Models;
using HB01.Logics;
using SoftTech;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace EPower.Base.Logic
{
    public class JournalTranLogic
    {
        public static JournalTranLogic Instance { get; } = new JournalTranLogic();
        public int totalSucess { get; set; }
        public int totalfail { get; set; }

        public List<JournalHistoryList> GetJournalHistoryLists(DateTime start, DateTime end)
        {
            totalSucess = 0;
            totalfail = 0;
            List<JournalHistoryList> lists = new List<JournalHistoryList>();
            var LastOBDate = OpeningBalanceLogic.Instance.getLastOpeningBalanceDate(0);
            start = start.Date;
            end = end.Date.AddDays(1).AddSeconds(-1);
            if (LastOBDate <= Constant.MinDate)
            {
                return lists;
            }
            if (end <= LastOBDate)
            {
                return lists;
            }
            if (start <= LastOBDate)
            {
                start = LastOBDate;
            }
            DBDataContext.Db.CommandTimeout = (int)TimeSpan.FromMinutes(5).TotalMilliseconds;
            //var dt = new DataTable();
            var dt = DBDataContext.Db.ExecuteQuery<JournalHistoryList>(@"EXEC HB01.HB01_JOURNAL_TRANSACTION_DETAIL  @p0, @p1", start, end);
            lists = dt.ToList();
            totalSucess = lists.Where(x => x.PostedDate != null).Count();
            totalfail = lists.Count() - totalSucess;
            var groupId = lists.Select(x => x.GroupId).Distinct().ToList();
            var renderId = 1;
            foreach (var id in groupId)
            {
                var groupDatas = lists.Where(x => x.GroupId == id).ToList();
                var parentId = renderId;

                var parent = new JournalHistoryList()
                {
                    RenderId = renderId,
                    ParentId = 0,
                    GroupName = "",
                    RefNo = $"{groupDatas.Max(x=>x.GroupName)} \t\t {groupDatas.Where(x => x.PostedDate != null).Count()}/{groupDatas.Count()}",
                    CustomerCode = "",
                    CustomerName = "",
                    TranDate = null,
                    PostedDate = null,
                    PostedAmount = null,
                    Amount = null,
                    CurrencyCode = ""
                };
                lists.Add(parent);
                renderId++;

                var currencies = groupDatas.Select(x => x.CurrencyCode).Distinct().ToList();
                foreach (var curr in currencies)
                {
                    var items = groupDatas.Where(x => x.CurrencyCode == curr).OrderBy(x=>x.RefNo).ToList();
                    var subParentId = renderId;
                    var subParent = new JournalHistoryList()
                    {

                        RenderId = renderId,
                        ParentId = parentId,
                        GroupName = "",
                        RefNo = $"{ Resources.CURRENCY } : {curr} \t\t {items.Where(x => x.PostedDate != null).Count()}/{items.Count()}",
                        CustomerCode = "",
                        CustomerName = "",
                        TranDate = null,
                        PostedDate = null,
                        PostedAmount = items.Sum(x => x.PostedAmount),
                        Amount = items.Sum(x => x.Amount),
                        CurrencyCode = ""
                    };
                    lists.Add(subParent);
                    renderId++;

                    foreach (var item in items)
                    {
                        item.RenderId = renderId;
                        item.ParentId = subParentId;
                        item.GroupName = "";
                        renderId++;
                    }
                }
            }

            return lists;
        }
    }
}
