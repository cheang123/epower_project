﻿using EPower.Base.Models;
using HB01.Domain.Enums;
using HB01.Domain.Models.Transactions;
using HB01.Helpers;
using HB01.Logics;
using SoftTech;
using SoftTech.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace EPower.Base.Logic
{
    class PushAdjustmentLogic : Integration<AdjustInvoice>
    {
        internal static PushAdjustmentLogic Instance { get; } = new PushAdjustmentLogic();
        public override List<AdjustInvoice> AvailableRecords(DateTime startDate, DateTime endDate)
        {
            var postedItems = from j in DBDataContext.Db.TBL_JOURNAL_ENTRies
                              join ja in DBDataContext.Db.TBL_JOURNAL_ENTRY_ACCOUNTs on j.JOURNAL_ENTRY_ID equals ja.JOURNAL_ENTRY_ID
                              where j.IS_ACTIVE && (j.REFERENCE_TYPE_ID == (int)ReferenceType.Adjustment)
                              select new
                              {
                                  j.JOURNAL_ENTRY_ID,
                                  ja.REFERENCE_ID
                              };

            var adjInvs = from a in DBDataContext.Db.TBL_INVOICE_ADJUSTMENTs
                          join i in DBDataContext.Db.TBL_INVOICEs on a.INVOICE_ID equals i.INVOICE_ID
                          join c in DBDataContext.Db.TLKP_CURRENCies on i.CURRENCY_ID equals c.CURRENCY_ID
                          join j in postedItems on a.ADJUST_INVOICE_ID equals j.REFERENCE_ID into l
                          from je in l.DefaultIfEmpty()
                          where a.CREATE_ON >= startDate
                               && a.CREATE_ON <= endDate
                               && c.EXTERNAL_CURRENCY_ID != 0
                               && je.JOURNAL_ENTRY_ID == null
                          select new
                          {
                              a.INVOICE_ID,
                              a.ADJUST_INVOICE_ID,
                              a.ADJUST_AMOUNT,
                              a.TAX_ADJUST_AMOUNT,
                              a.INVOICE_ITEM_ID,
                              c.CURRENCY_ID,
                              c.EXTERNAL_CURRENCY_ID,
                              i.IS_SERVICE_BILL,
                              i.CUSTOMER_CONNECTION_TYPE_ID,
                              a.CREATE_ON,
                              a.EXCHANGE_RATE
                          };

            var bills = from a in adjInvs
                        join ct in DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs on a.CUSTOMER_CONNECTION_TYPE_ID equals ct.CUSTOMER_CONNECTION_TYPE_ID
                        join ic in DBDataContext.Db.TBL_ACCOUNT_CHARTs on ct.IC_ACCOUNT equals ic.ACCOUNT_ID
                        where a.IS_SERVICE_BILL == false
                            && ic.EXT_ACCOUNT_ID != 0
                        select new AdjustInvoice
                        {
                            ADJUST_INVOICE_ID = a.ADJUST_INVOICE_ID,
                            AR_ACCOUNT_ID = a.CURRENCY_ID == (int)EPower.Currency.KHR ? ct.AR_KHR : a.CURRENCY_ID == (int)EPower.Currency.USD
                                                  ? ct.AR_USD : a.CURRENCY_ID == (int)EPower.Currency.THB ? ct.AR_THB : ct.AR_VND,
                            CURRENCY_ID = a.EXTERNAL_CURRENCY_ID,
                            INCOME_ACCOUNT_ID = ic.EXT_ACCOUNT_ID,
                            ADJUST_AMOUNT = a.ADJUST_AMOUNT,
                            EntryDate = a.CREATE_ON,
                            INVOICE_ITEM_NAME = ct.CUSTOMER_CONNECTION_TYPE_NAME,
                            ExchangeRate = a.EXCHANGE_RATE,
                            TAX_AMOUNT = 0,
                            TAX_ACCOUNT = 0
                        };

            var services = from a in adjInvs
                           join d in DBDataContext.Db.TBL_INVOICE_DETAILs on a.INVOICE_ID equals d.INVOICE_ID
                           join it in DBDataContext.Db.TBL_INVOICE_ITEMs on a.INVOICE_ITEM_ID equals it.INVOICE_ITEM_ID
                           join ic in DBDataContext.Db.TBL_ACCOUNT_CHARTs on it.INCOME_ACCOUNT_ID equals ic.ACCOUNT_ID
                           join t in DBDataContext.Db.TBL_TAXes on d.TAX_ID equals t.TAX_ID into tl
                           from tx in tl.DefaultIfEmpty()
                           join ac in DBDataContext.Db.TBL_ACCOUNT_CHARTs on tx.ACCOUNT_ID equals ac.ACCOUNT_ID into al
                           from ta in al.DefaultIfEmpty()
                           where a.IS_SERVICE_BILL == true
                               && ic.EXT_ACCOUNT_ID != 0
                               && a.INVOICE_ITEM_ID == d.INVOICE_ITEM_ID
                               && a.CREATE_ON >= d.TRAN_DATE
                           select new AdjustInvoice
                           {
                               ADJUST_INVOICE_ID = a.ADJUST_INVOICE_ID,
                               AR_ACCOUNT_ID = a.CURRENCY_ID == (int)EPower.Currency.KHR ? it.AR_KHR : a.CURRENCY_ID == (int)EPower.Currency.USD
                                                  ? it.AR_USD : a.CURRENCY_ID == (int)EPower.Currency.THB ? it.AR_THB : it.AR_VND,
                               CURRENCY_ID = a.EXTERNAL_CURRENCY_ID,
                               INCOME_ACCOUNT_ID = ic.EXT_ACCOUNT_ID,
                               ADJUST_AMOUNT = a.ADJUST_AMOUNT,
                               EntryDate = a.CREATE_ON,
                               INVOICE_ITEM_NAME = it.INVOICE_ITEM_NAME,
                               ExchangeRate = a.EXCHANGE_RATE,
                               TAX_AMOUNT = a.TAX_ADJUST_AMOUNT,
                               TAX_ACCOUNT = ta == null ? 0 : ta.EXT_ACCOUNT_ID,
                           };

            var result = bills.Concat(services);

            var iBil = from s in result
                       join a in DBDataContext.Db.TBL_ACCOUNT_CHARTs on s.AR_ACCOUNT_ID equals a.ACCOUNT_ID
                       where a.EXT_ACCOUNT_ID != 0
                       select new AdjustInvoice
                       {
                           ADJUST_INVOICE_ID = s.ADJUST_INVOICE_ID,
                           AR_ACCOUNT_ID = a.EXT_ACCOUNT_ID,
                           CURRENCY_ID = s.CURRENCY_ID,
                           INCOME_ACCOUNT_ID = s.INCOME_ACCOUNT_ID,
                           ADJUST_AMOUNT = s.ADJUST_AMOUNT,
                           EntryDate = s.EntryDate,
                           INVOICE_ITEM_NAME = s.INVOICE_ITEM_NAME,
                           TAX_ACCOUNT = s.TAX_ACCOUNT,
                           TAX_AMOUNT = s.TAX_AMOUNT,
                           ExchangeRate = s.ExchangeRate
                       };

            return iBil.ToList();
        }

        public override List<TBL_JOURNAL_ENTRY_ACCOUNT> EntryItems(List<AdjustInvoice> list, int EntryId)
        {
            var items = from s in list
                        select new
                        {
                            Income = new TBL_JOURNAL_ENTRY_ACCOUNT
                            {
                                JOURNAL_ENTRY_ID = EntryId,
                                REFERENCE_ID = s.ADJUST_INVOICE_ID,
                                ACCOUNT_ID = s.INCOME_ACCOUNT_ID,
                                AMOUNT = s.ADJUST_AMOUNT
                            },
                            AR = new TBL_JOURNAL_ENTRY_ACCOUNT
                            {
                                JOURNAL_ENTRY_ID = EntryId,
                                REFERENCE_ID = s.ADJUST_INVOICE_ID,
                                ACCOUNT_ID = s.AR_ACCOUNT_ID,
                                AMOUNT = s.ADJUST_AMOUNT
                            }
                        };

            //var items = bills.Concat(services).ToList();
            return items.Select(x => x.AR).Concat(items.Select(x => x.Income)).ToList();
        }

        public override List<JournalEntryLine> EntryLines(List<AdjustInvoice> list)
        {
            var lines = from s in list
                        group s by new { ACCOUNT_INCOME = s.INCOME_ACCOUNT_ID, ACCOUNT_RECEIVABLE = s.AR_ACCOUNT_ID, CURRENCY_ID = s.CURRENCY_ID, s.ExchangeRate, s.TAX_ACCOUNT } into g
                        select new
                        {
                            INCOME = new JournalEntryLine
                            {
                                AccountId = g.Key.ACCOUNT_INCOME,
                                CurrencyId = g.Key.CURRENCY_ID,
                                NetAmount = g.Sum(x => x.ADJUST_AMOUNT - x.TAX_AMOUNT) * g.Key.ExchangeRate,
                                CurrencyAmount = g.Sum(x => x.ADJUST_AMOUNT - x.TAX_AMOUNT)
                                //Title = string.Format("{0} {1}", Resources.BILL, g.Count()),
                            },
                            AR = new JournalEntryLine
                            {
                                AccountId = g.Key.ACCOUNT_RECEIVABLE,
                                CurrencyId = g.Key.CURRENCY_ID,
                                NetAmount = g.Sum(x => x.ADJUST_AMOUNT) * g.Key.ExchangeRate,
                                CurrencyAmount = g.Sum(x => x.ADJUST_AMOUNT)
                                //Title = string.Format("{0} {1}", Resources.BILL, g.Count()),
                            },
                            TAX = new JournalEntryLine
                            {
                                AccountId = g.Key.TAX_ACCOUNT,
                                NetAmount = g.Sum(x => x.TAX_AMOUNT) * g.Key.ExchangeRate,
                                CurrencyId = g.Key.CURRENCY_ID,
                                Active = true,
                                CurrencyAmount = g.Sum(x => x.TAX_AMOUNT),
                                ExchangeRate = g.Key.ExchangeRate,
                                ExchangeRateId = -1
                            }
                        };
            return lines.Select(x => x.INCOME).Concat(lines.Select(x => x.AR)).Concat(lines.Select(x => x.TAX).Where(x => x.NetAmount != 0)).ToList();
        }

        public override void PushJournal(DateTime startDate, DateTime endDate)
        {
            var k = 1;
            var currDate = DBDataContext.Db.GetSystemDate();
            var tran = (from t in AvailableRecords(startDate, endDate)
                        group t by new { EntryDate = t.EntryDate.Date, t.CURRENCY_ID } into g
                        orderby g.Key.EntryDate
                        select new Transaction
                        {
                            Date = g.Key.EntryDate,
                            CurrencyId = g.Key.CURRENCY_ID,
                            Transactions = g.ToList()
                        }).ToList();

            var journalId = AccountingLogic.Instance.Journal(AccountingConfig.JOURNAL_ADJUSTMENT).Id;

            foreach (var t in tran)
            {
                var tranOption = new TransactionOptions()
                {
                    IsolationLevel = IsolationLevel.ReadUncommitted,
                    Timeout = TimeSpan.MaxValue
                };

                TBL_JOURNAL_ENTRY afterUpdateJournal = new TBL_JOURNAL_ENTRY();
                TBL_JOURNAL_ENTRY beforeUpdateJournal = new TBL_JOURNAL_ENTRY();

                try
                {
                    using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required, tranOption))
                    {
                        if (t.Transactions.Count > 0)
                        {
                            Runner.Instance.Text = $"ចុះបញ្ជីកែតម្រូវ {k++}/{tran.Count}";
                            //Sumit E-Power
                            var eJournal = new TBL_JOURNAL_ENTRY
                            {
                                REF_NO = "",
                                CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                                CREATE_ON = currDate,
                                ENTRY_DATE = t.Date,
                                IS_ACTIVE = true,
                                CURRENCY_ID = t.CurrencyId,
                                REFERENCE_TYPE_ID = (int)ReferenceType.Adjustment,
                                HB01_JOURNAL_ID = 0,
                                NOTE = ""
                            };
                            DBDataContext.Db.TBL_JOURNAL_ENTRies.InsertOnSubmit(eJournal);
                            DBDataContext.Db.SubmitChanges();
                            var line = EntryItems(t.Transactions, eJournal.JOURNAL_ENTRY_ID);
                            DBDataContext.Db.BulkCopy(line._ToDataTable(), $"HB01.{nameof(TBL_JOURNAL_ENTRY_ACCOUNT)}");
                            DBDataContext.Db.SubmitChanges();
                            eJournal._CopyTo(afterUpdateJournal);
                            eJournal._CopyTo(beforeUpdateJournal);

                            //Submit Pointer
                            var journal = new JournalEntry()
                            {
                                CompanyId = Current.CompanyId,
                                Active = true,
                                CurrencyId = t.CurrencyId,
                                EntryDate = t.Date,
                                RefNo = "From E-Power",
                                JournalId = journalId,
                                SourceRefId = "AdjustInvoiceIds." + string.Join(",", t.Transactions.Select(x => x.ADJUST_INVOICE_ID).ToList()),
                                Posted = true,
                                Source = PostingSources.External,
                                Lines = EntryLines(t.Transactions),
                                Amount = t.Transactions.Sum(x => x.ADJUST_AMOUNT)
                            };
                            var j = JournalEntryLogic.Instance.PostExternal(journal);
                            afterUpdateJournal.HB01_JOURNAL_ID = j.Id;
                            afterUpdateJournal.REF_NO = j.EntryNo;
                        }
                        transaction.Complete();
                    }
                    DBDataContext.Db.Update(beforeUpdateJournal, afterUpdateJournal);
                    DBDataContext.Db.SubmitChanges();
                }
                catch (Exception e)
                {
                    //MsgBox.ShowInformation(e.Message);
                }
            }
        }
    }
}
