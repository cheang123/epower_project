﻿using EPower.Base.Helper.Encrypt;
using Newtonsoft.Json;
using RestSharp;
using SoftTech;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Net;
using System.Windows.Forms;

namespace EPower.Base.Logic
{
    public class ExchangeRateLogic
    {
        public TBL_EXCHANGE_RATE findLastExchangeRate(DateTime dateTime, int currencyId)
        {
            dateTime = dateTime.Date.AddDays(1).AddMilliseconds(-1);
            var exchange = new TBL_EXCHANGE_RATE();
            if (currencyId == 1)
            {
                exchange = new TBL_EXCHANGE_RATE()
                {
                    EXCHANGE_RATE = 1,
                    CREATE_ON = DBDataContext.Db.GetSystemDate()
                };
            }
            else
            {
                exchange = DBDataContext.Db.TBL_EXCHANGE_RATEs.OrderByDescending(x => x.CREATE_ON).FirstOrDefault(x => x.CURRENCY_ID == currencyId && x.CREATE_ON <= dateTime);
            }
            return exchange;
        }
        public void SyncExchangeRate()
        {
            //var functionCurrency = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.CURRENCY_ID == 1).CURRENCY_CODE;
            var currencies = DBDataContext.Db.TLKP_CURRENCies.ToList();
            GetExchangeRate(currencies.FirstOrDefault(x => x.IS_DEFAULT_CURRENCY == true), currencies.Where(x => x.IS_DEFAULT_CURRENCY == false).ToList());
        }
        private bool IsHaveExchangeRate(TLKP_CURRENCY currency)
        {
            bool bln = false;
            if (currency == null)
            {
                return bln;
            }
            bln = DBDataContext.Db.TBL_EXCHANGE_RATEs.Where(x => x.CREATE_ON.Date == DateTime.Now.Date && x.CURRENCY_ID == currency.CURRENCY_ID).Any();

            //return DBDataContext.Db.TBL_EXCHANGE_RATEs.Where(x => (DateTime.Now.Date - x.CREATE_ON).TotalDays == 0 && x.CURRENCY_ID == currency.CURRENCY_ID)?.Any() ?? false;
            return bln;
        }
        public static int countScan = 0;
        private void GetExchangeRate(TLKP_CURRENCY defaultCurrencies, List<TLKP_CURRENCY> NonDefaultCurrencies)
        {
            var timer = (new Timer() { Interval = (int)TimeSpan.FromMilliseconds(1).TotalMilliseconds });
            timer.Start();
            timer.Tick += (object sender, EventArgs e) =>
            {
                if (countScan > 5)
                {
                    timer.Stop();
                }
                //auto only exchange rate who not yet add for daily //
                var allforeignCurrencies = new List<TLKP_CURRENCY>();
                foreach (var item in NonDefaultCurrencies)
                {
                    if (IsHaveExchangeRate(item))
                    {
                        continue;
                    }
                    allforeignCurrencies.Add(item);
                }

                /* if all currency are already posted no need to check server */
                if (!allforeignCurrencies.Any())
                {
                    timer.Stop();
                }

                TimeTick(sender, e, defaultCurrencies, allforeignCurrencies);
                timer.Interval = (int)TimeSpan.FromMinutes(5).TotalMilliseconds;
            };
        }
        public void TimeTick(object send, EventArgs e, TLKP_CURRENCY functionalCurrency, List<TLKP_CURRENCY> foreignCurrencies)
        {
            countScan += 1;
            /*sample url*/
            //http://192.168.1.94:9988/api/ExchangeRate/2020-04-20/USD/KHR
            var client = new RestClient("http://crm.e-power.com.kh:9988/");
            var sqlServer = DBDataContext.Db.ExecuteQuery<string>("SELECT @@VERSION").FirstOrDefault()?.ToString() ?? "";
            var company = DBDataContext.Db.TBL_COMPANies.FirstOrDefault();
            var windowVersion = SimpleOSName();

            foreach (var foreignCurrency in foreignCurrencies)
            {
                var request = new RestRequest(string.Format("/api/ExchangeRate/{0}/{1}/{2}", DateTime.Now.ToString("yyyy-MM-dd"), foreignCurrency.CURRENCY_CODE, functionalCurrency.CURRENCY_CODE, RestSharp.Method.GET));
                request.AddHeader("Accept", "application/json");
                request.RequestFormat = DataFormat.Json;
                var clientInfo = new ClientInformation();
                clientInfo.AppName = "E-POWER";
                clientInfo.CompanyName = (company?.COMPANY_NAME ?? "") + " " + (company?.LICENSE_NUMBER ?? "");
                clientInfo.AppVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                clientInfo.WindowVersion = windowVersion;
                clientInfo.SqlServerVersion = sqlServer;
                var clientInfoJson = JsonConvert.SerializeObject(clientInfo);
                var clientInfoEncrypt = CryptoExchangeRate.Encrypt(clientInfoJson, CryptoExchangeRate.Pwd);
                request.AddHeader("ClientInfo", clientInfoEncrypt);
                IRestResponse respone = client.Execute(request);
                var status = respone.StatusCode;
                var desc = respone.StatusDescription;


                if (status != HttpStatusCode.OK)
                {
                    return;
                }
                var result = JsonConvert.DeserializeObject<ExchangeRateViewModel>(respone.Content);
                //KHR or USD or .... 
                var exchangeRate = new TBL_EXCHANGE_RATE()
                {
                    CURRENCY_ID = foreignCurrency.CURRENCY_ID,
                    CREATE_ON = result.RateDate,
                    EXCHANGE_RATE = result.Rate / 1000000m,
                    CREATE_BY = "SYSTEM",
                    EXCHANGE_CURRENCY_ID = 1,
                    MULTIPLIER_METHOD = true
                };
                DBDataContext.Db.TBL_EXCHANGE_RATEs.InsertOnSubmit(exchangeRate);
            }
            DBDataContext.Db.SubmitChanges();
        }

        public string SimpleOSName()
        {
            var name = new ManagementObjectSearcher("SELECT Caption FROM Win32_OperatingSystem").Get().Cast<ManagementObject>()
                .Select(x => x.GetPropertyValue("Caption").ToString())
                .First();
            var parts = name.Split(' ').ToArray();
            var take = name.Contains("Server") ? 3 : 2;
            return name;
        }

        public void ExchangeRateFirstLoad()
        {
            var now = DBDataContext.Db.GetSystemDate().Date;
            var currencies = DBDataContext.Db.TLKP_CURRENCies.Where(x => x.CURRENCY_ID > 1).ToList();
            var exchangeRates = DBDataContext.Db.TBL_EXCHANGE_RATEs.Where(x => x.CREATE_ON >= new DateTime(2021, 01, 01)).ToList();
            var sqlServer = DBDataContext.Db.ExecuteQuery<string>("SELECT @@VERSION").FirstOrDefault()?.ToString() ?? "";
            var company = DBDataContext.Db.TBL_COMPANies.FirstOrDefault();
            var windowVersion = SimpleOSName();

            var client = new RestClient("http://crm.e-power.com.kh:9988/");
            foreach (var c in currencies)
            {
                while (now >= new DateTime(2021, 01, 01))
                {
                    if (exchangeRates.Where(x => x.CURRENCY_ID == c.CURRENCY_ID && x.CREATE_ON.Date == now).Any())
                    {
                        now = now.AddDays(-1);
                        continue;
                    }
                    var request = new RestRequest(string.Format("/api/ExchangeRate/{0}/{1}/{2}", now.ToString("yyyy-MM-dd"), c.CURRENCY_CODE, "KHR", RestSharp.Method.GET));
                    request.AddHeader("Accept", "application/json");
                    request.RequestFormat = DataFormat.Json;
                    var clientInfo = new ClientInformation();
                    clientInfo.AppName = "E-POWER";
                    clientInfo.CompanyName = (company?.COMPANY_NAME ?? "") + " " + (company?.LICENSE_NUMBER ?? "");
                    clientInfo.AppVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                    clientInfo.WindowVersion = windowVersion;
                    clientInfo.SqlServerVersion = sqlServer;
                    var clientInfoJson = JsonConvert.SerializeObject(clientInfo);
                    var clientInfoEncrypt = CryptoExchangeRate.Encrypt(clientInfoJson, CryptoExchangeRate.Pwd);
                    IRestResponse respone = client.Execute(request);
                    var status = respone.StatusCode;
                    var desc = respone.StatusDescription;
                    request.AddHeader("ClientInfo", clientInfoEncrypt);
                    if (status != HttpStatusCode.OK)
                    {
                        now = now.AddDays(-1);
                        continue;
                    }
                    var result = JsonConvert.DeserializeObject<ExchangeRateViewModel>(respone.Content);
                    //KHR or USD or .... 
                    var exchangeRate = new TBL_EXCHANGE_RATE()
                    {
                        CURRENCY_ID = c.CURRENCY_ID,
                        CREATE_ON = result.RateDate,
                        EXCHANGE_RATE = result.Rate / 1000000m,
                        CREATE_BY = "SYSTEM",
                        EXCHANGE_CURRENCY_ID = 1,
                        MULTIPLIER_METHOD = true
                    };
                    DBDataContext.Db.TBL_EXCHANGE_RATEs.InsertOnSubmit(exchangeRate);
                    now = now.AddDays(-1);
                }
                DBDataContext.Db.SubmitChanges();
            }

            var sql = @"IF EXISTS (SELECT TOP 1 * FROM dbo.TBL_INVOICE_DETAIL WHERE REF_NO = '')
                            UPDATE    d
                            SET       REF_NO = i.INVOICE_NO,
                                      d.EXCHANGE_RATE = dbo.GET_EXCHANGE_RATE(i.CURRENCY_ID, 1, i.INVOICE_DATE),
                                      d.EXCHANGE_RATE_DATE = dbo.GET_EXCHANGE_RATE_DATE(i.CURRENCY_ID, 1, i.INVOICE_DATE),
                                      d.TRAN_DATE = i.INVOICE_DATE
                            FROM      dbo.TBL_INVOICE_DETAIL d
                           INNER JOIN dbo.TBL_INVOICE        i ON i.INVOICE_ID = d.INVOICE_ID
                            WHERE     REF_NO = '';";
            DBDataContext.Db.ExecuteCommand(sql);
        }
    }
    public class ExchangeRateViewModel
    {
        //public ExchangeRateViewModel();

        public string FromCurrency { get; set; }
        public string ToCurrency { get; set; }
        public decimal Rate { get; set; }
        public decimal ReverseRate { get; set; }
        public DateTime RateDate { get; set; }
        public bool Reverse { get; set; }
    }
    public class ClientInformation
    {
        public string UserName { get; set; }
        public string CompanyName { get; set; }
        public string AppName { get; set; }
        public string WindowVersion { get; set; }
        public string SqlServerVersion { get; set; }
        public string AppVersion { get; set; }
    }
}
