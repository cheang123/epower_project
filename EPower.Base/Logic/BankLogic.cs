﻿using SoftTech;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EPower.Base.Logic
{
    public class BankLogic
    {
        public static DateTime GetNextWorkingDay(DateTime datetime)
        {
            var holiday = DBDataContext.Db.TBL_BANK_HOLIDAYs.Where(x => !x.IS_HOLIDAY && x.DAY > datetime).OrderBy(x => x.DAY)?.FirstOrDefault();
            return holiday.DAY;
        }
    }
}
