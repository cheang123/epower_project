﻿using HB01.Domain.Models.Settings;
using HB01.Domain.Models.Taxes;
using HB01.Logics;
using SoftTech;
using SoftTech.Helper;
using System.Collections.Generic;
using System.Linq;

namespace EPower.Base.Logic
{
    public class AccountingLogic
    {
        public static AccountingLogic Instance { get; } = new AccountingLogic();

        public Journal Journal(AccountingConfig journalConfig)
        {
            TBL_ACCOUNTING_CONFIG config = DBDataContext.Db.TBL_ACCOUNTING_CONFIGs.FirstOrDefault(x => x.CONFIG_ID == (int)journalConfig);
            var id = DataHelper.ParseToInt(config?.CONFIG_VALUE);
            return JournalLogic.Instance.Find(id);
        }
        public Tax Tax(AccountingConfig journalConfig)
        {
            TBL_ACCOUNTING_CONFIG config = DBDataContext.Db.TBL_ACCOUNTING_CONFIGs.FirstOrDefault(x => x.CONFIG_ID == (int)journalConfig);
            var id = DataHelper.ParseToInt(config?.CONFIG_VALUE);
            return TaxLogic.Instance.Find(id);
        }
        public void UpdateConfig(AccountingConfig config, string value)
        {
            var obj = DBDataContext.Db.TBL_ACCOUNTING_CONFIGs.FirstOrDefault(x => x.CONFIG_ID == (int)config);
            var objOld = new TBL_ACCOUNTING_CONFIG();
            obj._CopyTo(objOld);
            obj.CONFIG_VALUE = value;
            DBDataContext.Db.Update(objOld, obj);
            DBDataContext.Db.SubmitChanges();
        }

        public List<int> AccountConfigId(AccountConfig config)
        {
            var q = DBDataContext.Db.TBL_ACCOUNT_CONFIGs.FirstOrDefault(x => x.CONFIG_ID == (int)config);
            return q.CONFIG_VALUE.Split(',', ';').Select(x => DataHelper.ParseToInt(x))
                                  .ToList();
        }
    }
}
