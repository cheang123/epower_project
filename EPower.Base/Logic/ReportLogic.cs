﻿using CrystalDecisions.CrystalReports.Engine;
using SoftTech;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace EPower.Base.Logic
{
    public class ReportLogic
    {
        public static ReportLogic Instance = new ReportLogic();
        private string loginName = SoftTech.Security.Logic.Login.CurrentLogin.LOGIN_NAME; 
        
        public ReportDocument GetReportNewDocument(string reportName, string printerName, int printInvoiceId)
        {
            var company = DBDataContext.Db.TBL_COMPANies.FirstOrDefault();
            var invoices = DBDataContext.Db.ExecuteQuery<ReportInvoice>(@"EXEC dbo.REPORT_INVOICE @p0", printInvoiceId).ToList();
            var histories = DBDataContext.Db.ExecuteQuery<ReportInvoiceHistory>(@"EXEC dbo.REPORT_INVOICE_HISTORY_ALL @p0", printInvoiceId).ToList();
            var usages = DBDataContext.Db.ExecuteQuery<ReportInvoiceUsage>(@"EXEC dbo.REPORT_INVOICE_USAGE @p0", printInvoiceId).ToList();

            var data = new Dictionary<string, IEnumerable>() { };
            data.Add("report_invoice", invoices);

            var subData = new Dictionary<string, Dictionary<string, IEnumerable>>();
            var sreportInvoiceUsage = new Dictionary<string, IEnumerable>();
            if (usages.Any())
            {
                sreportInvoiceUsage.Add("report_invoice_usage", usages);
                subData.Add("INVOICE_USAGE", sreportInvoiceUsage);
            }

            if (histories.Any())
            {
                var sreportHistory = new Dictionary<string, IEnumerable>();
                sreportHistory.Add("report_invoice_history", histories);
                subData.Add("REPORT_INVOICE_HISTORY", sreportHistory);
            }

            var param = new Dictionary<string, object>()
            {
                {"@COMPANY_NAME", company.COMPANY_NAME },
                {"@COMPANY_ADDRESS", company.ADDRESS },
                {"@LOGIN_NAME", loginName == null? "": loginName}
            };
            var report = ReportHelper.Instance.Load(reportName, data, subData, param);
            report.SummaryInfo.ReportTitle = reportName.Replace(".rpt","");
            return report;
        }

        public ReportDocument GetViewNewInvoice(string reportName, int printInvoiceId)
        {
            return GetReportNewDocument(reportName,"", printInvoiceId);
        }

        public ReportDocument GetPrintNewInvoice(string reportName, string printerName, int printInvoiceId)
        {
            var report = GetReportNewDocument(reportName, printerName, printInvoiceId);
            report.PrintOptions.PrinterName = printerName;
            report.PrintToPrinter(1, false, 1, 1000000);
            return report;
        }
    }
    public class ReportInvoice
    {
        public int PRINT_ORDER { get; set; }
        public long INVOICE_ID { get; set; }

        private string _invoice_id_string;
        public string INVOICE_ID_STRINGKEY { get { return INVOICE_ID.ToString(); } set { _invoice_id_string = value; } }
        public DateTime INVOICE_MONTH { get; set; }
        public string INVOICE_NO { get; set; }
        public string METER_CODE { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
        public DateTime INVOICE_DATE { get; set; }
        public decimal START_USAGE { get; set; }
        public decimal END_USAGE { get; set; }
        public int CUSTOMER_ID { get; set; }
        public decimal FORWARD_AMOUNT { get; set; }
        public decimal TOTAL_AMOUNT { get; set; }
        public decimal SETTLE_AMOUNT { get; set; }
        public decimal PAID_AMOUNT { get; set; }
        public decimal TOTAL_USAGE { get; set; }
        public int CURRENCY_ID { get; set; }
        public int CYCLE_ID { get; set; }
        public DateTime DUE_DATE { get; set; }
        public int INVOICE_STATUS { get; set; }
        public bool IS_SERVICE_BILL { get; set; }
        public int PRINT_COUNT { get; set; }
        public int RUN_ID { get; set; }
        public decimal DISCOUNT_USAGE { get; set; }
        public string DISCOUNT_USAGE_NAME { get; set; }
        public decimal DISCOUNT_AMOUNT { get; set; }
        public string DISCOUNT_AMOUNT_NAME { get; set; }
        public string INVOICE_TITLE { get; set; }
        public string LAST_NAME { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME_KH { get; set; }
        public string FIRST_NAME_KH { get; set; }
        public string ADDRESS { get; set; }
        public string HOUSE_NO { get; set; }
        public string STREET_NO { get; set; }
        public string PHONE_1 { get; set; }
        public string PHONE_2 { get; set; }
        public string COMPANY_NAME { get; set; }
        public string CUSTOMER_CODE { get; set; }
        public int AREA_ID { get; set; }
        public string AREA_NAME { get; set; }
        public string AREA_CODE { get; set; }
        public int BOX_ID { get; set; }
        public string BOX_CODE { get; set; }
        public int POLE_ID { get; set; }
        public string POLE_CODE { get; set; }
        public string AMPARE { get; set; }
        public decimal CURRENT_DUE { get; set; }
        public decimal ADJUST_AMOUNT { get; set; }
        public DateTime START_PAY_DATE { get; set; }
        public string CURRENCY_SING { get; set; }
        public int INVOICE_CUSTOMER_ID { get; set; }
        public long INVOICE_DETAIL_ID { get; set; }
        public long INVOICE_ID_DETAIL { get; set; }
        public decimal START_USAGE_DETAIL { get; set; }
        public decimal END_USAGE_DETAIL { get; set; }
        public decimal USAGE_DETAIL { get; set; }
        public decimal PRICE { get; set; }
        public int INVOICE_ITEM_ID { get; set; }
        public decimal AMOUNT { get; set; }
        public string CHARGE_DESCRIPTION { get; set; }
        public int REPORT_GROUP { get; set; }
        public int IS_DETAIL { get; set; }
        public int MULTIPLIER { get; set; }
        public int NO_USAGE_CUSTOMER { get; set; }
        public int NO_INVOICE_CUSTOMER { get; set; }
        public string ADJUST_NOTE { get; set; }
        public string STATISTIC { get; set; }
        public DateTime? LAST_PAY_DATE { get; set; }
        public decimal LAST_PAY { get; set; }
    }
    public class ReportInvoiceUsage
    {
        public int INVOICE_USAGE_ID { get; set; }
        public int INVOICE_ID { get; set; }
        private string _invoice_id_string;
        public string INVOICE_ID_STRINGKEY { get { return INVOICE_ID.ToString(); } set { _invoice_id_string = value; } }
        public int CUSTOMER_ID { get; set; }
        public int METER_ID { get; set; }
        public string METER_CODE { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public decimal START_USAGE { get; set; }
        public decimal END_USAGE { get; set; }
        public int MULTIPLIER { get; set; }
        public decimal TOTAL_USAGE { get; set; }
    }
    public class ReportInvoiceHistory
    {
        public int CUSTOMER_ID { get; set; }
        public DateTime INVOICE_MONTH { get; set; }
        public decimal TOTAL_USAGE { get; set; }
    }
    public class BillPrintingListModel 
    {
        public string TemplateName { get; set; }
        public bool IsOld { get; set; }

    } 
}
