﻿using EPower.Base.Models;
using HB01.Domain.Enums;
using HB01.Domain.Models.Transactions;
using HB01.Helpers;
using HB01.Logics;
using SoftTech;
using SoftTech.Component;
using SoftTech.Security.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace EPower.Base.Logic
{
    internal class PushPaymentLogic : Integration<Payment>
    {
        public static PushPaymentLogic Instance { get; } = new PushPaymentLogic();
        public override List<Payment> AvailableRecords(DateTime startDate, DateTime endDate)
        {
            var invs = from i in DBDataContext.Db.TBL_INVOICEs
                       join d in DBDataContext.Db.TBL_INVOICE_DETAILs on i.INVOICE_ID equals d.INVOICE_ID
                       where d.INVOICE_ITEM_ID != (int)InvoiceItem.Adjustment && d.INVOICE_ITEM_ID != (int)InvoiceItem.Reverse
                       group d by d.INVOICE_ID into g
                       select new
                       {
                           INVOICE_ID = g.Key,
                           EXCHANGE_RATE = g.First().EXCHANGE_RATE,
                           INVOICE_ITEM_ID = g.First().INVOICE_ITEM_ID
                       };

            var postedItems = from j in DBDataContext.Db.TBL_JOURNAL_ENTRies
                              join ja in DBDataContext.Db.TBL_JOURNAL_ENTRY_ACCOUNTs on j.JOURNAL_ENTRY_ID equals ja.JOURNAL_ENTRY_ID
                              where j.IS_ACTIVE && j.REFERENCE_TYPE_ID == (int)ReferenceType.Payment
                              select new
                              {
                                  j.JOURNAL_ENTRY_ID,
                                  ja.REFERENCE_ID
                              };
            var excludeInv = new List<int> { (int)InvoiceItem.Reverse, (int)InvoiceItem.Adjustment, (int)InvoiceItem.Power };
            var bills = from p in DBDataContext.Db.TBL_PAYMENTs
                        join d in DBDataContext.Db.TBL_PAYMENT_DETAILs on p.PAYMENT_ID equals d.PAYMENT_ID
                        join c in DBDataContext.Db.TLKP_CURRENCies on p.CURRENCY_ID equals c.CURRENCY_ID
                        join i in DBDataContext.Db.TBL_INVOICEs on d.INVOICE_ID equals i.INVOICE_ID
                        join t in invs on i.INVOICE_ID equals t.INVOICE_ID
                        join ct in DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs on i.CUSTOMER_CONNECTION_TYPE_ID equals ct.CUSTOMER_CONNECTION_TYPE_ID
                        join cs in DBDataContext.Db.TBL_ACCOUNT_CHARTs on p.PAYMENT_ACCOUNT_ID equals cs.ACCOUNT_ID
                        join j in postedItems on d.PAYMENT_DETAIL_ID equals j.REFERENCE_ID into l
                        from je in l.DefaultIfEmpty()
                        where p.PAY_DATE >= startDate && p.PAY_DATE <= endDate
                           && i.IS_SERVICE_BILL == false
                           && p.IS_ACTIVE
                           && p.USER_CASH_DRAWER_ID != 0
                           && cs.EXT_ACCOUNT_ID != 0
                           && c.EXTERNAL_CURRENCY_ID != 0
                           && je.JOURNAL_ENTRY_ID == null
                        select new Payment
                        {
                            PAYMENT_DETAIL_ID = d.PAYMENT_DETAIL_ID,
                            CURRENCY_ID = c.EXTERNAL_CURRENCY_ID,
                            CASH_ACCOUNT_ID = cs.EXT_ACCOUNT_ID,
                            AR_ACCOUNT_ID = i.CURRENCY_ID == (int)EPower.Currency.KHR ? ct.AR_KHR : i.CURRENCY_ID == (int)EPower.Currency.USD
                                              ? ct.AR_USD : i.CURRENCY_ID == (int)EPower.Currency.THB ? ct.AR_THB : ct.AR_VND,
                            PAY_AMOUNT = d.PAY_AMOUNT,
                            PAY_DATE = p.PAY_DATE,
                            InvoiceExchangeRate = t.EXCHANGE_RATE,
                            PaymentExchangeRate = p.EXCHANGE_RATE
                        };

            var services = from p in DBDataContext.Db.TBL_PAYMENTs
                           join d in DBDataContext.Db.TBL_PAYMENT_DETAILs on p.PAYMENT_ID equals d.PAYMENT_ID
                           join c in DBDataContext.Db.TLKP_CURRENCies on p.CURRENCY_ID equals c.CURRENCY_ID
                           join i in DBDataContext.Db.TBL_INVOICEs on d.INVOICE_ID equals i.INVOICE_ID
                           join id in invs on d.INVOICE_ID equals id.INVOICE_ID
                           join it in DBDataContext.Db.TBL_INVOICE_ITEMs on id.INVOICE_ITEM_ID equals it.INVOICE_ITEM_ID
                           join cs in DBDataContext.Db.TBL_ACCOUNT_CHARTs on p.PAYMENT_ACCOUNT_ID equals cs.ACCOUNT_ID
                           join j in postedItems on d.PAYMENT_DETAIL_ID equals j.REFERENCE_ID into l
                           from je in l.DefaultIfEmpty()
                           where p.PAY_DATE >= startDate && p.PAY_DATE <= endDate
                              && i.IS_SERVICE_BILL == true
                              && p.IS_ACTIVE
                              && p.USER_CASH_DRAWER_ID != 0
                              //&& (ar_bill.EXT_ACCOUNT_ID != 0 || ar_service.EXT_ACCOUNT_ID != 0)
                              && cs.EXT_ACCOUNT_ID != 0
                              && c.EXTERNAL_CURRENCY_ID != 0
                              && !excludeInv.Contains(id.INVOICE_ITEM_ID)
                              && je.JOURNAL_ENTRY_ID == null
                           select new Payment
                           {
                               PAYMENT_DETAIL_ID = d.PAYMENT_DETAIL_ID,
                               CURRENCY_ID = c.EXTERNAL_CURRENCY_ID,
                               CASH_ACCOUNT_ID = cs.EXT_ACCOUNT_ID,
                               AR_ACCOUNT_ID = i.CURRENCY_ID == (int)EPower.Currency.KHR ? it.AR_KHR : i.CURRENCY_ID == (int)EPower.Currency.USD
                                                 ? it.AR_USD : i.CURRENCY_ID == (int)EPower.Currency.THB ? it.AR_THB : it.AR_VND,
                               PAY_AMOUNT = d.PAY_AMOUNT,
                               PAY_DATE = p.PAY_DATE,
                               InvoiceExchangeRate = id.EXCHANGE_RATE,
                               PaymentExchangeRate = p.EXCHANGE_RATE
                           };
            var result = bills.Concat(services);
            var payment = from s in result
                          join a in DBDataContext.Db.TBL_ACCOUNT_CHARTs on s.AR_ACCOUNT_ID equals a.ACCOUNT_ID
                          where a.EXT_ACCOUNT_ID != 0
                          select new Payment
                          {
                              PAYMENT_DETAIL_ID = s.PAYMENT_DETAIL_ID,
                              CURRENCY_ID = s.CURRENCY_ID,
                              CASH_ACCOUNT_ID = s.CASH_ACCOUNT_ID,
                              AR_ACCOUNT_ID = a.EXT_ACCOUNT_ID,
                              PAY_AMOUNT = s.PAY_AMOUNT,
                              PAY_DATE = s.PAY_DATE,
                              InvoiceExchangeRate = s.InvoiceExchangeRate,
                              PaymentExchangeRate = s.PaymentExchangeRate
                          };

            return payment.ToList();
        }

        public override List<TBL_JOURNAL_ENTRY_ACCOUNT> EntryItems(List<Payment> list, int EntryId)
        {
            var items = from l in list
                        select new
                        {
                            INCOME = new TBL_JOURNAL_ENTRY_ACCOUNT
                            {
                                ACCOUNT_ID = l.CASH_ACCOUNT_ID,
                                AMOUNT = l.PAY_AMOUNT,
                                REFERENCE_ID = l.PAYMENT_DETAIL_ID,
                                JOURNAL_ENTRY_ID = EntryId
                            },
                            AR = new TBL_JOURNAL_ENTRY_ACCOUNT
                            {
                                ACCOUNT_ID = l.AR_ACCOUNT_ID,
                                AMOUNT = l.PAY_AMOUNT,
                                REFERENCE_ID = l.PAYMENT_DETAIL_ID,
                                JOURNAL_ENTRY_ID = EntryId
                            }
                        };
            return items.Select(x => x.INCOME).Union(items.Select(x => x.AR)).ToList();
        }

        public override List<JournalEntryLine> EntryLines(List<Payment> list)
        {
            var GainAccId = Current.Company.CompanySetting.GainExchangeRateAccountId;
            var LossAccId = Current.Company.CompanySetting.LossExchangeRateAccountId;
            var GainAcc = AccountLogic.Instance.Find(GainAccId);
            var LossAcc = AccountLogic.Instance.Find(LossAccId);


            var signGain = GainAcc.AccountType.Nature == AccountNatures.Expense ? -1 : 1;
            var signLoss = LossAcc.AccountType.Nature == AccountNatures.Expense ? -1 : 1;


            var payBills = (from b in list
                            group b by new { b.CURRENCY_ID, b.CASH_ACCOUNT_ID, b.AR_ACCOUNT_ID, b.InvoiceExchangeRate, b.PaymentExchangeRate }
                into g
                            select new
                            {
                                CASH = new JournalEntryLine
                                {
                                    AccountId = g.Key.CASH_ACCOUNT_ID,
                                    CurrencyId = g.Key.CURRENCY_ID,
                                    NetAmount = g.Distinct().Sum(x => x.PAY_AMOUNT) * g.Key.PaymentExchangeRate,
                                    Title = $"Bill payment - CASH",
                                    Active = true,
                                    CurrencyAmount = g.Distinct().Sum(x => x.PAY_AMOUNT),
                                    ExchangeRate = g.Key.PaymentExchangeRate,
                                    ExchangeRateId = -1
                                },
                                AR = new JournalEntryLine
                                {
                                    AccountId = g.Key.AR_ACCOUNT_ID,
                                    CurrencyId = g.Key.CURRENCY_ID,
                                    NetAmount = (-1 * g.Distinct().Sum(x => x.PAY_AMOUNT)) * g.Key.InvoiceExchangeRate,
                                    Title = $"Bill payment - AR",
                                    Active = true,
                                    CurrencyAmount = -1 * g.Distinct().Sum(x => x.PAY_AMOUNT),
                                    ExchangeRate = g.Key.InvoiceExchangeRate,
                                    ExchangeRateId = -1
                                },

                                GainLoss = new JournalEntryLine
                                {
                                    AccountId = 0,
                                    CurrencyId = g.Key.CURRENCY_ID,
                                    NetAmount = (g.Distinct().Sum(x => x.PAY_AMOUNT) * g.Key.PaymentExchangeRate) - (g.Distinct().Sum(x => x.PAY_AMOUNT) * g.Key.InvoiceExchangeRate),
                                    CurrencyAmount = 0,
                                    Title = $"GainLossExchange",
                                    Active = true,
                                    ExchangeRateId = -1
                                }
                            }).ToList();

            var result = new List<JournalEntryLine>();
            var bCash = payBills.Select(x => x.CASH).ToList();
            var bAR = payBills.Select(x => x.AR).ToList();
            var gainLoss = payBills.Select(x => x.GainLoss).ToList();
            //gainLoss.ForEach(x => { x.AccountId = x.NetAmount < 0 ? LossAccId : GainAccId; x.NetAmount = x.NetAmount < 0 ? signLoss * x.NetAmount : signGain * x.NetAmount; });
            foreach (var item in gainLoss)
            {
                if (item.NetAmount < 0)
                {
                    item.AccountId = LossAccId;
                    item.NetAmount = signLoss * item.NetAmount;
                }
                else
                {
                    item.AccountId = GainAccId;
                    item.NetAmount = signGain * item.NetAmount;
                }
            }
            result.AddRange(bCash);  //bCash.Union(bAR).Union(reversePayment).Union(reverseBill).ToList();
            result.AddRange(bAR);  //bCash.Union(bAR).Union(reversePayment).Union(reverseBill).ToList();
            result.AddRange(gainLoss);
            return result;
        }

        public override void PushJournal(DateTime startDate, DateTime endDate)
        {
            var k = 1;
            var currDate = DBDataContext.Db.GetSystemDate();
            var tran = (from t in AvailableRecords(startDate, endDate)
                        group t by new { t.CURRENCY_ID, PAY_DATE = t.PAY_DATE.Date } into g
                        orderby g.Key.PAY_DATE
                        select new Transaction
                        {
                            Date = g.Key.PAY_DATE,
                            CurrencyId = g.Key.CURRENCY_ID,
                            Transactions = g.ToList()
                        }).ToList();

            var journalId = AccountingLogic.Instance.Journal(AccountingConfig.JOURNAL_CASH).Id;

            foreach (var t in tran)
            {
                var tranOption = new TransactionOptions()
                {
                    IsolationLevel = IsolationLevel.ReadUncommitted,
                    Timeout = TimeSpan.MaxValue
                };

                TBL_JOURNAL_ENTRY afterUpdateJournal = new TBL_JOURNAL_ENTRY();
                TBL_JOURNAL_ENTRY beforeUpdateJournal = new TBL_JOURNAL_ENTRY();
                try
                {
                    using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required, tranOption))
                    {
                        if (t.Transactions.Count > 0)
                        {
                            Runner.Instance.Text = $"ចុះបញ្ជីការបង់ប្រាក់ {k++}/{tran.Count}";

                            //Sumit E-Power
                            var eJournal = new TBL_JOURNAL_ENTRY
                            {
                                REF_NO = "",
                                CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                                CREATE_ON = currDate,
                                ENTRY_DATE = t.Date,
                                IS_ACTIVE = true,
                                CURRENCY_ID = t.CurrencyId,
                                REFERENCE_TYPE_ID = (int)ReferenceType.Payment,
                                HB01_JOURNAL_ID = 0,
                                NOTE = ""
                            };
                            DBDataContext.Db.TBL_JOURNAL_ENTRies.InsertOnSubmit(eJournal);
                            DBDataContext.Db.SubmitChanges();
                            var line = EntryItems(t.Transactions, eJournal.JOURNAL_ENTRY_ID);
                            DBDataContext.Db.BulkCopy(line._ToDataTable(), $"HB01.{nameof(TBL_JOURNAL_ENTRY_ACCOUNT)}");
                            DBDataContext.Db.SubmitChanges();
                            eJournal._CopyTo(afterUpdateJournal);
                            eJournal._CopyTo(beforeUpdateJournal);

                            //Submit Pointer
                            var journal = new JournalEntry()
                            {
                                CompanyId = Current.CompanyId,
                                Active = true,
                                CurrencyId = t.CurrencyId,
                                EntryDate = t.Date,
                                RefNo = "From E-Power",
                                JournalId = journalId,
                                SourceRefId = "PaymentDetailIds." + string.Join(",", t.Transactions.Select(x => x.PAYMENT_DETAIL_ID).ToList()),
                                Posted = true,
                                Source = PostingSources.External,
                                Lines = EntryLines(t.Transactions),
                                Amount = t.Transactions.Sum(x => x.PAY_AMOUNT)
                            };

                            var j = JournalEntryLogic.Instance.PostExternal(journal);
                            afterUpdateJournal.HB01_JOURNAL_ID = j.Id;
                            afterUpdateJournal.REF_NO = j.EntryNo;
                        }
                        transaction.Complete();
                    }
                    DBDataContext.Db.Update(beforeUpdateJournal, afterUpdateJournal);
                    DBDataContext.Db.SubmitChanges();
                }
                catch (Exception e)
                {
                    //MsgBox.ShowInformation(e.Message);
                }
            }
        }
    }
}
