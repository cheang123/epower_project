﻿using EPower.Base.Models;
using EPower.Base.Properties;
using HB01.Domain.Enums;
using HB01.Domain.Models.Transactions;
using HB01.Helpers;
using HB01.Logics;
using SoftTech;
using SoftTech.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace EPower.Base.Logic
{
    internal class PushBillLogic : Integration<InvoiceBill>
    {
        internal static PushBillLogic Instance { get; } = new PushBillLogic();
        public override List<InvoiceBill> AvailableRecords(DateTime startDate, DateTime endDate)
        {
            var excludeInv = new List<int> { (int)InvoiceItem.Power, (int)InvoiceItem.Adjustment };

            var postedItems = from j in DBDataContext.Db.TBL_JOURNAL_ENTRies
                              join ja in DBDataContext.Db.TBL_JOURNAL_ENTRY_ACCOUNTs on j.JOURNAL_ENTRY_ID equals ja.JOURNAL_ENTRY_ID
                              where j.IS_ACTIVE && j.HB01_JOURNAL_ID != 0 && (j.REFERENCE_TYPE_ID == (int)ReferenceType.InvoiceBill || j.REFERENCE_TYPE_ID == (int)ReferenceType.InvoiceService)
                              select new
                              {
                                  j.JOURNAL_ENTRY_ID,
                                  ja.REFERENCE_ID
                              };

            var invs = (from i in DBDataContext.Db.TBL_INVOICEs
                        join id in DBDataContext.Db.TBL_INVOICE_DETAILs on i.INVOICE_ID equals id.INVOICE_ID
                        join c in DBDataContext.Db.TLKP_CURRENCies on i.CURRENCY_ID equals c.CURRENCY_ID
                        join j in postedItems on id.INVOICE_DETAIL_ID equals j.REFERENCE_ID into l
                        from je in l.DefaultIfEmpty()
                        where id.TRAN_DATE >= startDate
                         && id.TRAN_DATE <= endDate
                         && c.EXTERNAL_CURRENCY_ID != 0
                         && je.JOURNAL_ENTRY_ID == null
                        select new
                        {
                            id.INVOICE_DETAIL_ID,
                            id.INVOICE_ITEM_ID,
                            id.AMOUNT,
                            id.TAX_ID,
                            id.TAX_AMOUNT,
                            id.TRAN_DATE,
                            id.EXCHANGE_RATE,
                            i.CUSTOMER_CONNECTION_TYPE_ID,
                            i.IS_SERVICE_BILL,
                            c.CURRENCY_ID,
                            c.EXTERNAL_CURRENCY_ID
                        });

            var bills = from i in invs
                        join ct in DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs on i.CUSTOMER_CONNECTION_TYPE_ID equals ct.CUSTOMER_CONNECTION_TYPE_ID
                        //join ar in DBDataContext.Db.TBL_ACCOUNT_CHARTs on ct.AR_ACCOUNT equals ar.ACCOUNT_ID
                        join ic in DBDataContext.Db.TBL_ACCOUNT_CHARTs on ct.IC_ACCOUNT equals ic.ACCOUNT_ID
                        where i.IS_SERVICE_BILL == false
                        && (i.INVOICE_ITEM_ID == (int)InvoiceItem.Power || !excludeInv.Contains(i.INVOICE_ITEM_ID))
                        //&& ar.EXT_ACCOUNT_ID != 0
                        && ic.EXT_ACCOUNT_ID != 0
                        select new InvoiceBill
                        {
                            INVOICE_DETAIL_ID = i.INVOICE_DETAIL_ID,
                            TYPE_ID = ct.CUSTOMER_CONNECTION_TYPE_ID,
                            TYPE_NAME = ct.CUSTOMER_CONNECTION_TYPE_NAME,
                            AR_ACCOUNT_ID = i.CURRENCY_ID == (int)EPower.Currency.KHR ? ct.AR_KHR : i.CURRENCY_ID == (int)EPower.Currency.USD
                                                  ? ct.AR_USD : i.CURRENCY_ID == (int)EPower.Currency.THB ? ct.AR_THB : ct.AR_VND,
                            INCOME_ACCOUNT_ID = ic.EXT_ACCOUNT_ID,
                            TOTAL_AMOUNT = i.AMOUNT,
                            TAX_AMOUNT = i.TAX_AMOUNT,
                            TAX_ACCOUNT = 0,
                            CURRENCY_ID = i.EXTERNAL_CURRENCY_ID,
                            EntryDate = i.TRAN_DATE,
                            IS_SERVICE_BILL = false,
                            ExchangeRate = i.EXCHANGE_RATE
                        };

            var serviceBill = from i in invs
                              join it in DBDataContext.Db.TBL_INVOICE_ITEMs on i.INVOICE_ITEM_ID equals it.INVOICE_ITEM_ID
                              join ic in DBDataContext.Db.TBL_ACCOUNT_CHARTs on it.INCOME_ACCOUNT_ID equals ic.ACCOUNT_ID
                              join t in DBDataContext.Db.TBL_TAXes on i.TAX_ID equals t.TAX_ID into tl
                              from tx in tl.DefaultIfEmpty()
                              join a in DBDataContext.Db.TBL_ACCOUNT_CHARTs on tx.ACCOUNT_ID equals a.ACCOUNT_ID into al
                              from ta in al.DefaultIfEmpty()
                              where !excludeInv.Contains(i.INVOICE_ITEM_ID)
                              && i.IS_SERVICE_BILL
                              //&& i.INVOICE_ITEM_ID != (int)InvoiceItem.Reverse
                              && ic.EXT_ACCOUNT_ID != 0
                              select new InvoiceBill
                              {
                                  INVOICE_DETAIL_ID = i.INVOICE_DETAIL_ID,
                                  TYPE_ID = it.INVOICE_ITEM_ID,
                                  TYPE_NAME = it.INVOICE_ITEM_NAME,
                                  AR_ACCOUNT_ID = i.CURRENCY_ID == (int)EPower.Currency.KHR ? it.AR_KHR : i.CURRENCY_ID == (int)EPower.Currency.USD
                                                  ? it.AR_USD : i.CURRENCY_ID == (int)EPower.Currency.THB ? it.AR_THB : it.AR_VND,
                                  INCOME_ACCOUNT_ID = ic.EXT_ACCOUNT_ID,
                                  TOTAL_AMOUNT = i.AMOUNT,
                                  TAX_AMOUNT = i.TAX_AMOUNT,
                                  TAX_ACCOUNT = ta == null ? 0 : ta.EXT_ACCOUNT_ID,
                                  CURRENCY_ID = i.EXTERNAL_CURRENCY_ID,
                                  EntryDate = i.TRAN_DATE,
                                  IS_SERVICE_BILL = true,
                                  ExchangeRate = i.EXCHANGE_RATE
                              };

            //var revServiceBill = from i in invs
            //                     join 

            var q = from i in bills.Concat(serviceBill)
                    join ar in DBDataContext.Db.TBL_ACCOUNT_CHARTs on i.AR_ACCOUNT_ID equals ar.ACCOUNT_ID
                    where ar.EXT_ACCOUNT_ID != 0
                    select new InvoiceBill
                    {
                        INVOICE_DETAIL_ID = i.INVOICE_DETAIL_ID,
                        TYPE_ID = i.TYPE_ID,
                        TYPE_NAME = i.TYPE_NAME,
                        AR_ACCOUNT_ID = ar.EXT_ACCOUNT_ID,
                        INCOME_ACCOUNT_ID = i.INCOME_ACCOUNT_ID,
                        TOTAL_AMOUNT = i.TOTAL_AMOUNT,
                        TAX_AMOUNT = i.TAX_AMOUNT,
                        TAX_ACCOUNT = i.TAX_ACCOUNT,
                        CURRENCY_ID = i.CURRENCY_ID,
                        EntryDate = i.EntryDate,
                        IS_SERVICE_BILL = i.IS_SERVICE_BILL,
                        ExchangeRate = i.ExchangeRate
                    };

            return q.ToList();
        }

        public override List<TBL_JOURNAL_ENTRY_ACCOUNT> EntryItems(List<InvoiceBill> list, int EntryId)
        {
            var items = from l in list
                        select new
                        {
                            INCOME = new TBL_JOURNAL_ENTRY_ACCOUNT
                            {
                                ACCOUNT_ID = l.INCOME_ACCOUNT_ID,
                                AMOUNT = l.TOTAL_AMOUNT - l.TAX_AMOUNT,
                                REFERENCE_ID = l.INVOICE_DETAIL_ID,
                                JOURNAL_ENTRY_ID = EntryId
                            },
                            AR = new TBL_JOURNAL_ENTRY_ACCOUNT
                            {
                                ACCOUNT_ID = l.AR_ACCOUNT_ID,
                                AMOUNT = l.TOTAL_AMOUNT,
                                REFERENCE_ID = l.INVOICE_DETAIL_ID,
                                JOURNAL_ENTRY_ID = EntryId
                            },
                            TAX = new TBL_JOURNAL_ENTRY_ACCOUNT
                            {
                                ACCOUNT_ID = l.TAX_ACCOUNT,
                                AMOUNT = l.TAX_AMOUNT,
                                REFERENCE_ID = l.INVOICE_DETAIL_ID,
                                JOURNAL_ENTRY_ID = EntryId
                            }
                        };
            return items.Select(x => x.INCOME).Concat(items.Select(x => x.AR)).Concat(items.Select(x => x.TAX).Where(x => x.AMOUNT != 0)).ToList();
        }

        public override List<JournalEntryLine> EntryLines(List<InvoiceBill> list)
        {
            var lines = (from l in list
                         group l
                         by new
                         {
                             l.TYPE_ID,
                             l.TYPE_NAME,
                             l.AR_ACCOUNT_ID,
                             l.INCOME_ACCOUNT_ID,
                             l.TAX_ACCOUNT,
                             l.CURRENCY_ID,
                             l.ExchangeRate
                         } into g
                         select new
                         {
                             INCOME = new JournalEntryLine
                             {
                                 AccountId = g.Key.INCOME_ACCOUNT_ID,
                                 Title = $"Sold - Income - {g.Key.TYPE_NAME}",
                                 NetAmount = g.Sum(x => x.TOTAL_AMOUNT - x.TAX_AMOUNT) * g.Key.ExchangeRate,
                                 CurrencyId = g.Key.CURRENCY_ID,
                                 Active = true,
                                 RefId = $"Income.{g.Key.TYPE_ID}",
                                 CurrencyAmount = g.Sum(x => x.TOTAL_AMOUNT - x.TAX_AMOUNT),
                                 ExchangeRate = g.Key.ExchangeRate,
                                 ExchangeRateId = -1
                             },
                             AR = new JournalEntryLine
                             {
                                 AccountId = g.Key.AR_ACCOUNT_ID,
                                 Title = $"Sold - AR - {g.Key.TYPE_NAME}",
                                 NetAmount = g.Sum(x => x.TOTAL_AMOUNT) * g.Key.ExchangeRate,
                                 CurrencyId = g.Key.CURRENCY_ID,
                                 Active = true,
                                 RefId = $"AR.{g.Key.TYPE_ID}",
                                 CurrencyAmount = g.Sum(x => x.TOTAL_AMOUNT),
                                 ExchangeRate = g.Key.ExchangeRate,
                                 ExchangeRateId = -1
                             },
                             TAX = new JournalEntryLine
                             {
                                 AccountId = g.Key.TAX_ACCOUNT,
                                 Title = $"Sold - Tax - {g.Key.TYPE_NAME}",
                                 NetAmount = g.Sum(x => x.TAX_AMOUNT) * g.Key.ExchangeRate,
                                 CurrencyId = g.Key.CURRENCY_ID,
                                 Active = true,
                                 RefId = $"Tax.{g.Key.TYPE_ID}",
                                 CurrencyAmount = g.Sum(x => x.TAX_AMOUNT),
                                 ExchangeRate = g.Key.ExchangeRate,
                                 ExchangeRateId = -1
                             }
                         }).ToList();

            var incomes = lines.Select(x => x.INCOME).ToList();
            var AR = lines.Select(x => x.AR).ToList();
            var tax = lines.Select(x => x.TAX).Where(x => x.NetAmount != 0).ToList();

            var jeLine = new List<JournalEntryLine>() { };
            jeLine.AddRange(incomes);
            jeLine.AddRange(AR);
            jeLine.AddRange(tax);
            return jeLine;
        }

        public override void PushJournal(DateTime startDate, DateTime endDate)
        {
            var k = 1;
            var currDate = DBDataContext.Db.GetSystemDate();
            var tran = (from t in AvailableRecords(startDate, endDate)
                        group t by new { EntryDate = t.EntryDate.Date, t.CURRENCY_ID, t.IS_SERVICE_BILL } into g
                        orderby g.Key.EntryDate
                        select new Transaction
                        {
                            Date = g.Key.EntryDate,
                            CurrencyId = g.Key.CURRENCY_ID,
                            Transactions = g.ToList(),
                            IsServiceBill = g.Key.IS_SERVICE_BILL
                        }).ToList();

            var journalId = AccountingLogic.Instance.Journal(AccountingConfig.JOURNAL_SALE).Id;

            foreach (var t in tran)
            {
                var tranOption = new TransactionOptions()
                {
                    IsolationLevel = IsolationLevel.ReadUncommitted,
                    Timeout = TimeSpan.MaxValue
                };

                TBL_JOURNAL_ENTRY afterUpdateJournal = new TBL_JOURNAL_ENTRY();
                TBL_JOURNAL_ENTRY beforeUpdateJournal = new TBL_JOURNAL_ENTRY();

                try
                {
                    using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, tranOption))
                    {
                        if (t.Transactions.Count > 0)
                        {
                            Runner.Instance.Text = $"ចុះបញ្ជីវិក្កយបត្រអគ្គិសនី {k++}/{tran.Count}";

                            //Sumit E-Power
                            var eJournal = new TBL_JOURNAL_ENTRY
                            {
                                REF_NO = "",
                                CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                                CREATE_ON = currDate,
                                ENTRY_DATE = t.Date,
                                IS_ACTIVE = true,
                                CURRENCY_ID = t.CurrencyId,
                                REFERENCE_TYPE_ID = t.IsServiceBill ? (int)ReferenceType.InvoiceService : (int)ReferenceType.InvoiceBill,
                                HB01_JOURNAL_ID = 0,
                                NOTE = ""
                            };
                            DBDataContext.Db.TBL_JOURNAL_ENTRies.InsertOnSubmit(eJournal);
                            DBDataContext.Db.SubmitChanges();
                            var line = EntryItems(t.Transactions, eJournal.JOURNAL_ENTRY_ID);
                            DBDataContext.Db.BulkCopy(line._ToDataTable(), $"HB01.{nameof(TBL_JOURNAL_ENTRY_ACCOUNT)}");
                            DBDataContext.Db.SubmitChanges();
                            eJournal._CopyTo(afterUpdateJournal);
                            eJournal._CopyTo(beforeUpdateJournal);

                            //Submit Pointer

                            var journal = new JournalEntry()
                            {
                                CompanyId = Current.CompanyId,
                                Active = true,
                                CurrencyId = t.CurrencyId,
                                EntryDate = t.Date,
                                RefNo = "From E-Power",
                                JournalId = journalId,
                                SourceRefId = "InvoiceDetailIds." + string.Join(",", t.Transactions.Select(x => x.INVOICE_DETAIL_ID).ToList()),
                                Posted = true,
                                Source = PostingSources.External,
                                Lines = EntryLines(t.Transactions),
                                Amount = t.Transactions.Sum(x => x.TOTAL_AMOUNT),
                            };

                            var j = JournalEntryLogic.Instance.PostExternal(journal);
                            afterUpdateJournal.HB01_JOURNAL_ID = j.Id;
                            afterUpdateJournal.REF_NO = j.EntryNo;

                        }
                        transaction.Complete();
                    }
                    DBDataContext.Db.Update(beforeUpdateJournal, afterUpdateJournal);
                    DBDataContext.Db.SubmitChanges();
                }
                catch (Exception e)
                {
                    //MsgBox.ShowInformation(e.Message);
                }
            }
        }
    }
}
