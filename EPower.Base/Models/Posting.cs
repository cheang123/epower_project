﻿using System;

namespace EPower.Base.Models
{
    public class InvoiceBill
    {
        public long INVOICE_DETAIL_ID { get; set; }
        public int TYPE_ID { get; set; }
        public string TYPE_NAME { get; set; }
        public int AR_ACCOUNT_ID { get; set; }
        public int INCOME_ACCOUNT_ID { get; set; }
        public int CURRENCY_ID { get; set; }
        public decimal TOTAL_AMOUNT { get; set; }
        public decimal TAX_AMOUNT { get; set; }
        public int TAX_ACCOUNT { get; set; }
        public DateTime EntryDate { get; set; }
        public bool IS_SERVICE_BILL { get; set; }
        public decimal ExchangeRate { get; set; }
    }

    public class AdjustInvoice
    {
        public int ADJUST_INVOICE_ID { get; set; }
        public string INVOICE_ITEM_NAME { get; set; }
        public int AR_ACCOUNT_ID { get; set; }
        public int INCOME_ACCOUNT_ID { get; set; }
        public int CURRENCY_ID { get; set; }
        public decimal ADJUST_AMOUNT { get; set; }
        public decimal TAX_AMOUNT { get; set; }
        public int TAX_ACCOUNT { get; set; }
        public DateTime EntryDate { get; set; }
        public decimal ExchangeRate { get; set; }
    }

    class Payment
    {
        public int PAYMENT_DETAIL_ID { get; set; }
        public decimal PAY_AMOUNT { get; set; }
        public DateTime PAY_DATE { get; set; }
        public int CASH_ACCOUNT_ID { get; set; }
        public int AR_ACCOUNT_ID { get; set; }
        public int CURRENCY_ID { get; set; }
        public decimal PaymentExchangeRate { get; set; }
        public decimal InvoiceExchangeRate { get; set; }
    }

    class Deposit
    {
        public int DepositId { get; set; }
        public int CashAcc { get; set; }
        public int DepositAcc { get; set; }
        public decimal Amount { get; set; }
        public int CurrencyId { get; set; }
        public DateTime DepositDate { get; set; }
        public bool IsServiceBill { get; set; }
        public decimal PaymentExchangeRate { get; set; }
        public decimal InvoiceExchangeRate { get; set; }
    }
}
