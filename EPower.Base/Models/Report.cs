﻿
using EPower.Base.Properties;
using System;
using System.Collections.Generic;

namespace EPower.Base.Models
{
    public class ReportBaseModel
    {
        public string FORMAT_DIGIT { get; set; }
    }
    public class PowerIncomeReport
    {
        public List<PowerIncomeMonthlyIncludeAllReport> PowerIncomeMonthlyIncludeAllListModels { get; set; }
        public List<PowerIncomeMonthlyIncludeAllSummaryReport> PowerIncomeMonthlyIncludeAllSummaryListModels { get; set; }
        public List<PowerIncomeMonthlyReport> PowerIncomeMonthlyListModels { set; get; }
        public List<PowerIncomeByAreaReport> PowerIncomeByAreaListModels { get; set; }
        public List<PowerIncomeSummaryReport> PowerIncomeSummaryListModels { get; set; }
        public List<PowerIncomeYearlyReport> PowerIncomeYearlyListModels { set; get; }
        public List<PowerIncomeByAmpareReport> PowerIncomeByAmpareListModels { get; set; }
        public List<PowerInvoiceDetailReport> PowerInvoiceDetailListModels { get; set; }
        public List<CustomerNoBillReport> CustomerNoBillListModels { get; set; }
    }

    public class PowerIncomeMonthlyIncludeAllReport : ReportBaseModel
    {
        //parameter
        public string COMPANY_NAME { get; set; }
        public string COMPANY_ADDRESS { get; set; }
        public string COMPANY_PHONE { get; set; }
        public DateTime MONTH { get; set; }
        public int AREA_ID { get; set; }
        public int CUS_TYPE_ID { get; set; }
        public int CYCLE_ID { get; set; }
        public int PRICE_ID { get; set; }
        public int CURRENCY_ID { get; set; }
        public int CUS_STATUS_ID { get; set; }
        public string AREA_CODE { get; set; }
        public string AREA_NAME { get; set; }
        public string PRICE_NAME { get; set; }
        public string CUS_STATUS_NAME { get; set; }
        public string CUS_TYPE_NAME { get; set; }
        public string CYCLE_NAME { get; set; }
        public string CURRENCY_NAME { get; set; }
        public string USER_NAME { get; set; }
        public List<PowerIncomeMonthlyIncludeAllReportDetail> DETAILS { get; set; }
        public PowerIncomeMonthlyIncludeAllSummaryReport SUBREPORT { get; set; }
    }
    public class PowerIncomeMonthlyIncludeAllReportDetail : ReportBaseModel
    {
        public int? RowNumber { get; set; }

        public int ParentId { get; set; }
        public int RenderId { get; set; }

        public int CURRENCY_ID { get; set; }
        public int CUSTOMER_ID { get; set; }
        public string METER_CODE { get; set; }
        public DateTime MONTH { get; set; }
        public decimal? START_USAGE { get; set; }
        public decimal? END_USAGE { get; set; }
        public decimal? TOTAL_USAGE { get; set; }
        public decimal? SETTLE_AMOUNT { get; set; }
        public decimal? PAID_AMOUNT { get; set; }
        public decimal? FORWARD_AMOUNT { get; set; }
        public decimal? PAID_AMOUNT_FORWARD { get; set; }
        public decimal? MAINTENANCE_FEE { get; set; }
        public decimal? PAID_AMOUNT_MAINTENANCE_FEE { get; set; }
        public decimal? OTHER_INCOME { get; set; }
        public decimal? PAID_AMOUNT_OTHER_INCOME { get; set; }
        public string CURRENCY_SING { get; set; }
        public string AREA_CODE { get; set; }
        public string CUSTOMER_CODE { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public int STATUS_ID { get; set; }
        public string CUS_STATUS_NAME { get; set; }


        //{REPORT_POWER_INCOME_MONTHLY_INCLUDE_ALL;1.PAID_AMOUNT
        //    }+
        //{REPORT_POWER_INCOME_MONTHLY_INCLUDE_ALL;1.PAID_AMOUNT_FORWARD
        //}
        //+
        //{ REPORT_POWER_INCOME_MONTHLY_INCLUDE_ALL; 1.PAID_AMOUNT_OTHER_INCOME}
        //+
        //{ REPORT_POWER_INCOME_MONTHLY_INCLUDE_ALL; 1.PAID_AMOUNT_MAINTENANCE_FEE}
        public decimal? Balance
        {
            get
            {
                var totalAmount = SETTLE_AMOUNT + FORWARD_AMOUNT + PAID_AMOUNT_FORWARD + MAINTENANCE_FEE + OTHER_INCOME + PAID_AMOUNT_OTHER_INCOME;
                var totalPaid = PAID_AMOUNT + PAID_AMOUNT_FORWARD + PAID_AMOUNT_OTHER_INCOME + PAID_AMOUNT_MAINTENANCE_FEE;
                var total = totalAmount - totalPaid;
                return total;
            }
            //set { this.Balance = value; }
        }
        public decimal? PaidAmount
        {
            get
            {
                var totalPaid = PAID_AMOUNT + PAID_AMOUNT_FORWARD + PAID_AMOUNT_OTHER_INCOME + PAID_AMOUNT_MAINTENANCE_FEE;
                return totalPaid;
            }
            //set
            //{
            //    this.PaidAmount = value;
            //}
        }
    }


    public class PowerIncomeMonthlyIncludeAllSummaryReport : ReportBaseModel
    {
        public int AREA_ID { get; set; }
        public DateTime MONTH { get; set; }
        public int CYCLE_ID { get; set; }
        public int PRICE_ID { get; set; }
        public int CUS_STATUS_ID { get; set; }
        public int CURRENCY_ID { get; set; }
        public int CUS_TYPE_ID { get; set; }
        public List<PowerIncomeMonthlyIncludeAllSummaryReportDetail> Details { get; set; }
    }

    public class PowerIncomeMonthlyIncludeAllSummaryReportDetail : ReportBaseModel
    {
        public int CUSTOMER_ID { get; set; }
        public string CUSTOMER_CODE { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public int CUS_TYPE_ID { get; set; }
        public int CYCLE_ID { get; set; }
        public int CUS_STATUS_ID { get; set; }
        public decimal TOTAL_MAINTENANCE_FEE { get; set; }
        public int TOTAL_CUSTOMER { get; set; }
        public int TOTAL_METER { get; set; }
        public decimal START_USAGE { get; set; }
        public decimal END_USAGE { get; set; }
        public decimal TOTAL_USAGE { get; set; }
        public decimal TOTAL_SETTLE_AMOUNT { get; set; }
        public decimal TOTAL_PAID_AMOUNT { get; set; }
        public decimal TOTAL_FORWARD_AMOUNT { get; set; }
        public decimal TOTAL_PAID_AMOUNT_FORWARD { get; set; }
        public decimal TOTAL_PAID_AMOUNT_MAINTENANCE_FEE { get; set; }
        public decimal TOTAL_OTHER_INCOME { get; set; }
        public decimal TOTAL_PAID_AMOUNT_OTHER_INCOME { get; set; }
        public int CURRENCY_ID { get; set; }
        public string CURRENCY_NAME { get; set; }
        public string CURRENCY_SING { get; set; }
        public int AREA_ID { get; set; }
        public string AREA_CODE { get; set; }
        public int STATUS_ID { get; set; }
        public DateTime MONTH { get; set; }
        public int PRICE_ID { get; set; }
        public string CUS_STATUS_NAME { get; set; }
        public string FormatDigit { get; set; }
        public int? RowNumber { get; set; }

        public decimal BALANCE
        {
            get
            {
                var totalAmount = TOTAL_SETTLE_AMOUNT + TOTAL_FORWARD_AMOUNT + TOTAL_PAID_AMOUNT_FORWARD + TOTAL_MAINTENANCE_FEE + TOTAL_OTHER_INCOME + TOTAL_PAID_AMOUNT_OTHER_INCOME;
                var totalPaid = TOTAL_PAID_AMOUNT + TOTAL_PAID_AMOUNT_FORWARD + TOTAL_PAID_AMOUNT_OTHER_INCOME + TOTAL_PAID_AMOUNT_MAINTENANCE_FEE;
                var total = totalAmount - totalPaid;
                return total;
            }
        }

    }
    public class PowerIncomeMonthlyReport : ReportBaseModel
    {
        public List<PowerIncomeMonthlyDetailReport> DETAILS { get; set; }
        public DateTime MONTH { get; set; }
        public int AREA_ID { get; set; }
        public string AREA_NAME { get; set; }
        public int CURRENCY_ID { get; set; }
        public string CURRENCY_NAME { get; set; }
        public int PRICE_ID { get; set; }
        public string PRICE_NAME { get; set; }
        public int CUS_TYPE_ID { get; set; }
        public string CUS_TYPE_NAME { get; set; }
        public int CUS_STATUS_ID { get; set; }
        public string CUS_STATUS_NAME { get; set; }
        public int CYCLE_ID { get; set; }
        public string CYCLE_NAME { get; set; }
        public string USER_NAME { get; set; }
    }
    public class PowerIncomeMonthlyDetailReport : ReportBaseModel
    {
        public int? RowNumber { get; set; }
        public int ParentId { get; set; }
        public int RenderId { get; set; }
        public int CURRENCY_ID { get; set; }
        public string CURRENCY_NAME { get; set; }
        public string METER_CODE { get; set; }
        public decimal? START_USAGE { get; set; }
        public decimal? END_USAGE { get; set; }
        public decimal TOTAL_USAGE { get; set; }
        public decimal SETTLE_AMOUNT { get; set; }
        public decimal FORWARD_AMOUNT { get; set; }
        public string CURRENCY_SING { get; set; }
        public int AREA_ID { get; set; }
        public string AREA_CODE { get; set; }
        public string AREA_NAME { get; set; }
        public string POLE_CODE { get; set; }
        public string BOX_CODE { get; set; }
        public string CUSTOMER_CODE { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string AMPARE_NAME { get; set; }
        public string CUS_STATUS_NAME { get; set; }

        public decimal TOTAL_DUE
        {
            get
            {
                return SETTLE_AMOUNT + FORWARD_AMOUNT;
            }
        }
    }
    public class PowerIncomeByAreaReport : ReportBaseModel
    {
        public List<PowerIncomeByAreaDetailReport> DETAILS { get; set; }
        public DateTime MONTH { get; set; }
        public int AREA_ID { get; set; }
        public string AREA_NAME { get; set; }
        public int CURRENCY_ID { get; set; }
        public string CURRENCY_NAME { get; set; }
        public int PRICE_ID { get; set; }
        public string PRICE_NAME { get; set; }
        public int CUS_TYPE_ID { get; set; }
        public string CUS_TYPE_NAME { get; set; }
        public int CUS_STATUS_ID { get; set; }
        public string CUS_STATUS_NAME { get; set; }
        public int CYCLE_ID { get; set; }
        public string CYCLE_NAME { get; set; }
        public string USER_NAME { get; set; }
    }
    public class PowerIncomeByAreaDetailReport : ReportBaseModel
    {
        public int? RowNumber { get; set; }
        public int ParentId { get; set; }
        public int RenderId { get; set; }
        public string AMPARE_NAME { get; set; }
        public int CURRENCY_ID { get; set; }
        public string CURRENCY_NAME { get; set; }
        public string CURRENCY_SING { get; set; }
        public string AREA_NAME { get; set; }
        public int POSTPAID_COUNT { get; set; }
        public decimal POSTPAID_USAGE { get; set; }
        public decimal POSTPAID_AMOUNT { get; set; }
        public decimal POSTPAID_PAID_AMOUNT { get; set; }
        public decimal POSTPAID_BALANCE { get; set; }
        public string CUS_STATUS_NAME { get; set; }


    }
    public class PowerIncomeSummaryReport : ReportBaseModel
    {
        public List<PowerIncomeSummaryDetailReport> DETAILS { get; set; }
        public DateTime MONTH { get; set; }
        public int AREA_ID { get; set; }
        public string AREA_NAME { get; set; }
        public int CURRENCY_ID { get; set; }
        public string CURRENCY_NAME { get; set; }
        public int PRICE_ID { get; set; }
        public string PRICE_NAME { get; set; }
        public int CUS_TYPE_ID { get; set; }
        public string CUS_TYPE_NAME { get; set; }
        public int CUS_STATUS_ID { get; set; }
        public string CUS_STATUS_NAME { get; set; }
        public int CYCLE_ID { get; set; }
        public string CYCLE_NAME { get; set; }
        public string USER_NAME { get; set; }
    }
    public class PowerIncomeSummaryDetailReport : ReportBaseModel
    {
        public int? RowNumber { get; set; }
        public int ParentId { get; set; }
        public int RenderId { get; set; }
        public int CURRENCY_ID { get; set; }
        public string CURRENCY_NAME { get; set; }
        public string CURRENCY_SING { get; set; }
        public int GROUP_ID { get; set; }
        public string GROUP_NAME { get; set; }
        public string CUSTOMER_TYPE_NAME { get; set; }
        public int? POSTPAID_COUNT { get; set; }
        public decimal? POSTPAID_USAGE { get; set; }
        public decimal? POSTPAID_AMOUNT { get; set; }
        public int PREPAID_COUNT { get; set; }
        public decimal? PREPAID_USAGE { get; set; }
        public decimal? PREPAID_AMOUNT { get; set; }
        public string CUS_STATUS_NAME { get; set; }
        public string FormatDigit { get; set; }


        public decimal? Customer_Count
        {
            get
            {
                var CustomerCount = POSTPAID_COUNT + PREPAID_COUNT;
                return CustomerCount;
            }

        }
        public decimal? Usage_Count
        {
            get
            {
                var usageCount = POSTPAID_USAGE + PREPAID_USAGE;
                return usageCount;
            }
        }
        public decimal? TotalAmount
        {
            get
            {
                var totalAmount = POSTPAID_AMOUNT + PREPAID_AMOUNT;
                return totalAmount;

            }
        }
    }
    public class PowerIncomeYearlyReport : ReportBaseModel
    {
        public List<PowerIncomeYearlyDetailReport> DETAILS { get; set; }
        public int YEAR { get; set; }
        public int AREA_ID { get; set; }
        public string AREA_NAME { get; set; }
        public int CURRENCY_ID { get; set; }
        public string CURRENCY_NAME { get; set; }
        public int PRICE_ID { get; set; }
        public string PRICE_NAME { get; set; }
        public int CUS_TYPE_ID { get; set; }
        public string CUS_TYPE_NAME { get; set; }
        public int CUS_STATUS_ID { get; set; }
        public string CUS_STATUS_NAME { get; set; }
        public int CYCLE_ID { get; set; }
        public string CYCLE_NAME { get; set; }
        public string USER_NAME { get; set; }
    }
    public class PowerIncomeYearlyDetailReport : ReportBaseModel
    {
        public int? RowNumber { get; set; }
        public int ParentId { get; set; }
        public int RenderId { get; set; }
        public int CURRENCY_ID { get; set; }
        public string CURRENCY_NAME { get; set; }
        public string CURRENCY_SING { get; set; }
        public DateTime INVOICE_MONTH { get; set; }
        public string INVOICE_MONTH_STRING { get; set; }
        public string CUSTOMER_TYPE_NAME { get; set; }
        public decimal? PRICE { get; set; }
        public int POSTPAID_COUNT { get; set; }
        public decimal? POSTPAID_USAGE { get; set; }
        public decimal? POSTPAID_AMOUNT { get; set; }
        public int PREPAID_COUNT { get; set; }
        public decimal? PREPAID_USAGE { get; set; }
        public decimal? PREPAID_AMOUNT { get; set; }
        public string CUS_STATUS_NAME { get; set; }

        public decimal? M3
        {
            get
            {
                var m3 = POSTPAID_USAGE + PREPAID_USAGE;
                return m3;
            }
        }
        public decimal? Amount
        {
            get
            {
                var amount = POSTPAID_AMOUNT + PREPAID_AMOUNT;
                return amount;
            }
        }
    }
    public class PowerIncomeByAmpareReport : ReportBaseModel
    {
        public List<PowerIncomeByAmpareDetailReport> DETAILS { get; set; }
        public DateTime MONTH { get; set; }
        public int AREA_ID { get; set; }
        public string AREA_NAME { get; set; }
        public int CURRENCY_ID { get; set; }
        public string CURRENCY_NAME { get; set; }
        public int PRICE_ID { get; set; }
        public string PRICE_NAME { get; set; }
        public int CUS_TYPE_ID { get; set; }
        public string CUS_TYPE_NAME { get; set; }
        public int CUS_STATUS_ID { get; set; }
        public string CUS_STATUS_NAME { get; set; }
        public int CYCLE_ID { get; set; }
        public string CYCLE_NAME { get; set; }
        public string USER_NAME { get; set; }
    }
    public class PowerIncomeByAmpareDetailReport : ReportBaseModel
    {
        public int? RowNumber { get; set; }
        public int CURRENCY_ID { get; set; }
        public string CURRENCY_NAME { get; set; }
        public string CURRENCY_SING { get; set; }
        public string AMPARE_NAME { get; set; }
        public int POSTPAID_COUNT { get; set; }
        public decimal POSTPAID_USAGE { get; set; }
        public decimal POSTPAID_AMOUNT { get; set; }
        public int PREPAID_COUNT { get; set; }
        public decimal PREPAID_USAGE { get; set; }
        public decimal PREPAID_AMOUNT { get; set; }
        public string CUS_STATUS_NAME { get; set; }
        public string FormatDigit { get; set; }


    }
    public class PowerInvoiceDetailReport : ReportBaseModel
    {
        public int? RowNumber { get; set; }
        public List<PowerIncomeByInvoiceDetailReport> DETAILS { get; set; }
        public DateTime MONTH { get; set; }
        public int AREA_ID { get; set; }
        public string AREA_NAME { get; set; }
        public int CURRENCY_ID { get; set; }
        public string CURRENCY_NAME { get; set; }
        public int PRICE_ID { get; set; }
        public string PRICE_NAME { get; set; }
        public int CUS_TYPE_ID { get; set; }
        public int CUS_STATUS_ID { get; set; }
        public int CYCLE_ID { get; set; }
        public string CYCLE_NAME { get; set; }
        public string USER_NAME { get; set; }


    }
    public class PowerIncomeByInvoiceDetailReport : ReportBaseModel
    {
        public int? RowNumber { get; set; }
        public int ParentId { get; set; }
        public int RenderId { get; set; }
        public int CURRENCY_ID { get; set; }
        public string CURRENCY_SING { get; set; }
        public string METER_CODE { get; set; }
        public decimal? START_USAGE { get; set; }
        public decimal? END_USAGE { get; set; }
        public decimal? TOTAL_USAGE { get; set; }
        public decimal SETTLE_AMOUNT { get; set; }
        public decimal? PRICE { get; set; }
        public string INVOICE_NO { get; set; }
        public string BOX_CODE { get; set; }
        public string CUSTOMER_CODE { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string AMPARE_NAME { get; set; }
        public string CUS_STATUS_NAME { get; set; }


    }
    public class CustomerNoBillReport : ReportBaseModel
    {
        public List<CustomerNoBillDetailReport> DETAILS { get; set; }
        public DateTime MONTH { get; set; }
        public int AREA_ID { get; set; }
        public string AREA_NAME { get; set; }
        public int CURRENCY_ID { get; set; }
        public string CURRENCY_NAME { get; set; }
        public int PRICE_ID { get; set; }
        public string PRICE_NAME { get; set; }
        public int CUS_TYPE_ID { get; set; }
        public string CUS_TYPE_NAME { get; set; }
        public int CUS_STATUS_ID { get; set; }
        public string CUS_STATUS_NAME { get; set; }
        public int CYCLE_ID { get; set; }
        public string CYCLE_NAME { get; set; }
        public string USER_NAME { get; set; }
    }
    public class CustomerNoBillDetailReport : ReportBaseModel
    {
        public int? RowNumber { get; set; }
        public int ParentId { get; set; }
        public int RenderId { get; set; }
        public string CUSTOMER_CODE { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string AREA_NAME { get; set; }
        public string AREA_CODE { get; set; }
        public string AMPARE_NAME { get; set; }
        public string BOX_CODE { get; set; }
        public string METER_CODE { get; set; }
        public decimal? END_USAGE { get; set; }
        public int STATUS_ID { get; set; }
        public string CUS_STATUS_NAME { get; set; }
        public DateTime? ACTIVATE_DATE { get; set; }

    }
    enum ReportName
    {
        PowerIncomeMonthlyIncludeAll = 1,
        PowerIncomeMonthly = 2,
        PowerInvoiceDetail = 3,
        PowerIncomeByArea = 4,
        PowerIncomeByAmpare = 5,
        PowerIncomeSummary = 6,
        PowerIncomeYearly = 7,
        ReportCustomersNoBill = 8
    }
    public class ReportPaymentSearchParam
    {
        public DateTime DATE_PAID_FROM { get; set; }
        public DateTime DATE_PAID_TO { get; set; }
        public int PLAN_GROUP { get; set; }
        public int PAYMENT_ACCOUNT_ID { get; set; }
        public string BANK_NAME { get; set; } = "";
        public string BANK_BRANCH { get; set; } = "";
        public int BILLING_CYCLE_ID { get; set; }
        public int AREA_ID { get; set; }
        public int ITEM_TYPE_ID { get; set; }
        public int ITEM_ID { get; set; }
        public int CUSTOMER_TYPE_ID { get; set; }
        public int CUSTOMER_CONNETION_TYPE_ID { get; set; }
        public int PRICE_ID { get; set; }
        public int CURRENCY_ID { get; set; }
        public int USER_CASH_DRAWER_ID { get; set; }
    }
    public class ReportPaymentModel
    {
        public long ID { get; set; }
        public string No { get; set; }
        public string ROW_NO { get { return ID.ToString(); } set { value = No; } }
        public string CUSTOMER_CODE { set; get; }
        public string CUSTOMER_NAME { set; get; }
        public string PHONE_NUMBER { set; get; }
        public string CUSTOMER_TYPE { set; get; }
        public string ADDRESS_CUSTOMER { set; get; }
        public string METER_CODE { set; get; }
        public string AMPARE_NAME { set; get; }
        public string AREA_NAME { set; get; }
        public string POLE_NAME { set; get; }
        public string BOX_CODE { set; get; }
        public DateTime CREATE_ON { set; get; }
        public DateTime INVOICE_DATE { set; get; }

        public DateTime PAID_DATE { set; get; }

        public DateTime BANK_CUT_OFF_DATE { set; get; }
        public DateTime _EXACT_PAID_DATE;
        public DateTime EXACT_PAID_DATE { get { return PAID_DATE; } set { _EXACT_PAID_DATE = value; } }
        public DateTime _SHORT_PAID_DATE;
        public DateTime SHORT_PAID_DATE { get { return PAID_DATE.Date; } set { _SHORT_PAID_DATE = value; } }

        public DateTime DUE_DATE { set; get; }
        public string REFERENCE { set; get; }
        public string DESCRIPTION { set; get; }
        public string PAYMENT_ACCOUNT { set; get; }
        public string PAYMENT_METHOD { set; get; }
        public string PAY_AT_BANK { set; get; }
        public string RECEIVE_BY { set; get; }
        public decimal TOTAL_PAY { set; get; }
        public decimal TOTAL_VOID { set; get; }
        public decimal TOTAL_PAID { set; get; }
        public string STATUS { set; get; }
        public string NOTE { set; get; }
        public string CURRENCY { get; set; }
        public int NUMBER_OF_INVOICE { get; set; } = 1;
        public string CASH_DRAWER_NAME { get; set; }
        public string PAYMENT_NO { get; set; }
        public string BILLING_CYCLE { get; set; }
        public string PRICE_NAME { get; set; }
        public string INVOICE_ITEM_TYPE_NAME { get; set; }
        public string INVOICE_ITEM_NAME { get; set; }
        public bool IS_POST_PAID { get; set; }
        public string _PLAN;
        public string PLAN
        {
            get
            {
                return (IS_POST_PAID == false) ? Resources.PREPAID : Resources.POSTPAID;
            }
            set { _PLAN = value; }
        }
        public string _PAYMENT_TYPE_GROUP;
        public string PAYMENT_TYPE_GROUP
        {
            get
            {
                return (PAY_AT_BANK == "-") ? Resources.PAY_AT_COUNTER : Resources.PAY_AT_BANK;
            }
            set { _PAYMENT_TYPE_GROUP = value; }
        }
        public string TYPE_OF_TRANSACTION { get; set; }
    }
    public class ReportAgingSearchParam
    {
        public DateTime DATE { get; set; }
        public int INTERVAL { get; set; }
    }
    public class ReportAgingModel
    {
        public int ID { get; set; }
        public string No { get; set; }
        public string ROW_NO { get { return ID.ToString(); } set { value = No; } }
        public string INVOICE_ITEM_TYPE { get; set; }
        public string INVOICE_ITEM { get; set; }
        public int STATUS_ID { get; set; }
        public string STATUS { get; set; }
        public string INVOICE_NO { get; set; }
        public DateTime? INVOICE_DATE { get; set; }
        public DateTime? INVOICE_MONTH { get; set; }
        public string INVOICE_TITLE { get; set; }
        public string CUSTOMER_CODE { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string METER_CODE { get; set; }
        public string PHONE { get; set; }
        public string AREA_NAME { get; set; }
        public string POLE_CODE { get; set; }
        public string BOX_CODE { get; set; }
        public string CYCLE_NAME { get; set; }
        public string CUSTOMER_GROUP { get; set; }
        public string CUSTOMER_CONNECTION_TYPE { get; set; }
        private decimal _P0;
        public decimal P0
        {
            get
            {
                if (OPEN_DAYS <= 0)
                {
                    return SETTLE_AMOUNT;
                }
                else
                {
                    return 0;
                }
            }
            set { _P0 = value; }
        }

        public decimal P1
        {
            get
            {
                if (OPEN_DAYS > 0 && OPEN_DAYS <= 30)
                {
                    return SETTLE_AMOUNT;
                }
                else
                {
                    return 0;
                }
            }
            set { _P0 = value; }
        }

        public decimal P2
        {
            get
            {
                if (OPEN_DAYS > 30 && OPEN_DAYS <= 60)
                {
                    return SETTLE_AMOUNT;
                }
                else
                {
                    return 0;
                }
            }
            set { _P0 = value; }
        }

        public decimal P3
        {
            get
            {
                if (OPEN_DAYS > 60 && OPEN_DAYS <= 90)
                {
                    return SETTLE_AMOUNT;
                }
                else
                {
                    return 0;
                }
            }
            set { _P0 = value; }
        }
        public decimal P4
        {
            get
            {
                if (OPEN_DAYS > 90 && OPEN_DAYS <= 120)
                {
                    return SETTLE_AMOUNT;
                }
                else
                {
                    return 0;
                }
            }
            set { _P0 = value; }
        }
        public decimal P5
        {
            get
            {
                if (OPEN_DAYS > 120)
                {
                    return SETTLE_AMOUNT;
                }
                else
                {
                    return 0;
                }
            }
            set { _P0 = value; }
        }
        public decimal SETTLE_AMOUNT { get; set; }
        public decimal PAID_AMOUNT { get; set; }
        public string CURRENCY_SING { get; set; }
        public string CURRENCY_CODE { get; set; }
        public int OPEN_DAYS { get; set; }
        public decimal TOTAL
        {
            get
            {
                return SETTLE_AMOUNT - PAID_AMOUNT;
            }
            set { _P0 = value; }
        }
    }
    public class GroupCustomerType
    {
        public int ParentId { get; set; }
        public int RenderId { get; set; }
        public int TYPE_ID { get; set; }
        public string TYPE_NAME { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public int AREA_ID { get; set; }
        public string AREA_NAME { get; set; }
        public int CYCLE_ID { get; set; }
        public string CYCLE_NAME { get; set; }
        public int CURRENCY_ID { get; set; }
        public string CURRENCY_NAME { get; set; }
        public int CUSTOMER_TYPE_ID { get; set; }
        public int CUSTOMER_CONNETION_TYPE_ID { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string CUSTOMER_GROUP_NAME { get; set; }
        public string CUSTOMER_CONNECTION_TYPE_NAME { get; set; }
        public int TOTAL_CUSTOMER { get; set; }
        public decimal KWH_USAGE { get; set; }
        public decimal KVAR_USAGE { get; set; }
        public decimal KWH_AMOUNT { get; set; }
        public decimal KVAR_AMOUNT { get; set; }
        public decimal CAPACITY_CHARGE { get; set; }
        public decimal TOTAL_AMOUNT { get; set; }
        public string CURRENCY_SING { get; set; }
        public string CURRENCY_CODE { get; set; }
        public string FORMAT { get; set; }
        public string CUSTOMER_GROUP_NAME_SUB { get; set; }
    }

    public class GeneralCustomerListModel
    {
        public int Id { get; set; }
        public string Customer_Code { get; set; }
        public string Customer_Name_Kh { get; set; }
        public string Village_Code { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string National_Card_No { get; set; }
        public DateTime Birth_Date_Raw { get; set; }
        public DateTime? Birth_Date
        {
            get
            {
                if (Birth_Date_Raw.Year == 1900)
                {
                    return null;
                }
                else
                {
                    return Birth_Date_Raw;
                }
            }
        }
        public string Birth_Place { get; set; }
        public string Job { get; set; }
        public string House_No { get; set; }
        public string Street_No { get; set; }
        public string Area_Name { get; set; }
        public string Box_Code { get; set; }
        public string Pole_Code { get; set; }
        public DateTime Activate_Date { get; set; }
        public DateTime Closed_Date { get; set; }
        public string Status { get; set; }
    }
}
