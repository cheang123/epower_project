﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EPower.Base.Models
{
    public class JournalHistoryList
    {
        public int RenderId { get; set; }
        public int ParentId { get; set; }
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public string RefNo { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public DateTime? TranDate { get; set; }
        public DateTime? PostedDate { get; set; }
        public decimal? PostedAmount { get; set; }
        public decimal? Amount { get; set; }
        public int CurrencyId { get; set; }
        public string CurrencyCode { get; set; }
        public string CurrencySign { get; set; }
        public string Note { get; set; }
    }
}
