﻿using EPower.Base.Helper.DevExpressCustomize;
using SoftTech.Component;
using SoftTech.Helper;
using Spire.Xls;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EPower.Base.Helper
{
    public class CsvHelper
    {
        public static void ExportCsv(CrystalReportHelper ch = null)
        {
            var ReportName = ch.ReportName;
            Runner.RunNewThread(() =>
            {
                var excel = $"{ReportName}{DateTime.Now.ToString("yyyy-MM")}.xls";
                var csv = $"{ReportName}{DateTime.Now.ToString("yyyy-MM")}.csv";
                var downloadPath = ExportData.NextAvailableFilename(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"\Downloads\");

                if (!Directory.Exists(downloadPath))
                {
                    Directory.CreateDirectory(downloadPath);
                }

                excel = ExportData.NextAvailableFilename(Path.Combine(downloadPath, excel));
                ch.ExportToExcel(excel);

                //Create an instance of Workbook class
                Workbook workbook = new Workbook();
                //Load an Excel file
                workbook.LoadFromFile(excel);

                //Get the first worksheet
                Worksheet sheet = workbook.Worksheets[0];

                //Save the worksheet as CSV
                csv = ExportData.NextAvailableFilename(Path.Combine(downloadPath, csv));
                sheet.SaveToFile(csv, ",", Encoding.UTF8);

                File.Delete(excel);

                string argument = "/select, \"" + csv + "\"";
                System.Diagnostics.Process.Start("explorer.exe", argument);
            });
        }
    }
}
