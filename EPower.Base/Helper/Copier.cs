﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace EPower.Base.Helper
{
    public static class Copier
    {
        public static TDestination CopyTo<TSource, TDestination>(TSource source, TDestination destination)
        {
            var sources = source.GetType().GetProperties();
            var destinations = destination.GetType().GetProperties();
            foreach (var destPi in destinations
                .Where(x => sources.Any(s => s.Name == x.Name && s.PropertyType == x.PropertyType && x.GetSetMethod() != null)))
            {
                var sourceApi = sources.FirstOrDefault(x => x.Name == destPi.Name);
                var value = sourceApi.GetValue(source);
                destPi.SetValue(destination, value);
            }
            return destination;
        }

        public static TDestination CopyTo<TSource, TDestination>(TSource source, TDestination destination, params string[] excludeColums)
        {
            var sources = source.GetType().GetProperties();
            var destinations = destination.GetType().GetProperties();
            foreach (var destPi in destinations
                .Where(x => sources.Any(s => s.Name == x.Name && s.PropertyType == x.PropertyType && x.GetSetMethod() != null)))
            {
                if (excludeColums.Contains(destPi.Name))
                {
                    continue;
                }
                var sourceApi = sources.FirstOrDefault(x => x.Name == destPi.Name);
                var value = sourceApi.GetValue(source);
                destPi.SetValue(destination, value);
            }
            return destination;
        }
        public static List<TDestination> ToDataListModel<TDestination>(this DataTable source, params string[] excludeColums)
        {
            var columns = new List<string>();
            foreach (DataColumn column in source.Columns)
            {
                columns.Add(column.ColumnName);
            }
            var lst = new List<TDestination>();
            foreach (DataRow row in source.Rows)
            {
                var objDes = Activator.CreateInstance<TDestination>();
                var destinations = objDes.GetType().GetProperties();
                foreach (var destPi in destinations)
                {

                    if (excludeColums.Contains(destPi.Name))
                    {
                        continue;
                    }
                    if (!columns.Contains(destPi.Name))
                    {
                        continue;
                    }
                    destPi.SetValue(objDes, row.IsNull(destPi.Name) ? null : row[destPi.Name], null);
                }
                lst.Add(objDes);
            }
            return lst;
        }

        public static void CopyTo<T>(this T objSource, T objDes)
        {
            if (objDes == null)
            {
                objDes = Activator.CreateInstance<T>();
            }
            // Retrieve the Type passed into the Method
            Type _impliedType = typeof(T);

            //Get an array of the Type’s properties
            PropertyInfo[] _propInfo = _impliedType.GetProperties();

            //Create the columns in the DataTable
            foreach (PropertyInfo pi in _propInfo)
            {
                MethodInfo setMethod = pi.GetSetMethod(); 
                if (setMethod == null)
                {
                    continue;
                }
                pi.SetValue(objDes, pi.GetValue(objSource, null), null);  
            }
        }

        public static void CopyControl(this Control sourceControl, Control targetControl)
        {
            // make sure these are the same
            if (sourceControl.GetType() != targetControl.GetType())
            {
                throw new Exception("Incorrect control types");
            }

            foreach (PropertyInfo sourceProperty in sourceControl.GetType().GetProperties())
            {
                object newValue = sourceProperty.GetValue(sourceControl, null);

                MethodInfo mi = sourceProperty.GetSetMethod(true);
                if (mi != null)
                {
                    try
                    {
                        sourceProperty.SetValue(targetControl, newValue, null);
                    }
                    catch (Exception)
                    {
                         
                    }
                  
                }
            }
        } 
    }
}
