﻿using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Views.Layout;
using DevExpress.XtraPivotGrid;
using DevExpress.XtraTreeList;
using EPower.Base.Properties;
using HB01.Domain.ListModels;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Base.Helper.DevExpressCustomize
{
    public static class DevExtension
    {
        public static string DefaultFormatDigit { get; set; }
        private static GridColumn[] _skipFormatcolumns;
        public static void SetDefaultGridview(this GridView dgv, bool useRowCellFormat = true, params DevExpress.XtraGrid.Columns.GridColumn[] skipFormatcolumns)
        {
            _skipFormatcolumns = skipFormatcolumns;
            dgv.OptionsSelection.MultiSelect = true;
            dgv.OptionsSelection.MultiSelectMode = GridMultiSelectMode.CheckBoxRowSelect;
            dgv.GridControl.Cursor = Cursors.Hand;
            dgv.OptionsSelection.ShowCheckBoxSelectorInPrintExport = DevExpress.Utils.DefaultBoolean.False;
            dgv.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            //Header 
            //dgv.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            dgv.OptionsView.ShowIndicator = false;

            //Row
            dgv.OptionsBehavior.Editable = false;
            dgv.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            dgv.OptionsView.EnableAppearanceEvenRow = true;
            dgv.Appearance.EvenRow.BackColor = Color.FromArgb(245, 245, 245);
            dgv.Appearance.FocusedRow.BackColor = Color.FromArgb(153, 180, 209);
            dgv.Appearance.FocusedCell.BackColor = Color.FromArgb(153, 180, 209);
            //Print Appearance
            dgv.OptionsPrint.PrintSelectedRowsOnly = true;
            dgv.OptionsPrint.UsePrintStyles = false;

            //dgv.AppearancePrint.HeaderPanel.Font = dgv.Appearance.HeaderPanel.Font;
            //*create format decimal*/

            var formatDigitColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            formatDigitColumn.Name = $"col{nameof(BaseListModel.FormatDigit)}";
            formatDigitColumn.FieldName = nameof(BaseListModel.FormatDigit);
            formatDigitColumn.Visible = false;

            if (dgv.Columns.ColumnByName(formatDigitColumn.Name) == null)
            {
                dgv.Columns.Add(formatDigitColumn);
            }
            //Specific column

            var colRowNo = dgv.Columns.ColumnByFieldName("RowNo");
            if (colRowNo == null)
            {
                colRowNo = dgv.Columns.ColumnByName("colRowNo");
            }

            if (colRowNo != null)
            {
                colRowNo.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            }
            var colId = dgv.Columns.ColumnByFieldName("Id");
            if (colId == null)
            {
                colId = dgv.Columns.ColumnByName("colId");
            }
            if (colId != null)
            {
                colId.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
                colId.OptionsColumn.ShowInCustomizationForm = false;
            }
            var colFormatDigit = dgv.Columns.ColumnByFieldName("FormatDigit");
            if (colFormatDigit == null)
            {
                colFormatDigit = dgv.Columns.ColumnByName("colFormatDigit");

            }

            if (colFormatDigit != null)
            {
                colFormatDigit.OptionsColumn.ShowInCustomizationForm = false;
            }
            dgv.GroupPanelText = " ";//Resources.DragaColumnHeadertoGroupbyThatcolumn;
            dgv.OptionsSelection.EnableAppearanceFocusedRow = true;
            dgv.Appearance.FocusedRow.BackColor = Color.FromArgb(173, 205, 239);
            dgv.Appearance.FocusedRow.BackColor = Color.FromArgb(173, 205, 239);

            //set check box size
            dgv.OptionsSelection.CheckBoxSelectorColumnWidth = 50;

            //set row no box size
            if (colRowNo != null)
            {
                colRowNo.OptionsColumn.FixedWidth = true;
                colRowNo.Width = 50;
            }

            //Footer
            //dgv.OptionsView.ShowFooter = true;
            //dgv.Columns[colRowNo.FieldName].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;
            //dgv.Columns[colRowNo.FieldName].SummaryItem.DisplayFormat = Resources.Total1 + ":{0}"; 

            //printing option 

            dgv.AppearancePrint.HeaderPanel.Font = dgv.Appearance.HeaderPanel.Font;
            dgv.AppearancePrint.Row.Font = dgv.Appearance.Row.Font;
            dgv.AppearancePrint.Lines.Font = dgv.Appearance.Row.Font;
            dgv.AppearancePrint.EvenRow.Font = dgv.Appearance.Row.Font;
            dgv.AppearancePrint.GroupRow.Font = dgv.Appearance.GroupRow.Font;
            // dgv.OptionsPrint.ExpandAllGroups = true;

            //Register event 
            dgv.CustomDrawGroupPanel += Dgv_CustomDrawGroupPanel; ;
            dgv.KeyDown += Dgv_KeyDown;
            if (useRowCellFormat)
            {
                dgv.RowCellStyle += Dgv_RowCellStyle;
            }
        }

        public static void SetDefaultBandGridview(this BandedGridView dgv, bool useRowCellFormat = true)
        {
            dgv.OptionsSelection.MultiSelect = false;
            dgv.OptionsSelection.MultiSelectMode = GridMultiSelectMode.CheckBoxRowSelect;
            dgv.OptionsSelection.ShowCheckBoxSelectorInColumnHeader = DefaultBoolean.True;

            dgv.GridControl.Cursor = System.Windows.Forms.Cursors.Hand;
            dgv.OptionsSelection.ShowCheckBoxSelectorInPrintExport = DevExpress.Utils.DefaultBoolean.False;
            dgv.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            //Header 
            //dgv.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            dgv.OptionsView.ShowIndicator = false;
            //dgv.OptionsView.ShowColumnHeaders = false;

            //Row
            dgv.OptionsBehavior.Editable = false;
            dgv.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            dgv.OptionsView.EnableAppearanceEvenRow = true;
            dgv.Appearance.EvenRow.BackColor = Color.FromArgb(245, 245, 245);
            dgv.Appearance.FocusedRow.BackColor = Color.FromArgb(153, 180, 209);
            dgv.Appearance.FocusedCell.BackColor = Color.FromArgb(153, 180, 209);

            //Print Appearance
            dgv.OptionsPrint.PrintSelectedRowsOnly = true;
            dgv.AppearancePrint.HeaderPanel.Font = dgv.Appearance.HeaderPanel.Font;
            dgv.OptionsPrint.PrintHeader = false;
            dgv.AppearancePrint.BandPanel.Font = dgv.Appearance.HeaderPanel.Font;

            //*create format decimal*/ 
            var formatDigitColumn = new BandedGridColumn();
            formatDigitColumn.Name = $"col{nameof(BaseListModel.FormatDigit)}";
            formatDigitColumn.FieldName = nameof(BaseListModel.FormatDigit);
            formatDigitColumn.Visible = false;

            if (dgv.Columns.ColumnByName(formatDigitColumn.Name) == null)
            {
                dgv.Columns.Add(formatDigitColumn);
            }
            //Specific column
            var colRowNo = dgv.Columns.ColumnByName("colRowNo");

            //    dgv.Columns.ColumnByName();

            // for detail gridview 
            colRowNo = (colRowNo == null) ? dgv.Columns.ColumnByName("colRowNo_1") : colRowNo;
            if (colRowNo != null)
            {
                colRowNo.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            }
            var colId = dgv.Columns.ColumnByName("colId");
            if (colId != null)
            {
                colId.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
                colId.OptionsColumn.ShowInCustomizationForm = false;
            }
            var colFormatDigit = dgv.Columns.ColumnByName("colFormatDigit");
            if (colFormatDigit != null)
            {
                colFormatDigit.OptionsColumn.ShowInCustomizationForm = false;
            }
            dgv.GroupPanelText = " ";// resourse.Resources.DragaColumnHeadertoGroupbyThatcolumn;
            dgv.OptionsSelection.EnableAppearanceFocusedRow = true;
            dgv.Appearance.FocusedRow.BackColor = Color.FromArgb(173, 205, 239);
            dgv.Appearance.FocusedRow.BackColor = Color.FromArgb(173, 205, 239);

            //set check box size
            dgv.OptionsSelection.CheckBoxSelectorColumnWidth = 50;
            dgv.OptionsSelection.ShowCheckBoxSelectorInColumnHeader = DefaultBoolean.True;

            //set row no box size
            //  dgv.OptionsView.ColumnAutoWidth = false;
            colRowNo.OptionsColumn.FixedWidth = true;
            colRowNo.Width = 50;
            colRowNo.MinWidth = 50;

            //Footer
            dgv.OptionsView.ShowFooter = true;
            //dgv.Columns[colRowNo.FieldName].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;
            //dgv.Columns[colRowNo.FieldName].SummaryItem.DisplayFormat = Resources.Total1 + ":{0}"; 

            //printing option 

            dgv.AppearancePrint.HeaderPanel.Font = dgv.Appearance.HeaderPanel.Font;
            dgv.AppearancePrint.Row.Font = dgv.Appearance.Row.Font;
            dgv.AppearancePrint.Lines.Font = dgv.Appearance.Row.Font;
            dgv.AppearancePrint.EvenRow.Font = dgv.Appearance.Row.Font;
            dgv.AppearancePrint.GroupRow.Font = dgv.Appearance.GroupRow.Font;
            // dgv.OptionsPrint.ExpandAllGroups = true;

            //Register event 
            dgv.CustomDrawGroupPanel += Dgv_CustomDrawGroupPanel; ;
            if (useRowCellFormat)
            {
                dgv.RowCellStyle += Dgv_RowCellStyle;
            }
            dgv.KeyDown += Dgv_KeyDown;
        }

        public static void SetDefaultTreeList(this TreeList tgv)
        {
            tgv.OptionsView.ShowIndentAsRowStyle = false;
            tgv.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            tgv.OptionsView.ShowVertLines = false;
            tgv.OptionsView.ShowHorzLines = false;
            tgv.CustomDrawColumnHeader += Tgv_CustomDrawColumnHeader;

            //print
            tgv.OptionsPrint.PrintCheckBoxes = false;
            tgv.OptionsPrint.PrintTree = false;
            tgv.OptionsPrint.PrintTreeButtons = false;
            tgv.AppearancePrint.GroupFooter.Font = tgv.Appearance.GroupFooter.Font;
            tgv.AppearancePrint.HeaderPanel.Font = tgv.Appearance.HeaderPanel.Font;
            tgv.AppearancePrint.Row.Font = tgv.Appearance.Row.Font;
        }

        public static void SetDefaultPivotview(this PivotGridControl pgv)
        {
            pgv.OptionsSelection.MultiSelect = true;
            pgv.Appearance.FocusedCell.BackColor = Color.FromArgb(153, 180, 209);
            pgv.Appearance.ColumnHeaderArea.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;

            //*create format decimal*/        ​
            var formatDigitColumn = new GridColumn();
            formatDigitColumn.Name = $"col{nameof(BaseListModel.FormatDigit)}";
            formatDigitColumn.FieldName = nameof(BaseListModel.FormatDigit);
            formatDigitColumn.Visible = false;


            pgv.KeyDown += Dgv_KeyDown;
            pgv.FieldValueDisplayText += ((object sender, PivotFieldDisplayTextEventArgs e) =>
            {
                if (e.ValueType == PivotGridValueType.GrandTotal)
                {
                    if (e.DisplayText == "Grand Total")
                    {
                        e.DisplayText = e.DisplayText.Replace("Grand Total", Resources.TOTAL);
                    }
                    else
                    {
                        e.DisplayText = e.DisplayText.Replace("Total", Resources.TOTAL);
                    }
                }
                if (e.ValueType == PivotGridValueType.Total)
                {
                    e.DisplayText = e.DisplayText.Replace("Total", Resources.TOTAL);
                }
            });
        }

        private static void Tgv_CustomDrawColumnHeader(object sender, CustomDrawColumnHeaderEventArgs e)
        {
            if (e.Column != null)
            {
                e.ObjectArgs.Bounds = new System.Drawing.Rectangle(e.Bounds.Left, e.Bounds.Top, e.Bounds.Width + 1, e.Bounds.Height);
                e.Painter.DrawObject(e.ObjectArgs);
                e.ObjectArgs.Bounds = new System.Drawing.Rectangle(e.Bounds.Left, e.Bounds.Top, e.Bounds.Width - 1, e.Bounds.Height);
                e.Handled = true;
            }
        }

        private static void Dgv_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                GridView view = sender as GridView;
                if (e.Control && e.KeyCode == Keys.C)
                {
                    if (view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn) != null && view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn).ToString() != String.Empty)
                    {
                        Clipboard.SetText(view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn).ToString());
                    }
                    e.Handled = true;
                }
            }
            catch (Exception)
            {

            }

        }
        //can have only one fit 
        //public static int GetColumnWidth(this GridView dgv,params GridColumn[] ignoreColumns)
        //{
        //    System.Drawing.Rectangle resolution = Screen.PrimaryScreen.Bounds;
        //    int width = resolution.Width;
        //    int leftMenuWidth = 236;
        //    return (width - leftMenuWidth- dgv.Columns.Where(x => ignoreColumns.Contains(x)).Sum(x => x.Width));
        //} 

        public static int GetColumnWidth(this GridView dgv)
        {
            return dgv.Columns.Where(x => x.Visible).Sum(x => x.Width);
        }

        public static void SetBestFitColumns(this GridView dgv)
        {
            dgv.Columns.Where(x => x.Visible).ToList().ForEach(x =>
            {
                if (x.Name == "colActions" || x.Name == "_colAction")
                {
                    x.MaxWidth = 30;
                    x.MinWidth = 30;
                }

                if (x.Name == "_colPrint" || x.Name == "colPrint")
                {
                    x.MaxWidth = 30;
                    x.MinWidth = 30;
                }

                if (x.Name == "colCurrencyHide" || x.Name == "_colCurrencyHide")
                {
                    x.MaxWidth = 30;
                    x.MinWidth = 30;
                }
                x.BestFit();
            });
        }

        public static void SetBestFitColumns(this BandedGridView dgv)
        {
            dgv.BestFitColumns();
            dgv.Columns.Where(x => x.Visible).ToList().ForEach(x =>
            {
                if (x.Name == "colActions" || x.Name == "_colAction")
                {
                    x.MaxWidth = 30;
                    x.MinWidth = 30;
                }

                if (x.Name == "_colPrint" || x.Name == "colPrint")
                {
                    x.MaxWidth = 30;
                    x.MinWidth = 30;
                }

                if (x.Name == "colCurrencyHide" || x.Name == "_colCurrencyHide")
                {
                    x.MaxWidth = 30;
                    x.MinWidth = 30;
                }
                x.BestFit();
            });
        }

        public static void SetBestFitColumns(this TreeList tgv)
        {
            tgv.OptionsView.AutoWidth = false;
            tgv.Columns.Where(x => x.Visible).ToList().ForEach(x =>
            {
                if (x.Name == "colActions" || x.Name == "_colAction")
                {
                    x.MaxWidth = 30;
                    x.MinWidth = 30;
                }

                if (x.Name == "_colPrint" || x.Name == "colPrint")
                {
                    x.MaxWidth = 30;
                    x.MinWidth = 30;
                }

                if (x.Name == "colCurrencyHide" || x.Name == "_colCurrencyHide")
                {
                    x.MaxWidth = 30;
                    x.MinWidth = 30;
                }
                x.BestFit();
            });
        }
        public static bool IsRowCellClick(this GridView sender, EventArgs e)
        {
            DXMouseEventArgs ea = e as DXMouseEventArgs;
            GridHitInfo info = sender.CalcHitInfo(ea.Location);
            return (info.InRow || info.InRowCell);
        }
        private static void Dgv_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            var colFormat = "colFormatDigit";
            var gridView = sender as GridView;
            //ignore if format type = P2
            if (e.Column.ColumnType == typeof(decimal) && e.Column.DisplayFormat.FormatString != "P2")
            {
                if (e.Column.Name == "colExchangeRate")
                {
                    return;
                }
                var formatColumn = gridView.Columns.ColumnByName(colFormat);
                if (formatColumn != null)
                {
                    try
                    {
                        var format = gridView.GetRowCellValue(e.RowHandle, formatColumn).ToString();
                        e.Column.DisplayFormat.FormatType = FormatType.Numeric;
                        e.Column.DisplayFormat.FormatString = format;
                    }
                    catch (Exception)
                    {
                        e.Column.DisplayFormat.FormatType = FormatType.Numeric;
                        e.Column.DisplayFormat.FormatString = DefaultFormatDigit;
                    }
                }
                if ((decimal)e.CellValue < 0)
                {
                    e.Appearance.ForeColor = Color.Red;
                }
            }
            else if (e.Column.ColumnType == typeof(DateTime))
            {
                e.Column.OptionsColumn.AllowSize = false;
            }
        }
        private static void Dgv_CustomDrawGroupPanel(object sender, DevExpress.XtraGrid.Views.Base.CustomDrawEventArgs e)
        {
            e.Cache.FillRectangle(Color.FromArgb(235, 236, 239), e.Bounds);
            var pen = new Pen(Color.FromArgb(217, 218, 224));
            e.Graphics.DrawLine(pen, e.Bounds.X, e.Bounds.Height, e.Bounds.Width, e.Bounds.Height);
            e.Handled = true;
        }

        //show print preview (all layout view) to printer
        public static void ShowGridPreview(GridControl grid, string reportTitle = "", string reportDescription = "", bool printLanscape = false,bool ignoreHeader = false)
        {
            // Check whether the GridControl can be previewed.
            if (!grid.IsPrintingAvailable)
            {
                MsgBox.ShowInformation("The 'DevExpress.XtraPrinting' library is not found");
                return;
            }
            // Open the Preview window. 
            ExportData.Print(grid, reportTitle, reportDescription, printLanscape, null,true,ignoreHeader);
        }
        //export to grid (all layout view) to pdf file
        public static void ToPdf(GridControl grid, string reportTitle = "", string reportDescription = "", string defaultName = "", bool printLanscape = false)
        {
            defaultName = $"{defaultName}.pdf";
            ExportData.ExportTo(defaultName, grid, reportTitle, reportDescription, printLanscape, null);
        }
        //export to grid (all layout view) to excel file
        public static void ToExcel(GridControl grid, string reportTitle = "", string reportDescription = "", string defaultName = "", bool printLanscape = false, bool exportallview = false)
        {
            defaultName = $"{defaultName}.xlsx";
            ExportData.ExportTo(defaultName, grid, reportTitle, reportDescription, printLanscape, null, true, exportallview);
        }
        public static void ExportEFiling(Control curr, GridControl grid, string forMonth, Dictionary<string, object> gridSource, Dictionary<string, string> gridSheetNames, string reportTitle, string reportDescription = "", string defaultName = "", bool printLanscape = false, bool exportallview = false, DateTime? ToDate = null)
        {
            //if (grid.DataMember.Count() <= 0)
            //{
            //    var c = (DataTable)grid.DataSource;
            //    var n = c.Rows.Count;
            //    grid.DataSource = c;
            //}
            defaultName = $"{defaultName}.xls";
            ExportData.ExportEFiling(curr, defaultName, forMonth, grid, gridSource, gridSheetNames, reportDescription, reportTitle, printLanscape, null, true, exportallview, ToDate);
        }

        //show print preview (all layout view) to printer
        public static void ShowGridPreview(GridControl grid, List<ExportHeader> headers, bool useCompanyHeader = true, bool printLanscape = false,bool ignoreHeader = false)
        {
            // Check whether the GridControl can be previewed.
            if (!grid.IsPrintingAvailable)
            {
                MsgBox.ShowInformation("The 'DevExpress.XtraPrinting' library is not found");
                return;
            }
            // Open the Preview window. 
            ExportData.Print(grid, "", "", printLanscape, headers,true, ignoreHeader);
        }
        //export to grid (all layout view) to pdf file
        public static void ToPdf(GridControl grid, List<ExportHeader> headers, bool useCompanyHeader = true, string defaultName = "", bool printLanscape = false)
        {
            defaultName = $"{defaultName}.pdf";
            ExportData.ExportTo(defaultName, grid, "", "", printLanscape, headers);
        }
        //export to grid (all layout view) to excel file
        public static void ToExcel(GridControl grid, List<ExportHeader> headers, bool useCompanyHeader = true, string defaultName = "", bool printLanscape = false)
        {
            defaultName = $"{defaultName}.xls";
            ExportData.ExportTo(defaultName, grid, "", "", printLanscape, headers);
        }

        //export to grid (all layout view) to csv file
        public static void ToCsv(GridControl grid, List<ExportHeader> headers, bool useCompanyHeader = true, string defaultName = "", bool printLanscape = false)
        {
            defaultName = $"{defaultName}.csv";
            ExportData.ExportTo(defaultName, grid, "", "", printLanscape, headers);
        }
        //export to grid (all layout view) to csv file
        public static void ToCsv(GridControl grid, string reportTitle = "", string reportDescription = "", string defaultName = "", bool printLanscape = false)
        {
            defaultName = $"{defaultName}.csv";
            ExportData.ExportTo(defaultName, grid, reportTitle, reportDescription, printLanscape, null);
        }

        //show print preview (all layout view) to printer
        public static void ShowGridPreview(TreeList grid, string reportTitle = "", string reportDescription = "", bool printLanscape = false,bool ignoreHeader= false)
        {
            DevExpress.XtraPrinting.PrintingSystem printTreeList = new DevExpress.XtraPrinting.PrintingSystem();
            DevExpress.XtraPrinting.PrintableComponentLink printLink = new DevExpress.XtraPrinting.PrintableComponentLink();

            printLink.Component = grid;
            printLink.CreateDocument(printTreeList);

            //Set to Landscape  
            printLink.Landscape = true;
            printTreeList.PageSettings.Landscape = true;

            // Check whether the GridControl can be previewed.
            if (!grid.IsPrintingAvailable)
            {
                MsgBox.ShowInformation("The 'DevExpress.XtraPrinting' library is not found");
                return;
            }
            // Open the Preview window. 
            ExportData.Print(grid, reportTitle, reportDescription, printLanscape, null,ignoreHeader);
        }

        //export to grid (all layout view) to pdf file
        public static void ToPdf(TreeList grid, string reportTitle = "", string reportDescription = "", string defaultName = "", bool printLanscape = false)
        {
            defaultName = $"{defaultName}.pdf";
            ExportData.ExportTo(defaultName, grid, reportTitle, reportDescription, printLanscape, null);
        }
        //export to grid (all layout view) to excel file
        public static void ToExcel(TreeList grid, string reportTitle = "", string reportDescription = "", string exPath = "", string defaultName = "", bool printLanscape = false, bool IsEmail = false)
        {
            defaultName = $"{defaultName}.xlsx";
            ExportData.ExportTo(defaultName, grid, reportTitle, reportDescription, exPath, printLanscape, IsEmail);
        }
        //export to grid (all layout view) to csv file
        public static void ToCsv(TreeList grid, string reportTitle = "", string reportDescription = "", string defaultName = "", bool printLanscape = false)
        {
            defaultName = $"{defaultName}.csv";
            ExportData.ExportTo(defaultName, grid, reportTitle, reportDescription, printLanscape, null);
        }

        //show print preview (all layout view) to printer
        public static void ShowGridPreview(PivotGridControl grid, string reportTitle = "", string reportDescription = "", bool printLanscape = false,bool ignoreHeader = false)
        {
            DevExpress.XtraPrinting.PrintingSystem printTreeList = new DevExpress.XtraPrinting.PrintingSystem();
            DevExpress.XtraPrinting.PrintableComponentLink printLink = new DevExpress.XtraPrinting.PrintableComponentLink();

            printLink.Component = grid;
            printLink.CreateDocument(printTreeList);

            //Set to Landscape  
            // printLink.Landscape = true;
            // printTreeList.PageSettings.Landscape = true;
            printLink.PaperKind = System.Drawing.Printing.PaperKind.A4;
            printLink.CustomPaperSize = new System.Drawing.Size(0, 0);
            printLink.VerticalContentSplitting = DevExpress.XtraPrinting.VerticalContentSplitting.Smart;

            // Check whether the GridControl can be previewed.
            if (!grid.IsPrintingAvailable)
            {
                MsgBox.ShowInformation("The 'DevExpress.XtraPrinting' library is not found");
                return;
            }
            // Open the Preview window. 
            ExportData.Print(grid, reportTitle, reportDescription, printLanscape, null,true,ignoreHeader);
        }

        //export to grid (all layout view) to pdf file
        public static void ToPdf(PivotGridControl grid, string reportTitle = "", string reportDescription = "", string defaultName = "", bool printLanscape = false)
        {
            defaultName = $"{defaultName}.pdf";
            ExportData.ExportTo(defaultName, grid, reportTitle, reportDescription, printLanscape, null);
        }
        //export to grid (all layout view) to excel file
        public static void ToExcel(PivotGridControl grid, string reportTitle = "", string reportDescription = "", string defaultName = "", bool printLanscape = false)
        {
            defaultName = $"{defaultName}.xlsx";
            ExportData.ExportTo(defaultName, grid, reportTitle, reportDescription, printLanscape, null);
        }
        //export to grid (all layout view) to csv file
        public static void ToCsv(PivotGridControl grid, string reportTitle = "", string reportDescription = "", string defaultName = "", bool printLanscape = false)
        {
            defaultName = $"{defaultName}.csv";
            ExportData.ExportTo(defaultName, grid, reportTitle, reportDescription, printLanscape, null);
        }

        public static string SaveUserConfigureToXml(this GridView dgv, string defaultName = "")
        {
            string path = "";
            var baseDirectory = Path.Combine(Application.StartupPath, "UserConfigures");
            if (!System.IO.Directory.Exists(baseDirectory))
            {
                /*dgv.GridControl.Parent.Name +"." +  dgv.GridControl.Name +  "." + dgv.Name*/
                System.IO.Directory.CreateDirectory(baseDirectory);
            }
            try
            {
                if (string.IsNullOrEmpty(defaultName))
                {
                    defaultName = $"{dgv.GridControl.Name}.{dgv.Name}";
                }
                dgv.FindFilterText = "";
                var fileName = Path.Combine(baseDirectory, $"{dgv.GridControl.Parent.Name}.{defaultName}.xml");
                path = fileName;
                dgv.SaveLayoutToXml(fileName);
            }
            catch (Exception)
            {

            }
            return path;
        }
        public static void RestoreUserConfigureFromXml(this GridView dgv)
        {
            var baseDirectory = Path.Combine(Application.StartupPath, "UserConfigures");
            if (!System.IO.Directory.Exists(baseDirectory))
            {
                /*dgv.GridControl.Parent.Name +"." +  dgv.GridControl.Name +  "." + dgv.Name*/
                System.IO.Directory.CreateDirectory(baseDirectory);
            }
            try
            {
                var fileName = Path.Combine(baseDirectory, $"{dgv.GridControl.Parent.Name}.{dgv.GridControl.Name}.{dgv.Name}.xml");
                dgv.RestoreLayoutFromXml(fileName);
            }
            catch (Exception)
            {

            }
        }
        public static string RestoreUserConfigureFromXml(this GridView dgv, string templateName)
        {
            var fileName = "";
            var baseDirectory = Path.Combine(System.Windows.Forms.Application.StartupPath, "UserConfigures");
            if (!System.IO.Directory.Exists(baseDirectory))
            {
                /*dgv.GridControl.Parent.Name +"." +  dgv.GridControl.Name +  "." + dgv.Name*/
                System.IO.Directory.CreateDirectory(baseDirectory);
            }
            try
            {
                fileName = Path.Combine(baseDirectory, $"{dgv.GridControl.Parent.Name}.{templateName}.xml");
                dgv.RestoreLayoutFromXml(fileName);
            }
            catch (Exception e)
            {
                fileName = "";
            }
            return fileName;
        }
        public static void SaveUserConfigureToXml(this LayoutView lgv)
        {
            var baseDirectory = Path.Combine(System.Windows.Forms.Application.StartupPath, "UserConfigures");
            if (!System.IO.Directory.Exists(baseDirectory))
            {
                /*dgv.GridControl.Parent.Name +"." +  dgv.GridControl.Name +  "." + dgv.Name*/
                System.IO.Directory.CreateDirectory(baseDirectory);
            }
            try
            {
                lgv.FindFilterText = "";
                var fileName = Path.Combine(baseDirectory, $"{lgv.GridControl.Parent.Name}.{lgv.GridControl.Name}.{lgv.Name}.xml");
                lgv.SaveLayoutToXml(fileName);
            }
            catch (Exception)
            {

            }
        }
        public static void RestoreUserConfigureFromXml(this LayoutView lgv)
        {
            var baseDirectory = Path.Combine(System.Windows.Forms.Application.StartupPath, "UserConfigures");
            if (!System.IO.Directory.Exists(baseDirectory))
            {
                /*dgv.GridControl.Parent.Name +"." +  dgv.GridControl.Name +  "." + dgv.Name*/
                System.IO.Directory.CreateDirectory(baseDirectory);
            }
            try
            {
                var fileName = Path.Combine(baseDirectory, $"{lgv.GridControl.Parent.Name}.{lgv.GridControl.Name}.{lgv.Name}.xml");
                lgv.RestoreLayoutFromXml(fileName);
            }
            catch (Exception)
            {

            }
        }

        public static void SaveUserConfigureToXml(this PivotGridControl pgv)
        {
            var baseDirectory = Path.Combine(System.Windows.Forms.Application.StartupPath, "UserConfigures");
            if (!System.IO.Directory.Exists(baseDirectory))
            {
                /*dgv.GridControl.Parent.Name +"." +  dgv.GridControl.Name +  "." + dgv.Name*/
                System.IO.Directory.CreateDirectory(baseDirectory);
            }
            try
            {
                //pgv.FindFilterText = "";
                var fileName = Path.Combine(baseDirectory, $"{pgv.Parent.Name}.{pgv.Name}.xml");
                pgv.SaveLayoutToXml(fileName);
            }
            catch (Exception)
            {

            }
        }
        public static string SaveUserConfigureToXml(this PivotGridControl pgv, string templateName)
        {
            var fileName = "";
            var baseDirectory = Path.Combine(System.Windows.Forms.Application.StartupPath, "UserConfigures");
            if (!System.IO.Directory.Exists(baseDirectory))
            {
                /*dgv.GridControl.Parent.Name +"." +  dgv.GridControl.Name +  "." + dgv.Name*/
                System.IO.Directory.CreateDirectory(baseDirectory);
            }
            try
            {
                //pgv.FindFilterText = "";
                fileName = Path.Combine(baseDirectory, $"{templateName}");
                pgv.SaveLayoutToXml(fileName);
            }
            catch (Exception)
            {
                fileName = "";
            }
            return fileName;
        }
        public static void RestoreUserConfigureFromXml(this PivotGridControl pgv)
        {
            var baseDirectory = Path.Combine(System.Windows.Forms.Application.StartupPath, "UserConfigures");
            if (!System.IO.Directory.Exists(baseDirectory))
            {
                /*dgv.GridControl.Parent.Name +"." +  dgv.GridControl.Name +  "." + dgv.Name*/
                System.IO.Directory.CreateDirectory(baseDirectory);
            }
            try
            {
                var fileName = Path.Combine(baseDirectory, $"{pgv.Parent.Name}.{pgv.Name}.xml");
                pgv.RestoreLayoutFromXml(fileName);
            }
            catch (Exception e)
            {
                
            }
        }
        public static string RestoreUserConfigureFromXml(this PivotGridControl pgv, string templateName)
        {
            var fileName = "";
            var baseDirectory = Path.Combine(System.Windows.Forms.Application.StartupPath, "UserConfigures");
            if (!System.IO.Directory.Exists(baseDirectory))
            {
                /*dgv.GridControl.Parent.Name +"." +  dgv.GridControl.Name +  "." + dgv.Name*/
                System.IO.Directory.CreateDirectory(baseDirectory);
            }
            try
            {
                fileName = Path.Combine(baseDirectory, $"{pgv.Name}.{templateName}.xml");
                pgv.RestoreLayoutFromXml(fileName);
            }
            catch (Exception​​ e)
            {
                fileName = "";
            }
            return fileName;
        }

        public static List<T> ToListModel<T>(this GridView dgv) where T : class
        {
            var lines = new List<T>();
            for (int i = 0; i < dgv.RowCount; i++)
            {
                var entity = dgv.GetRow(i) as T;
                if (entity == null)
                {
                    continue;
                }
                lines.Add(entity);
            }
            return lines;
        }
        public static List<T> ToListModel<T>(this GridView dgv, bool selected = false) where T : BasedModel
        {
            ArrayList rows = new ArrayList();
            var lines = new List<T>();
            // Add the selected rows to the list.
            Int32[] selectedRowHandles = dgv.GetSelectedRows();
            for (int i = 0; i < dgv.RowCount; i++)
            {
                if (!selectedRowHandles.Contains(i))
                {
                    continue;
                }
                var entity = dgv.GetRow(i) as T;
                if (entity == null)
                {
                    continue;
                }
                lines.Add(entity);
            }
            return lines;
        }
        public static List<T> GetCurrentRow<T>(this GridView dgv) where T : BasedModel
        {
            var lines = new List<T>();
            for (int i = 0; i < dgv.RowCount; i++)
            {
                var entity = dgv.GetRow(i) as T;
                lines.Add(entity);
            }
            return lines;
        }

        //public static void Exists(this TextEdit textbox, string value)
        //{
        //    textbox.DevShowValidation(string.Format(Properties.Resources.MSG_FIELD_EXISTS, value));
        //}
        public static void SetDatasource<T>(this LookUpEdit cbo, List<T> source, string displayMember = "") where T : BasedModel
        {
            cbo.Properties.DataSource = source;
            cbo.Properties.ValueMember = nameof(BasedModel.Id);
            cbo.Properties.DisplayMember = displayMember;
        }
        //public static void SetDatasourceList<TList>(this LookUpEdit cbo, List<TList> source, string displayMember = "") where TList : BaseListModel
        //{
        //    cbo.Properties.DataSource = source;
        //    cbo.Properties.ValueMember = nameof(BaseListModel.Id);
        //    cbo.Properties.DisplayMember = displayMember;
        //} 

        public static void SetDatasource<T>(this RepositoryItemLookUpEdit cbo, List<T> source, string displayMember = "") where T : BasedModel
        {
            cbo.DataSource = source;
            cbo.ValueMember = nameof(BasedModel.Id);
            cbo.DisplayMember = displayMember;
        }
        public static void SetDatasourceList<TList>(this RepositoryItemLookUpEdit cbo, List<TList> source, string displayMember = "") where TList : BaseListModel
        {
            cbo.DataSource = source;
            cbo.ValueMember = nameof(BaseListModel.Id);
            cbo.DisplayMember = displayMember;
        }

        public static void SetDataSourceToComboBox<T>(this LookUpEdit cbo, List<T> source, string valueMember, string displayMember = "")
        {
            cbo.Properties.DataSource = source;
            cbo.Properties.ValueMember = valueMember;
            cbo.Properties.DisplayMember = displayMember;
            foreach (LookUpColumnInfo item in cbo.Properties.Columns)
            {
                item.Caption = ResourceHelper.Translate(item.Caption);
            }
        }
        public static void SetDatasourceList<TList>(this LookUpEdit cbo, List<TList> source, string displayMember = "") where TList : BaseListModel
        {
            cbo.Properties.DataSource = source;
            cbo.Properties.ValueMember = nameof(BaseListModel.Id);
            cbo.Properties.DisplayMember = displayMember;
        }
        public partial class LookEditModel : UserControl
        {
        }
        public static void SetEnumSource(this LookUpEdit cbo, Type enumType, bool all = false, bool allTextWithEnumType = true, params Enum[] ignores)
        {
            var enumLists = Enum.GetValues(enumType).Cast<Enum>().Where(x => !ignores.Contains(x)).Select(x => new LookUpEditModel
            {
                Name = ResourceHelper.Translate(x.ToString()),
                Id = Convert.ToInt32(x)
            });

            cbo.Properties.DataSource = enumLists.ToList();
            cbo.Properties.DisplayMember = nameof(LookUpEditModel.Name);
            cbo.Properties.ValueMember = nameof(LookUpEditModel.Id);

        }
        public static void AllowEdit(this GridView dgv, bool allowEdit = true, params GridColumn[] ignoreColumn)
        {
            foreach (GridColumn column in dgv.Columns)
            {
                if (ignoreColumn.Contains(column))
                {
                    continue;
                }
                column.OptionsColumn.AllowEdit = allowEdit;
            }
        }
        public static void FormatEditAndDisplay(this DevExpress.XtraEditors.Repository.RepositoryItemPopupBase item, FormatType type, string format)
        {
            item.DisplayFormat.FormatType = type;
            item.DisplayFormat.FormatString = format;

            item.EditFormat.FormatType = type;
            item.EditFormat.FormatString = format;
        }

        public static void FormatEditAndDisplay(this GridColumn item, FormatType type, string format)
        {
            item.DisplayFormat.FormatType = type;
            item.DisplayFormat.FormatString = format;
        }
        public static void FormatEditAndDisplay(this TextEdit item, FormatType type, string format)
        {
            item.Properties.EditFormat.FormatType = type;
            item.Properties.EditFormat.FormatString = format;

            item.Properties.DisplayFormat.FormatType = type;
            item.Properties.DisplayFormat.FormatString = format;

            item.Properties.Mask.EditMask = format;
            item.Properties.Mask.UseMaskAsDisplayFormat = true;
        }

        public static void FormatEditAndDisplay(this CalcEdit item, FormatType type, string format)
        {
            item.Properties.DisplayFormat.FormatType = type;
            item.Properties.DisplayFormat.FormatString = format;

            item.Properties.EditFormat.FormatType = type;
            item.Properties.EditFormat.FormatString = format;

            item.Properties.Mask.EditMask = format;
            item.Properties.Mask.UseMaskAsDisplayFormat = true;
        }
    }
    //use for type of enum
    public class LookUpEditModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    //public class BaseListModel
    //{
    //    public int Id { get; set; }

    //    //base on currency format in each row;
    //    public string FormatDigit { get; set; }
    //    public BaseListModel()
    //    {
    //    }
    //    public BaseListModel(BasedModel source)
    //    {
    //        Copier.CopyTo(source, this);
    //    }
    //}
    public class BasedModel
    {
        /// <summary>
        /// Primary key column.
        /// </summary>

        public int Id { get; set; }
        /// <summary>
        /// Last updated date.
        /// </summary>
        public DateTime RowDate { get; set; } = DateTime.Now;
        /// <summary>
        /// Active will be in used.
        /// </summary>
        public bool Active { get; set; } = true;
        public override bool Equals(object obj)
        {
            var t = obj as BasedModel;
            if (t == null)
            {
                return false;
            }
            else
            {
                return (Id == t.Id);
            }
        }
        public override int GetHashCode()
        {
            return Id;
        }
    }
}

