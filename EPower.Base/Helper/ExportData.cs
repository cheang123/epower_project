﻿using ClosedXML.Excel;
using DevExpress.Spreadsheet;
using DevExpress.Utils;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPivotGrid;
using DevExpress.XtraPrinting;
using DevExpress.XtraTreeList;
using EPower.Base.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Logic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EPower.Base.Helper.DevExpressCustomize
{
    public class ExportData
    {
        private static string _numberPattern = " ({0})";
        private static List<ExportHeader> _headers;
        private static bool _useCompanyeader;
        private static Font _exportFont;
        private static bool IsPivotGrid = false;
        public static string NextAvailableFilename(string path)
        {
            // Short-cut if already available
            if (!File.Exists(path))
            {
                return path;
            }
            // If path has extension then insert the number pattern just before the extension and return next filename
            if (Path.HasExtension(path))
            {
                return GetNextFilename(path.Insert(path.LastIndexOf(Path.GetExtension(path)), _numberPattern));
            }
            // Otherwise just append the pattern to the path and return next filename
            return GetNextFilename(path + _numberPattern);
        }
        private static string GetNextFilename(string pattern)
        {
            string tmp = string.Format(pattern, 1);
            if (tmp == pattern)
                throw new ArgumentException("The pattern must include an index place-holder", "pattern");

            if (!File.Exists(tmp))
                return tmp; // short-circuit if no matches

            int min = 1, max = 2; // min is inclusive, max is exclusive/untested

            while (File.Exists(string.Format(pattern, max)))
            {
                min = max;
                max *= 2;
            }

            while (max != min + 1)
            {
                int pivot = (max + min) / 2;
                if (File.Exists(string.Format(pattern, pivot)))
                    min = pivot;
                else
                    max = pivot;
            }

            return string.Format(pattern, max);
        }
        public static void OpenFolderAndSelectFile(string filePath)
        {
            if (!System.IO.File.Exists(filePath))
            {
                return;
            }
            //Clean up file path so it can be navigated OK
            filePath = System.IO.Path.GetFullPath(filePath);
            System.Diagnostics.Process.Start("explorer.exe", string.Format("/select,\"{0}\"", filePath));
        }
        public static void Print(GridControl gridControl, string reportTitle, string reportDescription, bool printLanscape = false, List<ExportHeader> headers = null, bool useCompanyHeader = true, bool ignoreHeader = false)
        {
            IsPivotGrid = false;
            var gridView = gridControl.MainView as GridView;

            if (gridView.OptionsPrint.PrintSelectedRowsOnly)
            {
                if (gridView.GetSelectedRows().Count() == 0)
                {
                    MsgBox.ShowInformation(Resources.REQUIRE_CHECK_EXPORT);
                    return;
                }

            } 
            PrintingSystem ps = new PrintingSystem();
            PrintableComponentLink link = new PrintableComponentLink(ps);
            link.PaperKind = System.Drawing.Printing.PaperKind.A4;
            link.CustomPaperSize = new System.Drawing.Size(0, 0);
            link.VerticalContentSplitting = DevExpress.XtraPrinting.VerticalContentSplitting.Smart;

            link.Component = gridControl;
            headers = headers ?? new List<ExportHeader>();
            //headers = headers.Any() ?headers : GetDefaultHeader("បញ្ជីវិក្ក័យបត្រ័", "ចាប់ពី 30-Sep-2020 ដល់ 30-Sep-2020", gridControl.Font) ;
            headers = headers.Any() ? headers : GetDefaultHeader(reportTitle, reportDescription, gridControl.Font);
            _headers = headers;
            _exportFont = gridControl.Font;
            _useCompanyeader = useCompanyHeader;
            link.Landscape = printLanscape;
            link.Margins = new System.Drawing.Printing.Margins(1, 1, 2, 40);
            if (!ignoreHeader)
            {
                link.CreateReportHeaderArea += Link_CreateReportHeaderArea;
            }
            
            link.CreateMarginalFooterArea += Link_CreateMarginalFooterArea;
            // Generate a report.
            link.CreateDocument();
            link.ShowPreview();
        }
        //public static void LayoutPrint(GridControl gridControl, string reportTitle, string reportDescription, bool printLanscape = false, List<ExportHeader> headers = null, bool useCompanyHeader = true)
        //{
        //    var gridView = gridControl.MainView as LayoutView;
        //    if (gridView.GetSelectedRows().Count() == 0)
        //    {
        //        HMsgBox.Show("Please select");
        //        return;
        //    }

        //    DevExpress.XtraPrinting.PrintingSystem ps = new DevExpress.XtraPrinting.PrintingSystem();
        //    DevExpress.XtraPrinting.PrintableComponentLink link = new PrintableComponentLink(ps);
        //    link.Component = gridControl;
        //    headers = headers ?? new List<ExportHeader>();
        //    //headers = headers.Any() ?headers : GetDefaultHeader("បញ្ជីវិក្ក័យបត្រ័", "ចាប់ពី 30-Sep-2020 ដល់ 30-Sep-2020", gridControl.Font) ;
        //    headers = headers.Any() ? headers : GetDefaultHeader(reportTitle, reportDescription, gridControl.Font);
        //    _headers = headers;
        //    _exportFont = gridControl.Font;
        //    _useCompanyeader = useCompanyHeader;
        //    link.Landscape = printLanscape;
        //    link.Margins = new System.Drawing.Printing.Margins(1, 1, 2, 40);
        //    link.CreateReportHeaderArea += Link_CreateReportHeaderArea;
        //    // Generate a report.
        //    link.CreateDocument();
        //    link.ShowPreview();
        //}
        public static void Print(TreeList treeList, string reportTitle, string reportDescription, bool printLanscape = false, List<ExportHeader> headers = null, bool useCompanyHeader = true,bool ignoreHeader = false)
        {
            DevExpress.XtraPrinting.PrintingSystem ps = new DevExpress.XtraPrinting.PrintingSystem();
            DevExpress.XtraPrinting.PrintableComponentLink link = new PrintableComponentLink(ps);
            link.Component = treeList;
            headers = headers ?? new List<ExportHeader>();
            //headers = headers.Any() ?headers : GetDefaultHeader("បញ្ជីវិក្ក័យបត្រ័", "ចាប់ពី 30-Sep-2020 ដល់ 30-Sep-2020", gridControl.Font) ;
            headers = headers.Any() ? headers : GetDefaultHeader(reportTitle, reportDescription, treeList.Font);
            _headers = headers;
            //            ps.Landscape = view.OptionsPrint
            _exportFont = treeList.Font;
            _useCompanyeader = useCompanyHeader;
            //      link.CreateMarginalHeaderArea += Link_CreateMarginalHeaderArea;  
            link.Landscape = printLanscape;
            link.Margins = new System.Drawing.Printing.Margins(1, 1, 2, 20);
            if (!ignoreHeader)
            {
                link.CreateReportHeaderArea += Link_CreateReportHeaderArea;
            }
            link.CreateMarginalFooterArea += Link_CreateMarginalFooterArea;
            // Generate a report.
            link.CreateDocument();
            link.ShowPreview();
        }
        public static void ExportTo(string fileName, GridControl gridControl, string reportTitle, string reportDescription, bool printLanscape = false, List<ExportHeader> headers = null, bool useCompanyHeader = true, bool exportAllview = false)
        {
            var gridView = gridControl.MainView as GridView;
            if (gridView.OptionsPrint.PrintSelectedRowsOnly)
            {
                if (gridView.GetSelectedRows().Count() == 0)
                {
                    MsgBox.ShowInformation(Resources.REQUIRE_CHECK_EXPORT);
                    return;
                }

            }
            DevExpress.XtraPrinting.PrintingSystem ps = new DevExpress.XtraPrinting.PrintingSystem();
            DevExpress.XtraPrinting.PrintableComponentLink link = new PrintableComponentLink(ps);
            var downloadPath = NextAvailableFilename(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"\Downloads\");
            if (!Directory.Exists(downloadPath))
            {
                Directory.CreateDirectory(downloadPath);
            }
            downloadPath = downloadPath + fileName;
            //"Excel (2003)(.xls)|*.xls|Excel (2010) (.xlsx)|*.xlsx |RichText File (.rtf)|*.rtf |Pdf File (.pdf)|*.pdf |Html File (.html)|*.html"; 
            string fileExtenstion = new FileInfo(downloadPath).Extension;
            link.Landscape = printLanscape;
            // Specify the control to be printed.                  
            link.Component = gridControl;
            headers = headers ?? new List<ExportHeader>();
            //headers = headers.Any() ?headers : GetDefaultHeader("បញ្ជីវិក្ក័យបត្រ័", "ចាប់ពី 30-Sep-2020 ដល់ 30-Sep-2020", gridControl.Font) ;
            headers = headers.Any() ? headers : GetDefaultHeader(reportTitle, reportDescription, gridControl.Font);
            _headers = headers;
            _exportFont = gridControl.Font;
            _useCompanyeader = useCompanyHeader;

            //      link.CreateMarginalHeaderArea += Link_CreateMarginalHeaderArea;  

            link.Margins = new System.Drawing.Printing.Margins(1, 1, 2, 40);
            link.CreateReportHeaderArea += Link_CreateReportHeaderArea;
            link.CreateMarginalFooterArea += Link_CreateMarginalFooterArea;

            //    link.CreatePageForEachLink();
            // Generate a report.
            link.CreateDocument();
            Runner.RunNewThread(() =>
            {
                //write your code export here...
                switch (fileExtenstion)
                {
                    case ".xls":
                        link.PrintingSystem.ExportToXls(downloadPath);
                        break;
                    case ".xlsx":
                        link.PrintingSystem.ExportToXlsx(downloadPath);
                        break;
                    case ".rtf":
                        link.PrintingSystem.ExportToRtf(downloadPath);
                        break;
                    case ".pdf":
                        link.PrintingSystem.ExportToPdf(downloadPath);
                        break;
                    case ".html":
                        link.PrintingSystem.ExportToHtml(downloadPath);
                        break;
                    case ".mht":
                        link.PrintingSystem.ExportToMht(downloadPath);
                        break;
                    case ".csv":
                        link.PrintingSystem.ExportOptions.Csv.Encoding = Encoding.Unicode;
                        link.PrintingSystem.ExportOptions.Csv.Separator = CultureInfo.CurrentCulture.TextInfo.ListSeparator.ToString();
                        link.PrintingSystem.ExportToCsv(downloadPath);
                        break;
                    case ".docx":
                        link.PrintingSystem.ExportToDocx(downloadPath);
                        break;
                    default:
                        break;
                }
                OpenFolderAndSelectFile(downloadPath);
            });        
        }

        //public static void LayoutExportTo(string fileName, GridControl gridControl, string reportTitle, string reportDescription, bool printLanscape = false, List<ExportHeader> headers = null, bool useCompanyHeader = true)
        //{
        //    var gridView = gridControl.MainView as LayoutView;
        //    if (gridView.GetSelectedRows().Count() == 0)
        //    {
        //        HMsgBox.Show(Resources.RequirecheckExport);
        //        return;
        //    }
        //    DevExpress.XtraPrinting.PrintingSystem ps = new DevExpress.XtraPrinting.PrintingSystem();
        //    DevExpress.XtraPrinting.PrintableComponentLink link = new PrintableComponentLink(ps);

        //    string downloadPath = NextAvailableFilename(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"\Downloads\" + fileName);
        //    //"Excel (2003)(.xls)|*.xls|Excel (2010) (.xlsx)|*.xlsx |RichText File (.rtf)|*.rtf |Pdf File (.pdf)|*.pdf |Html File (.html)|*.html"; 
        //    string fileExtenstion = new FileInfo(downloadPath).Extension;
        //    // Specify the control to be printed.                  
        //    link.Component = gridControl;
        //    headers = headers ?? new List<ExportHeader>();
        //    //headers = headers.Any() ?headers : GetDefaultHeader("បញ្ជីវិក្ក័យបត្រ័", "ចាប់ពី 30-Sep-2020 ដល់ 30-Sep-2020", gridControl.Font) ;
        //    headers = headers.Any() ? headers : GetDefaultHeader(reportTitle, reportDescription, gridControl.Font);
        //    _headers = headers;
        //    _exportFont = gridControl.Font;
        //    _useCompanyeader = useCompanyHeader;

        //    //      link.CreateMarginalHeaderArea += Link_CreateMarginalHeaderArea;  
        //    //    gridView.GroupedColumns
        //    link.Landscape = printLanscape;
        //    link.Margins = new System.Drawing.Printing.Margins(1, 1, 2, 40);
        //    link.CreateReportHeaderArea += Link_CreateReportHeaderArea;
        //    link.CreateMarginalFooterArea += Link_CreateMarginalFooterArea;

        //    // Generate a report.
        //    link.CreateDocument();
        //    //write your code export here...
        //    switch (fileExtenstion)
        //    {
        //        case ".xls":
        //            link.PrintingSystem.ExportToXls(downloadPath);
        //            break;
        //        case ".xlsx":
        //            link.PrintingSystem.ExportToXlsx(downloadPath);
        //            break;
        //        case ".rtf":
        //            link.PrintingSystem.ExportToRtf(downloadPath);
        //            break;
        //        case ".pdf":
        //            //link.PrintingSystem.ExportOptions.Pdf.DocumentOptions.Author = Current.User?.Name??";
        //            link.PrintingSystem.ExportToPdf(downloadPath);
        //            break;
        //        case ".html":
        //            link.PrintingSystem.ExportToHtml(downloadPath);
        //            break;
        //        case ".mht":
        //            link.PrintingSystem.ExportToMht(downloadPath);
        //            break;
        //        case ".csv":
        //            link.PrintingSystem.ExportOptions.Csv.Encoding = Encoding.Unicode;
        //            link.PrintingSystem.ExportOptions.Csv.Separator = CultureInfo.CurrentCulture.TextInfo.ListSeparator.ToString();
        //            link.PrintingSystem.ExportToCsv(downloadPath);
        //            break;
        //        case ".docx":
        //            link.PrintingSystem.ExportToDocx(downloadPath);
        //            break;
        //        default:
        //            break;
        //    }
        //    OpenFolderAndSelectFile(downloadPath);
        //}
        public static void ExportTo(string fileName, TreeList treeList, string reportTitle, string reportDescription, bool printLanscape = false, List<ExportHeader> headers = null, bool useCompanyHeader = true)
        {
            DevExpress.XtraPrinting.PrintingSystem ps = new DevExpress.XtraPrinting.PrintingSystem();
            DevExpress.XtraPrinting.PrintableComponentLink link = new PrintableComponentLink(ps);
            var downloadPath = NextAvailableFilename(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"\Downloads\");
            if (!Directory.Exists(downloadPath))
            {
                Directory.CreateDirectory(downloadPath);
            }
            downloadPath = downloadPath + fileName;
            //"Excel (2003)(.xls)|*.xls|Excel (2010) (.xlsx)|*.xlsx |RichText File (.rtf)|*.rtf |Pdf File (.pdf)|*.pdf |Html File (.html)|*.html"; 
            string fileExtenstion = new FileInfo(downloadPath).Extension;
            // Specify the control to be printed.                  
            link.Component = treeList;
            headers = headers ?? new List<ExportHeader>();
            //headers = headers.Any() ?headers : GetDefaultHeader("បញ្ជីវិក្ក័យបត្រ័", "ចាប់ពី 30-Sep-2020 ដល់ 30-Sep-2020", gridControl.Font) ;
            headers = headers.Any() ? headers : GetDefaultHeader(reportTitle, reportDescription, treeList.Font);
            _headers = headers;
            _exportFont = treeList.Font;
            _useCompanyeader = useCompanyHeader;


            //      link.CreateMarginalHeaderArea += Link_CreateMarginalHeaderArea;  
            link.Landscape = printLanscape;
            link.Margins = new System.Drawing.Printing.Margins(1, 1, 2, 40);
            link.CreateReportHeaderArea += Link_CreateReportHeaderArea;
            link.CreateMarginalFooterArea += Link_CreateMarginalFooterArea;

            // Generate a report.
            link.CreateDocument();
            //write your code export here...
            switch (fileExtenstion)
            {
                case ".xls":
                    link.PrintingSystem.ExportToXls(downloadPath);
                    break;
                case ".xlsx":
                    link.PrintingSystem.ExportToXlsx(downloadPath);
                    break;
                case ".rtf":
                    link.PrintingSystem.ExportToRtf(downloadPath);
                    break;
                case ".pdf":
                    //link.PrintingSystem.ExportOptions.Pdf.DocumentOptions.Author = Current.User?.Name??";
                    link.PrintingSystem.ExportToPdf(downloadPath);
                    break;
                case ".html":
                    link.PrintingSystem.ExportToHtml(downloadPath);
                    break;
                case ".mht":
                    link.PrintingSystem.ExportToMht(downloadPath);
                    break;
                case ".csv":
                    link.PrintingSystem.ExportOptions.Csv.Encoding = Encoding.Unicode;
                    link.PrintingSystem.ExportOptions.Csv.Separator = CultureInfo.CurrentCulture.TextInfo.ListSeparator.ToString();
                    link.PrintingSystem.ExportToCsv(downloadPath);
                    break;
                case ".docx":
                    link.PrintingSystem.ExportToDocx(downloadPath);
                    break;
                default:
                    break;
            }
            OpenFolderAndSelectFile(downloadPath);
        }
        public static void ExportTo(string fileName, PivotGridControl gridControl, string reportTitle, string reportDescription, bool printLanscape = false, List<ExportHeader> headers = null, bool useCompanyHeader = true)
        {
            DevExpress.XtraPrinting.PrintingSystem ps = new DevExpress.XtraPrinting.PrintingSystem();
            DevExpress.XtraPrinting.PrintableComponentLink link = new PrintableComponentLink(ps);
            link.PaperKind = System.Drawing.Printing.PaperKind.A4;
            link.CustomPaperSize = new System.Drawing.Size(0, 0);
            link.VerticalContentSplitting = DevExpress.XtraPrinting.VerticalContentSplitting.Smart;
            var downloadPath = NextAvailableFilename(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"\Downloads\");
            if (!Directory.Exists(downloadPath))
            {
                Directory.CreateDirectory(downloadPath);
            }
            downloadPath = downloadPath + fileName;
            //"Excel (2003)(.xls)|*.xls|Excel (2010) (.xlsx)|*.xlsx |RichText File (.rtf)|*.rtf |Pdf File (.pdf)|*.pdf |Html File (.html)|*.html"; 
            string fileExtenstion = new FileInfo(downloadPath).Extension;
            Runner.RunNewThread(() =>
            {
                // Specify the control to be printed.                  
                link.Component = gridControl;
                headers = headers ?? new List<ExportHeader>();
                //headers = headers.Any() ?headers : GetDefaultHeader("បញ្ជីវិក្ក័យបត្រ័", "ចាប់ពី 30-Sep-2020 ដល់ 30-Sep-2020", gridControl.Font) ;
                headers = headers.Any() ? headers : GetDefaultHeader(reportTitle, reportDescription, gridControl.Font);
                _headers = headers;
                _exportFont = gridControl.Font;
                _useCompanyeader = useCompanyHeader;
                gridControl.OptionsPrint.PrintRowFieldValues = true;
                gridControl.OptionsPrint.UsePrintAppearance = true;
                


                //      link.CreateMarginalHeaderArea += Link_CreateMarginalHeaderArea;  
                link.Landscape = printLanscape;
                link.Margins = new System.Drawing.Printing.Margins(1, 1, 2, 40);
                link.CreateReportHeaderArea += Link_CreateReportHeaderArea;
                link.CreateMarginalFooterArea += Link_CreateMarginalFooterArea;
                var option = new PivotXlsxExportOptions()
                {
                    ExportType = DevExpress.Export.ExportType.DataAware,
                    RawDataMode = true,
                    AllowGrouping = DefaultBoolean.False,
                    AllowCombinedBandAndColumnHeaderCellMerge = DefaultBoolean.True,
                    AllowFixedColumns = DefaultBoolean.False,
                    AllowConditionalFormatting = DefaultBoolean.True,
                    TextExportMode = TextExportMode.Value,
                };
                option.CustomizeCell += Option_CustomizeCell; ;
                // Generate a report.
                link.CreateDocument();
                //write your code export here...
                switch(fileExtenstion)
                {
                    case ".xls":
                        link.PrintingSystem.ExportToXls(downloadPath);
                        break;
                    case ".xlsx":
                        link.PrintingSystem.ExportToXlsx(downloadPath);
                        break;
                    case ".rtf":
                        link.PrintingSystem.ExportToRtf(downloadPath);
                        break;
                    case ".pdf":
                        //link.PrintingSystem.ExportOptions.Pdf.DocumentOptions.Author = Current.User?.Name??";
                        link.PrintingSystem.ExportToPdf(downloadPath);
                        break;
                    case ".html":
                        link.PrintingSystem.ExportToHtml(downloadPath);
                        break;
                    case ".mht":
                        link.PrintingSystem.ExportToMht(downloadPath);
                        break;
                    case ".csv":
                        link.PrintingSystem.ExportOptions.Csv.Encoding = Encoding.Unicode;
                        link.PrintingSystem.ExportOptions.Csv.Separator = CultureInfo.CurrentCulture.TextInfo.ListSeparator.ToString();
                        link.PrintingSystem.ExportToCsv(downloadPath);
                        break;
                    case ".docx":
                        link.PrintingSystem.ExportToDocx(downloadPath);
                        break;
                    default:
                        break;
                }
            });
            OpenFolderAndSelectFile(downloadPath);
        }

        private static void Option_CustomizeCell(CustomizePivotCellEventArgs e)
        {
            if(e.ExportArea == PivotExportArea.Data)
            {
                e.Formatting.BackColor = Color.Red;
                e.Formatting.Font.Italic = true;
            }
            e.Handled = true;
        } 

        public static void Print(PivotGridControl gridControl, string reportTitle, string reportDescription, bool printLanscape = false, List<ExportHeader> headers = null, bool useCompanyHeader = true, bool ignoreHeader = false)
        {
            IsPivotGrid = true;
            DevExpress.XtraPrinting.PrintingSystem ps = new DevExpress.XtraPrinting.PrintingSystem();
            DevExpress.XtraPrinting.PrintableComponentLink link = new PrintableComponentLink(ps);
            link.PaperKind = System.Drawing.Printing.PaperKind.A4;
            //ps.Document.AutoFitToPagesWidth = 1;

            link.Component = gridControl;
            headers = headers ?? new List<ExportHeader>();
            //headers = headers.Any() ?headers : GetDefaultHeader("បញ្ជីវិក្ក័យបត្រ័", "ចាប់ពី 30-Sep-2020 ដល់ 30-Sep-2020", gridControl.Font) ;
            headers = headers.Any() ? headers : GetDefaultHeader(reportTitle, reportDescription, gridControl.Font);
            _headers = headers;
            _exportFont = gridControl.Font;
            _useCompanyeader = useCompanyHeader;
            link.Landscape = printLanscape;
            
            link.Margins = new System.Drawing.Printing.Margins(1, 1, 2, 40);
            //if (!ignoreHeader)
            //{
            //    link.CreateReportHeaderArea += Link_CreateReportHeaderArea;
            //}
            link.CreateReportHeaderArea += Link_CreateReportHeaderArea;
            link.CreateMarginalFooterArea += Link_CreateMarginalFooterArea;
            // Generate a report.
            link.CreateDocument();
            link.ShowPreview();
        }

        private static void Link_CreateMarginalFooterArea(object sender, CreateAreaEventArgs e)
        {
            var footerInfo = $"{Resources.PRINT_SETUP}៖ {Login.CurrentLogin.LOGIN_NAME ?? ""}  {Resources.DATE}៖ {DateTime.Now.ToLongDate()}  {Resources.PAGE}៖ ";
            e.Graph.Font = new Font(_exportFont.FontFamily, 8);
            e.Graph.BackColor = Color.Transparent;
            RectangleF r = new RectangleF(0, 0, e.Graph.ClientPageSize.Width, e.Graph.Font.Height);
            //  PageInfoBrick brick = e.Graph.DrawPageInfo(PageInfo.None, text, Color.Black, r, BorderSide.None);
            var brick = e.Graph.DrawPageInfo(PageInfo.NumberOfTotal, footerInfo + "{0}/{1}", Color.Black, r, BorderSide.None);
            brick.VertAlignment = VertAlignment.Center;
            brick.HorzAlignment = HorzAlignment.Center;
        }

        private static void Link_CreateReportHeaderArea(object sender, CreateAreaEventArgs e)
        {
            var link = sender as PrintableComponentLink;
            var companyHeaderHeight = 0;

            if (_useCompanyeader)
            {
                var company = DBDataContext.Db.TBL_COMPANies.FirstOrDefault();
                var racCompanyName = new RectangleF(0, 0, e.Graph.ClientPageSize.Width, 30);
                //if (IsPivotGrid)
                //{
                //    racCompanyName = new RectangleF(100, 0, e.Graph.ClientPageSize.Width, 30);
                //}
                e.Graph.Font = new Font(_exportFont.FontFamily, _exportFont.Size, FontStyle.Bold);
                var companyName = e.Graph.DrawString(company.COMPANY_NAME, Color.Black, racCompanyName, BorderSide.None);
                companyName.VertAlignment = VertAlignment.Center;
                companyName.HorzAlignment = HorzAlignment.Center;

                var racAddress = new RectangleF(0, 30, e.Graph.ClientPageSize.Width, 40);
                //if (IsPivotGrid)
                //{
                //    racAddress = new RectangleF(100, 30, e.Graph.ClientPageSize.Width, 40);
                //}
                e.Graph.Font = _exportFont;
                if (company.ADDRESS.Contains("\n"))
                {
                    racAddress.Height += 11;
                }
                var address = e.Graph.DrawString(company.ADDRESS, Color.Black, racAddress, BorderSide.None);
                address.VertAlignment = VertAlignment.Center;
                address.HorzAlignment = HorzAlignment.Center;

                var logo = UIHelper.ConvertBinaryToImage(company.COMPANY_LOGO);//HPictureBox.GetImageValue(Current.Company.LogoId ?? 0);
                //draw logo
                var ract = new RectangleF(0, 0, 70, 70);
                var imageDraw = e.Graph.DrawImage(logo, ract, BorderSide.None, Color.Transparent);
                imageDraw.SizeMode = ImageSizeMode.ZoomImage;
                imageDraw.BorderStyle = BrickBorderStyle.Inset;

                var startPoint = new PointF(0, 71);
                var endPoint = new PointF(e.Graph.ClientPageSize.Width, 71);
                //var line = e.Graph.DrawLine(startPoint, endPoint, Color.Gray, 0.1f);
                //line.BorderStyle = BrickBorderStyle.Center;
                //line.BorderColor = Color.Gray;
                companyHeaderHeight = 80;
            }
            //TextBrick brick = null;
            foreach (var header in _headers)
            {
                var rectWidth = header.Rectangle.Width;
                if (header.UseClientSizeWidth)
                {
                    rectWidth = e.Graph.ClientPageSize.Width;
                }
                var rect = new RectangleF(header.Rectangle.X, header.Rectangle.Y + companyHeaderHeight, rectWidth, header.Rectangle.Height);
                e.Graph.Font = header.Font;
                e.Graph.BorderColor = Color.Gray;
                var brick = e.Graph.DrawString(header.Text, header.Color, rect, header.BorderSide);
                brick.VertAlignment = header.VertAlignment;
                brick.HorzAlignment = header.HorAlignment;
            }
        }
        private static List<ExportHeader> GetDefaultHeader(string reportTitle, string reportDescription, Font defaultFont)
        {
           
            var headers = new List<ExportHeader>();
            // var companyName = new ExportHeader(){Text =Current.Company.Name, HorAlignment =  HorzAlignment.Near,BorderSide = BorderSide.None,Color = Color.Black,Font = defaultFont,Rectangle = new RectangleF(0, 0, 0, 30)};
            var titleFont = new Font(defaultFont.FontFamily, defaultFont.Size, FontStyle.Bold);
            var title = new ExportHeader() { Text = reportTitle, HorAlignment = HorzAlignment.Center, BorderSide = BorderSide.None, Color = Color.Black, Font = titleFont, Rectangle = new RectangleF(0, 10, 0, 30) };
            var description = new ExportHeader() { Text = reportDescription, HorAlignment = HorzAlignment.Center, BorderSide = BorderSide.None, Color = Color.Black, Font = defaultFont, Rectangle = new RectangleF(0, 40, 0, 30) };
            //if (IsPivotGrid)
            //{
            //    title = new ExportHeader() { Text = reportTitle, HorAlignment = HorzAlignment.Center, BorderSide = BorderSide.None, Color = Color.Black, Font = titleFont, Rectangle = new RectangleF(100, 10, 0, 30) };
            //    description = new ExportHeader() { Text = reportDescription, HorAlignment = HorzAlignment.Center, BorderSide = BorderSide.None, Color = Color.Black, Font = defaultFont, Rectangle = new RectangleF(100, 40, 0, 30) };
            //}
            //     headers.Add(companyName);
            headers.Add(title);
            headers.Add(description);
            return headers;
        }
        public static void ExportTo(string fileName, TreeList treelist, string reportTitle, string reportDescription, string exPath, bool printLanscape = false, bool isEmail = false, List<ExportHeader> headers = null, bool useCompanyHeader = true)
        {
            DevExpress.XtraPrinting.PrintingSystem ps = new DevExpress.XtraPrinting.PrintingSystem();
            DevExpress.XtraPrinting.PrintableComponentLink link = new PrintableComponentLink(ps);
            string path = "";
            path = Path.Combine(Directory.GetCurrentDirectory(), path);
            string exportPath = path + "\\" + exPath;
            string downloadPath = "";
            if (isEmail == true)
            {
                downloadPath = NextAvailableFilename(new System.IO.FileInfo(exportPath).FullName + fileName);
            }
            else
            {
                 downloadPath = NextAvailableFilename(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"\Downloads\");
                if (!Directory.Exists(downloadPath))
                {
                    Directory.CreateDirectory(downloadPath);
                }
                downloadPath = downloadPath + fileName;
            }
            //"Excel (2003)(.xls)|*.xls|Excel (2010) (.xlsx)|*.xlsx |RichText File (.rtf)|*.rtf |Pdf File (.pdf)|*.pdf |Html File (.html)|*.html"; 
            string fileExtenstion = new FileInfo(downloadPath).Extension;
            Runner.RunNewThread(() =>
            {
                // Specify the control to be printed.                  
                link.Component = treelist;
                headers = headers ?? new List<ExportHeader>();
                //headers = headers.Any() ?headers : GetDefaultHeader("បញ្ជីវិក្ក័យបត្រ័", "ចាប់ពី 30-Sep-2020 ដល់ 30-Sep-2020", gridControl.Font) ;
                headers = headers.Any() ? headers : GetDefaultHeader(reportTitle, reportDescription, treelist.Font);
                _headers = headers;
                //            ps.Landscape = view.OptionsPrint
                _exportFont = treelist.Font;
                _useCompanyeader = useCompanyHeader;
                //      link.CreateMarginalHeaderArea += Link_CreateMarginalHeaderArea;  
                link.Landscape = printLanscape;
                link.Margins = new System.Drawing.Printing.Margins(1, 1, 2, 2);
                link.MinMargins = new System.Drawing.Printing.Margins(1, 1, 1, 1);
                link.CreateReportHeaderArea += Link_CreateReportHeaderArea;
                link.CreateMarginalFooterArea += Link_CreateMarginalFooterArea;

                // Specify export options.
                XlsxExportOptions Options = new XlsxExportOptions()
                {
                    TextExportMode = TextExportMode.Value,
                    FitToPrintedPageWidth = true,
                };
                // Generate a report.
                link.CreateDocument();
                //write your code export here...
                switch (fileExtenstion)
                {
                    case ".xls":
                        link.PrintingSystem.ExportToXls(downloadPath);
                        break;
                    case ".xlsx":
                        link.PrintingSystem.ExportToXlsx(downloadPath);
                        break;
                    case ".rtf":
                        link.PrintingSystem.ExportToRtf(downloadPath);
                        break;
                    case ".pdf":
                        //link.PrintingSystem.ExportOptions.Pdf.DocumentOptions.Author = Current.User?.Name??";
                        link.PrintingSystem.ExportToPdf(downloadPath);
                        break;
                    case ".html":
                        link.PrintingSystem.ExportToHtml(downloadPath);
                        break;
                    case ".mht":
                        link.PrintingSystem.ExportToMht(downloadPath);
                        break;
                    case ".csv":
                        link.PrintingSystem.ExportOptions.Csv.Encoding = Encoding.Unicode;
                        link.PrintingSystem.ExportOptions.Csv.Separator = CultureInfo.CurrentCulture.TextInfo.ListSeparator.ToString();
                        link.PrintingSystem.ExportToCsv(downloadPath);
                        break;
                    case ".docx":
                        link.PrintingSystem.ExportToDocx(downloadPath);
                        break;
                    default:
                        break;
                }
            });
            OpenFolderAndSelectFile(downloadPath);
        }

        public static void ExportEFiling(Control curr, string fileName, string forMonth, GridControl gridControl, Dictionary<string, object> gridSource, Dictionary<string, string> gridSheetNames, string reportTitle, string reportDescription, bool printLanscape = false, List<ExportHeader> headers = null, bool useCompanyHeader = true, bool exportAllview = false, DateTime? ToDate = null)
        {
            var downloadPath = NextAvailableFilename(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"\Downloads\");
            if (!Directory.Exists(downloadPath))
            {
                Directory.CreateDirectory(downloadPath);
            }
            ToDate = ToDate ?? DateTime.Now;
            //"Excel (2003)(.xls)|*.xls|Excel (2010) (.xlsx)|*.xlsx |RichText File (.rtf)|*.rtf |Pdf File (.pdf)|*.pdf |Html File (.html)|*.html"; 
            //string fileExtenstion = new FileInfo(downloadPath).Extension;

            var tmpFiles = new Dictionary<string, string>();
            var writeTitle = false;
            foreach (GridView view in gridControl.ViewCollection.OrderBy(x => x.LevelName))
            {
                if (view.Name == "dgvEFilingSale")
                {
                    curr.Invoke(new MethodInvoker(() => view.Columns[17].Visible = false));
                }
                var tmpFile = Path.GetTempFileName();
                PrintingSystem ps = new PrintingSystem();
                PrintableComponentLink link = new PrintableComponentLink(ps);
                curr.Invoke(new MethodInvoker(() =>
                {
                    gridControl.MainView = view;
                    var source = gridSource[view.Name];
                    gridControl.DataSource = source;
                }));
                // Specify the control to be printed.                  
                link.Component = gridControl;
                var sheetName = gridSheetNames.FirstOrDefault(x => x.Key == view.Name);
                _exportFont = gridControl.Font;
                link.Landscape = printLanscape;
                link.Margins = new System.Drawing.Printing.Margins(1, 1, 2, 40);
                curr.Invoke(new MethodInvoker(() => link.CreateDocument()));
                link.PrintingSystem.ExportToXls(tmpFile);
                tmpFiles.Add(sheetName.Value, tmpFile);
                _headers = new List<ExportHeader>();
            }

            //Minus amount
            var tmpFile1 = Path.GetTempFileName();
            PrintingSystem ps1 = new PrintingSystem();
            PrintableComponentLink link1 = new PrintableComponentLink(ps1);
            var source1 = gridSource["dgvEFilingSaleMinus"];
            GridView view1 = (GridView)gridControl.ViewCollection.OrderBy(x => x.LevelName).FirstOrDefault();
            curr.Invoke(new MethodInvoker(() =>
            {
                view1.Columns[17].Visible = true;
                gridControl.MainView = view1;
                gridControl.DataSource = source1;
            }));

            link1.Component = gridControl;
            var sheetName1 = gridSheetNames.FirstOrDefault(x => x.Key == "dgvEFilingSaleMinus");
            _exportFont = gridControl.Font;
            link1.Landscape = printLanscape;
            link1.Margins = new System.Drawing.Printing.Margins(1, 1, 2, 40);
            curr.Invoke(new MethodInvoker(() => { link1.CreateDocument(); }));
            link1.PrintingSystem.ExportToXls(tmpFile1);
            tmpFiles.Add(sheetName1.Value, tmpFile1);
            _headers = new List<ExportHeader>();

            ///merged  
            ///
            DevExpress.Spreadsheet.Workbook workbook = new Workbook();
            var index = 1;
            foreach (var item in tmpFiles)
            {
                var workbookByItem = new Workbook();
                workbookByItem.LoadDocument(item.Value);
                if (index == 1 || index == 4)
                {

                    workbookByItem.Worksheets[0].Rows.Insert(0);

                    var tinTitle = workbookByItem.Worksheets[0].Range["A1:B1"];
                    workbookByItem.Worksheets[0].MergeCells(tinTitle);
                    tinTitle.Value = "លេខសម្គាល់អត្តសញ្ញាណ៖";
                    tinTitle.Font.Name = gridControl.Font.Name;
                    tinTitle.Font.Size = gridControl.Font.Size;


                    var tin = workbookByItem.Worksheets[0].Range["C1"];
                    tin.Value = DBDataContext.Db.TBL_COMPANies.FirstOrDefault().VATTIN;
                    tin.Font.Name = gridControl.Font.Name;
                    tin.Font.Size = gridControl.Font.Size;
                    tin.Font.Bold = true;


                    var dateTitle = workbookByItem.Worksheets[0].Range["D1"];
                    dateTitle.Value = "សម្រាប់ខែ៖";
                    dateTitle.Font.Name = gridControl.Font.Name;
                    dateTitle.Font.Size = gridControl.Font.Size;

                    var date = workbookByItem.Worksheets[0].Range["E1"];
                    date.Value = forMonth;
                    date.Font.Name = gridControl.Font.Name;
                    date.Font.Size = gridControl.Font.Size;
                    date.Font.Bold = true;
                }



                workbook.Worksheets.Insert(index, item.Key);
                workbook.Worksheets[index].CopyFrom(workbookByItem.Worksheets[0]);
                index += 1;
            }
            workbook.Worksheets.RemoveAt(0);



            //header
            var exportNumber = DataHelper.ParseToDecimal(DBDataContext.Db.TBL_UTILITies.FirstOrDefault(x => x.UTILITY_ID == (int)Utility.E_FILLING_EXPORT_NUMBER).UTILITY_VALUE);
            var exported = exportNumber;
            decimal startRow = 4;
            var countSale = workbook.Worksheets[0].GetDataRange().RowCount - 3;
            var biggestRecord = countSale;// (countSale > countIdividualCustomer) ? countSale : countIdividualCustomer;
            var totalFiles = Math.Ceiling(biggestRecord / exportNumber);
            for (int i = 0; i < totalFiles; i++)
            {
                 downloadPath = NextAvailableFilename(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"\Downloads\" + fileName);
                var tmp = new Workbook();
                tmp.LoadDocument(Directory.GetCurrentDirectory() + @"\Template\E-FilingSale.xlsx");
                //var tmp = t.Clone();
                //t.Dispose();

                tmp.Worksheets[0].Range["1:1"].CopyFrom(workbook.Worksheets[0].Range["1:1"]);
                workbook.Worksheets[0].Range[string.Format("{0}:{1}", startRow, exported)].MoveTo(tmp.Worksheets[0].Range[string.Format("4:5000")]);
                //workbook.Worksheets[2].Range[string.Format("{0}:{1}", 2, exported)].MoveTo(tmp.Worksheets[1].Range[string.Format("2:5000")]);

                tmp.Worksheets.ActiveWorksheet = tmp.Worksheets[0];
                tmp.SaveDocument(downloadPath);
                startRow = exported + 1;
                exported += exportNumber - 3;
                tmp.Dispose();
            }

            //Minus amount record
            exported = exportNumber;
            startRow = 4;
            countSale = workbook.Worksheets[3].GetDataRange().RowCount - 3;
            totalFiles = Math.Ceiling(countSale / exportNumber);
            for (var i = 0; i < totalFiles; i++)
            {

                ToDate = ToDate ?? DateTime.Now;
                var creditNoteFileName = $"E-Filing-Credit-Notes {ToDate?.ToString("yyyy-MM")}";
                downloadPath = NextAvailableFilename(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"\Downloads\" + creditNoteFileName + ".xls");
                var tmp = new Workbook();
                tmp.LoadDocument(Directory.GetCurrentDirectory() + @"\Template\E-FilingSale.xlsx");
                tmp.Worksheets[0].Range["1:3"].CopyFrom(workbook.Worksheets[3].Range["1:3"]);
                //tmp.Worksheets[0].Range["N2:N3"].CopyFrom(workbook.Worksheets[3].Range["N2:N3"]);
                workbook.Worksheets[3].Range[string.Format("{0}:{1}", startRow, exported)].MoveTo(tmp.Worksheets[0].Range[string.Format("4:5000")]);
                tmp.Worksheets.ActiveWorksheet = tmp.Worksheets[0];
                tmp.SaveDocument(downloadPath);
                startRow = exported + 1;
                exported += exportNumber - 3;
                tmp.Dispose();
            }
            OpenFolderAndSelectFile(downloadPath);
        }
        public static void ToExcel(DataTable dt, string defaultFileName, string title, string description)
        {
            var downloadPath = NextAvailableFilename(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"\Downloads\" + defaultFileName);
            if (!Directory.Exists(downloadPath))
            {
                Directory.CreateDirectory(downloadPath);
            }
            defaultFileName = Path.Combine(downloadPath, $"{defaultFileName}{DateTime.Now.ToString("yyyyyMMddhhmmss")}.xlsx");
            XLWorkbook wb = new XLWorkbook();
            wb.Worksheets.Add(dt, "Sheet1");

            wb.SaveAs(defaultFileName);
            if (!File.Exists(defaultFileName))
            {
                return;
            }
            // combine the arguments together
            // it doesn't matter if there is a space after ','
            string argument = "/select, \"" + defaultFileName + "\"";
            System.Diagnostics.Process.Start("explorer.exe", argument);
        }
    }
    public class ExportHeader
    {
        public string Text { get; set; }
        public System.Drawing.Font Font { get; set; }
        public VertAlignment VertAlignment { get; set; } = VertAlignment.Center;
        public HorzAlignment HorAlignment { get; set; }
        public Color Color { get; set; } = Color.Black;
        public RectangleF Rectangle { get; set; }// new RectangleF(0, 0, 500, 20)

        public BorderSide BorderSide { get; set; } = BorderSide.None;
        public bool UseClientSizeWidth { get; set; } = true;
    }
}
