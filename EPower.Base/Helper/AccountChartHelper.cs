﻿using HB01.Domain.Enums;
using HB01.Domain.ListModels;
using SoftTech;
using SoftTech.Helper;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace EPower.Base.Helper
{
    public class AccountChartHelper
    {
        public static DataTable GetAccountsTable(AccountConfig accConfig, string all = null)
        {
            var tmp = GetAccounts(accConfig, all);
            var dt = tmp.Select(x => new { x.ACCOUNT_ID, ITEM_NAME = x.ACCOUNT_CODE + " " + x.ACCOUNT_NAME })
                    .OrderBy(x => x.ITEM_NAME)
                    ._ToDataTable();
            return dt;
        }
        public static List<TBL_ACCOUNT_CHART> GetAccounts(AccountConfig accountConfig, string all = null)
        {
            var tmp = GetAccounts(accountConfig);
            if (all != null)
            {
                tmp.Insert(0, new TBL_ACCOUNT_CHART()
                {
                    ACCOUNT_ID = 0,
                    ACCOUNT_CODE = "",
                    ACCOUNT_NAME = all,
                    PARENT_ID = -1
                });
            }
            return tmp;
        }
        public static List<TBL_ACCOUNT_CHART> GetAccounts(AccountConfig accountConfig)
        {
            // bind IncomeAccount
            var config = DBDataContext.Db.TBL_ACCOUNT_CONFIGs.FirstOrDefault(x => x.CONFIG_ID == (int)accountConfig);
            if (config == null)
            {
                return null;
            }
            var categories = config.CONFIG_VALUE.Split(',', ';')
                                  .Select(x => DataHelper.ParseToInt(x))
                                  .ToArray();
            return DBDataContext.Db.TBL_ACCOUNT_CHARTs
                                    .Where(x
                                        => x.IS_ACTIVE
                                            && (categories.Contains(x.ACCOUNT_ID))
                                    ).ToList();
        }

        public static List<TBL_ACCOUNT_CHART> GetAccounts(AccountConfig accountConfig, int defualutId, string defaultName, int parrentId)
        {
            // bind IncomeAccount
            var config = DBDataContext.Db.TBL_ACCOUNT_CONFIGs.FirstOrDefault(x => x.CONFIG_ID == (int)accountConfig);

            var categories = config.CONFIG_VALUE.Split(',', ';')
                                  .Select(x => DataHelper.ParseToInt(x))
                                  .ToArray();
            List<TBL_ACCOUNT_CHART> _list = new List<TBL_ACCOUNT_CHART>();
            TBL_ACCOUNT_CHART _default = new TBL_ACCOUNT_CHART()
            {
                ACCOUNT_ID = defualutId,
                ACCOUNT_CODE = "",
                ACCOUNT_NAME = defaultName,
                PARENT_ID = parrentId
            };
            _list.Add(_default);
            var accs = DBDataContext.Db.TBL_ACCOUNT_CHARTs
                                   .Where(x
                                       => categories.Contains(x.ACCOUNT_ID)
                                          && x.IS_ACTIVE
                                   ).ToList();
            accs.ForEach(x => x.PARENT_ID = 71);
            _list.AddRange(accs);

            return _list;
        }
        public static List<TBL_ACCOUNT_CHART> GetAccounts(int accountConfigId)
        {
            var config = DBDataContext.Db.TBL_ACCOUNT_CONFIGs.FirstOrDefault(x => x.CONFIG_ID == accountConfigId);

            var categories = config.CONFIG_VALUE.Split(',', ';')
                                  .Select(x => DataHelper.ParseToInt(x))
                                  .ToArray();
            return DBDataContext.Db.TBL_ACCOUNT_CHARTs
                                    .Where(x
                                        => categories.Contains(x.ACCOUNT_ID)
                                           && x.IS_ACTIVE
                                    ).ToList();
        }

        public static List<AccountListModel> GetAccounts(AccountSearchParam param)
        {
            var currency = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.IS_DEFAULT_CURRENCY);
            var _accounts = new List<AccountListModel>();

            param.Natures = param.Natures ?? new List<AccountNatures>();
            param.AccountTypeIds = param.AccountTypeIds ?? new List<int>();
            param.AccountClassifies = param.AccountClassifies ?? new List<ClassifiyAccountType>();
            var natures = param.Natures.Select(x => (int)x).ToList();

            _accounts = (from a in DBDataContext.Db.TBL_ACCOUNT_CHARTs
                         join at in DBDataContext.Db.TBL_ACCOUNT_TYPEs.Where(x => x.IS_ACTIVE) on a.ACCOUNT_TYPE_ID equals at.TYPE_ID
                         join cc in DBDataContext.Db.TLKP_CURRENCies on a.CURRENCY_ID equals cc.CURRENCY_ID
                         into ccj
                         from cc in ccj.DefaultIfEmpty()
                         where a.IS_ACTIVE
                         && (param.AccountTypeIds.Count() == 0 || param.AccountTypeIds.Contains(a.ACCOUNT_TYPE_ID))
                         //&& (param.AccountClassifies.Contains(at.Classifiy) || param.AccountClassifies.Count() == 0)
                         && (natures.Count == 0 || natures.Contains(at.NATURE))
                            && (param.CurrencyId == 0 || a.CURRENCY_ID == param.CurrencyId)
                         select new AccountListModel()
                         {
                             Id = a.ACCOUNT_ID,
                             ParentAccountId = a.PARENT_ID,
                             AccountTypeId = a.ACCOUNT_TYPE_ID,
                             AccountCode = a.ACCOUNT_CODE,
                             AccountName = (a.ACCOUNT_NAME),
                             AccountNature = ((AccountNatures)at.NATURE).ToString(),
                             AccountType = at.TYPE_NAME,
                             DisplayAccountName = (a.ACCOUNT_CODE + " " + a.ACCOUNT_NAME),
                             Currency = (cc == null) ? currency.CURRENCY_CODE : cc.CURRENCY_CODE,
                             AccountTypeCode = at.INDEX.ToString()
                         }).ToList();
            return _accounts.OrderBy(x => DataHelper.ParseToInt(x.AccountTypeCode)).ThenBy(x => x.AccountCode).ToList();
        }
        public static string ConcatAccountId(IEnumerable<string> accountChart)
        {
            return string.Join(",", accountChart.ToArray());
        }
    }
}
