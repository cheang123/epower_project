﻿namespace EPower.Base.Helper.Component
{
    partial class ReportHeader
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._lblName = new System.Windows.Forms.Label();
            this._lblPhone = new System.Windows.Forms.Label();
            this._lblAddress = new System.Windows.Forms.Label();
            this._lblReportSub1 = new System.Windows.Forms.Label();
            this._lblTitle = new System.Windows.Forms.Label();
            this.picLogo = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // _lblName
            // 
            this._lblName.Cursor = System.Windows.Forms.Cursors.Hand;
            this._lblName.Dock = System.Windows.Forms.DockStyle.Top;
            this._lblName.Font = new System.Drawing.Font("Kh Muol", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblName.Location = new System.Drawing.Point(0, 10);
            this._lblName.Name = "_lblName";
            this._lblName.Size = new System.Drawing.Size(810, 25);
            this._lblName.TabIndex = 18;
            this._lblName.Text = "យូប៊ីល២៤ ខូអិលធីឌី";
            this._lblName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this._lblName.MouseDown += new System.Windows.Forms.MouseEventHandler(this._lblName_MouseDown);
            // 
            // _lblPhone
            // 
            this._lblPhone.Cursor = System.Windows.Forms.Cursors.Hand;
            this._lblPhone.Font = new System.Drawing.Font("Khmer Kep", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblPhone.Location = new System.Drawing.Point(0, 57);
            this._lblPhone.Name = "_lblPhone";
            this._lblPhone.Size = new System.Drawing.Size(801, 21);
            this._lblPhone.TabIndex = 25;
            this._lblPhone.Text = "093 99 49 06";
            this._lblPhone.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this._lblPhone.Visible = false;
            this._lblPhone.MouseDown += new System.Windows.Forms.MouseEventHandler(this._lblPhone_MouseDown);
            // 
            // _lblAddress
            // 
            this._lblAddress.Cursor = System.Windows.Forms.Cursors.Hand;
            this._lblAddress.Dock = System.Windows.Forms.DockStyle.Top;
            this._lblAddress.Font = new System.Drawing.Font("Khmer Kep", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblAddress.Location = new System.Drawing.Point(0, 35);
            this._lblAddress.Name = "_lblAddress";
            this._lblAddress.Size = new System.Drawing.Size(810, 52);
            this._lblAddress.TabIndex = 27;
            this._lblAddress.Text = "អាសយដ្ឋាន";
            this._lblAddress.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // _lblReportSub1
            // 
            this._lblReportSub1.Cursor = System.Windows.Forms.Cursors.Hand;
            this._lblReportSub1.Dock = System.Windows.Forms.DockStyle.Top;
            this._lblReportSub1.Font = new System.Drawing.Font("Khmer Kep", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblReportSub1.Location = new System.Drawing.Point(0, 108);
            this._lblReportSub1.Name = "_lblReportSub1";
            this._lblReportSub1.Size = new System.Drawing.Size(810, 21);
            this._lblReportSub1.TabIndex = 29;
            this._lblReportSub1.Text = "Loading ...";
            this._lblReportSub1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _lblTitle
            // 
            this._lblTitle.Cursor = System.Windows.Forms.Cursors.Hand;
            this._lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this._lblTitle.Font = new System.Drawing.Font("Kh Muol", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblTitle.Location = new System.Drawing.Point(0, 87);
            this._lblTitle.Name = "_lblTitle";
            this._lblTitle.Size = new System.Drawing.Size(810, 21);
            this._lblTitle.TabIndex = 28;
            this._lblTitle.Text = "Loading ...";
            this._lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // picLogo
            // 
            this.picLogo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picLogo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picLogo.Location = new System.Drawing.Point(5, 10);
            this.picLogo.Margin = new System.Windows.Forms.Padding(0);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(111, 111);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picLogo.TabIndex = 30;
            this.picLogo.TabStop = false;
            // 
            // ReportHeader
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.picLogo);
            this.Controls.Add(this._lblReportSub1);
            this.Controls.Add(this._lblTitle);
            this.Controls.Add(this._lblAddress);
            this.Controls.Add(this._lblPhone);
            this.Controls.Add(this._lblName);
            this.Font = new System.Drawing.Font("Khmer Kep", 9.75F);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ReportHeader";
            this.Padding = new System.Windows.Forms.Padding(0, 10, 0, 3);
            this.Size = new System.Drawing.Size(810, 132);
            this.Load += new System.EventHandler(this.ReportHeader_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label _lblName;
        private System.Windows.Forms.Label _lblPhone;
        private System.Windows.Forms.Label _lblAddress;
        private System.Windows.Forms.Label _lblReportSub1;
        private System.Windows.Forms.Label _lblTitle;
        private System.Windows.Forms.PictureBox picLogo;
    }
}
