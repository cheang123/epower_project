﻿using DevExpress.Utils.Filtering.Internal;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Localization;
using EPower.Base.Properties;

namespace EPower.Base.Helper.DevExpressCustomize
{
    public class KHEditorsLocalizer : Localizer
    {
        //public override string Language { get { return "km-KH"; } }
        public override string GetLocalizedString(StringId id)
        {
            switch (id)
            {
                case StringId.CaptionError: return "កំហុស";
                case StringId.XtraMessageBoxYesButtonText: return "យល់ព្រម";
                case StringId.XtraMessageBoxOkButtonText: return "យល់ព្រម";
                case StringId.XtraMessageBoxCancelButtonText: return "ទេ";
                case StringId.XtraMessageBoxNoButtonText: return "ទេ";
                case StringId.XtraMessageBoxAbortButtonText: return "ទេ";
                case StringId.FilterShowAll: return "(ជ្រើស​យក​ទាំងអស់)";
                default:
                    return base.GetLocalizedString(id);
            }
        }

    }
    public class KHGridLocalizer : GridLocalizer
    {
        //public override string Language { get { return "km-KH"; } }
        public override string GetLocalizedString(GridStringId id)
        {
            switch (id)
            {
                case GridStringId.ColumnViewExceptionMessage: return "តើអ្នកចង់កែតម្រូវឫទេ?";
                default:
                    return base.GetLocalizedString(id);
            }
        }
    } 
    public class CustomFilterUIElementResXLocalizer : DevExpress.Utils.Filtering.Internal.FilterUIElementResXLocalizer
    {
        public CustomFilterUIElementResXLocalizer() { }
        public override string GetLocalizedString(FilterUIElementLocalizerStringId id)
        {
            switch (id)
            {
                case FilterUIElementLocalizerStringId.CustomUIFilterBetweenName: return Resources.BETWEEN;
                case FilterUIElementLocalizerStringId.CustomUIFilterGreaterThanName: return Resources.GREATER_THAN;
                case FilterUIElementLocalizerStringId.CustomUIFilterLessThanName: return Resources.LESS_THAN;
                case FilterUIElementLocalizerStringId.CustomUIFilterEqualsName: return Resources.EQUAL;
                case FilterUIElementLocalizerStringId.CustomUIFilterDoesNotEqualName : return Resources.NOT_EQUAL;
                case FilterUIElementLocalizerStringId.CustomUIFilterIsNullName: return Resources.IS_NULL;
                case FilterUIElementLocalizerStringId.CustomUIFilterIsNotNullName: return Resources.IS_NOT_NULL;
                case FilterUIElementLocalizerStringId.CustomUIFilterGreaterThanOrEqualToName: return Resources.GREATER_THAN_OR_EQUAL;
                case FilterUIElementLocalizerStringId.CustomUIFilterLessThanOrEqualToName: return Resources.LESS_THAN_OR_EQUAL;
                //case FilterUIElementLocalizerStringId.CustomUIFilterTopNName: return Resources.LESS_THAN;
                //case FilterUIElementLocalizerStringId.CustomUIFilterBottomNName: return Resources.LESS_THAN;
                //case FilterUIElementLocalizerStringId.CustomUIFilterAboveAverageName: return Resources.LESS_THAN;
                //case FilterUIElementLocalizerStringId.CustomUIFilterBelowAverageName: return Resources.LESS_THAN;
                //case FilterUIElementLocalizerStringId.CustomUIFilterCustomName: return Resources.LESS_THAN;
                case FilterUIElementLocalizerStringId.CustomUINullValuePromptSelectAValue: return Resources.VALUE;
                case FilterUIElementLocalizerStringId.CustomUIFilterTodayName:return Resources.TODAY;
                case FilterUIElementLocalizerStringId.CustomUIFilterThisWeekName:return Resources.THIS_WEEK;
                case FilterUIElementLocalizerStringId.CustomUIFilterThisMonthName:return Resources.THIS_MONTH;
                case FilterUIElementLocalizerStringId.CustomUIFilterThisYearName:return Resources.THIS_YEAR;
                case FilterUIElementLocalizerStringId.CustomUIFilterYesterdayName: return Resources.YESTERDAY;
                case FilterUIElementLocalizerStringId.CustomUIFilterLastWeekName:return Resources.LAST_WEEK;
                case FilterUIElementLocalizerStringId.CustomUIFilterLastMonthName:return Resources.LAST_MONTH;
                case FilterUIElementLocalizerStringId.CustomUIFilterLastYearName:return Resources.LAST_YEAR;
                case FilterUIElementLocalizerStringId.CustomUIFilterTomorrowName:return Resources.TOMORROW;
                case FilterUIElementLocalizerStringId.CustomUIFilterNextWeekName:return Resources.NEXT_WEEK;
                case FilterUIElementLocalizerStringId.CustomUIFilterNextMonthName:return Resources.NEXT_MONTH;
                case FilterUIElementLocalizerStringId.CustomUIFilterNextYearName:return Resources.NEXT_YEAR;
                default:
                    return base.GetLocalizedString(id);
            }
            return base.GetLocalizedString(id);
        }
        protected override string GetLocalizedStringCore(FilterUIElementLocalizerStringId id)
        {
            switch (id)
            {
                case FilterUIElementLocalizerStringId.FilteringUIClose: return Resources.CLOSE;
                case FilterUIElementLocalizerStringId.FilteringUITabValues: return Resources.VALUE;
            }
            return base.GetLocalizedStringCore(id);
        }
    }

}
