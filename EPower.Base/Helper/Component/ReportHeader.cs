﻿using SoftTech;
using SoftTech.Helper;
using System;
using System.Data.Linq;
using System.Windows.Forms;

namespace EPower.Base.Helper.Component
{
    public partial class ReportHeader : UserControl
    {
        TBL_COMPANY company = new TBL_COMPANY();
        public ReportHeader()
        {
            InitializeComponent();
            picLogo.BorderStyle = BorderStyle.None;
            _lblTitle.Text = "Loading ...";
            _lblReportSub1.Text = "Loading ...";
        }
        public void SetVisibleReportSub1(bool visible)
        {
            _lblReportSub1.Visible = visible;
        }
        private void ReportHeader_Load(object sender, EventArgs e)
        {
            //SetFormat();
        }
        //public void SetFormat()
        //{
        //    var format = Current.Company.Setting();
        //    if (format != null)
        //    {
        //        var fmCompanyName = format.ReportFormats[ReportFormats.CompanyName];
        //        _lblName.Font = fmCompanyName.Font;
        //        _lblName.ForeColor = ColorTranslator.FromHtml(fmCompanyName.ForeColor);
        //        var fmAddress = format.ReportFormats[ReportFormats.Address];
        //        _lblAddress.Font = fmAddress.Font;
        //        _lblAddress.ForeColor = ColorTranslator.FromHtml(fmAddress.ForeColor);
        //        var fmPhone = format.ReportFormats[ReportFormats.Phone];
        //        _lblPhone.Font = fmPhone.Font;
        //        _lblPhone.ForeColor = ColorTranslator.FromHtml(fmPhone.ForeColor);
        //        var fmTitle = format.ReportFormats[ReportFormats.Title];
        //        _lblTitle.Font = fmTitle.Font;
        //        _lblTitle.ForeColor = ColorTranslator.FromHtml(fmTitle.ForeColor);
        //        var fmSubTitle1 = format.ReportFormats[ReportFormats.SubTitle1];
        //        _lblReportSub1.Font = fmSubTitle1.Font;
        //        _lblReportSub1.ForeColor = ColorTranslator.FromHtml(fmSubTitle1.ForeColor);

        //        picLogo.Value = company.LogoId ?? 0;
                
        //    }
        //}
        public TBL_COMPANY Company
        {
            get { return company; }
            set
            {
                company = value;
                //var media = MediaDataLogic.Instance.Find(company.PhotoUrl ?? 0);
                //picLogo.Image = media != null ? ImageHelper.ConvertBinaryToImage(media.Data) : Resources.logo;
                picLogo.Image = UIHelper.ConvertBinaryToImage(company.COMPANY_LOGO);
                this._lblName.Text = company?.COMPANY_NAME ?? company?.COMPANY_NAME_EN;
                this._lblAddress.Text = company?.ADDRESS ?? company?.ADDRESS;
                this._lblPhone.Text = company?.PHONE ?? "";
            }
        }
        public Binary Logo => this.Company?.COMPANY_LOGO;
        public new string CompanyName => company.COMPANY_NAME ?? company.COMPANY_NAME_EN;
        public string Address => company.ADDRESS ?? "";
        public string Phone => company?.PHONE ?? "";
        public string ReportTitle
        {
            get { return this._lblTitle.Text; }
            set { this._lblTitle.Text = value; }
        }
        public string ReportSubTitle1
        {
            get { return this._lblReportSub1.Text; }
            set { this._lblReportSub1.Text = value; }
        }

        private void _lblName_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Right) return;
            //ChangeFormat(ReportFormats.CompanyName, _lblName);
        }

        private void _lblAddress_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Right) return;
            //ChangeFormat(ReportFormats.Address, _lblAddress);
        }

        private void _lblPhone_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Right) return;
            //ChangeFormat(ReportFormats.Phone, _lblPhone);
        }

        private void _lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Right) return;
            //ChangeFormat(ReportFormats.Title, _lblTitle);
        }

        private void _lblReportSub1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Right) return;
            //ChangeFormat(ReportFormats.SubTitle1, _lblReportSub1);
        }
    }
}
