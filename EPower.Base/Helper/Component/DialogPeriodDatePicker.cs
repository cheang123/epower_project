﻿using EPower.Base.Properties;
using SoftTech.Helper;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace EPower.Base.Helper.Component
{
    public partial class DialogPeriodDatePicker : SoftTech.Component.ExDialog
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }

        #region Constructor
        public DialogPeriodDatePicker(DateTime d1, DateTime d2)
        {
            InitializeComponent();
            ResourceHelper.ApplyResource(this);
            dtpFromDate.FormatSortDate();
            dtpToDate.FormatSortDate();

            dtpFromDate.Value = d1;
            dtpToDate.Value = d2;
        }
        #endregion

        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool EnabledToDate
        {
            get { return dtpToDate.Enabled; }
            set { dtpToDate.Enabled = value; }
        }
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public DateTime MaxFromDate
        {
            get { return dtpFromDate.MaxDate; }
            set { dtpFromDate.MaxDate = value; }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            FromDate = dtpFromDate.Value;
            ToDate = dtpToDate.Value;
            DialogResult = DialogResult.OK;
        }
    }
}
