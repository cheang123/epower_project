﻿using Dynamic.Component;
using Dynamic.Helpers;
using EPower.Base.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;


namespace EPower.Base.Helper.Component
{
    [DefaultEvent("ValueChanged")]
    public partial class ReportDatePicker : UserControl
    {
        //private bool _isNeedRefresh = true;
        private bool _isCustomBinding = false;
        private DateTime oldCustomFromDate​ = DateTime.Now;
        private DateTime oldCustomToDate = DateTime.Now;

        public bool Today { get; set; } = true;
        public bool ThisWeek { get; set; } = true;
        public bool ThisMonth { get; set; } = true;
        public bool ThisQuarter { get; set; } = true;
        public bool ThisFinancialYear { get; set; } = true;

        public bool Yesterday { get; set; } = true;
        public bool LastWeek { get; set; } = true;
        public bool LastMonth { get; set; } = true;
        public bool LastQuarter { get; set; } = true;
        public bool LastFinancialYear { get; set; } = true;

        private bool _isReady = true;

        public DateRanges DefaultValue { get; set; }
        private DateRanges PreviousValue { get; set; }
        public ReportDatePicker()
        {
            InitializeComponent();
            InitializeObj();
        }
        private void ReportDatePicker_Load(object sender, EventArgs e)
        {
            // Reload setting
            var targetName = this.GetFullName();
            //Properties.Settings.Default.USER_DATA = Properties.Settings.Default.USER_DATA ?? new HB01.Base.Helpers.UISetting();
            //if (Properties.Settings.Default.USER_DATA.DatePickerSettings[targetName] is DatePickerSetting dateRange)
            //{
            //    if ((DateRanges)dateRange.DateRange == DateRanges.Custom)
            //    {
            //        _isCustomBinding = true;
            //        oldCustomFromDate = dateRange.FromDate;
            //        oldCustomToDate = dateRange.ToDate;
            //        InitializeObj($"{dateRange.FromDate.ToShortDate()} {Resources.To} {dateRange.ToDate.ToShortDate()}");
            //    }
            //    else if ((DateRanges)dateRange.DateRange == DateRanges.LastCustomDays)
            //    {
            //        _isLastCustom = true;
            //        LastCustomDays = (int)(dateRange.ToDate.Date - dateRange.FromDate.Date).TotalDays + 1;
            //        toDate = DateTime.Now.Date.AddDays(1).AddMilliseconds(-1);
            //        fromDate = GetFromLastCustomDate();
            //        InitializeObj();
            //    }
            //    else
            //    {
            //        if ((DateRanges)dateRange.DateRange != Value)
            //        {
            //            Value = (DateRanges)dateRange.DateRange;
            //        }
            //        // if save date range equal custom no need to update value
            //        updateValue();
            //    }
            //}
            //else
            //{
                // register update value 
                updateValue(true);
            //}
            cbo.SelectedIndexChanged += cbo_SelectedIndexChanged;
        }

        public event EventHandler ValueChanged;

        private DateTime fromDate { get; set; } = DateTime.Now;
        private DateTime toDate { get; set; } = DateTime.Now;
        public bool AutoSave { get; set; } = true;

        [Browsable(false)]
        public DateTime FromDate
        {
            get { return fromDate; }
            set { fromDate = value; }
        }

        [Browsable(false)]
        public DateTime ToDate
        {
            get { return toDate; }
            set { toDate = value; }
        }

        private bool _isLastCustom { get; set; } = false;

        public int LastCustomDays { get; set; } = 30;

        private DateFilterType _dateFilterType;

        public DateFilterType DateFilterTypes
        {
            get { return _dateFilterType; }
            set
            {
                cbo.DisplayMember = value == DateFilterType.AsOfDate ? "AsOfName" : "Name";
                _dateFilterType = value;
            }
        }
        public DateRanges Value
        {
            set
            {
                cbo.SelectedIndex = (int)value;
            }
            get
            {
                return (DateRanges)cbo.SelectedIndex;
            }
        }
        protected virtual void OnValueChanged(object sender, EventArgs e)
        {
            if (ValueChanged != null)
            {
                //if (AutoSave && DesignMode == false)
                //{
                //    // Save
                //    var targetName = this.GetFullName();
                //    Properties.Settings.Default.USER_DATA = Properties.Settings.Default.USER_DATA ?? new Base.Helpers.UISetting();
                //    var setting = Properties.Settings.Default.USER_DATA.DatePickerSettings[targetName];
                //    if (setting == null)
                //    {
                //        Properties.Settings.Default.USER_DATA.DatePickerSettings[targetName] = new DatePickerSetting()
                //        {
                //            Name = targetName,
                //            DateRange = (int)Value,
                //            FromDate = FromDate,
                //            ToDate = ToDate
                //        };
                //    }
                //    else
                //    {
                //        setting.DateRange = (int)Value;
                //        setting.FromDate = FromDate;
                //        setting.ToDate = ToDate;
                //    }
                //    Properties.Settings.Default.Save();
                //}
                ValueChanged(sender, e);
            }
        }
        private void InitializeObj(string chooseText = "")
        {
            var endOf = $"{Resources.END_OF} ";
            var objs = new List<obj>()
            {
                new obj()
                {
                    Id = (int) DateRanges.Today,
                    Name =$"{Resources.TODAY} ({DateTime.Now.ToShortDate()})",
                    AsOfName = $"{endOf}{Resources.TODAY} ({DateTime.Now.ToShortDate()})"
                },
                new obj()
                {
                    Id = (int) DateRanges.ThisWeek,
                    Name = $"{Resources.THIS_WEEK} ({Resources.THIS_WEEK}ទី {GetWeekNumber()}-{DateTime.Now:yyyy})",
                    AsOfName = $"{endOf}{Resources.THIS_WEEK} ({GetPeriod(DateRanges.ThisWeek).Item2.ToShortDate()})"
                },
                new obj()
                {
                    Id = (int) DateRanges.ThisMonth,
                    Name = $"{Resources.THIS_MONTH} ({DateTime.Now:MM-yyyy})",
                    AsOfName = $"{endOf}{Resources.THIS_MONTH} ({GetPeriod(DateRanges.ThisMonth).Item2.ToShortDate()})"
                },
                new obj()
                {
                    Id = (int) DateRanges.ThisQuarter,
                    Name = $"{Resources.THIS_QUARTER} ({GetQuarter(DateTime.Now.Month)} {DateTime.Now:yyyy})",
                    AsOfName = $"{endOf}{Resources.THIS_QUARTER} ({GetPeriod(DateRanges.ThisQuarter).Item2.ToShortDate()})"
                },
                new obj()
                {
                    Id = (int) DateRanges.ThisFiscalYear,
                    Name = $"{Resources.THIS_YEAR} ({DateTime.Now:yyyy})",
                    AsOfName = $"{endOf}{Resources.THIS_YEAR} ({GetPeriod(DateRanges.ThisFiscalYear).Item2.ToShortDate()})"
                },
                    new obj()
                {
                    Id = (int)DateRanges.BreakLine1,
                    Name =     $"-".PadRight(250,'-'),
                    AsOfName = $"-".PadRight(250,'-')
                },
                new obj() // Last week
                {
                    Id = (int) DateRanges.LastWeek,
                    Name = $"{Resources.LAST_WEEK} ({Resources.WEEKLY}ទី {GetWeekNumber()-1}-{DateTime.Now:yyyy})",
                    AsOfName = $"{endOf}{Resources.LAST_WEEK} ({GetPeriod(DateRanges.LastWeek).Item2.ToShortDate()})"
                },
                new obj()
                {
                    Id = (int) DateRanges.LastMonth,
                    Name = $"{Resources.LAST_MONTH} ({DateTime.Now.AddMonths(-1):MM-yyyy})",
                    AsOfName = $"{endOf}{Resources.LAST_MONTH}​ ({GetPeriod(DateRanges.LastMonth).Item2.ToShortDate()})"
                },
                new obj()
                {
                    Id = (int) DateRanges.LastQuarter,
                    Name = $"{Resources.LAST_QUARTER} ({GetQuarter(FirstDayOfLastQuarter().Month)} {DateTime.Now.AddMonths(-3):yyyy})",
                    AsOfName = $"{endOf}{Resources.LAST_QUARTER} ({GetPeriod(DateRanges.LastQuarter).Item2.ToShortDate()})"
                },
                new obj()
                {
                    Id = (int) DateRanges.LastFiscalYear,
                    Name = $"{Resources.LAST_YEAR} ({DateTime.Now.AddYears(-1):yyyy})",
                    AsOfName = $"{endOf}{Resources.LAST_YEAR} ({GetPeriod(DateRanges.LastFiscalYear).Item2.ToShortDate()})"
                },
                      new obj()
                {
                    Id = (int)DateRanges.BreakLine2,
                    Name =     $"-".PadRight(250,'-'),
                    AsOfName = $"-".PadRight(250,'-')
                },

                new obj()
                {
                    Id = (int) DateRanges.LastCustomDays,
                    Name = $"{string.Format(Properties.Resources.LAST_DAY, LastCustomDays)} ({fromDate.ToShortDate()})",
                    AsOfName = $"{endOf}{string.Format(Properties.Resources.LAST_DAY, LastCustomDays)} ({DateTime.Now.ToShortDate()})"
                },
                new obj()
                {
                    Id = (int) DateRanges.Custom,
                    Name = string.IsNullOrEmpty(chooseText) ? Resources.CHOOSE : chooseText,
                    AsOfName = string.IsNullOrEmpty(chooseText) ? Resources.CHOOSE : chooseText,
                }
            };

            if (!Today)
            {
                objs = objs.Where(x => (DateRanges)x.Id != DateRanges.Today).ToList();
            }
            if (!ThisWeek)
            {
                objs = objs.Where(x => (DateRanges)x.Id != DateRanges.ThisWeek).ToList();
            }
            if (!ThisMonth)
            {
                objs = objs.Where(x => (DateRanges)x.Id != DateRanges.ThisMonth).ToList();
            }
            if (!ThisQuarter)
            {
                objs = objs.Where(x => (DateRanges)x.Id != DateRanges.ThisQuarter).ToList();
            }
            if (!ThisFinancialYear)
            {
                objs = objs.Where(x => (DateRanges)x.Id != DateRanges.ThisFiscalYear).ToList();
            }

            if (!LastWeek)
            {
                objs = objs.Where(x => (DateRanges)x.Id != DateRanges.LastWeek).ToList();
            }
            if (!LastMonth)
            {
                objs = objs.Where(x => (DateRanges)x.Id != DateRanges.LastMonth).ToList();
            }
            if (!LastQuarter)
            {
                objs = objs.Where(x => (DateRanges)x.Id != DateRanges.LastQuarter).ToList();
            }
            if (!LastFinancialYear)
            {
                objs = objs.Where(x => (DateRanges)x.Id != DateRanges.LastFiscalYear).ToList();
            }

            //* check break line 1;*/
            if ((Today || ThisWeek || ThisMonth || ThisQuarter || ThisFinancialYear) && (Yesterday || LastWeek || LastMonth || LastQuarter || LastFinancialYear) == false)
            {
                objs = objs.Where(x => (DateRanges)x.Id != DateRanges.BreakLine1).ToList();
            }

            //break line2;
            if ((Today || ThisWeek || ThisMonth || ThisQuarter || ThisFinancialYear || Yesterday || LastWeek || LastMonth || LastQuarter || LastFinancialYear) == false)
            {
                objs = objs.Where(x => (DateRanges)x.Id != DateRanges.BreakLine2).ToList();
            }

            cbo.DisplayMember = "Name";
            if (DateFilterTypes == DateFilterType.AsOfDate)
            {
                cbo.DisplayMember = "AsOfName";
            }
            cbo.ValueMember = "Id";
            cbo.DataSource = objs;
            if (_isCustomBinding)
            {
                cbo.SelectedValue = (int)DateRanges.Custom;
                fromDate = oldCustomFromDate;
                toDate = oldCustomToDate;
                _isCustomBinding = false;
            }
            else if (_isLastCustom)
            {
                cbo.SelectedValue = (int)DateRanges.LastCustomDays;
                _isLastCustom = false;
            }
            else
            {
                cbo.SelectedValue = (int)DateRanges.Today;
            }
        }


        public enum DateRanges
        {
            Today = 0,
            ThisWeek,
            ThisMonth,
            ThisQuarter,
            ThisFiscalYear,
            BreakLine1,
            LastWeek,
            LastMonth,
            LastQuarter,
            LastFiscalYear,
            BreakLine2,
            LastCustomDays,
            Custom
        }
        public enum DateFilterType
        {
            Period,
            AsOfDate
        }
        public class obj
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string AsOfName { get; set; }
        }

        public Tuple<DateTime, DateTime> GetPeriod(DateRanges range)
        {
            var d1 = DateTime.Now;
            var d2 = DateTime.Now;
            switch (range)
            {
                case DateRanges.Today:
                    d1 = DateTime.Now.Date;
                    d2 = DateTime.Now.Date/*.AddDays(1).AddSeconds(-1)*/;
                    break;
                case DateRanges.ThisWeek:
                    d1 = FirstDayOfWeek(DateTime.Now);
                    d2 = LastDayOfWeek(DateTime.Now);
                    break;
                case DateRanges.ThisMonth:
                    d1 = FirstDayOfMonth(DateTime.Now);
                    d2 = LastDayOfMonth(DateTime.Now);
                    break;
                case DateRanges.ThisQuarter:
                    d1 = FirstDayOfQuarter();
                    d2 = LastDayOfQuarter();
                    break;
                case DateRanges.ThisFiscalYear:
                    d1 = FirstDayOfYear();
                    d2 = LastDayOfYears();
                    break;
                case DateRanges.LastWeek:
                    d1 = FirstDayOfLastWeek(DateTime.Now);
                    d2 = LastDayOfLastWeek(DateTime.Now);
                    break;
                case DateRanges.LastMonth:
                    d1 = FirstDayOfLastMonth();
                    d2 = LastDayOfLastMonth();
                    break;
                case DateRanges.LastQuarter:
                    d1 = FirstDayOfLastQuarter();
                    d2 = LastDayOfLastQuarter();
                    break;
                case DateRanges.LastFiscalYear:
                    d1 = FirstDayOfLastYear();
                    d2 = LastDayOfLastYears();
                    break;
                case DateRanges.LastCustomDays:
                    d2 = DateTime.Now.Date.AddDays(1).AddMilliseconds(-1);
                    d1 = GetFromLastCustomDate();
                    break;
            }
            return new Tuple<DateTime, DateTime>(d1.Date, d2.Date.AddDays(1).AddSeconds(-1));
        }
        private void cbo_SelectedIndexChanged(object sender, EventArgs e)
        {
            var range = (DateRanges)Dynamic.Helpers.Parse.ToInt(cbo.SelectedValue?.ToString() ?? "-1");
            if (range == DateRanges.BreakLine2 || range == DateRanges.BreakLine1)
            {
                _isCustomBinding = true;
                this.Value = PreviousValue;
                _isCustomBinding = false;
                return;
            }
            if (!_isCustomBinding && !_isLastCustom)
            {
                updateValue();
                OnValueChanged(sender, e);
            }
        }
        private void updateValue(bool ignoreLastCustomDaysDialog = false)
        {

            var range = (DateRanges)Dynamic.Helpers.Parse.ToInt(cbo.SelectedValue?.ToString() ?? "-1");
            if (range == DateRanges.Custom)
            {
                if (DateFilterTypes == DateFilterType.AsOfDate)
                {
                    var dig = new DialogAsOfDatePicker(ToDate);
                    if (dig.ShowDialog() != DialogResult.OK) return;
                    fromDate = dig.FromDate.Date;
                    toDate = oldCustomToDate = dig.ToDate.Date.AddDays(1).AddSeconds(-1);
                    //_isNeedRefresh = false;
                    _isCustomBinding = true;
                    InitializeObj($"{Resources.AsOfDate} {toDate.ToShortDate()}");
                    //cbo.SelectedValue = (int)DateRanges.Custom;

                }
                else
                {
                    var dig = new DialogPeriodDatePicker(oldCustomFromDate, oldCustomToDate);
                    if (dig.ShowDialog() != DialogResult.OK) return;
                    fromDate = oldCustomFromDate = dig.FromDate.Date;
                    toDate = oldCustomToDate = dig.ToDate.Date.AddDays(1).AddSeconds(-1);
                    //_isNeedRefresh = false;
                    _isCustomBinding = true;
                    InitializeObj($"{fromDate.ToShortDate()} {Resources.TO} {toDate.ToShortDate()}");
                    //cbo.SelectedValue = (int)DateRanges.Custom;
                }
            }
            else if (range == DateRanges.LastCustomDays)
            {
                if (!DesignMode)
                {
                    if (!ignoreLastCustomDaysDialog)
                    {
                        var dig = new DialogPeriodDatePicker(GetFromLastCustomDate(), DateTime.Now);
                        dig.EnabledToDate = false;
                        dig.MaxFromDate = DateTime.Now;
                        if (dig.ShowDialog() != DialogResult.OK) return;
                        fromDate = dig.FromDate.Date;
                        toDate = dig.ToDate.Date.AddDays(1).AddSeconds(-1);
                        LastCustomDays = (int)(toDate.Date - fromDate.Date).TotalDays + 1;
                        //_isNeedRefresh = false;
                        _isLastCustom = true;
                        InitializeObj();
                    }
                    else
                    {
                        var period = GetPeriod(range);
                        fromDate = period.Item1.Date;
                        toDate = period.Item2.Date.AddDays(1).AddSeconds(-1);
                    }
                }
            }
            else
            {
                var period = GetPeriod(range);
                fromDate = period.Item1.Date;
                toDate = period.Item2.Date.AddDays(1).AddSeconds(-1);
                //_isNeedRefresh = false;
                //InitializeObj();
                //cbo.SelectedIndex = (int)range;
            }
        }

        public int GetWeekNumber()
        {
            CultureInfo ciCurr = new CultureInfo("en-GB");
            int weekNum = ciCurr.Calendar.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            return weekNum;
        }

        // GetQuarter
        public string GetQuarter(int month)
        {
            var quarter = string.Empty;
            if (month >= 1 && month <= 3)
                quarter = "Q1";
            if (month >= 4 && month <= 6)
                quarter = "Q2";
            if (month >= 7 && month <= 9)
                quarter = "Q3";
            if (month >= 10 && month <= 12)
                quarter = "Q4";
            return quarter;
        }

        // Week
        public static DateTime FirstDayOfWeek(DateTime date)
        {
            DayOfWeek fdow = FormatHelper.CurrentCulture.DateTimeFormat.FirstDayOfWeek;
            int offset = GetDayOfWeekNumber(fdow, fdow) - GetDayOfWeekNumber(date.DayOfWeek, fdow);
            DateTime fdowDate = date.AddDays(offset);
            return fdowDate;
        }

        private static int GetDayOfWeekNumber(DayOfWeek dayOfWeek, DayOfWeek firstDayOfWeek)
        {
            var val = DayOfWeek.Sunday - firstDayOfWeek;
            var day = (int)(dayOfWeek) + val;
            if (day < 0)
            {
                return 7 + day;
            }
            else
            {
                return day;
            }
        }

        public static DateTime FirstDayOfLastWeek(DateTime date)
        {
            DateTime mondayOfLastWeek = date.AddDays(-(int)date.DayOfWeek - 6);
            return mondayOfLastWeek;
        }
        public static DateTime LastDayOfWeek(DateTime date)
        {
            DateTime ldowDate = FirstDayOfWeek(date).AddDays(6);
            return ldowDate;
        }
        public static DateTime LastDayOfLastWeek(DateTime date)
        {
            DateTime ldowDate = FirstDayOfLastWeek(date).AddDays(6);
            return ldowDate;
        }

        //Month
        public DateTime FirstDayOfMonth(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, 1);
        }
        public DateTime LastDayOfMonth(DateTime dateTime)
        {
            DateTime firstDayOfTheMonth = new DateTime(dateTime.Year, dateTime.Month, 1);
            return firstDayOfTheMonth.AddMonths(1).AddDays(-1);
        }
        public DateTime FirstDayOfLastMonth()
        {
            return DateTime.Now.AddDays(1 - DateTime.Now.Day).AddMonths(-1);
        }
        public DateTime LastDayOfLastMonth()
        {
            DateTime firstDayLastMonth = DateTime.Now.AddDays(1 - DateTime.Now.Day).AddMonths(-1);
            return new DateTime(firstDayLastMonth.Year, firstDayLastMonth.Month, DateTime.DaysInMonth(firstDayLastMonth.Year, firstDayLastMonth.Month));
        }

        //Quarter
        public DateTime FirstDayOfQuarter()
        {
            var date = DateTime.Now;
            var quarterNumber = (date.Month - 1) / 3 + 1;
            return new DateTime(date.Year, (quarterNumber - 1) * 3 + 1, 1);
        }
        public DateTime LastDayOfQuarter()
        {
            return FirstDayOfQuarter().AddMonths(3).AddDays(-1);
        }
        public DateTime FirstDayOfLastQuarter()
        {
            return FirstDayOfQuarter().AddMonths(-3);
        }
        public DateTime LastDayOfLastQuarter()
        {
            return FirstDayOfLastQuarter().AddMonths(3).AddDays(-1);
        }

        // Financial Years
        public DateTime FirstDayOfYear()
        {
            return new DateTime(DateTime.Now.Year, 1, 1);
        }
        public DateTime LastDayOfYears()
        {
            return new DateTime(DateTime.Now.Year, 12, 31);
        }
        public DateTime FirstDayOfLastYear()
        {
            return new DateTime(DateTime.Now.Year - 1, 1, 1);
        }
        public DateTime LastDayOfLastYears()
        {
            return new DateTime(DateTime.Now.Year - 1, 12, 31);
        }

        //
        public DateTime GetFromLastCustomDate()
        {
            return ToDate.Date.AddDays((-1) * (LastCustomDays - 1));
        }

        protected override void OnFontChanged(EventArgs e)
        {
            cbo.Font = this.Font;
            base.OnFontChanged(e);
        }

        private void cbo_Click(object sender, EventArgs e)
        {
            PreviousValue = (DateRanges)Dynamic.Helpers.Parse.ToInt(cbo.SelectedValue?.ToString() ?? "-1");
        }
    }

    public static class UISettingExt
    {
        public static string GetFullName(this TreeGridView tgv)
        {
            return $"{GetLastParent(tgv).Name}.{tgv.Name}";
        }

        public static string GetFullName(this DataGridView dgv)
        {
            return $"{GetLastParent(dgv).Name}.{dgv.Name}";
        }

        public static string GetFullName(this ReportDatePicker dp)
        {
            return $"{GetLastParent(dp)?.Name}.{dp.Name}";
        }

        public static Control GetLastParent(Control child)
        {
            return child.FindForm();
        }
    }
}
