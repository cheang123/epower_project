﻿using EPower.Base.Properties;
using SoftTech.Helper;
using System;
using System.Windows.Forms;

namespace EPower.Base.Helper.Component
{
    public partial class DialogAsOfDatePicker : SoftTech.Component.ExDialog
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }

        #region Constructor
        public DialogAsOfDatePicker(DateTime d1)
        {
            InitializeComponent();
            ResourceHelper.SecondaryResourceManager = new System.Collections.Generic.List<System.Resources.ResourceManager>() { Resources.ResourceManager };
            ResourceHelper.ApplyResource(this);
            dtpFromDate.FormatSortDate();
        }
        #endregion

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            FromDate = dtpFromDate.Value;
            ToDate = dtpFromDate.Value;
            DialogResult = DialogResult.OK;
        }
    }
}
