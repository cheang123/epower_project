﻿using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPivotGrid;
using DevExpress.XtraTreeList.Columns;
using EPower.Base.Logic;
using EPower.Base.Properties;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Base.Helper
{
    public static class DataHelperExtension
    {
        public static void FormatByCurrency(this DataGridViewColumn col, int CurrencyId)
        {
            var defaultCurrency = SettingLogic.Currencies.FirstOrDefault(x => x.IS_DEFAULT_CURRENCY);
            var currency = SettingLogic.Currencies.FirstOrDefault(x => x.CURRENCY_ID == CurrencyId);
            if (currency == null)
            {
                col.DefaultCellStyle.Format = defaultCurrency.FORMAT;
            }
            else
            {
                col.DefaultCellStyle.Format = currency.FORMAT;
            }
        }
    }
    public static class DevFormatHelper
    {
        public static string NumberFormat = "#,##0.00;-#,##0.00;0.00";
        public static string PriceFormat = "#,##0.00##;-#,##0.00##;0.00";
        public static string PriceDisplayFormat = "#,##0.00##;-#,##0.00##;-";
        public static string FormatExchangeRate = "#,##0.00#####;-#,##0.00#####;-";
        public static string ShortDate => "dd-MM-yyyy";
        public static string LongDate => "dd-MM-yyyy hh:mm:ss tt";
        public static string Time => "hh:mm:ss tt";

        public static string ToDevShortDate(this DateTime d)
        {
            return d.ToString(ShortDate);
        }
        public static string ToLongDate(this DateTime d)
        {
            return d.ToString(LongDate);
        }
        public static void FormatShortDate(this GridColumn col)
        {
            col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            col.DisplayFormat.FormatString = ShortDate;
            col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            col.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        }
        public static void FormatShortDate(this BandedGridColumn col)
        {
            col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            col.DisplayFormat.FormatString = ShortDate;
            col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            col.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        }
        public static void FormatSortDate(this DateEdit dtp)
        {
            dtp.Properties.DisplayFormat.FormatString = "dd-MM-yyyy";
            dtp.Properties.EditFormat.FormatString = "dd-MM-yyyy";
        }
        public static void FormatShortDate(this GridBand col)
        {
            col.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        }
        public static void FormatShortDate(this PivotGridField col)
        {
            if (col.GroupInterval == PivotGroupInterval.DateHour || col.GroupInterval == PivotGroupInterval.DateHourMinute
                || col.GroupInterval == PivotGroupInterval.DateHourMinuteSecond || col.GroupInterval == PivotGroupInterval.DateDayOfYear
                || col.GroupInterval == PivotGroupInterval.Date || col.GroupInterval == PivotGroupInterval.DateWeekYear
                || col.GroupInterval == PivotGroupInterval.Default || col.GroupInterval == PivotGroupInterval.DateWeekOfYear)
            {
                col.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                col.ValueFormat.FormatString = ShortDate;

                col.Appearance.Value.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                col.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

                col.Appearance.CellGrandTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                col.Appearance.CellTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                col.Appearance.ValueGrandTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                col.Appearance.ValueTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            }
            else if (col.GroupInterval == PivotGroupInterval.DateMonth)
            {
                col.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                col.ValueFormat.FormatString = "MM";

                col.Appearance.Value.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                col.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

                col.Appearance.CellGrandTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                col.Appearance.CellTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                col.Appearance.ValueGrandTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                col.Appearance.ValueTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            }
            else if (col.GroupInterval == PivotGroupInterval.DateMonthYear)
            {
                col.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                col.ValueFormat.FormatString = "MM-yyyy";
                col.Appearance.Value.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                col.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

                col.Appearance.CellGrandTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                col.Appearance.CellTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                col.Appearance.ValueGrandTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                col.Appearance.ValueTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            }
            else if (col.GroupInterval == PivotGroupInterval.DateYear)
            {
                col.ValueFormat.FormatType = DevExpress.Utils.FormatType.None;
                col.ValueFormat.FormatString = "";
                col.Appearance.Value.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                col.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

                col.Appearance.CellGrandTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                col.Appearance.CellTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                col.Appearance.ValueGrandTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                col.Appearance.ValueTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            }
            else if (col.GroupInterval == PivotGroupInterval.DateQuarterYear)
            {
                col.ValueFormat.FormatType = DevExpress.Utils.FormatType.None;
                col.ValueFormat.FormatString = string.Empty;
                col.Appearance.Value.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                col.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

                col.Appearance.CellGrandTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                col.Appearance.CellTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                col.Appearance.ValueGrandTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                col.Appearance.ValueTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            }
            else
            {
                col.ValueFormat.FormatType = DevExpress.Utils.FormatType.None;
                col.ValueFormat.FormatString = "";
                col.Appearance.Value.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                col.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                col.Appearance.CellGrandTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                col.Appearance.CellTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                col.Appearance.ValueGrandTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                col.Appearance.ValueTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;

            }
        }
        public static void FormatTime(this PivotGridField col)
        {
            col.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            col.CellFormat.FormatString = Time;
            col.Appearance.Cell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            col.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        }
        public static void FormatViewNumeric(this PivotGridField col)
        {
            col.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            col.CellFormat.FormatString = PriceFormat;
            col.Appearance.Cell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            col.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
        }
        public static void FormatNumeric(this PivotGridField col)
        {
            col.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            col.CellFormat.FormatString = "#,##0;-#,##0;0";
            col.Appearance.Cell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            col.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
        }
        public static void FormatNumeric(this GridColumn col)
        {
            col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            col.DisplayFormat.FormatString = "#,###.###;0";
            col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            col.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
        }
    }
}
