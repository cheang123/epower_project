﻿using DevExpress.XtraEditors;
using EPower.Base.Helper;
using EPower.Base.Helper.DevExpressCustomize;
using EPower.Base.Logic;
using EPower.Interface;
using EPower.Logic;
using EPower.Properties;
using EPower.Update;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.ServiceProcess;
using System.Threading;
using System.Windows.Forms;

namespace EPower
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

           //test

            //var sending_email = new SendingEmailLogic();
            //sending_email.SendEmail(new SendingEmailModel()
            //{
            //     EmailToId  = "thunnynt@gmail.com",
            //     EmailToName  = "Thunny",
            //     EmailBody  = "test",
            //     EmailSubject  = "subject",
            //     EmailAttachments = @"D:\Bill24\Technical skill\test.zip"
            //});

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            WindowsFormsSettings.DefaultLookAndFeel.SetSkinStyle("DevExpress Style");
            WindowsFormsSettings.DefaultFont = new System.Drawing.Font("Khmer Kep", 9);

            try
            {
                //set auto detect and start SQL Server
                var builder = new SqlConnectionStringBuilder(Method.GetConnectionString(Settings.Default.CONNECTION));
                var resource = builder.DataSource;
                var hostName = resource.Split(new string[] { "\\" }, StringSplitOptions.RemoveEmptyEntries)[0];
                var domain = Dns.GetHostEntry(hostName.Replace(".", "")).HostName;

                var controller = new ServiceController("MSSQL$SQLEXPRESS", domain);
                if (domain == Environment.MachineName)
                {

                    if (controller.Status != ServiceControllerStatus.Running)
                    {
                        if (!IsRunasAdmin())
                        {
                            Elevate();
                            Environment.Exit(0);
                            return;
                        }
                        controller.Start();
                    }
                }

                var services = ServiceController.GetServices().Where(x => x.ServiceName.Contains("Pointer") || x.ServiceName == "MSDTC" || x.ServiceName == "WcesComm");
                foreach (var service in services)
                {
                    if (service.Status != ServiceControllerStatus.Running)
                    {
                        try
                        {
                            if (!IsRunasAdmin())
                            {
                                Elevate();
                                Environment.Exit(0);
                                return;
                            }
                            service.Start();
                        }
                        catch
                        {

                        }
                    }
                }
            }
            catch
            {

            }
            // set connection string
            try
            {
                var s = Settings.Default.CONNECTION;
                DBDataContext.SetConnectionString(Method.GetConnectionString(Settings.Default.CONNECTION));
            }
            catch (Exception ex)
            {
                MsgBox.ShowWarning(Resources.MS_SYSTEM_CANNOT_CONNECT_TO_SERVER, Resources.WARNING);
                Environment.Exit(0);
            }

            // check for update if possible.
            new Updater().CheckUpdate();


            try
            {
                //Install additional application
                AutoInstallAditionalFileLogic.DeployMSIFileWithCommandline32(Application.StartupPath + @"\install_crystal32.bat");
                AutoInstallAditionalFileLogic.DeployMSIFileWithCommandline64(Application.StartupPath + @"\install_crystal64.bat");
                AutoInstallAditionalFileLogic.DeployREGFileWithCommandline(Application.StartupPath + @"\fix_small_font.bat");

            }
            catch (Exception ex)
            {

            } 
            //submit usage. 
            SUBMIT_PENDING_USAGE();
            //initiallize mail server
            // decrypt urlity value mail server 
            WebHelper.MailServiceURL = "http://172.16.9.128:9988/v1/Email/SendEmailWithAttachment"; 

            //remove if have duplicated usage
            BillLogic.RemoveDuplicatedUsage();

            try
            {
                var url = DBDataContext.Db.TBL_UTILITies.FirstOrDefault(x => x.UTILITY_ID == (int)Utility.MAIL_SERVER).UTILITY_VALUE;
                var _url = Cryption.Cryption.EncryptString("http://amr.e-power.com.kh:9988/v1/Email/SendEmailWithAttachment");
                WebHelper.MailServiceURL = Cryption.Cryption.DecryptString(url);

            } catch (Exception ex)
            {
                MsgBox.LogError(ex);
            }

            // check for datetime vs last operation date
            var objAudit = DBDataContext.Db.TBL_AUDITTRIALs.OrderByDescending(x => x.AUDIT_TRIAL_ID).FirstOrDefault();
            var now = DBDataContext.Db.GetSystemDate();
            var lastOperationDate = now;
            if (objAudit != null)
            {
                lastOperationDate = objAudit.AUDIT_DATE;
            }
            if (lastOperationDate > now)
            {
                MsgBox.ShowWarning(string.Format(Resources.MS_SYSTEM_DATETIME_NOT_CORRECT, lastOperationDate, now), "");
                return;
            }

            // Change globale localization.  
            var cult = new CultureInfo(Settings.Default.LANGUAGE);
            Thread.CurrentThread.CurrentUICulture = cult;
            Thread.CurrentThread.CurrentCulture = cult;


            // Apply resource to devexpress
            if (cult.Name == "")
            {
                DevExpress.XtraEditors.Controls.Localizer.Active = new KHEditorsLocalizer();
                DevExpress.XtraGrid.Localization.GridLocalizer.Active = new KHGridLocalizer();
                DevExpress.Utils.Filtering.Internal.FilterUIElementResXLocalizer.Active = new CustomFilterUIElementResXLocalizer();
            }

            //Get utilites
            var k = Method.Utilities;

            ResourceHelper.SecondaryResourceManager.Add(Base.Properties.Resources.ResourceManager);

            // Crystal report setting.
            string path = string.Format(Settings.Default.PATH_REPORT, Settings.Default.LANGUAGE == "en-US" ? "" : "." + Settings.Default.LANGUAGE);
            CrystalReportHelper.SetDefaultLogonInfo(Method.GetConnectionString(Settings.Default.CONNECTION));
            CrystalReportHelper.SetDefaultReportPath(path);

            // create directory if not have!
            if (!Directory.Exists("MobileDB"))
            {
                Directory.CreateDirectory("MobileDB");
            }
            if (!Directory.Exists(Settings.Default.PATH_TEMP))
            {
                Directory.CreateDirectory(Settings.Default.PATH_TEMP);
            }

            ///CRMService.StartTrackMeterInNewThread();

            //if (Method.GetUtilityValue(Utility.EBILL_SUPPLIER_AUTO_UPLOAD)=="1")
            //{
            //    new EBillConnect().RunBackground();
            //}

            if (new DialogAuthenticate().ShowDialog() == DialogResult.OK)
            {
                Application.Run(new FormMain());
            }
            else
            {
                /*
                 * Clean process when reject authentication form.
                 */
                Environment.Exit(0);
            }

        }

        //set auto detect and start SQL Server
        private static bool IsRunasAdmin()
        {
            var role = new WindowsPrincipal(WindowsIdentity.GetCurrent());
            return role.IsInRole(WindowsBuiltInRole.Administrator);
        }
        private static void SUBMIT_PENDING_USAGE()
        {
            string ConnectionString = Method.GetConnectionString(Settings.Default.CONNECTION);
            using (var cnn = new SqlConnection(ConnectionString))
            {
                if (cnn.State == ConnectionState.Closed)
                {
                    cnn.Open();
                }
                using (var cmd = new SqlCommand("dbo.SUBMIT_PENDING_USAGE", cnn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;
                    cmd.ExecuteNonQuery();
                }
            }
        } 
        private static bool Elevate()
        {
            var selfProcess = new ProcessStartInfo
            {
                UseShellExecute = true,
                WorkingDirectory = Environment.CurrentDirectory,
                FileName = Application.ExecutablePath,
                Verb = "runas"
            };

            try
            {
                System.Diagnostics.Process.Start(selfProcess);
                return true;
            }
            catch
            {
                return false;
            }
        }

    }

}