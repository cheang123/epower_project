﻿namespace EPower.Update
{
    class Version7_1_4_0 : Version
    {
        protected override string GetBatchCommand()
        {
            /* 
                * @Vonn Kimputhmunyvorn 2022-08-03
                * 1.Fixed URl send email
            */
            return @"
UPDATE dbo.TBL_UTILITY
SET UTILITY_VALUE = 'DHMaYktmqkjVsjgH+SUpeQrz0f6otQU5znmvnLLw/zNfr52yCCBrVe6E+zjt+v1NiPul9Ym8I7eLul8dWQi05Q=='
WHERE UTILITY_ID = 111
GO

IF NOT EXISTS(SELECT TOP 1 * FROM TBL_UTILITY WHERE UTILITY_ID = 109)
    INSERT INTO TBL_UTILITY
    SELECT 109, N'UPDATE_SERVICE_URL', 'UPDATE_SERVICE_URL', N'http://amr.e-power.com.kh:1115'
GO
";
        }
    }
}
