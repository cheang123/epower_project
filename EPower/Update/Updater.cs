﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using System;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Update
{
    class Updater
    {
        public void CheckUpdate()
        {
            // Update Version Process
            // 2011-06-27 Rith 
            try
            {
                UpdateVersion(); // for old version
                new Version7_0_0_1().CheckUpdate();
                new Version7_0_1_0().CheckUpdate();
                new Version7_0_1_1().CheckUpdate();
                new Version7_0_2_0().CheckUpdate();
                new Version7_0_2_1().CheckUpdate();
                new Version7_0_2_2().CheckUpdate();
                new Version7_0_3_0().CheckUpdate();
                new Version7_0_3_1().CheckUpdate();
                new Version7_0_3_2().CheckUpdate();
                new Version7_1_0_0().CheckUpdate();
                new Version7_1_1_0().CheckUpdate();
                new Version7_1_2_0().CheckUpdate();
                new Version7_1_2_6().CheckUpdate();
                new Version7_1_3_0().CheckUpdate();
                new Version7_1_3_1().CheckUpdate();
                new Version7_1_3_2().CheckUpdate();
                new Version7_1_3_7().CheckUpdate();
                new Version7_1_4_0().CheckUpdate();
                new Version7_1_4_3().CheckUpdate();
                // if .exe version is lower than database version
                // lock user to run e-power
                string dbVersion = Method.Utilities[Utility.VERSION];
                if (Version.ConvertToLong(Version.ClientVersion) < Version.ConvertToLong(dbVersion))
                {
                    if (MsgBox.ShowQuestion(string.Format(Resources.MS_PLEASE_CONNECT_INTERNET_TO_UPDATE_SYSTEM, dbVersion), "") != DialogResult.Yes)
                    {
                        Environment.Exit(0);
                    }
                    if (new DialogUpdate(dbVersion).ShowDialog() != DialogResult.OK)
                    {
                        Environment.Exit(0);
                    }
                }
                //Reset E-Power Utilities
                Method.utilities = null;
            }
            catch (Exception ex)
            {
                ///MsgBox.ShowWarning(string.Format(Resources.YOU_CANNOT_PROCESS,"",Resources.UPDATE_NEW_VERSION),Resources.WARNING);
                MsgBox.LogError(ex);
            }
        }
        public bool IsRequiredVersion(string version)
        {
            return parseVersion(GetCurrentVersion()) < parseVersion(version);
        }
        public string GetCurrentVersion()
        {
            TBL_UTILITY objConf = DBDataContext.Db.TBL_UTILITies.FirstOrDefault(row => row.UTILITY_ID == (int)Utility.VERSION);
            if (objConf == null)
            {
                return "0.0.0.0";
            }
            else
            {
                return objConf.UTILITY_VALUE;
            }
        }
        private long parseVersion(string version)
        {
            string tmp = "0";
            foreach (string s in version.Split(new char[] { '.' }))
            {
                tmp += s.PadLeft(3, '0');
            }
            return long.Parse(tmp);
        }

        private void UpdateVersion()
        {
            if (IsRequiredVersion("1.0.0.0"))
            {
                Runner.RunNewThread(delegate ()
                {
                    new Version1_0_0_0().Update();
                }, "Updating...");
            }
            if (IsRequiredVersion("1.0.0.1"))
            {
                Runner.RunNewThread(delegate ()
                {
                    new Version1_0_0_1().Update();
                }, "Updating...");
            }
            if (IsRequiredVersion("1.0.0.2"))
            {
                Runner.RunNewThread(delegate ()
                {
                    new Version1_0_0_2().Update();
                }, "Updating...");
            }
            if (IsRequiredVersion("1.0.0.3"))
            {
                Runner.RunNewThread(delegate ()
                {
                    new Version1_0_0_3().Update();
                }, "Updating...");
            }
            if (IsRequiredVersion("1.0.0.4"))
            {
                Runner.RunNewThread(delegate ()
                {
                    new Version1_0_0_4().Update();
                }, "Updating...");
            }
            if (IsRequiredVersion("1.0.0.5"))
            {
                Runner.RunNewThread(delegate ()
                {
                    new Version1_0_0_5().Update();
                }, "Updating...");
            }
            new Version1_0_0_6().CheckUpdate();
            new Version1_0_0_7().CheckUpdate();
            new Version1_0_0_8().CheckUpdate();
            new Version1_0_0_9().CheckUpdate();
            new Version1_0_1_1().CheckUpdate();
            new Version1_0_1_2().CheckUpdate();
            new Version1_0_1_3().CheckUpdate();
            new Version1_0_1_4().CheckUpdate();
            new Version1_0_1_5().CheckUpdate();
            new Version1_0_1_6().CheckUpdate();
            new Version1_0_1_7().CheckUpdate();
            new Version1_0_1_8().CheckUpdate();
            new Version1_0_1_9().CheckUpdate();
            new Version1_0_2_0().CheckUpdate();
            new Version1_0_2_1().CheckUpdate();
            new Version1_0_2_2().CheckUpdate();
            new Version1_0_2_3().CheckUpdate();
            new Version1_0_2_4().CheckUpdate();
            new Version1_0_2_5().CheckUpdate();
            new Version1_0_2_6().CheckUpdate();
            new Version1_0_2_7().CheckUpdate();
            new Version1_0_2_8().CheckUpdate();
            new Version1_0_2_9().CheckUpdate();
            new Version1_0_3_0().CheckUpdate();
            new Version1_0_3_1().CheckUpdate();
            new Version1_0_3_2().CheckUpdate();
            new Version1_0_3_3().CheckUpdate();
            new Version1_0_3_4().CheckUpdate();
            new Version1_0_3_5().CheckUpdate();
            new Version1_0_3_6().CheckUpdate();
            new Version1_0_3_7().CheckUpdate();
            new Version1_0_3_8().CheckUpdate();
            new Version1_0_3_9().CheckUpdate();
            new Version1_0_4_0().CheckUpdate();
            new Version1_0_4_1().CheckUpdate();
            new Version1_0_4_2().CheckUpdate();
            new Version1_0_4_3().CheckUpdate();
            new Version1_0_4_4().CheckUpdate();
            new Version1_0_4_5().CheckUpdate();
            new Version1_0_4_6().CheckUpdate();
            new Version1_0_4_7().CheckUpdate();
            new Version1_0_4_8().CheckUpdate();
            new Version1_0_4_9().CheckUpdate();
            // v 1.0.5.0 is not available for update.
            new Version1_0_5_1().CheckUpdate();
            new Version1_0_5_2().CheckUpdate();
            new Version1_0_5_3().CheckUpdate();
            new Version1_0_5_4().CheckUpdate();
            new Version1_0_5_5().CheckUpdate();
            new Version1_0_5_6().CheckUpdate();
            new Version1_0_5_7().CheckUpdate();
            new Version1_0_5_8().CheckUpdate();
            new Version1_0_5_9().CheckUpdate();
            new Version1_0_6_0().CheckUpdate();
            new Version1_0_6_1().CheckUpdate();
            new Version1_0_6_2().CheckUpdate();
            new Version1_0_6_3().CheckUpdate();
            new Version1_0_6_4().CheckUpdate();
            new Version1_0_6_5().CheckUpdate();
            new Version1_0_6_6().CheckUpdate();
            new Version1_0_6_7().CheckUpdate();
            new Version1_0_6_8().CheckUpdate();
            new Version1_0_6_9().CheckUpdate();
            new Version1_0_7_0().CheckUpdate();
            new Version1_0_7_1().CheckUpdate();
            new Version1_0_7_2().CheckUpdate();
            new Version1_0_7_3().CheckUpdate();
            new Version1_0_7_4().CheckUpdate();
            new Version1_0_7_5().CheckUpdate();
            new Version1_0_7_6().CheckUpdate();
            new Version1_0_7_7().CheckUpdate();
            new Version1_0_7_8().CheckUpdate();
            new Version1_0_7_9().CheckUpdate();
            new Version1_0_8_0().CheckUpdate();
            new Version1_0_8_1().CheckUpdate();
            new Version1_0_8_2().CheckUpdate();
            new Version1_0_8_3().CheckUpdate();
            new Version1_0_8_4().CheckUpdate();
            new Version1_0_8_5().CheckUpdate();
            new Version1_0_8_6().CheckUpdate();
            new Version1_0_8_7().CheckUpdate();
            new Version1_0_8_8().CheckUpdate();
            new Version1_0_8_9().CheckUpdate();
            new Version1_0_9_0().CheckUpdate(); // RUN_BILL,GET_MONTHLY_USAGE
            new Version1_0_9_1().CheckUpdate();
            new Version1_0_9_2().CheckUpdate();
            new Version1_0_9_3().CheckUpdate();
            new Version1_0_9_4().CheckUpdate();
            new Version1_0_9_5().CheckUpdate();
            new Version1_0_9_6().CheckUpdate();
            new Version1_0_9_7().CheckUpdate();
            new Version1_0_9_8().CheckUpdate();
            new Version1_0_9_9().CheckUpdate();
            new Version1_1_0_0().CheckUpdate();
            new Version1_1_0_1().CheckUpdate();
            new Version1_1_0_2().CheckUpdate();
            new Version1_1_0_3().CheckUpdate();
            new Version1_1_0_4().CheckUpdate();
            new Version1_1_0_5().CheckUpdate();
            new Version1_1_0_6().CheckUpdate();
            new Version1_1_0_7().CheckUpdate();
            new Version1_1_0_8().CheckUpdate();
            new Version1_1_0_9().CheckUpdate();
            new Version1_1_1_0().CheckUpdate();
            new Version1_1_1_1().CheckUpdate();
            new Version1_1_1_2().CheckUpdate();
            new Version1_1_1_3().CheckUpdate();
            new Version1_1_1_4().CheckUpdate();
            new Version1_1_1_5().CheckUpdate();
            new Version1_1_1_6().CheckUpdate(); // Fix RUN_IR_USAGE
            new Version1_1_1_7().CheckUpdate();
            new Version1_1_1_8().CheckUpdate();
            new Version1_1_1_9().CheckUpdate();
            new Version1_1_2_0().CheckUpdate();
            new Version1_1_2_1().CheckUpdate(); // REPORT_INVOICE & CUSTOMIZED REPORT
            new Version1_1_2_2().CheckUpdate(); // Fix Backup & Restore
            new Version1_1_2_3().CheckUpdate(); // Fix Bank Trans, Backup Restore, Invoice, Bank - Pending Amount
            new Version1_1_2_4().CheckUpdate(); // RUN_BILL, EXTRA_CHARGE
            new Version1_1_2_5().CheckUpdate(); // Fix RUN_IR_USAGE
            new Version1_1_2_6().CheckUpdate();
            new Version1_1_2_7().CheckUpdate();
            new Version1_1_2_8().CheckUpdate();
            new Version1_1_2_9().CheckUpdate();
            new Version1_1_3_0().CheckUpdate();
            new Version1_1_3_1().CheckUpdate();
            new Version1_1_3_2().CheckUpdate();
            new Version1_1_3_3().CheckUpdate();
            new Version1_1_3_4().CheckUpdate();
            new Version1_1_3_5().CheckUpdate(); // E-Power.PublicPrepaid
            new Version1_1_3_6().CheckUpdate(); // E-Power.PublicPrepaid 
            new Version1_1_3_7().CheckUpdate();
            new Version1_1_3_8().CheckUpdate();
            new Version1_1_3_9().CheckUpdate(); // GET_MONTHLY_USAGE, Collect Prepaid Usage
            new Version1_1_4_0().CheckUpdate();
            new Version1_1_4_1().CheckUpdate();
            new Version1_1_4_2().CheckUpdate();
            new Version1_1_4_3().CheckUpdate();
            new Version1_1_4_4().CheckUpdate();
            new Version1_1_4_5().CheckUpdate();
            new Version1_1_4_6().CheckUpdate();
            new Version1_1_4_7().CheckUpdate();
            new Version1_1_4_8().CheckUpdate();
            new Version1_1_4_9().CheckUpdate();
            new Version1_1_5_0().CheckUpdate();
            new Version1_1_5_1().CheckUpdate();
            new Version1_1_5_2().CheckUpdate();
            new Version1_1_5_3().CheckUpdate();
            new Version1_1_5_4().CheckUpdate();
            new Version1_1_5_5().CheckUpdate();
            new Version1_1_5_6().CheckUpdate();
            new Version1_1_5_7().CheckUpdate();
            new Version1_1_5_8().CheckUpdate();
            new Version1_1_5_9().CheckUpdate();
            new Version1_1_6_0().CheckUpdate();
            new Version1_1_6_1().CheckUpdate();
            new Version1_1_6_2().CheckUpdate();
            new Version1_1_6_3().CheckUpdate();
            new Version1_1_6_4().CheckUpdate();
            new Version1_1_6_5().CheckUpdate();
            new Version1_1_6_6().CheckUpdate();
            new Version1_1_6_7().CheckUpdate();
            new Version1_1_6_8().CheckUpdate();
            new Version1_1_6_9().CheckUpdate();
            new Version1_1_7_0().CheckUpdate();
            new Version1_1_7_1().CheckUpdate();
            new Version1_1_7_2().CheckUpdate();
            new Version1_1_7_3().CheckUpdate();
            new Version1_1_7_4().CheckUpdate();
            new Version1_1_7_5().CheckUpdate();
            new Version1_1_7_6().CheckUpdate();
            new Version1_1_7_7().CheckUpdate();
            new Version1_1_7_8().CheckUpdate();
            new Version1_1_7_9().CheckUpdate();
            new Version1_1_8_0().CheckUpdate();
            new Version1_1_8_1().CheckUpdate();
            new Version1_1_8_2().CheckUpdate();
            new Version1_1_8_3().CheckUpdate();
            new Version1_1_8_4().CheckUpdate();
            new Version1_1_8_5().CheckUpdate();
            new Version1_1_8_6().CheckUpdate();
            new Version1_1_8_7().CheckUpdate();
            new Version1_1_8_8().CheckUpdate();
            new Version1_1_8_9().CheckUpdate();
            new Version1_1_9_0().CheckUpdate();
            new Version1_1_9_1().CheckUpdate();
            new Version1_1_9_2().CheckUpdate();
            new Version1_1_9_3().CheckUpdate();
            new Version1_1_9_4().CheckUpdate();
            new Version1_1_9_5().CheckUpdate();
            new Version1_1_9_6().CheckUpdate();
            new Version1_1_9_7().CheckUpdate();
            new Version1_1_9_8().CheckUpdate();
            new Version1_1_9_9().CheckUpdate();
            new Version1_2_0_0().CheckUpdate();
            new Version1_2_0_1().CheckUpdate();
            new Version1_2_0_2().CheckUpdate();
            new Version1_2_0_3().CheckUpdate();
            new Version1_2_0_4().CheckUpdate();
            new Version1_2_0_5().CheckUpdate();
            new Version1_2_0_6().CheckUpdate();
            new Version1_2_0_7().CheckUpdate();
            new Version1_2_0_8().CheckUpdate();
            new Version1_2_0_9().CheckUpdate();
            new Version1_2_1_0().CheckUpdate();
            new Version1_2_1_1().CheckUpdate();
            new Version1_2_1_2().CheckUpdate();
            new Version1_2_1_3().CheckUpdate();
            new Version1_2_1_4().CheckUpdate();
            new Version1_2_1_5().CheckUpdate();
            new Version1_2_1_6().CheckUpdate();
            new Version1_2_1_7().CheckUpdate();
            new Version1_2_1_8().CheckUpdate();
            new Version1_2_1_9().CheckUpdate();
            new Version1_2_2_0().CheckUpdate();
            new Version1_2_2_1().CheckUpdate();
            new Version1_2_2_2().CheckUpdate();
            new Version1_2_2_3().CheckUpdate();
            new Version1_2_2_4().CheckUpdate();
            new Version1_2_2_5().CheckUpdate();
            new Version1_2_2_6().CheckUpdate();
            new Version1_2_2_7().CheckUpdate();
            new Version1_2_2_8().CheckUpdate();
            new Version1_2_2_9().CheckUpdate();
            new Version1_2_3_0().CheckUpdate();
            new Version1_2_3_1().CheckUpdate();
            new Version1_2_3_2().CheckUpdate();
            new Version1_2_3_3().CheckUpdate();
            new Version1_2_3_4().CheckUpdate();
            new Version1_2_3_5().CheckUpdate();
            new Version1_2_3_6().CheckUpdate();
            new Version1_2_5_0().CheckUpdate();
            new Version1_2_5_3().CheckUpdate();
            new Version1_2_6_0().CheckUpdate();
            new Version1_2_7_0().CheckUpdate();
            new Version1_2_8_0().CheckUpdate();
            new Version1_2_8_1().CheckUpdate();
            new Version1_2_8_2().CheckUpdate();
            new Version1_2_9_0().CheckUpdate();
            new Version1_2_10_0().CheckUpdate();
            new Version1_2_11_0().CheckUpdate();
        }
    }
}
