﻿namespace EPower.Update
{
    class Version7_1_4_3 : Version
    {
        protected override string GetBatchCommand()
        {
            /* 
                @Kheang Kimkhorn
                1.Add IR service URL to UTILITY for switch demo/production

                @Vonn Kimputhmunyvorn 2022-08-19
                1.Fixed report with save data
            */
            return @"
IF NOT EXISTS(SELECT TOP 1 * FROM TBL_UTILITY WHERE UTILITY_ID = 112)
    INSERT INTO TBL_UTILITY
    SELECT 112, N'HB02_SERVICE_URL', N'IR service URL', N'DHMaYktmqkjVsjgH+SUpeaT/dxzv69CiduHqdy6pXrE='
GO
";
        }
    }
}
