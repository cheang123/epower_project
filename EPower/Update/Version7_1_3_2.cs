﻿namespace EPower.Update
{
    class Version7_1_3_2 : Version
    {
        protected override string GetBatchCommand()
        {
            /* Add Report for ARAKAWA
                * @Vonn Kimputhmunyvorn 2022-07-19
                * 1. Modify REPORT_CUSTOMER_ARAKAWA
            */
            return @"
IF NOT EXISTS(SELECT * FROM dbo.TBL_UTILITY WHERE UTILITY_ID = 110)
INSERT INTO dbo.TBL_UTILITY(UTILITY_ID, UTILITY_NAME, DESCRIPTION, UTILITY_VALUE)
VALUES
(110, N'ARAKAWA', N'USE_FOR_ARAKAWA', 0);

GO

IF OBJECT_ID('REPORT_CUSTOMER_ARAKAWA') IS NOT NULL
	DROP PROC REPORT_CUSTOMER_ARAKAWA

GO

CREATE PROC REPORT_CUSTOMER_ARAKAWA
	@AREA_ID                     INT      = 0,
    @PRICE_ID                    INT      = 0,
    @CYCLE_ID                    INT      = 0,
    @CUSTOMER_CONNECTION_TYPE_ID INT      = 0,
    @MONTH                       DATETIME = '2022-06-01',
    @CUS_STATUS_ID               INT      = -1,
    @AMPERE_ID                   INT      = 0,
    @CUSTOMER_GROUP_TYPE_ID      INT      = 0
AS
DECLARE @CMD NVARCHAR (MAX);
DECLARE @ID NVARCHAR (MAX);
WITH c AS (
          SELECT    RowNumber = ROW_NUMBER () OVER (PARTITION BY CUSTOMER_ID ORDER BY CUS_METER_ID DESC),
                    *
          FROM  TBL_CUSTOMER_METER
          )

SELECT  * INTO  #TempCusMeter FROM  c WHERE c.RowNumber = 1


SELECT	[Year]			= YEAR(i.INVOICE_MONTH),
		[Month]			= MONTH(i.INVOICE_MONTH),
		[Block]			= ISNULL(a.AREA_NAME,'-'),
        [Floor]			= ISNULL(p.POLE_CODE,'-'),
        [Room]			= ISNULL(b.BOX_CODE,'-'),
        CustomerName	= c.LAST_NAME_KH + ' ' + c.FIRST_NAME_KH,
        MerterNumber    = i.METER_CODE,
        StartUsage		= 0,
        EndUsage		= 0,
		PayAmount		= i.SETTLE_AMOUNT,
		TaxAmount	    = 0,
		StatusId		= c.STATUS_ID,
		Currency        = i.CURRENCY_ID,
		CustomerId      = i.CUSTOMER_ID,
		IsService       = i.IS_SERVICE_BILL,
		MeterId         = iu.METER_ID,
		InvoiceMonth    = i.INVOICE_MONTH
INTO #TEMP
FROM    TBL_CUSTOMER                                 c
        INNER JOIN TLKP_CUSTOMER_CONNECTION_TYPE ct ON c.CUSTOMER_CONNECTION_TYPE_ID = ct.CUSTOMER_CONNECTION_TYPE_ID
        INNER JOIN TBL_INVOICE i ON i.CUSTOMER_ID = c.CUSTOMER_ID AND i.INVOICE_MONTH = @MONTH
		INNER JOIN TBL_INVOICE_USAGE iu ON iu.INVOICE_ID = i.INVOICE_ID AND i.METER_CODE = iu.METER_CODE
		LEFT JOIN #TempCusMeter                      cm ON cm.CUSTOMER_ID = c.CUSTOMER_ID
        LEFT JOIN TBL_METER                          m ON cm.METER_ID = m.METER_ID
        LEFT JOIN TBL_AREA                           a ON a.AREA_ID = c.AREA_ID
        LEFT JOIN TBL_BOX                            b ON b.BOX_ID = cm.BOX_ID
        LEFT JOIN TBL_POLE                           p ON p.POLE_ID = b.POLE_ID
        LEFT JOIN TBL_AMPARE                         amp ON amp.AMPARE_ID = c.AMP_ID
WHERE(@AREA_ID = 0 OR   a.AREA_ID = @AREA_ID)
     AND (@CYCLE_ID = 0 OR  c.BILLING_CYCLE_ID = @CYCLE_ID)
     AND (@PRICE_ID = 0 OR  c.PRICE_ID = @PRICE_ID)
     AND (
         @CUSTOMER_CONNECTION_TYPE_ID = 0
         OR c.CUSTOMER_CONNECTION_TYPE_ID = @CUSTOMER_CONNECTION_TYPE_ID
         )
     AND (@AMPERE_ID = 0 OR c.AMP_ID = @AMPERE_ID)
     AND (
         @CUSTOMER_GROUP_TYPE_ID = 0
         OR ct.NONLICENSE_CUSTOMER_GROUP_ID = @CUSTOMER_GROUP_TYPE_ID
         )
	 AND i.INVOICE_STATUS  NOT IN (3) AND i.IS_SERVICE_BILL = 0;

SELECT	t.Year,
		t.Month,
		t.Block,
		t.Floor,
		t.Room,
		t.CustomerName,
		t.MerterNumber,
		StartUsage = u.START_USAGE,
		EndUsage = u.END_USAGE,
		t.PayAmount,
		t.TaxAmount,
		t.StatusId,
		t.Currency
INTO #RESULT
FROM #TEMP t
INNER JOIN dbo.TBL_USAGE u ON u.METER_ID = t.MeterId AND u.USAGE_MONTH = t.InvoiceMonth

UNION ALL
SELECT	[Year]			= YEAR(i.INVOICE_MONTH),
		[Month]			= MONTH(i.INVOICE_MONTH),
		[Block]			= ISNULL(a.AREA_NAME,'-'),
        [Floor]			= ISNULL(p.POLE_CODE,'-'),
        [Room]			= ISNULL(b.BOX_CODE,'-'),
        CustomerName	= c.LAST_NAME_KH + ' ' + c.FIRST_NAME_KH,
        MerterNumber    = '-',
		StartUsage		= 0,
		EndUsage		= 0,
		PayAmount		= i.SETTLE_AMOUNT,
		TaxAmoun		= 0,
		StatusId		= c.STATUS_ID,
		Currency		= i.CURRENCY_ID
FROM    TBL_CUSTOMER                                 c
        INNER JOIN TLKP_CUSTOMER_CONNECTION_TYPE ct ON c.CUSTOMER_CONNECTION_TYPE_ID = ct.CUSTOMER_CONNECTION_TYPE_ID
        INNER JOIN TBL_INVOICE i ON i.CUSTOMER_ID = c.CUSTOMER_ID AND i.INVOICE_MONTH = @MONTH
		LEFT JOIN #TempCusMeter                      cm ON cm.CUSTOMER_ID = c.CUSTOMER_ID
        LEFT JOIN TBL_METER                          m ON cm.METER_ID = m.METER_ID
        LEFT JOIN TBL_AREA                           a ON a.AREA_ID = c.AREA_ID
        LEFT JOIN TBL_BOX                            b ON b.BOX_ID = cm.BOX_ID
        LEFT JOIN TBL_POLE                           p ON p.POLE_ID = b.POLE_ID
        LEFT JOIN TBL_AMPARE                         amp ON amp.AMPARE_ID = c.AMP_ID
WHERE(@AREA_ID = 0 OR   a.AREA_ID = @AREA_ID)
     AND (@CYCLE_ID = 0 OR  c.BILLING_CYCLE_ID = @CYCLE_ID)
     AND (@PRICE_ID = 0 OR  c.PRICE_ID = @PRICE_ID)
     AND (
         @CUSTOMER_CONNECTION_TYPE_ID = 0
         OR c.CUSTOMER_CONNECTION_TYPE_ID = @CUSTOMER_CONNECTION_TYPE_ID
         )
     AND (@AMPERE_ID = 0 OR c.AMP_ID = @AMPERE_ID)
     AND (
         @CUSTOMER_GROUP_TYPE_ID = 0
         OR ct.NONLICENSE_CUSTOMER_GROUP_ID = @CUSTOMER_GROUP_TYPE_ID
         )
	 AND i.INVOICE_STATUS  NOT IN (3) AND i.IS_SERVICE_BILL = 1;


IF @CUS_STATUS_ID = -1 BEGIN
    SELECT  * FROM  #RESULT WHERE StatusId IN (1, 2, 3) ORDER BY Block,Floor,Room ASC;
END;
ELSE IF @CUS_STATUS_ID = -2 BEGIN
         SELECT * FROM  #RESULT WHERE StatusId IN (4, 5) ORDER BY Block,Floor,Room ASC ;
END;
ELSE IF @CUS_STATUS_ID = 0 BEGIN
         SELECT * FROM  #RESULT ORDER BY Block,Floor,Room ASC;
END;
ELSE BEGIN
         SELECT * FROM  #RESULT WHERE StatusId = @CUS_STATUS_ID ORDER BY Block,Floor,Room ASC;
END;
--END OF  REPORT_CUSTOMER_ARAKAWA

GO";
        }
    }
}
