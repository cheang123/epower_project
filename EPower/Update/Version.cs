﻿using System;
using System.Linq;
using System.Transactions;
using EPower.CRMServiceReference;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Security.Logic;
using EPower.Logic;

namespace EPower.Update
{
    public class Version
    {
        public static string ClientVersion = "0.0.0.0";
        public static string DBVersion
        {
            get
            {
                return Method.Utilities[Utility.VERSION];
            }
        }
        public string GetVersion()
        {
            return this.GetType().Name.Replace("Version", "").Replace("_", ".");
        }

        protected virtual string GetBatchCommand() {
            return string.Empty;
        } 
        
        public void CheckUpdate()
        {
            ClientVersion = this.GetVersion();

            // if version is not require then pass through
            if (!IsRequiredVersion(this.GetVersion())) return;

            // update version
            Runner.RunNewThread(this.Update, Resources.UPDATING_VERSION + this.GetVersion());

            if (Runner.Instance.Error!=null){
                throw Runner.Instance.Error;
            }
        }
        static bool backup = false;
        public virtual void Update()
        { 
            // backup database before update
            try {
                if (!backup)
                {
                    DBA.BackupToDisk(Settings.Default.PATH_BACKUP, string.Format("{0} {1:yyyyMMddHHmmss} before update v{2}.bak", DBDataContext.Db.Connection.Database, DBDataContext.Db.GetSystemDate(), GetCurrentVersion()));
                    backup = true;
                }
            }
            catch (Exception) { }

            using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required,TimeSpan.MaxValue))
            {
                foreach (string cmd in this.GetBatchCommand().Split(new string[] { "\nGO" },StringSplitOptions.RemoveEmptyEntries))
                {
                    try
                    {
                        DBDataContext.Db.CommandTimeout = 20 * 60 ;
                        DBDataContext.Db.ExecuteCommand(cmd);
                        DBDataContext.Db.CommandTimeout = 60;
                    }
                    catch (Exception ex)
                    {
                        MsgBox.LogError(ex);
                        throw new Exception(string.Format(Base.Properties.Resources.YOU_CANNOT_PROCESS, "", Base.Properties.Resources.UPDATE_NEW_VERSION));
                    }
                    
                } 
                  //add TABLE
                DBDataContext.Db.ExecuteCommand(@"  INSERT INTO TLKP_TABLE
                                                    SELECT TABLE_NAME, TABLE_NAME,1 ,1FROM INFORMATION_SCHEMA.TABLES
                                                    WHERE TABLE_NAME NOT IN (SELECT TABLE_NAME FROM TLKP_TABLE UNION SELECT 'sysdiagrams')
                                                    AND TABLE_NAME NOT LIKE 'TLKP%'; ");
                
                //add FIELD
                DBDataContext.Db.ExecuteCommand(@"  INSERT INTO TLKP_TABLE_FIELD
                                                    SELECT col.COLUMN_NAME,col.COLUMN_NAME,tbl.TABLE_ID,1,'','',0
                                                    FROM INFORMATION_SCHEMA.COLUMNS col
	                                                    INNER JOIN TLKP_TABLE tbl ON tbl.TABLE_NAME=col.TABLE_NAME
                                                    WHERE tbl.IS_CHANGE_LOG_USED = 1
                                                    AND tbl.TABLE_NAME+'.'+col.COLUMN_NAME NOT IN (
	                                                    SELECT tbl.TABLE_NAME+'.'+col.FIELD_NAME
	                                                    FROM TLKP_TABLE_FIELD col INNER JOIN TLKP_TABLE tbl ON tbl.TABLE_ID=col.TABLE_ID
                                                    )");
                
                DBDataContext.Db.TBL_UTILITies.FirstOrDefault(row => row.UTILITY_ID == (int)Utility.VERSION).UTILITY_VALUE = GetVersion();
                DBDataContext.Db.SubmitChanges(); 
                tran.Complete();
            }

            // try log version update to the server
            // skip log version update for DEMO database.
            try
            {
                if (!DBDataContext.Db.Connection.Database.ToUpper().Contains("DEMO"))
                {
                    CRMService.Intance.AuthenticateRequest(ClientRequestType.UpdateVersion);
                }
            }
            catch (Exception) { }
             
        }

        public bool IsRequiredVersion(string version)
        {
            return ConvertToLong(GetCurrentVersion()) < ConvertToLong(version);
        }
        
        public static string GetCurrentVersion()
        {
            TBL_UTILITY objConf = DBDataContext.Db.TBL_UTILITies.FirstOrDefault(row => row.UTILITY_ID == (int)Utility.VERSION);
            if (objConf == null)
            {
                return "0.0.0.0";
            }
            else
            {
                return objConf.UTILITY_VALUE;
            }
        }

        public static long ConvertToLong(string version)
        {
            string tmp = "0";
            foreach (string s in version.Split(new char[] { '.' }))
            {
                tmp += s.PadLeft(3, '0');
            }
            return long.Parse(tmp);
        } 
    }
}
