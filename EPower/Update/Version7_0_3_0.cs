﻿using EPower.Properties;
using System.Configuration;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Update
{
    class Version7_0_3_0 : Version
    {
        /*
        @Kheang Kimkhorn 2022-02-11
        1. Update 2022 customer connection type
        2. Auto send report statistic to EAC

        @Vonn Kimputhmunyvorn 2020-02-15
        1. Add Report Aging New
        */

        protected override string GetBatchCommand()
        {
            return Resources.v7_0_3_0;
        }
    }
}
