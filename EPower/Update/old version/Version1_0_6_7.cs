﻿namespace EPower.Update
{
    /// <summary> 
    /// Fix Readonly file
    /// </summary>
    class Version1_0_6_7 : Version
    {
        /// <summary>
        /// ALLOW COLLECT USAGE FROM IR GPRS (GF1100-GPRS)
        /// </summary>
        /// <returns></returns>
        protected override string GetBatchCommand()
        {
            return @"DELETE TBL_UTILITY WHERE UTILITY_ID = 54;
INSERT INTO TBL_UTILITY(UTILITY_ID, UTILITY_NAME, DESCRIPTION, UTILITY_VALUE) VALUES(54,	N'USAGE_SERVICE_URL',	N'BLANK IF NOT USE: http://crm.e-power.com.kh:1112/DesktopService.svc/ws',	'');
DELETE TBL_AUDITTRIAL_GROUP WHERE AUDIT_GROUP_ID IN (9,10);
INSERT INTO TBL_AUDITTRIAL_GROUP(AUDIT_GROUP_ID,AUDIT_GROUP_NAME,AUDIT_GROUP_NAME_LOCAL) VALUES (9, N'Send Usage', N'បញ្ជូនអំណានថ្មី');
INSERT INTO TBL_AUDITTRIAL_GROUP(AUDIT_GROUP_ID,AUDIT_GROUP_NAME,AUDIT_GROUP_NAME_LOCAL) VALUES (10, N'Receive Usage', N'ទាញយកអំណាន');
"; 
        }
    }
}
