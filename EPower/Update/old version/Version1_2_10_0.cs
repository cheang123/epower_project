﻿using EPower.Properties;
namespace EPower.Update
{
    class Version1_2_10_0 : Version
    {
        /* Thunny 
                -Update function Get_Sequence add parameter Invoice_Date
                -Update RunBill,ReverseBill,RUN_BANK_PAYMENT to support new function Get_Sequence
                -Update TBL_SEQUENCE
         */
        protected override string GetBatchCommand()
        {
            return Resources.Version1_2_10_0 + @"

IF NOT EXISTS(SELECT 1 FROM sys.columns  WHERE name = N'FORMAT' AND Object_ID = Object_ID(N'TLKP_CURRENCY'))
BEGIN
  ALTER TABLE dbo.TLKP_CURRENCY ADD [FORMAT] NVARCHAR(200) DEFAULT '' NOT NULL
END

GO

IF  EXISTS(SELECT 1 FROM sys.columns  WHERE name = N'INVOICE_ITEM_ID' AND Object_ID = Object_ID(N'TMP_BILL_USAGE_RATE'))
BEGIN
  ALTER TABLE dbo.TMP_BILL_USAGE_RATE DROP COLUMN INVOICE_ITEM_ID
END

GO

ALTER TABLE ANR.TBL_RESULT_11
ALTER COLUMN TOTAL_POWER DECIMAL(18, 4) NULL;

ALTER TABLE ANR.TBL_RESULT_11
ALTER COLUMN TOTAL_INCOME DECIMAL(18, 4) NULL;

ALTER TABLE ANR.TBL_RESULT_11
ALTER COLUMN TOTAL_RECEIVE DECIMAL(18, 4) NULL;

UPDATE dbo.TBL_SEQUENCE
SET FORMAT='CINV{YY}-{N:6}', VALUE=0, DATE=GETDATE()
WHERE SEQUENCE_ID=1 AND FORMAT !='CINV{YY}-{N:6}'

UPDATE dbo.TBL_SEQUENCE
SET FORMAT='TINV{YY}-{N:6}', VALUE=0, DATE=GETDATE()
WHERE SEQUENCE_ID=4 AND FORMAT !='TINV{YY}-{N:6}'

UPDATE dbo.TBL_SEQUENCE
SET FORMAT='VOID{YY}-{N:6}', VALUE=0, DATE=GETDATE()
WHERE SEQUENCE_ID=-2 AND FORMAT !='VOID{YY}-{N:6}'

UPDATE dbo.TBL_SEQUENCE
SET FORMAT='ADJ{YY}-{N:6}', VALUE=0, DATE=GETDATE()
WHERE SEQUENCE_ID=-1 AND FORMAT !='ADJ{YY}-{N:6}'

UPDATE TBL_UTILITY
SET UTILITY_VALUE = 1
WHERE UTILITY_ID = 34;

UPDATE dbo.TBL_SEQUENCE_TYPE
SET SEQUENCE_ID=4
WHERE SEQUENCE_TYPE_ID=2

UPDATE dbo.TBL_SEQUENCE_TYPE
SET SEQUENCE_ID=1
WHERE SEQUENCE_TYPE_ID=3

UPDATE dbo.TLKP_CURRENCY
SET FORMAT='#,##0;-#,##0;0'
WHERE CURRENCY_ID=1	
	
UPDATE dbo.TLKP_CURRENCY
SET FORMAT='#,##0.00;-#,##0.00;0.00'
WHERE CURRENCY_ID=2

UPDATE dbo.TLKP_CURRENCY
SET FORMAT='#,##0.00;-#,##0.00;0.00'
WHERE CURRENCY_ID=3

UPDATE dbo.TLKP_CURRENCY
SET CURRENCY_SING=N'៛'
WHERE CURRENCY_ID=1

UPDATE dbo.TLKP_CURRENCY
SET CURRENCY_SING=N'B', CURRENCY_CODE='THB'
WHERE CURRENCY_ID=3


GO

IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'IX_INVOICE_INVOICEDATE_INVOICESTATUS_ISSERVICE')
BEGIN
	DROP INDEX IX_INVOICE_INVOICEDATE_INVOICESTATUS_ISSERVICE ON dbo.TBL_INVOICE
END
CREATE INDEX IX_INVOICE_INVOICEDATE_INVOICESTATUS_ISSERVICE ON dbo.TBL_INVOICE 
(INVOICE_STATUS, INVOICE_DATE, IS_SERVICE_BILL)

GO

IF OBJECT_ID('GET_DATE_WITH_CURRENTTIME')IS NOT NULL
	DROP FUNCTION GET_DATE_WITH_CURRENTTIME

GO

CREATE FUNCTION GET_DATE_WITH_CURRENTTIME(@DATE AS DATETIME)
RETURNS DATETIME 
AS
BEGIN
	DECLARE @CURRENT_DATE AS DATETIME;
	SET @CURRENT_DATE = GETDATE(); 
		DECLARE @DAY AS NVARCHAR(20)  
		DECLARE @MONTH AS NVARCHAR(20) 
		DECLARE @YEAR AS NVARCHAR(20)

		DECLARE @HOUR AS NVARCHAR(20)
		DECLARE @MINUTE AS NVARCHAR(20) 
		DECLARE @SECOND AS NVARCHAR(20)
		DECLARE @MILISECOND AS NVARCHAR(200) 
		 
		--SET
		SET @DAY		=CONVERT(NVARCHAR(20),DATEPART(DAY,@DATE));
		SET @MONTH		=CONVERT(NVARCHAR(20),DATEPART(MONTH,@DATE));
		SET @YEAR		=CONVERT(NVARCHAR(20),DATEPART(YEAR,@DATE));
		SET @HOUR		=CONVERT(NVARCHAR(20),DATEPART(HOUR,@CURRENT_DATE));
		SET @MINUTE		=CONVERT(NVARCHAR(20),DATEPART(MINUTE,@CURRENT_DATE));
		SET @SECOND		=CONVERT(NVARCHAR(20),DATEPART(SECOND,@CURRENT_DATE));
		SET @MILISECOND =CONVERT(NVARCHAR(20),DATEPART(MILLISECOND,@CURRENT_DATE));
		 
	DECLARE @CURRENT_TIME_STRING AS NVARCHAR(200);
	SET @CURRENT_TIME_STRING =  @HOUR + ':' + @MINUTE + ':' + @SECOND + '.' + @MILISECOND;

	DECLARE @FULLDATE  AS DATETIME; 
	DECLARE @FULL_DATE_STRING AS NVARCHAR(200);
	SET @FULL_DATE_STRING =@YEAR + '-' + @MONTH + '-' + @DAY  + ' ' + @CURRENT_TIME_STRING;
	SET @FULLDATE =  CONVERT(DATETIME ,@FULL_DATE_STRING)
	RETURN @FULLDATE;
END

GO

IF NOT EXISTS
(
    SELECT *
    FROM dbo.TBL_HOLIDAY
    WHERE HOLIDAY_ID IN ( 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150,
                          151, 152
                        )
)
BEGIN
    INSERT INTO dbo.TBL_HOLIDAY
    VALUES
    (   N'ទិវា​ចូល​ឆ្នាំ​សកល',     -- HOLIDAY_NAME - nvarchar(200)
        '2021-01-01 00:00:00.000', -- HOLIDAY_DATE - datetime
        N'',                       -- NOTE - nvarchar(200)
        1                          -- IS_ACTIVE - bit
        );
    INSERT INTO dbo.TBL_HOLIDAY
    VALUES
    (   N'ទិវា​ជ័យជំនះ​លើ​របប​ប្រល័យ​ពូជ​សាសន៍', -- HOLIDAY_NAME - nvarchar(200)
        '2021-01-07 00:00:00.000',               -- HOLIDAY_DATE - datetime
        N'',                                     -- NOTE - nvarchar(200)
        1                                        -- IS_ACTIVE - bit
        );
    INSERT INTO dbo.TBL_HOLIDAY
    VALUES
    (   N'ទិវា​នារី​អន្តរជាតិ',    -- HOLIDAY_NAME - nvarchar(200)
        '2021-03-08 00:00:00.000', -- HOLIDAY_DATE - datetime
        N'',                       -- NOTE - nvarchar(200)
        1                          -- IS_ACTIVE - bit
        );
    INSERT INTO dbo.TBL_HOLIDAY
    VALUES
    (   N'ពិធីបុណ្យ​ចូលឆ្នាំថ្មី ប្រពៃណីជាតិ ឆ្នាំឆ្លូវ​ ត្រីស័ក​', -- HOLIDAY_NAME - nvarchar(200)
        '2021-04-14 00:00:00.000',                       -- HOLIDAY_DATE - datetime
        N'',                                             -- NOTE - nvarchar(200)
        1                                                -- IS_ACTIVE - bit
        );
    INSERT INTO dbo.TBL_HOLIDAY
    VALUES
    (   N'ពិធីបុណ្យ​ចូលឆ្នាំថ្មី ប្រពៃណីជាតិ ឆ្នាំឆ្លូវ​ ត្រីស័ក​', -- HOLIDAY_NAME - nvarchar(200)
        '2021-04-15 00:00:00.000',                       -- HOLIDAY_DATE - datetime
        N'',                                             -- NOTE - nvarchar(200)
        1                                                -- IS_ACTIVE - bit
        );
    INSERT INTO dbo.TBL_HOLIDAY
    VALUES
    (   N'ពិធីបុណ្យ​ចូលឆ្នាំថ្មី ប្រពៃណីជាតិ ឆ្នាំឆ្លូវ​ ត្រីស័ក​', -- HOLIDAY_NAME - nvarchar(200)
        '2021-04-16 00:00:00.000',                       -- HOLIDAY_DATE - datetime
        N'',                                             -- NOTE - nvarchar(200)
        1                                                -- IS_ACTIVE - bit
        );
    INSERT INTO dbo.TBL_HOLIDAY
    VALUES
    (   N'ពិធីបុណ្យ​វិសាខបូជា​',   -- HOLIDAY_NAME - nvarchar(200)
        '2021-04-26 00:00:00.000', -- HOLIDAY_DATE - datetime
        N'',                       -- NOTE - nvarchar(200)
        1                          -- IS_ACTIVE - bit
        );
    INSERT INTO dbo.TBL_HOLIDAY
    VALUES
    (   N'ព្រះ​រាជ​ពិធី​ច្រត់​ព្រះ​នង្គ័ល​', -- HOLIDAY_NAME - nvarchar(200)
        '2021-04-30 00:00:00.000',           -- HOLIDAY_DATE - datetime
        N'',                                 -- NOTE - nvarchar(200)
        1                                    -- IS_ACTIVE - bit
        );
    INSERT INTO dbo.TBL_HOLIDAY
    VALUES
    (   N'ទិវា​ពលកម្ម​អន្តរជាតិ',  -- HOLIDAY_NAME - nvarchar(200)
        '2021-05-01 00:00:00.000', -- HOLIDAY_DATE - datetime
        N'',                       -- NOTE - nvarchar(200)
        1                          -- IS_ACTIVE - bit
        );
    INSERT INTO dbo.TBL_HOLIDAY
    VALUES
    (   N'ព្រះរាជពិធីចម្រើនព្រះជន្ម ព្រះ​ករុណា​ព្រះ​បាទ​សម្តេច​ព្រះ​បរម​នាថ នរោត្តម សីហមុនី ព្រះ​មហាក្សត្រ​នៃ​ព្រះរាជាណាចក្រ​កម្ពុជា', -- HOLIDAY_NAME - nvarchar(200)
        '2021-05-14 00:00:00.000',                                                                                                     -- HOLIDAY_DATE - datetime
        N'',                                                                                                                           -- NOTE - nvarchar(200)
        1                                                                                                                              -- IS_ACTIVE - bit
        );
    INSERT INTO dbo.TBL_HOLIDAY
    VALUES
    (   N'​ព្រះរាជពិធីចម្រើនព្រះជន្ម សម្តេច​ព្រះ​មហាក្សត្រី នរោត្តម មុនិនាថ សីហនុ ព្រះ​វររាជ​មាតា​ជាតិ​ខ្មែរ ក្នុង​សេរីភាព សេចក្តីថ្លៃថ្នូរ និង​សុភមង្គល', -- HOLIDAY_NAME - nvarchar(200)
        '2021-06-18 00:00:00.000',                                                                                                                         -- HOLIDAY_DATE - datetime
        N'',                                                                                                                                               -- NOTE - nvarchar(200)
        1                                                                                                                                                  -- IS_ACTIVE - bit
        );
    INSERT INTO dbo.TBL_HOLIDAY
    VALUES
    (   N'ទិវា​ប្រកាស​រដ្ឋធម្មនុញ្ញ​', -- HOLIDAY_NAME - nvarchar(200)
        '2021-09-24 00:00:00.000',     -- HOLIDAY_DATE - datetime
        N'',                           -- NOTE - nvarchar(200)
        1                              -- IS_ACTIVE - bit
        );
    INSERT INTO dbo.TBL_HOLIDAY
    VALUES
    (   N'ពិធី​បុណ្យ​ភ្ជុំ​បិណ្ឌ', -- HOLIDAY_NAME - nvarchar(200)
        '2021-10-05 00:00:00.000', -- HOLIDAY_DATE - datetime
        N'',                       -- NOTE - nvarchar(200)
        1                          -- IS_ACTIVE - bit
        );
    INSERT INTO dbo.TBL_HOLIDAY
    VALUES
    (   N'ពិធី​បុណ្យ​ភ្ជុំ​បិណ្ឌ', -- HOLIDAY_NAME - nvarchar(200)
        '2021-10-06 00:00:00.000', -- HOLIDAY_DATE - datetime
        N'',                       -- NOTE - nvarchar(200)
        1                          -- IS_ACTIVE - bit
        );
    INSERT INTO dbo.TBL_HOLIDAY
    VALUES
    (   N'ពិធី​បុណ្យ​ភ្ជុំ​បិណ្ឌ', -- HOLIDAY_NAME - nvarchar(200)
        '2021-10-07 00:00:00.000', -- HOLIDAY_DATE - datetime
        N'',                       -- NOTE - nvarchar(200)
        1                          -- IS_ACTIVE - bit
        );
    INSERT INTO dbo.TBL_HOLIDAY
    VALUES
    (   N'​ទិវាប្រារព្ធពិធីគោរពព្រះវិញ្ញាណក្ខន្ ព្រះករុណា​ព្រះបាទ​សម្តេច​ព្រះ នរោត្តម សីហនុ ព្រះមហាវីរក្សត្រ ព្រះ​វររាជ​បិតា​ឯករាជ្យ បូរណភាព​ទឹកដី និង​ឯកភាព​ជាតិ​ខ្មែរ', -- HOLIDAY_NAME - nvarchar(200)
        '2021-10-15 00:00:00.000',                                                                                                                                        -- HOLIDAY_DATE - datetime
        N'',                                                                                                                                                              -- NOTE - nvarchar(200)
        1                                                                                                                                                                 -- IS_ACTIVE - bit
        );
    INSERT INTO dbo.TBL_HOLIDAY
    VALUES
    (   N'ព្រះ​រាជ​ពិធី​គ្រង​ព្រះ​បរម​រាជ​សម្បត្តិ​របស់​ ព្រះ​ករុណា​ព្រះ​បាទ​សម្ដេច​ព្រះ​បរមនាថ ព្រះ​ករុណា​ព្រះ​បាទ​សម្ដេច​ព្រះ​បរមនាថ នរោត្ដម សីហមុនី ព្រះមហាក្សត្រ នៃ​ព្រះរាជាណាចក្រ​កម្ពុជា', -- HOLIDAY_NAME - nvarchar(200)
        '2021-10-29 00:00:00.000',                                                                                                                                                               -- HOLIDAY_DATE - datetime
        N'',                                                                                                                                                                                     -- NOTE - nvarchar(200)
        1                                                                                                                                                                                        -- IS_ACTIVE - bit
        );
    INSERT INTO dbo.TBL_HOLIDAY
    VALUES
    (   N'ពិធី​បុណ្យ​ឯករាជ្យ​ជាតិ​', -- HOLIDAY_NAME - nvarchar(200)
        '2021-11-09 00:00:00.000',   -- HOLIDAY_DATE - datetime
        N'',                         -- NOTE - nvarchar(200)
        1                            -- IS_ACTIVE - bit
        );
    INSERT INTO dbo.TBL_HOLIDAY
    VALUES
    (   N'ព្រះ​រាជ​ពិធី​បុណ្យ​អុំ​ទូក បណ្ដែត​ប្រទីប និង​សំពះ​ព្រះ​ខែ អកអំបុក', -- HOLIDAY_NAME - nvarchar(200)
        '2021-11-18 00:00:00.000',                                             -- HOLIDAY_DATE - datetime
        N'',                                                                   -- NOTE - nvarchar(200)
        1                                                                      -- IS_ACTIVE - bit
        );
    INSERT INTO dbo.TBL_HOLIDAY
    VALUES
    (   N'ព្រះ​រាជ​ពិធី​បុណ្យ​អុំ​ទូក បណ្ដែត​ប្រទីប និង​សំពះ​ព្រះ​ខែ អកអំបុក', -- HOLIDAY_NAME - nvarchar(200)
        '2021-11-19 00:00:00.000',                                             -- HOLIDAY_DATE - datetime
        N'',                                                                   -- NOTE - nvarchar(200)
        1                                                                      -- IS_ACTIVE - bit
        );
    INSERT INTO dbo.TBL_HOLIDAY
    VALUES
    (   N'ព្រះ​រាជ​ពិធី​បុណ្យ​អុំ​ទូក បណ្ដែត​ប្រទីប និង​សំពះ​ព្រះ​ខែ អកអំបុក', -- HOLIDAY_NAME - nvarchar(200)
        '2021-11-20 00:00:00.000',                                             -- HOLIDAY_DATE - datetime
        N'',                                                                   -- NOTE - nvarchar(200)
        1                                                                      -- IS_ACTIVE - bit
        );
END;

GO

IF OBJECT_ID('GET_SEQUENCE')IS NOT NULL
	DROP FUNCTION GET_SEQUENCE
GO
CREATE FUNCTION dbo.GET_SEQUENCE
(
    @SEQUENCE_ID INT,
    @OFFSET INT = 0,
    @INVOICE_DATE DATETIME = NULL
)
RETURNS NVARCHAR(50)
AS
BEGIN
    DECLARE @CUR_VAL INT,
            @CUR_DATE DATETIME,
            @FORMAT NVARCHAR(50),
            @TMP NVARCHAR(50);

    SELECT @CUR_VAL = VALUE,
           @CUR_DATE = DATE,
           @FORMAT = FORMAT,
           @TMP = FORMAT
    FROM TBL_SEQUENCE
    WHERE SEQUENCE_ID = @SEQUENCE_ID;

  IF @INVOICE_DATE IS NULL 
  BEGIN
    SET @INVOICE_DATE = GETDATE()
  END  

    -- REPLACEING PATTERN
    SET @TMP = REPLACE(@TMP, '{YY}', RIGHT(CONVERT(NVARCHAR, @INVOICE_DATE, 101), 2));
    SET @TMP = REPLACE(@TMP, '{YYYY}', RIGHT(CONVERT(NVARCHAR, @INVOICE_DATE, 101), 4));
    SET @TMP = REPLACE(@TMP, '{MM}', LEFT(CONVERT(NVARCHAR, @INVOICE_DATE, 110), 2));

    DECLARE @INDEX INT,
            @DIGIT INT;
    SELECT @INDEX = CHARINDEX('N:', @TMP);
    SELECT @DIGIT = REPLACE(SUBSTRING(@TMP, @INDEX + 2, @INDEX + 4), '}', '');
    SET @TMP = REPLACE(@TMP, '{N:' + CONVERT(NVARCHAR, @DIGIT) + '}', '{N}');

    IF @FORMAT LIKE '%{Y%'
       AND DATEDIFF(YEAR, @CUR_DATE, @INVOICE_DATE) <> 0
        SET @CUR_VAL = 0;
    IF @FORMAT LIKE '%{M%'
       AND DATEDIFF(MONTH, @CUR_DATE, @INVOICE_DATE) <> 0
        SET @CUR_VAL = 0;
    -- INCREMENT
    SET @CUR_VAL = @CUR_VAL + @OFFSET + 1;

    SET @TMP = REPLACE(@TMP, '{N}', RIGHT(REPLICATE('0', @DIGIT) + CONVERT(NVARCHAR, @CUR_VAL), @DIGIT));

    RETURN @TMP;

END;
-- END GET_SEQUENCE

GO
IF OBJECT_ID('SPLIT_STRING')IS NOT NULL
	DROP FUNCTION SPLIT_STRING
GO 
CREATE FUNCTION SPLIT_STRING
(
    @in_string NVARCHAR(500),
    @delimeter NVARCHAR(1)
)
RETURNS @list TABLE(Id INT IDENTITY(1,1), tuple VARCHAR(100))
AS
BEGIN
        WHILE LEN(@in_string) > 0
        BEGIN
            INSERT INTO @list(tuple)
            SELECT left(@in_string, charindex(@delimeter, @in_string+@delimeter) -1) as tuple 
            SET @in_string = stuff(@in_string, 1, charindex(@delimeter, @in_string + @delimeter), '')
        end
    RETURN 
END
GO


IF OBJECT_ID('RUN_BILL_SPECIAL') IS NOT NULL
	DROP PROC RUN_BILL_SPECIAL
GO

CREATE PROC dbo.RUN_BILL_SPECIAL 
    @MONTH DATETIME = '2011-1-1',
    @START_DATE DATETIME = '2011-1-1',
    @END_DATE DATETIME = '2011-1-31',
    @CYCLE_ID INT = 1,
    @CREATE_BY NVARCHAR(100) = 'ADMIN',
    @DUE_DATE DATETIME = '2011-2-20',
    @START_PAY_DATE DATETIME = '2011-2-01',
    @INVOICE_DATE DATETIME = '2021-01-01'
WITH ENCRYPTION
AS
/*
BY	: RY RITH
SINCE: 2011-01-17 
SINCE: 2011-02-25 Update Discount
SINCE: 2011-03-03 Update Service Bill
SINCE: 2011-03-31 Update Utility Value
SINCE: 2011-04-06 Filter Close and Delete Custoemr 
SINCE: 2011-04-20 Update Filter Error(Change From CustomerType to Status)
SINCE: 2011-04-25 Add @DUE_DATE 
SINEC: 2011-07-25 o Revise Invoice No
				  o Add Invoice Order by customer for FakeDB
				  o Add log billing summary
				  o Add log AR before and after run bill.
				  o Add log Invoice(s)
SINCE: 2011-12-22 multiplier support
SINCE: 2011-12-27 3-phase meter support
SINCE: 2012-01-18 recurring service
SINCE: 2012-01-25 add keep remain billing amount.
SINCE: 2011-01-27 fix sequence.
SINCE: 2012-03-02 fix keep remain billing amount (reverse condition).
SINCE: 2012-03-02 fix unlimit (change from 1m -> 100m)
SINCE: 2012-08-27 ... 
SINCE: 2014-03-21 Rith, fix inserting duplicate value TBL_RUN_BILL_INVOICE
SINCE: 2014-03-21 Rith, add Reactive Power Calculation.
SINCE: 2016-01-16 Rith, add Price extra charge.
SINCE: 2016-03-29 Rith, add Price, based price history
SINCE: 2016-04-23 Rith, floor TOTAL_USAGE
                        discount calculation based on TOTAL_USAGE (before TOTAL_USAGE-DISCOUNT_USAGE)
SINCE: 2018-03-02 Smey, LOG AGIN before and After RUN_BILL
SINCE: 2018-06-13 Smey, add ROW_DATE to TBL_INVOICE
SINCE: 2019-03-04 Smey, add CUSTOMER_CONN
SINCE: 2020-09-10 Khorn, seperate sequence invoice bill and service
*/

TRUNCATE TABLE TMP_BILL_USAGE;
TRUNCATE TABLE TMP_BILL_USAGE_SUMMARY;
TRUNCATE TABLE TMP_BILL_USAGE_RATE;

-- DECLARE ENUMERATION *******************************************************************
DECLARE @CUSTOMER_STATUS_ACTIVE INT,
        @CUSTOMER_STATUS_BLOCKED INT,
        @CUSTOMER_IN_PRODUCTION INT,
        @INVOICE_STATUS_OPEN INT,
        @INVOICE_STATUS_CLOSE INT,
        @FIX_CHARGE_STATUS_OPEN INT,
        @FIX_CHARGE_STATUS_CLOSE INT,
        @DISCOUNT_TYPE_USAGE INT,
        @DISCOUNT_TYPE_AMOUNT INT,
        @INVOICE_ITEM_POWER INT,
        @MAX_DAY_TO_IGNORE_FIXED_AMOUNT INT,
        @NOW DATETIME;

SET @CUSTOMER_STATUS_ACTIVE = 2;
SET @CUSTOMER_STATUS_BLOCKED = 3;
SET @CUSTOMER_IN_PRODUCTION = -1;
SET @INVOICE_STATUS_OPEN = 1;
SET @INVOICE_STATUS_CLOSE = 4;
SET @FIX_CHARGE_STATUS_OPEN = 1;
SET @FIX_CHARGE_STATUS_CLOSE = 4;
SET @DISCOUNT_TYPE_USAGE = 1;
SET @DISCOUNT_TYPE_AMOUNT = 2;
SET @INVOICE_ITEM_POWER = 1;
SET @NOW = GETDATE();

SET @MAX_DAY_TO_IGNORE_FIXED_AMOUNT = ISNULL((SELECT UTILITY_VALUE FROM TBL_UTILITY WHERE UTILITY_ID = 16), 0);

SELECT SEQUENCE_TYPE_ID = SEQUENCE_TYPE_ID,
			SEQUENCE_ID = s.SEQUENCE_ID,
			SEQUENCE_DATE = [DATE],
			[CURRENT_DATE] = [DATE],
			CURRENT_VALUE = [VALUE],
			FORMART = [FORMAT],
			LAST_INVOICE_NO = '                   ',
			IS_BACKDATE = 0
INTO #tmpSequence
FROM dbo.TBL_SEQUENCE_TYPE st
INNER JOIN dbo.TBL_SEQUENCE s ON s.SEQUENCE_ID = st.SEQUENCE_ID
WHERE st.SEQUENCE_TYPE_ID IN (1,2)

DECLARE @INVOICE_TITLE NVARCHAR(200),
        @RUN_ID INT,
        @DATE DATETIME;
SET @DATE = GETDATE();
SET @INVOICE_DATE = dbo.GET_DATE_WITH_CURRENTTIME(@INVOICE_DATE);
SET @INVOICE_TITLE = N'ថ្លៃអគ្គិសនី  ខែ' + CONVERT(NVARCHAR, MONTH(@MONTH)) + N'ឆ្នាំ' + CONVERT(NVARCHAR, YEAR(@MONTH));

--- ExchangeRate
;
WITH c
AS (SELECT CURRENCY_ID,
           EXCHANGE_RATE,
           CREATE_ON,
           rowNo = ROW_NUMBER() OVER (PARTITION BY CURRENCY_ID ORDER BY CREATE_ON DESC)
    FROM dbo.TBL_EXCHANGE_RATE
    WHERE EXCHANGE_CURRENCY_ID = 1 AND CREATE_ON <= @INVOICE_DATE)
SELECT *
INTO #tmpExchangeRate
FROM c
WHERE c.rowNo = 1;

--****************** Sequence when user run back date *************************--
--Thunny 11-03-2021
/*
	Jan-2021 
		INV2101-00001
		-------------
		INV2101-00010
	Feb-2021
		INV2102-00001
		-------------
		INV2102-00010
	run back month or back year reset by the sequence format 
	If back month to Jan-2021
		INV2101-00011,INV2101-00012..............
*/
UPDATE #tmpSequence
SET IS_BACKDATE = CASE 
										WHEN YEAR(@INVOICE_DATE) < YEAR(SEQUENCE_DATE) THEN 1 
										WHEN MONTH(@INVOICE_DATE) < MONTH(SEQUENCE_DATE) AND YEAR(@INVOICE_DATE) = YEAR(SEQUENCE_DATE) THEN 1
										ELSE 0
								END

UPDATE #tmpSequence
SET IS_BACKDATE = CASE 
										WHEN FORMART LIKE '%{Y%'  AND YEAR(@INVOICE_DATE) < YEAR(SEQUENCE_DATE) THEN 1 
										WHEN FORMART LIKE '%{M%'  AND MONTH(@INVOICE_DATE) < MONTH(SEQUENCE_DATE) AND YEAR(@INVOICE_DATE) = YEAR(SEQUENCE_DATE) THEN 1
										ELSE 0
				END

UPDATE #tmpSequence 
	SET LAST_INVOICE_NO =  CASE WHEN FORMART LIKE '%{Y%' THEN  (SELECT TOP 1 INVOICE_NO  FROM dbo.TBL_INVOICE  INV 
																								WHERE  YEAR(INV.INVOICE_DATE) = YEAR(@INVOICE_DATE)  AND  ((SEQUENCE_TYPE_ID = 1 AND  INV.IS_SERVICE_BILL =0 ) OR (SEQUENCE_TYPE_ID =2 AND INV.IS_SERVICE_BILL =1)) ORDER BY INV.INVOICE_ID DESC)
								WHEN FORMART LIKE '%{M%' THEN   (SELECT TOP 1 INVOICE_NO  FROM dbo.TBL_INVOICE  INV 
																								WHERE  MONTH(INVOICE_DATE) = MONTH(@INVOICE_DATE)   AND  YEAR(INV.INVOICE_DATE) = YEAR(@INVOICE_DATE)
																								AND  ( (SEQUENCE_TYPE_ID = 1 AND  INV.IS_SERVICE_BILL =0 ) OR (SEQUENCE_TYPE_ID =2 AND INV.IS_SERVICE_BILL =1)) ORDER BY INV.INVOICE_ID DESC)
							END	
WHERE IS_BACKDATE =1

UPDATE s
SET s.DATE = @INVOICE_DATE,
	   s.VALUE = (SELECT TOP 1 tuple FROM dbo.SPLIT_STRING(LAST_INVOICE_NO,'-') ORDER BY Id DESC)
FROM dbo.TBL_SEQUENCE s
INNER JOIN #tmpSequence ts ON ts.SEQUENCE_ID = s.SEQUENCE_ID
WHERE ts.IS_BACKDATE = 1

--****************** Sequence when user run back date *************************--
--At last reset sequnece back

-- INSERT A RUN BILL RECORD
INSERT INTO TBL_RUN_BILL
VALUES
(@CYCLE_ID, @DATE, @CREATE_BY, @MONTH, 0, 0, 0, 0, 0, 0, 0, 1);

SET @RUN_ID = @@identity;

-- LOG AGING BEFORE RUN BILL
DECLARE @PERIOD INT;
SET @PERIOD = 15;
INSERT INTO TBL_RUN_BILL_AGING
(RUN_ID, DATE, IS_FEFORE, CUSTOMER_ID, CURRENCY_ID, BAL1, BAL2, BAL3, BAL4, TOTAL)
SELECT RUN_ID = @RUN_ID,
       DATE = @DATE,
       IS_BEFORE = 1,
       c.CUSTOMER_ID,
       i.CURRENCY_ID,
       BAL1 = SUM(CASE WHEN DATEDIFF(D, i.INVOICE_DATE, @DATE) BETWEEN @PERIOD * 0 AND @PERIOD * 1 THEN SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT, 0) + ISNULL(aa.ADJUST_AMOUNT, 0) - ISNULL(p.PAY_AMOUNT, 0) ELSE 0 END),
       BAL2 = SUM(CASE WHEN DATEDIFF(D, i.INVOICE_DATE, @DATE) BETWEEN @PERIOD * 1 + 1 AND @PERIOD * 2 THEN SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT, 0) + ISNULL(aa.ADJUST_AMOUNT, 0) - ISNULL(p.PAY_AMOUNT, 0) ELSE 0 END),
       BAL3 = SUM(CASE WHEN DATEDIFF(D, i.INVOICE_DATE, @DATE) BETWEEN @PERIOD * 2 + 1 AND @PERIOD * 3 THEN SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT, 0) + ISNULL(aa.ADJUST_AMOUNT, 0) - ISNULL(p.PAY_AMOUNT, 0) ELSE 0 END),
       BAL4 = SUM(CASE WHEN DATEDIFF(D, i.INVOICE_DATE, @DATE) > @PERIOD * 3 THEN SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT, 0) + ISNULL(aa.ADJUST_AMOUNT, 0) - ISNULL(p.PAY_AMOUNT, 0) ELSE 0 END),
       TOTAL = SUM(SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT, 0) + ISNULL(aa.ADJUST_AMOUNT, 0) - ISNULL(p.PAY_AMOUNT, 0))
FROM TBL_INVOICE i
INNER JOIN TBL_CUSTOMER c ON i.CUSTOMER_ID = c.CUSTOMER_ID
OUTER APPLY
(SELECT SUM(PD.PAY_AMOUNT) AS PAY_AMOUNT
 FROM TBL_PAYMENT_DETAIL PD
 INNER JOIN TBL_PAYMENT P ON P.PAYMENT_ID = PD.PAYMENT_ID
 WHERE P.PAY_DATE < @DATE
       AND INVOICE_ID = i.INVOICE_ID
)p -- PAYMENT
OUTER APPLY
(SELECT SUM(IA.ADJUST_AMOUNT) AS ADJUST_AMOUNT
 FROM TBL_INVOICE_ADJUSTMENT IA
 WHERE INVOICE_ID = i.INVOICE_ID
)ta -- TOTAL ADJUSTMENT
OUTER APPLY
(SELECT SUM(IA.ADJUST_AMOUNT) AS ADJUST_AMOUNT
 FROM TBL_INVOICE_ADJUSTMENT IA
 WHERE IA.CREATE_ON <= @DATE
       AND INVOICE_ID = i.INVOICE_ID
)aa -- ACCUMULATED ADJUSTMENT 	
WHERE i.INVOICE_STATUS NOT IN (3)
      --AND (c.CUSTOMER_TYPE_ID = @CUSTOMER_TYPE_ID OR @CUSTOMER_TYPE_ID = 0)
      AND (INVOICE_DATE <= @DATE)
      AND c.BILLING_CYCLE_ID = @CYCLE_ID --LOG AGIN before RUN_BILL
GROUP BY c.CUSTOMER_ID, i.CURRENCY_ID
HAVING SUM(SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT, 0) + ISNULL(aa.ADJUST_AMOUNT, 0) - ISNULL(p.PAY_AMOUNT, 0)) > 0;
-- END LOG AGING BEFORE RUN

-- RECURRING SERVICE *********************************************************************************
INSERT INTO TBL_INVOICE
SELECT INVOICE_NO = '',
       INVOICE_MONTH = @MONTH,
       METER_CODE = '',
       START_DATE = @MONTH,
       END_DATE = @MONTH,
       INVOICE_DATE = @INVOICE_DATE,
       START_USAGE = 0,
       END_USAGE = 0,
       CUSTOMER_ID = c.CUSTOMER_ID,
       PAID_AMOUNT = 0,
       TOTAL_USAGE = 0,
       CURRENCY_ID = i.CURRENCY_ID,
       CYCLE_ID = @CYCLE_ID,
       DUE_DATE = @DUE_DATE,
       INVOICE_STATUS = 1,
       IS_SERVICE_BILL = 1,
       -- DUMMY AS INVOICE_ITEM_ID
       PRINT_COUNT = s.INVOICE_ITEM_ID,
       RUN_ID = @RUN_ID,
       FORWARD_AMOUNT = 0,
       TOTAL_AMOUNT = QTY * i.PRICE,
       SETTLE_AMOUNT = dbo.ROUND_BY_CURRENCY(QTY * PRICE, i.CURRENCY_ID),
       DISCOUNT_USAGE = 0,
       DISCOUNT_USAGE_NAME = '',
       DISCOUNT_AMOUNT = 0,
       DISCOUNT_AMOUNT_NAME = '',
       INVOICE_TITLE = INVOICE_ITEM_NAME,
       START_PAY_DATE = @START_PAY_DATE,
       PRICE_ID = 0,
       PRICE = 0,
       BASED_PRICE = 0,
       ROW_DATE = @NOW,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       EXCHANGE_RATE = ISNULL(r.EXCHANGE_RATE, 1),
       EXCHANGE_RATE_DATE = ISNULL(r.CREATE_ON, GETDATE())
FROM TBL_CUSTOMER c
INNER JOIN TBL_CUSTOMER_SERVICE s ON c.CUSTOMER_ID = s.CUSTOMER_ID
INNER JOIN TBL_INVOICE_ITEM i ON i.INVOICE_ITEM_ID = s.INVOICE_ITEM_ID
LEFT JOIN #tmpExchangeRate r ON r.CURRENCY_ID = i.CURRENCY_ID
WHERE c.BILLING_CYCLE_ID = @CYCLE_ID
      AND c.STATUS_ID IN (2, 3)
      AND s.IS_ACTIVE = 1
      AND s.NEXT_BILLING_MONTH = @MONTH;

INSERT INTO TBL_INVOICE_DETAIL
SELECT INVOICE_ID = i.INVOICE_ID,
       START_USAGE = 0,
       END_USAGE = 0,
       USAGE = s.QTY,
       PRICE = t.PRICE,
       INVOICE_ITEM_ID = t.INVOICE_ITEM_ID,
       AMOUNT = s.QTY * t.PRICE,
       CHARGE_DESCRIPTION = t.INVOICE_ITEM_NAME,
       REF_NO = '',
       TRAN_DATE = i.INVOICE_DATE,
       EXCHANGE_RATE = ISNULL(r.EXCHANGE_RATE, 1),
       EXCHANGE_RATE_DATE = ISNULL(r.CREATE_ON, GETDATE()),
       TAX_AMOUNT = 0
FROM TBL_INVOICE i
INNER JOIN TBL_CUSTOMER_SERVICE s ON s.INVOICE_ITEM_ID = i.PRINT_COUNT AND i.CUSTOMER_ID = s.CUSTOMER_ID
INNER JOIN TBL_INVOICE_ITEM t ON t.INVOICE_ITEM_ID = s.INVOICE_ITEM_ID
LEFT JOIN #tmpExchangeRate r ON r.CURRENCY_ID = i.CURRENCY_ID
WHERE i.INVOICE_MONTH = @MONTH
      AND s.IS_ACTIVE = 1
      AND i.INVOICE_STATUS <> 3;

UPDATE TBL_CUSTOMER_SERVICE
SET LAST_BILLING_MONTH = @MONTH,
    NEXT_BILLING_MONTH = DATEADD(M, i.RECURRING_MONTH, @MONTH)
FROM TBL_CUSTOMER_SERVICE s
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = s.CUSTOMER_ID
INNER JOIN TBL_INVOICE_ITEM i ON i.INVOICE_ITEM_ID = s.INVOICE_ITEM_ID
WHERE c.BILLING_CYCLE_ID = @CYCLE_ID
      AND c.STATUS_ID IN (2, 3)
      AND s.IS_ACTIVE = 1
      AND s.NEXT_BILLING_MONTH = @MONTH;

-- clear dummy invoice_item_id
UPDATE TBL_INVOICE
SET PRINT_COUNT = 0
WHERE INVOICE_MONTH = @MONTH
      AND IS_SERVICE_BILL = 1
      AND RUN_ID = @RUN_ID;

-- USAGE AND INVOICE *******************************************************************************
-- SNAPSHOT A CURRENT BILLING MONTH USAGE
-- FROM TBL_USAGE TO TEMP TABLE FOR PERFORMANCE
-- FOR METER NEW CYCLE, GET MAX_USAGE TO CAL CULATE TOTAL USAGE
INSERT INTO TMP_BILL_USAGE
SELECT USAGE_ID,
       CUSTOMER_ID = c.CUSTOMER_ID,
       METER_ID,
       START_USAGE,
       END_USAGE,
       TOTAL_USAGE = MULTIPLIER * (CASE
                                       WHEN IS_METER_RENEW_CYCLE = 0 THEN END_USAGE - START_USAGE
                                       ELSE (SELECT CONVERT(INT, REPLICATE('9', LEN(CONVERT(NVARCHAR, CONVERT(INT, START_USAGE)))))) - START_USAGE + END_USAGE + 1
                                   END),
       MULTIPLIER,
       USAGE_CUSTOMER_ID = (CASE WHEN c.USAGE_CUSTOMER_ID = 0 THEN c.CUSTOMER_ID ELSE c.USAGE_CUSTOMER_ID END)
FROM TBL_USAGE u
INNER JOIN TBL_CUSTOMER c ON u.CUSTOMER_ID = c.CUSTOMER_ID
WHERE c.BILLING_CYCLE_ID = @CYCLE_ID
      AND u.USAGE_MONTH = @MONTH
      AND u.COLLECTOR_ID <> 0
      AND c.STATUS_ID IN (2, 3);

-- MAKE SURE ALL INVOICE TOTAL USAGE MUST BE INTEGER
UPDATE TMP_BILL_USAGE
SET TOTAL_USAGE = FLOOR(TOTAL_USAGE);

-- SUMMARIZE USAGE,
-- ONE CUSTOMER HAVE ONLY ONE RECORD
-- IF CUSTOMER HAVE TWO OR MORE USAGE RECORD(S)
--  START USAGE IS THE START OF THE THE FIRST RECORD
--  END USAGE IS THE END OF THE LAST RECORD
--  TOTAL USAGE, IS SUM UP OF ALL RECORD(S)' TOTAL USAGE
INSERT INTO TMP_BILL_USAGE_SUMMARY
SELECT c.CUSTOMER_ID,
       START_USAGE = (SELECT TOP 1 START_USAGE
                      FROM TMP_BILL_USAGE
                      WHERE CUSTOMER_ID = c.CUSTOMER_ID
                      ORDER BY USAGE_ID DESC),
       END_USAGE = (SELECT TOP 1 END_USAGE
                    FROM TMP_BILL_USAGE
                    WHERE CUSTOMER_ID = c.CUSTOMER_ID
                    ORDER BY USAGE_ID DESC),
       TOTAL_USAGE = SUM(u.TOTAL_USAGE),
       DISCOUNT_USAGE = 0,
       DISCOUNT_USAGE_NAME = ''
FROM TMP_BILL_USAGE u
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = u.USAGE_CUSTOMER_ID
GROUP BY c.CUSTOMER_ID;

-- OVERRIDE RE-ACTIVE POWER.
UPDATE sr
SET TOTAL_USAGE = dbo.GET_REACTIVE(r.REACTIVE_RULE_ID, sa.TOTAL_USAGE, sr.TOTAL_USAGE)
FROM TMP_BILL_USAGE_SUMMARY sr
INNER JOIN TBL_CUSTOMER r ON r.CUSTOMER_ID = sr.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER a ON r.INVOICE_CUSTOMER_ID = a.CUSTOMER_ID
INNER JOIN TMP_BILL_USAGE_SUMMARY sa ON sa.CUSTOMER_ID = a.CUSTOMER_ID
WHERE r.IS_REACTIVE = 1;

-- SUMMARY DISCOUNT USAGE FOR CUSTOMER INPRODUCTION
UPDATE TMP_BILL_USAGE_SUMMARY
SET DISCOUNT_USAGE = s.TOTAL_USAGE,
    DISCOUNT_USAGE_NAME = N'ប្រើក្នុងផលិតកម្ម'
FROM TBL_CUSTOMER c
INNER JOIN TMP_BILL_USAGE_SUMMARY s ON s.CUSTOMER_ID = c.CUSTOMER_ID
WHERE CUSTOMER_TYPE_ID = -1
      AND BILLING_CYCLE_ID = @CYCLE_ID;

-- SUMMARY DISCOUNT USAGE FOR DISCOUNT CUSTOMER
UPDATE TMP_BILL_USAGE_SUMMARY
SET DISCOUNT_USAGE = (CASE
                          WHEN IS_PERCENTAGE = 1 THEN CONVERT(INT, s.TOTAL_USAGE * DISCOUNT / 100)
                          ELSE (CASE WHEN s.TOTAL_USAGE > d.DISCOUNT THEN d.DISCOUNT ELSE s.TOTAL_USAGE END)
                      END),
    DISCOUNT_USAGE_NAME = d.DISCOUNT_NAME
FROM TBL_CUSTOMER_DISCOUNT cd
INNER JOIN TMP_BILL_USAGE_SUMMARY s ON s.CUSTOMER_ID = cd.CUSTOMER_ID
INNER JOIN TBL_DISCOUNT d ON d.DISCOUNT_ID = cd.DISCOUNT_ID
WHERE d.DISCOUNT_TYPE_ID = 1
      AND cd.IS_ACTIVE = 1
      AND d.IS_ACTIVE = 1;

-- PRICING

-- STANDARD PRICNG
-- CALCULATE PRICE BASE ON RANK(START & END)
-- AMOUNT = SUM(PRICE * X) WHERE X IS USAGE OF EACH RANK
INSERT INTO TMP_BILL_USAGE_RATE
SELECT u.CUSTOMER_ID,
       u.TOTAL_USAGE,
       p.START_USAGE,
       p.END_USAGE,
       USAGE = (CASE
                    WHEN (u.TOTAL_USAGE - u.DISCOUNT_USAGE) > p.END_USAGE AND p.END_USAGE <> -1 THEN p.END_USAGE - p.START_USAGE + (CASE WHEN p.START_USAGE = 0 THEN 0 ELSE 1 END)
                    ELSE (u.TOTAL_USAGE - u.DISCOUNT_USAGE) - p.START_USAGE + (CASE WHEN p.START_USAGE = 0 THEN 0 ELSE 1 END)
                END),
       PRICE = p.PRICE,
       FIXED_AMOUNT = p.FIXED_AMOUNT,
       IS_ALLOW_FIXED_AMOUNT = p.IS_ALLOW_FIXED_AMOUNT,
       AMOUNT = 0
FROM TMP_BILL_USAGE_SUMMARY u
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = u.CUSTOMER_ID
INNER JOIN TBL_PRICE_DETAIL p ON p.PRICE_ID = c.PRICE_ID
INNER JOIN TBL_PRICE pl ON pl.PRICE_ID = c.PRICE_ID AND pl.IS_STANDARD_RATING = 1
WHERE (u.TOTAL_USAGE - u.DISCOUNT_USAGE) >= p.START_USAGE
ORDER BY u.CUSTOMER_ID;

-- NON-STANDARD PRICING(PRICE DISCRIMINATION)
-- CALCULATE PRICE BASE ON RANK (START & END)
-- AMOUNT = PRICE * TOTAL_USAGE 
DECLARE @PRICE_ID INT,
        @PRICE DECIMAL(18, 5);
DECLARE C CURSOR FOR
SELECT DISTINCT c.PRICE_ID
FROM TMP_BILL_USAGE_SUMMARY u
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = u.CUSTOMER_ID
INNER JOIN TBL_PRICE p ON p.PRICE_ID = c.PRICE_ID
WHERE IS_STANDARD_RATING = 0;
OPEN C;
FETCH NEXT FROM C
INTO @PRICE_ID;
WHILE @@FETCH_STATUS = 0
BEGIN
     INSERT INTO TMP_BILL_USAGE_RATE
     SELECT CUSTOMER_ID = u.CUSTOMER_ID,
            TOTAL_USAGE = (u.TOTAL_USAGE * 0.95),
            START_USAGE = 0,
            END_USAGE = (u.TOTAL_USAGE * 0.95) - u.DISCOUNT_USAGE,
            USAGE = (u.TOTAL_USAGE * 0.95) - u.DISCOUNT_USAGE,
            PRICE = d.PRICE,
            FIXED_AMOUNT = d.FIXED_AMOUNT,
            IS_ALLOW_FIXED_AMOUNT = d.IS_ALLOW_FIXED_AMOUNT,
            AMOUNT = 0
     FROM TMP_BILL_USAGE_SUMMARY u
     INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = u.CUSTOMER_ID
     INNER JOIN TBL_PRICE_DETAIL d ON d.PRICE_ID = c.PRICE_ID
     WHERE c.PRICE_ID = @PRICE_ID 
           AND ((u.TOTAL_USAGE * 0.95) /*-u.DISCOUNT_USAGE*/) BETWEEN d.START_USAGE AND (CASE WHEN d.END_USAGE = -1 THEN 100000000 ELSE d.END_USAGE + 0.99 END);

     INSERT INTO TMP_BILL_USAGE_RATE
     SELECT CUSTOMER_ID = u.CUSTOMER_ID,
            TOTAL_USAGE = (u.TOTAL_USAGE * 0.05),
            START_USAGE = 0,
            END_USAGE = (u.TOTAL_USAGE * 0.05) -u.DISCOUNT_USAGE,
            USAGE = (u.TOTAL_USAGE * 0.05) -u.DISCOUNT_USAGE,
            PRICE = 1600,
            FIXED_AMOUNT = 0,
            IS_ALLOW_FIXED_AMOUNT = 0,
            AMOUNT=0
     FROM TMP_BILL_USAGE_SUMMARY u
     INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = u.CUSTOMER_ID
     INNER JOIN TBL_PRICE_DETAIL d ON d.PRICE_ID = c.PRICE_ID
     WHERE c.PRICE_ID = @PRICE_ID
           AND ((u.TOTAL_USAGE * 0.05) /*-u.DISCOUNT_USAGE*/) BETWEEN d.START_USAGE AND (CASE WHEN d.END_USAGE = -1 THEN 100000000 ELSE d.END_USAGE + 0.99 END);
     FETCH NEXT FROM C
     INTO @PRICE_ID;
END;
CLOSE C;
DEALLOCATE C;

-- UPDATE AMOUNT
UPDATE TMP_BILL_USAGE_RATE
SET AMOUNT = (CASE WHEN TOTAL_USAGE <= END_USAGE AND IS_ALLOW_FIXED_AMOUNT = 1 AND DATEDIFF(d, c.ACTIVATE_DATE, @INVOICE_DATE) > @MAX_DAY_TO_IGNORE_FIXED_AMOUNT THEN FIXED_AMOUNT ELSE PRICE * USAGE END)
FROM TMP_BILL_USAGE_RATE r
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = r.CUSTOMER_ID;

-- INSERT INVOICE
INSERT INTO TBL_INVOICE
SELECT INVOICE_NO = '',
       INVOICE_MONTH = @MONTH,
       METER_CODE = REPLACE(LTRIM(REPLACE(m.METER_CODE, '0', ' ')), ' ', '0'), -- (CASE WHEN m.MULTIPLIER<>1 THEN ' x '+CONVERT(NVARCHAR,m.MULTIPLIER) ELSE '' END),
       START_DATE = @START_DATE,
       END_DATE = @END_DATE,
       INVOICE_DATE = @INVOICE_DATE,
       START_USAGE = u.START_USAGE,
       END_USAGE = u.END_USAGE,
       CUSTOMER_ID = u.CUSTOMER_ID,
       PAID_AMOUNT = 0,
       TOTAL_USAGE = u.TOTAL_USAGE,
       CURRENCY_ID = p.CURRENCY_ID,
       CYCLE_ID = c.BILLING_CYCLE_ID,
       DUE_DATE = @DUE_DATE,
       INVOICE_STATUS = 1,
       IS_SERVICE_BILL = 0,
       PRINT_COUNT = 1,
       RUN_ID = @RUN_ID,
       FORWARD_AMOUNT = 0,
       TOTAL_AMOUNT = 0,
       SETTLE_AMOUNT = 0,
       DISCOUNT_USAGE = u.DISCOUNT_USAGE,
       DISCOUNT_USAGE_NAME = u.DISCOUNT_USAGE_NAME,
       DISCOUNT_AMOUNT = 0,
       DISCOUNT_AMOUNT_NAME = '',
       INVOICE_TITLE = @INVOICE_TITLE,
       START_PAY_DATE = @START_PAY_DATE,
       PRICE_ID = 0,
       PRICE = 0,
       BASED_PRICE = 0,
       ROW_DATE = @NOW,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       EXCHANGE_RATE = ISNULL(r.EXCHANGE_RATE, 1),
       EXCHANGE_RATE_DATE = ISNULL(r.CREATE_ON, GETDATE())
FROM TMP_BILL_USAGE_SUMMARY u
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = u.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_METER cm ON cm.CUSTOMER_ID = c.CUSTOMER_ID AND cm.IS_ACTIVE = 1
INNER JOIN TBL_METER m ON m.METER_ID = cm.METER_ID
INNER JOIN TBL_AREA a ON a.AREA_ID = c.AREA_ID
INNER JOIN TBL_PRICE p ON p.PRICE_ID = c.PRICE_ID
LEFT JOIN #tmpExchangeRate r ON r.CURRENCY_ID = p.CURRENCY_ID
-- SORT BY AREACODE,CUSTOMERID for fakeDB
ORDER BY a.AREA_CODE, c.CUSTOMER_ID;

--- INSERT INVOICE_USAGE
INSERT INTO TBL_INVOICE_USAGE
SELECT INVOICE_ID = i.INVOICE_ID,
       CUSTOMER_ID = c.CUSTOMER_ID,
       METER_ID = m.METER_ID,
       METER_CODE = REPLACE(LTRIM(REPLACE(m.METER_CODE, '0', ' ')), ' ', '0'),
       CUSTOMER_NAME = c.LAST_NAME_KH + ' ' + c.FIRST_NAME_KH,
       START_USAGE = u.START_USAGE,
       END_USAGE = u.END_USAGE,
       MULTIPLIER = u.MULTIPLIER,
       TOTAL_USAGE = u.TOTAL_USAGE
FROM TBL_INVOICE i
INNER JOIN TMP_BILL_USAGE u ON i.CUSTOMER_ID = u.USAGE_CUSTOMER_ID
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = u.CUSTOMER_ID
INNER JOIN TBL_METER m ON m.METER_ID = u.METER_ID
WHERE i.RUN_ID = @RUN_ID
      AND i.IS_SERVICE_BILL = 0;

INSERT INTO TBL_INVOICE_DETAIL
SELECT INVOICE_ID = i.INVOICE_ID,
       START_USAGE = u.START_USAGE,
       END_USAGE = u.START_USAGE + (CASE WHEN u.START_USAGE = 0 THEN u.USAGE ELSE u.USAGE - 1 END),
       USAGE = u.USAGE,
       PRICE = u.PRICE,
       INVOICE_ITEM_ID = 1,
       AMOUNT = u.AMOUNT,
       CHARGE_DESCRIPTION = '',
       REF_NO = '',
       TRAN_DATE = i.INVOICE_DATE,
       EXCHANGE_RATE = ISNULL(r.EXCHANGE_RATE, 1),
       EXCHANGE_RATE_DATE = ISNULL(r.CREATE_ON, GETDATE()),
       TAX_AMOUNT = 0
FROM TBL_INVOICE i
INNER JOIN TMP_BILL_USAGE_RATE u ON i.CUSTOMER_ID = u.CUSTOMER_ID
LEFT JOIN #tmpExchangeRate r ON r.CURRENCY_ID = i.CURRENCY_ID
WHERE i.INVOICE_MONTH = @MONTH
      AND i.RUN_ID = @RUN_ID
      AND i.IS_SERVICE_BILL = 0
ORDER BY u.PRICE DESC;

--
-- PRICE EXTRA CHARGE 
--

-- TEMPORARY UPDATE INVOICE_AMOUNT
UPDATE TBL_INVOICE
SET TOTAL_AMOUNT = x.TOTAL_AMOUNT
FROM TBL_INVOICE i
INNER JOIN
(SELECT v.INVOICE_ID,
        SUM(AMOUNT) AS TOTAL_AMOUNT
 FROM TBL_INVOICE_DETAIL d
 INNER JOIN TBL_INVOICE v ON v.INVOICE_ID = d.INVOICE_ID
 WHERE v.RUN_ID = @RUN_ID
       AND v.IS_SERVICE_BILL = 0
 GROUP BY v.INVOICE_ID
)x ON x.INVOICE_ID = i.INVOICE_ID;

DECLARE @EXTRA_CHARGE_ID INT;
DECLARE C CURSOR FOR
SELECT DISTINCT
       c.PRICE_ID,
       g.EXTRA_CHARGE_ID
FROM TMP_BILL_USAGE_SUMMARY u
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = u.CUSTOMER_ID
INNER JOIN TBL_PRICE p ON p.PRICE_ID = c.PRICE_ID
INNER JOIN TBL_PRICE_EXTRA_CHARGE g ON g.PRICE_ID = p.PRICE_ID AND g.IS_ACTIVE = 1;
OPEN C;
FETCH NEXT FROM C
INTO @PRICE_ID,
     @EXTRA_CHARGE_ID;
WHILE @@FETCH_STATUS = 0
BEGIN
    -- MERGE INVOICE
    INSERT INTO TBL_INVOICE_DETAIL
    SELECT INVOICE_ID = i.INVOICE_ID,
           START_USAGE = 0,
           END_USAGE = 0,
           USAGE = 1,
           PRICE = CASE WHEN g.CHARGE_BY_PERCENTAGE = 1 THEN g.CHARGE_VALUE * TOTAL_AMOUNT / 100.00 ELSE g.CHARGE_VALUE END,
           INVOICE_ITEM_ID = g.INVOICE_ITEM_ID,
           AMOUNT = CASE WHEN g.CHARGE_BY_PERCENTAGE = 1 THEN g.CHARGE_VALUE * TOTAL_AMOUNT / 100.00 ELSE g.CHARGE_VALUE END,
           CHARGE_DESCRIPTION = g.CHARGE_DESCRIPTION,
           REF_NO = '',
           TRAN_DATE = i.INVOICE_DATE,
           EXCHANGE_RATE = ISNULL(r.EXCHANGE_RATE, 1),
           EXCHANGE_RATE_DATE = ISNULL(r.CREATE_ON, GETDATE()),
           TAX_AMOUNT = 0
    FROM TBL_INVOICE i
    INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = i.CUSTOMER_ID
    INNER JOIN TBL_PRICE p ON p.PRICE_ID = c.PRICE_ID
    INNER JOIN TBL_PRICE_EXTRA_CHARGE g ON g.PRICE_ID = c.PRICE_ID
    LEFT JOIN #tmpExchangeRate r ON r.CURRENCY_ID = i.CURRENCY_ID
    WHERE i.RUN_ID = @RUN_ID
          AND IS_SERVICE_BILL = 0
          AND g.IS_ACTIVE = 1
          AND g.EXTRA_CHARGE_ID = @EXTRA_CHARGE_ID
          AND i.TOTAL_AMOUNT BETWEEN g.CHARGE_AFTER AND g.CHARGE_BEFORE
          AND g.IS_MERGE_INVOICE = 1;

    -- NOT MERGE INVOICE
    INSERT INTO TBL_INVOICE
    SELECT INVOICE_NO = '',
           INVOICE_MONTH = @MONTH,
           METER_CODE = '',
           START_DATE = @MONTH,
           END_DATE = @MONTH,
           INVOICE_DATE = @INVOICE_DATE,
           START_USAGE = 0,
           END_USAGE = 0,
           CUSTOMER_ID = c.CUSTOMER_ID,
           PAID_AMOUNT = 0,
           TOTAL_USAGE = 0,
           CURRENCY_ID = p.CURRENCY_ID,
           CYCLE_ID = INVOICE_ITEM_ID, -- dummy
           DUE_DATE = @DUE_DATE,
           INVOICE_STATUS = 1,
           IS_SERVICE_BILL = 1,
           PRINT_COUNT = 0,
           RUN_ID = -9999, --- dummy
           FORWARD_AMOUNT = 0,
           TOTAL_AMOUNT = CASE WHEN g.CHARGE_BY_PERCENTAGE = 1 THEN g.CHARGE_VALUE * TOTAL_AMOUNT / 100.00 ELSE g.CHARGE_VALUE END,
           SETTLE_AMOUNT = CASE WHEN g.CHARGE_BY_PERCENTAGE = 1 THEN g.CHARGE_VALUE * TOTAL_AMOUNT / 100.00 ELSE g.CHARGE_VALUE END,
           DISCOUNT_USAGE = 0,
           DISCOUNT_USAGE_NAME = '',
           DISCOUNT_AMOUNT = 0,
           DISCOUNT_AMOUNT_NAME = '',
           INVOICE_TITLE = g.CHARGE_DESCRIPTION,
           START_PAY_DATE = @START_PAY_DATE,
           PRICE_ID = 0,
           PRICE = 0,
           BASED_PRICE = 0,
           ROW_DATE = @NOW,
           c.CUSTOMER_CONNECTION_TYPE_ID,
           EXCHANGE_RATE = i.EXCHANGE_RATE,
           i.EXCHANGE_RATE_DATE
    FROM TBL_INVOICE i
    INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = i.CUSTOMER_ID
    INNER JOIN TBL_PRICE p ON p.PRICE_ID = c.PRICE_ID
    INNER JOIN TBL_PRICE_EXTRA_CHARGE g ON g.PRICE_ID = c.PRICE_ID
    WHERE i.RUN_ID = @RUN_ID
          AND IS_SERVICE_BILL = 0
          AND g.IS_ACTIVE = 1
          AND g.EXTRA_CHARGE_ID = @EXTRA_CHARGE_ID
          AND i.TOTAL_AMOUNT BETWEEN g.CHARGE_AFTER AND g.CHARGE_BEFORE
          AND g.IS_MERGE_INVOICE = 0;

    INSERT INTO TBL_INVOICE_DETAIL
    SELECT INVOICE_ID,
           START_USAGE = 0,
           END_USAGE = 0,
           USAGE = 1,
           PRICE = SETTLE_AMOUNT,
           INVOICE_ITEM_ID = CYCLE_ID,
           AMOUNT = SETTLE_AMOUNT,
           CHARGE_DESCRIPTION = INVOICE_TITLE,
           REF_NO = '',
           TRAN_DATE = i.INVOICE_DATE,
           EXCHANGE_RATE = ISNULL(r.EXCHANGE_RATE, 1),
           EXCHANGE_RATE_DATE = ISNULL(r.CREATE_ON, GETDATE()),
           TAX_AMOUNT = 0
    FROM TBL_INVOICE i
    LEFT JOIN #tmpExchangeRate r ON r.CURRENCY_ID = i.CURRENCY_ID
    WHERE RUN_ID = -9999;

    -- clear dummy 
    UPDATE TBL_INVOICE
    SET CYCLE_ID = @CYCLE_ID,
        RUN_ID = @RUN_ID
    WHERE RUN_ID = -9999;

    FETCH NEXT FROM C
    INTO @PRICE_ID,
         @EXTRA_CHARGE_ID;
END;
CLOSE C;
DEALLOCATE C;
------------ END PRICE EXTRA CHARGE ----------------- 

-- UPDATE PRICING - RECORD PRICE FOR EASY REPORT CACULATION.
SELECT p.PRICE_ID,
       BASED_PRICE = ISNULL(x.PRICE, 0)
INTO #TMP_PRICE
FROM TBL_PRICE p
OUTER APPLY
(SELECT TOP 1 PRICE
 FROM TBL_PRICE_DETAIL
 WHERE PRICE_ID = p.PRICE_ID
 ORDER BY START_USAGE DESC
)x;

UPDATE TBL_INVOICE
SET PRICE_ID = c.PRICE_ID,
    BASED_PRICE = ISNULL(b.BASED_PRICE, 0),
    PRICE = ISNULL(d.PRICE, 0)
FROM TBL_INVOICE i
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = i.CUSTOMER_ID
INNER JOIN #TMP_PRICE b ON b.PRICE_ID = c.PRICE_ID
OUTER APPLY
(SELECT TOP 1 PRICE
 FROM TBL_INVOICE_DETAIL
 WHERE INVOICE_ID = i.INVOICE_ID
 ORDER BY PRICE
)d
WHERE i.RUN_ID = @RUN_ID;

-- UDPATE INVOICE_NO (for invoice bill)
UPDATE TBL_INVOICE
SET INVOICE_NO = x.INVOICE_NO
FROM TBL_INVOICE i
INNER JOIN
(SELECT INVOICE_ID,
        INVOICE_NO = dbo.GET_SEQUENCE((SELECT SEQUENCE_ID FROM #tmpSequence WHERE SEQUENCE_TYPE_ID = 1), ROW_NUMBER() OVER (ORDER BY INVOICE_ID) - 1, @INVOICE_DATE)
 FROM TBL_INVOICE
 WHERE RUN_ID = @RUN_ID
       AND INVOICE_NO = ''
       AND IS_SERVICE_BILL = 0
)x ON x.INVOICE_ID = i.INVOICE_ID;

-- UDPATE SEQUENCE
UPDATE TBL_SEQUENCE
SET VALUE = CASE
                WHEN FORMAT LIKE '%{Y%' AND DATEDIFF(YEAR, DATE, @INVOICE_DATE) <> 0 THEN @@ROWCOUNT
                WHEN FORMAT LIKE '%{M%' AND DATEDIFF(MONTH, DATE, @INVOICE_DATE) <> 0 THEN @@ROWCOUNT
                ELSE VALUE + @@ROWCOUNT
            END,
    DATE = @INVOICE_DATE
WHERE SEQUENCE_ID = (SELECT SEQUENCE_ID FROM #tmpSequence WHERE SEQUENCE_TYPE_ID = 1);

-- UDPATE INVOICE_NO (for service bill)
UPDATE TBL_INVOICE
SET INVOICE_NO = x.INVOICE_NO
FROM TBL_INVOICE i
INNER JOIN
(SELECT INVOICE_ID, INVOICE_NO = dbo.GET_SEQUENCE((SELECT SEQUENCE_ID FROM #tmpSequence WHERE SEQUENCE_TYPE_ID = 2), ROW_NUMBER() OVER (ORDER BY INVOICE_ID) - 1, @INVOICE_DATE)
 FROM TBL_INVOICE
 WHERE RUN_ID = @RUN_ID
       AND INVOICE_NO = ''
       AND IS_SERVICE_BILL = 1
)x ON x.INVOICE_ID = i.INVOICE_ID;

-- UDPATE SEQUENCE
UPDATE TBL_SEQUENCE
SET VALUE = CASE
                WHEN FORMAT LIKE '%{Y%' AND DATEDIFF(YEAR, DATE, @INVOICE_DATE) <> 0 THEN @@ROWCOUNT
                WHEN FORMAT LIKE '%{M%' AND DATEDIFF(MONTH, DATE, @INVOICE_DATE) <> 0 THEN @@ROWCOUNT
                ELSE VALUE + @@ROWCOUNT
            END,
    DATE = @DATE
WHERE SEQUENCE_ID = (SELECT SEQUENCE_ID FROM #tmpSequence WHERE SEQUENCE_TYPE_ID = 2);

-- UPDATE REF_NO IN INVOICE_DETAIL
UPDATE id
SET id.REF_NO = i.INVOICE_NO
FROM dbo.TBL_INVOICE i
INNER JOIN dbo.TBL_INVOICE_DETAIL id ON id.INVOICE_ID = i.INVOICE_ID
WHERE i.RUN_ID = @RUN_ID;

-- CALCULATE BALANCE FOWARD!
UPDATE TBL_INVOICE
SET FORWARD_AMOUNT = x.FORWARD_AMOUNT
FROM TBL_INVOICE i
CROSS APPLY
(SELECT TOP 1 FORWARD_AMOUNT = TOTAL_AMOUNT - SETTLE_AMOUNT
 FROM TBL_INVOICE
 WHERE INVOICE_ID < i.INVOICE_ID
       AND IS_SERVICE_BILL = 0
       AND CUSTOMER_ID = i.CUSTOMER_ID
       AND CURRENCY_ID = i.CURRENCY_ID
       AND INVOICE_STATUS <> 3
 ORDER BY INVOICE_ID DESC
)x;

-- UPDATE TOTAL AMOUNT
UPDATE TBL_INVOICE
SET TOTAL_AMOUNT = x.TOTAL_AMOUNT
FROM TBL_INVOICE i
INNER JOIN
(SELECT v.INVOICE_ID,
        SUM(AMOUNT) AS TOTAL_AMOUNT
 FROM TBL_INVOICE_DETAIL d
 INNER JOIN TBL_INVOICE v ON v.INVOICE_ID = d.INVOICE_ID
 WHERE v.RUN_ID = @RUN_ID
       AND v.IS_SERVICE_BILL = 0
 GROUP BY v.INVOICE_ID
)x ON x.INVOICE_ID = i.INVOICE_ID;

-- UPDATE TOTAL AMOUNT
UPDATE TBL_INVOICE
SET TOTAL_AMOUNT = TOTAL_AMOUNT + FORWARD_AMOUNT
WHERE RUN_ID = @RUN_ID
      AND IS_SERVICE_BILL = 0;

-- UPDATE SETTLE AMOUNT.
UPDATE TBL_INVOICE
SET SETTLE_AMOUNT = dbo.ROUND_BY_CURRENCY(TOTAL_AMOUNT, CURRENCY_ID)
WHERE RUN_ID = @RUN_ID
      AND IS_SERVICE_BILL = 0;

-- IF NOT KEEP_BILLING_REMAIN_AMOUNT 
-- NOT ALLOW FORWARD LAST REMAIN AMOUNT TO NEXT BILLING 
IF NOT EXISTS (SELECT * FROM TBL_UTILITY WHERE UTILITY_ID = 29 AND UPPER(UTILITY_VALUE) IN ('1', 'TRUE', 'ON'))
	BEGIN
		UPDATE TBL_INVOICE
		SET TOTAL_AMOUNT = SETTLE_AMOUNT
		WHERE RUN_ID = @RUN_ID
			  AND IS_SERVICE_BILL = 0;
	END;

-- UPDATE INVOICE STATUS 
UPDATE TBL_INVOICE
SET INVOICE_STATUS = 4
WHERE RUN_ID = @RUN_ID
      AND IS_SERVICE_BILL = 0
      AND SETTLE_AMOUNT = 0;

-- LOG BILLING's CUSTOMER LOG
INSERT INTO TBL_RUN_BILL_INVOICE
SELECT RUN_ID = @RUN_ID,
       INVOICE_ID = i.INVOICE_ID,
       CUSTOMER_ID = i.CUSTOMER_ID,
       CUSTOMER_CODE = c.CUSTOMER_CODE,
       INVOICE_NO = i.INVOICE_NO,
       METER_CODE = i.METER_CODE,
       START_USAGE = i.START_USAGE,
       END_USAGE = i.END_USAGE,
       TOTAL_USAGE = i.TOTAL_USAGE,
       DISCOUNT_USAGE = i.DISCOUNT_USAGE,
       SETTLE_AMOUNT = i.SETTLE_AMOUNT,
       AR_BEFORE_RUN = 0, -- NO ANY AR BEFORE RUN.
       CUSTOMER_STATUS_ID = c.STATUS_ID,
       i.CURRENCY_ID
FROM TBL_INVOICE i
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = i.CUSTOMER_ID
WHERE i.IS_SERVICE_BILL = 0
      AND i.RUN_ID = @RUN_ID;

-- LOG AGING AFTER RUN
SET @DATE = GETDATE();
INSERT INTO TBL_RUN_BILL_AGING
(RUN_ID, DATE, IS_FEFORE, CUSTOMER_ID, CURRENCY_ID, BAL1, BAL2, BAL3, BAL4, TOTAL)
SELECT RUN_ID = @RUN_ID,
       DATE = @DATE,
       IS_BEFORE = 0,
       c.CUSTOMER_ID,
       i.CURRENCY_ID,
       BAL1 = SUM(CASE WHEN DATEDIFF(D, i.INVOICE_DATE, @DATE) BETWEEN @PERIOD * 0 AND @PERIOD * 1 THEN SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT, 0) + ISNULL(aa.ADJUST_AMOUNT, 0) - ISNULL(p.PAY_AMOUNT, 0) ELSE 0 END),
       BAL2 = SUM(CASE WHEN DATEDIFF(D, i.INVOICE_DATE, @DATE) BETWEEN @PERIOD * 1 + 1 AND @PERIOD * 2 THEN SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT, 0) + ISNULL(aa.ADJUST_AMOUNT, 0) - ISNULL(p.PAY_AMOUNT, 0) ELSE 0 END),
       BAL3 = SUM(CASE WHEN DATEDIFF(D, i.INVOICE_DATE, @DATE) BETWEEN @PERIOD * 2 + 1 AND @PERIOD * 3 THEN SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT, 0) + ISNULL(aa.ADJUST_AMOUNT, 0) - ISNULL(p.PAY_AMOUNT, 0) ELSE 0 END),
       BAL4 = SUM(CASE WHEN DATEDIFF(D, i.INVOICE_DATE, @DATE) > @PERIOD * 3 THEN SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT, 0) + ISNULL(aa.ADJUST_AMOUNT, 0) - ISNULL(p.PAY_AMOUNT, 0) ELSE 0 END),
       TOTAL = SUM(SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT, 0) + ISNULL(aa.ADJUST_AMOUNT, 0) - ISNULL(p.PAY_AMOUNT, 0))
FROM TBL_INVOICE i
INNER JOIN TBL_CUSTOMER c ON i.CUSTOMER_ID = c.CUSTOMER_ID
OUTER APPLY
(SELECT SUM(PD.PAY_AMOUNT) AS PAY_AMOUNT
 FROM TBL_PAYMENT_DETAIL PD
 INNER JOIN TBL_PAYMENT P ON P.PAYMENT_ID = PD.PAYMENT_ID
 WHERE P.PAY_DATE < @DATE
       AND INVOICE_ID = i.INVOICE_ID
)p -- PAYMENT
OUTER APPLY
(SELECT SUM(IA.ADJUST_AMOUNT) AS ADJUST_AMOUNT
 FROM TBL_INVOICE_ADJUSTMENT IA
 WHERE INVOICE_ID = i.INVOICE_ID
)ta -- TOTAL ADJUSTMENT
OUTER APPLY
(SELECT SUM(IA.ADJUST_AMOUNT) AS ADJUST_AMOUNT
 FROM TBL_INVOICE_ADJUSTMENT IA
 WHERE IA.CREATE_ON <= @DATE
       AND INVOICE_ID = i.INVOICE_ID
)aa -- ACCUMULATED ADJUSTMENT 	
WHERE i.INVOICE_STATUS NOT IN (3)
      --AND (c.CUSTOMER_TYPE_ID = @CUSTOMER_TYPE_ID OR @CUSTOMER_TYPE_ID = 0)
      AND (INVOICE_DATE <= @DATE)
      AND c.BILLING_CYCLE_ID = @CYCLE_ID --LOG AGIN after RUN_BILL
GROUP BY c.CUSTOMER_ID, i.CURRENCY_ID
HAVING SUM(SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT, 0) + ISNULL(aa.ADJUST_AMOUNT, 0) - ISNULL(p.PAY_AMOUNT, 0)) > 0;

-- RUN BILL's CUSTOMER INFO
UPDATE TBL_RUN_BILL
SET CUSTOMER_TOTAL = ISNULL(x.CUSTOMER_TOTAL, 0),
    CUSTOMER_ACTIVE = ISNULL(x.CUSTOMER_ACTIVE, 0),
    CUSTOMER_BLOCKED = ISNULL(x.CUSTOMER_BLOCKED, 0),
    CUSTOMER_PENDING = ISNULL(x.CUSTOMER_PENDING, 0),
    CUSTOMER_CLOSE = ISNULL(x.CUSTOMER_CLOSE, 0)
FROM TBL_RUN_BILL r
CROSS APPLY
(SELECT CUSTOMER_TOTAL = SUM(CASE WHEN STATUS_ID <> 5 THEN 1 ELSE 0 END),
        CUSTOMER_ACTIVE = SUM(CASE WHEN STATUS_ID = 2 THEN 1 ELSE 0 END),
        CUSTOMER_BLOCKED = SUM(CASE WHEN STATUS_ID = 3 THEN 1 ELSE 0 END),
        CUSTOMER_PENDING = SUM(CASE WHEN STATUS_ID = 1 THEN 1 ELSE 0 END),
        CUSTOMER_CLOSE = SUM(CASE WHEN STATUS_ID = 4 THEN 1 ELSE 0 END)
 FROM TBL_CUSTOMER
 WHERE IS_POST_PAID = 1
)x
WHERE r.RUN_ID = @RUN_ID;

-- RUN BILL's INVOICE INFO
INSERT INTO TBL_RUN_BILL_AMOUNT
SELECT RUN_ID,
       CURRENCY_ID,
       TOTAL_INVOICE = COUNT(INVOICE_ID),
       TOTAL_POWER = SUM(TOTAL_USAGE),
       TOTAL_AMOUNT = SUM(SETTLE_AMOUNT),
       TOTAL_INVOICE_ZERO_USAGE = SUM(CASE WHEN TOTAL_USAGE = 0 THEN 1 ELSE 0 END),
       TOTAL_INVOICE_ZERO_AMOUNT = SUM(CASE WHEN SETTLE_AMOUNT = 0 THEN 1 ELSE 0 END),
       AR_BEFORE_RUN = 0,
       AR_AFTER_RUN = 0
FROM TBL_INVOICE i
WHERE IS_SERVICE_BILL = 0
      AND RUN_ID = @RUN_ID
GROUP BY RUN_ID, CURRENCY_ID;

UPDATE TBL_RUN_BILL
SET TOTAL_POWER = t.TOTAL_POWER,
    TOTAL_INVOICE = t.TOTAL_INVOICE
FROM TBL_RUN_BILL b
OUTER APPLY
(SELECT TOTAL_POWER = SUM(TOTAL_POWER),
        TOTAL_INVOICE = SUM(TOTAL_INVOICE)
 FROM TBL_RUN_BILL_AMOUNT
 WHERE RUN_ID = b.RUN_ID
)t
WHERE RUN_ID = @RUN_ID;

--RUN BILL's 
UPDATE TBL_RUN_BILL_AMOUNT
SET AR_BEFORE_RUN = ISNULL(x.AR_BEFORE_RUN, 0),
    AR_AFTER_RUN = ISNULL(x.AR_AFTER_RUN, 0)
FROM TBL_RUN_BILL_AMOUNT r
CROSS APPLY
(SELECT CURRENCY_ID,
        AR_BEFORE_RUN = SUM(CASE WHEN IS_FEFORE = 1 THEN TOTAL ELSE 0 END),
        AR_AFTER_RUN = SUM(CASE WHEN IS_FEFORE = 0 THEN TOTAL ELSE 0 END)
 FROM TBL_RUN_BILL_AGING
 WHERE RUN_ID = r.RUN_ID
 GROUP BY CURRENCY_ID
)x
WHERE r.RUN_ID = @RUN_ID
      AND r.CURRENCY_ID = x.CURRENCY_ID;
---- END OF RUN_BILL

--RESET BILLING SEQUENCE IF BACK-DATE
UPDATE s
SET VALUE = CURRENT_VALUE,
	[DATE] = [CURRENT_DATE]
FROM dbo.TBL_SEQUENCE s
INNER JOIN #tmpSequence ts ON ts.SEQUENCE_ID = s.SEQUENCE_ID
WHERE ts.IS_BACKDATE = 1

GO

IF OBJECT_ID('RUN_BILL_NORMAL') IS NOT NULL
	DROP PROC dbo.RUN_BILL_NORMAL
GO

CREATE PROC RUN_BILL_NORMAL
    @MONTH DATETIME = '2020-2-1',
    @START_DATE DATETIME = '2020-1-1',
    @END_DATE DATETIME = '2021-1-31',
    @CYCLE_ID INT = 1,
    @CREATE_BY NVARCHAR(100) = 'ADMIN',
    @DUE_DATE DATETIME = '2021-2-20',
    @START_PAY_DATE DATETIME = '2011-2-01',
    @INVOICE_DATE DATETIME = '2020-02-01'
WITH ENCRYPTION
AS
/*
BY	: RY RITH
SINCE: 2011-01-17 
SINCE: 2011-02-25 Update Discount
SINCE: 2011-03-03 Update Service Bill
SINCE: 2011-03-31 Update Utility Value
SINCE: 2011-04-06 Filter Close and Delete Custoemr 
SINCE: 2011-04-20 Update Filter Error(Change From CustomerType to Status)
SINCE: 2011-04-25 Add @DUE_DATE 
SINEC: 2011-07-25 o Revise Invoice No
				  o Add Invoice Order by customer for FakeDB
				  o Add log billing summary
				  o Add log AR before and after run bill.
				  o Add log Invoice(s)
SINCE: 2011-12-22 multiplier support
SINCE: 2011-12-27 3-phase meter support
SINCE: 2012-01-18 recurring service
SINCE: 2012-01-25 add keep remain billing amount.
SINCE: 2011-01-27 fix sequence.
SINCE: 2012-03-02 fix keep remain billing amount (reverse condition).
SINCE: 2012-03-02 fix unlimit (change from 1m -> 100m)
SINCE: 2012-08-27 ... 
SINCE: 2014-03-21 Rith, fix inserting duplicate value TBL_RUN_BILL_INVOICE
SINCE: 2014-03-21 Rith, add Reactive Power Calculation.
SINCE: 2016-01-16 Rith, add Price extra charge.
SINCE: 2016-03-29 Rith, add Price, based price history
SINCE: 2016-04-23 Rith, floor TOTAL_USAGE
                        discount calculation based on TOTAL_USAGE (before TOTAL_USAGE-DISCOUNT_USAGE)
SINCE: 2018-03-02 Smey, LOG AGIN before and After RUN_BILL
SINCE: 2018-06-13 Smey, add ROW_DATE to TBL_INVOICE
SINCE: 2019-03-04 Smey, add CUSTOMER_CONN
SINCE: 2020-09-10 Khorn, seperate sequence invoice bill and service
*/

TRUNCATE TABLE TMP_BILL_USAGE;
TRUNCATE TABLE TMP_BILL_USAGE_SUMMARY;
TRUNCATE TABLE TMP_BILL_USAGE_RATE;

-- DECLARE ENUMERATION *******************************************************************
DECLARE @CUSTOMER_STATUS_ACTIVE INT,
        @CUSTOMER_STATUS_BLOCKED INT,
        @CUSTOMER_IN_PRODUCTION INT,
        @INVOICE_STATUS_OPEN INT,
        @INVOICE_STATUS_CLOSE INT,
        @FIX_CHARGE_STATUS_OPEN INT,
        @FIX_CHARGE_STATUS_CLOSE INT,
        @DISCOUNT_TYPE_USAGE INT,
        @DISCOUNT_TYPE_AMOUNT INT,
        @INVOICE_ITEM_POWER INT,
        @MAX_DAY_TO_IGNORE_FIXED_AMOUNT INT,
        @NOW DATETIME;

SET @CUSTOMER_STATUS_ACTIVE = 2;
SET @CUSTOMER_STATUS_BLOCKED = 3;
SET @CUSTOMER_IN_PRODUCTION = -1;
SET @INVOICE_STATUS_OPEN = 1;
SET @INVOICE_STATUS_CLOSE = 4;
SET @FIX_CHARGE_STATUS_OPEN = 1;
SET @FIX_CHARGE_STATUS_CLOSE = 4;
SET @DISCOUNT_TYPE_USAGE = 1;
SET @DISCOUNT_TYPE_AMOUNT = 2;
SET @INVOICE_ITEM_POWER = 1;
SET @NOW = GETDATE();

SET @MAX_DAY_TO_IGNORE_FIXED_AMOUNT = ISNULL((SELECT UTILITY_VALUE FROM TBL_UTILITY WHERE UTILITY_ID = 16), 0);

SELECT SEQUENCE_TYPE_ID = SEQUENCE_TYPE_ID,
			SEQUENCE_ID = s.SEQUENCE_ID,
			SEQUENCE_DATE = [DATE],
			[CURRENT_DATE] = [DATE],
			CURRENT_VALUE = [VALUE],
			FORMART = [FORMAT],
			[LAST_INVOICE_NO] = '                            ',
			IS_BACKDATE = 0
INTO #tmpSequence
FROM dbo.TBL_SEQUENCE_TYPE st
INNER JOIN dbo.TBL_SEQUENCE s ON s.SEQUENCE_ID = st.SEQUENCE_ID
WHERE st.SEQUENCE_TYPE_ID IN (1,2)

DECLARE @INVOICE_TITLE NVARCHAR(200),
        @RUN_ID INT,
        @DATE DATETIME;
SET @DATE = GETDATE();
SET @INVOICE_DATE = dbo.GET_DATE_WITH_CURRENTTIME(@INVOICE_DATE);
SET @INVOICE_TITLE = N'ថ្លៃអគ្គិសនី  ខែ' + CONVERT(NVARCHAR, MONTH(@MONTH)) + N'ឆ្នាំ' + CONVERT(NVARCHAR, YEAR(@MONTH));

--- ExchangeRate
;
WITH c
AS (SELECT CURRENCY_ID,
           EXCHANGE_RATE,
           CREATE_ON,
           rowNo = ROW_NUMBER() OVER (PARTITION BY CURRENCY_ID ORDER BY CREATE_ON DESC)
    FROM dbo.TBL_EXCHANGE_RATE
    WHERE EXCHANGE_CURRENCY_ID = 1 AND CREATE_ON <= @INVOICE_DATE)
SELECT *
INTO #tmpExchangeRate
FROM c
WHERE c.rowNo = 1;

--****************** Sequence when user run back date *************************--
--Thunny 11-03-2021
/*
	Jan-2021 
		INV2101-00001
		-------------
		INV2101-00010
	Feb-2021
		INV2102-00001
		-------------
		INV2102-00010
	run back month or back year reset by the sequence format 
	If back month to Jan-2021
		INV2101-00011,INV2101-00012..............
*/ 
UPDATE #tmpSequence
SET IS_BACKDATE = CASE 
										WHEN FORMART LIKE '%{Y%'  AND YEAR(@INVOICE_DATE) < YEAR(SEQUENCE_DATE) THEN 1 
										WHEN FORMART LIKE '%{M%'  AND MONTH(@INVOICE_DATE) < MONTH(SEQUENCE_DATE) AND YEAR(@INVOICE_DATE) = YEAR(SEQUENCE_DATE) THEN 1
										ELSE 0
				END

UPDATE #tmpSequence 
	SET LAST_INVOICE_NO =  CASE WHEN FORMART LIKE '%{Y%' THEN  (SELECT TOP 1 INVOICE_NO  FROM dbo.TBL_INVOICE  INV 
																								WHERE  YEAR(INV.INVOICE_DATE) = YEAR(@INVOICE_DATE)  AND  ((SEQUENCE_TYPE_ID = 1 AND  INV.IS_SERVICE_BILL =0 ) OR (SEQUENCE_TYPE_ID =2 AND INV.IS_SERVICE_BILL =1)) ORDER BY INV.INVOICE_ID DESC)
								WHEN FORMART LIKE '%{M%' THEN   (SELECT TOP 1 INVOICE_NO  FROM dbo.TBL_INVOICE  INV 
																								WHERE  MONTH(INVOICE_DATE) = MONTH(@INVOICE_DATE)   AND  YEAR(INV.INVOICE_DATE) = YEAR(@INVOICE_DATE)
																								AND  ( (SEQUENCE_TYPE_ID = 1 AND  INV.IS_SERVICE_BILL =0 ) OR (SEQUENCE_TYPE_ID =2 AND INV.IS_SERVICE_BILL =1)) ORDER BY INV.INVOICE_ID DESC)
							END	
WHERE IS_BACKDATE =1

UPDATE s
SET s.DATE = @INVOICE_DATE,
	s.VALUE = (SELECT TOP 1 tuple FROM dbo.SPLIT_STRING(LAST_INVOICE_NO,'-') ORDER BY Id DESC)
FROM dbo.TBL_SEQUENCE s
INNER JOIN #tmpSequence ts ON ts.SEQUENCE_ID = s.SEQUENCE_ID AND IS_BACKDATE =1

--****************** Sequence when user run back date *************************--
--At last reset sequnece back

-- INSERT A RUN BILL RECORD
INSERT INTO TBL_RUN_BILL
VALUES
(@CYCLE_ID, @DATE, @CREATE_BY, @MONTH, 0, 0, 0, 0, 0, 0, 0, 1);

SET @RUN_ID = @@identity;

-- LOG AGING BEFORE RUN BILL
DECLARE @PERIOD INT;
SET @PERIOD = 15;
INSERT INTO TBL_RUN_BILL_AGING
(RUN_ID, DATE, IS_FEFORE, CUSTOMER_ID, CURRENCY_ID, BAL1, BAL2, BAL3, BAL4, TOTAL)
SELECT RUN_ID = @RUN_ID,
       DATE = @DATE,
       IS_BEFORE = 1,
       c.CUSTOMER_ID,
       i.CURRENCY_ID,
       BAL1 = SUM(CASE WHEN DATEDIFF(D, i.INVOICE_DATE, @DATE) BETWEEN @PERIOD * 0 AND @PERIOD * 1 THEN SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT, 0) + ISNULL(aa.ADJUST_AMOUNT, 0) - ISNULL(p.PAY_AMOUNT, 0) ELSE 0 END),
       BAL2 = SUM(CASE WHEN DATEDIFF(D, i.INVOICE_DATE, @DATE) BETWEEN @PERIOD * 1 + 1 AND @PERIOD * 2 THEN SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT, 0) + ISNULL(aa.ADJUST_AMOUNT, 0) - ISNULL(p.PAY_AMOUNT, 0) ELSE 0 END),
       BAL3 = SUM(CASE WHEN DATEDIFF(D, i.INVOICE_DATE, @DATE) BETWEEN @PERIOD * 2 + 1 AND @PERIOD * 3 THEN SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT, 0) + ISNULL(aa.ADJUST_AMOUNT, 0) - ISNULL(p.PAY_AMOUNT, 0) ELSE 0 END),
       BAL4 = SUM(CASE WHEN DATEDIFF(D, i.INVOICE_DATE, @DATE) > @PERIOD * 3 THEN SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT, 0) + ISNULL(aa.ADJUST_AMOUNT, 0) - ISNULL(p.PAY_AMOUNT, 0) ELSE 0 END),
       TOTAL = SUM(SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT, 0) + ISNULL(aa.ADJUST_AMOUNT, 0) - ISNULL(p.PAY_AMOUNT, 0))
FROM TBL_INVOICE i
INNER JOIN TBL_CUSTOMER c ON i.CUSTOMER_ID = c.CUSTOMER_ID
OUTER APPLY
(SELECT SUM(PD.PAY_AMOUNT) AS PAY_AMOUNT
 FROM TBL_PAYMENT_DETAIL PD
 INNER JOIN TBL_PAYMENT P ON P.PAYMENT_ID = PD.PAYMENT_ID
 WHERE P.PAY_DATE < @DATE
       AND INVOICE_ID = i.INVOICE_ID
)p -- PAYMENT
OUTER APPLY
(SELECT SUM(IA.ADJUST_AMOUNT) AS ADJUST_AMOUNT
 FROM TBL_INVOICE_ADJUSTMENT IA
 WHERE INVOICE_ID = i.INVOICE_ID
)ta -- TOTAL ADJUSTMENT
OUTER APPLY
(SELECT SUM(IA.ADJUST_AMOUNT) AS ADJUST_AMOUNT
 FROM TBL_INVOICE_ADJUSTMENT IA
 WHERE IA.CREATE_ON <= @DATE
       AND INVOICE_ID = i.INVOICE_ID
)aa -- ACCUMULATED ADJUSTMENT 	
WHERE i.INVOICE_STATUS NOT IN (3)
      --AND (c.CUSTOMER_TYPE_ID = @CUSTOMER_TYPE_ID OR @CUSTOMER_TYPE_ID = 0)
      AND (INVOICE_DATE <= @DATE)
      AND c.BILLING_CYCLE_ID = @CYCLE_ID --LOG AGIN before RUN_BILL
GROUP BY c.CUSTOMER_ID, i.CURRENCY_ID
HAVING SUM(SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT, 0) + ISNULL(aa.ADJUST_AMOUNT, 0) - ISNULL(p.PAY_AMOUNT, 0)) > 0;
-- END LOG AGING BEFORE RUN

-- RECURRING SERVICE *********************************************************************************
INSERT INTO TBL_INVOICE
SELECT INVOICE_NO = '',
       INVOICE_MONTH = @MONTH,
       METER_CODE = '',
       START_DATE = @MONTH,
       END_DATE = @MONTH,
       INVOICE_DATE = @INVOICE_DATE,
       START_USAGE = 0,
       END_USAGE = 0,
       CUSTOMER_ID = c.CUSTOMER_ID,
       PAID_AMOUNT = 0,
       TOTAL_USAGE = 0,
       CURRENCY_ID = i.CURRENCY_ID,
       CYCLE_ID = @CYCLE_ID,
       DUE_DATE = @DUE_DATE,
       INVOICE_STATUS = 1,
       IS_SERVICE_BILL = 1,
       -- DUMMY AS INVOICE_ITEM_ID
       PRINT_COUNT = s.INVOICE_ITEM_ID,
       RUN_ID = @RUN_ID,
       FORWARD_AMOUNT = 0,
       TOTAL_AMOUNT = QTY * i.PRICE,
       SETTLE_AMOUNT = dbo.ROUND_BY_CURRENCY(QTY * PRICE, i.CURRENCY_ID),
       DISCOUNT_USAGE = 0,
       DISCOUNT_USAGE_NAME = '',
       DISCOUNT_AMOUNT = 0,
       DISCOUNT_AMOUNT_NAME = '',
       INVOICE_TITLE = INVOICE_ITEM_NAME,
       START_PAY_DATE = @START_PAY_DATE,
       PRICE_ID = 0,
       PRICE = 0,
       BASED_PRICE = 0,
       ROW_DATE = @NOW,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       EXCHANGE_RATE = ISNULL(r.EXCHANGE_RATE, 1),
       EXCHANGE_RATE_DATE = ISNULL(r.CREATE_ON, GETDATE())
FROM TBL_CUSTOMER c
INNER JOIN TBL_CUSTOMER_SERVICE s ON c.CUSTOMER_ID = s.CUSTOMER_ID
INNER JOIN TBL_INVOICE_ITEM i ON i.INVOICE_ITEM_ID = s.INVOICE_ITEM_ID
LEFT JOIN #tmpExchangeRate r ON r.CURRENCY_ID = i.CURRENCY_ID
WHERE c.BILLING_CYCLE_ID = @CYCLE_ID
      AND c.STATUS_ID IN (2, 3)
      AND s.IS_ACTIVE = 1
      AND s.NEXT_BILLING_MONTH = @MONTH;

INSERT INTO TBL_INVOICE_DETAIL
SELECT INVOICE_ID = i.INVOICE_ID,
       START_USAGE = 0,
       END_USAGE = 0,
       USAGE = s.QTY,
       PRICE = t.PRICE,
       INVOICE_ITEM_ID = t.INVOICE_ITEM_ID,
       AMOUNT = s.QTY * t.PRICE,
       CHARGE_DESCRIPTION = t.INVOICE_ITEM_NAME,
       REF_NO = '',
       TRAN_DATE = i.INVOICE_DATE,
       EXCHANGE_RATE = ISNULL(r.EXCHANGE_RATE, 1),
       EXCHANGE_RATE_DATE = ISNULL(r.CREATE_ON, GETDATE()),
       TAX_AMOUNT = 0
FROM TBL_INVOICE i
INNER JOIN TBL_CUSTOMER_SERVICE s ON s.INVOICE_ITEM_ID = i.PRINT_COUNT AND i.CUSTOMER_ID = s.CUSTOMER_ID
INNER JOIN TBL_INVOICE_ITEM t ON t.INVOICE_ITEM_ID = s.INVOICE_ITEM_ID
LEFT JOIN #tmpExchangeRate r ON r.CURRENCY_ID = i.CURRENCY_ID
WHERE i.INVOICE_MONTH = @MONTH
      AND s.IS_ACTIVE = 1
      AND i.INVOICE_STATUS <> 3;

UPDATE TBL_CUSTOMER_SERVICE
SET LAST_BILLING_MONTH = @MONTH,
    NEXT_BILLING_MONTH = DATEADD(M, i.RECURRING_MONTH, @MONTH)
FROM TBL_CUSTOMER_SERVICE s
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = s.CUSTOMER_ID
INNER JOIN TBL_INVOICE_ITEM i ON i.INVOICE_ITEM_ID = s.INVOICE_ITEM_ID
WHERE c.BILLING_CYCLE_ID = @CYCLE_ID
      AND c.STATUS_ID IN (2, 3)
      AND s.IS_ACTIVE = 1
      AND s.NEXT_BILLING_MONTH = @MONTH;

-- clear dummy invoice_item_id
UPDATE TBL_INVOICE
SET PRINT_COUNT = 0
WHERE INVOICE_MONTH = @MONTH
      AND IS_SERVICE_BILL = 1
      AND RUN_ID = @RUN_ID;

-- USAGE AND INVOICE *******************************************************************************
-- SNAPSHOT A CURRENT BILLING MONTH USAGE
-- FROM TBL_USAGE TO TEMP TABLE FOR PERFORMANCE
-- FOR METER NEW CYCLE, GET MAX_USAGE TO CAL CULATE TOTAL USAGE
INSERT INTO TMP_BILL_USAGE
SELECT USAGE_ID,
       CUSTOMER_ID = c.CUSTOMER_ID,
       METER_ID,
       START_USAGE,
       END_USAGE,
       TOTAL_USAGE = MULTIPLIER * (CASE
                                       WHEN IS_METER_RENEW_CYCLE = 0 THEN END_USAGE - START_USAGE
                                       ELSE (SELECT CONVERT(INT, REPLICATE('9', LEN(CONVERT(NVARCHAR, CONVERT(INT, START_USAGE)))))) - START_USAGE + END_USAGE + 1
                                   END),
       MULTIPLIER,
       USAGE_CUSTOMER_ID = (CASE WHEN c.USAGE_CUSTOMER_ID = 0 THEN c.CUSTOMER_ID ELSE c.USAGE_CUSTOMER_ID END)
FROM TBL_USAGE u
INNER JOIN TBL_CUSTOMER c ON u.CUSTOMER_ID = c.CUSTOMER_ID
WHERE c.BILLING_CYCLE_ID = @CYCLE_ID
      AND u.USAGE_MONTH = @MONTH
      AND u.COLLECTOR_ID <> 0
      AND c.STATUS_ID IN (2, 3);

-- MAKE SURE ALL INVOICE TOTAL USAGE MUST BE INTEGER
UPDATE TMP_BILL_USAGE
SET TOTAL_USAGE = FLOOR(TOTAL_USAGE);

-- SUMMARIZE USAGE,
-- ONE CUSTOMER HAVE ONLY ONE RECORD
-- IF CUSTOMER HAVE TWO OR MORE USAGE RECORD(S)
--  START USAGE IS THE START OF THE THE FIRST RECORD
--  END USAGE IS THE END OF THE LAST RECORD
--  TOTAL USAGE, IS SUM UP OF ALL RECORD(S)' TOTAL USAGE
INSERT INTO TMP_BILL_USAGE_SUMMARY
SELECT c.CUSTOMER_ID,
       START_USAGE = (SELECT TOP 1 START_USAGE
                      FROM TMP_BILL_USAGE
                      WHERE CUSTOMER_ID = c.CUSTOMER_ID
                      ORDER BY USAGE_ID DESC),
       END_USAGE = (SELECT TOP 1 END_USAGE
                    FROM TMP_BILL_USAGE
                    WHERE CUSTOMER_ID = c.CUSTOMER_ID
                    ORDER BY USAGE_ID DESC),
       TOTAL_USAGE = SUM(u.TOTAL_USAGE),
       DISCOUNT_USAGE = 0,
       DISCOUNT_USAGE_NAME = ''
FROM TMP_BILL_USAGE u
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = u.USAGE_CUSTOMER_ID
GROUP BY c.CUSTOMER_ID;

-- OVERRIDE RE-ACTIVE POWER.
UPDATE sr
SET TOTAL_USAGE = dbo.GET_REACTIVE(r.REACTIVE_RULE_ID, sa.TOTAL_USAGE, sr.TOTAL_USAGE)
FROM TMP_BILL_USAGE_SUMMARY sr
INNER JOIN TBL_CUSTOMER r ON r.CUSTOMER_ID = sr.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER a ON r.INVOICE_CUSTOMER_ID = a.CUSTOMER_ID
INNER JOIN TMP_BILL_USAGE_SUMMARY sa ON sa.CUSTOMER_ID = a.CUSTOMER_ID
WHERE r.IS_REACTIVE = 1;

-- SUMMARY DISCOUNT USAGE FOR CUSTOMER INPRODUCTION
UPDATE TMP_BILL_USAGE_SUMMARY
SET DISCOUNT_USAGE = s.TOTAL_USAGE,
    DISCOUNT_USAGE_NAME = N'ប្រើក្នុងផលិតកម្ម'
FROM TBL_CUSTOMER c
INNER JOIN TMP_BILL_USAGE_SUMMARY s ON s.CUSTOMER_ID = c.CUSTOMER_ID
WHERE CUSTOMER_TYPE_ID = -1
      AND BILLING_CYCLE_ID = @CYCLE_ID;

-- SUMMARY DISCOUNT USAGE FOR DISCOUNT CUSTOMER
UPDATE TMP_BILL_USAGE_SUMMARY
SET DISCOUNT_USAGE = (CASE
                          WHEN IS_PERCENTAGE = 1 THEN CONVERT(INT, s.TOTAL_USAGE * DISCOUNT / 100)
                          ELSE (CASE WHEN s.TOTAL_USAGE > d.DISCOUNT THEN d.DISCOUNT ELSE s.TOTAL_USAGE END)
                      END),
    DISCOUNT_USAGE_NAME = d.DISCOUNT_NAME
FROM TBL_CUSTOMER_DISCOUNT cd
INNER JOIN TMP_BILL_USAGE_SUMMARY s ON s.CUSTOMER_ID = cd.CUSTOMER_ID
INNER JOIN TBL_DISCOUNT d ON d.DISCOUNT_ID = cd.DISCOUNT_ID
WHERE d.DISCOUNT_TYPE_ID = 1
      AND cd.IS_ACTIVE = 1
      AND d.IS_ACTIVE = 1;

-- PRICING

-- STANDARD PRICNG
-- CALCULATE PRICE BASE ON RANK(START & END)
-- AMOUNT = SUM(PRICE * X) WHERE X IS USAGE OF EACH RANK
INSERT INTO dbo.TMP_BILL_USAGE_RATE
(
    CUSTOMER_ID,
    TOTAL_USAGE,
    START_USAGE,
    END_USAGE,
    USAGE,
    PRICE,
    FIXED_AMOUNT,
    IS_ALLOW_FIXED_AMOUNT,
    AMOUNT
) 
SELECT u.CUSTOMER_ID,
       u.TOTAL_USAGE,
       p.START_USAGE,
       p.END_USAGE,
       USAGE = (CASE
                    WHEN (u.TOTAL_USAGE - u.DISCOUNT_USAGE) > p.END_USAGE AND p.END_USAGE <> -1 THEN p.END_USAGE - p.START_USAGE + (CASE WHEN p.START_USAGE = 0 THEN 0 ELSE 1 END)
                    ELSE (u.TOTAL_USAGE - u.DISCOUNT_USAGE) - p.START_USAGE + (CASE WHEN p.START_USAGE = 0 THEN 0 ELSE 1 END)
                END),
       PRICE = p.PRICE,
       FIXED_AMOUNT = p.FIXED_AMOUNT,
       IS_ALLOW_FIXED_AMOUNT = p.IS_ALLOW_FIXED_AMOUNT,
       AMOUNT = 0
FROM TMP_BILL_USAGE_SUMMARY u
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = u.CUSTOMER_ID
INNER JOIN TBL_PRICE_DETAIL p ON p.PRICE_ID = c.PRICE_ID
INNER JOIN TBL_PRICE pl ON pl.PRICE_ID = c.PRICE_ID AND pl.IS_STANDARD_RATING = 1
WHERE (u.TOTAL_USAGE - u.DISCOUNT_USAGE) >= p.START_USAGE
ORDER BY u.CUSTOMER_ID;

-- NON-STANDARD PRICING(PRICE DISCRIMINATION)
-- CALCULATE PRICE BASE ON RANK (START & END)
-- AMOUNT = PRICE * TOTAL_USAGE 
DECLARE @PRICE_ID INT,
        @PRICE DECIMAL(18, 5);
DECLARE C CURSOR FOR
SELECT DISTINCT c.PRICE_ID
FROM TMP_BILL_USAGE_SUMMARY u
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = u.CUSTOMER_ID
INNER JOIN TBL_PRICE p ON p.PRICE_ID = c.PRICE_ID
WHERE IS_STANDARD_RATING = 0;
OPEN C;
FETCH NEXT FROM C
INTO @PRICE_ID;
WHILE @@FETCH_STATUS = 0
BEGIN
    INSERT INTO dbo.TMP_BILL_USAGE_RATE
    (
        CUSTOMER_ID,
        TOTAL_USAGE,
        START_USAGE,
        END_USAGE,
        USAGE,
        PRICE,
        FIXED_AMOUNT,
        IS_ALLOW_FIXED_AMOUNT,
        AMOUNT
		)
    SELECT CUSTOMER_ID = u.CUSTOMER_ID,
           TOTAL_USAGE = u.TOTAL_USAGE,
           START_USAGE = 0,
           END_USAGE = u.TOTAL_USAGE - u.DISCOUNT_USAGE,
           USAGE = u.TOTAL_USAGE - u.DISCOUNT_USAGE,
           PRICE = d.PRICE,
           FIXED_AMOUNT = d.FIXED_AMOUNT,
           IS_ALLOW_FIXED_AMOUNT = d.IS_ALLOW_FIXED_AMOUNT,
           AMOUNT = 0
    FROM TMP_BILL_USAGE_SUMMARY u
    INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = u.CUSTOMER_ID
    INNER JOIN TBL_PRICE_DETAIL d ON d.PRICE_ID = c.PRICE_ID
    WHERE c.PRICE_ID = @PRICE_ID
          AND (u.TOTAL_USAGE /*-u.DISCOUNT_USAGE*/) BETWEEN d.START_USAGE AND (CASE WHEN d.END_USAGE = -1 THEN 100000000 ELSE d.END_USAGE END); 
    FETCH NEXT FROM C
    INTO @PRICE_ID;
END;
CLOSE C;
DEALLOCATE C;

-- UPDATE AMOUNT
UPDATE TMP_BILL_USAGE_RATE
SET AMOUNT = (CASE WHEN TOTAL_USAGE <= END_USAGE AND IS_ALLOW_FIXED_AMOUNT = 1 AND DATEDIFF(d, c.ACTIVATE_DATE, @INVOICE_DATE) > @MAX_DAY_TO_IGNORE_FIXED_AMOUNT THEN FIXED_AMOUNT ELSE PRICE * USAGE END)
FROM TMP_BILL_USAGE_RATE r
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = r.CUSTOMER_ID;

-- INSERT INVOICE
INSERT INTO TBL_INVOICE
SELECT INVOICE_NO = '',
       INVOICE_MONTH = @MONTH,
       METER_CODE = REPLACE(LTRIM(REPLACE(m.METER_CODE, '0', ' ')), ' ', '0'), -- (CASE WHEN m.MULTIPLIER<>1 THEN ' x '+CONVERT(NVARCHAR,m.MULTIPLIER) ELSE '' END),
       START_DATE = @START_DATE,
       END_DATE = @END_DATE,
       INVOICE_DATE = @INVOICE_DATE,
       START_USAGE = u.START_USAGE,
       END_USAGE = u.END_USAGE,
       CUSTOMER_ID = u.CUSTOMER_ID,
       PAID_AMOUNT = 0,
       TOTAL_USAGE = u.TOTAL_USAGE,
       CURRENCY_ID = p.CURRENCY_ID,
       CYCLE_ID = c.BILLING_CYCLE_ID,
       DUE_DATE = @DUE_DATE,
       INVOICE_STATUS = 1,
       IS_SERVICE_BILL = 0,
       PRINT_COUNT = 1,
       RUN_ID = @RUN_ID,
       FORWARD_AMOUNT = 0,
       TOTAL_AMOUNT = 0,
       SETTLE_AMOUNT = 0,
       DISCOUNT_USAGE = u.DISCOUNT_USAGE,
       DISCOUNT_USAGE_NAME = u.DISCOUNT_USAGE_NAME,
       DISCOUNT_AMOUNT = 0,
       DISCOUNT_AMOUNT_NAME = '',
       INVOICE_TITLE = @INVOICE_TITLE,
       START_PAY_DATE = @START_PAY_DATE,
       PRICE_ID = 0,
       PRICE = 0,
       BASED_PRICE = 0,
       ROW_DATE = @NOW,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       EXCHANGE_RATE = ISNULL(r.EXCHANGE_RATE, 1),
       EXCHANGE_RATE_DATE = ISNULL(r.CREATE_ON, GETDATE())
FROM TMP_BILL_USAGE_SUMMARY u
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = u.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_METER cm ON cm.CUSTOMER_ID = c.CUSTOMER_ID AND cm.IS_ACTIVE = 1
INNER JOIN TBL_METER m ON m.METER_ID = cm.METER_ID
INNER JOIN TBL_AREA a ON a.AREA_ID = c.AREA_ID
INNER JOIN TBL_PRICE p ON p.PRICE_ID = c.PRICE_ID
LEFT JOIN #tmpExchangeRate r ON r.CURRENCY_ID = p.CURRENCY_ID
-- SORT BY AREACODE,CUSTOMERID for fakeDB
ORDER BY a.AREA_CODE, c.CUSTOMER_ID;

--- INSERT INVOICE_USAGE
INSERT INTO TBL_INVOICE_USAGE
SELECT INVOICE_ID = i.INVOICE_ID,
       CUSTOMER_ID = c.CUSTOMER_ID,
       METER_ID = m.METER_ID,
       METER_CODE = REPLACE(LTRIM(REPLACE(m.METER_CODE, '0', ' ')), ' ', '0'),
       CUSTOMER_NAME = c.LAST_NAME_KH + ' ' + c.FIRST_NAME_KH,
       START_USAGE = u.START_USAGE,
       END_USAGE = u.END_USAGE,
       MULTIPLIER = u.MULTIPLIER,
       TOTAL_USAGE = u.TOTAL_USAGE
FROM TBL_INVOICE i
INNER JOIN TMP_BILL_USAGE u ON i.CUSTOMER_ID = u.USAGE_CUSTOMER_ID
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = u.CUSTOMER_ID
INNER JOIN TBL_METER m ON m.METER_ID = u.METER_ID
WHERE i.RUN_ID = @RUN_ID
      AND i.IS_SERVICE_BILL = 0;

INSERT INTO TBL_INVOICE_DETAIL
SELECT INVOICE_ID = i.INVOICE_ID,
       START_USAGE = u.START_USAGE,
       END_USAGE = u.START_USAGE + (CASE WHEN u.START_USAGE = 0 THEN u.USAGE ELSE u.USAGE - 1 END),
       USAGE = u.USAGE,
       PRICE = u.PRICE,
       INVOICE_ITEM_ID = 1,
       AMOUNT = u.AMOUNT,
       CHARGE_DESCRIPTION = '',
       REF_NO = '',
       TRAN_DATE = i.INVOICE_DATE,
       EXCHANGE_RATE = ISNULL(r.EXCHANGE_RATE, 1),
       EXCHANGE_RATE_DATE = ISNULL(r.CREATE_ON, GETDATE()),
       TAX_AMOUNT = 0
FROM TBL_INVOICE i
INNER JOIN TMP_BILL_USAGE_RATE u ON i.CUSTOMER_ID = u.CUSTOMER_ID
LEFT JOIN #tmpExchangeRate r ON r.CURRENCY_ID = i.CURRENCY_ID
WHERE i.INVOICE_MONTH = @MONTH
      AND i.RUN_ID = @RUN_ID
      AND i.IS_SERVICE_BILL = 0
ORDER BY u.PRICE DESC;

--
-- PRICE EXTRA CHARGE 
--

-- TEMPORARY UPDATE INVOICE_AMOUNT
UPDATE TBL_INVOICE
SET TOTAL_AMOUNT = x.TOTAL_AMOUNT
FROM TBL_INVOICE i
INNER JOIN
(SELECT v.INVOICE_ID,
        SUM(AMOUNT) AS TOTAL_AMOUNT
 FROM TBL_INVOICE_DETAIL d
 INNER JOIN TBL_INVOICE v ON v.INVOICE_ID = d.INVOICE_ID
 WHERE v.RUN_ID = @RUN_ID
       AND v.IS_SERVICE_BILL = 0
 GROUP BY v.INVOICE_ID
)x ON x.INVOICE_ID = i.INVOICE_ID;

DECLARE @EXTRA_CHARGE_ID INT;
DECLARE C CURSOR FOR
SELECT DISTINCT
       c.PRICE_ID,
       g.EXTRA_CHARGE_ID
FROM TMP_BILL_USAGE_SUMMARY u
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = u.CUSTOMER_ID
INNER JOIN TBL_PRICE p ON p.PRICE_ID = c.PRICE_ID
INNER JOIN TBL_PRICE_EXTRA_CHARGE g ON g.PRICE_ID = p.PRICE_ID AND g.IS_ACTIVE = 1;
OPEN C;
FETCH NEXT FROM C
INTO @PRICE_ID,
     @EXTRA_CHARGE_ID;
WHILE @@FETCH_STATUS = 0
BEGIN
    -- MERGE INVOICE
    INSERT INTO TBL_INVOICE_DETAIL
    SELECT INVOICE_ID = i.INVOICE_ID,
           START_USAGE = 0,
           END_USAGE = 0,
           USAGE = 1,
           PRICE = CASE WHEN g.CHARGE_BY_PERCENTAGE = 1 THEN g.CHARGE_VALUE * TOTAL_AMOUNT / 100.00 ELSE g.CHARGE_VALUE END,
           INVOICE_ITEM_ID = g.INVOICE_ITEM_ID,
           AMOUNT = CASE WHEN g.CHARGE_BY_PERCENTAGE = 1 THEN g.CHARGE_VALUE * TOTAL_AMOUNT / 100.00 ELSE g.CHARGE_VALUE END,
           CHARGE_DESCRIPTION = g.CHARGE_DESCRIPTION,
           REF_NO = '',
           TRAN_DATE = i.INVOICE_DATE,
           EXCHANGE_RATE = ISNULL(r.EXCHANGE_RATE, 1),
           EXCHANGE_RATE_DATE = ISNULL(r.CREATE_ON, GETDATE()),
           TAX_AMOUNT = 0
    FROM TBL_INVOICE i
    INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = i.CUSTOMER_ID
    INNER JOIN TBL_PRICE p ON p.PRICE_ID = c.PRICE_ID
    INNER JOIN TBL_PRICE_EXTRA_CHARGE g ON g.PRICE_ID = c.PRICE_ID
    LEFT JOIN #tmpExchangeRate r ON r.CURRENCY_ID = i.CURRENCY_ID
    WHERE i.RUN_ID = @RUN_ID
          AND IS_SERVICE_BILL = 0
          AND g.IS_ACTIVE = 1
          AND g.EXTRA_CHARGE_ID = @EXTRA_CHARGE_ID
          AND i.TOTAL_AMOUNT BETWEEN g.CHARGE_AFTER AND g.CHARGE_BEFORE
          AND g.IS_MERGE_INVOICE = 1;

    -- NOT MERGE INVOICE
    INSERT INTO TBL_INVOICE
    SELECT INVOICE_NO = '',
           INVOICE_MONTH = @MONTH,
           METER_CODE = '',
           START_DATE = @MONTH,
           END_DATE = @MONTH,
           INVOICE_DATE = @INVOICE_DATE,
           START_USAGE = 0,
           END_USAGE = 0,
           CUSTOMER_ID = c.CUSTOMER_ID,
           PAID_AMOUNT = 0,
           TOTAL_USAGE = 0,
           CURRENCY_ID = p.CURRENCY_ID,
           CYCLE_ID = INVOICE_ITEM_ID, -- dummy
           DUE_DATE = @DUE_DATE,
           INVOICE_STATUS = 1,
           IS_SERVICE_BILL = 1,
           PRINT_COUNT = 0,
           RUN_ID = -9999, --- dummy
           FORWARD_AMOUNT = 0,
           TOTAL_AMOUNT = CASE WHEN g.CHARGE_BY_PERCENTAGE = 1 THEN g.CHARGE_VALUE * TOTAL_AMOUNT / 100.00 ELSE g.CHARGE_VALUE END,
           SETTLE_AMOUNT = CASE WHEN g.CHARGE_BY_PERCENTAGE = 1 THEN g.CHARGE_VALUE * TOTAL_AMOUNT / 100.00 ELSE g.CHARGE_VALUE END,
           DISCOUNT_USAGE = 0,
           DISCOUNT_USAGE_NAME = '',
           DISCOUNT_AMOUNT = 0,
           DISCOUNT_AMOUNT_NAME = '',
           INVOICE_TITLE = g.CHARGE_DESCRIPTION,
           START_PAY_DATE = @START_PAY_DATE,
           PRICE_ID = 0,
           PRICE = 0,
           BASED_PRICE = 0,
           ROW_DATE = @NOW,
           c.CUSTOMER_CONNECTION_TYPE_ID,
           EXCHANGE_RATE = i.EXCHANGE_RATE,
           i.EXCHANGE_RATE_DATE
    FROM TBL_INVOICE i
    INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = i.CUSTOMER_ID
    INNER JOIN TBL_PRICE p ON p.PRICE_ID = c.PRICE_ID
    INNER JOIN TBL_PRICE_EXTRA_CHARGE g ON g.PRICE_ID = c.PRICE_ID
    WHERE i.RUN_ID = @RUN_ID
          AND IS_SERVICE_BILL = 0
          AND g.IS_ACTIVE = 1
          AND g.EXTRA_CHARGE_ID = @EXTRA_CHARGE_ID
          AND i.TOTAL_AMOUNT BETWEEN g.CHARGE_AFTER AND g.CHARGE_BEFORE
          AND g.IS_MERGE_INVOICE = 0;

    INSERT INTO TBL_INVOICE_DETAIL
    SELECT INVOICE_ID,
           START_USAGE = 0,
           END_USAGE = 0,
           USAGE = 1,
           PRICE = SETTLE_AMOUNT,
           INVOICE_ITEM_ID = CYCLE_ID,
           AMOUNT = SETTLE_AMOUNT,
           CHARGE_DESCRIPTION = INVOICE_TITLE,
           REF_NO = '',
           TRAN_DATE = i.INVOICE_DATE,
           EXCHANGE_RATE = ISNULL(r.EXCHANGE_RATE, 1),
           EXCHANGE_RATE_DATE = ISNULL(r.CREATE_ON, GETDATE()),
           TAX_AMOUNT = 0
    FROM TBL_INVOICE i
    LEFT JOIN #tmpExchangeRate r ON r.CURRENCY_ID = i.CURRENCY_ID
    WHERE RUN_ID = -9999;

    -- clear dummy 
    UPDATE TBL_INVOICE
    SET CYCLE_ID = @CYCLE_ID,
        RUN_ID = @RUN_ID
    WHERE RUN_ID = -9999;

    FETCH NEXT FROM C
    INTO @PRICE_ID,
         @EXTRA_CHARGE_ID;
END;
CLOSE C;
DEALLOCATE C;
------------ END PRICE EXTRA CHARGE ----------------- 

-- UPDATE PRICING - RECORD PRICE FOR EASY REPORT CACULATION.
SELECT p.PRICE_ID,
       BASED_PRICE = ISNULL(x.PRICE, 0)
INTO #TMP_PRICE
FROM TBL_PRICE p
OUTER APPLY
(SELECT TOP 1 PRICE
 FROM TBL_PRICE_DETAIL
 WHERE PRICE_ID = p.PRICE_ID
 ORDER BY START_USAGE DESC
)x;

UPDATE TBL_INVOICE
SET PRICE_ID = c.PRICE_ID,
    BASED_PRICE = ISNULL(b.BASED_PRICE, 0),
    PRICE = ISNULL(d.PRICE, 0)
FROM TBL_INVOICE i
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = i.CUSTOMER_ID
INNER JOIN #TMP_PRICE b ON b.PRICE_ID = c.PRICE_ID
OUTER APPLY
(SELECT TOP 1 PRICE
 FROM TBL_INVOICE_DETAIL
 WHERE INVOICE_ID = i.INVOICE_ID
 ORDER BY PRICE
)d
WHERE i.RUN_ID = @RUN_ID;

-- UDPATE INVOICE_NO (for invoice bill)
UPDATE TBL_INVOICE
SET INVOICE_NO = x.INVOICE_NO
FROM TBL_INVOICE i
INNER JOIN
(SELECT INVOICE_ID,
        INVOICE_NO = dbo.GET_SEQUENCE((SELECT SEQUENCE_ID FROM #tmpSequence WHERE SEQUENCE_TYPE_ID = 1), ROW_NUMBER() OVER (ORDER BY INVOICE_ID) - 1, @INVOICE_DATE)
 FROM TBL_INVOICE
 WHERE RUN_ID = @RUN_ID
       AND INVOICE_NO = ''
       AND IS_SERVICE_BILL = 0
)x ON x.INVOICE_ID = i.INVOICE_ID;

-- UDPATE SEQUENCE
UPDATE TBL_SEQUENCE
SET VALUE = CASE
                WHEN FORMAT LIKE '%{Y%' AND DATEDIFF(YEAR, DATE, @INVOICE_DATE) <> 0 THEN @@ROWCOUNT
                WHEN FORMAT LIKE '%{M%' AND DATEDIFF(MONTH, DATE, @INVOICE_DATE) <> 0 THEN @@ROWCOUNT
                ELSE VALUE + @@ROWCOUNT
            END,
    DATE = @INVOICE_DATE
WHERE SEQUENCE_ID = (SELECT SEQUENCE_ID FROM #tmpSequence WHERE SEQUENCE_TYPE_ID = 1);

-- UDPATE INVOICE_NO (for service bill)
UPDATE TBL_INVOICE
SET INVOICE_NO = x.INVOICE_NO
FROM TBL_INVOICE i
INNER JOIN
(SELECT INVOICE_ID, INVOICE_NO = dbo.GET_SEQUENCE((SELECT SEQUENCE_ID FROM #tmpSequence WHERE SEQUENCE_TYPE_ID = 2), ROW_NUMBER() OVER (ORDER BY INVOICE_ID) - 1, @INVOICE_DATE)
 FROM TBL_INVOICE
 WHERE RUN_ID = @RUN_ID
       AND INVOICE_NO = ''
       AND IS_SERVICE_BILL = 1
)x ON x.INVOICE_ID = i.INVOICE_ID;

-- UDPATE SEQUENCE
UPDATE TBL_SEQUENCE
SET VALUE = CASE
                WHEN FORMAT LIKE '%{Y%' AND DATEDIFF(YEAR, DATE, @INVOICE_DATE) <> 0 THEN @@ROWCOUNT
                WHEN FORMAT LIKE '%{M%' AND DATEDIFF(MONTH, DATE, @INVOICE_DATE) <> 0 THEN @@ROWCOUNT
                ELSE VALUE + @@ROWCOUNT
            END,
    DATE = @DATE
WHERE SEQUENCE_ID = (SELECT SEQUENCE_ID FROM #tmpSequence WHERE SEQUENCE_TYPE_ID = 2);

-- UPDATE REF_NO IN INVOICE_DETAIL
UPDATE id
SET id.REF_NO = i.INVOICE_NO
FROM dbo.TBL_INVOICE i
INNER JOIN dbo.TBL_INVOICE_DETAIL id ON id.INVOICE_ID = i.INVOICE_ID
WHERE i.RUN_ID = @RUN_ID;

-- CALCULATE BALANCE FOWARD!
UPDATE TBL_INVOICE
SET FORWARD_AMOUNT = x.FORWARD_AMOUNT
FROM TBL_INVOICE i
CROSS APPLY
(SELECT TOP 1 FORWARD_AMOUNT = TOTAL_AMOUNT - SETTLE_AMOUNT
 FROM TBL_INVOICE
 WHERE INVOICE_ID < i.INVOICE_ID
       AND IS_SERVICE_BILL = 0
       AND CUSTOMER_ID = i.CUSTOMER_ID
       AND CURRENCY_ID = i.CURRENCY_ID
       AND INVOICE_STATUS <> 3
 ORDER BY INVOICE_ID DESC
)x;

-- UPDATE TOTAL AMOUNT
UPDATE TBL_INVOICE
SET TOTAL_AMOUNT = x.TOTAL_AMOUNT
FROM TBL_INVOICE i
INNER JOIN
(SELECT v.INVOICE_ID,
        SUM(AMOUNT) AS TOTAL_AMOUNT
 FROM TBL_INVOICE_DETAIL d
 INNER JOIN TBL_INVOICE v ON v.INVOICE_ID = d.INVOICE_ID
 WHERE v.RUN_ID = @RUN_ID
       AND v.IS_SERVICE_BILL = 0
 GROUP BY v.INVOICE_ID
)x ON x.INVOICE_ID = i.INVOICE_ID;

-- UPDATE TOTAL AMOUNT
UPDATE TBL_INVOICE
SET TOTAL_AMOUNT = TOTAL_AMOUNT + FORWARD_AMOUNT
WHERE RUN_ID = @RUN_ID
      AND IS_SERVICE_BILL = 0;

-- UPDATE SETTLE AMOUNT.
UPDATE TBL_INVOICE
SET SETTLE_AMOUNT = dbo.ROUND_BY_CURRENCY(TOTAL_AMOUNT, CURRENCY_ID)
WHERE RUN_ID = @RUN_ID
      AND IS_SERVICE_BILL = 0;

-- IF NOT KEEP_BILLING_REMAIN_AMOUNT 
-- NOT ALLOW FORWARD LAST REMAIN AMOUNT TO NEXT BILLING 
IF NOT EXISTS (SELECT * FROM TBL_UTILITY WHERE UTILITY_ID = 29 AND UPPER(UTILITY_VALUE) IN ('1', 'TRUE', 'ON'))
	BEGIN
		UPDATE TBL_INVOICE
		SET TOTAL_AMOUNT = SETTLE_AMOUNT
		WHERE RUN_ID = @RUN_ID
			  AND IS_SERVICE_BILL = 0;
	END;

-- UPDATE INVOICE STATUS 
UPDATE TBL_INVOICE
SET INVOICE_STATUS = 4
WHERE RUN_ID = @RUN_ID
      AND IS_SERVICE_BILL = 0
      AND SETTLE_AMOUNT = 0;

-- LOG BILLING's CUSTOMER LOG
INSERT INTO TBL_RUN_BILL_INVOICE
SELECT RUN_ID = @RUN_ID,
       INVOICE_ID = i.INVOICE_ID,
       CUSTOMER_ID = i.CUSTOMER_ID,
       CUSTOMER_CODE = c.CUSTOMER_CODE,
       INVOICE_NO = i.INVOICE_NO,
       METER_CODE = i.METER_CODE,
       START_USAGE = i.START_USAGE,
       END_USAGE = i.END_USAGE,
       TOTAL_USAGE = i.TOTAL_USAGE,
       DISCOUNT_USAGE = i.DISCOUNT_USAGE,
       SETTLE_AMOUNT = i.SETTLE_AMOUNT,
       AR_BEFORE_RUN = 0, -- NO ANY AR BEFORE RUN.
       CUSTOMER_STATUS_ID = c.STATUS_ID,
       i.CURRENCY_ID
FROM TBL_INVOICE i
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = i.CUSTOMER_ID
WHERE i.IS_SERVICE_BILL = 0
      AND i.RUN_ID = @RUN_ID;

-- LOG AGING AFTER RUN
SET @DATE = GETDATE();
INSERT INTO TBL_RUN_BILL_AGING
(RUN_ID, DATE, IS_FEFORE, CUSTOMER_ID, CURRENCY_ID, BAL1, BAL2, BAL3, BAL4, TOTAL)
SELECT RUN_ID = @RUN_ID,
       DATE = @DATE,
       IS_BEFORE = 0,
       c.CUSTOMER_ID,
       i.CURRENCY_ID,
       BAL1 = SUM(CASE WHEN DATEDIFF(D, i.INVOICE_DATE, @DATE) BETWEEN @PERIOD * 0 AND @PERIOD * 1 THEN SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT, 0) + ISNULL(aa.ADJUST_AMOUNT, 0) - ISNULL(p.PAY_AMOUNT, 0) ELSE 0 END),
       BAL2 = SUM(CASE WHEN DATEDIFF(D, i.INVOICE_DATE, @DATE) BETWEEN @PERIOD * 1 + 1 AND @PERIOD * 2 THEN SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT, 0) + ISNULL(aa.ADJUST_AMOUNT, 0) - ISNULL(p.PAY_AMOUNT, 0) ELSE 0 END),
       BAL3 = SUM(CASE WHEN DATEDIFF(D, i.INVOICE_DATE, @DATE) BETWEEN @PERIOD * 2 + 1 AND @PERIOD * 3 THEN SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT, 0) + ISNULL(aa.ADJUST_AMOUNT, 0) - ISNULL(p.PAY_AMOUNT, 0) ELSE 0 END),
       BAL4 = SUM(CASE WHEN DATEDIFF(D, i.INVOICE_DATE, @DATE) > @PERIOD * 3 THEN SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT, 0) + ISNULL(aa.ADJUST_AMOUNT, 0) - ISNULL(p.PAY_AMOUNT, 0) ELSE 0 END),
       TOTAL = SUM(SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT, 0) + ISNULL(aa.ADJUST_AMOUNT, 0) - ISNULL(p.PAY_AMOUNT, 0))
FROM TBL_INVOICE i
INNER JOIN TBL_CUSTOMER c ON i.CUSTOMER_ID = c.CUSTOMER_ID
OUTER APPLY
(SELECT SUM(PD.PAY_AMOUNT) AS PAY_AMOUNT
 FROM TBL_PAYMENT_DETAIL PD
 INNER JOIN TBL_PAYMENT P ON P.PAYMENT_ID = PD.PAYMENT_ID
 WHERE P.PAY_DATE < @DATE
       AND INVOICE_ID = i.INVOICE_ID
)p -- PAYMENT
OUTER APPLY
(SELECT SUM(IA.ADJUST_AMOUNT) AS ADJUST_AMOUNT
 FROM TBL_INVOICE_ADJUSTMENT IA
 WHERE INVOICE_ID = i.INVOICE_ID
)ta -- TOTAL ADJUSTMENT
OUTER APPLY
(SELECT SUM(IA.ADJUST_AMOUNT) AS ADJUST_AMOUNT
 FROM TBL_INVOICE_ADJUSTMENT IA
 WHERE IA.CREATE_ON <= @DATE
       AND INVOICE_ID = i.INVOICE_ID
)aa -- ACCUMULATED ADJUSTMENT 	
WHERE i.INVOICE_STATUS NOT IN (3)
      --AND (c.CUSTOMER_TYPE_ID = @CUSTOMER_TYPE_ID OR @CUSTOMER_TYPE_ID = 0)
      AND (INVOICE_DATE <= @DATE)
      AND c.BILLING_CYCLE_ID = @CYCLE_ID --LOG AGIN after RUN_BILL
GROUP BY c.CUSTOMER_ID, i.CURRENCY_ID
HAVING SUM(SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT, 0) + ISNULL(aa.ADJUST_AMOUNT, 0) - ISNULL(p.PAY_AMOUNT, 0)) > 0;

-- RUN BILL's CUSTOMER INFO
UPDATE TBL_RUN_BILL
SET CUSTOMER_TOTAL = ISNULL(x.CUSTOMER_TOTAL, 0),
    CUSTOMER_ACTIVE = ISNULL(x.CUSTOMER_ACTIVE, 0),
    CUSTOMER_BLOCKED = ISNULL(x.CUSTOMER_BLOCKED, 0),
    CUSTOMER_PENDING = ISNULL(x.CUSTOMER_PENDING, 0),
    CUSTOMER_CLOSE = ISNULL(x.CUSTOMER_CLOSE, 0)
FROM TBL_RUN_BILL r
CROSS APPLY
(SELECT CUSTOMER_TOTAL = SUM(CASE WHEN STATUS_ID <> 5 THEN 1 ELSE 0 END),
        CUSTOMER_ACTIVE = SUM(CASE WHEN STATUS_ID = 2 THEN 1 ELSE 0 END),
        CUSTOMER_BLOCKED = SUM(CASE WHEN STATUS_ID = 3 THEN 1 ELSE 0 END),
        CUSTOMER_PENDING = SUM(CASE WHEN STATUS_ID = 1 THEN 1 ELSE 0 END),
        CUSTOMER_CLOSE = SUM(CASE WHEN STATUS_ID = 4 THEN 1 ELSE 0 END)
 FROM TBL_CUSTOMER
 WHERE IS_POST_PAID = 1
)x
WHERE r.RUN_ID = @RUN_ID;

-- RUN BILL's INVOICE INFO
INSERT INTO TBL_RUN_BILL_AMOUNT
SELECT RUN_ID,
       CURRENCY_ID,
       TOTAL_INVOICE = COUNT(INVOICE_ID),
       TOTAL_POWER = SUM(TOTAL_USAGE),
       TOTAL_AMOUNT = SUM(SETTLE_AMOUNT),
       TOTAL_INVOICE_ZERO_USAGE = SUM(CASE WHEN TOTAL_USAGE = 0 THEN 1 ELSE 0 END),
       TOTAL_INVOICE_ZERO_AMOUNT = SUM(CASE WHEN SETTLE_AMOUNT = 0 THEN 1 ELSE 0 END),
       AR_BEFORE_RUN = 0,
       AR_AFTER_RUN = 0
FROM TBL_INVOICE i
WHERE IS_SERVICE_BILL = 0
      AND RUN_ID = @RUN_ID
GROUP BY RUN_ID, CURRENCY_ID;

UPDATE TBL_RUN_BILL
SET TOTAL_POWER = t.TOTAL_POWER,
    TOTAL_INVOICE = t.TOTAL_INVOICE
FROM TBL_RUN_BILL b
OUTER APPLY
(SELECT TOTAL_POWER = SUM(TOTAL_POWER),
        TOTAL_INVOICE = SUM(TOTAL_INVOICE)
 FROM TBL_RUN_BILL_AMOUNT
 WHERE RUN_ID = b.RUN_ID
)t
WHERE RUN_ID = @RUN_ID;

--RUN BILL's 
UPDATE TBL_RUN_BILL_AMOUNT
SET AR_BEFORE_RUN = ISNULL(x.AR_BEFORE_RUN, 0),
    AR_AFTER_RUN = ISNULL(x.AR_AFTER_RUN, 0)
FROM TBL_RUN_BILL_AMOUNT r
CROSS APPLY
(SELECT CURRENCY_ID,
        AR_BEFORE_RUN = SUM(CASE WHEN IS_FEFORE = 1 THEN TOTAL ELSE 0 END),
        AR_AFTER_RUN = SUM(CASE WHEN IS_FEFORE = 0 THEN TOTAL ELSE 0 END)
 FROM TBL_RUN_BILL_AGING
 WHERE RUN_ID = r.RUN_ID
 GROUP BY CURRENCY_ID
)x
WHERE r.RUN_ID = @RUN_ID
      AND r.CURRENCY_ID = x.CURRENCY_ID;
---- END OF RUN_BILL

--RESET BILLING SEQUENCE IF BACK-DATE
UPDATE s
SET VALUE = CURRENT_VALUE,
	[DATE] = [CURRENT_DATE]
FROM dbo.TBL_SEQUENCE s
INNER JOIN #tmpSequence ts ON ts.SEQUENCE_ID = s.SEQUENCE_ID
WHERE ts.IS_BACKDATE = 1

GO 

IF OBJECT_ID('RUN_BILL') IS NOT NULL
	DROP PROC RUN_BILL
GO

CREATE PROC dbo.RUN_BILL
    @MONTH DATETIME = '2011-1-1',
    @START_DATE DATETIME = '2011-1-1',
    @END_DATE DATETIME = '2011-1-31',
    @CYCLE_ID INT = 1,
    @CREATE_BY NVARCHAR(100) = 'ADMIN',
    @DUE_DATE DATETIME = '2011-2-20',
    @START_PAY_DATE DATETIME = '2011-2-01',
    @INVOICE_DATE DATETIME = '2021-01-01'
WITH ENCRYPTION
AS
	BEGIN
		DECLARE @AREA_CODE AS NVARCHAR(300);
		SELECT TOP(1) @AREA_CODE=AREA_CODE FROM dbo.TBL_CONFIG
		IF(@AREA_CODE = '138')
		BEGIN
			EXEC dbo.RUN_BILL_SPECIAL 
				@MONTH=@MONTH,
			    @START_DATE=@START_DATE,
			    @END_DATE=@END_DATE,
			    @CYCLE_ID=@CYCLE_ID,
			    @CREATE_BY=@CREATE_BY,
			    @DUE_DATE=@DUE_DATE,
			    @START_PAY_DATE=@START_PAY_DATE,
			    @INVOICE_DATE=@INVOICE_DATE
		END
		ELSE 
		BEGIN
			EXEC dbo.RUN_BILL_NORMAL 
				@MONTH=@MONTH,
			    @START_DATE=@START_DATE,
			    @END_DATE=@END_DATE,
			    @CYCLE_ID=@CYCLE_ID,
			    @CREATE_BY=@CREATE_BY,
			    @DUE_DATE=@DUE_DATE,
			    @START_PAY_DATE=@START_PAY_DATE,
			    @INVOICE_DATE=@INVOICE_DATE
		END
	END

GO


IF OBJECT_ID('REVERSE_BILL')IS NOT NULL
	DROP PROC REVERSE_BILL
GO

CREATE PROC dbo.REVERSE_BILL 
	@RUN_ID INT = 0,
	@REVERSE_BY NVARCHAR(250) = 'Khorn',
	@USER_CASH_DRAWER_ID INT = 0
WITH ENCRYPTION
AS
/*
@03-02-2021 By Kheang Kimkhorn
	1. CREATE
*/
DECLARE @DATE DATETIME;
SET @DATE = GETDATE();

INSERT INTO dbo.TBL_INVOICE_DETAIL
(INVOICE_ID, START_USAGE, END_USAGE, USAGE, PRICE, INVOICE_ITEM_ID, AMOUNT, CHARGE_DESCRIPTION, REF_NO, TRAN_DATE, EXCHANGE_RATE, EXCHANGE_RATE_DATE,TAX_AMOUNT)
SELECT
i.INVOICE_ID,
id.START_USAGE,
id.END_USAGE,
id.USAGE,
id.PRICE,
-3,
id.AMOUNT*-1,
id.REF_NO,
dbo.GET_SEQUENCE(-2, ROW_NUMBER() OVER ( ORDER BY id.INVOICE_DETAIL_ID DESC ) -1,NULL),
GETDATE(),
EXCHANGE_RATE = id.EXCHANGE_RATE,
EXCHANGE_RATE_DATE = id.EXCHANGE_RATE_DATE,
TAX_AMOUNT = id.TAX_AMOUNT * -1
FROM dbo.TBL_INVOICE i
INNER JOIN dbo.TBL_INVOICE_DETAIL id ON id.INVOICE_ID = i.INVOICE_ID
WHERE i.RUN_ID = @RUN_ID AND i.INVOICE_STATUS NOT IN (3)
ORDER BY i.INVOICE_ID, id.INVOICE_DETAIL_ID DESC

--- update sequence
UPDATE
    TBL_SEQUENCE
SET
    VALUE=CASE WHEN FORMAT LIKE '%{Y%' AND DATEDIFF(YEAR, DATE, @DATE)<>0 THEN @@rowcount
          WHEN FORMAT LIKE '%{M%' AND DATEDIFF(MONTH, DATE, @DATE)<>0 THEN @@rowcount ELSE VALUE+@@rowcount END,
    DATE=@DATE
WHERE SEQUENCE_ID=-2;

UPDATE dbo.TBL_INVOICE
SET INVOICE_STATUS = 3,
	INVOICE_TITLE = INVOICE_TITLE + N'(លុប)',
	TOTAL_AMOUNT  = 0,
	SETTLE_AMOUNT = 0
WHERE RUN_ID = @RUN_ID
AND INVOICE_STATUS NOT IN (3)
--- END OF REVERSE_BILL

GO

IF OBJECT_ID('RUN_BANK_PAYMENT')IS NOT NULL
	DROP PROC RUN_BANK_PAYMENT

GO

CREATE PROC dbo.RUN_BANK_PAYMENT
	@ID INT=1,
	@USER_CASH_DRAWER_ID INT=1,
	@USER NVARCHAR(50)='admin'
AS

DECLARE @INCULDE_DEPOSIT BIT,
		@CODE NVARCHAR(50),
		@MSG NVARCHAR(MAX);  
SELECT @INCULDE_DEPOSIT = CASE WHEN LOWER(UTILITY_VALUE) IN('1','on','true')  THEN 1 ELSE 0 END FROM TBL_UTILITY WHERE UTILITY_ID=57;
SELECT @CODE=UTILITY_VALUE FROM TBL_UTILITY WHERE UTILITY_ID  = 44;

SET @MSG='Verifying currency...';RaisError(@MSG, 1, 1) With NoWait
UPDATE TBL_BANK_PAYMENT_DETAIL
SET CURRENCY_ID=ISNULL(c.CURRENCY_ID,0), 
	RESULT_NOTE=RESULT_NOTE+CASE WHEN c.CURRENCY_ID IS NULL THEN 'CURRENCY,' ELSE ''END
FROM TBL_BANK_PAYMENT_DETAIL d
LEFT JOIN TLKP_CURRENCY c ON c.CURRENCY_CODE=d.CURRENCY
WHERE RESULT=0 AND d.BANK_PAYMENT_ID=@ID;

SET @MSG='Verifying customer...';RAISERROR(@MSG,1,1) WITH NOWAIT;
UPDATE TBL_BANK_PAYMENT_DETAIL
SET CUSTOMER_ID=ISNULL(c.CUSTOMER_ID,0), 
	RESULT_NOTE=RESULT_NOTE+CASE WHEN c.CUSTOMER_ID IS NULL THEN 'CUSTOMER,' ELSE ''END
FROM TBL_BANK_PAYMENT_DETAIL d
LEFT JOIN TBL_CUSTOMER c ON c.CUSTOMER_CODE=REPLACE(d.CUSTOMER_CODE,@CODE+'-','')
WHERE RESULT=0 AND d.BANK_PAYMENT_ID=@ID;

SET @MSG='Marking result...'; RAISERROR(@MSG,1,1) WITH NOWAIT;
UPDATE  TBL_BANK_PAYMENT_DETAIL
SET RESULT=CASE WHEN RESULT_NOTE='' THEN 2 ELSE 1 END
WHERE RESULT=0 AND BANK_PAYMENT_ID=@ID;

SET @MSG='Checking duplicate...'; RAISERROR(@MSG,1,1) WITH NOWAIT;
UPDATE TBL_BANK_PAYMENT_DETAIL 
SET RESULT= 3
FROM TBL_BANK_PAYMENT_DETAIL d1
WHERE RESULT=2 AND BANK_PAYMENT_ID=@ID
	  AND EXISTS(SELECT * FROM TBL_BANK_PAYMENT_DETAIL d2
		  WHERE d1.CUSTOMER_CODE=d2.CUSTOMER_CODE
			    AND d1.CURRENCY_ID=d2.CURRENCY_ID
				AND d1.PAY_DATE=d2.PAY_DATE
				AND d1.BANK_PAYMENT_DETAIL_ID>d2.BANK_PAYMENT_DETAIL_ID
				AND d2.RESULT=2
		  ); 

DECLARE @DETAIL_ID INT,
		@CUSTOMER_ID INT,
		@PAY_AMOUNT DECIMAL(18,4),
		@PAID_AMOUNT DECIMAL(18,4),
		@CURRENCY_ID INT,
		@PAY_DATE DATETIME;

-- UPDATE TOTAL DETAIL RECORD
UPDATE TBL_BANK_PAYMENT 
SET TOTAL_RECORD=(SELECT COUNT(*) FROM TBL_BANK_PAYMENT_DETAIL WHERE BANK_PAYMENT_ID=@ID)
WHERE BANK_PAYMENT_ID=@ID;


SET @MSG='Initializing settlement...'; RAISERROR(@MSG,1,1) WITH NOWAIT;
DECLARE C CURSOR FOR 
SELECT BANK_PAYMENT_DETAIL_ID,
	   CUSTOMER_ID,
	   PAY_AMOUNT,
	   PAID_AMOUNT,
	   CURRENCY_ID,
	   PAY_DATE
FROM TBL_BANK_PAYMENT_DETAIL
WHERE  RESULT= 2 
    AND IS_VOID = 0
	AND ABS(PAY_AMOUNT) > ABS(PAID_AMOUNT)
	AND BANK_PAYMENT_ID<=@ID -- INCLUDE PENDING DETAIL


DECLARE @N INT,@I INT;
SET @N=(SELECT COUNT(*) FROM TBL_BANK_PAYMENT_DETAIL
WHERE  RESULT= 2 
    AND IS_VOID = 0
	AND ABS(PAY_AMOUNT) > ABS(PAID_AMOUNT)
	AND BANK_PAYMENT_ID<=@ID) -- INCLUDE PENDING DETAIL);
SET @I = 0;

OPEN C;
FETCH NEXT FROM C INTO @DETAIL_ID,@CUSTOMER_ID,@PAY_AMOUNT,@PAID_AMOUNT,@CURRENCY_ID,@PAY_DATE;
WHILE @@FETCH_STATUS = 0 BEGIN
	SET @I=@I+1;
	IF @I%1=0 BEGIN -- UPDATE PROGRESS EVERY 50 record
		SET @MSG ='Settlement...'+ CONVERT(NVARCHAR,@I)+'/'+CONVERT(NVARCHAR,@N);
		RAISERROR(@MSG, 1, 1) WITH NOWAIT;
	END

	-- 1. VOID BANK PAYMENT  *************************************************************************************
	IF @PAY_AMOUNT<0 BEGIN
		-- 1.1 FIND BANK PAYMENT TO VOID. 
		DECLARE @VOID_ID INT;
		SELECT @VOID_ID=BANK_PAYMENT_DETAIL_ID 
		FROM TBL_BANK_PAYMENT_DETAIL
		WHERE PAY_AMOUNT = - @PAY_AMOUNT 
		      AND CURRENCY_ID = @CURRENCY_ID
              AND CUSTOMER_ID = @CUSTOMER_ID
              AND IS_VOID = 0
		ORDER BY PAY_DATE DESC; 
		 
		IF @VOID_ID IS NULL BEGIN
			FETCH NEXT FROM C INTO @DETAIL_ID,@CUSTOMER_ID,@PAY_AMOUNT,@PAID_AMOUNT,@CURRENCY_ID,@PAY_DATE;
			CONTINUE;
		END
		-- 1.2 VOID BANK PAYMENT DETAIL.
		UPDATE TBL_BANK_PAYMENT_DETAIL SET IS_VOID=1 WHERE BANK_PAYMENT_DETAIL_ID=@VOID_ID;
		UPDATE TBL_BANK_PAYMENT_DETAIL SET PAID_AMOUNT=PAY_AMOUNT WHERE BANK_PAYMENT_DETAIL_ID=@DETAIL_ID;
		-- 1.3 ROLLBACK CUS_DEPOSIT PAYMENT
		UPDATE TBL_CUS_DEPOSIT SET IS_PAID=0 WHERE BANK_PAYMENT_DETAIL_ID=@VOID_ID;
		-- 1.4 FIND PAYMENT TO VOID
		DECLARE @VOID_PAYMENT_ID INT;
		SELECT @VOID_PAYMENT_ID=PAYMENT_ID FROM TBL_PAYMENT WHERE BANK_PAYMENT_DETAIL_ID=@VOID_ID AND IS_VOID=0;
		IF @VOID_PAYMENT_ID IS NULL BEGIN
			FETCH NEXT FROM C INTO @DETAIL_ID,@CUSTOMER_ID,@PAY_AMOUNT,@PAID_AMOUNT,@CURRENCY_ID,@PAY_DATE;
			CONTINUE;
		END
		-- 1.4 VOID PAYMENT
		INSERT INTO TBL_PAYMENT
		SELECT  PAYMENT_NO,CUSTOMER_ID,
				-PAY_AMOUNT,PAY_DATE,CREATE_BY=@USER,
				CREATE_ON=GETDATE(),IS_ACTIVE,
				USER_CASH_DRAWER_ID=@USER_CASH_DRAWER_ID,
				DUE_AMOUNT=0,BANK_PAYMENT_DETAIL_ID=@DETAIL_ID,
				CURRENCY_ID=@CURRENCY_ID,
				IS_VOID=1,PARENT_ID=@VOID_PAYMENT_ID,
				PAYMENT_ACCOUNT_ID --copy from parent
		FROM TBL_PAYMENT 
		WHERE PAYMENT_ID=@VOID_PAYMENT_ID;
		
		INSERT INTO TBL_PAYMENT_DETAIL
		SELECT  PAYMENT_ID=@@IDENTITY,INVOICE_ID,-PAY_AMOUNT,DUE_AMOUNT=0 
		FROM TBL_PAYMENT_DETAIL WHERE PAYMENT_ID=@VOID_PAYMENT_ID

		UPDATE TBL_PAYMENT SET IS_VOID=1 WHERE PAYMENT_ID=@VOID_PAYMENT_ID;
		UPDATE TBL_INVOICE SET PAID_AMOUNT=PAID_AMOUNT-d.PAY_AMOUNT,
							   INVOICE_STATUS=CASE WHEN SETTLE_AMOUNT=PAID_AMOUNT-d.PAY_AMOUNT THEN 4 ELSE 1 END
		FROM TBL_PAYMENT_DETAIL d
		INNER JOIN TBL_INVOICE i ON i.INVOICE_ID=d.INVOICE_ID
		WHERE PAYMENT_ID=@VOID_PAYMENT_ID;
		 
		
		FETCH NEXT FROM C INTO @DETAIL_ID,@CUSTOMER_ID,@PAY_AMOUNT,@PAID_AMOUNT,@CURRENCY_ID,@PAY_DATE;
		CONTINUE;
	END

	DECLARE @AVAILABLE_AMOUNT DECIMAL(18,4);
	SET @AVAILABLE_AMOUNT=@PAY_AMOUNT-@PAID_AMOUNT;

	-- SETTLE WITH DEPOSIT **************************************************************************************
	IF @INCULDE_DEPOSIT=1 BEGIN
	  
		DECLARE @DEPOSIT_ID INT,@DEPOSIT_AMOUNT DECIMAL(18,4);
		SELECT TOP 1 @DEPOSIT_ID=CUS_DEPOSIT_ID, @DEPOSIT_AMOUNT=AMOUNT
		FROM TBL_CUS_DEPOSIT 
		WHERE CUSTOMER_ID=@CUSTOMER_ID AND CURRENCY_ID=@CURRENCY_ID AND IS_PAID=0 AND AMOUNT>0;

		WHILE @AVAILABLE_AMOUNT>0 AND @DEPOSIT_ID IS NOT NULL BEGIN
			
			IF @DEPOSIT_AMOUNT>0 AND @DEPOSIT_AMOUNT<=@AVAILABLE_AMOUNT BEGIN
				UPDATE TBL_CUS_DEPOSIT
				SET BANK_PAYMENT_DETAIL_ID=@DETAIL_ID,
					IS_PAID = 1,
					USER_CASH_DRAWER_ID=@USER_CASH_DRAWER_ID,
					DEPOSIT_DATE=@PAY_DATE
				WHERE CUS_DEPOSIT_ID=@DEPOSIT_ID;

				UPDATE TBL_BANK_PAYMENT_DETAIL
				SET PAID_AMOUNT=PAID_AMOUNT+@DEPOSIT_AMOUNT
				WHERE BANK_PAYMENT_DETAIL_ID=@DETAIL_ID;

				SET @AVAILABLE_AMOUNT=@AVAILABLE_AMOUNT-@DEPOSIT_AMOUNT;
			END

			-- RESET TO PUSH NEXT WHILE.
			SELECT @DEPOSIT_ID=NULL,@DEPOSIT_AMOUNT=NULL;
			SELECT TOP 1 @DEPOSIT_ID=CUS_DEPOSIT_ID, @DEPOSIT_AMOUNT=AMOUNT
			FROM TBL_CUS_DEPOSIT 
			WHERE CUSTOMER_ID=@CUSTOMER_ID AND CURRENCY_ID=@CURRENCY_ID AND IS_PAID=0  AND AMOUNT>0;
		END -- WHILE
	END -- IF @INCULDE_DEPOSIT

	IF @AVAILABLE_AMOUNT>0 BEGIN  
		
		DECLARE @PAYMENT_ID INT,
				@PAYMENT_AMOUNT DECIMAL(18,4),
				@INVOICE_ID INT,
				@INVOICE_AMOUNT DECIMAL(18,4);
		SET @PAYMENT_AMOUNT=0;

		SELECT TOP 1  @INVOICE_ID=INVOICE_ID,
					  @INVOICE_AMOUNT=SETTLE_AMOUNT-PAID_AMOUNT
		FROM TBL_INVOICE i 
		INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=i.CUSTOMER_ID
		WHERE (c.CUSTOMER_ID=@CUSTOMER_ID OR c.INVOICE_CUSTOMER_ID=@CUSTOMER_ID)
			  AND SETTLE_AMOUNT>PAID_AMOUNT
			  AND CURRENCY_ID=@CURRENCY_ID;
		IF @INVOICE_ID IS NOT NULL BEGIN
			INSERT INTO TBL_PAYMENT 
			SELECT  PAYMENT_NO=dbo.GET_SEQUENCE(2,0,NULL),
					CUSTOMER_ID=@CUSTOMER_ID,
					PAY_AMOUNT=0,PAY_DATE=@PAY_DATE,
					CREATE_BY=@USER,CREATE_ON=GETDATE(),IS_ACTIVE=1,
					USER_CASH_DRAWER_ID=@USER_CASH_DRAWER_ID,
					DUE_AMOUNT=0,BANK_PAYMENT_DETAIL_ID=@DETAIL_ID,
					CURRENCY_ID=@CURRENCY_ID,IS_VOID=0,PARENT_ID=0,
					PAYMENT_ACCOUNT_ID=73 -- CASH ON BANK
			SELECT @PAYMENT_ID=@@IDENTITY; 
			EXEC UPDATE_SEQUENCE 2,0 ;
		END -- IF @INVOICE

		WHILE @INVOICE_ID IS NOT NULL AND @AVAILABLE_AMOUNT>0 BEGIN
			DECLARE @AMOUNT DECIMAL(18,4);
			SET @AMOUNT=CASE WHEN @INVOICE_AMOUNT>@AVAILABLE_AMOUNT THEN @AVAILABLE_AMOUNT ELSE @INVOICE_AMOUNT END;
			
			SET @PAYMENT_AMOUNT=@PAYMENT_AMOUNT+@AMOUNT;
			INSERT INTO TBL_PAYMENT_DETAIL SELECT @PAYMENT_ID,@INVOICE_ID,PAY_AMOUNT=@AMOUNT,DUE_AMOUNT=0;
			UPDATE TBL_INVOICE SET PAID_AMOUNT=PAID_AMOUNT+@AMOUNT,
							       INVOICE_STATUS=CASE WHEN PAID_AMOUNT+@AMOUNT=SETTLE_AMOUNT THEN 4 ELSE 1 END 
			WHERE INVOICE_ID=@INVOICE_ID;
			SET @AVAILABLE_AMOUNT=@AVAILABLE_AMOUNT-@AMOUNT;
			UPDATE TBL_BANK_PAYMENT_DETAIL SET PAID_AMOUNT=PAID_AMOUNT+@AMOUNT WHERE BANK_PAYMENT_DETAIL_ID=@DETAIL_ID;

			-- PUSH NEXT WHILE
			SELECT @INVOICE_ID=NULL,@INVOICE_AMOUNT=NULL
			SELECT TOP 1 @INVOICE_ID=INVOICE_ID,
						 @INVOICE_AMOUNT=SETTLE_AMOUNT-PAID_AMOUNT
			FROM TBL_INVOICE i 
			INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=i.CUSTOMER_ID
			WHERE (c.CUSTOMER_ID=@CUSTOMER_ID OR c.INVOICE_CUSTOMER_ID=@CUSTOMER_ID)
					AND SETTLE_AMOUNT>PAID_AMOUNT
					AND CURRENCY_ID=@CURRENCY_ID;
		END--WHILE @INVOICE_ID

		UPDATE TBL_PAYMENT 
		SET PAY_AMOUNT=@PAYMENT_AMOUNT
		WHERE PAYMENT_ID=@PAYMENT_ID AND @PAYMENT_AMOUNT IS NOT NULL;

	END--IF @AVAILABLE_AMOUNT>0

	FETCH NEXT FROM C INTO @DETAIL_ID,@CUSTOMER_ID,@PAY_AMOUNT,@PAID_AMOUNT,@CURRENCY_ID,@PAY_DATE; 
END--@FETCH CURSOR
CLOSE C;
DEALLOCATE C;

SET @MSG='Finalizig settlement...'; RAISERROR(@MSG,1,1) WITH NOWAIT;
-----------------------------------------------------------------------------------------------------------
-- END RUN BANK PAYMENT 
-----------------------------------------------------------------------------------------------------------
GO

IF OBJECT_ID('REPORT_CUSTOMER_MERGE_STOP_USING')IS NOT NULL
	DROP PROC REPORT_CUSTOMER_MERGE_STOP_USING
GO
CREATE PROC [dbo].[REPORT_CUSTOMER_MERGE_STOP_USING]
	@AREA_ID INT=0,
	@PRICE_ID INT=0,
	@CYCLE_ID INT=0,
	@CUSTOMER_CONNECTION_TYPE_ID INT=0,
	@MONTH DATETIME='2021-02-01'
AS 
	BEGIN 
		--GET LAST CHANGE STATUS TO REMOVE
		WITH cs AS
		(SELECT RowNumber=ROW_NUMBER() OVER (PARTITION BY CUSTOMER_ID ORDER BY CHNAGE_ID DESC), * FROM dbo.TBL_CUS_STATUS_CHANGE 
		WHERE MONTH(CHNAGE_DATE) = MONTH(@MONTH ) AND YEAR(CHNAGE_DATE) =  YEAR(@MONTH) AND NEW_STATUS_ID IN (4,5))
		SELECT * INTO #TEMP_CUS_STATUS_CHANGE FROM cs WHERE cs.RowNumber=1;
		--GET MERGEIDS BY CHANGING CUSTOMER ID 
		WITH r AS
		(
			SELECT RowNumber=ROW_NUMBER() OVER (PARTITION BY MERGE_ID ORDER BY  RUN_BILL_CUSTOMER_MERGE_ID DESC),  MERGE_ID
			FROM TBL_RUN_BILL_CUSTOMER_MERGE WHERE CUSTOMER_ID IN (SELECT CUSTOMER_ID FROM #TEMP_CUS_STATUS_CHANGE)
		 )
		 SELECT * INTO #TMP_MERGEIDS FROM r WHERE r.RowNumber  =1;
		 
		 --GET CUSTOMER
		 WITH v_CUSTOMERMERGE AS 
		 (
			 SELECT RowNumber = ROW_NUMBER() OVER(PARTITION BY CUSTOMER_ID ORDER BY RUN_BILL_CUSTOMER_MERGE_ID DESC),RUN_BILL_CUSTOMER_MERGE_ID  FROM dbo.TBL_RUN_BILL_CUSTOMER_MERGE
			 WHERE MERGE_ID IN (SELECT MERGE_ID FROM #TMP_MERGEIDS) 
		 )  SELECT *  INTO  #TMP_CUSTOMERMERGEIDS FROM  v_CUSTOMERMERGE WHERE RowNumber = 1 
		  
	SELECT	rbm.CUSTOMER_MERGE_TYPE_ID,
			rbm.CUSTOMER_MERGE_TYPE_NAME,
			rbm.CUSTOMER_CODE,
			rbm.CUSTOMER_NAME,
			rbm.POLE_CODE,
			rbm.BOX_CODE,
			METER_CODE=REPLACE(LTRIM(REPLACE(ISNULL(rbm.METER_CODE, '-'), '0', ' ')), ' ', '0'),
			rbm.MERGE_ID,
			rbm.RUN_BILL_CUSTOMER_MERGE_ID,
			rbm.PARENT_NAME,
			rbm.CREATED_BY,
			csc.NEW_STATUS_ID,
			csc.CHNAGE_DATE
	FROM TBL_CUSTOMER c 
	INNER JOIN dbo.TBL_RUN_BILL_CUSTOMER_MERGE rbm ON c.CUSTOMER_ID=rbm.CUSTOMER_ID 
	LEFT JOIN #TEMP_CUS_STATUS_CHANGE csc ON csc.CUSTOMER_ID = c.CUSTOMER_ID
	INNER JOIN TLKP_CUSTOMER_CONNECTION_TYPE ct ON ct.CUSTOMER_CONNECTION_TYPE_ID=c.CUSTOMER_CONNECTION_TYPE_ID
	WHERE (@AREA_ID=0 OR rbm.AREA_ID=@AREA_ID)
		AND (@CYCLE_ID=0 OR rbm.CYCLE_ID=@CYCLE_ID)
		AND (@PRICE_ID=0 OR rbm.PRICE_ID=@PRICE_ID)
		AND (@CUSTOMER_CONNECTION_TYPE_ID=0 OR c.CUSTOMER_CONNECTION_TYPE_ID=@CUSTOMER_CONNECTION_TYPE_ID)  
		AND	 rbm.RUN_BILL_CUSTOMER_MERGE_ID IN  (SELECT RUN_BILL_CUSTOMER_MERGE_ID FROM #TMP_CUSTOMERMERGEIDS)
	ORDER BY  CUSTOMER_MERGE_TYPE_ID,MERGE_ID,AREA_CODE,POLE_CODE,BOX_CODE,POSITION_IN_BOX
	--END REPORT_CUSTOMER_MERGE_STOP_USING
END 

GO

IF OBJECT_ID('RUN_REF_03A_2021')IS NOT NULL
	DROP PROC dbo.RUN_REF_03A_2021
GO

CREATE PROC dbo.RUN_REF_03A_2021
    @DATA_MONTH DATETIME = '2021-01-01',
    @BILLING_CYCLE_ID INT = 0,
    @AREA_ID INT = 0
AS
/*

@2019-12-03 By Hor Dara
	Update REF 2020
	.--FIll all data with 730 base price even school type is 610 to include 1 row
@2020-07-28 By Hor Dara
	Update REF 2020
	--validate for by license type market vendor even usage over 2000 still have subsidy
*/
-- CLEAR DATA
DELETE FROM TBL_REF_03A
WHERE DATA_MONTH = @DATA_MONTH;

DECLARE @BASED_TARIFF DECIMAL(18, 5),
        @SUBSIDY_TARIFF DECIMAL(18, 5),
        @LICENSE_TYPE_ID INT,
        @TYPE_SALE_CUSTOMER_SUBSIDY NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_SUBSIDY_MARKET_VENDOR NVARCHAR(200);
SET @TYPE_SALE_CUSTOMER_SUBSIDY = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវ ទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ ';
SET @TYPE_SALE_CUSTOMER_SUBSIDY_MARKET_VENDOR = N'លក់ឲ្យអតិថិជនក្នុងផ្សារ';
SELECT @BASED_TARIFF = BASED_TARIFF,
       @SUBSIDY_TARIFF = SUBSIDY_TARIFF,
       @LICENSE_TYPE_ID = LICENSE_TYPE_ID
FROM TBL_TARIFF
WHERE DATA_MONTH = @DATA_MONTH
      AND IS_ACTIVE = 1;

IF (@LICENSE_TYPE_ID <> 2)
BEGIN
    -- for Normal Customer(TOTAL_USAGE<2001)
    SELECT TOTAL_SOLD = SUM(TOTAL_USAGE),
           CURRENCY_ID = 1,
           CURRENCY_NAME = N'រៀល',
           CURRENCY_SIGN = N'៛'
    INTO #TMP
    FROM TBL_REF_INVOICE i
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 22, 23, 134, 135, 136, 137, 138, 139, 140, 144 )
          AND TOTAL_USAGE <= 2000
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Normal Customer (when tariff rate lower than zero)
    UNION ALL
    SELECT TOTAL_SOLD = SUM(TOTAL_USAGE),
           CURRENCY_ID = 1,
           CURRENCY_NAME = N'រៀល',
           CURRENCY_SIGN = N'៛'
    FROM TBL_REF_INVOICE i
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND TOTAL_USAGE > 200
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

	UNION ALL
    SELECT TOTAL_SOLD = SUM(TOTAL_USAGE),
           CURRENCY_ID = 1,
           CURRENCY_NAME = N'រៀល',
           CURRENCY_SIGN = N'៛'
    FROM TBL_REF_INVOICE i
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 ) AND i.PRICE=730
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Normal Customer (when tariff rate equal or greater than zero)
    UNION ALL
    SELECT TOTAL_SOLD = SUM(TOTAL_USAGE),
           CURRENCY_ID = 1,
           CURRENCY_NAME = N'រៀល',
           CURRENCY_SIGN = N'៛'
    FROM TBL_REF_INVOICE i
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for School, Hospital, Health Center at countryside (when tariff rate < 0 ,exclude)
    UNION ALL
    SELECT TOTAL_SOLD = SUM(TOTAL_USAGE),
           CURRENCY_ID = 1,
           CURRENCY_NAME = N'រៀល',
           CURRENCY_SIGN = N'៛'
    FROM TBL_REF_INVOICE i
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN
    UNION ALL
    SELECT TOTAL_SOLD = SUM(TOTAL_USAGE),
           CURRENCY_ID = 1,
           CURRENCY_NAME = N'រៀល',
           CURRENCY_SIGN = N'៛'
    FROM TBL_REF_INVOICE i
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN (24)
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN;

    INSERT INTO TBL_REF_03A
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_SUBSIDY,
           TOTAL_SOLD = SUM(TOTAL_SOLD),
           RATE = 730, --FIll all data with 730 base price even school type is 610 to include 1 row
           BASED_TARIFF = @BASED_TARIFF,
           SUBSIDY_TARIFF = @SUBSIDY_TARIFF,
           SUBSIDY_RATE = @BASED_TARIFF - @SUBSIDY_TARIFF,
           SUBSIDY_AMOUNT = (@BASED_TARIFF - @SUBSIDY_TARIFF) * SUM(TOTAL_SOLD),
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM #TMP
    GROUP BY CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN;

    -- FILL BLANK RESULT
    IF NOT EXISTS (SELECT * FROM TBL_REF_03A WHERE DATA_MONTH = @DATA_MONTH)
        INSERT INTO TBL_REF_03A
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'1',
               TYPE_OF_SALE_ID = 1,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_SUBSIDY,
               TOTAL_SOLD = 0,
               RATE = 0,
               BASED_TARIFF = 0,
               SUBSIDY_TARIFF = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = 1,
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1;
END;
ELSE IF (@LICENSE_TYPE_ID = 2)
BEGIN
    -- for MARKET VENDOR
    SELECT TOTAL_SOLD = SUM(TOTAL_USAGE),
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_MARKET_VENDOR = 1
    INTO #TMP_MARKET_VENDOR
    FROM TBL_REF_INVOICE i
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 144 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN;

    INSERT INTO TBL_REF_03A
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_SUBSIDY_MARKET_VENDOR,
           TOTAL_SOLD = SUM(TOTAL_SOLD),
           RATE = 730,
           BASED_TARIFF = @BASED_TARIFF,
           SUBSIDY_TARIFF = @SUBSIDY_TARIFF,
           SUBSIDY_RATE = @BASED_TARIFF - @SUBSIDY_TARIFF,
           SUBSIDY_AMOUNT = (@BASED_TARIFF - @SUBSIDY_TARIFF) * SUM(TOTAL_SOLD),
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM #TMP_MARKET_VENDOR
    WHERE IS_MARKET_VENDOR = 1
    GROUP BY CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN;

    -- FILL BLANK RESULT
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03A
        WHERE DATA_MONTH = @DATA_MONTH
              AND TYPE_OF_SALE_ID = 1
    )
        INSERT INTO TBL_REF_03A
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'1',
               TYPE_OF_SALE_ID = 1,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_SUBSIDY_MARKET_VENDOR,
               TOTAL_SOLD = 0,
               RATE = 0,
               BASED_TARIFF = 0,
               SUBSIDY_TARIFF = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = 1,
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1;
END;
--END OF RUN_REF_03A_2021

GO

IF OBJECT_ID('RUN_REF_03B_2021')IS NOT NULL
	DROP PROC RUN_REF_03B_2021
GO

CREATE PROC dbo.RUN_REF_03B_2021
    @DATA_MONTH DATETIME = '2021-01-01',
    @BILLING_CYCLE_ID INT = 0,
    @AREA_ID INT = 0
AS
-- CLEAR DATA
DELETE FROM TBL_REF_03B
WHERE DATA_MONTH = @DATA_MONTH;

DECLARE @TYPE_SALE_CUSTOMER_USAGE_0_10 NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_USAGE_11_50 NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_USAGE_51_200 NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_SCHOOL NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_AGRI NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_SPECIAL NVARCHAR(200),
        @LICENSE_TYPE_ID INT,
        @BASED_TARIFF DECIMAL(18, 5),
        @SUBSIDY_TARIFF DECIMAL(18, 5),
        @TYPE_CUSTOMER_NORMAL NVARCHAR(200),
        @TYPE_CUSTOMER_FAVOR NVARCHAR(200),
        @TYPE_CUSTOMER_MV NVARCHAR(200),
        @TYPE_CUSTOMER_LV_LICENSE NVARCHAR(200),
        @TYPE_CUSTOMER_LV_CUSTOMER_BUY NVARCHAR(200),
        @TYPE_CUSTOMER_LV_GENERAL NVARCHAR(200);
SET @TYPE_CUSTOMER_NORMAL = N'  1' + SPACE(4) + N'អតិថិជនលំនៅដ្ឋាន';
SET @TYPE_CUSTOMER_FAVOR = N'  2' + SPACE(4) + N'អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស';
SET @TYPE_SALE_CUSTOMER_USAGE_0_10 = N'1.1' + N' លំនៅដ្ឋានប្រើ មិនលើស  10 kWh/ខែ';
SET @TYPE_SALE_CUSTOMER_USAGE_11_50 = N'1.2' + N' លំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ';
SET @TYPE_SALE_CUSTOMER_USAGE_51_200 = N'1.3' + N' លំនៅដ្ឋានប្រើចាប់ពី 51 ដល់ 200 kWh/ខែ';
SET @TYPE_SALE_CUSTOMER_SCHOOL = N'2.1' + N' សាលារៀន, មន្ទីរពេទ្យ, និងមណ្ឌលសុខភាព នៅតំបន់ជនបទ';
SET @TYPE_SALE_CUSTOMER_AGRI = N'2.2' + N' អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម(ម៉ោង៩យប់ដល់៧ព្រឹក)';
SET @TYPE_CUSTOMER_MV = N'  2.2.1' + N'  អ្នកប្រើប្រាស់ MV';
SET @TYPE_CUSTOMER_LV_LICENSE = N'  2.2.2' + N'  អ្នកប្រើប្រាស់ LV (ត្រង់ស្វូរអ្នកលក់)';
SET @TYPE_CUSTOMER_LV_CUSTOMER_BUY = N'  2.2.3' + N'  អ្នកប្រើប្រាស់ LV (ត្រង់ស្វូរអ្នកទិញ)';
SET @TYPE_CUSTOMER_LV_GENERAL = N'  2.2.4' + N'  អ្នកប្រើប្រាស់ LV (ភ្ជាប់ពីខ្សែសាធារណៈ)';

SELECT @LICENSE_TYPE_ID = LICENSE_TYPE_ID,
       @SUBSIDY_TARIFF = SUBSIDY_TARIFF,
       @BASED_TARIFF = BASED_TARIFF
FROM dbo.TBL_TARIFF
WHERE DATA_MONTH = @DATA_MONTH;

IF (@LICENSE_TYPE_ID <> 2)
BEGIN
    INSERT INTO TBL_REF_03B
    (
        DATA_MONTH,
        ROW_NO,
        TYPE_OF_SALE_ID,
        TYPE_OF_SALE_NAME,
        TOTAL_CUSTOMER,
        TOTAL_POWER_SOLD,
        PRICE,
        SUBSIDY_RATE,
        SUBSIDY_AMOUNT,
        CURRENCY_ID,
        CURRENCY_NAME,
        CURRENCY_SIGN,
        IS_ACTIVE,
        TYPE_ID,
        TYPE_NAME
    )
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1.1',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_0_10,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_NORMAL
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE <= 10
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1.2',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_11_50,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_NORMAL
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND (i.TOTAL_USAGE
          BETWEEN 11 AND 50
              )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1.3',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_51_200,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_NORMAL
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND (i.TOTAL_USAGE
          BETWEEN 51 AND 200
              )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.1',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_SCHOOL,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_AGRI,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.1',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(0.1370 * 4120, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(ROUND(0.1370 * 4120, 0), 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.2',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_LICENSE,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(0.15048 * 4120, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(ROUND(0.15048 * 4120, 0), 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.3',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_CUSTOMER_BUY,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(0.14248 * 4120, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(ROUND(0.14248 * 4120, 0), 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.4',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_GENERAL,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(730, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(730, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN

    --WHEN BASE TARFF < SUBSIDY TARIFF
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1.1',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_0_10,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.BASED_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.BASED_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_NORMAL
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE <= 10
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1.2',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_11_50,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.BASED_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.BASED_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_NORMAL
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND (i.TOTAL_USAGE
          BETWEEN 11 AND 50
              )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1.3',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_51_200,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.BASED_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.BASED_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_NORMAL
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND (i.TOTAL_USAGE
          BETWEEN 51 AND 200
              )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.1',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_SCHOOL,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.BASED_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.BASED_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_AGRI,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.BASED_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.BASED_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND (i.TOTAL_USAGE
          BETWEEN 51 AND 200
              )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.1',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(0.1370 * 4120, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(ROUND(0.1370 * 4120, 0), 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.2',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_LICENSE,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(0.15048 * 4120, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(ROUND(0.15048 * 4120, 0), 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.3',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_CUSTOMER_BUY,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(0.14248 * 4120, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(ROUND(0.14248 * 4120, 0), 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.4',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_GENERAL,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(@BASED_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(@BASED_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN;

    -- BLANK RECORD
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '1.1'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'1.1',
               TYPE_OF_SALE_ID = 1,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_0_10,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 1,
               TYPE_NAME = @TYPE_CUSTOMER_NORMAL;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '1.2'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'1.2',
               TYPE_OF_SALE_ID = 1,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_11_50,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 1,
               TYPE_NAME = @TYPE_CUSTOMER_NORMAL;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '1.3'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'1.3',
               TYPE_OF_SALE_ID = 1,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_51_200,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 1,
               TYPE_NAME = @TYPE_CUSTOMER_NORMAL;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '2.1'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'2.1',
               TYPE_OF_SALE_ID = 2,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_SCHOOL,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 2,
               TYPE_NAME = @TYPE_CUSTOMER_FAVOR;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '2.2'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'2.2',
               TYPE_OF_SALE_ID = 2,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_AGRI,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 2,
               TYPE_NAME = @TYPE_CUSTOMER_FAVOR;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '2.2.1'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'2.2.1',
               TYPE_OF_SALE_ID = 2,
               TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_MV,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 2,
               TYPE_NAME = @TYPE_CUSTOMER_FAVOR;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '2.2.2'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'2.2.2',
               TYPE_OF_SALE_ID = 2,
               TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_LICENSE,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 2,
               TYPE_NAME = @TYPE_CUSTOMER_FAVOR;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '2.2.3'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'2.2.3',
               TYPE_OF_SALE_ID = 2,
               TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_CUSTOMER_BUY,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 2,
               TYPE_NAME = @TYPE_CUSTOMER_FAVOR;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '2.2.4'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'2.2.4',
               TYPE_OF_SALE_ID = 2,
               TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_GENERAL,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 2,
               TYPE_NAME = @TYPE_CUSTOMER_FAVOR;
END;
-- END RUN_REF_03B_2021

GO


IF OBJECT_ID('GET_USAGE_HISTORY')IS NOT NULL
	DROP PROC GET_USAGE_HISTORY
GO

CREATE PROC GET_USAGE_HISTORY
    @CUSTOMER_ID INT = 1
AS
SELECT TOP 12 INV_MONTH=i.INVOICE_MONTH,
	USAGE=ISNULL(i.TOTAL_USAGE,0)+ISNULL(a.ADJ_USAGE,0)
INTO #USAGE_HISTORY
FROM TBL_INVOICE i
OUTER APPLY(
	SELECT ADJ_USAGE=SUM(ADJUST_USAGE)
	FROM TBL_INVOICE_ADJUSTMENT
	WHERE INVOICE_ID=i.INVOICE_ID
)a
WHERE CUSTOMER_ID=@CUSTOMER_ID
	AND i.IS_SERVICE_BILL=0
	ORDER BY i.INVOICE_MONTH DESC

SELECT [Value] = STUFF(( SELECT '; '
	+ CASE WHEN (ROW_NUMBER() OVER (ORDER BY INV_MONTH DESC))=7 
		THEN '{0} '
		ELSE ''
	  END
	+ CONVERT(NVARCHAR(7),INV_MONTH,120)
	+ ':' 
	+ ' ' +CONVERT(NVARCHAR,(CONVERT(INTEGER, +USAGE)))
FROM #USAGE_HISTORY
ORDER BY INV_MONTH DESC
	FOR XML PATH('')),1,1,'')
GO

IF OBJECT_ID('REPORT_REF_INVOICE')IS NOT NULL
	DROP PROC REPORT_REF_INVOICE
GO
CREATE PROC [dbo].[REPORT_REF_INVOICE]
    @DATA_MONTH DATETIME = '2021-01-01',
    @CUSTOMER_GROUP_ID INT = 0,
    @CUSTOMER_CONNECTION_TYPE_ID INT = 0,
    @CURRENCY_ID INT = 0,
    @V1 DECIMAL(18, 4) = 0,
    @V2 DECIMAL(18, 4) = 1000000,
    @BILLING_CYCLE_ID INT = 0,
    @AREA_ID INT = 0
AS
/*
@ 2016-05-25 Morm Raksmey
	Add Column Billing Cycle Id and Billing Cycle Name
@ 2017-03-10 Sieng Sothera
	Trim meter code without '0'
@ 2017-04-19 Sieng Sotheara
	Update REF Invoice 2016 & 2017
@ 2018-09-19 Pov Lyhorng
	fix Organization customer is non subsidy type
@ 2019-01-16 Phuong Sovathvong
	Update REF Invoice 2019
@ 2019-12-12 HOR DARA
	Update REF Invoice 2020
@ 2020-07-28 HOR DARA
	Update REF Invoice 2020 - market vendor get subsidy even over 2000kwh (49 licensee)
@ 2021-01-08 Vonn kimputhmunyvorn
	Update REF Invoice 2021
*/
DECLARE @LICENSE_TYPE_ID INT,
        @BASED_TARIFF DECIMAL(18, 5),
        @SUBSIDY_TARIFF DECIMAL(18, 5);
SELECT @LICENSE_TYPE_ID = LICENSE_TYPE_ID,
       @SUBSIDY_TARIFF = SUBSIDY_TARIFF,
       @BASED_TARIFF = BASED_TARIFF
FROM dbo.TBL_TARIFF
WHERE DATA_MONTH = @DATA_MONTH;
CREATE TABLE #RESULT
(
    TYPE_ID INT,
    TYPE_NAME NVARCHAR(255),
    TYPE_OF_SALE_ID INT,
    TYPE_OF_SALE_NAME NVARCHAR(255),
    HISTORY_ID BIGINT,
    CUSTOMER_GROUP_ID INT,
    CUSTOMER_GROUP_NAME NVARCHAR(200),
    INVOICE_ID BIGINT,
    DATA_MONTH DATETIME,
    CUSTOMER_ID INT,
    CUSTOMER_CODE NVARCHAR(50),
    FIRST_NAME_KH NVARCHAR(50),
    LAST_NAME_KH NVARCHAR(50),
    AREA_ID INT,
    AREA_CODE NVARCHAR(50),
    BOX_ID INT,
    BOX_CODE NVARCHAR(50),
    CUSTOMER_CONNECTION_TYPE_ID INT,
    CUSTOMER_CONNECTION_TYPE_NAME NVARCHAR(200),
    AMPARE_ID INT,
    AMPARE_NAME NVARCHAR(50),
    PRICE_ID INT,
    METER_ID INT,
    METER_CODE NVARCHAR(50),
    MULTIPLIER INT,
    CURRENCY_SIGN NVARCHAR(50),
    CURRENCY_NAME NVARCHAR(50),
    CURRENCY_ID INT,
    PRICE DECIMAL(18, 5),
    BASED_PRICE DECIMAL(18, 5),
    START_USAGE DECIMAL(18, 4),
    END_USAGE DECIMAL(18, 4),
    TOTAL_USAGE DECIMAL(18, 4),
    TOTAL_AMOUNT DECIMAL(18, 5),
    IS_POST_PAID BIT,
    IS_AGRICULTURE BIT,
    SHIFT_ID INT,
    IS_ACTIVE BIT,
    BILLING_CYCLE_ID INT,
    BILLING_CYCLE_NAME NVARCHAR(50),
    ROW_DATE DATETIME,
    CUSTOMER_TYPE_ID INT,
    CUSTOMER_TYPE_NAME NVARCHAR(50),
    LICENSE_NUMBER NVARCHAR(50)
);
/*REF 2016*/
IF @DATA_MONTH
   BETWEEN '2016-03-01' AND '2017-02-01'
BEGIN
    INSERT INTO #RESULT
    /*លក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ*/
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'លក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 3
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 4
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'លក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 3
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 3
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'លក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 3
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 2
    UNION ALL
    /*លក់ឲ្យអតិថិជន MV​(ឧស្សាហកម្ម ពាណិជ្ជកម្ម)*/
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'លក់ឲ្យអតិថិជន MV​(ឧស្សាហកម្ម ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 2
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 4
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'លក់ឲ្យអតិថិជន MV​(ឧស្សាហកម្ម ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 2
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 3
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'លក់ឲ្យអតិថិជន MV​(ឧស្សាហកម្ម ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 2
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 2
    UNION ALL
    /*លក់លើតង់ស្យុងទាប*/
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'លក់លើតង់ស្យុងទាប',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'អតិថិជនធម្មតា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 1
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 1;
END;
/*REF 2017*/
ELSE IF @DATA_MONTH
        BETWEEN '2017-03-01' AND '2018-12-01'
BEGIN
    INSERT INTO #RESULT
    /*ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ*/
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 3
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 11
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 3
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 12
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 3
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 13
    UNION ALL
    /*ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)*/
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID IN ( 1, 2 )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 4, 7, 8, 10 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID IN ( 1, 2 )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 3, 6 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID IN ( 1, 2 )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 2, 5 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID IN ( 1, 2, 5 )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9, 14, 15, 16 )
          AND i.TOTAL_USAGE >= 2001
    UNION ALL
    /*ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)*/
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)',
           TYPE_OF_SALE_ID = -1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើក្រោម 2001 kWh/ខែ ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID IN ( 1, 2, 5 )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9, 14, 15, 16 )
          AND i.TOTAL_USAGE < 2001
    UNION ALL
    /* ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន */
    SELECT TYPE_ID = 0,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើតិចជាង  10 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID IN ( 4 )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE <= 10
    UNION ALL
    SELECT TYPE_ID = 0,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID IN ( 4 )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE
          BETWEEN 11 AND 50
    UNION ALL
    SELECT TYPE_ID = 0,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើធំជាង 51 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID IN ( 4 )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE >= 51;
END;
/*REF 2019*/
ELSE IF @DATA_MONTH
        BETWEEN '2019-01-01' AND '2020-01-01'
BEGIN
    INSERT INTO #RESULT
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 11
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 12
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 13
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 4 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវាលា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 7 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 3 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 2 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 14 )
          AND i.TOTAL_USAGE >= 2001
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 8 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 10 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 6 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 5 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 15, 16 )
          AND i.TOTAL_USAGE >= 2001
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអតិថិជន សាលារៀន មន្ទីរពេទ្យ មណ្ឌលសុខភាព នៅតំបន់ជនបទ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)',
           TYPE_OF_SALE_ID = -1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើក្រោម 2001 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 14, 15, 16 )
          AND i.TOTAL_USAGE < 2001
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើតិចជាង  10 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE <= 10
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE
          BETWEEN 11 AND 50
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 51 ដល់ 200 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE
          BETWEEN 51 AND 200
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើធំជាង 201 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE >= 201;
END;
/*REF 2020 - NORMAL LICENSE TYPE ID =1*/
ELSE IF @DATA_MONTH
        BETWEEN '2020-02-01' AND '2020-12-01'
        AND @LICENSE_TYPE_ID = 1
BEGIN
    INSERT INTO #RESULT
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 11
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 12
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 13
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 4 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 7 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 3 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 17 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 2 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 18 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 14 )
          AND i.TOTAL_USAGE >= 2001
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 8 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 10 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 6 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 19 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 5 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 20 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = -1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 15, 16 )
          AND i.TOTAL_USAGE >= 2001
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = -2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអតិថិជន សាលារៀន មន្ទីរពេទ្យ មណ្ឌលសុខភាព នៅតំបន់ជនបទ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)',
           TYPE_OF_SALE_ID = -3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើក្រោម 2001 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 14, 15, 16 )
          AND i.TOTAL_USAGE < 2001
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើតិចជាង  10 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE <= 10
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE
          BETWEEN 11 AND 50
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 51 ដល់ 200 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE
          BETWEEN 51 AND 200
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -7,
           TYPE_OF_SALE_NAE = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើធំជាង 201 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE >= 201;
END;
/*REF 2020 - LICENSE TYPE ID =2*/
ELSE IF @DATA_MONTH
        BETWEEN '2020-02-01' AND '2020-12-01'
        AND @LICENSE_TYPE_ID = 2
BEGIN
    INSERT INTO #RESULT
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជននៅក្នុងបរិវេណផ្សារ',
           TYPE_OF_SALE_ID = -1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនក្នុងផ្សារ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 14, 15, 16 );
--AND i.TOTAL_USAGE<2001
END;

/*REF 2021 - NORMAL LICENSE TYPE ID =1*/
ELSE IF @DATA_MONTH
        BETWEEN '2021-01-01' AND '9999-12-01'
        AND @LICENSE_TYPE_ID = 1
BEGIN
    INSERT INTO #RESULT
    SELECT TYPE_ID = 12,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 11
    UNION ALL
    SELECT TYPE_ID = 12,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 12
    UNION ALL
    SELECT TYPE_ID = 12,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 13

    --ឧស្សាហកម្មវិស័យរុករករ៉ែ
    UNION ALL
    SELECT TYPE_ID = 11,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 12,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 26
    UNION ALL
    SELECT TYPE_ID = 11,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 27
    UNION ALL
    SELECT TYPE_ID = 11,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 28
    UNION ALL
    SELECT TYPE_ID = 11,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 29
    UNION ALL
    SELECT TYPE_ID = 11,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 30
    UNION ALL
    SELECT TYPE_ID = 11,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 31
    UNION ALL
    SELECT TYPE_ID = 11,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 32
    UNION ALL
    SELECT TYPE_ID = 11,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 33
    UNION ALL
    SELECT TYPE_ID = 11,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 34
    UNION ALL
    SELECT TYPE_ID = 11,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 35
    UNION ALL
    SELECT TYPE_ID = 11,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 36
    UNION ALL
    SELECT TYPE_ID = 11,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ) ',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 37

    --ឧស្សាហកម្មវិស័យកម្មន្តសាល
    UNION ALL
    SELECT TYPE_ID = 10,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 12,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 38
    UNION ALL
    SELECT TYPE_ID = 10,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 39
    UNION ALL
    SELECT TYPE_ID = 10,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 40
    UNION ALL
    SELECT TYPE_ID = 10,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 41
    UNION ALL
    SELECT TYPE_ID = 10,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 42
    UNION ALL
    SELECT TYPE_ID = 10,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 43
    UNION ALL
    SELECT TYPE_ID = 10,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 44
    UNION ALL
    SELECT TYPE_ID = 10,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 45
    UNION ALL
    SELECT TYPE_ID = 10,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 46
    UNION ALL
    SELECT TYPE_ID = 10,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 47
    UNION ALL
    SELECT TYPE_ID = 10,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 48
    UNION ALL
    SELECT TYPE_ID = 10,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល) ',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 49

    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ
    UNION ALL
    SELECT TYPE_ID = 9,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 12,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 50
    UNION ALL
    SELECT TYPE_ID = 9,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 51
    UNION ALL
    SELECT TYPE_ID = 9,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 52
    UNION ALL
    SELECT TYPE_ID = 9,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 53
    UNION ALL
    SELECT TYPE_ID = 9,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 54
    UNION ALL
    SELECT TYPE_ID = 9,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 55
    UNION ALL
    SELECT TYPE_ID = 9,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 56
    UNION ALL
    SELECT TYPE_ID = 9,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 57
    UNION ALL
    SELECT TYPE_ID = 9,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 58
    UNION ALL
    SELECT TYPE_ID = 9,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 59
    UNION ALL
    SELECT TYPE_ID = 9,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 60
    UNION ALL
    SELECT TYPE_ID = 9,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ) ',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 61

    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ
    UNION ALL
    SELECT TYPE_ID = 8,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 12,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 62
    UNION ALL
    SELECT TYPE_ID = 8,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 63
    UNION ALL
    SELECT TYPE_ID = 8,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 64
    UNION ALL
    SELECT TYPE_ID = 8,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 65
    UNION ALL
    SELECT TYPE_ID = 8,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 66
    UNION ALL
    SELECT TYPE_ID = 8,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 67
    UNION ALL
    SELECT TYPE_ID = 8,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 68
    UNION ALL
    SELECT TYPE_ID = 8,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 69
    UNION ALL
    SELECT TYPE_ID = 8,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 70
    UNION ALL
    SELECT TYPE_ID = 8,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 71
    UNION ALL
    SELECT TYPE_ID = 8,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 72
    UNION ALL
    SELECT TYPE_ID = 8,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ) ',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 73

    --សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។)
    UNION ALL
    SELECT TYPE_ID = 7,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 12,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 74
    UNION ALL
    SELECT TYPE_ID = 7,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 75
    UNION ALL
    SELECT TYPE_ID = 7,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 76
    UNION ALL
    SELECT TYPE_ID = 7,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 77
    UNION ALL
    SELECT TYPE_ID = 7,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 78
    UNION ALL
    SELECT TYPE_ID = 7,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 79
    UNION ALL
    SELECT TYPE_ID = 7,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 80
    UNION ALL
    SELECT TYPE_ID = 7,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 81
    UNION ALL
    SELECT TYPE_ID = 7,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 82
    UNION ALL
    SELECT TYPE_ID = 7,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 83
    UNION ALL
    SELECT TYPE_ID = 7,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 84
    UNION ALL
    SELECT TYPE_ID = 7,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម) ',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 85

    --សកម្មភាពកែច្នៃផលិតផលកសិកម្ម
    UNION ALL
    SELECT TYPE_ID = 6,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 12,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 86
    UNION ALL
    SELECT TYPE_ID = 6,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 87
    UNION ALL
    SELECT TYPE_ID = 6,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 88
    UNION ALL
    SELECT TYPE_ID = 6,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 89
    UNION ALL
    SELECT TYPE_ID = 6,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 90
    UNION ALL
    SELECT TYPE_ID = 6,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 91
    UNION ALL
    SELECT TYPE_ID = 6,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 92
    UNION ALL
    SELECT TYPE_ID = 6,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 93
    UNION ALL
    SELECT TYPE_ID = 6,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 94
    UNION ALL
    SELECT TYPE_ID = 6,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 95
    UNION ALL
    SELECT TYPE_ID = 6,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 96
    UNION ALL
    SELECT TYPE_ID = 6,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម) ',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 97

    --ពាណិជ្ជកម្ម
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 12,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 98
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 99
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 100
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 101
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 102
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 103
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 104
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 105
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 106
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 107
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 108
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម) ',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 109

    --រដ្ឋបាលសាធារណៈ
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 12,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 110
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 111
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 112
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 113
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 114
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 115
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 116
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 117
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 118
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 119
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 120
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ) ',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 121

    --សេវាកម្មផ្សេងៗ
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 12,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 122
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 123
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 124
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 125
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 126
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 127
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 128
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 129
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 130
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 131
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 132
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ) ',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 133
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែបណ្តាញសាធារណៈ)',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 22, 23, 134, 135, 136, 137, 138, 139, 140, 144 )
          AND i.TOTAL_USAGE >= 2001
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអតិថិជន សាលារៀន មន្ទីរពេទ្យ មណ្ឌលសុខភាព នៅតំបន់ជនបទ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើក្រោម 2001 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 22, 23, 134, 135, 136, 137, 138, 139, 140,144 )
          AND i.TOTAL_USAGE < 2001
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើតិចជាង  10 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE <= 10
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE
          BETWEEN 11 AND 50
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 51 ដល់ 200 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE
          BETWEEN 51 AND 200
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAE = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើធំជាង 201 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE >= 201
    UNION ALL
    SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម(ទិញ MV)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
          AND i.PRICE = 0.13700
    UNION ALL
    SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម (ទិញ MV ម៉ោង៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
          AND i.PRICE = 480
    UNION ALL
    SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម (ទិញ LV ត្រង់ស្វូរអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
          AND i.PRICE = 0.15048
    UNION ALL
    SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម (ទិញ LV ត្រង់ស្វូរអ្នកលក់ ម៉ោង៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
          AND i.PRICE = 480
    UNION ALL
    SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម (ទិញ LV ត្រង់ស្វូរអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
          AND i.PRICE = 0.14248
    UNION ALL
    SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម (ទិញ LV ត្រង់ស្វូរអ្នកទិញ ម៉ោង៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
          AND i.PRICE = 480
    UNION ALL
    SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម (ទិញ LV សាធារណៈ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
          AND i.PRICE = 730
    UNION ALL
    SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម (ទិញ LV សាធារណៈ ៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
          AND i.PRICE = 480
END;
ELSE IF @DATA_MONTH
        BETWEEN '2021-01-01' AND '9999-12-31'
        AND @LICENSE_TYPE_ID = 2
BEGIN
    INSERT INTO #RESULT
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជននៅក្នុងបរិវេណផ្សារ',
           TYPE_OF_SALE_ID = -1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនក្នុងផ្សារ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 144 );
END;

/*RESULT*/
SELECT r.*,
       METER_FORMAT = REPLACE(LTRIM(REPLACE(ISNULL(METER_CODE, '-'), '0', ' ')), ' ', '0')
FROM #RESULT r
WHERE DATA_MONTH = @DATA_MONTH
      AND IS_ACTIVE = 1
      AND
      (
          @CUSTOMER_GROUP_ID = 0
          OR CUSTOMER_GROUP_ID = @CUSTOMER_GROUP_ID
      )
      AND
      (
          @CURRENCY_ID = 0
          OR CURRENCY_ID = @CURRENCY_ID
      )
      AND
      (
          @CUSTOMER_CONNECTION_TYPE_ID = 0
          OR CUSTOMER_CONNECTION_TYPE_ID = @CUSTOMER_CONNECTION_TYPE_ID
      )
      AND (TOTAL_USAGE
      BETWEEN @V1 AND @V2
          )
      AND
      (
          @BILLING_CYCLE_ID = 0
          OR BILLING_CYCLE_ID = @BILLING_CYCLE_ID
      )
      AND
      (
          @AREA_ID = 0
          OR AREA_ID = @AREA_ID
      )
ORDER BY TYPE_ID DESC,
         TYPE_OF_SALE_ID DESC,
         AREA_CODE,
         BOX_CODE;
--END REPORT_REF_INVOICE

GO

IF OBJECT_ID('REPORT_OTHER_INCOME')IS NOT NULL
	DROP PROC REPORT_OTHER_INCOME
GO
CREATE PROC dbo.REPORT_OTHER_INCOME
    @MONTH DATETIME = '2012-08-01',
    @AREA_ID INT = 0,
    @ITEM_TYPE_ID INT = 0,
    @ITEM_ID INT = 0,
    @CURRENCY_ID INT = 0
AS
SET @MONTH = CONVERT(NVARCHAR(7), @MONTH, 126) + '-01';
SELECT cx.CURRENCY_ID,
       cx.CURRENCY_NAME,
       cx.CURRENCY_SING,
       c.CUSTOMER_CODE,
       CUSTOMER_NAME = c.LAST_NAME_KH + ' ' + c.FIRST_NAME_KH,
       p.POLE_CODE,
       b.BOX_CODE,
       i.INVOICE_NO,
       i.INVOICE_MONTH,
       i.INVOICE_DATE,
       t.INVOICE_ITEM_ID,
       t.INVOICE_ITEM_NAME,
       tt.INVOICE_ITEM_TYPE_ID,
       tt.INVOICE_ITEM_TYPE_NAME,
       i.SETTLE_AMOUNT,
       i.PAID_AMOUNT
FROM TBL_INVOICE i
    CROSS APPLY
(
    SELECT TOP 1
           INVOICE_ITEM_ID
    FROM TBL_INVOICE_DETAIL
    WHERE INVOICE_ID = i.INVOICE_ID
) d
    INNER JOIN TBL_INVOICE_ITEM t
        ON t.INVOICE_ITEM_ID = d.INVOICE_ITEM_ID
    INNER JOIN TBL_INVOICE_ITEM_TYPE tt
        ON tt.INVOICE_ITEM_TYPE_ID = t.INVOICE_ITEM_TYPE_ID
    INNER JOIN TBL_CUSTOMER c
        ON c.CUSTOMER_ID = i.CUSTOMER_ID
    LEFT JOIN TBL_CUSTOMER_METER cm
        ON cm.CUSTOMER_ID = c.CUSTOMER_ID
           AND cm.IS_ACTIVE = 1
    LEFT JOIN TBL_POLE p
        ON p.POLE_ID = cm.POLE_ID
    LEFT JOIN TBL_BOX b
        ON b.BOX_ID = cm.BOX_ID
    INNER JOIN TLKP_CURRENCY cx
        ON cx.CURRENCY_ID = i.CURRENCY_ID
WHERE INVOICE_MONTH = @MONTH
      AND i.INVOICE_STATUS NOT IN ( 3 )
      AND
      (
          @AREA_ID = 0
          OR c.AREA_ID = @AREA_ID
      )
      AND
      (
          @ITEM_TYPE_ID = 0
          OR t.INVOICE_ITEM_TYPE_ID = @ITEM_TYPE_ID
      )
      AND
      (
          @ITEM_ID = 0
          OR t.INVOICE_ITEM_ID = @ITEM_ID
      )
      AND
      (
          @CURRENCY_ID = 0
          OR cx.CURRENCY_ID = @CURRENCY_ID
      )
ORDER BY i.INVOICE_NO ASC
--- END OF REPORT_OTHER_INCOME

GO
/****** Object:  Table [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_GROUP]    Script Date: 3/18/2021 5:46:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TLKP_REPORT_QUARTER_CUSTOMER_GROUP]') AND type in (N'U'))
DROP TABLE [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_GROUP]
GO
/****** Object:  Table [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE]    Script Date: 3/18/2021 5:46:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE]') AND type in (N'U'))
DROP TABLE [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE]
GO
/****** Object:  Table [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE]    Script Date: 3/18/2021 5:46:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE](
	[REPORT_DETAIL_TEMPLATE_ID] [int] IDENTITY(1,1) NOT NULL,
	[CUSTOMER_CONNECTION_TYPE_IDS] [nvarchar](500) NULL,
	[ORDERING] [int] NULL,
	[SHOW_ZERO] [bit] NULL,
	[EFFECTIVE_FROM] [datetime] NULL,
	[EFFECTIVE_TO] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_GROUP]    Script Date: 3/18/2021 5:46:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_GROUP](
	[REPORT_GROUP_TEMPLATE_ID] [int] IDENTITY(1,1) NOT NULL,
	[CUSTOMER_GROUP_ID] [int] NULL,
	[ORDERING] [int] NULL,
	[EFFECTIVE_FROM] [datetime] NULL,
	[EFFECTIVE_TO] [datetime] NULL,
	[SHOW_ZERO] [bit] NULL,
	[CUSTOMER_CONNECTION_TYPE_IDS] [nvarchar](500) NULL
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] ON 
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] ([REPORT_DETAIL_TEMPLATE_ID], [CUSTOMER_CONNECTION_TYPE_IDS], [ORDERING], [SHOW_ZERO], [EFFECTIVE_FROM], [EFFECTIVE_TO]) VALUES (1, N'1', 1, 0, CAST(N'2021-01-01T00:00:00.000' AS DateTime), CAST(N'2050-12-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] ([REPORT_DETAIL_TEMPLATE_ID], [CUSTOMER_CONNECTION_TYPE_IDS], [ORDERING], [SHOW_ZERO], [EFFECTIVE_FROM], [EFFECTIVE_TO]) VALUES (2, N'22, 23, 134, 144', 2, 0, CAST(N'2021-01-01T00:00:00.000' AS DateTime), CAST(N'2050-12-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] ([REPORT_DETAIL_TEMPLATE_ID], [CUSTOMER_CONNECTION_TYPE_IDS], [ORDERING], [SHOW_ZERO], [EFFECTIVE_FROM], [EFFECTIVE_TO]) VALUES (3, N'135, 136, 137', 3, 0, CAST(N'2021-01-01T00:00:00.000' AS DateTime), CAST(N'2050-12-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] ([REPORT_DETAIL_TEMPLATE_ID], [CUSTOMER_CONNECTION_TYPE_IDS], [ORDERING], [SHOW_ZERO], [EFFECTIVE_FROM], [EFFECTIVE_TO]) VALUES (4, N' 138, 139, 140 ', 4, 0, CAST(N'2021-01-01T00:00:00.000' AS DateTime), CAST(N'2050-12-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] ([REPORT_DETAIL_TEMPLATE_ID], [CUSTOMER_CONNECTION_TYPE_IDS], [ORDERING], [SHOW_ZERO], [EFFECTIVE_FROM], [EFFECTIVE_TO]) VALUES (5, N'9', 5, 0, CAST(N'2021-01-01T00:00:00.000' AS DateTime), CAST(N'2050-12-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] ([REPORT_DETAIL_TEMPLATE_ID], [CUSTOMER_CONNECTION_TYPE_IDS], [ORDERING], [SHOW_ZERO], [EFFECTIVE_FROM], [EFFECTIVE_TO]) VALUES (6, N' 24, 141, 142, 143', 6, 0, CAST(N'2021-01-01T00:00:00.000' AS DateTime), CAST(N'2050-12-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] ([REPORT_DETAIL_TEMPLATE_ID], [CUSTOMER_CONNECTION_TYPE_IDS], [ORDERING], [SHOW_ZERO], [EFFECTIVE_FROM], [EFFECTIVE_TO]) VALUES (7, N'26, 27, 28, 29, 30, 31, 32, 33, 34,35, 36, 37', 7, 0, CAST(N'2021-01-01T00:00:00.000' AS DateTime), CAST(N'2050-12-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] ([REPORT_DETAIL_TEMPLATE_ID], [CUSTOMER_CONNECTION_TYPE_IDS], [ORDERING], [SHOW_ZERO], [EFFECTIVE_FROM], [EFFECTIVE_TO]) VALUES (18, N'122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133', 15, 0, CAST(N'2021-01-01T00:00:00.000' AS DateTime), CAST(N'2050-12-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] ([REPORT_DETAIL_TEMPLATE_ID], [CUSTOMER_CONNECTION_TYPE_IDS], [ORDERING], [SHOW_ZERO], [EFFECTIVE_FROM], [EFFECTIVE_TO]) VALUES (9, N'38, 39, 40, 41, 42, 43, 44, 45, 46,47, 48, 49', 8, 0, CAST(N'2021-01-01T00:00:00.000' AS DateTime), CAST(N'2050-12-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] ([REPORT_DETAIL_TEMPLATE_ID], [CUSTOMER_CONNECTION_TYPE_IDS], [ORDERING], [SHOW_ZERO], [EFFECTIVE_FROM], [EFFECTIVE_TO]) VALUES (10, N'50, 51, 52, 53, 54, 55, 56, 57, 58,59, 60, 61', 9, 0, CAST(N'2021-01-01T00:00:00.000' AS DateTime), CAST(N'2050-12-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] ([REPORT_DETAIL_TEMPLATE_ID], [CUSTOMER_CONNECTION_TYPE_IDS], [ORDERING], [SHOW_ZERO], [EFFECTIVE_FROM], [EFFECTIVE_TO]) VALUES (11, N'62, 63, 64, 65, 66, 67, 68, 69, 70,71, 72, 73', 10, 0, CAST(N'2021-01-01T00:00:00.000' AS DateTime), CAST(N'2050-12-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] ([REPORT_DETAIL_TEMPLATE_ID], [CUSTOMER_CONNECTION_TYPE_IDS], [ORDERING], [SHOW_ZERO], [EFFECTIVE_FROM], [EFFECTIVE_TO]) VALUES (12, N'74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85', 11, 0, CAST(N'2021-01-01T00:00:00.000' AS DateTime), CAST(N'2050-12-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] ([REPORT_DETAIL_TEMPLATE_ID], [CUSTOMER_CONNECTION_TYPE_IDS], [ORDERING], [SHOW_ZERO], [EFFECTIVE_FROM], [EFFECTIVE_TO]) VALUES (13, N'86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97', 12, 0, CAST(N'2021-01-01T00:00:00.000' AS DateTime), CAST(N'2050-12-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] ([REPORT_DETAIL_TEMPLATE_ID], [CUSTOMER_CONNECTION_TYPE_IDS], [ORDERING], [SHOW_ZERO], [EFFECTIVE_FROM], [EFFECTIVE_TO]) VALUES (15, N'98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109', 13, 0, CAST(N'2021-01-01T00:00:00.000' AS DateTime), CAST(N'2050-12-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] ([REPORT_DETAIL_TEMPLATE_ID], [CUSTOMER_CONNECTION_TYPE_IDS], [ORDERING], [SHOW_ZERO], [EFFECTIVE_FROM], [EFFECTIVE_TO]) VALUES (16, N'110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121', 14, 0, CAST(N'2021-01-01T00:00:00.000' AS DateTime), CAST(N'2050-12-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] ([REPORT_DETAIL_TEMPLATE_ID], [CUSTOMER_CONNECTION_TYPE_IDS], [ORDERING], [SHOW_ZERO], [EFFECTIVE_FROM], [EFFECTIVE_TO]) VALUES (17, N'1', 1, 1, CAST(N'2010-01-01T00:00:00.000' AS DateTime), CAST(N'2020-12-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] ([REPORT_DETAIL_TEMPLATE_ID], [CUSTOMER_CONNECTION_TYPE_IDS], [ORDERING], [SHOW_ZERO], [EFFECTIVE_FROM], [EFFECTIVE_TO]) VALUES (19, N'4', 2, 1, CAST(N'2010-01-01T00:00:00.000' AS DateTime), CAST(N'2020-12-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] ([REPORT_DETAIL_TEMPLATE_ID], [CUSTOMER_CONNECTION_TYPE_IDS], [ORDERING], [SHOW_ZERO], [EFFECTIVE_FROM], [EFFECTIVE_TO]) VALUES (20, N'7', 3, 1, CAST(N'2010-01-01T00:00:00.000' AS DateTime), CAST(N'2020-12-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] ([REPORT_DETAIL_TEMPLATE_ID], [CUSTOMER_CONNECTION_TYPE_IDS], [ORDERING], [SHOW_ZERO], [EFFECTIVE_FROM], [EFFECTIVE_TO]) VALUES (21, N'21', 4, 1, CAST(N'2010-01-01T00:00:00.000' AS DateTime), CAST(N'2020-12-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] ([REPORT_DETAIL_TEMPLATE_ID], [CUSTOMER_CONNECTION_TYPE_IDS], [ORDERING], [SHOW_ZERO], [EFFECTIVE_FROM], [EFFECTIVE_TO]) VALUES (22, N'2', 5, 1, CAST(N'2010-01-01T00:00:00.000' AS DateTime), CAST(N'2020-12-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] ([REPORT_DETAIL_TEMPLATE_ID], [CUSTOMER_CONNECTION_TYPE_IDS], [ORDERING], [SHOW_ZERO], [EFFECTIVE_FROM], [EFFECTIVE_TO]) VALUES (23, N'18', 6, 1, CAST(N'2010-01-01T00:00:00.000' AS DateTime), CAST(N'2020-12-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] ([REPORT_DETAIL_TEMPLATE_ID], [CUSTOMER_CONNECTION_TYPE_IDS], [ORDERING], [SHOW_ZERO], [EFFECTIVE_FROM], [EFFECTIVE_TO]) VALUES (24, N'3', 7, 1, CAST(N'2010-01-01T00:00:00.000' AS DateTime), CAST(N'2020-12-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] ([REPORT_DETAIL_TEMPLATE_ID], [CUSTOMER_CONNECTION_TYPE_IDS], [ORDERING], [SHOW_ZERO], [EFFECTIVE_FROM], [EFFECTIVE_TO]) VALUES (25, N'7', 8, 1, CAST(N'2010-01-01T00:00:00.000' AS DateTime), CAST(N'2020-12-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] ([REPORT_DETAIL_TEMPLATE_ID], [CUSTOMER_CONNECTION_TYPE_IDS], [ORDERING], [SHOW_ZERO], [EFFECTIVE_FROM], [EFFECTIVE_TO]) VALUES (26, N'14', 9, 1, CAST(N'2010-01-01T00:00:00.000' AS DateTime), CAST(N'2020-12-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] ([REPORT_DETAIL_TEMPLATE_ID], [CUSTOMER_CONNECTION_TYPE_IDS], [ORDERING], [SHOW_ZERO], [EFFECTIVE_FROM], [EFFECTIVE_TO]) VALUES (27, N'8', 10, 1, CAST(N'2010-01-01T00:00:00.000' AS DateTime), CAST(N'2020-12-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] ([REPORT_DETAIL_TEMPLATE_ID], [CUSTOMER_CONNECTION_TYPE_IDS], [ORDERING], [SHOW_ZERO], [EFFECTIVE_FROM], [EFFECTIVE_TO]) VALUES (28, N'10', 11, 1, CAST(N'2010-01-01T00:00:00.000' AS DateTime), CAST(N'2020-12-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] ([REPORT_DETAIL_TEMPLATE_ID], [CUSTOMER_CONNECTION_TYPE_IDS], [ORDERING], [SHOW_ZERO], [EFFECTIVE_FROM], [EFFECTIVE_TO]) VALUES (29, N'5', 12, 1, CAST(N'2010-01-01T00:00:00.000' AS DateTime), CAST(N'2020-12-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] ([REPORT_DETAIL_TEMPLATE_ID], [CUSTOMER_CONNECTION_TYPE_IDS], [ORDERING], [SHOW_ZERO], [EFFECTIVE_FROM], [EFFECTIVE_TO]) VALUES (30, N'6', 13, 1, CAST(N'2010-01-01T00:00:00.000' AS DateTime), CAST(N'2020-12-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] ([REPORT_DETAIL_TEMPLATE_ID], [CUSTOMER_CONNECTION_TYPE_IDS], [ORDERING], [SHOW_ZERO], [EFFECTIVE_FROM], [EFFECTIVE_TO]) VALUES (31, N'19', 14, 1, CAST(N'2010-01-01T00:00:00.000' AS DateTime), CAST(N'2020-12-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] ([REPORT_DETAIL_TEMPLATE_ID], [CUSTOMER_CONNECTION_TYPE_IDS], [ORDERING], [SHOW_ZERO], [EFFECTIVE_FROM], [EFFECTIVE_TO]) VALUES (32, N'15', 15, 1, CAST(N'2010-01-01T00:00:00.000' AS DateTime), CAST(N'2020-12-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] ([REPORT_DETAIL_TEMPLATE_ID], [CUSTOMER_CONNECTION_TYPE_IDS], [ORDERING], [SHOW_ZERO], [EFFECTIVE_FROM], [EFFECTIVE_TO]) VALUES (33, N'16', 16, 1, CAST(N'2010-01-01T00:00:00.000' AS DateTime), CAST(N'2020-12-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] ([REPORT_DETAIL_TEMPLATE_ID], [CUSTOMER_CONNECTION_TYPE_IDS], [ORDERING], [SHOW_ZERO], [EFFECTIVE_FROM], [EFFECTIVE_TO]) VALUES (34, N'9', 17, 1, CAST(N'2010-01-01T00:00:00.000' AS DateTime), CAST(N'2020-12-31T00:00:00.000' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE] OFF
GO
SET IDENTITY_INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_GROUP] ON 
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_GROUP] ([REPORT_GROUP_TEMPLATE_ID], [CUSTOMER_GROUP_ID], [ORDERING], [EFFECTIVE_FROM], [EFFECTIVE_TO], [SHOW_ZERO], [CUSTOMER_CONNECTION_TYPE_IDS]) VALUES (1, 4, 1, CAST(N'2021-01-01T00:00:00.000' AS DateTime), CAST(N'2050-12-31T00:00:00.000' AS DateTime), 0, NULL)
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_GROUP] ([REPORT_GROUP_TEMPLATE_ID], [CUSTOMER_GROUP_ID], [ORDERING], [EFFECTIVE_FROM], [EFFECTIVE_TO], [SHOW_ZERO], [CUSTOMER_CONNECTION_TYPE_IDS]) VALUES (2, 7, 2, CAST(N'2021-01-01T00:00:00.000' AS DateTime), CAST(N'2050-12-31T00:00:00.000' AS DateTime), 0, NULL)
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_GROUP] ([REPORT_GROUP_TEMPLATE_ID], [CUSTOMER_GROUP_ID], [ORDERING], [EFFECTIVE_FROM], [EFFECTIVE_TO], [SHOW_ZERO], [CUSTOMER_CONNECTION_TYPE_IDS]) VALUES (3, 17, 3, CAST(N'2021-01-01T00:00:00.000' AS DateTime), CAST(N'2050-12-31T00:00:00.000' AS DateTime), 0, NULL)
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_GROUP] ([REPORT_GROUP_TEMPLATE_ID], [CUSTOMER_GROUP_ID], [ORDERING], [EFFECTIVE_FROM], [EFFECTIVE_TO], [SHOW_ZERO], [CUSTOMER_CONNECTION_TYPE_IDS]) VALUES (4, 18, 4, CAST(N'2021-01-01T00:00:00.000' AS DateTime), CAST(N'2050-12-31T00:00:00.000' AS DateTime), 0, NULL)
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_GROUP] ([REPORT_GROUP_TEMPLATE_ID], [CUSTOMER_GROUP_ID], [ORDERING], [EFFECTIVE_FROM], [EFFECTIVE_TO], [SHOW_ZERO], [CUSTOMER_CONNECTION_TYPE_IDS]) VALUES (5, 5, 5, CAST(N'2021-01-01T00:00:00.000' AS DateTime), CAST(N'2050-12-31T00:00:00.000' AS DateTime), 0, NULL)
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_GROUP] ([REPORT_GROUP_TEMPLATE_ID], [CUSTOMER_GROUP_ID], [ORDERING], [EFFECTIVE_FROM], [EFFECTIVE_TO], [SHOW_ZERO], [CUSTOMER_CONNECTION_TYPE_IDS]) VALUES (6, 6, 6, CAST(N'2021-01-01T00:00:00.000' AS DateTime), CAST(N'2050-12-31T00:00:00.000' AS DateTime), 0, NULL)
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_GROUP] ([REPORT_GROUP_TEMPLATE_ID], [CUSTOMER_GROUP_ID], [ORDERING], [EFFECTIVE_FROM], [EFFECTIVE_TO], [SHOW_ZERO], [CUSTOMER_CONNECTION_TYPE_IDS]) VALUES (7, 16, 7, CAST(N'2021-01-01T00:00:00.000' AS DateTime), CAST(N'2050-12-31T00:00:00.000' AS DateTime), 0, NULL)
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_GROUP] ([REPORT_GROUP_TEMPLATE_ID], [CUSTOMER_GROUP_ID], [ORDERING], [EFFECTIVE_FROM], [EFFECTIVE_TO], [SHOW_ZERO], [CUSTOMER_CONNECTION_TYPE_IDS]) VALUES (8, 15, 8, CAST(N'2021-01-01T00:00:00.000' AS DateTime), CAST(N'2050-12-31T00:00:00.000' AS DateTime), 0, NULL)
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_GROUP] ([REPORT_GROUP_TEMPLATE_ID], [CUSTOMER_GROUP_ID], [ORDERING], [EFFECTIVE_FROM], [EFFECTIVE_TO], [SHOW_ZERO], [CUSTOMER_CONNECTION_TYPE_IDS]) VALUES (9, 14, 9, CAST(N'2021-01-01T00:00:00.000' AS DateTime), CAST(N'2050-12-31T00:00:00.000' AS DateTime), 0, NULL)
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_GROUP] ([REPORT_GROUP_TEMPLATE_ID], [CUSTOMER_GROUP_ID], [ORDERING], [EFFECTIVE_FROM], [EFFECTIVE_TO], [SHOW_ZERO], [CUSTOMER_CONNECTION_TYPE_IDS]) VALUES (10, 13, 10, CAST(N'2021-01-01T00:00:00.000' AS DateTime), CAST(N'2050-12-31T00:00:00.000' AS DateTime), 0, NULL)
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_GROUP] ([REPORT_GROUP_TEMPLATE_ID], [CUSTOMER_GROUP_ID], [ORDERING], [EFFECTIVE_FROM], [EFFECTIVE_TO], [SHOW_ZERO], [CUSTOMER_CONNECTION_TYPE_IDS]) VALUES (11, 12, 11, CAST(N'2021-01-01T00:00:00.000' AS DateTime), CAST(N'2050-12-31T00:00:00.000' AS DateTime), 0, NULL)
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_GROUP] ([REPORT_GROUP_TEMPLATE_ID], [CUSTOMER_GROUP_ID], [ORDERING], [EFFECTIVE_FROM], [EFFECTIVE_TO], [SHOW_ZERO], [CUSTOMER_CONNECTION_TYPE_IDS]) VALUES (12, 11, 12, CAST(N'2021-01-01T00:00:00.000' AS DateTime), CAST(N'2050-12-31T00:00:00.000' AS DateTime), 0, NULL)
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_GROUP] ([REPORT_GROUP_TEMPLATE_ID], [CUSTOMER_GROUP_ID], [ORDERING], [EFFECTIVE_FROM], [EFFECTIVE_TO], [SHOW_ZERO], [CUSTOMER_CONNECTION_TYPE_IDS]) VALUES (13, 10, 13, CAST(N'2021-01-01T00:00:00.000' AS DateTime), CAST(N'2050-12-31T00:00:00.000' AS DateTime), 0, NULL)
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_GROUP] ([REPORT_GROUP_TEMPLATE_ID], [CUSTOMER_GROUP_ID], [ORDERING], [EFFECTIVE_FROM], [EFFECTIVE_TO], [SHOW_ZERO], [CUSTOMER_CONNECTION_TYPE_IDS]) VALUES (14, 9, 14, CAST(N'2021-01-01T00:00:00.000' AS DateTime), CAST(N'2050-12-31T00:00:00.000' AS DateTime), 0, NULL)
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_GROUP] ([REPORT_GROUP_TEMPLATE_ID], [CUSTOMER_GROUP_ID], [ORDERING], [EFFECTIVE_FROM], [EFFECTIVE_TO], [SHOW_ZERO], [CUSTOMER_CONNECTION_TYPE_IDS]) VALUES (15, 8, 15, CAST(N'2021-01-01T00:00:00.000' AS DateTime), CAST(N'2050-12-31T00:00:00.000' AS DateTime), 0, NULL)
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_GROUP] ([REPORT_GROUP_TEMPLATE_ID], [CUSTOMER_GROUP_ID], [ORDERING], [EFFECTIVE_FROM], [EFFECTIVE_TO], [SHOW_ZERO], [CUSTOMER_CONNECTION_TYPE_IDS]) VALUES (16, 4, 1, CAST(N'2010-01-01T00:00:00.000' AS DateTime), CAST(N'2020-12-31T00:00:00.000' AS DateTime), 1, NULL)
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_GROUP] ([REPORT_GROUP_TEMPLATE_ID], [CUSTOMER_GROUP_ID], [ORDERING], [EFFECTIVE_FROM], [EFFECTIVE_TO], [SHOW_ZERO], [CUSTOMER_CONNECTION_TYPE_IDS]) VALUES (17, 2, 2, CAST(N'2010-01-01T00:00:00.000' AS DateTime), CAST(N'2020-12-31T00:00:00.000' AS DateTime), 1, NULL)
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_GROUP] ([REPORT_GROUP_TEMPLATE_ID], [CUSTOMER_GROUP_ID], [ORDERING], [EFFECTIVE_FROM], [EFFECTIVE_TO], [SHOW_ZERO], [CUSTOMER_CONNECTION_TYPE_IDS]) VALUES (18, 1, 3, CAST(N'2010-01-01T00:00:00.000' AS DateTime), CAST(N'2020-12-31T00:00:00.000' AS DateTime), 1, NULL)
GO
INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_GROUP] ([REPORT_GROUP_TEMPLATE_ID], [CUSTOMER_GROUP_ID], [ORDERING], [EFFECTIVE_FROM], [EFFECTIVE_TO], [SHOW_ZERO], [CUSTOMER_CONNECTION_TYPE_IDS]) VALUES (19, 5, 4, CAST(N'2010-01-01T00:00:00.000' AS DateTime), CAST(N'2020-12-31T00:00:00.000' AS DateTime), 1, NULL)
GO
SET IDENTITY_INSERT [dbo].[TLKP_REPORT_QUARTER_CUSTOMER_GROUP] OFF

GO

IF OBJECT_ID('REPORT_QUARTER_CUSTOMER_CONSUMER')IS NOT NULL
	DROP PROC REPORT_QUARTER_CUSTOMER_CONSUMER

GO 

CREATE PROC dbo.REPORT_QUARTER_CUSTOMER_CONSUMER @DATE DATETIME='2020-01-01'
AS
DECLARE @D1 DATETIME, @D2 DATETIME;
SET @DATE=DATEADD(D, 0, DATEDIFF(D, 0, @DATE));
SET @D1=@DATE;
SET @D2=DATEADD(S, -1, DATEADD(M, 3, @DATE));
 
SELECT 
	RowNumber =	ROW_NUMBER() OVER (PARTITION BY inv.CUSTOMER_ID ORDER BY inv.INVOICE_DATE DESC),
	inv.CUSTOMER_ID, 
	inv.CUSTOMER_CONNECTION_TYPE_ID
	INTO #TMP_INVOICES 
FROM dbo.TBL_INVOICE inv
WHERE inv.INVOICE_DATE BETWEEN @D1 AND @D2 AND inv.IS_SERVICE_BILL=0 AND inv.INVOICE_STATUS NOT IN(3)
ORDER BY inv.CUSTOMER_ID
 
SELECT   
	MAX(RowNumber) AS RowNumber, 
	CUSTOMER_ID
	INTO #TMP_LASTINVOICE_CONNECTIONTYPE
FROM #TMP_INVOICES 
GROUP BY CUSTOMER_ID   

 
SELECT 
	ORDERING,
	CUSTOMER_CONNECTION_TYPE_ID = v.tuple,
	CUSTOMER_CONNECTION_TYPE_ID_ORDERING = v.Id
	INTO #TMP_CUSTOMER_CONNECTION_TYPE
FROM dbo.TLKP_REPORT_QUARTER_CUSTOMER_CONNECTION_TYPE rd
OUTER APPLY
(
	SELECT * FROM dbo.SPLIT_STRING(rd.CUSTOMER_CONNECTION_TYPE_IDS,',')
) AS v	 
WHERE  @D1 BETWEEN rd.EFFECTIVE_FROM AND rd.EFFECTIVE_TO

SELECT 
	CUSTOMER_GROUP_ID = rg.ORDERING, 
	CUSTOMER_GROUP_NAME =cg.CUSTOMER_GROUP_NAME,
	CUSTOMER_CONNECTION_TYPE_ID =  rd.ORDERING, 
	CUSTOMER_CONNECTION_TYPE_NAME = ct.CUSTOMER_CONNECTION_TYPE_NAME ,
	NUMBER = COUNT(lct.CUSTOMER_ID),
	CUSTOMER_CONNECTION_TYPE_ID_ORDERING  = rd.CUSTOMER_CONNECTION_TYPE_ID_ORDERING,
	SHOW_ZERO = rg.SHOW_ZERO
	INTO #TMP_RESULT	
FROM dbo.TLKP_CUSTOMER_GROUP cg 
INNER JOIN  dbo.TLKP_CUSTOMER_CONNECTION_TYPE ct ON cg.CUSTOMER_GROUP_ID = ct.NONLICENSE_CUSTOMER_GROUP_ID
LEFT JOIN dbo.TLKP_REPORT_QUARTER_CUSTOMER_GROUP rg ON rg.CUSTOMER_GROUP_ID = cg.CUSTOMER_GROUP_ID AND @D1 BETWEEN rg.EFFECTIVE_FROM AND rg.EFFECTIVE_TO
INNER JOIN #TMP_CUSTOMER_CONNECTION_TYPE rd ON ct.CUSTOMER_CONNECTION_TYPE_ID = rd.CUSTOMER_CONNECTION_TYPE_ID
LEFT JOIN #TMP_INVOICES inv ON inv.CUSTOMER_CONNECTION_TYPE_ID = ct.CUSTOMER_CONNECTION_TYPE_ID
LEFT JOIN #TMP_LASTINVOICE_CONNECTIONTYPE lct ON  lct.CUSTOMER_ID = inv.CUSTOMER_ID AND lct.RowNumber = inv.RowNumber
GROUP BY 	
	cg.CUSTOMER_GROUP_NAME,
	rd.CUSTOMER_CONNECTION_TYPE_ID_ORDERING,	
	rg.SHOW_ZERO,
	rg.ORDERING,
	rd.ORDERING,
	ct.CUSTOMER_CONNECTION_TYPE_NAME
ORDER BY rg.ORDERING, rd.ORDERING,rd.CUSTOMER_CONNECTION_TYPE_ID_ORDERING

SELECT 
	r.CUSTOMER_GROUP_ID, 
	r.CUSTOMER_GROUP_NAME,
	r.CUSTOMER_CONNECTION_TYPE_ID,
	r.CUSTOMER_CONNECTION_TYPE_NAME,
	DAILY_SUPPLY_HOUR = 24,
	r.NUMBER
FROM #TMP_RESULT r
WHERE  ( SHOW_ZERO = 1 OR NUMBER >0)  
 
GO

IF OBJECT_ID('REPORT_BANK_TRAN')IS NOT NULL
	DROP PROC REPORT_BANK_TRAN
GO

CREATE PROC dbo.REPORT_BANK_TRAN
	@D1 DATETIME='2021-02-01',
	@D2 DATETIME='2021-02-28',
	@CURRENCY_ID INT=0
AS
/*
@2021-Mar-19 by Phuong Sovathvong
- Add ORDER BY x.CURRENCY_ID,
		CONVERT(NVARCHAR,DATE,112)
*/
SET @D1 = DATEADD(D,0,DATEDIFF(D,0,@D1));
SET @D2 = DATEADD(S,-1,DATEADD(D,1,DATEDIFF(D,0,@D2)));

SELECT 
	CURRENCY_ID, 
	AMOUNT= SUM(AMOUNT)
INTO #TMP 
FROM(
	SELECT	
		CURRENCY_ID,
		AMOUNT= PAY_AMOUNT
	FROM TBL_PAYMENT 
	WHERE PAY_DATE < @D1 AND BANK_PAYMENT_DETAIL_ID = 0  
	UNION ALL
	SELECT
		CURRENCY_ID,  
		AMOUNT
	FROM TBL_CUS_DEPOSIT
	WHERE DEPOSIT_DATE < @D1 
	UNION ALL
	SELECT 
		CURRENCY_ID,
		BUY_AMOUNT
	FROM TBL_CUSTOMER_BUY
	WHERE CREATE_ON < @D1 
	 
	UNION ALL
	
	SELECT 
	BALANCE_CURRENCY_ID,
	AMOUNT=-CASE WHEN MULTIPLIER_METHOD =1 THEN AMOUNT*EXCHANGE_RATE ELSE AMOUNT/EXCHANGE_RATE END
	FROM TBL_BANK_TRAN
	WHERE TRAN_DATE < @D1 AND IS_ACTIVE=1
) x 
GROUP BY CURRENCY_ID;  

SELECT DATE = CONVERT(DATETIME,CONVERT(NVARCHAR,DATE,112)),	
	   x.CURRENCY_ID,
	   c.CURRENCY_NAME,
	   c.CURRENCY_SING,
	   CASH_RECEIVED = SUM(CASH),
	   BANK_DEPOSIT = SUM(BANK),
	   BEGIN_BALANCE = ISNULL((SELECT TOP 1 AMOUNT FROM #TMP WHERE CURRENCY_ID = x.CURRENCY_ID),0)
FROM(
	SELECT	CURRENCY_ID,
			DATE = PAY_DATE,
			CASH= PAY_AMOUNT,
			BANK =0
	FROM TBL_PAYMENT
	WHERE PAY_DATE BETWEEN @D1 AND @D2 AND BANK_PAYMENT_DETAIL_ID = 0  

	UNION ALL
	SELECT  CURRENCY_ID, 
			DEPOSIT_DATE,
			AMOUNT,
			0
	FROM TBL_CUS_DEPOSIT 
	WHERE DEPOSIT_DATE BETWEEN @D1 AND @D2
	 
	UNION ALL
	SELECT  CURRENCY_ID,
			CREATE_ON,
			BUY_AMOUNT,
			0
	FROM TBL_CUSTOMER_BUY
	WHERE CREATE_ON BETWEEN @D1 AND @D2 
 
	UNION ALL
	SELECT  BALANCE_CURRENCY_ID,
			TRAN_DATE,
			CASH = 0, 
			BANK = CASE WHEN MULTIPLIER_METHOD =1 THEN AMOUNT*EXCHANGE_RATE ELSE AMOUNT/EXCHANGE_RATE END
	FROM TBL_BANK_TRAN
	WHERE TRAN_DATE BETWEEN @D1 AND @D2 AND IS_ACTIVE=1
) x 
INNER JOIN TLKP_CURRENCY c ON x.CURRENCY_ID = c.CURRENCY_ID 
WHERE (@CURRENCY_ID=0 OR c.CURRENCY_ID=@CURRENCY_ID)
GROUP BY CONVERT(NVARCHAR,DATE,112),
		x.CURRENCY_ID,
		c.CURRENCY_NAME,c.CURRENCY_SING
ORDER BY x.CURRENCY_ID,
		CONVERT(NVARCHAR,DATE,112)
--END REPORT_BANK_TRAN
GO


";
        }
    }
}
