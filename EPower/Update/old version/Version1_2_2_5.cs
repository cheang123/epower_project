﻿namespace EPower.Update
{
	class Version1_2_2_5 : Version
	{
		/*
         
         @2020-January-02 By Hor Dara
         1.Update Quarter Report
		 .REPORT_QUARTER_CUSTOMER_CONSUMER
         2. Add 2020 Public holiday
		 3. Fix Subject text on dialog send mail
		 4. Modify DialogCompany for 2020 ref tariff
		 @13-Jan-2020 Kheang Kimkhorn
         1. Update bank service URL
        */
		protected override string GetBatchCommand()
		{
			return @"
GO			
IF EXISTS(SELECT * FROM TBL_UTILITY WHERE UTILITY_ID=47)			
UPDATE TBL_UTILITY SET UTILITY_VALUE=N'http://esvc.bankgateway.net/EPowerService.svc' WHERE UTILITY_ID=47
GO
-- CREATE REPORT_QUARTER_CUSTOMER_CONSUMER
IF OBJECT_ID('REPORT_QUARTER_CUSTOMER_CONSUMER') IS NOT NULL
DROP PROCEDURE REPORT_QUARTER_CUSTOMER_CONSUMER
GO
CREATE PROC [dbo].[REPORT_QUARTER_CUSTOMER_CONSUMER]
	@DATE DATETIME='2019-01-01'
/*
@2016-07-11 By Morm Raksmey
- Customer Licensee not include in Consumer 
@2017-07-11 By Morm Raksmey
- Total Customer
@2018-01-10 By Morm Raksmey
- add case : WHEN OLD_STATUS_ID=3 AND NEW_STATUS_ID=4 THEN -1 -- ផ្តាច់ => ឈប់ប្រើ 
@2019-03-29 By Phuong Sovathvong
- Add Join TLKP_CUSTOMER_CONNECTION_TYPE, TLKP_CUSTOMER_GROUP
@2019-12-03 By Hor Dara
Show all Customer Connection Type even it has no customer
*/
AS
DECLARE @D1 DATETIME,
		@D2 DATETIME

SET @DATE = DATEADD(D,0,DATEDIFF(D,0,@DATE));
SET @D1= @DATE; 
SET @D2= DATEADD(S,-1,DATEADD(M,3,@DATE));

SELECT *
INTO #TMP
FROM (
SELECT	cg.CUSTOMER_GROUP_ID,
		cg.CUSTOMER_GROUP_NAME,
		t.CUSTOMER_CONNECTION_TYPE_ID,
		t.CUSTOMER_CONNECTION_TYPE_NAME,
		DAILY_SUPPLY_HOUR=24,
		NUMBER = ISNULL(SUM(CASE WHEN OLD_STATUS_ID=1 AND NEW_STATUS_ID=2 THEN 1 -- អតិថិជនថ្មី
								 WHEN OLD_STATUS_ID=2 AND NEW_STATUS_ID=4 THEN -1 -- កំពុងប្រើប្រាស => ឈប់ប្រើ
								 WHEN OLD_STATUS_ID=4 AND NEW_STATUS_ID=2 THEN 1 -- ឈប់ប្រើប្រាស់ => កំពុងប្រើប្រាស់
								 WHEN OLD_STATUS_ID=3 AND NEW_STATUS_ID=4 THEN -1 -- ផ្តាច់ => ឈប់ប្រើ 
								 ELSE 0 END),0),
		--NUMBER = ISNULL(SUM(CASE WHEN NEW_STATUS_ID=2 THEN 1 WHEN OLD_STATUS_ID=2 THEN -1 ELSE 0 END),0),
		REMARK = ''
FROM TBL_CUSTOMER c
INNER JOIN TLKP_CUSTOMER_CONNECTION_TYPE t ON t.CUSTOMER_CONNECTION_TYPE_ID = c.CUSTOMER_CONNECTION_TYPE_ID
INNER JOIN TLKP_CUSTOMER_GROUP cg ON cg.CUSTOMER_GROUP_ID=t.NONLICENSE_CUSTOMER_GROUP_ID
INNER JOIN TBL_CUS_STATUS_CHANGE g ON g.CUSTOMER_ID=c.CUSTOMER_ID  
WHERE g.CHNAGE_DATE  <= @D2
	AND (c.USAGE_CUSTOMER_ID=0 OR c.USAGE_CUSTOMER_ID=c.CUSTOMER_ID)--only not 3-phase
	AND (c.IS_REACTIVE =0 ) -- not reactive customer
	AND c.CUSTOMER_ID NOT IN(SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE=1 AND (START_DATE <= @D2)
	AND (IS_INUSED=1 OR END_DATE > @D1 ))
GROUP BY cg.CUSTOMER_GROUP_ID,
		cg.CUSTOMER_GROUP_NAME,	
		t.CUSTOMER_CONNECTION_TYPE_ID,
		t.CUSTOMER_CONNECTION_TYPE_NAME
UNION ALL
SELECT cg.CUSTOMER_GROUP_ID,
		cg.CUSTOMER_GROUP_NAME,
		t.CUSTOMER_CONNECTION_TYPE_ID,
		t.CUSTOMER_CONNECTION_TYPE_NAME,
		DAILY_SUPPLY_HOUR=24,
		NUMBER = 0,
		REMARK = ''
FROM TLKP_CUSTOMER_CONNECTION_TYPE t
LEFT JOIN TLKP_CUSTOMER_GROUP cg ON cg.CUSTOMER_GROUP_ID=t.NONLICENSE_CUSTOMER_GROUP_ID
WHERE
	t.CUSTOMER_CONNECTION_TYPE_ID NOT IN(SELECT CUSTOMER_CONNECTION_TYPE_ID FROM TBL_CUSTOMER WHERE STATUS_ID NOT IN(1,4,5))
	AND t.CUSTOMER_CONNECTION_TYPE_ID NOT IN(11,12,13)
GROUP BY cg.CUSTOMER_GROUP_ID,
		cg.CUSTOMER_GROUP_NAME,	
		t.CUSTOMER_CONNECTION_TYPE_ID,
		t.CUSTOMER_CONNECTION_TYPE_NAME
) x
SELECT CUSTOMER_GROUP_ID= CASE WHEN CUSTOMER_GROUP_ID=4 THEN 1
                      WHEN CUSTOMER_GROUP_ID=2 THEN 2       
                      WHEN CUSTOMER_GROUP_ID=1 THEN 3
                      WHEN CUSTOMER_GROUP_ID=5 THEN 4
                    END,
		CUSTOMER_GROUP_NAME,
		CUSTOMER_CONNECTION_TYPE_ID = CASE WHEN CUSTOMER_CONNECTION_TYPE_ID=1 THEN 1
                      WHEN CUSTOMER_CONNECTION_TYPE_ID IN(4) THEN 2
					  WHEN CUSTOMER_CONNECTION_TYPE_ID IN(7) THEN 3
					  WHEN CUSTOMER_CONNECTION_TYPE_ID IN(2) THEN 4
					  WHEN CUSTOMER_CONNECTION_TYPE_ID IN(3) THEN 5
					  WHEN CUSTOMER_CONNECTION_TYPE_ID IN(14) THEN 6          
                      WHEN CUSTOMER_CONNECTION_TYPE_ID IN(8) THEN 7
                      WHEN CUSTOMER_CONNECTION_TYPE_ID IN(10) THEN 8
                      WHEN CUSTOMER_CONNECTION_TYPE_ID IN(5) THEN 9
					  WHEN CUSTOMER_CONNECTION_TYPE_ID IN(6) THEN 10
					  WHEN CUSTOMER_CONNECTION_TYPE_ID IN(15) THEN 11
					  WHEN CUSTOMER_CONNECTION_TYPE_ID IN(16) THEN 12
                      WHEN CUSTOMER_CONNECTION_TYPE_ID IN(9) THEN 13
                    END,
		CUSTOMER_CONNECTION_TYPE_NAME,
		DAILY_SUPPLY_HOUR,
		NUMBER
FROM #TMP
--WHERE NUMBER>0
-- END REPORT_QUARTER_CUSTOMER_CONSUMER
GO
IF NOT EXISTS(SELECT * FROM TBL_HOLIDAY WHERE YEAR(HOLIDAY_DATE)=2020)
BEGIN
	INSERT INTO TBL_HOLIDAY 
	SELECT N'ទិវា​ចូល​ឆ្នាំ​សកល','2020-01-01 00:00:00','',1
	UNION ALL SELECT N'ទិវា​ជ័យជម្នះ​លើ​របប​ប្រល័យ​ពូជ​សាសន៍','2020-01-07 00:00:00','',1
	UNION ALL SELECT N'ទិវា​នារី​អន្តរជាតិ','2020-03-08 00:00:00','',1
	UNION ALL SELECT N'ពិធី​បុណ្យ​ចូល​ឆ្នាំ​ថ្មី ប្រពៃណី​ជាតិ ឆ្នាំ កុរ ឯកស័ក','2020-04-13 00:00:00','',1
	UNION ALL SELECT N'ពិធី​បុណ្យ​ចូល​ឆ្នាំ​ថ្មី ប្រពៃណី​ជាតិ ឆ្នាំ កុរ ឯកស័ក','2020-04-14 00:00:00','',1
	UNION ALL SELECT N'ពិធី​បុណ្យ​ចូល​ឆ្នាំ​ថ្មី ប្រពៃណី​ជាតិ ឆ្នាំ កុរ ឯកស័ក','2020-04-15 00:00:00','',1
	UNION ALL SELECT N'ពិធី​បុណ្យ​ចូល​ឆ្នាំ​ថ្មី ប្រពៃណី​ជាតិ ឆ្នាំ កុរ ឯកស័ក','2020-04-16 00:00:00','',1
	UNION ALL SELECT N'ទិវា​ពលកម្ម​អន្តរជាតិ','2020-05-01 00:00:00','',1
	UNION ALL SELECT N'ពិធី​បុណ្យ​វិសាខ​បូជា','2020-05-06 00:00:00','',1
	UNION ALL SELECT N'ព្រះ​រាជ​ពិធី​ច្រត់​ព្រះ​នង្គ័ល','2020-05-10 00:00:00','',1
	UNION ALL SELECT N'ព្រះ​រាជ​ពិធី​បុណ្យ​ចម្រើន​ព្រះ​ជន្ម ព្រះ​ករុណា ព្រះ​បាទ​សម្តេច ព្រះ​បរម​នាថ នរោត្តម សីហមុនី ព្រះមហាក្សត្រ នៃព្រះរាជាណាចក្រកម្ពុជា','2020-05-14 00:00:00','',1
	UNION ALL SELECT N'ព្រះ​រាជ​ពិធី​បុណ្យ​ចម្រើន​ព្រះ​ជន្ម សម្តេច​ព្រះ​មហាក្សត្រី នរោត្តម មុនិនាថ សីហនុ ព្រះវររាជមាតាជាតិខ្មែរ','2020-06-18 00:00:00','',1
	UNION ALL SELECT N'ពិធី​បុណ្យ​ភ្ជុំ​បិណ្ឌ','2020-09-16 00:00:00','',1
	UNION ALL SELECT N'ពិធី​បុណ្យ​ភ្ជុំ​បិណ្ឌ','2020-09-17 00:00:00','',1
	UNION ALL SELECT N'ពិធី​បុណ្យ​ភ្ជុំ​បិណ្ឌ','2020-09-18 00:00:00','',1
	UNION ALL SELECT N'ទិវា​ប្រកាស​រដ្ឋ​ធម្មនុញ្ញ','2020-09-24 00:00:00','',1
	UNION ALL SELECT N'ទិវា​ប្រារព្ធ​ពិធី​គោរព​ព្រះវិញ្ញាណក្ខន្ធ ព្រះករុណា​ ព្រះបាទ​សម្តេច​ព្រះ នរោត្តម សីហនុ ព្រះមហាវីរក្សត្រ ព្រះ​វររាជ​បិតា​ ឯករាជ្យបូរណភាព​ទឹកដី និង​ឯកភាព​ជាតិ​ខ្មែរ ព្រះបរមរតនកោដ្ឋ','2020-10-15 00:00:00','',1
	UNION ALL SELECT N'ទិវារំលឹក​ខួប​នៃ​កិច្ចព្រមព្រៀង​សន្តិភាព​ទីក្រុង​ប៉ារីស','2020-10-23 00:00:00','',1
	UNION ALL SELECT N'ព្រះ​រាជ​ពិធី​គ្រង​ព្រះ​បរម​រាជ​សម្បត្តិ​របស់ ព្រះ​ករុណា ព្រះ​បាទ​សម្តេចព្រះ​បរមនាថ នរោត្តម សីហមុនី ព្រះ​មហាក្សត្រ​ នៃ​ព្រះរាជាណាចក្រ​កម្ពុជា','2020-10-29 00:00:00','',1
	UNION ALL SELECT N'ព្រះ​រាជ​ពិធី​បុណ្យ​អុំ​ទូក បណ្ដែត​ប្រទីប និង​សំពះ​ព្រះ​ខែ អកអំបុក','2020-10-30 00:00:00','',1
	UNION ALL SELECT N'ព្រះ​រាជ​ពិធី​បុណ្យ​អុំ​ទូក បណ្ដែត​ប្រទីប និង​សំពះ​ព្រះ​ខែ អកអំបុក','2020-10-31 00:00:00','',1
	UNION ALL SELECT N'ព្រះ​រាជ​ពិធី​បុណ្យ​អុំ​ទូក បណ្ដែត​ប្រទីប និង​សំពះ​ព្រះ​ខែ អកអំបុក','2020-11-01 00:00:00','',1
	UNION ALL SELECT N'ពិធី​បុណ្យ​ឯករាជ្យ​ជាតិ','2020-11-09 00:00:00','',1
END
GO
--RUN_REF_02
IF OBJECT_ID('RUN_REF_02') IS NOT NULL
DROP PROCEDURE RUN_REF_02
GO
CREATE PROC [dbo].[RUN_REF_02]
	@DATA_MONTH DATETIME='2017-03-01',
	@BILLING_CYCLE_ID INT=0,
	@AREA_ID INT=0
AS
IF @DATA_MONTH BETWEEN '2016-03-01' AND '2017-02-01'
	EXEC RUN_REF_02_2016 @DATA_MONTH,@BILLING_CYCLE_ID,@AREA_ID

ELSE IF @DATA_MONTH BETWEEN '2017-03-01' AND '2018-12-01'
	EXEC RUN_REF_02_2017 @DATA_MONTH,@BILLING_CYCLE_ID,@AREA_ID

ELSE IF @DATA_MONTH BETWEEN '2019-01-01' AND '2020-01-01'
	EXEC RUN_REF_02_2019 @DATA_MONTH,@BILLING_CYCLE_ID,@AREA_ID
--END OF RUN_REF_02
GO
--RUN_REF_O3A
IF OBJECT_ID('RUN_REF_03A') IS NOT NULL
DROP PROCEDURE RUN_REF_03A
GO

CREATE PROC [dbo].[RUN_REF_03A]
	@DATA_MONTH DATETIME='2017-03-01', 
	@BILLING_CYCLE_ID INT=0,
	@AREA_ID INT=0
AS
IF @DATA_MONTH BETWEEN '2016-03-01' AND '2017-02-01'
	EXEC RUN_REF_03A_2016 @DATA_MONTH,@BILLING_CYCLE_ID,@AREA_ID

ELSE IF @DATA_MONTH BETWEEN '2017-03-01' AND '2018-12-01'
	EXEC RUN_REF_03A_2017 @DATA_MONTH,@BILLING_CYCLE_ID,@AREA_ID

ELSE IF @DATA_MONTH BETWEEN '2019-01-01' AND '2020-01-01'
	EXEC RUN_REF_03A_2019 @DATA_MONTH,@BILLING_CYCLE_ID,@AREA_ID
--END OF RUN_REF_03A
GO
--RUN_REF_03B
IF OBJECT_ID('RUN_REF_03B') IS NOT NULL
DROP PROCEDURE RUN_REF_03B
GO
CREATE PROC [dbo].[RUN_REF_03B]
	@DATA_MONTH DATETIME='2017-03-01',
	@BILLING_CYCLE_ID INT=0,
	@AREA_ID INT=0
AS
IF @DATA_MONTH BETWEEN '2016-03-01' AND '2017-02-01'
	EXEC RUN_REF_03B_2016 @DATA_MONTH,@BILLING_CYCLE_ID,@AREA_ID

ELSE IF @DATA_MONTH BETWEEN '2017-03-01' AND '2018-12-01'
	EXEC RUN_REF_03B_2017 @DATA_MONTH,@BILLING_CYCLE_ID,@AREA_ID
ELSE IF @DATA_MONTH BETWEEN '2019-01-01' AND '2020-01-01'
	EXEC RUN_REF_03B_2019 @DATA_MONTH,@BILLING_CYCLE_ID,@AREA_ID

-- END RUN_REF_03B
GO
--REPORT_REF_INVOICE
IF OBJECT_ID('REPORT_REF_INVOICE') IS NOT NULL
DROP PROCEDURE REPORT_REF_INVOICE
GO
CREATE PROC [dbo].[REPORT_REF_INVOICE]
	@DATA_MONTH DATETIME='2020-01-01',
	@CUSTOMER_GROUP_ID INT =0,
    @CUSTOMER_CONNECTION_TYPE_ID INT=0,
	@CURRENCY_ID INT=0,
	@V1 DECIMAL(18,4)=0,
	@V2 DECIMAL(18,4)=1000000,
	@BILLING_CYCLE_ID INT=0,
	@AREA_ID INT=0
AS
/*
@ 2016-05-25 Morm Raksmey
	Add Column Billing Cycle Id and Billing Cycle Name
@ 2017-03-10 Sieng Sothera
	Trim meter code without '0'
@ 2017-04-19 Sieng Sotheara
	Update REF Invoice 2016 & 2017
@ 2018-09-19 Pov Lyhorng
	fix Organization customer is non subsidy type
@ 2019-01-16 Phuong Sovathvong
	Update REF Invoice 2019
@ 2019-12-12 HOR DARA
	Update REF Invoice 2020
*/
CREATE TABLE #RESULT
(
	TYPE_ID INT
	,TYPE_NAME NVARCHAR(255)
	,TYPE_OF_SALE_ID INT
	,TYPE_OF_SALE_NAME NVARCHAR(255)
	,HISTORY_ID BIGINT
	,CUSTOMER_GROUP_ID INT
	,CUSTOMER_GROUP_NAME NVARCHAR(200)
	,INVOICE_ID BIGINT
	,DATA_MONTH DATETIME
	,CUSTOMER_ID INT
	,CUSTOMER_CODE NVARCHAR(50)
	,FIRST_NAME_KH NVARCHAR(50)
	,LAST_NAME_KH NVARCHAR(50)
	,AREA_ID INT
	,AREA_CODE NVARCHAR(50)
	,BOX_ID INT 
	,BOX_CODE NVARCHAR(50)
	,CUSTOMER_CONNECTION_TYPE_ID INT
	,CUSTOMER_CONNECTION_TYPE_NAME NVARCHAR(200)
	,AMPARE_ID INT
	,AMPARE_NAME NVARCHAR(50)
	,PRICE_ID INT 
	,METER_ID INT
	,METER_CODE NVARCHAR(50)
	,MULTIPLIER INT
	,CURRENCY_SIGN NVARCHAR(50)
	,CURRENCY_NAME NVARCHAR(50)
	,CURRENCY_ID INT 
	,PRICE DECIMAL(18,4)
	,BASED_PRICE DECIMAL(18,4)
	,START_USAGE DECIMAL(18,4)
	,END_USAGE DECIMAL(18,4)
	,TOTAL_USAGE DECIMAL(18,4)
	,TOTAL_AMOUNT DECIMAL(18,4)
	,IS_POST_PAID BIT
	,IS_AGRICULTURE BIT
	,SHIFT_ID INT
	,IS_ACTIVE BIT
	,BILLING_CYCLE_ID INT
	,BILLING_CYCLE_NAME NVARCHAR(50)
	,ROW_DATE DATETIME
	,CUSTOMER_TYPE_ID INT
	,CUSTOMER_TYPE_NAME NVARCHAR(50)
	,LICENSE_NUMBER NVARCHAR(50)
)
/*REF 2016*/
IF @DATA_MONTH BETWEEN '2016-03-01' AND '2017-02-01'
BEGIN
	INSERT INTO #RESULT
	/*លក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ*/
	SELECT TYPE_ID=3
		  ,TYPE_NAME=N'លក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ'
		  ,TYPE_OF_SALE_ID=4
		  ,TYPE_OF_SALE_NAME=N'លក់លើ MV នាឡិកាស្ទង់ MV'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_GROUP_ID=3
		AND i.CUSTOMER_CONNECTION_TYPE_ID=4
	UNION ALL
	SELECT TYPE_ID=3
		  ,TYPE_NAME=N'លក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ'
		  ,TYPE_OF_SALE_ID=4
		  ,TYPE_OF_SALE_NAME=N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_GROUP_ID=3
		AND i.CUSTOMER_CONNECTION_TYPE_ID=3
	UNION ALL
	SELECT TYPE_ID=3
		  ,TYPE_NAME=N'លក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ'
		  ,TYPE_OF_SALE_ID=4
		  ,TYPE_OF_SALE_NAME=N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_GROUP_ID=3
		AND i.CUSTOMER_CONNECTION_TYPE_ID=2
	UNION ALL
	/*លក់ឲ្យអតិថិជន MV​(ឧស្សាហកម្ម ពាណិជ្ជកម្ម)*/
	SELECT TYPE_ID=2
		  ,TYPE_NAME=N'លក់ឲ្យអតិថិជន MV​(ឧស្សាហកម្ម ពាណិជ្ជកម្ម)'
		  ,TYPE_OF_SALE_ID=4
		  ,TYPE_OF_SALE_NAME=N'លក់លើ MV នាឡិកាស្ទង់ MV'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_GROUP_ID=2
		AND i.CUSTOMER_CONNECTION_TYPE_ID=4
	UNION ALL
	SELECT TYPE_ID=2
		  ,TYPE_NAME=N'លក់ឲ្យអតិថិជន MV​(ឧស្សាហកម្ម ពាណិជ្ជកម្ម)'
		  ,TYPE_OF_SALE_ID=4
		  ,TYPE_OF_SALE_NAME=N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_GROUP_ID=2
		AND i.CUSTOMER_CONNECTION_TYPE_ID=3
	UNION ALL
	SELECT TYPE_ID=2
		  ,TYPE_NAME=N'លក់ឲ្យអតិថិជន MV​(ឧស្សាហកម្ម ពាណិជ្ជកម្ម)'
		  ,TYPE_OF_SALE_ID=4
		  ,TYPE_OF_SALE_NAME=N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_GROUP_ID=2
		AND i.CUSTOMER_CONNECTION_TYPE_ID=2
	UNION ALL
	/*លក់លើតង់ស្យុងទាប*/
	SELECT TYPE_ID=1
		  ,TYPE_NAME=N'លក់លើតង់ស្យុងទាប'
		  ,TYPE_OF_SALE_ID=4
		  ,TYPE_OF_SALE_NAME=N'អតិថិជនធម្មតា'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_GROUP_ID=1
		AND i.CUSTOMER_CONNECTION_TYPE_ID=1
END 
/*REF 2017*/
ELSE IF @DATA_MONTH BETWEEN '2017-03-01' AND '2018-12-01'
BEGIN
	INSERT INTO #RESULT
	/*ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ*/
	SELECT TYPE_ID=3
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ'
		  ,TYPE_OF_SALE_ID=4
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_GROUP_ID=3
		AND i.CUSTOMER_CONNECTION_TYPE_ID=11
	UNION ALL
	SELECT TYPE_ID=3
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ'
		  ,TYPE_OF_SALE_ID=3
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_GROUP_ID=3
		AND i.CUSTOMER_CONNECTION_TYPE_ID=12
	UNION ALL
	SELECT TYPE_ID=3
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ'
		  ,TYPE_OF_SALE_ID=2
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_GROUP_ID=3
		AND i.CUSTOMER_CONNECTION_TYPE_ID=13
	UNION ALL
	/*ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)*/
	SELECT TYPE_ID=2
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)'
		  ,TYPE_OF_SALE_ID=4
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_GROUP_ID IN(1,2)
		AND i.CUSTOMER_CONNECTION_TYPE_ID IN(4,7,8,10)
	UNION ALL
	SELECT TYPE_ID=2
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)'
		  ,TYPE_OF_SALE_ID=3
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_GROUP_ID IN(1,2)
		AND i.CUSTOMER_CONNECTION_TYPE_ID IN(3,6)
	UNION ALL
	SELECT TYPE_ID=2
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)'
		  ,TYPE_OF_SALE_ID=2
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_GROUP_ID IN(1,2)
		AND i.CUSTOMER_CONNECTION_TYPE_ID IN(2,5)
	UNION ALL
	SELECT TYPE_ID=2
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)'
		  ,TYPE_OF_SALE_ID=1
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_GROUP_ID IN(1,2,5)
		AND i.CUSTOMER_CONNECTION_TYPE_ID IN(9,14,15,16)
		AND i.TOTAL_USAGE>=2001
	UNION ALL
	/*ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)*/
	SELECT TYPE_ID=1
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)'
		  ,TYPE_OF_SALE_ID=-1
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើក្រោម 2001 kWh/ខែ '
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_GROUP_ID IN(1,2,5)
		AND i.CUSTOMER_CONNECTION_TYPE_ID IN(9,14,15,16)
		AND i.TOTAL_USAGE<2001

	UNION ALL
	/* ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន */
	SELECT TYPE_ID=0
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន'
		  ,TYPE_OF_SALE_ID=-3
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើតិចជាង  10 kWh/ខែ'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_GROUP_ID IN(4)
		AND i.CUSTOMER_CONNECTION_TYPE_ID IN(1)
		AND i.TOTAL_USAGE<=10
	UNION ALL
	SELECT TYPE_ID=0
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន'
		  ,TYPE_OF_SALE_ID=-4
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_GROUP_ID IN(4)
		AND i.CUSTOMER_CONNECTION_TYPE_ID IN(1)
		AND i.TOTAL_USAGE BETWEEN 11 AND 50
	UNION ALL
	SELECT TYPE_ID=0
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន'
		  ,TYPE_OF_SALE_ID=-5
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើធំជាង 51 kWh/ខែ'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_GROUP_ID IN(4)
		AND i.CUSTOMER_CONNECTION_TYPE_ID IN(1)
		AND i.TOTAL_USAGE >=51
END
/*REF 2019*/
ELSE IF @DATA_MONTH BETWEEN '2019-01-01' AND '2020-01-01'
BEGIN
	INSERT INTO #RESULT
	SELECT TYPE_ID=4
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ'
		  ,TYPE_OF_SALE_ID=5
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID=11

	UNION ALL
	SELECT TYPE_ID=4
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ'
		  ,TYPE_OF_SALE_ID=4
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID=12

	UNION ALL
	SELECT TYPE_ID=4
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ'
		  ,TYPE_OF_SALE_ID=3
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID=13

	UNION ALL
	SELECT TYPE_ID=3
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)'
		  ,TYPE_OF_SALE_ID=6
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN(4)
	
	UNION ALL
	SELECT TYPE_ID=3
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)'
		  ,TYPE_OF_SALE_ID=5
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវាលា'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN(7)
	
	UNION ALL
	SELECT TYPE_ID=3
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)'
		  ,TYPE_OF_SALE_ID=4
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN(3)
	
	UNION ALL
	SELECT TYPE_ID=3
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)'
		  ,TYPE_OF_SALE_ID=3
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN(2)
	
	UNION ALL
	SELECT TYPE_ID=3
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)'
		  ,TYPE_OF_SALE_ID=2
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN(14)
		AND i.TOTAL_USAGE>=2001
	
	UNION ALL
	SELECT TYPE_ID=2
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)'
		  ,TYPE_OF_SALE_ID=6
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN(8)

	UNION ALL
	SELECT TYPE_ID=2
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)'
		  ,TYPE_OF_SALE_ID=5
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN(10)
	
	UNION ALL
	SELECT TYPE_ID=2
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)'
		  ,TYPE_OF_SALE_ID=4
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN(6)
	
	UNION ALL
	SELECT TYPE_ID=2
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)'
		  ,TYPE_OF_SALE_ID=3
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN(5)
	
	UNION ALL
	SELECT TYPE_ID=2
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)'
		  ,TYPE_OF_SALE_ID=2
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN(15,16)
		AND i.TOTAL_USAGE>=2001
	
	UNION ALL
	SELECT TYPE_ID=2
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)'
		  ,TYPE_OF_SALE_ID=1
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់ឲ្យអតិថិជន សាលារៀន មន្ទីរពេទ្យ មណ្ឌលសុខភាព នៅតំបន់ជនបទ'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN(9)
	
	UNION ALL
	SELECT TYPE_ID=1
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)'
		  ,TYPE_OF_SALE_ID=-1
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើក្រោម 2001 kWh/ខែ'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN(14,15,16)
		AND i.TOTAL_USAGE<2001

	UNION ALL
	SELECT TYPE_ID=1
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន'
		  ,TYPE_OF_SALE_ID=-3
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើតិចជាង  10 kWh/ខែ'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN(1)
		AND i.TOTAL_USAGE<=10
	
	UNION ALL
	SELECT TYPE_ID=1
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន'
		  ,TYPE_OF_SALE_ID=-4
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN(1)
		AND i.TOTAL_USAGE BETWEEN 11 AND 50
	
	UNION ALL
	SELECT TYPE_ID=1
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន'
		  ,TYPE_OF_SALE_ID=-5
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 51 ដល់ 200 kWh/ខែ'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN(1)
		AND i.TOTAL_USAGE BETWEEN 51 AND 200
	
	UNION ALL
	SELECT TYPE_ID=1
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន'
		  ,TYPE_OF_SALE_ID=-6
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើធំជាង 201 kWh/ខែ'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN(1)
		AND i.TOTAL_USAGE >=201
END
/*REF 2020*/
ELSE IF YEAR(@DATA_MONTH)=2020
BEGIN
	INSERT INTO #RESULT
	SELECT TYPE_ID=4
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ'
		  ,TYPE_OF_SALE_ID=5
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID=11

	UNION ALL
	SELECT TYPE_ID=4
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ'
		  ,TYPE_OF_SALE_ID=4
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID=12

	UNION ALL
	SELECT TYPE_ID=4
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ'
		  ,TYPE_OF_SALE_ID=3
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID=13

	UNION ALL
	SELECT TYPE_ID=3
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)'
		  ,TYPE_OF_SALE_ID=5
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN(4)
	
	UNION ALL
	SELECT TYPE_ID=3
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)'
		  ,TYPE_OF_SALE_ID=4
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវាលា'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN(7)
	
	UNION ALL
	SELECT TYPE_ID=3
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)'
		  ,TYPE_OF_SALE_ID=3
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN(3)
	
	UNION ALL
	SELECT TYPE_ID=3
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)'
		  ,TYPE_OF_SALE_ID=2
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN(2)
	
	UNION ALL
	SELECT TYPE_ID=3
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)'
		  ,TYPE_OF_SALE_ID=1
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN(14)
		AND i.TOTAL_USAGE>=2001
	
	UNION ALL
	SELECT TYPE_ID=2
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)'
		  ,TYPE_OF_SALE_ID=5
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN(8)

	UNION ALL
	SELECT TYPE_ID=2
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)'
		  ,TYPE_OF_SALE_ID=4
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN(10)
	
	UNION ALL
	SELECT TYPE_ID=2
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)'
		  ,TYPE_OF_SALE_ID=3
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN(6)
	
	UNION ALL
	SELECT TYPE_ID=2
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)'
		  ,TYPE_OF_SALE_ID=2
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN(5)
	
	UNION ALL
	SELECT TYPE_ID=2
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)'
		  ,TYPE_OF_SALE_ID=1
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN(15,16)
		AND i.TOTAL_USAGE>=2001
	
	UNION ALL
	SELECT TYPE_ID=1
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)'
		  ,TYPE_OF_SALE_ID=-1
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់ឲ្យអតិថិជន សាលារៀន មន្ទីរពេទ្យ មណ្ឌលសុខភាព នៅតំបន់ជនបទ'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN(9)
	
	UNION ALL
	SELECT TYPE_ID=1
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)'
		  ,TYPE_OF_SALE_ID=-2
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើក្រោម 2001 kWh/ខែ'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN(14,15,16)
		AND i.TOTAL_USAGE<2001

	UNION ALL
	SELECT TYPE_ID=1
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន'
		  ,TYPE_OF_SALE_ID=-4
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើតិចជាង  10 kWh/ខែ'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN(1)
		AND i.TOTAL_USAGE<=10
	
	UNION ALL
	SELECT TYPE_ID=1
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន'
		  ,TYPE_OF_SALE_ID=-5
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN(1)
		AND i.TOTAL_USAGE BETWEEN 11 AND 50
	
	UNION ALL
	SELECT TYPE_ID=1
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន'
		  ,TYPE_OF_SALE_ID=-6
		  ,TYPE_OF_SALE_NAME=N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 51 ដល់ 200 kWh/ខែ'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN(1)
		AND i.TOTAL_USAGE BETWEEN 51 AND 200
	
	UNION ALL
	SELECT TYPE_ID=1
		  ,TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន'
		  ,TYPE_OF_SALE_ID=-7
		  ,TYPE_OF_SALE_NAE=N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើធំជាង 201 kWh/ខែ'
		  ,i.*
	FROM TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN(1)
		AND i.TOTAL_USAGE >=201
END
/*RESULT*/
SELECT r.*, METER_FORMAT=REPLACE(LTRIM(REPLACE(ISNULL(METER_CODE,'-'),'0',' ')),' ','0')
FROM #RESULT r
WHERE DATA_MONTH=@DATA_MONTH
	AND IS_ACTIVE=1
	AND (@CUSTOMER_GROUP_ID=0 OR CUSTOMER_GROUP_ID=@CUSTOMER_GROUP_ID)
	AND (@CURRENCY_ID=0 OR CURRENCY_ID=@CURRENCY_ID)
	AND (@CUSTOMER_CONNECTION_TYPE_ID=0 OR CUSTOMER_CONNECTION_TYPE_ID=@CUSTOMER_CONNECTION_TYPE_ID)
	AND (TOTAL_USAGE BETWEEN @V1 AND @V2)
	AND (@BILLING_CYCLE_ID=0 OR BILLING_CYCLE_ID=@BILLING_CYCLE_ID)
	AND (@AREA_ID=0 OR AREA_ID=@AREA_ID)
ORDER BY TYPE_ID DESC, TYPE_OF_SALE_ID DESC,AREA_CODE,BOX_CODE
--END REPORT_REF_INVOICE
GO
            ";
		}
	}
}
