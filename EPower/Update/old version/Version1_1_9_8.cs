﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EPower.Properties;

namespace EPower.Update
{
    
    class Version1_1_9_8 : Version
    {
        /// <summary>  
        /// Morm Raksmey
        /// 1. Add Province Tboung Khmum & Rename កំពង់លាវ ទៅ ពោធិ៍រៀង
        /// </summary>
        /// <returns></returns>
        protected override string GetBatchCommand()
        {
            return @"
IF OBJECT_ID('DATA_MAPPING_VILLAGE') IS NOT NULL
DROP TABLE DATA_MAPPING_VILLAGE
	CREATE TABLE DATA_MAPPING_VILLAGE
	(
		OLD_CODE NVARCHAR(50) NOT NULL,
		NEW_CODE NVARCHAR(50) NOT NULL,
		TYPE_ID INT NOT NULL
	)
GO

INSERT INTO DATA_MAPPING_VILLAGE
SELECT 
	OLD_CODE=DISTRICT_CODE
	,NEW_CODE=REPLACE(DISTRICT_CODE, LEFT(DISTRICT_CODE, 3),'250' + CAST(ROW_NUMBER() OVER(ORDER BY DISTRICT_CODE) AS NVARCHAR(10)))
	,TYPE_ID=2
FROM TLKP_DISTRICT
WHERE DISTRICT_CODE IN (304,309,310,311,312,316,317) 
UNION ALL 
SELECT 
	OLD_CODE=COMMUNE_CODE 
	,NEW_CODE = REPLACE(COMMUNE_CODE, LEFT(COMMUNE_CODE, 5), '2500' + CAST(ROW_NUMBER() OVER(ORDER BY COMMUNE_CODE)AS NVARCHAR(10)))
	,TYPE_ID=3
FROM TLKP_COMMUNE
WHERE DISTRICT_CODE IN (304,309,310,311,312,316,317) 
UNION ALL 
SELECT 
	OLD_CODE=v.VILLAGE_CODE
	,NEW_CODE = REPLACE(v.VILLAGE_CODE, LEFT(v.VILLAGE_CODE, 7), '250000' + CAST(ROW_NUMBER() OVER(ORDER BY v.VILLAGE_CODE) AS NVARCHAR(10)))
	,TYPE_ID=4
FROM TLKP_VILLAGE v
INNER JOIN TLKP_COMMUNE c ON c.COMMUNE_CODE = v.COMMUNE_CODE
INNER JOIN TLKP_DISTRICT d ON d.DISTRICT_CODE = c.DISTRICT_CODE 
WHERE d.DISTRICT_CODE IN (304,309,310,311,312,316,317)

IF NOT EXISTS(SELECT * FROM TLKP_PROVINCE WHERE PROVINCE_CODE = '25')
	BEGIN
		--INSERT NEW_PROVINCE
		INSERT INTO TLKP_PROVINCE
		SELECT '25', N'ត្បូងឃ្មុំ', '1'
	
		--UPDATE TLKP_DISTRICT
		UPDATE d
		SET DISTRICT_CODE=md.NEW_CODE,
			PROVINCE_CODE='25'
		FROM TLKP_DISTRICT d
		INNER JOIN DATA_MAPPING_VILLAGE md ON md.OLD_CODE=d.DISTRICT_CODE	
		WHERE md.TYPE_ID=2

		--UPDATE TLKP_COMMUNE
		UPDATE c
		SET c.COMMUNE_CODE=mc.NEW_CODE,
			c.DISTRICT_CODE=md1.NEW_CODE
		FROM TLKP_COMMUNE c
		INNER JOIN DATA_MAPPING_VILLAGE mc ON mc.OLD_CODE=c.COMMUNE_CODE 
		INNER JOIN DATA_MAPPING_VILLAGE md1 ON md1.OLD_CODE=c.DISTRICT_CODE
		WHERE mc.TYPE_ID=3 AND md1.TYPE_ID=2
	
		--UPDATE TLKP_VILLAGE
		UPDATE v
		SET v.VILLAGE_CODE=mv.NEW_CODE,
			v.COMMUNE_CODE=mc1.NEW_CODE
		FROM TLKP_VILLAGE v
		INNER JOIN DATA_MAPPING_VILLAGE mv ON mv.OLD_CODE=v.VILLAGE_CODE
		INNER JOIN DATA_MAPPING_VILLAGE mc1 ON mc1.OLD_CODE=v.COMMUNE_CODE
		WHERE mv.TYPE_ID=4 AND mc1.TYPE_ID=3

		--UPDATE VILLAGE_CODE IN TBL_CUSTOMER
		UPDATE cus 
		SET cus.VILLAGE_CODE=dmv.NEW_CODE
		FROM TBL_CUSTOMER cus
		INNER JOIN DATA_MAPPING_VILLAGE dmv ON dmv.OLD_CODE=cus.VILLAGE_CODE

		--UPDATE VILLAGE_CODE IN TBL_COMPANY
		UPDATE com
		SET com.VILLAGE_CODE=com_map.NEW_CODE
		FROM TBL_COMPANY com
		INNER JOIN DATA_MAPPING_VILLAGE com_map ON com_map.OLD_CODE=com.VILLAGE_CODE


		--UPDATE VILLAGE_CODE IN TBL_POLE
		UPDATE pole
		SET pole.VILLAGE_CODE=pole_map.NEW_CODE
		FROM TBL_POLE pole
		INNER JOIN DATA_MAPPING_VILLAGE pole_map ON pole_map.OLD_CODE=pole.VILLAGE_CODE

		--UPDATE VILLAGE_CODE IN TBL_LICENSE_VILLAGE
		UPDATE lv
		SET lv.VILLAGE_CODE=lv_map.NEW_CODE
		FROM TBL_LICENSE_VILLAGE lv
		INNER JOIN DATA_MAPPING_VILLAGE lv_map ON lv_map.OLD_CODE=lv.VILLAGE_CODE

		--UPDATE VILLAGE_CODE IN TBL_DISTRIBUTION_VILLAGE
		UPDATE dis
		SET dis.VILLAGE_CODE=dis_map.NEW_CODE
		FROM TBL_DISTRIBUTION_VILLAGE dis
		INNER JOIN DATA_MAPPING_VILLAGE dis_map ON dis_map.OLD_CODE=dis.VILLAGE_CODE

		--UPDATE DISTRICT_NAME FROM 'កំពង់លាវ' => 'ពោធិរៀង'
		UPDATE TLKP_DISTRICT 
		SET DISTRICT_NAME=N'ពោធិ៍រៀង'
		WHERE DISTRICT_CODE=1411

		--DROP DATA_MAPPING_VILLAGE FROM DATABASE
		DROP TABLE DATA_MAPPING_VILLAGE
	END
GO

IF OBJECT_ID('DATA_MAPPING') IS NOT NULL
DROP TABLE DATA_MAPPING
	BEGIN
		CREATE TABLE DATA_MAPPING
		(
			OLD_CODE NVARCHAR(50) NOT NULL,
			NEW_CODE NVARCHAR(50) NOT NULL,
			TYPE_ID INT NOT NULL
		)
		INSERT INTO DATA_MAPPING
		SELECT
			OLD_CODE=COMMUNE_CODE
			,NEW_CODE=REPLACE(COMMUNE_CODE, LEFT(COMMUNE_CODE, 6), '14110' + CAST(ROW_NUMBER() OVER(ORDER BY COMMUNE_CODE)AS NVARCHAR(10)))
			,TYPE_ID=3
		FROM TLKP_COMMUNE
		WHERE COMMUNE_CODE IN('141101','141102','141103','141104','141105','140804', '140806')
		UNION ALL
		SELECT
			OLD_CODE=vil.VILLAGE_CODE
			,NEW_CODE = REPLACE(vil.VILLAGE_CODE, LEFT(vil.VILLAGE_CODE, 8), '141100' + CAST(ROW_NUMBER() OVER(ORDER BY vil.VILLAGE_CODE) AS NVARCHAR(10)))
			,TYPE_ID=4
		FROM TLKP_VILLAGE vil
		WHERE COMMUNE_CODE IN('141101','141102','141103','141104','141105','140804', '140806')

		UPDATE com1
		SET	com1.COMMUNE_CODE=dm1.NEW_CODE
			,com1.DISTRICT_CODE='1411'
		FROM TLKP_COMMUNE com1
		INNER JOIN DATA_MAPPING dm1 ON dm1.OLD_CODE=com1.COMMUNE_CODE
		WHERE dm1.TYPE_ID=3

		UPDATE vil1
		SET vil1.VILLAGE_CODE=dmap.NEW_CODE
			,vil1.COMMUNE_CODE=dmap1.NEW_CODE
		FROM TLKP_VILLAGE vil1
		INNER JOIN DATA_MAPPING dmap ON dmap.OLD_CODE=vil1.VILLAGE_CODE
		INNER JOIN DATA_MAPPING dmap1 ON dmap1.OLD_CODE=vil1.COMMUNE_CODE
		WHERE dmap.TYPE_ID=4 AND dmap1.TYPE_ID=3

		--DROP DATA_MAPPING FROM DATABASE
		DROP TABLE DATA_MAPPING
	END
GO

--INSERT NEW VILLAGE IN SIEMREAB PROVINCE
IF NOT EXISTS(SELECT * FROM TLKP_VILLAGE WHERE VILLAGE_CODE='17030609')
	BEGIN
		INSERT INTO TLKP_VILLAGE 
		SELECT '17030609', N'តាពេន', '170306'
	END
GO
";
        }
    }
}
