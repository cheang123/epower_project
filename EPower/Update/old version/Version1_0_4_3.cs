﻿namespace EPower.Update
{
    /// <summary>
    /// fix report profit & loss
    /// add income account to customer type
    /// add income account to invoice item
    /// show page phase / hide page constant
    /// 
    /// </summary>
    class Version1_0_4_3 : Version
    {
        protected override string GetBatchCommand()
        {
            return @"
IF NOT EXISTS(SELECT * FROM TBL_VOLTAGE WHERE REPLACE(VOLTAGE_NAME,' ','')='0.4KV') 
	INSERT INTO TBL_VOLTAGE VALUES('0.4KV',1);
GO
IF NOT EXISTS(SELECT * FROM TBL_VOLTAGE WHERE REPLACE(VOLTAGE_NAME,' ','')='22KV') 
	INSERT INTO TBL_VOLTAGE VALUES('22KV',1); 
GO 
/*
SELECT 'UPDATE TBL_ACCOUNT_CATEGORY SET NOTE=N'''+NOTE+''' WHERE NOTE='''' AND CATEGORY_ID='+CONVERT(NVARCHAR,CATEGORY_ID) +';'
FROM TBL_ACCOUNT_CATEGORY
*/
UPDATE TBL_ACCOUNT_CATEGORY SET NOTE=N'រួមមានចំណូលពីការផ្គត់ផ្គង់ថាមពលអគ្គិសនី ដែលតំលៃសរុបផ្អែកលើថាមពលអគ្គិសនី ដែលផ្តល់ដោយវាស់តាមរយះកុងទ័រ។' WHERE NOTE='' AND CATEGORY_ID=2;
UPDATE TBL_ACCOUNT_CATEGORY SET NOTE=N'រួមទាំងចំណូលលើថាមពលអគ្គិសនី ដែលបានផ្គត់ផ្គង់ ដែលតំលៃត្រូវផ្អែកលើវិធីសាស្រ្តផ្សេងទៀតដោយឡែកពីចំនួនថាមពលអគ្គិសនី ដែលបានផ្គត់ផ្គង់បច្ចុប្បន្ន ដូចជាការលក់អគ្គិសនីក្នុងរយះពេលមួយ ឬក្រោមកិច្ចសន្យាណាមួយរបស់អង្គភាព។' WHERE NOTE='' AND CATEGORY_ID=3;
UPDATE TBL_ACCOUNT_CATEGORY SET NOTE=N'រួមមានចំណូលថាមពលអគ្គិសនីផ្សេងទៀត ដែលផ្តល់សេវាកម្មដល់អតិថិជន ដូចជាតំលៃតភ្ជាប់ ការស្ទង់នាឡិការ ការផ្តាច់ ឬការតភ្ជាប់ថ្មី ការផាកពិន័យលើការប្រើតិច ថ្លៃសេវាកម្មរដ្ឋបាល និងខ្សែតំឡើង។' WHERE NOTE='' AND CATEGORY_ID=4;
UPDATE TBL_ACCOUNT_CATEGORY SET NOTE=N'រួមមានចំណូលផ្សេងទៀត ដែលទាក់ទងនឺងសេវាកម្មអគ្គិសនី ដែលផ្តល់ដល់អតិថិជន ដូចជាទណ្ឌកម្មផាក ផាកបង់យឺត ចំណូលពីការជួលពីទ្រព្យដែលប្រើ និងចំណូលផ្សេងទៀត។' WHERE NOTE='' AND CATEGORY_ID=5;
UPDATE TBL_ACCOUNT_CATEGORY SET NOTE=N'តំណាងឲ្យថាមពលអគ្គិសនី ដែលទិញពីអ្នកផលិតឯករាជ្យ។' WHERE NOTE='' AND CATEGORY_ID=6;
UPDATE TBL_ACCOUNT_CATEGORY SET NOTE=N'រួមទាំងថ្លៃសម្ភារះទាំងអស់ ពលកម្ម និងចំណាយផ្សេងៗទៀត កើតមានក្នុងការផលិតអគ្គិាសនី។ រួមមានប្រេងឥន្ទនះ ប្រេងលំអិល និងរបស់ប្រើផ្សេងទៀត ការត្រួតពិនិត្យ និងវិស្វកម្ម ការធានា ជួលជុល ថែទាំ ជួល និងការចំណាយផ្សេងទៀត។' WHERE NOTE='' AND CATEGORY_ID=7;
UPDATE TBL_ACCOUNT_CATEGORY SET NOTE=N'' WHERE NOTE='' AND CATEGORY_ID=8;
UPDATE TBL_ACCOUNT_CATEGORY SET NOTE=N'  ' WHERE NOTE='' AND CATEGORY_ID=9;
UPDATE TBL_ACCOUNT_CATEGORY SET NOTE=N'' WHERE NOTE='' AND CATEGORY_ID=10;
UPDATE TBL_ACCOUNT_CATEGORY SET NOTE=N'រួមមានរំលោះ និងការសងប្រាក់លើទ្រព្យ និងរោងចក្រសំរាប់ឆ្នាំ។' WHERE NOTE='' AND CATEGORY_ID=11;
UPDATE TBL_ACCOUNT_CATEGORY SET NOTE=N'' WHERE NOTE='' AND CATEGORY_ID=12;
UPDATE TBL_ACCOUNT_CATEGORY SET NOTE=N'រួមមានការប្រាក់ដែលកើតឡើងលើប្រាក់ខ្លីរយះពេលវែង។ ការប្រាក់ត្រួវគិតប្រចាំខែជាមួយការប្រាក់ដែលមិនបានបង់នៅដំណាច់ខែនីមួយៗបូកបញ្ចូលក្នុងចំនួនគោលការណ៍ ប្រាក់ទុន និងប្រាក់ចំណេញក្នុងអត្រាដូចប្រាក់ការហើយដែលត្រូវតាមលក្ខណដូចប្រាក់ទុន។' WHERE NOTE='' AND CATEGORY_ID=13;
UPDATE TBL_ACCOUNT_CATEGORY SET NOTE=N'រួមទាំងតំលៃការប្រាក់នៃទុនដែលខ្លីប្រើសំរាប់ការសាងសង់ ដែលតំលៃនេះត្រូវគិតលើការងារមួយៗដែលទុននោះត្រូវប្រើ។ រយះពេលដែលការប្រាក់នោះត្រូវបានចំណេញ គឺកំណត់តែក្នុងរយះពេលសាងសង់តែប៉ុណ្ណោះ។ ' WHERE NOTE='' AND CATEGORY_ID=14;
UPDATE TBL_ACCOUNT_CATEGORY SET NOTE=N'រួមមានការគិតលើស និងពិន័យផ្សេងៗដែលកើតឡើងពីការបង់ប្រាក់យឺតលើឥណទានរយះពេលវែង។' WHERE NOTE='' AND CATEGORY_ID=15;
UPDATE TBL_ACCOUNT_CATEGORY SET NOTE=N'រួមមានរាល់ការប្រាក់ដទៃទៀតដែលពុំមានចែងក្នុងខ្ទង់គណនីខាងលើ។' WHERE NOTE='' AND CATEGORY_ID=16;
UPDATE TBL_ACCOUNT_CATEGORY SET NOTE=N'រួមមានការប្រាក់លើតុល្យការធនាគារ ប័ណ្ណផ្ញើរប្រាក់ ខ្ទង់គណនី ប័ណ្ណសាច់ ប្រាក់ប័ណ្ណផ្សេងៗទៀតជាសក្ខីភាពនៃបំណុល ចំណូលពីខ្ទង់ទាំងនោះជាទ្រព្យរបស់គណនីអង្គភាព។' WHERE NOTE='' AND CATEGORY_ID=17;
UPDATE TBL_ACCOUNT_CATEGORY SET NOTE=N'រួមមានការលក់លើរបស់លើស ឬមិនប្រើ ឬហួសសម័យ ដែលពុំបានប្រើប្រាស់តទៅទៀតក្នុងតំណើរការ។' WHERE NOTE='' AND CATEGORY_ID=18;
UPDATE TBL_ACCOUNT_CATEGORY SET NOTE=N'ចំណូលផ្សេងៗទៀតពីទ្រព្យមិនប្រតិបត្តិការ ឬក្នុងតំណើរការនៃសកម្មភាពមិនទាក់ទងផ្ទាល់ និងមូលដ្ឋានផ្គត់ផ្គង់សេវាកម្មដោយសហគ្រាសរដ្ឋាភិបាល។' WHERE NOTE='' AND CATEGORY_ID=19;
UPDATE TBL_ACCOUNT_CATEGORY SET NOTE=N'ការចំណេញ (ខាត) ពីការទុករូបិយប័ណ្ណបរទេសក្នុងរយះពេលមួយដែលជាលទ្ធផលនៃការផ្លាស់ប្តូរអត្រាប្តូរប្រាក់' WHERE NOTE='' AND CATEGORY_ID=20;
UPDATE TBL_ACCOUNT_CATEGORY SET NOTE=N'ការចំណាយដែលកើតមានចំពោះសកម្មភាព ដែលមិនទាក់ទងផ្ទាល់ទៅនឹងកិច្ចដំណើរការរបស់អង្គភាព ហើយដែលពុំមានចែងក្នុងខ្ទង់ណាផ្សេងទៀត។' WHERE NOTE='' AND CATEGORY_ID=21;
GO
SELECT 'UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'''+NOTE+''' WHERE NOTE='''' AND ITEM_ID='+CONVERT(NVARCHAR,ITEM_ID) +';'
FROM TBL_ACCOUNT_ITEM
GO
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'' WHERE NOTE='' AND ITEM_ID=1;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'' WHERE NOTE='' AND ITEM_ID=2;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'' WHERE NOTE='' AND ITEM_ID=3;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'' WHERE NOTE='' AND ITEM_ID=4;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'' WHERE NOTE='' AND ITEM_ID=5;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'ពោលដល់ការលក់ដែលគ្មានវិក្កយបត្រ គិតពីថ្ងៃកាត់កុងទ័ររហូតដល់ដំណាច់ខែ។' WHERE NOTE='' AND ITEM_ID=6;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'' WHERE NOTE='' AND ITEM_ID=7;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមទាំងចំណូលលើថាមពលអគ្គិសនី ដែលបានផ្គត់ផ្គង់ ដែលតំលៃត្រូវផ្អែកលើវិធីសាស្រ្តផ្សេងទៀតដោយឡែកពីចំនួនថាមពលអគ្គិសនី ដែលបានផ្គត់ផ្គង់បច្ចុប្បន្ន ដូចជាការលក់អគ្គិសនីក្នុងរយះពេលមួយ ឬក្រោមកិច្ចសន្យាណាមួយរបស់អង្គភាព។' WHERE NOTE='' AND ITEM_ID=8;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'ថ្លៃតភ្ចាប់គឺសំរាប់ភ្ជាប់សេវាអគ្គិសនីដល់អតិថិជន។ ម្យ៉ាងទៀតការផ្តាច់ និងតភ្ជាប់ថ្មី រួមមានការបង់ប្រាក់សំរាប់ភ្ជាប់ទៅសេវាអគ្គិសនីរបស់អង្គភាពក្រោយពេលកាត់ផ្តាច់ក្នុងរយះពេលណាមួយដែលមិនបានបង់ប្រាក់។' WHERE NOTE='' AND ITEM_ID=9;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'គឺថ្លៃលើការតំឡើង ពិនិត្យ ថ្លឹង ដោះ ឬជួលជុលឧបករណ៍កុងទ័រ។' WHERE NOTE='' AND ITEM_ID=10;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'ថ្លៃតំណាងឲ្យភាពខុសគ្នារវាងចំនួនអគ្គិសនីក្នុងវិក្កយបត្រ និងចំនួនប្រើក្នុងវិក្កយបត្រតិចបំផុត។' WHERE NOTE='' AND ITEM_ID=11;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'គឺថ្លៃសំរាប់ការងាររដ្ឋបាល និងជំនួយការផ្សេងៗ ឬសេវាកម្មផ្តល់ដោយថាមពលអគ្គិសនី។' WHERE NOTE='' AND ITEM_ID=12;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'ថ្លៃសំរាប់ការតំឡើងខ្សែដល់អតិថិជន។' WHERE NOTE='' AND ITEM_ID=13;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមទាំងការធ្វើឲ្យខូច ធ្វើឲ្យយឺតកុងទ័រ ឬលួចចរន្តអគ្គិសនី។' WHERE NOTE='' AND ITEM_ID=14;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមទាំងការប្រាក់ និងវិធានការផ្សេងៗទៀត ដែលអង្គភាពប្រើសំរាប់ប្រមូលប្រាក់ត្រូវបង់ហើយយឺត។' WHERE NOTE='' AND ITEM_ID=15;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមទាំងថ្លៃឈ្នួលលើអាគារដី និងទ្រព្យផ្សេងៗទៀត។ រួមមានទាំងការប្រើរួមនូវការិយាល័យ អាគារ និងការប៊ីនជួលដី និងទ្រព្យផ្សេងៗទៀត។' WHERE NOTE='' AND ITEM_ID=16;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមទាំងចំណូលដែលមិនបានចែងក្នុងខ្ទង់ចំណូលណាទៀត។' WHERE NOTE='' AND ITEM_ID=17;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'តំណាងឲ្យថាមពលអគ្គិសនី ដែលទិញពីអ្នកផលិតឯករាជ្យ។' WHERE NOTE='' AND ITEM_ID=18;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមទាំងតំលៃប្រេងដែលប្រើដូចជា ធ្យូង ឧស្ម័ន និងប្រេងផ្សេងៗទៀតក្នុងការផលិតអគ្គិសនី។' WHERE NOTE='' AND ITEM_ID=19;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N' តំណាងសំភារះដែលប្រើក្នុងដំណើរការដូចជាប្រេងលំអិល និងគ្រឿងបន្លាសផ្សេងទៀត ប្រដាប់ការិយាល័យ និងគ្រឿងផ្គត់ផ្គង់ផ្សេងទៀត។' WHERE NOTE='' AND ITEM_ID=20;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'គឺខ្ទង់ចំណាយសំរាប់ចំនួនទឹក ប្រើសំរាប់ក្នុងការផលិតអគ្គិសនី។ ទឹកដែលប្រើសំរាប់គោលបំណងផ្សេងៗទៀត មិនចុះក្នុងគណនីនេះទេ។' WHERE NOTE='' AND ITEM_ID=21;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានប្រាក់បៀវត្សសំរាប់មន្រ្តី វិស្វករ កម្មករ ដែលទាក់ទងនឹងទ្រព្យរបស់រោងចក្រ ឧបករណ៍ផ្សេងៗ។ រួមទាំងចាប់តាំងពីកន្លែងផលិត ទូបញ្ជារ កាប៊ីន ខ្សែបញ្ជូន និងចែកចាយ។' WHERE NOTE='' AND ITEM_ID=22;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមទាំងការបង់លើពន្ធសំរាប់ធានារបស់អង្គភាពលើរោងចក្រ និងឧបករណ៍។' WHERE NOTE='' AND ITEM_ID=23;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមទាំងការជួលទ្រព្យដែលប្រើក្នុ្ងងការផលិតផ្សេងៗទៀត។' WHERE NOTE='' AND ITEM_ID=24;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានសំភារះពលកម្ម និងការចំណាយផ្សេងៗទៀត ដែលកើតមានដើម្បីថែទាំអាគាររោងចក្រ និងឧបករណ៍ឲ្យបានល្អសំរាប់ផលិតថាមពល។ រួទាំងថ្លៃជួលជុលអាគារ និងគ្រឿងផ្សេងៗដូចជាទឺក ភ្លើង របង ជញ្ជាំង និងចំណាយផ្សេងៗទៀត។' WHERE NOTE='' AND ITEM_ID=25;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានការចំណាយដែលពុំមានចែងក្នុងខ្ទង់គណនីផលិតណាទៀតនោះ។' WHERE NOTE='' AND ITEM_ID=26;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមទាំងតំលៃពលកម្ម និងចំណាយដែលកើតមានក្នុងការណែនាំ និងត្រួតពិនិត្យទូទៅលើកិច្ចដំណើរការ និងថែទាំឧបករណ៍បញ្ជូន។' WHERE NOTE='' AND ITEM_ID=27;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រាល់ពលកម្ម និងការចំណាយផ្សេងៗ ដែលទាក់ទងក្នុងការបញ្ជូនថាមពល។' WHERE NOTE='' AND ITEM_ID=28;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'ពលកម្ម  និងចំណាយផ្សេងៗទាក់ទងការប៊ីន។' WHERE NOTE='' AND ITEM_ID=29;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'ពលកម្ម និងចំណាយផ្សេងទៀតទាក់ទងខ្សែអាកាសក្រោមដី និងខ្សែផ្សេងៗទៀតក្នុងកិច្ចដំណើរការបណ្តាញបញ្ជូន។' WHERE NOTE='' AND ITEM_ID=30;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'ពលកម្ម និងសំភារះដែលប្រើ និងការចំណាយដែលកើតមានក្នុងកិច្ចដំណើរការ និងថែទាំឧបករណ៍បញ្ជូន។' WHERE NOTE='' AND ITEM_ID=31;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'ថ្លៃដែលជួលឧបករណ៍ខ្សែបញ្ជូន។' WHERE NOTE='' AND ITEM_ID=32;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានតំលៃពលកម្ម និងសំភារះដែលប្រើ និងចំណាយដែលមានហើយ ដែលមិនបានចែងក្នុងខ្ទង់គណនីខ្សែបញ្ជូនផ្សេង។' WHERE NOTE='' AND ITEM_ID=33;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានតំលៃពលកម្ម និងចំណាយដែលមានឡើងចំពោះការត្រួតពិនិត្យជាទូទៅ លើកិច្ចដំណើរការ និងថែទាំឧបករណ៍ចែកចាយ។' WHERE NOTE='' AND ITEM_ID=34;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រាល់ពលកម្ម និងការចំណាយផ្សេងៗ ដែលទាក់ទងក្នុងការបញ្ជូនថាមពល។' WHERE NOTE='' AND ITEM_ID=35;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'ពលកម្ម និងការចំណាយ ដែលទាក់ទងកាប៊ីនចែកចាយ។ រួមទាំងចំណាយលើការថែទាំលើឧបករណ៍ចែកចាយ។ រួមមានទាំងថ្លៃថែទាំ ជួលជុលត្រង់ស្វូ គ្រឿងប្រដាប់ ទោះជានៅបង្គោល លើទីតាំងរបស់អតិថិជន រួមទាំងដូរប្រេងថ្មី។' WHERE NOTE='' AND ITEM_ID=36;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'' WHERE NOTE='' AND ITEM_ID=37;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានតំលៃពលកម្ម និងការផ្គត់ផ្គង់ដែលកើតមានក្នុងការថែរក្សាអំពូលតាមផ្លូវ និងពាណិជ្ជកម្ម និងឧបករណ៍រួមមានទាំងការផ្លាស់ប្តូរថ្មីនូវផ្នែកខូច ការជួសជុល ការកែច្នៃ សាកល្បងពិនិត្យក្នុងរោងជាងរបស់អង្គភាព ជួសជុល ផ្លាស់ប្តូរ គ្រឿងអំពូលផ្សេងៗ និងតំលៃទាក់ទងផ្សេងៗដែលទាក់ទងនឹងការបំភ្លឺផ្លូវ និងប្រព័ន្ធសញ្ញា។' WHERE NOTE='' AND ITEM_ID=38;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'តំលៃដែលទាក់ទងជាមួយការប្រតិបត្តិការនៃការតំឡើងកុងទ័រសំរាប់វាស់ថាមពលអគ្គិសនី ដែលបានលក់ដល់អតិថិជន។ រួមមានតំលៃថែទាំ និងជួសជុលផ្លាស់ប្តូរគ្រឿង។' WHERE NOTE='' AND ITEM_ID=39;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'តំលៃទាក់ទងលើការតំឡើងជួនអតិថិជន  រួមមានតំលៃទាក់ទងការថែរក្សាប្រសិទ្ធភាពនៃការតំឡើងជូនអតិថិជននៅកន្លែងរបស់គេ។' WHERE NOTE='' AND ITEM_ID=40;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'តំលៃរួមមានសំរាប់ជួលឧបករណ៍ចែកចាយ។' WHERE NOTE='' AND ITEM_ID=41;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានតំលៃពលកម្ម និងសំភារះដែលប្រើ និងចំណាយដែលមានហើយដែលពុំមានចាត់ចូលក្នុងគណនីចំណាយចែកចាយផ្សេងៗទៀត។' WHERE NOTE='' AND ITEM_ID=42;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានតំលៃពលកម្ម និងការចំណាយដែលមានក្នុងការត្រួតពិនិត្យនូវគណនេយ្យអតិថិជន និងសកម្មភាពប្រមូលប្រាក់។' WHERE NOTE='' AND ITEM_ID=43;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានការចំណាយរបស់អង្គភាពសំរាប់ផ្សាយពាណិជ្ជកម្មក្នុងការសែត ទូរទស្សន៍ ទស្សនាវដ្តី វិទ្យុ និងបណ្តាញពត៌មានដទៃទៀត រួមទាំងការចំណាយលើសៀវភៅតូច ក្រដាសផ្សាយផ្សេងៗ។' WHERE NOTE='' AND ITEM_ID=44;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានតំលៃពលកម្ម និងសំភារះដែលប្រើ និងការចំណាយដែលមាន (ដូចជា ផ្លាក អំពូល ឯកសណ្ឋាន សៀវភៅ សឺមី ការដឹកជញ្ជូន  អាហារ។ល។) ក្នុងការមើលកុងទ័រ រួមមានទាំងតំលៃពលកម្ម និងសំភារះដែលចំណាយក្នុងការមើលពាក្យសុំរបស់អតិថិជន កិច្ចសន្យា ពាក្យសុំទិញ វិក្កយបត្រ គណនេយ្យ ការប្រមូល និងការទិទៀន។ ' WHERE NOTE='' AND ITEM_ID=45;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'ចំនួនប៉ាន់ស្មានដែលមិនអាច ទារបាន  ដែលមានក្នុងរយះពេលនៃគណនេយ្យ។' WHERE NOTE='' AND ITEM_ID=46;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានតំលៃជាភាគរយ ដែលផ្តល់ដល់អ្នកប្រមូលប្រាក់ ដើម្បីបង្កើននូវការទារប្រាក់ នៃគណនីឥណទេយ្យរបស់អង្គភាព។' WHERE NOTE='' AND ITEM_ID=47;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានតំលៃពលកម្ម និងសំភារះដែលប្រើ និងការចំណាយដែលមានហើយ ដែលពុំបានចែងក្នុងខ្ទង់ចំណាយលើអតិថិជនផ្សេងទៀត។' WHERE NOTE='' AND ITEM_ID=48;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានរំលោះ និងការសងប្រាក់លើទ្រព្យ និងរោងចក្រសំរាប់ឆ្នាំ។' WHERE NOTE='' AND ITEM_ID=49;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានប្រាក់បៀវត្ស និងប្រាក់បន្ថែមផ្សេងៗសំរាប់បុគ្គលិកការិយាល័យទូទៅក្នុងកិច្ចប្រតិបត្តិការអង្គភាព និងដែលមិនផ្ទាស់ចំពោះមុខងារដំណើរការ។' WHERE NOTE='' AND ITEM_ID=50;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានប្រាក់បន្ថែមសំរាប់លើសម៉ោង និងថ្ងៃឈប់សំរាកដែលទាក់ទងនឹងរដ្ឋបាលទូទៅនៃកិច្ចប្រតិបត្តិការអង្គភាព។ ប្រាក់លើសម៉ោង និងថ្ងៃឈប់សំរាកសំរាប់មុខងារផ្សេងទៀត ត្រូវរួមក្នុងតំលៃពលកម្មសំរាប់ការថែទាំ និងដំណើរការ។' WHERE NOTE='' AND ITEM_ID=51;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានទាំងការចំណាយដែលកើតឡើងសំរាប់សេវាមិនមែនជំនាញ ដែលជួលដោយអង្គភាពដូចជាសន្តិសុខ និងការបោសសំអាត។' WHERE NOTE='' AND ITEM_ID=52;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានចំណែករបស់អង្គភាពលើពន្ធ លើតំលៃសន្តិសុខសង្គម។' WHERE NOTE='' AND ITEM_ID=53;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានការចំណាយសំរាប់ផលប្រយោជន៍ និងសំរាប់ប្រាក់និវត្តបុគ្គលិក។' WHERE NOTE='' AND ITEM_ID=54;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានការចំណាយទាំងអស់ ដែលទាក់ទងការសិក្សា និងអភិវឌ្ឃន៍ធនធានបុគ្គលិក។' WHERE NOTE='' AND ITEM_ID=55;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានការចំណាយដែលកើតមាន សំរាប់ការធ្វើដំណើរក្រៅប្រទេសរបស់មន្រ្តី បុគ្គលិក ដែលរួមមានថ្លៃសំបុត្រ និងសណ្ឋាគារ អាហារ ប្រាក់ហោប៉ៅ និងពន្ធព្រលានយន្តហោះ។' WHERE NOTE='' AND ITEM_ID=56;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានការធ្វើដំណើរទាំងអស់ ប្រាក់ហោប៉ៅដែលត្រូវចំណាយដោយបុគ្គលិកអង្គភាព ដូចជាការចុះខេត្តក្នុងប្រទេស។' WHERE NOTE='' AND ITEM_ID=57;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានការដឹកជញ្ជូន និងធ្វើដំណើរផ្សេងៗទៀតដោយបុគ្កលិក ដែលពុំមានក្នុងការចំណាយធ្វើដំណើរក្នុងស្រុក។' WHERE NOTE='' AND ITEM_ID=58;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានតំលៃនៃការផ្គត់ផ្គង់ការិយាល័យ  បោះពុម្ពកាសែត សៀវភៅ ថ្លៃសមាជិក និងការចំណាយផ្សេងៗទៀត។' WHERE NOTE='' AND ITEM_ID=59;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានការចំណាយសំរាប់ប្រៃសណីយ៍ ទូរស័ព្ទ email internet និងតំលៃទូរគមនាគមន៍ផ្សេងៗទៀត។' WHERE NOTE='' AND ITEM_ID=60;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រាល់ពន្ធដែលមិនមែនជាពន្ធលើប្រាក់ចំណេញ ដូចជាពន្ធប្រេង ពន្ធផលរបរ ពន្ធនាំចូល និងពន្ធផ្សេងៗទៀត។' WHERE NOTE='' AND ITEM_ID=61;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានការជួលសំរាប់ទ្រព្យដែលប្រើផ្សេងទៀតដែលទាក់ទងនឹងគណនីអតិថិជន ការលក់និងមុខងាររដ្ឋបាលរបស់អង្គភាព។' WHERE NOTE='' AND ITEM_ID=62;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'' WHERE NOTE='' AND ITEM_ID=63;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'តំលៃនៃការជួសជុល និងផ្លាស់ប្តូរគ្រឿងសង្ហារឹមការិយាល័យ ជួសជុលការិយាល័យទូទៅ និងការចំណាយផ្សេងទៀតលើការរិយាល័យទូទៅ។' WHERE NOTE='' AND ITEM_ID=64;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'ថ្លៃដែលបង់ឲ្យអ្នកច្បាប់ពីខាងក្រៅ គណនេយ្យករ ឬអ្នកពិនិត្យ វិស្វករ និងអ្នកជំនាញផ្សេងទៀតដែលជួលដោយអង្គភាព។' WHERE NOTE='' AND ITEM_ID=65;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានថ្លៃបង់ឲ្យទីប្រឹក្សា ឬក្រុមហ៊ុនសំរាប់សេវាកម្មរបស់គេក្នុងការគ្រប់គ្រង ឬជួយក្នុងកិច្ចដំណើរការអង្គភាព។ រួមមានទាំងចំនួនដែលបង់ឲ្យក្រុមហ៊ុននោះ ដើម្បីដកប្រាក់សំរាប់ការចំណាយ ដែលមានសំរាប់ជាប្រយោជន៍អង្គភាព ប្រសិនបើការចំណាយនោះពុំអាចចុះក្នុងគណនីចំណាយប្រតិបត្តិការ។' WHERE NOTE='' AND ITEM_ID=66;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានប្រាក់បង់លើពន្ធទ្រព្យ និងប្រាក់ធានាផ្សេងៗទៀត។ រួមទាំងការធានាបុគ្គលិក និងទុនបំរុងសំរាប់ការធានាទ្រព្យសម្បត្តិ។' WHERE NOTE='' AND ITEM_ID=67;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានតំលៃធានារ៉ាបរង ឫទុនបំរុងដែលចាំបាច់សំរាប់ផ្តល់នូវការការពារទប់ទល់ និងភាពប្រថុយប្រថាន ដូចជារបួស និងការខូចខាត  ដែលត្រូវអង្គភាពចេញសំរាប់ការបាត់បង់ដែលពុំមានចែងក្នុងការធានារ៉ាប់រង និងការចំណាយពិតប្រាកដដែលកើតឡើងក្នុងការទូទាត់។' WHERE NOTE='' AND ITEM_ID=68;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានរាល់ថ្លៃដែលគិតដោយធនាគារ ក្នុងសេវាកម្មគណនីធនាគាររបស់អង្គភាព។' WHERE NOTE='' AND ITEM_ID=69;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានការចំណាយដែលកើតឡើងក្នុងការបង្កើននូវមុខមាត់អង្គភាពតាមរយះបង្ហាញតំណាងជាមួយអតិថិជន និងមិនមែនអតិថិជន។ ' WHERE NOTE='' AND ITEM_ID=70;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានអំណោយសប្បុរសធម៌ សាសនា និងអំណោយផ្សេងៗទៀត និងវិភាគទានដោយអង្គភាព។' WHERE NOTE='' AND ITEM_ID=71;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'ការចំណាយទូទៅផ្សេងៗទៀតលើអ្វីដែលមិនមែនជាសំភារះដែលមិនអាចចុះក្នុងការចំណាយលើហិរញ្ញវត្ថុ។' WHERE NOTE='' AND ITEM_ID=72;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានការប្រាក់ដែលកើតឡើងលើប្រាក់ខ្លីរយះពេលវែង។ ការប្រាក់ត្រួវគិតប្រចាំខែជាមួយការប្រាក់ដែលមិនបានបង់នៅដំណាច់ខែនីមួយៗបូកបញ្ចូលក្នុងចំនួនគោលការណ៍ ប្រាក់ទុន និងប្រាក់ចំណេញក្នុងអត្រាដូចប្រាក់ការហើយដែលត្រូវតាមលក្ខណដូចប្រាក់ទុន។' WHERE NOTE='' AND ITEM_ID=73;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមទាំងតំលៃការប្រាក់នៃទុនដែលខ្លីប្រើសំរាប់ការសាងសង់ ដែលតំលៃនេះត្រូវគិតលើការងារមួយៗដែលទុននោះត្រូវប្រើ។ រយះពេលដែលការប្រាក់នោះត្រូវបានចំណេញ គឺកំណត់តែក្នុងរយះពេលសាងសង់តែប៉ុណ្ណោះ។ ' WHERE NOTE='' AND ITEM_ID=74;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានការគិតលើស និងពិន័យផ្សេងៗដែលកើតឡើងពីការបង់ប្រាក់យឺតលើឥណទានរយះពេលវែង។' WHERE NOTE='' AND ITEM_ID=75;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានរាល់ការប្រាក់ដទៃទៀតដែលពុំមានចែងក្នុងខ្ទង់គណនីខាងលើ។' WHERE NOTE='' AND ITEM_ID=76;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានការប្រាក់លើតុល្យការធនាគារ ប័ណ្ណផ្ញើរប្រាក់ ខ្ទង់គណនី ប័ណ្ណសាច់ ប្រាក់ប័ណ្ណផ្សេងៗទៀតជាសក្ខីភាពនៃបំណុល ចំណូលពីខ្ទង់ទាំងនោះជាទ្រព្យរបស់គណនីអង្គភាព។' WHERE NOTE='' AND ITEM_ID=77;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'រួមមានការលក់លើរបស់លើស ឬមិនប្រើ ឬហួសសម័យ ដែលពុំបានប្រើប្រាស់តទៅទៀតក្នុងតំណើរការ។' WHERE NOTE='' AND ITEM_ID=78;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'ចំណូលផ្សេងៗទៀតពីទ្រព្យមិនប្រតិបត្តិការ ឬក្នុងតំណើរការនៃសកម្មភាពមិនទាក់ទងផ្ទាល់ និងមូលដ្ឋានផ្គត់ផ្គង់សេវាកម្មដោយសហគ្រាសរដ្ឋាភិបាល។' WHERE NOTE='' AND ITEM_ID=79;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'ការចំណេញ (ខាត) ពីការទុករូបិយប័ណ្ណបរទេសក្នុងរយះពេលមួយដែលជាលទ្ធផលនៃការផ្លាស់ប្តូរអត្រាប្តូរប្រាក់' WHERE NOTE='' AND ITEM_ID=80;
UPDATE TBL_ACCOUNT_ITEM SET NOTE=N'ការចំណាយដែលកើតមានចំពោះសកម្មភាព ដែលមិនទាក់ទងផ្ទាល់ទៅនឹងកិច្ចដំណើរការរបស់អង្គភាព ហើយដែលពុំមានចែងក្នុងខ្ទង់ណាផ្សេងទៀត។' WHERE NOTE='' AND ITEM_ID=81;
";
        }
    }
}
