﻿namespace EPower.Update
{
    /// <summary>
    /// - Enable/Disable User to Edit Account Chart
    /// - Enable/Disable User to Edit Fixed Asset Category
    /// - Update Permission (Add Staff Expense, Loan, Fuel Source, Fuel Purchase)
    /// - Update Account Chart (Tax)
    /// - ANNUAL REPORT
    ///   * AS00 - AS013
    /// </summary>
    /// <returns></returns>

    class Version1_0_9_4 : Version
    {        
        protected override string GetBatchCommand()
        {
            return @"
-----------------------------------------------------------------------------
-- UTILITY UPDATE
-----------------------------------------------------------------------------
IF NOT EXISTS (SELECT * FROM TBL_UTILITY WHERE UTILITY_ID = 59)
    INSERT INTO TBL_UTILITY VALUES(59,	N'ENABLE_EDIT_ACCOUNT_CHART',	N'Allow user to edit account chart',0); 

IF NOT EXISTS (SELECT * FROM TBL_UTILITY WHERE UTILITY_ID = 60)
    INSERT INTO TBL_UTILITY VALUES(60,	N'ENABLE_EDIT_FIXED_ASSET_CATEGORY',	N'Allow user to edit fixed asset category',0);

IF NOT EXISTS (SELECT * FROM TBL_UTILITY WHERE UTILITY_ID = 61)
    INSERT INTO TBL_UTILITY VALUES(61,	N'ENABLE_ANNAUL_REPORT',	N'User EAC annual report',1);
GO
-----------------------------------------------------------------------------
-- PERMISSION UPDATE
-----------------------------------------------------------------------------
/*
SELECT 'UNION ALL SELECT '+
'N'''+CONVERT(NVARCHAR(MAX),PERMISSION_ID)+''''+','+
'N'''+CONVERT(NVARCHAR(MAX),PERMISSION_DISPLAY)+''''+','+
'N'''+CONVERT(NVARCHAR(MAX),PERMISSION_NAME)+''''+','+
'N'''+CONVERT(NVARCHAR(MAX),PERMISSION_ORDER)+''''+','+
'N'''+CONVERT(NVARCHAR(MAX),PERMISSION_PARENT_ID)+''''+','+
'N'''+CONVERT(NVARCHAR(MAX),PERMISSION_LEVEL)+'''' 
FROM TBL_PERMISSION;
*/
GO
TRUNCATE TABLE TBL_PERMISSION;
SET IDENTITY_INSERT TBL_PERMISSION ON;
INSERT INTO TBL_PERMISSION(PERMISSION_ID,PERMISSION_DISPLAY,PERMISSION_NAME,PERMISSION_ORDER,PERMISSION_PARENT_ID,PERMISSION_LEVEL)
SELECT N'8',N'បង់ប្រាក់មុន',N'PREPAID',N'0',N'0',N'0'
UNION ALL SELECT N'9',N'បង់ប្រាក់ក្រោយ',N'POSTPAID',N'2',N'0',N'0'
UNION ALL SELECT N'10',N'ទទួលប្រាក់បង់',N'PAYMENT',N'1',N'9',N'0'
UNION ALL SELECT N'11',N'បញ្ចូលអំណាន',N'INPUTUSAGE',N'4',N'9',N'0'
UNION ALL SELECT N'18',N'អតិថិជន និងការទូទាត់',N'CUSTOMERANDBILLING',N'3',N'0',N'1'
UNION ALL SELECT N'19',N'អតិថិជន',N'CUSTOMER',N'0',N'18',N'2'
UNION ALL SELECT N'20',N'គ្រប់គ្រងអតិថិជន',N'REGISTER',N'0',N'19',N'3'
UNION ALL SELECT N'21',N'ចាប់ផ្តើមប្រើ',N'ACTIVATE',N'21',N'19',N'3'
UNION ALL SELECT N'22',N'ផ្តាច់ចរន្ត',N'BLOCK',N'0',N'19',N'3'
UNION ALL SELECT N'23',N'ភ្ជាប់ចរន្ត',N'UNBLOCK',N'0',N'19',N'3'
UNION ALL SELECT N'24',N'កំណត់ការទូទាត់',N'SETUPRUNBILL',N'0',N'18',N'2'
UNION ALL SELECT N'25',N'តំលៃអគ្គិសនី',N'PRICE',N'0',N'24',N'3'
UNION ALL SELECT N'26',N'ជុំនៃការទូទាត់',N'BILLINGCYCLE',N'0',N'24',N'3'
UNION ALL SELECT N'27',N'ប្រភេទសេវាកម្ម',N'INVOICEITEMTYPE',N'0',N'24',N'3'
UNION ALL SELECT N'28',N'សេវាកម្ម',N'INVOICEITEM',N'0',N'24',N'3'
UNION ALL SELECT N'29',N'ការចុះតំលៃ',N'DISCOUNT',N'0',N'24',N'3'
UNION ALL SELECT N'30',N'ប្រតិបត្តិការណ៍ទូទាត់',N'BILLINGOPERATION',N'0',N'18',N'2'
UNION ALL SELECT N'31',N'ទូទាត់ការប្រើប្រាស់',N'RUNBILL',N'0',N'30',N'3'
UNION ALL SELECT N'32',N'បោះពុម្ពវិក័្កយបត្រ',N'PRINTINVOICE',N'0',N'30',N'3'
UNION ALL SELECT N'33',N'ប្រវត្តិនៃការទូទាត់',N'BILLINGHISTORY',N'0',N'30',N'3'
UNION ALL SELECT N'34',N'គ្រប់គ្រង់ទូទៅ',N'ADMIN',N'4',N'0',N'1'
UNION ALL SELECT N'35',N'ក្រុមហ៊ុន',N'COMPANY',N'1',N'34',N'2'
UNION ALL SELECT N'36',N'តំបន់',N'AREA',N'2',N'34',N'2'
UNION ALL SELECT N'37',N'បង្កោល និងប្រអប់',N'POLEANDBOX',N'3',N'34',N'2'
UNION ALL SELECT N'38',N'ប្រភេទកុងទ័រ',N'METERTYPE',N'4',N'34',N'2'
UNION ALL SELECT N'39',N'កុងទ័រ',N'METER',N'5',N'34',N'2'
UNION ALL SELECT N'40',N'ប្រភេទឌីសុងទ័រ',N'CIRCUITBREAKERTYPE',N'6',N'34',N'2'
UNION ALL SELECT N'41',N'ឌីសុងទ័រ',N'CIRCUITBREAKER',N'7',N'34',N'2'
UNION ALL SELECT N'42',N'សំភារៈប្រើប្រាស់',N'EQIPMENT',N'8',N'34',N'2'
UNION ALL SELECT N'43',N'ឩបករណ៍',N'DEVICEREGISTER',N'9',N'34',N'2'
UNION ALL SELECT N'44',N'បុគ្គលិក',N'EMPLOYEE',N'10',N'34',N'2'
UNION ALL SELECT N'45',N'កេះដាក់ប្រាក់',N'CASTDRAWER',N'11',N'34',N'2'
UNION ALL SELECT N'46',N'ថ្លៃភ្ជាប់ចរន្ត',N'CONNECTFEE',N'0',N'24',N'3'
UNION ALL SELECT N'47',N'ប្រភេទប្រើប្រាស់',N'CUSTOMERTYPE',N'12',N'34',N'2'
UNION ALL SELECT N'48',N'ខ្នាតអគ្គិសនី',N'UNITELECTRICITY',N'5',N'132',N'0'
UNION ALL SELECT N'49',N'អាំងតង់ស៊ីតេ',N'AMPARE',N'2',N'48',N'3'
UNION ALL SELECT N'50',N'តង់ស្យុង',N'VOLTAGE',N'1',N'48',N'3'
UNION ALL SELECT N'51',N'ហ្វា(Phase)',N'PHASE',N'3',N'48',N'3'
UNION ALL SELECT N'52',N'កុងស្តង់',N'CONTANT',N'4',N'48',N'3'
UNION ALL SELECT N'53',N'លក្ខណៈសំណរ',N'SEAL',N'5',N'48',N'3'
UNION ALL SELECT N'54',N'ផ្សេងៗ',N'OTHER',N'15',N'34',N'0'
UNION ALL SELECT N'55',N'លក្ខ័ណប្រើប្រាស់',N'USERSETTING',N'0',N'54',N'0'
UNION ALL SELECT N'57',N'សុវត្ថិភាពប្រព័ន្ធ',N'SECURITY',N'6',N'0',N'0'
UNION ALL SELECT N'58',N'អ្នកប្រើ',N'USER',N'0',N'57',N'2'
UNION ALL SELECT N'59',N'តួនាទី',N'PERMISION',N'0',N'57',N'2'
UNION ALL SELECT N'60',N'ទិន្នន័យ',N'DATA',N'0',N'57',N'2'
UNION ALL SELECT N'61',N'ចំលងទុក(Backup)',N'DATABACKUP',N'61',N'60',N'3'
UNION ALL SELECT N'62',N'ជំនួស(Restore)',N'DATARESTORE',N'0',N'60',N'3'
UNION ALL SELECT N'63',N'ការប្រើប្រាស់',N'USING',N'0',N'57',N'2'
UNION ALL SELECT N'64',N'ប្រវត្តិប្រតិបត្តិការណ៍',N'VIEWCHANGELOG',N'0',N'63',N'3'
UNION ALL SELECT N'66',N'របាយការណ៍',N'REPORT',N'7',N'0',N'0'
UNION ALL SELECT N'67',N'ប្រតិបត្តិការសង្ខេប',N'TRIALBALANCEDAILY',N'1',N'66',N'0'
UNION ALL SELECT N'68',N'ប្រតិបត្តិការអតិថិជនលំអិត',N'TRIALBALANCECUSTOMERDETAIL',N'2',N'66',N'0'
UNION ALL SELECT N'69',N'បំណុល និង ការបង់ប្រាក់លំអិត',N'TRIALBALANCEARDETAIL',N'3',N'66',N'0'
UNION ALL SELECT N'70',N'ប្រតិបត្តិការប្រាក់កក់លំអិត',N'TRIALBALANCEDEPOSITDETAIL',N'3',N'66',N'0'
UNION ALL SELECT N'71',N'បំណុលតាមកាល - Aging Report',N'AGING',N'71',N'66',N'0'
UNION ALL SELECT N'72',N'របាយការណ៍សង្ខេប',N'MONTHLY',N'101',N'66',N'0'
UNION ALL SELECT N'73',N'ចំណូលអគ្គីសនី',N'POWERINCOME',N'2',N'72',N'0'
UNION ALL SELECT N'74',N'ចំណូលផ្សេងៗ',N'OTHERREVENUE',N'3',N'72',N'0'
UNION ALL SELECT N'78',N'ទូទាត់ថ្លៃសេវាកម្ម',N'FIXCHARGE',N'0',N'20',N'4'
UNION ALL SELECT N'79',N'បន្ថែម',N'INSERT',N'0',N'20',N'4'
UNION ALL SELECT N'80',N'កែប្រែ',N'UPDATE',N'0',N'20',N'4'
UNION ALL SELECT N'81',N'ឈប់ប្រើប្រាស់',N'CLOSE',N'0',N'20',N'4'
UNION ALL SELECT N'82',N'កែប្រាក់កក់',N'ADJUSTDESPOSIT',N'83',N'80',N'5'
UNION ALL SELECT N'83',N'ប្តូរកុងទ័រ',N'CHANGEMETER',N'0',N'80',N'5'
UNION ALL SELECT N'84',N'ប្តូរឌីសុងទ័រ',N'CHANGECIRCUITBREAKER',N'0',N'80',N'5'
UNION ALL SELECT N'85',N'ប្តូរប្រអប់',N'CHANGEBOX',N'0',N'80',N'5'
UNION ALL SELECT N'86',N'កែសំរូលការប្រើប្រាស់',N'ADJUSTUSAGE',N'0',N'80',N'5'
UNION ALL SELECT N'87',N'កែសំរូលលើបំណុល',N'ADJUSTAMOUNT',N'0',N'80',N'5'
UNION ALL SELECT N'88',N'បោះពុម្ព',N'PRINT',N'0',N'80',N'5'
UNION ALL SELECT N'89',N'មើលព័ត៌មានអតិថិជន',N'VIEWCUSTOMER',N'0',N'20',N'4'
UNION ALL SELECT N'90',N'ស្រង់តាម IR Reader',N'IRREADER',N'5',N'9',N'0'
UNION ALL SELECT N'91',N'ទាញយកអំណាន',N'COLLECTUSAGE',N'91',N'90',N'0'
UNION ALL SELECT N'92',N'ចុះបញ្ចីកុងទ័រឌីជីថល',N'REGISTERMETER',N'92',N'90',N'0'
UNION ALL SELECT N'93',N'កែប្រែការកក់ប្រាក់',N'ADUSTDEPOSIT',N'0',N'80',N'5'
UNION ALL SELECT N'96',N'ប្រតិបត្តិការសាច់ប្រាក់សង្ខេប',N'CASHINGSUMARY',N'72',N'66',N'0'
UNION ALL SELECT N'97',N'ប្រតិបត្តិការសាច់ប្រាក់លំអិត',N'CASHINGDETAIL',N'73',N'66',N'0'
UNION ALL SELECT N'99',N'បញ្ជីអតិថិជន',N'CUSTOMERS',N'7',N'72',N'0'
UNION ALL SELECT N'101',N'ប្រើប្រាស់មិនគិតថ្លៃ',N'DISCOUNT',N'5',N'72',N'0'
UNION ALL SELECT N'102',N'លុបការបង់ប្រាក់',N'CANCELPAYMENT',N'3',N'9',N'0'
UNION ALL SELECT N'103',N'គ្រប់គ្រង់អតិថិជន',N'REGISTER',N'2',N'8',N'0'
UNION ALL SELECT N'104',N'ទូទាត់ថ្លៃសេវាកម្ម',N'FIXCHARGE',N'0',N'103',N'3'
UNION ALL SELECT N'105',N'បន្ថែម',N'INSERT',N'0',N'103',N'3'
UNION ALL SELECT N'106',N'កែប្រែ',N'UPDATE',N'0',N'103',N'3'
UNION ALL SELECT N'107',N'ប្តូរកុងទ័រ',N'CHANGEMETER',N'0',N'106',N'0'
UNION ALL SELECT N'108',N'ប្តូរឌីសុងទ័រ',N'CHANGEBREAKER',N'0',N'106',N'4'
UNION ALL SELECT N'109',N'ប្តូរប្រអប់',N'CHANGEBOX',N'0',N'106',N'4'
UNION ALL SELECT N'110',N'កែប្រាក់កក់',N'ADJUSTDESPOSIT',N'0',N'106',N'4'
UNION ALL SELECT N'111',N'ឈប់ប្រើប្រាស់',N'CLOSE',N'0',N'103',N'3'
UNION ALL SELECT N'112',N'មើលព័ត៌មានអតិថិជន',N'VIEWCUSTOMER',N'0',N'103',N'3'
UNION ALL SELECT N'113',N'ចាប់ផ្តើមប្រើ',N'ACTIVATE',N'0',N'103',N'3'
UNION ALL SELECT N'114',N'ប្រតិបត្តិការណប្រចាំថ្ងៃ',N'DIALYOPERATION',N'1',N'8',N'0'
UNION ALL SELECT N'115',N'លក់អគ្គិសនី',N'SALEPOWER',N'1',N'114',N'0'
UNION ALL SELECT N'116',N'លុបការលក់អគ្គិសនី',N'VOIDPOWER',N'2',N'114',N'0'
UNION ALL SELECT N'117',N'បង្កើតកាតទិញអគ្គិសនីថ្មី',N'RENEWCARD',N'3',N'114',N'0'
UNION ALL SELECT N'118',N'បង្កើតកាតពិសេស',N'CREATESPECIALCARD',N'4',N'114',N'0'
UNION ALL SELECT N'120',N'កែសំរួលលើបំណុល',N'ADJUSTAMOUNT',N'0',N'106',N'4'
UNION ALL SELECT N'121',N'បោះពុម្ព',N'PRINT',N'0',N'106',N'4'
UNION ALL SELECT N'122',N'ការប្រើប្រាស់ថាមពល',N'USAGE',N'4',N'72',N'3'
UNION ALL SELECT N'123',N'របាយការណ៏បង់ប្រាក់មុន',N'PREPAID',N'3',N'8',N'0'
UNION ALL SELECT N'124',N'ចំណូលពីការលក់អគ្គិសនី',N'SALEPOWERDETAIL',N'0',N'123',N'3'
UNION ALL SELECT N'125',N'ចំណូលពីការលក់អគ្គិសនីសង្ខេប',N'SALEPOWERSUMMARY',N'0',N'123',N'3'
UNION ALL SELECT N'126',N'ត្រង់ស្វូ',N'TRANSFORMER',N'2',N'34',N'2'
UNION ALL SELECT N'127',N'ការផលិតថាមពល',N'POWER',N'6',N'72',N'0'
UNION ALL SELECT N'128',N'ទូទាត់តាមធនាគារ',N'BANKPAYMENT',N'12',N'9',N'0'
UNION ALL SELECT N'129',N'ការទូរទាត់',N'SETTLEMENT',N'11',N'128',N'0'
UNION ALL SELECT N'130',N'សង្ខេបការទូទាត់',N'SUMMARY',N'12',N'128',N'0'
UNION ALL SELECT N'131',N'បោះពុម្ភបង្កាន់ដៃ',N'REPRINTRECIEPT',N'2',N'9',N'0'
UNION ALL SELECT N'132',N'គ្រប់គ្រងថាមពល',N'POWER',N'5',N'0',N'1'
UNION ALL SELECT N'134',N'ខ្សែបណ្តាញចែកចាយ',N'DISTRIBUTION_FACILITY',N'2',N'138',N'0'
UNION ALL SELECT N'135',N'ប្រភពទិញថាមពល',N'AGREEMENT',N'1',N'139',N'0'
UNION ALL SELECT N'136',N'ការទិញថាមពល',N'POWERAGREEMENT',N'2',N'139',N'0'
UNION ALL SELECT N'137',N'កំលាំង(kVA)',N'CAPACITY',N'0',N'48',N'3'
UNION ALL SELECT N'138',N'ការចែកចាយថាមពល',N'DISTRIBUTION',N'1',N'132',N'0'
UNION ALL SELECT N'139',N'ការទិញថាមពល',N'PURCHASEPOWER',N'2',N'132',N'0'
UNION ALL SELECT N'140',N'ការប្រើប្រាស់ថាមពល',N'USEPOWER',N'4',N'132',N'0'
UNION ALL SELECT N'141',N'ការកំណត់ថាមពល',N'POWER',N'0',N'140',N'0'
UNION ALL SELECT N'142',N'ថាមពលតាមត្រង់ស្វូ',N'POWERBYTRANSOFRMAER',N'142',N'140',N'0'
UNION ALL SELECT N'143',N'បញ្ចូលព៌តមានថ្មីៗ  ',N'SENDDATATOIR',N'0',N'90',N'0'
UNION ALL SELECT N'149',N'អំណានចាស់',N'STARTUSAGE',N'1',N'11',N'3'
UNION ALL SELECT N'150',N'អំណានថ្មី',N'ENDUSAGE',N'2',N'11',N'3'
UNION ALL SELECT N'151',N'ផ្នែកគណនេយ្យ',N'ACC',N'5',N'0',N'0'
UNION ALL SELECT N'152',N'ប្រតិបត្តិការ',N'TRANS',N'1',N'151',N'0'
UNION ALL SELECT N'153',N'ប្រតិបត្តិការចំណូល',N'INCOME',N'1',N'152',N'0'
UNION ALL SELECT N'154',N'ប្រតិបត្តិការចំណាយ',N'EXPENSE',N'2',N'152',N'0'
UNION ALL SELECT N'155',N'អត្រាប្តូរប្រាក់',N'EXCHANGERATE',N'5',N'152',N'0'
UNION ALL SELECT N'156',N'ការកំណត់គណនី',N'CHART',N'2',N'151',N'0'
UNION ALL SELECT N'157',N'គណនី',N'ACCOUNTCHART',N'1',N'156',N'0'
UNION ALL SELECT N'158',N'ការកំណត់',N'CONFIG',N'2',N'156',N'0'
UNION ALL SELECT N'159',N'អចលនទ្រព្យ',N'FIXASSET',N'3',N'151',N'0'
UNION ALL SELECT N'160',N'ប្រភេទអចលនទ្រព្យ',N'CATEGORY',N'1',N'159',N'0'
UNION ALL SELECT N'161',N'អចលនទ្រព្យ',N'ITEM',N'2',N'159',N'0'
UNION ALL SELECT N'162',N'របាយការណ៍',N'REPORT',N'4',N'151',N'0'
UNION ALL SELECT N'163',N'ប្រតិបត្តិការចំណូល',N'INCOME',N'1',N'162',N'0'
UNION ALL SELECT N'164',N'ប្រតិបត្តិការចំណាយ',N'EXPENSE',N'2',N'162',N'0'
UNION ALL SELECT N'165',N'របាយការណ៍ចំណេញខាត',N'PL',N'3',N'162',N'0'
UNION ALL SELECT N'166',N'របាយការណ៏អចលនទ្រព្យ',N'FIXASSET_SUMMARY',N'4',N'162',N'0'
UNION ALL SELECT N'167',N'របាយការណ៏រំលស់អចលនទ្រព្យ',N'DEPRECIATION',N'5',N'162',N'0'
UNION ALL SELECT N'168',N'ការទទួលប្រាក់',N'CASH_RECEIVE',N'1',N'72',N'0'
UNION ALL SELECT N'169',N'ការប្រើប្រាស់ថាមពល',N'POWERUSAGE',N'8',N'72',N'0'
UNION ALL SELECT N'170',N'ប្រវត្តិការប្រើប្រាស់ថាមពល',N'USAGESUMMARY',N'8',N'72',N'0'
UNION ALL SELECT N'171',N'សេវាទូទាត់ជាប្រចាំ',N'RECURRINGSERVICE',N'9',N'72',N'0'
UNION ALL SELECT N'172',N'ដាក់ប្រាក់ចូលធនាគារ',N'BANKDEPOSIT',N'10',N'72',N'0'
UNION ALL SELECT N'173',N'របាយការណ៍ត្រីមាស',N'QUATERREPORT',N'11',N'72',N'0'
UNION ALL SELECT N'174',N'ភូមិឃុំ ត្រូវចែកចាយ',N'LICENSEVILLAGE',N'1',N'138',N'0'
UNION ALL SELECT N'175',N'អតិថិជនជាអ្នកកាន់អាជ្ញាប័ណ្ណ',N'LICENSEE',N'3',N'138',N'0'
UNION ALL SELECT N'176',N'ការផលិតថាមពល',N'PRODUCTION',N'3',N'132',N'0'
UNION ALL SELECT N'177',N'ម៉ាស៊ីនភ្លើង',N'GENERATOR',N'1',N'176',N'0'
UNION ALL SELECT N'178',N'ការផលិតថាមពល',N'GENERATION',N'2',N'176',N'0'
UNION ALL SELECT N'179',N'របាយការណ៍',N'REPORT',N'6',N'132',N'0'
UNION ALL SELECT N'180',N'ស្ថិតិចំនួនអតិថិជន',N'CUSTOMERSTATISTIC',N'1',N'179',N'0'
UNION ALL SELECT N'181',N'ចំនួនកុងទ័រតាមត្រង់ស្វូ',N'METERBYTRANSFORMER',N'2',N'179',N'0'
UNION ALL SELECT N'182',N'កែប្រែពត៌មានអតិថិជន',N'CUSTOMERBATCHCHANGE',N'2',N'54',N'0'
UNION ALL SELECT N'183',N'ប្រតិបត្តការធនាគារ',N'BANK',N'13',N'34',N'0'
UNION ALL SELECT N'184',N'ដាក់ប្រាក់',N'DEPOSIT',N'1',N'183',N'0'
UNION ALL SELECT N'185',N'ស្តុក',N'STOCK',N'14',N'34',N'0'
UNION ALL SELECT N'186',N'គ្រប់គ្រងទំនិញ',N'ITEM',N'1',N'185',N'0'
UNION ALL SELECT N'187',N'គ្រប់គ្រងស្តុក',N'MGT',N'2',N'185',N'0'
UNION ALL SELECT N'188',N'ស្រង់តាម GPRS',N'GPRS',N'6',N'9',N'0'
UNION ALL SELECT N'189',N'ស្រង់អំណាន',N'USE',N'1',N'188',N'0'
UNION ALL SELECT N'190',N'អតិថិជនមិនបានទិញថាមពលអគ្គិសនី',N'CUSTOMER_NOT_BUY',N'3',N'123',N'0'
UNION ALL SELECT N'191',N'ស្ថិតិការទិញថាមពលអគ្គិសនី',N'BUYSTATISITIC',N'4',N'123',N'0'
UNION ALL SELECT N'192',N'លុបការទិញថាមពលអគិ្គសនី',N'VOIDPOWER',N'5',N'123',N'0'
UNION ALL SELECT N'193',N'ប្រតិបត្តិការផ្តាច់និងភ្ជាប់ចរន្ត',N'BLOCKANDRECONNECT',N'4',N'66',N'0'
UNION ALL SELECT N'194',N'ពិនិត្យព័ត៌មានលើកុងទ័រ',N'READDATA',N'5',N'114',N'0'
UNION ALL SELECT N'195',N'របាយការណ៍ប្រចាំឆ្នាំ',N'ANNUALREPORT',N'12',N'72',N'0'
UNION ALL SELECT N'196',N'ចំណាយបុគ្គលិក',N'STAFFEXPENSE',N'3',N'152',N'0'
UNION ALL SELECT N'197',N'ប្រាក់កម្ចី',N'LOAN',N'4',N'152',N'0'
UNION ALL SELECT N'198',N'ប្រភពទិញប្រេង',N'FUELSOURCE',N'3',N'176',N'0'
UNION ALL SELECT N'199',N'ការទិញប្រេង',N'FUELPURCHASE',N'4',N'176',N'0'
SET IDENTITY_INSERT TBL_PERMISSION OFF;
GO
DELETE FROM TBL_ROLE_PERMISSION WHERE ROLE_ID = 1
GO
INSERT INTO TBL_ROLE_PERMISSION 
SELECT	ROLE_ID=1,
		PERMISSION_ID,
		IS_ALLOWED=1
FROM TBL_PERMISSION;  
GO
-----------------------------------------------------------------------------
-- UPDATE ACCOUNT CHART - TAX 
-----------------------------------------------------------------------------
UPDATE TBL_ACCOUNT_CHART SET ACCOUNT_NAME=N'ពន្ធលើប្រេងឥន្ធនៈ' WHERE ACCOUNT_CODE='6210'
UPDATE TBL_ACCOUNT_CHART SET ACCOUNT_NAME=N'ពន្ធលើផលរបរ' WHERE ACCOUNT_CODE='6220'
UPDATE TBL_ACCOUNT_CHART SET ACCOUNT_NAME=N'ពន្ធប៉ាតង់' WHERE ACCOUNT_CODE='6230'
UPDATE TBL_ACCOUNT_CHART SET ACCOUNT_NAME=N'ពន្ធនាំចូល' WHERE ACCOUNT_CODE='6240'
UPDATE TBL_ACCOUNT_CHART SET ACCOUNT_NAME=N'ពន្ធលើមធ្យោបាយដឹកជញ្ជូន' WHERE ACCOUNT_CODE='6250'
UPDATE TBL_ACCOUNT_CHART SET ACCOUNT_NAME=N'ពន្ធផ្សេងៗទៀត' WHERE ACCOUNT_CODE='6290';

GO
-----------------------------------------------------------------------------
-- ANNUAL REPORT - TABLE DEFINITION
-----------------------------------------------------------------------------
IF OBJECT_ID('TBL_LOOKUP') IS NULL
CREATE TABLE TBL_LOOKUP(
	LOOKUP_ID INT PRIMARY KEY,
	LOOKUP_NAME NVARCHAR(100) NOT NULL,
	IS_ACTIVE BIT NOT NULL
);
GO
IF OBJECT_ID('TBL_LOOKUP_VALUE') IS NULL
CREATE TABLE TBL_LOOKUP_VALUE(
	VALUE_ID INT PRIMARY KEY,
	LOOKUP_ID INT NOT NULL,
	VALUE_NAME NVARCHAR(50) NOT NULL,
	VALUE_TEXT NVARCHAR(MAX) NOT NULL,
	IS_SYSTEM BIT NOT NULL,
	IS_ACTIVE BIT NOT NULL
);
GO
IF NOT EXISTS(SELECT * FROM dbo.TBL_LOOKUP)
INSERT INTO dbo.TBL_LOOKUP
SELECT 100,'TRANSFORMER_TYPE',1
UNION ALL SELECT 101,'TRANSFORMER_POSITION',1
UNION ALL SELECT 102,'BUSINESS_DIVISION',1
GO
IF NOT EXISTS(SELECT * FROM dbo.TBL_LOOKUP_VALUE)
INSERT INTO dbo.TBL_LOOKUP_VALUE
SELECT 10001,100,'DECREASE',N'ទម្លាក់',1,1
UNION ALL SELECT 10002,100,'INCREASE',N'តម្លើង',1,1
UNION ALL SELECT 10101,101,'OUTDOOR',N'លើបង្គោល',1,1
UNION ALL SELECT 10102,101,'INDOOR',N'ក្នុងបន្ទប់ភ្លើង',1,1
UNION ALL SELECT 10201,102,'PRODUCTION',N'ផ្នែកផលិតកម្ម',1,1
UNION ALL SELECT 10202,102,'TRANSMISSION',N'ផ្នែកបញ្ជូន',1,1
UNION ALL SELECT 10203,102,'DISTRIBUTION',N'ផ្នែកចែកចាយ',1,1
UNION ALL SELECT 10204,102,'ADMINISTRATION',N'ផ្នែករដ្ឋបាល',1,1
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = 'ANR')
BEGIN
	EXEC sp_executesql N'CREATE SCHEMA ANR;'
END
GO
IF OBJECT_ID('[ANR].[TBL_REPORT_ITEM]') IS NULL
CREATE TABLE [ANR].[TBL_REPORT_ITEM](
	[ITEM_ID] [int] IDENTITY(1,1) NOT NULL,
	[TABLE_ID] [int] NOT NULL,
	[ITEM_CODE] [nvarchar](50) COLLATE Latin1_General_BIN NOT NULL,
	[ITEM_NAME] [nvarchar](100) COLLATE Latin1_General_BIN NOT NULL,
	[ACCOUNT_CODE] [nvarchar](100) COLLATE Latin1_General_BIN NOT NULL,
	[PARENT_ID] [int] NOT NULL,
	[IS_ACTIVE] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ITEM_ID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/*
SELECT '''N''''''+'+ 'CONVERT(NVARCHAR(MAX),'+COLUMN_NAME+')+'''''',''+'
FROM INFORMATION_SCHEMA.COLUMNS 
WHERE TABLE_NAME='TBL_REPORT_ITEM'

SELECT ' UNION ALL SELECT '+
'N'''+CONVERT(NVARCHAR(MAX),ITEM_ID)+''','+
'N'''+CONVERT(NVARCHAR(MAX),TABLE_ID)+''','+
'N'''+CONVERT(NVARCHAR(MAX),ITEM_CODE)+''','+
'N'''+CONVERT(NVARCHAR(MAX),ITEM_NAME)+''','+
'N'''+CONVERT(NVARCHAR(MAX),ACCOUNT_CODE)+''','+
'N'''+CONVERT(NVARCHAR(MAX),PARENT_ID)+''','+
'N'''+CONVERT(NVARCHAR(MAX),IS_ACTIVE)+''''
FROM ANR.TBL_REPORT_ITEM
*/

IF NOT EXISTS(SELECT * FROM ANR.TBL_REPORT_ITEM)
BEGIN
	SET IDENTITY_INSERT ANR.TBL_REPORT_ITEM ON;
	INSERT INTO ANR.TBL_REPORT_ITEM(ITEM_ID,TABLE_ID,ITEM_CODE,ITEM_NAME,ACCOUNT_CODE,PARENT_ID,IS_ACTIVE)
	SELECT N'16',N'4',N'01',N'គ្រឿងបន្លាស់',N'5240/5435/5635',N'0',N'1'
 UNION ALL SELECT N'17',N'4',N'02',N'សម្ភារៈ និងការផ្គត់ផ្គង់',N'5250/5450/5645',N'0',N'1'
 UNION ALL SELECT N'18',N'4',N'03',N'ជួសជុល និងថែទាំ',N'5230/5440/5640',N'0',N'1'
 UNION ALL SELECT N'19',N'4',N'04',N'ជួល',N'5220/5460/5680',N'0',N'1'
 UNION ALL SELECT N'20',N'4',N'05',N'ទឹក',N'5130',N'0',N'1'
 UNION ALL SELECT N'21',N'4',N'06',N'បំណុលទារមិនបាន',N'5840',N'0',N'1'
 UNION ALL SELECT N'22',N'4',N'07',N'ចំណាយគ្រប់គ្រង និងវិស្វកម្ម',N'5200/5410/5610',N'0',N'1'
 UNION ALL SELECT N'23',N'4',N'08',N'ធានារ៉ាប់រង',N'5210',N'0',N'1'
 UNION ALL SELECT N'24',N'4',N'09',N'ចំណាយផ្នែកបែងចែកបន្ទុក',N'5420/5620',N'0',N'1'
 UNION ALL SELECT N'25',N'4',N'10',N'ចំណាយផ្នែកបន្ទប់ភ្លើង',N'5430/5630',N'0',N'1'
 UNION ALL SELECT N'26',N'4',N'11',N'ចំណាយបំភ្លឺសាធារណៈ និងប្រព័ន្ធឱ្យសញ្ញាផ្សេងៗ',N'5650',N'0',N'1'
 UNION ALL SELECT N'27',N'4',N'12',N'ចំណាយលើការតម្លើងជូនអតិថិជន',N'5670',N'0',N'1'
 UNION ALL SELECT N'28',N'4',N'13',N'ចំណាយលើការថែទាំនាឡិការស្ទង់',N'5660',N'0',N'1'
 UNION ALL SELECT N'29',N'4',N'13',N'ចំណាយលើការថែទាំនាឡិការស្ទង់',N'5660',N'0',N'1'
 UNION ALL SELECT N'30',N'4',N'14',N'ចំណាយផ្សេងៗ',N'5290/5490/5690',N'0',N'1'
 UNION ALL SELECT N'31',N'5',N'01',N'សេវាធនាគារ',N'6450',N'0',N'1'
 UNION ALL SELECT N'32',N'5',N'02',N'កម្រៃអាជ្ញាប័ណ្ណ',N'6360',N'0',N'1'
 UNION ALL SELECT N'33',N'5',N'03',N'សេវាសនកម្ម និងច្បាប់',N'6350',N'0',N'1'
 UNION ALL SELECT N'34',N'5',N'04',N'ការដឹកជញ្ជូន និងការធ្វើដំណើរក្នុងស្រុក',N'6130',N'0',N'1'
 UNION ALL SELECT N'35',N'5',N'05',N'ចំណាយលើប្រេងឥន្ធនៈបុគ្គលិក',N'6140',N'0',N'1'
 UNION ALL SELECT N'36',N'5',N'06',N'ចំណាយលើបេសកកម្មក្រៅប្រទេស',N'6110',N'0',N'1'
 UNION ALL SELECT N'37',N'5',N'07',N'ចំណាយលើបេសកកម្ម និងការទទួលភ្ញៀវ',N'6120',N'0',N'1'
 UNION ALL SELECT N'38',N'5',N'08',N'សេវាប្រៃសណីយ៍ និងទូរគមនាគមន៍',N'6160',N'0',N'1'
 UNION ALL SELECT N'39',N'5',N'09',N'ចំណាយសម្ភារៈការិយាល័យទូទៅ',N'6150',N'0',N'1'
 UNION ALL SELECT N'40',N'5',N'10',N'បណ្តុះបណ្តាល និងអភិវឌ្ឍន៍បុគ្គលិក',N'6100',N'0',N'1'
 UNION ALL SELECT N'41',N'5',N'11',N'ចំណាយលើបុគ្គលិកបណ្តែត',N'6030',N'0',N'1'
 UNION ALL SELECT N'42',N'5',N'12',N'កម្រៃជើងសារ',N'5850',N'0',N'1'
 UNION ALL SELECT N'43',N'5',N'13',N'ជួល',N'6300',N'0',N'1'
 UNION ALL SELECT N'44',N'5',N'14',N'ទឹក-ភ្លើង',N'6310',N'0',N'1'
 UNION ALL SELECT N'45',N'5',N'15',N'ជួសជុល និងថែទាំ',N'6320',N'0',N'1'
 UNION ALL SELECT N'46',N'5',N'16',N'ចំណាយលើការគ្រប់គ្រង',N'6370',N'0',N'1'
 UNION ALL SELECT N'47',N'5',N'17',N'ធានារ៉ាប់រង',N'6400',N'0',N'1'
 UNION ALL SELECT N'48',N'5',N'18',N'សំណងលើការខូចខាត និងគ្រោះថ្នាក់ការងារ',N'6410',N'0',N'1'
 UNION ALL SELECT N'49',N'5',N'19',N'ពិធីជប់លៀង និងការសំដែង',N'6500',N'0',N'1'
 UNION ALL SELECT N'50',N'5',N'20',N'អំណោយ និងវិភាគទាន',N'6510',N'0',N'1'
 UNION ALL SELECT N'51',N'5',N'20',N'ពន្ធ',N'6200',N'0',N'1'
 UNION ALL SELECT N'52',N'5',N'20',N'ពន្ធលើប្រេងឥន្ធនៈ',N'6210',N'51',N'1'
 UNION ALL SELECT N'53',N'5',N'20',N'ពន្ធលើផលរបរ',N'6220',N'51',N'1'
 UNION ALL SELECT N'54',N'5',N'20',N'ពន្ធប៉ាតង់',N'6230',N'51',N'1'
 UNION ALL SELECT N'55',N'5',N'20',N'ពន្ធនាំចូល',N'6240',N'51',N'1'
 UNION ALL SELECT N'56',N'5',N'20',N'ពន្ធលើមធ្យោបាយដឹកជញ្ជូន',N'6250',N'51',N'1'
 UNION ALL SELECT N'57',N'5',N'20',N'ពន្ធផ្សេងៗទៀត',N'6290',N'51',N'1'
 UNION ALL SELECT N'58',N'5',N'21',N'ចំណាយទូទៅផ្សេងៗ',N'6590',N'0',N'1'
 UNION ALL SELECT N'60',N'10',N'ក',N'ចំណាយផ្នែកផលិតកម្ម',N'',N'0',N'1'
 UNION ALL SELECT N'61',N'10',N'1',N'ចំណាយប្រេងប្រេងឥន្ធនៈ',N'AS1-5110',N'0',N'1'
 UNION ALL SELECT N'62',N'10',N'2',N'ចំណាយប្រេងរំអិល',N'AS1-5120',N'0',N'1'
 UNION ALL SELECT N'63',N'10',N'3',N'ចំណាយលើបុគ្គលិកផ្នែកផលិតកម្ម',N'AS3-6000',N'0',N'1'
 UNION ALL SELECT N'64',N'10',N'4',N'ចំណាយដំណើរការនិងការថែទាំ',N'AS4-5100',N'0',N'1'
 UNION ALL SELECT N'65',N'10',N'5',N'ចំណាយរដ្ឋបាលនិងការគ្រប់គ្រងទូទៅ',N'AS5-0001',N'0',N'1'
 UNION ALL SELECT N'66',N'10',N'6',N'ចំណាយសម្រាប់រំលស់',N'AS8-5920',N'0',N'1'
 UNION ALL SELECT N'67',N'10',N'7',N'ការប្រាក់',N'AS6-7000/7010/7030',N'0',N'1'
 UNION ALL SELECT N'68',N'10',N'8',N'ប្រាក់ចំណេញសមស្រប',N'AS9-0001',N'0',N'1'
 UNION ALL SELECT N'69',N'10',N'',N'ចំណាយត្រឹមត្រូវសរុបសំរាប់ផ្នែកផលិតកម្ម',N'',N'0',N'1'
 UNION ALL SELECT N'70',N'10',N'9',N'ថាមពលបញ្ជូនគិតជាគីឡូវ៉ាត់ម៉ោង',N'',N'0',N'1'
 UNION ALL SELECT N'71',N'10',N'10',N'ថ្លៃដើមផលិតក្នុង១គីឡូវ៉ាត់ម៉ោង',N'',N'0',N'1'
 UNION ALL SELECT N'72',N'10',N'ខ',N'ចំណាយផ្នែកបញ្ជូន',N'',N'0',N'1'
 UNION ALL SELECT N'73',N'10',N'1',N'ចំណាយទិញថាមពល',N'AS2-5000',N'0',N'1'
 UNION ALL SELECT N'74',N'10',N'2',N'ចំណាយលើបុគ្គលិកផ្នែកបញ្ជូន',N'AS3-6000',N'0',N'1'
 UNION ALL SELECT N'75',N'10',N'3',N'ចំណាយដំណើរការនិងការថែទាំ',N'AS4-5400',N'0',N'1'
 UNION ALL SELECT N'76',N'10',N'4',N'ចំណាយរដ្ឋបាលនិងការគ្រប់គ្រងទូទៅ',N'AS5-6000',N'0',N'1'
 UNION ALL SELECT N'77',N'10',N'5',N'ចំណាយសម្រាប់រំលស់',N'AS8-5940',N'0',N'1'
 UNION ALL SELECT N'78',N'10',N'6',N'ការប្រាក់',N'AS6-7000/7010/7030',N'0',N'1'
 UNION ALL SELECT N'79',N'10',N'7',N'ប្រាក់ចំណេញសមស្រប',N'AS9-0002',N'0',N'1'
 UNION ALL SELECT N'80',N'10',N'',N'ចំណាយត្រឹមត្រូវសរុបសំរាប់ផ្នែកបញ្ជូន',N'',N'0',N'1'
 UNION ALL SELECT N'81',N'10',N'គ',N'ចំណាយផ្នែកចែកចាយ',N'',N'0',N'1'
 UNION ALL SELECT N'82',N'10',N'1',N'ចំណាយទិញថាមពល',N'AS2-5000',N'0',N'1'
 UNION ALL SELECT N'83',N'10',N'2',N'ចំណាយលើបុគ្គលិកផ្នែកចែកចាយ',N'AS3-6000',N'0',N'1'
 UNION ALL SELECT N'84',N'10',N'3',N'ចំណាយដំណើរការនិងការថែទាំ',N'AS4-5600',N'0',N'1'
 UNION ALL SELECT N'85',N'10',N'4',N'ចំណាយរដ្ឋបាលនិងការគ្រប់គ្រងទូទៅ',N'AS5-6000',N'0',N'1'
 UNION ALL SELECT N'86',N'10',N'5',N'ចំណាយសម្រាប់រំលស់',N'AS8-5910/5960/5980',N'0',N'1'
 UNION ALL SELECT N'87',N'10',N'6',N'ការប្រាក់',N'AS6-7000/7010/7030',N'0',N'1'
 UNION ALL SELECT N'88',N'10',N'7',N'ប្រាក់ចំណេញសមស្រប',N'AS9-0003',N'0',N'1'
 UNION ALL SELECT N'89',N'10',N'',N'ចំណាយត្រឹមត្រូវសរុបសំរាប់ផ្នែកចែកចាយ',N'',N'0',N'1'
 UNION ALL SELECT N'90',N'10',N'',N'ចំណាយសរុប (ផលិតកម្ម ៎ បញ្ជូន ៎ ចែកចាយ)',N'',N'0',N'1'
 UNION ALL SELECT N'91',N'10',N'ឃ',N'ដកចំណូលផ្សេងក្រៅពីប្រភពថ្លៃលក់អគ្គិសនី(លើកលែងប្រាក់កក់អតិថិជន)',N'AS12-4200/4300/4400',N'0',N'1'
 UNION ALL SELECT N'92',N'10',N'ង',N'ចំណូលសរុបប្រចាំឆ្នាំដែលត្រូវមាន (ក+ខ៎គ-ឃ)',N'',N'0',N'1'
 UNION ALL SELECT N'93',N'10',N'ច',N'ចំណូលសរុបដែលបានមកពីថ្លៃលក់បច្ចុប្បន្ន',N'',N'0',N'1'
 UNION ALL SELECT N'94',N'10',N'ឆ',N'ចំណេញ/(ខាត) មុនកែសម្រួលថ្លៃលក់ (ច-ង)',N'',N'0',N'1'
 UNION ALL SELECT N'95',N'10',N'ជ',N'ផលប៉ះពាល់លើការកែសម្រួលថ្លៃលក់',N'',N'0',N'1'
 UNION ALL SELECT N'96',N'10',N'ឈ',N'ចំណេញ/(ខាត) ក្រោយកែសម្រួលថ្លៃលក់ (ច+ជ-ង)',N'',N'0',N'1'
 UNION ALL SELECT N'134',N'9',N'',N'អគារ',N'',N'0',N'1'
 UNION ALL SELECT N'135',N'9',N'1',N'តម្លៃដើមសរុបនៃអចលនទ្រព្យ (ដកវិភាគទានអ្នកប្រើប្រាស់និងជំនួយ)',N'AS7-1015',N'0',N'1'
 UNION ALL SELECT N'136',N'9',N'2',N'សំវិធានធនសម្រាប់ទុនចល័ត',N'',N'0',N'1'
 UNION ALL SELECT N'137',N'9',N'3',N'តម្លៃអចលនទ្រព្យសរុប',N'',N'0',N'1'
 UNION ALL SELECT N'138',N'9',N'0',N'',N'',N'0',N'1'
 UNION ALL SELECT N'139',N'9',N'4',N'ដក រំលស់បូកយោង',N'AS8-1105',N'0',N'1'
 UNION ALL SELECT N'140',N'9',N'5',N'តម្លៃអចលនទ្រព្យសុទ្ធ',N'',N'0',N'1'
 UNION ALL SELECT N'141',N'9',N'0',N'',N'',N'0',N'1'
 UNION ALL SELECT N'142',N'9',N'6',N'ប្រាក់កម្ចីសំរាប់អចលនទ្រព្យកំពុងប្រើប្រាស់',N'AS6-0001',N'0',N'1'
 UNION ALL SELECT N'143',N'9',N'7',N'មូលធនជាក៉ស្តែងផ្ទាល់ 9៥ - ៦ប',N'',N'0',N'1'
 UNION ALL SELECT N'144',N'9',N'0',N'',N'',N'0',N'1'
 UNION ALL SELECT N'145',N'9',N'0',N'អាជីវកម្មផ្នែកផលិតកម្ម',N'',N'0',N'1'
 UNION ALL SELECT N'146',N'9',N'1',N'តម្លៃដើមសរុបនៃអចលនទ្រព្យ (ដកវិភាគទានអ្នកប្រើប្រាស់និងជំនួយ)',N'AS7-1020',N'0',N'1'
 UNION ALL SELECT N'147',N'9',N'2',N'សំវិធានធនសម្រាប់ទុនចល័ត',N'',N'0',N'1'
 UNION ALL SELECT N'148',N'9',N'3',N'តម្លៃអចលនទ្រព្យសរុប',N'',N'0',N'1'
 UNION ALL SELECT N'149',N'9',N'0',N'',N'',N'0',N'1'
 UNION ALL SELECT N'150',N'9',N'4',N'ដក រំលស់បូកយោង',N'AS8-1110',N'0',N'1'
 UNION ALL SELECT N'151',N'9',N'5',N'តម្លៃអចលនទ្រព្យសុទ្ធ',N'',N'0',N'1'
 UNION ALL SELECT N'152',N'9',N'0',N'',N'',N'0',N'1'
 UNION ALL SELECT N'153',N'9',N'6',N'ប្រាក់កម្ចីសំរាប់អចលនទ្រព្យកំពុងប្រើប្រាស់',N'AS6-0002',N'0',N'1'
 UNION ALL SELECT N'154',N'9',N'7',N'មូលធនជាក៉ស្តែងផ្ទាល់ 9៥ - ៦ប',N'',N'0',N'1'
 UNION ALL SELECT N'155',N'9',N'0',N'',N'',N'0',N'1'
 UNION ALL SELECT N'156',N'9',N'8',N'អត្រាប្រាក់ចំណេញលើមូលធនជាក់ស្តែងផ្ទាល់',N'',N'0',N'1'
 UNION ALL SELECT N'157',N'9',N'9',N'ប្រាក់ចំណេញលើមូលធនជាក់ស្តែងផ្ទាល់ តាមអត្រាខាងលើ',N'',N'0',N'1'
 UNION ALL SELECT N'158',N'9',N'10',N'ប្រាក់ចំណេញសមស្រប',N'',N'0',N'1'
 UNION ALL SELECT N'159',N'9',N'0',N'',N'',N'0',N'1'
 UNION ALL SELECT N'160',N'9',N'0',N'អាជីវកម្មផ្នែកបញ្ជូន',N'',N'0',N'1'
 UNION ALL SELECT N'161',N'9',N'1',N'តម្លៃដើមសរុបនៃអចលនទ្រព្យ (ដកវិភាគទានអ្នកប្រើប្រាស់និងជំនួយ)',N'AS7-1040',N'0',N'1'
 UNION ALL SELECT N'162',N'9',N'2',N'សំវិធានធនសំរាប់ទុនចល័ត',N'',N'0',N'1'
 UNION ALL SELECT N'163',N'9',N'3',N'តម្លៃអចលនទ្រព្យសរុប',N'',N'0',N'1'
 UNION ALL SELECT N'164',N'9',N'0',N'',N'',N'0',N'1'
 UNION ALL SELECT N'165',N'9',N'4',N'ដក រំលោះបូកយោង',N'AS8-1130',N'0',N'1'
 UNION ALL SELECT N'166',N'9',N'5',N'តម្លៃអចលនទ្រព្យសុទ្ធ',N'',N'0',N'1'
 UNION ALL SELECT N'167',N'9',N'0',N'',N'',N'0',N'1'
 UNION ALL SELECT N'168',N'9',N'6',N'ប្រាក់កម្ចីសំរាប់អចលនទ្រព្យកំពុងប្រើប្រាស់',N'AS6-0003',N'0',N'1'
 UNION ALL SELECT N'169',N'9',N'7',N'មូលធនជាក៉ស្តែងផ្ទាល់ 9៥ - ៦ប',N'',N'0',N'1'
 UNION ALL SELECT N'170',N'9',N'0',N'',N'',N'0',N'1'
 UNION ALL SELECT N'171',N'9',N'8',N'អត្រាប្រាក់ចំណេញលើមូលធនជាក់ស្តែងផ្ទាល់',N'',N'0',N'1'
 UNION ALL SELECT N'172',N'9',N'9',N'ប្រាក់ចំណេញលើមូលធនជាក់ស្តែងផ្ទាល់ តាមអត្រាខាងលើ',N'',N'0',N'1'
 UNION ALL SELECT N'173',N'9',N'10',N'ប្រាក់ចំណេញសមស្រប',N'',N'0',N'1'
 UNION ALL SELECT N'174',N'9',N'0',N'',N'',N'0',N'1'
 UNION ALL SELECT N'175',N'9',N'0',N'អាជីវកម្មផ្នែកចែកចាយ',N'',N'0',N'1'
 UNION ALL SELECT N'176',N'9',N'1',N'តម្លៃដើមសរុបនៃអចលនទ្រព្យ (ដកវិភាគទានអ្នកប្រើប្រាស់និងជំនួយ)',N'AS7-1050/1080',N'0',N'1'
 UNION ALL SELECT N'177',N'9',N'2',N'សំវិធានធនសំរាប់ទុនចល័ត',N'',N'0',N'1'
 UNION ALL SELECT N'178',N'9',N'3',N'តម្លៃអចលនទ្រព្យសរុប',N'',N'0',N'1'
 UNION ALL SELECT N'179',N'9',N'0',N'',N'',N'0',N'1'
 UNION ALL SELECT N'180',N'9',N'4',N'ដក រំលោះបូកយោង',N'AS8-1150/1170',N'0',N'1'
 UNION ALL SELECT N'181',N'9',N'5',N'តម្លៃអចលនទ្រព្យសុទ្ធ',N'',N'0',N'1'
 UNION ALL SELECT N'182',N'9',N'0',N'',N'',N'0',N'1'
 UNION ALL SELECT N'183',N'9',N'6',N'ប្រាក់កម្ចីសំរាប់អចលនទ្រព្យកំពុងប្រើប្រាស់',N'AS6-0004',N'0',N'1'
 UNION ALL SELECT N'184',N'9',N'7',N'មូលធនជាក៉ស្តែងផ្ទាល់ 9៥ - ៦ប',N'',N'0',N'1'
 UNION ALL SELECT N'185',N'9',N'0',N'',N'',N'0',N'1'
 UNION ALL SELECT N'186',N'9',N'8',N'អត្រាប្រាក់ចំណេញលើមូលធនជាក់ស្តែងផ្ទាល់',N'',N'0',N'1'
 UNION ALL SELECT N'187',N'9',N'9',N'ប្រាក់ចំណេញលើមូលធនជាក់ស្តែងផ្ទាល់ តាមអត្រាខាងលើ',N'',N'0',N'1'
 UNION ALL SELECT N'188',N'9',N'10',N'ប្រាក់ចំណេញសមស្រប',N'',N'0',N'1'
 UNION ALL SELECT N'202',N'12',N'ក',N'ចំណូលផ្សេងៗ',N'4200',N'0',N'1'
 UNION ALL SELECT N'203',N'12',N'ខ',N'ចំណូលផ្សេងៗពីអាជីវកម្មអគ្គិសនី',N'4300',N'0',N'1'
 UNION ALL SELECT N'204',N'12',N'1',N'ចំណូលពីការផ្តាច់-ភ្ជាប់ចរន្ត',N'4310',N'0',N'1'
 UNION ALL SELECT N'205',N'12',N'2',N'ចំណូលពីប្រាក់កក់របស់អតិថិជន',N'3150',N'0',N'1'
 UNION ALL SELECT N'206',N'12',N'3',N'ចំណូលពីការថ្លឹង-ប្តូរនាឡិការស្ទង់',N'4320',N'0',N'1'
 UNION ALL SELECT N'207',N'12',N'4',N'ចំណូលពីការផាកពិន័យលើការប្រើប្រាស់អប្បបរមា',N'4330',N'0',N'1'
 UNION ALL SELECT N'208',N'12',N'5',N'សេវារដ្ឋបាល និងសេវាផ្សេងៗ',N'4340',N'0',N'1'
 UNION ALL SELECT N'209',N'12',N'6',N'ឈ្នួលពីការតម្លើងបណ្តាញ',N'4350',N'0',N'1'
 UNION ALL SELECT N'210',N'12',N'គ',N'ចំណូលផ្សេងៗទៀតពីអាជីវកម្មអគ្គិសនី',N'4400',N'0',N'1'
 UNION ALL SELECT N'211',N'12',N'1',N'ចំណូលពីការផាកពិន័យ',N'4410',N'0',N'1'
 UNION ALL SELECT N'212',N'12',N'2',N'ចំណូលផាកពិន័យពីការបង់យឺត',N'4420',N'0',N'1'
 UNION ALL SELECT N'213',N'12',N'3',N'ចំណូលពីការជួលទ្រព្យអាជីវកម្ម',N'4430',N'0',N'1'
 UNION ALL SELECT N'214',N'12',N'4',N'ចំណូលផ្សេងៗ',N'4490',N'0',N'1'
 UNION ALL SELECT N'215',N'11',N'1',N'លំនៅដ្ឌាន',N'4010',N'0',N'1'
 UNION ALL SELECT N'216',N'11',N'2',N'លំនៅដ្ឌានជនបរទេស',N'4020',N'0',N'1'
 UNION ALL SELECT N'217',N'11',N'3',N'ពាណិជ្ជកម្ម-អាជីវកម្ម',N'4030',N'0',N'1'
 UNION ALL SELECT N'218',N'11',N'4',N'ឧស្សាហកម្ម-សិប្បកម្ម',N'4040',N'0',N'1'
 UNION ALL SELECT N'219',N'11',N'5',N'ស្ថាប័នរដ្ឌាភិបាល',N'4050',N'0',N'1'
 UNION ALL SELECT N'220',N'11',N'6',N'បំភ្លឺតាមផ្លូវសារធារណៈ',N'4060',N'0',N'1'
 UNION ALL SELECT N'221',N'11',N'7',N'លក់តាមរយៈនាឡិកាស្ទង់ខូច',N'4070',N'0',N'1'
 UNION ALL SELECT N'222',N'11',N'8',N'ផ្សេងៗ',N'4090',N'0',N'1'
	SET IDENTITY_INSERT ANR.TBL_REPORT_ITEM OFF;
END
GO
IF OBJECT_ID('[ANR].[TBL_REPORT_ITEM_DETAIL]') IS NULL
CREATE TABLE [ANR].[TBL_REPORT_ITEM_DETAIL](
	[ITEM_DETAIL_ID] [int] IDENTITY(1,1) NOT NULL,
	[ITEM_ID] [int] NOT NULL,
	[ACCOUNT_CODE] [nvarchar](50) COLLATE Latin1_General_BIN NOT NULL,
	[BUSINESS_DIVISION_ID] [int] NOT NULL,
	[CELL_ID] [int] NOT NULL,
	[NOTE] [nvarchar](max) COLLATE Latin1_General_BIN NOT NULL,
	[IS_ACTIVE] [bit] NOT NULL,
 CONSTRAINT [PK__TBL_REPORT_ITEM___70148828] PRIMARY KEY CLUSTERED 
(
	[ITEM_DETAIL_ID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/*
SELECT '''N''''''+'+ 'CONVERT(NVARCHAR(MAX),'+COLUMN_NAME+')+'''''',''+'
FROM INFORMATION_SCHEMA.COLUMNS 
WHERE TABLE_NAME='TBL_REPORT_ITEM_DETAIL'

SELECT ' UNION ALL SELECT '+
'N'''+CONVERT(NVARCHAR(MAX),ITEM_DETAIL_ID)+''','+
'N'''+CONVERT(NVARCHAR(MAX),ITEM_ID)+''','+
'N'''+CONVERT(NVARCHAR(MAX),ACCOUNT_CODE)+''','+
'N'''+CONVERT(NVARCHAR(MAX),BUSINESS_DIVISION_ID)+''','+
'N'''+CONVERT(NVARCHAR(MAX),CELL_ID)+''','+
'N'''+CONVERT(NVARCHAR(MAX),NOTE)+''','+
'N'''+CONVERT(NVARCHAR(MAX),IS_ACTIVE)+''''
FROM ANR.TBL_REPORT_ITEM_DETAIL
*/

IF NOT EXISTS(SELECT * FROM ANR.TBL_REPORT_ITEM_DETAIL)
BEGIN
	SET IDENTITY_INSERT ANR.TBL_REPORT_ITEM_DETAIL ON;
	INSERT INTO ANR.TBL_REPORT_ITEM_DETAIL(ITEM_DETAIL_ID,ITEM_ID,ACCOUNT_CODE,BUSINESS_DIVISION_ID,CELL_ID,NOTE,IS_ACTIVE)
	SELECT N'1',N'20',N'5130',N'10201',N'0',N'ទឹក',N'1'
 UNION ALL SELECT N'2',N'22',N'5200',N'10201',N'0',N'ចំណាយគ្រប់គ្រង និងវិស្វកម្ម',N'1'
 UNION ALL SELECT N'3',N'22',N'5410',N'10202',N'0',N'ចំណាយគ្រប់គ្រង និងវិស្វកម្ម',N'1'
 UNION ALL SELECT N'4',N'22',N'5610',N'10203',N'0',N'ចំណាយគ្រប់គ្រង និងវិស្វកម្ម',N'1'
 UNION ALL SELECT N'5',N'23',N'5210',N'10201',N'0',N'ធានារ៉ាប់រង',N'1'
 UNION ALL SELECT N'6',N'19',N'5220',N'10201',N'0',N'ជួល',N'1'
 UNION ALL SELECT N'7',N'19',N'5460',N'10202',N'0',N'ជួល',N'1'
 UNION ALL SELECT N'8',N'19',N'5680',N'10203',N'0',N'ជួល',N'1'
 UNION ALL SELECT N'9',N'18',N'5230',N'10201',N'0',N'ជួសជុល និងថែទាំ',N'1'
 UNION ALL SELECT N'10',N'18',N'5440',N'10202',N'0',N'ជួសជុល និងថែទាំ',N'1'
 UNION ALL SELECT N'11',N'18',N'5640',N'10203',N'0',N'ជួសជុល និងថែទាំ',N'1'
 UNION ALL SELECT N'12',N'16',N'5240',N'10201',N'0',N'គ្រឿងបន្លាស់',N'1'
 UNION ALL SELECT N'13',N'16',N'5435',N'10202',N'0',N'គ្រឿងបន្លាស់',N'1'
 UNION ALL SELECT N'14',N'16',N'5635',N'10203',N'0',N'គ្រឿងបន្លាស់',N'1'
 UNION ALL SELECT N'15',N'17',N'5250',N'10201',N'0',N'សម្ភារៈ និងការផ្គត់ផ្គង់',N'1'
 UNION ALL SELECT N'16',N'17',N'5450',N'10202',N'0',N'សម្ភារៈ និងការផ្គត់ផ្គង់',N'1'
 UNION ALL SELECT N'17',N'17',N'5645',N'10203',N'0',N'សម្ភារៈ និងការផ្គត់ផ្គង់',N'1'
 UNION ALL SELECT N'18',N'30',N'5290',N'10201',N'0',N'ចំណាយផ្សេងៗ',N'1'
 UNION ALL SELECT N'19',N'30',N'5490',N'10202',N'0',N'ចំណាយផ្សេងៗ',N'1'
 UNION ALL SELECT N'20',N'30',N'5690',N'10203',N'0',N'ចំណាយផ្សេងៗ',N'1'
 UNION ALL SELECT N'21',N'24',N'5420',N'10202',N'0',N'ចំណាយផ្នែកបែងចែកបន្ទុក',N'1'
 UNION ALL SELECT N'22',N'24',N'5620',N'10203',N'0',N'ចំណាយផ្នែកបែងចែកបន្ទុក',N'1'
 UNION ALL SELECT N'23',N'25',N'5430',N'10202',N'0',N'ចំណាយផ្នែកបន្ទប់ភ្លើង',N'1'
 UNION ALL SELECT N'24',N'25',N'5630',N'10203',N'0',N'ចំណាយផ្នែកបន្ទប់ភ្លើង',N'1'
 UNION ALL SELECT N'25',N'26',N'5650',N'10203',N'0',N'ចំណាយបំភ្លឺសាធារណៈ និងប្រព័ន្ធឱ្យសញ្ញាផ្សេងៗ',N'1'
 UNION ALL SELECT N'26',N'28',N'5660',N'10203',N'0',N'ចំណាយលើការថែទាំនាឡិការស្ទង់',N'1'
 UNION ALL SELECT N'27',N'29',N'5660',N'10203',N'0',N'ចំណាយលើការថែទាំនាឡិការស្ទង់',N'1'
 UNION ALL SELECT N'28',N'27',N'5670',N'10203',N'0',N'ចំណាយលើការតម្លើងជូនអតិថិជន',N'1'
 UNION ALL SELECT N'29',N'21',N'5840',N'10203',N'0',N'បំណុលទារមិនបាន',N'1'
 UNION ALL SELECT N'30',N'42',N'5850',N'10203',N'0',N'កម្រៃជើងសារ',N'1'
 UNION ALL SELECT N'31',N'41',N'6030',N'10204',N'0',N'ចំណាយលើបុគ្គលិកបណ្តែត',N'1'
 UNION ALL SELECT N'32',N'40',N'6100',N'10204',N'0',N'បណ្តុះបណ្តាល និងអភិវឌ្ឍន៍បុគ្គលិក',N'1'
 UNION ALL SELECT N'33',N'36',N'6110',N'10204',N'0',N'ចំណាយលើបេសកកម្មក្រៅប្រទេស',N'1'
 UNION ALL SELECT N'34',N'37',N'6120',N'10204',N'0',N'ចំណាយលើបេសកកម្ម និងការទទួលភ្ញៀវ',N'1'
 UNION ALL SELECT N'35',N'34',N'6130',N'10204',N'0',N'ការដឹកជញ្ជូន និងការធ្វើដំណើរក្នុងស្រុក',N'1'
 UNION ALL SELECT N'36',N'35',N'6140',N'10204',N'0',N'ចំណាយលើប្រេងឥន្ធនៈបុគ្គលិក',N'1'
 UNION ALL SELECT N'37',N'39',N'6150',N'10204',N'0',N'ចំណាយសម្ភារៈការិយាល័យទូទៅ',N'1'
 UNION ALL SELECT N'38',N'38',N'6160',N'10204',N'0',N'សេវាប្រៃសណីយ៍ និងទូរគមនាគមន៍',N'1'
 UNION ALL SELECT N'39',N'51',N'6200',N'10204',N'0',N'ពន្ធ',N'1'
 UNION ALL SELECT N'40',N'52',N'6210',N'10204',N'0',N'ពន្ធលើប្រេងឥន្ធនៈ',N'1'
 UNION ALL SELECT N'41',N'53',N'6220',N'10204',N'0',N'ពន្ធលើផលរបរ',N'1'
 UNION ALL SELECT N'42',N'54',N'6230',N'10204',N'0',N'ពន្ធប៉ាតង់',N'1'
 UNION ALL SELECT N'43',N'55',N'6240',N'10204',N'0',N'ពន្ធនាំចូល',N'1'
 UNION ALL SELECT N'44',N'56',N'6250',N'10204',N'0',N'ពន្ធលើមធ្យោបាយដឹកជញ្ជូន',N'1'
 UNION ALL SELECT N'45',N'57',N'6290',N'10204',N'0',N'ពន្ធផ្សេងៗទៀត',N'1'
 UNION ALL SELECT N'46',N'43',N'6300',N'10204',N'0',N'ជួល',N'1'
 UNION ALL SELECT N'47',N'44',N'6310',N'10204',N'0',N'ទឹក-ភ្លើង',N'1'
 UNION ALL SELECT N'48',N'45',N'6320',N'10204',N'0',N'ជួសជុល និងថែទាំ',N'1'
 UNION ALL SELECT N'49',N'33',N'6350',N'10204',N'0',N'សេវាសនកម្ម និងច្បាប់',N'1'
 UNION ALL SELECT N'50',N'32',N'6360',N'10204',N'0',N'កម្រៃអាជ្ញាប័ណ្ណ',N'1'
 UNION ALL SELECT N'51',N'46',N'6370',N'10204',N'0',N'ចំណាយលើការគ្រប់គ្រង',N'1'
 UNION ALL SELECT N'52',N'47',N'6400',N'10204',N'0',N'ធានារ៉ាប់រង',N'1'
 UNION ALL SELECT N'53',N'48',N'6410',N'10204',N'0',N'សំណងលើការខូចខាត និងគ្រោះថ្នាក់ការងារ',N'1'
 UNION ALL SELECT N'54',N'31',N'6450',N'10204',N'0',N'សេវាធនាគារ',N'1'
 UNION ALL SELECT N'55',N'49',N'6500',N'10204',N'0',N'ពិធីជប់លៀង និងការសំដែង',N'1'
 UNION ALL SELECT N'56',N'50',N'6510',N'10204',N'0',N'អំណោយ និងវិភាគទាន',N'1'
 UNION ALL SELECT N'57',N'58',N'6590',N'10204',N'0',N'ចំណាយទូទៅផ្សេងៗ',N'1'
 UNION ALL SELECT N'58',N'61',N'AS1-5110',N'0',N'1',N'ចំណាយប្រេងប្រេងឥន្ធនៈ',N'1'
 UNION ALL SELECT N'59',N'62',N'AS1-5120',N'0',N'2',N'ចំណាយប្រេងរំអិល',N'1'
 UNION ALL SELECT N'60',N'63',N'AS3-6000',N'0',N'5',N'ចំណាយលើបុគ្គលិកផ្នែកចែកចាយ',N'1'
 UNION ALL SELECT N'61',N'63',N'AS3-6000',N'0',N'6',N'ចំណាយលើបុគ្គលិកផ្នែកបញ្ជូន',N'1'
 UNION ALL SELECT N'62',N'63',N'AS3-6000',N'0',N'7',N'ចំណាយលើបុគ្គលិកផ្នែកផលិតកម្ម',N'1'
 UNION ALL SELECT N'63',N'64',N'AS4-5100',N'0',N'8',N'ចំណាយដំណើរការនិងការថែទាំ',N'1'
 UNION ALL SELECT N'64',N'65',N'AS5-0001',N'0',N'11',N'ចំណាយរដ្ឋបាលនិងការគ្រប់គ្រងទូទៅ',N'1'
 UNION ALL SELECT N'65',N'66',N'AS8-5920',N'0',N'27',N'ចំណាយសម្រាប់រំលស់',N'1'
 UNION ALL SELECT N'66',N'67',N'AS6-7000/7010/7030',N'0',N'17',N'ការប្រាក់',N'1'
 UNION ALL SELECT N'67',N'68',N'AS9-0001',N'0',N'29',N'ប្រាក់ចំណេញសមស្រប',N'1'
 UNION ALL SELECT N'68',N'73',N'AS2-5000',N'0',N'4',N'ចំណាយទិញថាមពល',N'1'
 UNION ALL SELECT N'69',N'74',N'AS3-6000',N'0',N'5',N'ចំណាយលើបុគ្គលិកផ្នែកចែកចាយ',N'1'
 UNION ALL SELECT N'70',N'74',N'AS3-6000',N'0',N'6',N'ចំណាយលើបុគ្គលិកផ្នែកបញ្ជូន',N'1'
 UNION ALL SELECT N'71',N'74',N'AS3-6000',N'0',N'7',N'ចំណាយលើបុគ្គលិកផ្នែកផលិតកម្ម',N'1'
 UNION ALL SELECT N'72',N'75',N'AS4-5400',N'0',N'9',N'ចំណាយដំណើរការនិងការថែទាំ',N'1'
 UNION ALL SELECT N'73',N'76',N'AS5-6000',N'0',N'12',N'ចំណាយរដ្ឋបាលនិងការគ្រប់គ្រងទូទៅ',N'1'
 UNION ALL SELECT N'74',N'77',N'AS8-5940',N'0',N'28',N'ចំណាយសម្រាប់រំលស់',N'1'
 UNION ALL SELECT N'75',N'78',N'AS6-7000/7010/7030',N'0',N'17',N'ការប្រាក់',N'1'
 UNION ALL SELECT N'76',N'79',N'AS9-0002',N'0',N'30',N'ប្រាក់ចំណេញសមស្រប',N'1'
 UNION ALL SELECT N'77',N'82',N'AS2-5000',N'0',N'4',N'ចំណាយទិញថាមពល',N'0'
 UNION ALL SELECT N'78',N'83',N'AS3-6000',N'0',N'5',N'ចំណាយលើបុគ្គលិកផ្នែកចែកចាយ',N'1'
 UNION ALL SELECT N'79',N'83',N'AS3-6000',N'0',N'6',N'ចំណាយលើបុគ្គលិកផ្នែកបញ្ជូន',N'1'
 UNION ALL SELECT N'80',N'83',N'AS3-6000',N'0',N'7',N'ចំណាយលើបុគ្គលិកផ្នែកផលិតកម្ម',N'1'
 UNION ALL SELECT N'81',N'84',N'AS4-5600',N'0',N'10',N'ចំណាយដំណើរការនិងការថែទាំ',N'1'
 UNION ALL SELECT N'82',N'85',N'AS5-6000',N'0',N'12',N'ចំណាយរដ្ឋបាលនិងការគ្រប់គ្រងទូទៅ',N'1'
 UNION ALL SELECT N'83',N'86',N'AS8-5910/5960/5980',N'0',N'26',N'ចំណាយសម្រាប់រំលស់',N'1'
 UNION ALL SELECT N'84',N'87',N'AS6-7000/7010/7030',N'0',N'17',N'ការប្រាក់',N'1'
 UNION ALL SELECT N'85',N'88',N'AS9-0003',N'0',N'31',N'ប្រាក់ចំណេញសមស្រប',N'1'
 UNION ALL SELECT N'86',N'91',N'AS12-4200/4300/4400',N'0',N'3',N'ដកចំណូលផ្សេងក្រៅពីប្រភពថ្លៃលក់អគ្គិសនី(លើកលែងប្រាក់កក់អតិថិជន)',N'1'
 UNION ALL SELECT N'87',N'135',N'AS7-1015',N'0',N'18',N'តម្លៃដើមសរុបនៃអចលនទ្រព្យ (ដកវិភាគទានអ្នកប្រើប្រាស់និងជំនួយ)',N'1'
 UNION ALL SELECT N'88',N'139',N'AS8-1105',N'0',N'22',N'ដក រំលស់បូកយោង',N'1'
 UNION ALL SELECT N'89',N'142',N'AS6-0001',N'0',N'13',N'ប្រាក់កម្ចីសំរាប់អចលនទ្រព្យកំពុងប្រើប្រាស់',N'1'
 UNION ALL SELECT N'90',N'146',N'AS7-1020',N'0',N'19',N'តម្លៃដើមសរុបនៃអចលនទ្រព្យ (ដកវិភាគទានអ្នកប្រើប្រាស់និងជំនួយ)',N'1'
 UNION ALL SELECT N'91',N'150',N'AS8-1110',N'0',N'23',N'ដក រំលស់បូកយោង',N'1'
 UNION ALL SELECT N'92',N'153',N'AS6-0002',N'0',N'14',N'ប្រាក់កម្ចីសំរាប់អចលនទ្រព្យកំពុងប្រើប្រាស់',N'1'
 UNION ALL SELECT N'93',N'161',N'AS7-1040',N'0',N'20',N'តម្លៃដើមសរុបនៃអចលនទ្រព្យ (ដកវិភាគទានអ្នកប្រើប្រាស់និងជំនួយ)',N'1'
 UNION ALL SELECT N'94',N'165',N'AS8-1130',N'0',N'24',N'ដក រំលោះបូកយោង',N'1'
 UNION ALL SELECT N'95',N'168',N'AS6-0003',N'0',N'15',N'ប្រាក់កម្ចីសំរាប់អចលនទ្រព្យកំពុងប្រើប្រាស់',N'1'
 UNION ALL SELECT N'96',N'176',N'AS7-1050/1080',N'0',N'21',N'តម្លៃដើមសរុបនៃអចលនទ្រព្យ (ដកវិភាគទានអ្នកប្រើប្រាស់និងជំនួយ)',N'1'
 UNION ALL SELECT N'97',N'180',N'AS8-1150/1170',N'0',N'25',N'ដក រំលោះបូកយោង',N'1'
 UNION ALL SELECT N'98',N'183',N'AS6-0004',N'0',N'16',N'ប្រាក់កម្ចីសំរាប់អចលនទ្រព្យកំពុងប្រើប្រាស់',N'1'
 UNION ALL SELECT N'99',N'202',N'4200',N'0',N'0',N'ចំណូលផ្សេងៗ',N'1'
 UNION ALL SELECT N'100',N'203',N'4300',N'0',N'0',N'ចំណូលផ្សេងៗពីអាជីវកម្មអគ្គិសនី',N'1'
 UNION ALL SELECT N'101',N'210',N'4400',N'0',N'0',N'ចំណូលផ្សេងៗទៀតពីអាជីវកម្មអគ្គិសនី',N'1'
 UNION ALL SELECT N'102',N'205',N'3150',N'0',N'0',N'ចំណូលពីប្រាក់កក់របស់អតិថិជន',N'1'
 UNION ALL SELECT N'103',N'204',N'4310',N'0',N'0',N'ចំណូលពីការផ្តាច់-ភ្ជាប់ចរន្ត',N'1'
 UNION ALL SELECT N'104',N'206',N'4320',N'0',N'0',N'ចំណូលពីការថ្លឹង-ប្តូរនាឡិការស្ទង់',N'1'
 UNION ALL SELECT N'105',N'207',N'4330',N'0',N'0',N'ចំណូលពីការផាកពិន័យលើការប្រើប្រាស់អប្បបរមា',N'1'
 UNION ALL SELECT N'106',N'208',N'4340',N'0',N'0',N'សេវារដ្ឋបាល និងសេវាផ្សេងៗ',N'1'
 UNION ALL SELECT N'107',N'209',N'4350',N'0',N'0',N'ឈ្នួលពីការតម្លើងបណ្តាញ',N'1'
 UNION ALL SELECT N'108',N'211',N'4410',N'0',N'0',N'ចំណូលពីការផាកពិន័យ',N'1'
 UNION ALL SELECT N'109',N'212',N'4420',N'0',N'0',N'ចំណូលផាកពិន័យពីការបង់យឺត',N'1'
 UNION ALL SELECT N'110',N'213',N'4430',N'0',N'0',N'ចំណូលពីការជួលទ្រព្យអាជីវកម្ម',N'1'
 UNION ALL SELECT N'111',N'214',N'4490',N'0',N'0',N'ចំណូលផ្សេងៗ',N'1'
	SET IDENTITY_INSERT ANR.TBL_REPORT_ITEM_DETAIL OFF;
END
GO
IF OBJECT_ID('[ANR].[TBL_REPORT_LINE]') IS NULL
CREATE TABLE [ANR].[TBL_REPORT_LINE](
	[LINE_ID] [int] IDENTITY(1,1) NOT NULL,
	[TABLE_ID] [int] NOT NULL,
	[ROW_NO] [nvarchar](50) COLLATE Latin1_General_BIN NOT NULL,
	[ITEM_NAME] [nvarchar](100) COLLATE Latin1_General_BIN NOT NULL,
	[TABLE_CODE] [nvarchar](100) COLLATE Latin1_General_BIN NOT NULL,
	[ACCOUNT_CODE] [nvarchar](100) COLLATE Latin1_General_BIN NOT NULL,
	[CELL_ID] [int] NOT NULL,
 CONSTRAINT [PK_TBL_REPORT_LINE] PRIMARY KEY CLUSTERED 
(
	[LINE_ID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/*
SELECT '''N''''''+'+ 'CONVERT(NVARCHAR(MAX),'+COLUMN_NAME+')+'''''',''+'
FROM INFORMATION_SCHEMA.COLUMNS 
WHERE TABLE_NAME='TBL_REPORT_LINE'

SELECT ' UNION ALL SELECT '+
'N'''+CONVERT(NVARCHAR(MAX),LINE_ID)+''','+
'N'''+CONVERT(NVARCHAR(MAX),TABLE_ID)+''','+
'N'''+CONVERT(NVARCHAR(MAX),ROW_NO)+''','+
'N'''+CONVERT(NVARCHAR(MAX),ITEM_NAME)+''','+
'N'''+CONVERT(NVARCHAR(MAX),TABLE_CODE)+''','+
'N'''+CONVERT(NVARCHAR(MAX),ACCOUNT_CODE)+''','+
'N'''+CONVERT(NVARCHAR(MAX),CELL_ID)+''''
FROM ANR.TBL_REPORT_LINE
*/

IF NOT EXISTS(SELECT * FROM ANR.TBL_REPORT_LINE)
BEGIN
	SET IDENTITY_INSERT ANR.TBL_REPORT_LINE ON;
	INSERT INTO ANR.TBL_REPORT_LINE(LINE_ID,TABLE_ID,ROW_NO,ITEM_NAME,TABLE_CODE,ACCOUNT_CODE,CELL_ID)
	SELECT N'1',N'10',N'ក',N'ចំណាយផ្នែកផលិតកម្ម',N'',N'',N'0'
 UNION ALL SELECT N'2',N'10',N'1',N'ចំណាយប្រេងប្រេងឥន្ធនៈ',N'AS1',N'5110',N'1'
 UNION ALL SELECT N'3',N'10',N'2',N'ចំណាយប្រេងរំអិល',N'AS1',N'5120',N'2'
 UNION ALL SELECT N'4',N'10',N'3',N'ចំណាយលើបុគ្គលិកផ្នែកផលិតកម្ម',N'AS3',N'6000',N'6'
 UNION ALL SELECT N'5',N'10',N'4',N'ចំណាយដំណើរការនិងការថែទាំ',N'AS4',N'5100',N'8'
 UNION ALL SELECT N'6',N'10',N'5',N'ចំណាយរដ្ឋបាលនិងការគ្រប់គ្រងទូទៅ',N'AS5',N'0001',N'0'
 UNION ALL SELECT N'7',N'10',N'6',N'ចំណាយសម្រាប់រំលស់',N'AS8',N'5920',N'27'
 UNION ALL SELECT N'8',N'10',N'7',N'ការប្រាក់',N'AS6',N'7000/7010/7030',N'17'
 UNION ALL SELECT N'9',N'10',N'8',N'ប្រាក់ចំណេញសមស្រប',N'AS9',N'0001',N'29'
 UNION ALL SELECT N'10',N'10',N'',N'ចំណាយត្រឹមត្រូវសរុបសំរាប់ផ្នែកផលិតកម្ម',N'',N'',N'34'
 UNION ALL SELECT N'11',N'10',N'9',N'ថាមពលបញ្ជូនគិតជាគីឡូវ៉ាត់ម៉ោង',N'',N'',N'0'
 UNION ALL SELECT N'12',N'10',N'10',N'ថ្លៃដើមផលិតក្នុង១គីឡូវ៉ាត់ម៉ោង',N'',N'',N'0'
 UNION ALL SELECT N'13',N'10',N'ខ',N'ចំណាយផ្នែកបញ្ជូន',N'',N'',N'0'
 UNION ALL SELECT N'14',N'10',N'1',N'ចំណាយទិញថាមពល',N'AS2',N'5000',N'0'
 UNION ALL SELECT N'15',N'10',N'2',N'ចំណាយលើបុគ្គលិកផ្នែកបញ្ជូន',N'AS3',N'6000',N'5'
 UNION ALL SELECT N'16',N'10',N'3',N'ចំណាយដំណើរការនិងការថែទាំ',N'AS4',N'5400',N'9'
 UNION ALL SELECT N'17',N'10',N'4',N'ចំណាយរដ្ឋបាលនិងការគ្រប់គ្រងទូទៅ',N'AS5',N'6000',N'0'
 UNION ALL SELECT N'18',N'10',N'5',N'ចំណាយសម្រាប់រំលស់',N'AS8',N'5940',N'28'
 UNION ALL SELECT N'19',N'10',N'6',N'ការប្រាក់',N'AS6',N'7000/7010/7030',N'32'
 UNION ALL SELECT N'20',N'10',N'7',N'ប្រាក់ចំណេញសមស្រប',N'AS9',N'0002',N'30'
 UNION ALL SELECT N'21',N'10',N'',N'ចំណាយត្រឹមត្រូវសរុបសំរាប់ផ្នែកបញ្ជូន',N'',N'',N'35'
 UNION ALL SELECT N'22',N'10',N'គ',N'ចំណាយផ្នែកចែកចាយ',N'',N'',N'0'
 UNION ALL SELECT N'23',N'10',N'1',N'ចំណាយទិញថាមពល',N'AS2',N'5000',N'4'
 UNION ALL SELECT N'24',N'10',N'2',N'ចំណាយលើបុគ្គលិកផ្នែកចែកចាយ',N'AS3',N'6000',N'7'
 UNION ALL SELECT N'25',N'10',N'3',N'ចំណាយដំណើរការនិងការថែទាំ',N'AS4',N'5600',N'10'
 UNION ALL SELECT N'26',N'10',N'4',N'ចំណាយរដ្ឋបាលនិងការគ្រប់គ្រងទូទៅ',N'AS5',N'6000',N'12'
 UNION ALL SELECT N'27',N'10',N'5',N'ចំណាយសម្រាប់រំលស់',N'AS8',N'5910/5960/5980',N'26'
 UNION ALL SELECT N'28',N'10',N'6',N'ការប្រាក់',N'AS6',N'7000/7010/7030',N'33'
 UNION ALL SELECT N'29',N'10',N'7',N'ប្រាក់ចំណេញសមស្រប',N'AS9',N'0003',N'31'
 UNION ALL SELECT N'30',N'10',N'',N'ចំណាយត្រឹមត្រូវសរុបសំរាប់ផ្នែកចែកចាយ',N'',N'',N'36'
 UNION ALL SELECT N'31',N'10',N'',N'ចំណាយសរុប (ផលិតកម្ម + បញ្ជូន + ចែកចាយ)',N'',N'',N'37'
 UNION ALL SELECT N'32',N'10',N'ឃ',N'ដកចំណូលផ្សេងក្រៅពីប្រភពថ្លៃលក់អគ្គិសនី(លើកលែងប្រាក់កក់អតិថិជន)',N'AS1',N'AS12-4200/4300/4400',N'3'
 UNION ALL SELECT N'33',N'10',N'ង',N'ចំណូលសរុបប្រចាំឆ្នាំដែលត្រូវមាន (ក+ខ+គ-ឃ)',N'',N'',N'38'
 UNION ALL SELECT N'34',N'10',N'ច',N'ចំណូលសរុបដែលបានមកពីថ្លៃលក់បច្ចុប្បន្ន',N'',N'',N'39'
 UNION ALL SELECT N'35',N'10',N'ឆ',N'ចំណេញ/(ខាត) មុនកែសម្រួលថ្លៃលក់ (ច-ង)',N'',N'',N'40'
 UNION ALL SELECT N'36',N'10',N'ជ',N'ផលប៉ះពាល់លើការកែសម្រួលថ្លៃលក់',N'',N'',N'41'
 UNION ALL SELECT N'37',N'10',N'ឈ',N'ចំណេញ/(ខាត) ក្រោយកែសម្រួលថ្លៃលក់ (ច+ជ-ង)',N'',N'',N'42'
 UNION ALL SELECT N'38',N'9',N'',N'អគារ',N'',N'',N'0'
 UNION ALL SELECT N'39',N'9',N'1',N'តម្លៃដើមសរុបនៃអចលនទ្រព្យ (ដកវិភាគទានអ្នកប្រើប្រាស់និងជំនួយ)',N'AS7',N'1015',N'18'
 UNION ALL SELECT N'40',N'9',N'2',N'សំវិធានធនសម្រាប់ទុនចល័ត',N'',N'',N'43'
 UNION ALL SELECT N'41',N'9',N'3',N'តម្លៃអចលនទ្រព្យសរុប',N'',N'',N'44'
 UNION ALL SELECT N'42',N'9',N'0',N'',N'',N'',N'0'
 UNION ALL SELECT N'43',N'9',N'4',N'ដក រំលស់បូកយោង',N'AS8',N'1105',N'22'
 UNION ALL SELECT N'44',N'9',N'5',N'តម្លៃអចលនទ្រព្យសុទ្ធ',N'',N'',N'45'
 UNION ALL SELECT N'45',N'9',N'0',N'',N'',N'',N'0'
 UNION ALL SELECT N'46',N'9',N'6',N'ប្រាក់កម្ចីសំរាប់អចលនទ្រព្យកំពុងប្រើប្រាស់',N'AS6',N'0001',N'13'
 UNION ALL SELECT N'47',N'9',N'7',N'មូលធនជាក៉ស្តែងផ្ទាល់ 9៥ - ៦ប',N'',N'',N'46'
 UNION ALL SELECT N'48',N'9',N'0',N'',N'',N'',N'0'
 UNION ALL SELECT N'49',N'9',N'0',N'អាជីវកម្មផ្នែកផលិតកម្ម',N'',N'',N'0'
 UNION ALL SELECT N'50',N'9',N'1',N'តម្លៃដើមសរុបនៃអចលនទ្រព្យ (ដកវិភាគទានអ្នកប្រើប្រាស់និងជំនួយ)',N'AS7',N'1020',N'19'
 UNION ALL SELECT N'51',N'9',N'2',N'សំវិធានធនសម្រាប់ទុនចល័ត',N'',N'',N'47'
 UNION ALL SELECT N'52',N'9',N'3',N'តម្លៃអចលនទ្រព្យសរុប',N'',N'',N'48'
 UNION ALL SELECT N'53',N'9',N'0',N'',N'',N'',N'0'
 UNION ALL SELECT N'54',N'9',N'4',N'ដក រំលស់បូកយោង',N'AS8',N'1110',N'23'
 UNION ALL SELECT N'55',N'9',N'5',N'តម្លៃអចលនទ្រព្យសុទ្ធ',N'',N'',N'49'
 UNION ALL SELECT N'56',N'9',N'0',N'',N'',N'',N'0'
 UNION ALL SELECT N'57',N'9',N'6',N'ប្រាក់កម្ចីសំរាប់អចលនទ្រព្យកំពុងប្រើប្រាស់',N'AS6',N'0002',N'14'
 UNION ALL SELECT N'58',N'9',N'7',N'មូលធនជាក៉ស្តែងផ្ទាល់ 9៥ - ៦ប',N'',N'',N'50'
 UNION ALL SELECT N'59',N'9',N'0',N'',N'',N'',N'0'
 UNION ALL SELECT N'60',N'9',N'8',N'អត្រាប្រាក់ចំណេញលើមូលធនជាក់ស្តែងផ្ទាល់',N'',N'',N'0'
 UNION ALL SELECT N'61',N'9',N'9',N'ប្រាក់ចំណេញលើមូលធនជាក់ស្តែងផ្ទាល់ តាមអត្រាខាងលើ',N'',N'',N'0'
 UNION ALL SELECT N'62',N'9',N'10',N'ប្រាក់ចំណេញសមស្រប',N'',N'',N'0'
 UNION ALL SELECT N'63',N'9',N'0',N'',N'',N'',N'0'
 UNION ALL SELECT N'64',N'9',N'0',N'អាជីវកម្មផ្នែកបញ្ជូន',N'',N'',N'0'
 UNION ALL SELECT N'65',N'9',N'1',N'តម្លៃដើមសរុបនៃអចលនទ្រព្យ (ដកវិភាគទានអ្នកប្រើប្រាស់និងជំនួយ)',N'AS7',N'1040',N'20'
 UNION ALL SELECT N'66',N'9',N'2',N'សំវិធានធនសំរាប់ទុនចល័ត',N'',N'',N'51'
 UNION ALL SELECT N'67',N'9',N'3',N'តម្លៃអចលនទ្រព្យសរុប',N'',N'',N'52'
 UNION ALL SELECT N'68',N'9',N'0',N'',N'',N'',N'0'
 UNION ALL SELECT N'69',N'9',N'4',N'ដក រំលោះបូកយោង',N'AS8',N'1130',N'24'
 UNION ALL SELECT N'70',N'9',N'5',N'តម្លៃអចលនទ្រព្យសុទ្ធ',N'',N'',N'53'
 UNION ALL SELECT N'71',N'9',N'0',N'',N'',N'',N'0'
 UNION ALL SELECT N'72',N'9',N'6',N'ប្រាក់កម្ចីសំរាប់អចលនទ្រព្យកំពុងប្រើប្រាស់',N'AS6',N'0003',N'15'
 UNION ALL SELECT N'73',N'9',N'7',N'មូលធនជាក៉ស្តែងផ្ទាល់ 9៥ - ៦ប',N'',N'',N'54'
 UNION ALL SELECT N'74',N'9',N'0',N'',N'',N'',N'0'
 UNION ALL SELECT N'75',N'9',N'8',N'អត្រាប្រាក់ចំណេញលើមូលធនជាក់ស្តែងផ្ទាល់',N'',N'',N'0'
 UNION ALL SELECT N'76',N'9',N'9',N'ប្រាក់ចំណេញលើមូលធនជាក់ស្តែងផ្ទាល់ តាមអត្រាខាងលើ',N'',N'',N'0'
 UNION ALL SELECT N'77',N'9',N'10',N'ប្រាក់ចំណេញសមស្រប',N'',N'',N'0'
 UNION ALL SELECT N'78',N'9',N'0',N'',N'',N'',N'0'
 UNION ALL SELECT N'79',N'9',N'0',N'អាជីវកម្មផ្នែកចែកចាយ',N'',N'',N'0'
 UNION ALL SELECT N'80',N'9',N'1',N'តម្លៃដើមសរុបនៃអចលនទ្រព្យ (ដកវិភាគទានអ្នកប្រើប្រាស់និងជំនួយ)',N'AS7',N'1050/1080',N'21'
 UNION ALL SELECT N'81',N'9',N'2',N'សំវិធានធនសំរាប់ទុនចល័ត',N'',N'',N'55'
 UNION ALL SELECT N'82',N'9',N'3',N'តម្លៃអចលនទ្រព្យសរុប',N'',N'',N'56'
 UNION ALL SELECT N'83',N'9',N'0',N'',N'',N'',N'0'
 UNION ALL SELECT N'84',N'9',N'4',N'ដក រំលោះបូកយោង',N'AS8',N'1150/1170',N'25'
 UNION ALL SELECT N'85',N'9',N'5',N'តម្លៃអចលនទ្រព្យសុទ្ធ',N'',N'',N'57'
 UNION ALL SELECT N'86',N'9',N'0',N'',N'',N'',N'0'
 UNION ALL SELECT N'87',N'9',N'6',N'ប្រាក់កម្ចីសំរាប់អចលនទ្រព្យកំពុងប្រើប្រាស់',N'AS6',N'0004',N'16'
 UNION ALL SELECT N'88',N'9',N'7',N'មូលធនជាក៉ស្តែងផ្ទាល់ 9៥ - ៦ប',N'',N'',N'58'
 UNION ALL SELECT N'89',N'9',N'0',N'',N'',N'',N'0'
 UNION ALL SELECT N'90',N'9',N'8',N'អត្រាប្រាក់ចំណេញលើមូលធនជាក់ស្តែងផ្ទាល់',N'',N'',N'0'
 UNION ALL SELECT N'91',N'9',N'9',N'ប្រាក់ចំណេញលើមូលធនជាក់ស្តែងផ្ទាល់ តាមអត្រាខាងលើ',N'',N'',N'0'
 UNION ALL SELECT N'92',N'9',N'10',N'ប្រាក់ចំណេញសមស្រប',N'',N'',N'0'
	SET IDENTITY_INSERT ANR.TBL_REPORT_LINE OFF;
END
GO
IF OBJECT_ID('[ANR].[TBL_TREE]') IS NULL
CREATE TABLE [ANR].[TBL_TREE](
	[TREE_ID] [int] IDENTITY(1,1) NOT NULL,
	[TABLE_CODE] [nvarchar](50) COLLATE Latin1_General_BIN NOT NULL,
	[TREE_NAME] [nvarchar](100) COLLATE Latin1_General_BIN NOT NULL,
	[ICON_KEY] [nvarchar](50) COLLATE Latin1_General_BIN NOT NULL,
	[PARENT_ID] [int] NOT NULL,
	[CLASS] [nvarchar](100) COLLATE Latin1_General_BIN NOT NULL,
 CONSTRAINT [PK_TBL_TREE] PRIMARY KEY CLUSTERED 
(
	[TREE_ID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] 
GO

/*
SELECT '''N''''''+'+ 'CONVERT(NVARCHAR(MAX),'+COLUMN_NAME+')+'''''',''+'
FROM INFORMATION_SCHEMA.COLUMNS 
WHERE TABLE_NAME='TBL_TREE'

SELECT ' UNION ALL SELECT '+
'N'''+CONVERT(NVARCHAR(MAX),TREE_ID)+''','+
'N'''+CONVERT(NVARCHAR(MAX),TABLE_CODE)+''','+
'N'''+CONVERT(NVARCHAR(MAX),TREE_NAME)+''','+
'N'''+CONVERT(NVARCHAR(MAX),ICON_KEY)+''','+
'N'''+CONVERT(NVARCHAR(MAX),PARENT_ID)+''','+
'N'''+CONVERT(NVARCHAR(MAX),CLASS)+''''
FROM ANR.TBL_TREE
*/

IF NOT EXISTS(SELECT * FROM ANR.TBL_TREE)
BEGIN
	SET IDENTITY_INSERT ANR.TBL_TREE ON;
	INSERT INTO ANR.TBL_TREE(TREE_ID,TABLE_CODE,TREE_NAME,ICON_KEY,PARENT_ID,CLASS)
    SELECT N'1',N'AS01',N'AS01 ទិន្នន័យផ្នែកផលិតកម្ម',N'REPORT',N'0',N''
 UNION ALL SELECT N'2',N'AS02',N'AS02 ពត៌មានលំអិអំពីការទិញថាមពល',N'REPORT',N'0',N''
 UNION ALL SELECT N'3',N'AS03',N'AS03 ពត៌មានលំអិតអំពីចំណាយសម្រាប់បុគ្គលិក',N'REPORT',N'0',N''
 UNION ALL SELECT N'4',N'AS04',N'AS04 ចំណាយដំណើការនិងការថែទាំ',N'REPORT',N'0',N''
 UNION ALL SELECT N'5',N'AS05',N'AS05 ពត៌មានលំអិតសម្រាប់ចំណាយរដ្ឋបាលនិងការគ្រប់គ្រងទូទៅ',N'REPORT',N'0',N''
 UNION ALL SELECT N'6',N'AS06',N'AS06 ពត៌មានលំអិតអំពីប្រាក់កម្ចីសម្រាប់អាជីវកម្មរបស់អ្នកកាន់អជ្ញាប័ណ្ណ',N'REPORT',N'0',N''
 UNION ALL SELECT N'7',N'AS07',N'AS07 ពត៌មានលំអិតអំពីអចលនទ្រព្យសរុប',N'REPORT',N'0',N''
 UNION ALL SELECT N'8',N'AS08',N'AS08 ពត៌មានលំអិតអំពីរលស់',N'REPORT',N'0',N''
 UNION ALL SELECT N'9',N'AS09',N'AS09 ការគណនាប្រាក់ចំណេញសមស្រប',N'REPORT',N'0',N''
 UNION ALL SELECT N'10',N'AS10',N'AS10 ការគណនាចំណាយត្រឹមត្រូវ',N'REPORT',N'0',N''
 UNION ALL SELECT N'11',N'AS11',N'AS11 ចំណូលពីថ្លៃលក់អគ្គិសនីកំពុងអនុវត្ត',N'REPORT',N'0',N''
 UNION ALL SELECT N'12',N'AS12',N'AS12 ពត៌មានលំអិតអំពីចំណូលផ្សេងក្រៅពីប្រភពថ្លៃលក់អគ្គិសនី',N'REPORT',N'0',N''
 UNION ALL SELECT N'13',N'AS13',N'AS13 ពត៌មានអំពីថាមពល',N'REPORT',N'0',N''
 UNION ALL SELECT N'14',N'AS01A',N'AS01A ពត៌មាន ផ្នែកម៉ាស៊ីនផលិត',N'REPORT',N'0',N''
 UNION ALL SELECT N'15',N'AS01B',N'AS01B ព៌ត័មានអំពីខ្សែបណ្ណាញ',N'REPORT',N'0',N''
 UNION ALL SELECT N'16',N'AS01C',N'AS01C ព័ត៌មានអំពីត្រង់ស្វូ',N'REPORT',N'0',N''
 UNION ALL SELECT N'20',N'AS01',N'ការផលិតថាមពល',N'EDIT',N'1',N'EPower.Interface.PagePowerGeneration'
 UNION ALL SELECT N'21',N'AS01',N'ប្រភពទិញឥន្ធនៈ',N'EDIT',N'1',N'EPower.Interface.PageFuelSource'
 UNION ALL SELECT N'22',N'AS01',N'ការទិញឥន្ធនៈ',N'EDIT',N'1',N'EPower.Interface.PageFuelPurchase'
 UNION ALL SELECT N'27',N'AS01',N'បោះពុម្ភ',N'PRINT',N'1',N''
 UNION ALL SELECT N'28',N'AS01A',N'ម៉ាស៊ីនផលិត',N'EDIT',N'14',N'EPower.Interface.PageGenerator'
 UNION ALL SELECT N'30',N'AS01A',N'បោះពុម្ភ',N'PRINT',N'14',N''
 UNION ALL SELECT N'31',N'AS99',N'គ្រប់របាយការណ៍',N'REPORTS',N'0',N''
 UNION ALL SELECT N'33',N'AS99',N'បោះពុម្ភ',N'PRINT',N'31',N'EPower.Interface.AnnualReport.PageReportGroup'
 UNION ALL SELECT N'34',N'AS01B',N'ខ្សែបណ្តាញចែកចាយ',N'EDIT',N'15',N'EPower.Interface.PageDistributionFacility'
 UNION ALL SELECT N'36',N'AS01B',N'បោះពុម្ភ',N'PRINT',N'15',N''
 UNION ALL SELECT N'37',N'AS01C',N'ត្រង់ស្វូ',N'EDIT',N'16',N'EPower.Interface.PageTransformer'
 UNION ALL SELECT N'39',N'AS01C',N'បោះពុម្ភ',N'PRINT',N'16',N''
 UNION ALL SELECT N'40',N'AS02',N'ប្រភពទិញថាមពល',N'EDIT',N'2',N'EPower.Interface.PagePowerSource'
 UNION ALL SELECT N'43',N'AS02',N'ការទិញថាមពល',N'EDIT',N'2',N'EPower.Interface.PagePowerPurchase'
 UNION ALL SELECT N'45',N'AS02',N'បោះពុម្ភ',N'PRINT',N'2',N''
 UNION ALL SELECT N'46',N'AS03',N'ចំណាយបុគ្គលិក',N'EDIT',N'3',N'EPower.Interface.PageStaffExpense'
 UNION ALL SELECT N'48',N'AS03',N'បោះពុម្ភ',N'PRINT',N'3',N''
 UNION ALL SELECT N'49',N'AS04',N'ប្រតិបត្តការចំណាយ',N'EDIT',N'4',N'EPower.Interface.PageAccountTransactionExpense'
 UNION ALL SELECT N'51',N'AS04',N'បោះពុម្ភ',N'PRINT',N'4',N''
 UNION ALL SELECT N'52',N'AS05',N'ប្រតិបត្តការចំណាយ',N'EDIT',N'5',N'EPower.Interface.PageAccountTransactionExpense'
 UNION ALL SELECT N'54',N'AS05',N'បោះពុម្ភ',N'PRINT',N'5',N''
 UNION ALL SELECT N'55',N'AS06',N'ប្រាក់កម្ចី',N'EDIT',N'6',N'EPower.Interface.PageLoan'
 UNION ALL SELECT N'57',N'AS06',N'បោះពុម្ភ',N'PRINT',N'6',N''
 UNION ALL SELECT N'58',N'AS07',N'អចលនទ្រព្យ',N'EDIT',N'7',N'EPower.Interface.PageFixAssetItem'
 UNION ALL SELECT N'60',N'AS07',N'បោះពុម្ភ',N'PRINT',N'7',N''
 UNION ALL SELECT N'61',N'AS08',N'អចលនទ្រព្យ',N'EDIT',N'8',N'EPower.Interface.PageFixAssetItem'
 UNION ALL SELECT N'63',N'AS08',N'បោះពុម្ភ',N'PRINT',N'8',N''
 UNION ALL SELECT N'65',N'AS09',N'បោះពុម្ភ',N'PRINT',N'9',N''
 UNION ALL SELECT N'67',N'AS10',N'បោះពុម្ភ',N'PRINT',N'10',N''
 UNION ALL SELECT N'69',N'AS11',N'បោះពុម្ភ',N'PRINT',N'11',N''
 UNION ALL SELECT N'71',N'AS12',N'បោះពុម្ភ',N'PRINT',N'12',N''
 UNION ALL SELECT N'73',N'AS13',N'បោះពុម្ភ',N'PRINT',N'13',N''
 UNION ALL SELECT N'74',N'AS00A',N'គំរប',N'REPORT',N'0',N''
 UNION ALL SELECT N'75',N'AS00A',N'បោះពុម្ភ',N'PRINT',N'74',N''
 UNION ALL SELECT N'76',N'AS00B',N'សង្ខេបរបាយការណ៍',N'REPORT',N'0',N''
 UNION ALL SELECT N'77',N'AS00C',N'មាតិការ',N'REPORT',N'0',N''
 UNION ALL SELECT N'79',N'AS00B',N'គម្រោងអាជីវកម្ម',N'EDIT',N'76',N'EPower.Interface.AnnualReport.PageBussinessPlan'
 UNION ALL SELECT N'80',N'AS00C',N'បោះពុម្ភ',N'PRINT',N'77',N''
 UNION ALL SELECT N'81',N'AS00B',N'បោះពុម្ភ',N'PRINT',N'76',N''
	SET IDENTITY_INSERT ANR.TBL_TREE OFF;
END
GO

IF OBJECT_ID('ANR.TBL_YEAR') IS NULL
CREATE TABLE ANR.TBL_YEAR(
	YEAR_ID INT PRIMARY KEY,
	YEAR_NAME NVARCHAR(50) NOT NULL,
	IS_ACTIVE BIT NOT NULL
)
IF OBJECT_ID('ANR.TBL_TABLE') IS NULL
CREATE TABLE ANR.TBL_TABLE(
	TABLE_ID INT PRIMARY KEY,
	TABLE_CODE NVARCHAR(50) NOT NULL,
	TABLE_NAME NVARCHAR(100) NOT NULL,
	IS_ACTIVE BIT NOT NULL
)	
IF OBJECT_ID('ANR.TBL_TABLE_CELL') IS NULL
	CREATE TABLE ANR.TBL_TABLE_CELL(
		CELL_ID INT IDENTITY PRIMARY KEY,
		TABLE_ID INT NOT NULL,
		CELL_CODE NVARCHAR(50) NOT NULL,
		CELL_NAME NVARCHAR(100) NOT NULL,
		NOTE NVARCHAR(MAX) NOT NULL,
		IS_ACTIVE BIT NOT NULL
	);
IF OBJECT_ID('ANR.TBL_RESULT_CELL') IS NULL
	CREATE TABLE ANR.TBL_RESULT_CELL(
		ID INT IDENTITY PRIMARY KEY,
		YEAR_ID INT NOT NULL,
		CELL_ID INT NOT NULL,
		TEXT_VALUE NVARCHAR(MAX) NULL,
		NUMERIC_VALUE DECIMAL(18,4) NOT NULL,
		IS_ACTIVE BIT NOT NULL
	);
GO

/*
SELECT '''N''''''+'+ 'CONVERT(NVARCHAR(MAX),'+COLUMN_NAME+')+'''''',''+'
FROM INFORMATION_SCHEMA.COLUMNS 
WHERE TABLE_NAME='TBL_TABLE'

SELECT ' UNION ALL SELECT '+
'N'''+CONVERT(NVARCHAR(MAX),TABLE_ID)+''','+
'N'''+CONVERT(NVARCHAR(MAX),TABLE_CODE)+''','+
'N'''+CONVERT(NVARCHAR(MAX),TABLE_NAME)+''','+
'N'''+CONVERT(NVARCHAR(MAX),IS_ACTIVE)+''''
FROM ANR.TBL_TABLE
*/

IF NOT EXISTS(SELECT * FROM ANR.TBL_TABLE)
BEGIN
	INSERT INTO ANR.TBL_TABLE(TABLE_ID,TABLE_CODE,TABLE_NAME,IS_ACTIVE)
 SELECT N'1',N'AS01',N'ទិន្នន័យផ្នែកផលិតកម្ម',N'1'
 UNION ALL SELECT N'2',N'AS02',N'ពត៌មានលំអិអំពីការទិញថាមពល',N'1'
 UNION ALL SELECT N'3',N'AS03',N'ពត៌មានលំអិតអំពីចំណាយសម្រាប់បុគ្គលិក',N'1'
 UNION ALL SELECT N'4',N'AS04',N'ចំណាយដំណើការនិងការថែទាំ',N'1'
 UNION ALL SELECT N'5',N'AS05',N'ពត៌មានលំអិតសម្រាប់ចំណាយរដ្ឋបាលនិងការគ្រប់គ្រងទូទៅ',N'1'
 UNION ALL SELECT N'6',N'AS06',N'ពត៌មានលំអិតអំពីប្រាក់កម្ចីសម្រាប់អាជីវកម្មរបស់អ្នកកាន់អជ្ញាប័ណ្ណ',N'1'
 UNION ALL SELECT N'7',N'AS07',N'ពត៌មានលំអិតអំពីអចលនទ្រព្យសរុប',N'1'
 UNION ALL SELECT N'8',N'AS08',N'ពត៌មានលំអិតអំពីរលស់',N'1'
 UNION ALL SELECT N'9',N'AS09',N'ការគណនាប្រាក់ចំណេញសមស្រប',N'1'
 UNION ALL SELECT N'10',N'AS10',N'ការគណនាចំណាយត្រឹមត្រូវ',N'1'
 UNION ALL SELECT N'11',N'AS11',N'ចំណូលពីថ្លៃលក់អគ្គិសនីកំពុងអនុវត្ត',N'1'
 UNION ALL SELECT N'12',N'AS12',N'ពត៌មានលំអិតអំពីចំណូលផ្សេងក្រៅពីប្រភពថ្លៃលក់អគ្គិសនី',N'1'
 UNION ALL SELECT N'13',N'AS13',N'ពត៌មានអំពីថាមពល',N'1'
 UNION ALL SELECT N'14',N'AS01A',N'ពត៌មាន ផ្នែកម៉ាស៊ីនផលិត',N'1'
 UNION ALL SELECT N'15',N'AS01B',N'ព៌ត័មានអំពីខ្សែបណ្ណាញ',N'1'
 UNION ALL SELECT N'16',N'AS01C',N'ព័ត៌មានអំពីត្រង់ស្វូ',N'1'
 UNION ALL SELECT N'17',N'AS00A',N'គំរប',N'1'
 UNION ALL SELECT N'18',N'AS00B',N'របាយការណ៍សង្ខេប',N'1'
 UNION ALL SELECT N'19',N'AS00C',N'ឧបសម្ពន្ធ័',N'1'
END
GO


/*
SELECT '''N''''''+'+ 'CONVERT(NVARCHAR(MAX),'+COLUMN_NAME+')+'''''',''+'
FROM INFORMATION_SCHEMA.COLUMNS 
WHERE TABLE_NAME='TBL_TABLE_CELL'

SELECT ' UNION ALL SELECT '+
'N'''+CONVERT(NVARCHAR(MAX),CELL_ID)+''','+
'N'''+CONVERT(NVARCHAR(MAX),TABLE_ID)+''','+
'N'''+CONVERT(NVARCHAR(MAX),CELL_CODE)+''','+
'N'''+CONVERT(NVARCHAR(MAX),CELL_NAME)+''','+
'N'''+CONVERT(NVARCHAR(MAX),NOTE)+''','+
'N'''+CONVERT(NVARCHAR(MAX),IS_ACTIVE)+''''
FROM ANR.TBL_TABLE_CELL
*/

IF NOT EXISTS(SELECT * FROM ANR.TBL_TABLE_CELL)
BEGIN
	SET IDENTITY_INSERT ANR.TBL_TABLE_CELL ON;
	INSERT INTO ANR.TBL_TABLE_CELL(CELL_ID,TABLE_ID,CELL_CODE,CELL_NAME,NOTE,IS_ACTIVE)
 SELECT N'1',N'1',N'AS1-5110',N'ចំណាយប្រេងប្រេងឥន្ធនៈ',N'',N'1'
 UNION ALL SELECT N'2',N'1',N'AS1-5120',N'ចំណាយប្រេងរំអិល',N'',N'1'
 UNION ALL SELECT N'3',N'12',N'AS12-4200/4300/4400',N'ដកចំណូលផ្សេងក្រៅពីប្រភពថ្លៃលក់អគ្គិសនី(លើកលែងប្រាក់កក់អតិថិជន)',N'',N'1'
 UNION ALL SELECT N'4',N'2',N'AS2-5000B',N'ចំណាយទិញថាមពល',N'',N'1'
 UNION ALL SELECT N'5',N'3',N'AS3-6000B',N'ចំណាយលើបុគ្គលិកផ្នែកចែកចាយ',N'',N'1'
 UNION ALL SELECT N'6',N'3',N'AS3-6000A',N'ចំណាយលើបុគ្គលិកផ្នែកបញ្ជូន',N'',N'1'
 UNION ALL SELECT N'7',N'3',N'AS3-6000C',N'ចំណាយលើបុគ្គលិកផ្នែកផលិតកម្ម',N'',N'1'
 UNION ALL SELECT N'8',N'4',N'AS4-5100A',N'ចំណាយដំណើរការនិងការថែទាំ',N'',N'1'
 UNION ALL SELECT N'9',N'4',N'AS4-5400',N'ចំណាយដំណើរការនិងការថែទាំ',N'',N'1'
 UNION ALL SELECT N'10',N'4',N'AS4-5600',N'ចំណាយដំណើរការនិងការថែទាំ',N'',N'1'
 UNION ALL SELECT N'11',N'5',N'AS5-0001',N'ចំណាយរដ្ឋបាលនិងការគ្រប់គ្រងទូទៅ',N'',N'1'
 UNION ALL SELECT N'12',N'5',N'AS5-6000',N'ចំណាយរដ្ឋបាលនិងការគ្រប់គ្រងទូទៅ',N'',N'1'
 UNION ALL SELECT N'13',N'6',N'AS6-0001',N'ប្រាក់កម្ចីសំរាប់អចលនទ្រព្យកំពុងប្រើប្រាស់',N'',N'1'
 UNION ALL SELECT N'14',N'6',N'AS6-0002',N'ប្រាក់កម្ចីសំរាប់អចលនទ្រព្យកំពុងប្រើប្រាស់',N'',N'1'
 UNION ALL SELECT N'15',N'6',N'AS6-0003',N'ប្រាក់កម្ចីសំរាប់អចលនទ្រព្យកំពុងប្រើប្រាស់',N'',N'1'
 UNION ALL SELECT N'16',N'6',N'AS6-0004',N'ប្រាក់កម្ចីសំរាប់អចលនទ្រព្យកំពុងប្រើប្រាស់',N'',N'1'
 UNION ALL SELECT N'17',N'6',N'AS6-7000A',N'ការប្រាក់-ផលិត',N'',N'1'
 UNION ALL SELECT N'18',N'7',N'AS7-1015',N'តម្លៃដើមសរុបនៃអចលនទ្រព្យ (ដកវិភាគទានអ្នកប្រើប្រាស់និងជំនួយ)',N'',N'1'
 UNION ALL SELECT N'19',N'7',N'AS7-1020',N'តម្លៃដើមសរុបនៃអចលនទ្រព្យ (ដកវិភាគទានអ្នកប្រើប្រាស់និងជំនួយ)',N'',N'1'
 UNION ALL SELECT N'20',N'7',N'AS7-1040',N'តម្លៃដើមសរុបនៃអចលនទ្រព្យ (ដកវិភាគទានអ្នកប្រើប្រាស់និងជំនួយ)',N'',N'1'
 UNION ALL SELECT N'21',N'7',N'AS7-1050/1080',N'តម្លៃដើមសរុបនៃអចលនទ្រព្យ (ដកវិភាគទានអ្នកប្រើប្រាស់និងជំនួយ)',N'',N'1'
 UNION ALL SELECT N'22',N'8',N'AS8-1105',N'ដក រំលស់បូកយោង',N'',N'1'
 UNION ALL SELECT N'23',N'8',N'AS8-1110',N'ដក រំលស់បូកយោង',N'',N'1'
 UNION ALL SELECT N'24',N'8',N'AS8-1130',N'ដក រំលោះបូកយោង',N'',N'1'
 UNION ALL SELECT N'25',N'8',N'AS8-1150/1170',N'ដក រំលោះបូកយោង',N'',N'1'
 UNION ALL SELECT N'26',N'8',N'AS8-5910/5960/5980',N'ចំណាយសម្រាប់រំលស់',N'',N'1'
 UNION ALL SELECT N'27',N'8',N'AS8-5920',N'ចំណាយសម្រាប់រំលស់',N'',N'1'
 UNION ALL SELECT N'28',N'8',N'AS8-5940',N'ចំណាយសម្រាប់រំលស់',N'',N'1'
 UNION ALL SELECT N'29',N'9',N'AS9-0001A',N'ប្រាក់ចំណេញសមស្រប',N'',N'1'
 UNION ALL SELECT N'30',N'9',N'AS9-0002',N'ប្រាក់ចំណេញសមស្រប',N'',N'1'
 UNION ALL SELECT N'31',N'9',N'AS9-0003',N'ប្រាក់ចំណេញសមស្រប',N'',N'1'
 UNION ALL SELECT N'32',N'6',N'AS6-7000B',N'ការប្រាក់-បញ្ជួន',N'',N'1'
 UNION ALL SELECT N'33',N'6',N'AS6-7000C',N'ការប្រាក់-ចែកចាយ',N'',N'1'
 UNION ALL SELECT N'34',N'0',N'AS_A',N'ចំណាយត្រឹមត្រូវសរុបសំរាប់ផ្នែកផលិតកម្ម',N'',N'1'
 UNION ALL SELECT N'35',N'0',N'AS_B',N'ចំណាយត្រឹមត្រូវសរុបសំរាប់ផ្នែកបញ្ជូន',N'',N'1'
 UNION ALL SELECT N'36',N'0',N'AS_C',N'ចំណាយត្រឹមត្រូវសរុបសំរាប់ផ្នែកចែកចាយ',N'',N'1'
 UNION ALL SELECT N'37',N'0',N'AS_D',N'ចំណាយសរុប (ផលិតកម្ម + បញ្ជូន + ចែកចាយ)',N'',N'1'
 UNION ALL SELECT N'38',N'0',N'AS_E',N'ចំណូលសរុបប្រចាំឆ្នាំដែលត្រូវមាន (ក+ខ+គ-ឃ)',N'',N'1'
 UNION ALL SELECT N'39',N'11',N'AS11-4000',N'ចំណូលសរុបដែលបានមកពីថ្លៃលក់បច្ចុប្បន្ន',N'',N'1'
 UNION ALL SELECT N'40',N'0',N'AS_F',N'ចំណេញ/(ខាត) មុនកែសម្រួលថ្លៃលក់ (ច-ង)',N'',N'1'
 UNION ALL SELECT N'41',N'0',N'AS_G',N'ផលប៉ះពាល់លើការកែសម្រួលថ្លៃលក់',N'',N'1'
 UNION ALL SELECT N'42',N'0',N'AS_H',N'ចំណេញ/(ខាត) ក្រោយកែសម្រួលថ្លៃលក់ (ច+ជ-ង)',N'',N'1'
 UNION ALL SELECT N'43',N'0',N'',N'សំវិធានធនសម្រាប់ទុនចល័ត',N'',N'1'
 UNION ALL SELECT N'44',N'0',N'AS9_A',N'តម្លៃអចលនទ្រព្យសរុប',N'',N'1'
 UNION ALL SELECT N'45',N'0',N'AS9_B',N'តម្លៃអចលនទ្រព្យសុទ្ធ',N'',N'1'
 UNION ALL SELECT N'46',N'0',N'AS9_C',N'មូលធនជាក៉ស្តែងផ្ទាល់ (៥ - ៦)',N'',N'1'
 UNION ALL SELECT N'47',N'0',N'',N'សំវិធានធនសម្រាប់ទុនចល័ត',N'',N'1'
 UNION ALL SELECT N'48',N'0',N'AS9_D',N'តម្លៃអចលនទ្រព្យសរុប',N'',N'1'
 UNION ALL SELECT N'49',N'0',N'AS9_E',N'តម្លៃអចលនទ្រព្យសុទ្ធ',N'',N'1'
 UNION ALL SELECT N'50',N'0',N'AS9_F',N'មូលធនជាក៉ស្តែងផ្ទាល់ (៥ - ៦)',N'',N'1'
 UNION ALL SELECT N'51',N'0',N'',N'សំវិធានធនសំរាប់ទុនចល័ត',N'',N'1'
 UNION ALL SELECT N'52',N'0',N'AS9_G',N'តម្លៃអចលនទ្រព្យសរុប',N'',N'1'
 UNION ALL SELECT N'53',N'0',N'AS9_H',N'តម្លៃអចលនទ្រព្យសុទ្ធ',N'',N'1'
 UNION ALL SELECT N'54',N'0',N'AS9_I',N'មូលធនជាក៉ស្តែងផ្ទាល់ (៥ - ៦)',N'',N'1'
 UNION ALL SELECT N'55',N'0',N'AS9_J',N'សំវិធានធនសំរាប់ទុនចល័ត',N'',N'1'
 UNION ALL SELECT N'56',N'0',N'AS9_K',N'តម្លៃអចលនទ្រព្យសរុប',N'',N'1'
 UNION ALL SELECT N'57',N'0',N'AS9_L',N'តម្លៃអចលនទ្រព្យសុទ្ធ',N'',N'1'
 UNION ALL SELECT N'58',N'0',N'AS9_M',N'មូលធនជាក៉ស្តែងផ្ទាល់ (៥ - ៦)',N'',N'1'
 SET IDENTITY_INSERT ANR.TBL_TABLE_CELL OFF;
END
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='TBL_CUSTOMER' AND COLUMN_NAME='IS_MV' )
	ALTER TABLE TBL_CUSTOMER ADD IS_MV BIT NULL;
GO
UPDATE TBL_CUSTOMER SET IS_MV=0 WHERE IS_MV IS NULL;
ALTER TABLE TBL_CUSTOMER ALTER COLUMN IS_MV BIT NOT NULL;
GO
-- **********************************************************************************
-- AS01A ***
-- **********************************************************************************
IF OBJECT_ID('ANR.TBL_RESULT_01') IS NULL
	CREATE TABLE ANR.TBL_RESULT_01(
		ID INT IDENTITY PRIMARY KEY,
		YEAR_ID INT NOT NULL,
		ROW_TYPE_ID INT NOT NULL,
		ROW_NO NVARCHAR(50) NOT NULL,
		ROW_TYPE_NAME NVARCHAR(100) NOT NULL,
		UNIT NVARCHAR(50) NOT NULL,
		ACCOUNT_CODE NVARCHAR(50) NOT NULL,
		JAN DECIMAL(18,4) NULL,
		FEB DECIMAL(18,4) NULL,
		MAR DECIMAL(18,4) NULL,
		APR DECIMAL(18,4) NULL,
		MAY DECIMAL(18,4) NULL,
		JUN DECIMAL(18,4) NULL,
		JUL DECIMAL(18,4) NULL,
		AUG DECIMAL(18,4) NULL,
		SEP DECIMAL(18,4) NULL,
		OCT DECIMAL(18,4) NULL,
		NOV DECIMAL(18,4) NULL,
		DEC DECIMAL(18,4) NULL,
		TOTAL DECIMAL(18,4) NULL,
		IS_ACTIVE BIT NOT NULL
	);
GO
IF OBJECT_ID('ANR.TBL_RESULT_01A') IS NULL
	CREATE TABLE ANR.TBL_RESULT_01A(
		ID INT IDENTITY PRIMARY KEY,
		YEAR_ID INT NOT NULL,
		ROW_NO NVARCHAR(50) NOT NULL,
		DESCRIPITOIN NVARCHAR(100) NOT NULL,
		UNIT NVARCHAR(100) NOT NULL,
		GENERATOR_1 NVARCHAR(100) NULL,
		GENERATOR_2 NVARCHAR(100) NULL,
		GENERATOR_3 NVARCHAR(100) NULL,
		GENERATOR_4 NVARCHAR(100) NULL,
		GENERATOR_5 NVARCHAR(100) NULL,
		GENERATOR_6 NVARCHAR(100) NULL,
		TOTAL NVARCHAR(100) NULL,
		IS_ACTIVE BIT NOT NULL,
	)
GO 
IF NOT EXISTS (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TBL_GENERATOR' AND COLUMN_NAME = 'HORSE_POWER')
	ALTER TABLE TBL_GENERATOR ADD HORSE_POWER DECIMAL(18,4) NULL;
GO
UPDATE TBL_GENERATOR SET HORSE_POWER = 0 WHERE HORSE_POWER IS NULL;
GO
ALTER TABLE TBL_GENERATOR ALTER COLUMN HORSE_POWER DECIMAL(18,4) NOT NULL;
GO
IF NOT EXISTS (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TBL_GENERATOR' AND COLUMN_NAME = 'OPERATION_HOUR')
	ALTER TABLE TBL_GENERATOR ADD OPERATION_HOUR DECIMAL(18,4) NULL;
GO
UPDATE TBL_GENERATOR SET OPERATION_HOUR = 0 WHERE OPERATION_HOUR IS NULL;
GO
ALTER TABLE TBL_GENERATOR ALTER COLUMN OPERATION_HOUR DECIMAL(18,4) NOT NULL;
GO
IF NOT EXISTS (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TBL_GENERATOR' AND COLUMN_NAME = 'SHIFT_1')
	ALTER TABLE TBL_GENERATOR ADD SHIFT_1 NVARCHAR(100) NULL;
GO
UPDATE TBL_GENERATOR SET SHIFT_1 = '' WHERE SHIFT_1 IS NULL;
GO
ALTER TABLE TBL_GENERATOR ALTER COLUMN SHIFT_1 NVARCHAR(100) NOT NULL;
GO
IF NOT EXISTS (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TBL_GENERATOR' AND COLUMN_NAME = 'SHIFT_2')
	ALTER TABLE TBL_GENERATOR ADD SHIFT_2 NVARCHAR(100) NULL;
GO
UPDATE TBL_GENERATOR SET SHIFT_2 = '' WHERE SHIFT_2 IS NULL;
GO
ALTER TABLE TBL_GENERATOR ALTER COLUMN SHIFT_2 NVARCHAR(100) NOT NULL;



-- **********************************************************************************
-- AS01B
-- **********************************************************************************
IF OBJECT_ID('ANR.TBL_RESULT_01B') IS NULL
	CREATE TABLE ANR.TBL_RESULT_01B(
		ID INT IDENTITY PRIMARY KEY,
		YEAR_ID INT NOT NULL,
		ROW_NO NVARCHAR(50) NOT NULL,
		DISTRIBUTION NVARCHAR(100) NOT NULL,
		PHASE NVARCHAR(50) NOT NULL,
		VOLTAGE NVARCHAR(50) NOT NULL,
		UNIT NVARCHAR(50) NOT NULL,
		SHIELDED_LENGTH DECIMAL(18,4) NOT NULL,
		UNSHIELDED_LENGTH DECIMAL(18,4) NOT NULL,
		ENDING_VOLTAGE NVARCHAR(50) NOT NULL,
		TOTAL_POLE DECIMAL(18,4) NOT NULL,
		IS_ACTIVE BIT NOT NULL
	);
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='TBL_DISTRIBUTION_FACILITY' AND COLUMN_NAME='IS_SHIELDED')
	ALTER TABLE TBL_DISTRIBUTION_FACILITY ADD IS_SHIELDED  BIT NULL;
GO
UPDATE TBL_DISTRIBUTION_FACILITY SET IS_SHIELDED =0 WHERE IS_SHIELDED  IS NULL;
GO
ALTER TABLE TBL_DISTRIBUTION_FACILITY ALTER COLUMN IS_SHIELDED  BIT NOT NULL;
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='TBL_DISTRIBUTION_FACILITY' AND COLUMN_NAME='TOTAL_POLE')
	ALTER TABLE TBL_DISTRIBUTION_FACILITY ADD TOTAL_POLE DECIMAL(18,4) NULL;
GO
UPDATE TBL_DISTRIBUTION_FACILITY SET TOTAL_POLE=0 WHERE TOTAL_POLE IS NULL;
GO
ALTER TABLE TBL_DISTRIBUTION_FACILITY ALTER COLUMN TOTAL_POLE DECIMAL(18,4) NOT NULL;
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='TBL_DISTRIBUTION_FACILITY' AND COLUMN_NAME='ENDING_VOLTAGE')
	ALTER TABLE TBL_DISTRIBUTION_FACILITY ADD ENDING_VOLTAGE NVARCHAR(50) NULL;
GO
UPDATE TBL_DISTRIBUTION_FACILITY SET ENDING_VOLTAGE='' WHERE ENDING_VOLTAGE IS NULL;
GO
ALTER TABLE TBL_DISTRIBUTION_FACILITY ALTER COLUMN ENDING_VOLTAGE NVARCHAR(50) NOT NULL;
GO 
-- **********************************************************************************
-- AS01C
-- **********************************************************************************
IF OBJECT_ID('ANR.TBL_RESULT_01C') IS NULL
	CREATE TABLE ANR.TBL_RESULT_01C(
		ID INT IDENTITY PRIMARY KEY,
		YEAR_ID INT NOT NULL,
		ROW_NO NVARCHAR(50) NOT NULL,
		TRANSFORMER_TYPE  NVARCHAR(100) NOT NULL,
		TRANSFORMER_POSITION NVARCHAR(100) NOT NULL,
		PHASE NVARCHAR(50) NOT NULL,
		HIGH_VOTLATE NVARCHAR(50) NOT NULL,
		LOW_VOLTAGE NVARCHAR(50) NOT NULL,
		CAPACITY NVARCHAR(50) NOT NULL,
		TOTAL INT NOT NULL,
		IS_ACTIVE BIT NOT NULL
	);
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='TBL_TRANSFORMER' AND COLUMN_NAME='HIGH_VOLTAGE_ID')
	ALTER TABLE TBL_TRANSFORMER ADD HIGH_VOLTAGE_ID INT NULL;
GO
UPDATE TBL_TRANSFORMER SET HIGH_VOLTAGE_ID = (SELECT VOLTAGE_ID FROM TBL_VOLTAGE WHERE VOLTAGE_NAME='22 KV') WHERE HIGH_VOLTAGE_ID IS NULL;
UPDATE TBL_TRANSFORMER SET HIGH_VOLTAGE_ID = 1 WHERE HIGH_VOLTAGE_ID IS NULL;
GO
ALTER TABLE TBL_TRANSFORMER ALTER COLUMN HIGH_VOLTAGE_ID INT NOT NULL;
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='TBL_TRANSFORMER' AND COLUMN_NAME='LOW_VOLTAGE_ID')
	ALTER TABLE TBL_TRANSFORMER ADD LOW_VOLTAGE_ID INT NULL;
GO
UPDATE TBL_TRANSFORMER SET LOW_VOLTAGE_ID = (SELECT VOLTAGE_ID FROM TBL_VOLTAGE WHERE VOLTAGE_NAME='220V') WHERE LOW_VOLTAGE_ID IS NULL;
UPDATE TBL_TRANSFORMER SET LOW_VOLTAGE_ID = 1 WHERE LOW_VOLTAGE_ID IS NULL; 
GO
ALTER TABLE TBL_TRANSFORMER ALTER COLUMN LOW_VOLTAGE_ID INT NOT NULL;
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='TBL_TRANSFORMER' AND COLUMN_NAME='TRANSFORMER_POSITION_ID')
	ALTER TABLE TBL_TRANSFORMER ADD TRANSFORMER_POSITION_ID INT NULL;
GO
UPDATE TBL_TRANSFORMER SET TRANSFORMER_POSITION_ID = 1 WHERE TRANSFORMER_POSITION_ID IS NULL; 
GO
ALTER TABLE TBL_TRANSFORMER ALTER COLUMN TRANSFORMER_POSITION_ID INT NOT NULL;
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='TBL_TRANSFORMER' AND COLUMN_NAME='TRANSFORMER_TYPE_ID')
	ALTER TABLE TBL_TRANSFORMER ADD TRANSFORMER_TYPE_ID INT NULL;
GO
UPDATE TBL_TRANSFORMER SET TRANSFORMER_TYPE_ID = 1 WHERE TRANSFORMER_TYPE_ID IS NULL; 
GO
ALTER TABLE TBL_TRANSFORMER ALTER COLUMN TRANSFORMER_TYPE_ID INT NOT NULL;
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='TBL_TRANSFORMER' AND COLUMN_NAME='IS_PROPERTY')
	ALTER TABLE TBL_TRANSFORMER ADD IS_PROPERTY BIT NULL;
GO
UPDATE TBL_TRANSFORMER SET IS_PROPERTY = 1 WHERE IS_PROPERTY IS NULL; 
GO
ALTER TABLE TBL_TRANSFORMER ALTER COLUMN IS_PROPERTY BIT NOT NULL;


-- ***********************************************************************************
-- AS02
-- ***********************************************************************************

IF OBJECT_ID('ANR.TBL_RESULT_02') IS NULL
	CREATE TABLE ANR.TBL_RESULT_02(
		ID INT IDENTITY PRIMARY KEY,
		YEAR_ID INT NOT NULL,
        CURRENCY_ID INT NOT NULL,
		SOURCE_ID INT NOT NULL,
		ROW_TYPE_ID INT NOT NULL,
		ROW_NO NVARCHAR(50) NOT NULL,
		SOURCE_NAME NVARCHAR(100) NOT NULL,
		ROW_TYPE_NAME NVARCHAR(100) NOT NULL,
		ACCOUNT_CODE NVARCHAR(50) NOT NULL,
		JAN DECIMAL(18,4) NULL,
		FEB DECIMAL(18,4) NULL,
		MAR DECIMAL(18,4) NULL,
		APR DECIMAL(18,4) NULL,
		MAY DECIMAL(18,4) NULL,
		JUN DECIMAL(18,4) NULL,
		JUL DECIMAL(18,4) NULL,
		AUG DECIMAL(18,4) NULL,
		SEP DECIMAL(18,4) NULL,
		OCT DECIMAL(18,4) NULL,
		NOV DECIMAL(18,4) NULL,
		DEC DECIMAL(18,4) NULL,
		TOTAL DECIMAL(18,4) NULL,
		IS_ACTIVE BIT NOT NULL
	); 
GO 
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='TBL_TRANSFORMER' AND COLUMN_NAME='HIGH_VOLTAGE_ID')
	ALTER TABLE TBL_TRANSFORMER ADD HIGH_VOLTAGE_ID INT NULL;
GO
UPDATE TBL_TRANSFORMER SET HIGH_VOLTAGE_ID = (SELECT VOLTAGE_ID FROM TBL_VOLTAGE WHERE VOLTAGE_NAME='22 KV') WHERE HIGH_VOLTAGE_ID IS NULL;
UPDATE TBL_TRANSFORMER SET HIGH_VOLTAGE_ID = 1 WHERE HIGH_VOLTAGE_ID IS NULL;
GO
ALTER TABLE TBL_TRANSFORMER ALTER COLUMN HIGH_VOLTAGE_ID INT NOT NULL;
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='TBL_TRANSFORMER' AND COLUMN_NAME='LOW_VOLTAGE_ID')
	ALTER TABLE TBL_TRANSFORMER ADD LOW_VOLTAGE_ID INT NULL;
GO
UPDATE TBL_TRANSFORMER SET LOW_VOLTAGE_ID = (SELECT VOLTAGE_ID FROM TBL_VOLTAGE WHERE VOLTAGE_NAME='220V') WHERE LOW_VOLTAGE_ID IS NULL;
UPDATE TBL_TRANSFORMER SET LOW_VOLTAGE_ID = 1 WHERE LOW_VOLTAGE_ID IS NULL; 
GO
ALTER TABLE TBL_TRANSFORMER ALTER COLUMN LOW_VOLTAGE_ID INT NOT NULL;
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='TBL_TRANSFORMER' AND COLUMN_NAME='TRANSFORMER_POSITION_ID')
	ALTER TABLE TBL_TRANSFORMER ADD TRANSFORMER_POSITION_ID INT NULL;
GO
UPDATE TBL_TRANSFORMER SET TRANSFORMER_POSITION_ID = 10101 WHERE TRANSFORMER_POSITION_ID IS NULL; 
GO
ALTER TABLE TBL_TRANSFORMER ALTER COLUMN TRANSFORMER_POSITION_ID INT NOT NULL;
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='TBL_TRANSFORMER' AND COLUMN_NAME='TRANSFORMER_TYPE_ID')
	ALTER TABLE TBL_TRANSFORMER ADD TRANSFORMER_TYPE_ID INT NULL;
GO
UPDATE TBL_TRANSFORMER SET TRANSFORMER_TYPE_ID = 10001 WHERE TRANSFORMER_TYPE_ID IS NULL; 
GO
ALTER TABLE TBL_TRANSFORMER ALTER COLUMN TRANSFORMER_TYPE_ID INT NOT NULL;
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='TBL_TRANSFORMER' AND COLUMN_NAME='IS_PROPERTY')
	ALTER TABLE TBL_TRANSFORMER ADD IS_PROPERTY BIT NULL;
GO
UPDATE TBL_TRANSFORMER SET IS_PROPERTY = 1 WHERE IS_PROPERTY IS NULL; 
GO
ALTER TABLE TBL_TRANSFORMER ALTER COLUMN IS_PROPERTY BIT NOT NULL;
GO

-- ***********************************************************************************
-- AS02 
-- ***********************************************************************************

IF OBJECT_ID('ANR.TBL_RESULT_02') IS NULL
	CREATE TABLE ANR.TBL_RESULT_02(
		ID INT IDENTITY PRIMARY KEY,
		YEAR_ID INT NOT NULL,
		SOURCE_ID INT NOT NULL,
		SOURCE_NAME NVARCHAR(100) NOT NULL,
		ROW_TYPE_ID INT NOT NULL,
		ROW_TYPE_NAME NVARCHAR(100) NOT NULL,
		ACCOUNT_CODE NVARCHAR(50) NOT NULL,
		JAN DECIMAL(18,4) NOT NULL,
		FEB DECIMAL(18,4) NOT NULL,
		MAR DECIMAL(18,4) NOT NULL,
		APR DECIMAL(18,4) NOT NULL,
		MAY DECIMAL(18,4) NOT NULL,
		JUN DECIMAL(18,4) NOT NULL,
		JUL DECIMAL(18,4) NOT NULL,
		AUG DECIMAL(18,4) NOT NULL,
		SEP DECIMAL(18,4) NOT NULL,
		OCT DECIMAL(18,4) NOT NULL,
		NOV DECIMAL(18,4) NOT NULL,
		DEC DECIMAL(18,4) NOT NULL,
		TOTAL DECIMAL(18,4) NOT NULL,
		IS_ACTIVE BIT NOT NULL
	); 
GO 
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='TBL_TRANSFORMER' AND COLUMN_NAME='HIGH_VOLTAGE_ID')
	ALTER TABLE TBL_TRANSFORMER ADD HIGH_VOLTAGE_ID INT NULL;
GO
UPDATE TBL_TRANSFORMER SET HIGH_VOLTAGE_ID = (SELECT VOLTAGE_ID FROM TBL_VOLTAGE WHERE VOLTAGE_NAME='22 KV') WHERE HIGH_VOLTAGE_ID IS NULL;
UPDATE TBL_TRANSFORMER SET HIGH_VOLTAGE_ID = 1 WHERE HIGH_VOLTAGE_ID IS NULL;
GO
ALTER TABLE TBL_TRANSFORMER ALTER COLUMN HIGH_VOLTAGE_ID INT NOT NULL;
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='TBL_TRANSFORMER' AND COLUMN_NAME='LOW_VOLTAGE_ID')
	ALTER TABLE TBL_TRANSFORMER ADD LOW_VOLTAGE_ID INT NULL;
GO
UPDATE TBL_TRANSFORMER SET LOW_VOLTAGE_ID = (SELECT VOLTAGE_ID FROM TBL_VOLTAGE WHERE VOLTAGE_NAME='220V') WHERE LOW_VOLTAGE_ID IS NULL;
UPDATE TBL_TRANSFORMER SET LOW_VOLTAGE_ID = 1 WHERE LOW_VOLTAGE_ID IS NULL; 
GO
ALTER TABLE TBL_TRANSFORMER ALTER COLUMN LOW_VOLTAGE_ID INT NOT NULL;
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='TBL_TRANSFORMER' AND COLUMN_NAME='TRANSFORMER_POSITION_ID')
	ALTER TABLE TBL_TRANSFORMER ADD TRANSFORMER_POSITION_ID INT NULL;
GO
UPDATE TBL_TRANSFORMER SET TRANSFORMER_POSITION_ID = 1 WHERE TRANSFORMER_POSITION_ID IS NULL; 
GO
ALTER TABLE TBL_TRANSFORMER ALTER COLUMN TRANSFORMER_POSITION_ID INT NOT NULL;
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='TBL_TRANSFORMER' AND COLUMN_NAME='TRANSFORMER_TYPE_ID')
	ALTER TABLE TBL_TRANSFORMER ADD TRANSFORMER_TYPE_ID INT NULL;
GO
UPDATE TBL_TRANSFORMER SET TRANSFORMER_TYPE_ID = 1 WHERE TRANSFORMER_TYPE_ID IS NULL; 
GO
ALTER TABLE TBL_TRANSFORMER ALTER COLUMN TRANSFORMER_TYPE_ID INT NOT NULL;
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='TBL_TRANSFORMER' AND COLUMN_NAME='IS_PROPERTY')
	ALTER TABLE TBL_TRANSFORMER ADD IS_PROPERTY BIT NULL;
GO
UPDATE TBL_TRANSFORMER SET IS_PROPERTY = 1 WHERE IS_PROPERTY IS NULL; 
GO
ALTER TABLE TBL_TRANSFORMER ALTER COLUMN IS_PROPERTY BIT NOT NULL;
GO

-- ************************************************************************************
-- AS03
-- ************************************************************************************

IF OBJECT_ID('ANR.TBL_RESULT_03') IS NULL
	CREATE TABLE ANR.TBL_RESULT_03(
		ID INT IDENTITY PRIMARY KEY,
		YEAR_ID INT NOT NULL,
        CELL_ID INT NOT NULL,
		ROW_NO NVARCHAR(50) NOT NULL,
		DIVISION_ID INT NOT NULL,
		DIVISION_NAME NVARCHAR(100) NOT NULL,
		POSITION_NAME NVARCHAR(100) NOT NULL,
		TOTAL_STAFF DECIMAL(18,4) NULL,
		S6010 DECIMAL(18,4) NULL,-- 6010
		S6020 DECIMAL(18,4) NULL,-- 6020
		S6040 DECIMAL(18,4) NULL,-- 6040
		S6050 DECIMAL(18,4) NULL,-- 6050
		S6060 DECIMAL(18,4) NULL,-- 6060
		S6070 DECIMAL(18,4) NULL,-- 6070
		TOTAL DECIMAL(18,4) NULL,
		IS_ACTIVE BIT NOT NULL
	);
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='TBL_POSITION' AND COLUMN_NAME='IS_PAYROLL') 
	ALTER TABLE TBL_POSITION ADD IS_PAYROLL BIT NULL;
GO
UPDATE TBL_POSITION SET IS_PAYROLL=0 WHERE IS_PAYROLL IS NULL;
GO
ALTER TABLE TBL_POSITION ALTER COLUMN IS_PAYROLL BIT NOT NULL;
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='TBL_POSITION' AND COLUMN_NAME='BUSINESS_DIVISION_ID') 
	ALTER TABLE TBL_POSITION ADD BUSINESS_DIVISION_ID INT NULL;
GO
/*ផ្នែកចែកចាយ*/
UPDATE TBL_POSITION SET BUSINESS_DIVISION_ID=10203 WHERE BUSINESS_DIVISION_ID IS NULL;
GO
ALTER TABLE TBL_POSITION ALTER COLUMN BUSINESS_DIVISION_ID INT NOT NULL;
GO
IF (SELECT COUNT(*) FROM TBL_POSITION)=3 
	INSERT INTO TBL_POSITION
	SELECT N'អ្នកគ្រប់គ្រងទូទៅ',0,1,1,10204 
	UNION ALL SELECT N'គណនេយ្យករ',0,1,1,10204 
	UNION ALL SELECT N'បេឡាករ',0,1,1,10204 
	UNION ALL SELECT N'ប្រធានផ្នែកផលិត',0,1,1,10201
	UNION ALL SELECT N'បុគ្គលិកផ្នែកផលិត',0,1,1,10201
	UNION ALL SELECT N'បុគ្គលិកផ្នែកផលិត(២)',0,1,1,10201
	UNION ALL SELECT N'ប្រធានផ្នែកបញ្ជូន',0,1,1,10202
	UNION ALL SELECT N'បុគ្គលិកផ្នែកបញ្ជូន',0,1,1,10202
	UNION ALL SELECT N'បុគ្គលិកផ្នែកបញ្ជូន(២)',0,1,1,10202
	UNION ALL SELECT N'ប្រធានផ្នែកចែកចាយ',0,1,1,10203
	UNION ALL SELECT N'បុគ្គលិកផ្នែកចែកចាយ',0,1,1,10203
	UNION ALL SELECT N'បុគ្គលិកផ្នែកចែកចាយ(២)',0,1,1,10203

GO
IF OBJECT_ID('TBL_STAFF_EXPENSE') IS NULL
	CREATE TABLE TBL_STAFF_EXPENSE(
		EXPENSE_ID INT IDENTITY PRIMARY KEY,
		EXPENSE_DATE DATETIME NOT NULL,
		CURRENCY_ID INT NOT NULL,
		TOTAL_AMOUNT DECIMAL(18,4) NOT NULL,
		NOTE NVARCHAR(MAX) NOT NULL,
		PAYMENT_ACCOUNT_ID INT NOT NULL,
		CREATE_BY NVARCHAR(50) NOT NULL,
		CREATE_ON DATETIME NOT NULL,
		IS_ACTIVE BIT NOT NULL
	)
GO
IF OBJECT_ID('TBL_STAFF_EXPENSE_DETAIL') IS NULL
	CREATE TABLE TBL_STAFF_EXPENSE_DETAIL(
		EXPENSE_DETAIL_ID INT IDENTITY PRIMARY KEY,	
		EXPENSE_ID INT NOT NULL,
		POSITION_ID INT NOT NULL,
		TOTAL_STAFF DECIMAL(18,4) NOT NULL,
		S6010 DECIMAL(18,4) NOT NULL,-- 6010
		S6020 DECIMAL(18,4) NOT NULL,-- 6020
		S6040 DECIMAL(18,4) NOT NULL,-- 6040
		S6050 DECIMAL(18,4) NOT NULL,-- 6050
		S6060 DECIMAL(18,4) NOT NULL,-- 6060
		S6070 DECIMAL(18,4) NOT NULL,-- 6070
		TOTAL DECIMAL(18,4) NOT NULL,
		IS_ACTIVE BIT NOT NULL,
	)
GO

-- **************************************************************************
-- AS04
-- **************************************************************************

IF OBJECT_ID('ANR.TBL_REPORT_ITEM') IS NULL
CREATE TABLE ANR.TBL_REPORT_ITEM(
	ITEM_ID INT IDENTITY PRIMARY KEY,
	TABLE_ID INT NOT NULL,
	ITEM_CODE NVARCHAR(50) NOT NULL,
	ITEM_NAME NVARCHAR(100) NOT NULL,
	ACCOUNT_CODE NVARCHAR(100) NOT NULL,
	PARENT_ID INT NOT NULL,
	IS_ACTIVE BIT NOT NULL
);

IF OBJECT_ID('ANR.TBL_REPORT_ITEM_DETAIL') IS NULL
CREATE TABLE ANR.TBL_REPORT_ITEM_DETAIL(	
	ITEM_DETAIL_ID INT IDENTITY PRIMARY KEY,
	ITEM_ID INT NOT NULL,
	ACCOUNT_CODE NVARCHAR(50) NOT NULL,
	BUSINESS_DIVISION_ID INT NOT NULL,
	CELL_ID INT NOT NULL,-- 0 IF NOT CALCULATE
	NOTE NVARCHAR(50) NOT NULL,
	IS_ACTIVE BIT NOT NULL,
);
 
GO

SET IDENTITY_INSERT ANR.TBL_REPORT_ITEM ON;
IF NOT EXISTS(SELECT * FROM ANR.TBL_REPORT_ITEM)
INSERT INTO ANR.TBL_REPORT_ITEM(ITEM_ID,TABLE_ID,ITEM_CODE,ITEM_NAME,ACCOUNT_CODE,PARENT_ID,IS_ACTIVE)
SELECT N'16',N'4',N'1',N'គ្រឿងបន្លាស់',N'5240/5435/5635',N'0',N'1'
UNION ALL SELECT N'17',N'4',N'2',N'សម្ភារៈ និងការផ្គត់ផ្គង់',N'5250/5450/5645',N'0',N'1'
UNION ALL SELECT N'18',N'4',N'3',N'ជួសជុល និងថែទាំ',N'5230/5440/5640',N'0',N'1'
UNION ALL SELECT N'19',N'4',N'4',N'ជួល',N'5220/5460/5680',N'0',N'1'
UNION ALL SELECT N'20',N'4',N'5',N'ទឹក',N'5130',N'0',N'1'
UNION ALL SELECT N'21',N'4',N'6',N'បំណុលទារមិនបាន',N'5840',N'0',N'1'
UNION ALL SELECT N'22',N'4',N'7',N'ចំណាយគ្រប់គ្រង និងវិស្វកម្ម',N'5200/5410/5610',N'0',N'1'
UNION ALL SELECT N'23',N'4',N'8',N'ធានារ៉ាប់រង',N'5210',N'0',N'1'
UNION ALL SELECT N'24',N'4',N'9',N'ចំណាយផ្នែកបែងចែកបន្ទុក',N'5420/5620',N'0',N'1'
UNION ALL SELECT N'25',N'4',N'10',N'ចំណាយផ្នែកបន្ទប់ភ្លើង',N'5430/5630',N'0',N'1'
UNION ALL SELECT N'26',N'4',N'11',N'ចំណាយបំភ្លឺសាធារណៈ និងប្រព័ន្ធឱ្យសញ្ញាផ្សេងៗ',N'5650',N'0',N'1'
UNION ALL SELECT N'27',N'4',N'12',N'ចំណាយលើការតម្លើងជូនអតិថិជន',N'5670',N'0',N'1'
UNION ALL SELECT N'28',N'4',N'13',N'ចំណាយលើការថែទាំនាឡិការស្ទង់',N'5660',N'0',N'1'
UNION ALL SELECT N'29',N'4',N'13',N'ចំណាយលើការថែទាំនាឡិការស្ទង់',N'5660',N'0',N'1'
UNION ALL SELECT N'30',N'4',N'14',N'ចំណាយផ្សេងៗ',N'5290/5490/5690',N'0',N'1'
UNION ALL SELECT N'31',N'5',N'1',N'សេវាធនាគារ',N'6450',N'0',N'1'
UNION ALL SELECT N'32',N'5',N'2',N'កម្រៃអាជ្ញាប័ណ្ណ',N'6360',N'0',N'1'
UNION ALL SELECT N'33',N'5',N'3',N'សេវាសនកម្ម និងច្បាប់',N'6350',N'0',N'1'
UNION ALL SELECT N'34',N'5',N'4',N'ការដឹកជញ្ជូន និងការធ្វើដំណើរក្នុងស្រុក',N'6130',N'0',N'1'
UNION ALL SELECT N'35',N'5',N'5',N'ចំណាយលើប្រេងឥន្ធនៈបុគ្គលិក',N'6140',N'0',N'1'
UNION ALL SELECT N'36',N'5',N'6',N'ចំណាយលើបេសកកម្មក្រៅប្រទេស',N'6110',N'0',N'1'
UNION ALL SELECT N'37',N'5',N'7',N'ចំណាយលើបេសកកម្ម និងការទទួលភ្ញៀវ',N'6120',N'0',N'1'
UNION ALL SELECT N'38',N'5',N'8',N'សេវាប្រៃសណីយ៍ និងទូរគមនាគមន៍',N'6160',N'0',N'1'
UNION ALL SELECT N'39',N'5',N'9',N'ចំណាយសម្ភារៈការិយាល័យទូទៅ',N'6150',N'0',N'1'
UNION ALL SELECT N'40',N'5',N'10',N'បណ្តុះបណ្តាល និងអភិវឌ្ឍន៍បុគ្គលិក',N'6100',N'0',N'1'
UNION ALL SELECT N'41',N'5',N'11',N'ចំណាយលើបុគ្គលិកបណ្តែត',N'6030',N'0',N'1'
UNION ALL SELECT N'42',N'5',N'12',N'កម្រៃជើងសារ',N'5850',N'0',N'1'
UNION ALL SELECT N'43',N'5',N'13',N'ជួល',N'6300',N'0',N'1'
UNION ALL SELECT N'44',N'5',N'14',N'ទឹក-ភ្លើង',N'6310',N'0',N'1'
UNION ALL SELECT N'45',N'5',N'15',N'ជួសជុល និងថែទាំ',N'6320',N'0',N'1'
UNION ALL SELECT N'46',N'5',N'16',N'ចំណាយលើការគ្រប់គ្រង',N'6370',N'0',N'1'
UNION ALL SELECT N'47',N'5',N'17',N'ធានារ៉ាប់រង',N'6400',N'0',N'1'
UNION ALL SELECT N'48',N'5',N'18',N'សំណងលើការខូចខាត និងគ្រោះថ្នាក់ការងារ',N'6410',N'0',N'1'
UNION ALL SELECT N'49',N'5',N'19',N'ពិធីជប់លៀង និងការសំដែង',N'6500',N'0',N'1'
UNION ALL SELECT N'50',N'5',N'20',N'អំណោយ និងវិភាគទាន',N'6510',N'0',N'1'
UNION ALL SELECT N'51',N'5',N'20',N'ពន្ធ',N'6200',N'0',N'1'
UNION ALL SELECT N'52',N'5',N'',N'ពន្ធលើប្រេងឥន្ធនៈ',N'6210',N'51',N'1'
UNION ALL SELECT N'53',N'5',N'',N'ពន្ធលើផលរបរ',N'6220',N'51',N'1'
UNION ALL SELECT N'54',N'5',N'',N'ពន្ធប៉ាតង់',N'6230',N'51',N'1'
UNION ALL SELECT N'55',N'5',N'',N'ពន្ធនាំចូល',N'6240',N'51',N'1'
UNION ALL SELECT N'56',N'5',N'',N'ពន្ធលើមធ្យោបាយដឹកជញ្ជូន',N'6250',N'51',N'1'
UNION ALL SELECT N'57',N'5',N'',N'ពន្ធផ្សេងៗទៀត',N'6290',N'51',N'1'
UNION ALL SELECT N'58',N'5',N'22',N'ចំណាយទូទៅផ្សេងៗ',N'6590',N'0',N'1'
SET IDENTITY_INSERT ANR.TBL_REPORT_ITEM OFF;

GO

SET IDENTITY_INSERT ANR.TBL_REPORT_ITEM_DETAIL ON;
IF NOT EXISTS(SELECT * FROM ANR.TBL_REPORT_ITEM_DETAIL)
INSERT INTO ANR.TBL_REPORT_ITEM_DETAIL(ITEM_DETAIL_ID,ITEM_ID,ACCOUNT_CODE,BUSINESS_DIVISION_ID,NOTE,IS_ACTIVE)
SELECT N'1',N'20',N'5130',N'10201',N'ទឹក',N'1'
UNION ALL SELECT N'2',N'22',N'5200',N'10201',N'ចំណាយគ្រប់គ្រង និងវិស្វកម្ម',N'1'
UNION ALL SELECT N'3',N'22',N'5410',N'10202',N'ចំណាយគ្រប់គ្រង និងវិស្វកម្ម',N'1'
UNION ALL SELECT N'4',N'22',N'5610',N'10203',N'ចំណាយគ្រប់គ្រង និងវិស្វកម្ម',N'1'
UNION ALL SELECT N'5',N'23',N'5210',N'10201',N'ធានារ៉ាប់រង',N'1'
UNION ALL SELECT N'6',N'19',N'5220',N'10201',N'ជួល',N'1'
UNION ALL SELECT N'7',N'19',N'5460',N'10202',N'ជួល',N'1'
UNION ALL SELECT N'8',N'19',N'5680',N'10203',N'ជួល',N'1'
UNION ALL SELECT N'9',N'18',N'5230',N'10201',N'ជួសជុល និងថែទាំ',N'1'
UNION ALL SELECT N'10',N'18',N'5440',N'10202',N'ជួសជុល និងថែទាំ',N'1'
UNION ALL SELECT N'11',N'18',N'5640',N'10203',N'ជួសជុល និងថែទាំ',N'1'
UNION ALL SELECT N'12',N'16',N'5240',N'10201',N'គ្រឿងបន្លាស់',N'1'
UNION ALL SELECT N'13',N'16',N'5435',N'10202',N'គ្រឿងបន្លាស់',N'1'
UNION ALL SELECT N'14',N'16',N'5635',N'10203',N'គ្រឿងបន្លាស់',N'1'
UNION ALL SELECT N'15',N'17',N'5250',N'10201',N'សម្ភារៈ និងការផ្គត់ផ្គង់',N'1'
UNION ALL SELECT N'16',N'17',N'5450',N'10202',N'សម្ភារៈ និងការផ្គត់ផ្គង់',N'1'
UNION ALL SELECT N'17',N'17',N'5645',N'10203',N'សម្ភារៈ និងការផ្គត់ផ្គង់',N'1'
UNION ALL SELECT N'18',N'30',N'5290',N'10201',N'ចំណាយផ្សេងៗ',N'1'
UNION ALL SELECT N'19',N'30',N'5490',N'10202',N'ចំណាយផ្សេងៗ',N'1'
UNION ALL SELECT N'20',N'30',N'5690',N'10203',N'ចំណាយផ្សេងៗ',N'1'
UNION ALL SELECT N'21',N'24',N'5420',N'10202',N'ចំណាយផ្នែកបែងចែកបន្ទុក',N'1'
UNION ALL SELECT N'22',N'24',N'5620',N'10203',N'ចំណាយផ្នែកបែងចែកបន្ទុក',N'1'
UNION ALL SELECT N'23',N'25',N'5430',N'10202',N'ចំណាយផ្នែកបន្ទប់ភ្លើង',N'1'
UNION ALL SELECT N'24',N'25',N'5630',N'10203',N'ចំណាយផ្នែកបន្ទប់ភ្លើង',N'1'
UNION ALL SELECT N'25',N'26',N'5650',N'10203',N'ចំណាយបំភ្លឺសាធារណៈ និងប្រព័ន្ធឱ្យសញ្ញាផ្សេងៗ',N'1'
UNION ALL SELECT N'26',N'28',N'5660',N'10203',N'ចំណាយលើការថែទាំនាឡិការស្ទង់',N'1'
UNION ALL SELECT N'27',N'29',N'5660',N'10203',N'ចំណាយលើការថែទាំនាឡិការស្ទង់',N'1'
UNION ALL SELECT N'28',N'27',N'5670',N'10203',N'ចំណាយលើការតម្លើងជូនអតិថិជន',N'1'
UNION ALL SELECT N'29',N'21',N'5840',N'10203',N'បំណុលទារមិនបាន',N'1'
UNION ALL SELECT N'30',N'42',N'5850',N'10203',N'កម្រៃជើងសារ',N'1'
UNION ALL SELECT N'31',N'41',N'6030',N'10204',N'ចំណាយលើបុគ្គលិកបណ្តែត',N'1'
UNION ALL SELECT N'32',N'40',N'6100',N'10204',N'បណ្តុះបណ្តាល និងអភិវឌ្ឍន៍បុគ្គលិក',N'1'
UNION ALL SELECT N'33',N'36',N'6110',N'10204',N'ចំណាយលើបេសកកម្មក្រៅប្រទេស',N'1'
UNION ALL SELECT N'34',N'37',N'6120',N'10204',N'ចំណាយលើបេសកកម្ម និងការទទួលភ្ញៀវ',N'1'
UNION ALL SELECT N'35',N'34',N'6130',N'10204',N'ការដឹកជញ្ជូន និងការធ្វើដំណើរក្នុងស្រុក',N'1'
UNION ALL SELECT N'36',N'35',N'6140',N'10204',N'ចំណាយលើប្រេងឥន្ធនៈបុគ្គលិក',N'1'
UNION ALL SELECT N'37',N'39',N'6150',N'10204',N'ចំណាយសម្ភារៈការិយាល័យទូទៅ',N'1'
UNION ALL SELECT N'38',N'38',N'6160',N'10204',N'សេវាប្រៃសណីយ៍ និងទូរគមនាគមន៍',N'1'
UNION ALL SELECT N'39',N'51',N'6200',N'10204',N'ពន្ធ',N'1'
UNION ALL SELECT N'40',N'52',N'6210',N'10204',N'ពន្ធលើប្រេងឥន្ធនៈ',N'1'
UNION ALL SELECT N'41',N'53',N'6220',N'10204',N'ពន្ធលើផលរបរ',N'1'
UNION ALL SELECT N'42',N'54',N'6230',N'10204',N'ពន្ធប៉ាតង់',N'1'
UNION ALL SELECT N'43',N'55',N'6240',N'10204',N'ពន្ធនាំចូល',N'1'
UNION ALL SELECT N'44',N'56',N'6250',N'10204',N'ពន្ធលើមធ្យោបាយដឹកជញ្ជូន',N'1'
UNION ALL SELECT N'45',N'57',N'6290',N'10204',N'ពន្ធផ្សេងៗទៀត',N'1'
UNION ALL SELECT N'46',N'43',N'6300',N'10204',N'ជួល',N'1'
UNION ALL SELECT N'47',N'44',N'6310',N'10204',N'ទឹក-ភ្លើង',N'1'
UNION ALL SELECT N'48',N'45',N'6320',N'10204',N'ជួសជុល និងថែទាំ',N'1'
UNION ALL SELECT N'49',N'33',N'6350',N'10204',N'សេវាសនកម្ម និងច្បាប់',N'1'
UNION ALL SELECT N'50',N'32',N'6360',N'10204',N'កម្រៃអាជ្ញាប័ណ្ណ',N'1'
UNION ALL SELECT N'51',N'46',N'6370',N'10204',N'ចំណាយលើការគ្រប់គ្រង',N'1'
UNION ALL SELECT N'52',N'47',N'6400',N'10204',N'ធានារ៉ាប់រង',N'1'
UNION ALL SELECT N'53',N'48',N'6410',N'10204',N'សំណងលើការខូចខាត និងគ្រោះថ្នាក់ការងារ',N'1'
UNION ALL SELECT N'54',N'31',N'6450',N'10204',N'សេវាធនាគារ',N'1'
UNION ALL SELECT N'55',N'49',N'6500',N'10204',N'ពិធីជប់លៀង និងការសំដែង',N'1'
UNION ALL SELECT N'56',N'50',N'6510',N'10204',N'អំណោយ និងវិភាគទាន',N'1'
UNION ALL SELECT N'57',N'58',N'6590',N'10204',N'ចំណាយទូទៅផ្សេងៗ',N'1'
SET IDENTITY_INSERT ANR.TBL_REPORT_ITEM_DETAIL OFF;
GO
UPDATE TBL_ACCOUNT_CHART SET ACCOUNT_NAME=N'ពន្ធលើប្រេងឥន្ធនៈ' WHERE ACCOUNT_CODE='6210'
UPDATE TBL_ACCOUNT_CHART SET ACCOUNT_NAME=N'ពន្ធលើផលរបរ' WHERE ACCOUNT_CODE='6220'
UPDATE TBL_ACCOUNT_CHART SET ACCOUNT_NAME=N'ពន្ធប៉ាតង់' WHERE ACCOUNT_CODE='6230'
UPDATE TBL_ACCOUNT_CHART SET ACCOUNT_NAME=N'ពន្ធនាំចូល' WHERE ACCOUNT_CODE='6240'
UPDATE TBL_ACCOUNT_CHART SET ACCOUNT_NAME=N'ពន្ធលើមធ្យោបាយដឹកជញ្ជូន' WHERE ACCOUNT_CODE='6250'
UPDATE TBL_ACCOUNT_CHART SET ACCOUNT_NAME=N'ពន្ធផ្សេងៗទៀត' WHERE ACCOUNT_CODE='6290' 
GO
IF OBJECT_ID('ANR.TBL_RESULT_04') IS NULL
	CREATE TABLE ANR.TBL_RESULT_04(
		ID INT IDENTITY PRIMARY KEY,
		YEAR_ID INT NOT NULL,
		ROW_NO NVARCHAR(50) NULL,
		ITEM_NAME NVARCHAR(100) NULL,
		ACCOUNT_CODE NVARCHAR(100) NULL,
		PRODUCTION DECIMAL(18,4) NULL,
		TRANSMISSION DECIMAL(18,4) NULL,
		DISTRIBUTION DECIMAL(18,4) NULL,
		TOTAL DECIMAL(18,4) NULL,
		IS_ACTIVE BIT NOT NULL
	);
GO
-- **************************************************************************
-- AS05
-- **************************************************************************
IF OBJECT_ID('ANR.TBL_RESULT_05') IS NULL
	CREATE TABLE ANR.TBL_RESULT_05(
		ID INT IDENTITY PRIMARY KEY,
		YEAR_ID INT NOT NULL,
		ROW_NO NVARCHAR(50) NULL,
		ITEM_NAME NVARCHAR(100) NULL,
		ACCOUNT_CODE NVARCHAR(100) NULL,
		TOTAL DECIMAL(18,4) NULL,
		IS_ACTIVE BIT NOT NULL
	);
GO
-- **************************************************************************
-- AS06
-- **************************************************************************
IF OBJECT_ID('ANR.TBL_RESULT_06') IS NULL
	CREATE TABLE ANR.TBL_RESULT_06(
		ID INT IDENTITY PRIMARY KEY,
		YEAR_ID INT NOT NULL,
		DIVISION_ID NVARCHAR(50) NULL,
		DIVISION_NAME NVARCHAR(100) NULL,
		ROW_NO NVARCHAR(50) NULL,
		ACCOUNT_CODE NVARCHAR(50) NULL,

		REF_NO NVARCHAR(50) NULL,
		BANK_NAME NVARCHAR(100) NULL,
		LOAN_DATE DATETIME NULL,
		LOAN_AMOUNT DECIMAL(18,4) NULL,
		CLOSE_DATE DATETIME NULL,
		ANNUAL_INTEREST_RATE DECIMAL(18,4) NULL,
		BEGINING_BALANCE DECIMAL(18,4) NULL,
		INYEAR_LOAN DECIMAL(18,4) NULL,
		INYEAR_PAYMENT DECIMAL(18,4) NULL,
		ENDING_BALANCE DECIMAL(18,4) NULL,
		INYEAR_INTEREST DECIMAL(18,4) NULL,	

		IS_ACTIVE BIT NOT NULL
	);

GO
IF OBJECT_ID('TBL_LOAN') IS NULL
CREATE TABLE TBL_LOAN(
	LOAN_ID INT IDENTITY PRIMARY KEY,
	CURRENCY_ID INT NOT NULL,
	BUSINESS_DIVISION_ID INT NOT NULL,
	LOAN_NO NVARCHAR(50) NOT NULL,
	LOAN_DATE DATETIME NOT NULL,
	LOAN_AMOUNT DECIMAL(18,4) NOT NULL,
	PAID_AMOUNT DECIMAL(18,4) NOT NULL,
	BANK_NAME NVARCHAR(100) NOT NULL,
	ANNUAL_INTEREST_RATE DECIMAL(18,4) NOT NULL,
	CLOSE_DATE DATETIME NOT NULL,
	PAYMENT_ACCOUNT_ID INT NOT NULL,
	LIABILITY_ACCOUNT_ID INT NOT NULL,
	NOTE NVARCHAR(MAX) NOT NULL,
	CREATE_BY NVARCHAR(50) NOT NULL,
	CREATE_ON DATETIME NOT NULL,
	IS_ACTIVE BIT NOT NULL
);

IF OBJECT_ID('TBL_LOAN_PAYMENT') IS NULL
CREATE TABLE TBL_LOAN_PAYMENT(
	PAYMENT_ID INT IDENTITY PRIMARY KEY,
	LOAN_ID INT NOT NULL,
	PAY_DATE DATETIME NOT NULL,
	PAY_AMOUNT DECIMAL(18,4) NOT NULL,
	INTEREST_AMOUNT DECIMAL(18,4) NOT NULL,	
	CAPITAL_AMOUNT DECIMAL(18,4) NOT NULL,
	EXPENSE_ACCOUNT_ID INT NOT NULL,
	PAYMENT_ACCOUNT_ID INT NOT NULL,
	NOTE NVARCHAR(MAX) NOT NULL,
	CREATE_BY NVARCHAR(50) NOT NULL,
	CREATE_ON DATETIME NOT NULL,
	IS_ACTIVE BIT NOT NULL
)
	


-- ******************************************************************
-- AS07
-- ******************************************************************
IF OBJECT_ID('ANR.TBL_RESULT_07') IS NULL
CREATE TABLE ANR.TBL_RESULT_07(
	ID INT IDENTITY PRIMARY KEY,
	YEAR_ID INT NOT NULL,
	TYPE_ID INT NOT NULL,
	TYPE_NAME NVARCHAR(100) NOT NULL,
	ACCOUNT_CODE NVARCHAR(50) NOT NULL,
	ROW_NO NVARCHAR(50) NOT NULL,
	ITEM_NAME NVARCHAR(100) NOT NULL,
	
	BEGINING DECIMAL(18,4) NULL,
	NEW DECIMAL(18,4) NULL,
	DISPOSE DECIMAL(18,4) NULL,
	ENDING DECIMAL(18,4) NULL,

	IS_ACTIVE BIT NOT NULL
)

-- ******************************************************************
-- AS08
-- ******************************************************************
IF OBJECT_ID('ANR.TBL_RESULT_08') IS NULL
CREATE TABLE ANR.TBL_RESULT_08(
	ID INT IDENTITY PRIMARY KEY,
	YEAR_ID INT NOT NULL,
	TYPE_ID INT NOT NULL,
	TYPE_NAME NVARCHAR(100) NOT NULL,
	ACCOUNT_CODE NVARCHAR(50) NOT NULL,
	ROW_NO NVARCHAR(50) NOT NULL,
	ITEM_NAME NVARCHAR(100) NOT NULL,
	
	TOTAL_VALUE DECIMAL(18,4) NULL,
	BEGINING_DEPRECIATION DECIMAL(18,4) NULL,
	DEPRECIATION_RATE NVARCHAR(50) NULL,
	NEW_DEPRECIATION DECIMAL(18,4) NULL,
	ENDING_DEPRECIATION DECIMAL(18,4) NULL,	
	TOTAL_BOOK_VALUE DECIMAL(18,4) NULL,

	IS_ACTIVE BIT NOT NULL
)

-- ******************************************************************
-- AS09
-- ******************************************************************

IF OBJECT_ID('ANR.TBL_RESULT_09') IS NULL
CREATE TABLE ANR.TBL_RESULT_09(
	ID INT IDENTITY PRIMARY KEY,
	YEAR_ID INT NOT NULL,
	
	ROW_NO NVARCHAR(50) NOT NULL,
	DESCRIPITION NVARCHAR(100) NOT NULL,
	FORMAT NVARCHAR(50) NOT NULL,
	ACCOUNT_CODE NVARCHAR(50) NOT NULL,
	TABLE_CODE NVARCHAR(50) NOT NULL,
	AMOUNT DECIMAL(18,4) NULL,

	IS_ACTIVE BIT NOT NULL
)

-- ******************************************************************
-- AS10
-- ******************************************************************
IF OBJECT_ID('ANR.TBL_RESULT_10') IS NULL
CREATE TABLE ANR.TBL_RESULT_10(
	ID INT IDENTITY PRIMARY KEY,
	YEAR_ID INT NOT NULL,
	
	ROW_NO NVARCHAR(50) NOT NULL,
	DESCRIPITION NVARCHAR(100) NOT NULL,
	FORMAT NVARCHAR(50) NOT NULL,
	ACCOUNT_CODE NVARCHAR(50) NOT NULL,
	TABLE_CODE NVARCHAR(50) NOT NULL,
	AMOUNT DECIMAL(18,4) NULL,

	IS_ACTIVE BIT NOT NULL
)

-- ******************************************************************
-- AS11
-- ******************************************************************
IF OBJECT_ID('ANR.TBL_RESULT_11') IS NULL
CREATE TABLE ANR.TBL_RESULT_11(
	ID INT IDENTITY PRIMARY KEY,
	YEAR_ID INT NOT NULL,
	
	ROW_NO NVARCHAR(50) NOT NULL,
	PRICE_ID INT NOT NULL,
	PRICE_NAME NVARCHAR(100) NOT NULL,
	CUSTOMER_TYPE_ID INT NOT NULL,
	CUSTOMER_TYPE_NAME NVARCHAR(100) NOT NULL,
	ACCOUNT_CODE NVARCHAR(100) NOT NULL,
	TOTAL_CUSTOMER DECIMAL(18,4) NOT NULL,
	PRICE DECIMAL(18,4) NOT NULL,
	TOTAL_POWER DECIMAL(18,4) NOT NULL,
	TOTAL_INCOME DECIMAL(18,4) NOT NULL,
	TOTAL_RECEIVE DECIMAL(18,4) NOT NULL,
	IS_ACTIVE BIT NOT NULL
);
GO
-- ******************************************************************
-- AS12
-- ******************************************************************
IF OBJECT_ID('ANR.TBL_RESULT_12') IS NULL
CREATE TABLE ANR.TBL_RESULT_12(
	ID INT IDENTITY PRIMARY KEY,
	YEAR_ID INT NOT NULL,
	
	ROW_NO NVARCHAR(50) NOT NULL,
	DESCRIPITION NVARCHAR(100) NOT NULL,
	FORMAT NVARCHAR(50) NOT NULL,
	ACCOUNT_CODE NVARCHAR(50) NOT NULL,
	AMOUNT DECIMAL(18,4) NULL,

	IS_ACTIVE BIT NOT NULL
)
GO

-- ******************************************************************
-- AS13
-- ******************************************************************

IF OBJECT_ID('ANR.TBL_RESULT_13') IS NULL
	CREATE TABLE ANR.TBL_RESULT_13(
		ID INT IDENTITY PRIMARY KEY,
		YEAR_ID INT NOT NULL,
		ROW_TYPE_ID INT NOT NULL,
		ROW_NO NVARCHAR(50) NOT NULL,
		ROW_TYPE_NAME NVARCHAR(100) NOT NULL,
		UNIT NVARCHAR(50) NOT NULL,
		JAN DECIMAL(18,4) NULL,
		FEB DECIMAL(18,4) NULL,
		MAR DECIMAL(18,4) NULL,
		APR DECIMAL(18,4) NULL,
		MAY DECIMAL(18,4) NULL,
		JUN DECIMAL(18,4) NULL,
		JUL DECIMAL(18,4) NULL,
		AUG DECIMAL(18,4) NULL,
		SEP DECIMAL(18,4) NULL,
		OCT DECIMAL(18,4) NULL,
		NOV DECIMAL(18,4) NULL,
		DEC DECIMAL(18,4) NULL,
		TOTAL DECIMAL(18,4) NULL,
		IS_ACTIVE BIT NOT NULL
	); 
GO
IF OBJECT_ID('[ANR].[TBL_BUSINESS_PLAN]') IS NULL
CREATE TABLE [ANR].[TBL_BUSINESS_PLAN](
	[PLAN_ID] [int] IDENTITY(1,1) NOT NULL,
	[YEAR_ID] [int] NOT NULL,
	[THIS_YEAR_REASON] [nvarchar](max) COLLATE Latin1_General_BIN NOT NULL,
	[THIS_YEAR_IMPROVEMENT] [nvarchar](max) COLLATE Latin1_General_BIN NOT NULL,
	[NEXT_YEAR_INCOME] [decimal](18, 4) NOT NULL,
	[NEXT_YEAR_EXPENSE] [decimal](18, 4) NOT NULL,
	[IS_ACTIVE] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PLAN_ID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO




-- **********************************************************************************************************
-- UPDATE COLUMN AND TABLE -- SMEY
-- **********************************************************************************************************

IF OBJECT_ID('[dbo].[TBL_FUEL_PURCHASE]') IS NULL
CREATE TABLE [dbo].[TBL_FUEL_PURCHASE](
	[FUEL_PURCHASE_ID] [int] IDENTITY(1,1) NOT NULL,
	[REF_NO] [nvarchar](50) COLLATE Latin1_General_BIN NOT NULL,
	[PURCHASE_DATE] [datetime] NOT NULL,
	[FUEL_TYPE_ID] [int] NOT NULL,
	[FUEL_SOURCE_ID] [int] NOT NULL,
	[CURRENCY_ID] [int] NOT NULL,
	[PRICE] [decimal](18, 4) NOT NULL,
	[VOLUME] [decimal](18, 4) NOT NULL,
	[TOTAL_AMOUNT] [decimal](18, 4) NOT NULL,
	[NOTE] [nvarchar](50) COLLATE Latin1_General_BIN NOT NULL,
	[CREATE_BY] [nvarchar](50) COLLATE Latin1_General_BIN NOT NULL,
	[CREATE_ON] [datetime] NOT NULL,
	[IS_ACTIVE] [bit] NOT NULL,
	[PAYMENT_ACCOUNT_ID] [int] NOT NULL,
	[TRAN_BY] [nvarchar](50) COLLATE Latin1_General_BIN NOT NULL,
 CONSTRAINT [PK__TBL_FUEL_PURCHAS__781FBE44] PRIMARY KEY CLUSTERED 
(
	[FUEL_PURCHASE_ID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
IF OBJECT_ID('[dbo].[TBL_FUEL_SOURCE]') IS NULL
CREATE TABLE [dbo].[TBL_FUEL_SOURCE](
	[FUEL_SOURCE_ID] [int] IDENTITY(1,1) NOT NULL,
	[FUEL_SOURCE_NAME] [nvarchar](100) COLLATE Latin1_General_BIN NOT NULL,
	[NOTE] [nvarchar](max) COLLATE Latin1_General_BIN NOT NULL,
	[IS_ACTIVE] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[FUEL_SOURCE_ID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME='PAYMENT_ACCOUNT_ID' AND TABLE_NAME='TBL_FUEL_PURCHASE')
	ALTER TABLE TBL_FUEL_PURCHASE ADD PAYMENT_ACCOUNT_ID INT 
GO
UPDATE TBL_FUEL_PURCHASE SET PAYMENT_ACCOUNT_ID=72 WHERE PAYMENT_ACCOUNT_ID IS NULL
GO
ALTER TABLE TBL_FUEL_PURCHASE ALTER COLUMN PAYMENT_ACCOUNT_ID INT NOT NULL
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME='TRAN_BY' AND TABLE_NAME='TBL_FUEL_PURCHASE')
	ALTER TABLE TBL_FUEL_PURCHASE ADD TRAN_BY NVARCHAR(50) 
GO
UPDATE TBL_FUEL_PURCHASE SET TRAN_BY=N'' WHERE TRAN_BY IS NULL
GO
ALTER TABLE TBL_FUEL_PURCHASE ALTER COLUMN TRAN_BY NVARCHAR(50) NOT NULL
GO

IF NOT EXISTS(SELECT * FROM TLKP_FUEL_TYPE WHERE FUEL_TYPE_ID=3)
	INSERT INTO TLKP_FUEL_TYPE (FUEL_TYPE_ID,FUEL_TYPE_NAME)
	SELECT 3,N'ប្រេងរំអិល'
GO

ALTER TABLE TBL_GENERATOR ALTER COLUMN OPERATION_HOUR DECIMAL(18,4) NOT NULL

IF NOT EXISTS(SELECT * FROM TBL_ACCOUNT_CONFIG WHERE CONFIG_ID=14)
	INSERT INTO TBL_ACCOUNT_CONFIG (CONFIG_ID,CONFIG_NAME,CONFIG_VALUE)
	SELECT 14,N'LIABILITY_ACCOUNT',N'5,11,276,101,102,103,104,105,12,106,107,108,109,110,111,112,113'
GO

IF NOT EXISTS(SELECT * FROM TBL_ACCOUNT_CONFIG WHERE CONFIG_ID=15)
	INSERT INTO TBL_ACCOUNT_CONFIG (CONFIG_ID,CONFIG_NAME,CONFIG_VALUE)
	SELECT 15,N'EXPENSE_INTEREST',N'30,31,32,33'
GO

IF NOT EXISTS(SELECT * FROM TBL_ACCOUNT_CONFIG WHERE CONFIG_ID=16)
	INSERT INTO TBL_ACCOUNT_CONFIG (CONFIG_ID,CONFIG_NAME,CONFIG_VALUE)
	SELECT 16,N'OTHER_INCOME_ACCOUNT',N'17,18,19'
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='TBL_POWER_GENERATION' AND COLUMN_NAME='OPERATION_HOUR')
	ALTER TABLE TBL_POWER_GENERATION ADD OPERATION_HOUR DECIMAL(18,4) NULL
GO
UPDATE TBL_POWER_GENERATION SET OPERATION_HOUR=0 WHERE OPERATION_HOUR IS NULL
GO
ALTER TABLE TBL_POWER_GENERATION ALTER COLUMN OPERATION_HOUR DECIMAL(18,4) NOT NULL;

"; 
        }

    }
}
