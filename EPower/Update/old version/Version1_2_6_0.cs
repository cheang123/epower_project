﻿namespace EPower.Update
{
    class Version1_2_6_0 : Version
    {
        /*         
        @2020-12-08 Vonn kimputhmuyvorn
        1. Add REPORT_CUSTOMERS_NO_BILL
        2. Add REPORT_PREPAYMENT_REMAIN_DETAIL
        3. Fix REPORT_CUSTOMERS(Customer Stop & Delete, Show Meter & Box)
        4. Add REPORT_METER_DETAIL
        5. Add Customer TOU SOLA in TLKP__CUSTOMER_CONNECTION_TYPE
        6. Update REF_INVOICE
        7. Update RUN_REF_02_2020
        8. Update REPORT_POWER_SOLD_2018
        9. Update REPORT_QUARTER_SOLD_CONSUMER
        10 Fix REPORT_POWER_SOLD_LICENSEE
        11-Fix REPORT_QUARTER_CUSTOMER_CONSUMER
        12-Fix REPORT_QUARTER_POWER_COVERAGE
        */

        protected override string GetBatchCommand()
        {
            return @"
IF OBJECT_ID('REPORT_CUSTOMERS_NO_BILL') IS NOT NULL
	DROP PROC REPORT_CUSTOMERS_NO_BILL
GO

CREATE PROC REPORT_CUSTOMERS_NO_BILL
	@AREA_ID INT=0,
	@PRICE_ID INT=0,
	@CYCLE_ID INT=0,
	@CURRENCY_ID INT =0,
	@CUSTOMER_CONNECTION_TYPE_ID INT=0,
	@MONTH DATETIME='2020-12-01',
	@STATUS_ID INT=0
AS

-- @SINCE 2020-12-03 Vonn Kimputhmunyvorn

SELECT i.CUSTOMER_ID
INTO #TMP_INV_BILL
FROM dbo.TBL_INVOICE i
WHERE  i.INVOICE_MONTH=@MONTH AND i.IS_SERVICE_BILL=0 AND i.INVOICE_STATUS NOT IN(3)

SELECT c.CUSTOMER_ID
INTO #TMP_RUN_NO_BILL
FROM dbo.TBL_CUSTOMER c
LEFT JOIN #TMP_INV_BILL tib ON tib.CUSTOMER_ID = c.CUSTOMER_ID
INNER JOIN dbo.TBL_RUN_BILL rb ON rb.CYCLE_ID=c.BILLING_CYCLE_ID
WHERE	rb.BILLING_MONTH=@MONTH AND tib.CUSTOMER_ID IS NULL

;WITH c AS(
SELECT  RowNumber= ROW_NUMBER() OVER(PARTITION BY CUSTOMER_ID ORDER BY CUS_METER_ID DESC )  ,* 
FROM TBL_CUSTOMER_METER)
SELECT * INTO #TempCusMeter FROM c WHERE c.RowNumber = 1;

;WITH u AS(
SELECT  RowNumber= ROW_NUMBER() OVER(PARTITION BY CUSTOMER_ID ORDER BY USAGE_ID DESC )  ,*
FROM TBL_USAGE)
SELECT * INTO #TempCusUsage FROM u WHERE u.RowNumber = 1;


SELECT 
		 c.CUSTOMER_CODE
		,CUSTOMER_NAME = c.LAST_NAME_KH+' '+c.FIRST_NAME_KH+' '+c.FIRST_NAME+' '+c.LAST_NAME
		,a.AREA_ID
		,a.AREA_NAME
		,a.AREA_CODE
		,amp.AMPARE_NAME
		,POLE_NAME = ISNULL(p.POLE_CODE,'-')
		,BOX_CODE = ISNULL(b.BOX_CODE,'-')
		,METER_CODE =REPLACE(LTRIM(REPLACE(ISNULL(m.METER_CODE,'-'),'0',' ')),' ','0')
		,END_USAGE = COALESCE(u.END_USAGE,0)
		,c.STATUS_ID
		,STATUS_NAME=cs.CUS_STATUS_NAME
		,c.ACTIVATE_DATE
		,cx.CURRENCY_ID
		,cx.CURRENCY_NAME
		,ct.CUSTOMER_TYPE_ID
		,ct.CUSTOMER_TYPE_NAME
FROM #TMP_RUN_NO_BILL t
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=t.CUSTOMER_ID
INNER JOIN #TempCusMeter cm ON cm.CUSTOMER_ID=c.CUSTOMER_ID
INNER JOIN TBL_METER m ON cm.METER_ID=m.METER_ID
INNER JOIN TBL_AREA a ON a.AREA_ID=c.AREA_ID
INNER JOIN TBL_BOX b ON b.BOX_ID=cm.BOX_ID
INNER JOIN TBL_POLE p ON p.POLE_ID=b.POLE_ID
INNER JOIN TBL_AMPARE amp ON amp.AMPARE_ID = c.AMP_ID
INNER JOIN TBL_PRICE tp ON tp.PRICE_ID=c.PRICE_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=tp.CURRENCY_ID
INNER JOIN TLKP_CUS_STATUS cs ON cs.CUS_STATUS_ID=c.STATUS_ID
INNER JOIN TBL_CUSTOMER_TYPE ct ON ct.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
INNER JOIN #TempCusUsage u ON u.CUSTOMER_ID=c.CUSTOMER_ID

WHERE	(@AREA_ID=0 OR a.AREA_ID=@AREA_ID)
	AND (@PRICE_ID=0 OR c.PRICE_ID=@PRICE_ID)
	AND (@CYCLE_ID=0 OR c.BILLING_CYCLE_ID=@CYCLE_ID)
	AND (@CURRENCY_ID=0 OR cx.CURRENCY_ID=@CURRENCY_ID)
	AND (@CUSTOMER_CONNECTION_TYPE_ID=0 OR c.CUSTOMER_CONNECTION_TYPE_ID=@CUSTOMER_CONNECTION_TYPE_ID)
	AND (@STATUS_ID=0 OR c.STATUS_ID=@STATUS_ID)
-- END REPORT_CUSTOMERS_NO_BILL

GO

IF OBJECT_ID('REPORT_PREPAYMENT_REMAIN_DETAIL') IS NOT NULL
	DROP PROC REPORT_PREPAYMENT_REMAIN_DETAIL
GO

CREATE PROC REPORT_PREPAYMENT_REMAIN_DETAIL
	@START_DATE DATETIME='2020-12-11',
	@END_DATE DATETIME = '2020-12-11',
	@CURRENCY_ID INT = 0,
	@CYCLE_ID INT = 0,
	@AREA INT =0 
AS

-- @SINCE 2020-12-06 Vonn Kimputhmunyvorn

SET @START_DATE = DATEADD(D,0,DATEDIFF(D,0,@START_DATE));
SET @END_DATE = DATEADD(S,-1,DATEADD(D,1,DATEDIFF(D,0,@END_DATE)));


;WITH c AS(
SELECT  RowNumber= ROW_NUMBER() OVER(PARTITION BY CUSTOMER_ID,CURRENCY_ID ORDER BY CUS_PREPAYMENT_ID DESC )  ,* 
FROM TBL_CUS_PREPAYMENT)
SELECT * INTO #TempCusPrepayment FROM c WHERE c.RowNumber = 1;


SELECT
		c.CUSTOMER_CODE
		,CUSTOMER_NAME_KH = c.LAST_NAME_KH + ' ' + c.FIRST_NAME_KH
		,b.BOX_CODE
		,METER_CODE =REPLACE(LTRIM(REPLACE(ISNULL(m.METER_CODE,'-'),'0',' ')),' ','0')		
		,REMAIN_AMOUNT=cp.BALANCE
		,cp.CREATE_ON
		,cr.CURRENCY_ID
		,cr.CURRENCY_SING
		,cr.CURRENCY_NAME
FROM TBL_CUSTOMER c
INNER JOIN #TempCusPrepayment cp ON cp.CUSTOMER_ID=c.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_METER cm ON cm.CUSTOMER_ID=c.CUSTOMER_ID 
INNER JOIN TBL_METER m ON m.METER_ID=cm.METER_ID
INNER JOIN TBL_BOX b ON b.BOX_ID=cm.BOX_ID
INNER JOIN dbo.TLKP_CURRENCY cr ON cr.CURRENCY_ID = cp.CURRENCY_ID
WHERE	cp.CREATE_ON BETWEEN @START_DATE AND @END_DATE
		AND (@AREA = 0 OR c.AREA_ID = @AREA)
		AND (@CYCLE_ID = 0 OR c.BILLING_CYCLE_ID = @CYCLE_ID)
		AND (@CURRENCY_ID = 0 OR cp.CURRENCY_ID = @CURRENCY_ID)
		AND cp.RowNumber=1
		AND cm.IS_ACTIVE=1
		AND cp.BALANCE>0

--END REPORT_PREPAYMENT_REMAIN_DETAIL

GO

IF OBJECT_ID('REPORT_CUSTOMERS') IS NOT NULL
  DROP PROC REPORT_CUSTOMERS;
GO

CREATE PROC REPORT_CUSTOMERS
  @AREA_ID INT=0,
  @PRICE_ID INT=0,
  @CYCLE_ID INT=0,
  @CUSTOMER_CONNECTION_TYPE_ID INT=0,
  @MONTH DATETIME='2018-06-01',
  @CUS_STATUS_ID INT=-2,
  @IS_MARKET_VENDOR BIT=0,
  @AMPERE_ID INT=0
AS
-- @SINCE 2011-03-30 Update customer as reading list
-- @SINCE 2011-04-20 Trim Meter Code.
-- @SINCE 2011-11-04 EXCLUDE CLOSE AND CANCEL CUSTOMER 
-- @SINCE 2012-01-14 ADD AMPARE NAME
-- @SINCE 2014-04-28 ORDER CUSTOMER BY POSITION_IN_BOX
-- @SINCE 2016-12-12 ADD FILTER CUS_STATUS_ID
-- @SINCE 2020-06-24 ADD PARAMETER IS_MARKET_VENDOR
-- @SINCE 2020-07-23 ADD AMPERE PARAMETER
-- @SINCE 2020-12-08 FIX CUSTOMERS STOP & DELETE , Show Meter & Box
DECLARE @CMD NVARCHAR(MAX);
DECLARE @ID NVARCHAR(MAX);

WITH c AS
(SELECT
   RowNumber=ROW_NUMBER() OVER (PARTITION BY CUSTOMER_ID
ORDER BY CUS_METER_ID DESC),
   *
 FROM TBL_CUSTOMER_METER)
SELECT * INTO #TempCusMeter FROM c WHERE c.RowNumber=1

SELECT
  CUSTOMER_CODE=c.CUSTOMER_CODE,
  CUSTOMER_NAME=c.LAST_NAME_KH+' '+c.FIRST_NAME_KH+' '+c.FIRST_NAME+' '+c.LAST_NAME,
  PHONE=c.PHONE_1+' '+c.PHONE_2,
  a.AREA_ID,
  a.AREA_CODE,
  AREA_NAME=ISNULL(a.AREA_NAME, '-'),
  POLE_NAME=ISNULL(p.POLE_CODE, '-'),
  BOX_CODE=ISNULL(b.BOX_CODE, '-'),
  cm.POSITION_IN_BOX,
  METER_CODE=REPLACE(LTRIM(REPLACE(ISNULL(m.METER_CODE, '-'), '0', ' ')), ' ', '0'),
  START_USAGE=COALESCE(u.START_USAGE, o.END_USAGE, 0),
  END_USAGE=COALESCE(u.END_USAGE, 0),
  AMPARE_NAME=ISNULL(amp.AMPARE_NAME, '-'),
  c.STATUS_ID
INTO #TEMP
FROM TBL_CUSTOMER c
LEFT JOIN #TempCusMeter cm ON cm.CUSTOMER_ID=c.CUSTOMER_ID
LEFT JOIN TBL_METER m ON cm.METER_ID=m.METER_ID
LEFT JOIN TBL_AREA a ON a.AREA_ID=c.AREA_ID
LEFT JOIN TBL_BOX b ON b.BOX_ID=cm.BOX_ID
LEFT JOIN TBL_POLE p ON p.POLE_ID=b.POLE_ID
LEFT JOIN TBL_AMPARE amp ON amp.AMPARE_ID=c.AMP_ID
OUTER APPLY (
	SELECT TOP 1 START_USAGE,END_USAGE
	FROM TBL_USAGE 
	WHERE CUSTOMER_ID=c.CUSTOMER_ID AND USAGE_MONTH=@MONTH
	ORDER BY USAGE_ID DESC
) u
OUTER APPLY (
	SELECT TOP 1 START_USAGE,END_USAGE
	FROM TBL_USAGE 
	WHERE CUSTOMER_ID=c.CUSTOMER_ID AND USAGE_MONTH=DATEADD(MONTH,-1,@MONTH)
	ORDER BY USAGE_ID DESC 
) o
WHERE (@AREA_ID=0 OR a.AREA_ID=@AREA_ID) 
	AND (@CYCLE_ID=0 OR c.BILLING_CYCLE_ID=@CYCLE_ID) 
	AND (@PRICE_ID=0 OR c.PRICE_ID=@PRICE_ID) 
	AND (@CUSTOMER_CONNECTION_TYPE_ID=0 OR c.CUSTOMER_CONNECTION_TYPE_ID=@CUSTOMER_CONNECTION_TYPE_ID) 
	AND (@IS_MARKET_VENDOR=0 OR c.CUSTOMER_TYPE_ID=-2) AND (@AMPERE_ID=0 OR c.AMP_ID=@AMPERE_ID)
ORDER BY a.AREA_CODE,
  p.POLE_CODE,
  b.BOX_CODE,
  cm.POSITION_IN_BOX,
  c.CUSTOMER_CODE;

--SET @ID=CASE WHEN @CUS_STATUS_ID=-1 THEN '1,2,3' WHEN @CUS_STATUS_ID=-2 THEN '4,5' ELSE @CUS_STATUS_ID END;
IF @CUS_STATUS_ID=-1 BEGIN
  SELECT * FROM #TEMP WHERE STATUS_ID IN (1, 2, 3);
END;
ELSE IF @CUS_STATUS_ID=-2 BEGIN
       SELECT * FROM #TEMP WHERE STATUS_ID IN (4, 5);
END;
ELSE IF @CUS_STATUS_ID=0 BEGIN
SELECT * FROM #TEMP;
END;
ELSE BEGIN
       SELECT * FROM #TEMP WHERE STATUS_ID=@CUS_STATUS_ID;
END;
--END OF REPORT_CUSTOMERS
GO

IF OBJECT_ID('REPORT_METER_DETAIL') IS NOT NULL
  DROP PROC REPORT_METER_DETAIL;
GO

CREATE PROC REPORT_METER_DETAIL
  @CHECK_TAMPER BIT=0
AS
SELECT
  c.CUSTOMER_CODE,
  CUSTOMER_NAME=c.LAST_NAME_KH+' '+c.FIRST_NAME_KH+' '+c.FIRST_NAME+' '+c.LAST_NAME,
  METER_CODE=REPLACE(LTRIM(REPLACE(ISNULL(m.METER_CODE, '-'), '0', ' ')), ' ', '0'),
  a.AREA_NAME,
  p.POLE_CODE,
  tiu.BOX_CODE,
  tts.TAMPER_STATUS_NAME_KH
FROM TMP_IR_USAGE tiu
INNER JOIN TLKP_TAMPER_STATUS tts ON tts.TAMPER_STATUS_ID=tiu.TAMPER_STATUS
INNER JOIN TBL_METER m ON m.METER_CODE=tiu.METER_CODE
INNER JOIN TBL_CUSTOMER_METER cm ON cm.METER_ID=m.METER_ID AND cm.IS_ACTIVE=1
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=cm.CUSTOMER_ID
INNER JOIN TBL_AREA a ON c.AREA_ID=a.AREA_ID
INNER JOIN TBL_POLE p ON cm.POLE_ID=p.POLE_ID
INNER JOIN TBL_BOX b ON cm.BOX_ID=b.BOX_ID
WHERE (@CHECK_TAMPER=0);
--END REPORT_METER_DETAIL

GO


IF NOT EXISTS
(SELECT
   *
 FROM TLKP_CUSTOMER_CONNECTION_TYPE
 WHERE CUSTOMER_CONNECTION_TYPE_ID IN (21, 22, 23, 24, 25, 26)) BEGIN
  INSERT INTO TLKP_CUSTOMER_CONNECTION_TYPE
  (CUSTOMER_CONNECTION_TYPE_ID, CUSTOMER_CONNECTION_TYPE_NAME, DESCRIPTION, CURRENCY_ID, TARIFF, NONLICENSE_CUSTOMER_GROUP_ID)
  VALUES
  (
  21, -- CUSTOMER_CONNECTION_TYPE_ID - int
  N'ទិញ MV តម្លើងប្រភពពន្លឺព្រះអាទិត្យ', -- CUSTOMER_CONNECTION_TYPE_NAME - nvarchar
  N'1.អ្នកប្រើប្រាស់មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យទិញ MV', -- DESCRIPTION - nvarchar
  2 , -- CURRENCY_ID - int
  0.13, -- TARIFF - decimal
  2 -- NONLICENSE_CUSTOMER_GROUP_ID - int	
  );
  INSERT INTO TLKP_CUSTOMER_CONNECTION_TYPE
  (CUSTOMER_CONNECTION_TYPE_ID, CUSTOMER_CONNECTION_TYPE_NAME, DESCRIPTION, CURRENCY_ID, TARIFF, NONLICENSE_CUSTOMER_GROUP_ID)
  VALUES
  (
  22, -- CUSTOMER_CONNECTION_TYPE_ID - int
  N'ទិញ LV ក្រោមត្រង់ស្វូអ្នកទិញ តម្លើងប្រភពពន្លឺព្រះអាទិត្យ', -- CUSTOMER_CONNECTION_TYPE_NAME - nvarchar
  N'2.អ្នកប្រើប្រាស់មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យទិញ LV ក្រោមត្រង់ស្វូអ្នកទិញ', -- DESCRIPTION - nvarchar
  2 , -- CURRENCY_ID - int
  0.1352, -- TARIFF - decimal
  2 -- NONLICENSE_CUSTOMER_GROUP_ID - int	
  );
  INSERT INTO TLKP_CUSTOMER_CONNECTION_TYPE
  (CUSTOMER_CONNECTION_TYPE_ID, CUSTOMER_CONNECTION_TYPE_NAME, DESCRIPTION, CURRENCY_ID, TARIFF, NONLICENSE_CUSTOMER_GROUP_ID)
  VALUES
  (
  23, -- CUSTOMER_CONNECTION_TYPE_ID - int
  N'ទិញ LV ក្រោមត្រង់ស្វូអ្នកលក់ តម្លើងប្រភពពន្លឺព្រះអាទិត្យ', -- CUSTOMER_CONNECTION_TYPE_NAME - nvarchar
  N'3.អ្នកប្រើប្រាស់មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យទិញ LV ក្រោមត្រង់ស្វូអ្នកលក់', -- DESCRIPTION - nvarchar
  2 , -- CURRENCY_ID - int
  0.1432, -- TARIFF - decimal
  2 -- NONLICENSE_CUSTOMER_GROUP_ID - int	
  );
  INSERT INTO TLKP_CUSTOMER_CONNECTION_TYPE
  (CUSTOMER_CONNECTION_TYPE_ID, CUSTOMER_CONNECTION_TYPE_NAME, DESCRIPTION, CURRENCY_ID, TARIFF, NONLICENSE_CUSTOMER_GROUP_ID)
  VALUES
  (
  24, -- CUSTOMER_CONNECTION_TYPE_ID - int
  N'ទិញ MV តម្លើងប្រភពពន្លឺព្រះអាទិត្យ', -- CUSTOMER_CONNECTION_TYPE_NAME - nvarchar
  N'1.អ្នកប្រើប្រាស់មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យទិញ MV', -- DESCRIPTION - nvarchar
  2 , -- CURRENCY_ID - int
  0.15, -- TARIFF - decimal
  1 -- NONLICENSE_CUSTOMER_GROUP_ID - int	
  );
  INSERT INTO TLKP_CUSTOMER_CONNECTION_TYPE
  (CUSTOMER_CONNECTION_TYPE_ID, CUSTOMER_CONNECTION_TYPE_NAME, DESCRIPTION, CURRENCY_ID, TARIFF, NONLICENSE_CUSTOMER_GROUP_ID)
  VALUES
  (
  25, -- CUSTOMER_CONNECTION_TYPE_ID - int
  N'ទិញ LV ក្រោមត្រង់ស្វូអ្នកទិញ តម្លើងប្រភពពន្លឺព្រះអាទិត្យ', -- CUSTOMER_CONNECTION_TYPE_NAME - nvarchar
  N'2.អ្នកប្រើប្រាស់មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យទិញ LV ក្រោមត្រង់ស្វូអ្នកទិញ', -- DESCRIPTION - nvarchar
  2 , -- CURRENCY_ID - int
  0.156, -- TARIFF - decimal
  1 -- NONLICENSE_CUSTOMER_GROUP_ID - int	
  );
  INSERT INTO TLKP_CUSTOMER_CONNECTION_TYPE
  (CUSTOMER_CONNECTION_TYPE_ID, CUSTOMER_CONNECTION_TYPE_NAME, DESCRIPTION, CURRENCY_ID, TARIFF, NONLICENSE_CUSTOMER_GROUP_ID)
  VALUES
  (
  26, -- CUSTOMER_CONNECTION_TYPE_ID - int
  N'ទិញ LV ក្រោមត្រង់ស្វូអ្នកលក់ តម្លើងប្រភពពន្លឺព្រះអាទិត្យ', -- CUSTOMER_CONNECTION_TYPE_NAME - nvarchar
  N'3.អ្នកប្រើប្រាស់មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យទិញ LV ក្រោមត្រង់ស្វូអ្នកលក់', -- DESCRIPTION - nvarchar
  2 , -- CURRENCY_ID - int
  0.164, -- TARIFF - decimal
  1 -- NONLICENSE_CUSTOMER_GROUP_ID - int	
  );
END;
IF OBJECT_ID('REPORT_REF_INVOICE') IS NOT NULL
  DROP PROC REPORT_REF_INVOICE;
GO
CREATE PROC [dbo].[REPORT_REF_INVOICE]
  @DATA_MONTH DATETIME='2020-12-01',
  @CUSTOMER_GROUP_ID INT=0,
  @CUSTOMER_CONNECTION_TYPE_ID INT=0,
  @CURRENCY_ID INT=0,
  @V1 DECIMAL(18, 4) =0,
  @V2 DECIMAL(18, 4) =1000000,
  @BILLING_CYCLE_ID INT=0,
  @AREA_ID INT=0
AS
/*
@ 2016-05-25 Morm Raksmey
	Add Column Billing Cycle Id and Billing Cycle Name
@ 2017-03-10 Sieng Sothera
	Trim meter code without '0'
@ 2017-04-19 Sieng Sotheara
	Update REF Invoice 2016 & 2017
@ 2018-09-19 Pov Lyhorng
	fix Organization customer is non subsidy type
@ 2019-01-16 Phuong Sovathvong
	Update REF Invoice 2019
@ 2019-12-12 HOR DARA
	Update REF Invoice 2020
@ 2020-07-28 HOR DARA
	Update REF Invoice 2020 - market vendor get subsidy even over 2000kwh (49 licensee)
*/
DECLARE
  @LICENSE_TYPE_ID INT,
  @BASED_TARIFF DECIMAL(18, 5),
  @SUBSIDY_TARIFF DECIMAL(18, 5);
SELECT
  @LICENSE_TYPE_ID=LICENSE_TYPE_ID,
  @SUBSIDY_TARIFF=SUBSIDY_TARIFF,
  @BASED_TARIFF=BASED_TARIFF
FROM dbo.TBL_TARIFF
WHERE DATA_MONTH=@DATA_MONTH;
CREATE TABLE #RESULT
(
TYPE_ID INT,
TYPE_NAME NVARCHAR(255),
TYPE_OF_SALE_ID INT,
TYPE_OF_SALE_NAME NVARCHAR(255),
HISTORY_ID BIGINT,
CUSTOMER_GROUP_ID INT,
CUSTOMER_GROUP_NAME NVARCHAR(200),
INVOICE_ID BIGINT,
DATA_MONTH DATETIME,
CUSTOMER_ID INT,
CUSTOMER_CODE NVARCHAR(50),
FIRST_NAME_KH NVARCHAR(50),
LAST_NAME_KH NVARCHAR(50),
AREA_ID INT,
AREA_CODE NVARCHAR(50),
BOX_ID INT,
BOX_CODE NVARCHAR(50),
CUSTOMER_CONNECTION_TYPE_ID INT,
CUSTOMER_CONNECTION_TYPE_NAME NVARCHAR(200),
AMPARE_ID INT,
AMPARE_NAME NVARCHAR(50),
PRICE_ID INT,
METER_ID INT,
METER_CODE NVARCHAR(50),
MULTIPLIER INT,
CURRENCY_SIGN NVARCHAR(50),
CURRENCY_NAME NVARCHAR(50),
CURRENCY_ID INT,
PRICE DECIMAL(18, 5),
BASED_PRICE DECIMAL(18, 5),
START_USAGE DECIMAL(18, 4),
END_USAGE DECIMAL(18, 4),
TOTAL_USAGE DECIMAL(18, 4),
TOTAL_AMOUNT DECIMAL(18, 5),
IS_POST_PAID BIT,
IS_AGRICULTURE BIT,
SHIFT_ID INT,
IS_ACTIVE BIT,
BILLING_CYCLE_ID INT,
BILLING_CYCLE_NAME NVARCHAR(50),
ROW_DATE DATETIME,
CUSTOMER_TYPE_ID INT,
CUSTOMER_TYPE_NAME NVARCHAR(50),
LICENSE_NUMBER NVARCHAR(50)
);
/*REF 2016*/
IF @DATA_MONTH BETWEEN '2016-03-01' AND '2017-02-01' BEGIN
  INSERT INTO #RESULT
  /*លក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ*/
  SELECT
    TYPE_ID=3,
    TYPE_NAME=N'លក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ',
    TYPE_OF_SALE_ID=4,
    TYPE_OF_SALE_NAME=N'លក់លើ MV នាឡិកាស្ទង់ MV',
    i.*
  FROM TBL_REF_INVOICE i
  WHERE i.CUSTOMER_GROUP_ID=3 AND i.CUSTOMER_CONNECTION_TYPE_ID=4
  UNION ALL
  SELECT
    TYPE_ID=3,
    TYPE_NAME=N'លក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ',
    TYPE_OF_SALE_ID=4,
    TYPE_OF_SALE_NAME=N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
    i.*
  FROM TBL_REF_INVOICE i
  WHERE i.CUSTOMER_GROUP_ID=3 AND i.CUSTOMER_CONNECTION_TYPE_ID=3
  UNION ALL
  SELECT
    TYPE_ID=3,
    TYPE_NAME=N'លក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ',
    TYPE_OF_SALE_ID=4,
    TYPE_OF_SALE_NAME=N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
    i.*
  FROM TBL_REF_INVOICE i
  WHERE i.CUSTOMER_GROUP_ID=3 AND i.CUSTOMER_CONNECTION_TYPE_ID=2
  UNION ALL
  /*លក់ឲ្យអតិថិជន MV​(ឧស្សាហកម្ម ពាណិជ្ជកម្ម)*/
  SELECT
    TYPE_ID=2,
    TYPE_NAME=N'លក់ឲ្យអតិថិជន MV​(ឧស្សាហកម្ម ពាណិជ្ជកម្ម)',
    TYPE_OF_SALE_ID=4,
    TYPE_OF_SALE_NAME=N'លក់លើ MV នាឡិកាស្ទង់ MV',
    i.*
  FROM TBL_REF_INVOICE i
  WHERE i.CUSTOMER_GROUP_ID=2 AND i.CUSTOMER_CONNECTION_TYPE_ID=4
  UNION ALL
  SELECT
    TYPE_ID=2,
    TYPE_NAME=N'លក់ឲ្យអតិថិជន MV​(ឧស្សាហកម្ម ពាណិជ្ជកម្ម)',
    TYPE_OF_SALE_ID=4,
    TYPE_OF_SALE_NAME=N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
    i.*
  FROM TBL_REF_INVOICE i
  WHERE i.CUSTOMER_GROUP_ID=2 AND i.CUSTOMER_CONNECTION_TYPE_ID=3
  UNION ALL
  SELECT
    TYPE_ID=2,
    TYPE_NAME=N'លក់ឲ្យអតិថិជន MV​(ឧស្សាហកម្ម ពាណិជ្ជកម្ម)',
    TYPE_OF_SALE_ID=4,
    TYPE_OF_SALE_NAME=N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
    i.*
  FROM TBL_REF_INVOICE i
  WHERE i.CUSTOMER_GROUP_ID=2 AND i.CUSTOMER_CONNECTION_TYPE_ID=2
  UNION ALL
  /*លក់លើតង់ស្យុងទាប*/
  SELECT
    TYPE_ID=1,
    TYPE_NAME=N'លក់លើតង់ស្យុងទាប',
    TYPE_OF_SALE_ID=4,
    TYPE_OF_SALE_NAME=N'អតិថិជនធម្មតា',
    i.*
  FROM TBL_REF_INVOICE i
  WHERE i.CUSTOMER_GROUP_ID=1 AND i.CUSTOMER_CONNECTION_TYPE_ID=1;
END;
/*REF 2017*/
ELSE IF @DATA_MONTH BETWEEN '2017-03-01' AND '2018-12-01' BEGIN
       INSERT INTO #RESULT
       /*ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ*/
       SELECT
         TYPE_ID=3,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ',
         TYPE_OF_SALE_ID=4,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_GROUP_ID=3 AND i.CUSTOMER_CONNECTION_TYPE_ID=11
       UNION ALL
       SELECT
         TYPE_ID=3,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ',
         TYPE_OF_SALE_ID=3,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_GROUP_ID=3 AND i.CUSTOMER_CONNECTION_TYPE_ID=12
       UNION ALL
       SELECT
         TYPE_ID=3,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ',
         TYPE_OF_SALE_ID=2,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_GROUP_ID=3 AND i.CUSTOMER_CONNECTION_TYPE_ID=13
       UNION ALL
       /*ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)*/
       SELECT
         TYPE_ID=2,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)',
         TYPE_OF_SALE_ID=4,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_GROUP_ID IN (1, 2) AND i.CUSTOMER_CONNECTION_TYPE_ID IN (4, 7, 8, 10)
       UNION ALL
       SELECT
         TYPE_ID=2,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)',
         TYPE_OF_SALE_ID=3,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_GROUP_ID IN (1, 2) AND i.CUSTOMER_CONNECTION_TYPE_ID IN (3, 6)
       UNION ALL
       SELECT
         TYPE_ID=2,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)',
         TYPE_OF_SALE_ID=2,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_GROUP_ID IN (1, 2) AND i.CUSTOMER_CONNECTION_TYPE_ID IN (2, 5)
       UNION ALL
       SELECT
         TYPE_ID=2,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)',
         TYPE_OF_SALE_ID=1,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_GROUP_ID IN (1, 2, 5) AND i.CUSTOMER_CONNECTION_TYPE_ID IN (9, 14, 15, 16) AND i.TOTAL_USAGE>=2001
       UNION ALL
       /*ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)*/
       SELECT
         TYPE_ID=1,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)',
         TYPE_OF_SALE_ID=-1,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើក្រោម 2001 kWh/ខែ ',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_GROUP_ID IN (1, 2, 5) AND i.CUSTOMER_CONNECTION_TYPE_ID IN (9, 14, 15, 16) AND i.TOTAL_USAGE<2001
       UNION ALL
       /* ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន */
       SELECT
         TYPE_ID=0,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
         TYPE_OF_SALE_ID=-3,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើតិចជាង  10 kWh/ខែ',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_GROUP_ID IN (4) AND i.CUSTOMER_CONNECTION_TYPE_ID IN (1) AND i.TOTAL_USAGE<=10
       UNION ALL
       SELECT
         TYPE_ID=0,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
         TYPE_OF_SALE_ID=-4,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_GROUP_ID IN (4) AND i.CUSTOMER_CONNECTION_TYPE_ID IN (1) AND i.TOTAL_USAGE BETWEEN 11 AND 50
       UNION ALL
       SELECT
         TYPE_ID=0,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
         TYPE_OF_SALE_ID=-5,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើធំជាង 51 kWh/ខែ',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_GROUP_ID IN (4) AND i.CUSTOMER_CONNECTION_TYPE_ID IN (1) AND i.TOTAL_USAGE>=51;
END;
/*REF 2019*/
ELSE IF @DATA_MONTH BETWEEN '2019-01-01' AND '2020-01-01' BEGIN
       INSERT INTO #RESULT
       SELECT
         TYPE_ID=4,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
         TYPE_OF_SALE_ID=5,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID=11
       UNION ALL
       SELECT
         TYPE_ID=4,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
         TYPE_OF_SALE_ID=4,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID=12
       UNION ALL
       SELECT
         TYPE_ID=4,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
         TYPE_OF_SALE_ID=3,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID=13
       UNION ALL
       SELECT
         TYPE_ID=3,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
         TYPE_OF_SALE_ID=6,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (4)
       UNION ALL
       SELECT
         TYPE_ID=3,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
         TYPE_OF_SALE_ID=5,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវាលា',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (7)
       UNION ALL
       SELECT
         TYPE_ID=3,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
         TYPE_OF_SALE_ID=4,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (3)
       UNION ALL
       SELECT
         TYPE_ID=3,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
         TYPE_OF_SALE_ID=3,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (2)
       UNION ALL
       SELECT
         TYPE_ID=3,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
         TYPE_OF_SALE_ID=2,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (14) AND i.TOTAL_USAGE>=2001
       UNION ALL
       SELECT
         TYPE_ID=2,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
         TYPE_OF_SALE_ID=6,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (8)
       UNION ALL
       SELECT
         TYPE_ID=2,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
         TYPE_OF_SALE_ID=5,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (10)
       UNION ALL
       SELECT
         TYPE_ID=2,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
         TYPE_OF_SALE_ID=4,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (6)
       UNION ALL
       SELECT
         TYPE_ID=2,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
         TYPE_OF_SALE_ID=3,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (5)
       UNION ALL
       SELECT
         TYPE_ID=2,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
         TYPE_OF_SALE_ID=2,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (15, 16) AND i.TOTAL_USAGE>=2001
       UNION ALL
       SELECT
         TYPE_ID=2,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
         TYPE_OF_SALE_ID=1,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់ឲ្យអតិថិជន សាលារៀន មន្ទីរពេទ្យ មណ្ឌលសុខភាព នៅតំបន់ជនបទ',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (9)
       UNION ALL
       SELECT
         TYPE_ID=1,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)',
         TYPE_OF_SALE_ID=-1,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើក្រោម 2001 kWh/ខែ',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (14, 15, 16) AND i.TOTAL_USAGE<2001
       UNION ALL
       SELECT
         TYPE_ID=1,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
         TYPE_OF_SALE_ID=-3,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើតិចជាង  10 kWh/ខែ',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (1) AND i.TOTAL_USAGE<=10
       UNION ALL
       SELECT
         TYPE_ID=1,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
         TYPE_OF_SALE_ID=-4,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (1) AND i.TOTAL_USAGE BETWEEN 11 AND 50
       UNION ALL
       SELECT
         TYPE_ID=1,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
         TYPE_OF_SALE_ID=-5,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 51 ដល់ 200 kWh/ខែ',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (1) AND i.TOTAL_USAGE BETWEEN 51 AND 200
       UNION ALL
       SELECT
         TYPE_ID=1,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
         TYPE_OF_SALE_ID=-6,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើធំជាង 201 kWh/ខែ',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (1) AND i.TOTAL_USAGE>=201;
END;
/*REF 2020 - NORMAL LICENSE TYPE ID =1*/
ELSE IF @DATA_MONTH BETWEEN '2020-02-01' AND '9999-12-01' AND @LICENSE_TYPE_ID=1 BEGIN
       INSERT INTO #RESULT
       SELECT
         TYPE_ID=4,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
         TYPE_OF_SALE_ID=5,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID=11
       UNION ALL
       SELECT
         TYPE_ID=4,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
         TYPE_OF_SALE_ID=4,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID=12
       UNION ALL
       SELECT
         TYPE_ID=4,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
         TYPE_OF_SALE_ID=3,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID=13
       UNION ALL
       SELECT
         TYPE_ID=3,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
         TYPE_OF_SALE_ID=11,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (4)
       UNION ALL
       SELECT
         TYPE_ID=3,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
         TYPE_OF_SALE_ID=10,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (7)
       UNION ALL
       SELECT
         TYPE_ID=3,
         TYPE_NAMEN=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
         TYPE_OF_SALE_ID=9,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (21)
       UNION ALL
       SELECT
         TYPE_ID=3,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
         TYPE_OF_SALE_ID=8,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (3)
       UNION ALL
       SELECT
         TYPE_ID=3,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
         TYPE_OF_SALE_ID=7,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (17)
       UNION ALL
       SELECT
         TYPE_ID=3,
         TYPE_NAMEN=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
         TYPE_OF_SALE_ID=6,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (22)
       UNION ALL
       SELECT
         TYPE_ID=3,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
         TYPE_OF_SALE_ID=5,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (2)
       UNION ALL
       SELECT
         TYPE_ID=3,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
         TYPE_OF_SALE_ID=4,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (18)
       UNION ALL
       SELECT
         TYPE_ID=3,
         TYPE_NAMEN=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
         TYPE_OF_SALE_ID=3,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (23)
       UNION ALL
       SELECT
         TYPE_ID=3,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
         TYPE_OF_SALE_ID=2,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (14) AND i.TOTAL_USAGE>=2001
       UNION ALL
       SELECT
         TYPE_ID=2,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
         TYPE_OF_SALE_ID=8,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (8)
       UNION ALL
       SELECT
         TYPE_ID=2,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
         TYPE_OF_SALE_ID=7,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (10)
       UNION ALL
       SELECT
         TYPE_ID=2,
         TYPE_NAMEN=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
         TYPE_OF_SALE_ID=6,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (24)
       UNION ALL
       SELECT
         TYPE_ID=2,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
         TYPE_OF_SALE_ID=5,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (6)
       UNION ALL
       SELECT
         TYPE_ID=2,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
         TYPE_OF_SALE_ID=4,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (19)
       UNION ALL
       SELECT
         TYPE_ID=2,
         TYPE_NAMEN=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
         TYPE_OF_SALE_ID=3,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (25)
       UNION ALL
       SELECT
         TYPE_ID=2,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
         TYPE_OF_SALE_ID=2,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (5)
       UNION ALL
       SELECT
         TYPE_ID=2,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
         TYPE_OF_SALE_ID=1,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (20)
       UNION ALL
       SELECT
         TYPE_ID=2,
         TYPE_NAMEN=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
         TYPE_OF_SALE_ID=0,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (26)
       UNION ALL
       SELECT
         TYPE_ID=2,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
         TYPE_OF_SALE_ID=-1,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (15, 16) AND i.TOTAL_USAGE>=2001
       UNION ALL
       SELECT
         TYPE_ID=1,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
         TYPE_OF_SALE_ID=-2,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់ឲ្យអតិថិជន សាលារៀន មន្ទីរពេទ្យ មណ្ឌលសុខភាព នៅតំបន់ជនបទ',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (9)
       UNION ALL
       SELECT
         TYPE_ID=1,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)',
         TYPE_OF_SALE_ID=-3,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើក្រោម 2001 kWh/ខែ',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (14, 15, 16) AND i.TOTAL_USAGE<2001
       UNION ALL
       SELECT
         TYPE_ID=1,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
         TYPE_OF_SALE_ID=-4,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើតិចជាង  10 kWh/ខែ',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (1) AND i.TOTAL_USAGE<=10
       UNION ALL
       SELECT
         TYPE_ID=1,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
         TYPE_OF_SALE_ID=-5,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (1) AND i.TOTAL_USAGE BETWEEN 11 AND 50
       UNION ALL
       SELECT
         TYPE_ID=1,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
         TYPE_OF_SALE_ID=-6,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 51 ដល់ 200 kWh/ខែ',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (1) AND i.TOTAL_USAGE BETWEEN 51 AND 200
       UNION ALL
       SELECT
         TYPE_ID=1,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
         TYPE_OF_SALE_ID=-7,
         TYPE_OF_SALE_NAE=N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើធំជាង 201 kWh/ខែ',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (1) AND i.TOTAL_USAGE>=201;
END;
/*REF 2020 - LICENSE TYPE ID =2*/
ELSE IF @DATA_MONTH BETWEEN '2020-02-01' AND '9999-12-01' AND @LICENSE_TYPE_ID=2 BEGIN
       INSERT INTO #RESULT
       SELECT
         TYPE_ID=1,
         TYPE_NAME=N'ថាមពលលក់ឲ្យអតិថិជននៅក្នុងបរិវេណផ្សារ',
         TYPE_OF_SALE_ID=-1,
         TYPE_OF_SALE_NAME=N'ថាមពលលក់ឲ្យអតិថិជនក្នុងផ្សារ',
         i.*
       FROM TBL_REF_INVOICE i
       WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN (14, 15, 16);
--AND i.TOTAL_USAGE<2001
END;
/*RESULT*/
SELECT
  r.*,
  METER_FORMAT=REPLACE(LTRIM(REPLACE(ISNULL(METER_CODE, '-'), '0', ' ')), ' ', '0')
FROM #RESULT r
WHERE DATA_MONTH=@DATA_MONTH AND IS_ACTIVE=1 
	AND (@CUSTOMER_GROUP_ID=0 OR CUSTOMER_GROUP_ID=@CUSTOMER_GROUP_ID) 
	AND (@CURRENCY_ID=0 OR CURRENCY_ID=@CURRENCY_ID) 
	AND (@CUSTOMER_CONNECTION_TYPE_ID=0 OR CUSTOMER_CONNECTION_TYPE_ID=@CUSTOMER_CONNECTION_TYPE_ID) 
	AND (TOTAL_USAGE BETWEEN @V1 AND @V2) 
	AND (@BILLING_CYCLE_ID=0 OR BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR AREA_ID=@AREA_ID)
ORDER BY TYPE_ID DESC,
  TYPE_OF_SALE_ID DESC,
  AREA_CODE,
  BOX_CODE;
--END REPORT_REF_INVOICE
GO

---3. Execute RUN_REF_02-2020
IF OBJECT_ID('RUN_REF_02_2020') IS NOT NULL
  DROP PROC RUN_REF_02_2020;
GO
CREATE PROC RUN_REF_02_2020
  @DATA_MONTH DATETIME='2020-07-01',
  @BILLING_CYCLE_ID INT=0,
  @AREA_ID INT=0
AS

/*
	@2018-09-19 Pov Lyhorng
		fix non subsidy for Organization customer
	@2019-01-16 Phoung Sovathvong
		Update REF 2019 split customer INDUSTRY, COMMERCIAL, SPECIAL
	@2019-02-22 Phoung Sovathvong
		Fix Incorrect word : រដ្ឋាបាល => រដ្ឋបាល
	@2019-12-03 Hor Dara
		Update REF 2020
	@2020-07-28 Hor Dara
		Update REF 2020 market vendor get subsudy even usage over 2000 
*/

-- CLEAR RESULT
DELETE FROM TBL_REF_02 WHERE DATA_MONTH=@DATA_MONTH;
DECLARE
  @LICENSE_TYPE_ID INT,
  @TYPE_LICENSEE NVARCHAR(200),
  @TYPE_LICENSEE_MV NVARCHAR(200),
  @TYPE_LICENSEE_MV_LV_CUSTOMER_TRANSFO NVARCHAR(200),
  @TYPE_LICENSEE_MV_LV_LICENSEE_TRANSFO NVARCHAR(200),
  @TYPE_INDUSTRY_NO_SUBSIDY NVARCHAR(200),
  @TYPE_INDUSTRY_SALE_MV NVARCHAR(200),
  @TYPE_INDUSTRY_SALE_MV_TOU_SOLA NVARCHAR(200),
  @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_SOLA NVARCHAR(200),
  @TYPE_INDUSTRY_SALE_MV_LV_TOU_LICENSEE_TRANSFO_TOU_SOLA NVARCHAR(200),
  @TYPE_INDUSTRY_SALE_MV_TOU NVARCHAR(200),
  @TYPE_INDUSTRY_SALE_MV_TOU_1 NVARCHAR(200),
  @TYPE_INDUSTRY_SALE_MV_TOU_2 NVARCHAR(200),
  @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO NVARCHAR(200),
  @TYPE_INDUSTRY_SALE_MV_LV_LICENSEE_TRANSFO NVARCHAR(200),
  @TYPE_INDUSTRY_SALE_LV_USAGE_OVER_2000 NVARCHAR(200),
  @TYPE_COMMERCIAL_SALE_CUSTOMER_AGRICULTURE NVARCHAR(200),
  @TYPE_COMMERCIAL_NO_SUBSIDY NVARCHAR(200),
  @TYPE_COMMERCIAL_SALE_MV NVARCHAR(200),
  @TYPE_COMMERCIAL_SALE_MV_TOU_SOLA NVARCHAR(200),
  @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_SOLA NVARCHAR(200),
  @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO_TOU_SOLA NVARCHAR(200),
  @TYPE_COMMERCIAL_SALE_MV_TOU NVARCHAR(200),
  @TYPE_COMMERCIAL_SALE_MV_TOU_1 NVARCHAR(200),
  @TYPE_COMMERCIAL_SALE_MV_TOU_2 NVARCHAR(200),
  @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO NVARCHAR(200),
  @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO NVARCHAR(200),
  @TYPE_COMMERCIAL_SALE_LV_USAGE_OVER_2000 NVARCHAR(200),
  @TYPE_COMMERCIAL_SALE_SCHOOL_HOSPITAL_HEALTH_CENTER_CUSTOMER NVARCHAR(200),
  @TYPE_SUBSIDY NVARCHAR(200),
  @TYPE_SUBSIDY_INDUSTRY_COMMERCIAL_SALE_LV_USAGE_UNDER_2001 NVARCHAR(200),
  @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_NORMAL NVARCHAR(200),
  @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_0_10 NVARCHAR(200),
  @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_11_50 NVARCHAR(200),
  @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_51_200 NVARCHAR(200),
  @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_OVER_200 NVARCHAR(200),
  @TYPE_COMMERCIAL_SALE_CUSTOMER_AGRICULTURE_1 NVARCHAR(200),
  @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO_TOU NVARCHAR(200),
  @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_1 NVARCHAR(200),
  @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_2 NVARCHAR(200),
  @TYPE_INDUSTRY_SALE_MV_LV_LICENSEE_TRANSFO_TOU NVARCHAR(200),
  @TYPE_INDUSTRY_SALE_MV_LV_LICENSEE_TRANSFO_TOU_1 NVARCHAR(200),
  @TYPE_INDUSTRY_SALE_MV_LV_LICENSEE_TRANSFO_TOU_2 NVARCHAR(200),
  @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO_TOU NVARCHAR(200),
  @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_1 NVARCHAR(200),
  @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_2 NVARCHAR(200),
  @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO_TOU NVARCHAR(200),
  @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO_TOU_1 NVARCHAR(200),
  @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO_TOU_2 NVARCHAR(200),
  @BASED_TARIFF DECIMAL(18, 5),
  @SUBSIDY_TARIFF DECIMAL(18, 5),
  @TYPE_SUBSIDY_SPECIAL_FOR_MARKET_VENDOR NVARCHAR(200),
  @TYPE_SALE_CUSTOMER_MARKET_VENDOR NVARCHAR(200);

--CHECK LICENSE TYPE ID
SELECT
  @LICENSE_TYPE_ID=LICENSE_TYPE_ID,
  @BASED_TARIFF=BASED_TARIFF,
  @SUBSIDY_TARIFF=SUBSIDY_TARIFF
FROM dbo.TBL_TARIFF
WHERE DATA_MONTH=@DATA_MONTH;
SET @TYPE_LICENSEE=N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ';
SET @TYPE_LICENSEE_MV=N'1. ថាមពលលក់ភ្ជាប់ពីខ្សែបណ្តាញតង់ស្យុងមធ្យម MV';
SET @TYPE_LICENSEE_MV_LV_CUSTOMER_TRANSFO=N'2. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកទិញ)';
SET @TYPE_LICENSEE_MV_LV_LICENSEE_TRANSFO=N'3. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកលក់)';
SET @TYPE_INDUSTRY_NO_SUBSIDY=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (អ្នកប្រើប្រាស់ធុនធំ និងធុនមធ្យម ប្រភេទ ឧស្សាហកម្ម និងកសិកម្ម)';
SET @TYPE_INDUSTRY_SALE_MV=N'1. ថាមពលលក់ភ្ជាប់ពីខ្សែបណ្តាញតង់ស្យុងមធ្យម MV';
SET @TYPE_INDUSTRY_SALE_MV_TOU=N'2. ថាមពលលក់ភ្ជាប់ពីខ្សែបណ្តាញតង់ស្យុងមធ្យម MV ជម្រើសតាមពេលវេលា និងអានុភាព';
SET @TYPE_INDUSTRY_SALE_MV_TOU_1=N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកច្រើន (៧ព្រឹកដល់៩យប់)  ';
SET @TYPE_INDUSTRY_SALE_MV_TOU_2=N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកតិច (៩យប់ដល់៧ព្រឹក)     ';
SET @TYPE_INDUSTRY_SALE_MV_TOU_SOLA=N'3. ថាមពលលក់ភ្ជាប់ពីខ្សែបណ្តាញតង់ស្យុងមធ្យម MV ដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ';
SET @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO=N'4. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកទិញ)';
SET @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO_TOU=N'5. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកទិញ) ជម្រើសតាមពេលវេលា និងអានុភាព';
SET @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_1=N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកច្រើន (៧ព្រឹកដល់៩យប់)   ';
SET @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_2=N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកតិច (៩យប់ដល់៧ព្រឹក)     ';
SET @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_SOLA=N'6. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកទិញ) ដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ';
SET @TYPE_INDUSTRY_SALE_MV_LV_LICENSEE_TRANSFO=N'7. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកលក់)';
SET @TYPE_INDUSTRY_SALE_MV_LV_LICENSEE_TRANSFO_TOU=N'8. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកលក់) ជម្រើសតាមពេលវេលា និងអានុភាព';
SET @TYPE_INDUSTRY_SALE_MV_LV_LICENSEE_TRANSFO_TOU_1=N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកច្រើន (៧ព្រឹកដល់៩យប់)     ';
SET @TYPE_INDUSTRY_SALE_MV_LV_LICENSEE_TRANSFO_TOU_2=N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកតិច (៩យប់ដល់៧ព្រឹក)     ';
SET @TYPE_INDUSTRY_SALE_MV_LV_TOU_LICENSEE_TRANSFO_TOU_SOLA=N'9. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកលក់) ដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ';
SET @TYPE_INDUSTRY_SALE_LV_USAGE_OVER_2000=N'10. ថាមពលលក់ឲ្យអតិថិជន ប្រភេទឧស្សាហកម្ម និងកសិកម្ម LV សាធារណៈ ប្រើចាប់ពី 2001 kWh/ខែ';
SET @TYPE_COMMERCIAL_SALE_CUSTOMER_AGRICULTURE=N'11. ថាមពលលក់ឲ្យការបូមទឹកធ្វើកសិកម្មម៉ោង07:00ព្រឹក ដល់ 09:00យប់';
SET @TYPE_COMMERCIAL_NO_SUBSIDY=N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (អ្នកប្រើប្រាស់ធុនធំ និងធុនមធ្យម ប្រភេទពាណិជ្ជកម្ម រដ្ឋបាល និងផ្សេងៗ)';
SET @TYPE_COMMERCIAL_SALE_MV=N'1. ថាមពលលក់ភ្ជាប់ពីខ្សែបណ្តាញតង់ស្យុងមធ្យម MV';
SET @TYPE_COMMERCIAL_SALE_MV_TOU=N'2. ថាមពលលក់ភ្ជាប់ពីខ្សែបណ្តាញតង់ស្យុងមធ្យម MV ជម្រើសតាមពេលវេលា និងអានុភាព';
SET @TYPE_COMMERCIAL_SALE_MV_TOU_1=N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកច្រើន (៧ព្រឹកដល់៩យប់)     ';
SET @TYPE_COMMERCIAL_SALE_MV_TOU_2=N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកតិច (៩យប់ដល់៧ព្រឹក)     ';
SET @TYPE_COMMERCIAL_SALE_MV_TOU_SOLA=N'3. ថាមពលលក់ភ្ជាប់ពីខ្សែបណ្តាញតង់ស្យុងមធ្យម MV ដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ';
SET @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO=N'4. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកទិញ)';
SET @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO_TOU=N'5. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកទិញ) ជម្រើសតាមពេលវេលា និងអានុភាព';
SET @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_1=N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកច្រើន (៧ព្រឹកដល់៩យប់)     ';
SET @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_2=N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកតិច (៩យប់ដល់៧ព្រឹក)     ';
SET @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_SOLA=N'6. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកទិញ) ដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ';
SET @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO=N'7. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកលក់)';
SET @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO_TOU=N'8. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកលក់) ជម្រើសតាមពេលវេលា និងអានុភាព';
SET @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO_TOU_1=N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកច្រើន (៧ព្រឹកដល់៩យប់)     ';
SET @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO_TOU_2=N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកតិច (៩យប់ដល់៧ព្រឹក)     ';
SET @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO_TOU_SOLA=N'9. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកលក់) ដាក់ប្រភពតម្លើងពន្លឺព្រះអាទិត្យ';
SET @TYPE_COMMERCIAL_SALE_LV_USAGE_OVER_2000=N'10. ថាមពលលក់ឲ្យអតិថិជន ប្រភេទឧស្សាហកម្ម និងកសិកម្ម LV សាធារណៈ ប្រើចាប់ពី 2001 kWh/ខែ';
SET @TYPE_SUBSIDY=N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)';
SET @TYPE_COMMERCIAL_SALE_SCHOOL_HOSPITAL_HEALTH_CENTER_CUSTOMER=N'1. ថាមពលលក់ឲ្យអតិថិជន សាលារៀន មន្ទីរពេទ្យ មណ្ឌលសុខភាព នៅតំបន់ជនបទ';
SET @TYPE_COMMERCIAL_SALE_CUSTOMER_AGRICULTURE_1=N'2. ថាមពលលក់ឲ្យការបូមទឹកធ្វើកសិកម្មម៉ោង09:00យប់ ដល់07:00ព្រឹក';
SET @TYPE_SUBSIDY_INDUSTRY_COMMERCIAL_SALE_LV_USAGE_UNDER_2001=N'3. ថាមពលលក់ឲ្យអតិថិជន ឧស្សាហកម្ម&កសិកម្ម ពាណិជ្ជកម្ម រដ្ឋបាល&សេវាកម្ម ប្រើក្រោម 2001 kWh/ខែ';
SET @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_NORMAL=N'4. ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន';
SET @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_0_10=N'4.1'+SPACE(9)+N'ថាមពលលក់ឲ្យលំនៅដ្ឋានប្រើ មិនលើសពី 10 kWh/ខែ';
SET @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_11_50=N'4.2'+SPACE(9)+N'ថាមពលលក់ឲ្យលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ';
SET @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_51_200=N'4.3'+SPACE(9)+N'ថាមពលលក់ឲ្យលំនៅដ្ឋានប្រើចាប់ពី 51 ដល់ 200 kWh/ខែ';
SET @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_OVER_200=N'4.4'+SPACE(9)+N'ថាមពលលក់ឲ្យលំនៅដ្ឋានប្រើចាប់ពី 201 kWh/ខែ';
SET @TYPE_SUBSIDY_SPECIAL_FOR_MARKET_VENDOR=N'ថាមពលលក់ឲ្យអតិថិជននៅក្នុងបរិវេណផ្សារ';
SET @TYPE_SALE_CUSTOMER_MARKET_VENDOR=N'1. ថាមពលលក់ឲ្យអតិថិជនក្នុងផ្សារ';
IF (@LICENSE_TYPE_ID=1) BEGIN
  -- Licensee Customer(MV)
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=4,
    TYPE_NAME=@TYPE_LICENSEE,
    TYPE_OF_SALE_ID=5,
    TYPE_OF_SALE_NAME=@TYPE_LICENSEE_MV,
    TOTAL_CUSTOMER=COUNT(*),
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  INTO
    #RESULT
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH 
  AND i.CUSTOMER_CONNECTION_TYPE_ID=11 
	AND (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) 
	AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID)
  GROUP BY CUSTOMER_GROUP_ID,
    CUSTOMER_GROUP_NAME,
    CUSTOMER_CONNECTION_TYPE_ID,
    CUSTOMER_CONNECTION_TYPE_NAME,
    PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- Licensee Customer(buyer's transformer)
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=4,
    TYPE_NAME=@TYPE_LICENSEE,
    TYPE_OF_SALE_ID=4,
    TYPE_OF_SALE_NAME=@TYPE_LICENSEE_MV_LV_CUSTOMER_TRANSFO,
    TOTAL_CUSTOMER=COUNT(*),
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH 
	AND i.CUSTOMER_CONNECTION_TYPE_ID=12 
	AND (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) 
	AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID)
  GROUP BY CUSTOMER_GROUP_ID,
    CUSTOMER_GROUP_NAME,
    CUSTOMER_CONNECTION_TYPE_ID,
    CUSTOMER_CONNECTION_TYPE_NAME,
    PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- Licensee Customer(license's transformer)
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=4,
    TYPE_NAME=@TYPE_LICENSEE,
    TYPE_OF_SALE_ID=3,
    TYPE_OF_SALE_NAME=@TYPE_LICENSEE_MV_LV_LICENSEE_TRANSFO,
    TOTAL_CUSTOMER=COUNT(*),
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH 
  AND i.CUSTOMER_CONNECTION_TYPE_ID=13 
  AND (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) 
  AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID)
  GROUP BY CUSTOMER_GROUP_ID,
    CUSTOMER_GROUP_NAME,
    CUSTOMER_CONNECTION_TYPE_ID,
    CUSTOMER_CONNECTION_TYPE_NAME,
    PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- MV Customer INDUSTRY
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=3,
    TYPE_NAME=@TYPE_INDUSTRY_NO_SUBSIDY,
    TYPE_OF_SALE_ID=17,
    TYPE_OF_SALE_NAME=@TYPE_INDUSTRY_SALE_MV,
    TOTAL_CUSTOMER=COUNT(*),
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (4) 
	AND (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) 
	AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID)
  GROUP BY PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- MV TOU Customer INDUSTRY
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=3,
    TYPE_NAME=@TYPE_INDUSTRY_NO_SUBSIDY,
    TYPE_OF_SALE_ID=16,
    TYPE_OF_SALE_NAME=@TYPE_INDUSTRY_SALE_MV_TOU,
    TOTAL_CUSTOMER=COUNT(*),
    TOTAL_POWER_SOLD=0,
    RATE=1,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (7) 
	AND (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) 
	AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID)
  GROUP BY CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- MV TOU Customer INDUSTRY(7:00am-9:00pm)
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=3,
    TYPE_NAME=@TYPE_INDUSTRY_NO_SUBSIDY,
    TYPE_OF_SALE_ID=15,
    TYPE_OF_SALE_NAME=@TYPE_INDUSTRY_SALE_MV_TOU_1,
    TOTAL_CUSTOMER=0,
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (7) AND
                                                                        (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID) AND i.PRICE=0.1300
  GROUP BY PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- MV TOU Customer INDUSTRY(9:00pm-7:00pm)
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=3,
    TYPE_NAME=@TYPE_INDUSTRY_NO_SUBSIDY,
    TYPE_OF_SALE_ID=14,
    TYPE_OF_SALE_NAME=@TYPE_INDUSTRY_SALE_MV_TOU_2,
    TOTAL_CUSTOMER=0,
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (7) 
	AND (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID) AND i.PRICE=0.1100
  GROUP BY PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- MV TOU SOLA
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=3,
    TYPE_NAME=@TYPE_INDUSTRY_NO_SUBSIDY,
    TYPE_OF_SALE_ID=13,
    TYPE_OF_SALE_NAME=@TYPE_INDUSTRY_SALE_MV_TOU_SOLA,
    TOTAL_CUSTOMER=COUNT(*),
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (21) 
	AND (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID)
  GROUP BY PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- MV/LV Customer INDUSTRY (buyer's transformer)
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=3,
    TYPE_NAME=@TYPE_INDUSTRY_NO_SUBSIDY,
    TYPE_OF_SALE_ID=12,
    TYPE_OF_SALE_NAME=@TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO,
    TOTAL_CUSTOMER=COUNT(*),
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (3) AND
                                                                        (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID)
  GROUP BY PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- MV/LV Customer INDUSTRY (buyer's transformer) TOU
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=3,
    TYPE_NAME=@TYPE_INDUSTRY_NO_SUBSIDY,
    TYPE_OF_SALE_ID=11,
    TYPE_OF_SALE_NAME=@TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO_TOU,
    TOTAL_CUSTOMER=COUNT(*),
    TOTAL_POWER_SOLD=0,
    RATE=1,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (17) AND
                                                                         (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID)
  GROUP BY CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- MV/LV Customer INDUSTRY (buyer's transformer)(7:00am-9:00pm)
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=3,
    TYPE_NAME=@TYPE_INDUSTRY_NO_SUBSIDY,
    TYPE_OF_SALE_ID=10,
    TYPE_OF_SALE_NAME=@TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_1,
    TOTAL_CUSTOMER=0,
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (17) AND
                                                                         (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID) AND i.PRICE=0.1352
  GROUP BY PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- MV/LV Customer INDUSTRY (buyer's transformer)(9:00pm-7:00pm)
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=3,
    TYPE_NAME=@TYPE_INDUSTRY_NO_SUBSIDY,
    TYPE_OF_SALE_ID=9,
    TYPE_OF_SALE_NAME=@TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_2,
    TOTAL_CUSTOMER=0,
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (17) AND
                                                                         (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID) AND i.PRICE=0.1144
  GROUP BY PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- MV/LV Customer INDUSTRY (buyer's transformer) TOU SOLA
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=3,
    TYPE_NAME=@TYPE_INDUSTRY_NO_SUBSIDY,
    TYPE_OF_SALE_ID=8,
    TYPE_OF_SALE_NAME=@TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_SOLA,
    TOTAL_CUSTOMER=COUNT(*),
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (22) AND
                                                                         (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID)
  GROUP BY PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- MV/LV Customer INDUSTRY (licensee's transformer)
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=3,
    TYPE_NAME=@TYPE_INDUSTRY_NO_SUBSIDY,
    TYPE_OF_SALE_ID=7,
    TYPE_OF_SALE_NAME=@TYPE_INDUSTRY_SALE_MV_LV_LICENSEE_TRANSFO,
    TOTAL_CUSTOMER=COUNT(*),
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (2) AND
                                                                        (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID)
  GROUP BY PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- MV/LV Customer INDUSTRY (licensee's transformer) TOU
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=3,
    TYPE_NAME=@TYPE_INDUSTRY_NO_SUBSIDY,
    TYPE_OF_SALE_ID=6,
    TYPE_OF_SALE_NAME=@TYPE_INDUSTRY_SALE_MV_LV_LICENSEE_TRANSFO_TOU,
    TOTAL_CUSTOMER=COUNT(*),
    TOTAL_POWER_SOLD=0,
    RATE=1,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (18) AND
                                                                         (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID)
  GROUP BY CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- MV/LV Customer INDUSTRY (licensee's transformer)(7:00am-9:00pm)
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=3,
    TYPE_NAME=@TYPE_INDUSTRY_NO_SUBSIDY,
    TYPE_OF_SALE_ID=5,
    TYPE_OF_SALE_NAME=@TYPE_INDUSTRY_SALE_MV_LV_LICENSEE_TRANSFO_TOU_1,
    TOTAL_CUSTOMER=0,
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (18) AND
                                                                         (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID) AND i.PRICE=0.1432
  GROUP BY PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- MV/LV Customer INDUSTRY (licensee's transformer)(9:00pm-7:00pm)
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=3,
    TYPE_NAME=@TYPE_INDUSTRY_NO_SUBSIDY,
    TYPE_OF_SALE_ID=4,
    TYPE_OF_SALE_NAME=@TYPE_INDUSTRY_SALE_MV_LV_LICENSEE_TRANSFO_TOU_2,
    TOTAL_CUSTOMER=0,
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (18) AND
                                                                         (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID) AND i.PRICE=0.1224
  GROUP BY PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  --MV.LV Customer INDUSTRY (licensee's transformer) TOU SOLA
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=3,
    TYPE_NAME=@TYPE_INDUSTRY_NO_SUBSIDY,
    TYPE_OF_SALE_ID=3,
    TYPE_OF_SALE_NAME=@TYPE_INDUSTRY_SALE_MV_LV_TOU_LICENSEE_TRANSFO_TOU_SOLA,
    TOTAL_CUSTOMER=COUNT(*),
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (23) AND
                                                                         (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID)
  GROUP BY PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- for Normal Customer(TOTAL_USAGE>=2001) INDUSTRY
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=3,
    TYPE_NAME=@TYPE_INDUSTRY_NO_SUBSIDY,
    TYPE_OF_SALE_ID=2,
    TYPE_OF_SALE_NAME=@TYPE_INDUSTRY_SALE_LV_USAGE_OVER_2000,
    TOTAL_CUSTOMER=COUNT(*),
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (14) AND i.TOTAL_USAGE>=2001 AND
                                                                                                 (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID)
  GROUP BY PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- MV Customer COMMERCIAL
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=2,
    TYPE_NAME=@TYPE_COMMERCIAL_NO_SUBSIDY,
    TYPE_OF_SALE_ID=16,
    TYPE_OF_SALE_NAME=@TYPE_COMMERCIAL_SALE_MV,
    TOTAL_CUSTOMER=COUNT(*),
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (8) AND
                                                                        (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID)
  GROUP BY PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- MV TOU Customer COMMERCIAL TOU
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=2,
    TYPE_NAME=@TYPE_COMMERCIAL_NO_SUBSIDY,
    TYPE_OF_SALE_ID=15,
    TYPE_OF_SALE_NAME=@TYPE_COMMERCIAL_SALE_MV_TOU,
    TOTAL_CUSTOMER=COUNT(*),
    TOTAL_POWER_SOLD=0,
    RATE=1,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (10) AND
                                                                         (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID)
  GROUP BY CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- MV TOU Customer COMMERCIAL(7:00am-9:00pm)
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=2,
    TYPE_NAME=@TYPE_COMMERCIAL_NO_SUBSIDY,
    TYPE_OF_SALE_ID=14,
    TYPE_OF_SALE_NAME=@TYPE_COMMERCIAL_SALE_MV_TOU_1,
    TOTAL_CUSTOMER=0,
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (10) AND
                                                                         (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID) AND i.PRICE=0.1500
  GROUP BY PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- MV TOU Customer COMMERCIAL(9:00pm-7:00am)
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=2,
    TYPE_NAME=@TYPE_COMMERCIAL_NO_SUBSIDY,
    TYPE_OF_SALE_ID=13,
    TYPE_OF_SALE_NAME=@TYPE_COMMERCIAL_SALE_MV_TOU_2,
    TOTAL_CUSTOMER=0,
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (10) AND
                                                                         (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID) AND i.PRICE=0.1240
  GROUP BY PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  ---MV TOU SOLA Customer
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=2,
    TYPE_NAME=@TYPE_INDUSTRY_NO_SUBSIDY,
    TYPE_OF_SALE_ID=12,
    TYPE_OF_SALE_NAME=@TYPE_COMMERCIAL_SALE_MV_TOU_SOLA,
    TOTAL_CUSTOMER=COUNT(*),
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (24) AND
                                                                         (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID)
  GROUP BY PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- MV/LV Customer Commercial (buyer's transformer)
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=2,
    TYPE_NAME=@TYPE_COMMERCIAL_NO_SUBSIDY,
    TYPE_OF_SALE_ID=11,
    TYPE_OF_SALE_NAME=@TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO,
    TOTAL_CUSTOMER=COUNT(*),
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (6) AND
                                                                        (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID)
  GROUP BY PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- MV/LV Customer Commercial (buyer's transformer)TOU
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=2,
    TYPE_NAME=@TYPE_INDUSTRY_NO_SUBSIDY,
    TYPE_OF_SALE_ID=10,
    TYPE_OF_SALE_NAME=@TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO_TOU,
    TOTAL_CUSTOMER=COUNT(*),
    TOTAL_POWER_SOLD=0,
    RATE=1,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (19) AND
                                                                         (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID)
  GROUP BY CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- MV/LV Customer Commercial (buyer's transformer)(7:00am-9:00pm)
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=2,
    TYPE_NAME=@TYPE_INDUSTRY_NO_SUBSIDY,
    TYPE_OF_SALE_ID=9,
    TYPE_OF_SALE_NAME=@TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_1,
    TOTAL_CUSTOMER=0,
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (19) AND
                                                                         (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID) AND i.PRICE=0.1560
  GROUP BY PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- MV/LV Customer Commercial (buyer's transformer)(9:00pm-7:00pm)
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=2,
    TYPE_NAME=@TYPE_INDUSTRY_NO_SUBSIDY,
    TYPE_OF_SALE_ID=8,
    TYPE_OF_SALE_NAME=@TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_2,
    TOTAL_CUSTOMER=0,
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (19) AND
                                                                         (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID) AND i.PRICE=0.12896
  GROUP BY PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- MV/LV Customer Commercial (buyer's transformer)TOU SOLA
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=2,
    TYPE_NAME=@TYPE_INDUSTRY_NO_SUBSIDY,
    TYPE_OF_SALE_ID=7,
    TYPE_OF_SALE_NAME=@TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_SOLA,
    TOTAL_CUSTOMER=COUNT(*),
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (25) AND
                                                                         (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID)
  GROUP BY PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- MV/LV Customer Commercial (licensee's transformer)
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=2,
    TYPE_NAME=@TYPE_COMMERCIAL_NO_SUBSIDY,
    TYPE_OF_SALE_ID=6,
    TYPE_OF_SALE_NAME=@TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO,
    TOTAL_CUSTOMER=COUNT(*),
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (5) AND
                                                                        (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID)
  GROUP BY PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- MV/LV Customer Commercial (licensee's transformer)TOU
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=2,
    TYPE_NAME=@TYPE_INDUSTRY_NO_SUBSIDY,
    TYPE_OF_SALE_ID=5,
    TYPE_OF_SALE_NAME=@TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO_TOU,
    TOTAL_CUSTOMER=COUNT(*),
    TOTAL_POWER_SOLD=0,
    RATE=1,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (20) AND
                                                                         (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID)
  GROUP BY CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- MV/LV Customer Commercial (licensee's transformer)(7:00am-9:00pm)
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=2,
    TYPE_NAME=@TYPE_INDUSTRY_NO_SUBSIDY,
    TYPE_OF_SALE_ID=4,
    TYPE_OF_SALE_NAME=@TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO_TOU_1,
    TOTAL_CUSTOMER=0,
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (20) AND
                                                                         (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID) AND i.PRICE=0.1640
  GROUP BY PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- MV/LV Customer Commercial (licensee's transformer)(9:00pm-7:00pm)
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=2,
    TYPE_NAME=@TYPE_INDUSTRY_NO_SUBSIDY,
    TYPE_OF_SALE_ID=3,
    TYPE_OF_SALE_NAME=@TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO_TOU_2,
    TOTAL_CUSTOMER=0,
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (20) AND
                                                                         (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID) AND i.PRICE=0.13696
  GROUP BY PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- MV/LV Customer Commercial (licensee's transformer)TOU SOLA
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=2,
    TYPE_NAME=@TYPE_INDUSTRY_NO_SUBSIDY,
    TYPE_OF_SALE_ID=2,
    TYPE_OF_SALE_NAME=@TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO_TOU_SOLA,
    TOTAL_CUSTOMER=COUNT(*),
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (26) AND
                                                                         (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID)
  GROUP BY PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- for Normal Customer(TOTAL_USAGE>=2001) COMMERCIAL
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=2,
    TYPE_NAME=@TYPE_COMMERCIAL_NO_SUBSIDY,
    TYPE_OF_SALE_ID=1,
    TYPE_OF_SALE_NAME=@TYPE_COMMERCIAL_SALE_LV_USAGE_OVER_2000,
    TOTAL_CUSTOMER=COUNT(*),
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (15, 16) AND i.TOTAL_USAGE>=2001 AND
                                                                                                     (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID)
  GROUP BY PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- for Normal Customer SPECIAL
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=1,
    TYPE_NAME=@TYPE_SUBSIDY,
    TYPE_OF_SALE_ID=0,
    TYPE_OF_SALE_NAME=@TYPE_COMMERCIAL_SALE_SCHOOL_HOSPITAL_HEALTH_CENTER_CUSTOMER,
    TOTAL_CUSTOMER=COUNT(*),
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (9) AND
                                                                        (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID)
  GROUP BY PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- for Normal Customer(TOTAL_USAGE<2001)
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=1,
    TYPE_NAME=@TYPE_SUBSIDY,
    TYPE_OF_SALE_ID=-2,
    TYPE_OF_SALE_NAME=@TYPE_SUBSIDY_INDUSTRY_COMMERCIAL_SALE_LV_USAGE_UNDER_2001,
    TOTAL_CUSTOMER=COUNT(*),
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=BASED_PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (14, 15, 16) AND i.TOTAL_USAGE<2001 AND
                                                                                                        (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID)
  GROUP BY BASED_PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- for Total Normal Customer
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=1,
    TYPE_NAME=@TYPE_SUBSIDY,
    TYPE_OF_SALE_ID=-3,
    TYPE_OF_SALE_NAME=@TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_NORMAL,
    TOTAL_CUSTOMER=COUNT(*),
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=BASED_PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (1) AND
                                                                        (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID)
  GROUP BY BASED_PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- for Normal Customer(TOTAL_USAGE<=10)
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=1,
    TYPE_NAME=@TYPE_SUBSIDY,
    TYPE_OF_SALE_ID=-4,
    TYPE_OF_SALE_NAME=@TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_0_10,
    TOTAL_CUSTOMER=COUNT(*),
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (1) AND i.TOTAL_USAGE<=10 AND
                                                                                              (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID)
  GROUP BY PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- for Normal Customer(TOTAL_USAGE BETWEEN 11 AND 50)
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=1,
    TYPE_NAME=@TYPE_SUBSIDY,
    TYPE_OF_SALE_ID=-5,
    TYPE_OF_SALE_NAME=@TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_11_50,
    TOTAL_CUSTOMER=COUNT(*),
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (1) AND (i.TOTAL_USAGE BETWEEN 11 AND 50) AND
                                                                                                              (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID)
  GROUP BY PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- for Normal Customer(TOTAL_USAGE BETWEEN 51 AND 200)
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=1,
    TYPE_NAME=@TYPE_SUBSIDY,
    TYPE_OF_SALE_ID=-6,
    TYPE_OF_SALE_NAME=@TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_51_200,
    TOTAL_CUSTOMER=COUNT(*),
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (1) AND (i.TOTAL_USAGE BETWEEN 51 AND 200) AND
                                                                                                               (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID)
  GROUP BY PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN

  -- for Normal Customer(TOTAL_USAGE>=200)
  UNION ALL
  SELECT
    DATA_MONTH=@DATA_MONTH,
    TYPE_ID=1,
    TYPE_NAME=@TYPE_SUBSIDY,
    TYPE_OF_SALE_ID=-7,
    TYPE_OF_SALE_NAME=@TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_OVER_200,
    TOTAL_CUSTOMER=COUNT(*),
    TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
    RATE=PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE=1
  FROM TBL_REF_INVOICE i
  WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (1) AND i.TOTAL_USAGE>=201 AND
                                                                                               (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID)
  GROUP BY PRICE,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN;
  CREATE TABLE #DEFAULT_RESULT
  (
  DATA_MONTH DATETIME,
  TYPE_ID INT,
  TYPE_NAME NVARCHAR(200),
  TYPE_OF_SALE_ID INT,
  TYPE_OF_SALE_NAME NVARCHAR(200),
  TOTAL_CUSTOMER INT,
  TOTAL_POWER_SOLD INT,
  RATE DECIMAL(18, 4),
  CURRENCY_ID INT,
  CURRENCY_NAME NVARCHAR(20),
  CURRENCY_SIGN NVARCHAR(5),
  IS_ACTIVE BIT
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 4, @TYPE_LICENSEE, 5, @TYPE_LICENSEE_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 4, @TYPE_LICENSEE, 4, @TYPE_LICENSEE_MV_LV_CUSTOMER_TRANSFO, 0, 0, 0, 2, N'ដុល្លា', N'$', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 4, @TYPE_LICENSEE, 3, @TYPE_LICENSEE_MV_LV_LICENSEE_TRANSFO, 0, 0, 0, 2, N'ដុល្លា', N'$', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 17, @TYPE_INDUSTRY_SALE_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 16, @TYPE_INDUSTRY_SALE_MV_TOU, 0, 0, 0, 2, N'ដុល្លា', N'$', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 15, @TYPE_INDUSTRY_SALE_MV_TOU_1, 0, 0, 0, 2, N'ដុល្លា', N'$', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 14, @TYPE_INDUSTRY_SALE_MV_TOU_2, 0, 0, 0, 2, N'ដុល្លា', N'$', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 13, @TYPE_INDUSTRY_SALE_MV_TOU_SOLA, 0, 0, 0, 2, N'ដុល្លា', N'$', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 12, @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO, 0, 0, 0, 2, N'ដុល្លា', N'$', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 11, @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO_TOU, 0, 0, 0, 2, N'ដុល្លា', N'$', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 10, @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_1, 0, 0, 0, 2, N'ដុល្លា', N'$', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 9, @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_2, 0, 0, 0, 2, N'ដុល្លា', N'$', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 8, @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_SOLA, 0, 0, 0, 2, N'ដុល្លា', N'$', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 7, @TYPE_INDUSTRY_SALE_MV_LV_LICENSEE_TRANSFO, 0, 0, 0, 2, N'ដុល្លា', N'$', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 6, @TYPE_INDUSTRY_SALE_MV_LV_LICENSEE_TRANSFO_TOU, 0, 0, 0, 2, N'ដុល្លា', N'$', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 5, @TYPE_INDUSTRY_SALE_MV_LV_LICENSEE_TRANSFO_TOU_1, 0, 0, 0, 2, N'ដុល្លា', N'$', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 4, @TYPE_INDUSTRY_SALE_MV_LV_LICENSEE_TRANSFO_TOU_2, 0, 0, 0, 2, N'ដុល្លា', N'$', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 3, @TYPE_INDUSTRY_SALE_MV_LV_TOU_LICENSEE_TRANSFO_TOU_SOLA, 0, 0, 0, 2, N'ដុល្លា', N'$', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 2, @TYPE_INDUSTRY_SALE_LV_USAGE_OVER_2000, 0, 0, 0, 1, N'រៀល', N'៛', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 0, @TYPE_COMMERCIAL_SALE_CUSTOMER_AGRICULTURE, 0, 0, 0, 1, N'រៀល', N'៛', 1
  ); --AGRI
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 2, @TYPE_COMMERCIAL_NO_SUBSIDY, 16, @TYPE_COMMERCIAL_SALE_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 2, @TYPE_COMMERCIAL_NO_SUBSIDY, 15, @TYPE_COMMERCIAL_SALE_MV_TOU, 0, 0, 0, 2, N'ដុល្លា', N'$', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 2, @TYPE_COMMERCIAL_NO_SUBSIDY, 14, @TYPE_COMMERCIAL_SALE_MV_TOU_1, 0, 0, 0, 2, N'ដុល្លា', N'$', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 2, @TYPE_COMMERCIAL_NO_SUBSIDY, 13, @TYPE_COMMERCIAL_SALE_MV_TOU_2, 0, 0, 0, 2, N'ដុល្លា', N'$', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 2, @TYPE_COMMERCIAL_NO_SUBSIDY, 12, @TYPE_COMMERCIAL_SALE_MV_TOU_SOLA, 0, 0, 0, 2, N'ដុល្លា', N'$', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 2, @TYPE_COMMERCIAL_NO_SUBSIDY, 11, @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO, 0, 0, 0, 2, N'ដុល្លា', N'$', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 2, @TYPE_COMMERCIAL_NO_SUBSIDY, 10, @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO_TOU, 0, 0, 0, 2, N'ដុល្លា', N'$', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 2, @TYPE_COMMERCIAL_NO_SUBSIDY, 9, @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_1, 0, 0, 0, 2, N'ដុល្លា', N'$', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 2, @TYPE_COMMERCIAL_NO_SUBSIDY, 8, @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_2, 0, 0, 0, 2, N'ដុល្លា', N'$', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 2, @TYPE_COMMERCIAL_NO_SUBSIDY, 7, @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_SOLA, 0, 0, 0, 2, N'ដុល្លា', N'$', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 2, @TYPE_COMMERCIAL_NO_SUBSIDY, 6, @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO, 0, 0, 0, 2, N'ដុល្លា', N'$', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 2, @TYPE_COMMERCIAL_NO_SUBSIDY, 5, @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO_TOU, 0, 0, 0, 2, N'ដុល្លា', N'$', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 2, @TYPE_COMMERCIAL_NO_SUBSIDY, 4, @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO_TOU_1, 0, 0, 0, 2, N'ដុល្លា', N'$', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 2, @TYPE_COMMERCIAL_NO_SUBSIDY, 3, @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO_TOU_2, 0, 0, 0, 2, N'ដុល្លា', N'$', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 2, @TYPE_COMMERCIAL_NO_SUBSIDY, 2, @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO_TOU_SOLA, 0, 0, 0, 2, N'ដុល្លា', N'$', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 2, @TYPE_COMMERCIAL_NO_SUBSIDY, 1, @TYPE_COMMERCIAL_SALE_LV_USAGE_OVER_2000, 0, 0, 0, 1, N'រៀល', N'៛', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 1, @TYPE_SUBSIDY, 0, @TYPE_COMMERCIAL_SALE_SCHOOL_HOSPITAL_HEALTH_CENTER_CUSTOMER, 0, 0, 0, 1, N'រៀល', N'៛', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 1, @TYPE_SUBSIDY, -1, @TYPE_COMMERCIAL_SALE_CUSTOMER_AGRICULTURE_1, 0, 0, 0, 1, N'រៀល', N'៛', 1
  ); --AGRI
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 1, @TYPE_SUBSIDY, -2, @TYPE_SUBSIDY_INDUSTRY_COMMERCIAL_SALE_LV_USAGE_UNDER_2001, 0, 0, 0, 1, N'រៀល', N'៛', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 1, @TYPE_SUBSIDY, -3, @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_NORMAL, 0, 0, 0, 1, N'រៀល', N'៛', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 1, @TYPE_SUBSIDY, -4, @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_0_10, 0, 0, 0, 1, N'រៀល', N'៛', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 1, @TYPE_SUBSIDY, -5, @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_11_50, 0, 0, 0, 1, N'រៀល', N'៛', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 1, @TYPE_SUBSIDY, -6, @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_51_200, 0, 0, 0, 1, N'រៀល', N'៛', 1
  );
  INSERT INTO #DEFAULT_RESULT
  VALUES
  (
  @DATA_MONTH, 1, @TYPE_SUBSIDY, -7, @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_OVER_200, 0, 0, 0, 1, N'រៀល', N'៛', 1
  );

  -- RESULT
  INSERT INTO TBL_REF_02
  SELECT * FROM #RESULT
  UNION ALL
  SELECT
    *
  FROM #DEFAULT_RESULT t
  WHERE NOT EXISTS
  (SELECT
     *
   FROM #RESULT
   WHERE TYPE_ID=t.TYPE_ID AND TYPE_OF_SALE_ID=t.TYPE_OF_SALE_ID)
  ORDER BY TYPE_ID DESC,
    TYPE_OF_SALE_ID DESC;
END;
ELSE IF (@LICENSE_TYPE_ID=2) BEGIN
       -- Market vendor customer
       SELECT
         DATA_MONTH=@DATA_MONTH,
         TYPE_ID=1,
         TYPE_NAME=@TYPE_SUBSIDY_SPECIAL_FOR_MARKET_VENDOR,
         TYPE_OF_SALE_ID=1,
         TYPE_OF_SALE_NAME=@TYPE_SALE_CUSTOMER_MARKET_VENDOR,
         TOTAL_CUSTOMER=COUNT(*),
         TOTAL_POWER_SOLD=SUM(TOTAL_USAGE),
         RATE=PRICE,
         CURRENCY_ID,
         CURRENCY_NAME,
         CURRENCY_SIGN,
         IS_ACTIVE=1
       INTO
         #RESULT_MARKET_VENDOR
       FROM TBL_REF_INVOICE i
       WHERE DATA_MONTH=@DATA_MONTH AND i.CUSTOMER_CONNECTION_TYPE_ID IN (14, 15, 16) AND
                                                                                      (@BILLING_CYCLE_ID=0 OR i.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND (@AREA_ID=0 OR i.AREA_ID=@AREA_ID)
       GROUP BY CUSTOMER_GROUP_ID,
         CUSTOMER_GROUP_NAME,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME,
         PRICE,
         CURRENCY_ID,
         CURRENCY_NAME,
         CURRENCY_SIGN;
       CREATE TABLE #DEFAULT_RESULT_MARKET_VENDOR
       (
       DATA_MONTH DATETIME,
       TYPE_ID INT,
       TYPE_NAME NVARCHAR(200),
       TYPE_OF_SALE_ID INT,
       TYPE_OF_SALE_NAME NVARCHAR(200),
       TOTAL_CUSTOMER INT,
       TOTAL_POWER_SOLD INT,
       RATE DECIMAL(18, 4),
       CURRENCY_ID INT,
       CURRENCY_NAME NVARCHAR(20),
       CURRENCY_SIGN NVARCHAR(5),
       IS_ACTIVE BIT
       );
       INSERT INTO #DEFAULT_RESULT_MARKET_VENDOR
       VALUES
       (
       @DATA_MONTH, 1, @TYPE_LICENSEE, 1, @TYPE_SALE_CUSTOMER_MARKET_VENDOR, 0, 0, 0, 1, N'រៀល', N'៛', 1
       );

       -- RESULT
       INSERT INTO TBL_REF_02
       SELECT * FROM #RESULT_MARKET_VENDOR
       UNION ALL
       SELECT
         *
       FROM #DEFAULT_RESULT_MARKET_VENDOR t
       WHERE NOT EXISTS
       (SELECT
          *
        FROM #RESULT_MARKET_VENDOR
        WHERE TYPE_ID=t.TYPE_ID AND TYPE_OF_SALE_ID=t.TYPE_OF_SALE_ID)
       ORDER BY TYPE_ID DESC,
         TYPE_OF_SALE_ID DESC;
END;
--END OF RUN_REF_02_2020
GO
IF OBJECT_ID('REPORT_POWER_SOLD_2018') IS NOT NULL
  DROP PROC REPORT_POWER_SOLD_2018;
GO
CREATE PROC [dbo].[REPORT_POWER_SOLD_2018]
  @YEAR_ID INT=2020,
  @AREA_ID INT=0,
  @BILLING_CYCLE_ID INT=0
AS

/*
	@Kheang kimkhorn 2020-10-22
	- Remove validation with TBL_LICENSEE_CONNECTION
*/
DECLARE @MONTH DATETIME;
SET @MONTH=CAST(@YEAR_ID AS NVARCHAR(50))+'-01-01';
-- collect all data
SELECT
  i.INVOICE_MONTH,
  c.CUSTOMER_TYPE_ID,
  i.CUSTOMER_CONNECTION_TYPE_ID,
  ct.CUSTOMER_TYPE_NAME,
  cct.CUSTOMER_CONNECTION_TYPE_NAME,
  USAGE=i.TOTAL_USAGE+ISNULL(adj.ADJUST_USAGE, 0),
  c.CUSTOMER_ID,
  i.PRICE
INTO
  #TMP
FROM TBL_INVOICE i
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=i.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_TYPE ct ON ct.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
INNER JOIN TLKP_CUSTOMER_CONNECTION_TYPE cct ON cct.CUSTOMER_CONNECTION_TYPE_ID=i.CUSTOMER_CONNECTION_TYPE_ID
OUTER APPLY
(SELECT
   ADJUST_USAGE=SUM(ADJUST_USAGE)
 FROM TBL_INVOICE_ADJUSTMENT
 WHERE INVOICE_ID=i.INVOICE_ID) adj
WHERE YEAR(INVOICE_MONTH)=@YEAR_ID AND i.IS_SERVICE_BILL=0 AND (@AREA_ID=0 OR c.AREA_ID=@AREA_ID) AND
                                                                                                  (@BILLING_CYCLE_ID=0 OR c.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) AND c.IS_REACTIVE=0 AND i.INVOICE_STATUS NOT IN (3)
UNION ALL
SELECT
  cr.CREDIT_MONTH,
  c.CUSTOMER_TYPE_ID,
  c.CUSTOMER_CONNECTION_TYPE_ID,
  ct.CUSTOMER_TYPE_NAME,
  cct.CUSTOMER_CONNECTION_TYPE_NAME,
  USAGE=cr.USAGE,
  c.CUSTOMER_ID,
  cr.PRICE
FROM TBL_PREPAID_CREDIT cr
INNER JOIN TBL_CUSTOMER c ON cr.CUSTOMER_ID=c.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_TYPE ct ON ct.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
INNER JOIN TLKP_CUSTOMER_CONNECTION_TYPE cct ON cct.CUSTOMER_CONNECTION_TYPE_ID=c.CUSTOMER_CONNECTION_TYPE_ID
ORDER BY i.INVOICE_MONTH,
  ct.CUSTOMER_TYPE_NAME;

-- for customer LICENSEE MV,LV/MV transfo Licensee and LV/MV transfo Customer
SELECT
  INVOICE_MONTH,
  USAGE_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN USAGE ELSE 0 END),
  USAGE_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN USAGE ELSE 0 END),
  USAGE_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN USAGE ELSE 0 END),
  USAGE_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN USAGE ELSE 0 END),
  USAGE_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN USAGE ELSE 0 END),
  TOTAL_USAGE=SUM(USAGE),
  CUSTOMER_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN 1 ELSE 0 END),
  CUSTOMER_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN 1 ELSE 0 END),
  CUSTOMER_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN 1 ELSE 0 END),
  CUSTOMER_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN 1 ELSE 0 END),
  CUSTOMER_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN 1 ELSE 0 END),
  TOTAL_CUSTOMER=COUNT(*),
  CUSTOMER_CONNECTION_TYPE_ID,
  GROUP_ID=1
INTO
  #RESULT
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN (11, 12, 13)
--AND CUSTOMER_ID IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
GROUP BY INVOICE_MONTH,
  CUSTOMER_CONNECTION_TYPE_ID,
  CUSTOMER_CONNECTION_TYPE_NAME

-- for customer MV,LV/MV transfo Licensee and LV/MV transfo Customer
UNION ALL
SELECT
  INVOICE_MONTH,
  USAGE_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN USAGE ELSE 0 END),
  USAGE_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN USAGE ELSE 0 END),
  USAGE_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN USAGE ELSE 0 END),
  USAGE_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN USAGE ELSE 0 END),
  USAGE_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN USAGE ELSE 0 END),
  TOTAL_USAGE=SUM(USAGE),
  CUSTOMER_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN 1 ELSE 0 END),
  CUSTOMER_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN 1 ELSE 0 END),
  CUSTOMER_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN 1 ELSE 0 END),
  CUSTOMER_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN 1 ELSE 0 END),
  CUSTOMER_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN 1 ELSE 0 END),
  TOTAL_CUSTOMER=COUNT(*),
  CUSTOMER_CONNECTION_TYPE_ID,
  GROUP_ID=2
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN (2, 3, 4, 5, 6, 8)
--AND CUSTOMER_ID NOT IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
GROUP BY INVOICE_MONTH,
  CUSTOMER_CONNECTION_TYPE_ID,
  CUSTOMER_CONNECTION_TYPE_NAME

-- for  TOU customer
UNION ALL
SELECT
  INVOICE_MONTH,
  USAGE_0_10=0,
  USAGE_11_50=0,
  USAGE_51_200=0,
  USAGE_201_2000=0,
  USAGE_OVER_2001=0,
  TOTAL_USAGE=0,
  CUSTOMER_0_10=0,
  CUSTOMER_11_50=0,
  CUSTOMER_51_200=0,
  CUSTOMER_201_2000=0,
  CUSTOMER_OVER_2001=0,
  TOTAL_CUSTOMER=0,
  CUSTOMER_CONNECTION_TYPE_ID,
  GROUP_ID=2
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN (7)
--AND CUSTOMER_ID NOT IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
GROUP BY INVOICE_MONTH,
  CUSTOMER_CONNECTION_TYPE_ID,
  CUSTOMER_CONNECTION_TYPE_NAME

-- for TOU customer(7AM:9PM)
UNION ALL
SELECT
  INVOICE_MONTH,
  USAGE_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN USAGE ELSE 0 END),
  USAGE_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN USAGE ELSE 0 END),
  USAGE_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN USAGE ELSE 0 END),
  USAGE_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN USAGE ELSE 0 END),
  USAGE_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN USAGE ELSE 0 END),
  TOTAL_USAGE=SUM(USAGE),
  CUSTOMER_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN 1 ELSE 0 END),
  CUSTOMER_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN 1 ELSE 0 END),
  CUSTOMER_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN 1 ELSE 0 END),
  CUSTOMER_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN 1 ELSE 0 END),
  CUSTOMER_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN 1 ELSE 0 END),
  TOTAL_CUSTOMER=COUNT(*),
  CUSTOMER_CONNECTION_TYPE_ID=21,
  GROUP_ID=2
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN (7) AND PRICE=0.1300
--AND CUSTOMER_ID NOT IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
GROUP BY INVOICE_MONTH,
  CUSTOMER_CONNECTION_TYPE_ID,
  CUSTOMER_CONNECTION_TYPE_NAME

-- for TOU customer(9PM:7AM)
UNION ALL
SELECT
  INVOICE_MONTH,
  USAGE_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN USAGE ELSE 0 END),
  USAGE_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN USAGE ELSE 0 END),
  USAGE_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN USAGE ELSE 0 END),
  USAGE_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN USAGE ELSE 0 END),
  USAGE_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN USAGE ELSE 0 END),
  TOTAL_USAGE=SUM(USAGE),
  CUSTOMER_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN 1 ELSE 0 END),
  CUSTOMER_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN 1 ELSE 0 END),
  CUSTOMER_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN 1 ELSE 0 END),
  CUSTOMER_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN 1 ELSE 0 END),
  CUSTOMER_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN 1 ELSE 0 END),
  TOTAL_CUSTOMER=COUNT(*),
  CUSTOMER_CONNECTION_TYPE_ID=22,
  GROUP_ID=2
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN (7) AND PRICE=0.1100
--AND CUSTOMER_ID NOT IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
GROUP BY INVOICE_MONTH,
  CUSTOMER_CONNECTION_TYPE_ID,
  CUSTOMER_CONNECTION_TYPE_NAME

-- for  TOU customer1
UNION ALL
SELECT
  INVOICE_MONTH,
  USAGE_0_10=0,
  USAGE_11_50=0,
  USAGE_51_200=0,
  USAGE_201_2000=0,
  USAGE_OVER_2001=0,
  TOTAL_USAGE=0,
  CUSTOMER_0_10=0,
  CUSTOMER_11_50=0,
  CUSTOMER_51_200=0,
  CUSTOMER_201_2000=0,
  CUSTOMER_OVER_2001=0,
  TOTAL_CUSTOMER=0,
  CUSTOMER_CONNECTION_TYPE_ID,
  GROUP_ID=2
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN (10)
--AND CUSTOMER_ID NOT IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
GROUP BY INVOICE_MONTH,
  CUSTOMER_CONNECTION_TYPE_ID,
  CUSTOMER_CONNECTION_TYPE_NAME

-- for  TOU customer1 (7AM:9PM)
UNION ALL
SELECT
  INVOICE_MONTH,
  USAGE_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN USAGE ELSE 0 END),
  USAGE_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN USAGE ELSE 0 END),
  USAGE_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN USAGE ELSE 0 END),
  USAGE_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN USAGE ELSE 0 END),
  USAGE_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN USAGE ELSE 0 END),
  TOTAL_USAGE=SUM(USAGE),
  CUSTOMER_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN 1 ELSE 0 END),
  CUSTOMER_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN 1 ELSE 0 END),
  CUSTOMER_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN 1 ELSE 0 END),
  CUSTOMER_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN 1 ELSE 0 END),
  CUSTOMER_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN 1 ELSE 0 END),
  TOTAL_CUSTOMER=COUNT(*),
  CUSTOMER_CONNECTION_TYPE_ID=23,
  GROUP_ID=2
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN (10) AND PRICE=0.1500
--AND CUSTOMER_ID NOT IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
GROUP BY INVOICE_MONTH,
  CUSTOMER_CONNECTION_TYPE_ID,
  CUSTOMER_CONNECTION_TYPE_NAME

-- for  TOU customer1 (9PM:7AM)
UNION ALL
SELECT
  INVOICE_MONTH,
  USAGE_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN USAGE ELSE 0 END),
  USAGE_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN USAGE ELSE 0 END),
  USAGE_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN USAGE ELSE 0 END),
  USAGE_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN USAGE ELSE 0 END),
  USAGE_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN USAGE ELSE 0 END),
  TOTAL_USAGE=SUM(USAGE),
  CUSTOMER_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN 1 ELSE 0 END),
  CUSTOMER_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN 1 ELSE 0 END),
  CUSTOMER_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN 1 ELSE 0 END),
  CUSTOMER_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN 1 ELSE 0 END),
  CUSTOMER_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN 1 ELSE 0 END),
  TOTAL_CUSTOMER=COUNT(*),
  CUSTOMER_CONNECTION_TYPE_ID=24,
  GROUP_ID=2
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN (10) AND PRICE=0.1240
--AND CUSTOMER_ID NOT IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
GROUP BY INVOICE_MONTH,
  CUSTOMER_CONNECTION_TYPE_ID,
  CUSTOMER_CONNECTION_TYPE_NAME

-- for  TOU mv/lv industry  customer's transformer
UNION ALL
SELECT
  INVOICE_MONTH,
  USAGE_0_10=0,
  USAGE_11_50=0,
  USAGE_51_200=0,
  USAGE_201_2000=0,
  USAGE_OVER_2001=0,
  TOTAL_USAGE=0,
  CUSTOMER_0_10=0,
  CUSTOMER_11_50=0,
  CUSTOMER_51_200=0,
  CUSTOMER_201_2000=0,
  CUSTOMER_OVER_2001=0,
  TOTAL_CUSTOMER=0,
  CUSTOMER_CONNECTION_TYPE_ID,
  GROUP_ID=2
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN (17)
--AND CUSTOMER_ID NOT IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
GROUP BY INVOICE_MONTH,
  CUSTOMER_CONNECTION_TYPE_ID,
  CUSTOMER_CONNECTION_TYPE_NAME

-- for  TOU mv/lv industry  customer's transformer (7AM:9PM)
UNION ALL
SELECT
  INVOICE_MONTH,
  USAGE_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN USAGE ELSE 0 END),
  USAGE_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN USAGE ELSE 0 END),
  USAGE_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN USAGE ELSE 0 END),
  USAGE_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN USAGE ELSE 0 END),
  USAGE_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN USAGE ELSE 0 END),
  TOTAL_USAGE=SUM(USAGE),
  CUSTOMER_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN 1 ELSE 0 END),
  CUSTOMER_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN 1 ELSE 0 END),
  CUSTOMER_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN 1 ELSE 0 END),
  CUSTOMER_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN 1 ELSE 0 END),
  CUSTOMER_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN 1 ELSE 0 END),
  TOTAL_CUSTOMER=COUNT(*),
  CUSTOMER_CONNECTION_TYPE_ID=25,
  GROUP_ID=2
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN (17) AND PRICE=0.1352
--AND CUSTOMER_ID NOT IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
GROUP BY INVOICE_MONTH,
  CUSTOMER_CONNECTION_TYPE_ID,
  CUSTOMER_CONNECTION_TYPE_NAME

-- for  TOU mv/lv industry  customer's transformer (9PM:7AM)
UNION ALL
SELECT
  INVOICE_MONTH,
  USAGE_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN USAGE ELSE 0 END),
  USAGE_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN USAGE ELSE 0 END),
  USAGE_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN USAGE ELSE 0 END),
  USAGE_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN USAGE ELSE 0 END),
  USAGE_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN USAGE ELSE 0 END),
  TOTAL_USAGE=SUM(USAGE),
  CUSTOMER_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN 1 ELSE 0 END),
  CUSTOMER_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN 1 ELSE 0 END),
  CUSTOMER_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN 1 ELSE 0 END),
  CUSTOMER_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN 1 ELSE 0 END),
  CUSTOMER_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN 1 ELSE 0 END),
  TOTAL_CUSTOMER=COUNT(*),
  CUSTOMER_CONNECTION_TYPE_ID=26,
  GROUP_ID=2
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN (17) AND PRICE=0.1144
--AND CUSTOMER_ID NOT IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
GROUP BY INVOICE_MONTH,
  CUSTOMER_CONNECTION_TYPE_ID,
  CUSTOMER_CONNECTION_TYPE_NAME

-- for  TOU mv/lv industry  lincensee's transformer
UNION ALL
SELECT
  INVOICE_MONTH,
  USAGE_0_10=0,
  USAGE_11_50=0,
  USAGE_51_200=0,
  USAGE_201_2000=0,
  USAGE_OVER_2001=0,
  TOTAL_USAGE=0,
  CUSTOMER_0_10=0,
  CUSTOMER_11_50=0,
  CUSTOMER_51_200=0,
  CUSTOMER_201_2000=0,
  CUSTOMER_OVER_2001=0,
  TOTAL_CUSTOMER=0,
  CUSTOMER_CONNECTION_TYPE_ID,
  GROUP_ID=2
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN (18)
--AND CUSTOMER_ID NOT IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
GROUP BY INVOICE_MONTH,
  CUSTOMER_CONNECTION_TYPE_ID,
  CUSTOMER_CONNECTION_TYPE_NAME

-- for  TOU mv/lv industry  lincensee's transformer(7AM:9PM)
UNION ALL
SELECT
  INVOICE_MONTH,
  USAGE_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN USAGE ELSE 0 END),
  USAGE_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN USAGE ELSE 0 END),
  USAGE_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN USAGE ELSE 0 END),
  USAGE_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN USAGE ELSE 0 END),
  USAGE_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN USAGE ELSE 0 END),
  TOTAL_USAGE=SUM(USAGE),
  CUSTOMER_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN 1 ELSE 0 END),
  CUSTOMER_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN 1 ELSE 0 END),
  CUSTOMER_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN 1 ELSE 0 END),
  CUSTOMER_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN 1 ELSE 0 END),
  CUSTOMER_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN 1 ELSE 0 END),
  TOTAL_CUSTOMER=COUNT(*),
  CUSTOMER_CONNECTION_TYPE_ID=27,
  GROUP_ID=2
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN (18) AND PRICE=0.1432
--AND CUSTOMER_ID NOT IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
GROUP BY INVOICE_MONTH,
  CUSTOMER_CONNECTION_TYPE_ID,
  CUSTOMER_CONNECTION_TYPE_NAME

-- for  TOU mv/lv industry  lincensee's transformer(9PM:7AM)
UNION ALL
SELECT
  INVOICE_MONTH,
  USAGE_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN USAGE ELSE 0 END),
  USAGE_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN USAGE ELSE 0 END),
  USAGE_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN USAGE ELSE 0 END),
  USAGE_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN USAGE ELSE 0 END),
  USAGE_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN USAGE ELSE 0 END),
  TOTAL_USAGE=SUM(USAGE),
  CUSTOMER_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN 1 ELSE 0 END),
  CUSTOMER_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN 1 ELSE 0 END),
  CUSTOMER_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN 1 ELSE 0 END),
  CUSTOMER_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN 1 ELSE 0 END),
  CUSTOMER_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN 1 ELSE 0 END),
  TOTAL_CUSTOMER=COUNT(*),
  CUSTOMER_CONNECTION_TYPE_ID=28,
  GROUP_ID=2
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN (18) AND PRICE=0.1224
--AND CUSTOMER_ID NOT IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
GROUP BY INVOICE_MONTH,
  CUSTOMER_CONNECTION_TYPE_ID,
  CUSTOMER_CONNECTION_TYPE_NAME

-- for  TOU mv/lv commercial  customer's transformer
UNION ALL
SELECT
  INVOICE_MONTH,
  USAGE_0_10=0,
  USAGE_11_50=0,
  USAGE_51_200=0,
  USAGE_201_2000=0,
  USAGE_OVER_2001=0,
  TOTAL_USAGE=0,
  CUSTOMER_0_10=0,
  CUSTOMER_11_50=0,
  CUSTOMER_51_200=0,
  CUSTOMER_201_2000=0,
  CUSTOMER_OVER_2001=0,
  TOTAL_CUSTOMER=0,
  CUSTOMER_CONNECTION_TYPE_ID,
  GROUP_ID=2
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN (19)
--AND CUSTOMER_ID NOT IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
GROUP BY INVOICE_MONTH,
  CUSTOMER_CONNECTION_TYPE_ID,
  CUSTOMER_CONNECTION_TYPE_NAME

-- for  TOU mv/lv commercial  customer's transformer(7AM:9PM)
UNION ALL
SELECT
  INVOICE_MONTH,
  USAGE_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN USAGE ELSE 0 END),
  USAGE_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN USAGE ELSE 0 END),
  USAGE_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN USAGE ELSE 0 END),
  USAGE_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN USAGE ELSE 0 END),
  USAGE_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN USAGE ELSE 0 END),
  TOTAL_USAGE=SUM(USAGE),
  CUSTOMER_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN 1 ELSE 0 END),
  CUSTOMER_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN 1 ELSE 0 END),
  CUSTOMER_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN 1 ELSE 0 END),
  CUSTOMER_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN 1 ELSE 0 END),
  CUSTOMER_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN 1 ELSE 0 END),
  TOTAL_CUSTOMER=COUNT(*),
  CUSTOMER_CONNECTION_TYPE_ID=29,
  GROUP_ID=2
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN (19) AND PRICE=0.1560
--AND CUSTOMER_ID NOT IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
GROUP BY INVOICE_MONTH,
  CUSTOMER_CONNECTION_TYPE_ID,
  CUSTOMER_CONNECTION_TYPE_NAME

-- for  TOU mv/lv commercial  customer's transformer(9PM:7AM)
UNION ALL
SELECT
  INVOICE_MONTH,
  USAGE_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN USAGE ELSE 0 END),
  USAGE_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN USAGE ELSE 0 END),
  USAGE_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN USAGE ELSE 0 END),
  USAGE_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN USAGE ELSE 0 END),
  USAGE_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN USAGE ELSE 0 END),
  TOTAL_USAGE=SUM(USAGE),
  CUSTOMER_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN 1 ELSE 0 END),
  CUSTOMER_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN 1 ELSE 0 END),
  CUSTOMER_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN 1 ELSE 0 END),
  CUSTOMER_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN 1 ELSE 0 END),
  CUSTOMER_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN 1 ELSE 0 END),
  TOTAL_CUSTOMER=COUNT(*),
  CUSTOMER_CONNECTION_TYPE_ID=30,
  GROUP_ID=2
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN (19) AND PRICE=0.12896
--AND CUSTOMER_ID NOT IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
GROUP BY INVOICE_MONTH,
  CUSTOMER_CONNECTION_TYPE_ID,
  CUSTOMER_CONNECTION_TYPE_NAME

-- for  TOU mv/lv commercial  lincensee's transformer
UNION ALL
SELECT
  INVOICE_MONTH,
  USAGE_0_10=0,
  USAGE_11_50=0,
  USAGE_51_200=0,
  USAGE_201_2000=0,
  USAGE_OVER_2001=0,
  TOTAL_USAGE=0,
  CUSTOMER_0_10=0,
  CUSTOMER_11_50=0,
  CUSTOMER_51_200=0,
  CUSTOMER_201_2000=0,
  CUSTOMER_OVER_2001=0,
  TOTAL_CUSTOMER=0,
  CUSTOMER_CONNECTION_TYPE_ID,
  GROUP_ID=2
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN (20)
--AND CUSTOMER_ID NOT IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
GROUP BY INVOICE_MONTH,
  CUSTOMER_CONNECTION_TYPE_ID,
  CUSTOMER_CONNECTION_TYPE_NAME

-- for  TOU mv/lv commercial  lincensee's transformer(7AM:9PM)
UNION ALL
SELECT
  INVOICE_MONTH,
  USAGE_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN USAGE ELSE 0 END),
  USAGE_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN USAGE ELSE 0 END),
  USAGE_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN USAGE ELSE 0 END),
  USAGE_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN USAGE ELSE 0 END),
  USAGE_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN USAGE ELSE 0 END),
  TOTAL_USAGE=SUM(USAGE),
  CUSTOMER_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN 1 ELSE 0 END),
  CUSTOMER_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN 1 ELSE 0 END),
  CUSTOMER_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN 1 ELSE 0 END),
  CUSTOMER_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN 1 ELSE 0 END),
  CUSTOMER_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN 1 ELSE 0 END),
  TOTAL_CUSTOMER=COUNT(*),
  CUSTOMER_CONNECTION_TYPE_ID=31,
  GROUP_ID=2
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN (20) AND PRICE=0.1640
--AND CUSTOMER_ID NOT IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
GROUP BY INVOICE_MONTH,
  CUSTOMER_CONNECTION_TYPE_ID,
  CUSTOMER_CONNECTION_TYPE_NAME

-- for  TOU mv/lv commercial  lincensee's transformer(9PM:7AM)
UNION ALL
SELECT
  INVOICE_MONTH,
  USAGE_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN USAGE ELSE 0 END),
  USAGE_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN USAGE ELSE 0 END),
  USAGE_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN USAGE ELSE 0 END),
  USAGE_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN USAGE ELSE 0 END),
  USAGE_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN USAGE ELSE 0 END),
  TOTAL_USAGE=SUM(USAGE),
  CUSTOMER_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN 1 ELSE 0 END),
  CUSTOMER_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN 1 ELSE 0 END),
  CUSTOMER_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN 1 ELSE 0 END),
  CUSTOMER_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN 1 ELSE 0 END),
  CUSTOMER_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN 1 ELSE 0 END),
  TOTAL_CUSTOMER=COUNT(*),
  CUSTOMER_CONNECTION_TYPE_ID=32,
  GROUP_ID=2
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN (20) AND PRICE=0.13696
--AND CUSTOMER_ID NOT IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
GROUP BY INVOICE_MONTH,
  CUSTOMER_CONNECTION_TYPE_ID,
  CUSTOMER_CONNECTION_TYPE_NAME

-- for customer normal business as Telecome,Bank,..
UNION ALL
SELECT
  INVOICE_MONTH,
  USAGE_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN USAGE ELSE 0 END),
  USAGE_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN USAGE ELSE 0 END),
  USAGE_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN USAGE ELSE 0 END),
  USAGE_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN USAGE ELSE 0 END),
  USAGE_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN USAGE ELSE 0 END),
  TOTAL_USAGE=SUM(USAGE),
  CUSTOMER_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN 1 ELSE 0 END),
  CUSTOMER_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN 1 ELSE 0 END),
  CUSTOMER_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN 1 ELSE 0 END),
  CUSTOMER_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN 1 ELSE 0 END),
  CUSTOMER_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN 1 ELSE 0 END),
  TOTAL_CUSTOMER=COUNT(*),
  CUSTOMER_CONNECTION_TYPE_ID=0,
  GROUP_ID=3
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN (14, 15, 16)
--AND CUSTOMER_ID NOT IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
GROUP BY INVOICE_MONTH

-- for customer normal 
UNION ALL
SELECT
  INVOICE_MONTH,
  USAGE_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN USAGE ELSE 0 END),
  USAGE_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN USAGE ELSE 0 END),
  USAGE_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN USAGE ELSE 0 END),
  USAGE_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN USAGE ELSE 0 END),
  USAGE_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN USAGE ELSE 0 END),
  TOTAL_USAGE=SUM(USAGE),
  CUSTOMER_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN 1 ELSE 0 END),
  CUSTOMER_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN 1 ELSE 0 END),
  CUSTOMER_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN 1 ELSE 0 END),
  CUSTOMER_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN 1 ELSE 0 END),
  CUSTOMER_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN 1 ELSE 0 END),
  TOTAL_CUSTOMER=COUNT(*),
  CUSTOMER_CONNECTION_TYPE_ID=1,
  GROUP_ID=4
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN (1)
--AND CUSTOMER_ID NOT IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
GROUP BY INVOICE_MONTH,
  CUSTOMER_CONNECTION_TYPE_ID,
  CUSTOMER_CONNECTION_TYPE_NAME

-- for customer governance school, hospital at country side 
UNION ALL
SELECT
  INVOICE_MONTH,
  USAGE_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN USAGE ELSE 0 END),
  USAGE_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN USAGE ELSE 0 END),
  USAGE_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN USAGE ELSE 0 END),
  USAGE_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN USAGE ELSE 0 END),
  USAGE_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN USAGE ELSE 0 END),
  TOTAL_USAGE=SUM(USAGE),
  CUSTOMER_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN 1 ELSE 0 END),
  CUSTOMER_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN 1 ELSE 0 END),
  CUSTOMER_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN 1 ELSE 0 END),
  CUSTOMER_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN 1 ELSE 0 END),
  CUSTOMER_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN 1 ELSE 0 END),
  TOTAL_CUSTOMER=COUNT(*),
  CUSTOMER_CONNECTION_TYPE_ID=9,
  GROUP_ID=5
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN (9)
--AND CUSTOMER_ID NOT IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
GROUP BY INVOICE_MONTH,
  CUSTOMER_CONNECTION_TYPE_ID,
  CUSTOMER_CONNECTION_TYPE_NAME

---TOU SOLA  MV Industry
UNION ALL
SELECT
  INVOICE_MONTH,
  USAGE_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN USAGE ELSE 0 END),
  USAGE_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN USAGE ELSE 0 END),
  USAGE_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN USAGE ELSE 0 END),
  USAGE_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN USAGE ELSE 0 END),
  USAGE_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN USAGE ELSE 0 END),
  TOTAL_USAGE=SUM(USAGE),
  CUSTOMER_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN 1 ELSE 0 END),
  CUSTOMER_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN 1 ELSE 0 END),
  CUSTOMER_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN 1 ELSE 0 END),
  CUSTOMER_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN 1 ELSE 0 END),
  CUSTOMER_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN 1 ELSE 0 END),
  TOTAL_CUSTOMER=COUNT(*),
  CUSTOMER_CONNECTION_TYPE_ID=33,
  GROUP_ID=2
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN (21) AND PRICE=0.13000
--AND CUSTOMER_ID NOT IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
GROUP BY INVOICE_MONTH,
  CUSTOMER_CONNECTION_TYPE_ID,
  CUSTOMER_CONNECTION_TYPE_NAME

---TOU SOLA  MV/LV Business
UNION ALL
SELECT
  INVOICE_MONTH,
  USAGE_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN USAGE ELSE 0 END),
  USAGE_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN USAGE ELSE 0 END),
  USAGE_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN USAGE ELSE 0 END),
  USAGE_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN USAGE ELSE 0 END),
  USAGE_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN USAGE ELSE 0 END),
  TOTAL_USAGE=SUM(USAGE),
  CUSTOMER_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN 1 ELSE 0 END),
  CUSTOMER_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN 1 ELSE 0 END),
  CUSTOMER_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN 1 ELSE 0 END),
  CUSTOMER_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN 1 ELSE 0 END),
  CUSTOMER_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN 1 ELSE 0 END),
  TOTAL_CUSTOMER=COUNT(*),
  CUSTOMER_CONNECTION_TYPE_ID=34,
  GROUP_ID=2
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN (24) AND PRICE=0.15000
--AND CUSTOMER_ID NOT IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
GROUP BY INVOICE_MONTH,
  CUSTOMER_CONNECTION_TYPE_ID,
  CUSTOMER_CONNECTION_TYPE_NAME

---TOU SOLA  MV/LV Industry Customer transfo
UNION ALL
SELECT
  INVOICE_MONTH,
  USAGE_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN USAGE ELSE 0 END),
  USAGE_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN USAGE ELSE 0 END),
  USAGE_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN USAGE ELSE 0 END),
  USAGE_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN USAGE ELSE 0 END),
  USAGE_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN USAGE ELSE 0 END),
  TOTAL_USAGE=SUM(USAGE),
  CUSTOMER_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN 1 ELSE 0 END),
  CUSTOMER_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN 1 ELSE 0 END),
  CUSTOMER_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN 1 ELSE 0 END),
  CUSTOMER_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN 1 ELSE 0 END),
  CUSTOMER_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN 1 ELSE 0 END),
  TOTAL_CUSTOMER=COUNT(*),
  CUSTOMER_CONNECTION_TYPE_ID=35,
  GROUP_ID=2
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN (22) AND PRICE=0.13520
--AND CUSTOMER_ID NOT IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
GROUP BY INVOICE_MONTH,
  CUSTOMER_CONNECTION_TYPE_ID,
  CUSTOMER_CONNECTION_TYPE_NAME

---TOU SOLA  MV/LV Business Customer transfo
UNION ALL
SELECT
  INVOICE_MONTH,
  USAGE_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN USAGE ELSE 0 END),
  USAGE_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN USAGE ELSE 0 END),
  USAGE_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN USAGE ELSE 0 END),
  USAGE_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN USAGE ELSE 0 END),
  USAGE_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN USAGE ELSE 0 END),
  TOTAL_USAGE=SUM(USAGE),
  CUSTOMER_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN 1 ELSE 0 END),
  CUSTOMER_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN 1 ELSE 0 END),
  CUSTOMER_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN 1 ELSE 0 END),
  CUSTOMER_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN 1 ELSE 0 END),
  CUSTOMER_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN 1 ELSE 0 END),
  TOTAL_CUSTOMER=COUNT(*),
  CUSTOMER_CONNECTION_TYPE_ID=36,
  GROUP_ID=2
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN (25) AND PRICE=0.15600
--AND CUSTOMER_ID NOT IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
GROUP BY INVOICE_MONTH,
  CUSTOMER_CONNECTION_TYPE_ID,
  CUSTOMER_CONNECTION_TYPE_NAME

---TOU SOLA  MV/LV Industry Liecensee transfo
UNION ALL
SELECT
  INVOICE_MONTH,
  USAGE_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN USAGE ELSE 0 END),
  USAGE_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN USAGE ELSE 0 END),
  USAGE_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN USAGE ELSE 0 END),
  USAGE_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN USAGE ELSE 0 END),
  USAGE_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN USAGE ELSE 0 END),
  TOTAL_USAGE=SUM(USAGE),
  CUSTOMER_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN 1 ELSE 0 END),
  CUSTOMER_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN 1 ELSE 0 END),
  CUSTOMER_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN 1 ELSE 0 END),
  CUSTOMER_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN 1 ELSE 0 END),
  CUSTOMER_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN 1 ELSE 0 END),
  TOTAL_CUSTOMER=COUNT(*),
  CUSTOMER_CONNECTION_TYPE_ID=37,
  GROUP_ID=2
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN (23) AND PRICE=0.14320
--AND CUSTOMER_ID NOT IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
GROUP BY INVOICE_MONTH,
  CUSTOMER_CONNECTION_TYPE_ID,
  CUSTOMER_CONNECTION_TYPE_NAME

---TOU SOLA  MV/LV Business Liecensee transfo
UNION ALL
SELECT
  INVOICE_MONTH,
  USAGE_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN USAGE ELSE 0 END),
  USAGE_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN USAGE ELSE 0 END),
  USAGE_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN USAGE ELSE 0 END),
  USAGE_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN USAGE ELSE 0 END),
  USAGE_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN USAGE ELSE 0 END),
  TOTAL_USAGE=SUM(USAGE),
  CUSTOMER_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN 1 ELSE 0 END),
  CUSTOMER_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN 1 ELSE 0 END),
  CUSTOMER_51_200=SUM(CASE WHEN USAGE BETWEEN 51 AND 200 THEN 1 ELSE 0 END),
  CUSTOMER_201_2000=SUM(CASE WHEN USAGE BETWEEN 201 AND 2000 THEN 1 ELSE 0 END),
  CUSTOMER_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN 1 ELSE 0 END),
  TOTAL_CUSTOMER=COUNT(*),
  CUSTOMER_CONNECTION_TYPE_ID=38,
  GROUP_ID=2
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN (26) AND PRICE=0.16400
--AND CUSTOMER_ID NOT IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
GROUP BY INVOICE_MONTH,
  CUSTOMER_CONNECTION_TYPE_ID,
  CUSTOMER_CONNECTION_TYPE_NAME;

-- BUILD 12 MONTHs TABLE
CREATE TABLE #M
(
COL_ORDER INT,
ID INT,
NAME NVARCHAR(200),
INV_MONTH DATETIME,
GROUP_ID INT
);
DECLARE @M INT;
SET @M=1;
WHILE @M<=12 BEGIN
  INSERT INTO #M
  VALUES
  (
  1 , 11, N'អ្នកកាន់អាជ្ញាបណ្ណ ភ្ជាប់ពីខ្សែបណ្តាញតង់ស្យុងមធ្យម MV', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 1
  );
  INSERT INTO #M
  VALUES
  (
  2 , 12, N'អ្នកកាន់អាជ្ញាបណ្ណ ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកទិញ)', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 1
  );
  INSERT INTO #M
  VALUES
  (
  3 , 13, N'អ្នកកាន់អាជ្ញាបណ្ណ ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកលក់)', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 1
  );
  INSERT INTO #M
  VALUES
  (
  4 , 4, N'ប្រភេទឧស្សាហកម្ម និងកសិកម្ម ភ្ជាប់ពីខ្សែបណ្តាញតង់ស្យុងមធ្យម MV', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 2
  );
  INSERT INTO #M
  VALUES
  (
  5 , 7, N'ប្រភេទឧស្សាហកម្ម និងកសិកម្ម ភ្ជាប់ពីខ្សែបណ្តាញតង់ស្យុងមធ្យម MV ជម្រើសតាមពេលវេលា និងអានុភាព', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 2
  );
  INSERT INTO #M
  VALUES
  (
  6 , 21, N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកច្រើន (៧ព្រឹកដល់៩យប់)    ', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 2
  );
  INSERT INTO #M
  VALUES
  (
  7 , 22, N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកតិច (៩យប់ដល់៧ព្រឹក)     ', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 2
  );
  INSERT INTO #M
  VALUES
  (
  8 , 33, N'ប្រភេទឧស្សាហកម្ម និងកសិកម្ម ភ្ជាប់ពីខ្សែបណ្តាញតង់ស្យុងមធ្យម MV តម្លើងប្រភពពន្លឺព្រះអាទិត្យ', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 2
  );
  INSERT INTO #M
  VALUES
  (
  9 , 8, N'ប្រភេទពាណិជ្ជកម្ម រដ្ឋបាល និងផ្សេងៗ ភ្ជាប់ពីខ្សែបណ្តាញតង់ស្យុងមធ្យម MV', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 2
  );
  INSERT INTO #M
  VALUES
  (
  10, 10, N'ប្រភេទពាណិជ្ជកម្ម រដ្ឋបាល និងផ្សេងៗ ភ្ជាប់ពីខ្សែបណ្តាញតង់ស្យុងមធ្យម MV ជម្រើសតាមពេលវេលា និងអានុភាព', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 2
  );
  INSERT INTO #M
  VALUES
  (
  11, 23, N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកច្រើន (៧ព្រឹកដល់៩យប់)    ', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 2
  );
  INSERT INTO #M
  VALUES
  (
  12, 24, N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកតិច (៩យប់ដល់៧ព្រឹក)     ', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 2
  );
  INSERT INTO #M
  VALUES
  (
  13, 34, N'ប្រភេទពាណិជ្ជកម្ម រដ្ឋបាល និងផ្សេងៗ ភ្ជាប់ពីខ្សែបណ្តាញតង់ស្យុងមធ្យម MV  តម្លើងប្រភពពន្លឺព្រះអាទិត្យ', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 2
  );
  INSERT INTO #M
  VALUES
  (
  14, 3, N'ប្រភេទឧស្សាហកម្ម និងកសិកម្ម ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកទិញ)', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 2
  );
  INSERT INTO #M
  VALUES
  (
  15, 17, N'ប្រភេទឧស្សាហកម្ម និងកសិកម្ម ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកទិញ) ជម្រើសតាមពេលវេលា និងអានុភាព', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 2
  );
  INSERT INTO #M
  VALUES
  (
  16, 25, N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកច្រើន (៧ព្រឹកដល់៩យប់)    ', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 2
  );
  INSERT INTO #M
  VALUES
  (
  17, 26, N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកតិច (៩យប់ដល់៧ព្រឹក)     ', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 2
  );
  INSERT INTO #M
  VALUES
  (
  18, 35, N'ប្រភេទឧស្សាហកម្ម និងកសិកម្ម ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកទិញ) តម្លើងប្រភពពន្លឺព្រះអាទិត្យ', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 2
  );
  INSERT INTO #M
  VALUES
  (
  19, 2, N'ប្រភេទឧស្សាហកម្ម និងកសិកម្ម ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកលក់)', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 2
  );
  INSERT INTO #M
  VALUES
  (
  20, 18, N'ប្រភេទឧស្សាហកម្ម និងកសិកម្ម ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកលក់) ជម្រើសតាមពេលវេលា និងអានុភាព', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 2
  );
  INSERT INTO #M
  VALUES
  (
  21, 27, N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកច្រើន (៧ព្រឹកដល់៩យប់)    ', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 2
  );
  INSERT INTO #M
  VALUES
  (
  22, 28, N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកតិច (៩យប់ដល់៧ព្រឹក)     ', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 2
  );
  INSERT INTO #M
  VALUES
  (
  23, 36, N'ប្រភេទឧស្សាហកម្ម និងកសិកម្ម ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកលក់) តម្លើងប្រភពពន្លឺព្រះអាទិត្យ', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 2
  );
  INSERT INTO #M
  VALUES
  (
  24, 6, N'ពាណិជ្ជកម្ម រដ្ឋបាល និងផ្សេងៗ ភ្ជាប់ពីត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកទិញ)', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 2
  );
  INSERT INTO #M
  VALUES
  (
  25, 19, N'ពាណិជ្ជកម្ម រដ្ឋបាល និងផ្សេងៗ ភ្ជាប់ពីត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកទិញ) ជម្រើសតាមពេលវេលា និងអានុភាព', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 2
  );
  INSERT INTO #M
  VALUES
  (
  26, 29, N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកច្រើន (៧ព្រឹកដល់៩យប់)    ', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 2
  );
  INSERT INTO #M
  VALUES
  (
  27, 30, N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកតិច (៩យប់ដល់៧ព្រឹក)     ', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 2
  );
  INSERT INTO #M
  VALUES
  (
  28, 37, N'ពាណិជ្ជកម្ម រដ្ឋបាល និងផ្សេងៗ ភ្ជាប់ពីត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកទិញ) តម្លើងប្រភពពន្លឺព្រះអាទិត្យ', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 2
  );
  INSERT INTO #M
  VALUES
  (
  29, 5, N'ពាណិជ្ជកម្ម រដ្ឋបាល និងផ្សេងៗ ភ្ជាប់ពីត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកលក់)', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 2
  );
  INSERT INTO #M
  VALUES
  (
  30, 20, N'ពាណិជ្ជកម្ម រដ្ឋបាល និងផ្សេងៗ ភ្ជាប់ពីត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកលក់) ជម្រើសតាមពេលវេលា និងអានុភាព', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 2
  );
  INSERT INTO #M
  VALUES
  (
  31, 31, N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកច្រើន (៧ព្រឹកដល់៩យប់)    ', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 2
  );
  INSERT INTO #M
  VALUES
  (
  32, 32, N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកតិច (៩យប់ដល់៧ព្រឹក)     ', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 2
  );
  INSERT INTO #M
  VALUES
  (
  33, 38, N'ពាណិជ្ជកម្ម រដ្ឋបាល និងផ្សេងៗ ភ្ជាប់ពីត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកលក់) តម្លើងប្រភពពន្លឺព្រះអាទិត្យ', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 2
  );
  INSERT INTO #M
  VALUES
  (
  34, 0, N'អាជីវកម្មភ្ជាប់ពីបណ្តាញសាធារណៈ', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 3
  );
  INSERT INTO #M
  VALUES
  (
  35, 1, N'អតិថិជនលំនៅដ្ឋាន', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 4
  );
  INSERT INTO #M
  VALUES
  (
  36, 9, N'សាលារៀន, មន្ទីរពេទ្យ និងមណ្ឌលសុខភាព នៅជនបទ', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 5
  );
  INSERT INTO #M
  VALUES
  (
  37, -2, N'បូមទឹកសម្រាប់កសិកម្ម', CONVERT(NVARCHAR, @YEAR_ID)+'-'+CONVERT(NVARCHAR, @M)+'-1', 4
  );
  SET @M=@M+1;
END;
SELECT
  INVOICE_MONTH=m.INV_MONTH,
  CUSTOMER_TYPE_NAME=m.NAME,
  USAGE_0_10=ISNULL(USAGE_0_10, 0),
  USAGE_11_50=ISNULL(USAGE_11_50, 0),
  USAGE_51_200=ISNULL(USAGE_51_200, 0),
  USAGE_201_2000=ISNULL(USAGE_201_2000, 0),
  USAGE_OVER_2001=ISNULL(USAGE_OVER_2001, 0),
  TOTAL_USAGE=ISNULL(TOTAL_USAGE, 0),
  CUSTOMER_0_10=ISNULL(CUSTOMER_0_10, 0),
  CUSTOMER_11_50=ISNULL(CUSTOMER_11_50, 0),
  CUSTOMER_51_200=ISNULL(CUSTOMER_51_200, 0),
  CUSTOMER_201_2000=ISNULL(CUSTOMER_201_2000, 0),
  CUSTOMER_OVER_2001=ISNULL(CUSTOMER_OVER_2001, 0),
  TOTAL_CUSTOMER=ISNULL(TOTAL_CUSTOMER, 0),
  GROUP_TYPE_ID=1,
  m.ID,
  m.GROUP_ID
INTO
  #RESULT2
FROM #M m
LEFT JOIN #RESULT r ON r.INVOICE_MONTH=m.INV_MONTH AND r.CUSTOMER_CONNECTION_TYPE_ID=m.ID AND m.GROUP_ID=r.GROUP_ID
ORDER BY m.INV_MONTH,
  m.COL_ORDER;
INSERT INTO #RESULT2
SELECT
  INVOICE_MONTH=NULL,
  N'សរុបថាមពល',
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0;
INSERT INTO #RESULT2
SELECT
  INVOICE_MONTH=NULL,
  m.NAME,
  d.USAGE_0_10,
  d.USAGE_11_50,
  d.USAGE_51_200,
  d.USAGE_201_2000,
  d.USAGE_OVER_2001,
  d.TOTAL_USAGE,
  CUSTOMER_0_10=0,
  CUSTOMER_11_50=0,
  CUSTOMER_51_200=0,
  CUSTOMER_201_2000=0,
  CUSTOMER_OVER_2001=0,
  TOTAL_CUSTOMER=0,
  GROUP_TYPE_ID=2,
  ID,
  GROUP_ID
FROM #M m
OUTER APPLY
(SELECT
   USAGE_0_10=SUM(USAGE_0_10),
   USAGE_11_50=SUM(USAGE_11_50),
   USAGE_51_200=SUM(USAGE_51_200),
   USAGE_201_2000=SUM(USAGE_201_2000),
   USAGE_OVER_2001=SUM(USAGE_OVER_2001),
   TOTAL_USAGE=SUM(TOTAL_USAGE)
 FROM #RESULT2 r
 WHERE r.ID=m.ID AND m.GROUP_ID=r.GROUP_ID) d
WHERE m.INV_MONTH=@MONTH;
SELECT
  *
FROM #RESULT2
ORDER BY CASE WHEN INVOICE_MONTH IS NULL THEN '2100-01-01' ELSE INVOICE_MONTH END,
  15;
--- END OF REPORT_POWER_SOLD_2018
GO
IF OBJECT_ID('REPORT_QUARTER_SOLD_CONSUMER') IS NOT NULL
  DROP PROC REPORT_QUARTER_SOLD_CONSUMER;
GO
CREATE PROC [dbo].[REPORT_QUARTER_SOLD_CONSUMER]
  @DATE DATETIME='2020-01-01'
AS
DECLARE
  @D1 DATETIME,
  @D2 DATETIME;
SET @DATE=DATEADD(D, 0, DATEDIFF(D, 0, @DATE));
SET @D1=CONVERT(NVARCHAR(4), @DATE, 126)+'-01-01';
SET @D2=DATEADD(S, -1, DATEADD(M, 3, @DATE));


-- BUILD 12 MONTHs TABLE
CREATE TABLE #MONTHS (M DATETIME);
DECLARE @M INT;
SET @M=1;
WHILE @M<=12 BEGIN
  INSERT INTO #MONTHS
  VALUES
  (
  CONVERT(NVARCHAR(4), @D1, 126)+'-'+CONVERT(NVARCHAR, @M)+'-1'
  );
  SET @M=@M+1;
END;
SELECT TYPE_ID=1, TYPE_NAME=N'លំនៅដ្ឋាន'
INTO
  #TMP_TYPE UNION ALL
SELECT TYPE_ID=2, TYPE_NAME=N'ឧស្សាហកម្ម MV'
UNION ALL
SELECT
  TYPE_ID=3,
  TYPE_NAME=N'ឧស្សាហកម្ម MV តាមពេលវេលា'
UNION ALL
SELECT
  TYPE_ID=4,
  TYPE_NAME=N'ឧស្សាហកម្ម MV ពន្លឺព្រះអាទិត្យ'
UNION ALL
SELECT
  TYPE_ID=5,
  TYPE_NAME=N'ឧស្សាហកម្ម LV ត្រង់ស្វូអ្នកលក់'
UNION ALL
SELECT
  TYPE_ID=6,
  TYPE_NAME=N'ឧស្សាហកម្ម LV ត្រង់ស្វូអ្នកលក់តាមពេលវេលា'
UNION ALL
SELECT
  TYPE_ID=7,
  TYPE_NAME=N'ឧស្សាហកម្ម LV ត្រង់ស្វូអ្នកលក់ពន្លឺព្រះអាទិត្យ'
UNION ALL
SELECT
  TYPE_ID=8,
  TYPE_NAME=N'ឧស្សាហកម្ម LV ត្រង់ស្វូអ្នកទិញ'
UNION ALL
SELECT
  TYPE_ID=9,
  TYPE_NAME=N'ឧស្សាហកម្ម LV ត្រង់ស្វូអ្នកទិញតាមពេល'
UNION ALL
SELECT
  TYPE_ID=10,
  TYPE_NAME=N'ឧស្សាហកម្ម LV ត្រង់ស្វូអ្នកទិញពន្លឺព្រះអាទិត្យ'
UNION ALL
SELECT TYPE_ID=11, TYPE_NAME=N'ឧស្សាហកម្ម LV សាធារណៈ'
UNION ALL
SELECT TYPE_ID=12, TYPE_NAME=N'ពាណិជ្ជកម្ម MV'
UNION ALL
SELECT
  TYPE_ID=13,
  TYPE_NAME=N'ពាណិជ្ជកម្ម MV តាមពេលវេលា'
UNION ALL
SELECT
  TYPE_ID=14,
  TYPE_NAME=N'ពាណិជ្ជកម្ម MV ពន្លឺព្រះអាទិត្យ'
UNION ALL
SELECT
  TYPE_ID=15,
  TYPE_NAME=N'ពាណិជ្ជកម្ម LV​ ត្រង់ស្វូអ្នកលក់'
UNION ALL
SELECT
  TYPE_ID=16,
  TYPE_NAME=N'ពាណិជ្ជកម្ម LV ត្រង់ស្វូអ្នកលក់​តាមពេល'
UNION ALL
SELECT
  TYPE_ID=17,
  TYPE_NAME=N'ពាណិជ្ជកម្ម LV ត្រង់ស្វូអ្នកលក់ពន្លឺព្រះអាទិត្យ'
UNION ALL
SELECT
  TYPE_ID=18,
  TYPE_NAME=N'ពាណិជ្ជកម្ម LV ត្រង់ស្វូអ្នកទិញ'
UNION ALL
SELECT
  TYPE_ID=19,
  TYPE_NAME=N'ពាណិជ្ជកម្ម LV​ ត្រង់ស្វូអ្នកទិញតាមពេលវេលា'
UNION ALL
SELECT
  TYPE_ID=20,
  TYPE_NAME=N'ពាណិជ្ជកម្ម LV ត្រង់ស្វូអ្នកទិញពន្លឺព្រះអាទិត្យ'
UNION ALL
SELECT TYPE_ID=21, TYPE_NAME=N'ពាណិជ្ជកម្ម LV សាធារណៈ'
UNION ALL
SELECT TYPE_ID=22, TYPE_NAME=N'អង្គភាពរដ្ឋ LV សាធារណៈ'
UNION ALL
SELECT TYPE_ID=23, TYPE_NAME=N'សាលារៀន, មន្ទីរពេទ្យ';

-- BUILD DATA VALUE TABLE
SELECT
  i.INVOICE_MONTH,
  i.CURRENCY_ID,
  cc.CURRENCY_SING,
  CUSTOMER_CONNECTION_TYPE_ID=CASE WHEN i.CUSTOMER_CONNECTION_TYPE_ID=1 THEN 1
                              WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (4) THEN 2
                              WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (7) THEN 3
                              WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (21) THEN 4
                              WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (2) THEN 5
                              WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (18) THEN 6
                              WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (23) THEN 7
                              WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (3) THEN 8
                              WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (17) THEN 9
                              WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (22) THEN 10
                              WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (14) THEN 11
                              WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (8) THEN 12
                              WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (10) THEN 13
                              WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (24) THEN 14
                              WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (5) THEN 15
                              WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (20) THEN 16
                              WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (26) THEN 17
                              WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (6) THEN 18
                              WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (19) THEN 19
                              WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (25) THEN 20
                              WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (15) THEN 21
                              WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (16) THEN 22
                              WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (9) THEN 23 ELSE i.CUSTOMER_CONNECTION_TYPE_ID END,
  TOTAL_USAGE=i.TOTAL_USAGE+ISNULL(adj.ADJUST_USAGE, 0),
  d.PRICE,
  i.TOTAL_AMOUNT
INTO
  #TMP
FROM TBL_INVOICE i
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=i.CUSTOMER_ID
INNER JOIN TLKP_CURRENCY cc ON cc.CURRENCY_ID=i.CURRENCY_ID
OUTER APPLY
(SELECT TOP 1
   PRICE
 FROM TBL_INVOICE_DETAIL
 WHERE INVOICE_ID=i.INVOICE_ID
 ORDER BY INVOICE_DETAIL_ID DESC) d
OUTER APPLY
(SELECT
   ADJUST_USAGE=SUM(ADJUST_USAGE)
 FROM TBL_INVOICE_ADJUSTMENT
 WHERE INVOICE_ID=i.INVOICE_ID) adj
WHERE i.INVOICE_MONTH BETWEEN @D1 AND @D2
      --AND c.CUSTOMER_CONNECTION_TYPE_ID <> -1 -- USE IN PRODUCTION;
      AND i.IS_SERVICE_BILL=0 AND c.CUSTOMER_ID NOT IN
                                  (SELECT
                                     CUSTOMER_ID
                                   FROM TBL_LICENSEE_CONNECTION
                                   WHERE IS_ACTIVE=1) AND c.IS_REACTIVE=0 AND i.INVOICE_STATUS NOT IN (3)
UNION ALL
SELECT
  CONVERT(NVARCHAR(7), CREATE_ON, 126)+'-01',
  b.CURRENCY_ID,
  cc.CURRENCY_SING,
  CUSTOMER_CONNECTION_TYPE_ID=CASE WHEN CUSTOMER_CONNECTION_TYPE_ID=1 THEN 1
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (4) THEN 2
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (7) THEN 3
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (21) THEN 4
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (2) THEN 5
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (18) THEN 6
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (23) THEN 7
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (3) THEN 8
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (17) THEN 9
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (22) THEN 10
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (14) THEN 11
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (8) THEN 12
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (10) THEN 13
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (24) THEN 14
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (5) THEN 15
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (20) THEN 16
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (26) THEN 17
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (6) THEN 18
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (19) THEN 19
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (25) THEN 20
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (15) THEN 21
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (16) THEN 22
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (9) THEN 23 ELSE CUSTOMER_CONNECTION_TYPE_ID END,
  b.BUY_QTY,
  b.PRICE,
  b.BUY_AMOUNT
FROM TBL_CUSTOMER_BUY b
INNER JOIN TBL_PREPAID_CUSTOMER p ON p.PREPAID_CUS_ID=b.PREPAID_CUS_ID
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=p.CUSTOMER_ID
INNER JOIN TLKP_CURRENCY cc ON cc.CURRENCY_ID=b.CURRENCY_ID
WHERE b.CREATE_ON BETWEEN @D1 AND @D2 AND c.CUSTOMER_ID NOT IN
                                          (SELECT
                                             CUSTOMER_ID
                                           FROM TBL_LICENSEE_CONNECTION
                                           WHERE IS_ACTIVE=1) AND c.IS_REACTIVE=0 AND CONVERT(NVARCHAR(7), CREATE_ON, 126)+'-01'<='2016-02-01'
UNION ALL
SELECT
  cr.CREDIT_MONTH,
  cr.CURRENCY_ID,
  cc.CURRENCY_SING,
  CUSTOMER_CONNECTION_TYPE_ID=CASE WHEN CUSTOMER_CONNECTION_TYPE_ID=1 THEN 1
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (4) THEN 2
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (7) THEN 3
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (21) THEN 4
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (2) THEN 5
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (18) THEN 6
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (23) THEN 7
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (3) THEN 8
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (17) THEN 9
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (22) THEN 10
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (14) THEN 11
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (8) THEN 12
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (10) THEN 13
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (24) THEN 14
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (5) THEN 15
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (20) THEN 16
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (26) THEN 17
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (6) THEN 18
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (19) THEN 19
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (25) THEN 20
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (15) THEN 21
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (16) THEN 22
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (9) THEN 23 ELSE CUSTOMER_CONNECTION_TYPE_ID END,
  cr.USAGE,
  PRICE=800,
  TOTAL_AMOUNT=cr.USAGE * 800
FROM TBL_PREPAID_CREDIT cr
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=cr.CUSTOMER_ID
INNER JOIN TLKP_CURRENCY cc ON cc.CURRENCY_ID=cr.CURRENCY_ID
WHERE cr.CREDIT_MONTH BETWEEN @D1 AND @D2 AND (cr.CREDIT_MONTH BETWEEN '2016-03-01' AND '2017-02-01')
UNION ALL
SELECT
  cr.CREDIT_MONTH,
  cr.CURRENCY_ID,
  cc.CURRENCY_SING,
  CUSTOMER_CONNECTION_TYPE_ID=CASE WHEN CUSTOMER_CONNECTION_TYPE_ID=1 THEN 1
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (4) THEN 2
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (7) THEN 3
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (21) THEN 4
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (2) THEN 5
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (18) THEN 6
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (23) THEN 7
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (3) THEN 8
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (17) THEN 9
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (22) THEN 10
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (14) THEN 11
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (8) THEN 12
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (10) THEN 13
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (24) THEN 14
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (5) THEN 15
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (20) THEN 16
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (26) THEN 17
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (6) THEN 18
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (19) THEN 19
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (25) THEN 20
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (15) THEN 21
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (16) THEN 22
                              WHEN CUSTOMER_CONNECTION_TYPE_ID IN (9) THEN 23 ELSE CUSTOMER_CONNECTION_TYPE_ID END,
  cr.USAGE,
  PRICE=cr.BASED_PRICE,
  TOTAL_AMOUNT=cr.USAGE * BASED_PRICE
FROM TBL_PREPAID_CREDIT cr
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=cr.CUSTOMER_ID
INNER JOIN TLKP_CURRENCY cc ON cc.CURRENCY_ID=cr.CURRENCY_ID
WHERE cr.CREDIT_MONTH BETWEEN @D1 AND @D2;

-- BUILD CROSS TAB DEFINITION
SELECT
  CUSTOMER_CONNECTION_TYPE_ID,
  COL=ROW_NUMBER() OVER (ORDER BY CUSTOMER_CONNECTION_TYPE_ID) % 2,
  ROW= (1+ROW_NUMBER() OVER (ORDER BY CUSTOMER_CONNECTION_TYPE_ID)) / 2
INTO
  #DEF
FROM (SELECT DISTINCT CUSTOMER_CONNECTION_TYPE_ID FROM #TMP) x;

-- TO FILL 12MONTHs DATA 
INSERT INTO #TMP
SELECT
  m.M,
  t.CURRENCY_ID,
  t.CURRENCY_SING,
  t.CUSTOMER_CONNECTION_TYPE_ID,
  TOTAL_USAGE=0,
  PRICE=0,
  TOTAL_AMOUNT=0
FROM #MONTHS m,
(SELECT DISTINCT
   CUSTOMER_CONNECTION_TYPE_ID,
   CURRENCY_ID,
   CURRENCY_SING
 FROM #TMP) t;

-- IN CASE NO RECORD
IF (SELECT COUNT(*)FROM #TMP) =0 BEGIN
  INSERT INTO #TMP
  SELECT M, 0, 0, 0, 0, 0, 0 FROM #MONTHS;
  INSERT INTO #DEF SELECT 0, 1, 1;
END;
-- RENDER RESULT
SELECT
  ROW,
  INVOICE_MONTH,
  TYPE_ID1=MAX(CASE WHEN COL=1 THEN t.CUSTOMER_CONNECTION_TYPE_ID ELSE 0 END),
  TYPE_NAME1=MAX(CASE WHEN COL=1 THEN TYPE_NAME ELSE '' END),
  USAGE1=NULLIF(SUM(CASE WHEN COL=1 THEN t.TOTAL_USAGE ELSE 0 END), 0),
  PRICE1=NULLIF(MAX(CASE WHEN COL=1 THEN t.PRICE ELSE 0 END), 0),
  CURRENCY_ID1=MAX(CASE WHEN COL=1 THEN t.CURRENCY_ID ELSE 0 END),
  CURR_SIGN1=MAX(CASE WHEN COL=1 THEN t.CURRENCY_SING ELSE '' END),
  AMOUNT1=NULLIF(SUM(CASE WHEN COL=1 THEN t.TOTAL_AMOUNT ELSE 0 END), 0),
  TYPE_ID2=MAX(CASE WHEN COL=0 THEN t.CUSTOMER_CONNECTION_TYPE_ID ELSE 0 END),
  TYPE_NAME2=MAX(CASE WHEN COL=0 THEN TYPE_NAME ELSE '' END),
  USAGE2=NULLIF(SUM(CASE WHEN COL=0 THEN t.TOTAL_USAGE ELSE 0 END), 0),
  PRICE2=NULLIF(MAX(CASE WHEN COL=0 THEN t.PRICE ELSE 0 END), 0),
  CURRENCY_ID2=MAX(CASE WHEN COL=0 THEN t.CURRENCY_ID ELSE 0 END),
  CURR_SIGN2=MAX(CASE WHEN COL=0 THEN t.CURRENCY_SING ELSE '' END),
  AMOUNT2=NULLIF(SUM(CASE WHEN COL=0 THEN t.TOTAL_AMOUNT ELSE 0 END), 0)
FROM #TMP t
LEFT JOIN #DEF d ON d.CUSTOMER_CONNECTION_TYPE_ID=t.CUSTOMER_CONNECTION_TYPE_ID
LEFT JOIN #TMP_TYPE c ON c.TYPE_ID=t.CUSTOMER_CONNECTION_TYPE_ID
GROUP BY ROW,
  INVOICE_MONTH
ORDER BY ROW,
  INVOICE_MONTH;
--END REPORT_QUARTER_SOLD_CONSUMER
GO 

IF OBJECT_ID('REPORT_POWER_SOLD_LICENSEE') IS NOT NULL
	DROP PROC dbo.REPORT_POWER_SOLD_LICENSEE
GO

CREATE PROC dbo.REPORT_POWER_SOLD_LICENSEE
	@MONTH DATETIME='2019-01-01',
	@START DATETIME='2019-01-01',
	@END DATETIME='2019-01-28'
AS
-- BUILD DATA VALUE TABLE
SELECT	i.INVOICE_MONTH,
		i.INVOICE_NO,
		l.LICENSEE_ID,
		li.LICENSEE_NAME,
		li.LICENSEE_NO, 
		TOTAL_USAGE = i.TOTAL_USAGE + ISNULL(adj.ADJUST_USAGE,0),
		d.PRICE,
		i.CURRENCY_ID,
		cu.CURRENCY_NAME,
		cu.CURRENCY_SING,
		i.TOTAL_AMOUNT
FROM TBL_INVOICE i 
INNER JOIN TBL_LICENSEE_CONNECTION l ON l.CUSTOMER_ID = i.CUSTOMER_ID AND l.IS_ACTIVE=1
INNER JOIN TLKP_CURRENCY cu ON i.CURRENCY_ID=cu.CURRENCY_ID
INNER JOIN TBL_LICENSEE li ON li.LICENSEE_ID=l.LICENSEE_ID AND li.IS_ACTIVE=1
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = i.CUSTOMER_ID
OUTER APPLY(
	SELECT TOP 1 PRICE 
	FROM TBL_INVOICE_DETAIL 
	WHERE INVOICE_ID = i.INVOICE_ID
	ORDER BY INVOICE_DETAIL_ID DESC
) d
OUTER APPLY(
		SELECT ADJUST_USAGE = SUM(ADJUST_USAGE)
		FROM TBL_INVOICE_ADJUSTMENT   
		WHERE INVOICE_ID =  i.INVOICE_ID
) adj 
WHERE i.INVOICE_MONTH =@MONTH  
	AND i.IS_SERVICE_BILL=0 
	AND c.IS_REACTIVE=0
	AND i.INVOICE_STATUS NOT IN (3)
UNION ALL 
SELECT	CONVERT(NVARCHAR(7),CREATE_ON,126)+'-01',
		b.BUY_NO,
		l.LICENSEE_ID,
		li.LICENSEE_NAME,
		li.LICENSEE_NO,  
		b.BUY_QTY,
		b.PRICE,
		b.CURRENCY_ID,
		cu.CURRENCY_NAME,
		cu.CURRENCY_SING,
		b.BUY_AMOUNT
FROM TBL_CUSTOMER_BUY b
INNER JOIN TBL_PREPAID_CUSTOMER p ON p.PREPAID_CUS_ID = b.PREPAID_CUS_ID
INNER JOIN TBL_LICENSEE_CONNECTION l ON l.CUSTOMER_ID = p.CUSTOMER_ID AND l.IS_ACTIVE=1
INNER JOIN TLKP_CURRENCY cu ON b.CURRENCY_ID=cu.CURRENCY_ID
INNER JOIN TBL_LICENSEE li ON li.LICENSEE_ID=l.LICENSEE_ID AND li.IS_ACTIVE=1
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = l.CUSTOMER_ID
WHERE b.CREATE_ON BETWEEN @START AND @END 
	 AND c.IS_REACTIVE = 0
ORDER BY l.LICENSEE_ID
--- END OF REPORT_POWER_SOLD_LICENSEE
GO

IF OBJECT_ID('REPORT_QUARTER_CUSTOMER_CONSUMER') IS NOT NULL
	DROP PROC REPORT_QUARTER_CUSTOMER_CONSUMER
GO

CREATE PROC [dbo].[REPORT_QUARTER_CUSTOMER_CONSUMER]
	@DATE DATETIME='2020-01-01'
AS
DECLARE @D1 DATETIME,
		@D2 DATETIME

SET @DATE = DATEADD(D,0,DATEDIFF(D,0,@DATE));
SET @D1= @DATE; 
SET @D2= DATEADD(S,-1,DATEADD(M,3,@DATE));

SELECT *
INTO #TMP
FROM (
SELECT	cg.CUSTOMER_GROUP_ID,
		cg.CUSTOMER_GROUP_NAME,
		t.CUSTOMER_CONNECTION_TYPE_ID,
		t.CUSTOMER_CONNECTION_TYPE_NAME,
		DAILY_SUPPLY_HOUR=24,
		NUMBER = ISNULL(SUM(CASE WHEN OLD_STATUS_ID=1 AND NEW_STATUS_ID=2 THEN 1 -- អតិថិជនថ្មី
								 WHEN OLD_STATUS_ID=2 AND NEW_STATUS_ID=4 THEN -1 -- កំពុងប្រើប្រាស => ឈប់ប្រើ
								 WHEN OLD_STATUS_ID=4 AND NEW_STATUS_ID=2 THEN 1 -- ឈប់ប្រើប្រាស់ => កំពុងប្រើប្រាស់
								 WHEN OLD_STATUS_ID=3 AND NEW_STATUS_ID=4 THEN -1 -- ផ្តាច់ => ឈប់ប្រើ 
								 ELSE 0 END),0),
		--NUMBER = ISNULL(SUM(CASE WHEN NEW_STATUS_ID=2 THEN 1 WHEN OLD_STATUS_ID=2 THEN -1 ELSE 0 END),0),
		REMARK = ''
FROM TBL_CUSTOMER c
INNER JOIN TLKP_CUSTOMER_CONNECTION_TYPE t ON t.CUSTOMER_CONNECTION_TYPE_ID = c.CUSTOMER_CONNECTION_TYPE_ID
INNER JOIN TLKP_CUSTOMER_GROUP cg ON cg.CUSTOMER_GROUP_ID=t.NONLICENSE_CUSTOMER_GROUP_ID
INNER JOIN TBL_CUS_STATUS_CHANGE g ON g.CUSTOMER_ID=c.CUSTOMER_ID  
WHERE g.CHNAGE_DATE  <= @D2
	AND (c.USAGE_CUSTOMER_ID=0 OR c.USAGE_CUSTOMER_ID=c.CUSTOMER_ID)--only not 3-phase
	AND (c.IS_REACTIVE =0 ) -- not reactive customer
	AND c.CUSTOMER_ID NOT IN(SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE=1 AND (START_DATE <= @D2)
	AND (IS_INUSED=1 OR END_DATE > @D1 ))
GROUP BY cg.CUSTOMER_GROUP_ID,
		cg.CUSTOMER_GROUP_NAME,	
		t.CUSTOMER_CONNECTION_TYPE_ID,
		t.CUSTOMER_CONNECTION_TYPE_NAME
UNION ALL
SELECT cg.CUSTOMER_GROUP_ID,
		cg.CUSTOMER_GROUP_NAME,
		t.CUSTOMER_CONNECTION_TYPE_ID,
		t.CUSTOMER_CONNECTION_TYPE_NAME,
		DAILY_SUPPLY_HOUR=24,
		NUMBER = 0,
		REMARK = ''
FROM TLKP_CUSTOMER_CONNECTION_TYPE t
LEFT JOIN TLKP_CUSTOMER_GROUP cg ON cg.CUSTOMER_GROUP_ID=t.NONLICENSE_CUSTOMER_GROUP_ID
WHERE
	t.CUSTOMER_CONNECTION_TYPE_ID NOT IN(SELECT CUSTOMER_CONNECTION_TYPE_ID FROM TBL_CUSTOMER WHERE STATUS_ID NOT IN(1,4,5))
	AND t.CUSTOMER_CONNECTION_TYPE_ID NOT IN(11,12,13)
GROUP BY cg.CUSTOMER_GROUP_ID,
		cg.CUSTOMER_GROUP_NAME,	
		t.CUSTOMER_CONNECTION_TYPE_ID,
		t.CUSTOMER_CONNECTION_TYPE_NAME
) x
SELECT CUSTOMER_GROUP_ID= CASE WHEN CUSTOMER_GROUP_ID=4 THEN 1
                      WHEN CUSTOMER_GROUP_ID=2 THEN 2       
                      WHEN CUSTOMER_GROUP_ID=1 THEN 3
                      WHEN CUSTOMER_GROUP_ID=5 THEN 4
                    END,
		CUSTOMER_GROUP_NAME,
		CUSTOMER_CONNECTION_TYPE_ID = CASE WHEN CUSTOMER_CONNECTION_TYPE_ID=1 THEN 1
                      WHEN CUSTOMER_CONNECTION_TYPE_ID IN(4) THEN 2
					  WHEN CUSTOMER_CONNECTION_TYPE_ID IN(7) THEN 3
					  WHEN CUSTOMER_CONNECTION_TYPE_ID IN(21) THEN 4
					  WHEN CUSTOMER_CONNECTION_TYPE_ID IN(2) THEN 5
					  WHEN CUSTOMER_CONNECTION_TYPE_ID IN(18) THEN 6
					  WHEN CUSTOMER_CONNECTION_TYPE_ID IN(23) THEN 7
					  WHEN CUSTOMER_CONNECTION_TYPE_ID IN(3) THEN 8
					  WHEN CUSTOMER_CONNECTION_TYPE_ID IN(17) THEN 9
					  WHEN CUSTOMER_CONNECTION_TYPE_ID IN (22) THEN 10
					  WHEN CUSTOMER_CONNECTION_TYPE_ID IN(14) THEN 11       
                      WHEN CUSTOMER_CONNECTION_TYPE_ID IN(8) THEN 12
					  WHEN CUSTOMER_CONNECTION_TYPE_ID IN(10) THEN 13
					  WHEN CUSTOMER_CONNECTION_TYPE_ID IN (24) THEN 14
                      WHEN CUSTOMER_CONNECTION_TYPE_ID IN(5) THEN 15
					  WHEN CUSTOMER_CONNECTION_TYPE_ID IN(20) THEN 16
					  WHEN CUSTOMER_CONNECTION_TYPE_ID IN (26) THEN 17
					  WHEN CUSTOMER_CONNECTION_TYPE_ID IN(6) THEN 18
					  WHEN CUSTOMER_CONNECTION_TYPE_ID IN(19) THEN 19
					  WHEN CUSTOMER_CONNECTION_TYPE_ID IN(25) THEN 20
					  WHEN CUSTOMER_CONNECTION_TYPE_ID IN(15) THEN 21
					  WHEN CUSTOMER_CONNECTION_TYPE_ID IN(16) THEN 22
                      WHEN CUSTOMER_CONNECTION_TYPE_ID IN(9) THEN 23
                    END,
		CUSTOMER_CONNECTION_TYPE_NAME,
		DAILY_SUPPLY_HOUR,
		NUMBER
FROM #TMP
WHERE CUSTOMER_GROUP_NAME !=N'អតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ'
-- END REPORT_QUARTER_CUSTOMER_CONSUMER
GO

IF OBJECT_ID('REPORT_QUARTER_POWER_COVERAGE') IS NOT NULL
    DROP PROC REPORT_QUARTER_POWER_COVERAGE;
GO

CREATE PROC [dbo].[REPORT_QUARTER_POWER_COVERAGE] @DATE DATETIME = '2019-01-01'
AS
DECLARE @D1 DATETIME,
        @D2 DATETIME;
SET @DATE = DATEADD(D, 0, DATEDIFF(D, 0, @DATE));
SET @D1 = @DATE;
SET @D2 = DATEADD(S, -1, DATEADD(M, 3, @DATE));

-- CALCULATE CUSTOMER BY VILLAGES
SELECT VILLAGE_CODE,
       TOTAL_COVERAGE = ISNULL(SUM(   CASE
                                          WHEN OLD_STATUS_ID = 1
                                               AND NEW_STATUS_ID = 2 THEN
                                              1  -- អតិថិជនថ្មី
                                          WHEN OLD_STATUS_ID = 2
                                               AND NEW_STATUS_ID = 4 THEN
                                              -1 -- កំពុងប្រើប្រាស => ឈប់ប្រើ
                                          WHEN OLD_STATUS_ID = 4
                                               AND NEW_STATUS_ID = 2 THEN
                                              1  -- ឈប់ប្រើប្រាស់ => កំពុងប្រើប្រាស់
                                          WHEN OLD_STATUS_ID = 3
                                               AND NEW_STATUS_ID = 4 THEN
                                              -1 -- ផ្តាច់ => ឈប់ប្រើ 
                                          ELSE
                                              0
                                      END
                                  ),
                               0
                              )
INTO #TMP_COVERAGES
FROM TBL_CUS_STATUS_CHANGE g
    INNER JOIN TBL_CUSTOMER c
        ON c.CUSTOMER_ID = g.CUSTOMER_ID
WHERE g.CHNAGE_DATE <= @D2
      AND
      (
          c.USAGE_CUSTOMER_ID = 0
          OR c.USAGE_CUSTOMER_ID = c.CUSTOMER_ID
      ) --only not 3-phase
      AND (c.IS_REACTIVE = 0) -- not reactive customer
      AND c.CUSTOMER_TYPE_ID NOT IN ( -1 ) -- customer in production
      AND c.CUSTOMER_ID NOT IN
          (
              SELECT CUSTOMER_ID
              FROM TBL_LICENSEE_CONNECTION
              WHERE IS_ACTIVE = 1
                    AND (START_DATE <= @D2)
                    AND
                    (
                        IS_INUSED = 1
                        OR END_DATE > @D1
                    )
          )
GROUP BY VILLAGE_CODE;

-- CALCULATE MV BY VILLAGES
SELECT VILLAGE_CODE,
       MV = MAX(   CASE
                       WHEN DISTRIBUTION_ID = 1 THEN
                           1
                       ELSE
                           0
                   END
               ),
       LV = MAX(   CASE
                       WHEN DISTRIBUTION_ID IN ( 2, 3 ) THEN
                           1
                       ELSE
                           0
                   END
               )
INTO #TMP_DISTRIBUTIONS
FROM TBL_DISTRIBUTION_FACILITY f
    INNER JOIN TBL_DISTRIBUTION_VILLAGE v
        ON v.DISTRIBUTION_FACILITY_ID = f.DISTRIBUTION_FACILITY_ID
WHERE f.IS_ACTIVE = 1
      AND v.IS_ACTIVE = 1
      AND (START_DATE <= @D2)
      AND
      (
          IS_INUSED = 1
          OR END_DATE > @D1
      )
GROUP BY VILLAGE_CODE;

-- CALCULATE TOTAL_USAGE MV, LV
SELECT c.VILLAGE_CODE,
       MV_USAGE = ISNULL(SUM(   CASE
                                    WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 4, 7, 8, 10, 11, 21, 24 ) THEN
                                        i.TOTAL_USAGE + ISNULL(adj.ADJUST_USAGE, 0)
                                END
                            ),
                         0
                        ),
       LV_USAGE = ISNULL(
                            SUM(   CASE
                                       WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1, 2, 3, 5, 6, 9, 12, 13, 14, 15, 16,
                                                                               17, 18, 19, 20, 22, 23, 25, 26
                                                                             ) THEN
                                           i.TOTAL_USAGE + ISNULL(adj.ADJUST_USAGE, 0)
                                   END
                               ),
                            0
                        )
INTO #TMP
FROM TBL_INVOICE i
    INNER JOIN TBL_CUSTOMER c
        ON c.CUSTOMER_ID = i.CUSTOMER_ID
    OUTER APPLY
(
    SELECT ADJUST_USAGE = SUM(ADJUST_USAGE)
    FROM TBL_INVOICE_ADJUSTMENT
    WHERE INVOICE_ID = i.INVOICE_ID
) adj
WHERE i.INVOICE_MONTH
      BETWEEN CONVERT(NVARCHAR(4), @DATE, 126) + '-01-01' AND @D2
      AND i.IS_SERVICE_BILL = 0
      AND c.CUSTOMER_ID NOT IN
          (
              SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1
          )
      AND c.IS_REACTIVE = 0
      AND i.INVOICE_STATUS NOT IN ( 3 )
GROUP BY c.VILLAGE_CODE
UNION ALL
SELECT c.VILLAGE_CODE,
       MV_USAGE = ISNULL(SUM(   CASE
                                    WHEN c.CUSTOMER_CONNECTION_TYPE_ID IN ( 4, 7, 8, 10, 11, 21, 24 ) THEN
                                        cr.USAGE
                                END
                            ),
                         0
                        ),
       LV_USAGE = ISNULL(
                            SUM(   CASE
                                       WHEN c.CUSTOMER_CONNECTION_TYPE_ID IN ( 1, 2, 3, 5, 6, 9, 12, 13, 14, 15, 16,
                                                                               17, 18, 19, 20, 22, 23, 25, 26
                                                                             ) THEN
                                           cr.USAGE
                                   END
                               ),
                            0
                        )
FROM TBL_PREPAID_CREDIT cr
    INNER JOIN TBL_CUSTOMER c
        ON c.CUSTOMER_ID = cr.CUSTOMER_ID
WHERE cr.CREDIT_MONTH
BETWEEN CONVERT(NVARCHAR(4), @DATE, 126) + '-01-01' AND @D2
GROUP BY c.VILLAGE_CODE;

SELECT VILLAGE_CODE,
       MV_TOTAL_USAGE = SUM(MV_USAGE),
       LV_TOTAL_USAGE = SUM(LV_USAGE)
INTO #TMP_INVOICE
FROM #TMP
GROUP BY VILLAGE_CODE;
-- RENDER RESULT
SELECT d.DISTRICT_NAME,
       c.COMMUNE_NAME,
       v.VILLAGE_NAME,
       MV_TOTAL_USAGE = ISNULL(i.MV_TOTAL_USAGE, 0),
       LV_TOTAL_USAGE = ISNULL(i.LV_TOTAL_USAGE, 0),
       TOTAL_FAMILY = ISNULL(TOTAL_FAMILIES, 0),
       TOTAL_COVERAGE = ISNULL(TOTAL_COVERAGE, 0),
       PERCENTAGE_COVERAGE = ISNULL(ISNULL(TOTAL_COVERAGE, 0) * 100.00 / NULLIF(TOTAL_FAMILIES, 0), 0),
       MV_COVERAGE = CONVERT(BIT, ISNULL(MV, 0)),
       LV_COVERAGE = CONVERT(BIT, ISNULL(LV, 0)),
       IS_OUTSIZE_ZONE = 0
FROM TBL_LICENSE_VILLAGE lv
    INNER JOIN TLKP_VILLAGE v
        ON lv.VILLAGE_CODE = v.VILLAGE_CODE
    INNER JOIN TLKP_COMMUNE c
        ON c.COMMUNE_CODE = v.COMMUNE_CODE
    INNER JOIN TLKP_DISTRICT d
        ON d.DISTRICT_CODE = c.DISTRICT_CODE
    LEFT JOIN #TMP_COVERAGES t
        ON t.VILLAGE_CODE = v.VILLAGE_CODE
    LEFT JOIN #TMP_DISTRIBUTIONS b
        ON b.VILLAGE_CODE = v.VILLAGE_CODE
    LEFT JOIN #TMP_INVOICE i
        ON i.VILLAGE_CODE = v.VILLAGE_CODE
WHERE lv.IS_ACTIVE = 1
--ORDER BY COMMUNE_NAME, VILLAGE_NAME
UNION ALL
SELECT d.DISTRICT_NAME,
       c.COMMUNE_NAME,
       v.VILLAGE_NAME,
       MV_TOTAL_USAGE = ISNULL(i.MV_TOTAL_USAGE, 0),
       LV_TOTAL_USAGE = ISNULL(i.LV_TOTAL_USAGE, 0),
       TOTAL_FAMILY = ISNULL(TOTAL_FAMILIES, 0),
       TOTAL_COVERAGE = ISNULL(TOTAL_COVERAGE, 0),
       PERCENTAGE_COVERAGE = ISNULL(ISNULL(TOTAL_COVERAGE, 0) * 100.00 / NULLIF(TOTAL_FAMILIES, 0), 0),
       MV_COVERAGE = CONVERT(BIT, ISNULL(MV, 0)),
       LV_COVERAGE = CONVERT(BIT, ISNULL(LV, 0)),
       IS_OUTSIZE_ZONE = 1
FROM TLKP_VILLAGE v
    LEFT JOIN TBL_LICENSE_VILLAGE lv
        ON lv.VILLAGE_CODE = v.VILLAGE_CODE
    INNER JOIN TLKP_COMMUNE c
        ON c.COMMUNE_CODE = v.COMMUNE_CODE
    INNER JOIN TLKP_DISTRICT d
        ON d.DISTRICT_CODE = c.DISTRICT_CODE
    LEFT JOIN #TMP_COVERAGES t
        ON t.VILLAGE_CODE = v.VILLAGE_CODE
    LEFT JOIN #TMP_DISTRIBUTIONS b
        ON b.VILLAGE_CODE = v.VILLAGE_CODE
    LEFT JOIN #TMP_INVOICE i
        ON i.VILLAGE_CODE = v.VILLAGE_CODE
WHERE TOTAL_COVERAGE > 0
      AND v.VILLAGE_CODE NOT IN
          (
              SELECT VILLAGE_CODE FROM TBL_LICENSE_VILLAGE WHERE IS_ACTIVE = 1
          );
--END OF REPORT_QUARTER_POWER_COVERAGE
GO
            ";
        }
    }
}
