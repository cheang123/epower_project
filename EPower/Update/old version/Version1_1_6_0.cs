﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EPower.Update
{
    class Version1_1_6_0 : Version
    {
        /// <summary>
        /// Sieng Sotheara
        /// Fix: Data customer that remain MERGE ID after deleted
        /// 
        /// Report
        /// - Fix ReportSummaryMeterByTransformer.rpt
        /// 
        /// Morm Raksmey
        /// - Disable RUN_BILL_CUSTOMER_MERGE
        /// </summary>
        /// <returns></returns>
        protected override string GetBatchCommand()
        {
            return @"
UPDATE TBL_CUSTOMER SET INVOICE_CUSTOMER_ID=0,USAGE_CUSTOMER_ID=0,IS_REACTIVE=0,REACTIVE_RULE_ID=0
WHERE CUSTOMER_ID IN(
	SELECT CUSTOMER_ID 
	FROM TBL_CUSTOMER 
	WHERE STATUS_ID IN(4,5) 
		AND (INVOICE_CUSTOMER_ID <>0 OR USAGE_CUSTOMER_ID<>0)
)
IF OBJECT_ID('REPORT_SUMMARY_METER_BY_TRANSFORMER') IS NOT NULL
	DROP PROC REPORT_SUMMARY_METER_BY_TRANSFORMER
GO
CREATE PROC REPORT_SUMMARY_METER_BY_TRANSFORMER
	@BILLING_CYCLE_ID INT=0
AS
/*
@2016-10-19 Sieng Sotheara
	Add Filter CYCLE_ID
*/
SELECT AMPARE_NAME=CONVERT(INT, REPLACE(upper( a.AMPARE_NAME),'A','')),
	NO_OF_METER= COUNT(a.AMPARE_NAME),
	TRANSFORMER_CODE=tf.TRANSFORMER_CODE + ' ' +CONVERT(NVARCHAR, CONVERT(INT, cp.CAPACITY))+'kVa'
FROM TBL_CUSTOMER c
INNER JOIN TBL_AMPARE a ON c.AMP_ID=a.AMPARE_ID
INNER JOIN TBL_CUSTOMER_METER cm ON c.CUSTOMER_ID = cm.CUSTOMER_ID AND cm.IS_ACTIVE=1 AND c.STATUS_ID IN(2,3)
INNER JOIN TBL_BOX b ON cm.BOX_ID=b.BOX_ID
INNER JOIN TBL_POLE p ON b.POLE_ID = p.POLE_ID
INNER JOIN TBL_TRANSFORMER tf ON p.TRANSFORMER_ID= tf.TRANSFORMER_ID
INNER JOIN TBL_CAPACITY cp ON tf.CAPACITY_ID = cp.CAPACITY_ID
WHERE (@BILLING_CYCLE_ID=0 OR c.BILLING_CYCLE_ID=@BILLING_CYCLE_ID)
GROUP BY a.AMPARE_NAME,tf.TRANSFORMER_CODE,cp.CAPACITY
ORDER BY tf.TRANSFORMER_CODE;

GO
";
        }
    }
}
