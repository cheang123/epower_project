﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using EPower.Properties;

namespace EPower.Update
{
    /// <summary>
    /// Morm Raksmey
    /// - Script Backup With ZIP
    /// - Script Restore form ZIP
    /// </summary>
    class Version1_1_5_8 : Version
    {
        protected override string GetBatchCommand()
        {
            return  Resources.Version1_1_5_8;
        }
    }
}
