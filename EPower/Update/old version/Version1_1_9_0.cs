﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EPower.Properties;

namespace EPower.Update
{
    
    class Version1_1_9_0 : Version
    {
        /// <summary> 
        /// Morm Raksmey
        /// 1. fix bug on OPS Report : error store bcos we just add new column ROW_DATE
        /// 2. fix bug on create new pole when create new customer bcos we just add ROW_DATE on TBL_POLE
        /// 3. Fix bug on alert error javascript : Change Web Browser to Picture Box on Form Main
        /// </summary>
        /// <returns></returns>
        protected override string GetBatchCommand()
        {
            return @"
IF OBJECT_ID('REPORT_OPS_R01') IS NOT NULL
	DROP PROC REPORT_OPS_R01
GO

CREATE PROC [dbo].[REPORT_OPS_R01]
	@YEAR_ID INT=2016,
	@QUARTER INT=4
AS
-- START REPORT_OPS_R01
/*
	
	@2017-12-29	By Roem Chamroeun
	1. Add defualt value for column ROW_DATE= '1900-01-01'
	
*/
SELECT r.* 
	,d.DISTRIBUTION_NAME
INTO #TMP
FROM TBL_OPS_R01 r 
INNER JOIN TLKP_DISTRIBUTION d ON d.DISTRIBUTION_ID=r.DISTRIBUTION_ID
WHERE r.IS_ACTIVE=1 
AND YEAR(r.OUTAGES_DATE)=@YEAR_ID 
AND MONTH(r.OUTAGES_DATE) BETWEEN (@QUARTER*3)-2 AND @QUARTER*3

DECLARE @COUNT INT;

IF (SELECT COUNT(*) FROM #TMP WHERE DISTRIBUTION_ID=1)<4
BEGIN
	SELECT @COUNT=COUNT(*) FROM #TMP WHERE DISTRIBUTION_ID=1
	WHILE @COUNT<4
	BEGIN
		INSERT INTO #TMP
		SELECT 0,1,'','1900-01-01',0,'1900-01-01','1900-01-01','',0,'',1,'1900-01-01',N'ខ្សែបណ្តាញតង់ស្យុងមធ្យម'
		SET @COUNT=@COUNT+1
	END 
END
IF (SELECT COUNT(*)FROM #TMP WHERE DISTRIBUTION_ID=2)<4
BEGIN
	SELECT @COUNT=COUNT(*) FROM #TMP WHERE DISTRIBUTION_ID=2
	WHILE @COUNT<4
	BEGIN
		INSERT INTO #TMP
		SELECT 0,2,'','1900-01-01',0,'1900-01-01','1900-01-01','',0,'',1,'1900-01-01',N'ខ្សែបណ្តាញតង់ស្យុងទាប'
		SET @COUNT=@COUNT+1
	END
END

SELECT * ,GET_AFFECTED_AREAS=dbo.GET_AFFECTED_AREAS(p.AFFECTED_AREAS)
FROM #TMP p
ORDER BY DISTRIBUTION_ID
	,CASE WHEN OUTAGES_DATE='1900-01-01' THEN '9999-12-31' ELSE OUTAGES_DATE END

GO
--------------------------------------------------------------
IF OBJECT_ID('REPORT_OPS_R02') IS NOT NULL
	DROP PROC REPORT_OPS_R02
GO

CREATE PROC [dbo].[REPORT_OPS_R02]
	@YEAR_ID INT=2016,
	@QUARTER INT=3
AS

-- START REPORT_OPS_R02
/*
	
	@2017-12-29	By Roem Chamroeun
	1. Add defualt value for column ROW_DATE= '1900-01-01'
	
*/

SELECT r.* 
	,d.DISTRIBUTION_NAME
INTO #TMP
FROM TBL_OPS_R02 r 
INNER JOIN TLKP_DISTRIBUTION d ON d.DISTRIBUTION_ID=r.DISTRIBUTION_ID
WHERE r.IS_ACTIVE=1 
AND YEAR(r.BLACKOUT_DATE)=@YEAR_ID 
AND MONTH(r.BLACKOUT_DATE) BETWEEN (@QUARTER*3)-2 AND @QUARTER*3

DECLARE @COUNT INT;

IF (SELECT COUNT(*) FROM #TMP WHERE DISTRIBUTION_ID=1)<4
BEGIN
	SELECT @COUNT=COUNT(*) FROM #TMP WHERE DISTRIBUTION_ID=1
	WHILE @COUNT<4
	BEGIN
		INSERT INTO #TMP
		SELECT 0,1,'','1900-01-01',0,'1900-01-01','1900-01-01','',0,1,'1900-01-01',N'ខ្សែបណ្តាញតង់ស្យុងមធ្យម'
		SET @COUNT=@COUNT+1
	END 
END
IF (SELECT COUNT(*)FROM #TMP WHERE DISTRIBUTION_ID=2)<4
BEGIN
	SELECT @COUNT=COUNT(*) FROM #TMP WHERE DISTRIBUTION_ID=2
	WHILE @COUNT<4
	BEGIN
		INSERT INTO #TMP
		SELECT 0,2,'','1900-01-01',0,'1900-01-01','1900-01-01','',0,1,'1900-01-01',N'ខ្សែបណ្តាញតង់ស្យុងទាប'
		SET @COUNT=@COUNT+1
	END
END

SELECT * ,GET_AFFECTED_AREAS=dbo.GET_AFFECTED_AREAS(p.AFFECTED_AREAS)
FROM #TMP p
ORDER BY DISTRIBUTION_ID
	,CASE WHEN BLACKOUT_DATE='1900-01-01' THEN '9999-12-31' ELSE BLACKOUT_DATE END

GO

IF OBJECT_ID('REPORT_OPS_R03') IS NOT NULL
	DROP PROC REPORT_OPS_R03
GO

CREATE PROC [dbo].[REPORT_OPS_R03]
	@YEAR_ID INT=2016,
	@QUARTER INT=3
AS
--START REPORT_OPS_R03

/*
	
	@2017-12-29	By Roem Chamroeun
	1. Add defualt value for column ROW_DATE= '1900-01-01'
	
*/

SELECT r.* 
	,c.CUSTOMER_CODE
	,c.LAST_NAME_KH
	,c.FIRST_NAME_KH
	,d.DISTRIBUTION_NAME
INTO #TMP
FROM TBL_OPS_R03 r 
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=r.CUSTOMER_ID
INNER JOIN TLKP_DISTRIBUTION d ON d.DISTRIBUTION_ID=r.DISTRIBUTION_ID
WHERE r.IS_ACTIVE=1 
AND YEAR(r.COMPLAINT_DATE)=@YEAR_ID 
AND MONTH(r.COMPLAINT_DATE) BETWEEN (@QUARTER*3)-2 AND @QUARTER*3

DECLARE @COUNT INT;

IF (SELECT COUNT(*) FROM #TMP WHERE DISTRIBUTION_ID=1)<4
BEGIN
	SELECT @COUNT=COUNT(*) FROM #TMP WHERE DISTRIBUTION_ID=1
	WHILE @COUNT<4
	BEGIN
		INSERT INTO #TMP
		SELECT 0,1,0,'1900-01-01','','1900-01-01','','',1,'1900-01-01','','','',N'ខ្សែបណ្តាញតង់ស្យុងមធ្យម'
		SET @COUNT=@COUNT+1
	END 
END
IF (SELECT COUNT(*)FROM #TMP WHERE DISTRIBUTION_ID=2)<4
BEGIN
	SELECT @COUNT=COUNT(*) FROM #TMP WHERE DISTRIBUTION_ID=2
	WHILE @COUNT<4
	BEGIN
		INSERT INTO #TMP
		SELECT 0,2,0,'1900-01-01','','1900-01-01','','',1,'1900-01-01','','','',N'ខ្សែបណ្តាញតង់ស្យុងទាប'
		SET @COUNT=@COUNT+1
	END
END

SELECT * 
FROM #TMP
ORDER BY DISTRIBUTION_ID,
	CASE WHEN COMPLAINT_DATE='1900-01-01' THEN '9999-12-31' ELSE COMPLAINT_DATE END

GO

IF OBJECT_ID('REPORT_OPS_R04') IS NOT NULL
	DROP PROC REPORT_OPS_R04
GO

CREATE PROC [dbo].[REPORT_OPS_R04]
	@YEAR_ID INT=2016,
	@QUARTER INT=3
AS
--START REPORT_OPS_R04
/*
	
	@2017-12-29	By Roem Chamroeun
	1. Add defualt value for column ROW_DATE= '1900-01-01'
	
*/

SELECT r.* 
	,c.CUSTOMER_CODE
	,c.LAST_NAME_KH
	,c.FIRST_NAME_KH
INTO #TMP
FROM TBL_OPS_R04 r 
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=r.CUSTOMER_ID
WHERE r.IS_ACTIVE=1 
AND YEAR(r.COMPLAINT_DATE)=@YEAR_ID 
AND MONTH(r.COMPLAINT_DATE) BETWEEN (@QUARTER*3)-2 AND @QUARTER*3

DECLARE @COUNT INT;

IF (SELECT COUNT(*) FROM #TMP)<11
BEGIN
	SELECT @COUNT=COUNT(*) FROM #TMP
	WHILE @COUNT<11
	BEGIN
		INSERT INTO #TMP
		SELECT 0,0,'1900-01-01','','1900-01-01','',1,'1900-01-01','','',''
		SET @COUNT=@COUNT+1
	END 
END

SELECT * 
FROM #TMP
ORDER BY CASE WHEN COMPLAINT_DATE='1900-01-01' THEN '9999-12-31' ELSE COMPLAINT_DATE END

GO

IF OBJECT_ID('REPORT_OPS_R05') IS NOT NULL
	DROP PROC REPORT_OPS_R05
GO

CREATE PROC [dbo].[REPORT_OPS_R05]
	@YEAR_ID INT=2016,
	@QUARTER INT=4
AS
--START REPORT_OPS_R05
/*
	
	@2017-12-29	By Roem Chamroeun
	1. Add defualt value for column ROW_DATE= '1900-01-01'
	
*/
SELECT r.* 
	,c.CUSTOMER_CODE
	,c.LAST_NAME_KH
	,c.FIRST_NAME_KH
INTO #TMP
FROM TBL_OPS_R05 r 
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=r.CUSTOMER_ID
WHERE r.IS_ACTIVE=1 
AND YEAR(r.DEFFECTIVE_DATE)=@YEAR_ID 
AND MONTH(r.DEFFECTIVE_DATE) BETWEEN (@QUARTER*3)-2 AND @QUARTER*3

DECLARE @COUNT INT;

IF (SELECT COUNT(*) FROM #TMP)<11
BEGIN
	SELECT @COUNT=COUNT(*) FROM #TMP
	WHILE @COUNT<11
	BEGIN
		INSERT INTO #TMP
		SELECT 0,0,'1900-01-01',0,'1900-01-01',1,'1900-01-01','','',''
		SET @COUNT=@COUNT+1
	END 
END

SELECT * 
FROM #TMP
ORDER BY CASE WHEN DEFFECTIVE_DATE='1900-01-01' THEN '9999-12-31' ELSE DEFFECTIVE_DATE END

GO

IF OBJECT_ID('REPORT_OPS_R06') IS NOT NULL
	DROP PROC REPORT_OPS_R06
GO

CREATE PROC [dbo].[REPORT_OPS_R06]
	@YEAR_ID INT=2016,
	@QUARTER INT=4
AS
--START REPORT_OPS_R06
/*
	
	@2017-12-29	By Roem Chamroeun
	1. Add defualt value for column ROW_DATE= '1900-01-01'
	
*/
SELECT r.* 
	,c.CUSTOMER_CODE
	,c.LAST_NAME_KH
	,c.FIRST_NAME_KH
INTO #TMP
FROM TBL_OPS_R06 r 
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=r.CUSTOMER_ID
WHERE r.IS_ACTIVE=1 
AND YEAR(r.REQUEST_DATE)=@YEAR_ID 
AND MONTH(r.REQUEST_DATE) BETWEEN (@QUARTER*3)-2 AND @QUARTER*3

DECLARE @COUNT INT;

IF (SELECT COUNT(*) FROM #TMP)<11
BEGIN
	SELECT @COUNT=COUNT(*) FROM #TMP
	WHILE @COUNT<11
	BEGIN
		INSERT INTO #TMP
		SELECT 0,0,'1900-01-01','','1900-01-01','1900-01-01','',1,'1900-01-01','','',''
		SET @COUNT=@COUNT+1
	END 
END

SELECT * 
FROM #TMP
ORDER BY CASE WHEN REQUEST_DATE='1900-01-01' THEN '9999-12-31' ELSE REQUEST_DATE END

GO

IF OBJECT_ID('REPORT_OPS_R07') IS NOT NULL
	DROP PROC REPORT_OPS_R07
GO

CREATE PROC [dbo].[REPORT_OPS_R07]
	@YEAR_ID INT=2016,
	@QUARTER INT=3

/*
	
	@2017-12-29	By Roem Chamroeun
	1. Add defualt value for column ROW_DATE= '1900-01-01'
	
*/
AS
SELECT r.* 
	,c.CUSTOMER_CODE
	,c.LAST_NAME_KH
	,c.FIRST_NAME_KH
INTO #TMP
FROM TBL_OPS_R07 r 
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=r.CUSTOMER_ID
WHERE r.IS_ACTIVE=1 
AND YEAR(r.COMPLAINT_DATE)=@YEAR_ID 
AND MONTH(r.COMPLAINT_DATE) BETWEEN (@QUARTER*3)-2 AND @QUARTER*3

DECLARE @COUNT INT;

IF (SELECT COUNT(*) FROM #TMP)<11
BEGIN
	SELECT @COUNT=COUNT(*) FROM #TMP
	WHILE @COUNT<11
	BEGIN
		INSERT INTO #TMP
		SELECT 0,0,'1900-01-01','','1900-01-01','',1,'1900-01-01','','',''
		SET @COUNT=@COUNT+1
	END 
END

SELECT * 
FROM #TMP
ORDER BY CASE WHEN COMPLAINT_DATE='1900-01-01' THEN '9999-12-31' ELSE COMPLAINT_DATE END

GO

IF OBJECT_ID('REPORT_OPS_R10') IS NOT NULL
	DROP PROC REPORT_OPS_R10
GO

CREATE PROC [dbo].[REPORT_OPS_R10]
	@YEAR_ID INT=2017,
	@QUARTER INT=1
AS	
-- START REPORT_OPS_R10

/*
	
	@2017-12-29	By Roem Chamroeun
	1. Add defualt value for column ROW_DATE= '1900-01-01'
	
*/

SELECT r.* 
	,c.CUSTOMER_CODE
	,c.LAST_NAME_KH
	,c.FIRST_NAME_KH
INTO #TMP
FROM TBL_OPS_R10 r 
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=r.CUSTOMER_ID
WHERE r.IS_ACTIVE=1 
	AND YEAR(r.DISCONNECT_DATE)=@YEAR_ID 
	AND MONTH(r.DISCONNECT_DATE) BETWEEN (@QUARTER*3)-2 AND @QUARTER*3
UNION ALL
SELECT  
	RECONNECTION_ID=ISNULL(g.CHNAGE_ID,1)
	,g.CUSTOMER_ID
	,DISCONNECT_DATE=d.CHNAGE_DATE 
	,DISCONNECT_REASON=N'អ្នកប្រើប្រាស់ពុំទូទាត់ថ្លៃប្រើប្រាស់អគ្គិសនី'
	,RECONNECT_DATE=ISNULL(g.CHNAGE_DATE,'1900-01-01')
	,IS_ACTIVE=1
	,ROW_DATE='1900-01-01'
	,c.CUSTOMER_CODE
	,c.LAST_NAME_KH
	,c.FIRST_NAME_KH 
FROM TBL_CUS_STATUS_CHANGE g 
INNER JOIN TBL_CUSTOMER c ON g.CUSTOMER_ID=c.CUSTOMER_ID
OUTER APPLY(
	SELECT TOP(1) *
	FROM TBL_CUS_STATUS_CHANGE 
	WHERE CUSTOMER_ID=g.CUSTOMER_ID
		AND CHNAGE_ID<g.CHNAGE_ID
		AND (NEW_STATUS_ID=3 AND OLD_STATUS_ID=2)
		AND YEAR(CHNAGE_DATE)=@YEAR_ID 
		AND MONTH(CHNAGE_DATE) BETWEEN (@QUARTER*3)-2 AND @QUARTER*3
		--AND YEAR(g.CHNAGE_DATE)=@YEAR_ID 
		--AND MONTH(g.CHNAGE_DATE) BETWEEN (@QUARTER*3)-2 AND @QUARTER*3
		ORDER BY CHNAGE_ID DESC
)d
WHERE  c.IS_POST_PAID=1 
	AND YEAR(d.CHNAGE_DATE)=@YEAR_ID 
	AND MONTH(d.CHNAGE_DATE) BETWEEN (@QUARTER*3)-2 AND @QUARTER*3
    AND (c.USAGE_CUSTOMER_ID=0 OR c.USAGE_CUSTOMER_ID=c.CUSTOMER_ID)--only not 3-phase
    AND (c.IS_REACTIVE =0 )
    AND (g.NEW_STATUS_ID=2 AND g.OLD_STATUS_ID=3)

DECLARE @COUNT INT;

IF (SELECT COUNT(*) FROM #TMP)<11
BEGIN
	SELECT @COUNT=COUNT(*) FROM #TMP
	WHILE @COUNT<11
	BEGIN
		INSERT INTO #TMP
		SELECT 0,0,'1900-01-01','','1900-01-01',1,'1900-01-01','','',''
		SET @COUNT=@COUNT+1
	END 
END

SELECT * 
FROM #TMP 
ORDER BY CASE WHEN DISCONNECT_DATE='1900-01-01' THEN '9999-12-31' ELSE DISCONNECT_DATE END,
		 CASE WHEN RECONNECT_DATE='1900-01-01' THEN '9999-12-31' ELSE RECONNECT_DATE END

GO
";
        }
    }
}
