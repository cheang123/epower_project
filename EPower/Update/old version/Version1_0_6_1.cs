﻿namespace EPower.Update
{
    /// <summary> 
    /// FIX IR read total usage over 10000 (TP900,H9300,GF100)
    /// FIX manual input usage with nagative value & decimal value
    /// FIX format price in table 7 of quarter report 
    /// </summary>
    class Version1_0_6_1 : Version
    {
        protected override string GetBatchCommand()
        {
            return @""; 
        }
    }
}
