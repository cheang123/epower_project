﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EPower.Properties;

namespace EPower.Update
{
    
    class Version1_1_9_1 : Version
    {
        /// <summary> 
        /// Morm Raksmey
        /// 1. fix bug on DialogRunBill: show total working day when form load
        /// 2. fix store REPORT_QUARTER_CUSTOMER_CONSUMER: add case ផ្តាច់ => ឈប់ប្រើ 
        /// 3. add Holiday for 2018
        /// 4. fix Ads on Dowload : change from Web Browser to PictureBox
        /// </summary>
        /// <returns></returns>
        protected override string GetBatchCommand()
        {
            return @"
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME='IS_ACTIVE' AND TABLE_NAME='TLKP_BOX_TYPE')
    ALTER TABLE TLKP_BOX_TYPE ADD IS_ACTIVE BIT 
GO
UPDATE TLKP_BOX_TYPE SET IS_ACTIVE=1 WHERE IS_ACTIVE IS NULL
GO
ALTER TABLE TLKP_BOX_TYPE ALTER COLUMN IS_ACTIVE BIT NOT NULL

GO
IF NOT EXISTS(SELECT * FROM TBL_HOLIDAY WHERE YEAR(HOLIDAY_DATE)=2018)
	INSERT INTO TBL_HOLIDAY
	SELECT N'ទិវា​ចូល​ឆ្នាំ​សកល','Jan  1 2018 12:00AM',N'',1 UNION ALL
	SELECT N'ទិវា​ជ័យជម្នះ​លើ​របប​ប្រល័យ​ពូជ​សាសន៍','Jan  7 2018 12:00AM',N'',1 UNION ALL
	SELECT N'ពិធី​បុណ្យ​មាឃ​បូជា','Jan 31 2018 12:00AM',N'',1 UNION ALL
	SELECT N'ទិវា​នារី​អន្តរជាតិ','Mar  8 2018 12:00AM',N'',1 UNION ALL
	SELECT N'ពិធី​បុណ្យ​ចូល​ឆ្នាំ​ថ្មី ប្រពៃណី​ជាតិ ឆ្នាំ ច សំរិទ្ធស័ក','Apr 14 2018 12:00AM',N'',1 UNION ALL
	SELECT N'ពិធី​បុណ្យ​ចូល​ឆ្នាំ​ថ្មី ប្រពៃណី​ជាតិ ឆ្នាំ ច សំរិទ្ធស័ក','Apr 15 2018 12:00AM',N'',1 UNION ALL
	SELECT N'ពិធី​បុណ្យ​ចូល​ឆ្នាំ​ថ្មី ប្រពៃណី​ជាតិ ឆ្នាំ ច សំរិទ្ធស័ក','Apr 16 2018 12:00AM',N'',1 UNION ALL
	SELECT N'ពិធី​បុណ្យ​វិសាខ​បូជា','Apr 29 2018 12:00AM',N'',1 UNION ALL
	SELECT N'ទិវា​ពលកម្ម​អន្តរជាតិ','May  1 2018 12:00AM',N'',1 UNION ALL
	SELECT N'ព្រះ​រាជ​ពិធី​ច្រត់​ព្រះ​នង្គ័ល','May  3 2018 12:00AM',N'',1 UNION ALL
	SELECT N'ព្រះ​រាជ​ពិធី​បុណ្យ​ចម្រើន​ព្រះ​ជន្ម ព្រះ​ករុណា ព្រះ​បាទ​សម្តេច ព្រះ​បរម​នាថ នរោត្តម សីហមុនី ','May 13 2018 12:00AM',N'',1 UNION ALL
	SELECT N'ព្រះ​រាជ​ពិធី​បុណ្យ​ចម្រើន​ព្រះ​ជន្ម ព្រះ​ករុណា ព្រះ​បាទ​សម្តេច ព្រះ​បរម​នាថ នរោត្តម សីហមុនី ','May 14 2018 12:00AM',N'',1 UNION ALL
	SELECT N'ព្រះ​រាជ​ពិធី​បុណ្យ​ចម្រើន​ព្រះ​ជន្ម ព្រះ​ករុណា ព្រះ​បាទ​សម្តេច ព្រះ​បរម​នាថ នរោត្តម សីហមុនី ','May 15 2018 12:00AM',N'',1 UNION ALL
	SELECT N'ទិវា​កុមារ​អន្តរ​ជាតិ','Jun  1 2018 12:00AM',N'',1 UNION ALL
	SELECT N'ព្រះ​រាជ​ពិធី​បុណ្យ​ចម្រើន​ព្រះ​ជន្ម សម្តេច​ព្រះ​មហាក្សត្រី ព្រះ​វររាជ​មាតា នរោត្តម មុនិនាថ សីហនុ','Jun 18 2018 12:00AM',N'',1 UNION ALL
	SELECT N'ទិវា​ប្រកាស​រដ្ឋ​ធម្មនុញ្ញ','Sep 24 2018 12:00AM',N'',1 UNION ALL
	SELECT N'ពិធី​បុណ្យ​ភ្ជុំ​បិណ្ឌ','Oct  8 2018 12:00AM',N'',1 UNION ALL
	SELECT N'ពិធី​បុណ្យ​ភ្ជុំ​បិណ្ឌ','Oct  9 2018 12:00AM',N'',1 UNION ALL
	SELECT N'ពិធី​បុណ្យ​ភ្ជុំ​បិណ្ឌ','Oct 10 2018 12:00AM',N'',1 UNION ALL
	SELECT N'ទិវា​ប្រារព្ធ​ពិធី​គោរព​ព្រះវិញ្ញាណក្ខន្ធ ព្រះករុណា​  ព្រះបាទ​សម្តេច​ព្រះនរោត្តម សីហនុ ព្រះមហាវីរក្សត្រ ព្រះ​វររាជ​បិតា​ឯករាជ្យ បូរណភាព​ទឹកដី និង​ឯកភាព​ជាតិ​ខ្មែរ ព្រះបរមរតនកោដ្ឋ','Oct 15 2018 12:00AM',N'',1 UNION ALL
	SELECT N'ទិវារំលឹក​ខួប​នៃ​កិច្ចព្រមព្រៀង​សន្តិភាព​ទីក្រុង​ប៉ារីស','Oct 23 2018 12:00AM',N'',1 UNION ALL
	SELECT N'ព្រះ​រាជ​ពិធី​គ្រង​ព្រះ​បរម​រាជ​សម្បត្តិ​របស់ ព្រះ​ករុណា ព្រះ​បាទ​សម្តេច ព្រះ​បរមនាថ នរោត្តម សីហមុនី ព្រះ​មហាក្សត្រ​ នៃ​ព្រះរាជាណាចក្រ​កម្ពុជា','Oct 29 2018 12:00AM',N'',1 UNION ALL
	SELECT N'ពិធី​បុណ្យ​ឯករាជ្យ​ជាតិ','Nov  9 2018 12:00AM',N'',1 UNION ALL
	SELECT N'ព្រះ​រាជ​ពិធី​បុណ្យ​អុំ​ទូក បណ្ដែត​ប្រទីប និង​សំពះ​ព្រះ​ខែ អកអំបុក','Nov 21 2018 12:00AM',N'',1 UNION ALL
	SELECT N'ព្រះ​រាជ​ពិធី​បុណ្យ​អុំ​ទូក បណ្ដែត​ប្រទីប និង​សំពះ​ព្រះ​ខែ អកអំបុក','Nov 22 2018 12:00AM',N'',1 UNION ALL
	SELECT N'ព្រះ​រាជ​ពិធី​បុណ្យ​អុំ​ទូក បណ្ដែត​ប្រទីប និង​សំពះ​ព្រះ​ខែ អកអំបុក','Nov 23 2018 12:00AM',N'',1 UNION ALL
	SELECT N'ទិវា​សិទ្ធិ​មនុស្ស​អន្តរជាតិ','Dec 10 2018 12:00AM',N'',1
GO

IF OBJECT_ID('REPORT_QUARTER_CUSTOMER_CONSUMER')IS NOT NULL
	DROP PROC REPORT_QUARTER_CUSTOMER_CONSUMER
GO

/*
@2016-07-11 By Morm Raksmey
- Customer Licensee not include in Consumer 
@2017-07-11 By Morm Raksmey
- Total Customer
@2018-01-10 By Morm Raksmey
- add case : WHEN OLD_STATUS_ID=3 AND NEW_STATUS_ID=4 THEN -1 -- ផ្តាច់ => ឈប់ប្រើ 
*/

CREATE PROC REPORT_QUARTER_CUSTOMER_CONSUMER
	@DATE DATETIME='2017-04-01'
AS
DECLARE @D1 DATETIME,
		@D2 DATETIME;
SET @DATE = DATEADD(D,0,DATEDIFF(D,0,@DATE));
SET @D1= @DATE; 
SET @D2= DATEADD(S,-1,DATEADD(M,3,@DATE));

SELECT TOP 12 *
FROM (
SELECT	t.CUSTOMER_TYPE_ID,
		t.CUSTOMER_TYPE_NAME,
		t.DAILY_SUPPLY_HOUR,
		NUMBER = ISNULL(SUM(CASE WHEN OLD_STATUS_ID=1 AND NEW_STATUS_ID=2 THEN 1 -- អតិថិជនថ្មី
								 WHEN OLD_STATUS_ID=2 AND NEW_STATUS_ID=4 THEN -1 -- កំពុងប្រើប្រាស => ឈប់ប្រើ
								 WHEN OLD_STATUS_ID=4 AND NEW_STATUS_ID=2 THEN 1 -- ឈប់ប្រើប្រាស់ => កំពុងប្រើប្រាស់
								 WHEN OLD_STATUS_ID=3 AND NEW_STATUS_ID=4 THEN -1 -- ផ្តាច់ => ឈប់ប្រើ 
								 ELSE 0 END),0),
		--NUMBER = ISNULL(SUM(CASE WHEN NEW_STATUS_ID=2 THEN 1 WHEN OLD_STATUS_ID=2 THEN -1 ELSE 0 END),0),
		REMARK = NOTE
FROM TBL_CUSTOMER c
INNER JOIN TBL_CUSTOMER_TYPE t ON t.CUSTOMER_TYPE_ID = c.CUSTOMER_TYPE_ID
INNER JOIN TBL_CUS_STATUS_CHANGE g ON g.CUSTOMER_ID=c.CUSTOMER_ID  
WHERE g.CHNAGE_DATE  <= @D2
      AND (c.USAGE_CUSTOMER_ID=0 OR c.USAGE_CUSTOMER_ID=c.CUSTOMER_ID)--only not 3-phase
	  AND (c.IS_REACTIVE =0 ) -- not reactive customer
	  AND c.CUSTOMER_TYPE_ID NOT IN (-1) -- customer in production
	  AND c.CUSTOMER_ID NOT IN(SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE=1 AND (START_DATE <= @D2)
		  AND (IS_INUSED=1 OR END_DATE > @D1 ))
GROUP BY 	t.CUSTOMER_TYPE_ID,
		t.CUSTOMER_TYPE_NAME,
		t.DAILY_SUPPLY_HOUR,
		t.NOTE 

UNION ALL SELECT NULL,NULL,NULL,NULL,NULL
UNION ALL SELECT NULL,NULL,NULL,NULL,NULL 
UNION ALL SELECT NULL,NULL,NULL,NULL,NULL 
UNION ALL SELECT NULL,NULL,NULL,NULL,NULL 
UNION ALL SELECT NULL,NULL,NULL,NULL,NULL 
UNION ALL SELECT NULL,NULL,NULL,NULL,NULL 
UNION ALL SELECT NULL,NULL,NULL,NULL,NULL 
UNION ALL SELECT NULL,NULL,NULL,NULL,NULL 
UNION ALL SELECT NULL,NULL,NULL,NULL,NULL 
UNION ALL SELECT NULL,NULL,NULL,NULL,NULL 
UNION ALL SELECT NULL,NULL,NULL,NULL,NULL 
UNION ALL SELECT NULL,NULL,NULL,NULL,NULL  
) x
";
        }
    }
}
