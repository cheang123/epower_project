﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EPower.Update
{
    /// <summary>
    /// Version 1.1.6.3
    /// - General fix bugs
    /// - Upgrade performance improvement
    /// - Modify holiday for new year 2017
    /// - Resovled issue when remove user login
    /// </summary>
    class Version1_1_6_3 : Version
    {
        /// <summary>
        /// Sieng Sotheara
        /// + add holiday for new year 2017
        /// Morm Raksmey
        /// + Fix on Delete : When delete record just Update status IS_ACTIVE to false not delete record 
        /// </summary>
        /// <returns></returns>
        protected override string GetBatchCommand()
        {
            return @"
IF NOT EXISTS(SELECT * FROM TBL_HOLIDAY WHERE YEAR(HOLIDAY_DATE)=2017)
BEGIN
	INSERT INTO TBL_HOLIDAY
    SELECT N'ទិវាចូលឆ្នាំសកល','2017-01-01','',1 UNION ALL
    SELECT N'ទិវាជ័យជម្នះលើរបបប្រល័យពូជសាសន៍','2017-01-07','',1 UNION ALL
    SELECT N'ពិធីបុណ្យមាឃបូជា','2017-02-11','',1 UNION ALL
    SELECT N'ទិវានារីអន្តរជាតិ','2017-03-08','',1 UNION ALL
    SELECT N'ពិធីបុណ្យចូលឆ្នាំថ្មី ប្រពៃណីជាតិ ឆ្នាំរកា','2017-04-14','',1 UNION ALL
    SELECT N'ពិធីបុណ្យចូលឆ្នាំថ្មី ប្រពៃណីជាតិ ឆ្នាំរកា','2017-04-15','',1 UNION ALL
    SELECT N'ពិធីបុណ្យចូលឆ្នាំថ្មី ប្រពៃណីជាតិ ឆ្នាំរកា','2017-04-16','',1 UNION ALL
    SELECT N'ទិវាពលកម្មអន្តរជាតិ','2017-05-01','',1 UNION ALL
    SELECT N'ពិធីបុណ្យវិសាខបូជា','2017-05-10','',1 UNION ALL
    SELECT N'ព្រះរាជពិធីបុណ្យចម្រើនព្រះជន្ម ព្រះករុណាព្រះបាទ សម្តេចព្រះបរមនាថ នរោត្តម សីហមុនី','2017-05-13','',1 UNION ALL
    SELECT N'ព្រះរាជពិធីបុណ្យចម្រើនព្រះជន្ម ព្រះករុណាព្រះបាទ សម្តេចព្រះបរមនាថ នរោត្តម សីហមុនី និងព្រះរាជពិធីច្រត់ព្រះនង្គ័ល','2017-05-14','',1 UNION ALL
    SELECT N'ព្រះរាជពិធីបុណ្យចម្រើនព្រះជន្ម ព្រះករុណាព្រះបាទ សម្តេចព្រះបរមនាថ នរោត្តម សីហមុនី','2017-05-15','',1 UNION ALL
    SELECT N'ទិវាកុមារអន្តរជាតិ','2017-06-01','',1 UNION ALL
    SELECT N'ព្រះរាជពិធីបុណ្យចម្រើនព្រះជន្ម សម្តេចព្រះមហាក្សត្រី ព្រះវររាជមាតា នរោត្តម មុនិនាថ សីហនុ','2017-06-18','',1 UNION ALL
    SELECT N'ពិធីបុណ្យភ្ជុំបិណ្ឌ','2017-09-19','',1 UNION ALL
    SELECT N'ពិធីបុណ្យភ្ជុំបិណ្ឌ','2017-09-20','',1 UNION ALL
    SELECT N'ពិធីបុណ្យភ្ជុំបិណ្ឌ','2017-09-21','',1 UNION ALL
    SELECT N'ទិវាប្រកាសរដ្ឋធម្មនុញ្ញ','2017-09-24','',1 UNION ALL
    SELECT N'ទិវាប្រារព្ធពិធីគោរពព្រះវិញ្ញាណក្ខន្ធ ព្រះករុណាព្រះបាទសម្តេចព្រះនរោត្តម ស៊ីហនុ ព្រះមហាវីរក្សត្រ ព្រះវររាជបិតា ឯករាជ្យ បូរណភាពទឹកដី និងឯកភាពជាតិខ្មែរ','2017-10-15','',1 UNION ALL
    SELECT N'ទិវារំលឹកខួបនៃកិច្ចព្រមព្រៀងសន្តិភាពក្រុងប៉ារីស','2017-10-23','',1 UNION ALL
    SELECT N'ព្រះរាជពិធីគ្រងព្រះបរមរាជសម្បត្តិព្រះបរមនាថ នរោត្តម សីហមុនី','2017-10-29','',1 UNION ALL
    SELECT N'ព្រះរាជពិធីបុណ្យអុំទូក បណ្តែតប្រទីប និងសំពះព្រះខែ អកអំបុក','2017-11-02','',1 UNION ALL
    SELECT N'ព្រះរាជពិធីបុណ្យអុំទូក បណ្តែតប្រទីប និងសំពះព្រះខែ អកអំបុក','2017-11-03','',1 UNION ALL
    SELECT N'ព្រះរាជពិធីបុណ្យអុំទូក បណ្តែតប្រទីប និងសំពះព្រះខែ អកអំបុក','2017-11-04','',1 UNION ALL
    SELECT N'ពិធីបុណ្យឯករាជ្យជាតិ','2017-11-09','',1 UNION ALL
    SELECT N'ទិវាសិទ្ធិមនុស្សអន្តរជាតិ','2017-12-10','',1
END
GO
";
        }
        
    }
}
