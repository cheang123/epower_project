﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EPower.Properties;

namespace EPower.Update
{
    
    class Version1_1_8_5 : Version
    {
        /// <summary> 
        /// Prak Serey 24-Oct-2017
        /// 1. Release new Android IR HT380D
        /// 2. Add adb directory and files to E-Power root directory
        /// 3. Add Newtonsoft.Json DLL
        /// 4. Update EPower.AMR System
        /// </summary>
        /// <returns></returns>
        protected override string GetBatchCommand()
        {
            return @"";
        }
    }
}
