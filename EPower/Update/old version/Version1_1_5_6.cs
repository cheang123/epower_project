﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using EPower.Properties;

namespace EPower.Update
{
    /// <summary>
    /// Ry Rith
    /// - EAC app
    /// </summary>
    class Version1_1_5_6 : Version
    {
        protected override string GetBatchCommand()
        {
            return  Resources.Version1_1_5_6;
        }
    }
}
