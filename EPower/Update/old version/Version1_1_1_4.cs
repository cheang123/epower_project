﻿namespace EPower.Update
{
    /// <summary>
    ///  - Add Feature Utiltiy 63 ENABLE_ORDER_RELAY_METER
    ///    + Table 
    ///         + TBL_ORDER_RELAY_METER
    ///         + TBL_RELAY_METER
    ///         + TMP_IR_RELAY_METER
    ///    + Proc 
    ///         + RUN_ORDER_RELAY_METER
    ///         + REPORT_ORDER_RELAY_METER
    /// </summary>
    /// <returns></returns>

    class Version1_1_1_4 : Version
    {
        protected override string GetBatchCommand()
        {
            return @"
IF OBJECT_ID('TBL_ORDER_RELAY_METER') IS NULL
CREATE TABLE TBL_ORDER_RELAY_METER
(
	ORDER_RELAY_METER_ID INT PRIMARY KEY IDENTITY,
	DEVICE_ID INT NOT NULL,
	CREATE_ON DATETIME NOT NULL,
	CREATE_BY NVARCHAR(50) NOT NULL
);
GO
IF OBJECT_ID('TBL_RELAY_METER') IS NULL
CREATE TABLE TBL_RELAY_METER
(
	RELAY_METER_ID UNIQUEIDENTIFIER PRIMARY KEY,
	CUSTOMER_ID INT NOT NULL,
	METER_ID INT NOT NULL,
	CUSTOMER_CODE NVARCHAR(50) NOT NULL,
	METER_CODE NVARCHAR(50) NOT NULL,
	BOX_CODE NVARCHAR(50) NOT NULL,
	FULL_NAME NVARCHAR(100) NOT NULL,
	AREA_CODE NVARCHAR(50) NOT NULL,
	POLE_CODE NVARCHAR(50) NOT NULL,
	CREATE_ON DATETIME NOT NULL,
	ORDER_RELAY_STATUS INT NOT NULL,
	METER_RELAY_STATUS INT NOT NULL,
	SEND_ORDER_RELAY_METER_ID INT NOT NULL,
	GET_ORDER_RELAY_METER_ID INT NOT NULL,
	CHNAGE_ID INT NOT NULL
);
GO
IF OBJECT_ID('TMP_IR_RELAY_METER') IS NULL
CREATE TABLE TMP_IR_RELAY_METER
(
	ID INT PRIMARY KEY IDENTITY,
	RELAY_METER_ID UNIQUEIDENTIFIER NOT NULL,
	METER_CODE NVARCHAR(50) NOT NULL,
	BOX_CODE NVARCHAR(50) NOT NULL,
	FULL_NAME NVARCHAR(100) NOT NULL,
	AREA_CODE NVARCHAR(50) NOT NULL,
	POLE_CODE NVARCHAR(50) NOT NULL,
	CREATE_ON DATETIME NOT NULL,
	ORDER_RELAY_STATUS INT NOT NULL,
	METER_RELAY_STATUS INT NOT NULL
);
GO

DELETE TBL_UTILITY WHERE UTILITY_ID = 63;
INSERT INTO TBL_UTILITY(UTILITY_ID, UTILITY_NAME, [DESCRIPTION], UTILITY_VALUE) 
VALUES(63, N'ENABLE_ORDER_RELAY_METER', N'USER USE IR TO CONTROL RELAY IN METER TO CONNECT OR DISCONNECT POWER', 0);

GO
IF OBJECT_ID('RUN_ORDER_RELAY_METER') IS NOT NULL
	DROP PROC RUN_ORDER_RELAY_METER
GO
CREATE PROC RUN_ORDER_RELAY_METER
	
	@DEVICE_ID INT = 19,
	@CREATE_BY NVARCHAR(100) = 'GF-1100'
WITH ENCRYPTION
AS
DECLARE @GET_ORDER_LOG INT;
DECLARE @CREATE_ON DATETIME;

SET @CREATE_ON = GETDATE();
INSERT INTO TBL_ORDER_RELAY_METER(DEVICE_ID, CREATE_ON, CREATE_BY) VALUES (@DEVICE_ID, @CREATE_ON ,@CREATE_BY);

SELECT 
	@GET_ORDER_LOG = MAX(ORDER_RELAY_METER_ID)
FROM TBL_ORDER_RELAY_METER;

-- CHECK METER NOT REGISTER OR UPDATE ALREADY
DELETE TMP_IR_RELAY_METER
WHERE RELAY_METER_ID IN(
	SELECT pc.RELAY_METER_ID 
	FROM TBL_RELAY_METER pc
	INNER JOIN TMP_IR_RELAY_METER ir ON  pc.RELAY_METER_ID = ir.RELAY_METER_ID
	INNER JOIN TBL_METER m ON pc.METER_ID = m.METER_ID
	WHERE m.REGISTER_CODE = ''
	OR pc.GET_ORDER_RELAY_METER_ID > 0);
	
-- UPDATE METER RELAY STATUS
UPDATE pc
SET pc.METER_RELAY_STATUS = ir.METER_RELAY_STATUS,
	pc.CREATE_ON = @CREATE_ON,
	pc.GET_ORDER_RELAY_METER_ID = @GET_ORDER_LOG
FROM TBL_RELAY_METER pc
INNER JOIN TMP_IR_RELAY_METER ir ON pc.RELAY_METER_ID = ir.RELAY_METER_ID;
 
-- UPDATE CUSTOMER STATUS CHANGE
INSERT INTO TBL_CUS_STATUS_CHANGE(
	CUSTOMER_ID,
	CREATE_BY,
	CHNAGE_DATE,
	OLD_STATUS_ID,
	NEW_STATUS_ID,
	INVOICE_ID)
SELECT 
	c.CUSTOMER_ID,
	CREATE_BY = @CREATE_BY,
	CHNAGE_DATE = pc.CREATE_ON,  
	OLD_STATUS_ID = c.STATUS_ID,
	NEW_STATUS_ID = pc.METER_RELAY_STATUS,
	INVOICE_ID = 0
FROM TBL_RELAY_METER pc
INNER JOIN TBL_CUSTOMER c ON pc.CUSTOMER_ID = c.CUSTOMER_ID
WHERE GET_ORDER_RELAY_METER_ID = @GET_ORDER_LOG;

-- UPDATE CHANGE_ID IN TBL_RELAY_METER
UPDATE TBL_RELAY_METER
SET CHNAGE_ID = (SELECT MAX(CHNAGE_ID) FROM TBL_CUS_STATUS_CHANGE WHERE CUSTOMER_ID = TBL_RELAY_METER.CUSTOMER_ID)
WHERE GET_ORDER_RELAY_METER_ID = @GET_ORDER_LOG;

-- UPDATE CUSTOMER STATUS
UPDATE c
SET STATUS_ID = pc.METER_RELAY_STATUS
FROM TBL_CUSTOMER c
INNER JOIN TBL_RELAY_METER pc ON c.CUSTOMER_ID = pc.CUSTOMER_ID
WHERE GET_ORDER_RELAY_METER_ID = @GET_ORDER_LOG;

GO
IF OBJECT_ID('REPORT_ORDER_RELAY_METER') IS NOT NULL
	DROP PROC REPORT_ORDER_RELAY_METER
GO
CREATE PROC [dbo].[REPORT_ORDER_RELAY_METER]
	@ORDER_RELAY_METER_ID INT = 1,
	@DEVICE_CODE NVARCHAR(100) = '',
	@CREATE_ON DATETIME = '1900-01-01',
	@CREATE_BY NVARCHAR(100) = '',
	@OPERATION NVARCHAR(100) = N'បញ្ជូនទិន្នន័យទៅឧបករណ៍'
AS

SELECT 
	@DEVICE_CODE = d.DEVICE_CODE,
	@CREATE_ON = o.CREATE_ON,
	@CREATE_BY = o.CREATE_BY
FROM TBL_ORDER_RELAY_METER o
INNER JOIN TBL_DEVICE d ON o.DEVICE_ID = d.DEVICE_ID 
WHERE ORDER_RELAY_METER_ID = @ORDER_RELAY_METER_ID
 
SELECT
	CUSTOMER_CODE,
	METER_CODE,
	BOX_CODE,
	FULL_NAME,
	AREA_CODE,
	POLE_CODE,
	METER_STATUS = CASE WHEN ORDER_RELAY_STATUS = 2 THEN N'ភ្ជាប់ចរន្ត' ELSE N'ផ្តាច់ចរន្ត'END
FROM TBL_RELAY_METER s
INNER JOIN TBL_ORDER_RELAY_METER o ON s.SEND_ORDER_RELAY_METER_ID = o.ORDER_RELAY_METER_ID
INNER JOIN TBL_DEVICE d ON o.DEVICE_ID = d.DEVICE_ID
WHERE o.ORDER_RELAY_METER_ID = @ORDER_RELAY_METER_ID

UNION ALL

SELECT
	CUSTOMER_CODE,
	METER_CODE,
	BOX_CODE,
	FULL_NAME,
	AREA_CODE,
	POLE_CODE,
	METER_STATUS = CASE WHEN ORDER_RELAY_STATUS = 2 THEN N'ភ្ជាប់ចរន្ត' ELSE N'ផ្តាច់ចរន្ត'END
FROM TBL_RELAY_METER s
INNER JOIN TBL_ORDER_RELAY_METER o ON s.GET_ORDER_RELAY_METER_ID = o.ORDER_RELAY_METER_ID
INNER JOIN TBL_DEVICE d ON o.DEVICE_ID = d.DEVICE_ID
WHERE o.ORDER_RELAY_METER_ID = @ORDER_RELAY_METER_ID";


        }
    }
}
