﻿using System.Linq;
using System.Transactions;
using SoftTech;

namespace EPower.Update
{
    class Version1_0_0_5
    {
        /// <summary>
        /// - ADD CONTROL STOCK     
        /// </summary>
        public void Update()
        {
            using (TransactionScope tran = new TransactionScope())
            {
                // Update version to 1.0.0.5
                string[] strSqlStatement = new string[]
                {
                    @"
                    IF NOT EXISTS( SELECT *
                    FROM INFORMATION_SCHEMA.TABLES
                    WHERE TABLE_NAME='TBL_STOCK_ITEM')
                    BEGIN
	                    CREATE TABLE TBL_STOCK_ITEM		
	                    (
		                    ITEM_ID	INT	PRIMARY KEY IDENTITY,
		                    ITEM_NAME	NVARCHAR(100)	NOT NULL,
		                    UNIT	NVARCHAR(100)	NOT NULL,
		                    IS_ACTIVE	BIT	NOT NULL
	                    );
                    END

                    IF NOT EXISTS( SELECT *
                    FROM INFORMATION_SCHEMA.TABLES
                    WHERE TABLE_NAME='TBL_STOCK_TRAN')
                    BEGIN
	                    CREATE TABLE TBL_STOCK_TRAN	
	                    (	
		                    STOCK_TRAN_ID	BIGINT	PRIMARY KEY IDENTITY,
		                    ITEM_ID	INT	NOT NULL,
		                    FORM_STOCK_TYPE_ID	INT	NOT NULL,
		                    TO_STOCK_TYPE_ID	INT	NOT NULL,
		                    STOCK_TRAN_TYPE_ID	INT	NOT NULL,
		                    CREATE_NO	DATETIME	NOT NULL,
		                    CREATE_BY	NVARCHAR(100)	NOT NULL,
		                    QTY	DECIMAL(18,4) 	NOT NULL,
		                    REMARK	NVARCHAR(200)	NOT NULL
	                    );
                    END 

                    IF NOT EXISTS( SELECT *
                    FROM INFORMATION_SCHEMA.TABLES
                    WHERE TABLE_NAME='TLKP_STOCK_TYPE')
                    BEGIN
	                    CREATE TABLE TLKP_STOCK_TYPE		
	                    (
		                    STOCK_TYPE_ID	INT	PRIMARY KEY IDENTITY,
		                    STOCK_TYPE_NAME	NVARCHAR(100)	NOT NULL
	                    );
                    END

                    IF NOT EXISTS( SELECT *
                    FROM INFORMATION_SCHEMA.TABLES
                    WHERE TABLE_NAME='TLKP_STOCK_TRAN_TYPE')
                    BEGIN
	                    CREATE TABLE TLKP_STOCK_TRAN_TYPE		
	                    (
	                    STOCK_TRAN_TYPE_ID	INT	PRIMARY KEY IDENTITY,
	                    STOCK_TRAN_TYPE	NVARCHAR(100)	NOT NULL,
	                    FORM_STOCK_TYPE_ID	INT	NOT NULL,
	                    TO_STOCK_TYPE_ID	INT	NOT NULL,
	                    )
                    END"                    
                    ,@"TRUNCATE TABLE TLKP_STOCK_TYPE
                    TRUNCATE TABLE TLKP_STOCK_TRAN_TYPE

                    SET IDENTITY_INSERT TLKP_STOCK_TYPE ON
                    INSERT INTO TLKP_STOCK_TYPE(STOCK_TYPE_ID,STOCK_TYPE_NAME) VALUES (1,N'ក្នុងស្តុក');--In_Stock
                    INSERT INTO TLKP_STOCK_TYPE(STOCK_TYPE_ID,STOCK_TYPE_NAME) VALUES (2,N'កំពុងប្រើប្រាស់');--In_Use
                    INSERT INTO TLKP_STOCK_TYPE(STOCK_TYPE_ID,STOCK_TYPE_NAME) VALUES (3,N'មិនដំណើរការ');--Unavalable
                    SET IDENTITY_INSERT TLKP_STOCK_TYPE OFF

                    SET IDENTITY_INSERT TLKP_STOCK_TRAN_TYPE ON
                    INSERT INTO TLKP_STOCK_TRAN_TYPE(STOCK_TRAN_TYPE_ID,STOCK_TRAN_TYPE,FORM_STOCK_TYPE_ID,TO_STOCK_TYPE_ID) 
                    VALUES (1,N'ចូលស្តុក',0,1);--In_Stock
                    INSERT INTO TLKP_STOCK_TRAN_TYPE(STOCK_TRAN_TYPE_ID,STOCK_TRAN_TYPE,FORM_STOCK_TYPE_ID,TO_STOCK_TYPE_ID) 
                    VALUES (2,N'បញ្ចេញស្តុក',1,0);--Out_Stock
                    INSERT INTO TLKP_STOCK_TRAN_TYPE(STOCK_TRAN_TYPE_ID,STOCK_TRAN_TYPE,FORM_STOCK_TYPE_ID,TO_STOCK_TYPE_ID) 
                    VALUES (3,N'ខូចខាត',0,0);--Damage
                    INSERT INTO TLKP_STOCK_TRAN_TYPE(STOCK_TRAN_TYPE_ID,STOCK_TRAN_TYPE,FORM_STOCK_TYPE_ID,TO_STOCK_TYPE_ID) 
                    VALUES (4,N'ប្រើប្រាស់',1,2);--Use
                    SET IDENTITY_INSERT TLKP_STOCK_TRAN_TYPE OFF",
                    //INSERT NEW [TABLE NAME] TO TLKP_TABLE
                    @"INSERT INTO TLKP_TABLE
                    SELECT TABLE_NAME, TABLE_NAME,1 ,1FROM INFORMATION_SCHEMA.TABLES
                    WHERE TABLE_NAME NOT IN (SELECT TABLE_NAME FROM TLKP_TABLE UNION SELECT 'sysdiagrams')",
                    //INSERT NEW [FIELD NAME] TO TLKP_FFIELD
                    @"INSERT INTO TLKP_TABLE_FIELD
                    SELECT col.COLUMN_NAME,col.COLUMN_NAME,tbl.TABLE_ID,1,'','',0
                    FROM INFORMATION_SCHEMA.COLUMNS col
                        INNER JOIN TLKP_TABLE tbl ON tbl.TABLE_NAME=col.TABLE_NAME
                    WHERE tbl.IS_CHANGE_LOG_USED = 1
                    AND
                    tbl.TABLE_NAME+'.'+col.COLUMN_NAME NOT IN (
                        SELECT tbl.TABLE_NAME+'.'+col.FIELD_NAME
                        FROM TLKP_TABLE_FIELD col INNER JOIN TLKP_TABLE tbl ON tbl.TABLE_ID=col.TABLE_ID
                    )",
                };
                foreach (string item in strSqlStatement)
                {
                    DBDataContext.Db.ExecuteCommand(item);
                }

                DBDataContext.Db.TBL_UTILITies.FirstOrDefault(row => row.UTILITY_ID == (int)Utility.VERSION).UTILITY_VALUE = "1.0.0.5";
                DBDataContext.Db.SubmitChanges(); 
                tran.Complete();
            }

        }
    }
}
