﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EPower.Properties;

namespace EPower.Update
{
    /// <summary>
    /// Version 1.1.7.4
    /// - fix bank payment 
    /// </summary>
    class Version1_1_7_4 : Version
    {
        /// <summary> 
        /// Morm Raksmey
        /// - fix bank payment 
        /// </summary>
        /// <returns></returns>
        protected override string GetBatchCommand()
        {
            return @"
UPDATE TBL_BANK_PAYMENT_DETAIL 
SET PAID_AMOUNT=d.PAY_AMOUNT,NOTE=d.NOTE+N' FIX BANK PAYMENT'
FROM TBL_BANK_PAYMENT_DETAIL d
INNER JOIN TBL_BANK_PAYMENT p ON p.BANK_PAYMENT_ID=d.BANK_PAYMENT_ID
WHERE d.PAY_AMOUNT>d.PAID_AMOUNT 
	AND d.BANK=N'NA' 
	AND p.BANK_PAYMENT_TYPE_ID<>3
";
        }
    }
}
