﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EPower.Properties;

namespace EPower.Update
{
    
    class Version1_1_7_7 : Version
    {
        /// <summary> 
        /// Prak Serey
        /// - Connect new android handheld via USB.
        /// - add Utility value 83 IR_ADB_PATH
        /// </summary>
        /// <returns></returns>
        protected override string GetBatchCommand()
        {
            return @"
IF NOT EXISTS(SELECT * FROM TBL_UTILITY WHERE UTILITY_ID=83)
	INSERT INTO TBL_UTILITY
	SELECT 83,N'IR_ADB_PATH',N'adb path platform-tools for connect to andoird via USB',N'adb\adb.exe'
GO
";
        }
    }
}
