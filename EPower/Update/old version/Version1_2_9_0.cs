﻿using EPower.Properties;
namespace EPower.Update
{
    class Version1_2_9_0 : Version
    {
        /*
        
        @Vonn Vonn kimputhmuyvorn
        1-Update REPORT_QUARTER_POWER_COVERAGE
        2-Update REPORT_QUARTER_CUSTOMER_CONSUMER
        3-Update REPORT_QUARTER_POWER_COVERAGE
        4-Update REPORT_POWER_SOLD_CONSUMER
        5-Update ANR.RUN_AS_01A
        6-Update REPORT_QUARTER_GENERATION_FACILITY
        7-Update RUN_PREPAID_CREDIT
            */
        protected override string GetBatchCommand()
        {
            return Resources.Version1_2_9_0;
        }
    }
}
