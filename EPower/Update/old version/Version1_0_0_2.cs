﻿using System.Linq;
using System.Transactions;
using SoftTech;

namespace EPower.Update
{
    class Version1_0_0_2
    {
        public void Update()
        {
            using (TransactionScope tran = new TransactionScope())
            {
                // Update version to 1.0.0.1
                DBDataContext.Db.TBL_UTILITies.FirstOrDefault(row => row.UTILITY_ID == (int)Utility.VERSION).UTILITY_VALUE = "1.0.0.2";
                DBDataContext.Db.SubmitChanges();

                  
                tran.Complete();
            }

        }
    }
}
