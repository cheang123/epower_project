﻿namespace EPower.Update
{
    /// <summary> 
    /// Fix Readonly file
    /// </summary>
    class Version1_0_6_4 : Version
    {
        /// <summary>
        /// Add new utility values.
        /// -ENABLE_ALL_HISTORY_USAGE
        /// </summary>
        /// <returns></returns>
        protected override string GetBatchCommand()
        {
            return @"DELETE TBL_UTILITY WHERE UTILITY_ID =53 
INSERT INTO TBL_UTILITY(UTILITY_ID,UTILITY_NAME,DESCRIPTION,UTILITY_VALUE) VALUES(53, 'ENABLE_ALL_HISTORY_USAGE', '1=ALLOW, 0=DENY', 0);"; 
        }
    }
}
