﻿namespace EPower.Update
{
    /// <summary>
    /// fix ReportQuarterly : load year based from (2009 up to current year)
    /// fix Change Meter for new activated customer : use collectorId = -1 denote change meter usage.
    /// </summary>
    class Version1_0_4_6 : Version
    {
        protected override string GetBatchCommand()
        {
            return @"";
        }
    }
}
