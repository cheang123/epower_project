﻿using System.Linq;
using System.Transactions;
using SoftTech;

namespace EPower.Update
{
    class Version1_0_0_1
    {
        public void Update()
        {
            using (TransactionScope tran = new TransactionScope())
            {
                string[] strSqlStatement = new string[]
                {
                    @"IF NOT EXISTS( SELECT *
                                        FROM INFORMATION_SCHEMA.TABLES
                                        WHERE  TABLE_NAME='TBL_CUSTOMER_BUY')
                    BEGIN
                    CREATE TABLE dbo.TBL_CUSTOMER_BUY(
                        CUSTOMER_BUY_ID BIGINT PRIMARY KEY IDENTITY(1,1) NOT NULL,
                        BUY_NO NVARCHAR(50) NOT NULL,
                        PREPAID_CUS_ID INT NOT NULL,
                        BUY_TIMES INT NOT NULL,
                        BUY_QTY DECIMAL(18, 4) NOT NULL,
                        DISCOUNT_QTY DECIMAL(18, 4) NOT NULL,
                        PRICE DECIMAL(18, 4) NOT NULL,
                        BUY_AMOUNT DECIMAL(18, 4) NOT NULL,
                        USER_CASH_DRAWER_ID INT NOT NULL,
                        CREATE_ON datetime NOT NULL,
                        CREATE_BY NVARCHAR(100) NOT NULL,
                        COMPENSATED DECIMAL(18, 4) NOT NULL,
                        IS_VOID BIT NOT NULL,
                        PARENT_ID BIGINT NOT NULL,
                     )
                    END",
                    @"IF NOT EXISTS( SELECT *
                                    FROM INFORMATION_SCHEMA.TABLES
                                    WHERE  TABLE_NAME='TBL_PREPAID_CUSTOMER')
                    BEGIN
                    CREATE TABLE dbo.TBL_PREPAID_CUSTOMER(
                        PREPAID_CUS_ID INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
                        CUSTOMER_ID INT NOT NULL,
                        CARD_CODE NVARCHAR(50) NOT NULL,
                        BUY_TIMES INT NOT NULL,
                        TOTAL_BUY_POWER DECIMAL(18, 4) NOT NULL,
                        ALARM_METHOD INT NOT NULL,
                        PRICE_ID INT NOT NULL,
                        COMPENSATED DECIMAL(18, 4) NOT NULL,
                     )
                    END",
                    @"IF NOT EXISTS( SELECT *
                                    FROM INFORMATION_SCHEMA.TABLES
                                    WHERE  TABLE_NAME='TBL_TRANSFORMER')
                    BEGIN
                    CREATE TABLE dbo.TBL_TRANSFORMER(
	                    TRANSFORMER_ID INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
	                    TRANSFORMER_CODE NVARCHAR(50) NOT NULL,
	                    IS_ACTIVE BIT NOT NULL,
	                    CAPACITY_ID INT NOT NULL,
                    )
                    END",
                    "INSERT INTO TBL_TRANSFORMER(TRANSFORMER_CODE,IS_ACTIVE,CAPACITY_ID) VALUES(N'ត្រង់ស្វូរទី១',1,1)"
                    ,@"IF NOT EXISTS( SELECT *
                                    FROM INFORMATION_SCHEMA.COLUMNS
                                    WHERE COLUMN_NAME='TRANSFORMER_ID' AND TABLE_NAME='TBL_POLE')
                    BEGIN
	                    ALTER TABLE TBL_POLE
	                    ADD  TRANSFORMER_ID INT NULL
                    END",
                    @"UPDATE TBL_POLE
	                SET TRANSFORMER_ID=1
                	
	                ALTER TABLE TBL_POLE
	                ALTER COLUMN TRANSFORMER_ID INT NOT NULL",
                    @"IF EXISTS( SELECT *
                                    FROM INFORMATION_SCHEMA.COLUMNS
                                    WHERE COLUMN_NAME='POLE_NAME' OR COLUMN_NAME='NO_METER' AND TABLE_NAME='TBL_POLE')
                    BEGIN
	                    ALTER TABLE TBL_POLE
	                    DROP COLUMN POLE_NAME
                    END	"
                    ,@"IF EXISTS( SELECT *
                                    FROM INFORMATION_SCHEMA.COLUMNS
                                    WHERE COLUMN_NAME='NO_METER' AND TABLE_NAME='TBL_POLE')
                    BEGIN
	                    ALTER TABLE TBL_POLE
	                    DROP COLUMN NO_METER
                    END"
                    ,@"IF NOT EXISTS( SELECT *
                                    FROM INFORMATION_SCHEMA.COLUMNS
                                    WHERE COLUMN_NAME='REGISTER_CODE' AND TABLE_NAME='TBL_METER')
                    BEGIN
	                    ALTER TABLE TBL_METER
	                    ADD REGISTER_CODE NVARCHAR(100) NULL,
	                    IS_DIGITAL BIT NULL;	
                    END"
                    ,@"UPDATE TBL_METER
                    SET REGISTER_CODE=N'', IS_DIGITAL =0

                    ALTER TABLE TBL_METER ALTER COLUMN REGISTER_CODE NVARCHAR(100) NOT NULL
                    ALTER TABLE TBL_METER ALTER COLUMN IS_DIGITAL BIT NOT NULL"
                    ,@"IF NOT EXISTS( SELECT *
                                    FROM INFORMATION_SCHEMA.COLUMNS
                                    WHERE COLUMN_NAME='IS_LOCK_METER' AND TABLE_NAME='TBL_CONFIG')
                    BEGIN
	                    ALTER TABLE TBL_CONFIG
	                    ADD IS_LOCK_METER BIT NULL
                    END"
                    ,@"UPDATE TBL_CONFIG
	                SET IS_LOCK_METER =0
                	
	                ALTER TABLE TBL_CONFIG
	                ALTER COLUMN IS_LOCK_METER BIT NOT NULL"
                    ,@"IF NOT EXISTS( SELECT *
                                    FROM INFORMATION_SCHEMA.COLUMNS
                                    WHERE COLUMN_NAME='IS_POST_PAID' AND TABLE_NAME='TBL_CUSTOMER')
                    BEGIN
	                    ALTER TABLE TBL_CUSTOMER 
	                    ADD IS_POST_PAID BIT NULL
                    END"
                    ,@"UPDATE TBL_CUSTOMER
	                SET IS_POST_PAID=1

	                ALTER TABLE TBL_CUSTOMER
	                ALTER COLUMN IS_POST_PAID BIT NOT NULL"
                    ,@"IF EXISTS( SELECT *
                                    FROM INFORMATION_SCHEMA.COLUMNS
                                    WHERE COLUMN_NAME='MININUM_USAGE' AND TABLE_NAME='TBL_CUSTOMER')
                    BEGIN
                        ALTER TABLE TBL_CUSTOMER
                        DROP COLUMN  MININUM_USAGE
                    END"
                    ,@"IF NOT EXISTS( SELECT *
                                    FROM INFORMATION_SCHEMA.COLUMNS
                                    WHERE COLUMN_NAME='IS_EDITABLE' AND TABLE_NAME='TBL_INVOICE_ITEM')
                    BEGIN
	                    ALTER TABLE TBL_INVOICE_ITEM 
	                    ADD IS_EDITABLE BIT NULL
                    END"
                    ,@"UPDATE TBL_INVOICE_ITEM
	                SET IS_EDITABLE =0
                	
	                ALTER TABLE TBL_INVOICE_ITEM
	                ALTER COLUMN IS_EDITABLE BIT NOT NULL"
                    ,@"IF NOT EXISTS( SELECT *
                                    FROM INFORMATION_SCHEMA.COLUMNS
                                    WHERE COLUMN_NAME='POWER_SOLD_PREPAID' AND TABLE_NAME='TBL_POWER')
                    BEGIN
	                    ALTER TABLE TBL_POWER 
	                    ADD POWER_SOLD_PREPAID DECIMAL(18,4) NULL,
	                    TRANSFORMER_ID INT NULL
                    END"
                    ,@"UPDATE TBL_POWER
	                SET POWER_SOLD_PREPAID=0, TRANSFORMER_ID=0

	                ALTER TABLE TBL_POWER ALTER COLUMN POWER_SOLD_PREPAID DECIMAL(18,4) NOT NULL
	                ALTER TABLE TBL_POWER ALTER COLUMN TRANSFORMER_ID INT NOT NULL"
                    ,@"IF EXISTS( SELECT *
                                    FROM INFORMATION_SCHEMA.COLUMNS
                                    WHERE COLUMN_NAME='METER_REGISTER_CODE' AND TABLE_NAME='TBL_TMP_USAGE')
                    BEGIN
	                    EXEC sp_rename 'TBL_TMP_USAGE.METER_REGISTER_CODE', 'REGISTER_CODE', 'COLUMN' 
                    END"
                    ,
                    "IF OBJECT_ID (N'dbo.REPORT_PREPAID_MONTH_TO_DATE_SALE_SUMMARY') IS NOT NULL DROP PROCEDURE REPORT_PREPAID_MONTH_TO_DATE_SALE_SUMMARY"
                    ,@"CREATE PROC dbo.REPORT_PREPAID_MONTH_TO_DATE_SALE_SUMMARY
                        @AREA_ID INT=0,	
                        @CUSTOMER_TYPE_ID INT=0,
                        @START_DATE DATETIME='2010-05-01',
                        @END_DATE DATETIME='2011-05-01'
                    AS
                    BEGIN
                        -- @BY    : PRAK SEREY
                        -- @SINCE : 2011-05-08 Add More parameters.

                        SELECT  CUSTOMER_CODE = c.CUSTOMER_CODE,
                                CUSTOMER_NAME = c.LAST_NAME_KH+' '+c.FIRST_NAME_KH+' '+c.FIRST_NAME+' '+c.LAST_NAME,
                                PHONE = c.PHONE_1+' '+c.PHONE_2,
                                a.AREA_ID, 
                                AREA_NAME = ISNULL(a.AREA_NAME,'-'),
                                POLE_NAME = ISNULL(p.POLE_CODE,'-'),
                                BOX_CODE = ISNULL(b.BOX_CODE,'-'),
                                METER_CODE = ISNULL(m.METER_CODE,'-'),		
                                c.ACTIVATE_DATE,
                                TOTAL_BUY_POWER=ISNULL( u.TOTAL_BUY_POWER ,0),
                                TOTAL_AMOUNT=ISNULL(u.TOTAL_AMOUNT,0),
                                TOTAL_DISCOUNT=ISNULL(u.TOTAL_DISCOUNT,0),
                                TOTAL_BUY_TIMES=ISNULL(u.TOTAL_BUY_TIMES,0)
                        FROM TBL_CUSTOMER c 
                        INNER JOIN TBL_PREPAID_CUSTOMER pc ON c.CUSTOMER_ID = pc.CUSTOMER_ID
                        LEFT JOIN TBL_CUSTOMER_METER cm ON cm.CUSTOMER_ID=c.CUSTOMER_ID AND cm.IS_ACTIVE=1
                        LEFT JOIN TBL_METER m ON cm.METER_ID=m.METER_ID
                        LEFT JOIN TBL_AREA a ON a.AREA_ID=c.AREA_ID
                        LEFT JOIN TBL_POLE p ON p.POLE_ID=cm.POLE_ID
                        LEFT JOIN TBL_BOX b ON b.BOX_ID=cm.BOX_ID
                        OUTER APPLY (
                            SELECT SUM(BUY_QTY) AS TOTAL_BUY_POWER, 
                                   SUM(BUY_AMOUNT) AS TOTAL_AMOUNT,
                                   SUM(DISCOUNT_QTY) AS TOTAL_DISCOUNT,	
                                   COUNT(BUY_TIMES) AS TOTAL_BUY_TIMES 
                            FROM TBL_CUSTOMER_BUY 
                            WHERE CREATE_ON BETWEEN @START_DATE AND @END_DATE
                                AND PREPAID_CUS_ID=pc.PREPAID_CUS_ID 
                                AND IS_VOID=0	        
                    			
                        ) u
                        WHERE (@AREA_ID=0 OR a.AREA_ID=@AREA_ID)	
                            AND (@CUSTOMER_TYPE_ID=0 OR c.CUSTOMER_TYPE_ID=@CUSTOMER_TYPE_ID)
                            AND c.STATUS_ID != 5
                            AND c.IS_POST_PAID=0;
                    END"
                    ,"IF OBJECT_ID (N'dbo.REPORT_METER_UNREGISTER') IS NOT NULL DROP PROCEDURE REPORT_METER_UNREGISTER "
                    ,@"CREATE PROC dbo.REPORT_METER_UNREGISTER
                    AS
                    -- @BY	  RY RITH
                    -- @SINCE 2011-04-18
                    -- @SINCE 2011-04-25 Trim meter code

                    SELECT	CUSTOMER_CODE,
		                    CUSTOMER_NAME = FIRST_NAME_KH + ' ' +LAST_NAME_KH,
		                    METER_CODE=REPLACE(LTRIM(REPLACE(m.METER_CODE,'0',' ')),' ','0') ,
		                    AREA_CODE,
		                    BOX_CODE
                    FROM TBL_METER m 
                    INNER JOIN TBL_CUSTOMER_METER cm ON m.METER_ID = cm.METER_ID and cm.IS_ACTIVE=1
                    INNER JOIN TBL_BOX b ON b.BOX_ID = cm.BOX_ID
                    INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = cm.CUSTOMER_ID
                    INNER JOIN TBL_AREA a ON a.AREA_ID = c.AREA_ID
                    WHERE m.REGISTER_CODE='';"
                    ,@"SELECT * INTO TBL_UTILITY_TMP FROM TBL_UTILITY
                    TRUNCATE TABLE TBL_UTILITY
                    INSERT INTO TBL_UTILITY VALUES(1,N'DEFAULT_CUT_OFF_DAY',N'ចំនួនថ្ងៃដែលអតិថិជនអាចជំពាក់បានមុនពេលផ្តាច់',N'1');
                    INSERT INTO TBL_UTILITY VALUES(2,N'INVOICE_DUE_DATE',N'ចំនួនថ្ងៃផុតកំណត់បង់ប្រាក់',N'10');
                    INSERT INTO TBL_UTILITY VALUES(3,N'CUT_OFF_AMOUNT',N'ទឹកប្រាក់ដែលអតិថិជនអាចជំពាក់មុនពេលផ្តាច់ចរន្ត',N'0');
                    INSERT INTO TBL_UTILITY VALUES(4,N'INVOICE_START_NUMBER',N'លេខចាប់ផ្តើមវិក្ក័យបត្រ័',N'1');
                    INSERT INTO TBL_UTILITY VALUES(5,N'INVOICE_DIGIT',N'ចំនួនខ្ទង់វិក្ក័យបត្រ័',N'10');
                    INSERT INTO TBL_UTILITY VALUES(6,N'INVOICE_HEADER',N'ក្បាលវិក័យប័ត្រ',N'INV');
                    INSERT INTO TBL_UTILITY VALUES(7,N'INVOICE_DISPLAY_YEAR',N'បង្ហាញឆ្នាំរបស់វិក័យប័ត្រ',N'False');
                    INSERT INTO TBL_UTILITY VALUES(8,N'PAYMENT_START_NUMBER',N'លេខចាប់ផ្តើម',N'1');
                    INSERT INTO TBL_UTILITY VALUES(9,N'PAYMENT_DIGIT',N'ចំនួនខ្ទង់',N'6');
                    INSERT INTO TBL_UTILITY VALUES(10,N'PAYMENT_HEADER',N'ក្បាលបង្កាន់ដៃបង់ប្រាក់',N'RPT');
                    INSERT INTO TBL_UTILITY VALUES(11,N'PAYMENT_DISPLAY_YEAR',N'បង្ហាញឆ្នាំរបស់បង្កាន់ដៃបង់ប្រាក់',N'False');
                    INSERT INTO TBL_UTILITY VALUES(12,N'DEPOSIT_START_NUMBER',N'លេខចាប់ផ្តើមបង្កាន់ដៃកក់ប្រាក់',N'1');
                    INSERT INTO TBL_UTILITY VALUES(13,N'DEPOSIT_DIGIT',N'ចំនួនខ្ទង់បង្កាន់ដៃកក់ប្រាក់',N'7');
                    INSERT INTO TBL_UTILITY VALUES(14,N'DEPOSIT_HEADER',N'ក្បាលបង្កាន់កក់ប្រាក់',N'DEP');
                    INSERT INTO TBL_UTILITY VALUES(15,N'DEPOSIT_DISPLAY_YEAR',N'បង្ហាញឆ្នាំរបស់បង្កាន់ដៃកក់ប្រាក់',N'False');
                    INSERT INTO TBL_UTILITY VALUES(16,N'MAX_DAY_TO_IGNORE_FIXED_AMOUNT',N' ',N'10');
                    INSERT INTO TBL_UTILITY VALUES(17,N'BUYPOWER_HEADER',N'ក្បាលបង្កាន់ដៃទិញអគ្គិសនី',N'BUY');
                    INSERT INTO TBL_UTILITY VALUES(18,N'BUYPOWER_DIGIT',N'ចំនួនខ្ទង់បង្កាន់ដៃទិញអគ្គិសនី',N'8');
                    INSERT INTO TBL_UTILITY VALUES(19,N'BUYPOWER_START_NUMBER',N'លេខចាប់ផ្តើមបង្កាន់ដៃទិញអគ្គិសនី',N'1');
                    INSERT INTO TBL_UTILITY VALUES(20,N'BUYPOWER_DISPLAY_YEAR',N'បង្ហាញឆ្នាំរបស់បង្កាន់ដៃទិញអគ្គិសនី',N'False');
                    INSERT INTO TBL_UTILITY VALUES(21,N'DEPOSIT_IS_PAID_AUTO ',N'ការកក់ប្រាក់បង់ដោយស្វ័យប្រវត្តិ',N'False');
                    INSERT INTO TBL_UTILITY VALUES(22,N'DEPOSIT_IS_IGNORED',N'ពុំគិតពីការកក់ប្រាក់',N'False');
                    INSERT INTO TBL_UTILITY VALUES(23,N'ALARM_QTY',N'ព័ត៌មានថាមពលប្រើប្រាស់ជិតអស់',N'1');
                    INSERT INTO TBL_UTILITY VALUES(24,N'VERSION',N'EPower client version.',N'1.0.0.0');

                    UPDATE TBL_UTILITY
                    SET UTILITY_VALUE =UT.UTILITY_VALUE
                    FROM TBL_UTILITY U
                    INNER JOIN TBL_UTILITY_TMP UT ON U.UTILITY_NAME=UT.UTILITY_NAME"
                    ,
                    "DROP TABLE TBL_UTILITY_TMP"
                    ,
                    @"
                    IF OBJECT_ID('REPORT_CUSTOMERS') IS NOT NULL
	                    DROP PROC REPORT_CUSTOMERS;
                    ",@"
                    CREATE PROC [dbo].[REPORT_CUSTOMERS]
	                    @AREA_ID INT=0,
	                    @PRICE_ID INT=0,
	                    @CYCLE_ID INT=0,
	                    @CUSTOMER_TYPE_ID INT=0,
	                    @MONTH DATETIME='2011-3-1'
                    AS
                    -- @SINCE 2011-03-30 Update customer as reading list
                    -- @SINCE 2011-04-20 Trim Meter Code.
                    SELECT  CUSTOMER_CODE = c.CUSTOMER_CODE,
		                    CUSTOMER_NAME = c.LAST_NAME_KH+' '+c.FIRST_NAME_KH+' '+c.FIRST_NAME+' '+c.LAST_NAME,
		                    PHONE = c.PHONE_1+' '+c.PHONE_2,
		                    a.AREA_ID, 
		                    AREA_NAME = ISNULL(a.AREA_NAME,'-'),
		                    POLE_NAME = ISNULL(p.POLE_CODE,'-'),
		                    BOX_CODE = ISNULL(b.BOX_CODE,'-'),
		                    METER_CODE =REPLACE(LTRIM(REPLACE(ISNULL(m.METER_CODE,'-'),'0',' ')),' ','0'),
		                    START_USAGE = COALESCE(u.START_USAGE,o.END_USAGE,0),
		                    END_USAGE = COALESCE(u.END_USAGE,0)
                    FROM TBL_CUSTOMER c 
                    LEFT JOIN TBL_CUSTOMER_METER cm ON cm.CUSTOMER_ID=c.CUSTOMER_ID AND cm.IS_ACTIVE=1
                    LEFT JOIN TBL_METER m ON cm.METER_ID=m.METER_ID
                    LEFT JOIN TBL_AREA a ON a.AREA_ID=c.AREA_ID
                    LEFT JOIN TBL_POLE p ON p.POLE_ID=cm.POLE_ID
                    LEFT JOIN TBL_BOX b ON b.BOX_ID=cm.BOX_ID
                    OUTER APPLY (
	                    SELECT TOP 1 START_USAGE,END_USAGE
	                    FROM TBL_USAGE 
	                    WHERE CUSTOMER_ID=c.CUSTOMER_ID AND USAGE_MONTH=@MONTH
	                    ORDER BY USAGE_ID DESC
                    ) u
                    OUTER APPLY (
	                    SELECT TOP 1 START_USAGE,END_USAGE
	                    FROM TBL_USAGE 
	                    WHERE CUSTOMER_ID=c.CUSTOMER_ID AND USAGE_MONTH=DATEADD(MONTH,-1,@MONTH)
	                    ORDER BY USAGE_ID DESC 
                    ) o
                    WHERE (@AREA_ID=0 OR a.AREA_ID=@AREA_ID)
	                    AND (@CYCLE_ID=0 OR c.BILLING_CYCLE_ID=@CYCLE_ID)
	                    AND (@PRICE_ID=0 OR c.PRICE_ID=@PRICE_ID)
	                    AND (@CUSTOMER_TYPE_ID=0 OR c.CUSTOMER_TYPE_ID=@CUSTOMER_TYPE_ID)
	                    AND c.STATUS_ID != 5
                    -- END OF REPORT_CUSTOMERS  
                    ",

                    @"IF OBJECT_ID ('REPORT_POWER_INCOME_YEARLY') IS NOT NULL
                        DROP PROC REPORT_POWER_INCOME_YEARLY;
                    ",
                    
                    @"
                        CREATE PROC [dbo].[REPORT_POWER_INCOME_YEARLY]
	                        @YEAR INT = 2010
                        AS
                        SELECT i.INVOICE_MONTH,
	                           t.CUSTOMER_TYPE_ID,
	                           t.CUSTOMER_TYPE_NAME,
	                           d.PRICE,
	                           AMOUNT=SUM(d.AMOUNT),
	                           USAGE=SUM(d.USAGE)
                        FROM TBL_INVOICE i
                        INNER JOIN TBL_INVOICE_DETAIL d ON i.INVOICE_ID=d.INVOICE_ID
                        INNER JOIN TBL_CUSTOMER c ON i.CUSTOMER_ID=c.CUSTOMER_ID
                        INNER JOIN TBL_CUSTOMER_TYPE t ON c.CUSTOMER_TYPE_ID=t.CUSTOMER_TYPE_ID

                        WHERE i.INVOICE_STATUS<>3 
		                        AND d.INVOICE_ITEM_ID=1 
                                AND YEAR(i.INVOICE_MONTH)=@YEAR
		                        AND i.IS_SERVICE_BILL=0
                                AND c.CUSTOMER_TYPE_ID <> -1
                        GROUP BY   i.INVOICE_MONTH,
		                           t.CUSTOMER_TYPE_ID,
		                           t.CUSTOMER_TYPE_NAME,
		                           d.PRICE
                    "
                };
                foreach (string item in strSqlStatement)
                {
                    DBDataContext.Db.ExecuteCommand(item);
                }

                // Update version to 1.0.0.1
                DBDataContext.Db.TBL_UTILITies.FirstOrDefault(row => row.UTILITY_ID == (int)Utility.VERSION).UTILITY_VALUE = "1.0.0.1";
                DBDataContext.Db.SubmitChanges(); 
                tran.Complete();
            }

        }
    }
}
