﻿using EPower.Properties;

namespace EPower.Update
{
    class Version1_2_8_0: Version
    {
        /*         
        @2021-01-05 Vonn kimputhmuyvorn
        1-Update TLKP_CUSTOMET_GROUP_TYPE
        2-Update TLKP_CUSTOMER_CONNECTION_TYPE
        3-Update TBL_PRICE
        4-Update TBL_PRICE_DETAI
        5-Update RUN_REF_02_2021
        6-Update RUN_REF_INVALID_CUSTOMER_2021
        7-Updare REPORT_REF_INVOICE
        8-Update RUN_REF_03B_2021
        9-Update RUN_REF_03A_2021
        10-Update RUN_PREPAID_CREDIT
        11-Update RUN_REF_03B_2020
        12-Update RUN_REF_03B_2019
        */

        protected override string GetBatchCommand()
        {
            return @"

UPDATE dbo.TLKP_CUSTOMER_CONNECTION_TYPE
SET CUSTOMER_CONNECTION_TYPE_NAME = N'អាជីវកម្មធុនតូច LV សាធារណៈ',
    DESCRIPTION = N'1.អាជីវកម្មធុនតូច LV សាធារណៈ'
WHERE CUSTOMER_CONNECTION_TYPE_ID = 22;

UPDATE dbo.TLKP_CUSTOMER_CONNECTION_TYPE
SET CUSTOMER_CONNECTION_TYPE_NAME = N'រដ្ឋបាល LV សាធារណៈ',
    DESCRIPTION = N'2.រដ្ឋបាល LV សាធារណៈ'
WHERE CUSTOMER_CONNECTION_TYPE_ID = 23;

UPDATE dbo.TLKP_CUSTOMER_CONNECTION_TYPE
SET CUSTOMER_CONNECTION_TYPE_NAME = N'ទិញ LV សាធារណៈ',
    DESCRIPTION = N'1.ទិញ LV សាធារណៈ'
WHERE CUSTOMER_CONNECTION_TYPE_ID = 24;

UPDATE dbo.TLKP_CUSTOMER_GROUP
SET CUSTOMER_GROUP_NAME = N'ពាណិជ្ជកម្ម រដ្ឋបាល និងសេវាកម្មផ្សេងៗ',
    DESCRIPTION = N'7.ពាណិជ្ជកម្ម រដ្ឋបាល និងសេវាកម្មផ្សេងៗ'
WHERE CUSTOMER_GROUP_ID = 7;

UPDATE dbo.TLKP_CUSTOMER_GROUP
SET CUSTOMER_GROUP_NAME = N'បូមទឹកកសិកម្មពេលយប់ ម៉ោង៩យប់ ដល់ម៉ោង៧ព្រឹក',
    DESCRIPTION = N'6.បូមទឹកកសិកម្មពេលយប់ ម៉ោង៩យប់ ដល់ម៉ោង៧ព្រឹក'
WHERE CUSTOMER_GROUP_ID = 6;

UPDATE dbo.TLKP_CUSTOMER_GROUP
SET IS_ACTIVE = 1
WHERE CUSTOMER_GROUP_ID = 5;

UPDATE dbo.TLKP_CUSTOMER_CONNECTION_TYPE
SET PRICE_ID = -30
WHERE CUSTOMER_CONNECTION_TYPE_ID = 9;

UPDATE dbo.TBL_CUSTOMER
SET CUSTOMER_CONNECTION_TYPE_ID = 9
WHERE CUSTOMER_CONNECTION_TYPE_ID IN
      (
          SELECT CUSTOMER_CONNECTION_TYPE_ID
          FROM dbo.TLKP_CUSTOMER_CONNECTION_TYPE
          WHERE CUSTOMER_CONNECTION_TYPE_ID = 135
                AND CUSTOMER_CONNECTION_TYPE_NAME = N'សាលារៀន មន្ទីរពេទ្យ និងមណ្ឌលសុខភាពនៅជនបទ'
      );

DELETE FROM dbo.TLKP_CUSTOMER_CONNECTION_TYPE
WHERE CUSTOMER_CONNECTION_TYPE_ID = 135
      AND CUSTOMER_CONNECTION_TYPE_NAME = N'សាលារៀន មន្ទីរពេទ្យ និងមណ្ឌលសុខភាពនៅជនបទ';

DELETE FROM dbo.TLKP_CUSTOMER_CONNECTION_TYPE
WHERE CUSTOMER_CONNECTION_TYPE_ID IN (921,922,923,924,925,926);

UPDATE dbo.TLKP_CUSTOMER_CONNECTION_TYPE
SET PRICE_ID=-33
WHERE CUSTOMER_CONNECTION_TYPE_ID=11;

UPDATE dbo.TLKP_CUSTOMER_CONNECTION_TYPE
SET PRICE_ID=-34
WHERE CUSTOMER_CONNECTION_TYPE_ID=12;

UPDATE dbo.TLKP_CUSTOMER_CONNECTION_TYPE
SET PRICE_ID=-35
WHERE CUSTOMER_CONNECTION_TYPE_ID=13;

IF NOT EXISTS
(
    SELECT *
    FROM dbo.TLKP_CUSTOMER_GROUP
    WHERE CUSTOMER_GROUP_ID IN ( 17, 18 )
)
BEGIN
    INSERT INTO dbo.TLKP_CUSTOMER_GROUP
    VALUES
    (17, N'ឧស្សាហកម្ម ទិញLVសាធារណៈ', N'17.ឧស្សាហកម្ម ទិញLVសាធារណៈ', 1);
    INSERT INTO dbo.TLKP_CUSTOMER_GROUP
    VALUES
    (18, N'កសិកម្ម ទិញLVសាធារណៈ', N'18.កសិកម្ម ទិញLVសាធារណៈ', 1);
END;

GO

IF NOT EXISTS (SELECT * FROM dbo.TBL_PRICE WHERE PRICE_ID IN ( -31, -32, -33, -34, -35, -36 ))
BEGIN
    SET IDENTITY_INSERT dbo.TBL_PRICE ON;


    INSERT INTO dbo.TBL_PRICE
    (
        PRICE_ID,
        PRICE_NAME,
        CREATE_ON,
        CREATE_BY,
        CURRENCY_ID,
        IS_ACTIVE,
        IS_STANDARD_RATING
    )
    VALUES
    (-31, N'ឧស្សាហកម្ម ទិញLVសាធារណៈ', GETDATE(), 'System', 1, 1, 0);

    INSERT INTO dbo.TBL_PRICE
    (
        PRICE_ID,
        PRICE_NAME,
        CREATE_ON,
        CREATE_BY,
        CURRENCY_ID,
        IS_ACTIVE,
        IS_STANDARD_RATING
    )
    VALUES
    (-32, N'កសិកម្ម ទិញLVសាធារណៈ', GETDATE(), 'System', 1, 1, 0);

	INSERT INTO dbo.TBL_PRICE
    (
        PRICE_ID,
        PRICE_NAME,
        CREATE_ON,
        CREATE_BY,
        CURRENCY_ID,
        IS_ACTIVE,
        IS_STANDARD_RATING
    )
    VALUES
    (-33, N'អ្នកកាន់អាជ្ញាបណ្ណ MV', GETDATE(), 'System', 2, 1, 0);

	INSERT INTO dbo.TBL_PRICE
    (
        PRICE_ID,
        PRICE_NAME,
        CREATE_ON,
        CREATE_BY,
        CURRENCY_ID,
        IS_ACTIVE,
        IS_STANDARD_RATING
    )
    VALUES
    (-34, N'អ្នកកាន់អាជ្ញាបណ្ណ LV (ត្រង់ស្វូអ្នកទិញ)', GETDATE(), 'System', 2, 1, 0);

	INSERT INTO dbo.TBL_PRICE
    (
        PRICE_ID,
        PRICE_NAME,
        CREATE_ON,
        CREATE_BY,
        CURRENCY_ID,
        IS_ACTIVE,
        IS_STANDARD_RATING
    )
    VALUES
    (-35, N'អ្នកកាន់អាជ្ញាបណ្ណ LV (ត្រង់ស្វូអ្នកលក់)', GETDATE(), 'System', 2, 1, 0);

	INSERT INTO dbo.TBL_PRICE
    (
        PRICE_ID,
        PRICE_NAME,
        CREATE_ON,
        CREATE_BY,
        CURRENCY_ID,
        IS_ACTIVE,
        IS_STANDARD_RATING
    )
    VALUES
    (-36, N'អាជីវករតូបលក់ដូរក្នុងផ្សារ', GETDATE(), 'System', 1, 1, 0);

    SET IDENTITY_INSERT dbo.TBL_PRICE OFF;
END;

GO

IF NOT EXISTS
(
    SELECT *
    FROM dbo.TBL_PRICE_DETAIL
    WHERE PRICE_ID IN ( -31, 32, -33, -34, -35, -36 )
)
BEGIN

    INSERT INTO dbo.TBL_PRICE_DETAIL
    VALUES
    (-31, 0, -1, 730, 0, 0);
    INSERT INTO dbo.TBL_PRICE_DETAIL
    VALUES
    (-32, 0, -1, 730, 0, 0);
	INSERT INTO dbo.TBL_PRICE_DETAIL
    VALUES
    (-33, 0, -1,  0.12900, 0, 0);
    INSERT INTO dbo.TBL_PRICE_DETAIL
    VALUES
    (-34, 0, -1,  0.13416, 0, 0);
	INSERT INTO dbo.TBL_PRICE_DETAIL
    VALUES
    (-35, 0, -1, 0.14216, 0, 0);
	INSERT INTO dbo.TBL_PRICE_DETAIL
    VALUES
    (-36, 0, -1, 730, 0, 0);
END;

GO

IF NOT EXISTS
(
    SELECT *
    FROM dbo.TLKP_CUSTOMER_CONNECTION_TYPE
    WHERE CUSTOMER_CONNECTION_TYPE_ID IN ( 135, 136, 137, 138, 139, 140, 141, 142, 143, 144 )
)
BEGIN

    INSERT INTO dbo.TLKP_CUSTOMER_CONNECTION_TYPE
    VALUES
    (135, N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV សាធារណៈ', N'1.ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញLV សាធារណៈ', 1, 730, 17, -31);

    INSERT INTO dbo.TLKP_CUSTOMER_CONNECTION_TYPE
    VALUES
    (136, N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV សាធារណៈ', N'2.ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញLV សាធារណៈ', 1, 730, 17, -31);

    INSERT INTO dbo.TLKP_CUSTOMER_CONNECTION_TYPE
    VALUES
    (137, N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV សាធារណៈ', N'3.ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញLV សាធារណៈ', 1, 730, 17, -31);

    INSERT INTO dbo.TLKP_CUSTOMER_CONNECTION_TYPE
    VALUES
    (138, N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV សាធារណៈ', N'1.កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញLV សាធារណៈ',
     1  , 730, 18, -32);

    INSERT INTO dbo.TLKP_CUSTOMER_CONNECTION_TYPE
    VALUES
    (139, N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV សាធារណៈ', N'2.សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញLV សាធារណៈ', 1, 730, 18,
     -32);

    INSERT INTO dbo.TLKP_CUSTOMER_CONNECTION_TYPE
    VALUES
    (140, N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV សាធារណៈ', N'3.សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញLV សាធារណៈ', 1, 730, 18,
     -32);
    INSERT INTO dbo.TLKP_CUSTOMER_CONNECTION_TYPE
    VALUES
    (141, N'ទិញ MV', N'2.បូមទឹកកសិកម្មពេលយប់ ម៉ោង៩យប់ ដល់ម៉ោង៧ព្រឹក ទិញMV', 1, 730, 6, -29);

    INSERT INTO dbo.TLKP_CUSTOMER_CONNECTION_TYPE
    VALUES
    (142, N'ទិញ LV ត្រង់ស្វូអ្នកលក់', N'3.បូមទឹកកសិកម្មពេលយប់ ម៉ោង៩យប់ ដល់ម៉ោង៧ព្រឹក ទិញLV ត្រង់ស្វូអ្នកលក់', 1, 730,
     6  , -29);

    INSERT INTO dbo.TLKP_CUSTOMER_CONNECTION_TYPE
    VALUES
    (143, N'ទិញ LV ត្រង់ស្វូអ្នកទិញ', N'3.សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញLV សាធារណៈ', 1, 730, 6, -29);

	INSERT INTO dbo.TLKP_CUSTOMER_CONNECTION_TYPE
    VALUES
    (144, N'អាជីវករតូបលក់ដូរក្នុងផ្សារ', N'4.អាជីវករតូបលក់ដូរក្នុងផ្សារ', 1, 730, 7, -36);

END;

GO

IF OBJECT_ID('RUN_REF_02_2021') IS NOT NULL
    DROP PROC RUN_REF_02_2021;
GO

CREATE PROC [dbo].[RUN_REF_02_2021]
    @DATA_MONTH DATETIME = '2021-01-01',
    @BILLING_CYCLE_ID INT = 0,
    @AREA_ID INT = 0
AS

/*
	@2018-09-19 Pov Lyhorng
		fix non subsidy for Organization customer
	@2019-01-16 Phoung Sovathvong
		Update REF 2019 split customer INDUSTRY, COMMERCIAL, SPECIAL
	@2019-02-22 Phoung Sovathvong
		Fix Incorrect word : រដ្ឋាបាល => រដ្ឋបាល
	@2019-12-03 Hor Dara
		Update REF 2020
	@2020-07-28 Hor Dara
		Update REF 2020 market vendor get subsudy even usage over 2000 
	@2021-01-08 Vonn Kimputhmunyvorn
		Update REF 2021
*/

-- CLEAR RESULT
DELETE FROM TBL_REF_02
WHERE DATA_MONTH = @DATA_MONTH;
DECLARE @LICENSE_TYPE_ID INT,
        @TYPE_LICENSEE NVARCHAR(200),
        @TYPE_LICENSEE_MV NVARCHAR(200),
        @TYPE_LICENSEE_MV_LV_CUSTOMER_TRANSFO NVARCHAR(200),
        @TYPE_LICENSEE_MV_LV_LICENSEE_TRANSFO NVARCHAR(200),

        --1.​ ឧស្សាហកម្មវិស័យរុករករ៉ែ
        @TYPE_MINING_INDUSTRY NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV_SALE_LV NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV_SUN NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV_SUN_SALE_LV NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV NVARCHAR(200),

        --2.​ ឧស្សាហកម្មវិស័យកម្មន្តសាល 
        @TYPE_MANUFACTURING_INDUSTRY NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV_SALE_LV NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV_SUN NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV_SUN_SALE_LV NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV NVARCHAR(200),

        --3.​ ឧស្សាហកម្មវិស័យវាយនភណ្ឌ
        @TYPE_TEXTILE_INDUSTRY NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV_SALE_LV NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV_SUN NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV_SUN_SALE_LV NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV NVARCHAR(200),

        --4.​ កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ
        @TYPE_AGRICULTURE NVARCHAR(200),
        @TYPE_AGRICULTURE_MV NVARCHAR(200),
        @TYPE_AGRICULTURE_MV_SALE_LV NVARCHAR(200),
        @TYPE_AGRICULTURE_MV_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT NVARCHAR(200),
        @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT_SALE_LV NVARCHAR(200),
        @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING NVARCHAR(200),
        @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING_SALE_LV NVARCHAR(200),
        @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_AGRICULTURE_MV_SUN NVARCHAR(200),
        @TYPE_AGRICULTURE_MV_SUN_SALE_LV NVARCHAR(200),
        @TYPE_AGRICULTURE_MV_SUN_CUSTOMER_BUY_LV NVARCHAR(200),

        --5.​ សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។)
        @TYPE_ACTIVITIES NVARCHAR(200),
        @TYPE_ACTIVITIES_MV NVARCHAR(200),
        @TYPE_ACTIVITIES_MV_SALE_LV NVARCHAR(200),
        @TYPE_ACTIVITIES_MV_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT NVARCHAR(200),
        @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT_SALE_LV NVARCHAR(200),
        @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING NVARCHAR(200),
        @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING_SALE_LV NVARCHAR(200),
        @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_ACTIVITIES_MV_SUN NVARCHAR(200),
        @TYPE_ACTIVITIES_MV_SUN_SALE_LV NVARCHAR(200),
        @TYPE_ACTIVITIES_MV_SUN_CUSTOMER_BUY_LV NVARCHAR(200),


        --6.​ សកម្មភាពកែច្នៃផលិតផលកសិកម្ម
        @TYPE_AGRICULTURAL NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV_SALE_LV NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT_SALE_LV NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING_SALE_LV NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV_SUN NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV_SUN_SALE_LV NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV_SUN_CUSTOMER_BUY_LV NVARCHAR(200),

        --1.ពាណិជ្ជកម្ម 
        @TYPE_BUSINESS NVARCHAR(200),
        @TYPE_BUSINESS_MV NVARCHAR(200),
        @TYPE_BUSINESS_MV_SALE_LV NVARCHAR(200),
        @TYPE_BUSINESS_MV_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT NVARCHAR(200),
        @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT_SALE_LV NVARCHAR(200),
        @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING NVARCHAR(200),
        @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING_SALE_LV NVARCHAR(200),
        @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_BUSINESS_MV_SUN NVARCHAR(200),
        @TYPE_BUSINESS_MV_SUN_SALE_LV NVARCHAR(200),
        @TYPE_BUSINESS_MV_SUN_CUSTOMER_BUY_LV NVARCHAR(200),

        --2.រដ្ឋបាលសាធារណៈ 
        @TYPE_PUBLIC_ADMINISTRATION NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_SALE_LV NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT_SALE_LV NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING_SALE_LV NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_SUN NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_SUN_SALE_LV NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_SUN_CUSTOMER_BUY_LV NVARCHAR(200),

        --3.សេវាកម្មផ្សេងៗ 
        @TYPE_OTHER_SERVICES NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV_SALE_LV NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT_SALE_LV NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING_SALE_LV NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV_SUN NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV_SUN_SALE_LV NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV_SUN_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_CUSTOMER_SPECIAl_FAVOR NVARCHAR(200),
        @TYPE_INDUSTRY_NO_SUBSIDY NVARCHAR(200),
        @TYPE_COMMERCIAL_NO_SUBSIDY NVARCHAR(200),
        @TYPE_COMMERCIAL_NO_SUBSIDY_SALE_LV_USAGE_OVER_2000 NVARCHAR(200),
        @TYPE_COMMERCIAL_SALE_LV_USAGE_OVER_2000 NVARCHAR(200),
        @TYPE_COMMERCIAL_SALE_SCHOOL_HOSPITAL_HEALTH_CENTER_CUSTOMER NVARCHAR(200),
        @TYPE_SUBSIDY NVARCHAR(200),
        @TYPE_SUBSIDY_INDUSTRY_COMMERCIAL_SALE_LV_USAGE_UNDER_2001 NVARCHAR(200),
        @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_NORMAL NVARCHAR(200),
        @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_0_10 NVARCHAR(200),
        @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_11_50 NVARCHAR(200),
        @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_51_200 NVARCHAR(200),
        @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_OVER_200 NVARCHAR(200),
        @BASED_TARIFF DECIMAL(18, 5),
        @SUBSIDY_TARIFF DECIMAL(18, 5),
        @TYPE_SUBSIDY_SPECIAL_FOR_MARKET_VENDOR NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_MARKET_VENDOR NVARCHAR(200),
        @TYPE_CUSTOMER_AGRICULTURE NVARCHAR(200),
        @TYPE_CUSTOMER_MV_AGRICULTURE NVARCHAR(200),
        @TYPE_CUSTOMER_MV_AGRICULTURE_MEDIUM NVARCHAR(200),
        @TYPE_CUSTOMER_MV_AGRICULTURE_MEDIUM_9_TO_7 NVARCHAR(200),
        @TYPE_CUSTOMER_LV_AGRICULTURE_LICENSE_BUY NVARCHAR(200),
        @TYPE_CUSTOMER_LV_AGRICULTURE_LICENSE_BUY_MEDIUM NVARCHAR(200),
        @TYPE_CUSTOMER_LV_AGRICULTURE_LICENSE_BUY_MEDIUM_9_TO_7 NVARCHAR(200),
        @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY NVARCHAR(200),
        @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY_MEDIUM NVARCHAR(200),
        @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY_MEDIUM_9_TO_7 NVARCHAR(200),
        @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER NVARCHAR(200),
        @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_GENERAL NVARCHAR(200),
        @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_9_TO_7 NVARCHAR(200);

--CHECK LICENSE TYPE ID
SELECT @LICENSE_TYPE_ID = LICENSE_TYPE_ID,
       @BASED_TARIFF = BASED_TARIFF,
       @SUBSIDY_TARIFF = SUBSIDY_TARIFF
FROM dbo.TBL_TARIFF
WHERE DATA_MONTH = @DATA_MONTH;
SET @TYPE_LICENSEE = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ';
SET @TYPE_LICENSEE_MV = N'1. ថាមពលលក់ភ្ជាប់ពីខ្សែបណ្តាញតង់ស្យុងមធ្យម MV';
SET @TYPE_LICENSEE_MV_LV_CUSTOMER_TRANSFO = N'2. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកទិញ)';
SET @TYPE_LICENSEE_MV_LV_LICENSEE_TRANSFO = N'3. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកលក់)';

SET @TYPE_INDUSTRY_NO_SUBSIDY
    = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (អ្នកប្រើប្រាស់ធុនធំ និងធុនមធ្យម ប្រភេទ ឧស្សាហកម្ម និងកសិកម្ម)';
SET @TYPE_MINING_INDUSTRY = N'1.​ ឧស្សាហកម្មវិស័យរុករករ៉ែ';
SET @TYPE_MINING_INDUSTRY_MV = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ MV';
SET @TYPE_MINING_INDUSTRY_MV_SALE_LV = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_MINING_INDUSTRY_MV_CUSTOMER_BUY_LV = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)';
SET @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV
    = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV
    = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)';
SET @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV
    = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV
    = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_MINING_INDUSTRY_MV_SUN = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV';
SET @TYPE_MINING_INDUSTRY_MV_SUN_SALE_LV
    = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_MINING_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV
    = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ';

SET @TYPE_MANUFACTURING_INDUSTRY = N'2.​ ឧស្សាហកម្មវិស័យកម្មន្តសាល';
SET @TYPE_MANUFACTURING_INDUSTRY_MV = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ MV';
SET @TYPE_MANUFACTURING_INDUSTRY_MV_SALE_LV = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_MANUFACTURING_INDUSTRY_MV_CUSTOMER_BUY_LV = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT
    = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)';
SET @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV
    = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV
    = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING
    = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)';
SET @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV
    = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV
    = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_MANUFACTURING_INDUSTRY_MV_SUN = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV';
SET @TYPE_MANUFACTURING_INDUSTRY_MV_SUN_SALE_LV
    = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_MANUFACTURING_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV
    = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ';

SET @TYPE_TEXTILE_INDUSTRY = N'3.​ ឧស្សាហកម្មវិស័យវាយនភណ្ឌ';
SET @TYPE_TEXTILE_INDUSTRY_MV = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ MV';
SET @TYPE_TEXTILE_INDUSTRY_MV_SALE_LV = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_TEXTILE_INDUSTRY_MV_CUSTOMER_BUY_LV = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)';
SET @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV
    = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV
    = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)';
SET @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV
    = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV
    = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_TEXTILE_INDUSTRY_MV_SUN = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV';
SET @TYPE_TEXTILE_INDUSTRY_MV_SUN_SALE_LV
    = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_TEXTILE_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV
    = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ';

SET @TYPE_AGRICULTURE = N'4.​ កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ';
SET @TYPE_AGRICULTURE_MV = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ MV';
SET @TYPE_AGRICULTURE_MV_SALE_LV = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_AGRICULTURE_MV_CUSTOMER_BUY_LV = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT
    = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)';
SET @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT_SALE_LV
    = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV
    = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING
    = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)';
SET @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING_SALE_LV
    = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV
    = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_AGRICULTURE_MV_SUN = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV';
SET @TYPE_AGRICULTURE_MV_SUN_SALE_LV
    = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_AGRICULTURE_MV_SUN_CUSTOMER_BUY_LV
    = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ';

SET @TYPE_ACTIVITIES = N'5.​ សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ។ល។)';
SET @TYPE_ACTIVITIES_MV = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ MV';
SET @TYPE_ACTIVITIES_MV_SALE_LV = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_ACTIVITIES_MV_CUSTOMER_BUY_LV = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)';
SET @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT_SALE_LV
    = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV
    = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)';
SET @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING_SALE_LV
    = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV
    = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_ACTIVITIES_MV_SUN = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV';
SET @TYPE_ACTIVITIES_MV_SUN_SALE_LV
    = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_ACTIVITIES_MV_SUN_CUSTOMER_BUY_LV
    = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ';

SET @TYPE_AGRICULTURAL = N'6.​ សកម្មភាពកែច្នៃផលិតផលកសិកម្ម';
SET @TYPE_AGRICULTURAL_MV = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ MV';
SET @TYPE_AGRICULTURAL_MV_SALE_LV = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_AGRICULTURAL_MV_CUSTOMER_BUY_LV = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)';
SET @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT_SALE_LV
    = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV
    = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)';
SET @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING_SALE_LV
    = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV
    = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_AGRICULTURAL_MV_SUN = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV';
SET @TYPE_AGRICULTURAL_MV_SUN_SALE_LV
    = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_AGRICULTURAL_MV_SUN_CUSTOMER_BUY_LV
    = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ';


SET @TYPE_COMMERCIAL_NO_SUBSIDY
    = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (អ្នកប្រើប្រាស់ធុនធំ និងធុនមធ្យម ប្រភេទពាណិជ្ជកម្ម រដ្ឋបាល និងផ្សេងៗ)';
SET @TYPE_BUSINESS = N'1.​ ពាណិជ្ជកម្ម';
SET @TYPE_BUSINESS_MV = N'	- ពាណិជ្ជកម្ម ទិញ MV';
SET @TYPE_BUSINESS_MV_SALE_LV = N'	- ពាណិជ្ជកម្ម ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_BUSINESS_MV_CUSTOMER_BUY_LV = N'	- ពាណិជ្ជកម្ម ទិញ LV ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT = N'	- ពាណិជ្ជកម្ម ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)';
SET @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT_SALE_LV
    = N'	- ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV
    = N'	- ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING = N'	- ពាណិជ្ជកម្ម ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)';
SET @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING_SALE_LV
    = N'	- ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV
    = N'	- ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_BUSINESS_MV_SUN = N'	- ពាណិជ្ជកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV';
SET @TYPE_BUSINESS_MV_SUN_SALE_LV = N'	- ពាណិជ្ជកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_BUSINESS_MV_SUN_CUSTOMER_BUY_LV
    = N'	- ពាណិជ្ជកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ';

SET @TYPE_PUBLIC_ADMINISTRATION = N'2.​ រដ្ឋបាលសាធារណៈ';
SET @TYPE_PUBLIC_ADMINISTRATION_MV = N'	- រដ្ឋបាលសាធារណៈ ទិញ MV';
SET @TYPE_PUBLIC_ADMINISTRATION_MV_SALE_LV = N'	- រដ្ឋបាលសាធារណៈ ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_PUBLIC_ADMINISTRATION_MV_CUSTOMER_BUY_LV = N'	- រដ្ឋបាលសាធារណៈ ទិញ LV ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT = N'	- រដ្ឋបាលសាធារណៈ ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)';
SET @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT_SALE_LV
    = N'	- រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV
    = N'	- រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING = N'	- រដ្ឋបាលសាធារណៈ ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)';
SET @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING_SALE_LV
    = N'	- រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV
    = N'	- រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_PUBLIC_ADMINISTRATION_MV_SUN = N'	- រដ្ឋបាលសាធារណៈ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV';
SET @TYPE_PUBLIC_ADMINISTRATION_MV_SUN_SALE_LV
    = N'	- រដ្ឋបាលសាធារណៈ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_PUBLIC_ADMINISTRATION_MV_SUN_CUSTOMER_BUY_LV
    = N'	- រដ្ឋបាលសាធារណៈ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ';

SET @TYPE_OTHER_SERVICES = N'3.​ សេវាកម្មផ្សេងៗ';
SET @TYPE_OTHER_SERVICES_MV = N'	- សេវាកម្មផ្សេងៗ ទិញ MV';
SET @TYPE_OTHER_SERVICES_MV_SALE_LV = N'	- សេវាកម្មផ្សេងៗ ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_OTHER_SERVICES_MV_CUSTOMER_BUY_LV = N'	- សេវាកម្មផ្សេងៗ ទិញ LV ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT = N'	- សេវាកម្មផ្សេងៗ ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)';
SET @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT_SALE_LV
    = N'	- សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV
    = N'	- សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING = N'	- សេវាកម្មផ្សេងៗ ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)';
SET @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING_SALE_LV
    = N'	- សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV
    = N'	- សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_OTHER_SERVICES_MV_SUN = N'	- សេវាកម្មផ្សេងៗ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV';
SET @TYPE_OTHER_SERVICES_MV_SUN_SALE_LV
    = N'	- សេវាកម្មផ្សេងៗ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_OTHER_SERVICES_MV_SUN_CUSTOMER_BUY_LV
    = N'	- សេវាកម្មផ្សេងៗ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ';

SET @TYPE_COMMERCIAL_NO_SUBSIDY_SALE_LV_USAGE_OVER_2000
    = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែបណ្តាញសាធារណៈ)';
SET @TYPE_COMMERCIAL_SALE_LV_USAGE_OVER_2000
    = N'1. អតិថិជន ឧស្សាហកម្ម&កសិកម្ម, ពាណិជ្ជកម្ម រដ្ឋបាល&សេវាកម្ម ប្រើចាប់ពី 2001 kWh/ខែ ឡើង';

SET @TYPE_SUBSIDY = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)';
SET @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_NORMAL = N'1. ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន';
SET @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_0_10 = SPACE(9) + N'1.1.' + N' ថាមពលលក់ឲ្យលំនៅដ្ឋានប្រើ មិនលើសពី 10 kWh/ខែ';
SET @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_11_50 = SPACE(9) + N'1.2.' + N' ថាមពលលក់ឲ្យលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ';
SET @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_51_200
    = SPACE(9) + N'1.3.' + N' ថាមពលលក់ឲ្យលំនៅដ្ឋានប្រើចាប់ពី 51 ដល់ 200 kWh/ខែ';
SET @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_OVER_200 = SPACE(9) + N'1.4.' + N' ថាមពលលក់ឲ្យលំនៅដ្ឋានប្រើចាប់ពី 201 kWh/ខែ';
SET @TYPE_SUBSIDY_INDUSTRY_COMMERCIAL_SALE_LV_USAGE_UNDER_2001
    = N'2. អតិថិជន ឧស្សាហកម្ម&កសិកម្ម, ពាណិជ្ជកម្ម រដ្ឋបាល&សេវាកម្ម ប្រើក្រោម 2001 kWh/ខែ ';
SET @TYPE_CUSTOMER_SPECIAl_FAVOR = N'3. អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស';
SET @TYPE_COMMERCIAL_SALE_SCHOOL_HOSPITAL_HEALTH_CENTER_CUSTOMER
    = SPACE(9) + N'3.1.' + N' អតិថិជន សាលារៀន មន្ទីរពេទ្យ មណ្ឌលសុខភាព នៅតំបន់ជនបទ';
SET @TYPE_CUSTOMER_AGRICULTURE
    = N' អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម ភ្ជាប់ពីខ្សែបណ្តាញចែកចាយ MV និង LV ';
SET @TYPE_CUSTOMER_MV_AGRICULTURE = N'1. អ្នកប្រើប្រាស់ MV';
SET @TYPE_CUSTOMER_MV_AGRICULTURE_MEDIUM = N'	- ទិញ MV តាមអត្រាថ្លៃមធ្យម';
SET @TYPE_CUSTOMER_MV_AGRICULTURE_MEDIUM_9_TO_7 = N'	- ទិញ MV តាមអត្រាថ្លៃអនុគ្រោះ (ម៉ោង៩យប់ដល់៧ព្រឹក)';
SET @TYPE_CUSTOMER_LV_AGRICULTURE_LICENSE_BUY = N'2. អ្នកប្រើប្រាស់ LV (ត្រង់ស្វូរអ្នកលក់)';
SET @TYPE_CUSTOMER_LV_AGRICULTURE_LICENSE_BUY_MEDIUM = N'	- ទិញ LV ត្រង់ស្វូរអ្នកលក់ តាមអត្រាថ្លៃមធ្យម';
SET @TYPE_CUSTOMER_LV_AGRICULTURE_LICENSE_BUY_MEDIUM_9_TO_7
    = N'	- ទិញ LV ត្រង់ស្វូរអ្នកលក់ តាមអត្រាថ្លៃអនុគ្រោះ (ម៉ោង៩យប់ដល់៧ព្រឹក)';
SET @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY = N'3. អ្នកប្រើប្រាស់ LV (ត្រង់ស្វូរអ្នកទិញ)';
SET @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY_MEDIUM = N'	- ទិញ LV ត្រង់ស្វូរអ្នកទិញ តាមអត្រាថ្លៃមធ្យម';
SET @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY_MEDIUM_9_TO_7
    = N'	- ទិញ LV ត្រង់ស្វូរអ្នកទិញ តាមអត្រាថ្លៃអនុគ្រោះ (ម៉ោង៩យប់ដល់៧ព្រឹក)';
SET @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER = N'4. អ្នកប្រើប្រាស់ LV (ភ្ជាប់ពីខ្សែសាធារណៈ)';
SET @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_GENERAL = N'	- ទិញតាមអត្រាថ្លៃទូទៅ';
SET @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_9_TO_7 = N'	- ទិញតាមអត្រាថ្លៃអនុគ្រោះ (៩យប់ដល់៧ព្រឹក)';
SET @TYPE_SUBSIDY_SPECIAL_FOR_MARKET_VENDOR = N'ថាមពលលក់ឲ្យអតិថិជននៅក្នុងបរិវេណផ្សារ';
SET @TYPE_SALE_CUSTOMER_MARKET_VENDOR = N'1. ថាមពលលក់ឲ្យអតិថិជនក្នុងផ្សារ';

IF (@LICENSE_TYPE_ID = 1)
BEGIN
    -- Licensee Customer(MV)
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 6,
           TYPE_NAME = @TYPE_LICENSEE,
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = @TYPE_LICENSEE_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    INTO #RESULT
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 11
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- Licensee Customer(buyer's transformer)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 6,
           TYPE_NAME = @TYPE_LICENSEE,
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = @TYPE_LICENSEE_MV_LV_CUSTOMER_TRANSFO,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 12
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- Licensee Customer(license's transformer)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 6,
           TYPE_NAME = @TYPE_LICENSEE,
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = @TYPE_LICENSEE_MV_LV_LICENSEE_TRANSFO,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 13
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យរុករករ៉ែ 2021 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 77,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 26
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 76,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 27
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 75,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 28
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 74,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 29
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN



    --ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 73,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 30
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 72,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 31
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 71,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 32
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 70,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 33
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 69,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 34
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យរុករករ៉ែ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 68,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV_SUN,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 35
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យរុករករ៉ែ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 67,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV_SUN_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 36
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យរុករករ៉ែ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 66,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 37
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --END ឧស្សាហកម្មវិស័យរុករករ៉ែ 2021 

    --ឧស្សាហកម្មវិស័យកម្មន្តសាល 2021
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 64,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 38
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 63,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 39
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 62,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 40
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 61,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 41
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN



    --ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 60,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 42
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 59,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 43
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 58,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 44
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 57,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 45
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 56,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 46
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យកម្មន្តសាល មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 55,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV_SUN,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 47
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យកម្មន្តសាល មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 54,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV_SUN_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 48
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យកម្មន្តសាល មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 53,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 49
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- END ឧស្សាហកម្មវិស័យកម្មន្តសាល 2021

    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ 2021
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 51,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 50
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 50,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 51
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 49,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 52
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 48,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 53
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN



    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 47,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 54
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 46,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 55
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 45,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 56
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 44,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 57
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 43,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 58
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 42,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV_SUN,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 59
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 41,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV_SUN_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 60
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 40,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 61
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- END ឧស្សាហកម្មវិស័យវាយនភណ្ឌ 2021

    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ 2021
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 38,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 62
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 37,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 63
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 36,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 64
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 35,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 65
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN



    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 34,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 66
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 33,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 67
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 32,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 68
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 31,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 69
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 30,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 70
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 29,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV_SUN,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 71
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 28,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV_SUN_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 72
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 27,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV_SUN_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 73
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- END កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ 2021

    -- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) 2021
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 25,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 74
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    -- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 24,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 75
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 23,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 76
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    -- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 22,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 77
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN



    -- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 21,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 78
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 20,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 79
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 19,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 80
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 18,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 81
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 17,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 82
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 16,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV_SUN,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 83
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 15,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV_SUN_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 84
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 14,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV_SUN_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 85
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- END  សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) 2021


    -- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម 2021
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 12,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 86
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    -- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 87
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 88
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    -- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 89
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN



    -- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 90
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 91
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 92
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 93
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 94
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV_SUN,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 95
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV_SUN_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 96
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV_SUN_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 97
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- END សកម្មភាពកែច្នៃផលិតផលកសិកម្ម 2021


    -- ពាណិជ្ជកម្ម 2021
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_COMMERCIAL_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 38,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 98
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    -- ពាណិជ្ជកម្ម ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 37,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 99
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --ពាណិជ្ជកម្ម ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 36,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 100
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    -- ពាណិជ្ជកម្ម ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 35,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 101
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN



    -- ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 34,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 102
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 33,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 103
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- ពាណិជ្ជកម្ម ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 32,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 104
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 31,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 105
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 30,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 106
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- ពាណិជ្ជកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 29,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV_SUN,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 107
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- ពាណិជ្ជកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 28,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV_SUN_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 108
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- ពាណិជ្ជកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 27,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV_SUN_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 109
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- END ពាណិជ្ជកម្ម 2021


    -- រដ្ឋបាលសាធារណៈ 2021
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_COMMERCIAL_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 25,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 110
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    -- រដ្ឋបាលសាធារណៈ ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 24,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 111
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --រដ្ឋបាលសាធារណៈ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 23,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 112
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    -- រដ្ឋបាលសាធារណៈ ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 22,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 113
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN



    -- រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 21,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 114
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 20,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 115
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- រដ្ឋបាលសាធារណៈ ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 19,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 116
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 18,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 117
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 17,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 118
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- រដ្ឋបាលសាធារណៈ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 16,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_SUN,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 119
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- រដ្ឋបាលសាធារណៈ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 15,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_SUN_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 120
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- រដ្ឋបាលសាធារណៈ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 14,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_SUN_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 121
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- END រដ្ឋបាលសាធារណៈ 2021


    -- សេវាកម្មផ្សេងៗ 2021
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_COMMERCIAL_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 12,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 122
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    -- សេវាកម្មផ្សេងៗ ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 123
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --សេវាកម្មផ្សេងៗ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 124
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    -- សេវាកម្មផ្សេងៗ ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 125
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN



    -- សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 126
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 127
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សេវាកម្មផ្សេងៗ ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 128
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 129
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 130
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សេវាកម្មផ្សេងៗ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV_SUN,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 131
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សេវាកម្មផ្សេងៗ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV_SUN_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 132
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សេវាកម្មផ្សេងៗ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV_SUN_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 133
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- END សេវាកម្មផ្សេងៗ 2021

    -- for Normal Customer(TOTAL_USAGE>=2001) COMMERCIAL 2021
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 3,
           TYPE_NAME = @TYPE_COMMERCIAL_NO_SUBSIDY_SALE_LV_USAGE_OVER_2000,
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_COMMERCIAL_SALE_LV_USAGE_OVER_2000,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 22, 23, 134, 135, 136, 137, 138, 139, 140,144 )
          AND i.TOTAL_USAGE >= 2001
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Total Normal Customer
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_SUBSIDY,
           TYPE_OF_SALE_ID = -12,
           TYPE_OF_SALE_NAME = @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_NORMAL,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = BASED_PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY BASED_PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Normal Customer(TOTAL_USAGE<=10)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_SUBSIDY,
           TYPE_OF_SALE_ID = -13,
           TYPE_OF_SALE_NAME = @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_0_10,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE <= 10
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Normal Customer(TOTAL_USAGE BETWEEN 11 AND 50)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_SUBSIDY,
           TYPE_OF_SALE_ID = -14,
           TYPE_OF_SALE_NAME = @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_11_50,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND (i.TOTAL_USAGE
          BETWEEN 11 AND 50
              )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Normal Customer(TOTAL_USAGE BETWEEN 51 AND 200)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_SUBSIDY,
           TYPE_OF_SALE_ID = -15,
           TYPE_OF_SALE_NAME = @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_51_200,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND (i.TOTAL_USAGE
          BETWEEN 51 AND 200
              )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Normal Customer(TOTAL_USAGE>=200)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_SUBSIDY,
           TYPE_OF_SALE_ID = -16,
           TYPE_OF_SALE_NAME = @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_OVER_200,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE >= 201
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Normal Customer(TOTAL_USAGE<2001)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_SUBSIDY,
           TYPE_OF_SALE_ID = -17,
           TYPE_OF_SALE_NAME = @TYPE_SUBSIDY_INDUSTRY_COMMERCIAL_SALE_LV_USAGE_UNDER_2001,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = BASED_PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 22, 23, 134, 135, 136, 137, 138, 139, 140, 144 )
          AND i.TOTAL_USAGE < 2001
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY BASED_PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Normal Customer SPECIAL
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_SUBSIDY,
           TYPE_OF_SALE_ID = -19,
           TYPE_OF_SALE_NAME = @TYPE_COMMERCIAL_SALE_SCHOOL_HOSPITAL_HEALTH_CENTER_CUSTOMER,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_AGRICULTURE,
           TYPE_OF_SALE_ID = -101,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_MV_AGRICULTURE_MEDIUM,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = ROUND((PRICE * 4120), 0),
           1,
           N'រៀល',
           N'៛',
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
          AND i.PRICE = 0.1370
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_AGRICULTURE,
           TYPE_OF_SALE_ID = -102,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_MV_AGRICULTURE_MEDIUM_9_TO_7,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_AGRICULTURE,
           TYPE_OF_SALE_ID = -104,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_AGRICULTURE_LICENSE_BUY_MEDIUM,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = ROUND((PRICE * 4120), 0),
           1,
           N'រៀល',
           N'៛',
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
          AND i.PRICE = 0.15048
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_AGRICULTURE,
           TYPE_OF_SALE_ID = -105,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_AGRICULTURE_LICENSE_BUY_MEDIUM_9_TO_7,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_AGRICULTURE,
           TYPE_OF_SALE_ID = -107,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY_MEDIUM,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = ROUND((PRICE * 4120), 0),
           1,
           N'រៀល',
           N'៛',
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
          AND i.PRICE = 0.14248
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_AGRICULTURE,
           TYPE_OF_SALE_ID = -108,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY_MEDIUM_9_TO_7,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_AGRICULTURE,
           TYPE_OF_SALE_ID = -110,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_GENERAL,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
          AND i.PRICE = 730
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_AGRICULTURE,
           TYPE_OF_SALE_ID = -111,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY_MEDIUM_9_TO_7,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN;

    CREATE TABLE #DEFAULT_RESULT
    (
        DATA_MONTH DATETIME,
        TYPE_ID INT,
        TYPE_NAME NVARCHAR(200),
        TYPE_OF_SALE_ID INT,
        TYPE_OF_SALE_NAME NVARCHAR(200),
        TOTAL_CUSTOMER INT,
        TOTAL_POWER_SOLD INT,
        RATE DECIMAL(18, 4),
        CURRENCY_ID INT,
        CURRENCY_NAME NVARCHAR(20),
        CURRENCY_SIGN NVARCHAR(5),
        IS_ACTIVE BIT
    );
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 6, @TYPE_LICENSEE, 5, @TYPE_LICENSEE_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 6, @TYPE_LICENSEE, 4, @TYPE_LICENSEE_MV_LV_CUSTOMER_TRANSFO, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 6, @TYPE_LICENSEE, 3, @TYPE_LICENSEE_MV_LV_LICENSEE_TRANSFO, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 78, @TYPE_MINING_INDUSTRY, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 77, @TYPE_MINING_INDUSTRY_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 76, @TYPE_MINING_INDUSTRY_MV_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 75, @TYPE_MINING_INDUSTRY_MV_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 74, @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 73, @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 72, @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV, 0,
     0  , 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 71, @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 70, @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 69, @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV, 0,
     0  , 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 68, @TYPE_MINING_INDUSTRY_MV_SUN, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 67, @TYPE_MINING_INDUSTRY_MV_SUN_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$',
     1  );
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 66, @TYPE_MINING_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    --END ឧស្សាហកម្មវិស័យរុករករ៉ែ


    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 65, @TYPE_MANUFACTURING_INDUSTRY, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 64, @TYPE_MANUFACTURING_INDUSTRY_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 63, @TYPE_MANUFACTURING_INDUSTRY_MV_SALE_LV, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 62, @TYPE_MANUFACTURING_INDUSTRY_MV_CUSTOMER_BUY_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 61, @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 60, @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV, 0, 0,
     0  , 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 59,
     @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 58, @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 57, @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV, 0, 0,
     0  , 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 56,
     @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 55, @TYPE_MANUFACTURING_INDUSTRY_MV_SUN, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 54, @TYPE_MANUFACTURING_INDUSTRY_MV_SUN_SALE_LV, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 53, @TYPE_MANUFACTURING_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    --END ឧស្សាហកម្មវិស័យកម្មន្តសាល


    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 52, @TYPE_TEXTILE_INDUSTRY, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 51, @TYPE_TEXTILE_INDUSTRY_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 50, @TYPE_TEXTILE_INDUSTRY_MV_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 49, @TYPE_TEXTILE_INDUSTRY_MV_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 48, @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 47, @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 46, @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV, 0,
     0  , 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 45, @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 44, @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 43, @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV, 0,
     0  , 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 42, @TYPE_TEXTILE_INDUSTRY_MV_SUN, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 41, @TYPE_TEXTILE_INDUSTRY_MV_SUN_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$',
     1  );
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 40, @TYPE_TEXTILE_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    --END ឧស្សាហកម្មវិស័យវាយនភណ្ឌ


    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 39, @TYPE_AGRICULTURE, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 38, @TYPE_AGRICULTURE_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 37, @TYPE_AGRICULTURE_MV_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 36, @TYPE_AGRICULTURE_MV_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា', N'$',
     1  );
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 35, @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 34, @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 33, @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV, 0, 0, 0,
     2  , N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 32, @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 31, @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 30, @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV, 0, 0, 0,
     2  , N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 29, @TYPE_AGRICULTURE_MV_SUN, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 28, @TYPE_AGRICULTURE_MV_SUN_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 27, @TYPE_AGRICULTURE_MV_SUN_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    --END កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ


    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 26, @TYPE_ACTIVITIES, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 25, @TYPE_ACTIVITIES_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 24, @TYPE_ACTIVITIES_MV_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 23, @TYPE_ACTIVITIES_MV_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 22, @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 21, @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 20, @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV, 0, 0, 0,
     2  , N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 19, @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 18, @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 17, @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV, 0, 0, 0,
     2  , N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 16, @TYPE_ACTIVITIES_MV_SUN, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 15, @TYPE_ACTIVITIES_MV_SUN_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 14, @TYPE_ACTIVITIES_MV_SUN_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    --END សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។)



    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 13, @TYPE_AGRICULTURAL, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 12, @TYPE_AGRICULTURAL_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 11, @TYPE_AGRICULTURAL_MV_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 10, @TYPE_AGRICULTURAL_MV_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា', N'$',
     1  );
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 9, @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 8, @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 7, @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV, 0, 0, 0,
     2  , N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 6, @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 5, @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 4, @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV, 0, 0, 0,
     2  , N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 3, @TYPE_AGRICULTURAL_MV_SUN, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 2, @TYPE_AGRICULTURAL_MV_SUN_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 1, @TYPE_AGRICULTURAL_MV_SUN_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    --END សកម្មភាពកែច្នៃផលិតផលកសិកម្ម


    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 39, @TYPE_BUSINESS, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 38, @TYPE_BUSINESS_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 37, @TYPE_BUSINESS_MV_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 36, @TYPE_BUSINESS_MV_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 35, @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 34, @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 33, @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV, 0, 0, 0,
     2  , N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 32, @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 31, @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 30, @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV, 0, 0, 0,
     2  , N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 29, @TYPE_BUSINESS_MV_SUN, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 28, @TYPE_BUSINESS_MV_SUN_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 27, @TYPE_BUSINESS_MV_SUN_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    --END ពាណិជ្ជកម្ម


    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 26, @TYPE_PUBLIC_ADMINISTRATION, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 25, @TYPE_PUBLIC_ADMINISTRATION_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 24, @TYPE_PUBLIC_ADMINISTRATION_MV_SALE_LV, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 23, @TYPE_PUBLIC_ADMINISTRATION_MV_CUSTOMER_BUY_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 22, @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 21, @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT_SALE_LV, 0,
     0  , 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 20,
     @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 19, @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 18, @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING_SALE_LV, 0,
     0  , 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 17,
     @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 16, @TYPE_PUBLIC_ADMINISTRATION_MV_SUN, 0, 0, 0, 2, N'ដុល្លា', N'$',
     1  );
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 15, @TYPE_PUBLIC_ADMINISTRATION_MV_SUN_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 14, @TYPE_PUBLIC_ADMINISTRATION_MV_SUN_CUSTOMER_BUY_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    --END រដ្ឋបាលសាធារណៈ


    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 13, @TYPE_OTHER_SERVICES, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 12, @TYPE_OTHER_SERVICES_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 11, @TYPE_OTHER_SERVICES_MV_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 10, @TYPE_OTHER_SERVICES_MV_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 9, @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 8, @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 7, @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV, 0,
     0  , 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 6, @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 5, @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 4, @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV, 0,
     0  , 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 3, @TYPE_OTHER_SERVICES_MV_SUN, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 2, @TYPE_OTHER_SERVICES_MV_SUN_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$',
     1  );
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 1, @TYPE_OTHER_SERVICES_MV_SUN_CUSTOMER_BUY_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    --END សេវាកម្មផ្សេងៗ

    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 3, @TYPE_COMMERCIAL_NO_SUBSIDY_SALE_LV_USAGE_OVER_2000, 1, @TYPE_COMMERCIAL_SALE_LV_USAGE_OVER_2000,
     0  , 0, 0, 1, N'រៀល', N'៛', 1);


    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_SUBSIDY, -12, @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_NORMAL, 0, 0, 0, 1, N'រៀល', N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_SUBSIDY, -13, @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_0_10, 0, 0, 0, 1, N'រៀល', N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_SUBSIDY, -14, @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_11_50, 0, 0, 0, 1, N'រៀល', N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_SUBSIDY, -15, @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_51_200, 0, 0, 0, 1, N'រៀល', N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_SUBSIDY, -16, @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_OVER_200, 0, 0, 0, 1, N'រៀល', N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_SUBSIDY, -17, @TYPE_SUBSIDY_INDUSTRY_COMMERCIAL_SALE_LV_USAGE_UNDER_2001, 0, 0, 0, 1,
     N'រៀល', N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_SUBSIDY, -18, @TYPE_CUSTOMER_SPECIAl_FAVOR, 0, 0, 0, 1, N'រៀល', N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_SUBSIDY, -19, @TYPE_COMMERCIAL_SALE_SCHOOL_HOSPITAL_HEALTH_CENTER_CUSTOMER, 0, 0, 0, 1,
     N'រៀល', N'៛', 1);

    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -100, @TYPE_CUSTOMER_MV_AGRICULTURE, 0, 0, 0, 1, N'រៀល', N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -101, @TYPE_CUSTOMER_MV_AGRICULTURE_MEDIUM, 0, 0, 0, 1, N'រៀល', N'៛',
     1  );
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -102, @TYPE_CUSTOMER_MV_AGRICULTURE_MEDIUM_9_TO_7, 0, 0, 0, 1, N'រៀល',
     N'៛', 1);

    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -103, @TYPE_CUSTOMER_LV_AGRICULTURE_LICENSE_BUY, 0, 0, 0, 1, N'រៀល',
     N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -104, @TYPE_CUSTOMER_LV_AGRICULTURE_LICENSE_BUY_MEDIUM, 0, 0, 0, 1,
     N'រៀល', N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -105, @TYPE_CUSTOMER_LV_AGRICULTURE_LICENSE_BUY_MEDIUM_9_TO_7, 0, 0,
     0  , 1, N'រៀល', N'៛', 1);

    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -106, @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY, 0, 0, 0, 1, N'រៀល',
     N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -107, @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY_MEDIUM, 0, 0, 0, 1,
     N'រៀល', N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -108, @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY_MEDIUM_9_TO_7, 0, 0,
     0  , 1, N'រៀល', N'៛', 1);

    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -109, @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER, 0, 0, 0, 1, N'រៀល',
     N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -110, @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_GENERAL, 0, 0, 0, 1,
     N'រៀល', N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -111, @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_9_TO_7, 0, 0, 0, 1,
     N'រៀល', N'៛', 1);

    -- RESULT
    INSERT INTO TBL_REF_02
    SELECT *
    FROM #RESULT
    UNION ALL
    SELECT *
    FROM #DEFAULT_RESULT t
    WHERE NOT EXISTS
    (
        SELECT *
        FROM #RESULT
        WHERE TYPE_ID = t.TYPE_ID
              AND TYPE_OF_SALE_ID = t.TYPE_OF_SALE_ID
    )
    ORDER BY TYPE_ID DESC,
             TYPE_OF_SALE_ID DESC;
END;
ELSE IF (@LICENSE_TYPE_ID = 2)
BEGIN
    -- Market vendor customer
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_SUBSIDY_SPECIAL_FOR_MARKET_VENDOR,
           TYPE_OF_SALE_ID = -1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_MARKET_VENDOR,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    INTO #RESULT_MARKET_VENDOR
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 144 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN;
    CREATE TABLE #DEFAULT_RESULT_MARKET_VENDOR
    (
        DATA_MONTH DATETIME,
        TYPE_ID INT,
        TYPE_NAME NVARCHAR(200),
        TYPE_OF_SALE_ID INT,
        TYPE_OF_SALE_NAME NVARCHAR(200),
        TOTAL_CUSTOMER INT,
        TOTAL_POWER_SOLD INT,
        RATE DECIMAL(18, 4),
        CURRENCY_ID INT,
        CURRENCY_NAME NVARCHAR(20),
        CURRENCY_SIGN NVARCHAR(5),
        IS_ACTIVE BIT
    );
    INSERT INTO #DEFAULT_RESULT_MARKET_VENDOR
    VALUES
    (@DATA_MONTH, 1, @TYPE_SUBSIDY_SPECIAL_FOR_MARKET_VENDOR, -1, @TYPE_SALE_CUSTOMER_MARKET_VENDOR, 0, 0, 0, 1, N'រៀល', N'៛', 1);

    -- RESULT
    INSERT INTO TBL_REF_02
    SELECT *
    FROM #RESULT_MARKET_VENDOR
    UNION ALL
    SELECT *
    FROM #DEFAULT_RESULT_MARKET_VENDOR t
    WHERE NOT EXISTS
    (
        SELECT *
        FROM #RESULT_MARKET_VENDOR
        WHERE TYPE_ID = t.TYPE_ID
              AND TYPE_OF_SALE_ID = t.TYPE_OF_SALE_ID
    )
    ORDER BY TYPE_ID DESC,
             TYPE_OF_SALE_ID DESC;
END;
--END OF RUN_REF_02_2021

GO

IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'TBL_REF_03B'
          AND COLUMN_NAME = 'TYPE_ID'
)
    ALTER TABLE dbo.TBL_REF_03B ADD TYPE_ID INT DEFAULT 0 NOT NULL;
GO

IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'TBL_REF_03B'
          AND COLUMN_NAME = 'TYPE_NAME'
)
    ALTER TABLE dbo.TBL_REF_03B ADD TYPE_NAME NVARCHAR(200) DEFAULT NULL;
GO

IF OBJECT_ID('RUN_REF_03B_2021') IS NOT NULL
    DROP PROC RUN_REF_03B_2021;
GO

CREATE PROC RUN_REF_03B_2021
    @DATA_MONTH DATETIME = '2021-01-01',
    @BILLING_CYCLE_ID INT = 0,
    @AREA_ID INT = 0
AS
-- CLEAR DATA
DELETE FROM TBL_REF_03B
WHERE DATA_MONTH = @DATA_MONTH;

DECLARE @TYPE_SALE_CUSTOMER_USAGE_0_10 NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_USAGE_11_50 NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_USAGE_51_200 NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_SCHOOL NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_AGRI NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_SPECIAL NVARCHAR(200),
        @LICENSE_TYPE_ID INT,
        @BASED_TARIFF DECIMAL(18, 5),
        @SUBSIDY_TARIFF DECIMAL(18, 5),
        @TYPE_CUSTOMER_NORMAL NVARCHAR(200),
        @TYPE_CUSTOMER_FAVOR NVARCHAR(200),
        @TYPE_CUSTOMER_MV NVARCHAR(200),
        @TYPE_CUSTOMER_LV_LICENSE NVARCHAR(200),
        @TYPE_CUSTOMER_LV_CUSTOMER_BUY NVARCHAR(200),
        @TYPE_CUSTOMER_LV_GENERAL NVARCHAR(200);
SET @TYPE_CUSTOMER_NORMAL = N'  1' + SPACE(4) + N'អតិថិជនលំនៅដ្ឋាន';
SET @TYPE_CUSTOMER_FAVOR = N'  2' + SPACE(4) + N'អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស';
SET @TYPE_SALE_CUSTOMER_USAGE_0_10 = N'1.1' + N' លំនៅដ្ឋានប្រើ មិនលើស  10 kWh/ខែ';
SET @TYPE_SALE_CUSTOMER_USAGE_11_50 = N'1.2' + N' លលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ';
SET @TYPE_SALE_CUSTOMER_USAGE_51_200 = N'1.3' + N' លំនៅដ្ឋានប្រើចាប់ពី 51 ដល់ 200 kWh/ខែ';
SET @TYPE_SALE_CUSTOMER_SCHOOL = N'2.1' + N' សាលារៀន, មន្ទីរពេទ្យ, និងមណ្ឌលសុខភាព នៅតំបន់ជនបទ';
SET @TYPE_SALE_CUSTOMER_AGRI = N'2.2' + N' អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម';
SET @TYPE_CUSTOMER_MV = N'  2.2.1' + N'  អ្នកប្រើប្រាស់ MV';
SET @TYPE_CUSTOMER_LV_LICENSE = N'  2.2.2' + N'  អ្នកប្រើប្រាស់ LV (ត្រង់ស្វូរអ្នកលក់)';
SET @TYPE_CUSTOMER_LV_CUSTOMER_BUY = N'  2.2.3' + N'  អ្នកប្រើប្រាស់ LV (ត្រង់ស្វូរអ្នកទិញ)';
SET @TYPE_CUSTOMER_LV_GENERAL = N'  2.2.4' + N'  អ្នកប្រើប្រាស់ LV (ភ្ជាប់ពីខ្សែសាធារណៈ)';

SELECT @LICENSE_TYPE_ID = LICENSE_TYPE_ID,
       @SUBSIDY_TARIFF = SUBSIDY_TARIFF,
       @BASED_TARIFF = BASED_TARIFF
FROM dbo.TBL_TARIFF
WHERE DATA_MONTH = @DATA_MONTH;

IF (@LICENSE_TYPE_ID <> 2)
BEGIN
    INSERT INTO TBL_REF_03B
    (
        DATA_MONTH,
        ROW_NO,
        TYPE_OF_SALE_ID,
        TYPE_OF_SALE_NAME,
        TOTAL_CUSTOMER,
        TOTAL_POWER_SOLD,
        PRICE,
        SUBSIDY_RATE,
        SUBSIDY_AMOUNT,
        CURRENCY_ID,
        CURRENCY_NAME,
        CURRENCY_SIGN,
        IS_ACTIVE,
        TYPE_ID,
        TYPE_NAME
    )
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1.1',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_0_10,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_NORMAL
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE <= 10
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1.2',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_11_50,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_NORMAL
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND (i.TOTAL_USAGE
          BETWEEN 11 AND 50
              )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1.3',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_51_200,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_NORMAL
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND (i.TOTAL_USAGE
          BETWEEN 51 AND 200
              )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.1',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_SCHOOL,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_AGRI,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.1',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(0.1370 * 4120, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(ROUND(0.1370 * 4120, 0), 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.2',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_LICENSE,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(0.15048 * 4120, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(ROUND(0.15048 * 4120, 0), 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.3',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_CUSTOMER_BUY,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(0.14248 * 4120, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(ROUND(0.14248 * 4120, 0), 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.4',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_GENERAL,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(730, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(730, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN

    --WHEN BASE TARFF < SUBSIDY TARIFF
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1.1',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_0_10,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.BASED_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.BASED_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_NORMAL
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE <= 10
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1.2',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_11_50,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.BASED_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.BASED_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_NORMAL
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND (i.TOTAL_USAGE
          BETWEEN 11 AND 50
              )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1.3',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_51_200,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.BASED_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.BASED_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_NORMAL
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND (i.TOTAL_USAGE
          BETWEEN 51 AND 200
              )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.1',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_SCHOOL,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.BASED_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.BASED_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_AGRI,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.BASED_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.BASED_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND (i.TOTAL_USAGE
          BETWEEN 51 AND 200
              )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.1',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(0.1370 * 4120, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(ROUND(0.1370 * 4120, 0), 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.2',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_LICENSE,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(0.15048 * 4120, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(ROUND(0.15048 * 4120, 0), 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.3',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_CUSTOMER_BUY,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(0.14248 * 4120, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(ROUND(0.14248 * 4120, 0), 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.4',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_GENERAL,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(730, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(730, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN;

    -- BLANK RECORD
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '1.1'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'1.1',
               TYPE_OF_SALE_ID = 1,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_0_10,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 1,
               TYPE_NAME = @TYPE_CUSTOMER_NORMAL;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '1.2'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'1.2',
               TYPE_OF_SALE_ID = 1,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_11_50,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 1,
               TYPE_NAME = @TYPE_CUSTOMER_NORMAL;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '1.3'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'1.3',
               TYPE_OF_SALE_ID = 1,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_51_200,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 1,
               TYPE_NAME = @TYPE_CUSTOMER_NORMAL;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '2.1'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'2.1',
               TYPE_OF_SALE_ID = 2,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_SCHOOL,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 2,
               TYPE_NAME = @TYPE_CUSTOMER_FAVOR;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '2.2'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'2.2',
               TYPE_OF_SALE_ID = 2,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_AGRI,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 2,
               TYPE_NAME = @TYPE_CUSTOMER_FAVOR;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '2.2.1'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'2.2.1',
               TYPE_OF_SALE_ID = 2,
               TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_MV,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 2,
               TYPE_NAME = @TYPE_CUSTOMER_FAVOR;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '2.2.2'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'2.2.2',
               TYPE_OF_SALE_ID = 2,
               TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_LICENSE,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 2,
               TYPE_NAME = @TYPE_CUSTOMER_FAVOR;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '2.2.3'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'2.2.3',
               TYPE_OF_SALE_ID = 2,
               TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_CUSTOMER_BUY,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 2,
               TYPE_NAME = @TYPE_CUSTOMER_FAVOR;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '2.2.4'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'2.2.4',
               TYPE_OF_SALE_ID = 2,
               TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_GENERAL,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 2,
               TYPE_NAME = @TYPE_CUSTOMER_FAVOR;

END;
-- END RUN_REF_03B_2021

GO

IF OBJECT_ID('RUN_REF_INVALID_CUSTOMER_2021') IS NOT NULL
    DROP PROC RUN_REF_INVALID_CUSTOMER_2021;
GO
CREATE PROC dbo.RUN_REF_INVALID_CUSTOMER_2021 @DATA_MONTH DATETIME = '2021-01-01'
AS
/*
@2017-09-06 By Morm Raksmey
1. Clear Data before INSERT DATA

@2018-03-23 By Morm Raksmey
1. Update REF for Tariff 2018

@2018-09-03 By Pov Lyhorng
1. Update find invalid customer by Connection type & Customer Gruop
	from TLKP_CUSTOMER_CONNECTION_TYPE & TLKP_CUSTOMER_GROUP

@2019-Jan-17 By Phoung Sovathvong
1. Update for REF 2019

@2019-Dec-02 By Hor Dara
1. Update for REF 2020

@20201-Jan-05 BY Vonn kimputhmunyvorn
1. Update for REF 2021
*/
DELETE FROM TBL_REF_INVALID_CUSTOMER
WHERE DATA_MONTH = @DATA_MONTH;
DECLARE @M DECIMAL(18, 4);
DECLARE @LICENSE_TYPE_ID INT

SET @M = 99999999999.9999;


IF OBJECT_ID('#TMP_VALIDATE') IS NOT NULL
    DROP TABLE #TMP_VALIDATE;
CREATE TABLE #TMP_VALIDATE
(
    DATA_MONTH DATETIME,
    VALIDATE_NAME NVARCHAR(250),
    START_USAGE DECIMAL(18, 4),
    END_USAGE DECIMAL(18, 4),
    PRICE DECIMAL(18, 5),
    BASED_PRICE DECIMAL(18, 5),
    CURRENCY_ID INT,
    CUSTOMER_CONNECTION_TYPE_ID INT,
    CUSTOMER_GROUP_ID INT
);

--CHECK LICENSE TYPE ID
SELECT @LICENSE_TYPE_ID = LICENSE_TYPE_ID
FROM dbo.TBL_TARIFF

INSERT INTO #TMP_VALIDATE
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យលំនៅដ្ឋានប្រើតិចជាង  10 kWh/ខែ',
       START_USAGE = 0,
       END_USAGE = 10,
       PRICE = 380,
       BASE_PRICE = 730,
       CURRENCY = 1,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យលំនៅដ្ឋានប្រើពី 11 ដល់ 50 kWh/ខែ',
       START_USAGE = 11,
       END_USAGE = 50,
       PRICE = 480,
       BASE_PRICE = 730,
       CURRENCY = 1,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យលំនៅដ្ឋានប្រើពី  51 ដល់ 200 kWh/ខែ',
       START_USAGE = 51,
       END_USAGE = 200,
       PRICE = 610,
       BASE_PRICE = 730,
       CURRENCY = 1,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យលំនៅដ្ឋានប្រើធំជាង  201 kWh/ខែ',
       START_USAGE = 201,
       END_USAGE = @M,
       PRICE = 730,
       BASE_PRICE = 730,
       CURRENCY = 1,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 730,
       BASE_PRICE = 730,
       CURRENCY = 1,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 22, 23, 134, 135, 136, 137, 138, 139, 140 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'សាលារៀន, មន្ទីរពេទ្យ, និងមណ្ឌលសុខភាព នៅតំបន់ជនបទ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 610,
       BASE_PRICE = 610,
       CURRENCY = 1,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ផ្សេងៗទៀតទាំងអស់ក្នុងវិស័យកសិកម្មប្រើ ពេលយប់ ម៉ោង៩យប់ ដល់ម៉ោង៧ព្រឹក',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 480,
       BASE_PRICE = 480,
       CURRENCY = 1,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
    INNER JOIN dbo.TBL_CUSTOMER tc
        ON tc.CUSTOMER_CONNECTION_TYPE_ID = c.CUSTOMER_CONNECTION_TYPE_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 24, 141, 142, 143 )
      AND tc.PRICE_ID IN
          (
              SELECT 480 FROM dbo.TBL_PRICE_DETAIL WHERE PRICE = 480
          )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ផ្សេងៗទៀតទាំងអស់ក្នុងវិស័យកសិកម្ម(ទិញតាមអត្រាថ្លៃទូទៅ)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 730,
       BASE_PRICE = 730,
       CURRENCY = 1,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
    INNER JOIN dbo.TBL_CUSTOMER tc
        ON tc.CUSTOMER_CONNECTION_TYPE_ID = c.CUSTOMER_CONNECTION_TYPE_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
      AND tc.PRICE_ID IN
          (
              SELECT 730 FROM dbo.TBL_PRICE_DETAIL WHERE PRICE = 730
          )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ផ្សេងៗទៀតទាំងអស់ក្នុងវិស័យកសិកម្ម(ទិញ MV តាមអត្រាថ្លៃមធ្យម)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.13700,
       BASE_PRICE = 0.13700,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
    INNER JOIN dbo.TBL_CUSTOMER tc
        ON tc.CUSTOMER_CONNECTION_TYPE_ID = c.CUSTOMER_CONNECTION_TYPE_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
      AND tc.PRICE_ID IN
          (
              SELECT 0.13700 FROM dbo.TBL_PRICE_DETAIL WHERE PRICE = 0.13700
          )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ផ្សេងៗទៀតទាំងអស់ក្នុងវិស័យកសិកម្ម(ទិញ LV ត្រង់ស្វូរអ្នកលក់ តាមអត្រាថ្លៃមធ្យម)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15048,
       BASE_PRICE = 0.150480,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
    INNER JOIN dbo.TBL_CUSTOMER tc
        ON tc.CUSTOMER_CONNECTION_TYPE_ID = c.CUSTOMER_CONNECTION_TYPE_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
      AND tc.PRICE_ID IN
          (
              SELECT 0.15048 FROM dbo.TBL_PRICE_DETAIL WHERE PRICE = 0.15048
          )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ផ្សេងៗទៀតទាំងអស់ក្នុងវិស័យកសិកម្ម(ទិញ LV ត្រង់ស្វូរអ្នកទិញ តាមអត្រាថ្លៃមធ្យម)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.14248,
       BASE_PRICE = 0.14248,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
    INNER JOIN dbo.TBL_CUSTOMER tc
        ON tc.CUSTOMER_CONNECTION_TYPE_ID = c.CUSTOMER_CONNECTION_TYPE_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
      AND tc.PRICE_ID IN
          (
              SELECT 0.14248 FROM dbo.TBL_PRICE_DETAIL WHERE PRICE = 0.14248
          )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យអ្នកកាន់អាជ្ញាប័ណ្ណ MV',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1290,
       BASE_PRICE = 0.1290,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 11 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យអ្នកកាន់អាជ្ញាប័ណ្ណ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.13416,
       BASE_PRICE = 0.13416,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 12 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យអ្នកកាន់អាជ្ញាប័ណ្ណ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.14216,
       BASE_PRICE = 0.14216,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 13 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យរុករករ៉ែ MV',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.137,
       BASE_PRICE = 0.137,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 26 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យឧស្សាហកម្មវិស័យរុករករ៉ែ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15048,
       BASE_PRICE = 0.15048,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 27 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យរុករករ៉ែ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.14248,
       BASE_PRICE = 0.14248,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 28 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យរុករករ៉ែ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.13,
       BASE_PRICE = 0.13,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 29 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យឧស្សាហកម្មវិស័យរុករករ៉ែ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1432,
       BASE_PRICE = 0.1432,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 30 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យរុករករ៉ែ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.13520,
       BASE_PRICE = 0.13520,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 31 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យរុករករ៉ែ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1100,
       BASE_PRICE = 0.1100,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 32 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យឧស្សាហកម្មវិស័យរុករករ៉ែ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.12240,
       BASE_PRICE = 0.12240,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 33 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យរុករករ៉ែ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.11440,
       BASE_PRICE = 0.11440,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 34 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យរុករករ៉ែ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1300,
       BASE_PRICE = 0.1300,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 35 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យឧស្សាហកម្មវិស័យរុករករ៉ែ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.14320,
       BASE_PRICE = 0.14320,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 36 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យរុករករ៉ែ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1352,
       BASE_PRICE = 0.1352,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 37 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យកម្មន្តសាល MV',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1370,
       BASE_PRICE = 0.1370,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 38 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យឧស្សាហកម្មវិស័យកម្មន្តសាល LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15048,
       BASE_PRICE = 0.15048,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 39 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យកម្មន្តសាល LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.14248,
       BASE_PRICE = 0.14248,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 40 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យកម្មន្តសាល MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1300,
       BASE_PRICE = 0.1300,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 41 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យឧស្សាហកម្មវិស័យកម្មន្តសាល LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.14320,
       BASE_PRICE = 0.14320,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 42 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យកម្មន្តសាល LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.13520,
       BASE_PRICE = 0.13520,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 43 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យកម្មន្តសាល MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1100,
       BASE_PRICE = 0.1100,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 44 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យឧស្សាហកម្មវិស័យកម្មន្តសាល LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.12240,
       BASE_PRICE = 0.12240,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 45 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យកម្មន្តសាល LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.11440,
       BASE_PRICE = 0.11440,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 46 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យកម្មន្តសាល MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1300,
       BASE_PRICE = 0.1300,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 47 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យឧស្សាហកម្មវិស័យកម្មន្តសាល LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.14320,
       BASE_PRICE = 0.14320,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 48 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យកម្មន្តសាល LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1352,
       BASE_PRICE = 0.1352,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 49 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យវាយនភណ្ឌ MV',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.137,
       BASE_PRICE = 0.137,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 50 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យឧស្សាហកម្មវិស័យវាយនភណ្ឌ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15048,
       BASE_PRICE = 0.15048,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 51 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យវាយនភណ្ឌ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.14248,
       BASE_PRICE = 0.14248,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 52 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យវាយនភណ្ឌ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.13,
       BASE_PRICE = 0.13,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 53 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យឧស្សាហកម្មវិស័យវាយនភណ្ឌ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1432,
       BASE_PRICE = 0.1432,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 54 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យវាយនភណ្ឌ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.13520,
       BASE_PRICE = 0.13520,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 55 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យវាយនភណ្ឌ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1100,
       BASE_PRICE = 0.1100,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 56 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យឧស្សាហកម្មវិស័យវាយនភណ្ឌ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.12240,
       BASE_PRICE = 0.12240,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 57 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យវាយនភណ្ឌ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.11440,
       BASE_PRICE = 0.11440,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 58 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យវាយនភណ្ឌ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1300,
       BASE_PRICE = 0.1300,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 59 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យឧស្សាហកម្មវិស័យវាយនភណ្ឌ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.14320,
       BASE_PRICE = 0.14320,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 60 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យវាយនភណ្ឌ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1352,
       BASE_PRICE = 0.1352,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 61 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យកសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ MV',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.137,
       BASE_PRICE = 0.137,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 62 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យកសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15048,
       BASE_PRICE = 0.15048,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 63 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យកសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.14248,
       BASE_PRICE = 0.14248,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 64 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យកសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.13,
       BASE_PRICE = 0.13,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 65 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យកសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1432,
       BASE_PRICE = 0.1432,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 66 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យកសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.13520,
       BASE_PRICE = 0.13520,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 67 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យកសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1100,
       BASE_PRICE = 0.1100,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 68 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យកសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.12240,
       BASE_PRICE = 0.12240,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 69 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យកសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.11440,
       BASE_PRICE = 0.11440,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 70 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យកសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1300,
       BASE_PRICE = 0.1300,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 71 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យកសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.14320,
       BASE_PRICE = 0.14320,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 72 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យកសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1352,
       BASE_PRICE = 0.1352,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 73 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ចិញ្ចឹមសត្វ និងនេសាទ MV',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.137,
       BASE_PRICE = 0.137,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 74 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យសកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15048,
       BASE_PRICE = 0.15048,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 75 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.14248,
       BASE_PRICE = 0.14248,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 76 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.13,
       BASE_PRICE = 0.13,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 77 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យសកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1432,
       BASE_PRICE = 0.1432,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 78 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.13520,
       BASE_PRICE = 0.13520,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 79 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1100,
       BASE_PRICE = 0.1100,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 80 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យសកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.12240,
       BASE_PRICE = 0.12240,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 81 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.11440,
       BASE_PRICE = 0.11440,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 82 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1300,
       BASE_PRICE = 0.1300,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 83 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យសកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.14320,
       BASE_PRICE = 0.14320,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 84 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1352,
       BASE_PRICE = 0.1352,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 85 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសកម្មភាពកែច្នៃផលិតផលកសិកម្ម ចិញ្ចឹមសត្វ និងនេសាទ MV',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.137,
       BASE_PRICE = 0.137,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 86 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យសកម្មភាពកែច្នៃផលិតផលកសិកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15048,
       BASE_PRICE = 0.15048,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 87 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសកម្មភាពកែច្នៃផលិតផលកសិកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.14248,
       BASE_PRICE = 0.14248,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 88 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសកម្មភាពកែច្នៃផលិតផលកសិកម្ម MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.13,
       BASE_PRICE = 0.13,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 89 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យសកម្មភាពកែច្នៃផលិតផលកសិកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1432,
       BASE_PRICE = 0.1432,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 90 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសកម្មភាពកែច្នៃផលិតផលកសិកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.13520,
       BASE_PRICE = 0.13520,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 91 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសកម្មភាពកែច្នៃផលិតផលកសិកម្ម MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1100,
       BASE_PRICE = 0.1100,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 92 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យសកម្មភាពកែច្នៃផលិតផលកសិកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.12240,
       BASE_PRICE = 0.12240,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 93 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសកម្មភាពកែច្នៃផលិតផលកសិកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.11440,
       BASE_PRICE = 0.11440,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 94 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសកម្មភាពកែច្នៃផលិតផលកសិកម្ម MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1300,
       BASE_PRICE = 0.1300,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 95 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យសកម្មភាពកែច្នៃផលិតផលកសិកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.14320,
       BASE_PRICE = 0.14320,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 96 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសកម្មភាពកែច្នៃផលិតផលកសិកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1352,
       BASE_PRICE = 0.1352,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 97 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យពាណិជ្ជកម្ម MV',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.158,
       BASE_PRICE = 0.158,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 98 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យពាណិជ្ជកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.17232,
       BASE_PRICE = 0.17232,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 99 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យពាណិជ្ជកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.16432,
       BASE_PRICE = 0.16432,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 100 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យពាណិជ្ជកម្ម MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15,
       BASE_PRICE = 0.15,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 101 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យពាណិជ្ជកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.16400,
       BASE_PRICE = 0.16400,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 102 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យពាណិជ្ជកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15600,
       BASE_PRICE = 0.15600,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 103 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យពាណិជ្ជកម្ម MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1240,
       BASE_PRICE = 0.1240,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 104 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យពាណិជ្ជកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.13696,
       BASE_PRICE = 0.13696,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 105 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យពាណិជ្ជកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.12896,
       BASE_PRICE = 0.12896,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 106 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យពាណិជ្ជកម្ម MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1500,
       BASE_PRICE = 0.1500,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 107 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យពាណិជ្ជកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.16400,
       BASE_PRICE = 0.16400,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 108 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យពាណិជ្ជកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15600,
       BASE_PRICE = 0.15600,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 109 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យរដ្ឋបាលសាធារណៈ MV',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.158,
       BASE_PRICE = 0.158,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 110 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យរដ្ឋបាលសាធារណៈ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.17232,
       BASE_PRICE = 0.17232,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 111 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យរដ្ឋបាលសាធារណៈ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.16432,
       BASE_PRICE = 0.16432,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 112 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យរដ្ឋបាលសាធារណៈ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15,
       BASE_PRICE = 0.15,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 113 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យរដ្ឋបាលសាធារណៈ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.16400,
       BASE_PRICE = 0.16400,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 114 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យរដ្ឋបាលសាធារណៈ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15600,
       BASE_PRICE = 0.15600,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 115 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យរដ្ឋបាលសាធារណៈ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1240,
       BASE_PRICE = 0.1240,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 116 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យរដ្ឋបាលសាធារណៈ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.13696,
       BASE_PRICE = 0.13696,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 117 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យរដ្ឋបាលសាធារណៈ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.12896,
       BASE_PRICE = 0.12896,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 118 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យរដ្ឋបាលសាធារណៈ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1500,
       BASE_PRICE = 0.1500,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 119 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យរដ្ឋបាលសាធារណៈ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.16400,
       BASE_PRICE = 0.16400,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 120 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យរដ្ឋបាលសាធារណៈ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15600,
       BASE_PRICE = 0.15600,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 121 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសេវាកម្មផ្សេងៗ MV',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.158,
       BASE_PRICE = 0.158,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 122 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យសេវាកម្មផ្សេងៗ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.17232,
       BASE_PRICE = 0.17232,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 123 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសេវាកម្មផ្សេងៗ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.16432,
       BASE_PRICE = 0.16432,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 124 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសេវាកម្មផ្សេងៗ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15,
       BASE_PRICE = 0.15,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 125 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យសេវាកម្មផ្សេងៗ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.16400,
       BASE_PRICE = 0.16400,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 126 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសេវាកម្មផ្សេងៗ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15600,
       BASE_PRICE = 0.15600,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 127 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសេវាកម្មផ្សេងៗ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1240,
       BASE_PRICE = 0.1240,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 128 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យសេវាកម្មផ្សេងៗ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.13696,
       BASE_PRICE = 0.13696,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 129 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសេវាកម្មផ្សេងៗ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.12896,
       BASE_PRICE = 0.12896,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 130 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសេវាកម្មផ្សេងៗ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1500,
       BASE_PRICE = 0.1500,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 131 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យរដ្ឋបាលសាធារណៈ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.16400,
       BASE_PRICE = 0.16400,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 132 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសេវាកម្មផ្សេងៗ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15600,
       BASE_PRICE = 0.15600,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 133 )

UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យអាជីវករតូបលក់ដូរក្នុងផ្សារ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 730,
       BASE_PRICE = 730,
       CURRENCY = 1,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 144 )

---Old Csutomer Type
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 730,
       BASE_PRICE = -730,
       CURRENCY = 1,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 14, 15, 16 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ MV',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1580,
       BASE_PRICE = -0.1580,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 8 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1500,
       BASE_PRICE = -0.1500,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
    INNER JOIN TBL_CUSTOMER cu
        ON c.CUSTOMER_CONNECTION_TYPE_ID = cu.CUSTOMER_CONNECTION_TYPE_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 10 )
      AND cu.PRICE_ID NOT IN
          (
              SELECT PRICE_ID
              FROM TBL_PRICE_DETAIL
              WHERE PRICE = 0.1500
                    OR PRICE = 0.1240
          )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.16432,
       BASE_PRICE = -0.16432,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 6 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.17232,
       BASE_PRICE = -0.17232,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 5 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ MV',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1370,
       BASE_PRICE = -0.1370,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 4 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1300,
       BASE_PRICE = -0.1300,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
    INNER JOIN TBL_CUSTOMER cu
        ON c.CUSTOMER_CONNECTION_TYPE_ID = cu.CUSTOMER_CONNECTION_TYPE_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 7 )
      AND cu.PRICE_ID NOT IN
          (
              SELECT PRICE_ID
              FROM TBL_PRICE_DETAIL
              WHERE PRICE = 0.1300
                    OR PRICE = 0.1100
          )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.14248,
       BASE_PRICE = -0.14248,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 3 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15048,
       BASE_PRICE = -0.15048,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 2 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យអាជីវករតូបលក់ដូរក្នុងផ្សារ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 730,
       BASE_PRICE = -730,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID NOT IN ( 144 ) AND @LICENSE_TYPE_ID=2;

DELETE FROM TBL_REF_INVALID_CUSTOMER
WHERE DATA_MONTH = @DATA_MONTH;

INSERT INTO TBL_REF_INVALID_CUSTOMER
SELECT TYPE_ID = i.CUSTOMER_GROUP_ID,
       TYPE_NAME = v.VALIDATE_NAME,
       METER_CODE_FORMAT = REPLACE(LTRIM(REPLACE(ISNULL(i.METER_CODE, '-'), '0', ' ')), ' ', '0'),
       IS_WRONG_BASED_PRICE = CASE
                                  WHEN i.BASED_PRICE != v.BASED_PRICE THEN
                                      1
                                  ELSE
                                      0
                              END,
       i.*
FROM TBL_REF_INVOICE i
    LEFT JOIN #TMP_VALIDATE v
        ON v.CUSTOMER_CONNECTION_TYPE_ID = i.CUSTOMER_CONNECTION_TYPE_ID
           AND v.CUSTOMER_GROUP_ID = i.CUSTOMER_GROUP_ID
WHERE i.DATA_MONTH = @DATA_MONTH
      AND
      (
          i.PRICE != v.PRICE
          OR i.BASED_PRICE != v.BASED_PRICE
          OR i.CURRENCY_ID != v.CURRENCY_ID
      )
      AND i.IS_ACTIVE = 1
      AND i.TOTAL_USAGE
      BETWEEN v.START_USAGE AND v.END_USAGE
      OR i.TOTAL_USAGE < 0;
--END OF RUN_REF_INVALID_CUSTOMER_2021

GO

IF OBJECT_ID('REPORT_REF_INVOICE') IS NOT NULL
    DROP PROC REPORT_REF_INVOICE;
GO

CREATE PROC [dbo].[REPORT_REF_INVOICE]
    @DATA_MONTH DATETIME = '2020-12-01',
    @CUSTOMER_GROUP_ID INT = 0,
    @CUSTOMER_CONNECTION_TYPE_ID INT = 0,
    @CURRENCY_ID INT = 0,
    @V1 DECIMAL(18, 4) = 0,
    @V2 DECIMAL(18, 4) = 1000000,
    @BILLING_CYCLE_ID INT = 0,
    @AREA_ID INT = 0
AS
/*
@ 2016-05-25 Morm Raksmey
	Add Column Billing Cycle Id and Billing Cycle Name
@ 2017-03-10 Sieng Sothera
	Trim meter code without '0'
@ 2017-04-19 Sieng Sotheara
	Update REF Invoice 2016 & 2017
@ 2018-09-19 Pov Lyhorng
	fix Organization customer is non subsidy type
@ 2019-01-16 Phuong Sovathvong
	Update REF Invoice 2019
@ 2019-12-12 HOR DARA
	Update REF Invoice 2020
@ 2020-07-28 HOR DARA
	Update REF Invoice 2020 - market vendor get subsidy even over 2000kwh (49 licensee)
@ 2021-01-08 Vonn kimputhmunyvorn
	Update REF Invoice 2021
*/
DECLARE @LICENSE_TYPE_ID INT,
        @BASED_TARIFF DECIMAL(18, 5),
        @SUBSIDY_TARIFF DECIMAL(18, 5);
SELECT @LICENSE_TYPE_ID = LICENSE_TYPE_ID,
       @SUBSIDY_TARIFF = SUBSIDY_TARIFF,
       @BASED_TARIFF = BASED_TARIFF
FROM dbo.TBL_TARIFF
WHERE DATA_MONTH = @DATA_MONTH;
CREATE TABLE #RESULT
(
    TYPE_ID INT,
    TYPE_NAME NVARCHAR(255),
    TYPE_OF_SALE_ID INT,
    TYPE_OF_SALE_NAME NVARCHAR(255),
    HISTORY_ID BIGINT,
    CUSTOMER_GROUP_ID INT,
    CUSTOMER_GROUP_NAME NVARCHAR(200),
    INVOICE_ID BIGINT,
    DATA_MONTH DATETIME,
    CUSTOMER_ID INT,
    CUSTOMER_CODE NVARCHAR(50),
    FIRST_NAME_KH NVARCHAR(50),
    LAST_NAME_KH NVARCHAR(50),
    AREA_ID INT,
    AREA_CODE NVARCHAR(50),
    BOX_ID INT,
    BOX_CODE NVARCHAR(50),
    CUSTOMER_CONNECTION_TYPE_ID INT,
    CUSTOMER_CONNECTION_TYPE_NAME NVARCHAR(200),
    AMPARE_ID INT,
    AMPARE_NAME NVARCHAR(50),
    PRICE_ID INT,
    METER_ID INT,
    METER_CODE NVARCHAR(50),
    MULTIPLIER INT,
    CURRENCY_SIGN NVARCHAR(50),
    CURRENCY_NAME NVARCHAR(50),
    CURRENCY_ID INT,
    PRICE DECIMAL(18, 5),
    BASED_PRICE DECIMAL(18, 5),
    START_USAGE DECIMAL(18, 4),
    END_USAGE DECIMAL(18, 4),
    TOTAL_USAGE DECIMAL(18, 4),
    TOTAL_AMOUNT DECIMAL(18, 5),
    IS_POST_PAID BIT,
    IS_AGRICULTURE BIT,
    SHIFT_ID INT,
    IS_ACTIVE BIT,
    BILLING_CYCLE_ID INT,
    BILLING_CYCLE_NAME NVARCHAR(50),
    ROW_DATE DATETIME,
    CUSTOMER_TYPE_ID INT,
    CUSTOMER_TYPE_NAME NVARCHAR(50),
    LICENSE_NUMBER NVARCHAR(50)
);
/*REF 2016*/
IF @DATA_MONTH
   BETWEEN '2016-03-01' AND '2017-02-01'
BEGIN
    INSERT INTO #RESULT
    /*លក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ*/
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'លក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 3
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 4
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'លក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 3
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 3
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'លក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 3
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 2
    UNION ALL
    /*លក់ឲ្យអតិថិជន MV​(ឧស្សាហកម្ម ពាណិជ្ជកម្ម)*/
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'លក់ឲ្យអតិថិជន MV​(ឧស្សាហកម្ម ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 2
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 4
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'លក់ឲ្យអតិថិជន MV​(ឧស្សាហកម្ម ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 2
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 3
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'លក់ឲ្យអតិថិជន MV​(ឧស្សាហកម្ម ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 2
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 2
    UNION ALL
    /*លក់លើតង់ស្យុងទាប*/
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'លក់លើតង់ស្យុងទាប',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'អតិថិជនធម្មតា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 1
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 1;
END;
/*REF 2017*/
ELSE IF @DATA_MONTH
        BETWEEN '2017-03-01' AND '2018-12-01'
BEGIN
    INSERT INTO #RESULT
    /*ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ*/
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 3
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 11
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 3
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 12
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 3
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 13
    UNION ALL
    /*ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)*/
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID IN ( 1, 2 )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 4, 7, 8, 10 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID IN ( 1, 2 )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 3, 6 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID IN ( 1, 2 )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 2, 5 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID IN ( 1, 2, 5 )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9, 14, 15, 16 )
          AND i.TOTAL_USAGE >= 2001
    UNION ALL
    /*ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)*/
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)',
           TYPE_OF_SALE_ID = -1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើក្រោម 2001 kWh/ខែ ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID IN ( 1, 2, 5 )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9, 14, 15, 16 )
          AND i.TOTAL_USAGE < 2001
    UNION ALL
    /* ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន */
    SELECT TYPE_ID = 0,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើតិចជាង  10 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID IN ( 4 )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE <= 10
    UNION ALL
    SELECT TYPE_ID = 0,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID IN ( 4 )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE
          BETWEEN 11 AND 50
    UNION ALL
    SELECT TYPE_ID = 0,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើធំជាង 51 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID IN ( 4 )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE >= 51;
END;
/*REF 2019*/
ELSE IF @DATA_MONTH
        BETWEEN '2019-01-01' AND '2020-01-01'
BEGIN
    INSERT INTO #RESULT
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 11
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 12
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 13
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 4 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវាលា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 7 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 3 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 2 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 14 )
          AND i.TOTAL_USAGE >= 2001
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 8 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 10 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 6 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 5 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 15, 16 )
          AND i.TOTAL_USAGE >= 2001
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអតិថិជន សាលារៀន មន្ទីរពេទ្យ មណ្ឌលសុខភាព នៅតំបន់ជនបទ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)',
           TYPE_OF_SALE_ID = -1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើក្រោម 2001 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 14, 15, 16 )
          AND i.TOTAL_USAGE < 2001
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើតិចជាង  10 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE <= 10
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE
          BETWEEN 11 AND 50
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 51 ដល់ 200 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE
          BETWEEN 51 AND 200
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើធំជាង 201 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE >= 201;
END;
/*REF 2020 - NORMAL LICENSE TYPE ID =1*/
ELSE IF @DATA_MONTH
        BETWEEN '2020-02-01' AND '2020-12-01'
        AND @LICENSE_TYPE_ID = 1
BEGIN
    INSERT INTO #RESULT
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 11
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 12
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 13
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 4 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 7 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAMEN = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 21 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 3 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 17 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAMEN = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 22 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 2 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 18 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAMEN = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 23 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 14 )
          AND i.TOTAL_USAGE >= 2001
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 8 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 10 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAMEN = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 6 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 19 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAMEN = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 25 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 5 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 20 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAMEN = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 0,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 26 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = -1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 15, 16 )
          AND i.TOTAL_USAGE >= 2001
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = -2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអតិថិជន សាលារៀន មន្ទីរពេទ្យ មណ្ឌលសុខភាព នៅតំបន់ជនបទ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)',
           TYPE_OF_SALE_ID = -3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើក្រោម 2001 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 14, 15, 16 )
          AND i.TOTAL_USAGE < 2001
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើតិចជាង  10 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE <= 10
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE
          BETWEEN 11 AND 50
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 51 ដល់ 200 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE
          BETWEEN 51 AND 200
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -7,
           TYPE_OF_SALE_NAE = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើធំជាង 201 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE >= 201;
END;
/*REF 2020 - LICENSE TYPE ID =2*/
ELSE IF @DATA_MONTH
        BETWEEN '2020-02-01' AND '2020-12-01'
        AND @LICENSE_TYPE_ID = 2
BEGIN
    INSERT INTO #RESULT
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជននៅក្នុងបរិវេណផ្សារ',
           TYPE_OF_SALE_ID = -1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនក្នុងផ្សារ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 14, 15, 16 );
--AND i.TOTAL_USAGE<2001
END;

/*REF 2021 - NORMAL LICENSE TYPE ID =1*/
ELSE IF @DATA_MONTH
        BETWEEN '2021-01-01' AND '9999-12-01'
        AND @LICENSE_TYPE_ID = 1
BEGIN
    INSERT INTO #RESULT
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 11
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 12
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 13

    --ឧស្សាហកម្មវិស័យរុករករ៉ែ
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 72,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 26
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 71,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 27
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 70,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 28
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 69,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 29
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 68,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 30
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 67,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 31
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 66,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 32
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 65,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 33
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 64,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 34
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 63,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 35
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 62,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 36
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ) ',
           TYPE_OF_SALE_ID = 61,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 37

    --ឧស្សាហកម្មវិស័យកម្មន្តសាល
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 60,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 38
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 59,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 39
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 58,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 40
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 57,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 41
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 56,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 42
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 55,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 43
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 54,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 44
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 53,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 45
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 52,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 46
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 51,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 47
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 50,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 48
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល) ',
           TYPE_OF_SALE_ID = 49,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 49

    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 48,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 50
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 47,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 51
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 46,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 52
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 45,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 53
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 44,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 54
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 43,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 55
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 42,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 56
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 41,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 57
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 40,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 58
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 39,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 59
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 38,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 60
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ) ',
           TYPE_OF_SALE_ID = 37,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 61

    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 36,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 62
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 35,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 63
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 34,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 64
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 33,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 65
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 32,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 66
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 31,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 67
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 30,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 68
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 29,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 69
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 28,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 70
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 27,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 71
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 26,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 72
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ) ',
           TYPE_OF_SALE_ID = 25,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 73

    --សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។)
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 24,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 74
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 23,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 75
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 22,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 76
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 21,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 77
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 20,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 78
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 19,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 79
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 18,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 80
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 17,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 81
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 16,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 82
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 15,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 83
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 14,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 84
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម) ',
           TYPE_OF_SALE_ID = 13,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 85

    --សកម្មភាពកែច្នៃផលិតផលកសិកម្ម
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 12,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 86
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 87
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 88
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 89
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 90
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 91
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 92
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 93
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 94
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 95
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 96
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម) ',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 97

    --ពាណិជ្ជកម្ម
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 36,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 98
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 35,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 99
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 34,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 100
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 33,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 101
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 32,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 102
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 31,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 103
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 30,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 104
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 29,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 105
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 28,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 106
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 27,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 107
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 26,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 108
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម) ',
           TYPE_OF_SALE_ID = 25,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 109

    --រដ្ឋបាលសាធារណៈ
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 24,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 110
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 23,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 111
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 22,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 112
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 21,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 113
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 20,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 114
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 19,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 115
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 18,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 116
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 17,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 117
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 16,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 118
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 15,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 119
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 14,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 120
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ) ',
           TYPE_OF_SALE_ID = 13,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 121

    --សេវាកម្មផ្សេងៗ
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 12,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 122
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 123
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 124
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 125
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 126
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 127
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 128
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 129
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 130
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 131
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 132
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ) ',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 133
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែបណ្តាញសាធារណៈ)',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 22, 23, 134, 135, 136, 137, 138, 139, 140 )
          AND i.TOTAL_USAGE >= 2001
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអតិថិជន សាលារៀន មន្ទីរពេទ្យ មណ្ឌលសុខភាព នៅតំបន់ជនបទ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើក្រោម 2001 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 22, 23, 134, 135, 136, 137, 138, 139, 140 )
          AND i.TOTAL_USAGE < 2001
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើតិចជាង  10 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE <= 10
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE
          BETWEEN 11 AND 50
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 51 ដល់ 200 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE
          BETWEEN 51 AND 200
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAE = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើធំជាង 201 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE >= 201
    UNION ALL
    SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម(ទិញ MV)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
          AND i.PRICE = 0.13700
    UNION ALL
    SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម (ទិញ MV ម៉ោង៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
          AND i.PRICE = 480
    UNION ALL
    SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម (ទិញ LV ត្រង់ស្វូរអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
          AND i.PRICE = 0.15048
    UNION ALL
    SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម (ទិញ LV ត្រង់ស្វូរអ្នកលក់ ម៉ោង៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
          AND i.PRICE = 480
    UNION ALL
    SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម (ទិញ LV ត្រង់ស្វូរអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
          AND i.PRICE = 0.14248
    UNION ALL
    SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម (ទិញ LV ត្រង់ស្វូរអ្នកទិញ ម៉ោង៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
          AND i.PRICE = 480
    UNION ALL
    SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម (ទិញ LV សាធារណៈ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
          AND i.PRICE = 730
    UNION ALL
    SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម (ទិញ LV សាធារណៈ ៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
          AND i.PRICE = 480
	UNION
	SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជននៅក្នុងបរិវេណផ្សារ',
           TYPE_OF_SALE_ID = -1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនក្នុងផ្សារ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 144 );
END;
ELSE IF @DATA_MONTH
        BETWEEN '2021-01-01' AND '9999-12-31'
        AND @LICENSE_TYPE_ID = 2
BEGIN
    INSERT INTO #RESULT
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជននៅក្នុងបរិវេណផ្សារ',
           TYPE_OF_SALE_ID = -1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនក្នុងផ្សារ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 144 );
END;

/*RESULT*/
SELECT r.*,
       METER_FORMAT = REPLACE(LTRIM(REPLACE(ISNULL(METER_CODE, '-'), '0', ' ')), ' ', '0')
FROM #RESULT r
WHERE DATA_MONTH = @DATA_MONTH
      AND IS_ACTIVE = 1
      AND
      (
          @CUSTOMER_GROUP_ID = 0
          OR CUSTOMER_GROUP_ID = @CUSTOMER_GROUP_ID
      )
      AND
      (
          @CURRENCY_ID = 0
          OR CURRENCY_ID = @CURRENCY_ID
      )
      AND
      (
          @CUSTOMER_CONNECTION_TYPE_ID = 0
          OR CUSTOMER_CONNECTION_TYPE_ID = @CUSTOMER_CONNECTION_TYPE_ID
      )
      AND (TOTAL_USAGE
      BETWEEN @V1 AND @V2
          )
      AND
      (
          @BILLING_CYCLE_ID = 0
          OR BILLING_CYCLE_ID = @BILLING_CYCLE_ID
      )
      AND
      (
          @AREA_ID = 0
          OR AREA_ID = @AREA_ID
      )
ORDER BY TYPE_ID DESC,
         TYPE_OF_SALE_ID DESC,
         AREA_CODE,
         BOX_CODE;
--END REPORT_REF_INVOICE

GO

IF OBJECT_ID('RUN_REF_03A_2021') IS NOT NULL
    DROP PROC RUN_REF_03A_2021;

GO

CREATE PROC [dbo].[RUN_REF_03A_2021]
    @DATA_MONTH DATETIME = '2021-01-01',
    @BILLING_CYCLE_ID INT = 0,
    @AREA_ID INT = 0
AS
/*

@2019-12-03 By Hor Dara
	Update REF 2020
	.--FIll all data with 730 base price even school type is 610 to include 1 row
@2020-07-28 By Hor Dara
	Update REF 2020
	--validate for by license type market vendor even usage over 2000 still have subsidy
*/
-- CLEAR DATA
DELETE FROM TBL_REF_03A
WHERE DATA_MONTH = @DATA_MONTH;

DECLARE @BASED_TARIFF DECIMAL(18, 5),
        @SUBSIDY_TARIFF DECIMAL(18, 5),
        @LICENSE_TYPE_ID INT,
        @TYPE_SALE_CUSTOMER_SUBSIDY NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_SUBSIDY_MARKET_VENDOR NVARCHAR(200);
SET @TYPE_SALE_CUSTOMER_SUBSIDY = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវ ទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ ';
SET @TYPE_SALE_CUSTOMER_SUBSIDY_MARKET_VENDOR = N'លក់ឲ្យអតិថិជនក្នុងផ្សារ';
SELECT @BASED_TARIFF = BASED_TARIFF,
       @SUBSIDY_TARIFF = SUBSIDY_TARIFF,
       @LICENSE_TYPE_ID = LICENSE_TYPE_ID
FROM TBL_TARIFF
WHERE DATA_MONTH = @DATA_MONTH
      AND IS_ACTIVE = 1;

IF (@LICENSE_TYPE_ID <> 2)
BEGIN
    -- for Normal Customer(TOTAL_USAGE<2001)
    SELECT TOTAL_SOLD = SUM(TOTAL_USAGE),
           CURRENCY_ID = 1,
           CURRENCY_NAME = N'រៀល',
           CURRENCY_SIGN = N'៛'
    INTO #TMP
    FROM TBL_REF_INVOICE i
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 22, 23, 134, 135, 136, 137, 138, 139, 140, 144 )
          AND TOTAL_USAGE <= 2000
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Normal Customer (when tariff rate lower than zero)
    UNION ALL
    SELECT TOTAL_SOLD = SUM(TOTAL_USAGE),
           CURRENCY_ID = 1,
           CURRENCY_NAME = N'រៀល',
           CURRENCY_SIGN = N'៛'
    FROM TBL_REF_INVOICE i
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND TOTAL_USAGE > 200
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

	UNION ALL
    SELECT TOTAL_SOLD = SUM(TOTAL_USAGE),
           CURRENCY_ID = 1,
           CURRENCY_NAME = N'រៀល',
           CURRENCY_SIGN = N'៛'
    FROM TBL_REF_INVOICE i
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 ) AND i.PRICE=730
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Normal Customer (when tariff rate equal or greater than zero)
    UNION ALL
    SELECT TOTAL_SOLD = SUM(TOTAL_USAGE),
           CURRENCY_ID = 1,
           CURRENCY_NAME = N'រៀល',
           CURRENCY_SIGN = N'៛'
    FROM TBL_REF_INVOICE i
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for School, Hospital, Health Center at countryside (when tariff rate < 0 ,exclude)
    UNION ALL
    SELECT TOTAL_SOLD = SUM(TOTAL_USAGE),
           CURRENCY_ID = 1,
           CURRENCY_NAME = N'រៀល',
           CURRENCY_SIGN = N'៛'
    FROM TBL_REF_INVOICE i
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN
    UNION ALL
    SELECT TOTAL_SOLD = SUM(TOTAL_USAGE),
           CURRENCY_ID = 1,
           CURRENCY_NAME = N'រៀល',
           CURRENCY_SIGN = N'៛'
    FROM TBL_REF_INVOICE i
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24, 141, 142, 143 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN;

    INSERT INTO TBL_REF_03A
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_SUBSIDY,
           TOTAL_SOLD = SUM(TOTAL_SOLD),
           RATE = 730, --FIll all data with 730 base price even school type is 610 to include 1 row
           BASED_TARIFF = @BASED_TARIFF,
           SUBSIDY_TARIFF = @SUBSIDY_TARIFF,
           SUBSIDY_RATE = @BASED_TARIFF - @SUBSIDY_TARIFF,
           SUBSIDY_AMOUNT = (@BASED_TARIFF - @SUBSIDY_TARIFF) * SUM(TOTAL_SOLD),
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM #TMP
    GROUP BY CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN;

    -- FILL BLANK RESULT
    IF NOT EXISTS (SELECT * FROM TBL_REF_03A WHERE DATA_MONTH = @DATA_MONTH)
        INSERT INTO TBL_REF_03A
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'1',
               TYPE_OF_SALE_ID = 1,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_SUBSIDY,
               TOTAL_SOLD = 0,
               RATE = 0,
               BASED_TARIFF = 0,
               SUBSIDY_TARIFF = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = 1,
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1;
END;
ELSE IF (@LICENSE_TYPE_ID = 2)
BEGIN
    -- for MARKET VENDOR
    SELECT TOTAL_SOLD = SUM(TOTAL_USAGE),
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_MARKET_VENDOR = 1
    INTO #TMP_MARKET_VENDOR
    FROM TBL_REF_INVOICE i
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 144 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN;

    INSERT INTO TBL_REF_03A
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_SUBSIDY_MARKET_VENDOR,
           TOTAL_SOLD = SUM(TOTAL_SOLD),
           RATE = 730,
           BASED_TARIFF = @BASED_TARIFF,
           SUBSIDY_TARIFF = @SUBSIDY_TARIFF,
           SUBSIDY_RATE = @BASED_TARIFF - @SUBSIDY_TARIFF,
           SUBSIDY_AMOUNT = (@BASED_TARIFF - @SUBSIDY_TARIFF) * SUM(TOTAL_SOLD),
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM #TMP_MARKET_VENDOR
    WHERE IS_MARKET_VENDOR = 1
    GROUP BY CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN;

    -- FILL BLANK RESULT
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03A
        WHERE DATA_MONTH = @DATA_MONTH
              AND TYPE_OF_SALE_ID = 1
    )
        INSERT INTO TBL_REF_03A
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'1',
               TYPE_OF_SALE_ID = 1,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_SUBSIDY_MARKET_VENDOR,
               TOTAL_SOLD = 0,
               RATE = 0,
               BASED_TARIFF = 0,
               SUBSIDY_TARIFF = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = 1,
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1;
END;
--END OF RUN_REF_03A_2021

GO

IF OBJECT_ID('RUN_PREPAID_CREDIT') IS NOT NULL
    DROP PROC dbo.RUN_PREPAID_CREDIT;
GO

CREATE PROC dbo.RUN_PREPAID_CREDIT
    @MONTH DATETIME = '2017-03-01',
    @START_DATE DATETIME = '2017-03-01',
    @END_DATE DATETIME = '2017-03-31',
    @CYCLE_ID INT = 2,
    @CREATE_BY NVARCHAR(200) = 'system'
WITH ENCRYPTION
AS
DECLARE @RUN_ID INT;
DECLARE @RUN_PREPAID_CREDIT_ID INT;
DECLARE @DATE DATETIME;
DECLARE @CURRENCY_ID INT;

IF OBJECT_ID('#TMP_CREDIT_TYPE') IS NOT NULL
    DROP TABLE #TMP_CREDIT_TYPE;
CREATE TABLE #TMP_CREDIT_TYPE
(
    TYPE_NAME NVARCHAR(100),
    START_USAGE DECIMAL(18, 4),
    END_USAGE DECIMAL(18, 4),
    PRICE_CREDIT DECIMAL(18, 4),
    SUBSIDY_TARIFF DECIMAL(18, 4),
    CUSTOMER_CONNECTION_TYPE_ID INT
);

IF @MONTH
   BETWEEN '2016-03-01' AND '2017-02-01'
    INSERT INTO #TMP_CREDIT_TYPE
    SELECT N'លក់ឱ្យលំនៅដ្ឋានប្រើមិនលើសពី  10 kWh/ខែ',
           0,
           10,
           480,
           800,
           1
    UNION ALL
    SELECT N'លក់ឱ្យលំនៅដ្ឋានប្រើធំជាង 10 kWh/ខែ',
           11,
           99999999999999.9999,
           800,
           800,
           1;
ELSE IF @MONTH
        BETWEEN '2017-03-01' AND '2018-02-21'
    INSERT INTO #TMP_CREDIT_TYPE
    SELECT N'លក់ឱ្យលំនៅដ្ឋានប្រើមិនលើសពី  10 kWh/ខែ',
           0,
           10,
           480,
           790,
           1
    UNION ALL
    SELECT N'លក់ឱ្យលំនៅដ្ឋានប្រើពី 11 ដល់ 50 kWh/ខែ',
           11,
           50,
           610,
           790,
           1
    UNION ALL
    SELECT N'លក់ឱ្យលំនៅដ្ឋានប្រើធំជាង 51 kWh/ខែ',
           51,
           99999999999999.9999,
           790,
           790,
           1;
ELSE IF @MONTH
        BETWEEN '2018-02-22' AND '2019-01-22'
    INSERT INTO #TMP_CREDIT_TYPE
    SELECT N'លក់ឱ្យលំនៅដ្ឋានប្រើមិនលើសពី  10 kWh/ខែ',
           0,
           10,
           480,
           770,
           1
    UNION ALL
    SELECT N'លក់ឱ្យលំនៅដ្ឋានប្រើពី 11 ដល់ 50 kWh/ខែ',
           11,
           50,
           610,
           770,
           1
    UNION ALL
    SELECT N'លក់ឱ្យលំនៅដ្ឋានប្រើធំជាង 50 kWh/ខែ',
           51,
           99999999999999.9999,
           770,
           770,
           1;
ELSE IF @MONTH
        BETWEEN '2019-01-23' AND '2020-01-31'
    INSERT INTO #TMP_CREDIT_TYPE
    SELECT N'លក់ឱ្យលំនៅដ្ឋានប្រើមិនលើសពី  10 kWh/ខែ',
           0,
           10,
           380,
           740,
           1
    UNION ALL
    SELECT N'លក់ឱ្យលំនៅដ្ឋានប្រើពី 11 ដល់ 50 kWh/ខែ',
           11,
           50,
           480,
           740,
           1
    UNION ALL
    SELECT N'លក់ឱ្យលំនៅដ្ឋានប្រើពី 51 ដល់ 200 kWh/ខែ',
           51,
           200,
           610,
           740,
           1
    UNION ALL
    SELECT N'លក់ឱ្យលំនៅដ្ឋានប្រើធំជាង 200 kWh/ខែ',
           201,
           99999999999999.9999,
           740,
           740,
           1
    UNION ALL
    SELECT N'សាលារៀន, មន្ទីរពេទ្យ, និងមណ្ឌលសុខភាព នៅតំបន់ជនបទ',
           0,
           99999999999999.9999,
           610,
           610,
           9
    UNION ALL
    SELECT N'លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ',
           0,
           99999999999999.9999,
           740,
           740,
           14
    UNION ALL
    SELECT N'លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ',
           0,
           99999999999999.9999,
           740,
           740,
           15
    UNION ALL
    SELECT N'លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ',
           0,
           99999999999999.9999,
           740,
           740,
           16;
ELSE IF @MONTH
        BETWEEN '2020-02-01' AND '2020-12-01'
    INSERT INTO #TMP_CREDIT_TYPE
    SELECT N'លក់ឱ្យលំនៅដ្ឋានប្រើមិនលើសពី  10 kWh/ខែ',
           0,
           10,
           380,
           730,
           1
    UNION ALL
    SELECT N'លក់ឱ្យលំនៅដ្ឋានប្រើពី 11 ដល់ 50 kWh/ខែ',
           11,
           50,
           480,
           730,
           1
    UNION ALL
    SELECT N'លក់ឱ្យលំនៅដ្ឋានប្រើពី 51 ដល់ 200 kWh/ខែ',
           51,
           200,
           610,
           730,
           1
    UNION ALL
    SELECT N'លក់ឱ្យលំនៅដ្ឋានប្រើធំជាង 200 kWh/ខែ',
           201,
           99999999999999.9999,
           730,
           730,
           1
    UNION ALL
    SELECT N'សាលារៀន, មន្ទីរពេទ្យ, និងមណ្ឌលសុខភាព នៅតំបន់ជនបទ',
           0,
           99999999999999.9999,
           610,
           610,
           9
    UNION ALL
    SELECT N'លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ',
           0,
           99999999999999.9999,
           730,
           730,
           14
    UNION ALL
    SELECT N'លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ',
           0,
           99999999999999.9999,
           730,
           730,
           15
    UNION ALL
    SELECT N'លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ',
           0,
           99999999999999.9999,
           730,
           730,
           16;
ELSE IF @MONTH
        BETWEEN '2021-01-01' AND '9999-12-01'
    INSERT INTO #TMP_CREDIT_TYPE
    SELECT N'លក់ឱ្យលំនៅដ្ឋានប្រើមិនលើសពី  10 kWh/ខែ',
           0,
           10,
           380,
           730,
           1
    UNION ALL
    SELECT N'លក់ឱ្យលំនៅដ្ឋានប្រើពី 11 ដល់ 50 kWh/ខែ',
           11,
           50,
           480,
           730,
           1
    UNION ALL
    SELECT N'លក់ឱ្យលំនៅដ្ឋានប្រើពី 51 ដល់ 200 kWh/ខែ',
           51,
           200,
           610,
           730,
           1
    UNION ALL
    SELECT N'លក់ឱ្យលំនៅដ្ឋានប្រើធំជាង 200 kWh/ខែ',
           201,
           99999999999999.9999,
           730,
           730,
           1
    UNION ALL
    SELECT N'សាលារៀន, មន្ទីរពេទ្យ, និងមណ្ឌលសុខភាព នៅតំបន់ជនបទ',
           0,
           99999999999999.9999,
           610,
           610,
           9
    UNION ALL
    SELECT N'លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ',
           0,
           99999999999999.9999,
           730,
           730,
           22
    UNION ALL
    SELECT N'លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ',
           0,
           99999999999999.9999,
           730,
           730,
           23
    UNION ALL
    SELECT N'លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ',
           0,
           99999999999999.9999,
           730,
           730,
           134
    UNION ALL
    SELECT N'លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ',
           0,
           99999999999999.9999,
           730,
           730,
           135
    UNION ALL
    SELECT N'លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ',
           0,
           99999999999999.9999,
           730,
           730,
           136
    UNION ALL
    SELECT N'លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ',
           0,
           99999999999999.9999,
           730,
           730,
           137
    UNION ALL
    SELECT N'លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ',
           0,
           99999999999999.9999,
           730,
           730,
           138
    UNION ALL
    SELECT N'លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ',
           0,
           99999999999999.9999,
           730,
           730,
           139
    UNION ALL
    SELECT N'លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ',
           0,
           99999999999999.9999,
           730,
           730,
           140
    UNION ALL
    SELECT N'លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ',
           0,
           99999999999999.9999,
           0.1370,
           0.1370,
           141
    UNION ALL
    SELECT N'លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ',
           0,
           99999999999999.9999,
           480,
           480,
           141
    UNION ALL
    SELECT N'លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ',
           0,
           99999999999999.9999,
           0.15048,
           0.15048,
           142
    UNION ALL
    SELECT N'លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ',
           0,
           99999999999999.9999,
           480,
           480,
           142
    UNION ALL
    SELECT N'លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ',
           0,
           99999999999999.9999,
           0.14248,
           0.14248,
           143
    UNION ALL
    SELECT N'លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ',
           0,
           99999999999999.9999,
           480,
           480,
           143
    UNION ALL
    SELECT N'លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ',
           0,
           99999999999999.9999,
           730,
           730,
           24
    UNION ALL
    SELECT N'លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ',
           0,
           99999999999999.9999,
           480,
           480,
           24;

SET @CURRENCY_ID = 1;
SET @DATE = GETDATE();

--TMP USAGE
SELECT c.CUSTOMER_ID,
       u.METER_ID,
       u.START_USAGE,
       u.END_USAGE,
       u.MULTIPLIER,
       u.IS_METER_RENEW_CYCLE,
       TOTAL_USAGE = u.MULTIPLIER
                     * (CASE
                            WHEN IS_METER_RENEW_CYCLE = 0 THEN
                                END_USAGE - START_USAGE
                            ELSE
       (
           SELECT CONVERT(INT, REPLICATE('9', LEN(CONVERT(NVARCHAR, CONVERT(INT, START_USAGE)))))
       ) - START_USAGE + END_USAGE + 1
                        END
                       ),
       CUSTOMER_CONNECTION_TYPE_ID,
       c.PRICE_ID
INTO #TMP_CREDIT
FROM TBL_CUSTOMER c
    INNER JOIN TBL_USAGE u
        ON c.CUSTOMER_ID = u.CUSTOMER_ID
WHERE u.USAGE_MONTH = @MONTH
      AND c.IS_POST_PAID = 0
      AND c.BILLING_CYCLE_ID = @CYCLE_ID
      AND c.STATUS_ID IN ( 2, 3 );
--CALCUATE TMP USAGE
SELECT CUSTOMER_ID,
       CUSTOMER_CONNECTION_TYPE_ID,
       PRICE_ID,
       TOTAL_USAGE = SUM(TOTAL_USAGE)
INTO #TMP_CREDIT_CAL_USAGE
FROM #TMP_CREDIT c
GROUP BY CUSTOMER_ID,
         CUSTOMER_CONNECTION_TYPE_ID,
         PRICE_ID
ORDER BY CUSTOMER_ID;


--INSERT TO RUN BILL
INSERT INTO TBL_RUN_BILL
(
    CYCLE_ID,
    CREATE_ON,
    CREATE_BY,
    BILLING_MONTH,
    TOTAL_INVOICE,
    TOTAL_POWER,
    CUSTOMER_TOTAL,
    CUSTOMER_ACTIVE,
    CUSTOMER_BLOCKED,
    CUSTOMER_PENDING,
    CUSTOMER_CLOSE
)
VALUES
(@CYCLE_ID, @DATE, @CREATE_BY, @MONTH, 0, 0, 0, 0, 0, 0, 0);

SET @RUN_ID = @@IDENTITY;

--INSERT TO PREPAID_RUN_CREDIT 
INSERT INTO TBL_RUN_PREPAID_CREDIT
(
    RUN_ID,
    CREATE_ON,
    CREATE_BY,
    CREDIT_MONTH,
    TOTAL_CUSTOMER,
    TOTAL_CREDIT_USAGE,
    TOTAL_CREDIT_AMOUNT,
    IS_ACTIVE
)
VALUES
(@RUN_ID, @DATE, @CREATE_BY, @MONTH, 0, 0, 0, 1);

SET @RUN_PREPAID_CREDIT_ID = @@IDENTITY;

INSERT INTO TBL_PREPAID_CREDIT
(
    RUN_PREPAID_CREDIT_ID,
    CUSTOMER_ID,
    START_DATE,
    END_DATE,
    CREDIT_MONTH,
    USAGE,
    PRICE,
    CREDIT_AMOUNT,
    PAID_AMOUNT,
    CURRENCY_ID,
    CREATE_ON,
    CREATE_BY,
    USAGE_CREDIT,
    PRICE_ID,
    BASED_PRICE
)
SELECT @RUN_PREPAID_CREDIT_ID,
       CUSTOMER_ID,
       @START_DATE,
       @END_DATE,
       @MONTH,
       USAGE = TOTAL_USAGE,
       PRICE = SUBSIDY_TARIFF - PRICE_CREDIT,
       CREDIT_AMOUNT = CASE
                           WHEN TOTAL_USAGE
                                BETWEEN t.START_USAGE AND t.END_USAGE THEN
                               TOTAL_USAGE * (SUBSIDY_TARIFF - t.PRICE_CREDIT)
                           ELSE
                               0
                       END,
       PAID_AMOUNT = 0,
       CURRENCY_ID = @CURRENCY_ID,
       CREATE_ON = @DATE,
       CREATE_BY = @CREATE_BY,
       USAGE_CREDIT = CASE
                          WHEN SUBSIDY_TARIFF - PRICE_CREDIT > 0 THEN
                              TOTAL_USAGE
                          ELSE
                              0
                      END,
       PRICE_ID = c.PRICE_ID,
       BASED_PRICE = t.SUBSIDY_TARIFF
FROM #TMP_CREDIT_CAL_USAGE c
    LEFT JOIN #TMP_CREDIT_TYPE t
        ON c.CUSTOMER_CONNECTION_TYPE_ID = t.CUSTOMER_CONNECTION_TYPE_ID
WHERE TOTAL_USAGE
BETWEEN t.START_USAGE AND t.END_USAGE;


INSERT INTO TBL_PREPAID_CREDIT_USAGE
(
    PREPAID_CREDIT_ID,
    METER_ID,
    START_USAGE,
    END_USAGE,
    MULTIPLIER,
    IS_METER_RENEW_CYCLE,
    TOTAL_USAGE
)
SELECT p.PREPAID_CREDIT_ID,
       t.METER_ID,
       t.START_USAGE,
       t.END_USAGE,
       t.MULTIPLIER,
       t.IS_METER_RENEW_CYCLE,
       t.TOTAL_USAGE
FROM #TMP_CREDIT t
    INNER JOIN TBL_PREPAID_CREDIT p
        ON t.CUSTOMER_ID = p.CUSTOMER_ID
           AND RUN_PREPAID_CREDIT_ID = @RUN_PREPAID_CREDIT_ID;

--Update summary of TOTAL_CUSTOMER and TOTAL_CREDIT_AMOUNT
UPDATE rp
SET TOTAL_CUSTOMER = t.TOTAL_CUSTOMER,
    TOTAL_CREDIT_USAGE = t.TOTAL_CREDIT_USAGE,
    TOTAL_CREDIT_AMOUNT = t.TOTAL_CREDIT_AMOUNT
FROM TBL_RUN_PREPAID_CREDIT rp
    INNER JOIN
    (
        SELECT RUN_PREPAID_CREDIT_ID,
               TOTAL_CUSTOMER = COUNT(DISTINCT CUSTOMER_ID),
               TOTAL_CREDIT_USAGE = SUM(   CASE
                                               WHEN CREDIT_AMOUNT > 0 THEN
                                                   USAGE
                                               ELSE
                                                   0
                                           END
                                       ),
               TOTAL_CREDIT_AMOUNT = SUM(CREDIT_AMOUNT)
        FROM TBL_PREPAID_CREDIT
        WHERE RUN_PREPAID_CREDIT_ID = @RUN_PREPAID_CREDIT_ID
        GROUP BY RUN_PREPAID_CREDIT_ID
    ) t
        ON rp.RUN_PREPAID_CREDIT_ID = t.RUN_PREPAID_CREDIT_ID
WHERE rp.RUN_PREPAID_CREDIT_ID = @RUN_PREPAID_CREDIT_ID;

--CLEAR TMEP TABLE
IF OBJECT_ID('#TMP_CREDIT') IS NOT NULL
    DROP TABLE #TMP_CREDIT;
--*** END OF RUN_PREPAID_CREDIT***--
------------------------------------

GO

IF OBJECT_ID('RUN_REF_03B_2020') IS NOT NULL
    DROP PROC dbo.RUN_REF_03B_2020;
GO
CREATE PROC dbo.RUN_REF_03B_2020
    @DATA_MONTH DATETIME = '2020-01-01',
    @BILLING_CYCLE_ID INT = 0,
    @AREA_ID INT = 0
AS
-- CLEAR DATA
DELETE FROM TBL_REF_03B
WHERE DATA_MONTH = @DATA_MONTH;

DECLARE @TYPE_SALE_CUSTOMER_USAGE_0_10 NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_USAGE_11_50 NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_USAGE_51_200 NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_SCHOOL NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_AGRI NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_SPECIAL NVARCHAR(200),
        @LICENSE_TYPE_ID INT,
        @BASED_TARIFF DECIMAL(18, 5),
        @SUBSIDY_TARIFF DECIMAL(18, 5);
SET @TYPE_SALE_CUSTOMER_USAGE_0_10 = N'លក់ឲ្យលំនៅដ្ឋានប្រើ មិនលើស  10 kWh/ខែ';
SET @TYPE_SALE_CUSTOMER_USAGE_11_50 = N'លក់ឲ្យលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ';
SET @TYPE_SALE_CUSTOMER_USAGE_51_200 = N'លក់ឲ្យលំនៅដ្ឋានប្រើចាប់ពី 51 ដល់ 200 kWh/ខែ';
SET @TYPE_SALE_CUSTOMER_SCHOOL = N'លក់ឲ្យសាលារៀន, មន្ទីរពេទ្យ, និងមណ្ឌលសុខភាព នៅតំបន់ជនបទ';
SET @TYPE_SALE_CUSTOMER_AGRI = N'លក់ឲ្យការបូមទឹកធ្វើកសិកម្មម៉ោង09:00យប់ ដល់07:00ព្រឹក';

SELECT @LICENSE_TYPE_ID = LICENSE_TYPE_ID,
       @SUBSIDY_TARIFF = SUBSIDY_TARIFF,
       @BASED_TARIFF = BASED_TARIFF
FROM dbo.TBL_TARIFF
WHERE DATA_MONTH = @DATA_MONTH;

IF (@LICENSE_TYPE_ID <> 2)
BEGIN
    INSERT INTO TBL_REF_03B
    (
        DATA_MONTH,
        ROW_NO,
        TYPE_OF_SALE_ID,
        TYPE_OF_SALE_NAME,
        TOTAL_CUSTOMER,
        TOTAL_POWER_SOLD,
        PRICE,
        SUBSIDY_RATE,
        SUBSIDY_AMOUNT,
        CURRENCY_ID,
        CURRENCY_NAME,
        CURRENCY_SIGN,
        IS_ACTIVE
    )
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_0_10,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE <= 10
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_11_50,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND (i.TOTAL_USAGE
          BETWEEN 11 AND 50
              )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'3',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_51_200,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND (i.TOTAL_USAGE
          BETWEEN 51 AND 200
              )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'4',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_SCHOOL,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'5',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_AGRI,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.IS_AGRICULTURE = 1
          AND i.SHIFT_ID = 2 -- NIGHT
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    --WHEN BASE TARFF < SUBSIDY TARIFF
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_0_10,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.BASED_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.BASED_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE <= 10
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_11_50,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.BASED_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.BASED_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND (i.TOTAL_USAGE
          BETWEEN 11 AND 50
              )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'3',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_51_200,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.BASED_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.BASED_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND (i.TOTAL_USAGE
          BETWEEN 51 AND 200
              )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'4',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_SCHOOL,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.BASED_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.BASED_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'5',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_AGRI,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.BASED_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.BASED_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.IS_AGRICULTURE = 1
          AND i.SHIFT_ID = 2 -- NIGHT
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN;
    -- BLANK RECORD
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '1'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'1',
               TYPE_OF_SALE_ID = 1,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_0_10,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '2'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'2',
               TYPE_OF_SALE_ID = 1,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_11_50,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '3'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'3',
               TYPE_OF_SALE_ID = 1,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_51_200,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '4'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'4',
               TYPE_OF_SALE_ID = 1,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_SCHOOL,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '5'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'5',
               TYPE_OF_SALE_ID = 1,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_AGRI,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1;
END;
-- END RUN_REF_03B_2020

GO

IF OBJECT_ID('RUN_REF_03B_2019') IS NOT NULL
    DROP PROC RUN_REF_03B_2019;
GO

CREATE PROC dbo.RUN_REF_03B_2019
    @DATA_MONTH DATETIME = '2019-01-01',
    @BILLING_CYCLE_ID INT = 0,
    @AREA_ID INT = 0
AS
-- CLEAR DATA
DELETE FROM TBL_REF_03B
WHERE DATA_MONTH = @DATA_MONTH;

DECLARE @TYPE_SALE_CUSTOMER_USAGE_0_10 NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_USAGE_11_50 NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_USAGE_51_200 NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_AGRI NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_SPECIAL NVARCHAR(200);
SET @TYPE_SALE_CUSTOMER_USAGE_0_10 = N'លក់ឲ្យលំនៅដ្ឋានប្រើ មិនលើស  10 kWh/ខែ';
SET @TYPE_SALE_CUSTOMER_USAGE_11_50 = N'លក់ឲ្យលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ';
SET @TYPE_SALE_CUSTOMER_USAGE_51_200 = N'លក់ឲ្យលំនៅដ្ឋានប្រើចាប់ពី 51 ដល់ 200 kWh/ខែ';
SET @TYPE_SALE_CUSTOMER_AGRI = N'លក់ឲ្យការបូមទឹកធ្វើកសិកម្មម៉ោង09:00យប់ ដល់07:00ព្រឹក';

INSERT INTO TBL_REF_03B
(
    DATA_MONTH,
    ROW_NO,
    TYPE_OF_SALE_ID,
    TYPE_OF_SALE_NAME,
    TOTAL_CUSTOMER,
    TOTAL_POWER_SOLD,
    PRICE,
    SUBSIDY_RATE,
    SUBSIDY_AMOUNT,
    CURRENCY_ID,
    CURRENCY_NAME,
    CURRENCY_SIGN,
    IS_ACTIVE
)
SELECT DATA_MONTH = @DATA_MONTH,
       ROW_NO = N'1',
       TYPE_OF_SALE_ID = 1,
       TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_0_10,
       TOTAL_CUSTOMER = COUNT(*),
       TOTAL_SOLD = SUM(i.TOTAL_USAGE),
       RATE = i.PRICE,
       SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
       SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
       i.CURRENCY_ID,
       i.CURRENCY_NAME,
       i.CURRENCY_SIGN,
       IS_ACTIVE = 1
FROM TBL_REF_INVOICE i
    LEFT JOIN TBL_TARIFF t
        ON t.CURRENCY_ID = i.CURRENCY_ID
           AND t.DATA_MONTH = @DATA_MONTH
WHERE i.DATA_MONTH = @DATA_MONTH
      AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
      AND i.TOTAL_USAGE <= 10
      AND
      (
          @BILLING_CYCLE_ID = 0
          OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
      )
      AND
      (
          @AREA_ID = 0
          OR i.AREA_ID = @AREA_ID
      )
GROUP BY t.SUBSIDY_TARIFF,
         i.PRICE,
         i.CURRENCY_ID,
         i.CURRENCY_NAME,
         i.CURRENCY_SIGN
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       ROW_NO = N'2',
       TYPE_OF_SALE_ID = 1,
       TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_11_50,
       TOTAL_CUSTOMER = COUNT(*),
       TOTAL_SOLD = SUM(i.TOTAL_USAGE),
       RATE = i.PRICE,
       SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
       SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
       i.CURRENCY_ID,
       i.CURRENCY_NAME,
       i.CURRENCY_SIGN,
       IS_ACTIVE = 1
FROM TBL_REF_INVOICE i
    LEFT JOIN TBL_TARIFF t
        ON t.CURRENCY_ID = i.CURRENCY_ID
           AND t.DATA_MONTH = @DATA_MONTH
WHERE i.DATA_MONTH = @DATA_MONTH
      AND (i.TOTAL_USAGE
      BETWEEN 11 AND 50
          )
      AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
      AND
      (
          @BILLING_CYCLE_ID = 0
          OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
      )
      AND
      (
          @AREA_ID = 0
          OR i.AREA_ID = @AREA_ID
      )
GROUP BY t.SUBSIDY_TARIFF,
         i.PRICE,
         i.CURRENCY_ID,
         i.CURRENCY_NAME,
         i.CURRENCY_SIGN
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       ROW_NO = N'3',
       TYPE_OF_SALE_ID = 1,
       TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_51_200,
       TOTAL_CUSTOMER = COUNT(*),
       TOTAL_SOLD = SUM(i.TOTAL_USAGE),
       RATE = i.PRICE,
       SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
       SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
       i.CURRENCY_ID,
       i.CURRENCY_NAME,
       i.CURRENCY_SIGN,
       IS_ACTIVE = 1
FROM TBL_REF_INVOICE i
    LEFT JOIN TBL_TARIFF t
        ON t.CURRENCY_ID = i.CURRENCY_ID
           AND t.DATA_MONTH = @DATA_MONTH
WHERE i.DATA_MONTH = @DATA_MONTH
      AND (i.TOTAL_USAGE
      BETWEEN 51 AND 200
          )
      AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
      AND
      (
          @BILLING_CYCLE_ID = 0
          OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
      )
      AND
      (
          @AREA_ID = 0
          OR i.AREA_ID = @AREA_ID
      )
GROUP BY t.SUBSIDY_TARIFF,
         i.PRICE,
         i.CURRENCY_ID,
         i.CURRENCY_NAME,
         i.CURRENCY_SIGN
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       ROW_NO = N'4',
       TYPE_OF_SALE_ID = 1,
       TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_AGRI,
       TOTAL_CUSTOMER = COUNT(*),
       TOTAL_SOLD = SUM(i.TOTAL_USAGE),
       RATE = i.PRICE,
       SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
       SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
       i.CURRENCY_ID,
       i.CURRENCY_NAME,
       i.CURRENCY_SIGN,
       IS_ACTIVE = 1
FROM TBL_REF_INVOICE i
    LEFT JOIN TBL_TARIFF t
        ON t.CURRENCY_ID = i.CURRENCY_ID
           AND t.DATA_MONTH = @DATA_MONTH
WHERE i.DATA_MONTH = @DATA_MONTH
      AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
      AND i.IS_AGRICULTURE = 1
      AND i.SHIFT_ID = 2 -- NIGHT
      AND
      (
          @AREA_ID = 0
          OR i.AREA_ID = @AREA_ID
      )
GROUP BY t.SUBSIDY_TARIFF,
         i.PRICE,
         i.CURRENCY_ID,
         i.CURRENCY_NAME,
         i.CURRENCY_SIGN;

-- BLANK RECORD
IF NOT EXISTS
(
    SELECT *
    FROM TBL_REF_03B
    WHERE DATA_MONTH = @DATA_MONTH
          AND ROW_NO = '1'
)
    INSERT INTO TBL_REF_03B
    (
        DATA_MONTH,
        ROW_NO,
        TYPE_OF_SALE_ID,
        TYPE_OF_SALE_NAME,
        TOTAL_CUSTOMER,
        TOTAL_POWER_SOLD,
        PRICE,
        SUBSIDY_RATE,
        SUBSIDY_AMOUNT,
        CURRENCY_ID,
        CURRENCY_NAME,
        CURRENCY_SIGN,
        IS_ACTIVE
    )
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_0_10,
           TOTAL_CUSTOMER = 0,
           TOTAL_SOLD = 0,
           RATE = 0,
           SUBSIDY_RATE = 0,
           SUBSIDY_AMOUNT = 0,
           CURRENCY_ID = 1,
           CURRENCY_NAME = '',
           CURRENCY_SIGN = '',
           IS_ACTIVE = 1;
IF NOT EXISTS
(
    SELECT *
    FROM TBL_REF_03B
    WHERE DATA_MONTH = @DATA_MONTH
          AND ROW_NO = '2'
)
    INSERT INTO TBL_REF_03B
    (
        DATA_MONTH,
        ROW_NO,
        TYPE_OF_SALE_ID,
        TYPE_OF_SALE_NAME,
        TOTAL_CUSTOMER,
        TOTAL_POWER_SOLD,
        PRICE,
        SUBSIDY_RATE,
        SUBSIDY_AMOUNT,
        CURRENCY_ID,
        CURRENCY_NAME,
        CURRENCY_SIGN,
        IS_ACTIVE
    )
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_11_50,
           TOTAL_CUSTOMER = 0,
           TOTAL_SOLD = 0,
           RATE = 0,
           SUBSIDY_RATE = 0,
           SUBSIDY_AMOUNT = 0,
           CURRENCY_ID = 1,
           CURRENCY_NAME = '',
           CURRENCY_SIGN = '',
           IS_ACTIVE = 1;
IF NOT EXISTS
(
    SELECT *
    FROM TBL_REF_03B
    WHERE DATA_MONTH = @DATA_MONTH
          AND ROW_NO = '3'
)
    INSERT INTO TBL_REF_03B
    (
        DATA_MONTH,
        ROW_NO,
        TYPE_OF_SALE_ID,
        TYPE_OF_SALE_NAME,
        TOTAL_CUSTOMER,
        TOTAL_POWER_SOLD,
        PRICE,
        SUBSIDY_RATE,
        SUBSIDY_AMOUNT,
        CURRENCY_ID,
        CURRENCY_NAME,
        CURRENCY_SIGN,
        IS_ACTIVE
    )
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'3',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_51_200,
           TOTAL_CUSTOMER = 0,
           TOTAL_SOLD = 0,
           RATE = 0,
           SUBSIDY_RATE = 0,
           SUBSIDY_AMOUNT = 0,
           CURRENCY_ID = 1,
           CURRENCY_NAME = '',
           CURRENCY_SIGN = '',
           IS_ACTIVE = 1;
IF NOT EXISTS
(
    SELECT *
    FROM TBL_REF_03B
    WHERE DATA_MONTH = @DATA_MONTH
          AND ROW_NO = '4'
)
    INSERT INTO TBL_REF_03B
    (
        DATA_MONTH,
        ROW_NO,
        TYPE_OF_SALE_ID,
        TYPE_OF_SALE_NAME,
        TOTAL_CUSTOMER,
        TOTAL_POWER_SOLD,
        PRICE,
        SUBSIDY_RATE,
        SUBSIDY_AMOUNT,
        CURRENCY_ID,
        CURRENCY_NAME,
        CURRENCY_SIGN,
        IS_ACTIVE
    )
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'4',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_AGRI,
           TOTAL_CUSTOMER = 0,
           TOTAL_SOLD = 0,
           RATE = 0,
           SUBSIDY_RATE = 0,
           SUBSIDY_AMOUNT = 0,
           CURRENCY_ID = 1,
           CURRENCY_NAME = '',
           CURRENCY_SIGN = '',
           IS_ACTIVE = 1;
-- END RUN_REF_03B_2019

GO

IF OBJECT_ID('REPORT_POWER_SOLD_2021') IS NOT NULL
    DROP PROC REPORT_POWER_SOLD_2021;

GO

CREATE PROC dbo.REPORT_POWER_SOLD_2021
    @YEAR_ID INT = 2021,
    @AREA_ID INT = 0,
    @BILLING_CYCLE_ID INT = 0
AS

/*
	@Kheang kimkhorn 2020-10-22
	- Remove validation with TBL_LICENSEE_CONNECTION
*/
DECLARE @MONTH DATETIME;
SET @MONTH = CAST(@YEAR_ID AS NVARCHAR(50)) + '-01-01';
-- collect all data
SELECT i.INVOICE_MONTH,
       c.CUSTOMER_TYPE_ID,
       i.CUSTOMER_CONNECTION_TYPE_ID,
       ct.CUSTOMER_TYPE_NAME,
       cct.CUSTOMER_CONNECTION_TYPE_NAME,
       USAGE = i.TOTAL_USAGE + ISNULL(adj.ADJUST_USAGE, 0),
       c.CUSTOMER_ID,
       i.PRICE
INTO #TMP
FROM TBL_INVOICE i
    INNER JOIN TBL_CUSTOMER c
        ON c.CUSTOMER_ID = i.CUSTOMER_ID
    INNER JOIN TBL_CUSTOMER_TYPE ct
        ON ct.CUSTOMER_TYPE_ID = c.CUSTOMER_TYPE_ID
    INNER JOIN TLKP_CUSTOMER_CONNECTION_TYPE cct
        ON cct.CUSTOMER_CONNECTION_TYPE_ID = i.CUSTOMER_CONNECTION_TYPE_ID
    OUTER APPLY
(
    SELECT ADJUST_USAGE = SUM(ADJUST_USAGE)
    FROM TBL_INVOICE_ADJUSTMENT
    WHERE INVOICE_ID = i.INVOICE_ID
) adj
WHERE YEAR(INVOICE_MONTH) = @YEAR_ID
      AND i.IS_SERVICE_BILL = 0
      AND
      (
          @AREA_ID = 0
          OR c.AREA_ID = @AREA_ID
      )
      AND
      (
          @BILLING_CYCLE_ID = 0
          OR c.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
      )
      AND c.IS_REACTIVE = 0
      AND i.INVOICE_STATUS NOT IN ( 3 )
UNION ALL
SELECT cr.CREDIT_MONTH,
       c.CUSTOMER_TYPE_ID,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       ct.CUSTOMER_TYPE_NAME,
       cct.CUSTOMER_CONNECTION_TYPE_NAME,
       USAGE = cr.USAGE,
       c.CUSTOMER_ID,
       cr.PRICE
FROM TBL_PREPAID_CREDIT cr
    INNER JOIN TBL_CUSTOMER c
        ON cr.CUSTOMER_ID = c.CUSTOMER_ID
    INNER JOIN TBL_CUSTOMER_TYPE ct
        ON ct.CUSTOMER_TYPE_ID = c.CUSTOMER_TYPE_ID
    INNER JOIN TLKP_CUSTOMER_CONNECTION_TYPE cct
        ON cct.CUSTOMER_CONNECTION_TYPE_ID = c.CUSTOMER_CONNECTION_TYPE_ID
ORDER BY i.INVOICE_MONTH,
         ct.CUSTOMER_TYPE_NAME;

-- for customer nomarl
SELECT INVOICE_MONTH,
       USAGE_0_10 = SUM(   CASE
                               WHEN USAGE
                                    BETWEEN 0 AND 10 THEN
                                   USAGE
                               ELSE
                                   0
                           END
                       ),
       USAGE_11_50 = SUM(   CASE
                                WHEN USAGE
                                     BETWEEN 11 AND 50 THEN
                                    USAGE
                                ELSE
                                    0
                            END
                        ),
       USAGE_51_200 = SUM(   CASE
                                 WHEN USAGE
                                      BETWEEN 51 AND 200 THEN
                                     USAGE
                                 ELSE
                                     0
                             END
                         ),
       USAGE_201_2000 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 201 AND 2000 THEN
                                       USAGE
                                   ELSE
                                       0
                               END
                           ),
       USAGE_OVER_2001 = SUM(   CASE
                                    WHEN USAGE >= 2001 THEN
                                        USAGE
                                    ELSE
                                        0
                                END
                            ),
       TOTAL_USAGE = SUM(USAGE),
       CUSTOMER_0_10 = SUM(   CASE
                                  WHEN USAGE
                                       BETWEEN 0 AND 10 THEN
                                      1
                                  ELSE
                                      0
                              END
                          ),
       CUSTOMER_11_50 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 11 AND 50 THEN
                                       1
                                   ELSE
                                       0
                               END
                           ),
       CUSTOMER_51_200 = SUM(   CASE
                                    WHEN USAGE
                                         BETWEEN 51 AND 200 THEN
                                        1
                                    ELSE
                                        0
                                END
                            ),
       CUSTOMER_201_2000 = SUM(   CASE
                                      WHEN USAGE
                                           BETWEEN 201 AND 2000 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ),
       CUSTOMER_OVER_2001 = SUM(   CASE
                                       WHEN USAGE >= 2001 THEN
                                           1
                                       ELSE
                                           0
                                   END
                               ),
       TOTAL_CUSTOMER = COUNT(*),
       CUSTOMER_CONNECTION_TYPE_ID,
       GROUP_ID = 6
INTO #RESULT
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
GROUP BY INVOICE_MONTH,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME
UNION ALL
SELECT INVOICE_MONTH,
       USAGE_0_10 = SUM(   CASE
                               WHEN USAGE
                                    BETWEEN 0 AND 10 THEN
                                   USAGE
                               ELSE
                                   0
                           END
                       ),
       USAGE_11_50 = SUM(   CASE
                                WHEN USAGE
                                     BETWEEN 11 AND 50 THEN
                                    USAGE
                                ELSE
                                    0
                            END
                        ),
       USAGE_51_200 = SUM(   CASE
                                 WHEN USAGE
                                      BETWEEN 51 AND 200 THEN
                                     USAGE
                                 ELSE
                                     0
                             END
                         ),
       USAGE_201_2000 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 201 AND 2000 THEN
                                       USAGE
                                   ELSE
                                       0
                               END
                           ),
       USAGE_OVER_2001 = SUM(   CASE
                                    WHEN USAGE >= 2001 THEN
                                        USAGE
                                    ELSE
                                        0
                                END
                            ),
       TOTAL_USAGE = SUM(USAGE),
       CUSTOMER_0_10 = SUM(   CASE
                                  WHEN USAGE
                                       BETWEEN 0 AND 10 THEN
                                      1
                                  ELSE
                                      0
                              END
                          ),
       CUSTOMER_11_50 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 11 AND 50 THEN
                                       1
                                   ELSE
                                       0
                               END
                           ),
       CUSTOMER_51_200 = SUM(   CASE
                                    WHEN USAGE
                                         BETWEEN 51 AND 200 THEN
                                        1
                                    ELSE
                                        0
                                END
                            ),
       CUSTOMER_201_2000 = SUM(   CASE
                                      WHEN USAGE
                                           BETWEEN 201 AND 2000 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ),
       CUSTOMER_OVER_2001 = SUM(   CASE
                                       WHEN USAGE >= 2001 THEN
                                           1
                                       ELSE
                                           0
                                   END
                               ),
       TOTAL_CUSTOMER = COUNT(*),
       CUSTOMER_CONNECTION_TYPE_ID,
       GROUP_ID = 5
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN ( 22, 23, 134, 135, 136, 137, 138, 139, 140 )
GROUP BY INVOICE_MONTH,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME
UNION ALL
SELECT INVOICE_MONTH,
       USAGE_0_10 = SUM(   CASE
                               WHEN USAGE
                                    BETWEEN 0 AND 10 THEN
                                   USAGE
                               ELSE
                                   0
                           END
                       ),
       USAGE_11_50 = SUM(   CASE
                                WHEN USAGE
                                     BETWEEN 11 AND 50 THEN
                                    USAGE
                                ELSE
                                    0
                            END
                        ),
       USAGE_51_200 = SUM(   CASE
                                 WHEN USAGE
                                      BETWEEN 51 AND 200 THEN
                                     USAGE
                                 ELSE
                                     0
                             END
                         ),
       USAGE_201_2000 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 201 AND 2000 THEN
                                       USAGE
                                   ELSE
                                       0
                               END
                           ),
       USAGE_OVER_2001 = SUM(   CASE
                                    WHEN USAGE >= 2001 THEN
                                        USAGE
                                    ELSE
                                        0
                                END
                            ),
       TOTAL_USAGE = SUM(USAGE),
       CUSTOMER_0_10 = SUM(   CASE
                                  WHEN USAGE
                                       BETWEEN 0 AND 10 THEN
                                      1
                                  ELSE
                                      0
                              END
                          ),
       CUSTOMER_11_50 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 11 AND 50 THEN
                                       1
                                   ELSE
                                       0
                               END
                           ),
       CUSTOMER_51_200 = SUM(   CASE
                                    WHEN USAGE
                                         BETWEEN 51 AND 200 THEN
                                        1
                                    ELSE
                                        0
                                END
                            ),
       CUSTOMER_201_2000 = SUM(   CASE
                                      WHEN USAGE
                                           BETWEEN 201 AND 2000 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ),
       CUSTOMER_OVER_2001 = SUM(   CASE
                                       WHEN USAGE >= 2001 THEN
                                           1
                                       ELSE
                                           0
                                   END
                               ),
       TOTAL_CUSTOMER = COUNT(*),
       CUSTOMER_CONNECTION_TYPE_ID,
       GROUP_ID = 4
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
GROUP BY INVOICE_MONTH,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME

UNION ALL
SELECT INVOICE_MONTH,
       USAGE_0_10 = SUM(   CASE
                               WHEN USAGE
                                    BETWEEN 0 AND 10 THEN
                                   USAGE
                               ELSE
                                   0
                           END
                       ),
       USAGE_11_50 = SUM(   CASE
                                WHEN USAGE
                                     BETWEEN 11 AND 50 THEN
                                    USAGE
                                ELSE
                                    0
                            END
                        ),
       USAGE_51_200 = SUM(   CASE
                                 WHEN USAGE
                                      BETWEEN 51 AND 200 THEN
                                     USAGE
                                 ELSE
                                     0
                             END
                         ),
       USAGE_201_2000 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 201 AND 2000 THEN
                                       USAGE
                                   ELSE
                                       0
                               END
                           ),
       USAGE_OVER_2001 = SUM(   CASE
                                    WHEN USAGE >= 2001 THEN
                                        USAGE
                                    ELSE
                                        0
                                END
                            ),
       TOTAL_USAGE = SUM(USAGE),
       CUSTOMER_0_10 = SUM(   CASE
                                  WHEN USAGE
                                       BETWEEN 0 AND 10 THEN
                                      1
                                  ELSE
                                      0
                              END
                          ),
       CUSTOMER_11_50 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 11 AND 50 THEN
                                       1
                                   ELSE
                                       0
                               END
                           ),
       CUSTOMER_51_200 = SUM(   CASE
                                    WHEN USAGE
                                         BETWEEN 51 AND 200 THEN
                                        1
                                    ELSE
                                        0
                                END
                            ),
       CUSTOMER_201_2000 = SUM(   CASE
                                      WHEN USAGE
                                           BETWEEN 201 AND 2000 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ),
       CUSTOMER_OVER_2001 = SUM(   CASE
                                       WHEN USAGE >= 2001 THEN
                                           1
                                       ELSE
                                           0
                                   END
                               ),
       TOTAL_CUSTOMER = COUNT(*),
       CUSTOMER_CONNECTION_TYPE_ID,
       GROUP_ID = 4
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID=141 AND PRICE=0.13700
GROUP BY INVOICE_MONTH,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME

UNION ALL
SELECT INVOICE_MONTH,
       USAGE_0_10 = SUM(   CASE
                               WHEN USAGE
                                    BETWEEN 0 AND 10 THEN
                                   USAGE
                               ELSE
                                   0
                           END
                       ),
       USAGE_11_50 = SUM(   CASE
                                WHEN USAGE
                                     BETWEEN 11 AND 50 THEN
                                    USAGE
                                ELSE
                                    0
                            END
                        ),
       USAGE_51_200 = SUM(   CASE
                                 WHEN USAGE
                                      BETWEEN 51 AND 200 THEN
                                     USAGE
                                 ELSE
                                     0
                             END
                         ),
       USAGE_201_2000 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 201 AND 2000 THEN
                                       USAGE
                                   ELSE
                                       0
                               END
                           ),
       USAGE_OVER_2001 = SUM(   CASE
                                    WHEN USAGE >= 2001 THEN
                                        USAGE
                                    ELSE
                                        0
                                END
                            ),
       TOTAL_USAGE = SUM(USAGE),
       CUSTOMER_0_10 = SUM(   CASE
                                  WHEN USAGE
                                       BETWEEN 0 AND 10 THEN
                                      1
                                  ELSE
                                      0
                              END
                          ),
       CUSTOMER_11_50 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 11 AND 50 THEN
                                       1
                                   ELSE
                                       0
                               END
                           ),
       CUSTOMER_51_200 = SUM(   CASE
                                    WHEN USAGE
                                         BETWEEN 51 AND 200 THEN
                                        1
                                    ELSE
                                        0
                                END
                            ),
       CUSTOMER_201_2000 = SUM(   CASE
                                      WHEN USAGE
                                           BETWEEN 201 AND 2000 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ),
       CUSTOMER_OVER_2001 = SUM(   CASE
                                       WHEN USAGE >= 2001 THEN
                                           1
                                       ELSE
                                           0
                                   END
                               ),
       TOTAL_CUSTOMER = COUNT(*),
       CUSTOMER_CONNECTION_TYPE_ID,
       GROUP_ID = -4
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID=141 AND PRICE=480
GROUP BY INVOICE_MONTH,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME

UNION ALL
SELECT INVOICE_MONTH,
       USAGE_0_10 = SUM(   CASE
                               WHEN USAGE
                                    BETWEEN 0 AND 10 THEN
                                   USAGE
                               ELSE
                                   0
                           END
                       ),
       USAGE_11_50 = SUM(   CASE
                                WHEN USAGE
                                     BETWEEN 11 AND 50 THEN
                                    USAGE
                                ELSE
                                    0
                            END
                        ),
       USAGE_51_200 = SUM(   CASE
                                 WHEN USAGE
                                      BETWEEN 51 AND 200 THEN
                                     USAGE
                                 ELSE
                                     0
                             END
                         ),
       USAGE_201_2000 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 201 AND 2000 THEN
                                       USAGE
                                   ELSE
                                       0
                               END
                           ),
       USAGE_OVER_2001 = SUM(   CASE
                                    WHEN USAGE >= 2001 THEN
                                        USAGE
                                    ELSE
                                        0
                                END
                            ),
       TOTAL_USAGE = SUM(USAGE),
       CUSTOMER_0_10 = SUM(   CASE
                                  WHEN USAGE
                                       BETWEEN 0 AND 10 THEN
                                      1
                                  ELSE
                                      0
                              END
                          ),
       CUSTOMER_11_50 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 11 AND 50 THEN
                                       1
                                   ELSE
                                       0
                               END
                           ),
       CUSTOMER_51_200 = SUM(   CASE
                                    WHEN USAGE
                                         BETWEEN 51 AND 200 THEN
                                        1
                                    ELSE
                                        0
                                END
                            ),
       CUSTOMER_201_2000 = SUM(   CASE
                                      WHEN USAGE
                                           BETWEEN 201 AND 2000 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ),
       CUSTOMER_OVER_2001 = SUM(   CASE
                                       WHEN USAGE >= 2001 THEN
                                           1
                                       ELSE
                                           0
                                   END
                               ),
       TOTAL_CUSTOMER = COUNT(*),
       CUSTOMER_CONNECTION_TYPE_ID,
       GROUP_ID = 4
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID=142 AND PRICE=0.15048
GROUP BY INVOICE_MONTH,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME

UNION ALL
SELECT INVOICE_MONTH,
       USAGE_0_10 = SUM(   CASE
                               WHEN USAGE
                                    BETWEEN 0 AND 10 THEN
                                   USAGE
                               ELSE
                                   0
                           END
                       ),
       USAGE_11_50 = SUM(   CASE
                                WHEN USAGE
                                     BETWEEN 11 AND 50 THEN
                                    USAGE
                                ELSE
                                    0
                            END
                        ),
       USAGE_51_200 = SUM(   CASE
                                 WHEN USAGE
                                      BETWEEN 51 AND 200 THEN
                                     USAGE
                                 ELSE
                                     0
                             END
                         ),
       USAGE_201_2000 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 201 AND 2000 THEN
                                       USAGE
                                   ELSE
                                       0
                               END
                           ),
       USAGE_OVER_2001 = SUM(   CASE
                                    WHEN USAGE >= 2001 THEN
                                        USAGE
                                    ELSE
                                        0
                                END
                            ),
       TOTAL_USAGE = SUM(USAGE),
       CUSTOMER_0_10 = SUM(   CASE
                                  WHEN USAGE
                                       BETWEEN 0 AND 10 THEN
                                      1
                                  ELSE
                                      0
                              END
                          ),
       CUSTOMER_11_50 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 11 AND 50 THEN
                                       1
                                   ELSE
                                       0
                               END
                           ),
       CUSTOMER_51_200 = SUM(   CASE
                                    WHEN USAGE
                                         BETWEEN 51 AND 200 THEN
                                        1
                                    ELSE
                                        0
                                END
                            ),
       CUSTOMER_201_2000 = SUM(   CASE
                                      WHEN USAGE
                                           BETWEEN 201 AND 2000 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ),
       CUSTOMER_OVER_2001 = SUM(   CASE
                                       WHEN USAGE >= 2001 THEN
                                           1
                                       ELSE
                                           0
                                   END
                               ),
       TOTAL_CUSTOMER = COUNT(*),
       CUSTOMER_CONNECTION_TYPE_ID,
       GROUP_ID = -3
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID=142 AND PRICE=480
GROUP BY INVOICE_MONTH,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME

UNION ALL
SELECT INVOICE_MONTH,
       USAGE_0_10 = SUM(   CASE
                               WHEN USAGE
                                    BETWEEN 0 AND 10 THEN
                                   USAGE
                               ELSE
                                   0
                           END
                       ),
       USAGE_11_50 = SUM(   CASE
                                WHEN USAGE
                                     BETWEEN 11 AND 50 THEN
                                    USAGE
                                ELSE
                                    0
                            END
                        ),
       USAGE_51_200 = SUM(   CASE
                                 WHEN USAGE
                                      BETWEEN 51 AND 200 THEN
                                     USAGE
                                 ELSE
                                     0
                             END
                         ),
       USAGE_201_2000 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 201 AND 2000 THEN
                                       USAGE
                                   ELSE
                                       0
                               END
                           ),
       USAGE_OVER_2001 = SUM(   CASE
                                    WHEN USAGE >= 2001 THEN
                                        USAGE
                                    ELSE
                                        0
                                END
                            ),
       TOTAL_USAGE = SUM(USAGE),
       CUSTOMER_0_10 = SUM(   CASE
                                  WHEN USAGE
                                       BETWEEN 0 AND 10 THEN
                                      1
                                  ELSE
                                      0
                              END
                          ),
       CUSTOMER_11_50 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 11 AND 50 THEN
                                       1
                                   ELSE
                                       0
                               END
                           ),
       CUSTOMER_51_200 = SUM(   CASE
                                    WHEN USAGE
                                         BETWEEN 51 AND 200 THEN
                                        1
                                    ELSE
                                        0
                                END
                            ),
       CUSTOMER_201_2000 = SUM(   CASE
                                      WHEN USAGE
                                           BETWEEN 201 AND 2000 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ),
       CUSTOMER_OVER_2001 = SUM(   CASE
                                       WHEN USAGE >= 2001 THEN
                                           1
                                       ELSE
                                           0
                                   END
                               ),
       TOTAL_CUSTOMER = COUNT(*),
       CUSTOMER_CONNECTION_TYPE_ID,
       GROUP_ID = 4
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID=143 AND PRICE=0.14248
GROUP BY INVOICE_MONTH,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME

UNION ALL
SELECT INVOICE_MONTH,
       USAGE_0_10 = SUM(   CASE
                               WHEN USAGE
                                    BETWEEN 0 AND 10 THEN
                                   USAGE
                               ELSE
                                   0
                           END
                       ),
       USAGE_11_50 = SUM(   CASE
                                WHEN USAGE
                                     BETWEEN 11 AND 50 THEN
                                    USAGE
                                ELSE
                                    0
                            END
                        ),
       USAGE_51_200 = SUM(   CASE
                                 WHEN USAGE
                                      BETWEEN 51 AND 200 THEN
                                     USAGE
                                 ELSE
                                     0
                             END
                         ),
       USAGE_201_2000 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 201 AND 2000 THEN
                                       USAGE
                                   ELSE
                                       0
                               END
                           ),
       USAGE_OVER_2001 = SUM(   CASE
                                    WHEN USAGE >= 2001 THEN
                                        USAGE
                                    ELSE
                                        0
                                END
                            ),
       TOTAL_USAGE = SUM(USAGE),
       CUSTOMER_0_10 = SUM(   CASE
                                  WHEN USAGE
                                       BETWEEN 0 AND 10 THEN
                                      1
                                  ELSE
                                      0
                              END
                          ),
       CUSTOMER_11_50 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 11 AND 50 THEN
                                       1
                                   ELSE
                                       0
                               END
                           ),
       CUSTOMER_51_200 = SUM(   CASE
                                    WHEN USAGE
                                         BETWEEN 51 AND 200 THEN
                                        1
                                    ELSE
                                        0
                                END
                            ),
       CUSTOMER_201_2000 = SUM(   CASE
                                      WHEN USAGE
                                           BETWEEN 201 AND 2000 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ),
       CUSTOMER_OVER_2001 = SUM(   CASE
                                       WHEN USAGE >= 2001 THEN
                                           1
                                       ELSE
                                           0
                                   END
                               ),
       TOTAL_CUSTOMER = COUNT(*),
       CUSTOMER_CONNECTION_TYPE_ID,
       GROUP_ID = -2
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID=143 AND PRICE=480
GROUP BY INVOICE_MONTH,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME
		 
UNION ALL
SELECT INVOICE_MONTH,
       USAGE_0_10 = SUM(   CASE
                               WHEN USAGE
                                    BETWEEN 0 AND 10 THEN
                                   USAGE
                               ELSE
                                   0
                           END
                       ),
       USAGE_11_50 = SUM(   CASE
                                WHEN USAGE
                                     BETWEEN 11 AND 50 THEN
                                    USAGE
                                ELSE
                                    0
                            END
                        ),
       USAGE_51_200 = SUM(   CASE
                                 WHEN USAGE
                                      BETWEEN 51 AND 200 THEN
                                     USAGE
                                 ELSE
                                     0
                             END
                         ),
       USAGE_201_2000 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 201 AND 2000 THEN
                                       USAGE
                                   ELSE
                                       0
                               END
                           ),
       USAGE_OVER_2001 = SUM(   CASE
                                    WHEN USAGE >= 2001 THEN
                                        USAGE
                                    ELSE
                                        0
                                END
                            ),
       TOTAL_USAGE = SUM(USAGE),
       CUSTOMER_0_10 = SUM(   CASE
                                  WHEN USAGE
                                       BETWEEN 0 AND 10 THEN
                                      1
                                  ELSE
                                      0
                              END
                          ),
       CUSTOMER_11_50 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 11 AND 50 THEN
                                       1
                                   ELSE
                                       0
                               END
                           ),
       CUSTOMER_51_200 = SUM(   CASE
                                    WHEN USAGE
                                         BETWEEN 51 AND 200 THEN
                                        1
                                    ELSE
                                        0
                                END
                            ),
       CUSTOMER_201_2000 = SUM(   CASE
                                      WHEN USAGE
                                           BETWEEN 201 AND 2000 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ),
       CUSTOMER_OVER_2001 = SUM(   CASE
                                       WHEN USAGE >= 2001 THEN
                                           1
                                       ELSE
                                           0
                                   END
                               ),
       TOTAL_CUSTOMER = COUNT(*),
       CUSTOMER_CONNECTION_TYPE_ID,
       GROUP_ID = 4
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID=24 AND PRICE=730
GROUP BY INVOICE_MONTH,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME

UNION ALL
SELECT INVOICE_MONTH,
       USAGE_0_10 = SUM(   CASE
                               WHEN USAGE
                                    BETWEEN 0 AND 10 THEN
                                   USAGE
                               ELSE
                                   0
                           END
                       ),
       USAGE_11_50 = SUM(   CASE
                                WHEN USAGE
                                     BETWEEN 11 AND 50 THEN
                                    USAGE
                                ELSE
                                    0
                            END
                        ),
       USAGE_51_200 = SUM(   CASE
                                 WHEN USAGE
                                      BETWEEN 51 AND 200 THEN
                                     USAGE
                                 ELSE
                                     0
                             END
                         ),
       USAGE_201_2000 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 201 AND 2000 THEN
                                       USAGE
                                   ELSE
                                       0
                               END
                           ),
       USAGE_OVER_2001 = SUM(   CASE
                                    WHEN USAGE >= 2001 THEN
                                        USAGE
                                    ELSE
                                        0
                                END
                            ),
       TOTAL_USAGE = SUM(USAGE),
       CUSTOMER_0_10 = SUM(   CASE
                                  WHEN USAGE
                                       BETWEEN 0 AND 10 THEN
                                      1
                                  ELSE
                                      0
                              END
                          ),
       CUSTOMER_11_50 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 11 AND 50 THEN
                                       1
                                   ELSE
                                       0
                               END
                           ),
       CUSTOMER_51_200 = SUM(   CASE
                                    WHEN USAGE
                                         BETWEEN 51 AND 200 THEN
                                        1
                                    ELSE
                                        0
                                END
                            ),
       CUSTOMER_201_2000 = SUM(   CASE
                                      WHEN USAGE
                                           BETWEEN 201 AND 2000 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ),
       CUSTOMER_OVER_2001 = SUM(   CASE
                                       WHEN USAGE >= 2001 THEN
                                           1
                                       ELSE
                                           0
                                   END
                               ),
       TOTAL_CUSTOMER = COUNT(*),
       CUSTOMER_CONNECTION_TYPE_ID,
       GROUP_ID = -1
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID=24 AND PRICE=480
GROUP BY INVOICE_MONTH,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME

UNION ALL
SELECT INVOICE_MONTH,
       USAGE_0_10 = SUM(   CASE
                               WHEN USAGE
                                    BETWEEN 0 AND 10 THEN
                                   USAGE
                               ELSE
                                   0
                           END
                       ),
       USAGE_11_50 = SUM(   CASE
                                WHEN USAGE
                                     BETWEEN 11 AND 50 THEN
                                    USAGE
                                ELSE
                                    0
                            END
                        ),
       USAGE_51_200 = SUM(   CASE
                                 WHEN USAGE
                                      BETWEEN 51 AND 200 THEN
                                     USAGE
                                 ELSE
                                     0
                             END
                         ),
       USAGE_201_2000 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 201 AND 2000 THEN
                                       USAGE
                                   ELSE
                                       0
                               END
                           ),
       USAGE_OVER_2001 = SUM(   CASE
                                    WHEN USAGE >= 2001 THEN
                                        USAGE
                                    ELSE
                                        0
                                END
                            ),
       TOTAL_USAGE = SUM(USAGE),
       CUSTOMER_0_10 = SUM(   CASE
                                  WHEN USAGE
                                       BETWEEN 0 AND 10 THEN
                                      1
                                  ELSE
                                      0
                              END
                          ),
       CUSTOMER_11_50 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 11 AND 50 THEN
                                       1
                                   ELSE
                                       0
                               END
                           ),
       CUSTOMER_51_200 = SUM(   CASE
                                    WHEN USAGE
                                         BETWEEN 51 AND 200 THEN
                                        1
                                    ELSE
                                        0
                                END
                            ),
       CUSTOMER_201_2000 = SUM(   CASE
                                      WHEN USAGE
                                           BETWEEN 201 AND 2000 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ),
       CUSTOMER_OVER_2001 = SUM(   CASE
                                       WHEN USAGE >= 2001 THEN
                                           1
                                       ELSE
                                           0
                                   END
                               ),
       TOTAL_CUSTOMER = COUNT(*),
       CUSTOMER_CONNECTION_TYPE_ID,
       GROUP_ID = 3
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN ( 11, 12, 13 )
GROUP BY INVOICE_MONTH,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME
UNION ALL
SELECT INVOICE_MONTH,
       USAGE_0_10 = SUM(   CASE
                               WHEN USAGE
                                    BETWEEN 0 AND 10 THEN
                                   USAGE
                               ELSE
                                   0
                           END
                       ),
       USAGE_11_50 = SUM(   CASE
                                WHEN USAGE
                                     BETWEEN 11 AND 50 THEN
                                    USAGE
                                ELSE
                                    0
                            END
                        ),
       USAGE_51_200 = SUM(   CASE
                                 WHEN USAGE
                                      BETWEEN 51 AND 200 THEN
                                     USAGE
                                 ELSE
                                     0
                             END
                         ),
       USAGE_201_2000 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 201 AND 2000 THEN
                                       USAGE
                                   ELSE
                                       0
                               END
                           ),
       USAGE_OVER_2001 = SUM(   CASE
                                    WHEN USAGE >= 2001 THEN
                                        USAGE
                                    ELSE
                                        0
                                END
                            ),
       TOTAL_USAGE = SUM(USAGE),
       CUSTOMER_0_10 = SUM(   CASE
                                  WHEN USAGE
                                       BETWEEN 0 AND 10 THEN
                                      1
                                  ELSE
                                      0
                              END
                          ),
       CUSTOMER_11_50 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 11 AND 50 THEN
                                       1
                                   ELSE
                                       0
                               END
                           ),
       CUSTOMER_51_200 = SUM(   CASE
                                    WHEN USAGE
                                         BETWEEN 51 AND 200 THEN
                                        1
                                    ELSE
                                        0
                                END
                            ),
       CUSTOMER_201_2000 = SUM(   CASE
                                      WHEN USAGE
                                           BETWEEN 201 AND 2000 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ),
       CUSTOMER_OVER_2001 = SUM(   CASE
                                       WHEN USAGE >= 2001 THEN
                                           1
                                       ELSE
                                           0
                                   END
                               ),
       TOTAL_CUSTOMER = COUNT(*),
       CUSTOMER_CONNECTION_TYPE_ID,
       GROUP_ID = 2
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID
BETWEEN '26' AND '97'
GROUP BY INVOICE_MONTH,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME
UNION ALL
SELECT INVOICE_MONTH,
       USAGE_0_10 = SUM(   CASE
                               WHEN USAGE
                                    BETWEEN 0 AND 10 THEN
                                   USAGE
                               ELSE
                                   0
                           END
                       ),
       USAGE_11_50 = SUM(   CASE
                                WHEN USAGE
                                     BETWEEN 11 AND 50 THEN
                                    USAGE
                                ELSE
                                    0
                            END
                        ),
       USAGE_51_200 = SUM(   CASE
                                 WHEN USAGE
                                      BETWEEN 51 AND 200 THEN
                                     USAGE
                                 ELSE
                                     0
                             END
                         ),
       USAGE_201_2000 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 201 AND 2000 THEN
                                       USAGE
                                   ELSE
                                       0
                               END
                           ),
       USAGE_OVER_2001 = SUM(   CASE
                                    WHEN USAGE >= 2001 THEN
                                        USAGE
                                    ELSE
                                        0
                                END
                            ),
       TOTAL_USAGE = SUM(USAGE),
       CUSTOMER_0_10 = SUM(   CASE
                                  WHEN USAGE
                                       BETWEEN 0 AND 10 THEN
                                      1
                                  ELSE
                                      0
                              END
                          ),
       CUSTOMER_11_50 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 11 AND 50 THEN
                                       1
                                   ELSE
                                       0
                               END
                           ),
       CUSTOMER_51_200 = SUM(   CASE
                                    WHEN USAGE
                                         BETWEEN 51 AND 200 THEN
                                        1
                                    ELSE
                                        0
                                END
                            ),
       CUSTOMER_201_2000 = SUM(   CASE
                                      WHEN USAGE
                                           BETWEEN 201 AND 2000 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ),
       CUSTOMER_OVER_2001 = SUM(   CASE
                                       WHEN USAGE >= 2001 THEN
                                           1
                                       ELSE
                                           0
                                   END
                               ),
       TOTAL_CUSTOMER = COUNT(*),
       CUSTOMER_CONNECTION_TYPE_ID,
       GROUP_ID = 1
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID
BETWEEN '98' AND '133'
GROUP BY INVOICE_MONTH,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME

UNION ALL
SELECT INVOICE_MONTH,
       USAGE_0_10 = SUM(   CASE
                               WHEN USAGE
                                    BETWEEN 0 AND 10 THEN
                                   USAGE
                               ELSE
                                   0
                           END
                       ),
       USAGE_11_50 = SUM(   CASE
                                WHEN USAGE
                                     BETWEEN 11 AND 50 THEN
                                    USAGE
                                ELSE
                                    0
                            END
                        ),
       USAGE_51_200 = SUM(   CASE
                                 WHEN USAGE
                                      BETWEEN 51 AND 200 THEN
                                     USAGE
                                 ELSE
                                     0
                             END
                         ),
       USAGE_201_2000 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 201 AND 2000 THEN
                                       USAGE
                                   ELSE
                                       0
                               END
                           ),
       USAGE_OVER_2001 = SUM(   CASE
                                    WHEN USAGE >= 2001 THEN
                                        USAGE
                                    ELSE
                                        0
                                END
                            ),
       TOTAL_USAGE = SUM(USAGE),
       CUSTOMER_0_10 = SUM(   CASE
                                  WHEN USAGE
                                       BETWEEN 0 AND 10 THEN
                                      1
                                  ELSE
                                      0
                              END
                          ),
       CUSTOMER_11_50 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 11 AND 50 THEN
                                       1
                                   ELSE
                                       0
                               END
                           ),
       CUSTOMER_51_200 = SUM(   CASE
                                    WHEN USAGE
                                         BETWEEN 51 AND 200 THEN
                                        1
                                    ELSE
                                        0
                                END
                            ),
       CUSTOMER_201_2000 = SUM(   CASE
                                      WHEN USAGE
                                           BETWEEN 201 AND 2000 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ),
       CUSTOMER_OVER_2001 = SUM(   CASE
                                       WHEN USAGE >= 2001 THEN
                                           1
                                       ELSE
                                           0
                                   END
                               ),
       TOTAL_CUSTOMER = COUNT(*),
       CUSTOMER_CONNECTION_TYPE_ID,
       GROUP_ID = -1
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID
IN (144)
GROUP BY INVOICE_MONTH,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME;



-- BUILD 12 MONTHs TABLE
CREATE TABLE #M
(
    COL_ORDER INT,
    ID INT,
    NAME NVARCHAR(250),
    INV_MONTH DATETIME,
    GROUP_ID INT
);
DECLARE @M INT;
SET @M = 1;
WHILE @M <= 12
BEGIN
    INSERT INTO #M
    VALUES
    (1, 1, N'​​អតិថិជនលំនៅដ្ឋាន', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 6);

    INSERT INTO #M
    VALUES
    (2, 22, N'​​អាជីវកម្មធុនតូច LV សាធារណៈ', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 5);
    INSERT INTO #M
    VALUES
    (3, 23, N'រដ្ឋបាល LV សាធារណៈ', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 5);
    INSERT INTO #M
    VALUES
    (4, 134, N'សេវាកម្មផ្សេងៗ LV សាធារណៈ', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 5);
	INSERT INTO #M
    VALUES
    (5, 135, N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV សាធារណៈ', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 5);
	INSERT INTO #M
    VALUES
    (6, 136, N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV សាធារណៈ', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 5);
	INSERT INTO #M
    VALUES
    (7, 137, N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV សាធារណៈ', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 5);
	INSERT INTO #M
    VALUES
    (8, 138, N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV សាធារណៈ', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 5);
	INSERT INTO #M
    VALUES
    (9, 139, N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV សាធារណៈ', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 5);
	INSERT INTO #M
    VALUES
    (10, 140, N'កសកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV សាធារណៈ', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 5);

    INSERT INTO #M
    VALUES
    (11, 9, N'​​សាលារៀន មន្ទីរពេទ្យ និងមណ្ឌលសុខភាពនៅជនបទ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 4);
    INSERT INTO #M
    VALUES
    (12, 141, N'អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម ទិញ MV',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 4);
    INSERT INTO #M
    VALUES
    (13, 141, N'​​អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម​ ទិញ MV ម៉ោង៩យប់ដល់៧ព្រឹក',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', -4);
    INSERT INTO #M
    VALUES
    (14, 142, N'អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្​ ទិញ LV ត្រង់ស្វូរអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 4);
    INSERT INTO #M
    VALUES
    (15, 142, N'​​អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម ទិញ​ LV ត្រង់ស្វូរអ្នកលក់ ម៉ោង៩យប់ដល់៧ព្រឹក',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', -3);
    INSERT INTO #M
    VALUES
    (16, 143, N'អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម ទិញ LV ត្រង់ស្វូរអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 4);
    INSERT INTO #M
    VALUES
    (17, 143, N'​​អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម ទិញ​ LV ត្រង់ស្វូរអ្នកទិញ ម៉ោង៩យប់ដល់៧ព្រឹក',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', -2);
	INSERT INTO #M
    VALUES
    (18, 24, N'​​អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម ទិញ​​ LV ភ្ជាប់ពីខ្សែសាធារណៈ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 4);
    INSERT INTO #M
    VALUES
    (19, 24, N'​​អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម ទីញ LV ៩យប់ដល់៧ព្រឹក',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', -1);

    INSERT INTO #M
    VALUES
    (20, 11, N'អ្នកកាន់អាជ្ញាបណ្ណ ទិញ MV', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 3);
    INSERT INTO #M
    VALUES
    (21, 13, N'អ្នកកាន់អាជ្ញាបណ្ណ ទិញ LV ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 3);
    INSERT INTO #M
    VALUES
    (22, 12, N'អ្នកកាន់អាជ្ញាបណ្ណ ទិញ LV ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 3);

    INSERT INTO #M
    VALUES
    (23, 26, N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ MV', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (24, 27, N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (25, 28, N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (26, 29, N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (27, 30, N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (28, 31, N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (29, 32, N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (30, 33, N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (31, 34, N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (32, 35, N'ឧស្សាហកម្មវិស័យរុករករ៉ែ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (33, 36, N'ឧស្សាហកម្មវិស័យរុករករ៉ែ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (34, 37, N'ឧស្សាហកម្មវិស័យរុករករ៉ែ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (35, 38, N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ MV', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (36, 39, N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (37, 40, N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (38, 41, N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (39, 42, N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (40, 43, N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (41, 44, N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (42, 45, N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (43, 46, N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (44, 47, N'ឧស្សាហកម្មវិស័យកម្មន្តសាល មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (45, 48, N'ឧស្សាហកម្មវិស័យកម្មន្តសាល មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (46, 49, N'ឧស្សាហកម្មវិស័យកម្មន្តសាល មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (47, 50, N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ MV', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (48, 51, N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (49, 52, N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (50, 53, N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (51, 54, N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (52, 55, N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (53, 56, N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (54, 57, N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (55, 58, N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (56, 59, N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (57, 60, N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (58, 61, N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (59, 62, N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ ឬនេសាទ ទិញ MV',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (60, 63, N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ ឬនេសាទ ទិញ LV ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (61, 64, N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ ឬនេសាទ ទិញ LV ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (62, 65, N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ ឬនេសាទ ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (63, 66, N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ ឬនេសាទ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (64, 67, N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ ឬនេសាទ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (65, 68, N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ ឬនេសាទ ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (66, 69, N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ ឬនេសាទ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (67, 70, N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ ឬនេសាទ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (68, 71, N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ ឬនេសាទ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (69, 72, N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ ឬនេសាទ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (70, 73, N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ ឬនេសាទ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (71, 74, N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ MV', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1',
     2  );
    INSERT INTO #M
    VALUES
    (72, 75, N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (73, 76, N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (74, 77, N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (75, 78, N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (76, 79, N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (77, 80, N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (78, 81, N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (79, 82, N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (80, 83, N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (81, 84, N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (82, 85, N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (83, 86, N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ MV', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (84, 87, N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (85, 88, N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (86, 89, N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (87, 90, N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (88, 91, N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (89, 92, N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (90, 93, N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (91, 94, N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (92, 95, N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (93, 96, N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (94, 97, N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (95, 98, N'ពាណិជ្ជកម្ម ទិញ MV', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (96, 99, N'ពាណិជ្ជកម្ម ទិញ LV ត្រង់ស្វូអ្នកលក់', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1',
     1  );
    INSERT INTO #M
    VALUES
    (97, 100, N'ពាណិជ្ជកម្ម ទិញ LV ត្រង់ស្វូអ្នកទិញ', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1',
     1);
    INSERT INTO #M
    VALUES
    (98, 101, N'ពាណិជ្ជកម្ម MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (99, 102, N'ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (100, 103, N'ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (101, 104, N'ពាណិជ្ជកម្ម ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (102, 105, N'ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (103, 106, N'ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (104, 107, N'ពាណិជ្ជកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (105, 108, N'ពាណិជ្ជកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (106, 109, N'ពាណិជ្ជកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (107, 110, N'រដ្ឋបាលសាធារណៈ ទិញ MV', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (108, 111, N'រដ្ឋបាលសាធារណៈ ទិញ LV ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (109, 112, N'រដ្ឋបាលសាធារណៈ ទិញ LV ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (110, 113, N'រដ្ឋបាលសាធារណៈ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (111, 114, N'រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (112, 115, N'រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (113, 116, N'រដ្ឋបាលសាធារណៈ ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (114, 117, N'រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (115, 118, N'រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (116, 119, N'រដ្ឋបាលសាធារណៈ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (117, 120, N'រដ្ឋបាលសាធារណៈ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (118, 121, N'រដ្ឋបាលសាធារណៈ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);

    INSERT INTO #M
    VALUES
    (119, 122, N'សេវាកម្មផ្សេងៗ ទិញ MV', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (120, 123, N'សេវាកម្មផ្សេងៗ ទិញ LV ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (121, 124, N'សេវាកម្មផ្សេងៗ ទិញ LV ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (122, 125, N'សេវាកម្មផ្សេងៗ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (123, 126, N'សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (124, 127, N'សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (125, 128, N'សេវាកម្មផ្សេងៗ ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (126, 129, N'សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (127, 130, N'សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (128, 131, N'សេវាកម្មផ្សេងៗ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (130, 132, N'សេវាកម្មផ្សេងៗ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (131, 133, N'សេវាកម្មផ្សេងៗ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
	INSERT INTO #M
    VALUES
    (132, 144, N'អាជីវករតូបលក់ដូរក្នុងផ្សារ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', -1);

    SET @M = @M + 1;
END;
SELECT INVOICE_MONTH = m.INV_MONTH,
       CUSTOMER_TYPE_NAME = m.NAME,
       USAGE_0_10 = ISNULL(USAGE_0_10, 0),
       USAGE_11_50 = ISNULL(USAGE_11_50, 0),
       USAGE_51_200 = ISNULL(USAGE_51_200, 0),
       USAGE_201_2000 = ISNULL(USAGE_201_2000, 0),
       USAGE_OVER_2001 = ISNULL(USAGE_OVER_2001, 0),
       TOTAL_USAGE = ISNULL(TOTAL_USAGE, 0),
       CUSTOMER_0_10 = ISNULL(CUSTOMER_0_10, 0),
       CUSTOMER_11_50 = ISNULL(CUSTOMER_11_50, 0),
       CUSTOMER_51_200 = ISNULL(CUSTOMER_51_200, 0),
       CUSTOMER_201_2000 = ISNULL(CUSTOMER_201_2000, 0),
       CUSTOMER_OVER_2001 = ISNULL(CUSTOMER_OVER_2001, 0),
       TOTAL_CUSTOMER = ISNULL(TOTAL_CUSTOMER, 0),
       GROUP_TYPE_ID = 1,
       m.ID,
       m.GROUP_ID
INTO #RESULT2
FROM #M m
    LEFT JOIN #RESULT r
        ON r.INVOICE_MONTH = m.INV_MONTH
           AND r.CUSTOMER_CONNECTION_TYPE_ID = m.ID
           AND m.GROUP_ID = r.GROUP_ID
ORDER BY m.INV_MONTH,
         m.COL_ORDER;
INSERT INTO #RESULT2
SELECT INVOICE_MONTH = NULL,
       N'សរុបថាមពល',
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0;
INSERT INTO #RESULT2
SELECT INVOICE_MONTH = NULL,
       m.NAME,
       d.USAGE_0_10,
       d.USAGE_11_50,
       d.USAGE_51_200,
       d.USAGE_201_2000,
       d.USAGE_OVER_2001,
       d.TOTAL_USAGE,
       CUSTOMER_0_10 = 0,
       CUSTOMER_11_50 = 0,
       CUSTOMER_51_200 = 0,
       CUSTOMER_201_2000 = 0,
       CUSTOMER_OVER_2001 = 0,
       TOTAL_CUSTOMER = 0,
       GROUP_TYPE_ID = 2,
       ID,
       GROUP_ID
FROM #M m
    OUTER APPLY
(
    SELECT USAGE_0_10 = SUM(USAGE_0_10),
           USAGE_11_50 = SUM(USAGE_11_50),
           USAGE_51_200 = SUM(USAGE_51_200),
           USAGE_201_2000 = SUM(USAGE_201_2000),
           USAGE_OVER_2001 = SUM(USAGE_OVER_2001),
           TOTAL_USAGE = SUM(TOTAL_USAGE)
    FROM #RESULT2 r
    WHERE r.ID = m.ID
          AND m.GROUP_ID = r.GROUP_ID
) d
WHERE m.INV_MONTH = @MONTH;
SELECT *
FROM #RESULT2
ORDER BY CASE
             WHEN INVOICE_MONTH IS NULL THEN
                 '2100-01-01'
             ELSE
                 INVOICE_MONTH
         END,
         15;
--- END OF REPORT_POWER_SOLD_2021
GO

IF NOT EXISTS(SELECT * FROM dbo.TBL_UTILITY WHERE UTILITY_ID = 102)
	INSERT INTO dbo.TBL_UTILITY (UTILITY_ID, UTILITY_NAME, DESCRIPTION, UTILITY_VALUE)
	VALUES (102 , N'E_FILLING_EXPORT_NUMBER', N'Max number of records in one file', N'5000')
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'PHONE' AND TABLE_NAME = 'TBL_COMPANY')
	ALTER TABLE TBL_COMPANY ADD PHONE nvarchar(50) NOT NULL DEFAULT '', VATTIN nvarchar(50) NOT NULL DEFAULT ''
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TBL_CUSTOMER' AND COLUMN_NAME = 'ROW_DATE')
	ALTER TABLE dbo.TBL_CUSTOMER ADD ROW_DATE DATETIME DEFAULT GETDATE() NOT NULL
GO

IF EXISTS (SELECT TOP 1 * FROM dbo.TBL_CUSTOMER WHERE ACTIVATE_DATE <> ROW_DATE)
	UPDATE dbo.TBL_CUSTOMER
	SET ROW_DATE = ACTIVATE_DATE, TITLE_ID = CASE WHEN GENDER_ID = 1 THEN -5 ELSE -4 END
GO

IF NOT EXISTS(SELECT * FROM dbo.TBL_SEQUENCE WHERE SEQUENCE_ID = -1 AND SEQUENCE_NAME = 'Adjustment')
	INSERT INTO dbo.TBL_SEQUENCE
	SELECT -1, 'Adjustment', 'ADJ{YY}{MM}-{N:6}', 0,GETDATE(), N'លេខរៀងវិក្កយបត្រកែសម្រួល', 1
GO

IF NOT EXISTS(SELECT * FROM dbo.TBL_SEQUENCE WHERE SEQUENCE_ID = -2 AND SEQUENCE_NAME = 'Reverse')
	INSERT INTO dbo.TBL_SEQUENCE
	SELECT -2, 'Reverse', 'VOID{YY}{MM}-{N:6}', 0,GETDATE(), N'លេខរៀងវិក្កយបត្រលុប', 1
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TBL_INVOICE' AND COLUMN_NAME = 'EXCHANGE_RATE')
	ALTER TABLE dbo.TBL_INVOICE ADD EXCHANGE_RATE DECIMAL(16,4) DEFAULT 1 NOT NULL
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TBL_INVOICE' AND COLUMN_NAME = 'EXCHANGE_RATE_DATE')
	ALTER TABLE dbo.TBL_INVOICE ADD EXCHANGE_RATE_DATE DATETIME DEFAULT '1900-01-01' NOT NULL
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TBL_INVOICE_DETAIL' AND COLUMN_NAME = 'REF_NO')
	ALTER TABLE TBL_INVOICE_DETAIL ADD [REF_NO] NVARCHAR(50) DEFAULT '' NOT NULL
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TBL_INVOICE_DETAIL' AND COLUMN_NAME = 'TRAN_DATE')
	ALTER TABLE dbo.TBL_INVOICE_DETAIL ADD [TRAN_DATE] DATETIME DEFAULT '1900-01-01' NOT NULL
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TBL_INVOICE_DETAIL' AND COLUMN_NAME='EXCHANGE_RATE')
	ALTER TABLE dbo.TBL_INVOICE_DETAIL ADD [EXCHANGE_RATE] DECIMAL(16,4) DEFAULT 1 NOT NULL
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TBL_INVOICE_DETAIL' AND COLUMN_NAME='EXCHANGE_RATE_DATE')
	ALTER TABLE dbo.TBL_INVOICE_DETAIL ADD [EXCHANGE_RATE_DATE] DATETIME DEFAULT '1900-01-01' NOT NULL
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE TABLE_NAME = 'TBL_INVOICE_USAGE')
	ALTER TABLE dbo.TBL_INVOICE_USAGE ADD PRIMARY KEY (INVOICE_USAGE_ID)
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TBL_INVOICE_DETAIL' AND COLUMN_NAME = 'TAX_AMOUNT')
	ALTER TABLE dbo.TBL_INVOICE_DETAIL ADD [TAX_AMOUNT] DECIMAL DEFAULT 0 NOT NULL
GO

IF OBJECT_ID('GET_EXCHANGE_RATE_DATE') IS NOT NULL
	DROP FUNCTION GET_EXCHANGE_RATE_DATE
GO

CREATE FUNCTION dbo.GET_EXCHANGE_RATE_DATE(
	@FROM_CURRENCY_ID INT,
	@TO_CURRENCY_ID INT,
	@DATE DATETIME
)RETURNS DATETIME
AS
BEGIN
	IF @FROM_CURRENCY_ID = @TO_CURRENCY_ID 
		RETURN '1900-01-01';

	DECLARE @RATE DATETIME;
	SELECT TOP 1 @RATE = CREATE_ON
	FROM TBL_EXCHANGE_RATE 
	WHERE CURRENCY_ID = @FROM_CURRENCY_ID 
		AND EXCHANGE_CURRENCY_ID = @TO_CURRENCY_ID
		AND CREATE_ON<=@DATE
	ORDER BY CREATE_ON DESC
	RETURN ISNULL(@RATE,'1900-01-01');
END
--- END OF GET_EXCHANGE_RATE_DATE
GO

IF OBJECT_ID('REVERSE_BILL') IS NOT NULL
	DROP PROC REVERSE_BILL
GO

CREATE PROC dbo.REVERSE_BILL
	@RUN_ID INT = 0,
	@REVERSE_BY NVARCHAR(250) = 'Khorn',
	@USER_CASH_DRAWER_ID INT = 0
WITH ENCRYPTION
AS
/*
@03-02-2021 By Kheang Kimkhorn
	1. CREATE
*/
DECLARE @DATE DATETIME;
SET @DATE = GETDATE();

INSERT INTO dbo.TBL_INVOICE_DETAIL
(INVOICE_ID, START_USAGE, END_USAGE, USAGE, PRICE, INVOICE_ITEM_ID, AMOUNT, CHARGE_DESCRIPTION, REF_NO, TRAN_DATE, EXCHANGE_RATE, EXCHANGE_RATE_DATE,TAX_AMOUNT)
SELECT
i.INVOICE_ID,
id.START_USAGE,
id.END_USAGE,
id.USAGE,
id.PRICE,
-3,
id.AMOUNT*-1,
id.REF_NO,
dbo.GET_SEQUENCE(-2, ROW_NUMBER() OVER ( ORDER BY id.INVOICE_DETAIL_ID DESC ) -1),
GETDATE(),
EXCHANGE_RATE = id.EXCHANGE_RATE,
EXCHANGE_RATE_DATE = id.EXCHANGE_RATE_DATE,
TAX_AMOUNT = id.TAX_AMOUNT * -1
FROM dbo.TBL_INVOICE i
INNER JOIN dbo.TBL_INVOICE_DETAIL id ON id.INVOICE_ID = i.INVOICE_ID
WHERE i.RUN_ID = @RUN_ID AND i.INVOICE_STATUS NOT IN (3)
ORDER BY i.INVOICE_ID, id.INVOICE_DETAIL_ID DESC

--- update sequence
UPDATE
    TBL_SEQUENCE
SET
    VALUE=CASE WHEN FORMAT LIKE '%{Y%' AND DATEDIFF(YEAR, DATE, @DATE)<>0 THEN @@rowcount
          WHEN FORMAT LIKE '%{M%' AND DATEDIFF(MONTH, DATE, @DATE)<>0 THEN @@rowcount ELSE VALUE+@@rowcount END,
    DATE=@DATE
WHERE SEQUENCE_ID=-2;

UPDATE dbo.TBL_INVOICE
SET INVOICE_STATUS = 3,
	INVOICE_TITLE = INVOICE_TITLE + N'(លុប)',
	TOTAL_AMOUNT  = 0,
	SETTLE_AMOUNT = 0
WHERE RUN_ID = @RUN_ID
AND INVOICE_STATUS NOT IN (3)
--- END OF REVERSE_BILL
GO

IF OBJECT_ID('REPORT_INVOICE_REVERSED') IS NOT NULL
	DROP PROC REPORT_INVOICE_REVERSED
GO

CREATE PROC dbo.REPORT_INVOICE_REVERSED
    @D1 DATETIME='2020-01-01',
    @D2 DATETIME='2021-06-01',
    @CURRENCY_ID INT=0,
    @AREA_ID INT=0,
    @BILLING_CYCLE_ID INT=0,
    @DATA_MONTH DATETIME='1900-01-01'
AS
/*
@31-07-2020 By Kheang Kimkhorn
	1. CREATE
@03-02-2021 By Kheang Kimkhorn
	1. Modify REF_NO, TRAN_DATE ect
*/

SET @D1=DATEADD(D, 0, DATEDIFF(D, 0, @D1));
SET @D2=DATEADD(S, -1, DATEADD(D, 1, DATEDIFF(D, 0, @D2)));

SELECT 
	cx.CURRENCY_ID,
    cx.CURRENCY_NAME,
    cx.CURRENCY_SING,
    c.CUSTOMER_CODE,
    CUSTOMER_NAME=c.LAST_NAME_KH+' '+c.FIRST_NAME_KH,
    a.AREA_NAME,
    i.INVOICE_NO,
	id.REF_NO,
    i.INVOICE_DATE,
	id.TRAN_DATE,
    i.INVOICE_MONTH,
    i.INVOICE_TITLE,
    id.AMOUNT
FROM dbo.TBL_INVOICE i 
INNER JOIN dbo.TBL_INVOICE_DETAIL id ON id.INVOICE_ID = i.INVOICE_ID
INNER JOIN dbo.TBL_CUSTOMER c ON c.CUSTOMER_ID = i.CUSTOMER_ID
INNER JOIN dbo.TBL_AREA a ON a.AREA_ID = c.AREA_ID
INNER JOIN dbo.TLKP_CURRENCY cx ON cx.CURRENCY_ID = i.CURRENCY_ID
WHERE id.TRAN_DATE BETWEEN @D1 AND @D2
	AND i.INVOICE_STATUS = 3
	AND id.INVOICE_ITEM_ID = -3
	AND ( @CURRENCY_ID=0 OR cx.CURRENCY_ID=@CURRENCY_ID ) 
	AND ( @AREA_ID=0 OR c.AREA_ID=@AREA_ID ) AND (  @BILLING_CYCLE_ID=0 OR c.BILLING_CYCLE_ID=@BILLING_CYCLE_ID ) 
	AND (@DATA_MONTH='1900-01-01' OR i.INVOICE_MONTH=@DATA_MONTH )
ORDER BY  i.INVOICE_NO, id.REF_NO
-- END OF REPORT_INVOICE_REVERSED
GO

IF OBJECT_ID('RUN_BILL') IS NOT NULL
	DROP PROC RUN_BILL
GO

CREATE PROC dbo.RUN_BILL
	@MONTH DATETIME='2011-1-1',
	@START_DATE DATETIME='2011-1-1',
	@END_DATE DATETIME='2011-1-31',
	@CYCLE_ID INT=1,
	@CREATE_BY NVARCHAR(100)='ADMIN',
	@DUE_DATE DATETIME='2011-2-20',
	@START_PAY_DATE DATETIME='2011-2-01'
	WITH ENCRYPTION 
AS 
/*
BY	: RY RITH
SINCE: 2011-01-17 
SINCE: 2011-02-25 Update Discount
SINCE: 2011-03-03 Update Service Bill
SINCE: 2011-03-31 Update Utility Value
SINCE: 2011-04-06 Filter Close and Delete Custoemr 
SINCE: 2011-04-20 Update Filter Error(Change From CustomerType to Status)
SINCE: 2011-04-25 Add @DUE_DATE 
SINEC: 2011-07-25 o Revise Invoice No
				  o Add Invoice Order by customer for FakeDB
				  o Add log billing summary
				  o Add log AR before and after run bill.
				  o Add log Invoice(s)
SINCE: 2011-12-22 multiplier support
SINCE: 2011-12-27 3-phase meter support
SINCE: 2012-01-18 recurring service
SINCE: 2012-01-25 add keep remain billing amount.
SINCE: 2011-01-27 fix sequence.
SINCE: 2012-03-02 fix keep remain billing amount (reverse condition).
SINCE: 2012-03-02 fix unlimit (change from 1m -> 100m)
SINCE: 2012-08-27 ... 
SINCE: 2014-03-21 Rith, fix inserting duplicate value TBL_RUN_BILL_INVOICE
SINCE: 2014-03-21 Rith, add Reactive Power Calculation.
SINCE: 2016-01-16 Rith, add Price extra charge.
SINCE: 2016-03-29 Rith, add Price, based price history
SINCE: 2016-04-23 Rith, floor TOTAL_USAGE
                        discount calculation based on TOTAL_USAGE (before TOTAL_USAGE-DISCOUNT_USAGE)
SINCE: 2018-03-02 Smey, LOG AGIN before and After RUN_BILL
SINCE: 2018-06-13 Smey, add ROW_DATE to TBL_INVOICE
SINCE: 2019-03-04 Smey, add CUSTOMER_CONN
SINCE: 2020-09-10 Khorn, seperate sequence invoice bill and service
*/

TRUNCATE TABLE TMP_BILL_USAGE;
TRUNCATE TABLE TMP_BILL_USAGE_SUMMARY;
TRUNCATE TABLE TMP_BILL_USAGE_RATE; 

-- DECLARE ENUMERATION *******************************************************************
DECLARE @SEQUENCE_INVOICE INT,
		@SEQUENCE_SERVICE INT,
		@CUSTOMER_STATUS_ACTIVE INT,
		@CUSTOMER_STATUS_BLOCKED INT,
		@CUSTOMER_IN_PRODUCTION INT,
		@INVOICE_STATUS_OPEN INT,
		@INVOICE_STATUS_CLOSE INT,
		@FIX_CHARGE_STATUS_OPEN INT,
		@FIX_CHARGE_STATUS_CLOSE INT, 
		@DISCOUNT_TYPE_USAGE INT,
		@DISCOUNT_TYPE_AMOUNT INT,
		@INVOICE_ITEM_POWER INT, 
		@MAX_DAY_TO_IGNORE_FIXED_AMOUNT INT,
		@NOW DATETIME;
 
SET @CUSTOMER_STATUS_ACTIVE=2;
SET @CUSTOMER_STATUS_BLOCKED=3;
SET @CUSTOMER_IN_PRODUCTION=-1;
SET @INVOICE_STATUS_OPEN=1;
SET @INVOICE_STATUS_CLOSE=4;
SET @FIX_CHARGE_STATUS_OPEN=1;
SET @FIX_CHARGE_STATUS_CLOSE=4; 
SET @DISCOUNT_TYPE_USAGE=1;
SET @DISCOUNT_TYPE_AMOUNT=2;
SET @INVOICE_ITEM_POWER=1; 
SET @NOW=GETDATE();

SET @MAX_DAY_TO_IGNORE_FIXED_AMOUNT=ISNULL((SELECT UTILITY_VALUE 
											 FROM TBL_UTILITY 
											 WHERE UTILITY_ID=16),0); 

SET @SEQUENCE_INVOICE = (SELECT SEQUENCE_ID FROM dbo.TBL_SEQUENCE_TYPE WHERE SEQUENCE_TYPE_ID = 1)
SET @SEQUENCE_SERVICE = (SELECT SEQUENCE_ID FROM dbo.TBL_SEQUENCE_TYPE WHERE SEQUENCE_TYPE_ID = 2)

DECLARE @INVOICE_TITLE NVARCHAR(200),
		@RUN_ID INT,
		@DATE DATETIME; 
SET @DATE = GETDATE();
SET @INVOICE_TITLE = N'ថ្លៃអគ្គិសនី  ខែ'+CONVERT(NVARCHAR,MONTH(@MONTH))+N'ឆ្នាំ'+CONVERT(NVARCHAR,YEAR(@MONTH));

--- ExchangeRate
;WITH c AS (
SELECT CURRENCY_ID, EXCHANGE_RATE, CREATE_ON, 
	rowNo = ROW_NUMBER() OVER (PARTITION BY CURRENCY_ID ORDER BY CREATE_ON DESC)
FROM dbo.TBL_EXCHANGE_RATE
WHERE EXCHANGE_CURRENCY_ID = 1)
SELECT * INTO #tmpExchangeRate FROM c WHERE c.rowNo = 1

-- INSERT A RUN BILL RECORD
INSERT INTO TBL_RUN_BILL
   VALUES(@CYCLE_ID,@DATE,@CREATE_BY,@MONTH,0,0,0,0,0,0,0,1);

SET @RUN_ID=@@identity; 

-- LOG AGING BEFORE RUN BILL
DECLARE @PERIOD INT;
SET @PERIOD = 15;
INSERT INTO TBL_RUN_BILL_AGING(RUN_ID,DATE,IS_FEFORE,CUSTOMER_ID,CURRENCY_ID,BAL1,BAL2,BAL3,BAL4,TOTAL)
SELECT RUN_ID= @RUN_ID,
	   DATE  = @DATE,
	   IS_BEFORE = 1,
	   c.CUSTOMER_ID,
       i.CURRENCY_ID,
	   BAL1  = SUM( CASE WHEN DATEDIFF(D,i.INVOICE_DATE,@DATE) BETWEEN @PERIOD*0 AND @PERIOD*1 THEN SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT,0) + ISNULL(aa.ADJUST_AMOUNT,0) - ISNULL(p.PAY_AMOUNT,0) ELSE 0 END ),
	   BAL2  = SUM( CASE WHEN DATEDIFF(D,i.INVOICE_DATE,@DATE) BETWEEN @PERIOD*1+1 AND @PERIOD*2 THEN SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT,0) + ISNULL(aa.ADJUST_AMOUNT,0) - ISNULL(p.PAY_AMOUNT,0) ELSE 0 END ),
	   BAL3  = SUM( CASE WHEN DATEDIFF(D,i.INVOICE_DATE,@DATE) BETWEEN @PERIOD*2+1 AND @PERIOD*3 THEN SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT,0) + ISNULL(aa.ADJUST_AMOUNT,0) - ISNULL(p.PAY_AMOUNT,0) ELSE 0 END ),
	   BAL4  = SUM( CASE WHEN DATEDIFF(D,i.INVOICE_DATE,@DATE) > @PERIOD*3                       THEN SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT,0) + ISNULL(aa.ADJUST_AMOUNT,0) - ISNULL(p.PAY_AMOUNT,0) ELSE 0 END ),
       TOTAL = SUM( SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT,0) + ISNULL(aa.ADJUST_AMOUNT,0) - ISNULL(p.PAY_AMOUNT,0)) 
FROM TBL_INVOICE i
INNER JOIN TBL_CUSTOMER c ON i.CUSTOMER_ID = c.CUSTOMER_ID
OUTER APPLY (
    SELECT SUM(PD.PAY_AMOUNT) AS PAY_AMOUNT
    FROM TBL_PAYMENT_DETAIL PD INNER JOIN TBL_PAYMENT P ON P.PAYMENT_ID=PD.PAYMENT_ID
    WHERE P.PAY_DATE <@DATE AND INVOICE_ID=i.INVOICE_ID
) p -- PAYMENT
OUTER APPLY (
    SELECT SUM(IA.ADJUST_AMOUNT) AS ADJUST_AMOUNT
    FROM TBL_INVOICE_ADJUSTMENT IA 
    WHERE INVOICE_ID =i.INVOICE_ID
) ta -- TOTAL ADJUSTMENT
OUTER APPLY (
    SELECT SUM(IA.ADJUST_AMOUNT) AS ADJUST_AMOUNT
    FROM TBL_INVOICE_ADJUSTMENT IA 
    WHERE IA.CREATE_ON <= @DATE AND INVOICE_ID = i.INVOICE_ID
) aa  -- ACCUMULATED ADJUSTMENT 	
WHERE	i.INVOICE_STATUS NOT IN (3)
		--AND (c.CUSTOMER_TYPE_ID = @CUSTOMER_TYPE_ID OR @CUSTOMER_TYPE_ID = 0)
		AND (INVOICE_DATE <= @DATE)
		AND c.BILLING_CYCLE_ID=@CYCLE_ID --LOG AGIN before RUN_BILL
GROUP BY c.CUSTOMER_ID,i.CURRENCY_ID
HAVING SUM( SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT,0) + ISNULL(aa.ADJUST_AMOUNT,0) - ISNULL(p.PAY_AMOUNT,0))>0;
-- END LOG AGING BEFORE RUN

-- RECURRING SERVICE *********************************************************************************
INSERT INTO TBL_INVOICE
SELECT  INVOICE_NO='',INVOICE_MONTH=@MONTH,
		METER_CODE='',START_DATE=@MONTH,END_DATE=@MONTH,
		INVOICE_DATE=@DATE,START_USAGE=0,END_USAGE=0,
		CUSTOMER_ID=c.CUSTOMER_ID,PAID_AMOUNT=0,
		TOTAL_USAGE=0,
		CURRENCY_ID=i.CURRENCY_ID,
		CYCLE_ID=@CYCLE_ID, 
		DUE_DATE=@DUE_DATE,
		INVOICE_STATUS=1,IS_SERVICE_BILL=1,
		-- DUMMY AS INVOICE_ITEM_ID
		PRINT_COUNT=s.INVOICE_ITEM_ID,
		RUN_ID=@RUN_ID,
		FORWARD_AMOUNT=0,TOTAL_AMOUNT=QTY*i.PRICE,SETTLE_AMOUNT=dbo.ROUND_BY_CURRENCY(QTY*PRICE,i.CURRENCY_ID),
		DISCOUNT_USAGE=0,DISCOUNT_USAGE_NAME='',					
		DISCOUNT_AMOUNT=0,DISCOUNT_AMOUNT_NAME='',
		INVOICE_TITLE=INVOICE_ITEM_NAME,
		START_PAY_DATE = @START_PAY_DATE,
        PRICE_ID=0, PRICE=0, BASED_PRICE=0,
		ROW_DATE=@NOW,
		c.CUSTOMER_CONNECTION_TYPE_ID,
		EXCHANGE_RATE = ISNULL(r.EXCHANGE_RATE, 1),
	EXCHANGE_RATE_DATE = ISNULL(r.CREATE_ON, GETDATE())
FROM TBL_CUSTOMER c
INNER JOIN TBL_CUSTOMER_SERVICE s ON c.CUSTOMER_ID = s.CUSTOMER_ID 
INNER JOIN TBL_INVOICE_ITEM i ON i.INVOICE_ITEM_ID = s.INVOICE_ITEM_ID
LEFT JOIN #tmpExchangeRate r ON r.CURRENCY_ID = i.CURRENCY_ID
WHERE c.BILLING_CYCLE_ID=@CYCLE_ID
		AND c.STATUS_ID IN (2,3)
		AND s.IS_ACTIVE=1
		AND s.NEXT_BILLING_MONTH=@MONTH;

INSERT INTO TBL_INVOICE_DETAIL
SELECT	INVOICE_ID	= i.INVOICE_ID,
		START_USAGE	= 0,
		END_USAGE	= 0,
		USAGE		= s.QTY,
		PRICE		= t.PRICE,
		INVOICE_ITEM_ID	= t.INVOICE_ITEM_ID,
		AMOUNT			= s.QTY*t.PRICE,
		CHARGE_DESCRIPTION = t.INVOICE_ITEM_NAME,
		REF_NO = '',
		TRAN_DATE = i.INVOICE_DATE,
		EXCHANGE_RATE = ISNULL(r.EXCHANGE_RATE, 1),
		EXCHANGE_RATE_DATE = ISNULL(r.CREATE_ON, GETDATE()),
		TAX_AMOUNT = 0
FROM TBL_INVOICE i
INNER JOIN TBL_CUSTOMER_SERVICE s ON s.INVOICE_ITEM_ID = i.PRINT_COUNT AND i.CUSTOMER_ID = s.CUSTOMER_ID 
INNER JOIN TBL_INVOICE_ITEM t ON t.INVOICE_ITEM_ID = s.INVOICE_ITEM_ID
LEFT JOIN #tmpExchangeRate r ON r.CURRENCY_ID = i.CURRENCY_ID
WHERE i.INVOICE_MONTH=@MONTH AND s.IS_ACTIVE=1 AND i.INVOICE_STATUS <> 3;

UPDATE TBL_CUSTOMER_SERVICE
SET LAST_BILLING_MONTH = @MONTH,
	NEXT_BILLING_MONTH = DATEADD(M,i.RECURRING_MONTH,@MONTH)
FROM TBL_CUSTOMER_SERVICE s
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = s.CUSTOMER_ID
INNER JOIN TBL_INVOICE_ITEM i ON i.INVOICE_ITEM_ID = s.INVOICE_ITEM_ID
WHERE	c.BILLING_CYCLE_ID=@CYCLE_ID
		AND c.STATUS_ID IN (2,3)
		AND s.IS_ACTIVE=1
		AND s.NEXT_BILLING_MONTH=@MONTH;

-- clear dummy invoice_item_id
UPDATE TBL_INVOICE
SET PRINT_COUNT = 0 
WHERE	INVOICE_MONTH=@MONTH 
		AND IS_SERVICE_BILL=1 
		AND RUN_ID = @RUN_ID;

-- USAGE AND INVOICE *******************************************************************************
-- SNAPSHOT A CURRENT BILLING MONTH USAGE
-- FROM TBL_USAGE TO TEMP TABLE FOR PERFORMANCE
-- FOR METER NEW CYCLE, GET MAX_USAGE TO CAL CULATE TOTAL USAGE
INSERT INTO TMP_BILL_USAGE
SELECT	USAGE_ID,
		CUSTOMER_ID =c.CUSTOMER_ID,
		METER_ID,
		START_USAGE,
		END_USAGE,
		TOTAL_USAGE = MULTIPLIER*(CASE 
			WHEN IS_METER_RENEW_CYCLE=0 THEN END_USAGE-START_USAGE
			ELSE (SELECT CONVERT(INT,REPLICATE('9',LEN(CONVERT(NVARCHAR,CONVERT(INT,START_USAGE))))))-START_USAGE+END_USAGE+1
		END),
		MULTIPLIER,
		USAGE_CUSTOMER_ID=(CASE WHEN c.USAGE_CUSTOMER_ID =0 THEN c.CUSTOMER_ID ELSE c.USAGE_CUSTOMER_ID END)
FROM TBL_USAGE u 
INNER JOIN TBL_CUSTOMER c ON u.CUSTOMER_ID=c.CUSTOMER_ID
WHERE c.BILLING_CYCLE_ID=@CYCLE_ID 
		AND u.USAGE_MONTH=@MONTH
		AND u.COLLECTOR_ID <> 0
		AND c.STATUS_ID IN (2,3);

-- MAKE SURE ALL INVOICE TOTAL USAGE MUST BE INTEGER
UPDATE TMP_BILL_USAGE SET TOTAL_USAGE=FLOOR(TOTAL_USAGE);
 
-- SUMMARIZE USAGE,
-- ONE CUSTOMER HAVE ONLY ONE RECORD
-- IF CUSTOMER HAVE TWO OR MORE USAGE RECORD(S)
--  START USAGE IS THE START OF THE THE FIRST RECORD
--  END USAGE IS THE END OF THE LAST RECORD
--  TOTAL USAGE, IS SUM UP OF ALL RECORD(S)' TOTAL USAGE
INSERT INTO TMP_BILL_USAGE_SUMMARY
SELECT	c.CUSTOMER_ID,
		START_USAGE=(SELECT TOP 1 START_USAGE FROM TMP_BILL_USAGE  WHERE CUSTOMER_ID=c.CUSTOMER_ID ORDER BY USAGE_ID DESC),
		END_USAGE=(SELECT TOP 1 END_USAGE FROM TMP_BILL_USAGE  WHERE CUSTOMER_ID=c.CUSTOMER_ID ORDER BY USAGE_ID DESC),
		TOTAL_USAGE=SUM(u.TOTAL_USAGE),
		DISCOUNT_USAGE=0,
		DISCOUNT_USAGE_NAME=''
FROM TMP_BILL_USAGE u
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = u.USAGE_CUSTOMER_ID
GROUP BY c.CUSTOMER_ID;

-- OVERRIDE RE-ACTIVE POWER.
UPDATE sr
SET TOTAL_USAGE = dbo.GET_REACTIVE(r.REACTIVE_RULE_ID,sa.TOTAL_USAGE,sr.TOTAL_USAGE)
FROM TMP_BILL_USAGE_SUMMARY sr
INNER JOIN TBL_CUSTOMER r ON r.CUSTOMER_ID=sr.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER a ON r.INVOICE_CUSTOMER_ID=a.CUSTOMER_ID
INNER JOIN TMP_BILL_USAGE_SUMMARY sa ON sa.CUSTOMER_ID=a.CUSTOMER_ID
WHERE r.IS_REACTIVE=1;

-- SUMMARY DISCOUNT USAGE FOR CUSTOMER INPRODUCTION
UPDATE  TMP_BILL_USAGE_SUMMARY
SET		DISCOUNT_USAGE=s.TOTAL_USAGE,
		DISCOUNT_USAGE_NAME=N'ប្រើក្នុងផលិតកម្ម'
FROM TBL_CUSTOMER c
INNER JOIN TMP_BILL_USAGE_SUMMARY s ON s.CUSTOMER_ID=c.CUSTOMER_ID
WHERE CUSTOMER_TYPE_ID=-1 AND BILLING_CYCLE_ID=@CYCLE_ID;
 
-- SUMMARY DISCOUNT USAGE FOR DISCOUNT CUSTOMER
UPDATE	TMP_BILL_USAGE_SUMMARY
SET		DISCOUNT_USAGE=(CASE WHEN IS_PERCENTAGE=1 
							THEN CONVERT(INT,s.TOTAL_USAGE*DISCOUNT/100) 
							ELSE (CASE WHEN s.TOTAL_USAGE>d.DISCOUNT 
										THEN d.DISCOUNT 
										ELSE s.TOTAL_USAGE 
								 END)
						END),
		DISCOUNT_USAGE_NAME=d.DISCOUNT_NAME
FROM TBL_CUSTOMER_DISCOUNT cd
INNER JOIN TMP_BILL_USAGE_SUMMARY s ON s.CUSTOMER_ID=cd.CUSTOMER_ID
INNER JOIN TBL_DISCOUNT d ON d.DISCOUNT_ID=cd.DISCOUNT_ID
WHERE d.DISCOUNT_TYPE_ID=1 AND cd.IS_ACTIVE=1 AND d.IS_ACTIVE=1;
 
-- PRICING

-- STANDARD PRICNG
-- CALCULATE PRICE BASE ON RANK(START & END)
-- AMOUNT = SUM(PRICE * X) WHERE X IS USAGE OF EACH RANK
INSERT INTO TMP_BILL_USAGE_RATE
SELECT	u.CUSTOMER_ID,
		u.TOTAL_USAGE,
		p.START_USAGE,
		p.END_USAGE,
		USAGE=(CASE WHEN (u.TOTAL_USAGE-u.DISCOUNT_USAGE)>p.END_USAGE AND p.END_USAGE<>-1
					THEN p.END_USAGE-p.START_USAGE + (CASE WHEN p.START_USAGE=0 THEN 0 ELSE 1 END)
					ELSE (u.TOTAL_USAGE-u.DISCOUNT_USAGE)-p.START_USAGE + (CASE WHEN p.START_USAGE=0 THEN 0 ELSE 1 END)
				END), 
		PRICE=p.PRICE,
		FIXED_AMOUNT=p.FIXED_AMOUNT,
		IS_ALLOW_FIXED_AMOUNT=p.IS_ALLOW_FIXED_AMOUNT,
		AMOUNT=0 
FROM TMP_BILL_USAGE_SUMMARY u  
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = u.CUSTOMER_ID
INNER JOIN TBL_PRICE_DETAIL p ON p.PRICE_ID=c.PRICE_ID
INNER JOIN TBL_PRICE pl ON pl.PRICE_ID = c.PRICE_ID AND pl.IS_STANDARD_RATING = 1
WHERE (u.TOTAL_USAGE-u.DISCOUNT_USAGE)>= p.START_USAGE
ORDER BY u.CUSTOMER_ID; 

-- NON-STANDARD PRICING(PRICE DISCRIMINATION)
-- CALCULATE PRICE BASE ON RANK (START & END)
-- AMOUNT = PRICE * TOTAL_USAGE 
DECLARE @PRICE_ID INT,
		@PRICE DECIMAL(18,5);
DECLARE C CURSOR FOR 
SELECT DISTINCT c.PRICE_ID 
FROM TMP_BILL_USAGE_SUMMARY u
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = u.CUSTOMER_ID
INNER JOIN TBL_PRICE p ON p.PRICE_ID=c.PRICE_ID WHERE IS_STANDARD_RATING=0;
OPEN C;
FETCH NEXT FROM C INTO @PRICE_ID;
WHILE @@FETCH_STATUS = 0
BEGIN
	
	INSERT INTO TMP_BILL_USAGE_RATE
	SELECT CUSTOMER_ID=u.CUSTOMER_ID,
			TOTAL_USAGE=u.TOTAL_USAGE,
			START_USAGE=0,
			END_USAGE=u.TOTAL_USAGE-u.DISCOUNT_USAGE,
			USAGE=u.TOTAL_USAGE-u.DISCOUNT_USAGE,
			PRICE=d.PRICE,
			FIXED_AMOUNT=d.FIXED_AMOUNT,
			IS_ALLOW_FIXED_AMOUNT=d.IS_ALLOW_FIXED_AMOUNT,
			AMOUNT=0 
	FROM TMP_BILL_USAGE_SUMMARY u
	INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = u.CUSTOMER_ID
	INNER JOIN TBL_PRICE_DETAIL d ON d.PRICE_ID=c.PRICE_ID
	WHERE c.PRICE_ID=@PRICE_ID 
		AND (u.TOTAL_USAGE/*-u.DISCOUNT_USAGE*/) BETWEEN d.START_USAGE AND (CASE WHEN d.END_USAGE=-1 THEN 100000000 ELSE d.END_USAGE END);

	FETCH NEXT FROM C INTO @PRICE_ID; 
END
CLOSE C;
DEALLOCATE C;

-- UPDATE AMOUNT
UPDATE TMP_BILL_USAGE_RATE
SET AMOUNT=(CASE WHEN TOTAL_USAGE<=END_USAGE AND IS_ALLOW_FIXED_AMOUNT=1 AND DATEDIFF(d,c.ACTIVATE_DATE,@DATE)> @MAX_DAY_TO_IGNORE_FIXED_AMOUNT
				THEN FIXED_AMOUNT
				ELSE PRICE*USAGE
			END)
FROM TMP_BILL_USAGE_RATE r 
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=r.CUSTOMER_ID;

-- INSERT INVOICE
INSERT INTO TBL_INVOICE
SELECT  INVOICE_NO='',
		INVOICE_MONTH=@MONTH,
		METER_CODE= REPLACE(LTRIM(REPLACE(m.METER_CODE,'0',' ')),' ','0'),-- (CASE WHEN m.MULTIPLIER<>1 THEN ' x '+CONVERT(NVARCHAR,m.MULTIPLIER) ELSE '' END),
		START_DATE=@START_DATE,
		END_DATE=@END_DATE,
		INVOICE_DATE=@DATE,
		START_USAGE=u.START_USAGE,
		END_USAGE=u.END_USAGE,
		CUSTOMER_ID=u.CUSTOMER_ID,
		PAID_AMOUNT=0,
		TOTAL_USAGE=u.TOTAL_USAGE,
		CURRENCY_ID=p.CURRENCY_ID,
		CYCLE_ID=c.BILLING_CYCLE_ID,
		DUE_DATE=@DUE_DATE,
		INVOICE_STATUS=1,
		IS_SERVICE_BILL=0,
		PRINT_COUNT=1,
		RUN_ID=@RUN_ID,
		FORWARD_AMOUNT=0,
		TOTAL_AMOUNT=0,
		SETTLE_AMOUNT=0,
		DISCOUNT_USAGE=u.DISCOUNT_USAGE,
		DISCOUNT_USAGE_NAME=u.DISCOUNT_USAGE_NAME,
		DISCOUNT_AMOUNT=0,
		DISCOUNT_AMOUNT_NAME='',
		INVOICE_TITLE=@INVOICE_TITLE,
		START_PAY_DATE=@START_PAY_DATE,
        PRICE_ID = 0, PRICE=0, BASED_PRICE = 0,
		ROW_DATE=@NOW,
		c.CUSTOMER_CONNECTION_TYPE_ID,
		EXCHANGE_RATE = ISNULL(r.EXCHANGE_RATE, 1),
		EXCHANGE_RATE_DATE = ISNULL(r.CREATE_ON, GETDATE())
FROM TMP_BILL_USAGE_SUMMARY u
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=u.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_METER cm ON cm.CUSTOMER_ID=c.CUSTOMER_ID AND cm.IS_ACTIVE=1
INNER JOIN TBL_METER m ON m.METER_ID=cm.METER_ID
INNER JOIN TBL_AREA a ON a.AREA_ID = c.AREA_ID
INNER JOIN TBL_PRICE p ON p.PRICE_ID=c.PRICE_ID
LEFT JOIN #tmpExchangeRate r ON r.CURRENCY_ID = p.CURRENCY_ID
-- SORT BY AREACODE,CUSTOMERID for fakeDB
ORDER BY a.AREA_CODE,c.CUSTOMER_ID;

--- INSERT INVOICE_USAGE
INSERT INTO TBL_INVOICE_USAGE
SELECT  INVOICE_ID=i.INVOICE_ID,
		CUSTOMER_ID=c.CUSTOMER_ID,
		METER_ID=m.METER_ID,
		METER_CODE= REPLACE(LTRIM(REPLACE(m.METER_CODE,'0',' ')),' ','0') ,
		CUSTOMER_NAME=c.LAST_NAME_KH+' '+ c.FIRST_NAME_KH,
		START_USAGE=u.START_USAGE,
		END_USAGE=u.END_USAGE,
		MULTIPLIER=u.MULTIPLIER,
		TOTAL_USAGE = u.TOTAL_USAGE
FROM TBL_INVOICE i
INNER JOIN TMP_BILL_USAGE u ON i.CUSTOMER_ID = u.USAGE_CUSTOMER_ID
INNER JOIN TBL_CUSTOMER c  ON c.CUSTOMER_ID =u.CUSTOMER_ID
INNER JOIN TBL_METER m ON m.METER_ID = u.METER_ID
WHERE i.RUN_ID=@RUN_ID AND i.IS_SERVICE_BILL=0;

INSERT INTO TBL_INVOICE_DETAIL
SELECT  INVOICE_ID=i.INVOICE_ID,
		START_USAGE=u.START_USAGE,
		END_USAGE=u.START_USAGE+(CASE WHEN u.START_USAGE=0 THEN u.USAGE ELSE u.USAGE-1 END),
		USAGE=u.USAGE,
		PRICE=u.PRICE,
		INVOICE_ITEM_ID=1,
		AMOUNT=u.AMOUNT,
		CHARGE_DESCRIPTION='',
		REF_NO = '',
		TRAN_DATE = i.INVOICE_DATE,
		EXCHANGE_RATE = ISNULL(r.EXCHANGE_RATE, 1),
		EXCHANGE_RATE_DATE = ISNULL(r.CREATE_ON, GETDATE()),
		TAX_AMOUNT = 0
FROM TBL_INVOICE i 
INNER JOIN TMP_BILL_USAGE_RATE u ON i.CUSTOMER_ID=u.CUSTOMER_ID
LEFT JOIN #tmpExchangeRate r ON r.CURRENCY_ID = i.CURRENCY_ID
WHERE i.INVOICE_MONTH=@MONTH AND i.RUN_ID=@RUN_ID AND i.IS_SERVICE_BILL=0;

-- PRICE EXTRA CHARGE 

-- TEMPORARY UPDATE INVOICE_AMOUNT
UPDATE TBL_INVOICE 
SET TOTAL_AMOUNT= x.TOTAL_AMOUNT 
FROM TBL_INVOICE i 
INNER JOIN(
	SELECT v.INVOICE_ID,SUM(AMOUNT) AS TOTAL_AMOUNT
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE v ON v.INVOICE_ID=d.INVOICE_ID
	WHERE v.RUN_ID=@RUN_ID AND v.IS_SERVICE_BILL=0
	GROUP BY v.INVOICE_ID
)x ON x.INVOICE_ID=i.INVOICE_ID;

DECLARE @EXTRA_CHARGE_ID INT;
DECLARE C CURSOR FOR 
SELECT DISTINCT c.PRICE_ID,g.EXTRA_CHARGE_ID
FROM TMP_BILL_USAGE_SUMMARY u
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = u.CUSTOMER_ID
INNER JOIN TBL_PRICE p ON p.PRICE_ID=c.PRICE_ID 
INNER JOIN TBL_PRICE_EXTRA_CHARGE g ON g.PRICE_ID=p.PRICE_ID AND g.IS_ACTIVE=1
OPEN C;
FETCH NEXT FROM C INTO @PRICE_ID,@EXTRA_CHARGE_ID;
WHILE @@FETCH_STATUS = 0
BEGIN  

    -- MERGE INVOICE
    INSERT INTO TBL_INVOICE_DETAIL
	SELECT  INVOICE_ID = i.INVOICE_ID,
			START_USAGE = 0,
			END_USAGE = 0,
			USAGE = 1,
			PRICE = CASE WHEN g.CHARGE_BY_PERCENTAGE=1 THEN g.CHARGE_VALUE* TOTAL_AMOUNT/100.00 ELSE g.CHARGE_VALUE END,
			INVOICE_ITEM_ID = g.INVOICE_ITEM_ID,
			AMOUNT = CASE WHEN g.CHARGE_BY_PERCENTAGE=1 THEN g.CHARGE_VALUE* TOTAL_AMOUNT/100.00 ELSE g.CHARGE_VALUE END,
			CHARGE_DESCRIPTION = g.CHARGE_DESCRIPTION,
			REF_NO = '',
			TRAN_DATE = i.INVOICE_DATE,
			EXCHANGE_RATE = ISNULL(r.EXCHANGE_RATE, 1),
			EXCHANGE_RATE_DATE = ISNULL(r.CREATE_ON, GETDATE()),
			TAX_AMOUNT = 0
	FROM TBL_INVOICE i
	INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = i.CUSTOMER_ID
	INNER JOIN TBL_PRICE p ON p.PRICE_ID=c.PRICE_ID
	INNER JOIN TBL_PRICE_EXTRA_CHARGE g ON g.PRICE_ID = c.PRICE_ID 
	LEFT JOIN #tmpExchangeRate r ON r.CURRENCY_ID = i.CURRENCY_ID
	WHERE i.RUN_ID=@RUN_ID AND IS_SERVICE_BILL=0 
			AND g.IS_ACTIVE=1 AND g.EXTRA_CHARGE_ID = @EXTRA_CHARGE_ID
            AND i.TOTAL_AMOUNT BETWEEN g.CHARGE_AFTER AND g.CHARGE_BEFORE
            AND g.IS_MERGE_INVOICE=1
    
    -- NOT MERGE INVOICE
	INSERT INTO TBL_INVOICE 
	SELECT  INVOICE_NO = '',
			INVOICE_MONTH = @MONTH, 
			METER_CODE = '',
			START_DATE = @MONTH,
			END_DATE = @MONTH,
			INVOICE_DATE = @DATE,
			START_USAGE = 0,
			END_USAGE = 0,
			CUSTOMER_ID = c.CUSTOMER_ID,
			PAID_AMOUNT = 0,
			TOTAL_USAGE = 0,
			CURRENCY_ID = p.CURRENCY_ID,
			CYCLE_ID = INVOICE_ITEM_ID,-- dummy
			DUE_DATE = @DUE_DATE,
			INVOICE_STATUS = 1,
			IS_SERVICE_BILL= 1,
			PRINT_COUNT = 0,
			RUN_ID = -9999, --- dummy
			FORWARD_AMOUNT = 0,
			TOTAL_AMOUNT = CASE WHEN g.CHARGE_BY_PERCENTAGE=1 THEN g.CHARGE_VALUE* TOTAL_AMOUNT/100.00 ELSE g.CHARGE_VALUE END,
			SETTLE_AMOUNT = CASE WHEN g.CHARGE_BY_PERCENTAGE=1 THEN g.CHARGE_VALUE* TOTAL_AMOUNT/100.00 ELSE g.CHARGE_VALUE END,
			DISCOUNT_USAGE = 0,
			DISCOUNT_USAGE_NAME = '',
			DISCOUNT_AMOUNT = 0,
			DISCOUNT_AMOUNT_NAME = '',
			INVOICE_TITLE =  g.CHARGE_DESCRIPTION,
			START_PAY_DATE = @START_PAY_DATE,
            PRICE_ID = 0, PRICE=0, BASED_PRICE = 0,
			ROW_DATE=@NOW,
			c.CUSTOMER_CONNECTION_TYPE_ID,
			EXCHANGE_RATE = i.EXCHANGE_RATE,
			i.EXCHANGE_RATE_DATE
	FROM TBL_INVOICE i
	INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = i.CUSTOMER_ID
	INNER JOIN TBL_PRICE p ON p.PRICE_ID=c.PRICE_ID
	INNER JOIN TBL_PRICE_EXTRA_CHARGE g ON g.PRICE_ID = c.PRICE_ID 
	WHERE i.RUN_ID=@RUN_ID AND IS_SERVICE_BILL=0 
			AND g.IS_ACTIVE=1 AND g.EXTRA_CHARGE_ID = @EXTRA_CHARGE_ID
            AND i.TOTAL_AMOUNT BETWEEN g.CHARGE_AFTER AND g.CHARGE_BEFORE
            AND g.IS_MERGE_INVOICE=0

	INSERT INTO TBL_INVOICE_DETAIL 
	SELECT 	INVOICE_ID,
			START_USAGE=0,
			END_USAGE=0,
			USAGE=1,
			PRICE=SETTLE_AMOUNT,
			INVOICE_ITEM_ID=CYCLE_ID,
			AMOUNT=SETTLE_AMOUNT,
			CHARGE_DESCRIPTION=INVOICE_TITLE,
			REF_NO = '',
			TRAN_DATE = i.INVOICE_DATE,
			EXCHANGE_RATE = ISNULL(r.EXCHANGE_RATE, 1),
			EXCHANGE_RATE_DATE = ISNULL(r.CREATE_ON, GETDATE()),
			TAX_AMOUNT = 0
	FROM TBL_INVOICE i
	LEFT JOIN #tmpExchangeRate r ON r.CURRENCY_ID = i.CURRENCY_ID
	WHERE RUN_ID=-9999;

	-- clear dummy 
	UPDATE TBL_INVOICE 
	SET CYCLE_ID=@CYCLE_ID,RUN_ID=@RUN_ID 
	WHERE RUN_ID=-9999;

	FETCH NEXT FROM C INTO @PRICE_ID,@EXTRA_CHARGE_ID;
END
CLOSE C;
DEALLOCATE C; 
------------ END PRICE EXTRA CHARGE ----------------- 

-- UPDATE PRICING - RECORD PRICE FOR EASY REPORT CACULATION.
SELECT p.PRICE_ID,BASED_PRICE=ISNULL(x.PRICE,0)
INTO #TMP_PRICE
FROM TBL_PRICE p
OUTER APPLY(SELECT TOP 1 PRICE FROM TBL_PRICE_DETAIL WHERE PRICE_ID=p.PRICE_ID  ORDER BY START_USAGE DESC) x

UPDATE TBL_INVOICE 
SET PRICE_ID = c.PRICE_ID,
    BASED_PRICE = ISNULL(b.BASED_PRICE,0),
    PRICE = ISNULL(d.PRICE,0)
FROM TBL_INVOICE i
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = i.CUSTOMER_ID
INNER JOIN #TMP_PRICE b ON b.PRICE_ID=c.PRICE_ID 
OUTER APPLY(SELECT TOP 1 PRICE FROM TBL_INVOICE_DETAIL WHERE INVOICE_ID=i.INVOICE_ID) d
WHERE i.RUN_ID=@RUN_ID

-- UDPATE INVOICE_NO (for invoice bill)
UPDATE TBL_INVOICE
SET INVOICE_NO = x.INVOICE_NO 
FROM TBL_INVOICE i
INNER JOIN (
	SELECT INVOICE_ID,INVOICE_NO=dbo.GET_SEQUENCE(@SEQUENCE_INVOICE,ROW_NUMBER() OVER(ORDER BY INVOICE_ID)-1)
	FROM TBL_INVOICE
	WHERE RUN_ID=@RUN_ID AND INVOICE_NO='' AND IS_SERVICE_BILL = 0
)x ON x.INVOICE_ID = i.INVOICE_ID; 

-- UDPATE SEQUENCE
UPDATE TBL_SEQUENCE
SET VALUE = CASE WHEN FORMAT LIKE '%{Y%' AND DATEDIFF(YEAR,DATE,@DATE)<>0 THEN @@ROWCOUNT
				 WHEN FORMAT LIKE '%{M%' AND DATEDIFF(MONTH,DATE,@DATE)<>0 THEN @@ROWCOUNT
			ELSE VALUE + @@ROWCOUNT END,
	DATE = @DATE
WHERE SEQUENCE_ID = @SEQUENCE_INVOICE;

-- UPDATE REF_NO IN INVOICE_DETAIL
UPDATE id
SET id.REF_NO = i.INVOICE_NO
FROM dbo.TBL_INVOICE i
INNER JOIN dbo.TBL_INVOICE_DETAIL id ON id.INVOICE_ID = i.INVOICE_ID
WHERE i.RUN_ID = @RUN_ID

-- UDPATE INVOICE_NO (for service bill)
UPDATE TBL_INVOICE
SET INVOICE_NO = x.INVOICE_NO 
FROM TBL_INVOICE i
INNER JOIN (
	SELECT INVOICE_ID,INVOICE_NO=dbo.GET_SEQUENCE(@SEQUENCE_SERVICE,ROW_NUMBER() OVER(ORDER BY INVOICE_ID)-1)
	FROM TBL_INVOICE
	WHERE RUN_ID=@RUN_ID AND INVOICE_NO='' AND IS_SERVICE_BILL = 1
)x ON x.INVOICE_ID = i.INVOICE_ID; 

-- UDPATE SEQUENCE
UPDATE TBL_SEQUENCE
SET VALUE = CASE WHEN FORMAT LIKE '%{Y%' AND DATEDIFF(YEAR,DATE,@DATE)<>0 THEN @@ROWCOUNT
				 WHEN FORMAT LIKE '%{M%' AND DATEDIFF(MONTH,DATE,@DATE)<>0 THEN @@ROWCOUNT
			ELSE VALUE + @@ROWCOUNT END,
	DATE = @DATE
WHERE SEQUENCE_ID = @SEQUENCE_SERVICE;

-- CALCULATE BALANCE FOWARD!
UPDATE TBL_INVOICE
SET FORWARD_AMOUNT = x.FORWARD_AMOUNT
FROM TBL_INVOICE i 
CROSS APPLY(
	SELECT TOP 1 FORWARD_AMOUNT=TOTAL_AMOUNT-SETTLE_AMOUNT
	FROM TBL_INVOICE  
	WHERE INVOICE_ID<i.INVOICE_ID 
		  AND IS_SERVICE_BILL=0
		  AND CUSTOMER_ID=i.CUSTOMER_ID
          AND CURRENCY_ID=i.CURRENCY_ID
		  AND INVOICE_STATUS <> 3
	ORDER BY INVOICE_ID DESC
) x

-- UPDATE TOTAL AMOUNT
UPDATE TBL_INVOICE 
SET TOTAL_AMOUNT= x.TOTAL_AMOUNT 
FROM TBL_INVOICE i 
INNER JOIN(
	SELECT v.INVOICE_ID,SUM(AMOUNT) AS TOTAL_AMOUNT
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE v ON v.INVOICE_ID=d.INVOICE_ID
	WHERE v.RUN_ID=@RUN_ID AND v.IS_SERVICE_BILL=0
	GROUP BY v.INVOICE_ID
)x ON x.INVOICE_ID=i.INVOICE_ID;

-- UPDATE TOTAL AMOUNT
UPDATE TBL_INVOICE
SET TOTAL_AMOUNT=TOTAL_AMOUNT+FORWARD_AMOUNT
WHERE RUN_ID=@RUN_ID AND IS_SERVICE_BILL=0;

-- UPDATE SETTLE AMOUNT.
UPDATE TBL_INVOICE 
SET SETTLE_AMOUNT=dbo.ROUND_BY_CURRENCY(TOTAL_AMOUNT,CURRENCY_ID)
WHERE RUN_ID=@RUN_ID AND IS_SERVICE_BILL=0;

-- IF NOT KEEP_BILLING_REMAIN_AMOUNT 
-- NOT ALLOW FORWARD LAST REMAIN AMOUNT TO NEXT BILLING 
IF NOT EXISTS(SELECT * FROM TBL_UTILITY WHERE UTILITY_ID=29 AND UPPER(UTILITY_VALUE) IN ('1','TRUE','ON'))
BEGIN
	UPDATE TBL_INVOICE 
	SET TOTAL_AMOUNT = SETTLE_AMOUNT
	WHERE RUN_ID=@RUN_ID AND IS_SERVICE_BILL=0;
END

-- UPDATE INVOICE STATUS 
UPDATE TBL_INVOICE
SET INVOICE_STATUS=4
WHERE RUN_ID=@RUN_ID AND IS_SERVICE_BILL=0 AND SETTLE_AMOUNT=0;

-- LOG BILLING's CUSTOMER LOG
INSERT INTO TBL_RUN_BILL_INVOICE
SELECT  RUN_ID = @RUN_ID,
		INVOICE_ID = i.INVOICE_ID,
		CUSTOMER_ID = i.CUSTOMER_ID,
		CUSTOMER_CODE = c.CUSTOMER_CODE,
		INVOICE_NO = i.INVOICE_NO,
		METER_CODE = i.METER_CODE, 
		START_USAGE = i.START_USAGE,
		END_USAGE = i.END_USAGE,
		TOTAL_USAGE = i.TOTAL_USAGE,
		DISCOUNT_USAGE = i.DISCOUNT_USAGE,
		SETTLE_AMOUNT = i.SETTLE_AMOUNT,
		AR_BEFORE_RUN = 0 ,-- NO ANY AR BEFORE RUN.
		CUSTOMER_STATUS_ID = c.STATUS_ID,
		i.CURRENCY_ID
FROM TBL_INVOICE i
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = i.CUSTOMER_ID
WHERE i.IS_SERVICE_BILL = 0  AND i.RUN_ID = @RUN_ID;

-- LOG AGING AFTER RUN
SET @DATE = GETDATE();
INSERT INTO TBL_RUN_BILL_AGING(RUN_ID,DATE,IS_FEFORE,CUSTOMER_ID,CURRENCY_ID,BAL1,BAL2,BAL3,BAL4,TOTAL)
SELECT RUN_ID= @RUN_ID,
	   DATE  = @DATE,
	   IS_BEFORE = 0,
	   c.CUSTOMER_ID,
	   i.CURRENCY_ID,
	   BAL1  = SUM( CASE WHEN DATEDIFF(D,i.INVOICE_DATE,@DATE) BETWEEN @PERIOD*0 AND @PERIOD*1 THEN SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT,0) + ISNULL(aa.ADJUST_AMOUNT,0) - ISNULL(p.PAY_AMOUNT,0) ELSE 0 END ),
	   BAL2  = SUM( CASE WHEN DATEDIFF(D,i.INVOICE_DATE,@DATE) BETWEEN @PERIOD*1+1 AND @PERIOD*2 THEN SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT,0) + ISNULL(aa.ADJUST_AMOUNT,0) - ISNULL(p.PAY_AMOUNT,0) ELSE 0 END ),
	   BAL3  = SUM( CASE WHEN DATEDIFF(D,i.INVOICE_DATE,@DATE) BETWEEN @PERIOD*2+1 AND @PERIOD*3 THEN SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT,0) + ISNULL(aa.ADJUST_AMOUNT,0) - ISNULL(p.PAY_AMOUNT,0) ELSE 0 END ),
	   BAL4  = SUM( CASE WHEN DATEDIFF(D,i.INVOICE_DATE,@DATE) > @PERIOD*3                       THEN SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT,0) + ISNULL(aa.ADJUST_AMOUNT,0) - ISNULL(p.PAY_AMOUNT,0) ELSE 0 END ),
       TOTAL = SUM( SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT,0) + ISNULL(aa.ADJUST_AMOUNT,0) - ISNULL(p.PAY_AMOUNT,0)) 
FROM TBL_INVOICE i
INNER JOIN TBL_CUSTOMER c ON i.CUSTOMER_ID = c.CUSTOMER_ID
OUTER APPLY (
    SELECT SUM(PD.PAY_AMOUNT) AS PAY_AMOUNT
    FROM TBL_PAYMENT_DETAIL PD INNER JOIN TBL_PAYMENT P ON P.PAYMENT_ID=PD.PAYMENT_ID
    WHERE P.PAY_DATE <@DATE AND INVOICE_ID=i.INVOICE_ID
) p -- PAYMENT
OUTER APPLY (
    SELECT SUM(IA.ADJUST_AMOUNT) AS ADJUST_AMOUNT
    FROM TBL_INVOICE_ADJUSTMENT IA 
    WHERE INVOICE_ID =i.INVOICE_ID
) ta -- TOTAL ADJUSTMENT
OUTER APPLY (
    SELECT SUM(IA.ADJUST_AMOUNT) AS ADJUST_AMOUNT
    FROM TBL_INVOICE_ADJUSTMENT IA 
    WHERE IA.CREATE_ON <= @DATE AND INVOICE_ID =i.INVOICE_ID
) aa  -- ACCUMULATED ADJUSTMENT 	
WHERE	i.INVOICE_STATUS NOT IN (3)
		--AND (c.CUSTOMER_TYPE_ID = @CUSTOMER_TYPE_ID OR @CUSTOMER_TYPE_ID = 0)
		AND (INVOICE_DATE <= @DATE)
		AND c.BILLING_CYCLE_ID=@CYCLE_ID --LOG AGIN after RUN_BILL
GROUP BY c.CUSTOMER_ID,i.CURRENCY_ID
HAVING SUM( SETTLE_AMOUNT - ISNULL(ta.ADJUST_AMOUNT,0) + ISNULL(aa.ADJUST_AMOUNT,0) - ISNULL(p.PAY_AMOUNT,0))>0;

-- RUN BILL's CUSTOMER INFO
UPDATE TBL_RUN_BILL
SET CUSTOMER_TOTAL = ISNULL(x.CUSTOMER_TOTAL,0),
	CUSTOMER_ACTIVE = ISNULL(x.CUSTOMER_ACTIVE,0),
	CUSTOMER_BLOCKED = ISNULL(x.CUSTOMER_BLOCKED,0),
	CUSTOMER_PENDING = ISNULL(x.CUSTOMER_PENDING,0),
	CUSTOMER_CLOSE = ISNULL(x.CUSTOMER_CLOSE,0)
FROM TBL_RUN_BILL r
CROSS APPLY(
	SELECT	CUSTOMER_TOTAL  = SUM(CASE WHEN STATUS_ID <> 5 THEN 1 ELSE 0 END),
			CUSTOMER_ACTIVE = SUM(CASE WHEN STATUS_ID = 2 THEN 1 ELSE 0 END),
			CUSTOMER_BLOCKED = SUM(CASE WHEN STATUS_ID = 3 THEN 1 ELSE 0 END),
			CUSTOMER_PENDING = SUM(CASE WHEN STATUS_ID =1  THEN 1 ELSE 0 END),
			CUSTOMER_CLOSE = SUM(CASE WHEN STATUS_ID = 4 THEN 1 ELSE 0 END)
	FROM TBL_CUSTOMER
	WHERE IS_POST_PAID = 1
) x
WHERE r.RUN_ID =@RUN_ID;

-- RUN BILL's INVOICE INFO
INSERT INTO TBL_RUN_BILL_AMOUNT
SELECT  RUN_ID,
		CURRENCY_ID,
		TOTAL_INVOICE = COUNT(INVOICE_ID),
		TOTAL_POWER   = SUM(TOTAL_USAGE),
		TOTAL_AMOUNT  = SUM(SETTLE_AMOUNT),
		TOTAL_INVOICE_ZERO_USAGE =	SUM(CASE WHEN TOTAL_USAGE = 0 THEN 1 ELSE 0 END),
		TOTAL_INVOICE_ZERO_AMOUNT = SUM(CASE WHEN SETTLE_AMOUNT = 0 THEN 1 ELSE 0 END),
		AR_BEFORE_RUN = 0,
		AR_AFTER_RUN = 0
FROM TBL_INVOICE
WHERE IS_SERVICE_BILL=0 AND RUN_ID=@RUN_ID
GROUP BY RUN_ID,CURRENCY_ID;
 
UPDATE TBL_RUN_BILL
SET TOTAL_POWER=t.TOTAL_POWER,
	TOTAL_INVOICE=t.TOTAL_INVOICE
FROM TBL_RUN_BILL b
OUTER APPLY(
	SELECT TOTAL_POWER=SUM(TOTAL_POWER),
		   TOTAL_INVOICE=SUM(TOTAL_INVOICE)
	FROM TBL_RUN_BILL_AMOUNT 
	WHERE RUN_ID=b.RUN_ID
) t
WHERE RUN_ID=@RUN_ID;

--RUN BILL's 
UPDATE TBL_RUN_BILL_AMOUNT
SET AR_BEFORE_RUN = ISNULL(x.AR_BEFORE_RUN,0),
	AR_AFTER_RUN = ISNULL(x.AR_AFTER_RUN,0) 
FROM TBL_RUN_BILL_AMOUNT r
CROSS APPLY(
	SELECT	CURRENCY_ID,
			AR_BEFORE_RUN = SUM(CASE WHEN IS_FEFORE = 1 THEN TOTAL ELSE 0 END),
			AR_AFTER_RUN = SUM(CASE WHEN IS_FEFORE = 0 THEN TOTAL ELSE 0 END)
	FROM TBL_RUN_BILL_AGING 
	WHERE RUN_ID = r.RUN_ID
	GROUP BY CURRENCY_ID
) x
WHERE r.RUN_ID = @RUN_ID AND r.CURRENCY_ID=x.CURRENCY_ID
---- END OF RUN_BILL
GO
";
        }
    }
}
