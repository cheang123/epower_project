﻿namespace EPower.Update
{
    /// <summary>
    /// 1. fix Permisison
    /// 2. fix Account Chart for Income/Expense of other business 
    /// 3. fix P&L - multiple level of income/expense.
    /// </summary>
    /// <returns></returns>
     
    class Version1_0_8_5 : Version
    {        
        protected override string GetBatchCommand()
        {
            return @"
ALTER TABLE TBL_PERMISSION ALTER COLUMN PERMISSION_NAME NVARCHAR(100) COLLATE LATIN1_GENERAL_BIN;
/*
SELECT 'UNION ALL SELECT '+
'N'''+CONVERT(NVARCHAR(MAX),PERMISSION_ID)+''''+','+
'N'''+CONVERT(NVARCHAR(MAX),PERMISSION_DISPLAY)+''''+','+
'N'''+CONVERT(NVARCHAR(MAX),PERMISSION_NAME)+''''+','+
'N'''+CONVERT(NVARCHAR(MAX),PERMISSION_ORDER)+''''+','+
'N'''+CONVERT(NVARCHAR(MAX),PERMISSION_PARENT_ID)+''''+','+
'N'''+CONVERT(NVARCHAR(MAX),PERMISSION_LEVEL)+'''' 
FROM TBL_PERMISSION;
*/
GO
TRUNCATE TABLE TBL_PERMISSION;
SET IDENTITY_INSERT TBL_PERMISSION ON;
INSERT INTO TBL_PERMISSION(PERMISSION_ID,PERMISSION_DISPLAY,PERMISSION_NAME,PERMISSION_ORDER,PERMISSION_PARENT_ID,PERMISSION_LEVEL)

SELECT N'8',N'បង់ប្រាក់មុន',N'PREPAID',N'0',N'0',N'0'
UNION ALL SELECT N'9',N'បង់ប្រាក់ក្រោយ',N'POSTPAID',N'2',N'0',N'0'
UNION ALL SELECT N'10',N'ទទួលប្រាក់បង់',N'PAYMENT',N'1',N'9',N'0'
UNION ALL SELECT N'11',N'បញ្ចូលអំណាន',N'INPUTUSAGE',N'4',N'9',N'0'
UNION ALL SELECT N'18',N'អតិថិជន និងការទូទាត់',N'CUSTOMERANDBILLING',N'3',N'0',N'1'
UNION ALL SELECT N'19',N'អតិថិជន',N'CUSTOMER',N'0',N'18',N'2'
UNION ALL SELECT N'20',N'គ្រប់គ្រងអតិថិជន',N'REGISTER',N'0',N'19',N'3'
UNION ALL SELECT N'21',N'ចាប់ផ្តើមប្រើ',N'ACTIVATE',N'21',N'19',N'3'
UNION ALL SELECT N'22',N'ផ្តាច់ចរន្ត',N'BLOCK',N'0',N'19',N'3'
UNION ALL SELECT N'23',N'ភ្ជាប់ចរន្ត',N'UNBLOCK',N'0',N'19',N'3'
UNION ALL SELECT N'24',N'កំណត់ការទូទាត់',N'SETUPRUNBILL',N'0',N'18',N'2'
UNION ALL SELECT N'25',N'តំលៃអគ្គិសនី',N'PRICE',N'0',N'24',N'3'
UNION ALL SELECT N'26',N'ជុំនៃការទូទាត់',N'BILLINGCYCLE',N'0',N'24',N'3'
UNION ALL SELECT N'27',N'ប្រភេទសេវាកម្ម',N'INVOICEITEMTYPE',N'0',N'24',N'3'
UNION ALL SELECT N'28',N'សេវាកម្ម',N'INVOICEITEM',N'0',N'24',N'3'
UNION ALL SELECT N'29',N'ការចុះតំលៃ',N'DISCOUNT',N'0',N'24',N'3'
UNION ALL SELECT N'30',N'ប្រតិបត្តិការណ៍ទូទាត់',N'BILLINGOPERATION',N'0',N'18',N'2'
UNION ALL SELECT N'31',N'ទូទាត់ការប្រើប្រាស់',N'RUNBILL',N'0',N'30',N'3'
UNION ALL SELECT N'32',N'បោះពុម្ពវិក័្កយបត្រ',N'PRINTINVOICE',N'0',N'30',N'3'
UNION ALL SELECT N'33',N'ប្រវត្តិនៃការទូទាត់',N'BILLINGHISTORY',N'0',N'30',N'3'
UNION ALL SELECT N'34',N'គ្រប់គ្រង់ទូទៅ',N'ADMIN',N'4',N'0',N'1'
UNION ALL SELECT N'35',N'ក្រុមហ៊ុន',N'COMPANY',N'1',N'34',N'2'
UNION ALL SELECT N'36',N'តំបន់',N'AREA',N'2',N'34',N'2'
UNION ALL SELECT N'37',N'បង្កោល និងប្រអប់',N'POLEANDBOX',N'3',N'34',N'2'
UNION ALL SELECT N'38',N'ប្រភេទកុងទ័រ',N'METERTYPE',N'4',N'34',N'2'
UNION ALL SELECT N'39',N'កុងទ័រ',N'METER',N'5',N'34',N'2'
UNION ALL SELECT N'40',N'ប្រភេទឌីសុងទ័រ',N'CIRCUITBREAKERTYPE',N'6',N'34',N'2'
UNION ALL SELECT N'41',N'ឌីសុងទ័រ',N'CIRCUITBREAKER',N'7',N'34',N'2'
UNION ALL SELECT N'42',N'សំភារៈប្រើប្រាស់',N'EQIPMENT',N'8',N'34',N'2'
UNION ALL SELECT N'43',N'ឩបករណ៍',N'DEVICEREGISTER',N'9',N'34',N'2'
UNION ALL SELECT N'44',N'បុគ្គលិក',N'EMPLOYEE',N'10',N'34',N'2'
UNION ALL SELECT N'45',N'កេះដាក់ប្រាក់',N'CASTDRAWER',N'11',N'34',N'2'
UNION ALL SELECT N'46',N'ថ្លៃភ្ជាប់ចរន្ត',N'CONNECTFEE',N'0',N'24',N'3'
UNION ALL SELECT N'47',N'ប្រភេទប្រើប្រាស់',N'CUSTOMERTYPE',N'12',N'34',N'2'
UNION ALL SELECT N'48',N'ខ្នាតអគ្គិសនី',N'UNITELECTRICITY',N'5',N'132',N'0'
UNION ALL SELECT N'49',N'អាំងតង់ស៊ីតេ',N'AMPARE',N'2',N'48',N'3'
UNION ALL SELECT N'50',N'តង់ស្យុង',N'VOLTAGE',N'1',N'48',N'3'
UNION ALL SELECT N'51',N'ហ្វា(Phase)',N'PHASE',N'3',N'48',N'3'
UNION ALL SELECT N'52',N'កុងស្តង់',N'CONTANT',N'4',N'48',N'3'
UNION ALL SELECT N'53',N'លក្ខណៈសំណរ',N'SEAL',N'5',N'48',N'3'
UNION ALL SELECT N'54',N'ផ្សេងៗ',N'OTHER',N'15',N'34',N'0'
UNION ALL SELECT N'55',N'លក្ខ័ណប្រើប្រាស់',N'USERSETTING',N'0',N'54',N'0'
UNION ALL SELECT N'57',N'សុវត្ថិភាពប្រព័ន្ធ',N'SECURITY',N'6',N'0',N'0'
UNION ALL SELECT N'58',N'អ្នកប្រើ',N'USER',N'0',N'57',N'2'
UNION ALL SELECT N'59',N'តួនាទី',N'PERMISION',N'0',N'57',N'2'
UNION ALL SELECT N'60',N'ទិន្នន័យ',N'DATA',N'0',N'57',N'2'
UNION ALL SELECT N'61',N'ចំលងទុក(Backup)',N'DATABACKUP',N'61',N'60',N'3'
UNION ALL SELECT N'62',N'ជំនួស(Restore)',N'DATARESTORE',N'0',N'60',N'3'
UNION ALL SELECT N'63',N'ការប្រើប្រាស់',N'USING',N'0',N'57',N'2'
UNION ALL SELECT N'64',N'ប្រវត្តិប្រតិបត្តិការណ៍',N'VIEWCHANGELOG',N'0',N'63',N'3'
UNION ALL SELECT N'66',N'របាយការណ៍',N'REPORT',N'7',N'0',N'0'
UNION ALL SELECT N'67',N'ប្រតិបត្តិការសង្ខេប',N'TRIALBALANCEDAILY',N'1',N'66',N'0'
UNION ALL SELECT N'68',N'ប្រតិបត្តិការអតិថិជនលំអិត',N'TRIALBALANCECUSTOMERDETAIL',N'2',N'66',N'0'
UNION ALL SELECT N'69',N'បំណុល និង ការបង់ប្រាក់លំអិត',N'TRIALBALANCEARDETAIL',N'3',N'66',N'0'
UNION ALL SELECT N'70',N'ប្រតិបត្តិការប្រាក់កក់លំអិត',N'TRIALBALANCEDEPOSITDETAIL',N'3',N'66',N'0'
UNION ALL SELECT N'71',N'បំណុលតាមកាល - Aging Report',N'AGING',N'71',N'66',N'0'
UNION ALL SELECT N'72',N'របាយការណ៍សង្ខេប',N'MONTHLY',N'101',N'66',N'0'
UNION ALL SELECT N'73',N'ចំណូលអគ្គីសនី',N'POWERINCOME',N'2',N'72',N'0'
UNION ALL SELECT N'74',N'ចំណូលផ្សេងៗ',N'OTHERREVENUE',N'3',N'72',N'0'
UNION ALL SELECT N'78',N'ទូទាត់ថ្លៃសេវាកម្ម',N'FIXCHARGE',N'0',N'20',N'4'
UNION ALL SELECT N'79',N'បន្ថែម',N'INSERT',N'0',N'20',N'4'
UNION ALL SELECT N'80',N'កែប្រែ',N'UPDATE',N'0',N'20',N'4'
UNION ALL SELECT N'81',N'ឈប់ប្រើប្រាស់',N'CLOSE',N'0',N'20',N'4'
UNION ALL SELECT N'82',N'កែប្រាក់កក់',N'ADJUSTDESPOSIT',N'83',N'80',N'5'
UNION ALL SELECT N'83',N'ប្តូរកុងទ័រ',N'CHANGEMETER',N'0',N'80',N'5'
UNION ALL SELECT N'84',N'ប្តូរឌីសុងទ័រ',N'CHANGECIRCUITBREAKER',N'0',N'80',N'5'
UNION ALL SELECT N'85',N'ប្តូរប្រអប់',N'CHANGEBOX',N'0',N'80',N'5'
UNION ALL SELECT N'86',N'កែសំរូលការប្រើប្រាស់',N'ADJUSTUSAGE',N'0',N'80',N'5'
UNION ALL SELECT N'87',N'កែសំរូលលើបំណុល',N'ADJUSTAMOUNT',N'0',N'80',N'5'
UNION ALL SELECT N'88',N'បោះពុម្ព',N'PRINT',N'0',N'80',N'5'
UNION ALL SELECT N'89',N'មើលព័ត៌មានអតិថិជន',N'VIEWCUSTOMER',N'0',N'20',N'4'
UNION ALL SELECT N'90',N'ស្រង់តាម IR Reader',N'IRREADER',N'5',N'9',N'0'
UNION ALL SELECT N'91',N'ទាញយកអំណាន',N'COLLECTUSAGE',N'91',N'90',N'0'
UNION ALL SELECT N'92',N'ចុះបញ្ចីកុងទ័រឌីជីថល',N'REGISTERMETER',N'92',N'90',N'0'
UNION ALL SELECT N'93',N'កែប្រែការកក់ប្រាក់',N'ADUSTDEPOSIT',N'0',N'80',N'5'
UNION ALL SELECT N'96',N'ប្រតិបត្តិការសាច់ប្រាក់សង្ខេប',N'CASHINGSUMARY',N'72',N'66',N'0'
UNION ALL SELECT N'97',N'ប្រតិបត្តិការសាច់ប្រាក់លំអិត',N'CASHINGDETAIL',N'73',N'66',N'0'
UNION ALL SELECT N'99',N'បញ្ជីអតិថិជន',N'CUSTOMERS',N'7',N'72',N'0'
UNION ALL SELECT N'101',N'ប្រើប្រាស់មិនគិតថ្លៃ',N'DISCOUNT',N'5',N'72',N'0'
UNION ALL SELECT N'102',N'លុបការបង់ប្រាក់',N'CANCELPAYMENT',N'3',N'9',N'0'
UNION ALL SELECT N'103',N'គ្រប់គ្រង់អតិថិជន',N'REGISTER',N'2',N'8',N'0'
UNION ALL SELECT N'104',N'ទូទាត់ថ្លៃសេវាកម្ម',N'FIXCHARGE',N'0',N'103',N'3'
UNION ALL SELECT N'105',N'បន្ថែម',N'INSERT',N'0',N'103',N'3'
UNION ALL SELECT N'106',N'កែប្រែ',N'UPDATE',N'0',N'103',N'3'
UNION ALL SELECT N'107',N'ប្តូរកុងទ័រ',N'CHANGEMETER',N'0',N'106',N'0'
UNION ALL SELECT N'108',N'ប្តូរឌីសុងទ័រ',N'CHANGEBREAKER',N'0',N'106',N'4'
UNION ALL SELECT N'109',N'ប្តូរប្រអប់',N'CHANGEBOX',N'0',N'106',N'4'
UNION ALL SELECT N'110',N'កែប្រាក់កក់',N'ADJUSTDESPOSIT',N'0',N'106',N'4'
UNION ALL SELECT N'111',N'ឈប់ប្រើប្រាស់',N'CLOSE',N'0',N'103',N'3'
UNION ALL SELECT N'112',N'មើលព័ត៌មានអតិថិជន',N'VIEWCUSTOMER',N'0',N'103',N'3'
UNION ALL SELECT N'113',N'ចាប់ផ្តើមប្រើ',N'ACTIVATE',N'0',N'103',N'3'
UNION ALL SELECT N'114',N'ប្រតិបត្តិការណប្រចាំថ្ងៃ',N'DIALYOPERATION',N'1',N'8',N'0'
UNION ALL SELECT N'115',N'លក់អគ្គិសនី',N'SALEPOWER',N'1',N'114',N'0'
UNION ALL SELECT N'116',N'លុបការលក់អគ្គិសនី',N'VOIDPOWER',N'2',N'114',N'0'
UNION ALL SELECT N'117',N'បង្កើតកាតទិញអគ្គិសនីថ្មី',N'RENEWCARD',N'3',N'114',N'0'
UNION ALL SELECT N'118',N'បង្កើតកាតពិសេស',N'CREATESPECIALCARD',N'4',N'114',N'0'
UNION ALL SELECT N'120',N'កែសំរួលលើបំណុល',N'ADJUSTAMOUNT',N'0',N'106',N'4'
UNION ALL SELECT N'121',N'បោះពុម្ព',N'PRINT',N'0',N'106',N'4'
UNION ALL SELECT N'122',N'ការប្រើប្រាស់ថាមពល',N'USAGE',N'4',N'72',N'3'
UNION ALL SELECT N'123',N'របាយការណ៏បង់ប្រាក់មុន',N'PREPAID',N'3',N'8',N'0'
UNION ALL SELECT N'124',N'ចំណូលពីការលក់អគ្គិសនី',N'SALEPOWERDETAIL',N'0',N'123',N'3'
UNION ALL SELECT N'125',N'ចំណូលពីការលក់អគ្គិសនីសង្ខេប',N'SALEPOWERSUMMARY',N'0',N'123',N'3'
UNION ALL SELECT N'126',N'ត្រង់ស្វូ',N'TRANSFORMER',N'2',N'34',N'2'
UNION ALL SELECT N'127',N'ការផលិតថាមពល',N'POWER',N'6',N'72',N'0'
UNION ALL SELECT N'128',N'ទូទាត់តាមធនាគារ',N'BANKPAYMENT',N'12',N'9',N'0'
UNION ALL SELECT N'129',N'ការទូរទាត់',N'SETTLEMENT',N'11',N'128',N'0'
UNION ALL SELECT N'130',N'សង្ខេបការទូទាត់',N'SUMMARY',N'12',N'128',N'0'
UNION ALL SELECT N'131',N'បោះពុម្ភបង្កាន់ដៃ',N'REPRINTRECIEPT',N'2',N'9',N'0'
UNION ALL SELECT N'132',N'គ្រប់គ្រងថាមពល',N'POWER',N'5',N'0',N'1'
UNION ALL SELECT N'134',N'ខ្សែបណ្តាញចែកចាយ',N'DISTRIBUTION_FACILITY',N'2',N'138',N'0'
UNION ALL SELECT N'135',N'ប្រភពទិញថាមពល',N'AGREEMENT',N'1',N'139',N'0'
UNION ALL SELECT N'136',N'ការទិញថាមពល',N'POWERAGREEMENT',N'2',N'139',N'0'
UNION ALL SELECT N'137',N'កំលាំង(kVA)',N'CAPACITY',N'0',N'48',N'3'
UNION ALL SELECT N'138',N'ការចែកចាយថាមពល',N'DISTRIBUTION',N'1',N'132',N'0'
UNION ALL SELECT N'139',N'ការទិញថាមពល',N'PURCHASEPOWER',N'2',N'132',N'0'
UNION ALL SELECT N'140',N'ការប្រើប្រាស់ថាមពល',N'USEPOWER',N'4',N'132',N'0'
UNION ALL SELECT N'141',N'ការកំណត់ថាមពល',N'POWER',N'0',N'140',N'0'
UNION ALL SELECT N'142',N'ថាមពលតាមត្រង់ស្វូ',N'POWERBYTRANSOFRMAER',N'142',N'140',N'0'
UNION ALL SELECT N'143',N'បញ្ចូលព៌តមានថ្មីៗ  ',N'SENDDATATOIR',N'0',N'90',N'0'
UNION ALL SELECT N'149',N'អំណានចាស់',N'STARTUSAGE',N'1',N'11',N'3'
UNION ALL SELECT N'150',N'អំណានថ្មី',N'ENDUSAGE',N'2',N'11',N'3'
UNION ALL SELECT N'151',N'ផ្នែកគណនេយ្យ',N'ACC',N'5',N'0',N'0'
UNION ALL SELECT N'152',N'ប្រតិបត្តិការ',N'TRANS',N'1',N'151',N'0'
UNION ALL SELECT N'153',N'ប្រតិបត្តិការចំណូល',N'INCOME',N'1',N'152',N'0'
UNION ALL SELECT N'154',N'ប្រតិបត្តិការចំណាយ',N'EXPENSE',N'2',N'152',N'0'
UNION ALL SELECT N'155',N'អត្រាប្តូរប្រាក់',N'EXCHANGERATE',N'3',N'152',N'0'
UNION ALL SELECT N'156',N'ការកំណត់គណនី',N'CHART',N'2',N'151',N'0'
UNION ALL SELECT N'157',N'គណនី',N'ACCOUNTCHART',N'1',N'156',N'0'
UNION ALL SELECT N'158',N'ការកំណត់',N'CONFIG',N'2',N'156',N'0'
UNION ALL SELECT N'159',N'អចលនទ្រព្យ',N'FIXASSET',N'3',N'151',N'0'
UNION ALL SELECT N'160',N'ប្រភេទអចលនទ្រព្យ',N'CATEGORY',N'1',N'159',N'0'
UNION ALL SELECT N'161',N'អចលនទ្រព្យ',N'ITEM',N'2',N'159',N'0'
UNION ALL SELECT N'162',N'របាយការណ៍',N'REPORT',N'4',N'151',N'0'
UNION ALL SELECT N'163',N'ប្រតិបត្តិការចំណូល',N'INCOME',N'1',N'162',N'0'
UNION ALL SELECT N'164',N'ប្រតិបត្តិការចំណាយ',N'EXPENSE',N'2',N'162',N'0'
UNION ALL SELECT N'165',N'របាយការណ៍ចំណេញខាត',N'PL',N'3',N'162',N'0'
UNION ALL SELECT N'166',N'របាយការណ៏អចលនទ្រព្យ',N'FIXASSET_SUMMARY',N'4',N'162',N'0'
UNION ALL SELECT N'167',N'របាយការណ៏រំលស់អចលនទ្រព្យ',N'DEPRECIATION',N'5',N'162',N'0'
UNION ALL SELECT N'168',N'ការទទួលប្រាក់',N'CASH_RECEIVE',N'1',N'72',N'0'
UNION ALL SELECT N'169',N'ការប្រើប្រាស់ថាមពល',N'POWERUSAGE',N'8',N'72',N'0'
UNION ALL SELECT N'170',N'ប្រវត្តិការប្រើប្រាស់ថាមពល',N'USAGESUMMARY',N'8',N'72',N'0'
UNION ALL SELECT N'171',N'សេវាទូទាត់ជាប្រចាំ',N'RECURRINGSERVICE',N'9',N'72',N'0'
UNION ALL SELECT N'172',N'ដាក់ប្រាក់ចូលធនាគារ',N'BANKDEPOSIT',N'10',N'72',N'0'
UNION ALL SELECT N'173',N'របាយការណ៍ត្រីមាស',N'QUATERREPORT',N'11',N'72',N'0'
UNION ALL SELECT N'174',N'ភូមិឃុំ ត្រូវចែកចាយ',N'LICENSEVILLAGE',N'1',N'138',N'0'
UNION ALL SELECT N'175',N'អតិថិជនជាអ្នកកាន់អាជ្ញាប័ណ្ណ',N'LICENSEE',N'3',N'138',N'0'
UNION ALL SELECT N'176',N'ការផលិតថាមពល',N'PRODUCTION',N'3',N'132',N'0'
UNION ALL SELECT N'177',N'ម៉ាស៊ីនភ្លើង',N'GENERATOR',N'1',N'176',N'0'
UNION ALL SELECT N'178',N'ការផលិតថាមពល',N'GENERATION',N'2',N'176',N'0'
UNION ALL SELECT N'179',N'របាយការណ៍',N'REPORT',N'6',N'132',N'0'
UNION ALL SELECT N'180',N'ស្ថិតិចំនួនអតិថិជន',N'CUSTOMERSTATISTIC',N'1',N'179',N'0'
UNION ALL SELECT N'181',N'ចំនួនកុងទ័រតាមត្រង់ស្វូ',N'METERBYTRANSFORMER',N'2',N'179',N'0'
UNION ALL SELECT N'182',N'កែប្រែពត៌មានអតិថិជន',N'CUSTOMERBATCHCHANGE',N'2',N'54',N'0'
UNION ALL SELECT N'183',N'ប្រតិបត្តការធនាគារ',N'BANK',N'13',N'34',N'0'
UNION ALL SELECT N'184',N'ដាក់ប្រាក់',N'DEPOSIT',N'1',N'183',N'0'
UNION ALL SELECT N'185',N'ស្តុក',N'STOCK',N'14',N'34',N'0'
UNION ALL SELECT N'186',N'គ្រប់គ្រងទំនិញ',N'ITEM',N'1',N'185',N'0'
UNION ALL SELECT N'187',N'គ្រប់គ្រងស្តុក',N'MGT',N'2',N'185',N'0'
UNION ALL SELECT N'188',N'ស្រង់តាម GPRS',N'GPRS',N'6',N'9',N'0'
UNION ALL SELECT N'189',N'ស្រង់អំណាន',N'USE',N'1',N'188',N'0'
UNION ALL SELECT N'190',N'អតិថិជនមិនបានទិញថាមពលអគ្គិសនី',N'CUSTOMER_NOT_BUY',N'3',N'123',N'0'
UNION ALL SELECT N'191',N'ស្ថិតិការទិញថាមពលអគ្គិសនី',N'BUYSTATISITIC',N'4',N'123',N'0'
UNION ALL SELECT N'192',N'លុបការទិញថាមពលអគិ្គសនី',N'VOIDPOWER',N'5',N'123',N'0'
UNION ALL SELECT N'193',N'ប្រតិបត្តិការផ្តាច់និងភ្ជាប់ចរន្ត',N'BLOCKANDRECONNECT',N'4',N'66',N'0'
UNION ALL SELECT N'194',N'ពិនិត្យព័ត៌មានលើកុងទ័រ',N'READDATA',N'5',N'114',N'0'
SET IDENTITY_INSERT TBL_PERMISSION OFF;
GO
DELETE FROM TBL_ROLE_PERMISSION WHERE ROLE_ID = 1
GO
INSERT INTO TBL_ROLE_PERMISSION 
SELECT	ROLE_ID=1,
		PERMISSION_ID,
		IS_ALLOWED=1
FROM TBL_PERMISSION;  
GO

/*
SELECT 'UNION ALL SELECT '+
'N'''+CONVERT(NVARCHAR(MAX),ACCOUNT_ID)+''''+','+
'N'''+CONVERT(NVARCHAR(MAX),ACCOUNT_CODE)+''''+','+
'N'''+CONVERT(NVARCHAR(MAX),ACCOUNT_NAME)+''''+','+
'N'''+CONVERT(NVARCHAR(MAX),ACCOUNT_NAME_EN)+''''+','+
'N'''+CONVERT(NVARCHAR(MAX),NOTE)+''''+','+
'N'''+CONVERT(NVARCHAR(MAX),PARENT_ID)+''''+','+
'N'''+CONVERT(NVARCHAR(MAX),POST_TYPE_ID)+''''+','+
'N'''+CONVERT(NVARCHAR(MAX),IS_ACTIVE)+'''' 
FROM TBL_ACCOUNT_CHART
WHERE IS_ACTIVE=1; 
*/
TRUNCATE TABLE TBL_ACCOUNT_CHART;
IF NOT EXISTS(SELECT * FROM TBL_ACCOUNT_CHART) 
BEGIN
SET IDENTITY_INSERT TBL_ACCOUNT_CHART ON;
INSERT INTO TBL_ACCOUNT_CHART(ACCOUNT_ID,ACCOUNT_CODE,ACCOUNT_NAME,ACCOUNT_NAME_EN,NOTE,PARENT_ID,POST_TYPE_ID,IS_ACTIVE)
SELECT N'3',N'1000',N'ទ្រព្យ',N'Asset',N'',N'0',N'1',N'1'
UNION ALL SELECT N'5',N'3000',N'បំណុល',N'Liability',N'',N'0',N'2',N'1'
UNION ALL SELECT N'6',N'4000',N'ចំណូល',N'Income',N'',N'0',N'2',N'1'
UNION ALL SELECT N'7',N'5000',N'ចំណាយ',N'Expense',N'',N'0',N'1',N'1'
UNION ALL SELECT N'8',N'1000',N'អចលនទ្រព្យ',N'Fixed Asset',N'',N'3',N'1',N'1'
UNION ALL SELECT N'9',N'1500',N'ចលនទ្រព្យ',N'Virable Asset',N'',N'3',N'1',N'1'
UNION ALL SELECT N'10',N'2000',N'មូលធន',N'',N'  ',N'0',N'2',N'1'
UNION ALL SELECT N'11',N'3000',N'បំណុលរយៈពេលវែង',N'Long-term Liability',N'',N'5',N'2',N'1'
UNION ALL SELECT N'12',N'3500',N'បំណុលរយៈពេលខ្លី',N'Short-term liablity',N'',N'5',N'2',N'1'
UNION ALL SELECT N'13',N'3800',N'ចំណូលដែលបានទទួលមុន',N'',N'',N'5',N'2',N'1'
UNION ALL SELECT N'14',N'3900',N'សំវិធានធនអាជីវកម្ម',N'',N'',N'5',N'2',N'1'
UNION ALL SELECT N'15',N'4000',N'ចំណូលអាជីវកម្ម',N'',N'',N'6',N'2',N'1'
UNION ALL SELECT N'16',N'4000',N'ចំណូលបានមកពីការលក់អគ្គិសនី',N'',N'',N'15',N'2',N'1'
UNION ALL SELECT N'17',N'4200',N'ចំណូលផ្សេងៗ',N'',N'',N'15',N'2',N'1'
UNION ALL SELECT N'18',N'4300',N'ចំណូលផ្សេងៗពីអាជីវកម្មអគ្គិសនី',N'',N'',N'15',N'2',N'1'
UNION ALL SELECT N'19',N'4400',N'ចំណូលផ្សេងៗទៀតពីអាជីវកម្មអគ្គិសនី',N'',N'',N'15',N'2',N'1'
UNION ALL SELECT N'20',N'5000',N'ចំណាយអាជីវកម្ម',N'',N'',N'7',N'1',N'1'
UNION ALL SELECT N'21',N'5000',N'ចំណាយទិញអគ្គិសនី',N'',N'',N'20',N'1',N'1'
UNION ALL SELECT N'22',N'5100',N'ផ្នែកផលិតកម្ម-ចំណាយដំណើរការ និងការថែទាំ',N'',N'',N'20',N'1',N'1'
UNION ALL SELECT N'23',N'5400',N'ផ្នែកបញ្ជូន-ចំណាយដំណើរការ និងការថែទាំ',N'',N'',N'20',N'1',N'1'
UNION ALL SELECT N'24',N'5600',N' ផ្នែកចែកចាយ-ចំណាយដំណើរការ និងការថែទាំ',N'',N'',N'20',N'1',N'1'
UNION ALL SELECT N'25',N'5800',N'ចំណាយលើគណនីអតិថិជន',N'',N'',N'20',N'1',N'1'
UNION ALL SELECT N'26',N'5900',N'រំលស់',N'',N'',N'20',N'1',N'1'
UNION ALL SELECT N'27',N'6000',N'ចំណាយរដ្ឋបាល និងចំណាយទូទៅ',N'',N'',N'20',N'1',N'1'
UNION ALL SELECT N'28',N'7000',N'ចំណាយហិរញ្ញវត្ថុ',N'',N'',N'7',N'1',N'1'
UNION ALL SELECT N'29',N'7100',N'ចំណូល/(ចំណាយ)ក្រៅពីប្រតិបត្តិការអាជីវកម្ម',N'',N'',N'6',N'2',N'1'
UNION ALL SELECT N'30',N'7000',N'ចំណាយការប្រាក់លើបំណុលរយៈពេលវែង',N'',N'',N'28',N'1',N'1'
UNION ALL SELECT N'31',N'7010',N'ចំណាយការប្រាក់លើការសាងសង់',N'',N'',N'28',N'1',N'1'
UNION ALL SELECT N'32',N'7020',N'ចំណាយផាកពិន័យលើប្រាក់កម្ចី',N'',N'',N'28',N'1',N'1'
UNION ALL SELECT N'33',N'7030',N'ចំណាយការប្រាក់ផ្សេងៗ',N'',N'',N'28',N'1',N'1'
UNION ALL SELECT N'34',N'7100',N'ចំណូលការប្រាក់',N'',N'',N'29',N'2',N'1'
UNION ALL SELECT N'35',N'7120',N'ចំណេញ/(ខាត)លើការលក់(ខូច)ទ្រព្យ និងបរិក្ខារ',N'',N'',N'29',N'2',N'1'
UNION ALL SELECT N'36',N'7140',N'ចំណូលក្រៅពីប្រតិបត្តិការអាជីវកម្ម',N'',N'',N'29',N'2',N'1'
UNION ALL SELECT N'37',N'7160',N'ចំណេញ/(ខាត)លើអត្រាប្តូរប្រាក់',N'',N'',N'29',N'2',N'1'
UNION ALL SELECT N'38',N'7190',N'ចំណាយក្រៅពីប្រតិបត្តិការអាជីវកម្ម',N'',N'',N'29',N'2',N'1'
UNION ALL SELECT N'39',N'1000',N'ទ្រព្យប្រើក្នុងសេវាកម្ម',N'',N'',N'8',N'1',N'1'
UNION ALL SELECT N'40',N'1005',N'ដី',N'',N'',N'39',N'1',N'1'
UNION ALL SELECT N'41',N'1010',N'កែលម្អដី',N'',N'',N'39',N'1',N'1'
UNION ALL SELECT N'42',N'1015',N'អគារ',N'',N'',N'39',N'1',N'1'
UNION ALL SELECT N'43',N'1020',N'មធ្យោបាយផលិតកម្មអគ្គិសនី',N'',N'',N'39',N'1',N'1'
UNION ALL SELECT N'44',N'1040',N'មធ្យោបាយផ្នែកបណ្តាញបញ្ជូនអគ្គិសនី',N'',N'',N'39',N'1',N'1'
UNION ALL SELECT N'45',N'1050',N'មធ្យោបាយចែកចាយអគ្គិសនី',N'',N'',N'39',N'1',N'1'
UNION ALL SELECT N'46',N'1080',N'មធ្យោបាយធ្វើការងារ',N'',N'',N'39',N'1',N'1'
UNION ALL SELECT N'60',N'1250',N'ការវិនិយោគ',N'',N'',N'8',N'1',N'1'
UNION ALL SELECT N'61',N'1260',N'ការវិនិយោគមូលបត្រជាមួយក្រុមហ៊ុនពាក់ព័ន្ធ',N'',N'',N'60',N'1',N'1'
UNION ALL SELECT N'62',N'1270',N'បុរេប្រទានរយៈពេលវែងជាមួយក្រុមហ៊ុនពាក់ព័ន្ធ',N'',N'',N'60',N'1',N'1'
UNION ALL SELECT N'63',N'1290',N'ការវិនិយោគផ្សេងៗ',N'',N'',N'60',N'1',N'1'
UNION ALL SELECT N'70',N'1400',N'អចលនទ្រព្យផ្សេងទៀត',N'',N'',N'8',N'1',N'1'
UNION ALL SELECT N'71',N'1500',N'សាច់ប្រាក់ និងមូលនិធិកំពុងចរាចរ',N'',N'',N'9',N'1',N'1'
UNION ALL SELECT N'72',N'1510',N'សាច់ប្រាក់ក្នុងកេះ',N'',N'',N'71',N'1',N'1'
UNION ALL SELECT N'73',N'1600',N'សាច់ប្រាក់ក្នុងធនាគារ',N'',N'',N'71',N'1',N'1'
UNION ALL SELECT N'74',N'1650',N'មូលនិធិកំពុងចរាចរ',N'',N'',N'71',N'1',N'1'
UNION ALL SELECT N'75',N'1700',N'ការវិនិយោគរយៈពេលខ្លី',N'',N'',N'9',N'1',N'1'
UNION ALL SELECT N'76',N'1720',N'ប្រាក់កក់ពិសេស',N'',N'',N'9',N'1',N'1'
UNION ALL SELECT N'77',N'1740',N'គណនីបំណុលត្រូវទារ-អតិថិជន',N'',N'',N'9',N'1',N'1'
UNION ALL SELECT N'78',N'1750',N'សំវិធានធនសម្រាប់បំណុលពិបាកទារ',N'',N'',N'9',N'1',N'1'
UNION ALL SELECT N'79',N'1760',N'បុរេប្រទានចំពោះមន្រ្តី-បុគ្គលិក',N'',N'',N'9',N'1',N'1'
UNION ALL SELECT N'80',N'1770',N'គណនីបំណុលត្រូវទារ-ផ្សេងៗ',N'',N'',N'9',N'1',N'1'
UNION ALL SELECT N'81',N'1780',N'ចំណូលត្រូវទទួល',N'',N'',N'9',N'1',N'1'
UNION ALL SELECT N'82',N'1790',N'កម្ចី និងប័ណ្ណត្រូវទារ',N'',N'',N'9',N'1',N'1'
UNION ALL SELECT N'83',N'1800',N'បំណុលត្រូវទារពីក្រុមហ៊ុនពាក់ព័ន្ធ',N'',N'',N'9',N'1',N'1'
UNION ALL SELECT N'84',N'1820',N'ការប្រាក់ និងភាគលាភត្រូវទទួល',N'',N'',N'9',N'1',N'1'
UNION ALL SELECT N'85',N'1840',N'សន្និធិ-វត្ថុធាតុដើម និងសម្ភារៈផ្គត់ផ្គង់',N'',N'',N'9',N'1',N'1'
UNION ALL SELECT N'86',N'1900',N'ប្រាក់បង់មុន',N'',N'',N'9',N'1',N'1'
UNION ALL SELECT N'87',N'1950',N'ចំណាយដែលបានបង់មុន',N'',N'',N'9',N'1',N'1'
UNION ALL SELECT N'88',N'1990',N'ចលនទ្រព្យផ្សេងៗ',N'',N'',N'9',N'1',N'1'
UNION ALL SELECT N'89',N'2000',N'មូលធនភាគហ៊ុន',N'',N'',N'10',N'2',N'1'
UNION ALL SELECT N'92',N'2100',N'គណនីឯកកម្មសិទ្ធ',N'',N'',N'10',N'2',N'1'
UNION ALL SELECT N'93',N'2150',N'ការឧបត្ថម្ភធនរបស់រដ្ឋាភិបាល',N'',N'',N'10',N'2',N'1'
UNION ALL SELECT N'94',N'2170',N'ជំនួយឧបត្ថម្ភសម្រាប់ការសាងសង់',N'',N'',N'10',N'2',N'1'
UNION ALL SELECT N'95',N'2200',N'ឧបត្ថម្ភធនផ្សេងៗ',N'',N'',N'10',N'2',N'1'
UNION ALL SELECT N'96',N'2300',N'ការវាយតម្លៃឡើងវិញ លើស/ខ្វះ',N'',N'',N'10',N'2',N'1'
UNION ALL SELECT N'97',N'2400',N'ប្រាក់ចំណេញរក្សាទុក',N'',N'',N'10',N'2',N'1'
UNION ALL SELECT N'101',N'3010',N'បំណុលត្រូវសងរយៈពេលវែង-បរទេស',N'',N'',N'11',N'2',N'1'
UNION ALL SELECT N'102',N'3100',N'បំណុលត្រូវសងរយៈពេលវែង-ក្រុមហ៊ុនពាក់ព័ន្ធ',N'',N'',N'11',N'2',N'1'
UNION ALL SELECT N'103',N'3150',N'ប្រាក់កក់របស់អតិថិជន',N'',N'',N'11',N'2',N'1'
UNION ALL SELECT N'104',N'3200',N'បំណុលត្រូវសងរយៈពេលវែង-ផ្សេងៗ',N'',N'',N'11',N'2',N'1'
UNION ALL SELECT N'105',N'3500',N'បំណុលត្រូវសង',N'',N'',N'12',N'2',N'1'
UNION ALL SELECT N'106',N'3600',N'កម្ចីរយៈពេលខ្លី',N'',N'',N'12',N'2',N'1'
UNION ALL SELECT N'107',N'3620',N'ភាគខ្លះនៃបំណុលរយៈពេលវែងត្រូវសង',N'',N'',N'12',N'2',N'1'
UNION ALL SELECT N'108',N'3640',N'ការប្រាក់ត្រូវសងលើការខ្ចីបុល',N'',N'',N'12',N'2',N'1'
UNION ALL SELECT N'109',N'3660',N'បំណុលត្រូវសងទៅក្រុមហ៊ុនពាក់ព័ន្ធ',N'',N'',N'12',N'2',N'1'
UNION ALL SELECT N'110',N'3680',N'ប្រាក់ខែត្រូវបើក',N'',N'',N'12',N'2',N'1'
UNION ALL SELECT N'111',N'3700',N'ពន្ធកាត់ទុកត្រូវបង់',N'',N'',N'12',N'2',N'1'
UNION ALL SELECT N'112',N'3740',N'ពន្ធផ្សេងៗទៀតត្រូវបង់',N'',N'',N'12',N'2',N'1'
UNION ALL SELECT N'113',N'3790',N'បំណុលផ្សេងៗទៀតត្រូវសង',N'',N'',N'12',N'2',N'1'
UNION ALL SELECT N'114',N'3800',N'ការបង់មុនរបស់អតិថិជនសម្រាប់ការសាងសង់',N'',N'',N'13',N'2',N'1'
UNION ALL SELECT N'115',N'3850',N'ចំណូលដែលបានទទួលមុនផ្សេងៗ',N'',N'',N'13',N'2',N'1'
UNION ALL SELECT N'116',N'3900',N'សំវិធានធនសម្រាប់ធានារ៉ាប់រងទ្រព្យសម្បត្តិ',N'',N'',N'14',N'2',N'1'
UNION ALL SELECT N'117',N'3920',N'សំវិធានធនសម្រាប់គ្រោះថ្នាក់ការងារ និងការខូចខាត',N'',N'',N'14',N'2',N'1'
UNION ALL SELECT N'118',N'3940',N'សំវិធានធនសម្រាប់សោធន និងអត្ថប្រយោជន៍',N'',N'',N'14',N'2',N'1'
UNION ALL SELECT N'119',N'3950',N'សំវិធានធនអាជីវកម្មផ្សេងៗ',N'',N'',N'14',N'2',N'1'
UNION ALL SELECT N'120',N'4010',N'លំនៅដ្ឋាន',N'',N'',N'16',N'2',N'1'
UNION ALL SELECT N'121',N'4020',N'លំនៅដ្ឋានជនបរទេស',N'',N'',N'16',N'2',N'1'
UNION ALL SELECT N'122',N'4030',N'ពាណិជ្ជកម្ម/អាជីវកម្ម',N'',N'',N'16',N'2',N'1'
UNION ALL SELECT N'123',N'4040',N'ឧស្សាហកម្ម/សិប្បកម្ម',N'',N'',N'16',N'2',N'1'
UNION ALL SELECT N'124',N'4050',N'ស្ថាប័នរដ្ឋាភិបាល',N'',N'',N'16',N'2',N'1'
UNION ALL SELECT N'125',N'4060',N'បំភ្លឺតាមផ្លូវសាធារណៈ',N'',N'',N'16',N'2',N'1'
UNION ALL SELECT N'126',N'4070',N'លក់តាមរយៈនាឡិកាស្ទង់ខូច',N'',N'',N'16',N'2',N'1'
UNION ALL SELECT N'127',N'4090',N'ផ្សេងៗ',N'',N'',N'16',N'2',N'1'
UNION ALL SELECT N'128',N'4310',N'ចំណូលពីការភ្ជាប់ចរន្ត/ផ្តាច់ចរន្ត',N'',N'',N'18',N'2',N'1'
UNION ALL SELECT N'129',N'4320',N'ចំណូលពីប្រាក់កក់របស់អតិថិជន',N'',N'',N'18',N'2',N'1'
UNION ALL SELECT N'130',N'4330',N'ចំណូលពីការថ្លឹង/ប្តូរនាឡិការស្ទង់',N'',N'',N'18',N'2',N'1'
UNION ALL SELECT N'131',N'4340',N'ចំណូលពីការផាកពិន័យលើការប្រើប្រាស់អប្បបរិមា',N'',N'',N'18',N'2',N'1'
UNION ALL SELECT N'132',N'4350',N'សេវារដ្ឋបាល និងសេវាផ្សេងៗ',N'',N'',N'18',N'2',N'1'
UNION ALL SELECT N'133',N'4010',N'ចំណូលផាកពិន័យ',N'',N'',N'19',N'2',N'1'
UNION ALL SELECT N'135',N'4420',N'ចំណូលផាកពិន័យពីការបង់យឺត',N'',N'',N'19',N'2',N'1'
UNION ALL SELECT N'136',N'4430',N'ចំណូលពីការជួលទ្រព្យអាជីវកម្ម',N'',N'',N'19',N'2',N'1'
UNION ALL SELECT N'137',N'5020',N'ចំណាយលើអានុភាព ',N'',N'',N'21',N'1',N'1'
UNION ALL SELECT N'138',N'5110',N'ប្រេងឥន្ធនៈ',N'',N'',N'22',N'1',N'1'
UNION ALL SELECT N'139',N'5120',N'ប្រេងរំអិល និងការប្រើប្រាស់ផ្សេងៗ',N'',N'',N'22',N'1',N'1'
UNION ALL SELECT N'140',N'5130',N'ទឹក',N'',N'',N'22',N'1',N'1'
UNION ALL SELECT N'141',N'5200',N'ចំណាយគ្រប់គ្រង និងវិស្វកម្ម',N'',N'',N'22',N'1',N'1'
UNION ALL SELECT N'142',N'5210',N'ធានារ៉ាប់រង',N'',N'',N'22',N'1',N'1'
UNION ALL SELECT N'143',N'5220',N'ជួល',N'',N'',N'22',N'1',N'1'
UNION ALL SELECT N'144',N'5230',N'ជួសជុល និងថែទាំម៉ាស៊ីនផលិត',N'',N'',N'22',N'1',N'1'
UNION ALL SELECT N'145',N'5240',N'គ្រឿងបន្លាស់',N'',N'',N'22',N'1',N'1'
UNION ALL SELECT N'146',N'5250',N'សម្ភារៈ និងការផ្គត់ផ្គង់',N'',N'',N'22',N'1',N'1'
UNION ALL SELECT N'147',N'5290',N'ចំណាយផ្សេងៗផ្នែកផលិតកម្ម',N'',N'',N'22',N'1',N'1'
UNION ALL SELECT N'148',N'5420',N'ចំណាយផ្នែកបែងចែកបន្ទុក',N'',N'',N'23',N'1',N'1'
UNION ALL SELECT N'149',N'5430',N'ចំណាយផ្នែកបន្ទប់ភ្លើង',N'',N'',N'23',N'1',N'1'
UNION ALL SELECT N'151',N'4490',N'ចំណូលផ្សេងៗ',N'',N'',N'19',N'2',N'1'
UNION ALL SELECT N'152',N'5010',N'ចំណាយទិញអគ្គិសនី',N'',N'',N'21',N'1',N'1'
UNION ALL SELECT N'153',N'5410',N'ចំណាយគ្រប់គ្រង និងវិស្វកម្ម',N'',N'',N'23',N'1',N'1'
UNION ALL SELECT N'154',N'5435',N'គ្រឿងបន្លាស់',N'',N'',N'23',N'1',N'1'
UNION ALL SELECT N'155',N'5440',N'ជួសជុល និងថែទាំបណ្តាញ',N'',N'',N'23',N'1',N'1'
UNION ALL SELECT N'156',N'5450',N'សម្ភារៈ និងការផ្គត់ផ្គង់',N'',N'',N'23',N'1',N'1'
UNION ALL SELECT N'157',N'5460',N'ជួល',N'',N'',N'23',N'1',N'1'
UNION ALL SELECT N'158',N'5490',N'ចំណាយផ្សេងៗលើផ្នែកបញ្ជូន',N'',N'',N'23',N'1',N'1'
UNION ALL SELECT N'159',N'5610',N'ចំណាយគ្រប់គ្រង និងវិស្វកម្ម',N'',N'',N'24',N'1',N'1'
UNION ALL SELECT N'160',N'5620',N'ចំណាយផ្នែកបែងចែកបន្ទុក',N'',N'',N'24',N'1',N'1'
UNION ALL SELECT N'161',N'5630',N'ចំណាយផ្នែកបន្ទប់ភ្លើង',N'',N'',N'24',N'1',N'1'
UNION ALL SELECT N'162',N'5635',N'គ្រឿងបន្លាស់',N'',N'',N'24',N'1',N'1'
UNION ALL SELECT N'163',N'5640',N'ជួសជុល និងថែទាំបណ្តាញ',N'',N'',N'24',N'1',N'1'
UNION ALL SELECT N'164',N'5645',N'សម្ភារៈ និងការផ្គត់ផ្គង់',N'',N'',N'24',N'1',N'1'
UNION ALL SELECT N'165',N'5650',N'ចំណាយបំភ្លឺសាធារណៈ និងប្រព័ន្ធឱ្យសញ្ញាផ្សេងៗ',N'',N'',N'24',N'1',N'1'
UNION ALL SELECT N'166',N'5660',N'ចំណាយលើការថែទាំនាឡិការស្ទង់',N'',N'',N'24',N'1',N'1'
UNION ALL SELECT N'167',N'5670',N'ចំណាយលើការតម្លើងជូនអតិថិជន',N'',N'',N'24',N'1',N'1'
UNION ALL SELECT N'168',N'5680',N'ជួល',N'',N'',N'24',N'1',N'1'
UNION ALL SELECT N'169',N'5690',N'ចំណាយផ្សេងៗលើផ្នែកចែកចាយ',N'',N'',N'24',N'1',N'1'
UNION ALL SELECT N'170',N'5810',N'ចំណាយគ្រប់គ្រង',N'',N'',N'25',N'1',N'1'
UNION ALL SELECT N'171',N'5820',N'ចំណាយលើព័ត៌មាន និងសេវាអតិថិជន',N'',N'',N'25',N'1',N'1'
UNION ALL SELECT N'172',N'5830',N'ចំណាយលើការស្រង់អំណាន និងការប្រមូលប្រាក់',N'',N'',N'25',N'1',N'1'
UNION ALL SELECT N'173',N'5840',N'បំណុលទារមិនបាន',N'',N'',N'25',N'1',N'1'
UNION ALL SELECT N'174',N'5850',N'កម្រៃជើងសារ',N'',N'',N'25',N'1',N'1'
UNION ALL SELECT N'175',N'5890',N'ចំណាយផ្សេងៗទៀតលើគណនីអតិថិជន',N'',N'',N'25',N'1',N'1'
UNION ALL SELECT N'176',N'5910',N'រំលស់អគារ',N'',N'',N'26',N'1',N'1'
UNION ALL SELECT N'177',N'5920',N'រំលស់ផ្នែកមធ្យោបាយផលិតកម្មអគ្គិសនី',N'',N'',N'26',N'1',N'1'
UNION ALL SELECT N'178',N'5940',N'រំលស់ផ្នែកបណ្តាញបញ្ជូនអគ្គិសនី',N'',N'',N'26',N'1',N'1'
UNION ALL SELECT N'179',N'5960',N'រំលស់ផ្នែកមធ្យោបាយចែកចាយអគ្គិសនី',N'',N'',N'26',N'1',N'1'
UNION ALL SELECT N'180',N'5980',N'រំលស់ផ្នែកមធ្យោបាយធ្វើការងារ',N'',N'',N'26',N'1',N'1'
UNION ALL SELECT N'199',N'6010',N'ប្រាក់ខែទូទៅ',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'200',N'6020',N'ប្រាក់ធ្វើការលើសម៉ោង និងឧបត្ថម្ភផ្សេងៗ',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'201',N'6030',N'ចំណាយលើបុគ្គលិកបណ្តែត',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'202',N'6040',N'បន្ទុកសន្តិសុខសង្គម',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'203',N'6050',N'បុព្វលាភ និងរង្វាន់លើកទឹកចិត្ត',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'204',N'6060',N'ប្រាក់ឧបត្ថម្ភម្ហូមអាហារ និងអត្ថប្រយោជន៍ផ្សេងៗ',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'205',N'6070',N'ប្រាក់ឧបត្ថម្ភផ្សេងៗទៀត',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'207',N'6100',N'បណ្តុះបណ្តាល និងអភិវឌ្ឍន៍បុគ្គលិក',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'208',N'6110',N'បេសកកម្មក្រៅប្រទេស',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'209',N'6120',N'បេសកកម្មក្នុងស្រុក និងការទទួលភ្ញៀវ',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'210',N'6130',N'ការដឹកជញ្ជូន និងការធ្វើដំណើរក្នុងស្រុក',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'211',N'6140',N'ចំណាយលើប្រេងឥន្ធនៈបុគ្គលិក',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'212',N'6150',N'ចំណាយសម្ភារៈការិយាល័យទូទៅ',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'213',N'6160',N'សេវាប្រៃសណីយ៍ និងទូរគមនាគមន៍',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'214',N'6200',N'ពន្ធ',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'219',N'6300',N'ជួល',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'220',N'6310',N'ទឹក-ភ្លើង',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'221',N'6320',N'ជួសជុល និងថែទាំ',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'222',N'6350',N'សេវាសវនកម្ម និងច្បាប់',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'223',N'6360',N'កម្រៃអាជ្ញាប័ណ្ណ',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'224',N'6370',N'ចំណាយលើការគ្រប់គ្រង',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'225',N'6400',N'ធានារ៉ាប់រង',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'226',N'6410',N'សំណងលើការខូចខាត និងគ្រោះថ្នាក់ការងារ',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'227',N'6450',N'សេវាធនាគារ',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'228',N'6500',N'ពិធីជប់លៀង និងការសំដែង',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'229',N'6510',N'អំណោយ និងវិភាគទាន',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'230',N'6590',N'ចំណាយទូទៅផ្សេងៗ',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'240',N'4380',N'ឈ្នួលលើការតម្លើងបណ្តាញ',N'',N'',N'18',N'2',N'1'
UNION ALL SELECT N'241',N'6210',N'ពន្ធលើប្រេងឥន្ធនៈ',N'',N'',N'214',N'1',N'1'
UNION ALL SELECT N'242',N'6220',N'ពន្ធលើប្រាក់ចំណេញដុល',N'',N'',N'214',N'1',N'1'
UNION ALL SELECT N'243',N'6240',N'ពន្ធលើមធ្យោបាយដឹកជញ្ជូន',N'',N'',N'214',N'1',N'1'
UNION ALL SELECT N'244',N'6230',N'ពន្ធប៉ាតង់',N'',N'',N'214',N'1',N'1'
UNION ALL SELECT N'245',N'6250',N'ពន្ធផ្សេងៗទៀត',N'',N'',N'214',N'1',N'1'
UNION ALL SELECT N'246',N'6290',N'ពន្ធលើផលរបរ',N'',N'',N'214',N'1',N'1'
UNION ALL SELECT N'248',N'1100',N'ដករំលស់បូកយោង-ទ្រព្យប្រើក្នុងសេវាកម្ម ',N'',N'',N'8',N'1',N'1'
UNION ALL SELECT N'249',N'1105',N'រំលស់បូកយោង-អគារ',N'',N'',N'248',N'1',N'1'
UNION ALL SELECT N'250',N'1110',N'រំលស់បូកយោង-មធ្យោបាយផលិតកម្មអគ្គិសនី',N'',N'',N'248',N'1',N'1'
UNION ALL SELECT N'251',N'1130',N'រំលស់បូកយោង-មធ្យោបាយផ្នែកបណ្តាញបញ្ជូន',N'',N'',N'248',N'1',N'1'
UNION ALL SELECT N'252',N'1150',N'រំលស់បូកយោង-មធ្យោបាយចែកចាយអគ្គិសនី',N'',N'',N'248',N'1',N'1'
UNION ALL SELECT N'253',N'1170',N'រំលស់បូកយោង-មធ្យោបាយធ្វើការងារ',N'',N'',N'248',N'1',N'1'
UNION ALL SELECT N'254',N'1180',N'ការកែសម្រួលទ្រព្យប្រើក្នុងសេវាកម្ម',N'',N'',N'8',N'1',N'1'
UNION ALL SELECT N'255',N'1190',N'ដករំលស់បូកយោង-ការកែសម្រួលទ្រព្យប្រើក្នុងសេវាកម្ម',N'',N'',N'8',N'1',N'1'
UNION ALL SELECT N'256',N'1200',N'ការងារសាងសង់កំពុងដំណើរការ',N'',N'',N'8',N'1',N'1'
UNION ALL SELECT N'257',N'1210',N'ទ្រព្យ និងបរិក្ខារបម្រុងប្រើនាពេលខាងមុខ',N'',N'',N'8',N'1',N'1'
UNION ALL SELECT N'258',N'1220',N'ដករំលស់បូកយោង-ទ្រព្យ និងបរិក្ខារបម្រុងប្រើនាពេលខាងមុខ',N'',N'',N'8',N'1',N'1'
UNION ALL SELECT N'259',N'1230',N'ទ្រព្យមិនអាចចាត់ចំណាត់ថ្នាក់',N'',N'',N'8',N'1',N'1'
UNION ALL SELECT N'260',N'1240',N'ដករំលស់បូកយោង-ទ្រព្យមិនអាចចាត់ចំណាត់ថ្នាក់',N'',N'',N'8',N'1',N'1'
UNION ALL SELECT N'261',N'1300',N'គណនីមូលនិធិ',N'',N'',N'8',N'1',N'1'
UNION ALL SELECT N'262',N'1310',N'មូលនិធិកប់ក្នុងការវិនិយោគ',N'',N'',N'261',N'1',N'1'
UNION ALL SELECT N'263',N'1340',N'មូលនិធិពិសេសផ្សេងៗ',N'',N'',N'261',N'1',N'1'
UNION ALL SELECT N'264',N'1350',N'ទ្រព្យអរូបី',N'',N'',N'8',N'1',N'1'
UNION ALL SELECT N'265',N'1360',N'Organization',N'',N'',N'264',N'1',N'1'
UNION ALL SELECT N'266',N'1390',N'ទ្រព្យអរូបីផ្សេងៗទៀត',N'',N'',N'264',N'1',N'1'
UNION ALL SELECT N'267',N'1850',N'វត្ថុធាតុដើម និងការផ្គត់ផ្គង់-ប្រេងឥន្ធនៈ',N'',N'',N'85',N'1',N'1'
UNION ALL SELECT N'268',N'1860',N'វត្ថុធាតុដើម និងការផ្គត់ផ្គង់-ប្រេងរំអិល',N'',N'',N'85',N'1',N'1'
UNION ALL SELECT N'269',N'1870',N'វត្ថុធាតុដើម និងការផ្គត់ផ្គង់-អគ្គិសនី',N'',N'',N'85',N'1',N'1'
UNION ALL SELECT N'270',N'1880',N'វត្ថុធាតុដើម និងការផ្គត់ផ្គង់-ការិយាល័យ',N'',N'',N'85',N'1',N'1'
UNION ALL SELECT N'271',N'1890',N'វត្ថុធាតុដើម និងការផ្គត់ផ្គង់-ផ្សេងៗ',N'',N'',N'85',N'1',N'1'
UNION ALL SELECT N'273',N'2010',N'ភាគហ៊ុនធម្មតា',N'',N'',N'89',N'2',N'1'
UNION ALL SELECT N'274',N'2020',N'ភាគហ៊ុនអាទិភាព',N'',N'',N'89',N'2',N'1'
UNION ALL SELECT N'275',N'2030',N'មូលធនដាក់បន្ថែម',N'',N'',N'89',N'2',N'1'
UNION ALL SELECT N'276',N'3000',N'បំណុលត្រូវសងរយៈពេលវែង-ក្នុងស្រុក',N'',N'',N'11',N'2',N'1'
SET IDENTITY_INSERT TBL_ACCOUNT_CHART OFF;
END
GO
IF OBJECT_ID('[REPORT_ACCOUNT_PROFIT_LOSS]') IS NOT NULL	
	DROP PROC [REPORT_ACCOUNT_PROFIT_LOSS];
GO
CREATE PROC [dbo].[REPORT_ACCOUNT_PROFIT_LOSS]
	@D1 DATETIME = '2012-01-01',
	@D2 DATETIME = '2015-10-30',
	@DISPLAY_CURRENCY_ID INT=1
AS
SET @D1 = DATEADD(D,0,DATEDIFF(D,0,@D1));
SET @D2 = DATEADD(S,-1,DATEADD(D,1,DATEDIFF(D,0,@D2)));


-- POPULATE P&L ACCOUNT_TREE
SELECT * INTO #ACCOUNT_TREE
FROM GET_ACCOUNT_CHART_ACENSTORS(-1);

INSERT INTO  #ACCOUNT_TREE SELECT * FROM GET_ACCOUNT_CHART_ACENSTORS(15);
INSERT INTO  #ACCOUNT_TREE SELECT * FROM GET_ACCOUNT_CHART_ACENSTORS(20);
INSERT INTO  #ACCOUNT_TREE SELECT * FROM GET_ACCOUNT_CHART_ACENSTORS(28);
INSERT INTO  #ACCOUNT_TREE SELECT * FROM GET_ACCOUNT_CHART_ACENSTORS(29); 

-- P&L ROOT ACCOUNT
SELECT *
INTO #ACCOUNT_TREE_ROOT
FROM #ACCOUNT_TREE 
WHERE ACCOUNT_ID IN(SELECT PARENT_ID FROM TBL_ACCOUNT_CHART WHERE IS_ACTIVE=1) 

-- P&L LEAF ACCOUNT
SELECT t.*,a.POST_TYPE_ID
INTO #ACCOUNT_TREE_LEAF
FROM #ACCOUNT_TREE t
INNER JOIN TBL_ACCOUNT_CHART a ON t.ACCOUNT_ID =a.ACCOUNT_ID
WHERE t.ACCOUNT_ID NOT IN(SELECT PARENT_ID FROM TBL_ACCOUNT_CHART WHERE IS_ACTIVE=1) 

 
-- USER INPUT
SELECT 	ACCOUNT_ID = TRAN_ACCOUNT_ID,  
		AMOUNT  = AMOUNT * dbo.GET_EXCHANGE_RATE(CURRENCY_ID,@DISPLAY_CURRENCY_ID,TRAN_DATE)
INTO #TMP
FROM TBL_ACCOUNT_TRAN tr 
WHERE tr.IS_ACTIVE=1
	  AND tr.TRAN_DATE BETWEEN @D1 AND @D2;
-- INVOICE POWER
INSERT INTO #TMP
SELECT t.INCOME_ACCOUNT_ID,
	   aMOUN = i.SETTLE_AMOUNT * dbo.GET_EXCHANGE_RATE(CURRENCY_ID,@DISPLAY_CURRENCY_ID,INVOICE_DATE)
FROM TBL_INVOICE i
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = i.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_TYPE t ON t.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
WHERE INVOICE_MONTH BETWEEN @D1 AND @D2
	 AND IS_SERVICE_BILL=0;
-- POWER PREPAID
INSERT INTO #TMP
SELECT t.INCOME_ACCOUNT_ID,
		AMOUNT = BUY_AMOUNT*dbo.GET_EXCHANGE_RATE(CURRENCY_ID,@DISPLAY_CURRENCY_ID,CREATE_ON)
FROM TBL_CUSTOMER_BUY b
INNER JOIN TBL_PREPAID_CUSTOMER p ON b.PREPAID_CUS_ID=p.PREPAID_CUS_ID
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=p.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_TYPE t ON t.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
WHERE CREATE_ON BETWEEN @D1 AND @D2
-- INVOICE SERVICE
INSERT INTO #TMP
SELECT d.INCOME_ACCOUNT_ID,
	   AMOUNT = i.SETTLE_AMOUNT * dbo.GET_EXCHANGE_RATE(CURRENCY_ID,@DISPLAY_CURRENCY_ID,INVOICE_DATE)
FROM TBL_INVOICE i
OUTER APPLY(
	SELECT TOP 1 INCOME_ACCOUNT_ID 
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE_ITEM t ON t.INVOICE_ITEM_ID=d.INVOICE_ITEM_ID
	WHERE INVOICE_ID=i.INVOICE_ID 
) d
WHERE INVOICE_MONTH BETWEEN @D1 AND @D2
	 AND IS_SERVICE_BILL=1;
-- DEPRECIATION
INSERT INTO #TMP
SELECT c.DEPRECIATION_EXPENSE_ID,
	   AMOUNT = d.AMOUNT * dbo.GET_EXCHANGE_RATE(d.CURRENCY_ID,@DISPLAY_CURRENCY_ID,DEPRECIATION_DATE) 
FROM TBL_DEPRECIATION d
INNER JOIN TBL_FIX_ASSET_ITEM i ON i.FIX_ASSET_ITEM_ID=d.FIX_ASSET_ITEM_ID
INNER JOIN TBL_FIX_ASSET_CATEGORY c ON c.CATEGORY_ID=i.CATEGORY_ID
WHERE d.IS_ACTIVE  = 1
 AND DEPRECIATION_MONTH BETWEEN @D1 AND @D2
-- FIX ASSET MAINTENANCE
INSERT INTO #TMP
SELECT EXPENSE_MAINTENANCE_ACCOUNT_ID,AMOUNT
FROM TBL_FIX_ASSET_MAINTENANCE fm
WHERE fm.IS_ACTIVE=1 AND TRAN_DATE BETWEEN @D1 AND @D2
-- FIX ASSET DISPOSE
INSERT INTO #TMP
SELECT INCOME_DISPOSE_ACCOUNT_ID,AMOUNT = SOLD_VALUE-BOOK_VALUE
FROM TBL_FIX_ASSET_DISPOSE fd
WHERE fd.IS_ACTIVE = 1 AND TRAN_DATE BETWEEN @D1 AND @D2

 
-- SUMMARY INPUT 
SELECT ACCOUNT_ID,AMOUNT=SUM(AMOUNT)
INTO #TMP_RESULT
FROM #TMP
GROUP BY ACCOUNT_ID



-- DUMMY FOR CREATE HEADER & FOOTER
SELECT	ACCOUNT_ID,
		ACCOUNT_CODE,
		ACCOUNT_NAME,
		PATH=PATH+'-0000',
		LEVEL,
		TYPE=1,
		AMOUNT = 0.0,
		ORIGINAL_AMOUNT = 0.0,
		CURRENCY_ID  = @DISPLAY_CURRENCY_ID
INTO #RESULT_HEADER
FROM #ACCOUNT_TREE_ROOT
 
SELECT	ACCOUNT_ID,
		ACCOUNT_CODE,
		ACCOUNT_NAME,
		PATH=PATH+'-9999',
		LEVEL,	
		TYPE=2,
		AMOUNT = 0.0,
		ORIGINAL_AMOUNT = 0.0,
		CURRENCY_ID  = @DISPLAY_CURRENCY_ID
INTO #RESULT_FOOTER
FROM #ACCOUNT_TREE_ROOT



-- PRCOSESS TO RESULT 
SELECT *  FROM #RESULT_HEADER
UNION ALL
SELECT * FROM #RESULT_FOOTER 
UNION ALL
SELECT	t.ACCOUNT_ID,
		ACCOUNT_CODE,
		ACCOUNT_NAME,
		PATH,
		LEVEL,
		TYPE=0,
		AMOUNT =ISNULL(r.AMOUNT,0.00) ,
		ORIGINAL_AMOUNT= ISNULL(r.AMOUNT,0)*CASE WHEN POST_TYPE_ID=1 THEN -1.00 ELSE 1.00 END,
		CURRENCY_ID  = @DISPLAY_CURRENCY_ID
FROM  #ACCOUNT_TREE_LEAF t
LEFT JOIN #TMP_RESULT r ON t.ACCOUNT_ID = r.ACCOUNT_ID
ORDER BY PATH; 
-- END OF [REPORT_ACCOUNT_PROFIT_LOSS]
"; 
        }

    }
}
