﻿namespace EPower.Update
{
    /// <summary>
    /// FIX 
    ///  - ANR.RUN_AS_03
    ///     - Merge Administration to Destribution in AS_10
    ///  - ANR.RUN_AS_06
    ///     - Fix InYear Interest and Ending Balance Filter Is Active
    /// </summary>
    /// <returns></returns>

    class Version1_1_0_3 : Version
    {        
        protected override string GetBatchCommand()
        {
            return @"
GO
IF OBJECT_ID('ANR.RUN_AS_03') IS NOT NULL
	DROP PROC ANR.RUN_AS_03
GO

-- RUN ANR.RUN_AS_03
CREATE PROC ANR.RUN_AS_03
	@YEAR_ID INT = 2015,
	@CURRENCY_ID INT = 1
AS
DECLARE @PRODUCTION INT
	,@TRANSMISSION INT
	,@DISTRIBUTION INT;
SET @PRODUCTION = 6;
SET @TRANSMISSION = 5;
SET @DISTRIBUTION = 7;
--------------------
-- CLEAR LAST RESULT
--------------------
DELETE ANR.TBL_RESULT_03 WHERE YEAR_ID = @YEAR_ID; 
-- ADD TMP 
-- 1.DIVISION BUSINESS
-- 2.LAST MONTH EXPENSE
-----------------------
SELECT *
INTO #DIVISION
FROM (
	SELECT ROW_NO = 1, CELL_ID = @DISTRIBUTION, * FROM TBL_LOOKUP_VALUE WHERE VALUE_ID = 10204			-- ADMINISTRATION
	UNION ALL SELECT ROW = 2, CELL_ID = @PRODUCTION, * FROM TBL_LOOKUP_VALUE WHERE VALUE_ID = 10201	-- PRODUCTION
	UNION ALL SELECT ROW = 3, CELL_ID = @TRANSMISSION, * FROM TBL_LOOKUP_VALUE WHERE VALUE_ID = 10202	-- TRANSMISSION
	UNION ALL SELECT ROW = 4, CELL_ID = @DISTRIBUTION, * FROM TBL_LOOKUP_VALUE WHERE VALUE_ID = 10203	-- DISTRIBUTION
) t

SELECT -- SHEARCH LAST MONTH OF TOTAL STAFF IN YEAR
	POSITION_ID,
	EXPENSE_DATE = MAX(EXPENSE_DATE) 
INTO #LAST_MONTH
FROM TBL_STAFF_EXPENSE se 
INNER JOIN TBL_STAFF_EXPENSE_DETAIL sd ON se.EXPENSE_ID = sd.EXPENSE_ID
WHERE YEAR(se.EXPENSE_DATE) = @YEAR_ID 
GROUP BY POSITION_ID;
-------------
-- END OF TMP
-------------
-- RUN AS 03
------------
INSERT INTO ANR.TBL_RESULT_03
SELECT	
	YEAR_ID = @YEAR_ID,
	v.CELL_ID,
	v.ROW_NO,
	DIVISION_ID = v.VALUE_ID,
	DIVISION_NAME =  v.VALUE_TEXT,
	p.POSITION_NAME,
	TOTAL_STUFF = NULLIF(MAX(CASE WHEN MONTH(se.EXPENSE_DATE) = MONTH (lm.EXPENSE_DATE) THEN sd.TOTAL_STAFF ELSE 0 END),0),
	S6010 = NULLIF(SUM(sd.S6010 * dbo.GET_EXCHANGE_RATE(se.CURRENCY_ID, @CURRENCY_ID, se.EXPENSE_DATE)),0),
	S6020 = NULLIF(SUM(sd.S6020 * dbo.GET_EXCHANGE_RATE(se.CURRENCY_ID, @CURRENCY_ID, se.EXPENSE_DATE)),0),
	S6040 = NULLIF(SUM(sd.S6040 * dbo.GET_EXCHANGE_RATE(se.CURRENCY_ID, @CURRENCY_ID, se.EXPENSE_DATE)),0),
	S6050 = NULLIF(SUM(sd.S6050 * dbo.GET_EXCHANGE_RATE(se.CURRENCY_ID, @CURRENCY_ID, se.EXPENSE_DATE)),0),
	S6060 = NULLIF(SUM(sd.S6060 * dbo.GET_EXCHANGE_RATE(se.CURRENCY_ID, @CURRENCY_ID, se.EXPENSE_DATE)),0),
	S6070 = NULLIF(SUM(sd.S6070 * dbo.GET_EXCHANGE_RATE(se.CURRENCY_ID, @CURRENCY_ID, se.EXPENSE_DATE)),0),
	TOTAL = NULLIF(SUM(ISNULL(sd.S6010 * dbo.GET_EXCHANGE_RATE(se.CURRENCY_ID, @CURRENCY_ID, se.EXPENSE_DATE),0)
				+ ISNULL(sd.S6020 * dbo.GET_EXCHANGE_RATE(se.CURRENCY_ID, @CURRENCY_ID, se.EXPENSE_DATE),0)
				+ ISNULL(sd.S6040 * dbo.GET_EXCHANGE_RATE(se.CURRENCY_ID, @CURRENCY_ID, se.EXPENSE_DATE),0)
				+ ISNULL(sd.S6050 * dbo.GET_EXCHANGE_RATE(se.CURRENCY_ID, @CURRENCY_ID, se.EXPENSE_DATE),0)
				+ ISNULL(sd.S6060 * dbo.GET_EXCHANGE_RATE(se.CURRENCY_ID, @CURRENCY_ID, se.EXPENSE_DATE),0)
				+ ISNULL(sd.S6070 * dbo.GET_EXCHANGE_RATE(se.CURRENCY_ID, @CURRENCY_ID, se.EXPENSE_DATE),0)),0),
	ACTIVE = 1
FROM TBL_STAFF_EXPENSE se 
INNER JOIN TBL_STAFF_EXPENSE_DETAIL sd ON	se.EXPENSE_ID = sd.EXPENSE_ID AND sd.IS_ACTIVE = 1 
											AND YEAR(se.EXPENSE_DATE) = @YEAR_ID
INNER JOIN #LAST_MONTH lm ON lm.POSITION_ID = sd.POSITION_ID
RIGHT JOIN TBL_POSITION p ON sd.POSITION_ID = p.POSITION_ID --NOT IN 1 = COLLECT, 2 = BLOCK, 3 = INVOICE
INNER JOIN #DIVISION v ON p.BUSINESS_DIVISION_ID = v.VALUE_ID 
WHERE p.POSITION_ID > 3 
	AND se.IS_ACTIVE=1 
	AND sd.IS_ACTIVE=1
GROUP BY v.ROW_NO,v.CELL_ID, v.VALUE_ID, v.VALUE_TEXT,p.POSITION_NAME, p.POSITION_ID
ORDER BY v.ROW_NO,v.CELL_ID, v.VALUE_ID DESC, p.POSITION_ID;
-- END OF RUN AS 03
-------------------
-- RUN RESULT CELL AS 10
------------------------
DELETE ANR.TBL_RESULT_CELL WHERE YEAR_ID = @YEAR_ID AND CELL_ID IN (SELECT CELL_ID FROM #DIVISION WHERE CELL_ID != 0)

INSERT INTO ANR.TBL_RESULT_CELL
SELECT 
	YEAR_ID = @YEAR_ID,
	CELL_ID = CELL_ID,
	TEXT_VALUE = CONVERT(NVARCHAR,SUM(TOTAL)),
	NUMERIC_VALUE = ISNULL(SUM(TOTAL),0),
	IS_ACTIVE = 1
FROM ANR.TBL_RESULT_03
WHERE YEAR_ID = @YEAR_ID AND CELL_ID != 0
GROUP BY CELL_ID
----------------------------
-- END RUN RESULT CELL AS 10
----------------------------
-- CLEAR TMP TBALE
DROP TABLE #DIVISION
DROP TABLE #LAST_MONTH

GO
IF OBJECT_ID('ANR.RUN_AS_06')IS NOT NULL
	DROP PROC ANR.RUN_AS_06
GO

CREATE PROC [ANR].[RUN_AS_06]
	@YEAR_ID INT = 2015,
	@CURRENCY_ID INT = 1
AS
DECLARE @PRODUCTION INT, @DISTRIBUTION INT, @TRANSMISSION INT;
SET @PRODUCTION = 10201; SET @DISTRIBUTION = 10203; SET @TRANSMISSION = 10202;
-- CLEAR LAST RESULT 
DELETE ANR.TBL_RESULT_06 WHERE YEAR_ID = @YEAR_ID;
-- ADD TMP
-- 1.DIVISION BUSINESS 
SELECT * 
INTO #DIVISION
FROM(
	SELECT ROW_NO = 1, * FROM TBL_LOOKUP_VALUE WHERE VALUE_ID = @PRODUCTION -- PRODUCTION
	UNION ALL SELECT ROW_NO = 2, * FROM TBL_LOOKUP_VALUE WHERE VALUE_ID = @DISTRIBUTION -- DISTRIBUTION
	UNION ALL SELECT ROW_NO = 3,* FROM TBL_LOOKUP_VALUE WHERE VALUE_ID = @TRANSMISSION -- TRANSMISSION
)t
-- END OF TMP 
-- RUN RESULT AS 06 
INSERT INTO ANR.TBL_RESULT_06
SELECT
	YEAR_ID = @YEAR_ID,
	DIVISION_ID = v.VALUE_ID,
	DIVISION_NAME = v.VALUE_TEXT,
	ROW_NO,
	ACCOUNT_CODE = '7000/7010/7030' ,
	REF_NO =l.LOAN_NO, 
	BANK_NAME,
	LOAN_DATE,
	LOAN_AMOUNT = NULLIF(LOAN_AMOUNT * dbo.GET_EXCHANGE_RATE(CURRENCY_ID,@CURRENCY_ID,LOAN_DATE),0),
	CLOSE_DATE,
	ANNUAL_INTEREST_RATE = NULLIF(ANNUAL_INTEREST_RATE,0),
	BEGINING_BALANCE =	NULLIF(	CASE WHEN YEAR(LOAN_DATE) >= @YEAR_ID THEN 0 
								ELSE (LOAN_AMOUNT - SUM(CASE WHEN YEAR(PAY_DATE) < @YEAR_ID THEN CAPITAL_AMOUNT ELSE 0 END)) * dbo.GET_EXCHANGE_RATE(CURRENCY_ID,@CURRENCY_ID,LOAN_DATE) 
								END,0),
	INYEAR_LOAN =		NULLIF(CASE WHEN YEAR(LOAN_DATE) = @YEAR_ID THEN LOAN_AMOUNT * dbo.GET_EXCHANGE_RATE(CURRENCY_ID,@CURRENCY_ID,LOAN_DATE) ELSE 0 END,0),
	INYEAR_PAYMENT =	NULLIF(SUM(CASE WHEN YEAR(PAY_DATE) = @YEAR_ID THEN PAY_AMOUNT * dbo.GET_EXCHANGE_RATE(CURRENCY_ID,@CURRENCY_ID,LOAN_DATE) ELSE 0 END),0),
	ENDING_BALANCE =	NULLIF((LOAN_AMOUNT - SUM(CAPITAL_AMOUNT)) * dbo.GET_EXCHANGE_RATE(CURRENCY_ID,@CURRENCY_ID,LOAN_DATE),0),
	INYEAR_INTEREST =	NULLIF(SUM(CASE WHEN YEAR(PAY_DATE) = @YEAR_ID THEN INTEREST_AMOUNT * dbo.GET_EXCHANGE_RATE(CURRENCY_ID,@CURRENCY_ID,LOAN_DATE) ELSE 0 END),0),
	IS_ACTIVE = 1 
FROM TBL_LOAN l
LEFT JOIN dbo.TBL_LOAN_PAYMENT lp ON l.LOAN_ID = lp.LOAN_ID AND YEAR(LOAN_DATE) <= @YEAR_ID AND YEAR(PAY_DATE) <= @YEAR_ID AND l.IS_ACTIVE = 1 AND lp.IS_ACTIVE = 1
RIGHT JOIN #DIVISION v ON v.VALUE_ID = l.BUSINESS_DIVISION_ID  AND l.IS_ACTIVE=1
GROUP BY 
	l.LOAN_ID,
	LOAN_NO,
	BANK_NAME,
	ANNUAL_INTEREST_RATE,
	CURRENCY_ID,
	CLOSE_DATE,
	LOAN_DATE,
	VALUE_ID,
	VALUE_TEXT,
	LOAN_AMOUNT,
	ROW_NO 
ORDER BY ROW_NO
-- END OF RUN RESULT AS 06 
--RUN CELL AS 09 
DECLARE @AS6_0001 INT, @AS6_0002 INT, @AS6_0003 INT, @AS6_0004 INT;
SET @AS6_0001 = 13;
SET @AS6_0002 = 14;
SET @AS6_0003 = 15;
SET @AS6_0004 = 16;

DELETE ANR.TBL_RESULT_CELL WHERE YEAR_ID = @YEAR_ID AND CELL_ID IN(@AS6_0001,@AS6_0002,@AS6_0003,@AS6_0004);

INSERT INTO ANR.TBL_RESULT_CELL
SELECT YEAR_ID = @YEAR_ID,
	CELL_ID = @AS6_0001,
	TEXT_VALUE = CONVERT(NVARCHAR(MAX),''),
	NUMERIC_VALUE = 0,
	IS_ACTIVE = 1
UNION ALL SELECT
	YEAR_ID = @YEAR_ID,
	CELL_ID = @AS6_0002,
	TEXT_VALUE = CONVERT(NVARCHAR(MAX),SUM(CASE WHEN LOAN_AMOUNT IS NULL THEN BEGINING_BALANCE ELSE LOAN_AMOUNT END)),
	NUMERIC_VALUE = ISNULL(SUM(CASE WHEN LOAN_AMOUNT IS NULL THEN BEGINING_BALANCE ELSE LOAN_AMOUNT END),0),
	IS_ACTIVE = 1
FROM ANR.TBL_RESULT_06 WHERE YEAR_ID = @YEAR_ID AND IS_ACTIVE = 1 AND DIVISION_ID = @PRODUCTION
UNION ALL SELECT
	YEAR_ID = @YEAR_ID,
	CELL_ID = @AS6_0003,
	TEXT_VALUE = CONVERT(NVARCHAR(MAX),SUM(CASE WHEN LOAN_AMOUNT IS NULL THEN BEGINING_BALANCE ELSE LOAN_AMOUNT END)),
	NUMERIC_VALUE = ISNULL(SUM(CASE WHEN LOAN_AMOUNT IS NULL THEN BEGINING_BALANCE ELSE LOAN_AMOUNT END),0),
	IS_ACTIVE = 1
FROM ANR.TBL_RESULT_06 WHERE YEAR_ID = @YEAR_ID AND IS_ACTIVE = 1 AND DIVISION_ID = @TRANSMISSION
UNION ALL SELECT
	YEAR_ID = @YEAR_ID,
	CELL_ID = @AS6_0004,
	TEXT_VALUE = CONVERT(NVARCHAR(MAX),SUM(CASE WHEN LOAN_AMOUNT IS NULL THEN BEGINING_BALANCE ELSE LOAN_AMOUNT END)),
	NUMERIC_VALUE = ISNULL(SUM(CASE WHEN LOAN_AMOUNT IS NULL THEN BEGINING_BALANCE ELSE LOAN_AMOUNT END),0),
	IS_ACTIVE = 1
FROM ANR.TBL_RESULT_06 WHERE YEAR_ID = @YEAR_ID AND IS_ACTIVE = 1 AND DIVISION_ID = @DISTRIBUTION
-- END RUN RESULT CELL AS 09 
-- RUN RESULT CELL AS 10 
DECLARE @CELL_ID_PRODUCTION INT, @CELL_ID_TRANSMISSION INT, @CELL_ID_DISTRIBUTION INT;
SET @CELL_ID_PRODUCTION = 17; SET @CELL_ID_TRANSMISSION = 32; SET @CELL_ID_DISTRIBUTION = 33;

DELETE ANR.TBL_RESULT_CELL WHERE YEAR_ID = @YEAR_ID AND CELL_ID IN(@CELL_ID_PRODUCTION,@CELL_ID_TRANSMISSION,@CELL_ID_DISTRIBUTION);

SELECT 
	PRODUCTION = SUM(CASE WHEN DIVISION_ID = @PRODUCTION THEN INYEAR_INTEREST ELSE 0 END),
	TRANSMISSION = SUM(CASE WHEN DIVISION_ID = @TRANSMISSION THEN INYEAR_INTEREST ELSE 0 END),
	DISTRIBUTION = SUM(CASE WHEN DIVISION_ID = @DISTRIBUTION THEN INYEAR_INTEREST ELSE 0 END)
INTO #RESULT_CELL
FROM ANR.TBL_RESULT_06 WHERE YEAR_ID = @YEAR_ID AND IS_ACTIVE = 1

INSERT INTO ANR.TBL_RESULT_CELL
SELECT 
	YEAR_ID = @YEAR_ID,
	CELL_ID = @CELL_ID_PRODUCTION,
	TEXT_VALUE = CONVERT(NVARCHAR(MAX),PRODUCTION),
	NUMERIC_VALUE = PRODUCTION,
	IS_ACTIVE = 1 
FROM #RESULT_CELL
UNION ALL SELECT 
	YEAR_ID = @YEAR_ID,
	CELL_ID = @CELL_ID_TRANSMISSION,
	TEXT_VALUE = CONVERT(NVARCHAR,TRANSMISSION),
	NUMERIC_VALUE = TRANSMISSION,
	IS_ACTIVE = 1 
FROM #RESULT_CELL
UNION ALL SELECT 
	YEAR_ID = @YEAR_ID,
	CELL_ID = @CELL_ID_DISTRIBUTION,
	TEXT_VALUE = CONVERT(NVARCHAR,DISTRIBUTION),
	NUMERIC_VALUE = DISTRIBUTION,
	IS_ACTIVE = 1 
FROM #RESULT_CELL
-- END OF RUN RESULT CELL AS 10 
-- CLEAR TMP
DROP TABLE #DIVISION
DROP TABLE #RESULT_CELL
"; 
        }

    }
}
