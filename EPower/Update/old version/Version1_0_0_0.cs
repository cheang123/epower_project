﻿using System.Transactions;
using SoftTech;

namespace EPower.Update
{
    class Version1_0_0_0
    {
        public void Update()
        {
            using (TransactionScope tran = new TransactionScope())
            {
                // Add utility value to mark version. 
                // first version of epower release is "1.0.0.0"
                DBDataContext.Db.TBL_UTILITies.InsertOnSubmit(new TBL_UTILITY()
                {
                    UTILITY_ID = (int)Utility.VERSION,
                    UTILITY_NAME = "VERSION",
                    DESCRIPTION = "EPower client version.",
                    UTILITY_VALUE = "1.0.0.0"
                });
                DBDataContext.Db.SubmitChanges(); 
 
                tran.Complete();
            }
        }
    }
}
