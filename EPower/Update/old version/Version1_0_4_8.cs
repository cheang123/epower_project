﻿namespace EPower.Update
{
    /// <summary>
    /// Pricing can use up to 4 digit, in TLKP_CURRENCY use only 2 digit after decimal even if you want to use 4 digit price.
    /// </summary>
    class Version1_0_4_8 : Version
    {
        protected override string GetBatchCommand()
        {
            return @"";
        }
    }
}
