﻿namespace EPower.Update
{
    /// <summary>
    /// Version Version1_0_3_5
    /// add ENABLE_BANK_DEPOSIT utility
    /// add ENABLE_STOCK_MANGEMENT utility
    /// </summary>
    class Version1_0_3_5 : Version
    { 
        protected override string GetBatchCommand()
        {
            return @"
 IF NOT EXISTS(SELECT * FROM TBL_UTILITY WHERE UTILITY_ID=34)
    INSERT INTO TBL_UTILITY(UTILITY_ID,UTILITY_NAME, DESCRIPTION, UTILITY_VALUE)
    SELECT 34, N'ENABLE_BANK_DEPOSIT', N'Enable Bank Deposit', '0';
GO
IF NOT EXISTS(SELECT * FROM TBL_UTILITY WHERE UTILITY_ID=35)
    INSERT INTO TBL_UTILITY(UTILITY_ID,UTILITY_NAME, DESCRIPTION, UTILITY_VALUE)
    SELECT 35, N'ENABLE_STOCK_MANGEMENT', N'Enable Stock Management', '0';
GO
IF NOT EXISTS(SELECT * FROM TBL_UTILITY WHERE UTILITY_ID=36)
    INSERT INTO TBL_UTILITY(UTILITY_ID,UTILITY_NAME, DESCRIPTION, UTILITY_VALUE)
    SELECT 36, N'ENABLE_INCOME_EXPENSE', N'Enable Income Expense', '0';
";

        }
    }
}
