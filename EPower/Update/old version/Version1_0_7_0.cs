﻿namespace EPower.Update
{
    /// <summary> 
    /// Fix Readonly file
    /// </summary>
    class Version1_0_7_0 : Version
    {
        /// <summary>
        /// Fix Backup file from GF1100 before & after send file and receive file.
        /// </summary>
        /// <returns></returns>
        protected override string GetBatchCommand()
        {
            return @""; 
        }
    }
}
