﻿namespace EPower.Update
{
    class Version1_2_8_1 : Version
    {
        /*
         
         @Kheang kimkhorn 2021-02-05
         1. Downgrade .Net to 4.5.2
        */
        protected override string GetBatchCommand()
        {
            return @"
GO

IF OBJECT_ID('RUN_REF_03B_2021') IS NOT NULL
	DROP PROC RUN_REF_03B_2021
GO

CREATE PROC dbo.RUN_REF_03B_2021
    @DATA_MONTH DATETIME = '2021-01-01',
    @BILLING_CYCLE_ID INT = 0,
    @AREA_ID INT = 0
AS
-- CLEAR DATA
DELETE FROM TBL_REF_03B
WHERE DATA_MONTH = @DATA_MONTH;

DECLARE @TYPE_SALE_CUSTOMER_USAGE_0_10 NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_USAGE_11_50 NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_USAGE_51_200 NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_SCHOOL NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_AGRI NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_SPECIAL NVARCHAR(200),
        @LICENSE_TYPE_ID INT,
        @BASED_TARIFF DECIMAL(18, 5),
        @SUBSIDY_TARIFF DECIMAL(18, 5),
        @TYPE_CUSTOMER_NORMAL NVARCHAR(200),
        @TYPE_CUSTOMER_FAVOR NVARCHAR(200),
        @TYPE_CUSTOMER_MV NVARCHAR(200),
        @TYPE_CUSTOMER_LV_LICENSE NVARCHAR(200),
        @TYPE_CUSTOMER_LV_CUSTOMER_BUY NVARCHAR(200),
        @TYPE_CUSTOMER_LV_GENERAL NVARCHAR(200);
SET @TYPE_CUSTOMER_NORMAL = N'  1' + SPACE(4) + N'អតិថិជនលំនៅដ្ឋាន';
SET @TYPE_CUSTOMER_FAVOR = N'  2' + SPACE(4) + N'អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស';
SET @TYPE_SALE_CUSTOMER_USAGE_0_10 = N'1.1' + N' លំនៅដ្ឋានប្រើ មិនលើស  10 kWh/ខែ';
SET @TYPE_SALE_CUSTOMER_USAGE_11_50 = N'1.2' + N' លំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ';
SET @TYPE_SALE_CUSTOMER_USAGE_51_200 = N'1.3' + N' លំនៅដ្ឋានប្រើចាប់ពី 51 ដល់ 200 kWh/ខែ';
SET @TYPE_SALE_CUSTOMER_SCHOOL = N'2.1' + N' សាលារៀន, មន្ទីរពេទ្យ, និងមណ្ឌលសុខភាព នៅតំបន់ជនបទ';
SET @TYPE_SALE_CUSTOMER_AGRI = N'2.2' + N' អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម';
SET @TYPE_CUSTOMER_MV = N'  2.2.1' + N'  អ្នកប្រើប្រាស់ MV';
SET @TYPE_CUSTOMER_LV_LICENSE = N'  2.2.2' + N'  អ្នកប្រើប្រាស់ LV (ត្រង់ស្វូរអ្នកលក់)';
SET @TYPE_CUSTOMER_LV_CUSTOMER_BUY = N'  2.2.3' + N'  អ្នកប្រើប្រាស់ LV (ត្រង់ស្វូរអ្នកទិញ)';
SET @TYPE_CUSTOMER_LV_GENERAL = N'  2.2.4' + N'  អ្នកប្រើប្រាស់ LV (ភ្ជាប់ពីខ្សែសាធារណៈ)';

SELECT @LICENSE_TYPE_ID = LICENSE_TYPE_ID,
       @SUBSIDY_TARIFF = SUBSIDY_TARIFF,
       @BASED_TARIFF = BASED_TARIFF
FROM dbo.TBL_TARIFF
WHERE DATA_MONTH = @DATA_MONTH;

IF (@LICENSE_TYPE_ID <> 2)
BEGIN
    INSERT INTO TBL_REF_03B
    (
        DATA_MONTH,
        ROW_NO,
        TYPE_OF_SALE_ID,
        TYPE_OF_SALE_NAME,
        TOTAL_CUSTOMER,
        TOTAL_POWER_SOLD,
        PRICE,
        SUBSIDY_RATE,
        SUBSIDY_AMOUNT,
        CURRENCY_ID,
        CURRENCY_NAME,
        CURRENCY_SIGN,
        IS_ACTIVE,
        TYPE_ID,
        TYPE_NAME
    )
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1.1',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_0_10,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_NORMAL
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE <= 10
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1.2',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_11_50,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_NORMAL
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND (i.TOTAL_USAGE
          BETWEEN 11 AND 50
              )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1.3',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_51_200,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_NORMAL
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND (i.TOTAL_USAGE
          BETWEEN 51 AND 200
              )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.1',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_SCHOOL,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_AGRI,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.1',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(0.1370 * 4120, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(ROUND(0.1370 * 4120, 0), 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.2',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_LICENSE,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(0.15048 * 4120, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(ROUND(0.15048 * 4120, 0), 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.3',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_CUSTOMER_BUY,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(0.14248 * 4120, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(ROUND(0.14248 * 4120, 0), 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.4',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_GENERAL,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(730, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(730, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN

    --WHEN BASE TARFF < SUBSIDY TARIFF
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1.1',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_0_10,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.BASED_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.BASED_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_NORMAL
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE <= 10
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1.2',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_11_50,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.BASED_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.BASED_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_NORMAL
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND (i.TOTAL_USAGE
          BETWEEN 11 AND 50
              )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1.3',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_51_200,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.BASED_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.BASED_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_NORMAL
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND (i.TOTAL_USAGE
          BETWEEN 51 AND 200
              )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.1',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_SCHOOL,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.BASED_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.BASED_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_AGRI,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.BASED_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.BASED_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND (i.TOTAL_USAGE
          BETWEEN 51 AND 200
              )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.1',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(0.1370 * 4120, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(ROUND(0.1370 * 4120, 0), 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.2',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_LICENSE,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(0.15048 * 4120, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(ROUND(0.15048 * 4120, 0), 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.3',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_CUSTOMER_BUY,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(0.14248 * 4120, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(ROUND(0.14248 * 4120, 0), 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.4',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_GENERAL,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(730, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(730, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN;

    -- BLANK RECORD
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '1.1'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'1.1',
               TYPE_OF_SALE_ID = 1,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_0_10,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 1,
               TYPE_NAME = @TYPE_CUSTOMER_NORMAL;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '1.2'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'1.2',
               TYPE_OF_SALE_ID = 1,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_11_50,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 1,
               TYPE_NAME = @TYPE_CUSTOMER_NORMAL;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '1.3'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'1.3',
               TYPE_OF_SALE_ID = 1,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_51_200,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 1,
               TYPE_NAME = @TYPE_CUSTOMER_NORMAL;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '2.1'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'2.1',
               TYPE_OF_SALE_ID = 2,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_SCHOOL,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 2,
               TYPE_NAME = @TYPE_CUSTOMER_FAVOR;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '2.2'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'2.2',
               TYPE_OF_SALE_ID = 2,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_AGRI,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 2,
               TYPE_NAME = @TYPE_CUSTOMER_FAVOR;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '2.2.1'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'2.2.1',
               TYPE_OF_SALE_ID = 2,
               TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_MV,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 2,
               TYPE_NAME = @TYPE_CUSTOMER_FAVOR;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '2.2.2'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'2.2.2',
               TYPE_OF_SALE_ID = 2,
               TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_LICENSE,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 2,
               TYPE_NAME = @TYPE_CUSTOMER_FAVOR;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '2.2.3'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'2.2.3',
               TYPE_OF_SALE_ID = 2,
               TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_CUSTOMER_BUY,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 2,
               TYPE_NAME = @TYPE_CUSTOMER_FAVOR;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '2.2.4'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'2.2.4',
               TYPE_OF_SALE_ID = 2,
               TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_GENERAL,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 2,
               TYPE_NAME = @TYPE_CUSTOMER_FAVOR;
END;
-- END RUN_REF_03B_2021
GO

UPDATE i
SET i.TOTAL_AMOUNT = x.TOTAL_AMOUNT, i.SETTLE_AMOUNT = x.SETTLE_AMOUNT
FROM dbo.TBL_INVOICE i
INNER JOIN(
SELECT id.INVOICE_ID, TOTAL_AMOUNT = SUM(AMOUNT), SETTLE_AMOUNT = dbo.ROUND_BY_CURRENCY(SUM(id.AMOUNT),i.CURRENCY_ID)
FROM dbo.TBL_INVOICE_DETAIL id
INNER JOIN dbo.TBL_INVOICE i ON i.INVOICE_ID = id.INVOICE_ID
WHERE i.INVOICE_DATE >= '2021-02-05' AND i.IS_SERVICE_BILL = 1
GROUP BY id.INVOICE_ID, i.CURRENCY_ID) x ON x.INVOICE_ID = i.INVOICE_ID
GO
            ";
        }
    }
}
