﻿namespace EPower.Update
{
    /// <summary>
    /// Prak Serey
    ///     . Public Prepaid.
    /// </summary>
    /// <returns></returns>

    class Version1_1_3_5 : Version
    {
        protected override string GetBatchCommand()
        {
            return @" 
IF OBJECT_ID('TBL_PPR_CONFIG_AREA_METER') IS NULL
CREATE TABLE TBL_PPR_CONFIG_AREA_METER
(
	PPR_AREA_ID INT PRIMARY KEY,
	AREA_CODE INT NOT NULL, -- HID
	CT INT NOT NULL,
	IS_LIMITE INT NOT NULL, -- 0 Unlimite, 1 Limite
	LIMITE_KWH DECIMAL(18,4) NOT NULL, -- NNNNN.NNNN
	AUTO_TIMEOUT INT NOT NULL, -- 0 OFF, 5, 10, 15, 20, 25 minutes
	USE_TIME INT NOT NULL,
	SYSTEM_CODE INT NOT NULL, -- HID
	ZONE_CODE INT NOT NULL, -- HID
	PROCESSOR_ID NVARCHAR(100) NOT NULL -- HID
);
GO

IF OBJECT_ID('TBL_PPR_METER') IS NULL
CREATE TABLE TBL_PPR_METER
(
	PPR_METER_ID INT PRIMARY KEY IDENTITY,
	METER_ID INT NOT NULL,
	BOX_ID INT NOT NULL,
	PHASE_ID INT NOT NULL,
	ACTIVATE_DATE DATETIME NOT NULL,
	CREATE_ON DATETIME NOT NULL,
	CREATE_BY NVARCHAR(100) NOT NULL,
	IS_ACTIVE BIT NOT NULL
);
GO

IF OBJECT_ID('TBL_PPR_TARIFF') IS NULL
CREATE TABLE TBL_PPR_TARIFF
(
	PPR_TARIFF_ID INT PRIMARY KEY IDENTITY,
	TARIFF_NAME NVARCHAR(100) NOT NULL,
	CURRENCY_ID INT NOT NULL,
	CREATE_ON DATETIME NOT NULL,
	CREATE_BY NVARCHAR(100) NOT NULL,
	IS_ACTIVE BIT NOT NULL,
);
GO

IF OBJECT_ID('TBL_PPR_TARIFF_DETAIL') IS NULL
CREATE TABLE TBL_PPR_TARIFF_DETAIL
(
	PPR_TARIFF_DETAIL_ID INT PRIMARY KEY IDENTITY,
	PPR_TARIFF_ID INT NOT NULL,
	TARIFF_START_TIME DATETIME NOT NULL,
	TARIFF_END_TIME DATETIME NOT NULL,
	PRICE DECIMAL(18,4) NOT NULL,
	TARIFF_TYPE INT NOT NULL
);
GO

IF OBJECT_ID('TBL_PPR_CUSTOMER') IS NULL
CREATE TABLE TBL_PPR_CUSTOMER
(
	PPR_CUSTOMER_ID INT PRIMARY KEY IDENTITY,
	CUSTOMER_ID INT NOT NULL,
	PPR_TARIFF_ID INT NOT NULL,
	CREATE_BY NVARCHAR(100) NOT NULL,
	CREATE_ON DATETIME NOT NULL,
	IS_ACTIVE BIT NOT NULL
);
GO

IF OBJECT_ID('TBL_PPR_CUSTOMER_CARD') IS NULL
CREATE TABLE TBL_PPR_CUSTOMER_CARD
(
	PPR_CARD_ID INT PRIMARY KEY IDENTITY,
	PPR_CUSTOMER_ID INT NOT NULL,
	CARD_CODE INT NOT NULL,
	CARD_FLAG INT NOT NULL,
	CARD_TYPE INT NOT NULL,
	TARIFF_CARD_TYPE INT NOT NULL,
	NOTE NVARCHAR(100) NOT NULL,
	CREATE_BY NVARCHAR(100) NOT NULL,
	CREATE_ON DATETIME NOT NULL,
	IS_ACTIVE BIT NOT NULL
);
GO

IF OBJECT_ID('TBL_PPR_CUSTOMER_BUY') IS NULL
CREATE TABLE TBL_PPR_CUSTOMER_BUY
(	
	PPR_BUY_ID INT PRIMARY KEY IDENTITY,
	PPR_CARD_ID INT NOT NULL,
	CUSTOMER_ID INT NOT NULL,
	BUY_NO NVARCHAR(100) NOT NULL,
	BUY_DATE DATETIME NOT NULL,
	BUY_COUNT INT NOT NULL,
	CARD_COUNT INT NOT NULL,
	INFORMATION_FLAG INT NOT NULL,
	PLUS INT NOT NULL,
	OVERDRAFT_FLAG INT NOT NULL,
	PRICE DECIMAL(18,4) NOT NULL,
	BUY_AMOUNT DECIMAL(18,4) NOT NULL,
	REMAIN_AMOUNT DECIMAL(18,4) NOT NULL,
	TOTAL_AMOUNT DECIMAL(18,4) NOT NULL,
	DISCOUNT_AMOUNT DECIMAL(18,4) NOT NULL,
	USER_CASH_DRAWER_ID INT NOT NULL,
	CURRENCY_ID INT NOT NULL,
	IS_ACTIVE BIT NOT NULL
);
GO
GO
--REPORT_PPR_RECEIPT
IF OBJECT_ID('REPORT_PPR_RECEIPT')IS NOT NULL
	DROP PROC REPORT_PPR_RECEIPT
GO
CREATE PROC REPORT_PPR_RECEIPT
	@PPR_BUY_ID INT  = 1
AS
SELECT	pc.CUSTOMER_ID,
		c.CUSTOMER_CODE,
		FULL_NAME = c.LAST_NAME_KH+' '+ c.FIRST_NAME_KH, 
	 	PHONE =  c.PHONE_1+', '+c.PHONE_2,
		CARD_CODE = RIGHT('000000'+CONVERT(NVARCHAR,cd.CARD_CODE),6),
		cb.BUY_NO,
		cb.BUY_DATE,
		pt.TARIFF_NAME,
		tcu.CURRENCY_SING,
		cb.PRICE,
		cb.BUY_COUNT,
		cb.BUY_AMOUNT,
		cb.DISCOUNT_AMOUNT,
		TOTAL_AMOUNT = cb.BUY_AMOUNT-cb.DISCOUNT_AMOUNT,
		cd.NOTE,
		START_TIME=LEFT(RIGHT(CONVERT(NVARCHAR,td.TARIFF_START_TIME, 120),8),5),
		END_TIME=LEFT(RIGHT(CONVERT(NVARCHAR,td.TARIFF_END_TIME,120),8),5)
		
FROM TBL_PPR_CUSTOMER pc
INNER JOIN TBL_CUSTOMER c ON pc.CUSTOMER_ID=c.CUSTOMER_ID
INNER JOIN TBL_PPR_CUSTOMER_BUY cb ON c.CUSTOMER_ID=cb.CUSTOMER_ID
INNER JOIN TBL_PPR_CUSTOMER_CARD cd ON cd.PPR_CARD_ID=cb.PPR_CARD_ID
INNER JOIN TBL_PPR_TARIFF pt ON pc.PPR_TARIFF_ID=pt.PPR_TARIFF_ID
INNER JOIN TBL_PPR_TARIFF_DETAIL td ON pt.PPR_TARIFF_ID = td.PPR_TARIFF_ID AND cd.TARIFF_CARD_TYPE = TARIFF_TYPE
INNER JOIN TLKP_CURRENCY tcu ON cb.CURRENCY_ID=tcu.CURRENCY_ID

WHERE cb.PPR_BUY_ID=@PPR_BUY_ID

--REPORT_PPR_CUSTOMER_BUY
GO
IF OBJECT_ID('REPORT_PPR_CUSTOMER_BUY')IS NOT NULL
	DROP PROC REPORT_PPR_CUSTOMER_BUY
GO
CREATE PROC REPORT_PPR_CUSTOMER_BUY
	@D1 DATETIME='2015-11-01',
	@D2 DATETIME='2015-12-31',
	@CURRENCY_ID INT=0

AS
SET @D1 = DATEADD(D,0,DATEDIFF(D,0,@D1));
SET @D2 = DATEADD(S,-1,DATEADD(D,1,DATEDIFF(D,0,@D2)));
SELECT	c.CUSTOMER_CODE,
		CUSTOMER_NAME = c.LAST_NAME_KH+' '+c.FIRST_NAME_KH,
		cb.PPR_BUY_ID,
		cb.BUY_NO,
		cb.BUY_DATE,
		cb.BUY_AMOUNT,
		cb.DISCOUNT_AMOUNT,
		TOTAL_AMOUNT = cb.BUY_AMOUNT-cb.DISCOUNT_AMOUNT,
		cb.BUY_COUNT,
		CARD_CODE = RIGHT('000000'+CONVERT(NVARCHAR,cc.CARD_CODE),6),
		NOTE = LEFT(RIGHT(CONVERT(NVARCHAR,td.TARIFF_START_TIME,120),8),5)+' - '+LEFT(RIGHT(CONVERT(NVARCHAR,td.TARIFF_END_TIME,120),8),5),
		tcu.CURRENCY_ID,
		tcu.CURRENCY_SING,
		tcu.CURRENCY_NAME

FROM TBL_PPR_CUSTOMER pc
INNER JOIN TBL_CUSTOMER c ON pc.CUSTOMER_ID=c.CUSTOMER_ID
INNER JOIN TBL_PPR_CUSTOMER_BUY cb ON pc.CUSTOMER_ID=cb.CUSTOMER_ID
INNER JOIN TBL_PPR_CUSTOMER_CARD cc ON cb.PPR_CARD_ID=cc.PPR_CARD_ID
INNER JOIN TBL_PPR_TARIFF_DETAIL td ON TARIFF_TYPE = cc.TARIFF_CARD_TYPE AND td.PPR_TARIFF_ID = pc.PPR_TARIFF_ID
INNER JOIN TLKP_CURRENCY tcu ON cb.CURRENCY_ID=tcu.CURRENCY_ID

WHERE BUY_DATE BETWEEN @D1 AND @D2 
AND (tcu.CURRENCY_ID=@CURRENCY_ID OR @CURRENCY_ID = 0)

GO
--REPORT_PPR_CUSTOMER_BUY_SUMMARY

IF OBJECT_ID('REPORT_PPR_CUSTOMER_BUY_SUMMARY')IS NOT NULL
	DROP PROC REPORT_PPR_CUSTOMER_BUY_SUMMARY
GO
CREATE PROC REPORT_PPR_CUSTOMER_BUY_SUMMARY
	@D1 DATETIME='2015-05-01',
	@D2 DATETIME='2015-12-31',
	@CURRENCY_ID INT=0
AS
SELECT 
	c.CUSTOMER_ID,
	CUSTOMER_CODE,
	FULL_NAME = c.LAST_NAME_KH+' '+	c.FIRST_NAME_KH,
	CARD_CODE,
	BUY_COUNT,
	BUY_AMOUNT,
	DISCOUNT_AMOUNT,
	PAID_AMOUNT,
	CURRENCY_SING,
	CURRENCY_ID,
	CURRENCY_NAME,
	NOTE
FROM TBL_CUSTOMER c
INNER JOIN TBL_PPR_CUSTOMER pc ON c.CUSTOMER_ID = pc.CUSTOMER_ID
LEFT JOIN  
	(SELECT
		cb.CUSTOMER_ID,	 
		CARD_CODE =  RIGHT('000000'+CONVERT(NVARCHAR,cc.CARD_CODE),6),
		BUY_COUNT = COUNT(*),
		BUY_AMOUNT = SUM(BUY_AMOUNT),
		DISCOUNT_AMOUNT = SUM(DISCOUNT_AMOUNT),
 		PAID_AMOUNT = SUM(BUY_AMOUNT-DISCOUNT_AMOUNT),
 		cr.CURRENCY_SING,
		cr.CURRENCY_ID,
		cr.CURRENCY_NAME,
 		NOTE = LEFT(RIGHT(CONVERT(NVARCHAR,td.TARIFF_START_TIME,120),8),5)+' - '+LEFT(RIGHT(CONVERT(NVARCHAR,td.TARIFF_END_TIME,120),8),5)
	FROM TBL_PPR_CUSTOMER_BUY cb
	INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = cb.CUSTOMER_ID 
	INNER JOIN TBL_PPR_CUSTOMER_CARD cc ON cb.PPR_CARD_ID = cc.PPR_CARD_ID
	INNER JOIN TBL_PPR_CUSTOMER pc ON cb.CUSTOMER_ID = pc.CUSTOMER_ID
	INNER JOIN TBL_PPR_TARIFF_DETAIL td ON TARIFF_TYPE = cc.TARIFF_CARD_TYPE AND td.PPR_TARIFF_ID = pc.PPR_TARIFF_ID
	INNER JOIN TLKP_CURRENCY cr ON cb.CURRENCY_ID = cr.CURRENCY_ID
	WHERE cb.IS_ACTIVE = 1
		AND cb.BUY_DATE BETWEEN @D1 AND @D2
		AND(@CURRENCY_ID = 0 OR cb.CURRENCY_ID = @CURRENCY_ID)
	GROUP BY 
		cb.CUSTOMER_ID, 
		cb.PPR_CARD_ID, 
		cr.CURRENCY_ID, 
		cc.CARD_CODE,
		cr.CURRENCY_SING,
		cr.CURRENCY_NAME,
		td.TARIFF_START_TIME,
		td.TARIFF_END_TIME) t ON t.CUSTOMER_ID = c.CUSTOMER_ID
ORDER BY c.CUSTOMER_CODE,CARD_CODE

GO
GO
IF OBJECT_ID('REPORT_PPR_DIALY_CUSTOMER_BUY')IS NOT NULL
	DROP PROC REPORT_PPR_DIALY_CUSTOMER_BUY
GO
CREATE PROC REPORT_PPR_DIALY_CUSTOMER_BUY
	@D1 DATETIME='2015-05-01',
	@D2 DATETIME='2015-12-31',
	@CURRENCY_ID INT=0
AS
SELECT  
	BUY_DATE = CONVERT(NVARCHAR(10),BUY_DATE,105),
	CUS_COUNT = COUNT(DISTINCT cb.CUSTOMER_ID),
	BUY_COUNT = COUNT(cb.PPR_BUY_ID),
	BUY_AMOUNT = SUM(cb.BUY_AMOUNT),
	DISCOUNT_AMOUNT = SUM(cb.DISCOUNT_AMOUNT),
	PAID_AMOUNT = SUM(cb.BUY_AMOUNT - cb.DISCOUNT_AMOUNT),
	cr.CURRENCY_SING,
	CURRENCY_NAME
FROM TBL_PPR_CUSTOMER_BUY cb
INNER JOIN TLKP_CURRENCY cr ON cb.CURRENCY_ID = cr.CURRENCY_ID
WHERE cb.IS_ACTIVE = 1
	AND cb.BUY_DATE BETWEEN @D1 AND @D2
	AND (@CURRENCY_ID = 0 OR cr.CURRENCY_ID = @CURRENCY_ID)
GROUP BY CONVERT(NVARCHAR(10),BUY_DATE,105),CURRENCY_SING,CURRENCY_NAME
ORDER BY BUY_DATE
GO

IF OBJECT_ID('REPORT_PPR_MONTHLY_CUSTOMER_BUY')IS NOT NULL
	DROP PROC REPORT_PPR_MONTHLY_CUSTOMER_BUY
GO
CREATE PROC REPORT_PPR_MONTHLY_CUSTOMER_BUY
	@YEAR INT = 2015,
	@CURRENCY_ID INT=0
AS
SELECT  
	BUY_DATE = MONTH(BUY_DATE),
	CUS_COUNT = COUNT(DISTINCT cb.CUSTOMER_ID),
	BUY_COUNT = COUNT(cb.PPR_BUY_ID),
	BUY_AMOUNT = SUM(cb.BUY_AMOUNT),
	DISCOUNT_AMOUNT = SUM(cb.DISCOUNT_AMOUNT),
	PAID_AMOUNT = SUM(cb.BUY_AMOUNT - cb.DISCOUNT_AMOUNT),
	cr.CURRENCY_SING,
	cr.CURRENCY_NAME
FROM TBL_PPR_CUSTOMER_BUY cb
INNER JOIN TLKP_CURRENCY cr ON cb.CURRENCY_ID = cr.CURRENCY_ID
WHERE cb.IS_ACTIVE = 1
	AND YEAR(cb.BUY_DATE) = @YEAR
	AND (@CURRENCY_ID = 0 OR cr.CURRENCY_ID = @CURRENCY_ID)
GROUP BY MONTH(BUY_DATE),CURRENCY_SING,CURRENCY_NAME
ORDER BY BUY_DATE
GO

IF OBJECT_ID('REPORT_PPR_YEARLY_CUSTOMER_BUY')IS NOT NULL
	DROP PROC REPORT_PPR_YEARLY_CUSTOMER_BUY
GO
CREATE PROC REPORT_PPR_YEARLY_CUSTOMER_BUY
	@Y1 INT = 2014,
	@Y2 INT = 2015,
	@CURRENCY_ID INT=0
AS
SELECT  
	BUY_DATE = YEAR(BUY_DATE),
	CUS_COUNT = COUNT(DISTINCT cb.CUSTOMER_ID),
	BUY_COUNT = COUNT(cb.PPR_BUY_ID),
	BUY_AMOUNT = SUM(cb.BUY_AMOUNT),
	DISCOUNT_AMOUNT = SUM(cb.DISCOUNT_AMOUNT),
	PAID_AMOUNT = SUM(cb.BUY_AMOUNT - cb.DISCOUNT_AMOUNT),
	cr.CURRENCY_SING,
	cr.CURRENCY_NAME
FROM TBL_PPR_CUSTOMER_BUY cb
INNER JOIN TLKP_CURRENCY cr ON cb.CURRENCY_ID = cr.CURRENCY_ID
WHERE cb.IS_ACTIVE = 1
	AND YEAR(cb.BUY_DATE) BETWEEN @Y1 AND @Y2
	AND (@CURRENCY_ID = 0 OR cr.CURRENCY_ID = @CURRENCY_ID)
GROUP BY YEAR(BUY_DATE),CURRENCY_SING,CURRENCY_NAME
ORDER BY BUY_DATE
GO

--INSERT INTO TBL_PPR_CONFIG_AREA_METER(PPR_AREA_ID,AREA_CODE,CT,IS_LIMITE,LIMITE_KWH,AUTO_TIMEOUT,USE_TIME,SYSTEM_CODE,ZONE_CODE,PROCESSOR_ID) VALUES(1,1,1,0,0,0,10,790938,3666,N'')
IF NOT EXISTS (SELECT AUDIT_GROUP_ID FROM TBL_AUDITTRIAL_GROUP WHERE AUDIT_GROUP_ID = 11)
	INSERT INTO TBL_AUDITTRIAL_GROUP VALUES(11,N'Public Prepaid',N'អគ្គិសនីបង់ប្រាក់មុនសាធារណៈ');

INSERT INTO TLKP_TABLE VALUES(N'TBL_PPR_CONFIG_AREA_METER',N'TBL_PPR_CONFIG_AREA_METER',11,1);
INSERT INTO TLKP_TABLE VALUES(N'TBL_PPR_METER',N'TBL_PPR_METER',11,1);
INSERT INTO TLKP_TABLE VALUES(N'TBL_PPR_TARIFF',N'TBL_PPR_TARIFF',11,1);
INSERT INTO TLKP_TABLE VALUES(N'TBL_PPR_TARIFF_DETAIL',N'TBL_PPR_TARIFF_DETAIL',11,1);
INSERT INTO TLKP_TABLE VALUES(N'TBL_PPR_CUSTOMER',N'TBL_PPR_CUSTOMER',11,1);
INSERT INTO TLKP_TABLE VALUES(N'TBL_PPR_CUSTOMER_CARD',N'TBL_PPR_CUSTOMER_CARD',11,1);
INSERT INTO TLKP_TABLE VALUES(N'TBL_PPR_CUSTOMER_BUY',N'TBL_PPR_CUSTOMER_PURCHEASE',11,1);
";
        }
    }
}
