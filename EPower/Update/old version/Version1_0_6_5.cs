﻿namespace EPower.Update
{
    /// <summary> 
    /// Fix Readonly file
    /// </summary>
    class Version1_0_6_5 : Version
    {
        /// <summary>
        /// Fix Send usage to IR H9300
        /// - filter customer status in (2,3) [Active & Block]
        /// </summary>
        /// <returns></returns>
        protected override string GetBatchCommand()
        {
            return @""; 
        }
    }
}
