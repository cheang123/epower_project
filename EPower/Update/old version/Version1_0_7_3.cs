﻿namespace EPower.Update
{
    /// <summary> 
    /// Fix Readonly file
    /// </summary>
    class Version1_0_7_3 : Version
    {
        /// <summary>
        /// Confirm user to send data to gateway and auto send setup.
        /// Support Tracker Update
        /// - Change the way to show message (remove random; static calculation based on customer and month in TBL_CONFIG)
        /// - Remove support tracker check on login screen
        /// </summary>
        /// <returns></returns>
        protected override string GetBatchCommand()
        {
            return @"
IF NOT EXISTS(SELECT * FROM TBL_UTILITY WHERE UTILITY_ID = 55)
	INSERT INTO TBL_UTILITY VALUES(55,'AUTO_SEND_DATA_TO_BANK','','0');
GO
"; 
        }
    }
}
