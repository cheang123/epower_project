﻿namespace EPower.Update
{
    /// <summary>
    ///  Morm Raksmey
    ///  - Fix Slow REPORT_CASH_RECEIVE_DEPOSIT_CONNECTION_FEE
    /// </summary>
    /// <returns></returns>

    class Version1_1_2_0 : Version
    {
        protected override string GetBatchCommand()
        {
            return @"
IF OBJECT_ID('REPORT_CASH_RECEIVE_DEPOSIT_CONNECTION_FEE')IS NOT NULL
	DROP PROC REPORT_CASH_RECEIVE_DEPOSIT_CONNECTION_FEE
GO

CREATE PROC [dbo].[REPORT_CASH_RECEIVE_DEPOSIT_CONNECTION_FEE]
	@D1 DATETIME='2012-07-01',
	@D2 DATETIME='2015-07-31',
	@ITEM_TYPE_ID INT=0,
	@ITEM_ID INT =0,
	@CURRENCY_ID INT=0,
	@AREA_ID INT=0,
	@BILLING_CYCLE_ID INT=0
AS   
SET @D1 = DATEADD(D,0,DATEDIFF(D,0,@D1));
SET @D2 = DATEADD(S,-1,DATEADD(D,1,DATEDIFF(D,0,@D2)));

SELECT  cd.CUSTOMER_ID,
		cd.CURRENCY_ID,
		DEPOSIT_AMOUNT=SUM(AMOUNT)
INTO #TMP_DEPOSIT
FROM TBL_CUS_DEPOSIT cd
WHERE cd.IS_PAID=1
	  AND (@CURRENCY_ID=0 OR cd.CURRENCY_ID=@CURRENCY_ID)
	  AND (DEPOSIT_DATE BETWEEN @D1 AND @D2)
GROUP BY cd.CUSTOMER_ID,cd.CURRENCY_ID

SELECT p.CUSTOMER_ID,
	   p.CURRENCY_ID,
	   CONNECTION_FEE_AMOUNT=SUM(pd.PAY_AMOUNT)
INTO #TMP_CONNECTION_FEE
FROM TBL_PAYMENT p 
INNER JOIN TBL_PAYMENT_DETAIL pd ON pd.PAYMENT_ID=p.PAYMENT_ID
INNER JOIN TBL_INVOICE i ON i.INVOICE_ID = pd.INVOICE_ID
WHERE	EXISTS(
			SELECT INVOICE_DETAIL_ID 
			FROM TBL_INVOICE_DETAIL
			WHERE INVOICE_ID=i.INVOICE_ID AND INVOICE_ITEM_ID = 3
		) 
		AND (@CURRENCY_ID=0 OR p.CURRENCY_ID=@CURRENCY_ID) 
		AND p.PAY_DATE BETWEEN @D1 AND @D2
GROUP BY p.CUSTOMER_ID,p.CURRENCY_ID

SELECT	cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		c.CUSTOMER_ID,
		CUSTOMER_CODE,
		CUSTOMER_NAME = c.LAST_NAME_KH+' '+ c.FIRST_NAME_KH,
		 AREA_NAME=a.AREA_CODE,
		ct.CUSTOMER_TYPE_NAME,
		b.BOX_CODE,
		METER_CODE =  REPLACE(LTRIM(REPLACE(m.METER_CODE,'0',' ')),' ','0') ,
		METER_NUMBER=1,
		  
		PHONE = c.PHONE_1,
		AMPARE_NAME,

		DEPOSIT=ISNULL(d.DEPOSIT_AMOUNT,0),
		CONNECTION_FEE=ISNULL(f.CONNECTION_FEE_AMOUNT,0)
FROM TBL_CUSTOMER c
INNER JOIN TBL_PRICE px ON px.PRICE_ID=c.PRICE_ID
LEFT JOIN TBL_CUSTOMER_METER cm ON cm.CUSTOMER_ID = c.CUSTOMER_ID AND cm.IS_ACTIVE = 1
LEFT JOIN TBL_AREA a ON a.AREA_ID = c.AREA_ID
LEFT JOIN TBL_BOX b ON b.BOX_ID = cm.BOX_ID
LEFT JOIN TBL_METER m ON m.METER_ID = cm.METER_ID
LEFT JOIN TBL_AMPARE am ON am.AMPARE_ID = c.AMP_ID
LEFT JOIN TBL_CUSTOMER_TYPE ct ON ct.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
CROSS JOIN TLKP_CURRENCY cx 
OUTER APPLY(
	SELECT DEPOSIT_AMOUNT
	FROM #TMP_DEPOSIT
	WHERE CUSTOMER_ID=c.CUSTOMER_ID
		  AND cx.CURRENCY_ID=CURRENCY_ID
)d
OUTER APPLY(
	SELECT CONNECTION_FEE_AMOUNT
	FROM #TMP_CONNECTION_FEE
	WHERE CUSTOMER_ID=c.CUSTOMER_ID
		  AND cx.CURRENCY_ID=CURRENCY_ID 
)f
  
WHERE  (@AREA_ID=0 OR c.AREA_ID=@AREA_ID)
		AND (@BILLING_CYCLE_ID=0 OR c.BILLING_CYCLE_ID=@BILLING_CYCLE_ID)
		AND ISNULL(f.CONNECTION_FEE_AMOUNT,0)+ISNULL(d.DEPOSIT_AMOUNT,0)>0
-- END OF REPORT_CASH_RECEIVE_DEPOSIT_CONNECTION_FEE
";


        }
    }
}
