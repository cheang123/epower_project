﻿namespace EPower.Update
{
    /// <summary>
    /// FIX  
    ///  - Morm Raksmey : 
    ///  1. Update REPORT_CASH_DIALY_DETAIL: Update Store 
    ///  2. Add New Report : REPORT_PAYMENT_TOTAL_DETAIL : Total All Payment Include Invoice, Deposit, Connection Fee, Other Service
    ///  3. Add New Report : REPORT_PAYMENT_TOTAL  : Summary All Payment and Group by Invoice Month
    /// </summary>
    /// <returns></returns>

    class Version1_1_0_8 : Version
    {        
        protected override string GetBatchCommand()
        {
            return @"
IF OBJECT_ID('REPORT_CASH_DAILY_DETAIL') IS NOT NULL
	DROP PROC REPORT_CASH_DAILY_DETAIL
GO

CREATE PROC [dbo].[REPORT_CASH_DAILY_DETAIL]
	@USER_CASH_DRAWER_ID INT=848
AS   
SELECT	p.PAYMENT_ID,
		cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		c.CUSTOMER_ID,
		c.CUSTOMER_CODE,
		CUSTOMER_NAME=c.LAST_NAME_KH+' ' + c.FIRST_NAME_KH,
		t.CUSTOMER_TYPE_NAME,
		i.INVOICE_ID,
		i.INVOICE_NO,
		i.INVOICE_DATE,
		i.INVOICE_TITLE,
		i.SETTLE_AMOUNT,
		d.PAY_AMOUNT,
		p.PAY_DATE,
		TYPE_ID=1,
		TYPE_NAME=N'វិក្កយបត្រអគ្គិសនី'
FROM TBL_PAYMENT p
INNER JOIN TBL_PAYMENT_DETAIL d ON p.PAYMENT_ID=d.PAYMENT_ID
INNER JOIN TBL_INVOICE i ON i.INVOICE_ID=d.INVOICE_ID
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=i.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_TYPE t ON t.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=p.CURRENCY_ID
WHERE p.USER_CASH_DRAWER_ID=@USER_CASH_DRAWER_ID
	AND i.IS_SERVICE_BILL=0
	
UNION ALL 
SELECT  b.CUSTOMER_BUY_ID,
		cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		c.CUSTOMER_ID,
		c.CUSTOMER_CODE,
		CUSTOMER_NAME=c.LAST_NAME_KH+' ' + c.FIRST_NAME_KH,
		t.CUSTOMER_TYPE_NAME,
		b.CUSTOMER_BUY_ID,
		b.BUY_NO,
		b.CREATE_ON,
		N'ទិញ Prepaid',
		b.BUY_AMOUNT,
		b.BUY_AMOUNT,
		b.CREATE_ON,
		TYPE_ID=2,
		TYPE_NAME=N'បង់ប្រាក់មុន'
FROM TBL_CUSTOMER_BUY b
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=b.PREPAID_CUS_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=b.CURRENCY_ID
INNER JOIN TBL_CUSTOMER_TYPE t ON t.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
WHERE b.USER_CASH_DRAWER_ID=@USER_CASH_DRAWER_ID

UNION ALL
SELECT	p.PAYMENT_ID,
		cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		c.CUSTOMER_ID,
		c.CUSTOMER_CODE,
		CUSTOMER_NAME=c.LAST_NAME_KH+' ' + c.FIRST_NAME_KH,
		t.CUSTOMER_TYPE_NAME,
		i.INVOICE_ID,
		i.INVOICE_NO,
		i.INVOICE_DATE,
		i.INVOICE_TITLE,
		i.SETTLE_AMOUNT,
		d.PAY_AMOUNT,
		p.PAY_DATE,
		TYPE_ID=3,
		TYPE_NAME=N'ប្រាក់ភ្ជាប់ចរន្ត'
FROM TBL_PAYMENT p
INNER JOIN TBL_PAYMENT_DETAIL d ON p.PAYMENT_ID=d.PAYMENT_ID
INNER JOIN TBL_INVOICE i ON i.INVOICE_ID=d.INVOICE_ID
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=i.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_TYPE t ON t.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=p.CURRENCY_ID
OUTER APPLY(
	SELECT TOP 1 t.INVOICE_ITEM_ID,t.INVOICE_ITEM_TYPE_ID
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE_ITEM t ON d.INVOICE_ITEM_ID=t.INVOICE_ITEM_ID 
	WHERE d.INVOICE_ID = i.INVOICE_ID
) itm
WHERE p.USER_CASH_DRAWER_ID=@USER_CASH_DRAWER_ID
	AND i.IS_SERVICE_BILL=1
	AND itm.INVOICE_ITEM_ID=3

UNION ALL 
SELECT	CUS_DEPOSIT_ID,
		cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		c.CUSTOMER_ID,
		c.CUSTOMER_CODE,
		CUSTOMER_NAME=c.LAST_NAME_KH+' ' + c.FIRST_NAME_KH,
		t.CUSTOMER_TYPE_NAME,
		d.CUS_DEPOSIT_ID,
		d.DEPOSIT_NO,
		d.DEPOSIT_DATE,
		N'កក់ប្រាក់',
		d.AMOUNT,
		d.AMOUNT,
		d.DEPOSIT_DATE,
		TYPE_ID=4,
		TYPE_NAME=N'ប្រាក់កក់'
FROM TBL_CUS_DEPOSIT d
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=d.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_TYPE t ON t.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=d.CURRENCY_ID
WHERE USER_CASH_DRAWER_ID=@USER_CASH_DRAWER_ID AND d.IS_PAID=1
UNION ALL
SELECT	p.PAYMENT_ID,
		cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		c.CUSTOMER_ID,
		c.CUSTOMER_CODE,
		CUSTOMER_NAME=c.LAST_NAME_KH+' ' + c.FIRST_NAME_KH,
		t.CUSTOMER_TYPE_NAME,
		i.INVOICE_ID,
		i.INVOICE_NO,
		i.INVOICE_DATE,
		i.INVOICE_TITLE,
		i.SETTLE_AMOUNT,
		d.PAY_AMOUNT,
		p.PAY_DATE,
		TYPE_ID=5,
		TYPE_NAME=N'ទូទាត់ថ្លៃសេវាកម្មផ្សេងៗ'
FROM TBL_PAYMENT p
INNER JOIN TBL_PAYMENT_DETAIL d ON p.PAYMENT_ID=d.PAYMENT_ID
INNER JOIN TBL_INVOICE i ON i.INVOICE_ID=d.INVOICE_ID
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=i.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_TYPE t ON t.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=p.CURRENCY_ID
OUTER APPLY(
	SELECT TOP 1 t.INVOICE_ITEM_ID,t.INVOICE_ITEM_TYPE_ID
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE_ITEM t ON d.INVOICE_ITEM_ID=t.INVOICE_ITEM_ID 
	WHERE d.INVOICE_ID = i.INVOICE_ID
) itm
WHERE p.USER_CASH_DRAWER_ID=@USER_CASH_DRAWER_ID
	AND i.IS_SERVICE_BILL=1
	AND itm.INVOICE_ITEM_ID!=3
ORDER BY 1  ASC


GO
IF OBJECT_ID('REPORT_PAYMENT_TOTAL')IS NOT NULL
	DROP PROC REPORT_PAYMENT_TOTAL
GO 
CREATE PROC REPORT_PAYMENT_TOTAL
	@D1 DATETIME='2014-05-01',
	@D2 DATETIME='2019-06-08',
	@ITEM_TYPE_ID INT=0,
	@ITEM_ID INT =0,
	@INCLUDE_DELETE BIT=1,
	@CURRENCY_ID INT=0,
	@AREA_ID INT=0,
	@BILLING_CYCLE_ID INT=0
AS   
SET @D1 = DATEADD(D,0,DATEDIFF(D,0,@D1));
SET @D2 = DATEADD(S,-1,DATEADD(D,1,DATEDIFF(D,0,@D2)));

SELECT	cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING, 
		INVOICE_MONTH=N'សរុបវិក្កយបត្រ '+REPLACE(RIGHT(CONVERT(VARCHAR(11), i.INVOICE_MONTH, 106), 8), ' ', '-'),
		PAY_AMOUNT=SUM(d.PAY_AMOUNT),
		TYPE_ID=1,
		TYPE_NAME=N'វិក្កយបត្រអគ្គិសនី' 
FROM TBL_PAYMENT p
INNER JOIN TBL_PAYMENT_DETAIL d ON p.PAYMENT_ID=d.PAYMENT_ID
INNER JOIN TBL_INVOICE i ON i.INVOICE_ID=d.INVOICE_ID
OUTER APPLY(
	SELECT TOP 1 t.INVOICE_ITEM_ID,t.INVOICE_ITEM_TYPE_ID
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE_ITEM t ON d.INVOICE_ITEM_ID=t.INVOICE_ITEM_ID 
	WHERE d.INVOICE_ID = i.INVOICE_ID
) itm
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=i.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_TYPE t ON t.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=p.CURRENCY_ID
WHERE (p.PAY_DATE BETWEEN @D1 AND @D2) 
		AND (@ITEM_ID = 0 OR INVOICE_ITEM_ID=@ITEM_ID)
		AND (@ITEM_TYPE_ID = 0 OR INVOICE_ITEM_TYPE_ID = @ITEM_TYPE_ID)
		AND d.PAY_AMOUNT>0
        -- filter payment that removed
		AND (@INCLUDE_DELETE=1 OR p.IS_VOID = 0)
		AND (@CURRENCY_ID=0 OR cx.CURRENCY_ID=@CURRENCY_ID)
		AND (@AREA_ID=0 OR c.AREA_ID=@AREA_ID)
		AND (@BILLING_CYCLE_ID=0 OR c.BILLING_CYCLE_ID=@BILLING_CYCLE_ID)
		AND i.IS_SERVICE_BILL=0
GROUP BY cx.CURRENCY_ID,cx.CURRENCY_NAME,cx.CURRENCY_SING,i.INVOICE_MONTH

UNION ALL
SELECT  cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,   
		INVOICE_MONTH=N'សរុបបង់ប្រាក់មុន',
		PAY_AMOUNT=SUM(b.BUY_AMOUNT),
		TYPE_ID=2,
		TYPE_NAME=N'បង់ប្រាក់មុន' 
FROM TBL_CUSTOMER_BUY b
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=b.PREPAID_CUS_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=b.CURRENCY_ID
INNER JOIN TBL_CUSTOMER_TYPE t ON t.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
WHERE (b.CREATE_ON BETWEEN @D1 AND @D2) 
		AND b.BUY_AMOUNT>0
        -- filter payment that removed
		AND (@INCLUDE_DELETE=1 OR b.IS_VOID = 0)
		AND (@CURRENCY_ID=0 OR cx.CURRENCY_ID=@CURRENCY_ID)
		AND (@AREA_ID=0 OR c.AREA_ID=@AREA_ID)
GROUP BY cx.CURRENCY_ID,cx.CURRENCY_NAME,cx.CURRENCY_SING

UNION ALL
SELECT	cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		INVOICE_MONTH=N'សរុបប្រាក់ភ្ជាប់ចរន្ត',
		PAY_AMOUNT=SUM(d.PAY_AMOUNT),
		TYPE_ID=3,
		TYPE_NAME=N'ប្រាក់ភ្ជាប់ចរន្ត'
FROM TBL_PAYMENT p
INNER JOIN TBL_PAYMENT_DETAIL d ON p.PAYMENT_ID=d.PAYMENT_ID
INNER JOIN TBL_INVOICE i ON i.INVOICE_ID=d.INVOICE_ID
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=i.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_TYPE t ON t.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=p.CURRENCY_ID
OUTER APPLY(
	SELECT TOP 1 t.INVOICE_ITEM_ID,t.INVOICE_ITEM_TYPE_ID
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE_ITEM t ON d.INVOICE_ITEM_ID=t.INVOICE_ITEM_ID 
	WHERE d.INVOICE_ID = i.INVOICE_ID
) itm
WHERE  (p.PAY_DATE BETWEEN @D1 AND @D2) 
		AND (@ITEM_ID = 0 OR INVOICE_ITEM_ID=@ITEM_ID)
		AND (@ITEM_TYPE_ID = 0 OR INVOICE_ITEM_TYPE_ID = @ITEM_TYPE_ID)
		AND d.PAY_AMOUNT>0
        -- filter payment that removed
		AND (@INCLUDE_DELETE=1 OR p.IS_VOID = 0)
		AND (@CURRENCY_ID=0 OR cx.CURRENCY_ID=@CURRENCY_ID)
		AND (@AREA_ID=0 OR c.AREA_ID=@AREA_ID)
		AND (@BILLING_CYCLE_ID=0 OR c.BILLING_CYCLE_ID=@BILLING_CYCLE_ID)
		AND i.IS_SERVICE_BILL=1
		AND itm.INVOICE_ITEM_ID=3
GROUP BY cx.CURRENCY_ID,cx.CURRENCY_NAME,cx.CURRENCY_SING

UNION ALL 
SELECT	cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING, 
		INVOICE_MONTH=N'សរុបប្រាក់កក់' , 
		PAY_AMOUNT=SUM(d.AMOUNT), 
		TYPE_ID=4,
		TYPE_NAME=N'ប្រាក់កក់' 
FROM TBL_CUS_DEPOSIT d
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=d.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_TYPE t ON t.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=d.CURRENCY_ID
WHERE (d.DEPOSIT_DATE BETWEEN @D1 AND @D2)
		AND d.IS_PAID=1
		AND (@CURRENCY_ID=0 OR cx.CURRENCY_ID=@CURRENCY_ID)
		AND (@AREA_ID=0 OR c.AREA_ID=@AREA_ID)
		AND (@BILLING_CYCLE_ID=0 OR c.BILLING_CYCLE_ID=@BILLING_CYCLE_ID)
GROUP BY cx.CURRENCY_ID,cx.CURRENCY_NAME,cx.CURRENCY_SING

UNION ALL
SELECT	cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		INVOICE_MONTH=N'សរុបទូទាត់ថ្លៃសេវាកម្មផ្សេងៗ' , 
		PAY_AMOUNT=SUM(d.PAY_AMOUNT),
		TYPE_ID=5,
		TYPE_NAME=N'ទូទាត់ថ្លៃសេវាកម្មផ្សេងៗ'
FROM TBL_PAYMENT p
INNER JOIN TBL_PAYMENT_DETAIL d ON p.PAYMENT_ID=d.PAYMENT_ID
INNER JOIN TBL_INVOICE i ON i.INVOICE_ID=d.INVOICE_ID
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=i.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_TYPE t ON t.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=p.CURRENCY_ID
OUTER APPLY(
	SELECT TOP 1 t.INVOICE_ITEM_ID,t.INVOICE_ITEM_TYPE_ID
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE_ITEM t ON d.INVOICE_ITEM_ID=t.INVOICE_ITEM_ID 
	WHERE d.INVOICE_ID = i.INVOICE_ID
) itm
WHERE (p.PAY_DATE BETWEEN @D1 AND @D2) 
		AND (@ITEM_ID = 0 OR INVOICE_ITEM_ID=@ITEM_ID)
		AND (@ITEM_TYPE_ID = 0 OR INVOICE_ITEM_TYPE_ID = @ITEM_TYPE_ID)
		AND d.PAY_AMOUNT>0
        -- filter payment that removed
		AND (@INCLUDE_DELETE=1 OR p.IS_VOID = 0)
		AND (@CURRENCY_ID=0 OR cx.CURRENCY_ID=@CURRENCY_ID)
		AND (@AREA_ID=0 OR c.AREA_ID=@AREA_ID)
		AND (@BILLING_CYCLE_ID=0 OR c.BILLING_CYCLE_ID=@BILLING_CYCLE_ID)
		AND i.IS_SERVICE_BILL=1
		AND itm.INVOICE_ITEM_ID!=3
GROUP BY cx.CURRENCY_ID,cx.CURRENCY_NAME,cx.CURRENCY_SING

GO
IF OBJECT_ID('REPORT_PAYMENT_TOTAL_DETAIL')IS NOT NULL
	DROP PROC REPORT_PAYMENT_TOTAL_DETAIL
GO

CREATE PROC REPORT_PAYMENT_TOTAL_DETAIL
	@D1 DATETIME='2014-05-01',
	@D2 DATETIME='2019-06-08',
	@ITEM_TYPE_ID INT=0,
	@ITEM_ID INT =0,
	@INCLUDE_DELETE BIT=1,
	@CURRENCY_ID INT=0,
	@AREA_ID INT=0,
	@BILLING_CYCLE_ID INT=0
AS   
SET @D1 = DATEADD(D,0,DATEDIFF(D,0,@D1));
SET @D2 = DATEADD(S,-1,DATEADD(D,1,DATEDIFF(D,0,@D2)));

SELECT	cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		c.CUSTOMER_ID,
		c.CUSTOMER_CODE,
		CUSTOMER_NAME=c.LAST_NAME_KH+' ' + c.FIRST_NAME_KH,
		t.CUSTOMER_TYPE_NAME,
		i.INVOICE_ID,
		i.INVOICE_NO,
		i.INVOICE_DATE,
		i.INVOICE_TITLE,
		i.SETTLE_AMOUNT,
		d.PAY_AMOUNT,
		p.PAY_DATE,
		TYPE_ID=1,
		TYPE_NAME=N'វិក្កយបត្រ'
		,p.CREATE_BY
FROM TBL_PAYMENT p
INNER JOIN TBL_PAYMENT_DETAIL d ON p.PAYMENT_ID=d.PAYMENT_ID
INNER JOIN TBL_INVOICE i ON i.INVOICE_ID=d.INVOICE_ID
OUTER APPLY(
	SELECT TOP 1 t.INVOICE_ITEM_ID,t.INVOICE_ITEM_TYPE_ID
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE_ITEM t ON d.INVOICE_ITEM_ID=t.INVOICE_ITEM_ID 
	WHERE d.INVOICE_ID = i.INVOICE_ID
) itm
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=i.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_TYPE t ON t.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=p.CURRENCY_ID
WHERE (p.PAY_DATE BETWEEN @D1 AND @D2) 
		AND (@ITEM_ID = 0 OR INVOICE_ITEM_ID=@ITEM_ID)
		AND (@ITEM_TYPE_ID = 0 OR INVOICE_ITEM_TYPE_ID = @ITEM_TYPE_ID)
		AND d.PAY_AMOUNT>0
        -- filter payment that removed
		AND (@INCLUDE_DELETE=1 OR p.IS_VOID = 0)
		AND (@CURRENCY_ID=0 OR cx.CURRENCY_ID=@CURRENCY_ID)
		AND (@AREA_ID=0 OR c.AREA_ID=@AREA_ID)
		AND (@BILLING_CYCLE_ID=0 OR c.BILLING_CYCLE_ID=@BILLING_CYCLE_ID)
	AND i.IS_SERVICE_BILL=0
	
UNION ALL 
SELECT  cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		c.CUSTOMER_ID,
		c.CUSTOMER_CODE,
		CUSTOMER_NAME=c.LAST_NAME_KH+' ' + c.FIRST_NAME_KH,
		t.CUSTOMER_TYPE_NAME,
		b.CUSTOMER_BUY_ID,
		b.BUY_NO,
		b.CREATE_ON,
		N'ទិញ Prepaid',
		b.BUY_AMOUNT,
		b.BUY_AMOUNT,
		b.CREATE_ON,
		TYPE_ID=2,
		TYPE_NAME=N'បង់ប្រាក់មុន'
		,b.CREATE_BY
FROM TBL_CUSTOMER_BUY b
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=b.PREPAID_CUS_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=b.CURRENCY_ID
INNER JOIN TBL_CUSTOMER_TYPE t ON t.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
WHERE (b.CREATE_ON BETWEEN @D1 AND @D2) 
		AND b.BUY_AMOUNT>0
        -- filter payment that removed
		AND (@INCLUDE_DELETE=1 OR b.IS_VOID = 0)
		AND (@CURRENCY_ID=0 OR cx.CURRENCY_ID=@CURRENCY_ID)
		AND (@AREA_ID=0 OR c.AREA_ID=@AREA_ID)
		
UNION ALL
SELECT	cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		c.CUSTOMER_ID,
		c.CUSTOMER_CODE,
		CUSTOMER_NAME=c.LAST_NAME_KH+' ' + c.FIRST_NAME_KH,
		t.CUSTOMER_TYPE_NAME,
		i.INVOICE_ID,
		i.INVOICE_NO,
		i.INVOICE_DATE,
		i.INVOICE_TITLE,
		i.SETTLE_AMOUNT,
		d.PAY_AMOUNT,
		p.PAY_DATE,
		TYPE_ID=3,
		TYPE_NAME=N'ប្រាក់ភ្ជាប់ចរន្ត'
		,p.CREATE_BY
FROM TBL_PAYMENT p
INNER JOIN TBL_PAYMENT_DETAIL d ON p.PAYMENT_ID=d.PAYMENT_ID
INNER JOIN TBL_INVOICE i ON i.INVOICE_ID=d.INVOICE_ID
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=i.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_TYPE t ON t.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=p.CURRENCY_ID
OUTER APPLY(
	SELECT TOP 1 t.INVOICE_ITEM_ID,t.INVOICE_ITEM_TYPE_ID
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE_ITEM t ON d.INVOICE_ITEM_ID=t.INVOICE_ITEM_ID 
	WHERE d.INVOICE_ID = i.INVOICE_ID
) itm
WHERE  (p.PAY_DATE BETWEEN @D1 AND @D2) 
		AND (@ITEM_ID = 0 OR INVOICE_ITEM_ID=@ITEM_ID)
		AND (@ITEM_TYPE_ID = 0 OR INVOICE_ITEM_TYPE_ID = @ITEM_TYPE_ID)
		AND d.PAY_AMOUNT>0
        -- filter payment that removed
		AND (@INCLUDE_DELETE=1 OR p.IS_VOID = 0)
		AND (@CURRENCY_ID=0 OR cx.CURRENCY_ID=@CURRENCY_ID)
		AND (@AREA_ID=0 OR c.AREA_ID=@AREA_ID)
		AND (@BILLING_CYCLE_ID=0 OR c.BILLING_CYCLE_ID=@BILLING_CYCLE_ID)
		AND i.IS_SERVICE_BILL=1
		AND itm.INVOICE_ITEM_ID=3

UNION ALL 
SELECT	cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		c.CUSTOMER_ID,
		c.CUSTOMER_CODE,
		CUSTOMER_NAME=c.LAST_NAME_KH+' ' + c.FIRST_NAME_KH,
		t.CUSTOMER_TYPE_NAME,
		d.CUS_DEPOSIT_ID,
		d.DEPOSIT_NO,
		d.DEPOSIT_DATE,
		N'ប្រាក់កក់',
		d.AMOUNT,
		d.AMOUNT,
		d.DEPOSIT_DATE,
		TYPE_ID=4,
		TYPE_NAME=N'ប្រាក់កក់'
		,d.CREATE_BY
FROM TBL_CUS_DEPOSIT d
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=d.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_TYPE t ON t.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=d.CURRENCY_ID
WHERE (d.DEPOSIT_DATE BETWEEN @D1 AND @D2)
		AND d.IS_PAID=1
		AND (@CURRENCY_ID=0 OR cx.CURRENCY_ID=@CURRENCY_ID)
		AND (@AREA_ID=0 OR c.AREA_ID=@AREA_ID)
		AND (@BILLING_CYCLE_ID=0 OR c.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) 

UNION ALL
SELECT	cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		c.CUSTOMER_ID,
		c.CUSTOMER_CODE,
		CUSTOMER_NAME=c.LAST_NAME_KH+' ' + c.FIRST_NAME_KH,
		t.CUSTOMER_TYPE_NAME,
		i.INVOICE_ID,
		i.INVOICE_NO,
		i.INVOICE_DATE,
		i.INVOICE_TITLE,
		i.SETTLE_AMOUNT,
		d.PAY_AMOUNT,
		p.PAY_DATE,
		TYPE_ID=5,
		TYPE_NAME=N'ទូទាត់ថ្លៃសេវាកម្មផ្សេងៗ'
		,p.CREATE_BY
FROM TBL_PAYMENT p
INNER JOIN TBL_PAYMENT_DETAIL d ON p.PAYMENT_ID=d.PAYMENT_ID
INNER JOIN TBL_INVOICE i ON i.INVOICE_ID=d.INVOICE_ID
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=i.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_TYPE t ON t.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=p.CURRENCY_ID
OUTER APPLY(
	SELECT TOP 1 t.INVOICE_ITEM_ID,t.INVOICE_ITEM_TYPE_ID
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE_ITEM t ON d.INVOICE_ITEM_ID=t.INVOICE_ITEM_ID 
	WHERE d.INVOICE_ID = i.INVOICE_ID
) itm
WHERE (p.PAY_DATE BETWEEN @D1 AND @D2) 
		AND (@ITEM_ID = 0 OR INVOICE_ITEM_ID=@ITEM_ID)
		AND (@ITEM_TYPE_ID = 0 OR INVOICE_ITEM_TYPE_ID = @ITEM_TYPE_ID)
		AND d.PAY_AMOUNT>0
        -- filter payment that removed
		AND (@INCLUDE_DELETE=1 OR p.IS_VOID = 0)
		AND (@CURRENCY_ID=0 OR cx.CURRENCY_ID=@CURRENCY_ID)
		AND (@AREA_ID=0 OR c.AREA_ID=@AREA_ID)
		AND (@BILLING_CYCLE_ID=0 OR c.BILLING_CYCLE_ID=@BILLING_CYCLE_ID)
		AND i.IS_SERVICE_BILL=1
		AND itm.INVOICE_ITEM_ID!=3
ORDER BY 11 , 14  ASC  
"; 
        }

    }
}
