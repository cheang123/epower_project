﻿namespace EPower.Update
{
    /// <summary>
    /// FIX PAYMENT : set IS_VOID for payment voided.
    /// UPDATE PrintReciptPayment & CancelPayment : filter by IS_VOID , before filter by PAYMENT_NO
    /// Fix Send duplicate Usage to Ir: TP900,BlueStar,GF1100: filter invoice IS_SERVICE_BILL=FALSE
    /// Fix DELETE BOX
    /// Fix error change HAND Cursor Payment History 
    /// Fix EXCEL Bank Payment Error -- Error: PaymentOperation.UpdateLabel is null
    /// </summary>
    class Version1_0_5_8 : Version
    {
        protected override string GetBatchCommand()
        {
            return @"
UPDATE TBL_PAYMENT
SET IS_VOID=1 
WHERE PAYMENT_ID IN(
	SELECT PARENT_ID 
	FROM TBL_PAYMENT
	WHERE IS_VOID=1
);
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='TBL_BANK_PAYMENT_LOG_CUSTOMER' AND COLUMN_NAME='CURRENCY')  
	ALTER TABLE TBL_BANK_PAYMENT_LOG_CUSTOMER 
	ADD CURRENCY NVARCHAR(50)NULL,
		TOTAL_RECORD INT NULL,
		TOTAL_AMOUNT DECIMAL(18,4);
GO  
UPDATE TBL_BANK_PAYMENT_LOG_CUSTOMER 
SET CURRENCY='',
	TOTAL_RECORD=0,
	TOTAL_AMOUNT=0
WHERE CURRENCY IS NULL;
GO
ALTER TABLE TBL_BANK_PAYMENT_LOG_CUSTOMER ALTER COLUMN CURRENCY NVARCHAR(50) NOT NULL;
ALTER TABLE TBL_BANK_PAYMENT_LOG_CUSTOMER ALTER COLUMN TOTAL_RECORD INT NOT NULL;
ALTER TABLE TBL_BANK_PAYMENT_LOG_CUSTOMER ALTER COLUMN TOTAL_AMOUNT DECIMAL(18,4) NOT NULL;
GO
IF OBJECT_ID('GET_PENDING_UPDATE_CUSTOMER') IS NOT NULL
	DROP PROC GET_PENDING_UPDATE_CUSTOMER;
GO
CREATE PROC GET_PENDING_UPDATE_CUSTOMER
	@LOG_ID UNIQUEIDENTIFIER='8398F5D8-5558-404A-A2F3-4E9637F0B2A0',
	@D1 DATETIME='2014-1-1',
	@D2 DATETIME='2014-05-05'
AS
INSERT INTO  TBL_BANK_PAYMENT_LOG_CUSTOMER
SELECT	LOG_ID=@LOG_ID,
		CUSTOMER_ID = c.CUSTOMER_ID,
		CUSTOMER_CODE = CUSTOMER_CODE,
		CUSTOMER_NAME = LAST_NAME_KH + ' '+FIRST_NAME_KH,
		METER_CODE=ISNULL(METER_CODE,'NA'),
		STATUS_ID = c.STATUS_ID,
		LOG_DATE=NULL,
		IS_COMPLETED=0,
		CURRENCY = ISNULL(ar.CURRENCY_CODE,'KHR'),
		TOTAL_RECORD = ISNULL(ar.TOTAL_RECORD,0),
		TOTAL_AMOUNT = ISNULL(ar.TOTAL_AMOUNT,0.0)
FROM TBL_CUSTOMER c
LEFT JOIN TBL_CUSTOMER_METER cm ON cm.CUSTOMER_ID=c.CUSTOMER_ID AND cm.IS_ACTIVE=1
LEFT JOIN TBL_METER m ON cm.METER_ID=m.METER_ID
OUTER APPLY(
	SELECT  CURRENCY_CODE,
			TOTAL_RECORD = COUNT(*),	
			TOTAL_AMOUNT = SUM(i.SETTLE_AMOUNT-ISNULL(pay.PAID_AMOUNT,0)+ISNULL(adj.ADJUST,0))
	FROM TBL_INVOICE i
	INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=i.CURRENCY_ID
	OUTER APPLY(
		SELECT PAID_AMOUNT = SUM(dd.PAY_AMOUNT) 
		FROM TBL_PAYMENT dp
		INNER JOIN TBL_PAYMENT_DETAIL dd ON dd.PAYMENT_ID = dp.PAYMENT_ID
		WHERE dd.INVOICE_ID=i.INVOICE_ID  AND dp.PAY_DATE<=@D2
	) pay
	OUTER APPLY(
		SELECT  ADJUST=SUM(CASE WHEN CREATE_ON<=@D2 THEN ADJUST_AMOUNT ELSE 0 END ),
				ALL_ADJUST=SUM(ADJUST_AMOUNT)
		FROM TBL_INVOICE_ADJUSTMENT 
		WHERE INVOICE_ID=i.INVOICE_ID
	) adj
	WHERE i.CUSTOMER_ID=c.CUSTOMER_ID 
		AND i.SETTLE_AMOUNT > i.PAID_AMOUNT
	GROUP BY CURRENCY_CODE
) ar

WHERE c.CUSTOMER_ID IN(
	SELECT PRIMARY_KEY_VALUE 
	FROM TBL_CHANGE_LOG l
	WHERE TABLE_ID=7
	AND CHANGE_DATE BETWEEN @D1 AND @D2
	UNION ALL 
	SELECT CUSTOMER_ID 
	FROM TBL_CUSTOMER_METER
	WHERE USED_DATE BETWEEN @D1 AND @D2
	UNION ALL
	SELECT CUSTOMER_ID
	FROM TBL_PAYMENT
	WHERE CREATE_ON BETWEEN @D1 AND @D2
	UNION ALL
	SELECT CUSTOMER_ID
	FROM TBL_INVOICE_ADJUSTMENT a 
	INNER JOIN TBL_INVOICE i ON i.INVOICE_ID=a.INVOICE_ID
	WHERE CREATE_ON BETWEEN @D1 AND @D2
	UNION ALL 
	SELECT CUSTOMER_ID 
	FROM TBL_INVOICE
	WHERE INVOICE_DATE BETWEEN @D1 AND @D2
);

SELECT * FROM TBL_BANK_PAYMENT_LOG_CUSTOMER
WHERE LOG_ID=@LOG_ID;
GO
IF OBJECT_ID('REPORT_INVOICE') IS NOT NULL
	DROP PROC REPORT_INVOICE;
GO
CREATE PROC REPORT_INVOICE
AS 
-- @SINCE : 2012-04-02
-- support both recurring service & merge invoice
-- @SINCE: 2014-05-28
-- support multi-currency and add currency sign.
-- @SINCE : 2014-06-30
-- support bank payment.
DECLARE @PRE NVARCHAR(50);
SELECT @PRE = UTILITY_VALUE 
FROM TBL_UTILITY
WHERE UTILITY_ID=44;
IF LTRIM(RTRIM(@PRE)) NOT IN('','000') SET @PRE = @PRE + '-' 
 
SELECT  ip.PRINT_ORDER,i.INVOICE_ID,
		i.INVOICE_MONTH,i.INVOICE_NO,
		METER_CODE=REPLACE(LTRIM(REPLACE(i.METER_CODE,'0',' ')),' ','0'),
		i.START_DATE,
		i.END_DATE,
		i.INVOICE_DATE,
		i.START_USAGE,i.END_USAGE,
		i.CUSTOMER_ID,i.FORWARD_AMOUNT,
		i.TOTAL_AMOUNT,i.SETTLE_AMOUNT,
		i.PAID_AMOUNT,i.TOTAL_USAGE,
		i.CURRENCY_ID,i.CYCLE_ID,
		i.DUE_DATE,i.INVOICE_STATUS,
		i.IS_SERVICE_BILL,i.PRINT_COUNT,
		i.RUN_ID,i.DISCOUNT_USAGE,
		i.DISCOUNT_USAGE_NAME,
		i.DISCOUNT_AMOUNT,
		i.DISCOUNT_AMOUNT_NAME,
		i.INVOICE_TITLE, 
		c.LAST_NAME,c.FIRST_NAME,
		c.LAST_NAME_KH,c.FIRST_NAME_KH, 
		c.ADDRESS,c.HOUSE_NO,c.STREET_NO,
		c.PHONE_1,c.PHONE_2, 
		c.COMPANY_NAME,CUSTOMER_CODE = @PRE+c.CUSTOMER_CODE, 
		a.AREA_ID,a.AREA_NAME,a.AREA_CODE,
		b.BOX_ID,b.BOX_CODE,
		l.POLE_ID,l.POLE_CODE,
		d.*,
		CURRENT_DUE= ISNULL((SELECT SUM(SETTLE_AMOUNT-PAID_AMOUNT) 
							 FROM TBL_INVOICE t
						     INNER JOIN TBL_CUSTOMER tc ON tc.CUSTOMER_ID = t.CUSTOMER_ID
							 WHERE (t.CUSTOMER_ID=i.CUSTOMER_ID OR tc.INVOICE_CUSTOMER_ID = i.CUSTOMER_ID)
									AND t.RUN_ID <> i.RUN_ID 
									AND t.INVOICE_DATE < i.INVOICE_DATE
									AND t.INVOICE_STATUS!=3
									AND t.CURRENCY_ID=i.CURRENCY_ID)
					         ,0),
	
		LAST_PAY_DATE= p.PAY_DATE,
		LAST_PAY= ISNULL(p.PAY_AMOUNT,0),
		ADJUST_AMOUNT= ISNULL((SELECT SUM(AMOUNT) 
			FROM TBL_INVOICE_DETAIL t
			WHERE t.INVOICE_ID=i.INVOICE_ID 
				  AND t.INVOICE_ITEM_ID=-1),0),
		ADJUST_NOTE = (
			SELECT TOP 1  ADJUST_REASON  
			FROM TBL_INVOICE_ADJUSTMENT
			WHERE INVOICE_ID=i.INVOICE_ID
			ORDER BY ADJUST_INVOICE_ID DESC
		), 
		NO_INVOICE_CUSTOMER = ISNULL(ni.NO_INVOICE_CUSTOMER,0),
		NO_USAGE_CUSTOMER = ISNULL(nu.NO_USAGE_CUSTOMER,0),
		i.START_PAY_DATE,
		cx.CURRENCY_SING
FROM TBL_INVOICE i 
INNER JOIN TBL_INVOICE_TO_PRINT ip ON i.INVOICE_ID=ip.INVOICE_ID
LEFT JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=i.CUSTOMER_ID
LEFT JOIN TBL_CUSTOMER_METER m ON m.CUSTOMER_ID=c.CUSTOMER_ID AND m.IS_ACTIVE=1
LEFT JOIN TBL_BOX b ON m.BOX_ID=b.BOX_ID
LEFT JOIN TBL_POLE l ON l.POLE_ID = b.POLE_ID
LEFT JOIN TBL_AREA a ON a.AREA_ID=c.AREA_ID
LEFT JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=i.CURRENCY_ID

-- GET INVOICE DETAIL AND INVOICE SERVICE.
OUTER APPLY
(
 -- 1 : THIS INVOICE POWER
 SELECT x.INVOICE_DETAIL_ID, 
		INVOICE_ID_DETAIL=x.INVOICE_ID,
        START_USAGE_DETAIL=x.START_USAGE,
		END_USAGE_DETAIL=x.END_USAGE,
        USAGE_DETAIL=x.USAGE,
        PRICE=x.PRICE,
        INVOICE_ITEM_ID=x.INVOICE_ITEM_ID,
        AMOUNT=x.AMOUNT,
        CHARGE_DESCRIPTION=x.CHARGE_DESCRIPTION,
		REPORT_GROUP=CASE WHEN x.INVOICE_ITEM_ID=1 THEN 1 ELSE 2 END,
		IS_DETAIL = 1,
		MULTIPLIER= 1
 FROM TBL_INVOICE_DETAIL x
 WHERE	x.INVOICE_ID=i.INVOICE_ID
		-- ADJUST AMOUNT (-1) ARE NOT DISPLAY AS DETAIL
		AND x.INVOICE_ITEM_ID!=-1 

 UNION ALL
 -- OTHER CUSTOMER'S INVOICE TO MERGE
 SELECT INVOICE_DETAIL_ID=0, 
		INVOICE_ID=i.INVOICE_ID,
        START_USAGE_DETAIL=x.START_USAGE,
		END_USAGE_DETAIL=x.END_USAGE,
        USAGE_DETAIL=x.TOTAL_USAGE,
        PRICE = p.PRICE,
        INVOICE_ITEM_ID = 0,
        x.SETTLE_AMOUNT,
        INVOICE_TITLE= (CASE WHEN IS_SERVICE_BILL=1 THEN INVOICE_TITLE ELSE x.METER_CODE  END),
		REPORT_GROUP = (CASE WHEN IS_SERVICE_BILL=1 THEN 2 ELSE 1 END),
		IS_DETAIL = 0,
		MULTIPLIER = u.MULTIPLIER
 FROM TBL_INVOICE x
 INNER JOIN TBL_CUSTOMER xc ON xc.CUSTOMER_ID = x.CUSTOMER_ID
 OUTER APPLY(SELECT TOP 1 PRICE FROM TBL_INVOICE_DETAIL WHERE INVOICE_ID=x.INVOICE_ID) p 
 OUTER APPLY(SELECT TOP 1 MULTIPLIER FROM TBL_INVOICE_USAGE WHERE INVOICE_ID=x.INVOICE_ID ORDER BY INVOICE_USAGE_ID DESC) u
 WHERE -- only power invoice will display other detail item.
	   i.IS_SERVICE_BILL =0
	   -- include only current billing cycle
	   AND x.RUN_ID = i.RUN_ID
	   -- other invoice of power invoice OR all invoice if cusotmer is customer merge
	   AND (x.INVOICE_ID <> i.INVOICE_ID OR c.INVOICE_CUSTOMER_ID<>0) 
	   AND (x.CURRENCY_ID=i.CURRENCY_ID) 
	   -- all invoice of current customer or customer to be merged to current customer
	   AND (xc.CUSTOMER_ID = i.CUSTOMER_ID OR xc.INVOICE_CUSTOMER_ID = i.CUSTOMER_ID)
	   -- merge invoice
	   AND ( -- service invoice will display
			(x.IS_SERVICE_BILL = 1)
             OR 
			 -- power invoice will diplay only when view in parent customer
			(x.IS_SERVICE_BILL=0 AND c.INVOICE_CUSTOMER_ID=c.CUSTOMER_ID)) 
) d
-- GET LAST PAYMENT.
OUTER APPLY(
	SELECT TOP 1 p.PAY_DATE,p.PAY_AMOUNT
	FROM TBL_PAYMENT p
	WHERE p.CUSTOMER_ID=i.CUSTOMER_ID
		  AND p.PAY_DATE<i.INVOICE_DATE	
		  AND p.CURRENCY_ID=i.CURRENCY_ID
    ORDER BY PAY_DATE DESC
) p  
OUTER APPLY(
	SELECT NO_USAGE_CUSTOMER = COUNT(*)
	FROM TBL_INVOICE_USAGE 
	WHERE INVOICE_ID = i.INVOICE_ID
		AND i.IS_SERVICE_BILL = 0
		-- only in power invoice
) nu
OUTER APPLY(
	SELECT NO_INVOICE_CUSTOMER = COUNT(*) 
	FROM TBL_CUSTOMER 
	WHERE INVOICE_CUSTOMER_ID= c.CUSTOMER_ID
		AND i.IS_SERVICE_BILL = 0
		-- only in power invoice
) ni 
ORDER BY ip.PRINT_ORDER,REPORT_GROUP,d.START_USAGE_DETAIL
-- END OF REPORT_INVOICE ---
GO
IF EXISTS(SELECT CUSTOMER_CODE,COUNT(*) FROM TBL_CUSTOMER GROUP BY CUSTOMER_CODE HAVING COUNT(*)>1)
	RAISERROR('CUSTOMER_CODE_CHECK: មានលេខកូដអតិថិជនស្ទួន សូមទំនាក់ទំនងអ្នកបច្ចេកទេស!', 16, 1)

-------------------------FIX REPORT_BLOCK_CUSTOMER----------------------------
------------------------------------------------------------------------------
GO
IF OBJECT_ID('REPORT_BLOCK_CUSTOMER') IS NOT NULL DROP PROC REPORT_BLOCK_CUSTOMER;

GO
CREATE PROC REPORT_BLOCK_CUSTOMER
	@CUT_AMOUNT DECIMAL=0.0, 
	@CUSTOMER_TYPE_ID INT=0,
	@AREA_ID INT=0,
	@CURRENCY_ID INT=0,
	@CURRENCY_NAME NVARCHAR(200)=''
AS
DECLARE @TODAY DATETIME;
SET @TODAY = GETDATE();

DECLARE @SHOW_ONLY_POWER BIT;
SELECT @SHOW_ONLY_POWER = UTILITY_VALUE 
FROM TBL_UTILITY WHERE UTILITY_ID=39;

SELECT  AREA_NAME,
		METER_CODE = REPLACE(LTRIM(REPLACE( METER_CODE,'0',' ')),' ','0'),
		CUSTOMER_CODE,
		CUSTOMER_NAME = LAST_NAME_KH + ' ' + FIRST_NAME_KH ,
		INV.DUE_DATE,
		BOX_CODE,
		POLE_CODE,
		INV.BALANCE_DUE,
		INV.BALANCE_QTY,
		INV.CURRENCY_ID,
		INV.CURRENCY_SING,
		INV.CURRENCY_NAME,
		OVERT_DUE_DAY = DATEDIFF(D,DUE_DATE,@TODAY),
		INV.DUE_DATE
FROM TBL_CUSTOMER c
LEFT JOIN TBL_CUSTOMER_TYPE ct ON ct.CUSTOMER_TYPE_ID = c.CUSTOMER_TYPE_ID
LEFT JOIN TBL_AREA a ON a.AREA_ID = c.AREA_ID
LEFT JOIN TBL_CUSTOMER_METER cm ON c.CUSTOMER_ID = cm.CUSTOMER_ID AND cm.IS_ACTIVE = 1 
LEFT JOIN TBL_METER m ON m.METER_ID = cm.METER_ID
LEFT JOIN TBL_BOX b ON b.BOX_ID = cm.BOX_ID
LEFT JOIN TBL_POLE p ON p.POLE_ID = b.POLE_ID
INNER JOIN(
	SELECT i.CUSTOMER_ID,
			BALANCE_DUE = SUM(SETTLE_AMOUNT-PAID_AMOUNT),
			BALANCE_QTY = COUNT(*),
			cr.CURRENCY_ID,
			cr.CURRENCY_SING,
			cr.CURRENCY_NAME,
			tmp.DUE_DATE
	FROM TBL_INVOICE i
	INNER JOIN TLKP_CURRENCY cr ON i.CURRENCY_ID = cr.CURRENCY_ID
	CROSS APPLY(
		SELECT TOP 1 INVOICE_ID,DUE_DATE 
		FROM TBL_INVOICE i 
		WHERE INVOICE_STATUS = 1 AND CUSTOMER_ID = i.CUSTOMER_ID
		ORDER BY DUE_DATE ASC
	) tmp 
	WHERE SETTLE_AMOUNT > PAID_AMOUNT
	      AND (@SHOW_ONLY_POWER=0 OR IS_SERVICE_BILL=0)
	      AND (@CURRENCY_ID = 0 OR cr.CURRENCY_ID = @CURRENCY_ID)
	GROUP BY i.CUSTOMER_ID,cr.CURRENCY_ID,cr.CURRENCY_NAME,cr.CURRENCY_SING,tmp.DUE_DATE
) INV ON INV.CUSTOMER_ID = c.CUSTOMER_ID
WHERE c.STATUS_ID = 2
AND INV.BALANCE_DUE >= @CUT_AMOUNT
AND DATEADD(D,c.CUT_OFF_DAY,INV.DUE_DATE) <= @TODAY
AND (@CUSTOMER_TYPE_ID=0 OR c.CUSTOMER_TYPE_ID=@CUSTOMER_TYPE_ID)
AND (@AREA_ID=0 OR c.AREA_ID=@AREA_ID);
GO
-------------------------UPDATE QUARTER REPORT--------------------------------
------------------------------------------------------------------------------
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='TBL_POWER_SOURCE' AND COLUMN_NAME='CURRENCY_ID')
ALTER TABLE TBL_POWER_SOURCE ADD CURRENCY_ID INT NULL
GO
UPDATE TBL_POWER_SOURCE SET CURRENCY_ID =(SELECT TOP 1 CURRENCY_ID FROM TLKP_CURRENCY WHERE IS_DEFAULT_CURRENCY=1) WHERE CURRENCY_ID IS NULL
GO
ALTER TABLE TBL_POWER_SOURCE ALTER COLUMN CURRENCY_ID INT NOT NULL
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='TBL_POWER_PURCHASE' AND COLUMN_NAME='CURRENCY_ID')
ALTER TABLE TBL_POWER_PURCHASE ADD CURRENCY_ID INT NULL
GO
UPDATE TBL_POWER_PURCHASE SET CURRENCY_ID =(SELECT TOP 1 CURRENCY_ID FROM TLKP_CURRENCY WHERE IS_DEFAULT_CURRENCY=1) WHERE CURRENCY_ID IS NULL
GO
ALTER TABLE TBL_POWER_PURCHASE ALTER COLUMN CURRENCY_ID INT NOT NULL;
IF OBJECT_ID('REPORT_QUARTER_POWER_SOURCE') IS NOT NULL DROP PROC REPORT_QUARTER_POWER_SOURCE
GO
CREATE PROC REPORT_QUARTER_POWER_SOURCE
	@DATE DATETIME = '2012-1-1'
AS
DECLARE @D1 DATETIME,
		@D2 DATETIME;
SET @DATE = DATEADD(D,0,DATEDIFF(D,0,@DATE));
SET @D1= @DATE;
SET @D2= DATEADD(S,-1,DATEADD(M,3,@DATE));

SELECT TOP 10 *
FROM(
	SELECT	AGREEMENT_SIGN_DATE=START_DATE,
			SOURCE_OF_PURCHASE=SELLER_NAME,
			VOLTAGE_NAME,
			NUMBER_OF_CONNECTION=NO_OF_CONNECTION,
			RATE_OF_PURCHASE=PRICE,
			c.CURRENCY_SING,
			c.CURRENCY_NAME,
			s.NOTE
	FROM TBL_POWER_SOURCE s
	INNER JOIN TBL_VOLTAGE v ON v.VOLTAGE_ID = s.VOLTAGE_ID
	INNER JOIN TLKP_CURRENCY c ON s.CURRENCY_ID = c.CURRENCY_ID
	WHERE	s.IS_ACTIVE=1
			AND (START_DATE <= @D2)
			AND (IS_INUSED=1 OR END_DATE > @D1 )

	UNION ALL SELECT NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
	UNION ALL SELECT NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
	UNION ALL SELECT NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
	UNION ALL SELECT NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
	UNION ALL SELECT NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
	UNION ALL SELECT NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
	UNION ALL SELECT NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
	UNION ALL SELECT NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
	UNION ALL SELECT NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
	UNION ALL SELECT NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
	UNION ALL SELECT NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
	UNION ALL SELECT NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
	UNION ALL SELECT NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
	UNION ALL SELECT NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
) x 
-- END OF REPORT_QUARTER_POWER_SOURCE
GO
IF OBJECT_ID('REPORT_QUARTER_POWER_PURCHASE') IS NOT NULL DROP PROC REPORT_QUARTER_POWER_PURCHASE
GO
CREATE PROC REPORT_QUARTER_POWER_PURCHASE
	@DATE DATETIME = '2012-8-1'
AS
DECLARE @D1 DATETIME,
		@D2 DATETIME;
SET @DATE = DATEADD(D,0,DATEDIFF(D,0,@DATE));
SET @D1= CONVERT(NVARCHAR(4),@DATE,126)+'-01-01'; 
SET @D2= DATEADD(S,-1,DATEADD(M,3,@DATE));

CREATE TABLE #MONTHS(M DATETIME);
DECLARE @M INT;
SET @M=1;
WHILE @M<=12
BEGIN
	INSERT INTO #MONTHS VALUES(CONVERT(NVARCHAR(4),@D1,126)+'-'+CONVERT(NVARCHAR,@M)+'-1');
	SET @M=@M+1;
END
 
SELECT m.M,SOURCE_ID
INTO #TMP
FROM #MONTHS m,
(	SELECT DISTINCT SOURCE_ID 
	FROM TBL_POWER_PURCHASE 
	WHERE	IS_ACTIVE=1 
			AND MONTH BETWEEN @D1 AND @D2) t

IF NOT EXISTS(SELECT * FROM #TMP)
	INSERT INTO #TMP
	SELECT m.M, SOURCE_ID 
	FROM #MONTHS m, (SELECT SOURCE_ID = 0) t

SELECT	SOURCE_ID = t.SOURCE_ID,
		SOURCE_NAME = SELLER_NAME,
		MONTH = t.M,
		POWER_PURCHASE, 
		p.PRICE,
		CURRENCY_SING,
		AMOUNT = p.PRICE * POWER_PURCHASE
FROM #TMP t 
LEFT JOIN (
	SELECT SOURCE_ID,MONTH,
		POWER_PURCHASE=SUM(POWER_PURCHASE),
		PRICE=MAX(PRICE),
		CURRENCY_SING
	FROM TBL_POWER_PURCHASE pp
	JOIN TLKP_CURRENCY c ON pp.CURRENCY_ID = c.CURRENCY_ID
	WHERE MONTH BETWEEN @D1 AND @D2 
	AND IS_ACTIVE = 1
	GROUP BY SOURCE_ID,c.CURRENCY_ID,c.CURRENCY_SING,MONTH
) p 
	ON t.SOURCE_ID = p.SOURCE_ID AND t.M = p.MONTH 
LEFT JOIN TBL_POWER_SOURCE s ON s.SOURCE_ID = t.SOURCE_ID
-- END OF REPORT_QUARTER_POWER_PURCHASE
	RAISERROR('CUSTOMER_CODE_CHECK: មានលេខកូដអតិថិជនស្ទួន សូមទំនាក់ទំនងអ្នកបច្ចេកទេស!', 16, 1);
"; 
        }
    }
}
