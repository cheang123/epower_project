﻿namespace EPower.Update
{
    /// <summary>
    /// FIX  
    ///  - Morm Raksmey : 
    ///     1.  Visible Billing Cycle at Specific Date
    ///     2.  Alert Bank Payment Submit when Add Penalty 
    ///     3.  Add Permission on Payment Data
    ///     4.  Update ReportPaymentSummary : Update Report Store to get all Payment althought void in Payment
    ///     5.  Update Store REPORT_PAYMENT_TOTAL & REPORT_PAYMENT_TOTAL_DETAIL : COMMENT CODE ON d.PAY_AMOUNT>0 AND (@INCLUDE_DELETE=1 OR p.IS_VOID = 0)
    ///     6.  Update TRANSFORMER_POSITION_TYPE_ID & TRANSFORMER_TYPE_ID in TBL_TRANSFORMER
    ///     7.  Udate Store and Report File : Add Area,Pole,Box in REPORT_BANK_PAYMENT_DETAIL
    ///     8.  Correction word in REPORT_AS_05.rpt
    ///     9.  Add New ReportPowerIncomeByAmpare.rpt (Anco Poi Pet Require)
    ///     10. Format 4 Digit in ReportQuarter.rpt in exe
   
    /// </summary>
    /// <returns></returns>

    class Version1_1_0_9 : Version
    {        
        protected override string GetBatchCommand()
        {
            return @"
IF OBJECT_ID('REPORT_PAYMENT_TOTAL')IS NOT NULL
	DROP PROC REPORT_PAYMENT_TOTAL
GO

--@15-JUN-2015 BY Morm Raksmey 
-- 1. COMMENT CODE ON d.PAY_AMOUNT>0 AND (@INCLUDE_DELETE=1 OR p.IS_VOID = 0)
CREATE PROC REPORT_PAYMENT_TOTAL
	@D1 DATETIME='2015-06-12',
	@D2 DATETIME='2015-06-12',
	@ITEM_TYPE_ID INT=0,
	@ITEM_ID INT =0,
	@INCLUDE_DELETE BIT=1,
	@CURRENCY_ID INT=0,
	@AREA_ID INT=0,
	@BILLING_CYCLE_ID INT=0
AS   
SET @D1 = DATEADD(D,0,DATEDIFF(D,0,@D1));
SET @D2 = DATEADD(S,-1,DATEADD(D,1,DATEDIFF(D,0,@D2)));

SELECT	cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING, 
		INVOICE_MONTH=N'សរុបវិក្កយបត្រ '+REPLACE(RIGHT(CONVERT(VARCHAR(11), i.INVOICE_MONTH, 106), 8), ' ', '-'),
		PAY_AMOUNT=SUM(d.PAY_AMOUNT),
		TYPE_ID=1,
		TYPE_NAME=N'វិក្កយបត្រប្រើប្រាស់ទឹក' 
FROM TBL_PAYMENT p
INNER JOIN TBL_PAYMENT_DETAIL d ON p.PAYMENT_ID=d.PAYMENT_ID
INNER JOIN TBL_INVOICE i ON i.INVOICE_ID=d.INVOICE_ID
OUTER APPLY(
	SELECT TOP 1 t.INVOICE_ITEM_ID,t.INVOICE_ITEM_TYPE_ID
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE_ITEM t ON d.INVOICE_ITEM_ID=t.INVOICE_ITEM_ID 
	WHERE d.INVOICE_ID = i.INVOICE_ID
) itm
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=i.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_TYPE t ON t.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=p.CURRENCY_ID
WHERE (p.PAY_DATE BETWEEN @D1 AND @D2) 
		AND (@ITEM_ID = 0 OR INVOICE_ITEM_ID=@ITEM_ID)
		AND (@ITEM_TYPE_ID = 0 OR INVOICE_ITEM_TYPE_ID = @ITEM_TYPE_ID)
		--AND d.PAY_AMOUNT>0
        -- filter payment that removed
		--AND (@INCLUDE_DELETE=1 OR p.IS_VOID = 0)
		AND (@CURRENCY_ID=0 OR cx.CURRENCY_ID=@CURRENCY_ID)
		AND (@AREA_ID=0 OR c.AREA_ID=@AREA_ID)
		AND (@BILLING_CYCLE_ID=0 OR c.BILLING_CYCLE_ID=@BILLING_CYCLE_ID)
		AND i.IS_SERVICE_BILL=0
GROUP BY cx.CURRENCY_ID,cx.CURRENCY_NAME,cx.CURRENCY_SING,i.INVOICE_MONTH

UNION ALL
SELECT  cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,   
		INVOICE_MONTH=N'សរុបបង់ប្រាក់មុន',
		PAY_AMOUNT=SUM(b.BUY_AMOUNT),
		TYPE_ID=2,
		TYPE_NAME=N'បង់ប្រាក់មុន' 
FROM TBL_CUSTOMER_BUY b
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=b.PREPAID_CUS_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=b.CURRENCY_ID
INNER JOIN TBL_CUSTOMER_TYPE t ON t.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
WHERE (b.CREATE_ON BETWEEN @D1 AND @D2) 
		--AND b.BUY_AMOUNT>0
        -- filter payment that removed
		--AND (@INCLUDE_DELETE=1 OR b.IS_VOID = 0)
		AND (@CURRENCY_ID=0 OR cx.CURRENCY_ID=@CURRENCY_ID)
		AND (@AREA_ID=0 OR c.AREA_ID=@AREA_ID)
GROUP BY cx.CURRENCY_ID,cx.CURRENCY_NAME,cx.CURRENCY_SING

UNION ALL
SELECT	cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		INVOICE_MONTH=N'សរុបប្រាក់ភ្ជាប់ចរន្ត',
		PAY_AMOUNT=SUM(d.PAY_AMOUNT),
		TYPE_ID=3,
		TYPE_NAME=N'ប្រាក់ភ្ជាប់ចរន្ត'
FROM TBL_PAYMENT p
INNER JOIN TBL_PAYMENT_DETAIL d ON p.PAYMENT_ID=d.PAYMENT_ID
INNER JOIN TBL_INVOICE i ON i.INVOICE_ID=d.INVOICE_ID
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=i.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_TYPE t ON t.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=p.CURRENCY_ID
OUTER APPLY(
	SELECT TOP 1 t.INVOICE_ITEM_ID,t.INVOICE_ITEM_TYPE_ID
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE_ITEM t ON d.INVOICE_ITEM_ID=t.INVOICE_ITEM_ID 
	WHERE d.INVOICE_ID = i.INVOICE_ID
) itm
WHERE  (p.PAY_DATE BETWEEN @D1 AND @D2) 
		AND (@ITEM_ID = 0 OR INVOICE_ITEM_ID=@ITEM_ID)
		AND (@ITEM_TYPE_ID = 0 OR INVOICE_ITEM_TYPE_ID = @ITEM_TYPE_ID)
		--AND d.PAY_AMOUNT>0
        -- filter payment that removed
		--AND (@INCLUDE_DELETE=1 OR p.IS_VOID = 0)
		AND (@CURRENCY_ID=0 OR cx.CURRENCY_ID=@CURRENCY_ID)
		AND (@AREA_ID=0 OR c.AREA_ID=@AREA_ID)
		AND (@BILLING_CYCLE_ID=0 OR c.BILLING_CYCLE_ID=@BILLING_CYCLE_ID)
		AND i.IS_SERVICE_BILL=1
		AND itm.INVOICE_ITEM_ID=3
GROUP BY cx.CURRENCY_ID,cx.CURRENCY_NAME,cx.CURRENCY_SING

UNION ALL 
SELECT	cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING, 
		INVOICE_MONTH=N'សរុបប្រាក់កក់' , 
		PAY_AMOUNT=SUM(d.AMOUNT), 
		TYPE_ID=4,
		TYPE_NAME=N'ប្រាក់កក់' 
FROM TBL_CUS_DEPOSIT d
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=d.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_TYPE t ON t.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=d.CURRENCY_ID
WHERE (d.DEPOSIT_DATE BETWEEN @D1 AND @D2)
		AND d.IS_PAID=1
		AND (@CURRENCY_ID=0 OR cx.CURRENCY_ID=@CURRENCY_ID)
		AND (@AREA_ID=0 OR c.AREA_ID=@AREA_ID)
		AND (@BILLING_CYCLE_ID=0 OR c.BILLING_CYCLE_ID=@BILLING_CYCLE_ID)
GROUP BY cx.CURRENCY_ID,cx.CURRENCY_NAME,cx.CURRENCY_SING

UNION ALL
SELECT	cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		INVOICE_MONTH=N'សរុបទូទាត់ថ្លៃសេវាកម្មផ្សេងៗ' , 
		PAY_AMOUNT=SUM(d.PAY_AMOUNT),
		TYPE_ID=5,
		TYPE_NAME=N'ទូទាត់ថ្លៃសេវាកម្មផ្សេងៗ'
FROM TBL_PAYMENT p
INNER JOIN TBL_PAYMENT_DETAIL d ON p.PAYMENT_ID=d.PAYMENT_ID
INNER JOIN TBL_INVOICE i ON i.INVOICE_ID=d.INVOICE_ID
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=i.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_TYPE t ON t.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=p.CURRENCY_ID
OUTER APPLY(
	SELECT TOP 1 t.INVOICE_ITEM_ID,t.INVOICE_ITEM_TYPE_ID
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE_ITEM t ON d.INVOICE_ITEM_ID=t.INVOICE_ITEM_ID 
	WHERE d.INVOICE_ID = i.INVOICE_ID
) itm
WHERE (p.PAY_DATE BETWEEN @D1 AND @D2) 
		AND (@ITEM_ID = 0 OR INVOICE_ITEM_ID=@ITEM_ID)
		AND (@ITEM_TYPE_ID = 0 OR INVOICE_ITEM_TYPE_ID = @ITEM_TYPE_ID)
		--AND d.PAY_AMOUNT>0
        -- filter payment that removed
		--AND (@INCLUDE_DELETE=1 OR p.IS_VOID = 0)
		AND (@CURRENCY_ID=0 OR cx.CURRENCY_ID=@CURRENCY_ID)
		AND (@AREA_ID=0 OR c.AREA_ID=@AREA_ID)
		AND (@BILLING_CYCLE_ID=0 OR c.BILLING_CYCLE_ID=@BILLING_CYCLE_ID)
		AND i.IS_SERVICE_BILL=1
		AND itm.INVOICE_ITEM_ID!=3
GROUP BY cx.CURRENCY_ID,cx.CURRENCY_NAME,cx.CURRENCY_SING


GO
IF NOT EXISTS(SELECT * FROM TBL_PERMISSION WHERE PERMISSION_NAME=N'PAYMENT_DATE')
	INSERT INTO TBL_PERMISSION 
	SELECT N'ថ្ងៃបង់ប្រាក់',N'PAYMENT_DATE',0,10,0
GO


IF OBJECT_ID('REPORT_PAYMENT_SUMMARY')IS NOT NULL
	DROP PROC REPORT_PAYMENT_SUMMARY
GO
-- @2015-Jun-10 by Morm Raksmey
-- 1. Upate ON Payment take Payment that VOID
CREATE PROC REPORT_PAYMENT_SUMMARY
	@D1 DATETIME='2014-04-01',
	@D2 DATETIME='2014-08-08',
	@ITEM_TYPE_ID INT=0,
	@ITEM_ID INT =0,
	@CURRENCY_ID INT=0,
	@AREA_ID INT=0,
	@BILLING_CYCLE_ID INT=0
AS   
SET @D1 = DATEADD(D,0,DATEDIFF(D,0,@D1));
SET @D2 = DATEADD(S,-1,DATEADD(D,1,DATEDIFF(D,0,@D2)));
DECLARE @DATE DATETIME;
SET @DATE=@D1;

-- PAYMENT
SELECT	p.CURRENCY_ID,
		INVOICE_MONTH,
		TYPE='PAYMENT',
		AMOUNT  = SUM(ISNULL(pd.PAY_AMOUNT,0))
INTO #TMP
FROM TBL_PAYMENT_DETAIL pd 
INNER JOIN TBL_PAYMENT p ON p.PAYMENT_ID=pd.PAYMENT_ID
INNER JOIN TBL_INVOICE i ON i.INVOICE_ID = pd.INVOICE_ID 
OUTER APPLY(
	SELECT TOP 1 t.INVOICE_ITEM_ID,t.INVOICE_ITEM_TYPE_ID
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE_ITEM t ON d.INVOICE_ITEM_ID=t.INVOICE_ITEM_ID 
	WHERE d.INVOICE_ID = i.INVOICE_ID
) itm
WHERE	(p.PAY_DATE BETWEEN @D1 AND @D2) 
		AND (@ITEM_ID = 0 OR INVOICE_ITEM_ID=@ITEM_ID)
		AND (@ITEM_TYPE_ID = 0 OR INVOICE_ITEM_TYPE_ID = @ITEM_TYPE_ID)
		--AND pd.PAY_AMOUNT>0
GROUP BY INVOICE_MONTH,p.CURRENCY_ID


-- NEW INVOICE
UNION ALL
SELECT  CURRENCY_ID,
		INVOICE_MONTH,
		TYPE = 'NEW_INVOICE',
		BEFORE_ADJUST =SUM( i.SETTLE_AMOUNT - ISNULL(adj.ALL_ADJUST,0))
		--ADJUST_AMOUNT = ISNULL(adj.ALL_ADJUST,0),
		--AFTER_ADJUST = i.SETTLE_AMOUNT
FROM TBL_INVOICE i 
OUTER APPLY(
	SELECT  ALL_ADJUST=SUM(ADJUST_AMOUNT)
	FROM TBL_INVOICE_ADJUSTMENT 
	WHERE INVOICE_ID=i.INVOICE_ID
) adj
OUTER APPLY(
	SELECT TOP 1 t.INVOICE_ITEM_ID,t.INVOICE_ITEM_TYPE_ID
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE_ITEM t ON d.INVOICE_ITEM_ID=t.INVOICE_ITEM_ID 
	WHERE d.INVOICE_ID = i.INVOICE_ID
) itm
WHERE	INVOICE_DATE BETWEEN @D1 AND @D2 
		AND (@ITEM_ID = 0 OR INVOICE_ITEM_ID=@ITEM_ID)
		AND (@ITEM_TYPE_ID = 0 OR INVOICE_ITEM_TYPE_ID = @ITEM_TYPE_ID)
GROUP BY INVOICE_MONTH,CURRENCY_ID

-- ADJUSTMENT
UNION ALL
SELECT  CURRENCY_ID,
		INVOICE_MONTH,
		TYPE='ADJUST_INVOICE',
		AMOUNT = SUM(ADJUST_AMOUNT)		
FROM TBL_INVOICE_ADJUSTMENT a
INNER JOIN TBL_INVOICE i ON a.INVOICE_ID = i.INVOICE_ID
OUTER APPLY(
	SELECT TOP 1 t.INVOICE_ITEM_ID,t.INVOICE_ITEM_TYPE_ID
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE_ITEM t ON d.INVOICE_ITEM_ID=t.INVOICE_ITEM_ID 
	WHERE d.INVOICE_ID = i.INVOICE_ID
) itm
WHERE (a.CREATE_ON BETWEEN @D1 AND @D2)
	AND (@ITEM_ID = 0 OR INVOICE_ITEM_ID=@ITEM_ID)
	AND (@ITEM_TYPE_ID = 0 OR INVOICE_ITEM_TYPE_ID = @ITEM_TYPE_ID) 
GROUP BY INVOICE_MONTH,CURRENCY_ID

-- CANCEL PAYMENT 
UNION ALL
SELECT	p.CURRENCY_ID,
		INVOICE_MONTH,
		TYPE='CANCEL_PAYMENT',
		AMOUNT=SUM(ISNULL(pd.PAY_AMOUNT,0)*-1)
FROM TBL_PAYMENT_DETAIL pd INNER JOIN TBL_PAYMENT p ON p.PAYMENT_ID=pd.PAYMENT_ID
INNER JOIN TBL_INVOICE i ON i.INVOICE_ID = pd.INVOICE_ID 
OUTER APPLY(
	SELECT TOP 1 t.INVOICE_ITEM_ID,t.INVOICE_ITEM_TYPE_ID
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE_ITEM t ON d.INVOICE_ITEM_ID=t.INVOICE_ITEM_ID 
	WHERE d.INVOICE_ID = i.INVOICE_ID
) itm
WHERE	(p.PAY_DATE BETWEEN @D1 AND @D2) 
		AND (@ITEM_ID = 0 OR INVOICE_ITEM_ID=@ITEM_ID)
		AND (@ITEM_TYPE_ID = 0 OR INVOICE_ITEM_TYPE_ID = @ITEM_TYPE_ID)
		AND p.PAY_AMOUNT<0
GROUP BY INVOICE_MONTH,p.CURRENCY_ID
-- REPORT PAYMENT CANCEL

-- BEGIN AR
UNION ALL
SELECT  CURRENCY_ID,
		INVOICE_MONTH,
		TYPE = 'BEGIN_AR',
		BALANCE=SUM( i.SETTLE_AMOUNT - ISNULL(adj.ALL_ADJUST,0) + ISNULL(adj.ADJUST,0) 
					- ISNULL(pay.PAID_AMOUNT,0))
FROM TBL_INVOICE i
OUTER APPLY(
	SELECT PAID_AMOUNT = SUM(dd.PAY_AMOUNT) 
	FROM TBL_PAYMENT dp
	INNER JOIN TBL_PAYMENT_DETAIL dd ON dd.PAYMENT_ID = dp.PAYMENT_ID
	WHERE dd.INVOICE_ID=i.INVOICE_ID  AND dp.PAY_DATE<=@D1
) pay
OUTER APPLY(
	SELECT  ADJUST=SUM(CASE WHEN CREATE_ON<=@D1 THEN ADJUST_AMOUNT ELSE 0 END ),
			ALL_ADJUST=SUM(ADJUST_AMOUNT)
	FROM TBL_INVOICE_ADJUSTMENT 
	WHERE INVOICE_ID=i.INVOICE_ID
) adj
OUTER APPLY(
	SELECT TOP 1 t.INVOICE_ITEM_ID,t.INVOICE_ITEM_TYPE_ID
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE_ITEM t ON d.INVOICE_ITEM_ID=t.INVOICE_ITEM_ID 
	WHERE d.INVOICE_ID = i.INVOICE_ID
) itm
WHERE	INVOICE_DATE<@D1
		AND i.SETTLE_AMOUNT - ISNULL(adj.ALL_ADJUST,0) + ISNULL(adj.ADJUST,0)-ISNULL(pay.PAID_AMOUNT,0)>0
		AND (@ITEM_ID = 0 OR INVOICE_ITEM_ID=@ITEM_ID)
		AND (@ITEM_TYPE_ID = 0 OR INVOICE_ITEM_TYPE_ID = @ITEM_TYPE_ID)
GROUP BY INVOICE_MONTH,CURRENCY_ID
 
-- ENDING_AR 
UNION ALL
SELECT  CURRENCY_ID,
		INVOICE_MONTH,
		TYPE = 'ENDING_AR',
		BALANCE=SUM( i.SETTLE_AMOUNT - ISNULL(adj.ALL_ADJUST,0) + ISNULL(adj.ADJUST,0) 
					- ISNULL(pay.PAID_AMOUNT,0))
FROM TBL_INVOICE i
OUTER APPLY(
	SELECT PAID_AMOUNT = SUM(dd.PAY_AMOUNT) 
	FROM TBL_PAYMENT dp
	INNER JOIN TBL_PAYMENT_DETAIL dd ON dd.PAYMENT_ID = dp.PAYMENT_ID
	WHERE dd.INVOICE_ID=i.INVOICE_ID  AND dp.PAY_DATE<=@D2
) pay
OUTER APPLY(
	SELECT  ADJUST=SUM(CASE WHEN CREATE_ON<=@D2 THEN ADJUST_AMOUNT ELSE 0 END ),
			ALL_ADJUST=SUM(ADJUST_AMOUNT)
	FROM TBL_INVOICE_ADJUSTMENT 
	WHERE INVOICE_ID=i.INVOICE_ID
) adj
OUTER APPLY(
	SELECT TOP 1 t.INVOICE_ITEM_ID,t.INVOICE_ITEM_TYPE_ID
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE_ITEM t ON d.INVOICE_ITEM_ID=t.INVOICE_ITEM_ID 
	WHERE d.INVOICE_ID = i.INVOICE_ID
) itm
INNER JOIN TBL_CUSTOMER c ON i.CUSTOMER_ID = c.CUSTOMER_ID 
WHERE	INVOICE_DATE<@D2
		AND i.SETTLE_AMOUNT - ISNULL(adj.ALL_ADJUST,0) + ISNULL(adj.ADJUST,0)-ISNULL(pay.PAID_AMOUNT,0)>0
		AND (@ITEM_ID = 0 OR INVOICE_ITEM_ID=@ITEM_ID)
		AND (@ITEM_TYPE_ID = 0 OR INVOICE_ITEM_TYPE_ID = @ITEM_TYPE_ID)
		AND (@BILLING_CYCLE_ID=0 OR c.BILLING_CYCLE_ID=@BILLING_CYCLE_ID)
GROUP BY INVOICE_MONTH,CURRENCY_ID
-- END OF REPORT_AGING_DETAIL


SELECT cx.CURRENCY_ID,
	   cx.CURRENCY_NAME,
	   cx.CURRENCY_SING,
	   INVOICE_MONTH, 
	   BEGIN_AR = SUM(CASE WHEN TYPE='BEGIN_AR' THEN AMOUNT ELSE 0 END),
	   NEW_INVOICE = SUM(CASE WHEN TYPE='NEW_INVOICE' THEN AMOUNT ELSE 0 END),
	   ADJUST_INVOICE = SUM(CASE WHEN TYPE='ADJUST_INVOICE' THEN AMOUNT ELSE 0 END),
	   PAYMENT = SUM(CASE WHEN TYPE='PAYMENT' THEN AMOUNT ELSE 0 END),
	   CANCEL_PAYMENT = SUM(CASE WHEN TYPE='CANCEL_PAYMENT' THEN AMOUNT ELSE 0 END), 
	   ENDING_AR = SUM(CASE WHEN TYPE='ENDING_AR' THEN AMOUNT ELSE 0 END) 
FROM #TMP t
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=t.CURRENCY_ID
GROUP BY INVOICE_MONTH,cx.CURRENCY_ID,cx.CURRENCY_NAME,cx.CURRENCY_SING
-- END OF REPORT_PAYMENT_SUMMARY





GO
IF OBJECT_ID('REPORT_PAYMENT_TOTAL_DETAIL')IS NOT NULL
	DROP PROC REPORT_PAYMENT_TOTAL_DETAIL
GO

--@15-JUN-2015 BY Morm Raksmey 
-- 1. COMMENT CODE ON d.PAY_AMOUNT>0 AND (@INCLUDE_DELETE=1 OR p.IS_VOID = 0)
CREATE PROC REPORT_PAYMENT_TOTAL_DETAIL
	@D1 DATETIME='2015-06-12',
	@D2 DATETIME='2015-06-12',
	@ITEM_TYPE_ID INT=0,
	@ITEM_ID INT =0,
	@INCLUDE_DELETE BIT=1,
	@CURRENCY_ID INT=0,
	@AREA_ID INT=0,
	@BILLING_CYCLE_ID INT=0
AS   
SET @D1 = DATEADD(D,0,DATEDIFF(D,0,@D1));
SET @D2 = DATEADD(S,-1,DATEADD(D,1,DATEDIFF(D,0,@D2)));

SELECT	cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		c.CUSTOMER_ID,
		c.CUSTOMER_CODE,
		CUSTOMER_NAME=c.LAST_NAME_KH+' ' + c.FIRST_NAME_KH,
		t.CUSTOMER_TYPE_NAME,
		i.INVOICE_ID,
		i.INVOICE_NO,
		i.INVOICE_DATE,
		i.INVOICE_TITLE,
		i.SETTLE_AMOUNT,
		d.PAY_AMOUNT,
		p.PAY_DATE,
		TYPE_ID=1,
		TYPE_NAME=N'វិក្កយបត្រ'
		,p.CREATE_BY 
FROM TBL_PAYMENT p
INNER JOIN TBL_PAYMENT_DETAIL d ON p.PAYMENT_ID=d.PAYMENT_ID
INNER JOIN TBL_INVOICE i ON i.INVOICE_ID=d.INVOICE_ID
OUTER APPLY(
	SELECT TOP 1 t.INVOICE_ITEM_ID,t.INVOICE_ITEM_TYPE_ID
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE_ITEM t ON d.INVOICE_ITEM_ID=t.INVOICE_ITEM_ID 
	WHERE d.INVOICE_ID = i.INVOICE_ID
) itm
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=i.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_TYPE t ON t.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=p.CURRENCY_ID
WHERE (p.PAY_DATE BETWEEN @D1 AND @D2) 
		AND (@ITEM_ID = 0 OR INVOICE_ITEM_ID=@ITEM_ID)
		AND (@ITEM_TYPE_ID = 0 OR INVOICE_ITEM_TYPE_ID = @ITEM_TYPE_ID)
		--AND d.PAY_AMOUNT>0
        -- filter payment that removed
		--AND (@INCLUDE_DELETE=1 OR p.IS_VOID = 0)
		AND (@CURRENCY_ID=0 OR cx.CURRENCY_ID=@CURRENCY_ID)
		AND (@AREA_ID=0 OR c.AREA_ID=@AREA_ID)
		AND (@BILLING_CYCLE_ID=0 OR c.BILLING_CYCLE_ID=@BILLING_CYCLE_ID)
	AND i.IS_SERVICE_BILL=0
	
UNION ALL 
SELECT  cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		c.CUSTOMER_ID,
		c.CUSTOMER_CODE,
		CUSTOMER_NAME=c.LAST_NAME_KH+' ' + c.FIRST_NAME_KH,
		t.CUSTOMER_TYPE_NAME,
		b.CUSTOMER_BUY_ID,
		b.BUY_NO,
		b.CREATE_ON,
		N'ទិញ Prepaid',
		b.BUY_AMOUNT,
		b.BUY_AMOUNT,
		b.CREATE_ON,
		TYPE_ID=2,
		TYPE_NAME=N'បង់ប្រាក់មុន'
		,b.CREATE_BY
FROM TBL_CUSTOMER_BUY b
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=b.PREPAID_CUS_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=b.CURRENCY_ID
INNER JOIN TBL_CUSTOMER_TYPE t ON t.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
WHERE (b.CREATE_ON BETWEEN @D1 AND @D2) 
		--AND b.BUY_AMOUNT>0
        -- filter payment that removed
		--AND (@INCLUDE_DELETE=1 OR b.IS_VOID = 0)
		AND (@CURRENCY_ID=0 OR cx.CURRENCY_ID=@CURRENCY_ID)
		AND (@AREA_ID=0 OR c.AREA_ID=@AREA_ID)
		
UNION ALL
SELECT	cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		c.CUSTOMER_ID,
		c.CUSTOMER_CODE,
		CUSTOMER_NAME=c.LAST_NAME_KH+' ' + c.FIRST_NAME_KH,
		t.CUSTOMER_TYPE_NAME,
		i.INVOICE_ID,
		i.INVOICE_NO,
		i.INVOICE_DATE,
		i.INVOICE_TITLE,
		i.SETTLE_AMOUNT,
		d.PAY_AMOUNT,
		p.PAY_DATE,
		TYPE_ID=3,
		TYPE_NAME=N'ប្រាក់ភ្ជាប់ចរន្ត'
		,p.CREATE_BY
FROM TBL_PAYMENT p
INNER JOIN TBL_PAYMENT_DETAIL d ON p.PAYMENT_ID=d.PAYMENT_ID
INNER JOIN TBL_INVOICE i ON i.INVOICE_ID=d.INVOICE_ID
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=i.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_TYPE t ON t.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=p.CURRENCY_ID
OUTER APPLY(
	SELECT TOP 1 t.INVOICE_ITEM_ID,t.INVOICE_ITEM_TYPE_ID
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE_ITEM t ON d.INVOICE_ITEM_ID=t.INVOICE_ITEM_ID 
	WHERE d.INVOICE_ID = i.INVOICE_ID
) itm
WHERE  (p.PAY_DATE BETWEEN @D1 AND @D2) 
		AND (@ITEM_ID = 0 OR INVOICE_ITEM_ID=@ITEM_ID)
		AND (@ITEM_TYPE_ID = 0 OR INVOICE_ITEM_TYPE_ID = @ITEM_TYPE_ID)
		--AND d.PAY_AMOUNT>0
        -- filter payment that removed
		--AND (@INCLUDE_DELETE=1 OR p.IS_VOID = 0)
		AND (@CURRENCY_ID=0 OR cx.CURRENCY_ID=@CURRENCY_ID)
		AND (@AREA_ID=0 OR c.AREA_ID=@AREA_ID)
		AND (@BILLING_CYCLE_ID=0 OR c.BILLING_CYCLE_ID=@BILLING_CYCLE_ID)
		AND i.IS_SERVICE_BILL=1
		AND itm.INVOICE_ITEM_ID=3

UNION ALL 
SELECT	cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		c.CUSTOMER_ID,
		c.CUSTOMER_CODE,
		CUSTOMER_NAME=c.LAST_NAME_KH+' ' + c.FIRST_NAME_KH,
		t.CUSTOMER_TYPE_NAME,
		d.CUS_DEPOSIT_ID,
		d.DEPOSIT_NO,
		d.DEPOSIT_DATE,
		N'ប្រាក់កក់',
		d.AMOUNT,
		d.AMOUNT,
		d.DEPOSIT_DATE,
		TYPE_ID=4,
		TYPE_NAME=N'ប្រាក់កក់'
		,d.CREATE_BY
FROM TBL_CUS_DEPOSIT d
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=d.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_TYPE t ON t.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=d.CURRENCY_ID
WHERE (d.DEPOSIT_DATE BETWEEN @D1 AND @D2)
		AND d.IS_PAID=1
		AND (@CURRENCY_ID=0 OR cx.CURRENCY_ID=@CURRENCY_ID)
		AND (@AREA_ID=0 OR c.AREA_ID=@AREA_ID)
		AND (@BILLING_CYCLE_ID=0 OR c.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) 

UNION ALL
SELECT	cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		c.CUSTOMER_ID,
		c.CUSTOMER_CODE,
		CUSTOMER_NAME=c.LAST_NAME_KH+' ' + c.FIRST_NAME_KH,
		t.CUSTOMER_TYPE_NAME,
		i.INVOICE_ID,
		i.INVOICE_NO,
		i.INVOICE_DATE,
		i.INVOICE_TITLE,
		i.SETTLE_AMOUNT,
		d.PAY_AMOUNT,
		p.PAY_DATE,
		TYPE_ID=5,
		TYPE_NAME=N'ទូទាត់ថ្លៃសេវាកម្មផ្សេងៗ'
		,p.CREATE_BY
FROM TBL_PAYMENT p
INNER JOIN TBL_PAYMENT_DETAIL d ON p.PAYMENT_ID=d.PAYMENT_ID
INNER JOIN TBL_INVOICE i ON i.INVOICE_ID=d.INVOICE_ID
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=i.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_TYPE t ON t.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=p.CURRENCY_ID
OUTER APPLY(
	SELECT TOP 1 t.INVOICE_ITEM_ID,t.INVOICE_ITEM_TYPE_ID
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE_ITEM t ON d.INVOICE_ITEM_ID=t.INVOICE_ITEM_ID 
	WHERE d.INVOICE_ID = i.INVOICE_ID
) itm
WHERE (p.PAY_DATE BETWEEN @D1 AND @D2) 
		AND (@ITEM_ID = 0 OR INVOICE_ITEM_ID=@ITEM_ID)
		AND (@ITEM_TYPE_ID = 0 OR INVOICE_ITEM_TYPE_ID = @ITEM_TYPE_ID)
		--AND d.PAY_AMOUNT>0
        -- filter payment that removed
		--AND (@INCLUDE_DELETE=1 OR p.IS_VOID = 0)
		AND (@CURRENCY_ID=0 OR cx.CURRENCY_ID=@CURRENCY_ID)
		AND (@AREA_ID=0 OR c.AREA_ID=@AREA_ID)
		AND (@BILLING_CYCLE_ID=0 OR c.BILLING_CYCLE_ID=@BILLING_CYCLE_ID)
		AND i.IS_SERVICE_BILL=1
		AND itm.INVOICE_ITEM_ID!=3
ORDER BY 15,11 , 14  ASC ;

GO
UPDATE TBL_TRANSFORMER SET TRANSFORMER_POSITION_ID=10101 WHERE TRANSFORMER_POSITION_ID=10001
UPDATE TBL_TRANSFORMER SET TRANSFORMER_TYPE_ID=10001 WHERE TRANSFORMER_TYPE_ID=10101
GO

IF OBJECT_ID('REPORT_BANK_PAYMENT_DETAIL')IS NOT NULL
	DROP PROC REPORT_BANK_PAYMENT_DETAIL
GO


CREATE PROC REPORT_BANK_PAYMENT_DETAIL
	@D1 DATETIME='2014-01-01',
	@D2 DATETIME='2014-12-31',
	@BANK NVARCHAR(50)='',
	@BRANCH NVARCHAR(50)='',
	@CURRENCY_ID INT=0,
	@AREA_ID INT=0,
	@BILLING_CYCLE_ID INT=0
AS
SET @D1 = DATEADD(D,0,DATEDIFF(D,0,@D1));
SET @D2 = DATEADD(S,-1,DATEADD(D,1,DATEDIFF(D,0,@D2)));

SELECT	BANK_PAYMENT_DETAIL_ID,
		BANK_PAYMENT_ID,
		EXT_ID,
		BANK,
		BRANCH,
		CURRENCY,
		d.CUSTOMER_CODE,
		PAY_DATE,
		PAY_AMOUNT,
		NOTE,
		CASHIER,
		PAYMENT_METHOD,
		PAYMENT_TYPE,
		RESULT,
		RESULT_NOTE,
		PAID_AMOUNT,
		UPDATE_DATE,
		c.CUSTOMER_ID,
		CUSTOMER_NAME=c.LAST_NAME_KH+' '+c.FIRST_NAME_KH,
		cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING ,
		DEPOSIT = ISNULL(pd.PAY_DEPOSIT,0),
		CONNECTION =ISNULL(pc.PAY_CONNECTION,0),
		POWER = ISNULL(pp.PAY_POWER,0),
		a.AREA_ID,
		a.AREA_NAME,
		p.POLE_ID,
		p.POLE_CODE,
		b.BOX_ID,
		b.BOX_CODE
FROM TBL_BANK_PAYMENT_DETAIL d
LEFT JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID =d.CUSTOMER_ID
LEFT JOIN TBL_AREA a ON a.AREA_ID=c.AREA_ID
LEFT JOIN TBL_CUSTOMER_METER cm ON cm.CUSTOMER_ID=c.CUSTOMER_ID AND cm.IS_ACTIVE=1
LEFT JOIN TBL_POLE p ON p.POLE_ID=cm.POLE_ID
LEFT JOIN TBL_BOX b ON b.BOX_ID=cm.BOX_ID
LEFT JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=d.CURRENCY_ID
OUTER APPLY(
	SELECT PAY_DEPOSIT = SUM(AMOUNT) 
	FROM TBL_CUS_DEPOSIT 
	WHERE BANK_PAYMENT_DETAIL_ID=d.BANK_PAYMENT_DETAIL_ID
) pd -- PAY DEPOSIT
OUTER APPLY(
	SELECT PAY_CONNECTION = SUM(xd.PAY_AMOUNT)
	FROM TBL_PAYMENT xp 
	INNER JOIN TBL_PAYMENT_DETAIL xd ON xp.PAYMENT_ID = xd.PAYMENT_ID 
	WHERE xp.IS_ACTIVE=1  
		AND xp.BANK_PAYMENT_DETAIL_ID = d.BANK_PAYMENT_DETAIL_ID
		AND EXISTS(SELECT INVOICE_DETAIL_ID 
				   FROM TBL_INVOICE_DETAIL 
				   WHERE INVOICE_ID=xd.INVOICE_ID
				   AND INVOICE_ITEM_ID=3)
) pc -- PAY CONNECTION
OUTER APPLY(
	SELECT PAY_POWER = SUM(xd.PAY_AMOUNT)
	FROM TBL_PAYMENT xp 
	INNER JOIN TBL_PAYMENT_DETAIL xd ON xp.PAYMENT_ID = xd.PAYMENT_ID 
	WHERE xp.IS_ACTIVE=1  
		AND xp.BANK_PAYMENT_DETAIL_ID = d.BANK_PAYMENT_DETAIL_ID
		AND NOT EXISTS(SELECT INVOICE_DETAIL_ID 
				   FROM TBL_INVOICE_DETAIL 
				   WHERE INVOICE_ID=xd.INVOICE_ID
				   AND INVOICE_ITEM_ID=3)
) pp -- PAY POWER
WHERE 
d.RESULT=2 -- SUCCESS!
AND d.PAY_DATE BETWEEN @D1 AND @D2
AND d.IS_VOID=0
AND (@CURRENCY_ID=0 OR cx.CURRENCY_ID=@CURRENCY_ID)
AND (@BANK='' OR d.BANK=@BANK)
AND (@BRANCH='' OR d.BRANCH=@BRANCH)
AND (@AREA_ID=0 OR c.AREA_ID=@AREA_ID)
AND (@BILLING_CYCLE_ID=0 OR c.BILLING_CYCLE_ID=@BILLING_CYCLE_ID)

GO
IF OBJECT_ID('REPORT_POWER_INCOME_BY_AMPARE')IS NOT NULL
	DROP PROC REPORT_POWER_INCOME_BY_AMPARE
GO

CREATE PROC REPORT_POWER_INCOME_BY_AMPARE
	@MONTH DATETIME='2015-03-01',
	@AREA_ID INT = 0,
	@CUSTOMER_TYPE_ID INT = 0,
	@CYCLE_ID INT = 0,
	@PRICE_ID INT = 0,
	@CURRENCY_ID INT =0
	
AS
SET @MONTH= CONVERT(NVARCHAR,YEAR(@MONTH))+'-' +CONVERT(NVARCHAR,MONTH(@MONTH))+'-1';
SELECT cx.CURRENCY_ID,
	   cx.CURRENCY_NAME,
	   cx.CURRENCY_SING,
	   a.AMPARE_ID,a.AMPARE_NAME,REPLACE(a.AMPARE_NAME,'A',''),
	   POSTPAID_COUNT  = SUM(CASE WHEN IS_PREPAID=0 THEN CUS ELSE 0 END),
	   POSTPAID_USAGE  = SUM(CASE WHEN IS_PREPAID=0 THEN USAGE ELSE 0 END),
	   POSTPAID_AMOUNT = SUM(CASE WHEN IS_PREPAID=0 THEN AMOUNT ELSE 0 END), 
	   PREPAID_COUNT   = SUM(CASE WHEN IS_PREPAID=1 THEN CUS ELSE 0 END),
	   PREPAID_USAGE   = SUM(CASE WHEN IS_PREPAID=1 THEN USAGE ELSE 0 END),
	   PREPAID_AMOUNT  = SUM(CASE WHEN IS_PREPAID=1 THEN AMOUNT ELSE 0 END)
FROM (
	SELECT  CURRENCY_ID,
			c.AMP_ID,
			CUS = SUM(CASE WHEN c.IS_REACTIVE=1 THEN 0 ELSE 1 END),
			USAGE = SUM( CASE WHEN c.IS_REACTIVE=1 THEN 0 ELSE  i.TOTAL_USAGE + ISNULL(adj.ADJUST_USAGE,0) END),
			AMOUNT =SUM( i.SETTLE_AMOUNT),
			IS_PREPAID = 0
	FROM TBL_INVOICE i
	INNER JOIN TBL_CUSTOMER c ON i.CUSTOMER_ID=c.CUSTOMER_ID 
	OUTER APPLY(
		SELECT ADJUST_USAGE = SUM(ADJUST_USAGE)
		FROM TBL_INVOICE_ADJUSTMENT   
		WHERE INVOICE_ID =  i.INVOICE_ID
	) adj 
	WHERE   i.INVOICE_MONTH=@MONTH 
			AND (@AREA_ID=0 OR c.AREA_ID=@AREA_ID)
			AND (@CUSTOMER_TYPE_ID=0 OR c.CUSTOMER_TYPE_ID=@CUSTOMER_TYPE_ID)
			AND (@PRICE_ID=0 OR c.PRICE_ID=@PRICE_ID)
			AND (@CYCLE_ID=0 OR c.BILLING_CYCLE_ID=@CYCLE_ID)
			AND i.IS_SERVICE_BILL = 0
	GROUP BY CURRENCY_ID,c.AMP_ID

	UNION ALL
	SELECT  CURRENCY_ID,
			c.AMP_ID,
			CUS = COUNT(DISTINCT c.CUSTOMER_ID),
			USAGE= SUM( CASE WHEN c.IS_REACTIVE=1 THEN 0 ELSE cb.BUY_QTY END),	 
			AMOUNT= SUM(cb.BUY_AMOUNT),
			IS_PREPAID = 1
	FROM  TBL_PREPAID_CUSTOMER pc
	INNER JOIN TBL_CUSTOMER_BUY cb ON pc.PREPAID_CUS_ID=cb.PREPAID_CUS_ID
	INNER JOIN TBL_CUSTOMER c ON pc.CUSTOMER_ID=c.CUSTOMER_ID
	WHERE cb.IS_VOID<>1 AND c.CUSTOMER_TYPE_ID<>-1 
		  AND YEAR(cb.CREATE_ON)=YEAR(@MONTH) 
		  AND MONTH(cb.CREATE_ON)=MONTH(@MONTH)
		  AND (@AREA_ID=0 OR c.AREA_ID=@AREA_ID)
		  AND (@CUSTOMER_TYPE_ID=0 OR c.CUSTOMER_TYPE_ID=@CUSTOMER_TYPE_ID)
		  AND (@PRICE_ID=0 OR c.PRICE_ID=@PRICE_ID)
		  AND (@CYCLE_ID=0 OR c.BILLING_CYCLE_ID=@CYCLE_ID)
	GROUP BY CURRENCY_ID,c.AMP_ID
) x
INNER JOIN TBL_AMPARE a ON a.AMPARE_ID=x.AMP_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=x.CURRENCY_ID
WHERE (@CURRENCY_ID=0 OR cx.CURRENCY_ID=@CURRENCY_ID)
GROUP BY cx.CURRENCY_ID,
		 cx.CURRENCY_NAME,
		 cx.CURRENCY_SING,
		 a.AMPARE_ID,
		 a.AMPARE_NAME
ORDER BY RIGHT('000000'+REPLACE(a.AMPARE_NAME,'A',''),6)
-- END OF REPORT_POWER_INCOME_BY_AREA




"; 
        }

    }
}
