﻿IF OBJECT_ID('[TBL_FIX_ASSET_CATEGORY]') IS NULL
	CREATE TABLE [dbo].[TBL_FIX_ASSET_CATEGORY](
		[CATEGORY_ID] [int] IDENTITY(1,1) PRIMARY KEY,
		[CATEGORY_CODE] [nvarchar](50) COLLATE Latin1_General_BIN NOT NULL,
		[CATEGORY_NAME] [nvarchar](100) COLLATE Latin1_General_BIN NOT NULL,
		[NOTE] [nvarchar](max) COLLATE Latin1_General_BIN NOT NULL,
		[ASSET_ACCOUNT_ID] [int] NOT NULL,
		[ACCUMULATED_DEPRECIATION_ACCOUNT_ID] [int] NOT NULL,
		[DEPRECIATION_EXPENSE_ID] [int] NOT NULL,
		[DEFAULT_USEFUL_LIFE] [int] NOT NULL,
		[PARENT_ID] [int] NOT NULL,
		[IS_ACTIVE] [bit] NOT NULL
	);
IF OBJECT_ID('TBL_FIX_ASSET_ITEM') IS NULL 
	CREATE TABLE [dbo].[TBL_FIX_ASSET_ITEM](
		[FIX_ASSET_ITEM_ID] [int] IDENTITY(1,1) PRIMARY KEY,
		[CATEGORY_ID] [int] NOT NULL,
		[FIX_ASSET_NAME] [nvarchar](200) COLLATE Latin1_General_BIN NOT NULL,
		[BRAND] [nvarchar](50) COLLATE Latin1_General_BIN NOT NULL,
		[SOURCE] [nvarchar](50) COLLATE Latin1_General_BIN NOT NULL,
		[YEAR] [nvarchar](50) COLLATE Latin1_General_BIN NOT NULL,
		[USE_DATE] [datetime] NOT NULL,
		[USE_STATUS] [nvarchar](50) COLLATE Latin1_General_BIN NOT NULL,
		[UNIT] [nvarchar](50) COLLATE Latin1_General_BIN NOT NULL,
		[CURRENCY_ID] [int] NOT NULL,
		[QUANTITY] [decimal](18, 2) NOT NULL,
		[PRICE] [decimal](18, 2) NOT NULL,
		[TOTAL_COST] [decimal](18, 2) NOT NULL,
		[USEFUL_LIFE] [int] NOT NULL,
		[ANNUAL_DEPRECIATION_RATE] [decimal](18, 2) NOT NULL,
		[ACCUMULATED_DEPRECIATION] [decimal](18, 2) NOT NULL,
		[SALVAGE_VALUE] [decimal](18, 2) NOT NULL,
		[NOTE] [nvarchar](200) COLLATE Latin1_General_BIN NOT NULL,
		[STATUS_ID] [int] NOT NULL,
		[IS_ACTIVE] [bit] NOT NULL
	);
GO

GO
/*
SELECT 'UNION ALL SELECT '+
'N'''+CONVERT(NVARCHAR,CATEGORY_ID)+''''+','+
'N'''+CONVERT(NVARCHAR,CATEGORY_CODE)+''''+','+
'N'''+CONVERT(NVARCHAR,CATEGORY_NAME)+''''+','+
'N'''+CONVERT(NVARCHAR,NOTE)+''''+','+
'N'''+CONVERT(NVARCHAR,ACCOUNT_ITEM_ID)+''''+','+
'N'''+CONVERT(NVARCHAR,DEFAULT_USEFUL_LIFE)+''''+','+
'N'''+CONVERT(NVARCHAR,PARENT_ID)+''''+','+
'N'''+CONVERT(NVARCHAR,IS_ACTIVE)+'''' 
FROM TBL_FIX_ASSET_CATEGORY 
WHERE IS_ACTIVE=1;
*/
--IF NOT EXISTS(SELECT * FROM TBL_FIX_ASSET_CATEGORY) 
--BEGIN
--	SET IDENTITY_INSERT TBL_FIX_ASSET_CATEGORY ON;
--	INSERT INTO TBL_FIX_ASSET_CATEGORY(CATEGORY_ID,CATEGORY_CODE,CATEGORY_NAME,NOTE,ACCOUNT_ITEM_ID,DEFAULT_USEFUL_LIFE,PARENT_ID,IS_ACTIVE)
--	SELECT N'1',N'I',N'ដី',N'',N'49',N'20',N'0',N'1'
--	UNION ALL SELECT N'2',N'II',N'កែលម្អដី',N'',N'49',N'10',N'0',N'1'
--	UNION ALL SELECT N'6',N'V-1-1',N'ខ្សែបណ្ដាញតង់ស្យុងមធ្យម (ប្រភេ',N'ផ្នែកតង់ស្យុងមធ្យម  ',N'49',N'27',N'27',N'1'
--	UNION ALL SELECT N'7',N'V-1-2',N'បង្គោលតង់ស្យុងមធ្យម (កំពស់, ប្',N'ផ្នែកតង់ស្យុងមធ្យម',N'49',N'27',N'27',N'1'
--	UNION ALL SELECT N'8',N'V-1-3',N'ត្រង់ស្វូរម៉ាទ័រ និងឧបករណ៍ពាក់',N'',N'49',N'27',N'27',N'1'
--	UNION ALL SELECT N'9',N'V-1-4',N'ឧបករណ៍ភ្ជាប់-ផ្តាច់ចរន្ត',N'',N'49',N'27',N'27',N'1'
--	UNION ALL SELECT N'10',N'V-2-1',N'ខ្សែបណ្ដាញតង់ស្យុងទាប (ប្រភេទ,',N'ផ្នែកតង់ស្យុងទាប   ',N'49',N'28',N'28',N'1'
--	UNION ALL SELECT N'11',N'V-2-2',N'បង្គោលតង់ស្យុងទាប (កំពស់, ប្រភ',N'ផ្នែកតង់ស្យុងទាប   ',N'49',N'28',N'28',N'1'
--	UNION ALL SELECT N'12',N'V-2-3',N'បរិក្ខារភ្ជាប់ចរន្ត (ប្រអប់ៈ ក',N'',N'1',N'28',N'28',N'1'
--	UNION ALL SELECT N'13',N'VI-1',N'សម្ភារៈការិយាល័យ និងគ្រឿងសង្ហា',N'មធ្យោបាយធ្វើការងារ  ',N'49',N'5',N'22',N'1'
--	UNION ALL SELECT N'14',N'VI-2',N'មធ្យោបាយដឹកជញ្ជូន',N'មធ្យោបាយធ្វើការងារ  ',N'49',N'5',N'22',N'1'
--	UNION ALL SELECT N'15',N'VI-3',N'ឧបករណ៍រោងជាង និងឧបករណ៍ការពារសុ',N'',N'49',N'5',N'22',N'1'
--	UNION ALL SELECT N'16',N'III',N'អគារ',N'',N'49',N'15',N'0',N'1'
--	UNION ALL SELECT N'20',N'IV',N'មធ្យបាយផលិតកម្មអគ្គិសនី',N'',N'49',N'15',N'0',N'1'
--	UNION ALL SELECT N'21',N'V',N'មធ្យោបាយចែកចាយអគ្គិសនី',N'',N'49',N'15',N'0',N'1'
--	UNION ALL SELECT N'22',N'VI',N'មធ្យោបាយធ្វើការងារ',N'',N'49',N'15',N'0',N'1'
--	UNION ALL SELECT N'23',N'IV-1',N'ម៉ាស៊ីនភ្លើង',N'',N'49',N'15',N'20',N'1'
--	UNION ALL SELECT N'26',N'IV-2',N'សម្ភារៈរួមផ្សំផ្នែកផលិតកម្ម',N'',N'49',N'15',N'20',N'1'
--	UNION ALL SELECT N'27',N'V-1',N'ផ្នែកតង់ស្យុងមធ្យម',N'',N'21',N'21',N'21',N'1'
--	UNION ALL SELECT N'28',N'V-2',N'ផ្នែកតង់ស្យុងទាប',N'',N'49',N'21',N'21',N'1'
--	SET IDENTITY_INSERT TBL_FIX_ASSET_CATEGORY OFF;
--END
GO
IF OBJECT_ID('REPORT_FIX_ASSET_SUMMARY') IS NOT NULL
	DROP PROC REPORT_FIX_ASSET_SUMMARY;
GO
CREATE PROC REPORT_FIX_ASSET_SUMMARY
AS
-- POPULATE CATEGORY TREE
WITH TMP(ID,CODE,NAME,PATH,LEVEL)
As
(
   SELECT ID = CATEGORY_ID, CODE = CATEGORY_CODE, NAME = CATEGORY_NAME,
		  PATH = CONVERT(NVARCHAR(100),RIGHT('000'+CONVERT(NVARCHAR,CATEGORY_ID),3)), LEVEL  = 1 
	FROM TBL_FIX_ASSET_CATEGORY e
	WHERE  ISNULL(PARENT_ID,0) = 0 
  UNION ALL 
  SELECT e.CATEGORY_ID, e.CATEGORY_CODE, e.CATEGORY_NAME, CONVERT(NVARCHAR(100),t.PATH + RIGHT('000'+CONVERT(NVARCHAR,CATEGORY_ID),3)),
		 t.LEVEL+1
  FROM TBL_FIX_ASSET_CATEGORY e
    INNER JOIN TMP t ON e.PARENT_ID=t.ID
  WHERE t.LEVEL<100 -- avoid infinite.
)
SELECT * 
INTO #CATEGORY_TREE
FROM TMP;


SELECT	PATH = PATH+'000',
		LEVEL,
		TYPE = 1, -- GROUP HEADER
		ID,
		CODE,
		NAME, 
		AMOUNT = 0.0
FROM #CATEGORY_TREE 
UNION ALL
SELECT	PATH = PATH+'999',
		LEVEL,
		TYPE = 2, -- GROUP FOOTER
		ID,
		CODE,
		NAME, 
		AMOUNT = 0.0
FROM #CATEGORY_TREE
UNION ALL 
SELECT	PATH = c.PATH+RIGHT('000'+CONVERT(NVARCHAR,FIX_ASSET_ITEM_ID),3),
		LEVEL = 0,
		TYPE = 0,
		FIX_ASSET_ITEM_ID,
		CODE = '',
		FIX_ASSET_NAME,
		AMOUNT = TOTAL_COST
FROM TBL_FIX_ASSET_ITEM i
INNER JOIN #CATEGORY_TREE c ON c.ID = i.CATEGORY_ID;
GO
IF OBJECT_ID('TLKP_ACCOUNT_POST_TYPE') IS NULL
	CREATE TABLE TLKP_ACCOUNT_POST_TYPE(
		POST_TYPE_ID INT PRIMARY KEY,
		POST_TYPE_NAME NVARCHAR(50) NOT NULL
	)
GO
IF NOT EXISTS(SELECT * FROM TLKP_ACCOUNT_POST_TYPE)
	INSERT INTO TLKP_ACCOUNT_POST_TYPE
	SELECT 1,N'ឥណពន្ធ'
	UNION ALL SELECT 2,N'ឥណទាន'
GO 
IF OBJECT_ID('[TBL_ACCOUNT_CHART]') IS NULL
	CREATE TABLE [dbo].[TBL_ACCOUNT_CHART](
		[ACCOUNT_ID] [int] IDENTITY(1,1) PRIMARY KEY,
		[ACCOUNT_CODE] [nvarchar](50) COLLATE Latin1_General_BIN NOT NULL,
		[ACCOUNT_NAME] [nvarchar](250) COLLATE Latin1_General_BIN NOT NULL,
		[ACCOUNT_NAME_EN] [nvarchar](250) COLLATE Latin1_General_BIN NOT NULL,
		[NOTE] [nvarchar](max) COLLATE Latin1_General_BIN NOT NULL,
		[POST_TYPE_ID] [int] NOT NULL,
		[PARENT_ID] [int] NOT NULL,
		[IS_ACTIVE] [bit] NOT NULL
	)
GO
ALTER TABLE TBL_ACCOUNT_CONFIG ALTER COLUMN CONFIG_VALUE NVARCHAR(MAX) NOT NULL;
GO
IF NOT EXISTS(SELECT * FROM TBL_ACCOUNT_CONFIG WHERE CONFIG_ID = 3 ) 
	INSERT INTO TBL_ACCOUNT_CONFIG SELECT 3,'FIX_ASSET_ASSET_ACCOUNTS','40,41,42,43,44,45,46';
IF NOT EXISTS(SELECT * FROM TBL_ACCOUNT_CONFIG WHERE CONFIG_ID = 4 ) 
	INSERT INTO TBL_ACCOUNT_CONFIG SELECT 4,'FIX_ASSET_ACCUMULATED_DEPRECIATION_ACCOUNTS','48,49,50,51,52';
IF NOT EXISTS(SELECT * FROM TBL_ACCOUNT_CONFIG WHERE CONFIG_ID = 5 ) 
	INSERT INTO TBL_ACCOUNT_CONFIG SELECT 5,'FIX_ASSET_DEPRECIATION_EXPENSE_ACCOUNTS','176,177,178,179,180';
IF NOT EXISTS(SELECT * FROM TBL_ACCOUNT_CONFIG WHERE CONFIG_ID = 6 ) 
	INSERT INTO TBL_ACCOUNT_CONFIG SELECT 6,'CASH_ACCOUNTS','72,73,74';
IF NOT EXISTS(SELECT * FROM TBL_ACCOUNT_CONFIG WHERE CONFIG_ID = 6 ) 
	INSERT INTO TBL_ACCOUNT_CONFIG SELECT 6,'PAYMENT_ACCOUNTS','72,73,74';
IF NOT EXISTS(SELECT * FROM TBL_ACCOUNT_CONFIG WHERE CONFIG_ID = 7 ) 
	INSERT INTO TBL_ACCOUNT_CONFIG SELECT 7,'EXPENSE_TRANS_ACCOUNTS','7,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,137,138,139,140,141,142,143,144,145,146,147,148,149,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,';
IF NOT EXISTS(SELECT * FROM TBL_ACCOUNT_CONFIG WHERE CONFIG_ID = 8 ) 
	INSERT INTO TBL_ACCOUNT_CONFIG SELECT 8,'INCOME_TRANS_ACCOUNTS',',6,15,16,17,18,19,120,121,122,123,124,125,126,127,128,129,130,131,132,133,135,136,150,151';
IF NOT EXISTS(SELECT * FROM TBL_ACCOUNT_CONFIG WHERE CONFIG_ID = 6 ) 
	INSERT INTO TBL_ACCOUNT_CONFIG SELECT 6,'PAYMENT_DEFAULT_ACCOUNTS','72';
GO
UPDATE TBL_ACCOUNT_CONFIG SET CONFIG_VALUE='120,121,122,123,124,125,126,127'
WHERE CONFIG_ID= 1
UPDATE TBL_ACCOUNT_CONFIG SET CONFIG_VALUE='120,121,122,123,124,125,126,127,128,129,130,131,132'
WHERE CONFIG_ID= 2
GO
IF OBJECT_ID('TLKP_DEPRECIATION_STATUS')IS NULL
	CREATE TABLE TLKP_DEPRECIATION_STATUS
	(
		STATUS_ID INT PRIMARY KEY ,
		STATUS_NAME NVARCHAR(50) NOT NULL
	)
GO

IF NOT EXISTS(SELECT * FROM TLKP_DEPRECIATION_STATUS)
    INSERT INTO TLKP_DEPRECIATION_STATUS
    SELECT 1,N'បានរំលស់'
    UNION ALL
    SELECT 2,N'មិនទាន់បានរំលស់'
GO

IF OBJECT_ID('TLKP_FIX_ASSET_STATUS')IS NULL
	CREATE TABLE TLKP_FIX_ASSET_STATUS
	(
		STATUS_ID INT PRIMARY KEY ,
		STATUS_NAME NVARCHAR(50) NOT NULL
	)
GO

IF NOT EXISTS(SELECT * FROM TLKP_FIX_ASSET_STATUS)
    INSERT INTO TLKP_FIX_ASSET_STATUS
    SELECT 1,N'កំពុងប្រើប្រាស់'
    UNION ALL
    SELECT 2,N'ឈប់ប្រើប្រាស់'
GO
IF OBJECT_ID('TBL_DEPRECIATION')IS NULL
	CREATE TABLE TBL_DEPRECIATION
	(
		DEPRECIATION_ID INT PRIMARY KEY IDENTITY(1,1),
		DEPRECIATION_MONTH DATETIME NOT NULL,
        FIX_ASSET_ITEM_ID INT NOT NULL,
		CURRENCY_ID INT NOT NULL,
		AMOUNT DECIMAL(18,2) NOT NULL,
        STATUS_ID INT NOT NULL,
        DEPRECIATION_DATE DATETIME NOT NULL,
        DEPRECIATION_BY NVARCHAR(50),
		CREATE_DATE DATETIME NOT NULL,
		CREATE_BY NVARCHAR(50) NOT NULL,
		IS_ACTIVE BIT NOT NULL 
	)
GO

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name='IX_TBL_DEPRECIATION_FIX_ASSET_ITEM_ID')
	CREATE INDEX IX_TBL_DEPRECIATION_FIX_ASSET_ITEM_ID ON TBL_DEPRECIATION(FIX_ASSET_ITEM_ID);
GO

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name='IX_TBL_DEPRECIATION_STATUS_ID')
	CREATE INDEX IX_TBL_DEPRECIATION_STATUS_ID ON TBL_DEPRECIATION(STATUS_ID);
GO

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name='IX_TBL_DEPRECIATION_CURRENCY_ID')
	CREATE INDEX IX_TBL_DEPRECIATION_CURRENCY_ID ON TBL_DEPRECIATION(CURRENCY_ID);
GO


IF OBJECT_ID('GENERATE_DEPRECIATION')IS NOT NULL
	DROP PROC GENERATE_DEPRECIATION
GO

CREATE PROC GENERATE_DEPRECIATION
	@FIX_ASSET_ITEM_ID INT=1,
	@CREATE_BY NVARCHAR(50)='admin'
AS

-- DERPRECIATION STATUS 1 Already Depreciation, 2 Not Depreciation

-- UPDATE OLD DATE
UPDATE TBL_DEPRECIATION SET IS_ACTIVE=0 WHERE FIX_ASSET_ITEM_ID=@FIX_ASSET_ITEM_ID;

-- DECLARE PARAMETER & SET VALUE
DECLARE @AMOUNT DECIMAL(18,2),
		@MONTH DATETIME,
		@CURRENCY_ID INT,
		@CREATE_DATE DATETIME,
		@ANN_RATE DECIMAL(18,2),
		@TOTAL_COST DECIMAL(18,2),
		@USEFUL_LIFE INT

SELECT @TOTAL_COST=TOTAL_COST
		,@MONTH=CONVERT(NVARCHAR(7),USE_DATE,126)+'-01'
		,@CURRENCY_ID=CURRENCY_ID
		,@USEFUL_LIFE=USEFUL_LIFE
		,@ANN_RATE=ANNUAL_DEPRECIATION_RATE
FROM TBL_FIX_ASSET_ITEM
WHERE FIX_ASSET_ITEM_ID=@FIX_ASSET_ITEM_ID

DECLARE @I INT;
SET @I=0;
SET @AMOUNT=(@TOTAL_COST*(@ANN_RATE/100))/12
WHILE @I<(@USEFUL_LIFE*12)
BEGIN
	IF (@I=(@USEFUL_LIFE*12)-1)
	BEGIN
		SET @AMOUNT=dbo.ROUND_BY_CURRENCY((@TOTAL_COST- ISNULL((SELECT SUM(AMOUNT) FROM TBL_DEPRECIATION WHERE FIX_ASSET_ITEM_ID=@FIX_ASSET_ITEM_ID AND IS_ACTIVE=1),0)),@CURRENCY_ID)
	END
	INSERT INTO TBL_DEPRECIATION (DEPRECIATION_MONTH,FIX_ASSET_ITEM_ID,CURRENCY_ID,AMOUNT,STATUS_ID,DEPRECIATION_DATE,DEPRECIATION_BY,CREATE_DATE,CREATE_BY,IS_ACTIVE)
	SELECT DATEADD(MONTH,@I,@MONTH),@FIX_ASSET_ITEM_ID,@CURRENCY_ID,dbo.ROUND_BY_CURRENCY(@AMOUNT,@CURRENCY_ID),2,GETDATE(),'',GETDATE(),@CREATE_BY,1
	SET @I=@I+1
END
GO

IF OBJECT_ID('REPORT_FIX_ASSET_SUMMARY') IS NOT NULL
	DROP PROC REPORT_FIX_ASSET_SUMMARY;
GO
CREATE PROC REPORT_FIX_ASSET_SUMMARY
AS
-- POPULATE CATEGORY TREE
WITH TMP(ID,CODE,NAME,PATH,LEVEL)
As
(
   SELECT ID = CATEGORY_ID, CODE = CATEGORY_CODE, NAME = CATEGORY_NAME,
		  PATH = CONVERT(NVARCHAR(100),RIGHT('000'+CONVERT(NVARCHAR,CATEGORY_ID),3)), LEVEL  = 1 
	FROM TBL_FIX_ASSET_CATEGORY e
	WHERE  ISNULL(PARENT_ID,0) = 0 
  UNION ALL 
  SELECT e.CATEGORY_ID, e.CATEGORY_CODE, e.CATEGORY_NAME, CONVERT(NVARCHAR(100),t.PATH + RIGHT('000'+CONVERT(NVARCHAR,CATEGORY_ID),3)),
		 t.LEVEL+1
  FROM TBL_FIX_ASSET_CATEGORY e
    INNER JOIN TMP t ON e.PARENT_ID=t.ID
  WHERE t.LEVEL<100 -- avoid infinite.
)
SELECT * 
INTO #CATEGORY_TREE
FROM TMP;


SELECT	PATH = PATH+'000',
		LEVEL,
		TYPE = 1, -- GROUP HEADER
		ID,
		CODE,
		NAME, 
		AMOUNT = 0.0
FROM #CATEGORY_TREE 
UNION ALL
SELECT	PATH = PATH+'999',
		LEVEL,
		TYPE = 2, -- GROUP FOOTER
		ID,
		CODE,
		NAME, 
		AMOUNT = 0.0
FROM #CATEGORY_TREE 
UNION ALL 
SELECT	PATH = c.PATH+RIGHT('000'+CONVERT(NVARCHAR,FIX_ASSET_ITEM_ID),3),
		LEVEL = 0,
		TYPE = 0,
		FIX_ASSET_ITEM_ID,
		CODE = '',
		FIX_ASSET_NAME,
		AMOUNT = TOTAL_COST
FROM TBL_FIX_ASSET_ITEM i
INNER JOIN #CATEGORY_TREE c ON c.ID = i.CATEGORY_ID;
GO

/*
SELECT 'UNION ALL SELECT '+
'N'''+CONVERT(NVARCHAR(MAX),ACCOUNT_ID)+''''+','+
'N'''+CONVERT(NVARCHAR(MAX),ACCOUNT_CODE)+''''+','+
'N'''+CONVERT(NVARCHAR(MAX),ACCOUNT_NAME)+''''+','+
'N'''+CONVERT(NVARCHAR(MAX),ACCOUNT_NAME_EN)+''''+','+
'N'''+CONVERT(NVARCHAR(MAX),NOTE)+''''+','+
'N'''+CONVERT(NVARCHAR(MAX),PARENT_ID)+''''+','+
'N'''+CONVERT(NVARCHAR(MAX),POST_TYPE_ID)+''''+','+
'N'''+CONVERT(NVARCHAR(MAX),IS_ACTIVE)+'''' 
FROM TBL_ACCOUNT_CHART
WHERE IS_ACTIVE=1; 
*/
IF NOT EXISTS(SELECT * FROM TBL_ACCOUNT_CHART) 
BEGIN
SET IDENTITY_INSERT TBL_ACCOUNT_CHART ON;
INSERT INTO TBL_ACCOUNT_CHART(ACCOUNT_ID,ACCOUNT_CODE,ACCOUNT_NAME,ACCOUNT_NAME_EN,NOTE,PARENT_ID,POST_TYPE_ID,IS_ACTIVE)
SELECT N'3',N'1000',N'ទ្រព្យ',N'Asset',N'',N'0',N'1',N'1'
UNION ALL SELECT N'4',N'2000',N'មូលធន និងបំណុល',N'Equity',N'',N'0',N'2',N'1'
UNION ALL SELECT N'5',N'3000',N'បំណុល',N'Liability',N'',N'0',N'2',N'1'
UNION ALL SELECT N'6',N'4000',N'ចំណូល',N'Income',N'',N'0',N'2',N'1'
UNION ALL SELECT N'7',N'5000',N'ចំណាយ',N'Expense',N'',N'0',N'1',N'1'
UNION ALL SELECT N'8',N'1000',N'អចលនទ្រព្យ',N'Fixed Asset',N'',N'3',N'1',N'1'
UNION ALL SELECT N'9',N'1500',N'ចលនទ្រព្យ',N'Virable Asset',N'',N'3',N'1',N'1'
UNION ALL SELECT N'10',N'2000',N'មូលធន',N'',N'  ',N'4',N'2',N'1'
UNION ALL SELECT N'11',N'3000',N'បំណុលរយៈពេលវែង',N'Long-term Liability',N'',N'5',N'2',N'1'
UNION ALL SELECT N'12',N'3500',N'បំណុលរយៈពេលខ្លី',N'Short-term liablity',N'',N'5',N'2',N'1'
UNION ALL SELECT N'13',N'3800',N'ចំណូលដែលបានទទួលមុន',N'',N'',N'5',N'2',N'1'
UNION ALL SELECT N'14',N'3900',N'សំវិធានធនអាជីវកម្ម',N'',N'',N'5',N'2',N'1'
UNION ALL SELECT N'15',N'4000',N'ចំណូលអាជីវកម្ម',N'',N'',N'6',N'2',N'1'
UNION ALL SELECT N'16',N'4000',N'ចំណូលបានមកពីការលក់អគ្គិសនី',N'',N'',N'15',N'2',N'1'
UNION ALL SELECT N'17',N'4200',N'ចំណូលផ្សេងៗ',N'',N'',N'15',N'2',N'1'
UNION ALL SELECT N'18',N'4300',N'ចំណូលផ្សេងៗពីអាជីវកម្មអគ្គិសនី',N'',N'',N'15',N'2',N'1'
UNION ALL SELECT N'19',N'4400',N'ចំណូលផ្សេងៗទៀតពីអាជីវកម្មអគ្គិសនី',N'',N'',N'15',N'2',N'1'
UNION ALL SELECT N'20',N'5000',N'ចំណាយអាជីវកម្ម',N'',N'',N'7',N'1',N'1'
UNION ALL SELECT N'21',N'5000',N'ចំណាយទិញអគ្គិសនី',N'',N'',N'20',N'1',N'1'
UNION ALL SELECT N'22',N'5100',N'ផ្នែកផលិតកម្ម-ចំណាយដំណើរការ និងការថែទាំ',N'',N'',N'20',N'1',N'1'
UNION ALL SELECT N'23',N'5400',N'ផ្នែកបញ្ជូន-ចំណាយដំណើរការ និងការថែទាំ',N'',N'',N'20',N'1',N'1'
UNION ALL SELECT N'24',N'5600',N' ផ្នែកចែកចាយ-ចំណាយដំណើរការ និងការថែទាំ',N'',N'',N'20',N'1',N'1'
UNION ALL SELECT N'25',N'5800',N'ចំណាយលើគណនីអតិថិជន',N'',N'',N'20',N'1',N'1'
UNION ALL SELECT N'26',N'5900',N'រំលស់',N'',N'',N'20',N'1',N'1'
UNION ALL SELECT N'27',N'6000',N'ចំណាយរដ្ឋបាល និងចំណាយទូទៅ',N'',N'',N'20',N'1',N'1'
UNION ALL SELECT N'28',N'7000',N'ចំណាយហិរញ្ញវត្ថុ',N'',N'',N'7',N'1',N'1'
UNION ALL SELECT N'29',N'7100',N'ចំណូល/(ចំណាយ)ក្រៅពីប្រតិបត្តិការអាជីវកម្ម',N'',N'',N'7',N'1',N'1'
UNION ALL SELECT N'30',N'7000',N'ចំណាយការប្រាក់លើបំណុលរយៈពេលវែង',N'',N'',N'28',N'1',N'1'
UNION ALL SELECT N'31',N'7010',N'ចំណាយការប្រាក់លើការសាងសង់',N'',N'',N'28',N'1',N'1'
UNION ALL SELECT N'32',N'7020',N'ចំណាយផាកពិន័យលើប្រាក់កម្ចី',N'',N'',N'28',N'1',N'1'
UNION ALL SELECT N'33',N'7030',N'ចំណាយការប្រាក់ផ្សេងៗ',N'',N'',N'28',N'1',N'1'
UNION ALL SELECT N'34',N'7100',N'ចំណូលការប្រាក់',N'',N'',N'29',N'1',N'1'
UNION ALL SELECT N'35',N'7120',N'ចំណេញ/(ខាត)លើការលក់ទ្រព្យ និងបរិក្ខារ',N'',N'',N'29',N'1',N'1'
UNION ALL SELECT N'36',N'7140',N'ចំណូលក្រៅពីប្រតិបត្តិការអាជីវកម្ម',N'',N'',N'29',N'1',N'1'
UNION ALL SELECT N'37',N'7160',N'ចំណេញ/(ខាត)លើអត្រាប្តូរប្រាក់',N'',N'',N'29',N'1',N'1'
UNION ALL SELECT N'38',N'7190',N'ចំណាយក្រៅពីប្រតិបត្តិការអាជីវកម្ម',N'',N'',N'29',N'1',N'1'
UNION ALL SELECT N'39',N'1000',N'ទ្រព្យប្រើក្នុងសេវាកម្ម',N'',N'',N'8',N'1',N'1'
UNION ALL SELECT N'40',N'1005',N'ដី',N'',N'',N'39',N'1',N'1'
UNION ALL SELECT N'41',N'1010',N'កែលម្អដី',N'',N'',N'39',N'1',N'1'
UNION ALL SELECT N'42',N'1015',N'អគារ',N'',N'',N'39',N'1',N'1'
UNION ALL SELECT N'43',N'1020',N'មធ្យោបាយផលិតកម្មអគ្គិសនី',N'',N'',N'39',N'1',N'1'
UNION ALL SELECT N'44',N'1040',N'មធ្យោបាយផ្នែកបណ្តាញបញ្ជូនអគ្គិសនី',N'',N'',N'39',N'1',N'1'
UNION ALL SELECT N'45',N'1050',N'មធ្យោបាយចែកចាយអគ្គិសនី',N'',N'',N'39',N'1',N'1'
UNION ALL SELECT N'46',N'1080',N'មធ្យោបាយធ្វើការងារ',N'',N'',N'39',N'1',N'1'
UNION ALL SELECT N'48',N'1105',N'រំលស់បូកយោង-អគារ',N'',N'',N'47',N'1',N'1'
UNION ALL SELECT N'49',N'1110',N'រំលស់បូកយោង-មធ្យោបាយផលិតកម្មអគ្គិសនី',N'',N'',N'47',N'1',N'1'
UNION ALL SELECT N'50',N'1130',N'រំលស់បូកយោង-មធ្យោបាយផ្នែកបណ្តាញបញ្ជូន',N'',N'',N'47',N'1',N'1'
UNION ALL SELECT N'51',N'1150',N'រំលស់បូកយោង-មធ្យោបាយចែកចាយអគ្គិសនី',N'',N'',N'47',N'1',N'1'
UNION ALL SELECT N'52',N'1170',N'រំលស់បូកយោង-មធ្យោបាយធ្វើការងារ',N'',N'',N'47',N'1',N'1'
UNION ALL SELECT N'60',N'1250',N'ការវិនិយោគ',N'',N'',N'8',N'1',N'1'
UNION ALL SELECT N'61',N'1260',N'ការវិនិយោគមូលបត្រជាមួយក្រុមហ៊ុនពាក់ព័ន្ធ',N'',N'',N'60',N'1',N'1'
UNION ALL SELECT N'62',N'1270',N'បុរេប្រទានរយៈពេលវែងជាមួយក្រុមហ៊ុនពាក់ព័ន្ធ',N'',N'',N'60',N'1',N'1'
UNION ALL SELECT N'63',N'1290',N'ការវិនិយោគផ្សេងៗ',N'',N'',N'60',N'1',N'1'
UNION ALL SELECT N'65',N'1310',N'មូលនិធិកប់ក្នុងការវិនិយោគ',N'',N'',N'64',N'1',N'1'
UNION ALL SELECT N'66',N'1340',N'មូលនិធិពិសេសផ្សេងៗ',N'',N'',N'64',N'1',N'1'
UNION ALL SELECT N'68',N'1360',N'តម្លៃអង្គភាព (Organization)',N'',N'',N'67',N'1',N'1'
UNION ALL SELECT N'69',N'1390',N'ទ្រព្យអរូបីផ្សេងៗទៀត',N'',N'',N'67',N'1',N'1'
UNION ALL SELECT N'70',N'1400',N'អចលនទ្រព្យផ្សេងទៀត',N'',N'',N'8',N'1',N'1'
UNION ALL SELECT N'71',N'1500',N'សាច់ប្រាក់ និងមូលនិធិកំពុងចរាចរ',N'',N'',N'9',N'1',N'1'
UNION ALL SELECT N'72',N'1510',N'សាច់ប្រាក់ក្នុងកេះ',N'',N'',N'71',N'1',N'1'
UNION ALL SELECT N'73',N'1600',N'សាច់ប្រាក់ក្នុងធនាគារ',N'',N'',N'71',N'1',N'1'
UNION ALL SELECT N'74',N'1650',N'មូលនិធិកំពុងចរាចរ',N'',N'',N'71',N'1',N'1'
UNION ALL SELECT N'75',N'1700',N'ការវិនិយោគរយៈពេលខ្លី',N'',N'',N'9',N'1',N'1'
UNION ALL SELECT N'76',N'1720',N'ប្រាក់កក់ពិសេស',N'',N'',N'9',N'1',N'1'
UNION ALL SELECT N'77',N'1740',N'គណនីបំណុលត្រូវទារ-អតិថិជន',N'',N'',N'9',N'1',N'1'
UNION ALL SELECT N'78',N'1750',N'សំវិធានធនសម្រាប់បំណុលពិបាកទារ',N'',N'',N'9',N'1',N'1'
UNION ALL SELECT N'79',N'1760',N'បុរេប្រទានចំពោះមន្រ្តី-បុគ្គលិក',N'',N'',N'9',N'1',N'1'
UNION ALL SELECT N'80',N'1770',N'គណនីបំណុលត្រូវទារ-ផ្សេងៗ',N'',N'',N'9',N'1',N'1'
UNION ALL SELECT N'81',N'1780',N'ចំណូលត្រូវទទួល',N'',N'',N'9',N'1',N'1'
UNION ALL SELECT N'82',N'1790',N'កម្ចី និងប័ណ្ណត្រូវទារ',N'',N'',N'9',N'1',N'1'
UNION ALL SELECT N'83',N'1800',N'បំណុលត្រូវទារពីក្រុមហ៊ុនពាក់ព័ន្ធ',N'',N'',N'9',N'1',N'1'
UNION ALL SELECT N'84',N'1820',N'ការប្រាក់ និងភាគលាភត្រូវទទួល',N'',N'',N'9',N'1',N'1'
UNION ALL SELECT N'85',N'1840',N'សន្និធិ-វត្ថុធាតុដើម និងសម្ភារៈផ្គត់ផ្គង់',N'',N'',N'9',N'1',N'1'
UNION ALL SELECT N'86',N'1900',N'ប្រាក់បង់មុន',N'',N'',N'9',N'1',N'1'
UNION ALL SELECT N'87',N'1950',N'ចំណាយដែលបានបង់មុន',N'',N'',N'9',N'1',N'1'
UNION ALL SELECT N'88',N'1990',N'ចលនទ្រព្យផ្សេងៗ',N'',N'',N'9',N'1',N'1'
UNION ALL SELECT N'89',N'2000',N'មូលធនភាគហ៊ុន',N'',N'',N'10',N'2',N'1'
UNION ALL SELECT N'92',N'2100',N'គណនីឯកកម្មសិទ្ធ',N'',N'',N'4',N'2',N'1'
UNION ALL SELECT N'93',N'2150',N'ការឧបត្ថម្ភធនរបស់រដ្ឋាភិបាល',N'',N'',N'4',N'2',N'1'
UNION ALL SELECT N'94',N'2170',N'ជំនួយឧបត្ថម្ភសម្រាប់ការសាងសង់',N'',N'',N'4',N'2',N'1'
UNION ALL SELECT N'95',N'2200',N'ឧបត្ថម្ភធនផ្សេងៗ',N'',N'',N'4',N'2',N'1'
UNION ALL SELECT N'96',N'2300',N'ការវាយតម្លៃឡើងវិញ លើស/ខ្វះ',N'',N'',N'4',N'2',N'1'
UNION ALL SELECT N'97',N'2400',N'ប្រាក់ចំណេញរក្សាទុក',N'',N'',N'4',N'2',N'1'
UNION ALL SELECT N'101',N'3010',N'បំណុលត្រូវសងរយៈពេលវែង-បរទេស',N'',N'',N'11',N'2',N'1'
UNION ALL SELECT N'102',N'3100',N'បំណុលត្រូវសងរយៈពេលវែង-ក្រុមហ៊ុនពាក់ព័ន្ធ',N'',N'',N'11',N'2',N'1'
UNION ALL SELECT N'103',N'3150',N'ប្រាក់កក់របស់អតិថិជន',N'',N'',N'11',N'2',N'1'
UNION ALL SELECT N'104',N'3200',N'បំណុលត្រូវសងរយៈពេលវែង-ផ្សេងៗ',N'',N'',N'11',N'2',N'1'
UNION ALL SELECT N'105',N'3500',N'បំណុលត្រូវសង',N'',N'',N'12',N'2',N'1'
UNION ALL SELECT N'106',N'3600',N'កម្ចីរយៈពេលខ្លី',N'',N'',N'12',N'2',N'1'
UNION ALL SELECT N'107',N'3620',N'ភាគខ្លះនៃបំណុលរយៈពេលវែងត្រូវសង',N'',N'',N'12',N'2',N'1'
UNION ALL SELECT N'108',N'3640',N'ការប្រាក់ត្រូវសងលើការខ្ចីបុល',N'',N'',N'12',N'2',N'1'
UNION ALL SELECT N'109',N'3660',N'បំណុលត្រូវសងទៅក្រុមហ៊ុនពាក់ព័ន្ធ',N'',N'',N'12',N'2',N'1'
UNION ALL SELECT N'110',N'3680',N'ប្រាក់ខែត្រូវបើក',N'',N'',N'12',N'2',N'1'
UNION ALL SELECT N'111',N'3700',N'ពន្ធកាត់ទុកត្រូវបង់',N'',N'',N'12',N'2',N'1'
UNION ALL SELECT N'112',N'3740',N'ពន្ធផ្សេងៗទៀតត្រូវបង់',N'',N'',N'12',N'2',N'1'
UNION ALL SELECT N'113',N'3790',N'បំណុលផ្សេងៗទៀតត្រូវសង',N'',N'',N'12',N'2',N'1'
UNION ALL SELECT N'114',N'3800',N'ការបង់មុនរបស់អតិថិជនសម្រាប់ការសាងសង់',N'',N'',N'13',N'2',N'1'
UNION ALL SELECT N'115',N'3850',N'ចំណូលដែលបានទទួលមុនផ្សេងៗ',N'',N'',N'13',N'2',N'1'
UNION ALL SELECT N'116',N'3900',N'សំវិធានធនសម្រាប់ធានារ៉ាប់រងទ្រព្យសម្បត្តិ',N'',N'',N'14',N'2',N'1'
UNION ALL SELECT N'117',N'3920',N'សំវិធានធនសម្រាប់គ្រោះថ្នាក់ការងារ និងការខូចខាត',N'',N'',N'14',N'2',N'1'
UNION ALL SELECT N'118',N'3940',N'សំវិធានធនសម្រាប់សោធន និងអត្ថប្រយោជន៍',N'',N'',N'14',N'2',N'1'
UNION ALL SELECT N'119',N'3950',N'សំវិធានធនអាជីវកម្មផ្សេងៗ',N'',N'',N'14',N'2',N'1'
UNION ALL SELECT N'120',N'4010',N'លំនៅដ្ឋាន',N'',N'',N'16',N'2',N'1'
UNION ALL SELECT N'121',N'4020',N'លំនៅដ្ឋានជនបរទេស',N'',N'',N'16',N'2',N'1'
UNION ALL SELECT N'122',N'4030',N'ពាណិជ្ជកម្ម/អាជីវកម្ម',N'',N'',N'16',N'2',N'1'
UNION ALL SELECT N'123',N'4040',N'ឧស្សាហកម្ម/សិប្បកម្ម',N'',N'',N'16',N'2',N'1'
UNION ALL SELECT N'124',N'4050',N'ស្ថាប័នរដ្ឋាភិបាល',N'',N'',N'16',N'2',N'1'
UNION ALL SELECT N'125',N'4060',N'បំភ្លឺតាមផ្លូវសាធារណៈ',N'',N'',N'16',N'2',N'1'
UNION ALL SELECT N'126',N'4070',N'លក់តាមរយៈនាឡិកាស្ទង់ខូច',N'',N'',N'16',N'2',N'1'
UNION ALL SELECT N'127',N'4090',N'ផ្សេងៗ',N'',N'',N'16',N'2',N'1'
UNION ALL SELECT N'128',N'4310',N'ចំណូលពីការភ្ជាប់ចរន្ត/ផ្តាច់ចរន្ត',N'',N'',N'18',N'2',N'1'
UNION ALL SELECT N'129',N'4320',N'ចំណូលពីប្រាក់កក់របស់អតិថិជន',N'',N'',N'18',N'2',N'1'
UNION ALL SELECT N'130',N'4330',N'ចំណូលពីការថ្លឹង/ប្តូរនាឡិការស្ទង់',N'',N'',N'18',N'2',N'1'
UNION ALL SELECT N'131',N'4340',N'ចំណូលពីការផាកពិន័យលើការប្រើប្រាស់អប្បបរិមា',N'',N'',N'18',N'2',N'1'
UNION ALL SELECT N'132',N'4350',N'សេវារដ្ឋបាល និងសេវាផ្សេងៗ',N'',N'',N'18',N'2',N'1'
UNION ALL SELECT N'133',N'4010',N'ចំណូលផាកពិន័យ',N'',N'',N'19',N'2',N'1'
UNION ALL SELECT N'135',N'4420',N'ចំណូលផាកពិន័យពីការបង់យឺត',N'',N'',N'19',N'2',N'1'
UNION ALL SELECT N'136',N'4430',N'ចំណូលពីការជួលទ្រព្យអាជីវកម្ម',N'',N'',N'19',N'2',N'1'
UNION ALL SELECT N'137',N'5020',N'ចំណាយលើអានុភាព ',N'',N'',N'21',N'1',N'1'
UNION ALL SELECT N'138',N'5110',N'បន្ទុកប្រេងឥន្ធនៈ',N'',N'',N'22',N'1',N'1'
UNION ALL SELECT N'139',N'5120',N'បន្ទុកប្រេងរំអិល និងការប្រើប្រាស់ផ្សេងៗ',N'',N'',N'22',N'1',N'1'
UNION ALL SELECT N'140',N'5130',N'ទឹក',N'',N'',N'22',N'1',N'1'
UNION ALL SELECT N'141',N'5200',N'ចំណាយគ្រប់គ្រង និងវិស្វកម្ម',N'',N'',N'22',N'1',N'1'
UNION ALL SELECT N'142',N'5210',N'ធានារ៉ាប់រង',N'',N'',N'22',N'1',N'1'
UNION ALL SELECT N'143',N'5220',N'ជួល',N'',N'',N'22',N'1',N'1'
UNION ALL SELECT N'144',N'5230',N'ជួសជុល និងថែទាំម៉ាស៊ីនផលិត',N'',N'',N'22',N'1',N'1'
UNION ALL SELECT N'145',N'5240',N'គ្រឿងបន្លាស់',N'',N'',N'22',N'1',N'1'
UNION ALL SELECT N'146',N'5250',N'សម្ភារៈ និងការផ្គត់ផ្គង់',N'',N'',N'22',N'1',N'1'
UNION ALL SELECT N'147',N'5290',N'ចំណាយផ្សេងៗផ្នែកផលិតកម្ម',N'',N'',N'22',N'1',N'1'
UNION ALL SELECT N'148',N'5420',N'ចំណាយផ្នែកបែងចែកបន្ទុក',N'',N'',N'23',N'1',N'1'
UNION ALL SELECT N'149',N'5430',N'ចំណាយផ្នែកបន្ទប់ភ្លើង',N'',N'',N'23',N'1',N'1'
UNION ALL SELECT N'151',N'4490',N'ចំណូលផ្សេងៗ',N'',N'',N'19',N'2',N'1'
UNION ALL SELECT N'152',N'5010',N'ចំណាយទិញអគ្គិសនី',N'',N'',N'21',N'1',N'1'
UNION ALL SELECT N'153',N'5410',N'ចំណាយគ្រប់គ្រង និងវិស្វកម្ម',N'',N'',N'23',N'1',N'1'
UNION ALL SELECT N'154',N'5435',N'គ្រឿងបន្លាស់',N'',N'',N'23',N'1',N'1'
UNION ALL SELECT N'155',N'5440',N'ជួសជុល និងថែទាំបណ្តាញ',N'',N'',N'23',N'1',N'1'
UNION ALL SELECT N'156',N'5450',N'សម្ភារៈ និងការផ្គត់ផ្គង់',N'',N'',N'23',N'1',N'1'
UNION ALL SELECT N'157',N'5460',N'ជួល',N'',N'',N'23',N'1',N'1'
UNION ALL SELECT N'158',N'5490',N'ចំណាយផ្សេងៗលើផ្នែកបញ្ជូន',N'',N'',N'23',N'1',N'1'
UNION ALL SELECT N'159',N'5610',N'ចំណាយគ្រប់គ្រង និងវិស្វកម្ម',N'',N'',N'24',N'1',N'1'
UNION ALL SELECT N'160',N'5620',N'ចំណាយផ្នែកបែងចែកបន្ទុក',N'',N'',N'24',N'1',N'1'
UNION ALL SELECT N'161',N'5630',N'ចំណាយផ្នែកបន្ទប់ភ្លើង',N'',N'',N'24',N'1',N'1'
UNION ALL SELECT N'162',N'5635',N'គ្រឿងបន្លាស់',N'',N'',N'24',N'1',N'1'
UNION ALL SELECT N'163',N'5640',N'ជួសជុល និងថែទាំបណ្តាញ',N'',N'',N'24',N'1',N'1'
UNION ALL SELECT N'164',N'5645',N'សម្ភារៈ និងការផ្គត់ផ្គង់',N'',N'',N'24',N'1',N'1'
UNION ALL SELECT N'165',N'5650',N'ចំណាយបំភ្លឺសាធារណៈ និងប្រព័ន្ធឱ្យសញ្ញាផ្សេងៗ',N'',N'',N'24',N'1',N'1'
UNION ALL SELECT N'166',N'5660',N'ចំណាយលើការថែទាំនាឡិការស្ទង់',N'',N'',N'24',N'1',N'1'
UNION ALL SELECT N'167',N'5670',N'ចំណាយលើការតម្លើងជូនអតិថិជន',N'',N'',N'24',N'1',N'1'
UNION ALL SELECT N'168',N'5680',N'ជួល',N'',N'',N'24',N'1',N'1'
UNION ALL SELECT N'169',N'5690',N'ចំណាយផ្សេងៗលើផ្នែកចែកចាយ',N'',N'',N'24',N'1',N'1'
UNION ALL SELECT N'170',N'5810',N'ចំណាយគ្រប់គ្រង',N'',N'',N'25',N'1',N'1'
UNION ALL SELECT N'171',N'5820',N'ចំណាយលើព័ត៌មាន និងសេវាអតិថិជន',N'',N'',N'25',N'1',N'1'
UNION ALL SELECT N'172',N'5830',N'ចំណាយលើការស្រង់អំណាន និងការប្រមូលប្រាក់',N'',N'',N'25',N'1',N'1'
UNION ALL SELECT N'173',N'5840',N'បំណុលទារមិនបាន',N'',N'',N'25',N'1',N'1'
UNION ALL SELECT N'174',N'5850',N'កម្រៃជើងសារ',N'',N'',N'25',N'1',N'1'
UNION ALL SELECT N'175',N'5890',N'ចំណាយផ្សេងៗទៀតលើគណនីអតិថិជន',N'',N'',N'25',N'1',N'1'
UNION ALL SELECT N'176',N'5910',N'អគារ',N'',N'',N'26',N'1',N'1'
UNION ALL SELECT N'177',N'5920',N'រំលស់ផ្នែកមធ្យោបាយផលិតកម្មអគ្គិសនី',N'',N'',N'26',N'1',N'1'
UNION ALL SELECT N'178',N'5940',N'រំលស់ផ្នែកបណ្តាញបញ្ជូនអគ្គិសនី',N'',N'',N'26',N'1',N'1'
UNION ALL SELECT N'179',N'5960',N'រំលស់ផ្នែកមធ្យោបាយចែកចាយអគ្គិសនី',N'',N'',N'26',N'1',N'1'
UNION ALL SELECT N'180',N'5980',N'រំលស់ផ្នែកមធ្យោបាយធ្វើការងារ',N'',N'',N'26',N'1',N'1'
UNION ALL SELECT N'184',N'5951',N'ខ្សែបណ្តាញតង់ស្យុងមធ្យម',N'',N'',N'183',N'1',N'1'
UNION ALL SELECT N'185',N'5953',N'បង្គោលតង់ស្យុងមធ្យម',N'',N'',N'183',N'1',N'1'
UNION ALL SELECT N'186',N'5955',N'ត្រង់ស្វូរម៉ាទ័រ',N'',N'',N'183',N'1',N'1'
UNION ALL SELECT N'187',N'5957',N'បរិក្ខាររួមផ្សំតង់ស្យុងមធ្យម',N'',N'',N'183',N'1',N'1'
UNION ALL SELECT N'188',N'5959',N'ខ្សែបណ្តាញតង់ស្យុងទាប',N'',N'',N'183',N'1',N'1'
UNION ALL SELECT N'189',N'5961',N'បង្គោលតង់ស្យុងទាប',N'',N'',N'183',N'1',N'1'
UNION ALL SELECT N'190',N'5963',N'ប្រអប់នាឡិកាស្ទង់',N'',N'',N'183',N'1',N'1'
UNION ALL SELECT N'191',N'5965',N'បរិក្ខាររួមផ្សំតង់ស្យុងទាប',N'',N'',N'183',N'1',N'1'
UNION ALL SELECT N'192',N'5967',N'បរិក្ខារផ្គត់ផ្គង់ឱ្យអ្នកប្រើប្រាស់',N'',N'',N'183',N'1',N'1'
UNION ALL SELECT N'194',N'5975',N'អគារការិយាល័យបង់ប្រាក់',N'',N'',N'193',N'1',N'1'
UNION ALL SELECT N'195',N'5980',N'សម្ភារៈការិយាល័យ',N'',N'',N'193',N'1',N'1'
UNION ALL SELECT N'196',N'5985',N'មធ្យោបាយដឹកជញ្ជូន',N'',N'',N'193',N'1',N'1'
UNION ALL SELECT N'197',N'5990',N'ឧបករណ៍រោងជាង និងឧបករណ៍ការពារសុវត្ថិភាព',N'',N'',N'193',N'1',N'1'
UNION ALL SELECT N'198',N'5995',N'អចនទ្រព្យផ្សេងទៀត',N'',N'',N'193',N'1',N'1'
UNION ALL SELECT N'199',N'6010',N'ប្រាក់ខែទូទៅ',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'200',N'6020',N'ប្រាក់ធ្វើការលើសម៉ោង និងឧបត្ថម្ភផ្សេងៗ',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'201',N'6030',N'ចំណាយលើបុគ្គលិកបណ្តែត',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'202',N'6040',N'បន្ទុកសន្តិសុខសង្គម',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'203',N'6050',N'បុព្វលាភ និងរង្វាន់លើកទឹកចិត្ត',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'204',N'6060',N'ប្រាក់ឧបត្ថម្ភម្ហូមអាហារ និងអត្ថប្រយោជន៍ផ្សេងៗ',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'205',N'6070',N'ប្រាក់ឧបត្ថម្ភផ្សេងៗទៀត',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'207',N'6100',N'បណ្តុះបណ្តាល និងអភិវឌ្ឍន៍បុគ្គលិក',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'208',N'6110',N'បេសកកម្មក្រៅប្រទេស',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'209',N'6120',N'បេសកកម្មក្នុងស្រុក និងការទទួលភ្ញៀវ',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'210',N'6130',N'ការដឹកជញ្ជូន និងការធ្វើដំណើរក្នុងស្រុក',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'211',N'6140',N'ចំណាយលើប្រេងឥន្ធនៈបុគ្គលិក',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'212',N'6150',N'ចំណាយសម្ភារៈការិយាល័យទូទៅ',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'213',N'6160',N'សេវាប្រៃសណីយ៍ និងទូរគមនាគមន៍',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'214',N'6200',N'ពន្ធ',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'219',N'6300',N'ជួល',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'220',N'6310',N'ទឹក-ភ្លើង',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'221',N'6320',N'ជួសជុល និងថែទាំ',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'222',N'6350',N'សេវាសវនកម្ម និងច្បាប់',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'223',N'6360',N'កម្រៃអាជ្ញាប័ណ្ណ',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'224',N'6370',N'ចំណាយលើការគ្រប់គ្រង',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'225',N'6400',N'ធានារ៉ាប់រង',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'226',N'6410',N'សំណងលើការខូចខាត និងគ្រោះថ្នាក់ការងារ',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'227',N'6450',N'សេវាធនាគារ',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'228',N'6500',N'ពិធីជប់លៀង និងការសំដែង',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'229',N'6510',N'អំណោយ និងវិភាគទាន',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'230',N'6590',N'ចំណាយទូទៅផ្សេងៗ',N'',N'',N'27',N'1',N'1'
UNION ALL SELECT N'240',N'4380',N'ឈ្នួលលើការតម្លើងបណ្តាញ',N'',N'',N'18',N'2',N'1'
UNION ALL SELECT N'241',N'6210',N'ពន្ធលើប្រេងឥន្ធនៈ',N'',N'',N'214',N'1',N'1'
UNION ALL SELECT N'242',N'6220',N'ពន្ធលើប្រាក់ចំណេញដុល',N'',N'',N'214',N'1',N'1'
UNION ALL SELECT N'243',N'6240',N'ពន្ធលើមធ្យោបាយដឹកជញ្ជូន',N'',N'',N'214',N'1',N'1'
UNION ALL SELECT N'244',N'6230',N'ពន្ធប៉ាតង់',N'',N'',N'214',N'1',N'1'
UNION ALL SELECT N'245',N'6250',N'ពន្ធផ្សេងៗទៀត',N'',N'',N'214',N'1',N'1'
UNION ALL SELECT N'246',N'6290',N'ពន្ធលើផលរបរ',N'',N'',N'214',N'1',N'1'
UNION ALL SELECT N'248',N'1100',N'ដករំលស់បូកយោង-ទ្រព្យប្រើក្នុងសេវាកម្ម ',N'',N'',N'8',N'1',N'1'
UNION ALL SELECT N'249',N'1105',N'រំលស់បូកយោង-អគារ',N'',N'',N'248',N'1',N'1'
UNION ALL SELECT N'250',N'1110',N'រំលស់បូកយោង-មធ្យោបាយផលិតកម្មអគ្គិសនី',N'',N'',N'248',N'1',N'1'
UNION ALL SELECT N'251',N'1130',N'រំលស់បូកយោង-មធ្យោបាយផ្នែកបណ្តាញបញ្ជូន',N'',N'',N'248',N'1',N'1'
UNION ALL SELECT N'252',N'1150',N'រំលស់បូកយោង-មធ្យោបាយចែកចាយអគ្គិសនី',N'',N'',N'248',N'1',N'1'
UNION ALL SELECT N'253',N'1170',N'រំលស់បូកយោង-មធ្យោបាយធ្វើការងារ',N'',N'',N'248',N'1',N'1'
UNION ALL SELECT N'254',N'1180',N'ការកែសម្រួលទ្រព្យប្រើក្នុងសេវាកម្ម',N'',N'',N'8',N'1',N'1'
UNION ALL SELECT N'255',N'1190',N'ដករំលស់បូកយោង-ការកែសម្រួលទ្រព្យប្រើក្នុងសេវាកម្ម',N'',N'',N'8',N'1',N'1'
UNION ALL SELECT N'256',N'1200',N'ការងារសាងសង់កំពុងដំណើរការ',N'',N'',N'8',N'1',N'1'
UNION ALL SELECT N'257',N'1210',N'ទ្រព្យ និងបរិក្ខារបម្រុងប្រើនាពេលខាងមុខ',N'',N'',N'8',N'1',N'1'
UNION ALL SELECT N'258',N'1220',N'ដករំលស់បូកយោង-ទ្រព្យ និងបរិក្ខារបម្រុងប្រើនាពេលខាងមុខ',N'',N'',N'8',N'1',N'1'
UNION ALL SELECT N'259',N'1230',N'ទ្រព្យមិនអាចចាត់ចំណាត់ថ្នាក់',N'',N'',N'8',N'1',N'1'
UNION ALL SELECT N'260',N'1240',N'ដករំលស់បូកយោង-ទ្រព្យមិនអាចចាត់ចំណាត់ថ្នាក់',N'',N'',N'8',N'1',N'1'
UNION ALL SELECT N'261',N'1300',N'គណនីមូលនិធិ',N'',N'',N'8',N'1',N'1'
UNION ALL SELECT N'262',N'1310',N'មូលនិធិកប់ក្នុងការវិនិយោគ',N'',N'',N'261',N'1',N'1'
UNION ALL SELECT N'263',N'1340',N'មូលនិធិពិសេសផ្សេងៗ',N'',N'',N'261',N'1',N'1'
UNION ALL SELECT N'264',N'1350',N'ទ្រព្យអរូបី',N'',N'',N'8',N'1',N'1'
UNION ALL SELECT N'265',N'1360',N'Organization',N'',N'',N'264',N'1',N'1'
UNION ALL SELECT N'266',N'1390',N'ទ្រព្យអរូបីផ្សេងៗទៀត',N'',N'',N'264',N'1',N'1'
UNION ALL SELECT N'267',N'1850',N'វត្ថុធាតុដើម និងការផ្គត់ផ្គង់-ប្រេងឥន្ធនៈ',N'',N'',N'85',N'1',N'1'
UNION ALL SELECT N'268',N'1860',N'វត្ថុធាតុដើម និងការផ្គត់ផ្គង់-ប្រេងរំអិល',N'',N'',N'85',N'1',N'1'
UNION ALL SELECT N'269',N'1870',N'វត្ថុធាតុដើម និងការផ្គត់ផ្គង់-អគ្គិសនី',N'',N'',N'85',N'1',N'1'
UNION ALL SELECT N'270',N'1880',N'វត្ថុធាតុដើម និងការផ្គត់ផ្គង់-ការិយាល័យ',N'',N'',N'85',N'1',N'1'
UNION ALL SELECT N'271',N'1890',N'វត្ថុធាតុដើម និងការផ្គត់ផ្គង់-ផ្សេងៗ',N'',N'',N'85',N'1',N'1'
UNION ALL SELECT N'273',N'2010',N'ភាគហ៊ុនធម្មតា',N'',N'',N'89',N'2',N'1'
UNION ALL SELECT N'274',N'2020',N'ភាគហ៊ុនអាទិភាព',N'',N'',N'89',N'2',N'1'
UNION ALL SELECT N'275',N'2030',N'មូលធនដាក់បន្ថែម',N'',N'',N'89',N'2',N'1'
UNION ALL SELECT N'276',N'3000',N'បំណុលត្រូវសងរយៈពេលវែង-ក្នុងស្រុក',N'',N'',N'11',N'2',N'1'
SET IDENTITY_INSERT TBL_ACCOUNT_CHART OFF;
END

GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='TBL_ACCOUNT_TRAN' AND COLUMN_NAME='ACCOUNT_TYPE_ID' )
	ALTER TABLE TBL_ACCOUNT_TRAN ADD ACCOUNT_TYPE_ID INT NULL;
GO
IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='TBL_ACCOUNT_TRAN' AND COLUMN_NAME='ITEM_ID' )
BEGIN
	EXEC sp_executesql N'
UPDATE TBL_ACCOUNT_TRAN 
SET ACCOUNT_TYPE_ID = c.TYPE_ID
FROM TBL_ACCOUNT_TRAN t
INNER JOIN TBL_ACCOUNT_ITEM i ON i.ITEM_ID = t.ITEM_ID
INNER JOIN TBL_ACCOUNT_CATEGORY c ON c.CATEGORY_ID = i.CATEGORY_ID
WHERE ACCOUNT_TYPE_ID IS NULL;'

	EXEC sp_rename 'dbo.TBL_ACCOUNT_TRAN.ITEM_ID', 'TRAN_ACCOUNT_ID', 'COLUMN';
END
GO 
ALTER TABLE TBL_ACCOUNT_TRAN ALTER COLUMN ACCOUNT_TYPE_ID INT NOT NULL;
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='TBL_ACCOUNT_TRAN' AND COLUMN_NAME='PAYMENT_ACCOUNT_ID' )
	ALTER TABLE TBL_ACCOUNT_TRAN ADD PAYMENT_ACCOUNT_ID INT NULL;
GO
UPDATE TBL_ACCOUNT_TRAN 
SET PAYMENT_ACCOUNT_ID=(SELECT CONFIG_VALUE FROM TBL_ACCOUNT_CONFIG WHERE CONFIG_ID=9) 
WHERE PAYMENT_ACCOUNT_ID IS NULL;
GO
ALTER TABLE TBL_ACCOUNT_TRAN ALTER COLUMN PAYMENT_ACCOUNT_ID INT NOT NULL;
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='TBL_ACCOUNT_TRAN' AND COLUMN_NAME='CURRENCY_ID' )
	ALTER TABLE TBL_ACCOUNT_TRAN ADD CURRENCY_ID INT NULL;
GO
UPDATE TBL_ACCOUNT_TRAN 
SET CURRENCY_ID=(SELECT CURRENCY_ID FROM TLKP_CURRENCY WHERE IS_DEFAULT_CURRENCY=1) 
WHERE CURRENCY_ID IS NULL;
GO
ALTER TABLE TBL_ACCOUNT_TRAN ALTER COLUMN CURRENCY_ID INT NOT NULL;

