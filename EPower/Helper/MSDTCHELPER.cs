﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EPower.Helper
{
   public class MSDTCHELPER
    {
        public static void BulkCopy(DataTable dt, string tableName)
        {
            var ConnectionString = "server=testing_ag;uid=sa;pwd=ePower#@168;database=EPowerTestRunbill;";
            //DBDataContext.Db.Connection.Close();
            var bulk = new SqlBulkCopy(ConnectionString);
            bulk.BulkCopyTimeout = 60 * 10;
            bulk.DestinationTableName = tableName;
            bulk.WriteToServer(dt);
            bulk.Close();
            //DBDataContext.Db.Connection.Open();
        }
        public static DataTable GetTableData()
        {
            var dt = new DataTable();
            dt.Columns.Add(new DataColumn() { ColumnName = "Id", DataType = typeof(int) });
            dt.Columns.Add(new DataColumn() { ColumnName = "Name1", DataType = typeof(string) });
            dt.Columns.Add(new DataColumn() { ColumnName = "Name2", DataType = typeof(string) });
            dt.Columns.Add(new DataColumn() { ColumnName = "Name3", DataType = typeof(string) });

            for (int i = 0; i < 10000; i++)
            {
                var row = dt.Rows.Add();
                row["Name1"] = "Name1 -> " + i.ToString();
                row["Name2"] = "Name2 ->" + i.ToString();
                row["Name3"] = "Name3 ->" + i.ToString();
            }
            return dt;
        }
        public  static void TestMSDTC()
        {
            BulkCopy(GetTableData(), "MSDTC_TABLE");
        } 
    }
}
