﻿UPDATE dbo.TBL_CUSTOMER
SET PHONE_1 = N'-'
WHERE PHONE_1 = N'';

UPDATE dbo.TBL_CUSTOMER
SET ID_CARD_NO = N'-'
WHERE ID_CARD_NO = N'';


IF NOT EXISTS
(
    SELECT *
    FROM dbo.TLKP_CUSTOMER_CONNECTION_TYPE
    WHERE CUSTOMER_CONNECTION_TYPE_ID = 21
)
BEGIN
    INSERT INTO dbo.TLKP_CUSTOMER_CONNECTION_TYPE
    VALUES
    (   21,                                      -- CUSTOMER_CONNECTION_TYPE_ID - int
        N'ទិញ MV តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',   -- CUSTOMER_CONNECTION_TYPE_NAME - nvarchar(250)
        N'1.ទិញ MV តម្លើងប្រភពពន្លឺព្រះអាទិត្យ', -- DESCRIPTION - nvarchar(200)
        2,                                       -- CURRENCY_ID - int
        0.137,                                   -- TARIFF - decimal(18, 4)
        1,                                       -- NONLICENSE_CUSTOMER_GROUP_ID - int
        0                                        -- PRICE_ID - int
        );
END;


IF OBJECT_ID('REPORT_QUARTER_POWER_COVERAGE') IS NOT NULL
    DROP PROC REPORT_QUARTER_POWER_COVERAGE;

GO

CREATE PROC dbo.REPORT_QUARTER_POWER_COVERAGE @DATE DATETIME = '2021-01-01'
AS
DECLARE @D1 DATETIME,
        @D2 DATETIME;
SET @DATE = DATEADD(D, 0, DATEDIFF(D, 0, @DATE));
SET @D1 = @DATE;
SET @D2 = DATEADD(S, -1, DATEADD(M, 3, @DATE));

-- CALCULATE CUSTOMER BY VILLAGES
SELECT VILLAGE_CODE,
       TOTAL_COVERAGE = ISNULL(SUM(   CASE
                                          WHEN OLD_STATUS_ID = 1
                                               AND NEW_STATUS_ID = 2 THEN
                                              1  -- អតិថិជនថ្មី
                                          WHEN OLD_STATUS_ID = 2
                                               AND NEW_STATUS_ID = 4 THEN
                                              -1 -- កំពុងប្រើប្រាស => ឈប់ប្រើ
                                          WHEN OLD_STATUS_ID = 4
                                               AND NEW_STATUS_ID = 2 THEN
                                              1  -- ឈប់ប្រើប្រាស់ => កំពុងប្រើប្រាស់
                                          WHEN OLD_STATUS_ID = 3
                                               AND NEW_STATUS_ID = 4 THEN
                                              -1 -- ផ្តាច់ => ឈប់ប្រើ 
                                          ELSE
                                              0
                                      END
                                  ),
                               0
                              )
INTO #TMP_COVERAGES
FROM TBL_CUS_STATUS_CHANGE g
    INNER JOIN TBL_CUSTOMER c
        ON c.CUSTOMER_ID = g.CUSTOMER_ID
WHERE g.CHNAGE_DATE <= @D2
      AND
      (
          c.USAGE_CUSTOMER_ID = 0
          OR c.USAGE_CUSTOMER_ID = c.CUSTOMER_ID
      ) --only not 3-phase
      AND (c.IS_REACTIVE = 0) -- not reactive customer
      AND c.CUSTOMER_TYPE_ID NOT IN ( -1 ) -- customer in production
      AND c.CUSTOMER_ID NOT IN
          (
              SELECT CUSTOMER_ID
              FROM TBL_LICENSEE_CONNECTION
              WHERE IS_ACTIVE = 1
                    AND (START_DATE <= @D2)
                    AND
                    (
                        IS_INUSED = 1
                        OR END_DATE > @D1
                    )
          )
GROUP BY VILLAGE_CODE;

-- CALCULATE MV BY VILLAGES
SELECT VILLAGE_CODE,
       MV = MAX(   CASE
                       WHEN DISTRIBUTION_ID = 1 THEN
                           1
                       ELSE
                           0
                   END
               ),
       LV = MAX(   CASE
                       WHEN DISTRIBUTION_ID IN ( 2, 3 ) THEN
                           1
                       ELSE
                           0
                   END
               )
INTO #TMP_DISTRIBUTIONS
FROM TBL_DISTRIBUTION_FACILITY f
    INNER JOIN TBL_DISTRIBUTION_VILLAGE v
        ON v.DISTRIBUTION_FACILITY_ID = f.DISTRIBUTION_FACILITY_ID
WHERE f.IS_ACTIVE = 1
      AND v.IS_ACTIVE = 1
      AND (START_DATE <= @D2)
      AND
      (
          IS_INUSED = 1
          OR END_DATE > @D1
      )
GROUP BY VILLAGE_CODE;

-- CALCULATE TOTAL_USAGE MV, LV
SELECT c.VILLAGE_CODE,
       MV_USAGE = ISNULL(
                            SUM(   CASE
                                       WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 4, 8, 11, 26, 38, 50, 62, 74, 86, 98,
                                                                               110, 122, 141
                                                                             ) THEN
                                           i.TOTAL_USAGE + ISNULL(adj.ADJUST_USAGE, 0)
                                   END
                               ),
                            0
                        ),
       LV_USAGE = ISNULL(
                            SUM(   CASE
                                       WHEN i.CUSTOMER_CONNECTION_TYPE_ID NOT IN ( 4, 8, 11, 26, 38, 50, 62, 74, 86,
                                                                                   98, 110, 122, 141
                                                                                 ) THEN
                                           i.TOTAL_USAGE + ISNULL(adj.ADJUST_USAGE, 0)
                                   END
                               ),
                            0
                        )
INTO #TMP
FROM TBL_INVOICE i
    INNER JOIN TBL_CUSTOMER c
        ON c.CUSTOMER_ID = i.CUSTOMER_ID
    OUTER APPLY
(
    SELECT ADJUST_USAGE = SUM(ADJUST_USAGE)
    FROM TBL_INVOICE_ADJUSTMENT
    WHERE INVOICE_ID = i.INVOICE_ID
) adj
WHERE i.INVOICE_MONTH
      BETWEEN CONVERT(NVARCHAR(4), @DATE, 126) + '-01-01' AND @D2
      AND i.IS_SERVICE_BILL = 0
      AND c.CUSTOMER_ID NOT IN
          (
              SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1
          )
      AND c.IS_REACTIVE = 0
      AND i.INVOICE_STATUS NOT IN ( 3 )
GROUP BY c.VILLAGE_CODE
UNION ALL
SELECT c.VILLAGE_CODE,
       MV_USAGE = ISNULL(
                            SUM(   CASE
                                       WHEN c.CUSTOMER_CONNECTION_TYPE_ID IN ( 4, 8, 11, 26, 38, 50, 62, 74, 86, 98,
                                                                               110, 122, 141
                                                                             ) THEN
                                           cr.USAGE
                                   END
                               ),
                            0
                        ),
       LV_USAGE = ISNULL(
                            SUM(   CASE
                                       WHEN c.CUSTOMER_CONNECTION_TYPE_ID NOT IN ( 4, 8, 11, 26, 38, 50, 62, 74, 86,
                                                                                   98, 110, 122, 141
                                                                                 ) THEN
                                           cr.USAGE
                                   END
                               ),
                            0
                        )
FROM TBL_PREPAID_CREDIT cr
    INNER JOIN TBL_CUSTOMER c
        ON c.CUSTOMER_ID = cr.CUSTOMER_ID
WHERE cr.CREDIT_MONTH
BETWEEN CONVERT(NVARCHAR(4), @DATE, 126) + '-01-01' AND @D2
GROUP BY c.VILLAGE_CODE;

SELECT VILLAGE_CODE,
       MV_TOTAL_USAGE = SUM(MV_USAGE),
       LV_TOTAL_USAGE = SUM(LV_USAGE)
INTO #TMP_INVOICE
FROM #TMP
GROUP BY VILLAGE_CODE;
-- RENDER RESULT
SELECT d.DISTRICT_NAME,
       c.COMMUNE_NAME,
       v.VILLAGE_NAME,
       MV_TOTAL_USAGE = ISNULL(i.MV_TOTAL_USAGE, 0),
       LV_TOTAL_USAGE = ISNULL(i.LV_TOTAL_USAGE, 0),
       TOTAL_FAMILY = ISNULL(TOTAL_FAMILIES, 0),
       TOTAL_COVERAGE = ISNULL(TOTAL_COVERAGE, 0),
       PERCENTAGE_COVERAGE = ISNULL(ISNULL(TOTAL_COVERAGE, 0) * 100.00 / NULLIF(TOTAL_FAMILIES, 0), 0),
       MV_COVERAGE = CONVERT(BIT, ISNULL(MV, 0)),
       LV_COVERAGE = CONVERT(BIT, ISNULL(LV, 0)),
       IS_OUTSIZE_ZONE = 0
FROM TBL_LICENSE_VILLAGE lv
    INNER JOIN TLKP_VILLAGE v
        ON lv.VILLAGE_CODE = v.VILLAGE_CODE
    INNER JOIN TLKP_COMMUNE c
        ON c.COMMUNE_CODE = v.COMMUNE_CODE
    INNER JOIN TLKP_DISTRICT d
        ON d.DISTRICT_CODE = c.DISTRICT_CODE
    LEFT JOIN #TMP_COVERAGES t
        ON t.VILLAGE_CODE = v.VILLAGE_CODE
    LEFT JOIN #TMP_DISTRIBUTIONS b
        ON b.VILLAGE_CODE = v.VILLAGE_CODE
    LEFT JOIN #TMP_INVOICE i
        ON i.VILLAGE_CODE = v.VILLAGE_CODE
WHERE lv.IS_ACTIVE = 1
--ORDER BY COMMUNE_NAME, VILLAGE_NAME
UNION ALL
SELECT d.DISTRICT_NAME,
       c.COMMUNE_NAME,
       v.VILLAGE_NAME,
       MV_TOTAL_USAGE = ISNULL(i.MV_TOTAL_USAGE, 0),
       LV_TOTAL_USAGE = ISNULL(i.LV_TOTAL_USAGE, 0),
       TOTAL_FAMILY = ISNULL(TOTAL_FAMILIES, 0),
       TOTAL_COVERAGE = ISNULL(TOTAL_COVERAGE, 0),
       PERCENTAGE_COVERAGE = ISNULL(ISNULL(TOTAL_COVERAGE, 0) * 100.00 / NULLIF(TOTAL_FAMILIES, 0), 0),
       MV_COVERAGE = CONVERT(BIT, ISNULL(MV, 0)),
       LV_COVERAGE = CONVERT(BIT, ISNULL(LV, 0)),
       IS_OUTSIZE_ZONE = 1
FROM TLKP_VILLAGE v
    LEFT JOIN TBL_LICENSE_VILLAGE lv
        ON lv.VILLAGE_CODE = v.VILLAGE_CODE
    INNER JOIN TLKP_COMMUNE c
        ON c.COMMUNE_CODE = v.COMMUNE_CODE
    INNER JOIN TLKP_DISTRICT d
        ON d.DISTRICT_CODE = c.DISTRICT_CODE
    LEFT JOIN #TMP_COVERAGES t
        ON t.VILLAGE_CODE = v.VILLAGE_CODE
    LEFT JOIN #TMP_DISTRIBUTIONS b
        ON b.VILLAGE_CODE = v.VILLAGE_CODE
    LEFT JOIN #TMP_INVOICE i
        ON i.VILLAGE_CODE = v.VILLAGE_CODE
WHERE TOTAL_COVERAGE > 0
      AND v.VILLAGE_CODE NOT IN
          (
              SELECT VILLAGE_CODE FROM TBL_LICENSE_VILLAGE WHERE IS_ACTIVE = 1
          );
--END OF REPORT_QUARTER_POWER_COVERAGE

GO

IF OBJECT_ID('REPORT_QUARTER_CUSTOMER_CONSUMER') IS NOT NULL
    DROP PROC dbo.REPORT_QUARTER_CUSTOMER_CONSUMER;
GO

CREATE PROC dbo.REPORT_QUARTER_CUSTOMER_CONSUMER @DATE DATETIME = '2021-01-01'
AS
DECLARE @D1 DATETIME,
        @D2 DATETIME;

SET @DATE = DATEADD(D, 0, DATEDIFF(D, 0, @DATE));
SET @D1 = @DATE;
SET @D2 = DATEADD(S, -1, DATEADD(M, 3, @DATE));

IF (@DATE >= '2021-01-01')
BEGIN
    SELECT *
    INTO #TMP1
    FROM
    (
        SELECT cg.CUSTOMER_GROUP_ID,
               cg.CUSTOMER_GROUP_NAME,
               t.CUSTOMER_CONNECTION_TYPE_ID,
               t.CUSTOMER_CONNECTION_TYPE_NAME,
               DAILY_SUPPLY_HOUR = 24,
               NUMBER = ISNULL(SUM(   CASE
                                          WHEN OLD_STATUS_ID = 1
                                               AND NEW_STATUS_ID = 2 THEN
                                              1  -- អតិថិជនថ្មី
                                          WHEN OLD_STATUS_ID = 2
                                               AND NEW_STATUS_ID = 4 THEN
                                              -1 -- កំពុងប្រើប្រាស => ឈប់ប្រើ
                                          WHEN OLD_STATUS_ID = 4
                                               AND NEW_STATUS_ID = 2 THEN
                                              1  -- ឈប់ប្រើប្រាស់ => កំពុងប្រើប្រាស់
                                          WHEN OLD_STATUS_ID = 3
                                               AND NEW_STATUS_ID = 4 THEN
                                              -1 -- ផ្តាច់ => ឈប់ប្រើ 
                                          ELSE
                                              0
                                      END
                                  ),
                               0
                              ),
               --NUMBER = ISNULL(SUM(CASE WHEN NEW_STATUS_ID=2 THEN 1 WHEN OLD_STATUS_ID=2 THEN -1 ELSE 0 END),0),
               REMARK = ''
        FROM TBL_CUSTOMER c
            INNER JOIN TLKP_CUSTOMER_CONNECTION_TYPE t
                ON t.CUSTOMER_CONNECTION_TYPE_ID = c.CUSTOMER_CONNECTION_TYPE_ID
            INNER JOIN TLKP_CUSTOMER_GROUP cg
                ON cg.CUSTOMER_GROUP_ID = t.NONLICENSE_CUSTOMER_GROUP_ID
            INNER JOIN TBL_CUS_STATUS_CHANGE g
                ON g.CUSTOMER_ID = c.CUSTOMER_ID
        WHERE g.CHNAGE_DATE <= @D2
              AND
              (
                  c.USAGE_CUSTOMER_ID = 0
                  OR c.USAGE_CUSTOMER_ID = c.CUSTOMER_ID
              ) --only not 3-phase
              AND (c.IS_REACTIVE = 0) -- not reactive customer
              AND c.CUSTOMER_ID NOT IN
                  (
                      SELECT CUSTOMER_ID
                      FROM TBL_LICENSEE_CONNECTION
                      WHERE IS_ACTIVE = 1
                            AND (START_DATE <= @D2)
                            AND
                            (
                                IS_INUSED = 1
                                OR END_DATE > @D1
                            )
                  )
        GROUP BY cg.CUSTOMER_GROUP_ID,
                 cg.CUSTOMER_GROUP_NAME,
                 t.CUSTOMER_CONNECTION_TYPE_ID,
                 t.CUSTOMER_CONNECTION_TYPE_NAME
        UNION ALL
        SELECT cg.CUSTOMER_GROUP_ID,
               cg.CUSTOMER_GROUP_NAME,
               t.CUSTOMER_CONNECTION_TYPE_ID,
               t.CUSTOMER_CONNECTION_TYPE_NAME,
               DAILY_SUPPLY_HOUR = 24,
               NUMBER = 0,
               REMARK = ''
        FROM TLKP_CUSTOMER_CONNECTION_TYPE t
            LEFT JOIN TLKP_CUSTOMER_GROUP cg
                ON cg.CUSTOMER_GROUP_ID = t.NONLICENSE_CUSTOMER_GROUP_ID
        WHERE t.CUSTOMER_CONNECTION_TYPE_ID NOT IN
              (
                  SELECT CUSTOMER_CONNECTION_TYPE_ID
                  FROM TBL_CUSTOMER
                  WHERE STATUS_ID NOT IN ( 1, 4, 5 )
              )
              AND t.CUSTOMER_CONNECTION_TYPE_ID NOT IN ( 11, 12, 13 )
              AND cg.IS_ACTIVE = 1
        GROUP BY cg.CUSTOMER_GROUP_ID,
                 cg.CUSTOMER_GROUP_NAME,
                 t.CUSTOMER_CONNECTION_TYPE_ID,
                 t.CUSTOMER_CONNECTION_TYPE_NAME
    ) x;
    SELECT CUSTOMER_GROUP_ID = CASE
                                   WHEN CUSTOMER_GROUP_ID = 4 THEN
                                       1
                                   WHEN CUSTOMER_GROUP_ID = 7 THEN
                                       2
                                   WHEN CUSTOMER_GROUP_ID = 17 THEN
                                       3
                                   WHEN CUSTOMER_GROUP_ID = 18 THEN
                                       4
                                   WHEN CUSTOMER_GROUP_ID = 5 THEN
                                       5
                                   WHEN CUSTOMER_GROUP_ID = 6 THEN
                                       6
                                   WHEN CUSTOMER_GROUP_ID = 16 THEN
                                       7
                                   WHEN CUSTOMER_GROUP_ID = 15 THEN
                                       8
                                   WHEN CUSTOMER_GROUP_ID = 14 THEN
                                       9
                                   WHEN CUSTOMER_GROUP_ID = 13 THEN
                                       10
                                   WHEN CUSTOMER_GROUP_ID = 12 THEN
                                       11
                                   WHEN CUSTOMER_GROUP_ID = 11 THEN
                                       12
                                   WHEN CUSTOMER_GROUP_ID = 10 THEN
                                       13
                                   WHEN CUSTOMER_GROUP_ID = 9 THEN
                                       14
                                   WHEN CUSTOMER_GROUP_ID = 8 THEN
                                       15
                               END,
           CUSTOMER_GROUP_NAME,
           CUSTOMER_CONNECTION_TYPE_ID = CASE
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID = 1 THEN
                                                 1
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 22, 23, 134, 144 ) THEN
                                                 2
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 135, 136, 137 ) THEN
                                                 3
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 138, 139, 140 ) THEN
                                                 4
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 9 ) THEN
                                                 5
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 24, 141, 142, 143 ) THEN
                                                 6
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 26, 27, 28, 29, 30, 31, 32, 33, 34,
                                                                                   35, 36, 37
                                                                                 ) THEN
                                                 7
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 38, 39, 40, 41, 42, 43, 44, 45, 46,
                                                                                   47, 48, 49
                                                                                 ) THEN
                                                 8
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 50, 51, 52, 53, 54, 55, 56, 57, 58,
                                                                                   59, 60, 61
                                                                                 ) THEN
                                                 9
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 62, 63, 64, 65, 66, 67, 68, 69, 70,
                                                                                   71, 72, 73
                                                                                 ) THEN
                                                 10
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 74, 75, 76, 77, 78, 79, 80, 81, 82,
                                                                                   83, 84, 85
                                                                                 ) THEN
                                                 11
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 86, 87, 88, 89, 90, 91, 92, 93, 94,
                                                                                   95, 96, 97
                                                                                 ) THEN
                                                 12
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 98, 99, 100, 101, 102, 103, 104,
                                                                                   105, 106, 107, 108, 109
                                                                                 ) THEN
                                                 13
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 110, 111, 112, 113, 114, 115, 116,
                                                                                   117, 118, 119, 120, 121
                                                                                 ) THEN
                                                 14
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 122, 123, 124, 125, 126, 127, 128,
                                                                                   129, 130, 131, 132, 133
                                                                                 ) THEN
                                                 15
                                         END,
           CUSTOMER_CONNECTION_TYPE_NAME,
           DAILY_SUPPLY_HOUR,
           NUMBER
    FROM #TMP1
    WHERE CUSTOMER_GROUP_NAME != N'អតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ'
          AND NUMBER > 0;
-- END REPORT_QUARTER_CUSTOMER_CONSUMER >=2021
END;

ELSE
BEGIN
    SELECT *
    INTO #TMP
    FROM
    (
        SELECT cg.CUSTOMER_GROUP_ID,
               cg.CUSTOMER_GROUP_NAME,
               t.CUSTOMER_CONNECTION_TYPE_ID,
               t.CUSTOMER_CONNECTION_TYPE_NAME,
               DAILY_SUPPLY_HOUR = 24,
               NUMBER = ISNULL(SUM(   CASE
                                          WHEN OLD_STATUS_ID = 1
                                               AND NEW_STATUS_ID = 2 THEN
                                              1  -- អតិថិជនថ្មី
                                          WHEN OLD_STATUS_ID = 2
                                               AND NEW_STATUS_ID = 4 THEN
                                              -1 -- កំពុងប្រើប្រាស => ឈប់ប្រើ
                                          WHEN OLD_STATUS_ID = 4
                                               AND NEW_STATUS_ID = 2 THEN
                                              1  -- ឈប់ប្រើប្រាស់ => កំពុងប្រើប្រាស់
                                          WHEN OLD_STATUS_ID = 3
                                               AND NEW_STATUS_ID = 4 THEN
                                              -1 -- ផ្តាច់ => ឈប់ប្រើ 
                                          ELSE
                                              0
                                      END
                                  ),
                               0
                              ),
               --NUMBER = ISNULL(SUM(CASE WHEN NEW_STATUS_ID=2 THEN 1 WHEN OLD_STATUS_ID=2 THEN -1 ELSE 0 END),0),
               REMARK = ''
        FROM TBL_CUSTOMER c
            INNER JOIN TLKP_CUSTOMER_CONNECTION_TYPE t
                ON t.CUSTOMER_CONNECTION_TYPE_ID = c.CUSTOMER_CONNECTION_TYPE_ID
            INNER JOIN TLKP_CUSTOMER_GROUP cg
                ON cg.CUSTOMER_GROUP_ID = t.NONLICENSE_CUSTOMER_GROUP_ID
            INNER JOIN TBL_CUS_STATUS_CHANGE g
                ON g.CUSTOMER_ID = c.CUSTOMER_ID
        WHERE g.CHNAGE_DATE <= @D2
              AND
              (
                  c.USAGE_CUSTOMER_ID = 0
                  OR c.USAGE_CUSTOMER_ID = c.CUSTOMER_ID
              ) --only not 3-phase
              AND (c.IS_REACTIVE = 0) -- not reactive customer
              AND c.CUSTOMER_ID NOT IN
                  (
                      SELECT CUSTOMER_ID
                      FROM TBL_LICENSEE_CONNECTION
                      WHERE IS_ACTIVE = 1
                            AND (START_DATE <= @D2)
                            AND
                            (
                                IS_INUSED = 1
                                OR END_DATE > @D1
                            )
                  )
        GROUP BY cg.CUSTOMER_GROUP_ID,
                 cg.CUSTOMER_GROUP_NAME,
                 t.CUSTOMER_CONNECTION_TYPE_ID,
                 t.CUSTOMER_CONNECTION_TYPE_NAME
        UNION ALL
        SELECT cg.CUSTOMER_GROUP_ID,
               cg.CUSTOMER_GROUP_NAME,
               t.CUSTOMER_CONNECTION_TYPE_ID,
               t.CUSTOMER_CONNECTION_TYPE_NAME,
               DAILY_SUPPLY_HOUR = 24,
               NUMBER = 0,
               REMARK = ''
        FROM TLKP_CUSTOMER_CONNECTION_TYPE t
            LEFT JOIN TLKP_CUSTOMER_GROUP cg
                ON cg.CUSTOMER_GROUP_ID = t.NONLICENSE_CUSTOMER_GROUP_ID
        WHERE t.CUSTOMER_CONNECTION_TYPE_ID NOT IN
              (
                  SELECT CUSTOMER_CONNECTION_TYPE_ID
                  FROM TBL_CUSTOMER
                  WHERE STATUS_ID NOT IN ( 1, 4, 5 )
              )
              AND t.CUSTOMER_CONNECTION_TYPE_ID NOT IN ( 11, 12, 13 )
        GROUP BY cg.CUSTOMER_GROUP_ID,
                 cg.CUSTOMER_GROUP_NAME,
                 t.CUSTOMER_CONNECTION_TYPE_ID,
                 t.CUSTOMER_CONNECTION_TYPE_NAME
    ) x;
    SELECT CUSTOMER_GROUP_ID = CASE
                                   WHEN CUSTOMER_GROUP_ID = 4 THEN
                                       1
                                   WHEN CUSTOMER_GROUP_ID = 2 THEN
                                       2
                                   WHEN CUSTOMER_GROUP_ID = 1 THEN
                                       3
                                   WHEN CUSTOMER_GROUP_ID = 5 THEN
                                       4
                               END,
           CUSTOMER_GROUP_NAME,
           CUSTOMER_CONNECTION_TYPE_ID = CASE
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID = 1 THEN
                                                 1
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 4 ) THEN
                                                 2
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 7 ) THEN
                                                 3
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 21 ) THEN
                                                 4
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 2 ) THEN
                                                 5
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 18 ) THEN
                                                 6
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 7
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 3 ) THEN
                                                 8
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 17 ) THEN
                                                 9
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 10
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 14 ) THEN
                                                 11
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 8 ) THEN
                                                 12
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 10 ) THEN
                                                 13
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 14
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 5 ) THEN
                                                 15
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 20 ) THEN
                                                 16
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 17
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 6 ) THEN
                                                 18
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 19 ) THEN
                                                 19
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 20
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 15 ) THEN
                                                 21
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 16 ) THEN
                                                 22
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 9 ) THEN
                                                 23
                                         END,
           CUSTOMER_CONNECTION_TYPE_NAME,
           DAILY_SUPPLY_HOUR,
           NUMBER
    FROM #TMP
    WHERE CUSTOMER_GROUP_NAME != N'អតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ';
-- END REPORT_QUARTER_CUSTOMER_CONSUMER <2021
END;

GO

IF OBJECT_ID('REPORT_QUARTER_SOLD_CONSUMER') IS NOT NULL
    DROP PROC dbo.REPORT_QUARTER_SOLD_CONSUMER;

GO

CREATE PROC dbo.REPORT_QUARTER_SOLD_CONSUMER @DATE DATETIME = '2021-01-01'
AS
DECLARE @D1 DATETIME,
        @D2 DATETIME;
SET @DATE = DATEADD(D, 0, DATEDIFF(D, 0, @DATE));
SET @D1 = CONVERT(NVARCHAR(4), @DATE, 126) + '-01-01';
SET @D2 = DATEADD(S, -1, DATEADD(M, 3, @DATE));


-- BUILD 12 MONTHs TABLE
CREATE TABLE #MONTHS
(
    M DATETIME
);
DECLARE @M INT;
SET @M = 1;
WHILE @M <= 12
BEGIN
    INSERT INTO #MONTHS
    VALUES
    (CONVERT(NVARCHAR(4), @D1, 126) + '-' + CONVERT(NVARCHAR, @M) + '-1');
    SET @M = @M + 1;
END;

IF (@DATE >= '2021-01-01')
BEGIN
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'លំនៅដ្ឋាន'
    INTO #TMP_TYPE1 UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'អាជីវកម្មធុនតូច LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'រដ្ឋបាល LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'អាជីវករតូបលក់ដូរក្នុងផ្សារ'
    UNION ALL
    SELECT TYPE_ID = 6,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 7,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 8,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 9,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 10,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 11,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 12,
           TYPE_NAME = N'សាលារៀន, មន្ទីរពេទ្យ, មណ្ឌលសុខភាព នៅតំបន់ជនបទ'
    UNION ALL
    SELECT TYPE_ID = 13,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 14,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 15,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 16,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT TYPE_ID = 17,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 18,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 19,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT TYPE_ID = 20,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 21,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 22,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 23,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 24,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 25,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 26,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 27,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 28,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT TYPE_ID = 29,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 30,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 31,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT TYPE_ID = 32,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 33,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 34,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 35,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 36,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 37,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 38,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 39,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 40,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT TYPE_ID = 41,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 42,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 43,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT TYPE_ID = 44,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 45,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 46,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 47,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 48,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 49,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 50,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 51,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 52,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT TYPE_ID = 53,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 54,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 55,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT TYPE_ID = 56,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 57,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 58,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 59,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 60,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 61,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 62,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 63,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 64,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT TYPE_ID = 65,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្មទ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 66,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 67,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT TYPE_ID = 68,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 69,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 70,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 71,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 72,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 73,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 74,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 75,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 76,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT TYPE_ID = 77,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 78,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 79,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT TYPE_ID = 80,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 81,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 82,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 83,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 84,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 85,
           TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 86,
           TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 87,
           TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 88,
           TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT TYPE_ID = 89,
           TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 90,
           TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 91,
           TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT TYPE_ID = 92,
           TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 93,
           TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 94,
           TYPE_NAME = N'ពាណិជ្ជកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 95,
           TYPE_NAME = N'ពាណិជ្ជកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 96,
           TYPE_NAME = N'ពាណិជ្ជកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 97,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 98,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 99,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 101,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT TYPE_ID = 102,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 103,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 104,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT TYPE_ID = 105,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 106,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 107,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 108,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 109,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 110,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 111,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 112,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 113,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT TYPE_ID = 114,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 115,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 116,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT TYPE_ID = 117,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 118,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 119,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 120,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 121,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 122,
           TYPE_NAME = N'បូមទឹកកសិកម្មពេលយប់ ទិញ MV (ម៉ោង៩យប់ដល់៧ព្រឹក)'
    UNION ALL
    SELECT TYPE_ID = 123,
           TYPE_NAME = N'បូមទឹកកសិកម្មពេលយប់ ទិញ LV (ម៉ោង៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូរអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 124,
           TYPE_NAME = N'បូមទឹកកសិកម្មពេលយប់ ទិញ LV (ម៉ោង៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូរអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 125,
           TYPE_NAME = N'បូមទឹកកសិកម្មពេលយប់ ទិញ LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 126,
           TYPE_NAME = N'បូមទឹកកសិកម្មពេលយប់ ទិញ MV តាមអត្រាថ្លៃមធ្យម'
    UNION ALL
    SELECT TYPE_ID = 127,
           TYPE_NAME = N'បូមទឹកកសិកម្មពេលយប់ ទិញ LV ត្រង់ស្វូរអ្នកលក់ តាមអត្រាថ្លៃមធ្យម'
    UNION ALL
    SELECT TYPE_ID = 128,
           TYPE_NAME = N'បូមទឹកកសិកម្មពេលយប់ ទិញ LV ត្រង់ស្វូរអ្នកទិញ តាមអត្រាថ្លៃមធ្យម'
    UNION ALL
    SELECT TYPE_ID = 129,
           TYPE_NAME = N'បូមទឹកកសិកម្មពេលយប់ ទិញ LV សាធារណៈ តាមអត្រាថ្លៃមធ្យម';
    -- BUILD DATA VALUE TABLE
    SELECT i.INVOICE_MONTH,
           i.CURRENCY_ID,
           cc.CURRENCY_SING,
           CUSTOMER_CONNECTION_TYPE_ID = CASE
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID = 1 THEN
                                                 1
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 22 ) THEN
                                                 2
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 23 ) THEN
                                                 3
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 134 ) THEN
                                                 4
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 144 ) THEN
                                                 5
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 135 ) THEN
                                                 6
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 136 ) THEN
                                                 7
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 137 ) THEN
                                                 8
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 138 ) THEN
                                                 9
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 139 ) THEN
                                                 10
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 140 ) THEN
                                                 11
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 ) THEN
                                                 12
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 26 ) THEN
                                                 13
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 27 ) THEN
                                                 14
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 28 ) THEN
                                                 15
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 29 ) THEN
                                                 16
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 30 ) THEN
                                                 17
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 31 ) THEN
                                                 18
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 32 ) THEN
                                                 19
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 33 ) THEN
                                                 20
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 34 ) THEN
                                                 21
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 35 ) THEN
                                                 22
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 36 ) THEN
                                                 23
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 37 ) THEN
                                                 24
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 38 ) THEN
                                                 25
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 39 ) THEN
                                                 26
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 40 ) THEN
                                                 27
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 41 ) THEN
                                                 28
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 42 ) THEN
                                                 29
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 43 ) THEN
                                                 30
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 44 ) THEN
                                                 31
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 45 ) THEN
                                                 32
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 46 ) THEN
                                                 33
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 47 ) THEN
                                                 34
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 48 ) THEN
                                                 35
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 49 ) THEN
                                                 36
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 50 ) THEN
                                                 37
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 51 ) THEN
                                                 38
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 52 ) THEN
                                                 39
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 53 ) THEN
                                                 40
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 54 ) THEN
                                                 41
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 55 ) THEN
                                                 42
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 56 ) THEN
                                                 43
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 57 ) THEN
                                                 44
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 58 ) THEN
                                                 45
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 59 ) THEN
                                                 46
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 60 ) THEN
                                                 47
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 61 ) THEN
                                                 48
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 62 ) THEN
                                                 49
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 63 ) THEN
                                                 50
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 64 ) THEN
                                                 51
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 65 ) THEN
                                                 52
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 66 ) THEN
                                                 53
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 67 ) THEN
                                                 54
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 68 ) THEN
                                                 55
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 69 ) THEN
                                                 56
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 70 ) THEN
                                                 57
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 71 ) THEN
                                                 58
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 72 ) THEN
                                                 59
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 73 ) THEN
                                                 60
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 74 ) THEN
                                                 61
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 75 ) THEN
                                                 62
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 76 ) THEN
                                                 63
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 77 ) THEN
                                                 64
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 78 ) THEN
                                                 65
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 79 ) THEN
                                                 66
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 80 ) THEN
                                                 67
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 81 ) THEN
                                                 68
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 82 ) THEN
                                                 69
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 83 ) THEN
                                                 70
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 84 ) THEN
                                                 71
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 85 ) THEN
                                                 72
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 86 ) THEN
                                                 73
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 87 ) THEN
                                                 74
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 88 ) THEN
                                                 75
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 89 ) THEN
                                                 76
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 90 ) THEN
                                                 77
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 91 ) THEN
                                                 78
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 92 ) THEN
                                                 79
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 93 ) THEN
                                                 80
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 94 ) THEN
                                                 81
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 95 ) THEN
                                                 82
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 96 ) THEN
                                                 83
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 97 ) THEN
                                                 84
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 98 ) THEN
                                                 85
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 99 ) THEN
                                                 86
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 100 ) THEN
                                                 87
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 101 ) THEN
                                                 88
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 102 ) THEN
                                                 89
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 103 ) THEN
                                                 90
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 104 ) THEN
                                                 91
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 105 ) THEN
                                                 92
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 106 ) THEN
                                                 93
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 107 ) THEN
                                                 94
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 108 ) THEN
                                                 95
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 109 ) THEN
                                                 96
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 110 ) THEN
                                                 97
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 111 ) THEN
                                                 98
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 112 ) THEN
                                                 99
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 113 ) THEN
                                                 101
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 114 ) THEN
                                                 102
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 115 ) THEN
                                                 103
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 116 ) THEN
                                                 104
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 117 ) THEN
                                                 105
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 118 ) THEN
                                                 106
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 119 ) THEN
                                                 107
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 120 ) THEN
                                                 108
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 121 ) THEN
                                                 109
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 122 ) THEN
                                                 110
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 123 ) THEN
                                                 111
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 124 ) THEN
                                                 112
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 125 ) THEN
                                                 113
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 126 ) THEN
                                                 114
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 127 ) THEN
                                                 115
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 128 ) THEN
                                                 116
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 129 ) THEN
                                                 117
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 130 ) THEN
                                                 118
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 131 ) THEN
                                                 119
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 132 ) THEN
                                                 120
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 133 ) THEN
                                                 121
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
                                                  AND i.PRICE = 480 THEN
                                                 122
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
                                                  AND i.PRICE = 480 THEN
                                                 123
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
                                                  AND i.PRICE = 480 THEN
                                                 124
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
                                                  AND i.PRICE = 480 THEN
                                                 125
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
                                                  AND i.PRICE = 0.1370 THEN
                                                 126
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
                                                  AND i.PRICE = 0.15048 THEN
                                                 127
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
                                                  AND i.PRICE = 0.14248 THEN
                                                 128
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
                                                  AND i.PRICE = 730 THEN
                                                 129
                                             ELSE
                                                 i.CUSTOMER_CONNECTION_TYPE_ID
                                         END,
           TOTAL_USAGE = i.TOTAL_USAGE + ISNULL(adj.ADJUST_USAGE, 0),
           d.PRICE,
           i.TOTAL_AMOUNT
    INTO #TMP1
    FROM TBL_INVOICE i
        INNER JOIN TBL_CUSTOMER c
            ON c.CUSTOMER_ID = i.CUSTOMER_ID
        INNER JOIN TLKP_CURRENCY cc
            ON cc.CURRENCY_ID = i.CURRENCY_ID
        OUTER APPLY
    (
        SELECT TOP 1
               PRICE
        FROM TBL_INVOICE_DETAIL
        WHERE INVOICE_ID = i.INVOICE_ID
        ORDER BY INVOICE_DETAIL_ID DESC
    ) d
        OUTER APPLY
    (
        SELECT ADJUST_USAGE = SUM(ADJUST_USAGE)
        FROM TBL_INVOICE_ADJUSTMENT
        WHERE INVOICE_ID = i.INVOICE_ID
    ) adj
    WHERE i.INVOICE_MONTH
          BETWEEN @D1 AND @D2
          --AND c.CUSTOMER_CONNECTION_TYPE_ID <> -1 -- USE IN PRODUCTION;
          AND i.IS_SERVICE_BILL = 0
          AND c.CUSTOMER_ID NOT IN
              (
                  SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1
              )
          AND c.IS_REACTIVE = 0
          AND i.INVOICE_STATUS NOT IN ( 3 )
    UNION ALL
    SELECT CONVERT(NVARCHAR(7), CREATE_ON, 126) + '-01',
           b.CURRENCY_ID,
           cc.CURRENCY_SING,
           CUSTOMER_CONNECTION_TYPE_ID = CASE
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID = 1 THEN
                                                 1
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 22 ) THEN
                                                 2
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 23 ) THEN
                                                 3
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 134 ) THEN
                                                 4
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 144 ) THEN
                                                 5
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 135 ) THEN
                                                 6
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 136 ) THEN
                                                 7
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 137 ) THEN
                                                 8
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 138 ) THEN
                                                 9
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 139 ) THEN
                                                 10
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 140 ) THEN
                                                 11
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 9 ) THEN
                                                 12
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 26 ) THEN
                                                 13
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 27 ) THEN
                                                 14
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 28 ) THEN
                                                 15
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 29 ) THEN
                                                 16
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 30 ) THEN
                                                 17
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 31 ) THEN
                                                 18
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 32 ) THEN
                                                 19
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 33 ) THEN
                                                 20
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 34 ) THEN
                                                 21
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 35 ) THEN
                                                 22
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 36 ) THEN
                                                 23
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 37 ) THEN
                                                 24
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 38 ) THEN
                                                 25
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 39 ) THEN
                                                 26
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 40 ) THEN
                                                 27
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 41 ) THEN
                                                 28
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 42 ) THEN
                                                 29
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 43 ) THEN
                                                 30
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 44 ) THEN
                                                 31
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 45 ) THEN
                                                 32
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 46 ) THEN
                                                 33
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 47 ) THEN
                                                 34
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 48 ) THEN
                                                 35
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 49 ) THEN
                                                 36
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 50 ) THEN
                                                 37
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 51 ) THEN
                                                 38
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 52 ) THEN
                                                 39
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 53 ) THEN
                                                 40
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 54 ) THEN
                                                 41
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 55 ) THEN
                                                 42
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 56 ) THEN
                                                 43
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 57 ) THEN
                                                 44
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 58 ) THEN
                                                 45
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 59 ) THEN
                                                 46
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 60 ) THEN
                                                 47
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 61 ) THEN
                                                 48
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 62 ) THEN
                                                 49
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 63 ) THEN
                                                 50
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 64 ) THEN
                                                 51
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 65 ) THEN
                                                 52
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 66 ) THEN
                                                 53
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 67 ) THEN
                                                 54
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 68 ) THEN
                                                 55
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 69 ) THEN
                                                 56
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 70 ) THEN
                                                 57
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 71 ) THEN
                                                 58
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 72 ) THEN
                                                 59
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 73 ) THEN
                                                 60
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 74 ) THEN
                                                 61
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 75 ) THEN
                                                 62
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 76 ) THEN
                                                 63
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 77 ) THEN
                                                 64
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 78 ) THEN
                                                 65
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 79 ) THEN
                                                 66
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 80 ) THEN
                                                 67
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 81 ) THEN
                                                 68
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 82 ) THEN
                                                 69
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 83 ) THEN
                                                 70
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 84 ) THEN
                                                 71
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 85 ) THEN
                                                 72
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 86 ) THEN
                                                 73
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 87 ) THEN
                                                 74
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 88 ) THEN
                                                 75
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 89 ) THEN
                                                 76
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 90 ) THEN
                                                 77
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 91 ) THEN
                                                 78
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 92 ) THEN
                                                 79
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 93 ) THEN
                                                 80
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 94 ) THEN
                                                 81
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 95 ) THEN
                                                 82
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 96 ) THEN
                                                 83
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 97 ) THEN
                                                 84
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 98 ) THEN
                                                 85
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 99 ) THEN
                                                 86
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 100 ) THEN
                                                 87
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 101 ) THEN
                                                 88
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 102 ) THEN
                                                 89
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 103 ) THEN
                                                 90
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 104 ) THEN
                                                 91
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 105 ) THEN
                                                 92
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 106 ) THEN
                                                 93
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 107 ) THEN
                                                 94
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 108 ) THEN
                                                 95
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 109 ) THEN
                                                 96
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 110 ) THEN
                                                 97
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 111 ) THEN
                                                 98
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 112 ) THEN
                                                 99
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 113 ) THEN
                                                 101
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 114 ) THEN
                                                 102
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 115 ) THEN
                                                 103
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 116 ) THEN
                                                 104
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 117 ) THEN
                                                 105
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 118 ) THEN
                                                 106
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 119 ) THEN
                                                 107
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 120 ) THEN
                                                 108
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 121 ) THEN
                                                 109
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 122 ) THEN
                                                 110
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 123 ) THEN
                                                 111
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 124 ) THEN
                                                 112
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 125 ) THEN
                                                 113
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 126 ) THEN
                                                 114
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 127 ) THEN
                                                 115
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 128 ) THEN
                                                 116
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 129 ) THEN
                                                 117
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 130 ) THEN
                                                 118
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 131 ) THEN
                                                 119
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 132 ) THEN
                                                 120
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 133 ) THEN
                                                 121
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
                                                  AND PRICE = 480 THEN
                                                 122
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
                                                  AND PRICE = 480 THEN
                                                 123
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
                                                  AND PRICE = 480 THEN
                                                 124
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
                                                  AND PRICE = 480 THEN
                                                 125
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
                                                  AND PRICE = 0.1370 THEN
                                                 126
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
                                                  AND PRICE = 0.15048 THEN
                                                 127
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
                                                  AND PRICE = 0.14248 THEN
                                                 128
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
                                                  AND PRICE = 730 THEN
                                                 129
                                             ELSE
                                                 CUSTOMER_CONNECTION_TYPE_ID
                                         END,
           b.BUY_QTY,
           b.PRICE,
           b.BUY_AMOUNT
    FROM TBL_CUSTOMER_BUY b
        INNER JOIN TBL_PREPAID_CUSTOMER p
            ON p.PREPAID_CUS_ID = b.PREPAID_CUS_ID
        INNER JOIN TBL_CUSTOMER c
            ON c.CUSTOMER_ID = p.CUSTOMER_ID
        INNER JOIN TLKP_CURRENCY cc
            ON cc.CURRENCY_ID = b.CURRENCY_ID
    WHERE b.CREATE_ON
          BETWEEN @D1 AND @D2
          AND c.CUSTOMER_ID NOT IN
              (
                  SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1
              )
          AND c.IS_REACTIVE = 0
          AND CONVERT(NVARCHAR(7), CREATE_ON, 126) + '-01' <= '2016-02-01'
    UNION ALL
    SELECT cr.CREDIT_MONTH,
           cr.CURRENCY_ID,
           cc.CURRENCY_SING,
           CUSTOMER_CONNECTION_TYPE_ID = CASE
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID = 1 THEN
                                                 1
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 22 ) THEN
                                                 2
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 23 ) THEN
                                                 3
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 134 ) THEN
                                                 4
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 144 ) THEN
                                                 5
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 135 ) THEN
                                                 6
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 136 ) THEN
                                                 7
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 137 ) THEN
                                                 8
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 138 ) THEN
                                                 9
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 139 ) THEN
                                                 10
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 140 ) THEN
                                                 11
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 9 ) THEN
                                                 12
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 26 ) THEN
                                                 13
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 27 ) THEN
                                                 14
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 28 ) THEN
                                                 15
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 29 ) THEN
                                                 16
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 30 ) THEN
                                                 17
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 31 ) THEN
                                                 18
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 32 ) THEN
                                                 19
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 33 ) THEN
                                                 20
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 34 ) THEN
                                                 21
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 35 ) THEN
                                                 22
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 36 ) THEN
                                                 23
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 37 ) THEN
                                                 24
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 38 ) THEN
                                                 25
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 39 ) THEN
                                                 26
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 40 ) THEN
                                                 27
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 41 ) THEN
                                                 28
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 42 ) THEN
                                                 29
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 43 ) THEN
                                                 30
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 44 ) THEN
                                                 31
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 45 ) THEN
                                                 32
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 46 ) THEN
                                                 33
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 47 ) THEN
                                                 34
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 48 ) THEN
                                                 35
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 49 ) THEN
                                                 36
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 50 ) THEN
                                                 37
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 51 ) THEN
                                                 38
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 52 ) THEN
                                                 39
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 53 ) THEN
                                                 40
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 54 ) THEN
                                                 41
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 55 ) THEN
                                                 42
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 56 ) THEN
                                                 43
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 57 ) THEN
                                                 44
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 58 ) THEN
                                                 45
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 59 ) THEN
                                                 46
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 60 ) THEN
                                                 47
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 61 ) THEN
                                                 48
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 62 ) THEN
                                                 49
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 63 ) THEN
                                                 50
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 64 ) THEN
                                                 51
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 65 ) THEN
                                                 52
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 66 ) THEN
                                                 53
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 67 ) THEN
                                                 54
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 68 ) THEN
                                                 55
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 69 ) THEN
                                                 56
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 70 ) THEN
                                                 57
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 71 ) THEN
                                                 58
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 72 ) THEN
                                                 59
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 73 ) THEN
                                                 60
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 74 ) THEN
                                                 61
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 75 ) THEN
                                                 62
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 76 ) THEN
                                                 63
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 77 ) THEN
                                                 64
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 78 ) THEN
                                                 65
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 79 ) THEN
                                                 66
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 80 ) THEN
                                                 67
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 81 ) THEN
                                                 68
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 82 ) THEN
                                                 69
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 83 ) THEN
                                                 70
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 84 ) THEN
                                                 71
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 85 ) THEN
                                                 72
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 86 ) THEN
                                                 73
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 87 ) THEN
                                                 74
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 88 ) THEN
                                                 75
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 89 ) THEN
                                                 76
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 90 ) THEN
                                                 77
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 91 ) THEN
                                                 78
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 92 ) THEN
                                                 79
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 93 ) THEN
                                                 80
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 94 ) THEN
                                                 81
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 95 ) THEN
                                                 82
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 96 ) THEN
                                                 83
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 97 ) THEN
                                                 84
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 98 ) THEN
                                                 85
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 99 ) THEN
                                                 86
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 100 ) THEN
                                                 87
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 101 ) THEN
                                                 88
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 102 ) THEN
                                                 89
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 103 ) THEN
                                                 90
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 104 ) THEN
                                                 91
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 105 ) THEN
                                                 92
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 106 ) THEN
                                                 93
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 107 ) THEN
                                                 94
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 108 ) THEN
                                                 95
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 109 ) THEN
                                                 96
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 110 ) THEN
                                                 97
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 111 ) THEN
                                                 98
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 112 ) THEN
                                                 99
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 113 ) THEN
                                                 101
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 114 ) THEN
                                                 102
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 115 ) THEN
                                                 103
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 116 ) THEN
                                                 104
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 117 ) THEN
                                                 105
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 118 ) THEN
                                                 106
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 119 ) THEN
                                                 107
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 120 ) THEN
                                                 108
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 121 ) THEN
                                                 109
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 122 ) THEN
                                                 110
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 123 ) THEN
                                                 111
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 124 ) THEN
                                                 112
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 125 ) THEN
                                                 113
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 126 ) THEN
                                                 114
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 127 ) THEN
                                                 115
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 128 ) THEN
                                                 116
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 129 ) THEN
                                                 117
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 130 ) THEN
                                                 118
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 131 ) THEN
                                                 119
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 132 ) THEN
                                                 120
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 133 ) THEN
                                                 121
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
                                                  AND PRICE = 480 THEN
                                                 122
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
                                                  AND PRICE = 480 THEN
                                                 123
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
                                                  AND PRICE = 480 THEN
                                                 124
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
                                                  AND PRICE = 480 THEN
                                                 125
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
                                                  AND PRICE = 0.1370 THEN
                                                 126
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
                                                  AND PRICE = 0.15048 THEN
                                                 127
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
                                                  AND PRICE = 0.14248 THEN
                                                 128
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
                                                  AND PRICE = 730 THEN
                                                 129
                                             ELSE
                                                 CUSTOMER_CONNECTION_TYPE_ID
                                         END,
           cr.USAGE,
           PRICE = 800,
           TOTAL_AMOUNT = cr.USAGE * 800
    FROM TBL_PREPAID_CREDIT cr
        INNER JOIN TBL_CUSTOMER c
            ON c.CUSTOMER_ID = cr.CUSTOMER_ID
        INNER JOIN TLKP_CURRENCY cc
            ON cc.CURRENCY_ID = cr.CURRENCY_ID
    WHERE cr.CREDIT_MONTH
          BETWEEN @D1 AND @D2
          AND (cr.CREDIT_MONTH
          BETWEEN '2016-03-01' AND '2017-02-01'
              )
    UNION ALL
    SELECT cr.CREDIT_MONTH,
           cr.CURRENCY_ID,
           cc.CURRENCY_SING,
           CUSTOMER_CONNECTION_TYPE_ID = CASE
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID = 1 THEN
                                                 1
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 22 ) THEN
                                                 2
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 23 ) THEN
                                                 3
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 134 ) THEN
                                                 4
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 144 ) THEN
                                                 5
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 135 ) THEN
                                                 6
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 136 ) THEN
                                                 7
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 137 ) THEN
                                                 8
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 138 ) THEN
                                                 9
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 139 ) THEN
                                                 10
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 140 ) THEN
                                                 11
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 9 ) THEN
                                                 12
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 26 ) THEN
                                                 13
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 27 ) THEN
                                                 14
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 28 ) THEN
                                                 15
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 29 ) THEN
                                                 16
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 30 ) THEN
                                                 17
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 31 ) THEN
                                                 18
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 32 ) THEN
                                                 19
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 33 ) THEN
                                                 20
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 34 ) THEN
                                                 21
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 35 ) THEN
                                                 22
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 36 ) THEN
                                                 23
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 37 ) THEN
                                                 24
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 38 ) THEN
                                                 25
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 39 ) THEN
                                                 26
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 40 ) THEN
                                                 27
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 41 ) THEN
                                                 28
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 42 ) THEN
                                                 29
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 43 ) THEN
                                                 30
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 44 ) THEN
                                                 31
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 45 ) THEN
                                                 32
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 46 ) THEN
                                                 33
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 47 ) THEN
                                                 34
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 48 ) THEN
                                                 35
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 49 ) THEN
                                                 36
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 50 ) THEN
                                                 37
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 51 ) THEN
                                                 38
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 52 ) THEN
                                                 39
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 53 ) THEN
                                                 40
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 54 ) THEN
                                                 41
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 55 ) THEN
                                                 42
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 56 ) THEN
                                                 43
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 57 ) THEN
                                                 44
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 58 ) THEN
                                                 45
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 59 ) THEN
                                                 46
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 60 ) THEN
                                                 47
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 61 ) THEN
                                                 48
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 62 ) THEN
                                                 49
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 63 ) THEN
                                                 50
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 64 ) THEN
                                                 51
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 65 ) THEN
                                                 52
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 66 ) THEN
                                                 53
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 67 ) THEN
                                                 54
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 68 ) THEN
                                                 55
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 69 ) THEN
                                                 56
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 70 ) THEN
                                                 57
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 71 ) THEN
                                                 58
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 72 ) THEN
                                                 59
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 73 ) THEN
                                                 60
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 74 ) THEN
                                                 61
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 75 ) THEN
                                                 62
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 76 ) THEN
                                                 63
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 77 ) THEN
                                                 64
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 78 ) THEN
                                                 65
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 79 ) THEN
                                                 66
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 80 ) THEN
                                                 67
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 81 ) THEN
                                                 68
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 82 ) THEN
                                                 69
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 83 ) THEN
                                                 70
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 84 ) THEN
                                                 71
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 85 ) THEN
                                                 72
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 86 ) THEN
                                                 73
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 87 ) THEN
                                                 74
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 88 ) THEN
                                                 75
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 89 ) THEN
                                                 76
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 90 ) THEN
                                                 77
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 91 ) THEN
                                                 78
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 92 ) THEN
                                                 79
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 93 ) THEN
                                                 80
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 94 ) THEN
                                                 81
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 95 ) THEN
                                                 82
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 96 ) THEN
                                                 83
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 97 ) THEN
                                                 84
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 98 ) THEN
                                                 85
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 99 ) THEN
                                                 86
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 100 ) THEN
                                                 87
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 101 ) THEN
                                                 88
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 102 ) THEN
                                                 89
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 103 ) THEN
                                                 90
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 104 ) THEN
                                                 91
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 105 ) THEN
                                                 92
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 106 ) THEN
                                                 93
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 107 ) THEN
                                                 94
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 108 ) THEN
                                                 95
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 109 ) THEN
                                                 96
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 110 ) THEN
                                                 97
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 111 ) THEN
                                                 98
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 112 ) THEN
                                                 99
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 113 ) THEN
                                                 101
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 114 ) THEN
                                                 102
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 115 ) THEN
                                                 103
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 116 ) THEN
                                                 104
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 117 ) THEN
                                                 105
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 118 ) THEN
                                                 106
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 119 ) THEN
                                                 107
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 120 ) THEN
                                                 108
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 121 ) THEN
                                                 109
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 122 ) THEN
                                                 110
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 123 ) THEN
                                                 111
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 124 ) THEN
                                                 112
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 125 ) THEN
                                                 113
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 126 ) THEN
                                                 114
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 127 ) THEN
                                                 115
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 128 ) THEN
                                                 116
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 129 ) THEN
                                                 117
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 130 ) THEN
                                                 118
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 131 ) THEN
                                                 119
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 132 ) THEN
                                                 120
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 133 ) THEN
                                                 121
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
                                                  AND PRICE = 480 THEN
                                                 122
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
                                                  AND PRICE = 480 THEN
                                                 123
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
                                                  AND PRICE = 480 THEN
                                                 124
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
                                                  AND PRICE = 480 THEN
                                                 125
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
                                                  AND PRICE = 0.1370 THEN
                                                 126
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
                                                  AND PRICE = 0.15048 THEN
                                                 127
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
                                                  AND PRICE = 0.14248 THEN
                                                 128
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
                                                  AND PRICE = 730 THEN
                                                 129
                                             ELSE
                                                 CUSTOMER_CONNECTION_TYPE_ID
                                         END,
           cr.USAGE,
           PRICE = cr.BASED_PRICE,
           TOTAL_AMOUNT = cr.USAGE * BASED_PRICE
    FROM TBL_PREPAID_CREDIT cr
        INNER JOIN TBL_CUSTOMER c
            ON c.CUSTOMER_ID = cr.CUSTOMER_ID
        INNER JOIN TLKP_CURRENCY cc
            ON cc.CURRENCY_ID = cr.CURRENCY_ID
    WHERE cr.CREDIT_MONTH
    BETWEEN @D1 AND @D2;

    -- BUILD CROSS TAB DEFINITION
    SELECT CUSTOMER_CONNECTION_TYPE_ID,
           COL = ROW_NUMBER() OVER (ORDER BY CUSTOMER_CONNECTION_TYPE_ID) % 2,
           ROW = (1 + ROW_NUMBER() OVER (ORDER BY CUSTOMER_CONNECTION_TYPE_ID)) / 2
    INTO #DEF1
    FROM
    (SELECT DISTINCT CUSTOMER_CONNECTION_TYPE_ID FROM #TMP1) x;

    -- TO FILL 12MONTHs DATA 
    INSERT INTO #TMP1
    SELECT m.M,
           t.CURRENCY_ID,
           t.CURRENCY_SING,
           t.CUSTOMER_CONNECTION_TYPE_ID,
           TOTAL_USAGE = 0,
           PRICE = 0,
           TOTAL_AMOUNT = 0
    FROM #MONTHS m,
    (
        SELECT DISTINCT
               CUSTOMER_CONNECTION_TYPE_ID,
               CURRENCY_ID,
               CURRENCY_SING
        FROM #TMP1
    ) t;

    -- IN CASE NO RECORD
    IF
    (
        SELECT COUNT(*)FROM #TMP1
    ) = 0
    BEGIN
        INSERT INTO #TMP1
        SELECT M,
               0,
               0,
               0,
               0,
               0,
               0
        FROM #MONTHS;
        INSERT INTO #DEF1
        SELECT 0,
               1,
               1;
    END;
    -- RENDER RESULT
    SELECT ROW,
           INVOICE_MONTH,
           TYPE_ID1 = MAX(   CASE
                                 WHEN COL = 1 THEN
                                     t.CUSTOMER_CONNECTION_TYPE_ID
                                 ELSE
                                     0
                             END
                         ),
           [TYPE_NAME] = CONVERT(NTEXT,
                                 MAX(   CASE
                                            WHEN COL = 1 THEN
                                                TYPE_NAME
                                            ELSE
                                                ''
                                        END
                                    )
                                ),
           USAGE1 = NULLIF(SUM(   CASE
                                      WHEN COL = 1 THEN
                                          t.TOTAL_USAGE
                                      ELSE
                                          0
                                  END
                              ), 0),
           PRICE1 = NULLIF(MAX(   CASE
                                      WHEN COL = 1 THEN
                                          t.PRICE
                                      ELSE
                                          0
                                  END
                              ), 0),
           CURRENCY_ID1 = MAX(   CASE
                                     WHEN COL = 1 THEN
                                         t.CURRENCY_ID
                                     ELSE
                                         0
                                 END
                             ),
           CURR_SIGN1 = MAX(   CASE
                                   WHEN COL = 1 THEN
                                       t.CURRENCY_SING
                                   ELSE
                                       ''
                               END
                           ),
           AMOUNT1 = NULLIF(SUM(   CASE
                                       WHEN COL = 1 THEN
                                           t.TOTAL_AMOUNT
                                       ELSE
                                           0
                                   END
                               ), 0),
           TYPE_ID2 = MAX(   CASE
                                 WHEN COL = 0 THEN
                                     t.CUSTOMER_CONNECTION_TYPE_ID
                                 ELSE
                                     0
                             END
                         ),
           TYPE_NAME2 = MAX(   CASE
                                   WHEN COL = 0 THEN
                                       TYPE_NAME
                                   ELSE
                                       ''
                               END
                           ),
           USAGE2 = NULLIF(SUM(   CASE
                                      WHEN COL = 0 THEN
                                          t.TOTAL_USAGE
                                      ELSE
                                          0
                                  END
                              ), 0),
           PRICE2 = NULLIF(MAX(   CASE
                                      WHEN COL = 0 THEN
                                          t.PRICE
                                      ELSE
                                          0
                                  END
                              ), 0),
           CURRENCY_ID2 = MAX(   CASE
                                     WHEN COL = 0 THEN
                                         t.CURRENCY_ID
                                     ELSE
                                         0
                                 END
                             ),
           CURR_SIGN2 = MAX(   CASE
                                   WHEN COL = 0 THEN
                                       t.CURRENCY_SING
                                   ELSE
                                       ''
                               END
                           ),
           AMOUNT2 = NULLIF(SUM(   CASE
                                       WHEN COL = 0 THEN
                                           t.TOTAL_AMOUNT
                                       ELSE
                                           0
                                   END
                               ), 0)
    FROM #TMP1 t
        LEFT JOIN #DEF1 d
            ON d.CUSTOMER_CONNECTION_TYPE_ID = t.CUSTOMER_CONNECTION_TYPE_ID
        LEFT JOIN #TMP_TYPE1 c
            ON c.TYPE_ID = t.CUSTOMER_CONNECTION_TYPE_ID
    GROUP BY ROW,
             INVOICE_MONTH
    ORDER BY ROW,
             INVOICE_MONTH;
--END REPORT_QUARTER_SOLD_CONSUMER
END;

ELSE
BEGIN
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'លំនៅដ្ឋាន'
    INTO #TMP_TYPE UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ឧស្សាហកម្ម MV'
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ឧស្សាហកម្ម MV តាមពេលវេលា'
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ឧស្សាហកម្ម MV ពន្លឺព្រះអាទិត្យ'
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'ឧស្សាហកម្ម LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 6,
           TYPE_NAME = N'ឧស្សាហកម្ម LV ត្រង់ស្វូអ្នកលក់តាមពេលវេលា'
    UNION ALL
    SELECT TYPE_ID = 7,
           TYPE_NAME = N'ឧស្សាហកម្ម LV ត្រង់ស្វូអ្នកលក់ពន្លឺព្រះអាទិត្យ'
    UNION ALL
    SELECT TYPE_ID = 8,
           TYPE_NAME = N'ឧស្សាហកម្ម LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 9,
           TYPE_NAME = N'ឧស្សាហកម្ម LV ត្រង់ស្វូអ្នកទិញតាមពេល'
    UNION ALL
    SELECT TYPE_ID = 10,
           TYPE_NAME = N'ឧស្សាហកម្ម LV ត្រង់ស្វូអ្នកទិញពន្លឺព្រះអាទិត្យ'
    UNION ALL
    SELECT TYPE_ID = 11,
           TYPE_NAME = N'ឧស្សាហកម្ម LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 12,
           TYPE_NAME = N'ពាណិជ្ជកម្ម MV'
    UNION ALL
    SELECT TYPE_ID = 13,
           TYPE_NAME = N'ពាណិជ្ជកម្ម MV តាមពេលវេលា'
    UNION ALL
    SELECT TYPE_ID = 14,
           TYPE_NAME = N'ពាណិជ្ជកម្ម MV ពន្លឺព្រះអាទិត្យ'
    UNION ALL
    SELECT TYPE_ID = 15,
           TYPE_NAME = N'ពាណិជ្ជកម្ម LV​ ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 16,
           TYPE_NAME = N'ពាណិជ្ជកម្ម LV ត្រង់ស្វូអ្នកលក់​តាមពេល'
    UNION ALL
    SELECT TYPE_ID = 17,
           TYPE_NAME = N'ពាណិជ្ជកម្ម LV ត្រង់ស្វូអ្នកលក់ពន្លឺព្រះអាទិត្យ'
    UNION ALL
    SELECT TYPE_ID = 18,
           TYPE_NAME = N'ពាណិជ្ជកម្ម LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 19,
           TYPE_NAME = N'ពាណិជ្ជកម្ម LV​ ត្រង់ស្វូអ្នកទិញតាមពេលវេលា'
    UNION ALL
    SELECT TYPE_ID = 20,
           TYPE_NAME = N'ពាណិជ្ជកម្ម LV ត្រង់ស្វូអ្នកទិញពន្លឺព្រះអាទិត្យ'
    UNION ALL
    SELECT TYPE_ID = 21,
           TYPE_NAME = N'ពាណិជ្ជកម្ម LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 22,
           TYPE_NAME = N'អង្គភាពរដ្ឋ LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 23,
           TYPE_NAME = N'សាលារៀន, មន្ទីរពេទ្យ';

    -- BUILD DATA VALUE TABLE
    SELECT i.INVOICE_MONTH,
           i.CURRENCY_ID,
           cc.CURRENCY_SING,
           CUSTOMER_CONNECTION_TYPE_ID = CASE
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID = 1 THEN
                                                 1
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 4 ) THEN
                                                 2
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 7 ) THEN
                                                 3
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 21 ) THEN
                                                 4
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 2 ) THEN
                                                 5
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 18 ) THEN
                                                 6
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 7
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 3 ) THEN
                                                 8
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 17 ) THEN
                                                 9
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 10
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 14 ) THEN
                                                 11
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 8 ) THEN
                                                 12
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 10 ) THEN
                                                 13
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 14
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 5 ) THEN
                                                 15
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 20 ) THEN
                                                 16
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 17
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 6 ) THEN
                                                 18
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 19 ) THEN
                                                 19
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 20
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 15 ) THEN
                                                 21
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 16 ) THEN
                                                 22
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 ) THEN
                                                 23
                                             ELSE
                                                 i.CUSTOMER_CONNECTION_TYPE_ID
                                         END,
           TOTAL_USAGE = i.TOTAL_USAGE + ISNULL(adj.ADJUST_USAGE, 0),
           d.PRICE,
           i.TOTAL_AMOUNT
    INTO #TMP
    FROM TBL_INVOICE i
        INNER JOIN TBL_CUSTOMER c
            ON c.CUSTOMER_ID = i.CUSTOMER_ID
        INNER JOIN TLKP_CURRENCY cc
            ON cc.CURRENCY_ID = i.CURRENCY_ID
        OUTER APPLY
    (
        SELECT TOP 1
               PRICE
        FROM TBL_INVOICE_DETAIL
        WHERE INVOICE_ID = i.INVOICE_ID
        ORDER BY INVOICE_DETAIL_ID DESC
    ) d
        OUTER APPLY
    (
        SELECT ADJUST_USAGE = SUM(ADJUST_USAGE)
        FROM TBL_INVOICE_ADJUSTMENT
        WHERE INVOICE_ID = i.INVOICE_ID
    ) adj
    WHERE i.INVOICE_MONTH
          BETWEEN @D1 AND @D2
          --AND c.CUSTOMER_CONNECTION_TYPE_ID <> -1 -- USE IN PRODUCTION;
          AND i.IS_SERVICE_BILL = 0
          AND c.CUSTOMER_ID NOT IN
              (
                  SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1
              )
          AND c.IS_REACTIVE = 0
          AND i.INVOICE_STATUS NOT IN ( 3 )
    UNION ALL
    SELECT CONVERT(NVARCHAR(7), CREATE_ON, 126) + '-01',
           b.CURRENCY_ID,
           cc.CURRENCY_SING,
           CUSTOMER_CONNECTION_TYPE_ID = CASE
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID = 1 THEN
                                                 1
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 4 ) THEN
                                                 2
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 7 ) THEN
                                                 3
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 21 ) THEN
                                                 4
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 2 ) THEN
                                                 5
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 18 ) THEN
                                                 6
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 7
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 3 ) THEN
                                                 8
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 17 ) THEN
                                                 9
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 10
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 14 ) THEN
                                                 11
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 8 ) THEN
                                                 12
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 10 ) THEN
                                                 13
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 14
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 5 ) THEN
                                                 15
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 20 ) THEN
                                                 16
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 17
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 6 ) THEN
                                                 18
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 19 ) THEN
                                                 19
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 20
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 15 ) THEN
                                                 21
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 16 ) THEN
                                                 22
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 9 ) THEN
                                                 23
                                             ELSE
                                                 CUSTOMER_CONNECTION_TYPE_ID
                                         END,
           b.BUY_QTY,
           b.PRICE,
           b.BUY_AMOUNT
    FROM TBL_CUSTOMER_BUY b
        INNER JOIN TBL_PREPAID_CUSTOMER p
            ON p.PREPAID_CUS_ID = b.PREPAID_CUS_ID
        INNER JOIN TBL_CUSTOMER c
            ON c.CUSTOMER_ID = p.CUSTOMER_ID
        INNER JOIN TLKP_CURRENCY cc
            ON cc.CURRENCY_ID = b.CURRENCY_ID
    WHERE b.CREATE_ON
          BETWEEN @D1 AND @D2
          AND c.CUSTOMER_ID NOT IN
              (
                  SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1
              )
          AND c.IS_REACTIVE = 0
          AND CONVERT(NVARCHAR(7), CREATE_ON, 126) + '-01' <= '2016-02-01'
    UNION ALL
    SELECT cr.CREDIT_MONTH,
           cr.CURRENCY_ID,
           cc.CURRENCY_SING,
           CUSTOMER_CONNECTION_TYPE_ID = CASE
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID = 1 THEN
                                                 1
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 4 ) THEN
                                                 2
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 7 ) THEN
                                                 3
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 21 ) THEN
                                                 4
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 2 ) THEN
                                                 5
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 18 ) THEN
                                                 6
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 7
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 3 ) THEN
                                                 8
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 17 ) THEN
                                                 9
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 10
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 14 ) THEN
                                                 11
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 8 ) THEN
                                                 12
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 10 ) THEN
                                                 13
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 14
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 5 ) THEN
                                                 15
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 20 ) THEN
                                                 16
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 17
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 6 ) THEN
                                                 18
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 19 ) THEN
                                                 19
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 20
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 15 ) THEN
                                                 21
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 16 ) THEN
                                                 22
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 9 ) THEN
                                                 23
                                             ELSE
                                                 CUSTOMER_CONNECTION_TYPE_ID
                                         END,
           cr.USAGE,
           PRICE = 800,
           TOTAL_AMOUNT = cr.USAGE * 800
    FROM TBL_PREPAID_CREDIT cr
        INNER JOIN TBL_CUSTOMER c
            ON c.CUSTOMER_ID = cr.CUSTOMER_ID
        INNER JOIN TLKP_CURRENCY cc
            ON cc.CURRENCY_ID = cr.CURRENCY_ID
    WHERE cr.CREDIT_MONTH
          BETWEEN @D1 AND @D2
          AND (cr.CREDIT_MONTH
          BETWEEN '2016-03-01' AND '2017-02-01'
              )
    UNION ALL
    SELECT cr.CREDIT_MONTH,
           cr.CURRENCY_ID,
           cc.CURRENCY_SING,
           CUSTOMER_CONNECTION_TYPE_ID = CASE
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID = 1 THEN
                                                 1
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 4 ) THEN
                                                 2
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 7 ) THEN
                                                 3
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 21 ) THEN
                                                 4
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 2 ) THEN
                                                 5
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 18 ) THEN
                                                 6
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 7
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 3 ) THEN
                                                 8
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 17 ) THEN
                                                 9
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 10
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 14 ) THEN
                                                 11
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 8 ) THEN
                                                 12
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 10 ) THEN
                                                 13
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 14
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 5 ) THEN
                                                 15
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 20 ) THEN
                                                 16
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 17
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 6 ) THEN
                                                 18
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 19 ) THEN
                                                 19
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 20
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 15 ) THEN
                                                 21
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 16 ) THEN
                                                 22
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 9 ) THEN
                                                 23
                                             ELSE
                                                 CUSTOMER_CONNECTION_TYPE_ID
                                         END,
           cr.USAGE,
           PRICE = cr.BASED_PRICE,
           TOTAL_AMOUNT = cr.USAGE * BASED_PRICE
    FROM TBL_PREPAID_CREDIT cr
        INNER JOIN TBL_CUSTOMER c
            ON c.CUSTOMER_ID = cr.CUSTOMER_ID
        INNER JOIN TLKP_CURRENCY cc
            ON cc.CURRENCY_ID = cr.CURRENCY_ID
    WHERE cr.CREDIT_MONTH
    BETWEEN @D1 AND @D2;

    -- BUILD CROSS TAB DEFINITION
    SELECT CUSTOMER_CONNECTION_TYPE_ID,
           COL = ROW_NUMBER() OVER (ORDER BY CUSTOMER_CONNECTION_TYPE_ID) % 2,
           ROW = (1 + ROW_NUMBER() OVER (ORDER BY CUSTOMER_CONNECTION_TYPE_ID)) / 2
    INTO #DEF
    FROM
    (SELECT DISTINCT CUSTOMER_CONNECTION_TYPE_ID FROM #TMP) x;

    -- TO FILL 12MONTHs DATA 
    INSERT INTO #TMP
    SELECT m.M,
           t.CURRENCY_ID,
           t.CURRENCY_SING,
           t.CUSTOMER_CONNECTION_TYPE_ID,
           TOTAL_USAGE = 0,
           PRICE = 0,
           TOTAL_AMOUNT = 0
    FROM #MONTHS m,
    (
        SELECT DISTINCT
               CUSTOMER_CONNECTION_TYPE_ID,
               CURRENCY_ID,
               CURRENCY_SING
        FROM #TMP
    ) t;

    -- IN CASE NO RECORD
    IF
    (
        SELECT COUNT(*)FROM #TMP
    ) = 0
    BEGIN
        INSERT INTO #TMP
        SELECT M,
               0,
               0,
               0,
               0,
               0,
               0
        FROM #MONTHS;
        INSERT INTO #DEF
        SELECT 0,
               1,
               1;
    END;
    -- RENDER RESULT
    SELECT ROW,
           INVOICE_MONTH,
           TYPE_ID1 = MAX(   CASE
                                 WHEN COL = 1 THEN
                                     t.CUSTOMER_CONNECTION_TYPE_ID
                                 ELSE
                                     0
                             END
                         ),
           [TYPE_NAME] = MAX(   CASE
                                    WHEN COL = 1 THEN
                                        TYPE_NAME
                                    ELSE
                                        ''
                                END
                            ),
           USAGE1 = NULLIF(SUM(   CASE
                                      WHEN COL = 1 THEN
                                          t.TOTAL_USAGE
                                      ELSE
                                          0
                                  END
                              ), 0),
           PRICE1 = NULLIF(MAX(   CASE
                                      WHEN COL = 1 THEN
                                          t.PRICE
                                      ELSE
                                          0
                                  END
                              ), 0),
           CURRENCY_ID1 = MAX(   CASE
                                     WHEN COL = 1 THEN
                                         t.CURRENCY_ID
                                     ELSE
                                         0
                                 END
                             ),
           CURR_SIGN1 = MAX(   CASE
                                   WHEN COL = 1 THEN
                                       t.CURRENCY_SING
                                   ELSE
                                       ''
                               END
                           ),
           AMOUNT1 = NULLIF(SUM(   CASE
                                       WHEN COL = 1 THEN
                                           t.TOTAL_AMOUNT
                                       ELSE
                                           0
                                   END
                               ), 0),
           TYPE_ID2 = MAX(   CASE
                                 WHEN COL = 0 THEN
                                     t.CUSTOMER_CONNECTION_TYPE_ID
                                 ELSE
                                     0
                             END
                         ),
           TYPE_NAME2 = MAX(   CASE
                                   WHEN COL = 0 THEN
                                       TYPE_NAME
                                   ELSE
                                       ''
                               END
                           ),
           USAGE2 = NULLIF(SUM(   CASE
                                      WHEN COL = 0 THEN
                                          t.TOTAL_USAGE
                                      ELSE
                                          0
                                  END
                              ), 0),
           PRICE2 = NULLIF(MAX(   CASE
                                      WHEN COL = 0 THEN
                                          t.PRICE
                                      ELSE
                                          0
                                  END
                              ), 0),
           CURRENCY_ID2 = MAX(   CASE
                                     WHEN COL = 0 THEN
                                         t.CURRENCY_ID
                                     ELSE
                                         0
                                 END
                             ),
           CURR_SIGN2 = MAX(   CASE
                                   WHEN COL = 0 THEN
                                       t.CURRENCY_SING
                                   ELSE
                                       ''
                               END
                           ),
           AMOUNT2 = NULLIF(SUM(   CASE
                                       WHEN COL = 0 THEN
                                           t.TOTAL_AMOUNT
                                       ELSE
                                           0
                                   END
                               ), 0)
    FROM #TMP t
        LEFT JOIN #DEF d
            ON d.CUSTOMER_CONNECTION_TYPE_ID = t.CUSTOMER_CONNECTION_TYPE_ID
        LEFT JOIN #TMP_TYPE c
            ON c.TYPE_ID = t.CUSTOMER_CONNECTION_TYPE_ID
    GROUP BY ROW,
             INVOICE_MONTH
    ORDER BY ROW,
             INVOICE_MONTH;
--END REPORT_QUARTER_SOLD_CONSUMER
END;

GO

IF OBJECT_ID('REPORT_POWER_SOLD_CONSUMER') IS NOT NULL
    DROP PROC dbo.REPORT_POWER_SOLD_CONSUMER;
GO

CREATE PROC dbo.REPORT_POWER_SOLD_CONSUMER
    @MONTH DATETIME = '2021-02-01',
    @START DATETIME = '2021-02-01',
    @END DATETIME = '2021-02-28'
AS
IF (@MONTH >= '2021-01-01')
BEGIN
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'លំនៅដ្ឋាន'
    INTO #TMP_TYPE1 UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'អាជីវកម្មធុនតូច LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'រដ្ឋបាល LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'អាជីវករតូបលក់ដូរក្នុងផ្សារ'
    UNION ALL
    SELECT TYPE_ID = 6,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 7,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 8,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 9,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 10,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 11,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 12,
           TYPE_NAME = N'សាលារៀន, មន្ទីរពេទ្យ, និងមណ្ឌលសុខភាព នៅតំបន់ជនបទ'
    UNION ALL
    SELECT TYPE_ID = 13,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 14,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 15,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 16,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT TYPE_ID = 17,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 18,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 19,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT TYPE_ID = 20,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 21,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 22,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 23,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 24,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 25,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 26,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 27,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 28,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT TYPE_ID = 29,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 30,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 31,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT TYPE_ID = 32,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 33,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 34,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 35,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 36,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 37,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 38,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 39,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 40,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT TYPE_ID = 41,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 42,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 43,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT TYPE_ID = 44,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 45,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 46,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 47,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 48,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 49,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 50,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 51,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 52,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT TYPE_ID = 53,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 54,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 55,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT TYPE_ID = 56,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 57,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 58,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 59,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 60,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 61,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 62,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 63,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 64,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT TYPE_ID = 65,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្មទ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 66,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 67,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT TYPE_ID = 68,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 69,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 70,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 71,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 72,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 73,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 74,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 75,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 76,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT TYPE_ID = 77,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 78,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 79,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT TYPE_ID = 80,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 81,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 82,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 83,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 84,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 85,
           TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 86,
           TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 87,
           TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 88,
           TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT TYPE_ID = 89,
           TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 90,
           TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 91,
           TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT TYPE_ID = 92,
           TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 93,
           TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 94,
           TYPE_NAME = N'ពាណិជ្ជកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 95,
           TYPE_NAME = N'ពាណិជ្ជកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 96,
           TYPE_NAME = N'ពាណិជ្ជកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 97,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 98,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 99,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 101,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT TYPE_ID = 102,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 103,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 104,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT TYPE_ID = 105,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 106,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 107,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 108,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 109,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 110,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 111,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 112,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 113,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT TYPE_ID = 114,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 115,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 116,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT TYPE_ID = 117,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 118,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 119,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 120,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 121,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 122,
           TYPE_NAME = N'បូមទឹកកសិកម្មពេលយប់ ទិញ MV (ម៉ោង៩យប់ដល់៧ព្រឹក)'
    UNION ALL
    SELECT TYPE_ID = 123,
           TYPE_NAME = N'បូមទឹកកសិកម្មពេលយប់ ទិញ LV (ម៉ោង៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូរអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 124,
           TYPE_NAME = N'បូមទឹកកសិកម្មពេលយប់ ទិញ LV (ម៉ោង៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូរអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 125,
           TYPE_NAME = N'បូមទឹកកសិកម្មពេលយប់ ទិញ LV សាធារណៈ (ម៉ោង៩យប់ដល់៧ព្រឹក)'
    UNION ALL
    SELECT TYPE_ID = 126,
           TYPE_NAME = N'បូមទឹកកសិកម្មពេលយប់ ទិញ MV តាមអត្រាថ្លៃមធ្យម'
    UNION ALL
    SELECT TYPE_ID = 127,
           TYPE_NAME = N'បូមទឹកកសិកម្មពេលយប់ ទិញ LV ត្រង់ស្វូរអ្នកលក់ តាមអត្រាថ្លៃមធ្យម'
    UNION ALL
    SELECT TYPE_ID = 128,
           TYPE_NAME = N'បូមទឹកកសិកម្មពេលយប់ ទិញ LV ត្រង់ស្វូរអ្នកទិញ តាមអត្រាថ្លៃមធ្យម'
    UNION ALL
    SELECT TYPE_ID = 129,
           TYPE_NAME = N'បូមទឹកកសិកម្មពេលយប់ ទិញ LV សាធារណៈ តាមអត្រាថ្លៃមធ្យម';

    SELECT *
    INTO #tmpInv1
    FROM dbo.TBL_INVOICE
    WHERE INVOICE_MONTH
          BETWEEN @START AND @END
          AND IS_SERVICE_BILL = 0
          AND INVOICE_STATUS NOT IN ( 3 );

    SELECT id.INVOICE_ID,
           id.PRICE,
           N = ROW_NUMBER() OVER (PARTITION BY id.PRICE ORDER BY id.INVOICE_DETAIL_ID DESC)
    INTO #tmpPrice1
    FROM dbo.TBL_INVOICE_DETAIL id
        INNER JOIN #tmpInv1 i
            ON i.INVOICE_ID = id.INVOICE_ID
    WHERE id.USAGE != 0;

    -- BUILD DATA VALUE TABLE
    SELECT i.INVOICE_MONTH,
           i.CURRENCY_ID,
           cc.CURRENCY_SING,
           CUSTOMER_CONNECTION_TYPE_ID = CASE
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID = 1 THEN
                                                 1
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 22 ) THEN
                                                 2
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 23 ) THEN
                                                 3
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 134 ) THEN
                                                 4
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 144 ) THEN
                                                 5
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 135 ) THEN
                                                 6
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 136 ) THEN
                                                 7
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 137 ) THEN
                                                 8
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 138 ) THEN
                                                 9
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 139 ) THEN
                                                 10
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 140 ) THEN
                                                 11
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 ) THEN
                                                 12
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 26 ) THEN
                                                 13
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 27 ) THEN
                                                 14
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 28 ) THEN
                                                 15
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 29 ) THEN
                                                 16
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 30 ) THEN
                                                 17
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 31 ) THEN
                                                 18
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 32 ) THEN
                                                 19
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 33 ) THEN
                                                 20
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 34 ) THEN
                                                 21
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 35 ) THEN
                                                 22
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 36 ) THEN
                                                 23
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 37 ) THEN
                                                 24
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 38 ) THEN
                                                 25
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 39 ) THEN
                                                 26
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 40 ) THEN
                                                 27
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 41 ) THEN
                                                 28
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 42 ) THEN
                                                 29
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 43 ) THEN
                                                 30
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 44 ) THEN
                                                 31
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 45 ) THEN
                                                 32
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 46 ) THEN
                                                 33
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 47 ) THEN
                                                 34
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 48 ) THEN
                                                 35
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 49 ) THEN
                                                 36
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 50 ) THEN
                                                 37
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 51 ) THEN
                                                 38
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 52 ) THEN
                                                 39
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 53 ) THEN
                                                 40
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 54 ) THEN
                                                 41
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 55 ) THEN
                                                 42
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 56 ) THEN
                                                 43
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 57 ) THEN
                                                 44
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 58 ) THEN
                                                 45
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 59 ) THEN
                                                 46
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 60 ) THEN
                                                 47
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 61 ) THEN
                                                 48
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 62 ) THEN
                                                 49
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 63 ) THEN
                                                 50
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 64 ) THEN
                                                 51
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 65 ) THEN
                                                 52
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 66 ) THEN
                                                 53
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 67 ) THEN
                                                 54
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 68 ) THEN
                                                 55
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 69 ) THEN
                                                 56
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 70 ) THEN
                                                 57
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 71 ) THEN
                                                 58
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 72 ) THEN
                                                 59
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 73 ) THEN
                                                 60
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 74 ) THEN
                                                 61
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 75 ) THEN
                                                 62
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 76 ) THEN
                                                 63
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 77 ) THEN
                                                 64
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 78 ) THEN
                                                 65
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 79 ) THEN
                                                 66
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 80 ) THEN
                                                 67
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 81 ) THEN
                                                 68
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 82 ) THEN
                                                 69
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 83 ) THEN
                                                 70
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 84 ) THEN
                                                 71
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 85 ) THEN
                                                 72
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 86 ) THEN
                                                 73
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 87 ) THEN
                                                 74
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 88 ) THEN
                                                 75
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 89 ) THEN
                                                 76
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 90 ) THEN
                                                 77
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 91 ) THEN
                                                 78
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 92 ) THEN
                                                 79
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 93 ) THEN
                                                 80
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 94 ) THEN
                                                 81
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 95 ) THEN
                                                 82
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 96 ) THEN
                                                 83
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 97 ) THEN
                                                 84
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 98 ) THEN
                                                 85
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 99 ) THEN
                                                 86
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 100 ) THEN
                                                 87
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 101 ) THEN
                                                 88
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 102 ) THEN
                                                 89
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 103 ) THEN
                                                 90
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 104 ) THEN
                                                 91
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 105 ) THEN
                                                 92
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 106 ) THEN
                                                 93
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 107 ) THEN
                                                 94
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 108 ) THEN
                                                 95
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 109 ) THEN
                                                 96
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 110 ) THEN
                                                 97
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 111 ) THEN
                                                 98
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 112 ) THEN
                                                 99
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 113 ) THEN
                                                 101
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 114 ) THEN
                                                 102
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 115 ) THEN
                                                 103
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 116 ) THEN
                                                 104
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 117 ) THEN
                                                 105
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 118 ) THEN
                                                 106
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 119 ) THEN
                                                 107
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 120 ) THEN
                                                 108
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 121 ) THEN
                                                 109
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 122 ) THEN
                                                 110
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 123 ) THEN
                                                 111
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 124 ) THEN
                                                 112
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 125 ) THEN
                                                 113
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 126 ) THEN
                                                 114
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 127 ) THEN
                                                 115
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 128 ) THEN
                                                 116
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 129 ) THEN
                                                 117
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 130 ) THEN
                                                 118
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 131 ) THEN
                                                 119
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 132 ) THEN
                                                 120
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 133 ) THEN
                                                 121
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
                                                  AND i.PRICE = 480 THEN
                                                 122
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
                                                  AND i.PRICE = 480 THEN
                                                 123
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
                                                  AND i.PRICE = 480 THEN
                                                 124
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
                                                  AND i.PRICE = 480 THEN
                                                 125
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
                                                  AND i.PRICE = 0.1370 THEN
                                                 126
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
                                                  AND i.PRICE = 0.15048 THEN
                                                 127
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
                                                  AND i.PRICE = 0.14248 THEN
                                                 128
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
                                                  AND i.PRICE = 730 THEN
                                                 129
                                             ELSE
                                                 i.CUSTOMER_CONNECTION_TYPE_ID
                                         END,
           TOTAL_USAGE = i.TOTAL_USAGE + ISNULL(adj.ADJUST_USAGE, 0),
           d.PRICE,
           i.TOTAL_AMOUNT
    INTO #TMP1
    FROM #tmpInv1 i
        INNER JOIN TBL_CUSTOMER c
            ON c.CUSTOMER_ID = i.CUSTOMER_ID
        INNER JOIN TLKP_CURRENCY cc
            ON cc.CURRENCY_ID = i.CURRENCY_ID
        INNER JOIN #tmpPrice1 d
            ON d.INVOICE_ID = i.INVOICE_ID
        --OUTER APPLY(
        --	SELECT TOP 1 PRICE 
        --	FROM TBL_INVOICE_DETAIL 
        --	WHERE INVOICE_ID = i.INVOICE_ID
        --	ORDER BY INVOICE_DETAIL_ID DESC
        --) d
        OUTER APPLY
    (
        SELECT ADJUST_USAGE = SUM(ADJUST_USAGE)
        FROM TBL_INVOICE_ADJUSTMENT
        WHERE INVOICE_ID = i.INVOICE_ID
    ) adj
    WHERE c.IS_REACTIVE = 0
          --AND i.INVOICE_MONTH BETWEEN @START AND @END 
          --AND c.CUSTOMER_CONNECTION_TYPE_ID <> -1 -- USE IN PRODUCTION;
          --AND i.IS_SERVICE_BILL=0 
          AND c.CUSTOMER_ID NOT IN
              (
                  SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1
              )
    --AND i.INVOICE_STATUS NOT IN (3)
    UNION ALL
    SELECT CONVERT(NVARCHAR(7), CREATE_ON, 126) + '-01',
           b.CURRENCY_ID,
           cc.CURRENCY_SING,
           CUSTOMER_CONNECTION_TYPE_ID = CASE
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID = 1 THEN
                                                 1
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 22 ) THEN
                                                 2
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 23 ) THEN
                                                 3
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 134 ) THEN
                                                 4
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 144 ) THEN
                                                 5
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 135 ) THEN
                                                 6
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 136 ) THEN
                                                 7
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 137 ) THEN
                                                 8
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 138 ) THEN
                                                 9
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 139 ) THEN
                                                 10
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 140 ) THEN
                                                 11
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 9 ) THEN
                                                 12
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 26 ) THEN
                                                 13
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 27 ) THEN
                                                 14
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 28 ) THEN
                                                 15
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 29 ) THEN
                                                 16
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 30 ) THEN
                                                 17
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 31 ) THEN
                                                 18
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 32 ) THEN
                                                 19
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 33 ) THEN
                                                 20
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 34 ) THEN
                                                 21
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 35 ) THEN
                                                 22
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 36 ) THEN
                                                 23
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 37 ) THEN
                                                 24
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 38 ) THEN
                                                 25
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 39 ) THEN
                                                 26
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 40 ) THEN
                                                 27
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 41 ) THEN
                                                 28
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 42 ) THEN
                                                 29
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 43 ) THEN
                                                 30
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 44 ) THEN
                                                 31
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 45 ) THEN
                                                 32
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 46 ) THEN
                                                 33
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 47 ) THEN
                                                 34
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 48 ) THEN
                                                 35
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 49 ) THEN
                                                 36
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 50 ) THEN
                                                 37
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 51 ) THEN
                                                 38
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 52 ) THEN
                                                 39
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 53 ) THEN
                                                 40
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 54 ) THEN
                                                 41
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 55 ) THEN
                                                 42
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 56 ) THEN
                                                 43
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 57 ) THEN
                                                 44
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 58 ) THEN
                                                 45
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 59 ) THEN
                                                 46
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 60 ) THEN
                                                 47
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 61 ) THEN
                                                 48
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 62 ) THEN
                                                 49
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 63 ) THEN
                                                 50
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 64 ) THEN
                                                 51
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 65 ) THEN
                                                 52
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 66 ) THEN
                                                 53
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 67 ) THEN
                                                 54
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 68 ) THEN
                                                 55
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 69 ) THEN
                                                 56
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 70 ) THEN
                                                 57
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 71 ) THEN
                                                 58
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 72 ) THEN
                                                 59
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 73 ) THEN
                                                 60
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 74 ) THEN
                                                 61
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 75 ) THEN
                                                 62
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 76 ) THEN
                                                 63
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 77 ) THEN
                                                 64
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 78 ) THEN
                                                 65
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 79 ) THEN
                                                 66
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 80 ) THEN
                                                 67
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 81 ) THEN
                                                 68
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 82 ) THEN
                                                 69
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 83 ) THEN
                                                 70
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 84 ) THEN
                                                 71
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 85 ) THEN
                                                 72
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 86 ) THEN
                                                 73
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 87 ) THEN
                                                 74
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 88 ) THEN
                                                 75
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 89 ) THEN
                                                 76
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 90 ) THEN
                                                 77
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 91 ) THEN
                                                 78
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 92 ) THEN
                                                 79
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 93 ) THEN
                                                 80
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 94 ) THEN
                                                 81
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 95 ) THEN
                                                 82
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 96 ) THEN
                                                 83
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 97 ) THEN
                                                 84
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 98 ) THEN
                                                 85
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 99 ) THEN
                                                 86
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 100 ) THEN
                                                 87
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 101 ) THEN
                                                 88
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 102 ) THEN
                                                 89
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 103 ) THEN
                                                 90
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 104 ) THEN
                                                 91
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 105 ) THEN
                                                 92
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 106 ) THEN
                                                 93
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 107 ) THEN
                                                 94
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 108 ) THEN
                                                 95
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 109 ) THEN
                                                 96
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 110 ) THEN
                                                 97
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 111 ) THEN
                                                 98
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 112 ) THEN
                                                 99
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 113 ) THEN
                                                 101
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 114 ) THEN
                                                 102
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 115 ) THEN
                                                 103
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 116 ) THEN
                                                 104
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 117 ) THEN
                                                 105
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 118 ) THEN
                                                 106
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 119 ) THEN
                                                 107
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 120 ) THEN
                                                 108
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 121 ) THEN
                                                 109
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 122 ) THEN
                                                 110
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 123 ) THEN
                                                 111
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 124 ) THEN
                                                 112
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 125 ) THEN
                                                 113
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 126 ) THEN
                                                 114
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 127 ) THEN
                                                 115
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 128 ) THEN
                                                 116
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 129 ) THEN
                                                 117
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 130 ) THEN
                                                 118
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 131 ) THEN
                                                 119
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 132 ) THEN
                                                 120
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 133 ) THEN
                                                 121
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
                                                  AND PRICE = 480 THEN
                                                 122
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
                                                  AND PRICE = 480 THEN
                                                 123
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
                                                  AND PRICE = 480 THEN
                                                 124
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
                                                  AND PRICE = 480 THEN
                                                 125
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
                                                  AND PRICE = 0.1370 THEN
                                                 126
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
                                                  AND PRICE = 0.15048 THEN
                                                 127
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
                                                  AND PRICE = 0.14248 THEN
                                                 128
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
                                                  AND PRICE = 730 THEN
                                                 129
                                             ELSE
                                                 CUSTOMER_CONNECTION_TYPE_ID
                                         END,
           b.BUY_QTY,
           b.PRICE,
           b.BUY_AMOUNT
    FROM TBL_CUSTOMER_BUY b
        INNER JOIN TBL_PREPAID_CUSTOMER p
            ON p.PREPAID_CUS_ID = b.PREPAID_CUS_ID
        INNER JOIN TBL_CUSTOMER c
            ON c.CUSTOMER_ID = p.CUSTOMER_ID
        INNER JOIN TLKP_CURRENCY cc
            ON cc.CURRENCY_ID = b.CURRENCY_ID
    WHERE b.CREATE_ON
          BETWEEN @START AND @END
          AND c.CUSTOMER_ID NOT IN
              (
                  SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1
              )
          AND c.IS_REACTIVE = 0
          AND CONVERT(NVARCHAR(7), CREATE_ON, 126) + '-01' <= '2016-02-01'
    UNION ALL
    SELECT cr.CREDIT_MONTH,
           cr.CURRENCY_ID,
           cc.CURRENCY_SING,
           CUSTOMER_CONNECTION_TYPE_ID = CASE
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID = 1 THEN
                                                 1
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 22 ) THEN
                                                 2
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 23 ) THEN
                                                 3
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 134 ) THEN
                                                 4
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 144 ) THEN
                                                 5
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 135 ) THEN
                                                 6
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 136 ) THEN
                                                 7
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 137 ) THEN
                                                 8
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 138 ) THEN
                                                 9
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 139 ) THEN
                                                 10
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 140 ) THEN
                                                 11
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 9 ) THEN
                                                 12
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 26 ) THEN
                                                 13
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 27 ) THEN
                                                 14
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 28 ) THEN
                                                 15
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 29 ) THEN
                                                 16
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 30 ) THEN
                                                 17
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 31 ) THEN
                                                 18
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 32 ) THEN
                                                 19
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 33 ) THEN
                                                 20
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 34 ) THEN
                                                 21
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 35 ) THEN
                                                 22
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 36 ) THEN
                                                 23
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 37 ) THEN
                                                 24
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 38 ) THEN
                                                 25
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 39 ) THEN
                                                 26
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 40 ) THEN
                                                 27
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 41 ) THEN
                                                 28
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 42 ) THEN
                                                 29
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 43 ) THEN
                                                 30
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 44 ) THEN
                                                 31
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 45 ) THEN
                                                 32
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 46 ) THEN
                                                 33
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 47 ) THEN
                                                 34
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 48 ) THEN
                                                 35
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 49 ) THEN
                                                 36
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 50 ) THEN
                                                 37
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 51 ) THEN
                                                 38
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 52 ) THEN
                                                 39
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 53 ) THEN
                                                 40
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 54 ) THEN
                                                 41
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 55 ) THEN
                                                 42
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 56 ) THEN
                                                 43
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 57 ) THEN
                                                 44
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 58 ) THEN
                                                 45
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 59 ) THEN
                                                 46
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 60 ) THEN
                                                 47
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 61 ) THEN
                                                 48
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 62 ) THEN
                                                 49
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 63 ) THEN
                                                 50
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 64 ) THEN
                                                 51
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 65 ) THEN
                                                 52
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 66 ) THEN
                                                 53
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 67 ) THEN
                                                 54
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 68 ) THEN
                                                 55
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 69 ) THEN
                                                 56
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 70 ) THEN
                                                 57
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 71 ) THEN
                                                 58
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 72 ) THEN
                                                 59
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 73 ) THEN
                                                 60
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 74 ) THEN
                                                 61
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 75 ) THEN
                                                 62
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 76 ) THEN
                                                 63
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 77 ) THEN
                                                 64
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 78 ) THEN
                                                 65
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 79 ) THEN
                                                 66
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 80 ) THEN
                                                 67
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 81 ) THEN
                                                 68
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 82 ) THEN
                                                 69
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 83 ) THEN
                                                 70
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 84 ) THEN
                                                 71
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 85 ) THEN
                                                 72
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 86 ) THEN
                                                 73
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 87 ) THEN
                                                 74
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 88 ) THEN
                                                 75
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 89 ) THEN
                                                 76
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 90 ) THEN
                                                 77
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 91 ) THEN
                                                 78
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 92 ) THEN
                                                 79
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 93 ) THEN
                                                 80
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 94 ) THEN
                                                 81
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 95 ) THEN
                                                 82
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 96 ) THEN
                                                 83
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 97 ) THEN
                                                 84
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 98 ) THEN
                                                 85
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 99 ) THEN
                                                 86
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 100 ) THEN
                                                 87
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 101 ) THEN
                                                 88
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 102 ) THEN
                                                 89
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 103 ) THEN
                                                 90
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 104 ) THEN
                                                 91
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 105 ) THEN
                                                 92
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 106 ) THEN
                                                 93
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 107 ) THEN
                                                 94
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 108 ) THEN
                                                 95
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 109 ) THEN
                                                 96
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 110 ) THEN
                                                 97
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 111 ) THEN
                                                 98
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 112 ) THEN
                                                 99
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 113 ) THEN
                                                 101
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 114 ) THEN
                                                 102
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 115 ) THEN
                                                 103
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 116 ) THEN
                                                 104
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 117 ) THEN
                                                 105
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 118 ) THEN
                                                 106
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 119 ) THEN
                                                 107
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 120 ) THEN
                                                 108
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 121 ) THEN
                                                 109
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 122 ) THEN
                                                 110
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 123 ) THEN
                                                 111
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 124 ) THEN
                                                 112
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 125 ) THEN
                                                 113
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 126 ) THEN
                                                 114
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 127 ) THEN
                                                 115
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 128 ) THEN
                                                 116
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 129 ) THEN
                                                 117
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 130 ) THEN
                                                 118
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 131 ) THEN
                                                 119
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 132 ) THEN
                                                 120
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 133 ) THEN
                                                 121
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
                                                  AND PRICE = 480 THEN
                                                 122
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
                                                  AND PRICE = 480 THEN
                                                 123
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
                                                  AND PRICE = 480 THEN
                                                 124
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
                                                  AND PRICE = 480 THEN
                                                 125
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
                                                  AND PRICE = 0.1370 THEN
                                                 126
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
                                                  AND PRICE = 0.15048 THEN
                                                 127
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
                                                  AND PRICE = 0.14248 THEN
                                                 128
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
                                                  AND PRICE = 730 THEN
                                                 129
                                             ELSE
                                                 CUSTOMER_CONNECTION_TYPE_ID
                                         END,
           cr.USAGE,
           PRICE = 800,
           TOTAL_AMOUNT = cr.USAGE * 800
    FROM TBL_PREPAID_CREDIT cr
        INNER JOIN TBL_CUSTOMER c
            ON c.CUSTOMER_ID = cr.CUSTOMER_ID
        INNER JOIN TLKP_CURRENCY cc
            ON cc.CURRENCY_ID = cr.CURRENCY_ID
    WHERE cr.CREDIT_MONTH
          BETWEEN @START AND @END
          AND (cr.CREDIT_MONTH
          BETWEEN '2016-03-01' AND '2017-02-01'
              )
    UNION ALL
    SELECT cr.CREDIT_MONTH,
           cr.CURRENCY_ID,
           cc.CURRENCY_SING,
           CUSTOMER_CONNECTION_TYPE_ID = CASE
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID = 1 THEN
                                                 1
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 22 ) THEN
                                                 2
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 23 ) THEN
                                                 3
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 134 ) THEN
                                                 4
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 144 ) THEN
                                                 5
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 135 ) THEN
                                                 6
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 136 ) THEN
                                                 7
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 137 ) THEN
                                                 8
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 138 ) THEN
                                                 9
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 139 ) THEN
                                                 10
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 140 ) THEN
                                                 11
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 9 ) THEN
                                                 12
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 26 ) THEN
                                                 13
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 27 ) THEN
                                                 14
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 28 ) THEN
                                                 15
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 29 ) THEN
                                                 16
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 30 ) THEN
                                                 17
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 31 ) THEN
                                                 18
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 32 ) THEN
                                                 19
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 33 ) THEN
                                                 20
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 34 ) THEN
                                                 21
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 35 ) THEN
                                                 22
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 36 ) THEN
                                                 23
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 37 ) THEN
                                                 24
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 38 ) THEN
                                                 25
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 39 ) THEN
                                                 26
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 40 ) THEN
                                                 27
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 41 ) THEN
                                                 28
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 42 ) THEN
                                                 29
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 43 ) THEN
                                                 30
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 44 ) THEN
                                                 31
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 45 ) THEN
                                                 32
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 46 ) THEN
                                                 33
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 47 ) THEN
                                                 34
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 48 ) THEN
                                                 35
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 49 ) THEN
                                                 36
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 50 ) THEN
                                                 37
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 51 ) THEN
                                                 38
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 52 ) THEN
                                                 39
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 53 ) THEN
                                                 40
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 54 ) THEN
                                                 41
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 55 ) THEN
                                                 42
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 56 ) THEN
                                                 43
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 57 ) THEN
                                                 44
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 58 ) THEN
                                                 45
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 59 ) THEN
                                                 46
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 60 ) THEN
                                                 47
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 61 ) THEN
                                                 48
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 62 ) THEN
                                                 49
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 63 ) THEN
                                                 50
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 64 ) THEN
                                                 51
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 65 ) THEN
                                                 52
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 66 ) THEN
                                                 53
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 67 ) THEN
                                                 54
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 68 ) THEN
                                                 55
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 69 ) THEN
                                                 56
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 70 ) THEN
                                                 57
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 71 ) THEN
                                                 58
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 72 ) THEN
                                                 59
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 73 ) THEN
                                                 60
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 74 ) THEN
                                                 61
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 75 ) THEN
                                                 62
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 76 ) THEN
                                                 63
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 77 ) THEN
                                                 64
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 78 ) THEN
                                                 65
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 79 ) THEN
                                                 66
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 80 ) THEN
                                                 67
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 81 ) THEN
                                                 68
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 82 ) THEN
                                                 69
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 83 ) THEN
                                                 70
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 84 ) THEN
                                                 71
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 85 ) THEN
                                                 72
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 86 ) THEN
                                                 73
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 87 ) THEN
                                                 74
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 88 ) THEN
                                                 75
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 89 ) THEN
                                                 76
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 90 ) THEN
                                                 77
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 91 ) THEN
                                                 78
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 92 ) THEN
                                                 79
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 93 ) THEN
                                                 80
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 94 ) THEN
                                                 81
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 95 ) THEN
                                                 82
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 96 ) THEN
                                                 83
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 97 ) THEN
                                                 84
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 98 ) THEN
                                                 85
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 99 ) THEN
                                                 86
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 100 ) THEN
                                                 87
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 101 ) THEN
                                                 88
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 102 ) THEN
                                                 89
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 103 ) THEN
                                                 90
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 104 ) THEN
                                                 91
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 105 ) THEN
                                                 92
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 106 ) THEN
                                                 93
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 107 ) THEN
                                                 94
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 108 ) THEN
                                                 95
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 109 ) THEN
                                                 96
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 110 ) THEN
                                                 97
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 111 ) THEN
                                                 98
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 112 ) THEN
                                                 99
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 113 ) THEN
                                                 101
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 114 ) THEN
                                                 102
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 115 ) THEN
                                                 103
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 116 ) THEN
                                                 104
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 117 ) THEN
                                                 105
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 118 ) THEN
                                                 106
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 119 ) THEN
                                                 107
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 120 ) THEN
                                                 108
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 121 ) THEN
                                                 109
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 122 ) THEN
                                                 110
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 123 ) THEN
                                                 111
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 124 ) THEN
                                                 112
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 125 ) THEN
                                                 113
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 126 ) THEN
                                                 114
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 127 ) THEN
                                                 115
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 128 ) THEN
                                                 116
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 129 ) THEN
                                                 117
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 130 ) THEN
                                                 118
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 131 ) THEN
                                                 119
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 132 ) THEN
                                                 120
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 133 ) THEN
                                                 121
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
                                                  AND PRICE = 480 THEN
                                                 122
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
                                                  AND PRICE = 480 THEN
                                                 123
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
                                                  AND PRICE = 480 THEN
                                                 124
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
                                                  AND PRICE = 480 THEN
                                                 125
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
                                                  AND PRICE = 0.1370 THEN
                                                 126
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
                                                  AND PRICE = 0.15048 THEN
                                                 127
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
                                                  AND PRICE = 0.14248 THEN
                                                 128
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
                                                  AND PRICE = 730 THEN
                                                 129
                                             ELSE
                                                 CUSTOMER_CONNECTION_TYPE_ID
                                         END,
           cr.USAGE,
           PRICE = cr.BASED_PRICE,
           TOTAL_AMOUNT = cr.USAGE * BASED_PRICE
    FROM TBL_PREPAID_CREDIT cr
        INNER JOIN TBL_CUSTOMER c
            ON c.CUSTOMER_ID = cr.CUSTOMER_ID
        INNER JOIN TLKP_CURRENCY cc
            ON cc.CURRENCY_ID = cr.CURRENCY_ID
    WHERE cr.CREDIT_MONTH
    BETWEEN @START AND @END;

    -- RENDER RESULT
    SELECT TYPE_ID = MAX(t.CUSTOMER_CONNECTION_TYPE_ID),
           TYPE_NAME = MAX(TYPE_NAME),
           USAGE = NULLIF(SUM(t.TOTAL_USAGE), 0),
           PRICE = NULLIF(MAX(t.PRICE), 0),
           CURRENCY_ID = MAX(t.CURRENCY_ID),
           CURR_SIGN = MAX(t.CURRENCY_SING),
           AMOUNT = NULLIF(SUM(t.TOTAL_AMOUNT), 0)
    FROM #TMP1 t
        LEFT JOIN #TMP_TYPE1 c
            ON c.TYPE_ID = t.CUSTOMER_CONNECTION_TYPE_ID
    GROUP BY TYPE_ID
    ORDER BY TYPE_ID;
--ORDER BY CURRENCY_ID,PRICE
--END REPORT_POWER_SOLD_CONSUMER
END;

ELSE
BEGIN
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'លំនៅដ្ឋាន'
    INTO #TMP_TYPE UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ឧស្សាហកម្ម MV'
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ឧស្សាហកម្ម MV តាមពេលវេលា'
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ឧស្សាហកម្ម LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'ឧស្សាហកម្ម LV ត្រង់ស្វូអ្នកលក់ តាមពេលវេលា'
    UNION ALL
    SELECT TYPE_ID = 6,
           TYPE_NAME = N'ឧស្សាហកម្ម LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 7,
           TYPE_NAME = N'ឧស្សាហកម្ម LV ត្រង់ស្វូអ្នកទិញ តាមពេលវេលា'
    UNION ALL
    SELECT TYPE_ID = 8,
           TYPE_NAME = N'ឧស្សាហកម្ម LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 9,
           TYPE_NAME = N'ពាណិជ្ជកម្ម MV'
    UNION ALL
    SELECT TYPE_ID = 10,
           TYPE_NAME = N'ពាណិជ្ជកម្ម MV តាមពេលវេលា'
    UNION ALL
    SELECT TYPE_ID = 11,
           TYPE_NAME = N'ពាណិជ្ជកម្ម LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 12,
           TYPE_NAME = N'ពាណិជ្ជកម្ម LV ត្រង់ស្វូអ្នកលក់ តាមពេលវេលា'
    UNION ALL
    SELECT TYPE_ID = 13,
           TYPE_NAME = N'ពាណិជ្ជកម្ម LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 14,
           TYPE_NAME = N'ពាណិជ្ជកម្ម LV ត្រង់ស្វូអ្នកទិញ តាមពេលវេលា'
    UNION ALL
    SELECT TYPE_ID = 15,
           TYPE_NAME = N'ពាណិជ្ជកម្ម LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 16,
           TYPE_NAME = N'អង្គភាពរដ្ឋ LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 17,
           TYPE_NAME = N'សាលារៀន, មន្ទីរពេទ្យ';

    SELECT *
    INTO #tmpInv
    FROM dbo.TBL_INVOICE
    WHERE INVOICE_MONTH
          BETWEEN @START AND @END
          AND IS_SERVICE_BILL = 0
          AND INVOICE_STATUS NOT IN ( 3 );

    SELECT id.INVOICE_ID,
           id.PRICE,
           N = ROW_NUMBER() OVER (PARTITION BY id.PRICE ORDER BY id.INVOICE_DETAIL_ID DESC)
    INTO #tmpPrice
    FROM dbo.TBL_INVOICE_DETAIL id
        INNER JOIN #tmpInv i
            ON i.INVOICE_ID = id.INVOICE_ID
    WHERE id.USAGE != 0;

    -- BUILD DATA VALUE TABLE
    SELECT i.INVOICE_MONTH,
           i.CURRENCY_ID,
           cc.CURRENCY_SING,
           CUSTOMER_CONNECTION_TYPE_ID = CASE
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID = 1 THEN
                                                 1
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 4 ) THEN
                                                 2
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 7 ) THEN
                                                 3
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 2 ) THEN
                                                 4
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 18 ) THEN
                                                 5
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 3 ) THEN
                                                 6
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 17 ) THEN
                                                 7
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 14 ) THEN
                                                 8
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 8 ) THEN
                                                 9
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 10 ) THEN
                                                 10
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 5 ) THEN
                                                 11
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 20 ) THEN
                                                 12
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 6 ) THEN
                                                 13
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 19 ) THEN
                                                 14
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 15 ) THEN
                                                 15
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 16 ) THEN
                                                 16
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 ) THEN
                                                 17
                                             ELSE
                                                 i.CUSTOMER_CONNECTION_TYPE_ID
                                         END,
           TOTAL_USAGE = i.TOTAL_USAGE + ISNULL(adj.ADJUST_USAGE, 0),
           d.PRICE,
           i.TOTAL_AMOUNT
    INTO #TMP
    FROM #tmpInv i
        INNER JOIN TBL_CUSTOMER c
            ON c.CUSTOMER_ID = i.CUSTOMER_ID
        INNER JOIN TLKP_CURRENCY cc
            ON cc.CURRENCY_ID = i.CURRENCY_ID
        INNER JOIN #tmpPrice d
            ON d.INVOICE_ID = i.INVOICE_ID
        --OUTER APPLY(
        --	SELECT TOP 1 PRICE 
        --	FROM TBL_INVOICE_DETAIL 
        --	WHERE INVOICE_ID = i.INVOICE_ID
        --	ORDER BY INVOICE_DETAIL_ID DESC
        --) d
        OUTER APPLY
    (
        SELECT ADJUST_USAGE = SUM(ADJUST_USAGE)
        FROM TBL_INVOICE_ADJUSTMENT
        WHERE INVOICE_ID = i.INVOICE_ID
    ) adj
    WHERE c.IS_REACTIVE = 0
          --AND i.INVOICE_MONTH BETWEEN @START AND @END 
          --AND c.CUSTOMER_CONNECTION_TYPE_ID <> -1 -- USE IN PRODUCTION;
          --AND i.IS_SERVICE_BILL=0 
          AND c.CUSTOMER_ID NOT IN
              (
                  SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1
              )
    --AND i.INVOICE_STATUS NOT IN (3)
    UNION ALL
    SELECT CONVERT(NVARCHAR(7), CREATE_ON, 126) + '-01',
           b.CURRENCY_ID,
           cc.CURRENCY_SING,
           CUSTOMER_CONNECTION_TYPE_ID = CASE
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID = 1 THEN
                                                 1
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 4 ) THEN
                                                 2
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 7 ) THEN
                                                 3
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 2 ) THEN
                                                 4
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 18 ) THEN
                                                 5
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 3 ) THEN
                                                 6
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 17 ) THEN
                                                 7
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 14 ) THEN
                                                 8
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 8 ) THEN
                                                 9
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 10 ) THEN
                                                 10
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 5 ) THEN
                                                 11
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 20 ) THEN
                                                 12
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 6 ) THEN
                                                 13
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 19 ) THEN
                                                 14
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 15 ) THEN
                                                 15
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 16 ) THEN
                                                 16
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 9 ) THEN
                                                 17
                                             ELSE
                                                 CUSTOMER_CONNECTION_TYPE_ID
                                         END,
           b.BUY_QTY,
           b.PRICE,
           b.BUY_AMOUNT
    FROM TBL_CUSTOMER_BUY b
        INNER JOIN TBL_PREPAID_CUSTOMER p
            ON p.PREPAID_CUS_ID = b.PREPAID_CUS_ID
        INNER JOIN TBL_CUSTOMER c
            ON c.CUSTOMER_ID = p.CUSTOMER_ID
        INNER JOIN TLKP_CURRENCY cc
            ON cc.CURRENCY_ID = b.CURRENCY_ID
    WHERE b.CREATE_ON
          BETWEEN @START AND @END
          AND c.CUSTOMER_ID NOT IN
              (
                  SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1
              )
          AND c.IS_REACTIVE = 0
          AND CONVERT(NVARCHAR(7), CREATE_ON, 126) + '-01' <= '2016-02-01'
    UNION ALL
    SELECT cr.CREDIT_MONTH,
           cr.CURRENCY_ID,
           cc.CURRENCY_SING,
           CUSTOMER_CONNECTION_TYPE_ID = CASE
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID = 1 THEN
                                                 1
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 4 ) THEN
                                                 2
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 7 ) THEN
                                                 3
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 2 ) THEN
                                                 4
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 18 ) THEN
                                                 5
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 3 ) THEN
                                                 6
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 17 ) THEN
                                                 7
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 14 ) THEN
                                                 8
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 8 ) THEN
                                                 9
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 10 ) THEN
                                                 10
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 5 ) THEN
                                                 11
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 20 ) THEN
                                                 12
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 6 ) THEN
                                                 13
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 19 ) THEN
                                                 14
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 15 ) THEN
                                                 15
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 16 ) THEN
                                                 16
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 9 ) THEN
                                                 17
                                             ELSE
                                                 CUSTOMER_CONNECTION_TYPE_ID
                                         END,
           cr.USAGE,
           PRICE = 800,
           TOTAL_AMOUNT = cr.USAGE * 800
    FROM TBL_PREPAID_CREDIT cr
        INNER JOIN TBL_CUSTOMER c
            ON c.CUSTOMER_ID = cr.CUSTOMER_ID
        INNER JOIN TLKP_CURRENCY cc
            ON cc.CURRENCY_ID = cr.CURRENCY_ID
    WHERE cr.CREDIT_MONTH
          BETWEEN @START AND @END
          AND (cr.CREDIT_MONTH
          BETWEEN '2016-03-01' AND '2017-02-01'
              )
    UNION ALL
    SELECT cr.CREDIT_MONTH,
           cr.CURRENCY_ID,
           cc.CURRENCY_SING,
           CUSTOMER_CONNECTION_TYPE_ID = CASE
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID = 1 THEN
                                                 1
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 4 ) THEN
                                                 2
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 7 ) THEN
                                                 3
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 2 ) THEN
                                                 4
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 18 ) THEN
                                                 5
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 3 ) THEN
                                                 6
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 17 ) THEN
                                                 7
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 14 ) THEN
                                                 8
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 8 ) THEN
                                                 9
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 10 ) THEN
                                                 10
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 5 ) THEN
                                                 11
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 20 ) THEN
                                                 12
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 6 ) THEN
                                                 13
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 19 ) THEN
                                                 14
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 15 ) THEN
                                                 15
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 16 ) THEN
                                                 16
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 9 ) THEN
                                                 17
                                             ELSE
                                                 CUSTOMER_CONNECTION_TYPE_ID
                                         END,
           cr.USAGE,
           PRICE = cr.BASED_PRICE,
           TOTAL_AMOUNT = cr.USAGE * BASED_PRICE
    FROM TBL_PREPAID_CREDIT cr
        INNER JOIN TBL_CUSTOMER c
            ON c.CUSTOMER_ID = cr.CUSTOMER_ID
        INNER JOIN TLKP_CURRENCY cc
            ON cc.CURRENCY_ID = cr.CURRENCY_ID
    WHERE cr.CREDIT_MONTH
    BETWEEN @START AND @END;

    -- RENDER RESULT
    SELECT TYPE_ID = MAX(t.CUSTOMER_CONNECTION_TYPE_ID),
           TYPE_NAME = MAX(TYPE_NAME),
           USAGE = NULLIF(SUM(t.TOTAL_USAGE), 0),
           PRICE = NULLIF(MAX(t.PRICE), 0),
           CURRENCY_ID = MAX(t.CURRENCY_ID),
           CURR_SIGN = MAX(t.CURRENCY_SING),
           AMOUNT = NULLIF(SUM(t.TOTAL_AMOUNT), 0)
    FROM #TMP t
        LEFT JOIN #TMP_TYPE c
            ON c.TYPE_ID = t.CUSTOMER_CONNECTION_TYPE_ID
    GROUP BY TYPE_ID
    ORDER BY TYPE_ID;
--ORDER BY CURRENCY_ID,PRICE
--END REPORT_POWER_SOLD_CONSUMER

END;
GO

IF OBJECT_ID('REPORT_QUARTER_GENERATION_FACILITY') IS NOT NULL
    DROP PROC dbo.REPORT_QUARTER_GENERATION_FACILITY;

GO

CREATE PROC [dbo].[REPORT_QUARTER_GENERATION_FACILITY] @DATE DATETIME = '2021-01-01'
AS
DECLARE @D1 DATETIME,
        @D2 DATETIME;
SET @DATE = DATEADD(D, 0, DATEDIFF(D, 0, @DATE));
SET @D1 = @DATE;
SET @D2 = DATEADD(S, -1, DATEADD(M, 3, @DATE));


SELECT TOP 7
       x.*,
       OPERATION_PERIOD = OPERATION_TIME,
       METER_EXISTS = HAVE_METER,
       METER_INSTALLED_YEAR = CASE
                                  WHEN HAVE_METER = 1 THEN
                                      CONVERT(NVARCHAR(4), YEAR_METER, 126)
                                  ELSE
                                      ''
                              END
FROM
(
    SELECT CAPACITY_NAME = c.CAPACITY,
           NUMBER = COUNT(*),
           FUEL_TYPE_NAME,
           REMARK = MAX(NOTE)
    FROM TBL_GENERATOR g
        INNER JOIN TBL_CAPACITY c
            ON c.CAPACITY_ID = g.CAPACITY_ID
        INNER JOIN TLKP_FUEL_TYPE f
            ON g.FUEL_TYPE_ID = f.FUEL_TYPE_ID
    WHERE g.IS_ACTIVE = 1
          AND (START_DATE <= @D2)
          AND
          (
              IS_INUSED = 1
              OR END_DATE > @D1
          )
    GROUP BY CAPACITY,
             FUEL_TYPE_NAME
    UNION ALL
    SELECT NULL,
           NULL,
           NULL,
           NULL
    UNION ALL
    SELECT NULL,
           NULL,
           NULL,
           NULL
    UNION ALL
    SELECT NULL,
           NULL,
           NULL,
           NULL
    UNION ALL
    SELECT NULL,
           NULL,
           NULL,
           NULL
    UNION ALL
    SELECT NULL,
           NULL,
           NULL,
           NULL
    UNION ALL
    SELECT NULL,
           NULL,
           NULL,
           NULL
    UNION ALL
    SELECT NULL,
           NULL,
           NULL,
           NULL
) x
    CROSS JOIN TBL_GENERATOR_OPERATION g;

GO
IF OBJECT_ID('[ANR].[RUN_AS_01A]') IS NOT NULL
    DROP PROC [ANR].[RUN_AS_01A];

GO

CREATE PROC [ANR].[RUN_AS_01A]
    @YEAR_ID INT = 2015,
    @CURRENCY_ID INT = 1
AS
-- CLEAR LAST RESULT
DELETE ANR.TBL_RESULT_01A
WHERE YEAR_ID = @YEAR_ID;

SELECT ID = ROW_NUMBER() OVER (ORDER BY GENERATOR_ID),
       GENERATOR_ID
INTO #TMP_GENERATOR
FROM TBL_GENERATOR
WHERE IS_INUSED = 1;

-- MODEL
INSERT INTO ANR.TBL_RESULT_01A
SELECT YEAR_ID = @YEAR_ID,
       ROW_NO = '1',
       DESCRIPTION = N'ម៉ាក',
       UNIT = '',
       G1 = MAX(   CASE
                       WHEN t.ID = 1 THEN
                           GENERATOR_NO
                       ELSE
                           ''
                   END
               ),
       G2 = MAX(   CASE
                       WHEN t.ID = 2 THEN
                           GENERATOR_NO
                       ELSE
                           ''
                   END
               ),
       G3 = MAX(   CASE
                       WHEN t.ID = 3 THEN
                           GENERATOR_NO
                       ELSE
                           ''
                   END
               ),
       G4 = MAX(   CASE
                       WHEN t.ID = 4 THEN
                           GENERATOR_NO
                       ELSE
                           ''
                   END
               ),
       G5 = MAX(   CASE
                       WHEN t.ID = 5 THEN
                           GENERATOR_NO
                       ELSE
                           ''
                   END
               ),
       G6 = MAX(   CASE
                       WHEN t.ID = 6 THEN
                           GENERATOR_NO
                       ELSE
                           ''
                   END
               ),
       TOTAL = '',
       ACTIVE = 1
FROM TBL_GENERATOR g
    INNER JOIN #TMP_GENERATOR t
        ON t.GENERATOR_ID = g.GENERATOR_ID
WHERE IS_INUSED = 1;

-- HORSE_POWER
INSERT INTO ANR.TBL_RESULT_01A
SELECT YEAR_ID = @YEAR_ID,
       ROW_NO = '2',
       DESCRIPTION = N'អានុភាពម៉ាស៊ីនអូស',
       UNIT = 'HP',
       G1 = CONVERT(NVARCHAR,
                    NULLIF(CONVERT(DECIMAL(18, 2),
                                   MAX(   CASE
                                              WHEN t.ID = 1 THEN
                                                  HORSE_POWER
                                              ELSE
                                                  0
                                          END
                                      )
                                  ), 0)
                   ),
       G2 = CONVERT(NVARCHAR,
                    NULLIF(CONVERT(DECIMAL(18, 2),
                                   MAX(   CASE
                                              WHEN t.ID = 2 THEN
                                                  HORSE_POWER
                                              ELSE
                                                  0
                                          END
                                      )
                                  ), 0)
                   ),
       G3 = CONVERT(NVARCHAR,
                    NULLIF(CONVERT(DECIMAL(18, 2),
                                   MAX(   CASE
                                              WHEN t.ID = 3 THEN
                                                  HORSE_POWER
                                              ELSE
                                                  0
                                          END
                                      )
                                  ), 0)
                   ),
       G4 = CONVERT(NVARCHAR,
                    NULLIF(CONVERT(DECIMAL(18, 2),
                                   MAX(   CASE
                                              WHEN t.ID = 4 THEN
                                                  HORSE_POWER
                                              ELSE
                                                  0
                                          END
                                      )
                                  ), 0)
                   ),
       G5 = CONVERT(NVARCHAR,
                    NULLIF(CONVERT(DECIMAL(18, 2),
                                   MAX(   CASE
                                              WHEN t.ID = 5 THEN
                                                  HORSE_POWER
                                              ELSE
                                                  0
                                          END
                                      )
                                  ), 0)
                   ),
       G6 = CONVERT(NVARCHAR,
                    NULLIF(CONVERT(DECIMAL(18, 2),
                                   MAX(   CASE
                                              WHEN t.ID = 6 THEN
                                                  HORSE_POWER
                                              ELSE
                                                  0
                                          END
                                      )
                                  ), 0)
                   ),
       TOTAL = CONVERT(NVARCHAR, NULLIF(CONVERT(DECIMAL(18, 2), SUM(HORSE_POWER)), 0)),
       ACTIVE = 1
FROM TBL_GENERATOR g
    INNER JOIN #TMP_GENERATOR t
        ON t.GENERATOR_ID = g.GENERATOR_ID
WHERE IS_INUSED = 1;

-- CAPACITY
INSERT INTO ANR.TBL_RESULT_01A
SELECT YEAR_ID = @YEAR_ID,
       ROW_NO = '3',
       DESCRIPTION = N'អានុភាពអាល់ទែរណាទ័រ',
       UNIT = N'kVA',
       G1 = NULLIF(MAX(   CASE
                              WHEN t.ID = 1 THEN
                                  REPLACE(CAPACITY, 'kVA', '')
                              ELSE
                                  0
                          END
                      ), 0),
       G2 = NULLIF(MAX(   CASE
                              WHEN t.ID = 2 THEN
                                  REPLACE(CAPACITY, 'kVA', '')
                              ELSE
                                  0
                          END
                      ), 0),
       G3 = NULLIF(MAX(   CASE
                              WHEN t.ID = 3 THEN
                                  REPLACE(CAPACITY, 'kVA', '')
                              ELSE
                                  0
                          END
                      ), 0),
       G4 = NULLIF(MAX(   CASE
                              WHEN t.ID = 4 THEN
                                  REPLACE(CAPACITY, 'kVA', '')
                              ELSE
                                  0
                          END
                      ), 0),
       G5 = NULLIF(MAX(   CASE
                              WHEN t.ID = 5 THEN
                                  REPLACE(CAPACITY, 'kVA', '')
                              ELSE
                                  0
                          END
                      ), 0),
       G6 = NULLIF(MAX(   CASE
                              WHEN t.ID = 6 THEN
                                  REPLACE(CAPACITY, 'kVA', '')
                              ELSE
                                  0
                          END
                      ), 0),
       TOTAL = '',
       ACTIVE = 1
FROM TBL_GENERATOR g
    INNER JOIN TBL_CAPACITY c
        ON g.CAPACITY_ID = c.CAPACITY_ID
    INNER JOIN #TMP_GENERATOR t
        ON t.GENERATOR_ID = g.GENERATOR_ID
WHERE IS_INUSED = 1;

-- Cos
INSERT INTO ANR.TBL_RESULT_01A
SELECT YEAR_ID = @YEAR_ID,
       ROW_NO = '4',
       DESCRIPTION = N'Cos',
       UNIT = '',
       G1 = MAX(   CASE
                       WHEN t.ID = 1 THEN
                           '0.8'
                       ELSE
                           ''
                   END
               ),
       G2 = MAX(   CASE
                       WHEN t.ID = 2 THEN
                           '0.8'
                       ELSE
                           ''
                   END
               ),
       G3 = MAX(   CASE
                       WHEN t.ID = 3 THEN
                           '0.8'
                       ELSE
                           ''
                   END
               ),
       G4 = MAX(   CASE
                       WHEN t.ID = 4 THEN
                           '0.8'
                       ELSE
                           ''
                   END
               ),
       G5 = MAX(   CASE
                       WHEN t.ID = 5 THEN
                           '0.8'
                       ELSE
                           ''
                   END
               ),
       G6 = MAX(   CASE
                       WHEN t.ID = 6 THEN
                           '0.8'
                       ELSE
                           ''
                   END
               ),
       TOTAL = '',
       ACTIVE = 1
FROM TBL_GENERATOR g
    INNER JOIN #TMP_GENERATOR t
        ON t.GENERATOR_ID = g.GENERATOR_ID
WHERE IS_INUSED = 1;

-- OPERATION_HOUR
INSERT INTO ANR.TBL_RESULT_01A
SELECT YEAR_ID = @YEAR_ID,
       ROW_NO = '5',
       DESCRIPTION = N'ចំនួនម៉ោងដំណើរការ/ថ្ងៃ',
       UNIT = 'h',
       G1 = SUM(   CASE
                       WHEN t.ID = 1 THEN
                           OPERATION_HOUR
                       ELSE
                           NULL
                   END
               ),
       G2 = SUM(   CASE
                       WHEN t.ID = 2 THEN
                           OPERATION_HOUR
                       ELSE
                           NULL
                   END
               ),
       G3 = SUM(   CASE
                       WHEN t.ID = 3 THEN
                           OPERATION_HOUR
                       ELSE
                           NULL
                   END
               ),
       G4 = SUM(   CASE
                       WHEN t.ID = 4 THEN
                           OPERATION_HOUR
                       ELSE
                           NULL
                   END
               ),
       G5 = SUM(   CASE
                       WHEN t.ID = 5 THEN
                           OPERATION_HOUR
                       ELSE
                           NULL
                   END
               ),
       G6 = SUM(   CASE
                       WHEN t.ID = 6 THEN
                           OPERATION_HOUR
                       ELSE
                           NULL
                   END
               ),
       TOTAL = CASE
                   WHEN SUM(OPERATION_HOUR) > 24 THEN
                       24
                   ELSE
                       NULLIF(SUM(OPERATION_HOUR), 0)
               END,
       ACTIVE = 1
FROM TBL_GENERATOR g
    INNER JOIN #TMP_GENERATOR t
        ON t.GENERATOR_ID = g.GENERATOR_ID
WHERE IS_INUSED = 1;

-- SHIFT_1
INSERT INTO ANR.TBL_RESULT_01A
SELECT YEAR_ID = @YEAR_ID,
       ROW_NO = '',
       DESCRIPTION = N'ពីម៉ោង ....ទៅ ម៉ោង....',
       UNIT = '',
       G1 = MAX(   CASE
                       WHEN t.ID = 1 THEN
                           SHIFT_1
                       ELSE
                           ''
                   END
               ),
       G2 = MAX(   CASE
                       WHEN t.ID = 2 THEN
                           SHIFT_1
                       ELSE
                           ''
                   END
               ),
       G3 = MAX(   CASE
                       WHEN t.ID = 3 THEN
                           SHIFT_1
                       ELSE
                           ''
                   END
               ),
       G4 = MAX(   CASE
                       WHEN t.ID = 4 THEN
                           SHIFT_1
                       ELSE
                           ''
                   END
               ),
       G5 = MAX(   CASE
                       WHEN t.ID = 5 THEN
                           SHIFT_1
                       ELSE
                           ''
                   END
               ),
       G6 = MAX(   CASE
                       WHEN t.ID = 6 THEN
                           SHIFT_1
                       ELSE
                           ''
                   END
               ),
       TOTAL = '',
       ACTIVE = 1
FROM TBL_GENERATOR g
    INNER JOIN #TMP_GENERATOR t
        ON t.GENERATOR_ID = g.GENERATOR_ID
WHERE IS_INUSED = 1;

-- SHIFT_2
INSERT INTO ANR.TBL_RESULT_01A
SELECT YEAR_ID = @YEAR_ID,
       ROW_NO = '',
       DESCRIPTION = N'ពីម៉ោង ....ទៅ ម៉ោង....',
       UNIT = '',
       G1 = MAX(   CASE
                       WHEN t.ID = 1 THEN
                           SHIFT_2
                       ELSE
                           ''
                   END
               ),
       G2 = MAX(   CASE
                       WHEN t.ID = 2 THEN
                           SHIFT_2
                       ELSE
                           ''
                   END
               ),
       G3 = MAX(   CASE
                       WHEN t.ID = 3 THEN
                           SHIFT_2
                       ELSE
                           ''
                   END
               ),
       G4 = MAX(   CASE
                       WHEN t.ID = 4 THEN
                           SHIFT_2
                       ELSE
                           ''
                   END
               ),
       G5 = MAX(   CASE
                       WHEN t.ID = 5 THEN
                           SHIFT_2
                       ELSE
                           ''
                   END
               ),
       G6 = MAX(   CASE
                       WHEN t.ID = 6 THEN
                           SHIFT_2
                       ELSE
                           ''
                   END
               ),
       TOTAL = '',
       ACTIVE = 1
FROM TBL_GENERATOR g
    INNER JOIN #TMP_GENERATOR t
        ON t.GENERATOR_ID = g.GENERATOR_ID
WHERE IS_INUSED = 1;

DROP TABLE #TMP_GENERATOR;

GO

IF OBJECT_ID('RUN_REF_03A_2020') IS NOT NULL
    DROP PROC RUN_REF_03A_2020;
GO

CREATE PROC dbo.RUN_REF_03A_2020
    @DATA_MONTH DATETIME = '2020-07-01',
    @BILLING_CYCLE_ID INT = 0,
    @AREA_ID INT = 0
AS
/*

@2019-12-03 By Hor Dara
	Update REF 2020
	.--FIll all data with 730 base price even school type is 610 to include 1 row
@2020-07-28 By Hor Dara
	Update REF 2020
	--validate for by license type market vendor even usage over 2000 still have subsidy
*/
-- CLEAR DATA
DELETE FROM TBL_REF_03A
WHERE DATA_MONTH = @DATA_MONTH;

DECLARE @BASED_TARIFF DECIMAL(18, 5),
        @SUBSIDY_TARIFF DECIMAL(18, 5),
        @LICENSE_TYPE_ID INT,
        @TYPE_SALE_CUSTOMER_SUBSIDY NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_SUBSIDY_MARKET_VENDOR NVARCHAR(200);
SET @TYPE_SALE_CUSTOMER_SUBSIDY = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវ ទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ ';
SET @TYPE_SALE_CUSTOMER_SUBSIDY_MARKET_VENDOR = N'លក់ឲ្យអតិថិជនក្នុងផ្សារ';
SELECT @BASED_TARIFF = BASED_TARIFF,
       @SUBSIDY_TARIFF = SUBSIDY_TARIFF,
       @LICENSE_TYPE_ID = LICENSE_TYPE_ID
FROM TBL_TARIFF
WHERE DATA_MONTH = @DATA_MONTH
      AND IS_ACTIVE = 1;

IF (@LICENSE_TYPE_ID <> 2)
BEGIN
    -- for Normal Customer(TOTAL_USAGE<2001)
    SELECT TOTAL_SOLD = SUM(TOTAL_USAGE),
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN
    INTO #TMP
    FROM TBL_REF_INVOICE i
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 14, 15, 16 )
          AND TOTAL_USAGE <= 2000
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Normal Customer (when tariff rate lower than zero)
    UNION ALL
    SELECT TOTAL_SOLD = SUM(TOTAL_USAGE),
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN
    FROM TBL_REF_INVOICE i
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND TOTAL_USAGE > 200
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Normal Customer (when tariff rate equal or greater than zero)
    UNION ALL
    SELECT TOTAL_SOLD = SUM(TOTAL_USAGE),
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN
    FROM TBL_REF_INVOICE i
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for School, Hospital, Health Center at countryside (when tariff rate < 0 ,exclude)
    UNION ALL
    SELECT TOTAL_SOLD = SUM(TOTAL_USAGE),
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN
    FROM TBL_REF_INVOICE i
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN;

    INSERT INTO TBL_REF_03A
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_SUBSIDY,
           TOTAL_SOLD = SUM(TOTAL_SOLD),
           RATE = 730, --FIll all data with 730 base price even school type is 610 to include 1 row
           BASED_TARIFF = @BASED_TARIFF,
           SUBSIDY_TARIFF = @SUBSIDY_TARIFF,
           SUBSIDY_RATE = @BASED_TARIFF - @SUBSIDY_TARIFF,
           SUBSIDY_AMOUNT = (@BASED_TARIFF - @SUBSIDY_TARIFF) * SUM(TOTAL_SOLD),
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM #TMP
    GROUP BY CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN;

    -- FILL BLANK RESULT
    IF NOT EXISTS (SELECT * FROM TBL_REF_03A WHERE DATA_MONTH = @DATA_MONTH)
        INSERT INTO TBL_REF_03A
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'1',
               TYPE_OF_SALE_ID = 1,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_SUBSIDY,
               TOTAL_SOLD = 0,
               RATE = 0,
               BASED_TARIFF = 0,
               SUBSIDY_TARIFF = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = 1,
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1;
END;
ELSE IF (@LICENSE_TYPE_ID = 2)
BEGIN
    -- for MARKET VENDOR
    SELECT TOTAL_SOLD = SUM(TOTAL_USAGE),
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_MARKET_VENDOR = 1
    INTO #TMP_MARKET_VENDOR
    FROM TBL_REF_INVOICE i
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 14, 15, 16 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN;

    INSERT INTO TBL_REF_03A
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_SUBSIDY_MARKET_VENDOR,
           TOTAL_SOLD = SUM(TOTAL_SOLD),
           RATE = 730,
           BASED_TARIFF = @BASED_TARIFF,
           SUBSIDY_TARIFF = @SUBSIDY_TARIFF,
           SUBSIDY_RATE = @BASED_TARIFF - @SUBSIDY_TARIFF,
           SUBSIDY_AMOUNT = (@BASED_TARIFF - @SUBSIDY_TARIFF) * SUM(TOTAL_SOLD),
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM #TMP_MARKET_VENDOR
    WHERE IS_MARKET_VENDOR = 1
    GROUP BY CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN;

    -- FILL BLANK RESULT
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03A
        WHERE DATA_MONTH = @DATA_MONTH
              AND TYPE_OF_SALE_ID = 1
    )
        INSERT INTO TBL_REF_03A
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'1',
               TYPE_OF_SALE_ID = 1,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_SUBSIDY_MARKET_VENDOR,
               TOTAL_SOLD = 0,
               RATE = 0,
               BASED_TARIFF = 0,
               SUBSIDY_TARIFF = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = 1,
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1;
END;
--END OF RUN_REF_03A_2020

GO

IF OBJECT_ID('REPORT_REF_INVOICE') IS NOT NULL
    DROP PROC REPORT_REF_INVOICE;
GO

CREATE PROC [dbo].[REPORT_REF_INVOICE]
    @DATA_MONTH DATETIME = '2020-12-01',
    @CUSTOMER_GROUP_ID INT = 0,
    @CUSTOMER_CONNECTION_TYPE_ID INT = 0,
    @CURRENCY_ID INT = 0,
    @V1 DECIMAL(18, 4) = 0,
    @V2 DECIMAL(18, 4) = 1000000,
    @BILLING_CYCLE_ID INT = 0,
    @AREA_ID INT = 0
AS
/*
@ 2016-05-25 Morm Raksmey
	Add Column Billing Cycle Id and Billing Cycle Name
@ 2017-03-10 Sieng Sothera
	Trim meter code without '0'
@ 2017-04-19 Sieng Sotheara
	Update REF Invoice 2016 & 2017
@ 2018-09-19 Pov Lyhorng
	fix Organization customer is non subsidy type
@ 2019-01-16 Phuong Sovathvong
	Update REF Invoice 2019
@ 2019-12-12 HOR DARA
	Update REF Invoice 2020
@ 2020-07-28 HOR DARA
	Update REF Invoice 2020 - market vendor get subsidy even over 2000kwh (49 licensee)
@ 2021-01-08 Vonn kimputhmunyvorn
	Update REF Invoice 2021
*/
DECLARE @LICENSE_TYPE_ID INT,
        @BASED_TARIFF DECIMAL(18, 5),
        @SUBSIDY_TARIFF DECIMAL(18, 5);
SELECT @LICENSE_TYPE_ID = LICENSE_TYPE_ID,
       @SUBSIDY_TARIFF = SUBSIDY_TARIFF,
       @BASED_TARIFF = BASED_TARIFF
FROM dbo.TBL_TARIFF
WHERE DATA_MONTH = @DATA_MONTH;
CREATE TABLE #RESULT
(
    TYPE_ID INT,
    TYPE_NAME NVARCHAR(255),
    TYPE_OF_SALE_ID INT,
    TYPE_OF_SALE_NAME NVARCHAR(255),
    HISTORY_ID BIGINT,
    CUSTOMER_GROUP_ID INT,
    CUSTOMER_GROUP_NAME NVARCHAR(200),
    INVOICE_ID BIGINT,
    DATA_MONTH DATETIME,
    CUSTOMER_ID INT,
    CUSTOMER_CODE NVARCHAR(50),
    FIRST_NAME_KH NVARCHAR(50),
    LAST_NAME_KH NVARCHAR(50),
    AREA_ID INT,
    AREA_CODE NVARCHAR(50),
    BOX_ID INT,
    BOX_CODE NVARCHAR(50),
    CUSTOMER_CONNECTION_TYPE_ID INT,
    CUSTOMER_CONNECTION_TYPE_NAME NVARCHAR(200),
    AMPARE_ID INT,
    AMPARE_NAME NVARCHAR(50),
    PRICE_ID INT,
    METER_ID INT,
    METER_CODE NVARCHAR(50),
    MULTIPLIER INT,
    CURRENCY_SIGN NVARCHAR(50),
    CURRENCY_NAME NVARCHAR(50),
    CURRENCY_ID INT,
    PRICE DECIMAL(18, 5),
    BASED_PRICE DECIMAL(18, 5),
    START_USAGE DECIMAL(18, 4),
    END_USAGE DECIMAL(18, 4),
    TOTAL_USAGE DECIMAL(18, 4),
    TOTAL_AMOUNT DECIMAL(18, 5),
    IS_POST_PAID BIT,
    IS_AGRICULTURE BIT,
    SHIFT_ID INT,
    IS_ACTIVE BIT,
    BILLING_CYCLE_ID INT,
    BILLING_CYCLE_NAME NVARCHAR(50),
    ROW_DATE DATETIME,
    CUSTOMER_TYPE_ID INT,
    CUSTOMER_TYPE_NAME NVARCHAR(50),
    LICENSE_NUMBER NVARCHAR(50)
);
/*REF 2016*/
IF @DATA_MONTH
   BETWEEN '2016-03-01' AND '2017-02-01'
BEGIN
    INSERT INTO #RESULT
    /*លក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ*/
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'លក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 3
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 4
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'លក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 3
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 3
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'លក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 3
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 2
    UNION ALL
    /*លក់ឲ្យអតិថិជន MV​(ឧស្សាហកម្ម ពាណិជ្ជកម្ម)*/
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'លក់ឲ្យអតិថិជន MV​(ឧស្សាហកម្ម ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 2
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 4
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'លក់ឲ្យអតិថិជន MV​(ឧស្សាហកម្ម ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 2
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 3
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'លក់ឲ្យអតិថិជន MV​(ឧស្សាហកម្ម ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 2
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 2
    UNION ALL
    /*លក់លើតង់ស្យុងទាប*/
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'លក់លើតង់ស្យុងទាប',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'អតិថិជនធម្មតា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 1
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 1;
END;
/*REF 2017*/
ELSE IF @DATA_MONTH
        BETWEEN '2017-03-01' AND '2018-12-01'
BEGIN
    INSERT INTO #RESULT
    /*ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ*/
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 3
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 11
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 3
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 12
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 3
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 13
    UNION ALL
    /*ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)*/
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID IN ( 1, 2 )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 4, 7, 8, 10 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID IN ( 1, 2 )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 3, 6 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID IN ( 1, 2 )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 2, 5 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID IN ( 1, 2, 5 )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9, 14, 15, 16 )
          AND i.TOTAL_USAGE >= 2001
    UNION ALL
    /*ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)*/
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)',
           TYPE_OF_SALE_ID = -1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើក្រោម 2001 kWh/ខែ ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID IN ( 1, 2, 5 )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9, 14, 15, 16 )
          AND i.TOTAL_USAGE < 2001
    UNION ALL
    /* ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន */
    SELECT TYPE_ID = 0,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើតិចជាង  10 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID IN ( 4 )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE <= 10
    UNION ALL
    SELECT TYPE_ID = 0,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID IN ( 4 )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE
          BETWEEN 11 AND 50
    UNION ALL
    SELECT TYPE_ID = 0,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើធំជាង 51 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID IN ( 4 )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE >= 51;
END;
/*REF 2019*/
ELSE IF @DATA_MONTH
        BETWEEN '2019-01-01' AND '2020-01-01'
BEGIN
    INSERT INTO #RESULT
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 11
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 12
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 13
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 4 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវាលា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 7 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 3 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 2 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 14 )
          AND i.TOTAL_USAGE >= 2001
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 8 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 10 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 6 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 5 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 15, 16 )
          AND i.TOTAL_USAGE >= 2001
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអតិថិជន សាលារៀន មន្ទីរពេទ្យ មណ្ឌលសុខភាព នៅតំបន់ជនបទ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)',
           TYPE_OF_SALE_ID = -1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើក្រោម 2001 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 14, 15, 16 )
          AND i.TOTAL_USAGE < 2001
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើតិចជាង  10 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE <= 10
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE
          BETWEEN 11 AND 50
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 51 ដល់ 200 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE
          BETWEEN 51 AND 200
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើធំជាង 201 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE >= 201;
END;
/*REF 2020 - NORMAL LICENSE TYPE ID =1*/
ELSE IF @DATA_MONTH
        BETWEEN '2020-02-01' AND '2020-12-01'
        AND @LICENSE_TYPE_ID = 1
BEGIN
    INSERT INTO #RESULT
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 11
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 12
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 13
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 4 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 7 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 3 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 17 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 2 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 18 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 14 )
          AND i.TOTAL_USAGE >= 2001
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 8 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 10 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 6 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 19 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 5 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 20 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = -1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 15, 16 )
          AND i.TOTAL_USAGE >= 2001
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = -2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអតិថិជន សាលារៀន មន្ទីរពេទ្យ មណ្ឌលសុខភាព នៅតំបន់ជនបទ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)',
           TYPE_OF_SALE_ID = -3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើក្រោម 2001 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 14, 15, 16 )
          AND i.TOTAL_USAGE < 2001
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើតិចជាង  10 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE <= 10
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE
          BETWEEN 11 AND 50
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 51 ដល់ 200 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE
          BETWEEN 51 AND 200
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -7,
           TYPE_OF_SALE_NAE = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើធំជាង 201 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE >= 201;
END;
/*REF 2020 - LICENSE TYPE ID =2*/
ELSE IF @DATA_MONTH
        BETWEEN '2020-02-01' AND '2020-12-01'
        AND @LICENSE_TYPE_ID = 2
BEGIN
    INSERT INTO #RESULT
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជននៅក្នុងបរិវេណផ្សារ',
           TYPE_OF_SALE_ID = -1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនក្នុងផ្សារ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 14, 15, 16 );
--AND i.TOTAL_USAGE<2001
END;

/*REF 2021 - NORMAL LICENSE TYPE ID =1*/
ELSE IF @DATA_MONTH
        BETWEEN '2021-01-01' AND '9999-12-01'
        AND @LICENSE_TYPE_ID = 1
BEGIN
    INSERT INTO #RESULT
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 11
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 12
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 13

    --ឧស្សាហកម្មវិស័យរុករករ៉ែ
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 72,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 26
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 71,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 27
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 70,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 28
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 69,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 29
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 68,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 30
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 67,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 31
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 66,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 32
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 65,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 33
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 64,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 34
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 63,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 35
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 62,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 36
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ) ',
           TYPE_OF_SALE_ID = 61,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 37

    --ឧស្សាហកម្មវិស័យកម្មន្តសាល
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 60,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 38
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 59,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 39
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 58,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 40
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 57,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 41
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 56,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 42
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 55,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 43
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 54,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 44
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 53,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 45
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 52,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 46
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 51,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 47
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 50,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 48
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល) ',
           TYPE_OF_SALE_ID = 49,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 49

    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 48,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 50
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 47,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 51
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 46,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 52
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 45,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 53
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 44,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 54
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 43,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 55
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 42,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 56
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 41,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 57
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 40,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 58
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 39,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 59
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 38,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 60
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ) ',
           TYPE_OF_SALE_ID = 37,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 61

    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 36,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 62
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 35,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 63
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 34,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 64
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 33,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 65
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 32,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 66
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 31,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 67
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 30,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 68
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 29,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 69
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 28,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 70
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 27,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 71
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 26,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 72
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ) ',
           TYPE_OF_SALE_ID = 25,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 73

    --សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។)
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 24,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 74
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 23,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 75
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 22,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 76
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 21,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 77
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 20,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 78
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 19,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 79
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 18,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 80
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 17,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 81
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 16,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 82
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 15,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 83
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 14,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 84
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម) ',
           TYPE_OF_SALE_ID = 13,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 85

    --សកម្មភាពកែច្នៃផលិតផលកសិកម្ម
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 12,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 86
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 87
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 88
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 89
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 90
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 91
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 92
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 93
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 94
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 95
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 96
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម) ',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 97

    --ពាណិជ្ជកម្ម
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 36,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 98
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 35,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 99
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 34,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 100
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 33,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 101
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 32,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 102
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 31,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 103
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 30,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 104
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 29,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 105
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 28,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 106
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 27,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 107
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 26,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 108
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម) ',
           TYPE_OF_SALE_ID = 25,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 109

    --រដ្ឋបាលសាធារណៈ
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 24,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 110
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 23,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 111
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 22,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 112
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 21,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 113
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 20,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 114
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 19,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 115
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 18,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 116
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 17,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 117
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 16,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 118
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 15,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 119
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 14,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 120
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ) ',
           TYPE_OF_SALE_ID = 13,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 121

    --សេវាកម្មផ្សេងៗ
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 12,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 122
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 123
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 124
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 125
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 126
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 127
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 128
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 129
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 130
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 131
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 132
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ) ',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 133
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែបណ្តាញសាធារណៈ)',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 22, 23, 134, 135, 136, 137, 138, 139, 140 )
          AND i.TOTAL_USAGE >= 2001
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអតិថិជន សាលារៀន មន្ទីរពេទ្យ មណ្ឌលសុខភាព នៅតំបន់ជនបទ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើក្រោម 2001 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 22, 23, 134, 135, 136, 137, 138, 139, 140 )
          AND i.TOTAL_USAGE < 2001
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើតិចជាង  10 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE <= 10
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE
          BETWEEN 11 AND 50
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 51 ដល់ 200 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE
          BETWEEN 51 AND 200
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAE = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើធំជាង 201 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE >= 201
    UNION ALL
    SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម(ទិញ MV)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
          AND i.PRICE = 0.13700
    UNION ALL
    SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម (ទិញ MV ម៉ោង៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
          AND i.PRICE = 480
    UNION ALL
    SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម (ទិញ LV ត្រង់ស្វូរអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
          AND i.PRICE = 0.15048
    UNION ALL
    SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម (ទិញ LV ត្រង់ស្វូរអ្នកលក់ ម៉ោង៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
          AND i.PRICE = 480
    UNION ALL
    SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម (ទិញ LV ត្រង់ស្វូរអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
          AND i.PRICE = 0.14248
    UNION ALL
    SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម (ទិញ LV ត្រង់ស្វូរអ្នកទិញ ម៉ោង៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
          AND i.PRICE = 480
    UNION ALL
    SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម (ទិញ LV សាធារណៈ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
          AND i.PRICE = 730
    UNION ALL
    SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម (ទិញ LV សាធារណៈ ៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
          AND i.PRICE = 480
    UNION
    SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជននៅក្នុងបរិវេណផ្សារ',
           TYPE_OF_SALE_ID = -1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនក្នុងផ្សារ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 144 );
END;
ELSE IF @DATA_MONTH
        BETWEEN '2021-01-01' AND '9999-12-31'
        AND @LICENSE_TYPE_ID = 2
BEGIN
    INSERT INTO #RESULT
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជននៅក្នុងបរិវេណផ្សារ',
           TYPE_OF_SALE_ID = -1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនក្នុងផ្សារ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 144 );
END;

/*RESULT*/
SELECT r.*,
       METER_FORMAT = REPLACE(LTRIM(REPLACE(ISNULL(METER_CODE, '-'), '0', ' ')), ' ', '0')
FROM #RESULT r
WHERE DATA_MONTH = @DATA_MONTH
      AND IS_ACTIVE = 1
      AND
      (
          @CUSTOMER_GROUP_ID = 0
          OR CUSTOMER_GROUP_ID = @CUSTOMER_GROUP_ID
      )
      AND
      (
          @CURRENCY_ID = 0
          OR CURRENCY_ID = @CURRENCY_ID
      )
      AND
      (
          @CUSTOMER_CONNECTION_TYPE_ID = 0
          OR CUSTOMER_CONNECTION_TYPE_ID = @CUSTOMER_CONNECTION_TYPE_ID
      )
      AND (TOTAL_USAGE
      BETWEEN @V1 AND @V2
          )
      AND
      (
          @BILLING_CYCLE_ID = 0
          OR BILLING_CYCLE_ID = @BILLING_CYCLE_ID
      )
      AND
      (
          @AREA_ID = 0
          OR AREA_ID = @AREA_ID
      )
ORDER BY TYPE_ID DESC,
         TYPE_OF_SALE_ID DESC,
         AREA_CODE,
         BOX_CODE;
--END REPORT_REF_INVOICE

GO

IF OBJECT_ID('RUN_REF_02_2020') IS NOT NULL
    DROP PROC RUN_REF_02_2020;
GO

CREATE PROC [dbo].[RUN_REF_02_2020]
    @DATA_MONTH DATETIME = '2020-07-01',
    @BILLING_CYCLE_ID INT = 0,
    @AREA_ID INT = 0
AS

/*
	@2018-09-19 Pov Lyhorng
		fix non subsidy for Organization customer
	@2019-01-16 Phoung Sovathvong
		Update REF 2019 split customer INDUSTRY, COMMERCIAL, SPECIAL
	@2019-02-22 Phoung Sovathvong
		Fix Incorrect word : រដ្ឋាបាល => រដ្ឋបាល
	@2019-12-03 Hor Dara
		Update REF 2020
	@2020-07-28 Hor Dara
		Update REF 2020 market vendor get subsudy even usage over 2000 
*/

-- CLEAR RESULT
DELETE FROM TBL_REF_02
WHERE DATA_MONTH = @DATA_MONTH;
DECLARE @LICENSE_TYPE_ID INT,
        @TYPE_LICENSEE NVARCHAR(200),
        @TYPE_LICENSEE_MV NVARCHAR(200),
        @TYPE_LICENSEE_MV_LV_CUSTOMER_TRANSFO NVARCHAR(200),
        @TYPE_LICENSEE_MV_LV_LICENSEE_TRANSFO NVARCHAR(200),
        @TYPE_INDUSTRY_NO_SUBSIDY NVARCHAR(200),
        @TYPE_INDUSTRY_SALE_MV NVARCHAR(200),
        @TYPE_INDUSTRY_SALE_MV_TOU_SOLA NVARCHAR(200),
        @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_SOLA NVARCHAR(200),
        @TYPE_INDUSTRY_SALE_MV_LV_TOU_LICENSEE_TRANSFO_TOU_SOLA NVARCHAR(200),
        @TYPE_INDUSTRY_SALE_MV_TOU NVARCHAR(200),
        @TYPE_INDUSTRY_SALE_MV_TOU_1 NVARCHAR(200),
        @TYPE_INDUSTRY_SALE_MV_TOU_2 NVARCHAR(200),
        @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO NVARCHAR(200),
        @TYPE_INDUSTRY_SALE_MV_LV_LICENSEE_TRANSFO NVARCHAR(200),
        @TYPE_INDUSTRY_SALE_LV_USAGE_OVER_2000 NVARCHAR(200),
        @TYPE_COMMERCIAL_SALE_CUSTOMER_AGRICULTURE NVARCHAR(200),
        @TYPE_COMMERCIAL_NO_SUBSIDY NVARCHAR(200),
        @TYPE_COMMERCIAL_SALE_MV NVARCHAR(200),
        @TYPE_COMMERCIAL_SALE_MV_TOU_SOLA NVARCHAR(200),
        @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_SOLA NVARCHAR(200),
        @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO_TOU_SOLA NVARCHAR(200),
        @TYPE_COMMERCIAL_SALE_MV_TOU NVARCHAR(200),
        @TYPE_COMMERCIAL_SALE_MV_TOU_1 NVARCHAR(200),
        @TYPE_COMMERCIAL_SALE_MV_TOU_2 NVARCHAR(200),
        @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO NVARCHAR(200),
        @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO NVARCHAR(200),
        @TYPE_COMMERCIAL_SALE_LV_USAGE_OVER_2000 NVARCHAR(200),
        @TYPE_COMMERCIAL_SALE_SCHOOL_HOSPITAL_HEALTH_CENTER_CUSTOMER NVARCHAR(200),
        @TYPE_SUBSIDY NVARCHAR(200),
        @TYPE_SUBSIDY_INDUSTRY_COMMERCIAL_SALE_LV_USAGE_UNDER_2001 NVARCHAR(200),
        @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_NORMAL NVARCHAR(200),
        @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_0_10 NVARCHAR(200),
        @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_11_50 NVARCHAR(200),
        @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_51_200 NVARCHAR(200),
        @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_OVER_200 NVARCHAR(200),
        @TYPE_COMMERCIAL_SALE_CUSTOMER_AGRICULTURE_1 NVARCHAR(200),
        @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO_TOU NVARCHAR(200),
        @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_1 NVARCHAR(200),
        @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_2 NVARCHAR(200),
        @TYPE_INDUSTRY_SALE_MV_LV_LICENSEE_TRANSFO_TOU NVARCHAR(200),
        @TYPE_INDUSTRY_SALE_MV_LV_LICENSEE_TRANSFO_TOU_1 NVARCHAR(200),
        @TYPE_INDUSTRY_SALE_MV_LV_LICENSEE_TRANSFO_TOU_2 NVARCHAR(200),
        @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO_TOU NVARCHAR(200),
        @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_1 NVARCHAR(200),
        @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_2 NVARCHAR(200),
        @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO_TOU NVARCHAR(200),
        @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO_TOU_1 NVARCHAR(200),
        @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO_TOU_2 NVARCHAR(200),
        @BASED_TARIFF DECIMAL(18, 5),
        @SUBSIDY_TARIFF DECIMAL(18, 5),
        @TYPE_SUBSIDY_SPECIAL_FOR_MARKET_VENDOR NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_MARKET_VENDOR NVARCHAR(200);

--CHECK LICENSE TYPE ID
SELECT @LICENSE_TYPE_ID = LICENSE_TYPE_ID,
       @BASED_TARIFF = BASED_TARIFF,
       @SUBSIDY_TARIFF = SUBSIDY_TARIFF
FROM dbo.TBL_TARIFF
WHERE DATA_MONTH = @DATA_MONTH;
SET @TYPE_LICENSEE = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ';
SET @TYPE_LICENSEE_MV = N'1. ថាមពលលក់ភ្ជាប់ពីខ្សែបណ្តាញតង់ស្យុងមធ្យម MV';
SET @TYPE_LICENSEE_MV_LV_CUSTOMER_TRANSFO = N'2. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកទិញ)';
SET @TYPE_LICENSEE_MV_LV_LICENSEE_TRANSFO = N'3. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកលក់)';
SET @TYPE_INDUSTRY_NO_SUBSIDY
    = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (អ្នកប្រើប្រាស់ធុនធំ និងធុនមធ្យម ប្រភេទ ឧស្សាហកម្ម និងកសិកម្ម)';
SET @TYPE_INDUSTRY_SALE_MV = N'1. ថាមពលលក់ភ្ជាប់ពីខ្សែបណ្តាញតង់ស្យុងមធ្យម MV';
SET @TYPE_INDUSTRY_SALE_MV_TOU = N'2. ថាមពលលក់ភ្ជាប់ពីខ្សែបណ្តាញតង់ស្យុងមធ្យម MV ជម្រើសតាមពេលវេលា និងអានុភាព';
SET @TYPE_INDUSTRY_SALE_MV_TOU_1 = N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកច្រើន (៧ព្រឹកដល់៩យប់)  ';
SET @TYPE_INDUSTRY_SALE_MV_TOU_2 = N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកតិច (៩យប់ដល់៧ព្រឹក)     ';
SET @TYPE_INDUSTRY_SALE_MV_TOU_SOLA = N'3. ថាមពលលក់ភ្ជាប់ពីខ្សែបណ្តាញតង់ស្យុងមធ្យម MV ដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ';
SET @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO
    = N'4. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកទិញ)';
SET @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO_TOU
    = N'5. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកទិញ) ជម្រើសតាមពេលវេលា និងអានុភាព';
SET @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_1
    = N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកច្រើន (៧ព្រឹកដល់៩យប់)   ';
SET @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_2
    = N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកតិច (៩យប់ដល់៧ព្រឹក)     ';
SET @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_SOLA
    = N'6. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកទិញ) ដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ';
SET @TYPE_INDUSTRY_SALE_MV_LV_LICENSEE_TRANSFO
    = N'7. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកលក់)';
SET @TYPE_INDUSTRY_SALE_MV_LV_LICENSEE_TRANSFO_TOU
    = N'8. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកលក់) ជម្រើសតាមពេលវេលា និងអានុភាព';
SET @TYPE_INDUSTRY_SALE_MV_LV_LICENSEE_TRANSFO_TOU_1
    = N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកច្រើន (៧ព្រឹកដល់៩យប់)     ';
SET @TYPE_INDUSTRY_SALE_MV_LV_LICENSEE_TRANSFO_TOU_2
    = N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកតិច (៩យប់ដល់៧ព្រឹក)     ';
SET @TYPE_INDUSTRY_SALE_MV_LV_TOU_LICENSEE_TRANSFO_TOU_SOLA
    = N'9. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកលក់) ដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ';
SET @TYPE_INDUSTRY_SALE_LV_USAGE_OVER_2000
    = N'10. ថាមពលលក់ឲ្យអតិថិជន ប្រភេទឧស្សាហកម្ម និងកសិកម្ម LV សាធារណៈ ប្រើចាប់ពី 2001 kWh/ខែ';
SET @TYPE_COMMERCIAL_SALE_CUSTOMER_AGRICULTURE = N'11. ថាមពលលក់ឲ្យការបូមទឹកធ្វើកសិកម្មម៉ោង07:00ព្រឹក ដល់ 09:00យប់';
SET @TYPE_COMMERCIAL_NO_SUBSIDY
    = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (អ្នកប្រើប្រាស់ធុនធំ និងធុនមធ្យម ប្រភេទពាណិជ្ជកម្ម រដ្ឋបាល និងផ្សេងៗ)';
SET @TYPE_COMMERCIAL_SALE_MV = N'1. ថាមពលលក់ភ្ជាប់ពីខ្សែបណ្តាញតង់ស្យុងមធ្យម MV';
SET @TYPE_COMMERCIAL_SALE_MV_TOU = N'2. ថាមពលលក់ភ្ជាប់ពីខ្សែបណ្តាញតង់ស្យុងមធ្យម MV ជម្រើសតាមពេលវេលា និងអានុភាព';
SET @TYPE_COMMERCIAL_SALE_MV_TOU_1 = N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកច្រើន (៧ព្រឹកដល់៩យប់)     ';
SET @TYPE_COMMERCIAL_SALE_MV_TOU_2 = N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកតិច (៩យប់ដល់៧ព្រឹក)     ';
SET @TYPE_COMMERCIAL_SALE_MV_TOU_SOLA
    = N'3. ថាមពលលក់ភ្ជាប់ពីខ្សែបណ្តាញតង់ស្យុងមធ្យម MV ដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ';
SET @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO
    = N'4. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកទិញ)';
SET @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO_TOU
    = N'5. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកទិញ) ជម្រើសតាមពេលវេលា និងអានុភាព';
SET @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_1
    = N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកច្រើន (៧ព្រឹកដល់៩យប់)     ';
SET @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_2
    = N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកតិច (៩យប់ដល់៧ព្រឹក)     ';
SET @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_SOLA
    = N'6. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកទិញ) ដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ';
SET @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO
    = N'7. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកលក់)';
SET @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO_TOU
    = N'8. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកលក់) ជម្រើសតាមពេលវេលា និងអានុភាព';
SET @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO_TOU_1
    = N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកច្រើន (៧ព្រឹកដល់៩យប់)     ';
SET @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO_TOU_2
    = N'       - ថាមពលប្រើប្រាស់ក្នុងពេលមានបន្ទុកតិច (៩យប់ដល់៧ព្រឹក)     ';
SET @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO_TOU_SOLA
    = N'9. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកលក់) ដាក់ប្រភពតម្លើងពន្លឺព្រះអាទិត្យ';
SET @TYPE_COMMERCIAL_SALE_LV_USAGE_OVER_2000
    = N'10. ថាមពលលក់ឲ្យអតិថិជន ប្រភេទឧស្សាហកម្ម និងកសិកម្ម LV សាធារណៈ ប្រើចាប់ពី 2001 kWh/ខែ';
SET @TYPE_SUBSIDY = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)';
SET @TYPE_COMMERCIAL_SALE_SCHOOL_HOSPITAL_HEALTH_CENTER_CUSTOMER
    = N'1. ថាមពលលក់ឲ្យអតិថិជន សាលារៀន មន្ទីរពេទ្យ មណ្ឌលសុខភាព នៅតំបន់ជនបទ';
SET @TYPE_COMMERCIAL_SALE_CUSTOMER_AGRICULTURE_1 = N'2. ថាមពលលក់ឲ្យការបូមទឹកធ្វើកសិកម្មម៉ោង09:00យប់ ដល់07:00ព្រឹក';
SET @TYPE_SUBSIDY_INDUSTRY_COMMERCIAL_SALE_LV_USAGE_UNDER_2001
    = N'3. ថាមពលលក់ឲ្យអតិថិជន ឧស្សាហកម្ម&កសិកម្ម ពាណិជ្ជកម្ម រដ្ឋបាល&សេវាកម្ម ប្រើក្រោម 2001 kWh/ខែ';
SET @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_NORMAL = N'4. ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន';
SET @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_0_10 = N'4.1' + SPACE(9) + N'ថាមពលលក់ឲ្យលំនៅដ្ឋានប្រើ មិនលើសពី 10 kWh/ខែ';
SET @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_11_50 = N'4.2' + SPACE(9) + N'ថាមពលលក់ឲ្យលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ';
SET @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_51_200 = N'4.3' + SPACE(9) + N'ថាមពលលក់ឲ្យលំនៅដ្ឋានប្រើចាប់ពី 51 ដល់ 200 kWh/ខែ';
SET @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_OVER_200 = N'4.4' + SPACE(9) + N'ថាមពលលក់ឲ្យលំនៅដ្ឋានប្រើចាប់ពី 201 kWh/ខែ';
SET @TYPE_SUBSIDY_SPECIAL_FOR_MARKET_VENDOR = N'ថាមពលលក់ឲ្យអតិថិជននៅក្នុងបរិវេណផ្សារ';
SET @TYPE_SALE_CUSTOMER_MARKET_VENDOR = N'1. ថាមពលលក់ឲ្យអតិថិជនក្នុងផ្សារ';
IF (@LICENSE_TYPE_ID = 1)
BEGIN
    -- Licensee Customer(MV)
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_LICENSEE,
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = @TYPE_LICENSEE_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    INTO #RESULT
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 11
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- Licensee Customer(buyer's transformer)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_LICENSEE,
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = @TYPE_LICENSEE_MV_LV_CUSTOMER_TRANSFO,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 12
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- Licensee Customer(license's transformer)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_LICENSEE,
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = @TYPE_LICENSEE_MV_LV_LICENSEE_TRANSFO,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 13
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- MV Customer INDUSTRY
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 3,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 17,
           TYPE_OF_SALE_NAME = @TYPE_INDUSTRY_SALE_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 4 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- MV TOU Customer INDUSTRY
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 3,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 16,
           TYPE_OF_SALE_NAME = @TYPE_INDUSTRY_SALE_MV_TOU,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = 0,
           RATE = 1,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 7 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- MV TOU Customer INDUSTRY(7:00am-9:00pm)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 3,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 15,
           TYPE_OF_SALE_NAME = @TYPE_INDUSTRY_SALE_MV_TOU_1,
           TOTAL_CUSTOMER = 0,
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 7 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND i.PRICE = 0.1300
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- MV TOU Customer INDUSTRY(9:00pm-7:00pm)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 3,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 14,
           TYPE_OF_SALE_NAME = @TYPE_INDUSTRY_SALE_MV_TOU_2,
           TOTAL_CUSTOMER = 0,
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 7 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND i.PRICE = 0.1100
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- MV TOU SOLA
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 3,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 13,
           TYPE_OF_SALE_NAME = @TYPE_INDUSTRY_SALE_MV_TOU_SOLA,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 21 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- MV/LV Customer INDUSTRY (buyer's transformer)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 3,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 12,
           TYPE_OF_SALE_NAME = @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 3 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- MV/LV Customer INDUSTRY (buyer's transformer) TOU
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 3,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO_TOU,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = 0,
           RATE = 1,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 17 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- MV/LV Customer INDUSTRY (buyer's transformer)(7:00am-9:00pm)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 3,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_1,
           TOTAL_CUSTOMER = 0,
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 17 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND i.PRICE = 0.1352
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- MV/LV Customer INDUSTRY (buyer's transformer)(9:00pm-7:00pm)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 3,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_2,
           TOTAL_CUSTOMER = 0,
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 17 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND i.PRICE = 0.1144
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- MV/LV Customer INDUSTRY (buyer's transformer) TOU SOLA
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 3,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_SOLA,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- MV/LV Customer INDUSTRY (licensee's transformer)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 3,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = @TYPE_INDUSTRY_SALE_MV_LV_LICENSEE_TRANSFO,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 2 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- MV/LV Customer INDUSTRY (licensee's transformer) TOU
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 3,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = @TYPE_INDUSTRY_SALE_MV_LV_LICENSEE_TRANSFO_TOU,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = 0,
           RATE = 1,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 18 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- MV/LV Customer INDUSTRY (licensee's transformer)(7:00am-9:00pm)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 3,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = @TYPE_INDUSTRY_SALE_MV_LV_LICENSEE_TRANSFO_TOU_1,
           TOTAL_CUSTOMER = 0,
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 18 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND i.PRICE = 0.1432
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- MV/LV Customer INDUSTRY (licensee's transformer)(9:00pm-7:00pm)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 3,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = @TYPE_INDUSTRY_SALE_MV_LV_LICENSEE_TRANSFO_TOU_2,
           TOTAL_CUSTOMER = 0,
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 18 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND i.PRICE = 0.1224
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --MV.LV Customer INDUSTRY (licensee's transformer) TOU SOLA
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 3,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = @TYPE_INDUSTRY_SALE_MV_LV_TOU_LICENSEE_TRANSFO_TOU_SOLA,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Normal Customer(TOTAL_USAGE>=2001) INDUSTRY
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 3,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_INDUSTRY_SALE_LV_USAGE_OVER_2000,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 14 )
          AND i.TOTAL_USAGE >= 2001
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- MV Customer COMMERCIAL
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_COMMERCIAL_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 16,
           TYPE_OF_SALE_NAME = @TYPE_COMMERCIAL_SALE_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 8 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- MV TOU Customer COMMERCIAL TOU
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_COMMERCIAL_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 15,
           TYPE_OF_SALE_NAME = @TYPE_COMMERCIAL_SALE_MV_TOU,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = 0,
           RATE = 1,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 10 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- MV TOU Customer COMMERCIAL(7:00am-9:00pm)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_COMMERCIAL_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 14,
           TYPE_OF_SALE_NAME = @TYPE_COMMERCIAL_SALE_MV_TOU_1,
           TOTAL_CUSTOMER = 0,
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 10 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND i.PRICE = 0.1500
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- MV TOU Customer COMMERCIAL(9:00pm-7:00am)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_COMMERCIAL_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 13,
           TYPE_OF_SALE_NAME = @TYPE_COMMERCIAL_SALE_MV_TOU_2,
           TOTAL_CUSTOMER = 0,
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 10 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND i.PRICE = 0.1240
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    ---MV TOU SOLA Customer
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 12,
           TYPE_OF_SALE_NAME = @TYPE_COMMERCIAL_SALE_MV_TOU_SOLA,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- MV/LV Customer Commercial (buyer's transformer)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_COMMERCIAL_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 6 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- MV/LV Customer Commercial (buyer's transformer)TOU
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO_TOU,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = 0,
           RATE = 1,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 19 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- MV/LV Customer Commercial (buyer's transformer)(7:00am-9:00pm)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_1,
           TOTAL_CUSTOMER = 0,
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 19 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND i.PRICE = 0.1560
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- MV/LV Customer Commercial (buyer's transformer)(9:00pm-7:00pm)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_2,
           TOTAL_CUSTOMER = 0,
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 19 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND i.PRICE = 0.12896
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- MV/LV Customer Commercial (buyer's transformer)TOU SOLA
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_SOLA,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- MV/LV Customer Commercial (licensee's transformer)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_COMMERCIAL_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 5 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- MV/LV Customer Commercial (licensee's transformer)TOU
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO_TOU,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = 0,
           RATE = 1,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 20 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- MV/LV Customer Commercial (licensee's transformer)(7:00am-9:00pm)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO_TOU_1,
           TOTAL_CUSTOMER = 0,
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 20 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND i.PRICE = 0.1640
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- MV/LV Customer Commercial (licensee's transformer)(9:00pm-7:00pm)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO_TOU_2,
           TOTAL_CUSTOMER = 0,
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 20 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND i.PRICE = 0.13696
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- MV/LV Customer Commercial (licensee's transformer)TOU SOLA
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO_TOU_SOLA,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Normal Customer(TOTAL_USAGE>=2001) COMMERCIAL
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_COMMERCIAL_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_COMMERCIAL_SALE_LV_USAGE_OVER_2000,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 15, 16 )
          AND i.TOTAL_USAGE >= 2001
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Normal Customer SPECIAL
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_SUBSIDY,
           TYPE_OF_SALE_ID = 0,
           TYPE_OF_SALE_NAME = @TYPE_COMMERCIAL_SALE_SCHOOL_HOSPITAL_HEALTH_CENTER_CUSTOMER,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Normal Customer(TOTAL_USAGE<2001)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_SUBSIDY,
           TYPE_OF_SALE_ID = -2,
           TYPE_OF_SALE_NAME = @TYPE_SUBSIDY_INDUSTRY_COMMERCIAL_SALE_LV_USAGE_UNDER_2001,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = BASED_PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 14, 15, 16 )
          AND i.TOTAL_USAGE < 2001
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY BASED_PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Total Normal Customer
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_SUBSIDY,
           TYPE_OF_SALE_ID = -3,
           TYPE_OF_SALE_NAME = @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_NORMAL,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = BASED_PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY BASED_PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Normal Customer(TOTAL_USAGE<=10)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_SUBSIDY,
           TYPE_OF_SALE_ID = -4,
           TYPE_OF_SALE_NAME = @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_0_10,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE <= 10
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Normal Customer(TOTAL_USAGE BETWEEN 11 AND 50)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_SUBSIDY,
           TYPE_OF_SALE_ID = -5,
           TYPE_OF_SALE_NAME = @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_11_50,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND (i.TOTAL_USAGE
          BETWEEN 11 AND 50
              )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Normal Customer(TOTAL_USAGE BETWEEN 51 AND 200)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_SUBSIDY,
           TYPE_OF_SALE_ID = -6,
           TYPE_OF_SALE_NAME = @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_51_200,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND (i.TOTAL_USAGE
          BETWEEN 51 AND 200
              )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Normal Customer(TOTAL_USAGE>=200)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_SUBSIDY,
           TYPE_OF_SALE_ID = -7,
           TYPE_OF_SALE_NAME = @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_OVER_200,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE >= 201
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN;
    CREATE TABLE #DEFAULT_RESULT
    (
        DATA_MONTH DATETIME,
        TYPE_ID INT,
        TYPE_NAME NVARCHAR(200),
        TYPE_OF_SALE_ID INT,
        TYPE_OF_SALE_NAME NVARCHAR(200),
        TOTAL_CUSTOMER INT,
        TOTAL_POWER_SOLD INT,
        RATE DECIMAL(18, 4),
        CURRENCY_ID INT,
        CURRENCY_NAME NVARCHAR(20),
        CURRENCY_SIGN NVARCHAR(5),
        IS_ACTIVE BIT
    );
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_LICENSEE, 5, @TYPE_LICENSEE_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_LICENSEE, 4, @TYPE_LICENSEE_MV_LV_CUSTOMER_TRANSFO, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_LICENSEE, 3, @TYPE_LICENSEE_MV_LV_LICENSEE_TRANSFO, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 17, @TYPE_INDUSTRY_SALE_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 16, @TYPE_INDUSTRY_SALE_MV_TOU, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 15, @TYPE_INDUSTRY_SALE_MV_TOU_1, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 14, @TYPE_INDUSTRY_SALE_MV_TOU_2, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 13, @TYPE_INDUSTRY_SALE_MV_TOU_SOLA, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 12, @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 11, @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO_TOU, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 10, @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_1, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 9, @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_2, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 8, @TYPE_INDUSTRY_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_SOLA, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 7, @TYPE_INDUSTRY_SALE_MV_LV_LICENSEE_TRANSFO, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 6, @TYPE_INDUSTRY_SALE_MV_LV_LICENSEE_TRANSFO_TOU, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 5, @TYPE_INDUSTRY_SALE_MV_LV_LICENSEE_TRANSFO_TOU_1, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 4, @TYPE_INDUSTRY_SALE_MV_LV_LICENSEE_TRANSFO_TOU_2, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 3, @TYPE_INDUSTRY_SALE_MV_LV_TOU_LICENSEE_TRANSFO_TOU_SOLA, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 2, @TYPE_INDUSTRY_SALE_LV_USAGE_OVER_2000, 0, 0, 0, 1, N'រៀល', N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 3, @TYPE_INDUSTRY_NO_SUBSIDY, 0, @TYPE_COMMERCIAL_SALE_CUSTOMER_AGRICULTURE, 0, 0, 0, 1, N'រៀល',
     N'៛', 1); --AGRI
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_COMMERCIAL_NO_SUBSIDY, 16, @TYPE_COMMERCIAL_SALE_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_COMMERCIAL_NO_SUBSIDY, 15, @TYPE_COMMERCIAL_SALE_MV_TOU, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_COMMERCIAL_NO_SUBSIDY, 14, @TYPE_COMMERCIAL_SALE_MV_TOU_1, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_COMMERCIAL_NO_SUBSIDY, 13, @TYPE_COMMERCIAL_SALE_MV_TOU_2, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_COMMERCIAL_NO_SUBSIDY, 12, @TYPE_COMMERCIAL_SALE_MV_TOU_SOLA, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_COMMERCIAL_NO_SUBSIDY, 11, @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_COMMERCIAL_NO_SUBSIDY, 10, @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO_TOU, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_COMMERCIAL_NO_SUBSIDY, 9, @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_1, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_COMMERCIAL_NO_SUBSIDY, 8, @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_2, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_COMMERCIAL_NO_SUBSIDY, 7, @TYPE_COMMERCIAL_SALE_MV_LV_CUSTOMER_TRANSFO_TOU_SOLA, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_COMMERCIAL_NO_SUBSIDY, 6, @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_COMMERCIAL_NO_SUBSIDY, 5, @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO_TOU, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_COMMERCIAL_NO_SUBSIDY, 4, @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO_TOU_1, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_COMMERCIAL_NO_SUBSIDY, 3, @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO_TOU_2, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_COMMERCIAL_NO_SUBSIDY, 2, @TYPE_COMMERCIAL_SALE_MV_LV_LICENSEE_TRANSFO_TOU_SOLA, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_COMMERCIAL_NO_SUBSIDY, 1, @TYPE_COMMERCIAL_SALE_LV_USAGE_OVER_2000, 0, 0, 0, 1, N'រៀល',
     N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_SUBSIDY, 0, @TYPE_COMMERCIAL_SALE_SCHOOL_HOSPITAL_HEALTH_CENTER_CUSTOMER, 0, 0, 0, 1,
     N'រៀល', N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_SUBSIDY, -1, @TYPE_COMMERCIAL_SALE_CUSTOMER_AGRICULTURE_1, 0, 0, 0, 1, N'រៀល', N'៛', 1); --AGRI
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_SUBSIDY, -2, @TYPE_SUBSIDY_INDUSTRY_COMMERCIAL_SALE_LV_USAGE_UNDER_2001, 0, 0, 0, 1, N'រៀល',
     N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_SUBSIDY, -3, @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_NORMAL, 0, 0, 0, 1, N'រៀល', N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_SUBSIDY, -4, @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_0_10, 0, 0, 0, 1, N'រៀល', N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_SUBSIDY, -5, @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_11_50, 0, 0, 0, 1, N'រៀល', N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_SUBSIDY, -6, @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_51_200, 0, 0, 0, 1, N'រៀល', N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_SUBSIDY, -7, @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_OVER_200, 0, 0, 0, 1, N'រៀល', N'៛', 1);

    -- RESULT
    INSERT INTO TBL_REF_02
    SELECT *
    FROM #RESULT
    UNION ALL
    SELECT *
    FROM #DEFAULT_RESULT t
    WHERE NOT EXISTS
    (
        SELECT *
        FROM #RESULT
        WHERE TYPE_ID = t.TYPE_ID
              AND TYPE_OF_SALE_ID = t.TYPE_OF_SALE_ID
    )
    ORDER BY TYPE_ID DESC,
             TYPE_OF_SALE_ID DESC;
END;
ELSE IF (@LICENSE_TYPE_ID = 2)
BEGIN
    -- Market vendor customer
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_SUBSIDY_SPECIAL_FOR_MARKET_VENDOR,
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_MARKET_VENDOR,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    INTO #RESULT_MARKET_VENDOR
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 14, 15, 16 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN;
    CREATE TABLE #DEFAULT_RESULT_MARKET_VENDOR
    (
        DATA_MONTH DATETIME,
        TYPE_ID INT,
        TYPE_NAME NVARCHAR(200),
        TYPE_OF_SALE_ID INT,
        TYPE_OF_SALE_NAME NVARCHAR(200),
        TOTAL_CUSTOMER INT,
        TOTAL_POWER_SOLD INT,
        RATE DECIMAL(18, 4),
        CURRENCY_ID INT,
        CURRENCY_NAME NVARCHAR(20),
        CURRENCY_SIGN NVARCHAR(5),
        IS_ACTIVE BIT
    );
    INSERT INTO #DEFAULT_RESULT_MARKET_VENDOR
    VALUES
    (@DATA_MONTH, 1, @TYPE_LICENSEE, 1, @TYPE_SALE_CUSTOMER_MARKET_VENDOR, 0, 0, 0, 1, N'រៀល', N'៛', 1);

    -- RESULT
    INSERT INTO TBL_REF_02
    SELECT *
    FROM #RESULT_MARKET_VENDOR
    UNION ALL
    SELECT *
    FROM #DEFAULT_RESULT_MARKET_VENDOR t
    WHERE NOT EXISTS
    (
        SELECT *
        FROM #RESULT_MARKET_VENDOR
        WHERE TYPE_ID = t.TYPE_ID
              AND TYPE_OF_SALE_ID = t.TYPE_OF_SALE_ID
    )
    ORDER BY TYPE_ID DESC,
             TYPE_OF_SALE_ID DESC;
END;
--END OF RUN_REF_02_2020

GO

IF OBJECT_ID('RUN_REF_03B_2021') IS NOT NULL
    DROP PROC dbo.RUN_REF_03B_2021;

GO

CREATE PROC dbo.RUN_REF_03B_2021
    @DATA_MONTH DATETIME = '2021-01-01',
    @BILLING_CYCLE_ID INT = 0,
    @AREA_ID INT = 0
AS
-- CLEAR DATA
DELETE FROM TBL_REF_03B
WHERE DATA_MONTH = @DATA_MONTH;

DECLARE @TYPE_SALE_CUSTOMER_USAGE_0_10 NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_USAGE_11_50 NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_USAGE_51_200 NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_SCHOOL NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_AGRI NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_SPECIAL NVARCHAR(200),
        @LICENSE_TYPE_ID INT,
        @BASED_TARIFF DECIMAL(18, 5),
        @SUBSIDY_TARIFF DECIMAL(18, 5),
        @TYPE_CUSTOMER_NORMAL NVARCHAR(200),
        @TYPE_CUSTOMER_FAVOR NVARCHAR(200),
        @TYPE_CUSTOMER_MV NVARCHAR(200),
        @TYPE_CUSTOMER_LV_LICENSE NVARCHAR(200),
        @TYPE_CUSTOMER_LV_CUSTOMER_BUY NVARCHAR(200),
        @TYPE_CUSTOMER_LV_GENERAL NVARCHAR(200);
SET @TYPE_CUSTOMER_NORMAL = N'  1' + SPACE(4) + N'អតិថិជនលំនៅដ្ឋាន';
SET @TYPE_CUSTOMER_FAVOR = N'  2' + SPACE(4) + N'អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស';
SET @TYPE_SALE_CUSTOMER_USAGE_0_10 = N'1.1' + N' លំនៅដ្ឋានប្រើ មិនលើស  10 kWh/ខែ';
SET @TYPE_SALE_CUSTOMER_USAGE_11_50 = N'1.2' + N' លំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ';
SET @TYPE_SALE_CUSTOMER_USAGE_51_200 = N'1.3' + N' លំនៅដ្ឋានប្រើចាប់ពី 51 ដល់ 200 kWh/ខែ';
SET @TYPE_SALE_CUSTOMER_SCHOOL = N'2.1' + N' សាលារៀន, មន្ទីរពេទ្យ, និងមណ្ឌលសុខភាព នៅតំបន់ជនបទ';
SET @TYPE_SALE_CUSTOMER_AGRI = N'2.2' + N' អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម';
SET @TYPE_CUSTOMER_MV = N'  2.2.1' + N'  អ្នកប្រើប្រាស់ MV';
SET @TYPE_CUSTOMER_LV_LICENSE = N'  2.2.2' + N'  អ្នកប្រើប្រាស់ LV (ត្រង់ស្វូរអ្នកលក់)';
SET @TYPE_CUSTOMER_LV_CUSTOMER_BUY = N'  2.2.3' + N'  អ្នកប្រើប្រាស់ LV (ត្រង់ស្វូរអ្នកទិញ)';
SET @TYPE_CUSTOMER_LV_GENERAL = N'  2.2.4' + N'  អ្នកប្រើប្រាស់ LV (ភ្ជាប់ពីខ្សែសាធារណៈ)';

SELECT @LICENSE_TYPE_ID = LICENSE_TYPE_ID,
       @SUBSIDY_TARIFF = SUBSIDY_TARIFF,
       @BASED_TARIFF = BASED_TARIFF
FROM dbo.TBL_TARIFF
WHERE DATA_MONTH = @DATA_MONTH;

IF (@LICENSE_TYPE_ID <> 2)
BEGIN
    INSERT INTO TBL_REF_03B
    (
        DATA_MONTH,
        ROW_NO,
        TYPE_OF_SALE_ID,
        TYPE_OF_SALE_NAME,
        TOTAL_CUSTOMER,
        TOTAL_POWER_SOLD,
        PRICE,
        SUBSIDY_RATE,
        SUBSIDY_AMOUNT,
        CURRENCY_ID,
        CURRENCY_NAME,
        CURRENCY_SIGN,
        IS_ACTIVE,
        TYPE_ID,
        TYPE_NAME
    )
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1.1',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_0_10,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_NORMAL
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE <= 10
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1.2',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_11_50,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_NORMAL
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND (i.TOTAL_USAGE
          BETWEEN 11 AND 50
              )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1.3',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_51_200,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_NORMAL
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND (i.TOTAL_USAGE
          BETWEEN 51 AND 200
              )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.1',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_SCHOOL,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_AGRI,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.1',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(0.1370 * 4120, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(ROUND(0.1370 * 4120, 0), 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.2',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_LICENSE,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(0.15048 * 4120, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(ROUND(0.15048 * 4120, 0), 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.3',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_CUSTOMER_BUY,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(0.14248 * 4120, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(ROUND(0.14248 * 4120, 0), 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.4',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_GENERAL,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(730, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(730, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN

    --WHEN BASE TARFF < SUBSIDY TARIFF
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1.1',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_0_10,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.BASED_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.BASED_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_NORMAL
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE <= 10
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1.2',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_11_50,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.BASED_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.BASED_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_NORMAL
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND (i.TOTAL_USAGE
          BETWEEN 11 AND 50
              )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1.3',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_51_200,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.BASED_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.BASED_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_NORMAL
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND (i.TOTAL_USAGE
          BETWEEN 51 AND 200
              )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.1',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_SCHOOL,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.BASED_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.BASED_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_AGRI,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.BASED_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.BASED_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND (i.TOTAL_USAGE
          BETWEEN 51 AND 200
              )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.1',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(0.1370 * 4120, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(ROUND(0.1370 * 4120, 0), 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.2',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_LICENSE,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(0.15048 * 4120, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(ROUND(0.15048 * 4120, 0), 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.3',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_CUSTOMER_BUY,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(0.14248 * 4120, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(ROUND(0.14248 * 4120, 0), 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.4',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_GENERAL,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(730, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(730, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN;

    -- BLANK RECORD
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '1.1'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'1.1',
               TYPE_OF_SALE_ID = 1,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_0_10,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 1,
               TYPE_NAME = @TYPE_CUSTOMER_NORMAL;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '1.2'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'1.2',
               TYPE_OF_SALE_ID = 1,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_11_50,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 1,
               TYPE_NAME = @TYPE_CUSTOMER_NORMAL;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '1.3'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'1.3',
               TYPE_OF_SALE_ID = 1,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_51_200,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 1,
               TYPE_NAME = @TYPE_CUSTOMER_NORMAL;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '2.1'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'2.1',
               TYPE_OF_SALE_ID = 2,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_SCHOOL,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 2,
               TYPE_NAME = @TYPE_CUSTOMER_FAVOR;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '2.2'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'2.2',
               TYPE_OF_SALE_ID = 2,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_AGRI,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 2,
               TYPE_NAME = @TYPE_CUSTOMER_FAVOR;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '2.2.1'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'2.2.1',
               TYPE_OF_SALE_ID = 2,
               TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_MV,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 2,
               TYPE_NAME = @TYPE_CUSTOMER_FAVOR;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '2.2.2'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'2.2.2',
               TYPE_OF_SALE_ID = 2,
               TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_LICENSE,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 2,
               TYPE_NAME = @TYPE_CUSTOMER_FAVOR;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '2.2.3'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'2.2.3',
               TYPE_OF_SALE_ID = 2,
               TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_CUSTOMER_BUY,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 2,
               TYPE_NAME = @TYPE_CUSTOMER_FAVOR;
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '2.2.4'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'2.2.4',
               TYPE_OF_SALE_ID = 2,
               TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_GENERAL,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 2,
               TYPE_NAME = @TYPE_CUSTOMER_FAVOR;
END;
-- END RUN_REF_03B_2021

GO

IF OBJECT_ID('RUN_REF_02_2021') IS NOT NULL
    DROP PROC RUN_REF_02_2021;
GO

CREATE PROC [dbo].[RUN_REF_02_2021]
    @DATA_MONTH DATETIME = '2021-01-01',
    @BILLING_CYCLE_ID INT = 0,
    @AREA_ID INT = 0
AS

/*
	@2018-09-19 Pov Lyhorng
		fix non subsidy for Organization customer
	@2019-01-16 Phoung Sovathvong
		Update REF 2019 split customer INDUSTRY, COMMERCIAL, SPECIAL
	@2019-02-22 Phoung Sovathvong
		Fix Incorrect word : រដ្ឋាបាល => រដ្ឋបាល
	@2019-12-03 Hor Dara
		Update REF 2020
	@2020-07-28 Hor Dara
		Update REF 2020 market vendor get subsudy even usage over 2000 
	@2021-01-08 Vonn Kimputhmunyvorn
		Update REF 2021
*/

-- CLEAR RESULT
DELETE FROM TBL_REF_02
WHERE DATA_MONTH = @DATA_MONTH;
DECLARE @LICENSE_TYPE_ID INT,
        @TYPE_LICENSEE NVARCHAR(200),
        @TYPE_LICENSEE_MV NVARCHAR(200),
        @TYPE_LICENSEE_MV_LV_CUSTOMER_TRANSFO NVARCHAR(200),
        @TYPE_LICENSEE_MV_LV_LICENSEE_TRANSFO NVARCHAR(200),

        --1.​ ឧស្សាហកម្មវិស័យរុករករ៉ែ
        @TYPE_MINING_INDUSTRY NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV_SALE_LV NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV_SUN NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV_SUN_SALE_LV NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV NVARCHAR(200),

        --2.​ ឧស្សាហកម្មវិស័យកម្មន្តសាល 
        @TYPE_MANUFACTURING_INDUSTRY NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV_SALE_LV NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV_SUN NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV_SUN_SALE_LV NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV NVARCHAR(200),

        --3.​ ឧស្សាហកម្មវិស័យវាយនភណ្ឌ
        @TYPE_TEXTILE_INDUSTRY NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV_SALE_LV NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV_SUN NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV_SUN_SALE_LV NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV NVARCHAR(200),

        --4.​ កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ
        @TYPE_AGRICULTURE NVARCHAR(200),
        @TYPE_AGRICULTURE_MV NVARCHAR(200),
        @TYPE_AGRICULTURE_MV_SALE_LV NVARCHAR(200),
        @TYPE_AGRICULTURE_MV_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT NVARCHAR(200),
        @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT_SALE_LV NVARCHAR(200),
        @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING NVARCHAR(200),
        @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING_SALE_LV NVARCHAR(200),
        @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_AGRICULTURE_MV_SUN NVARCHAR(200),
        @TYPE_AGRICULTURE_MV_SUN_SALE_LV NVARCHAR(200),
        @TYPE_AGRICULTURE_MV_SUN_CUSTOMER_BUY_LV NVARCHAR(200),

        --5.​ សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។)
        @TYPE_ACTIVITIES NVARCHAR(200),
        @TYPE_ACTIVITIES_MV NVARCHAR(200),
        @TYPE_ACTIVITIES_MV_SALE_LV NVARCHAR(200),
        @TYPE_ACTIVITIES_MV_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT NVARCHAR(200),
        @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT_SALE_LV NVARCHAR(200),
        @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING NVARCHAR(200),
        @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING_SALE_LV NVARCHAR(200),
        @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_ACTIVITIES_MV_SUN NVARCHAR(200),
        @TYPE_ACTIVITIES_MV_SUN_SALE_LV NVARCHAR(200),
        @TYPE_ACTIVITIES_MV_SUN_CUSTOMER_BUY_LV NVARCHAR(200),


        --6.​ សកម្មភាពកែច្នៃផលិតផលកសិកម្ម
        @TYPE_AGRICULTURAL NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV_SALE_LV NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT_SALE_LV NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING_SALE_LV NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV_SUN NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV_SUN_SALE_LV NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV_SUN_CUSTOMER_BUY_LV NVARCHAR(200),

        --1.ពាណិជ្ជកម្ម 
        @TYPE_BUSINESS NVARCHAR(200),
        @TYPE_BUSINESS_MV NVARCHAR(200),
        @TYPE_BUSINESS_MV_SALE_LV NVARCHAR(200),
        @TYPE_BUSINESS_MV_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT NVARCHAR(200),
        @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT_SALE_LV NVARCHAR(200),
        @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING NVARCHAR(200),
        @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING_SALE_LV NVARCHAR(200),
        @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_BUSINESS_MV_SUN NVARCHAR(200),
        @TYPE_BUSINESS_MV_SUN_SALE_LV NVARCHAR(200),
        @TYPE_BUSINESS_MV_SUN_CUSTOMER_BUY_LV NVARCHAR(200),

        --2.រដ្ឋបាលសាធារណៈ 
        @TYPE_PUBLIC_ADMINISTRATION NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_SALE_LV NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT_SALE_LV NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING_SALE_LV NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_SUN NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_SUN_SALE_LV NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_SUN_CUSTOMER_BUY_LV NVARCHAR(200),

        --3.សេវាកម្មផ្សេងៗ 
        @TYPE_OTHER_SERVICES NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV_SALE_LV NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT_SALE_LV NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING_SALE_LV NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV_SUN NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV_SUN_SALE_LV NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV_SUN_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_CUSTOMER_SPECIAl_FAVOR NVARCHAR(200),
        @TYPE_INDUSTRY_NO_SUBSIDY NVARCHAR(200),
        @TYPE_COMMERCIAL_NO_SUBSIDY NVARCHAR(200),
        @TYPE_COMMERCIAL_NO_SUBSIDY_SALE_LV_USAGE_OVER_2000 NVARCHAR(200),
        @TYPE_COMMERCIAL_SALE_LV_USAGE_OVER_2000 NVARCHAR(200),
        @TYPE_COMMERCIAL_SALE_SCHOOL_HOSPITAL_HEALTH_CENTER_CUSTOMER NVARCHAR(200),
        @TYPE_SUBSIDY NVARCHAR(200),
        @TYPE_SUBSIDY_INDUSTRY_COMMERCIAL_SALE_LV_USAGE_UNDER_2001 NVARCHAR(200),
        @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_NORMAL NVARCHAR(200),
        @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_0_10 NVARCHAR(200),
        @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_11_50 NVARCHAR(200),
        @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_51_200 NVARCHAR(200),
        @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_OVER_200 NVARCHAR(200),
        @BASED_TARIFF DECIMAL(18, 5),
        @SUBSIDY_TARIFF DECIMAL(18, 5),
        @TYPE_SUBSIDY_SPECIAL_FOR_MARKET_VENDOR NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_MARKET_VENDOR NVARCHAR(200),
        @TYPE_CUSTOMER_AGRICULTURE NVARCHAR(200),
        @TYPE_CUSTOMER_MV_AGRICULTURE NVARCHAR(200),
        @TYPE_CUSTOMER_MV_AGRICULTURE_MEDIUM NVARCHAR(200),
        @TYPE_CUSTOMER_MV_AGRICULTURE_MEDIUM_9_TO_7 NVARCHAR(200),
        @TYPE_CUSTOMER_LV_AGRICULTURE_LICENSE_BUY NVARCHAR(200),
        @TYPE_CUSTOMER_LV_AGRICULTURE_LICENSE_BUY_MEDIUM NVARCHAR(200),
        @TYPE_CUSTOMER_LV_AGRICULTURE_LICENSE_BUY_MEDIUM_9_TO_7 NVARCHAR(200),
        @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY NVARCHAR(200),
        @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY_MEDIUM NVARCHAR(200),
        @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY_MEDIUM_9_TO_7 NVARCHAR(200),
        @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER NVARCHAR(200),
        @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_GENERAL NVARCHAR(200),
        @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_9_TO_7 NVARCHAR(200);

--CHECK LICENSE TYPE ID
SELECT @LICENSE_TYPE_ID = LICENSE_TYPE_ID,
       @BASED_TARIFF = BASED_TARIFF,
       @SUBSIDY_TARIFF = SUBSIDY_TARIFF
FROM dbo.TBL_TARIFF
WHERE DATA_MONTH = @DATA_MONTH;
SET @TYPE_LICENSEE = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ';
SET @TYPE_LICENSEE_MV = N'1. ថាមពលលក់ភ្ជាប់ពីខ្សែបណ្តាញតង់ស្យុងមធ្យម MV';
SET @TYPE_LICENSEE_MV_LV_CUSTOMER_TRANSFO = N'2. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកទិញ)';
SET @TYPE_LICENSEE_MV_LV_LICENSEE_TRANSFO = N'3. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកលក់)';

SET @TYPE_INDUSTRY_NO_SUBSIDY
    = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (អ្នកប្រើប្រាស់ធុនធំ និងធុនមធ្យម ប្រភេទ ឧស្សាហកម្ម និងកសិកម្ម)';
SET @TYPE_MINING_INDUSTRY = N'1.​ ឧស្សាហកម្មវិស័យរុករករ៉ែ';
SET @TYPE_MINING_INDUSTRY_MV = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ MV';
SET @TYPE_MINING_INDUSTRY_MV_SALE_LV = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_MINING_INDUSTRY_MV_CUSTOMER_BUY_LV = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)';
SET @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV
    = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV
    = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)';
SET @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV
    = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV
    = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_MINING_INDUSTRY_MV_SUN = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV';
SET @TYPE_MINING_INDUSTRY_MV_SUN_SALE_LV
    = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_MINING_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV
    = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ';

SET @TYPE_MANUFACTURING_INDUSTRY = N'2.​ ឧស្សាហកម្មវិស័យកម្មន្តសាល';
SET @TYPE_MANUFACTURING_INDUSTRY_MV = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ MV';
SET @TYPE_MANUFACTURING_INDUSTRY_MV_SALE_LV = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_MANUFACTURING_INDUSTRY_MV_CUSTOMER_BUY_LV = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT
    = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)';
SET @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV
    = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV
    = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING
    = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)';
SET @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV
    = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV
    = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_MANUFACTURING_INDUSTRY_MV_SUN = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV';
SET @TYPE_MANUFACTURING_INDUSTRY_MV_SUN_SALE_LV
    = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_MANUFACTURING_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV
    = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ';

SET @TYPE_TEXTILE_INDUSTRY = N'3.​ ឧស្សាហកម្មវិស័យវាយនភណ្ឌ';
SET @TYPE_TEXTILE_INDUSTRY_MV = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ MV';
SET @TYPE_TEXTILE_INDUSTRY_MV_SALE_LV = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_TEXTILE_INDUSTRY_MV_CUSTOMER_BUY_LV = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)';
SET @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV
    = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV
    = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)';
SET @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV
    = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV
    = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_TEXTILE_INDUSTRY_MV_SUN = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV';
SET @TYPE_TEXTILE_INDUSTRY_MV_SUN_SALE_LV
    = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_TEXTILE_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV
    = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ';

SET @TYPE_AGRICULTURE = N'4.​ កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ';
SET @TYPE_AGRICULTURE_MV = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ MV';
SET @TYPE_AGRICULTURE_MV_SALE_LV = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_AGRICULTURE_MV_CUSTOMER_BUY_LV = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT
    = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)';
SET @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT_SALE_LV
    = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV
    = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING
    = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)';
SET @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING_SALE_LV
    = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV
    = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_AGRICULTURE_MV_SUN = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV';
SET @TYPE_AGRICULTURE_MV_SUN_SALE_LV
    = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_AGRICULTURE_MV_SUN_CUSTOMER_BUY_LV
    = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ';

SET @TYPE_ACTIVITIES = N'5.​ សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ។ល។)';
SET @TYPE_ACTIVITIES_MV = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ MV';
SET @TYPE_ACTIVITIES_MV_SALE_LV = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_ACTIVITIES_MV_CUSTOMER_BUY_LV = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)';
SET @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT_SALE_LV
    = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV
    = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)';
SET @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING_SALE_LV
    = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV
    = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_ACTIVITIES_MV_SUN = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV';
SET @TYPE_ACTIVITIES_MV_SUN_SALE_LV
    = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_ACTIVITIES_MV_SUN_CUSTOMER_BUY_LV
    = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ';

SET @TYPE_AGRICULTURAL = N'6.​ សកម្មភាពកែច្នៃផលិតផលកសិកម្ម';
SET @TYPE_AGRICULTURAL_MV = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ MV';
SET @TYPE_AGRICULTURAL_MV_SALE_LV = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_AGRICULTURAL_MV_CUSTOMER_BUY_LV = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)';
SET @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT_SALE_LV
    = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV
    = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)';
SET @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING_SALE_LV
    = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV
    = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_AGRICULTURAL_MV_SUN = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV';
SET @TYPE_AGRICULTURAL_MV_SUN_SALE_LV
    = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_AGRICULTURAL_MV_SUN_CUSTOMER_BUY_LV
    = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ';


SET @TYPE_COMMERCIAL_NO_SUBSIDY
    = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (អ្នកប្រើប្រាស់ធុនធំ និងធុនមធ្យម ប្រភេទពាណិជ្ជកម្ម រដ្ឋបាល និងផ្សេងៗ)';
SET @TYPE_BUSINESS = N'1.​ ពាណិជ្ជកម្ម';
SET @TYPE_BUSINESS_MV = N'	- ពាណិជ្ជកម្ម ទិញ MV';
SET @TYPE_BUSINESS_MV_SALE_LV = N'	- ពាណិជ្ជកម្ម ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_BUSINESS_MV_CUSTOMER_BUY_LV = N'	- ពាណិជ្ជកម្ម ទិញ LV ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT = N'	- ពាណិជ្ជកម្ម ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)';
SET @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT_SALE_LV
    = N'	- ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV
    = N'	- ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING = N'	- ពាណិជ្ជកម្ម ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)';
SET @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING_SALE_LV
    = N'	- ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV
    = N'	- ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_BUSINESS_MV_SUN = N'	- ពាណិជ្ជកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV';
SET @TYPE_BUSINESS_MV_SUN_SALE_LV = N'	- ពាណិជ្ជកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_BUSINESS_MV_SUN_CUSTOMER_BUY_LV
    = N'	- ពាណិជ្ជកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ';

SET @TYPE_PUBLIC_ADMINISTRATION = N'2.​ រដ្ឋបាលសាធារណៈ';
SET @TYPE_PUBLIC_ADMINISTRATION_MV = N'	- រដ្ឋបាលសាធារណៈ ទិញ MV';
SET @TYPE_PUBLIC_ADMINISTRATION_MV_SALE_LV = N'	- រដ្ឋបាលសាធារណៈ ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_PUBLIC_ADMINISTRATION_MV_CUSTOMER_BUY_LV = N'	- រដ្ឋបាលសាធារណៈ ទិញ LV ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT = N'	- រដ្ឋបាលសាធារណៈ ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)';
SET @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT_SALE_LV
    = N'	- រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV
    = N'	- រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING = N'	- រដ្ឋបាលសាធារណៈ ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)';
SET @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING_SALE_LV
    = N'	- រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV
    = N'	- រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_PUBLIC_ADMINISTRATION_MV_SUN = N'	- រដ្ឋបាលសាធារណៈ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV';
SET @TYPE_PUBLIC_ADMINISTRATION_MV_SUN_SALE_LV
    = N'	- រដ្ឋបាលសាធារណៈ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_PUBLIC_ADMINISTRATION_MV_SUN_CUSTOMER_BUY_LV
    = N'	- រដ្ឋបាលសាធារណៈ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ';

SET @TYPE_OTHER_SERVICES = N'3.​ សេវាកម្មផ្សេងៗ';
SET @TYPE_OTHER_SERVICES_MV = N'	- សេវាកម្មផ្សេងៗ ទិញ MV';
SET @TYPE_OTHER_SERVICES_MV_SALE_LV = N'	- សេវាកម្មផ្សេងៗ ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_OTHER_SERVICES_MV_CUSTOMER_BUY_LV = N'	- សេវាកម្មផ្សេងៗ ទិញ LV ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT = N'	- សេវាកម្មផ្សេងៗ ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)';
SET @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT_SALE_LV
    = N'	- សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV
    = N'	- សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING = N'	- សេវាកម្មផ្សេងៗ ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)';
SET @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING_SALE_LV
    = N'	- សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់';
SET @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV
    = N'	- សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ';
SET @TYPE_OTHER_SERVICES_MV_SUN = N'	- សេវាកម្មផ្សេងៗ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV';
SET @TYPE_OTHER_SERVICES_MV_SUN_SALE_LV
    = N'	- សេវាកម្មផ្សេងៗ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់';
SET @TYPE_OTHER_SERVICES_MV_SUN_CUSTOMER_BUY_LV
    = N'	- សេវាកម្មផ្សេងៗ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ';

SET @TYPE_COMMERCIAL_NO_SUBSIDY_SALE_LV_USAGE_OVER_2000
    = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែបណ្តាញសាធារណៈ)';
SET @TYPE_COMMERCIAL_SALE_LV_USAGE_OVER_2000
    = N'1. អតិថិជន ឧស្សាហកម្ម&កសិកម្ម, ពាណិជ្ជកម្ម រដ្ឋបាល&សេវាកម្ម ប្រើចាប់ពី 2001 kWh/ខែ ឡើង';

SET @TYPE_SUBSIDY = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)';
SET @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_NORMAL = N'1. ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន';
SET @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_0_10 = SPACE(9) + N'1.1.' + N' ថាមពលលក់ឲ្យលំនៅដ្ឋានប្រើ មិនលើសពី 10 kWh/ខែ';
SET @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_11_50 = SPACE(9) + N'1.2.' + N' ថាមពលលក់ឲ្យលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ';
SET @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_51_200
    = SPACE(9) + N'1.3.' + N' ថាមពលលក់ឲ្យលំនៅដ្ឋានប្រើចាប់ពី 51 ដល់ 200 kWh/ខែ';
SET @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_OVER_200 = SPACE(9) + N'1.4.' + N' ថាមពលលក់ឲ្យលំនៅដ្ឋានប្រើចាប់ពី 201 kWh/ខែ';
SET @TYPE_SUBSIDY_INDUSTRY_COMMERCIAL_SALE_LV_USAGE_UNDER_2001
    = N'2. អតិថិជន ឧស្សាហកម្ម&កសិកម្ម, ពាណិជ្ជកម្ម រដ្ឋបាល&សេវាកម្ម ប្រើក្រោម 2001 kWh/ខែ ';
SET @TYPE_CUSTOMER_SPECIAl_FAVOR = N'3. អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស';
SET @TYPE_COMMERCIAL_SALE_SCHOOL_HOSPITAL_HEALTH_CENTER_CUSTOMER
    = SPACE(9) + N'3.1.' + N' អតិថិជន សាលារៀន មន្ទីរពេទ្យ មណ្ឌលសុខភាព នៅតំបន់ជនបទ';
SET @TYPE_CUSTOMER_AGRICULTURE
    = N' អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម ភ្ជាប់ពីខ្សែបណ្តាញចែកចាយ MV និង LV ';
SET @TYPE_CUSTOMER_MV_AGRICULTURE = N'1. អ្នកប្រើប្រាស់ MV';
SET @TYPE_CUSTOMER_MV_AGRICULTURE_MEDIUM = N'	- ទិញ MV តាមអត្រាថ្លៃមធ្យម';
SET @TYPE_CUSTOMER_MV_AGRICULTURE_MEDIUM_9_TO_7 = N'	- ទិញ MV តាមអត្រាថ្លៃអនុគ្រោះ (ម៉ោង៩យប់ដល់៧ព្រឹក)';
SET @TYPE_CUSTOMER_LV_AGRICULTURE_LICENSE_BUY = N'2. អ្នកប្រើប្រាស់ LV (ត្រង់ស្វូរអ្នកលក់)';
SET @TYPE_CUSTOMER_LV_AGRICULTURE_LICENSE_BUY_MEDIUM = N'	- ទិញ LV ត្រង់ស្វូរអ្នកលក់ តាមអត្រាថ្លៃមធ្យម';
SET @TYPE_CUSTOMER_LV_AGRICULTURE_LICENSE_BUY_MEDIUM_9_TO_7
    = N'	- ទិញ LV ត្រង់ស្វូរអ្នកលក់ តាមអត្រាថ្លៃអនុគ្រោះ (ម៉ោង៩យប់ដល់៧ព្រឹក)';
SET @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY = N'3. អ្នកប្រើប្រាស់ LV (ត្រង់ស្វូរអ្នកទិញ)';
SET @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY_MEDIUM = N'	- ទិញ LV ត្រង់ស្វូរអ្នកទិញ តាមអត្រាថ្លៃមធ្យម';
SET @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY_MEDIUM_9_TO_7
    = N'	- ទិញ LV ត្រង់ស្វូរអ្នកទិញ តាមអត្រាថ្លៃអនុគ្រោះ (ម៉ោង៩យប់ដល់៧ព្រឹក)';
SET @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER = N'4. អ្នកប្រើប្រាស់ LV (ភ្ជាប់ពីខ្សែសាធារណៈ)';
SET @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_GENERAL = N'	- ទិញតាមអត្រាថ្លៃទូទៅ';
SET @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_9_TO_7 = N'	- ទិញតាមអត្រាថ្លៃអនុគ្រោះ (៩យប់ដល់៧ព្រឹក)';
SET @TYPE_SUBSIDY_SPECIAL_FOR_MARKET_VENDOR = N'ថាមពលលក់ឲ្យអតិថិជននៅក្នុងបរិវេណផ្សារ';
SET @TYPE_SALE_CUSTOMER_MARKET_VENDOR = N'1. ថាមពលលក់ឲ្យអតិថិជនក្នុងផ្សារ';

IF (@LICENSE_TYPE_ID = 1)
BEGIN
    -- Licensee Customer(MV)
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 6,
           TYPE_NAME = @TYPE_LICENSEE,
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = @TYPE_LICENSEE_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    INTO #RESULT
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 11
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- Licensee Customer(buyer's transformer)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 6,
           TYPE_NAME = @TYPE_LICENSEE,
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = @TYPE_LICENSEE_MV_LV_CUSTOMER_TRANSFO,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 12
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- Licensee Customer(license's transformer)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 6,
           TYPE_NAME = @TYPE_LICENSEE,
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = @TYPE_LICENSEE_MV_LV_LICENSEE_TRANSFO,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 13
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យរុករករ៉ែ 2021 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 77,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 26
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 76,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 27
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 75,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 28
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 74,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 29
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN



    --ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 73,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 30
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 72,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 31
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 71,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 32
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 70,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 33
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 69,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 34
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យរុករករ៉ែ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 68,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV_SUN,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 35
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យរុករករ៉ែ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 67,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV_SUN_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 36
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យរុករករ៉ែ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 66,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 37
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --END ឧស្សាហកម្មវិស័យរុករករ៉ែ 2021 

    --ឧស្សាហកម្មវិស័យកម្មន្តសាល 2021
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 64,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 38
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 63,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 39
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 62,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 40
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 61,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 41
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN



    --ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 60,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 42
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 59,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 43
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 58,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 44
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 57,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 45
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 56,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 46
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យកម្មន្តសាល មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 55,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV_SUN,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 47
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យកម្មន្តសាល មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 54,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV_SUN_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 48
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យកម្មន្តសាល មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 53,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 49
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- END ឧស្សាហកម្មវិស័យកម្មន្តសាល 2021

    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ 2021
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 51,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 50
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 50,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 51
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 49,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 52
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 48,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 53
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN



    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 47,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 54
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 46,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 55
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 45,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 56
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 44,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 57
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 43,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 58
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 42,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV_SUN,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 59
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 41,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV_SUN_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 60
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 40,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 61
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- END ឧស្សាហកម្មវិស័យវាយនភណ្ឌ 2021

    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ 2021
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 38,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 62
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 37,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 63
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 36,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 64
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 35,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 65
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN



    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 34,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 66
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 33,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 67
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 32,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 68
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 31,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 69
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 30,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 70
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 29,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV_SUN,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 71
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 28,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV_SUN_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 72
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 27,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV_SUN_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 73
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- END កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ 2021

    -- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) 2021
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 25,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 74
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    -- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 24,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 75
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 23,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 76
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    -- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 22,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 77
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN



    -- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 21,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 78
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 20,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 79
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 19,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 80
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 18,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 81
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 17,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 82
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 16,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV_SUN,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 83
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 15,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV_SUN_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 84
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 14,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV_SUN_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 85
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- END  សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) 2021


    -- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម 2021
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 12,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 86
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    -- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 87
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 88
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    -- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 89
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN



    -- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 90
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 91
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 92
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 93
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 94
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV_SUN,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 95
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV_SUN_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 96
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV_SUN_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 97
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- END សកម្មភាពកែច្នៃផលិតផលកសិកម្ម 2021


    -- ពាណិជ្ជកម្ម 2021
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_COMMERCIAL_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 38,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 98
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    -- ពាណិជ្ជកម្ម ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 37,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 99
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --ពាណិជ្ជកម្ម ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 36,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 100
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    -- ពាណិជ្ជកម្ម ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 35,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 101
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN



    -- ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 34,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 102
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 33,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 103
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- ពាណិជ្ជកម្ម ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 32,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 104
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 31,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 105
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 30,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 106
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- ពាណិជ្ជកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 29,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV_SUN,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 107
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- ពាណិជ្ជកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 28,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV_SUN_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 108
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- ពាណិជ្ជកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 27,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV_SUN_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 109
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- END ពាណិជ្ជកម្ម 2021


    -- រដ្ឋបាលសាធារណៈ 2021
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_COMMERCIAL_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 25,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 110
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    -- រដ្ឋបាលសាធារណៈ ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 24,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 111
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --រដ្ឋបាលសាធារណៈ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 23,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 112
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    -- រដ្ឋបាលសាធារណៈ ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 22,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 113
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN



    -- រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 21,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 114
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 20,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 115
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- រដ្ឋបាលសាធារណៈ ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 19,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 116
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 18,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 117
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 17,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 118
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- រដ្ឋបាលសាធារណៈ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 16,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_SUN,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 119
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- រដ្ឋបាលសាធារណៈ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 15,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_SUN_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 120
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- រដ្ឋបាលសាធារណៈ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 14,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_SUN_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 121
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- END រដ្ឋបាលសាធារណៈ 2021


    -- សេវាកម្មផ្សេងៗ 2021
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_COMMERCIAL_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 12,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 122
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    -- សេវាកម្មផ្សេងៗ ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 123
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --សេវាកម្មផ្សេងៗ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 124
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    -- សេវាកម្មផ្សេងៗ ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 125
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN



    -- សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 126
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 127
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សេវាកម្មផ្សេងៗ ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 128
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 129
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 130
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សេវាកម្មផ្សេងៗ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV_SUN,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 131
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សេវាកម្មផ្សេងៗ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV_SUN_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 132
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សេវាកម្មផ្សេងៗ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV_SUN_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 133
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- END សេវាកម្មផ្សេងៗ 2021

    -- for Normal Customer(TOTAL_USAGE>=2001) COMMERCIAL 2021
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 3,
           TYPE_NAME = @TYPE_COMMERCIAL_NO_SUBSIDY_SALE_LV_USAGE_OVER_2000,
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_COMMERCIAL_SALE_LV_USAGE_OVER_2000,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 22, 23, 134, 135, 136, 137, 138, 139, 140, 144 )
          AND i.TOTAL_USAGE >= 2001
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Total Normal Customer
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_SUBSIDY,
           TYPE_OF_SALE_ID = -12,
           TYPE_OF_SALE_NAME = @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_NORMAL,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = BASED_PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY BASED_PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Normal Customer(TOTAL_USAGE<=10)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_SUBSIDY,
           TYPE_OF_SALE_ID = -13,
           TYPE_OF_SALE_NAME = @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_0_10,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE <= 10
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Normal Customer(TOTAL_USAGE BETWEEN 11 AND 50)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_SUBSIDY,
           TYPE_OF_SALE_ID = -14,
           TYPE_OF_SALE_NAME = @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_11_50,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND (i.TOTAL_USAGE
          BETWEEN 11 AND 50
              )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Normal Customer(TOTAL_USAGE BETWEEN 51 AND 200)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_SUBSIDY,
           TYPE_OF_SALE_ID = -15,
           TYPE_OF_SALE_NAME = @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_51_200,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND (i.TOTAL_USAGE
          BETWEEN 51 AND 200
              )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Normal Customer(TOTAL_USAGE>=200)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_SUBSIDY,
           TYPE_OF_SALE_ID = -16,
           TYPE_OF_SALE_NAME = @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_OVER_200,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE >= 201
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Normal Customer(TOTAL_USAGE<2001)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_SUBSIDY,
           TYPE_OF_SALE_ID = -17,
           TYPE_OF_SALE_NAME = @TYPE_SUBSIDY_INDUSTRY_COMMERCIAL_SALE_LV_USAGE_UNDER_2001,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = BASED_PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 22, 23, 134, 135, 136, 137, 138, 139, 140, 144 )
          AND i.TOTAL_USAGE < 2001
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY BASED_PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Normal Customer SPECIAL
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_SUBSIDY,
           TYPE_OF_SALE_ID = -19,
           TYPE_OF_SALE_NAME = @TYPE_COMMERCIAL_SALE_SCHOOL_HOSPITAL_HEALTH_CENTER_CUSTOMER,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_AGRICULTURE,
           TYPE_OF_SALE_ID = -101,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_MV_AGRICULTURE_MEDIUM,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = ROUND((PRICE * 4120), 0),
           1,
           N'រៀល',
           N'៛',
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
          AND i.PRICE = 0.1370
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_AGRICULTURE,
           TYPE_OF_SALE_ID = -102,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_MV_AGRICULTURE_MEDIUM_9_TO_7,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_AGRICULTURE,
           TYPE_OF_SALE_ID = -104,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_AGRICULTURE_LICENSE_BUY_MEDIUM,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = ROUND((PRICE * 4120), 0),
           1,
           N'រៀល',
           N'៛',
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
          AND i.PRICE = 0.15048
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_AGRICULTURE,
           TYPE_OF_SALE_ID = -105,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_AGRICULTURE_LICENSE_BUY_MEDIUM_9_TO_7,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_AGRICULTURE,
           TYPE_OF_SALE_ID = -107,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY_MEDIUM,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = ROUND((PRICE * 4120), 0),
           1,
           N'រៀល',
           N'៛',
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
          AND i.PRICE = 0.14248
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_AGRICULTURE,
           TYPE_OF_SALE_ID = -108,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY_MEDIUM_9_TO_7,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_AGRICULTURE,
           TYPE_OF_SALE_ID = -110,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_GENERAL,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
          AND i.PRICE = 730
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_AGRICULTURE,
           TYPE_OF_SALE_ID = -111,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY_MEDIUM_9_TO_7,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN;

    CREATE TABLE #DEFAULT_RESULT
    (
        DATA_MONTH DATETIME,
        TYPE_ID INT,
        TYPE_NAME NVARCHAR(200),
        TYPE_OF_SALE_ID INT,
        TYPE_OF_SALE_NAME NVARCHAR(200),
        TOTAL_CUSTOMER INT,
        TOTAL_POWER_SOLD INT,
        RATE DECIMAL(18, 4),
        CURRENCY_ID INT,
        CURRENCY_NAME NVARCHAR(20),
        CURRENCY_SIGN NVARCHAR(5),
        IS_ACTIVE BIT
    );
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 6, @TYPE_LICENSEE, 5, @TYPE_LICENSEE_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 6, @TYPE_LICENSEE, 4, @TYPE_LICENSEE_MV_LV_CUSTOMER_TRANSFO, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 6, @TYPE_LICENSEE, 3, @TYPE_LICENSEE_MV_LV_LICENSEE_TRANSFO, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 78, @TYPE_MINING_INDUSTRY, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 77, @TYPE_MINING_INDUSTRY_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 76, @TYPE_MINING_INDUSTRY_MV_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 75, @TYPE_MINING_INDUSTRY_MV_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 74, @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 73, @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 72, @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV, 0,
     0  , 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 71, @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 70, @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 69, @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV, 0,
     0  , 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 68, @TYPE_MINING_INDUSTRY_MV_SUN, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 67, @TYPE_MINING_INDUSTRY_MV_SUN_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$',
     1  );
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 66, @TYPE_MINING_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    --END ឧស្សាហកម្មវិស័យរុករករ៉ែ


    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 65, @TYPE_MANUFACTURING_INDUSTRY, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 64, @TYPE_MANUFACTURING_INDUSTRY_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 63, @TYPE_MANUFACTURING_INDUSTRY_MV_SALE_LV, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 62, @TYPE_MANUFACTURING_INDUSTRY_MV_CUSTOMER_BUY_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 61, @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 60, @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV, 0, 0,
     0  , 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 59,
     @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 58, @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 57, @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV, 0, 0,
     0  , 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 56,
     @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 55, @TYPE_MANUFACTURING_INDUSTRY_MV_SUN, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 54, @TYPE_MANUFACTURING_INDUSTRY_MV_SUN_SALE_LV, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 53, @TYPE_MANUFACTURING_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    --END ឧស្សាហកម្មវិស័យកម្មន្តសាល


    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 52, @TYPE_TEXTILE_INDUSTRY, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 51, @TYPE_TEXTILE_INDUSTRY_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 50, @TYPE_TEXTILE_INDUSTRY_MV_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 49, @TYPE_TEXTILE_INDUSTRY_MV_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 48, @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 47, @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 46, @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV, 0,
     0  , 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 45, @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 44, @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 43, @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV, 0,
     0  , 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 42, @TYPE_TEXTILE_INDUSTRY_MV_SUN, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 41, @TYPE_TEXTILE_INDUSTRY_MV_SUN_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$',
     1  );
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 40, @TYPE_TEXTILE_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    --END ឧស្សាហកម្មវិស័យវាយនភណ្ឌ


    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 39, @TYPE_AGRICULTURE, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 38, @TYPE_AGRICULTURE_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 37, @TYPE_AGRICULTURE_MV_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 36, @TYPE_AGRICULTURE_MV_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា', N'$',
     1  );
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 35, @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 34, @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 33, @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV, 0, 0, 0,
     2  , N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 32, @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 31, @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 30, @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV, 0, 0, 0,
     2  , N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 29, @TYPE_AGRICULTURE_MV_SUN, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 28, @TYPE_AGRICULTURE_MV_SUN_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 27, @TYPE_AGRICULTURE_MV_SUN_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    --END កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ


    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 26, @TYPE_ACTIVITIES, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 25, @TYPE_ACTIVITIES_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 24, @TYPE_ACTIVITIES_MV_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 23, @TYPE_ACTIVITIES_MV_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 22, @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 21, @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 20, @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV, 0, 0, 0,
     2  , N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 19, @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 18, @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 17, @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV, 0, 0, 0,
     2  , N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 16, @TYPE_ACTIVITIES_MV_SUN, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 15, @TYPE_ACTIVITIES_MV_SUN_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 14, @TYPE_ACTIVITIES_MV_SUN_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    --END សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។)



    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 13, @TYPE_AGRICULTURAL, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 12, @TYPE_AGRICULTURAL_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 11, @TYPE_AGRICULTURAL_MV_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 10, @TYPE_AGRICULTURAL_MV_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា', N'$',
     1  );
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 9, @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 8, @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 7, @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV, 0, 0, 0,
     2  , N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 6, @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 5, @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 4, @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV, 0, 0, 0,
     2  , N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 3, @TYPE_AGRICULTURAL_MV_SUN, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 2, @TYPE_AGRICULTURAL_MV_SUN_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 1, @TYPE_AGRICULTURAL_MV_SUN_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    --END សកម្មភាពកែច្នៃផលិតផលកសិកម្ម


    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 39, @TYPE_BUSINESS, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 38, @TYPE_BUSINESS_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 37, @TYPE_BUSINESS_MV_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 36, @TYPE_BUSINESS_MV_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 35, @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 34, @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 33, @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV, 0, 0, 0,
     2  , N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 32, @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 31, @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 30, @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV, 0, 0, 0,
     2  , N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 29, @TYPE_BUSINESS_MV_SUN, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 28, @TYPE_BUSINESS_MV_SUN_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 27, @TYPE_BUSINESS_MV_SUN_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    --END ពាណិជ្ជកម្ម


    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 26, @TYPE_PUBLIC_ADMINISTRATION, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 25, @TYPE_PUBLIC_ADMINISTRATION_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 24, @TYPE_PUBLIC_ADMINISTRATION_MV_SALE_LV, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 23, @TYPE_PUBLIC_ADMINISTRATION_MV_CUSTOMER_BUY_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 22, @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 21, @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT_SALE_LV, 0,
     0  , 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 20,
     @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 19, @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 18, @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING_SALE_LV, 0,
     0  , 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 17,
     @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 16, @TYPE_PUBLIC_ADMINISTRATION_MV_SUN, 0, 0, 0, 2, N'ដុល្លា', N'$',
     1  );
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 15, @TYPE_PUBLIC_ADMINISTRATION_MV_SUN_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 14, @TYPE_PUBLIC_ADMINISTRATION_MV_SUN_CUSTOMER_BUY_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    --END រដ្ឋបាលសាធារណៈ


    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 13, @TYPE_OTHER_SERVICES, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 12, @TYPE_OTHER_SERVICES_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 11, @TYPE_OTHER_SERVICES_MV_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 10, @TYPE_OTHER_SERVICES_MV_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 9, @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 8, @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 7, @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV, 0,
     0  , 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 6, @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 5, @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 4, @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV, 0,
     0  , 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 3, @TYPE_OTHER_SERVICES_MV_SUN, 0, 0, 0, 2, N'ដុល្លា', N'$', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 2, @TYPE_OTHER_SERVICES_MV_SUN_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$',
     1  );
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 1, @TYPE_OTHER_SERVICES_MV_SUN_CUSTOMER_BUY_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1);
    --END សេវាកម្មផ្សេងៗ

    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 3, @TYPE_COMMERCIAL_NO_SUBSIDY_SALE_LV_USAGE_OVER_2000, 1, @TYPE_COMMERCIAL_SALE_LV_USAGE_OVER_2000,
     0  , 0, 0, 1, N'រៀល', N'៛', 1);


    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_SUBSIDY, -12, @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_NORMAL, 0, 0, 0, 1, N'រៀល', N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_SUBSIDY, -13, @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_0_10, 0, 0, 0, 1, N'រៀល', N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_SUBSIDY, -14, @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_11_50, 0, 0, 0, 1, N'រៀល', N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_SUBSIDY, -15, @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_51_200, 0, 0, 0, 1, N'រៀល', N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_SUBSIDY, -16, @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_OVER_200, 0, 0, 0, 1, N'រៀល', N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_SUBSIDY, -17, @TYPE_SUBSIDY_INDUSTRY_COMMERCIAL_SALE_LV_USAGE_UNDER_2001, 0, 0, 0, 1,
     N'រៀល', N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_SUBSIDY, -18, @TYPE_CUSTOMER_SPECIAl_FAVOR, 0, 0, 0, 1, N'រៀល', N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_SUBSIDY, -19, @TYPE_COMMERCIAL_SALE_SCHOOL_HOSPITAL_HEALTH_CENTER_CUSTOMER, 0, 0, 0, 1,
     N'រៀល', N'៛', 1);

    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -100, @TYPE_CUSTOMER_MV_AGRICULTURE, 0, 0, 0, 1, N'រៀល', N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -101, @TYPE_CUSTOMER_MV_AGRICULTURE_MEDIUM, 0, 0, 0, 1, N'រៀល', N'៛',
     1  );
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -102, @TYPE_CUSTOMER_MV_AGRICULTURE_MEDIUM_9_TO_7, 0, 0, 0, 1, N'រៀល',
     N'៛', 1);

    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -103, @TYPE_CUSTOMER_LV_AGRICULTURE_LICENSE_BUY, 0, 0, 0, 1, N'រៀល',
     N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -104, @TYPE_CUSTOMER_LV_AGRICULTURE_LICENSE_BUY_MEDIUM, 0, 0, 0, 1,
     N'រៀល', N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -105, @TYPE_CUSTOMER_LV_AGRICULTURE_LICENSE_BUY_MEDIUM_9_TO_7, 0, 0,
     0  , 1, N'រៀល', N'៛', 1);

    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -106, @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY, 0, 0, 0, 1, N'រៀល',
     N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -107, @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY_MEDIUM, 0, 0, 0, 1,
     N'រៀល', N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -108, @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY_MEDIUM_9_TO_7, 0, 0,
     0  , 1, N'រៀល', N'៛', 1);

    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -109, @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER, 0, 0, 0, 1, N'រៀល',
     N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -110, @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_GENERAL, 0, 0, 0, 1,
     N'រៀល', N'៛', 1);
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -111, @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_9_TO_7, 0, 0, 0, 1,
     N'រៀល', N'៛', 1);

    -- RESULT
    INSERT INTO TBL_REF_02
    SELECT *
    FROM #RESULT
    UNION ALL
    SELECT *
    FROM #DEFAULT_RESULT t
    WHERE NOT EXISTS
    (
        SELECT *
        FROM #RESULT
        WHERE TYPE_ID = t.TYPE_ID
              AND TYPE_OF_SALE_ID = t.TYPE_OF_SALE_ID
    )
    ORDER BY TYPE_ID DESC,
             TYPE_OF_SALE_ID DESC;
END;
ELSE IF (@LICENSE_TYPE_ID = 2)
BEGIN
    -- Market vendor customer
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_SUBSIDY_SPECIAL_FOR_MARKET_VENDOR,
           TYPE_OF_SALE_ID = -1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_MARKET_VENDOR,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    INTO #RESULT_MARKET_VENDOR
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 144 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN;
    CREATE TABLE #DEFAULT_RESULT_MARKET_VENDOR
    (
        DATA_MONTH DATETIME,
        TYPE_ID INT,
        TYPE_NAME NVARCHAR(200),
        TYPE_OF_SALE_ID INT,
        TYPE_OF_SALE_NAME NVARCHAR(200),
        TOTAL_CUSTOMER INT,
        TOTAL_POWER_SOLD INT,
        RATE DECIMAL(18, 4),
        CURRENCY_ID INT,
        CURRENCY_NAME NVARCHAR(20),
        CURRENCY_SIGN NVARCHAR(5),
        IS_ACTIVE BIT
    );
    INSERT INTO #DEFAULT_RESULT_MARKET_VENDOR
    VALUES
    (@DATA_MONTH, 1, @TYPE_SUBSIDY_SPECIAL_FOR_MARKET_VENDOR, -1, @TYPE_SALE_CUSTOMER_MARKET_VENDOR, 0, 0, 0, 1,
     N'រៀល', N'៛', 1);

    -- RESULT
    INSERT INTO TBL_REF_02
    SELECT *
    FROM #RESULT_MARKET_VENDOR
    UNION ALL
    SELECT *
    FROM #DEFAULT_RESULT_MARKET_VENDOR t
    WHERE NOT EXISTS
    (
        SELECT *
        FROM #RESULT_MARKET_VENDOR
        WHERE TYPE_ID = t.TYPE_ID
              AND TYPE_OF_SALE_ID = t.TYPE_OF_SALE_ID
    )
    ORDER BY TYPE_ID DESC,
             TYPE_OF_SALE_ID DESC;
END;
--END OF RUN_REF_02_2021

GO

IF EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'TLKP_CUSTOMER_GROUP'
          AND COLUMN_NAME = 'IS_ACTIVE'
          AND COLUMN_DEFAULT IS NOT NULL
)
    DECLARE @sql NVARCHAR(250);
SELECT @sql = N'ALTER TABLE TLKP_CUSTOMER_GROUP DROP CONSTRAINT ' + d.name
FROM
(
    SELECT name
    FROM sys.default_constraints
    WHERE name LIKE 'DF__TLKP_CUST__IS_AC%'
) AS d;
EXECUTE sp_executesql @sql;

ALTER TABLE dbo.TLKP_CUSTOMER_GROUP ALTER COLUMN IS_ACTIVE BIT NOT NULL;
GO

IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'TBL_CUSTOMER_BUY'
          AND COLUMN_NAME = 'EXCHANGE_RATE'
)
    ALTER TABLE dbo.TBL_CUSTOMER_BUY
    ADD EXCHANGE_RATE DECIMAL(16, 4)
            DEFAULT 1 NOT NULL;
GO

IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'TBL_CUSTOMER_BUY'
          AND COLUMN_NAME = 'EXCHANGE_RATE_DATE'
)
    ALTER TABLE dbo.TBL_CUSTOMER_BUY
    ADD EXCHANGE_RATE_DATE DATETIME
            DEFAULT '1900-01-01' NOT NULL;
GO
