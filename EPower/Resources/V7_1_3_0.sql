﻿IF EXISTS (
          SELECT    TOP 1  *
          FROM  dbo.TBL_UTILITY
          WHERE UTILITY_ID = 109
                AND UTILITY_NAME = N'UPDATE_SERVICE_URL'
          )
    DELETE  FROM TBL_UTILITY WHERE  UTILITY_ID = 109;
GO
IF OBJECT_ID ('NEXT_SEQUENCE') IS NOT NULL DROP PROC NEXT_SEQUENCE;
GO
CREATE PROC NEXT_SEQUENCE(@SEQUENCE_ID INT)
AS BEGIN
    DECLARE @CURRENT_VALUE AS INT;
    DECLARE @FORMAT AS NVARCHAR (300);
    DECLARE @NOW AS DATETIME;
    SET @NOW = SYSDATETIME ();
    DECLARE @SQUENCE_NO AS NVARCHAR (300);
    EXEC @SQUENCE_NO = dbo.GET_SEQUENCE @SEQUENCE_ID = @SEQUENCE_ID,
                                        @OFFSET = 0,
                                        @INVOICE_DATE = @NOW;
    SELECT  @CURRENT_VALUE = [VALUE],
            @FORMAT        = [FORMAT]
    FROM    dbo.TBL_SEQUENCE
    WHERE   SEQUENCE_ID = @SEQUENCE_ID;
    SET @CURRENT_VALUE = @CURRENT_VALUE + 1;
    UPDATE  dbo.TBL_SEQUENCE
    SET VALUE = CASE WHEN FORMAT LIKE '%{Y%'
                          AND   YEAR (DATE) != YEAR (@NOW) THEN 1
                WHEN FORMAT LIKE '%{M%'
                     AND MONTH (DATE) != MONTH (@NOW) THEN 1 ELSE @CURRENT_VALUE END,
        DATE = GETDATE ()
    WHERE   SEQUENCE_ID = @SEQUENCE_ID;
    SELECT  @SQUENCE_NO;
END;
--- END OF NEXT_SEQUENCE
GO
IF NOT EXISTS (
              SELECT    TOP 1  *
              FROM  TBL_ACCOUNT_CHART
              WHERE ACCOUNT_CODE = '16550'
                    AND IS_ACTIVE = 1
              )
    INSERT INTO TBL_ACCOUNT_CHART
    SELECT  '16550',
            N'គណនីសាច់ប្រាក់ទទួលពីអតិថិជន ជាប្រាក់បាត',
            '',
            '',
            1,
            74,
            1,
            19,
            3,
            0;
GO
IF NOT EXISTS (
              SELECT    TOP 1  *
              FROM  TBL_ACCOUNT_CHART
              WHERE ACCOUNT_CODE = '16551'
                    AND IS_ACTIVE = 1
              )
    INSERT INTO TBL_ACCOUNT_CHART
    SELECT  ACCOUNT_CODE = '16551',
            ACCOUNT_NAME = N'គណនីសាច់ប្រាក់ទទួលពីអតិថិជន ជាប្រាក់បាត',
            '',
            '',
            1,
            ACCOUNT_ID,
            1,
            19,
            3,
            0
    FROM    TBL_ACCOUNT_CHART
    WHERE   ACCOUNT_CODE = '16550';
GO
IF NOT EXISTS (
              SELECT    TOP 1  *
              FROM  TBL_ACCOUNT_CHART
              WHERE ACCOUNT_CODE = '16552'
                    AND IS_ACTIVE = 1
              )
    INSERT INTO TBL_ACCOUNT_CHART
    SELECT  ACCOUNT_CODE = '16552',
            ACCOUNT_NAME = N'មូលប្បទានបត្រ ជាប្រាក់បាត',
            '',
            '',
            1,
            ACCOUNT_ID,
            1,
            19,
            3,
            0
    FROM    TBL_ACCOUNT_CHART
    WHERE   ACCOUNT_CODE = '16550';
GO
IF NOT EXISTS (
              SELECT    TOP 1  *
              FROM  TBL_ACCOUNT_CHART
              WHERE ACCOUNT_CODE = '16553'
                    AND IS_ACTIVE = 1
              )
    INSERT INTO TBL_ACCOUNT_CHART
    SELECT  ACCOUNT_CODE = '16553',
            ACCOUNT_NAME = N'រតនាគារ ជាប្រាក់បាត',
            '',
            '',
            1,
            ACCOUNT_ID,
            1,
            19,
            3,
            0
    FROM    TBL_ACCOUNT_CHART
    WHERE   ACCOUNT_CODE = '16550';
GO
IF NOT EXISTS (
              SELECT    TOP 1  *
              FROM  TBL_ACCOUNT_CHART
              WHERE ACCOUNT_CODE = '16554'
                    AND IS_ACTIVE = 1
              )
    INSERT INTO TBL_ACCOUNT_CHART
    SELECT  ACCOUNT_CODE = '16554',
            ACCOUNT_NAME = N'បង្កាន់ដៃធនាគារ ជាប្រាក់បាត',
            '',
            '',
            1,
            ACCOUNT_ID,
            1,
            19,
            3,
            0
    FROM    TBL_ACCOUNT_CHART
    WHERE   ACCOUNT_CODE = '16550';
GO
IF OBJECT_ID ('REPORT_PAYMENT_DETAIL_BY_RECIEPT') IS NOT NULL
    DROP PROC REPORT_PAYMENT_DETAIL_BY_RECIEPT;
GO
CREATE PROC [dbo].[REPORT_PAYMENT_DETAIL_BY_RECIEPT]
    @D1                 DATETIME = '2021-12-07 00:00:00',
    @D2                 DATETIME = '2021-12-07 23:59:00',
    @ITEM_TYPE_ID       INT      = 0,
    @ITEM_ID            INT      = 0,
    @INCLUDE_DELETE     BIT      = 1,
    @CURRENCY_ID        INT      = 0,
    @AREA_ID            INT      = 0,
    @BILLING_CYCLE_ID   INT      = 0,
    @PAYMENT_ACCOUNT_ID INT      = 0,
    @DATA_MONTH         DATETIME = '1900-01-01'
AS
SELECT  DISTINCT INVOICE_ID = inv.INVOICE_ID
INTO    #InvoiceIds
FROM    dbo.TBL_INVOICE                   inv
        INNER JOIN dbo.TBL_INVOICE_DETAIL invd ON inv.INVOICE_ID = invd.INVOICE_ID
        INNER JOIN dbo.TBL_INVOICE_ITEM   invt ON invt.INVOICE_ITEM_ID = invd.INVOICE_ITEM_ID
WHERE(@ITEM_ID = 0 OR   invt.INVOICE_ITEM_ID = @ITEM_ID)
     AND (@ITEM_TYPE_ID = 0 OR  invt.INVOICE_ITEM_TYPE_ID = @ITEM_TYPE_ID);
SELECT  TYPE_ID                   = CASE WHEN i.IS_SERVICE_BILL = 0 THEN 1
                                    WHEN i.IS_SERVICE_BILL = 1 THEN 2 END,
        TYPE_NAME                 = CASE WHEN i.IS_SERVICE_BILL = 0 THEN N'ថ្លៃអគ្គិសនី'
                                    WHEN i.IS_SERVICE_BILL = 1 THEN N'ថ្លៃសេវាកម្ម' END,
        cx.CURRENCY_ID,
        cx.CURRENCY_NAME,
        cx.CURRENCY_SING,
        c.CUSTOMER_CODE,
        CUSTOMER_NAME             = c.LAST_NAME_KH + ' ' + c.FIRST_NAME_KH,
        r.AREA_NAME,
        p.PAYMENT_NO,
        p.PAY_DATE,
        CREATE_BY                 = ISNULL (bpd.BANK_ALIAS, p.CREATE_BY),
        ISNULL (pd.PAY_AMOUNT, 0) AS [AMOUNT]
FROM    TBL_PAYMENT                           p
        INNER JOIN TBL_PAYMENT_DETAIL         pd ON pd.PAYMENT_ID = p.PAYMENT_ID
        INNER JOIN TBL_INVOICE                i ON i.INVOICE_ID = pd.INVOICE_ID
        INNER JOIN #InvoiceIds                invId ON invId.INVOICE_ID = i.INVOICE_ID
        INNER JOIN TBL_CUSTOMER               c ON i.CUSTOMER_ID = c.CUSTOMER_ID
        INNER JOIN TBL_AREA                   r ON r.AREA_ID = c.AREA_ID
        INNER JOIN TLKP_CURRENCY              cx ON cx.CURRENCY_ID = p.CURRENCY_ID
        LEFT JOIN dbo.TBL_BANK_PAYMENT_DETAIL bpd ON bpd.BANK_PAYMENT_DETAIL_ID = p.BANK_PAYMENT_DETAIL_ID
WHERE(p.PAY_DATE BETWEEN @D1 AND @D2)
     AND p.USER_CASH_DRAWER_ID != 0
     -- AND pd.PAY_AMOUNT>0
     -- filter payment that removed
     AND (@INCLUDE_DELETE = 1 OR p.IS_VOID = 0)
     AND (@CURRENCY_ID = 0 OR   cx.CURRENCY_ID = @CURRENCY_ID)
     AND (@AREA_ID = 0 OR   c.AREA_ID = @AREA_ID)
     AND (@BILLING_CYCLE_ID = 0 OR  c.BILLING_CYCLE_ID = @BILLING_CYCLE_ID)
     AND (@PAYMENT_ACCOUNT_ID = 0 OR p.PAYMENT_ACCOUNT_ID = @PAYMENT_ACCOUNT_ID)
     AND (@DATA_MONTH = '1900-01-01' OR i.INVOICE_MONTH = @DATA_MONTH)
UNION ALL
SELECT  TYPE_ID              = -7,
        TYPE_NAME            = N'ប្រាក់តម្កល់',
        cx.CURRENCY_ID,
        cx.CURRENCY_NAME,
        cx.CURRENCY_SING,
        c.CUSTOMER_CODE,
        CUSTOMER_NAME        = c.LAST_NAME_KH + ' ' + c.FIRST_NAME_KH,
        r.AREA_NAME,
        p.PREPAYMENT_NO,
        p.CREATE_ON,
        CREATE_BY            = ISNULL (bpd.BANK_ALIAS, p.CREATE_BY),
        ISNULL (p.AMOUNT, 0) AS [AMOUNT]
FROM    dbo.TBL_CUS_PREPAYMENT                p
        INNER JOIN dbo.TBL_CUSTOMER           c ON p.CUSTOMER_ID = c.CUSTOMER_ID
        INNER JOIN dbo.TBL_AREA               r ON r.AREA_ID = c.AREA_ID
        INNER JOIN dbo.TLKP_CURRENCY          cx ON p.CURRENCY_ID = cx.CURRENCY_ID
        LEFT JOIN dbo.TBL_BANK_PAYMENT_DETAIL bpd ON bpd.BANK_PAYMENT_DETAIL_ID = p.BANK_PAYMENT_DETAIL_ID
WHERE   p.USER_CASH_DRAWER_ID != 0
        AND (@CURRENCY_ID = 0 OR cx.CURRENCY_ID = @CURRENCY_ID)
        AND p.CREATE_ON BETWEEN @D1 AND @D2
        AND p.ACTION_ID IN (1, 3)
        AND (@INCLUDE_DELETE = 1 OR p.ACTION_ID != 4)
        AND (
            @INCLUDE_DELETE = 1
            OR  p.CUS_PREPAYMENT_ID NOT IN(
                                          SELECT    rm.REVERSE_FROM_PREPAYMENT_ID
                                          FROM  dbo.TBL_CUS_PREPAYMENT rm
                                          WHERE rm.ACTION_ID = 4
                                          )
            )
        AND (@ITEM_ID = 0 OR @ITEM_ID = -7)
        AND (@ITEM_TYPE_ID = 0)
        AND (@AREA_ID = 0 OR c.AREA_ID = @AREA_ID)
        AND (@BILLING_CYCLE_ID = 0 OR   c.BILLING_CYCLE_ID = @BILLING_CYCLE_ID)
        AND (@PAYMENT_ACCOUNT_ID = 0 OR p.PAYMENT_ACCOUNT_ID = @PAYMENT_ACCOUNT_ID)
UNION ALL
SELECT  TYPE_ID               = -6,
        TYPE_NAME             = N'ប្រាក់កក់',
        cx.CURRENCY_ID,
        cx.CURRENCY_NAME,
        cx.CURRENCY_SING,
        c.CUSTOMER_CODE,
        CUSTOMER_NAME         = c.LAST_NAME_KH + ' ' + c.FIRST_NAME_KH,
        r.AREA_NAME,
        cd.DEPOSIT_NO,
        cd.CREATE_ON,
        CREATE_BY             = ISNULL (bpd.BANK_ALIAS, cd.CREATE_BY),
        ISNULL (cd.AMOUNT, 0) AS [AMOUNT]
FROM    dbo.TBL_CUS_DEPOSIT                   cd
        INNER JOIN dbo.TBL_CUSTOMER           c ON cd.CUSTOMER_ID = c.CUSTOMER_ID
        INNER JOIN dbo.TBL_AREA               r ON r.AREA_ID = c.AREA_ID
        INNER JOIN dbo.TLKP_CURRENCY          cx ON cd.CURRENCY_ID = cx.CURRENCY_ID
        LEFT JOIN dbo.TBL_BANK_PAYMENT_DETAIL bpd ON bpd.BANK_PAYMENT_DETAIL_ID = cd.BANK_PAYMENT_DETAIL_ID
WHERE   cd.USER_CASH_DRAWER_ID != 0
        AND (@CURRENCY_ID = 0 OR cx.CURRENCY_ID = @CURRENCY_ID)
        AND cd.CREATE_ON BETWEEN @D1 AND @D2
        AND (@INCLUDE_DELETE = 1 OR cd.DEPOSIT_ACTION_ID != 4)
        AND cd.IS_PAID = 1
        AND (@ITEM_ID = 0 OR @ITEM_ID = -6)
        AND (@ITEM_TYPE_ID = 0)
        AND (@AREA_ID = 0 OR c.AREA_ID = @AREA_ID)
        AND (@BILLING_CYCLE_ID = 0 OR   c.BILLING_CYCLE_ID = @BILLING_CYCLE_ID)
ORDER BY p.PAY_DATE ASC;
--- END OF REPORT_PAYMENT_DETAIL_BY_RECIEPT
GO
IF OBJECT_ID ('REPORT_SOLD_BY_CUSTOMER_TYPE') IS NOT NULL
    DROP PROC REPORT_SOLD_BY_CUSTOMER_TYPE;
GO
CREATE PROC dbo.REPORT_SOLD_BY_CUSTOMER_TYPE
    @FromDate         DATETIME = '2022-05-01',
    @ToDate           DATETIME = '2022-05-31',
    @BILLING_CYCLE_ID INT      = 0
AS

--- temp invs
SELECT  i.CUSTOMER_ID,
        i.INVOICE_ID,
        USAGE      = MAX (i.TOTAL_USAGE),
        AMOUNT     = SUM (d.AMOUNT),
        i.CURRENCY_ID,
        i.CUSTOMER_CONNECTION_TYPE_ID,
        i.IS_SERVICE_BILL,
        d.INVOICE_ITEM_ID,
        INVOICE_NO = MAX (i.INVOICE_NO)
INTO    #invs
FROM    dbo.TBL_INVOICE_DETAIL     d
        INNER JOIN dbo.TBL_INVOICE i ON i.INVOICE_ID = d.INVOICE_ID
WHERE   d.INVOICE_ITEM_ID != -1
        AND i.INVOICE_STATUS NOT IN (3)
        AND d.TRAN_DATE BETWEEN @FromDate AND @ToDate
GROUP BY i.CUSTOMER_ID,
         i.INVOICE_ID,
         i.CURRENCY_ID,
         d.INVOICE_ITEM_ID,
         i.IS_SERVICE_BILL,
         i.CUSTOMER_CONNECTION_TYPE_ID;

--- temp adjs
SELECT  a.INVOICE_ID,
        i.CUSTOMER_CONNECTION_TYPE_ID,
        i.CUSTOMER_ID,
        i.CURRENCY_ID,
        i.IS_SERVICE_BILL,
        INVOICE_NO      = MAX (i.INVOICE_NO),
        ADJUST_USAGE    = SUM (a.ADJUST_USAGE),
        ADJUST_AMOUNT   = SUM (a.ADJUST_AMOUNT),
        INVOICE_ITEM_ID = MAX (it.INVOICE_ITEM_ID)
INTO    #adjs
FROM    dbo.TBL_INVOICE_ADJUSTMENT a
        INNER JOIN dbo.TBL_INVOICE i ON i.INVOICE_ID = a.INVOICE_ID
        OUTER APPLY(
                   SELECT   TOP 1   INVOICE_ITEM_ID
                   FROM dbo.TBL_INVOICE_DETAIL
                   WHERE   INVOICE_ID = i.INVOICE_ID
                   )               it
WHERE   a.CREATE_ON BETWEEN @FromDate AND @ToDate
        --AND i.IS_SERVICE_BILL = 1
        AND i.INVOICE_STATUS NOT IN (3)
GROUP BY a.INVOICE_ID,
         i.CUSTOMER_CONNECTION_TYPE_ID,
         i.CUSTOMER_ID,
         i.CURRENCY_ID,
         i.IS_SERVICE_BILL;
SELECT  [TYPE_ID]       = 1,    ---Invoice bill
        [TYPE_NAME]     = N'INVOICE_BILL',
        i.CURRENCY_ID,
        g.CUSTOMER_GROUP_ID,
        g.CUSTOMER_GROUP_NAME,
        c.CUSTOMER_CONNECTION_TYPE_ID,
        c.CUSTOMER_CONNECTION_TYPE_NAME,
        CUSTOMER_NAME   = MAX (cus.CUSTOMER_CODE) + '-' + MAX (cus.LAST_NAME_KH) + ' ' + MAX (cus.FIRST_NAME_KH),
        INVOICE_NO      = MAX (i.INVOICE_NO),
        TOTAL_CUSTOMER  = MAX (CASE WHEN cus.IS_REACTIVE = 0 AND i.INVOICE_ITEM_ID = 1 THEN 1 ELSE 0 END),
        KWH_USAGE       = SUM (   CASE WHEN cus.IS_REACTIVE = 0
                                            AND   i.INVOICE_ITEM_ID = 1 THEN i.USAGE ELSE 0 END
                              ),
        KVAR_USAGE      = SUM (   CASE WHEN cus.IS_REACTIVE = 1
                                            AND  i.INVOICE_ITEM_ID = 1 THEN i.USAGE ELSE 0 END
                              ),
        KWH_AMOUNT      = SUM (   CASE WHEN cus.IS_REACTIVE = 0
                                            AND  i.INVOICE_ITEM_ID = 1 THEN i.AMOUNT ELSE 0 END
                              ),
        KVAR_AMOUNT     = SUM (   CASE WHEN cus.IS_REACTIVE = 1
                                            AND i.INVOICE_ITEM_ID = 1 THEN i.AMOUNT ELSE 0 END
                              ),
        CAPACITY_CHARGE = SUM (CASE WHEN i.INVOICE_ITEM_ID = -8 THEN i.AMOUNT ELSE 0 END),
        TOTAL_AMOUNT    = SUM (ISNULL (i.AMOUNT, 0))
INTO    #tmp
FROM    dbo.TLKP_CUSTOMER_GROUP                      g
        INNER JOIN dbo.TLKP_CUSTOMER_CONNECTION_TYPE c ON c.NONLICENSE_CUSTOMER_GROUP_ID = g.CUSTOMER_GROUP_ID
        LEFT JOIN #invs                              i ON i.CUSTOMER_CONNECTION_TYPE_ID = c.CUSTOMER_CONNECTION_TYPE_ID
        LEFT JOIN dbo.TBL_CUSTOMER                   cus ON cus.CUSTOMER_ID = i.CUSTOMER_ID
WHERE(
     INVOICE_ITEM_ID IS NULL
     OR i.INVOICE_ITEM_ID IN (-8, 1)
        AND (@BILLING_CYCLE_ID = 0 OR   cus.BILLING_CYCLE_ID = @BILLING_CYCLE_ID)
     )
GROUP BY i.CURRENCY_ID,
         g.CUSTOMER_GROUP_ID,
         g.CUSTOMER_GROUP_NAME,
         c.CUSTOMER_CONNECTION_TYPE_ID,
         c.CUSTOMER_CONNECTION_TYPE_NAME,
         cus.CUSTOMER_ID
UNION ALL
(SELECT [TYPE_ID]       = 2,
        [TYPE_NAME]     = N'BILL_ADJUSTMENT',
        i.CURRENCY_ID,
        g.CUSTOMER_GROUP_ID,
        g.CUSTOMER_GROUP_NAME,
        c.CUSTOMER_CONNECTION_TYPE_ID,
        c.CUSTOMER_CONNECTION_TYPE_NAME,
        CUSTOMER_NAME   = MAX (cus.CUSTOMER_CODE) + '-' + MAX (cus.LAST_NAME_KH) + ' ' + MAX (cus.FIRST_NAME_KH),
        INVOICE_NO      = MAX (i.INVOICE_NO),
        TOTAL_CUSTOMER  = MAX (CASE WHEN cus.IS_REACTIVE = 0 THEN 1 ELSE 0 END),
        KWH_USAGE       = SUM (CASE WHEN cus.IS_REACTIVE = 0 THEN i.ADJUST_USAGE ELSE 0 END),
        KVAR_USAGE      = SUM (CASE WHEN cus.IS_REACTIVE = 1 THEN i.ADJUST_USAGE ELSE 0 END),
        KWH_AMOUNT      = SUM (CASE WHEN cus.IS_REACTIVE = 0 THEN i.ADJUST_AMOUNT ELSE 0 END),
        KVAR_AMOUNT     = SUM (CASE WHEN cus.IS_REACTIVE = 1 THEN i.ADJUST_AMOUNT ELSE 0 END),
        CAPACITY_CHARGE = 0,
        TOTAL_AMOUNT    = SUM (ISNULL (i.ADJUST_AMOUNT, 0))
 FROM   dbo.TLKP_CUSTOMER_GROUP                      g
        INNER JOIN dbo.TLKP_CUSTOMER_CONNECTION_TYPE c ON c.NONLICENSE_CUSTOMER_GROUP_ID = g.CUSTOMER_GROUP_ID
        LEFT JOIN #adjs                              i ON i.CUSTOMER_CONNECTION_TYPE_ID = c.CUSTOMER_CONNECTION_TYPE_ID
        LEFT JOIN dbo.TBL_CUSTOMER                   cus ON cus.CUSTOMER_ID = i.CUSTOMER_ID
 WHERE  (i.IS_SERVICE_BILL IS NULL OR   i.IS_SERVICE_BILL = 0)
        AND (@BILLING_CYCLE_ID = 0 OR   cus.BILLING_CYCLE_ID = @BILLING_CYCLE_ID)
 GROUP BY i.CURRENCY_ID,
          g.CUSTOMER_GROUP_ID,
          g.CUSTOMER_GROUP_NAME,
          c.CUSTOMER_CONNECTION_TYPE_ID,
          c.CUSTOMER_CONNECTION_TYPE_NAME,
          cus.CUSTOMER_ID);
SELECT  t.TYPE_ID,
        t.TYPE_NAME,
        c.*,
        t.CUSTOMER_GROUP_ID,
        t.CUSTOMER_GROUP_NAME,
        t.CUSTOMER_CONNECTION_TYPE_ID,
        t.CUSTOMER_CONNECTION_TYPE_NAME,
        CUSTOMER_NAME   = t.CUSTOMER_NAME,
        INVOICE_NO      = t.INVOICE_NO,
        TOTAL_CUSTOMER  = CASE WHEN c.CURRENCY_ID = t.CURRENCY_ID THEN t.TOTAL_CUSTOMER ELSE 0 END,
        KWH_USAGE       = CASE WHEN c.CURRENCY_ID = t.CURRENCY_ID THEN t.KWH_USAGE ELSE 0 END,
        KVAR_USAGE      = CASE WHEN c.CURRENCY_ID = t.CURRENCY_ID THEN t.KVAR_USAGE ELSE 0 END,
        KWH_AMOUNT      = CASE WHEN c.CURRENCY_ID = t.CURRENCY_ID THEN t.KWH_AMOUNT ELSE 0 END,
        KVAR_AMOUNT     = CASE WHEN c.CURRENCY_ID = t.CURRENCY_ID THEN t.KVAR_AMOUNT ELSE 0 END,
        CAPACITY_CHARGE = CASE WHEN c.CURRENCY_ID = t.CURRENCY_ID THEN t.CAPACITY_CHARGE ELSE 0 END,
        TOTAL_AMOUNT    = CASE WHEN c.CURRENCY_ID = t.CURRENCY_ID THEN t.TOTAL_AMOUNT ELSE 0 END
FROM    dbo.TLKP_CURRENCY                c
        OUTER APPLY(SELECT  * FROM  #tmp) t
UNION ALL
(SELECT [TYPE_ID]                     = 3,
        [TYPE_NAME]                   = N'INVOICE_SERVICE',
        c.*,
        CUSTOMER_GROUP_ID             = t.INVOICE_ITEM_ID,
        CUSTOMER_GROUP_NAME           = t.INVOICE_ITEM_NAME,
        CUSTOMER_CONNECTION_TYPE_ID   = 0,
        CUSTOMER_CONNECTION_TYPE_NAME = '',
        CUSTOMER_NAME                 = cus.CUSTOMER_CODE + '-' + cus.LAST_NAME_KH + ' ' + cus.FIRST_NAME_KH,
        INVOICE_NO                    = ISNULL (i.INVOICE_NO, ''),
        TOTAL_CUSTOMER                = 1,  --i.CUSTOMER_ID,
        KWH_USAGE                     = 0,
        KVAR_USAGE                    = 0,
        KWH_AMOUNT                    = 0,
        KVAR_AMOUNT                   = 0,
        CAPACITY_CHARGE               = 0,
        TOTAL_AMOUNT                  = i.AMOUNT
 FROM   #invs                           i
        INNER JOIN dbo.TBL_INVOICE_ITEM t ON t.INVOICE_ITEM_ID = i.INVOICE_ITEM_ID
        INNER JOIN dbo.TLKP_CURRENCY    c ON c.CURRENCY_ID = i.CURRENCY_ID
        INNER JOIN dbo.TBL_CUSTOMER     cus ON cus.CUSTOMER_ID = i.CUSTOMER_ID
 WHERE  i.INVOICE_ITEM_ID NOT IN (-8, 1)
        AND (@BILLING_CYCLE_ID = 0 OR   cus.BILLING_CYCLE_ID = @BILLING_CYCLE_ID))
UNION ALL
(SELECT [TYPE_ID]                     = 4,
        [TYPE_NAME]                   = N'SERVICE_ADJUSTMENT',
        c.*,
        CUSTOMER_GROUP_ID             = t.INVOICE_ITEM_ID,
        CUSTOMER_GROUP_NAME           = t.INVOICE_ITEM_NAME,
        CUSTOMER_CONNECTION_TYPE_ID   = 0,
        CUSTOMER_CONNECTION_TYPE_NAME = '',
        CUSTOMER_NAME                 = cus.CUSTOMER_CODE + '-' + cus.LAST_NAME_KH + ' ' + cus.FIRST_NAME_KH,
        INVOICE_NO                    = i.INVOICE_NO,
        TOTAL_CUSTOMER                = 1,  --i.CUSTOMER_ID,
        KWH_USAGE                     = 0,
        KVAR_USAGE                    = 0,
        KWH_AMOUNT                    = 0,
        KVAR_AMOUNT                   = 0,
        CAPACITY_CHARGE               = 0,
        TOTAL_AMOUNT                  = i.ADJUST_AMOUNT
 FROM   #adjs                           i
        INNER JOIN dbo.TBL_INVOICE_ITEM t ON t.INVOICE_ITEM_ID = i.INVOICE_ITEM_ID
        INNER JOIN dbo.TLKP_CURRENCY    c ON c.CURRENCY_ID = i.CURRENCY_ID
        INNER JOIN dbo.TBL_CUSTOMER     cus ON cus.CUSTOMER_ID = i.CUSTOMER_ID
 WHERE  i.INVOICE_ITEM_ID != 1
        AND i.IS_SERVICE_BILL = 1
        AND (@BILLING_CYCLE_ID = 0 OR   cus.BILLING_CYCLE_ID = @BILLING_CYCLE_ID))
UNION ALL
(SELECT [TYPE_ID]                     = 5,
        [TYPE_NAME]                   = N'DEPOSIT',
        c.*,
        CUSTOMER_GROUP_ID             = 0,
        CUSTOMER_GROUP_NAME           = CASE WHEN d.AMOUNT >= 0 THEN N'កក់ប្រាក់' ELSE N'បង្វិលសង' END,
        CUSTOMER_CONNECTION_TYPE_ID   = 0,
        CUSTOMER_CONNECTION_TYPE_NAME = '',
        CUSTOMER_NAME                 = cus.CUSTOMER_CODE + '-' + cus.LAST_NAME_KH + ' ' + cus.FIRST_NAME_KH,
        INVOICE_NO                    = d.DEPOSIT_NO,
        TOTAL_CUSTOMER                = 1,  --COUNT(d.CUSTOMER_ID),
        KWH_USAGE                     = 0,
        KVAR_USAGE                    = 0,
        KWH_AMOUNT                    = 0,
        KVAR_AMOUNT                   = 0,
        CAPACITY_CHARGE               = 0,
        TOTAL_AMOUNT                  = d.AMOUNT
 FROM   dbo.TBL_CUS_DEPOSIT         d
        LEFT JOIN dbo.TBL_CUSTOMER  cus ON cus.CUSTOMER_ID = d.CUSTOMER_ID
        LEFT JOIN dbo.TLKP_CURRENCY c ON c.CURRENCY_ID = d.CURRENCY_ID
 WHERE  d.DEPOSIT_DATE BETWEEN @FromDate AND @ToDate
        AND d.IS_PAID = 1
        AND (@BILLING_CYCLE_ID = 0 OR   cus.BILLING_CYCLE_ID = @BILLING_CYCLE_ID));

--- END OF REPORT_SOLD_BY_CUSTOMER_TYPE
GO
IF OBJECT_ID ('RUN_REF_03B_2017') IS NOT NULL DROP PROC RUN_REF_03B_2017;
GO
CREATE PROC [dbo].[RUN_REF_03B_2017]
    @DATA_MONTH       DATETIME = '2017-03-01',
    @BILLING_CYCLE_ID INT      = 0,
    @AREA_ID          INT      = 0
AS
-- CLEAR DATA
DELETE  FROM TBL_REF_03B WHERE  DATA_MONTH = @DATA_MONTH;
DECLARE @TYPE_SALE_CUSTOMER_USAGE_0_10  NVARCHAR (200),
        @TYPE_SALE_CUSTOMER_USAGE_11_50 NVARCHAR (200),
        @TYPE_SALE_CUSTOMER_AGRI        NVARCHAR (200);
SET @TYPE_SALE_CUSTOMER_USAGE_0_10 = N'លក់ឲ្យលំនៅដ្ឋានប្រើ មិនលើស  10 kWh/ខែ';
SET @TYPE_SALE_CUSTOMER_USAGE_11_50 = N'លក់ឲ្យលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ';
SET @TYPE_SALE_CUSTOMER_AGRI = N'លក់ឲ្យការបូមទឹកធ្វើកសិកម្មម៉ោង09:00យប់ ដល់07:00ព្រឹក';
INSERT INTO TBL_REF_03B(
                       DATA_MONTH, ROW_NO, TYPE_OF_SALE_ID, TYPE_OF_SALE_NAME, TOTAL_CUSTOMER, TOTAL_POWER_SOLD, PRICE, SUBSIDY_RATE, SUBSIDY_AMOUNT, CURRENCY_ID, CURRENCY_NAME, CURRENCY_SIGN, IS_ACTIVE
                       )
SELECT  DATA_MONTH        = @DATA_MONTH,
        ROW_NO            = N'1',
        TYPE_OF_SALE_ID   = 1,
        TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_0_10,
        TOTAL_CUSTOMER    = COUNT (*),
        TOTAL_SOLD        = SUM (i.TOTAL_USAGE),
        RATE              = i.PRICE,
        SUBSIDY_RATE      = ISNULL (t.SUBSIDY_TARIFF, 0) - i.PRICE,
        SUBSIDY_AMOUNT    = (ISNULL (t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM (i.TOTAL_USAGE),
        i.CURRENCY_ID,
        i.CURRENCY_NAME,
        i.CURRENCY_SIGN,
        IS_ACTIVE         = 1
FROM    TBL_REF_INVOICE      i
        LEFT JOIN TBL_TARIFF t ON t.CURRENCY_ID = i.CURRENCY_ID
                                  AND  t.DATA_MONTH = @DATA_MONTH
WHERE   i.DATA_MONTH = @DATA_MONTH
        AND i.CUSTOMER_GROUP_ID IN (1, 2, 4, 5)
        AND i.CUSTOMER_CONNECTION_TYPE_ID IN (1, 9, 14, 15, 16)
        AND i.TOTAL_USAGE <= 10
        AND (@BILLING_CYCLE_ID = 0 OR   i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID)
        AND (@AREA_ID = 0 OR i.AREA_ID = @AREA_ID)
GROUP BY t.SUBSIDY_TARIFF,
         i.PRICE,
         i.CURRENCY_ID,
         i.CURRENCY_NAME,
         i.CURRENCY_SIGN
UNION ALL
SELECT  DATA_MONTH        = @DATA_MONTH,
        ROW_NO            = N'2',
        TYPE_OF_SALE_ID   = 1,
        TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_11_50,
        TOTAL_CUSTOMER    = COUNT (*),
        TOTAL_SOLD        = SUM (i.TOTAL_USAGE),
        RATE              = i.PRICE,
        SUBSIDY_RATE      = t.SUBSIDY_TARIFF - i.PRICE,
        SUBSIDY_AMOUNT    = (t.SUBSIDY_TARIFF - i.PRICE) * SUM (i.TOTAL_USAGE),
        i.CURRENCY_ID,
        i.CURRENCY_NAME,
        i.CURRENCY_SIGN,
        IS_ACTIVE         = 1
FROM    TBL_REF_INVOICE      i
        LEFT JOIN TBL_TARIFF t ON t.CURRENCY_ID = i.CURRENCY_ID
                                  AND  t.DATA_MONTH = @DATA_MONTH
WHERE   i.DATA_MONTH = @DATA_MONTH
        AND (i.TOTAL_USAGE BETWEEN 11 AND 50)
        AND i.CUSTOMER_GROUP_ID IN (1, 2, 4, 5)
        AND i.CUSTOMER_CONNECTION_TYPE_ID IN (1, 9, 14, 15, 16)
        AND (@BILLING_CYCLE_ID = 0 OR   i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID)
        AND (@AREA_ID = 0 OR i.AREA_ID = @AREA_ID)
GROUP BY t.SUBSIDY_TARIFF,
         i.PRICE,
         i.CURRENCY_ID,
         i.CURRENCY_NAME,
         i.CURRENCY_SIGN
UNION ALL
SELECT  DATA_MONTH        = @DATA_MONTH,
        ROW_NO            = N'3',
        TYPE_OF_SALE_ID   = 1,
        TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_AGRI,
        TOTAL_CUSTOMER    = COUNT (*),
        TOTAL_SOLD        = SUM (i.TOTAL_USAGE),
        RATE              = i.PRICE,
        SUBSIDY_RATE      = ISNULL (t.SUBSIDY_TARIFF, 0) - i.PRICE,
        SUBSIDY_AMOUNT    = (ISNULL (t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM (i.TOTAL_USAGE),
        i.CURRENCY_ID,
        i.CURRENCY_NAME,
        i.CURRENCY_SIGN,
        IS_ACTIVE         = 1
FROM    TBL_REF_INVOICE      i
        LEFT JOIN TBL_TARIFF t ON t.CURRENCY_ID = i.CURRENCY_ID
                                  AND  t.DATA_MONTH = @DATA_MONTH
WHERE   i.DATA_MONTH = @DATA_MONTH
        AND i.CUSTOMER_GROUP_ID IN (1, 2, 4, 5)
        AND i.CUSTOMER_CONNECTION_TYPE_ID IN (1, 9, 14, 15, 16)
        AND i.IS_AGRICULTURE = 1
        AND i.SHIFT_ID = 2 -- NIGHT
        AND (@AREA_ID = 0 OR i.AREA_ID = @AREA_ID)
GROUP BY t.SUBSIDY_TARIFF,
         i.PRICE,
         i.CURRENCY_ID,
         i.CURRENCY_NAME,
         i.CURRENCY_SIGN;

-- BLANK RECORD
IF NOT EXISTS (SELECT   * FROM  TBL_REF_03B WHERE   DATA_MONTH = @DATA_MONTH AND ROW_NO = '1')
    INSERT INTO TBL_REF_03B(
                           DATA_MONTH, ROW_NO, TYPE_OF_SALE_ID, TYPE_OF_SALE_NAME, TOTAL_CUSTOMER, TOTAL_POWER_SOLD, PRICE, SUBSIDY_RATE, SUBSIDY_AMOUNT, CURRENCY_ID, CURRENCY_NAME, CURRENCY_SIGN, IS_ACTIVE
                           )
    SELECT  DATA_MONTH        = @DATA_MONTH,
            ROW_NO            = N'1',
            TYPE_OF_SALE_ID   = 1,
            TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_0_10,
            TOTAL_CUSTOMER    = 0,
            TOTAL_SOLD        = 0,
            RATE              = 0,
            SUBSIDY_RATE      = 0,
            SUBSIDY_AMOUNT    = 0,
            CURRENCY_ID       = 1,
            CURRENCY_NAME     = '',
            CURRENCY_SIGN     = '',
            IS_ACTIVE         = 1;
IF NOT EXISTS (SELECT   * FROM  TBL_REF_03B WHERE   DATA_MONTH = @DATA_MONTH AND ROW_NO = '2')
    INSERT INTO TBL_REF_03B(
                           DATA_MONTH, ROW_NO, TYPE_OF_SALE_ID, TYPE_OF_SALE_NAME, TOTAL_CUSTOMER, TOTAL_POWER_SOLD, PRICE, SUBSIDY_RATE, SUBSIDY_AMOUNT, CURRENCY_ID, CURRENCY_NAME, CURRENCY_SIGN, IS_ACTIVE
                           )
    SELECT  DATA_MONTH        = @DATA_MONTH,
            ROW_NO            = N'2',
            TYPE_OF_SALE_ID   = 1,
            TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_11_50,
            TOTAL_CUSTOMER    = 0,
            TOTAL_SOLD        = 0,
            RATE              = 0,
            SUBSIDY_RATE      = 0,
            SUBSIDY_AMOUNT    = 0,
            CURRENCY_ID       = 1,
            CURRENCY_NAME     = '',
            CURRENCY_SIGN     = '',
            IS_ACTIVE         = 1;
IF NOT EXISTS (SELECT   * FROM  TBL_REF_03B WHERE   DATA_MONTH = @DATA_MONTH AND ROW_NO = '3')
    INSERT INTO TBL_REF_03B(
                           DATA_MONTH, ROW_NO, TYPE_OF_SALE_ID, TYPE_OF_SALE_NAME, TOTAL_CUSTOMER, TOTAL_POWER_SOLD, PRICE, SUBSIDY_RATE, SUBSIDY_AMOUNT, CURRENCY_ID, CURRENCY_NAME, CURRENCY_SIGN, IS_ACTIVE
                           )
    SELECT  DATA_MONTH        = @DATA_MONTH,
            ROW_NO            = N'3',
            TYPE_OF_SALE_ID   = 1,
            TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_AGRI,
            TOTAL_CUSTOMER    = 0,
            TOTAL_SOLD        = 0,
            RATE              = 0,
            SUBSIDY_RATE      = 0,
            SUBSIDY_AMOUNT    = 0,
            CURRENCY_ID       = 1,
            CURRENCY_NAME     = '',
            CURRENCY_SIGN     = '',
            IS_ACTIVE         = 1;
-- END RUN_REF_03B_2017
GO
IF OBJECT_ID ('RUN_REF_03B_2016') IS NOT NULL DROP PROC RUN_REF_03B_2016;
GO
CREATE PROC [dbo].[RUN_REF_03B_2016]
    @DATA_MONTH       DATETIME = '2017-02-01',
    @BILLING_CYCLE_ID INT      = 0,
    @AREA_ID          INT      = 0
AS
-- CLEAR DATA
DELETE  FROM TBL_REF_03B WHERE  DATA_MONTH = @DATA_MONTH;
INSERT INTO TBL_REF_03B(
                       DATA_MONTH, ROW_NO, TYPE_OF_SALE_ID, TYPE_OF_SALE_NAME, TOTAL_CUSTOMER, TOTAL_POWER_SOLD, PRICE, SUBSIDY_RATE, SUBSIDY_AMOUNT, CURRENCY_ID, CURRENCY_NAME, CURRENCY_SIGN, IS_ACTIVE
                       )
SELECT  DATA_MONTH        = @DATA_MONTH,
        ROW_NO            = N'1',
        TYPE_OF_SALE_ID   = 1,
        TYPE_OF_SALE_NAME = N'លក់ឱ្យលំនៅដ្ឋានប្រើតិចជាង  10 kWh/ខែ ',
        TOTAL_CUSTOMER    = COUNT (*),
        TOTAL_SOLD        = SUM (i.TOTAL_USAGE),
        RATE              = i.PRICE,
        SUBSIDY_RATE      = ISNULL (t.SUBSIDY_TARIFF, 0) - i.PRICE,
        SUBSIDY_AMOUNT    = (ISNULL (t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM (i.TOTAL_USAGE),
        i.CURRENCY_ID,
        i.CURRENCY_NAME,
        i.CURRENCY_SIGN,
        IS_ACTIVE         = 1
FROM    TBL_REF_INVOICE      i
        LEFT JOIN TBL_TARIFF t ON t.CURRENCY_ID = i.CURRENCY_ID
                                  AND  t.DATA_MONTH = @DATA_MONTH
WHERE   i.DATA_MONTH = @DATA_MONTH
        AND i.CUSTOMER_GROUP_ID = 1
        AND i.TOTAL_USAGE <= 10
        AND (@BILLING_CYCLE_ID = 0 OR   i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID)
        AND (@AREA_ID = 0 OR i.AREA_ID = @AREA_ID)
GROUP BY t.SUBSIDY_TARIFF,
         i.PRICE,
         i.CURRENCY_ID,
         i.CURRENCY_NAME,
         i.CURRENCY_SIGN
UNION ALL
SELECT  DATA_MONTH        = @DATA_MONTH,
        ROW_NO            = N'2',
        TYPE_OF_SALE_ID   = 1,
        TYPE_OF_SALE_NAME = N'លក់ឱ្យការបូមទឹកធ្វើកសិកម្មម៉ោង09:00យប់ ដល់07:00ព្រឹក',
        TOTAL_CUSTOMER    = COUNT (*),
        TOTAL_SOLD        = SUM (i.TOTAL_USAGE),
        RATE              = i.PRICE,
        SUBSIDY_RATE      = ISNULL (t.SUBSIDY_TARIFF, 0) - i.PRICE,
        SUBSIDY_AMOUNT    = (ISNULL (t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM (i.TOTAL_USAGE),
        i.CURRENCY_ID,
        i.CURRENCY_NAME,
        i.CURRENCY_SIGN,
        IS_ACTIVE         = 1
FROM    TBL_REF_INVOICE      i
        LEFT JOIN TBL_TARIFF t ON t.CURRENCY_ID = i.CURRENCY_ID
                                  AND  t.DATA_MONTH = @DATA_MONTH
WHERE   i.DATA_MONTH = @DATA_MONTH
        AND i.CUSTOMER_GROUP_ID = 1
        AND i.IS_AGRICULTURE = 1
        AND i.SHIFT_ID = 2 -- NIGHT
        AND (@AREA_ID = 0 OR i.AREA_ID = @AREA_ID)
GROUP BY t.SUBSIDY_TARIFF,
         i.PRICE,
         i.CURRENCY_ID,
         i.CURRENCY_NAME,
         i.CURRENCY_SIGN;

-- BLANK RECORD
IF NOT EXISTS (SELECT   * FROM  TBL_REF_03B WHERE   DATA_MONTH = @DATA_MONTH AND ROW_NO = '1')
    INSERT INTO TBL_REF_03B(
                           DATA_MONTH, ROW_NO, TYPE_OF_SALE_ID, TYPE_OF_SALE_NAME, TOTAL_CUSTOMER, TOTAL_POWER_SOLD, PRICE, SUBSIDY_RATE, SUBSIDY_AMOUNT, CURRENCY_ID, CURRENCY_NAME, CURRENCY_SIGN, IS_ACTIVE
                           )
    SELECT  DATA_MONTH        = @DATA_MONTH,
            ROW_NO            = N'1',
            TYPE_OF_SALE_ID   = 1,
            TYPE_OF_SALE_NAME = N'លក់ឱ្យលំនៅដ្ឋានប្រើតិចជាង  10 kWh/ខែ',
            TOTAL_CUSTOMER    = 0,
            TOTAL_SOLD        = 0,
            RATE              = 0,
            SUBSIDY_RATE      = 0,
            SUBSIDY_AMOUNT    = 0,
            CURRENCY_ID       = 1,
            CURRENCY_NAME     = '',
            CURRENCY_SIGN     = '',
            IS_ACTIVE         = 1;
IF NOT EXISTS (SELECT   * FROM  TBL_REF_03B WHERE   DATA_MONTH = @DATA_MONTH AND ROW_NO = '2')
    INSERT INTO TBL_REF_03B(
                           DATA_MONTH, ROW_NO, TYPE_OF_SALE_ID, TYPE_OF_SALE_NAME, TOTAL_CUSTOMER, TOTAL_POWER_SOLD, PRICE, SUBSIDY_RATE, SUBSIDY_AMOUNT, CURRENCY_ID, CURRENCY_NAME, CURRENCY_SIGN, IS_ACTIVE
                           )
    SELECT  DATA_MONTH        = @DATA_MONTH,
            ROW_NO            = N'3',
            TYPE_OF_SALE_ID   = 1,
            TYPE_OF_SALE_NAME = N'លក់ឱ្យការបូមទឹកធ្វើកសិកម្មម៉ោង09:00យប់ ដល់07:00ព្រឹក',
            TOTAL_CUSTOMER    = 0,
            TOTAL_SOLD        = 0,
            RATE              = 0,
            SUBSIDY_RATE      = 0,
            SUBSIDY_AMOUNT    = 0,
            CURRENCY_ID       = 1,
            CURRENCY_NAME     = '',
            CURRENCY_SIGN     = '',
            IS_ACTIVE         = 1;

-- END RUN_REF_03B_2016
GO
IF OBJECT_ID ('REPORT_QUARTER_SOLD_CONSUMER') IS NOT NULL
    DROP PROC REPORT_QUARTER_SOLD_CONSUMER;
GO
CREATE PROC [dbo].[REPORT_QUARTER_SOLD_CONSUMER]
    @DATE DATETIME = '2021-01-01'
AS

-- 14-JUN-2021 BY Phuong Sovathvong
-- 1. CUSTOMER_CONNECTION_TYPE_ID IN(141,142,143,24) get PRICE from TBL_INVOICE_DETAIL
DECLARE @D1 DATETIME,
        @D2 DATETIME;
SET @DATE = DATEADD (D, 0, DATEDIFF (D, 0, @DATE));
SET @D1 = CONVERT (NVARCHAR (4), @DATE, 126) + '-01-01';
SET @D2 = DATEADD (S, -1, DATEADD (M, 3, @DATE));

-- BUILD 12 MONTHs TABLE
CREATE TABLE #MONTHS (M DATETIME);
DECLARE @M INT;
SET @M = 1;
WHILE @M <= 12 BEGIN
    INSERT INTO #MONTHS
    VALUES(CONVERT (NVARCHAR (4), @D1, 126) + '-' + CONVERT (NVARCHAR, @M) + '-1');
    SET @M = @M + 1;
END;
SELECT  id.INVOICE_ID,
        id.PRICE,
        RowNumer = ROW_NUMBER () OVER (PARTITION BY id.INVOICE_ID ORDER BY id.INVOICE_DETAIL_ID DESC)
INTO    #tmpPrice1
FROM    dbo.TBL_INVOICE_DETAIL id
WHERE   id.INVOICE_ITEM_ID IN (-1, 1);
IF(@DATE >= '2021-01-01')BEGIN
    SELECT  TYPE_ID = 1, TYPE_NAME = N'លំនៅដ្ឋាន'
    INTO    #TMP_TYPE1 UNION ALL
    SELECT  TYPE_ID = 2, TYPE_NAME = N'អាជីវកម្មធុនតូច LV សាធារណៈ'
    UNION ALL
    SELECT  TYPE_ID = 3, TYPE_NAME = N'រដ្ឋបាល LV សាធារណៈ'
    UNION ALL
    SELECT  TYPE_ID = 4, TYPE_NAME = N'សេវាកម្មផ្សេងៗ LV សាធារណៈ'
    UNION ALL
    SELECT  TYPE_ID = 5, TYPE_NAME = N'អាជីវករតូបលក់ដូរក្នុងផ្សារ'
    UNION ALL
    SELECT  TYPE_ID = 6, TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV សាធារណៈ'
    UNION ALL
    SELECT  TYPE_ID   = 7,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV សាធារណៈ'
    UNION ALL
    SELECT  TYPE_ID = 8, TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV សាធារណៈ'
    UNION ALL
    SELECT  TYPE_ID   = 9,
            TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV សាធារណៈ'
    UNION ALL
    SELECT  TYPE_ID   = 10,
            TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV សាធារណៈ'
    UNION ALL
    SELECT  TYPE_ID   = 11,
            TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV សាធារណៈ'
    UNION ALL
    SELECT  TYPE_ID   = 12,
            TYPE_NAME = N'សាលារៀន, មន្ទីរពេទ្យ, មណ្ឌលសុខភាព នៅតំបន់ជនបទ'
    UNION ALL
    SELECT  TYPE_ID = 13, TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ MV'
    UNION ALL
    SELECT  TYPE_ID   = 14,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 15,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID   = 16,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT  TYPE_ID   = 17,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 18,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID   = 19,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT  TYPE_ID   = 20,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 21,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID   = 22,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV'
    UNION ALL
    SELECT  TYPE_ID   = 23,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 24,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID = 25, TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ MV'
    UNION ALL
    SELECT  TYPE_ID   = 26,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 27,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID   = 28,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT  TYPE_ID   = 29,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 30,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID   = 31,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT  TYPE_ID   = 32,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 33,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID   = 34,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV'
    UNION ALL
    SELECT  TYPE_ID   = 35,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 36,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID = 37, TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ MV'
    UNION ALL
    SELECT  TYPE_ID   = 38,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 39,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID   = 40,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT  TYPE_ID   = 41,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 42,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID   = 43,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT  TYPE_ID   = 44,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 45,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID   = 46,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV'
    UNION ALL
    SELECT  TYPE_ID   = 47,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 48,
            TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID   = 49,
            TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ MV'
    UNION ALL
    SELECT  TYPE_ID   = 50,
            TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 51,
            TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID   = 52,
            TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT  TYPE_ID   = 53,
            TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 54,
            TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID   = 55,
            TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT  TYPE_ID   = 56,
            TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 57,
            TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID   = 58,
            TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV'
    UNION ALL
    SELECT  TYPE_ID   = 59,
            TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 60,
            TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID = 61, TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ MV'
    UNION ALL
    SELECT  TYPE_ID   = 62,
            TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 63,
            TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID   = 64,
            TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT  TYPE_ID   = 65,
            TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្មទ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 66,
            TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID   = 67,
            TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT  TYPE_ID   = 68,
            TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 69,
            TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID   = 70,
            TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV'
    UNION ALL
    SELECT  TYPE_ID   = 71,
            TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 72,
            TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID = 73, TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ MV'
    UNION ALL
    SELECT  TYPE_ID   = 74,
            TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 75,
            TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID   = 76,
            TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT  TYPE_ID   = 77,
            TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 78,
            TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID   = 79,
            TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT  TYPE_ID   = 80,
            TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 81,
            TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID   = 82,
            TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV'
    UNION ALL
    SELECT  TYPE_ID   = 83,
            TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 84,
            TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID = 85, TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ MV'
    UNION ALL
    SELECT  TYPE_ID = 86, TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID = 87, TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID   = 88,
            TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT  TYPE_ID   = 89,
            TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 90,
            TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID   = 91,
            TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT  TYPE_ID   = 92,
            TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 93,
            TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID   = 94,
            TYPE_NAME = N'ពាណិជ្ជកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV'
    UNION ALL
    SELECT  TYPE_ID   = 95,
            TYPE_NAME = N'ពាណិជ្ជកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 96,
            TYPE_NAME = N'ពាណិជ្ជកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID = 97, TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ MV'
    UNION ALL
    SELECT  TYPE_ID = 98, TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID = 99, TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID   = 101,
            TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT  TYPE_ID   = 102,
            TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 103,
            TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID   = 104,
            TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT  TYPE_ID   = 105,
            TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 106,
            TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID   = 107,
            TYPE_NAME = N'រដ្ឋបាលសាធារណៈ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV'
    UNION ALL
    SELECT  TYPE_ID   = 108,
            TYPE_NAME = N'រដ្ឋបាលសាធារណៈ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 109,
            TYPE_NAME = N'រដ្ឋបាលសាធារណៈ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID = 110, TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ MV'
    UNION ALL
    SELECT  TYPE_ID   = 111,
            TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 112,
            TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID   = 113,
            TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT  TYPE_ID   = 114,
            TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 115,
            TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID   = 116,
            TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT  TYPE_ID   = 117,
            TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 118,
            TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID   = 119,
            TYPE_NAME = N'សេវាកម្មផ្សេងៗ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV'
    UNION ALL
    SELECT  TYPE_ID   = 120,
            TYPE_NAME = N'សេវាកម្មផ្សេងៗ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 121,
            TYPE_NAME = N'សេវាកម្មផ្សេងៗ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID   = 122,
            TYPE_NAME = N'បូមទឹកកសិកម្មពេលយប់ ទិញ MV (ម៉ោង៩យប់ដល់៧ព្រឹក)'
    UNION ALL
    SELECT  TYPE_ID   = 123,
            TYPE_NAME = N'បូមទឹកកសិកម្មពេលយប់ ទិញ LV (ម៉ោង៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូរអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 124,
            TYPE_NAME = N'បូមទឹកកសិកម្មពេលយប់ ទិញ LV (ម៉ោង៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូរអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID = 125, TYPE_NAME = N'បូមទឹកកសិកម្មពេលយប់ ទិញ LV សាធារណៈ'
    UNION ALL
    SELECT  TYPE_ID   = 126,
            TYPE_NAME = N'បូមទឹកកសិកម្មពេលយប់ ទិញ MV តាមអត្រាថ្លៃមធ្យម'
    UNION ALL
    SELECT  TYPE_ID   = 127,
            TYPE_NAME = N'បូមទឹកកសិកម្មពេលយប់ ទិញ LV ត្រង់ស្វូរអ្នកលក់ តាមអត្រាថ្លៃមធ្យម'
    UNION ALL
    SELECT  TYPE_ID   = 128,
            TYPE_NAME = N'បូមទឹកកសិកម្មពេលយប់ ទិញ LV ត្រង់ស្វូរអ្នកទិញ តាមអត្រាថ្លៃមធ្យម'
    UNION ALL
    SELECT  TYPE_ID   = 129,
            TYPE_NAME = N'បូមទឹកកសិកម្មពេលយប់ ទិញ LV សាធារណៈ តាមអត្រាថ្លៃមធ្យម';
    -- BUILD DATA VALUE TABLE
    SELECT  i.INVOICE_MONTH,
            i.CURRENCY_ID,
            cc.CURRENCY_SING,
            CUSTOMER_CONNECTION_TYPE_ID = CASE WHEN i.CUSTOMER_CONNECTION_TYPE_ID = 1 THEN 1
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (22) THEN 2
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (23) THEN 3
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (134) THEN 4
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (144) THEN 5
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (135) THEN 6
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (136) THEN 7
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (137) THEN 8
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (138) THEN 9
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (139) THEN 10
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (140) THEN 11
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (9) THEN 12
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (26) THEN 13
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (27) THEN 14
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (28) THEN 15
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (29) THEN 16
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (30) THEN 17
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (31) THEN 18
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (32) THEN 19
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (33) THEN 20
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (34) THEN 21
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (35) THEN 22
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (36) THEN 23
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (37) THEN 24
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (38) THEN 25
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (39) THEN 26
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (40) THEN 27
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (41) THEN 28
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (42) THEN 29
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (43) THEN 30
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (44) THEN 31
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (45) THEN 32
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (46) THEN 33
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (47) THEN 34
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (48) THEN 35
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (49) THEN 36
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (50) THEN 37
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (51) THEN 38
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (52) THEN 39
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (53) THEN 40
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (54) THEN 41
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (55) THEN 42
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (56) THEN 43
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (57) THEN 44
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (58) THEN 45
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (59) THEN 46
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (60) THEN 47
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (61) THEN 48
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (62) THEN 49
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (63) THEN 50
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (64) THEN 51
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (65) THEN 52
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (66) THEN 53
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (67) THEN 54
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (68) THEN 55
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (69) THEN 56
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (70) THEN 57
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (71) THEN 58
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (72) THEN 59
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (73) THEN 60
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (74) THEN 61
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (75) THEN 62
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (76) THEN 63
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (77) THEN 64
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (78) THEN 65
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (79) THEN 66
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (80) THEN 67
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (81) THEN 68
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (82) THEN 69
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (83) THEN 70
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (84) THEN 71
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (85) THEN 72
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (86) THEN 73
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (87) THEN 74
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (88) THEN 75
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (89) THEN 76
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (90) THEN 77
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (91) THEN 78
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (92) THEN 79
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (93) THEN 80
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (94) THEN 81
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (95) THEN 82
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (96) THEN 83
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (97) THEN 84
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (98) THEN 85
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (99) THEN 86
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (100) THEN 87
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (101) THEN 88
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (102) THEN 89
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (103) THEN 90
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (104) THEN 91
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (105) THEN 92
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (106) THEN 93
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (107) THEN 94
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (108) THEN 95
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (109) THEN 96
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (110) THEN 97
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (111) THEN 98
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (112) THEN 99
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (113) THEN 101
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (114) THEN 102
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (115) THEN 103
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (116) THEN 104
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (117) THEN 105
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (118) THEN 106
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (119) THEN 107
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (120) THEN 108
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (121) THEN 109
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (122) THEN 110
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (123) THEN 111
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (124) THEN 112
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (125) THEN 113
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (126) THEN 114
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (127) THEN 115
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (128) THEN 116
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (129) THEN 117
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (130) THEN 118
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (131) THEN 119
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (132) THEN 120
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (133) THEN 121
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (141)
                                               AND  d.PRICE = 480 THEN 122
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (142)
                                               AND  d.PRICE = 480 THEN 123
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (143)
                                               AND  d.PRICE = 480 THEN 124
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (24)
                                               AND  d.PRICE = 480 THEN 125
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (141)
                                               AND  d.PRICE = 0.1370 THEN 126
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (142)
                                               AND  d.PRICE = 0.15048 THEN 127
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (143)
                                               AND  d.PRICE = 0.14248 THEN 128
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (24)
                                               AND  d.PRICE = 730 THEN 129 ELSE i.CUSTOMER_CONNECTION_TYPE_ID END,
            TOTAL_USAGE                 = i.TOTAL_USAGE + ISNULL (adj.ADJUST_USAGE, 0),
            d.PRICE,
            i.TOTAL_AMOUNT
    INTO    #TMP1
    FROM    TBL_INVOICE              i
            INNER JOIN TBL_CUSTOMER  c ON c.CUSTOMER_ID = i.CUSTOMER_ID
            INNER JOIN TLKP_CURRENCY cc ON cc.CURRENCY_ID = i.CURRENCY_ID
            INNER JOIN #tmpPrice1    d ON d.INVOICE_ID = i.INVOICE_ID
                                          AND d.RowNumer = 1
            OUTER APPLY(
                       SELECT   ADJUST_USAGE = SUM (ADJUST_USAGE)
                       FROM TBL_INVOICE_ADJUSTMENT
                       WHERE   INVOICE_ID = i.INVOICE_ID
                       )             adj
    WHERE   i.INVOICE_MONTH BETWEEN @D1 AND @D2
            --AND c.CUSTOMER_CONNECTION_TYPE_ID <> -1 -- USE IN PRODUCTION;
            AND i.IS_SERVICE_BILL = 0
            AND c.CUSTOMER_ID NOT IN(SELECT CUSTOMER_ID FROM    TBL_LICENSEE_CONNECTION WHERE  IS_ACTIVE = 1)
            AND c.IS_REACTIVE = 0
            AND i.INVOICE_STATUS NOT IN (3)
            AND i.CUSTOMER_CONNECTION_TYPE_ID NOT IN (11,12,13)

    UNION ALL
    SELECT  CONVERT (NVARCHAR (7), CREATE_ON, 126) + '-01',
            b.CURRENCY_ID,
            cc.CURRENCY_SING,
            CUSTOMER_CONNECTION_TYPE_ID = CASE WHEN CUSTOMER_CONNECTION_TYPE_ID = 1 THEN 1
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (22) THEN 2
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (23) THEN 3
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (134) THEN 4
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (144) THEN 5
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (135) THEN 6
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (136) THEN 7
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (137) THEN 8
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (138) THEN 9
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (139) THEN 10
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (140) THEN 11
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (9) THEN 12
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (26) THEN 13
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (27) THEN 14
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (28) THEN 15
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (29) THEN 16
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (30) THEN 17
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (31) THEN 18
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (32) THEN 19
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (33) THEN 20
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (34) THEN 21
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (35) THEN 22
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (36) THEN 23
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (37) THEN 24
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (38) THEN 25
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (39) THEN 26
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (40) THEN 27
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (41) THEN 28
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (42) THEN 29
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (43) THEN 30
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (44) THEN 31
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (45) THEN 32
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (46) THEN 33
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (47) THEN 34
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (48) THEN 35
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (49) THEN 36
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (50) THEN 37
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (51) THEN 38
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (52) THEN 39
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (53) THEN 40
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (54) THEN 41
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (55) THEN 42
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (56) THEN 43
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (57) THEN 44
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (58) THEN 45
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (59) THEN 46
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (60) THEN 47
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (61) THEN 48
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (62) THEN 49
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (63) THEN 50
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (64) THEN 51
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (65) THEN 52
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (66) THEN 53
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (67) THEN 54
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (68) THEN 55
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (69) THEN 56
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (70) THEN 57
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (71) THEN 58
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (72) THEN 59
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (73) THEN 60
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (74) THEN 61
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (75) THEN 62
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (76) THEN 63
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (77) THEN 64
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (78) THEN 65
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (79) THEN 66
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (80) THEN 67
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (81) THEN 68
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (82) THEN 69
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (83) THEN 70
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (84) THEN 71
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (85) THEN 72
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (86) THEN 73
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (87) THEN 74
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (88) THEN 75
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (89) THEN 76
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (90) THEN 77
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (91) THEN 78
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (92) THEN 79
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (93) THEN 80
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (94) THEN 81
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (95) THEN 82
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (96) THEN 83
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (97) THEN 84
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (98) THEN 85
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (99) THEN 86
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (100) THEN 87
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (101) THEN 88
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (102) THEN 89
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (103) THEN 90
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (104) THEN 91
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (105) THEN 92
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (106) THEN 93
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (107) THEN 94
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (108) THEN 95
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (109) THEN 96
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (110) THEN 97
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (111) THEN 98
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (112) THEN 99
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (113) THEN 101
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (114) THEN 102
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (115) THEN 103
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (116) THEN 104
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (117) THEN 105
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (118) THEN 106
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (119) THEN 107
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (120) THEN 108
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (121) THEN 109
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (122) THEN 110
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (123) THEN 111
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (124) THEN 112
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (125) THEN 113
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (126) THEN 114
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (127) THEN 115
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (128) THEN 116
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (129) THEN 117
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (130) THEN 118
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (131) THEN 119
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (132) THEN 120
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (133) THEN 121
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (141)
                                               AND  PRICE = 480 THEN 122
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (142)
                                               AND  PRICE = 480 THEN 123
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (143)
                                               AND  PRICE = 480 THEN 124
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (24)
                                               AND  PRICE = 480 THEN 125
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (141)
                                               AND  PRICE = 0.1370 THEN 126
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (142)
                                               AND  PRICE = 0.15048 THEN 127
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (143)
                                               AND  PRICE = 0.14248 THEN 128
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (24)
                                               AND  PRICE = 730 THEN 129 ELSE CUSTOMER_CONNECTION_TYPE_ID END,
            b.BUY_QTY,
            b.PRICE,
            b.BUY_AMOUNT
    FROM    TBL_CUSTOMER_BUY                b
            INNER JOIN TBL_PREPAID_CUSTOMER p ON p.PREPAID_CUS_ID = b.PREPAID_CUS_ID
            INNER JOIN TBL_CUSTOMER         c ON c.CUSTOMER_ID = p.CUSTOMER_ID
            INNER JOIN TLKP_CURRENCY        cc ON cc.CURRENCY_ID = b.CURRENCY_ID
    WHERE   b.CREATE_ON BETWEEN @D1 AND @D2
            AND c.CUSTOMER_ID NOT IN(SELECT CUSTOMER_ID FROM    TBL_LICENSEE_CONNECTION WHERE  IS_ACTIVE = 1)
            AND c.IS_REACTIVE = 0
            AND CONVERT (NVARCHAR (7), CREATE_ON, 126) + '-01' <= '2016-02-01'
    UNION ALL
    SELECT  cr.CREDIT_MONTH,
            cr.CURRENCY_ID,
            cc.CURRENCY_SING,
            CUSTOMER_CONNECTION_TYPE_ID = CASE WHEN cr.CUSTOMER_CONNECTION_TYPE_ID = 1 THEN 1
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (22) THEN 2
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (23) THEN 3
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (134) THEN 4
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (144) THEN 5
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (135) THEN 6
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (136) THEN 7
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (137) THEN 8
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (138) THEN 9
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (139) THEN 10
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (140) THEN 11
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (9) THEN 12
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (26) THEN 13
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (27) THEN 14
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (28) THEN 15
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (29) THEN 16
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (30) THEN 17
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (31) THEN 18
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (32) THEN 19
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (33) THEN 20
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (34) THEN 21
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (35) THEN 22
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (36) THEN 23
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (37) THEN 24
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (38) THEN 25
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (39) THEN 26
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (40) THEN 27
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (41) THEN 28
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (42) THEN 29
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (43) THEN 30
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (44) THEN 31
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (45) THEN 32
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (46) THEN 33
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (47) THEN 34
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (48) THEN 35
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (49) THEN 36
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (50) THEN 37
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (51) THEN 38
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (52) THEN 39
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (53) THEN 40
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (54) THEN 41
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (55) THEN 42
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (56) THEN 43
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (57) THEN 44
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (58) THEN 45
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (59) THEN 46
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (60) THEN 47
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (61) THEN 48
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (62) THEN 49
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (63) THEN 50
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (64) THEN 51
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (65) THEN 52
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (66) THEN 53
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (67) THEN 54
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (68) THEN 55
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (69) THEN 56
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (70) THEN 57
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (71) THEN 58
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (72) THEN 59
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (73) THEN 60
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (74) THEN 61
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (75) THEN 62
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (76) THEN 63
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (77) THEN 64
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (78) THEN 65
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (79) THEN 66
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (80) THEN 67
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (81) THEN 68
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (82) THEN 69
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (83) THEN 70
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (84) THEN 71
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (85) THEN 72
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (86) THEN 73
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (87) THEN 74
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (88) THEN 75
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (89) THEN 76
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (90) THEN 77
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (91) THEN 78
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (92) THEN 79
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (93) THEN 80
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (94) THEN 81
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (95) THEN 82
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (96) THEN 83
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (97) THEN 84
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (98) THEN 85
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (99) THEN 86
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (100) THEN 87
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (101) THEN 88
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (102) THEN 89
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (103) THEN 90
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (104) THEN 91
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (105) THEN 92
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (106) THEN 93
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (107) THEN 94
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (108) THEN 95
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (109) THEN 96
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (110) THEN 97
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (111) THEN 98
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (112) THEN 99
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (113) THEN 101
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (114) THEN 102
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (115) THEN 103
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (116) THEN 104
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (117) THEN 105
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (118) THEN 106
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (119) THEN 107
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (120) THEN 108
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (121) THEN 109
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (122) THEN 110
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (123) THEN 111
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (124) THEN 112
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (125) THEN 113
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (126) THEN 114
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (127) THEN 115
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (128) THEN 116
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (129) THEN 117
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (130) THEN 118
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (131) THEN 119
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (132) THEN 120
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (133) THEN 121
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (141)
                                               AND  PRICE = 480 THEN 122
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (142)
                                               AND  PRICE = 480 THEN 123
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (143)
                                               AND  PRICE = 480 THEN 124
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (24)
                                               AND  PRICE = 480 THEN 125
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (141)
                                               AND  PRICE = 0.1370 THEN 126
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (142)
                                               AND  PRICE = 0.15048 THEN 127
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (143)
                                               AND  PRICE = 0.14248 THEN 128
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (24)
                                               AND  PRICE = 730 THEN 129 ELSE cr.CUSTOMER_CONNECTION_TYPE_ID END,
            cr.USAGE,
            PRICE                       = 800,
            TOTAL_AMOUNT                = cr.USAGE * 800
    FROM    TBL_PREPAID_CREDIT       cr
            INNER JOIN TBL_CUSTOMER  c ON c.CUSTOMER_ID = cr.CUSTOMER_ID
            INNER JOIN TLKP_CURRENCY cc ON cc.CURRENCY_ID = cr.CURRENCY_ID
    WHERE   cr.CREDIT_MONTH BETWEEN @D1 AND @D2
            AND (cr.CREDIT_MONTH BETWEEN '2016-03-01' AND '2017-02-01')
    UNION ALL
    SELECT  cr.CREDIT_MONTH,
            cr.CURRENCY_ID,
            cc.CURRENCY_SING,
            CUSTOMER_CONNECTION_TYPE_ID = CASE WHEN cr.CUSTOMER_CONNECTION_TYPE_ID = 1 THEN 1
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (22) THEN 2
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (23) THEN 3
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (134) THEN 4
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (144) THEN 5
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (135) THEN 6
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (136) THEN 7
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (137) THEN 8
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (138) THEN 9
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (139) THEN 10
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (140) THEN 11
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (9) THEN 12
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (26) THEN 13
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (27) THEN 14
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (28) THEN 15
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (29) THEN 16
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (30) THEN 17
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (31) THEN 18
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (32) THEN 19
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (33) THEN 20
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (34) THEN 21
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (35) THEN 22
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (36) THEN 23
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (37) THEN 24
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (38) THEN 25
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (39) THEN 26
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (40) THEN 27
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (41) THEN 28
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (42) THEN 29
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (43) THEN 30
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (44) THEN 31
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (45) THEN 32
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (46) THEN 33
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (47) THEN 34
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (48) THEN 35
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (49) THEN 36
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (50) THEN 37
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (51) THEN 38
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (52) THEN 39
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (53) THEN 40
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (54) THEN 41
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (55) THEN 42
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (56) THEN 43
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (57) THEN 44
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (58) THEN 45
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (59) THEN 46
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (60) THEN 47
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (61) THEN 48
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (62) THEN 49
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (63) THEN 50
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (64) THEN 51
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (65) THEN 52
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (66) THEN 53
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (67) THEN 54
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (68) THEN 55
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (69) THEN 56
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (70) THEN 57
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (71) THEN 58
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (72) THEN 59
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (73) THEN 60
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (74) THEN 61
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (75) THEN 62
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (76) THEN 63
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (77) THEN 64
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (78) THEN 65
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (79) THEN 66
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (80) THEN 67
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (81) THEN 68
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (82) THEN 69
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (83) THEN 70
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (84) THEN 71
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (85) THEN 72
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (86) THEN 73
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (87) THEN 74
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (88) THEN 75
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (89) THEN 76
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (90) THEN 77
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (91) THEN 78
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (92) THEN 79
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (93) THEN 80
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (94) THEN 81
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (95) THEN 82
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (96) THEN 83
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (97) THEN 84
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (98) THEN 85
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (99) THEN 86
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (100) THEN 87
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (101) THEN 88
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (102) THEN 89
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (103) THEN 90
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (104) THEN 91
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (105) THEN 92
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (106) THEN 93
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (107) THEN 94
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (108) THEN 95
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (109) THEN 96
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (110) THEN 97
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (111) THEN 98
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (112) THEN 99
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (113) THEN 101
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (114) THEN 102
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (115) THEN 103
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (116) THEN 104
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (117) THEN 105
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (118) THEN 106
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (119) THEN 107
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (120) THEN 108
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (121) THEN 109
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (122) THEN 110
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (123) THEN 111
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (124) THEN 112
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (125) THEN 113
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (126) THEN 114
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (127) THEN 115
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (128) THEN 116
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (129) THEN 117
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (130) THEN 118
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (131) THEN 119
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (132) THEN 120
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (133) THEN 121
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (141)
                                               AND  PRICE = 480 THEN 122
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (142)
                                               AND  PRICE = 480 THEN 123
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (143)
                                               AND  PRICE = 480 THEN 124
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (24)
                                               AND  PRICE = 480 THEN 125
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (141)
                                               AND  PRICE = 0.1370 THEN 126
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (142)
                                               AND  PRICE = 0.15048 THEN 127
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (143)
                                               AND  PRICE = 0.14248 THEN 128
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (24)
                                               AND  PRICE = 730 THEN 129 ELSE cr.CUSTOMER_CONNECTION_TYPE_ID END,
            cr.USAGE,
            PRICE                       = cr.BASED_PRICE,
            TOTAL_AMOUNT                = cr.USAGE * BASED_PRICE
    FROM    TBL_PREPAID_CREDIT       cr
            INNER JOIN TBL_CUSTOMER  c ON c.CUSTOMER_ID = cr.CUSTOMER_ID
            INNER JOIN TLKP_CURRENCY cc ON cc.CURRENCY_ID = cr.CURRENCY_ID
    WHERE   cr.CREDIT_MONTH BETWEEN @D1 AND @D2;

    -- BUILD CROSS TAB DEFINITION
    SELECT  CUSTOMER_CONNECTION_TYPE_ID,
            COL = ROW_NUMBER () OVER (ORDER BY CUSTOMER_CONNECTION_TYPE_ID) % 2,
            ROW = (1 + ROW_NUMBER () OVER (ORDER BY CUSTOMER_CONNECTION_TYPE_ID)) / 2
    INTO    #DEF1
    FROM(SELECT DISTINCT CUSTOMER_CONNECTION_TYPE_ID FROM   #TMP1) x;

    -- TO FILL 12MONTHs DATA 
    INSERT INTO #TMP1
    SELECT  m.M,
            t.CURRENCY_ID,
            t.CURRENCY_SING,
            t.CUSTOMER_CONNECTION_TYPE_ID,
            TOTAL_USAGE  = 0,
            PRICE        = 0,
            TOTAL_AMOUNT = 0
    FROM    #MONTHS m,
    (
    SELECT  DISTINCT CUSTOMER_CONNECTION_TYPE_ID,
                     CURRENCY_ID,
                     CURRENCY_SING
    FROM    #TMP1
    ) t;

    -- IN CASE NO RECORD
    IF(SELECT   COUNT (*) FROM  #TMP1) = 0 BEGIN
        INSERT INTO #TMP1 SELECT    M, 0, 0, 0, 0, 0, 0 FROM    #MONTHS;
        INSERT INTO #DEF1 SELECT    0, 1, 1;
    END;
    -- RENDER RESULT
    SELECT  ROW,
            INVOICE_MONTH,
            TYPE_ID1     = MAX (CASE WHEN COL = 1 THEN t.CUSTOMER_CONNECTION_TYPE_ID ELSE 0 END),
            [TYPE_NAME]  = CONVERT (NTEXT, MAX (CASE WHEN COL = 1 THEN TYPE_NAME ELSE '' END)),
            USAGE1       = NULLIF(SUM (CASE WHEN COL = 1 THEN t.TOTAL_USAGE ELSE 0 END), 0),
            PRICE1       = NULLIF(MAX (CASE WHEN COL = 1 THEN t.PRICE ELSE 0 END), 0),
            CURRENCY_ID1 = MAX (CASE WHEN COL = 1 THEN t.CURRENCY_ID ELSE 0 END),
            CURR_SIGN1   = MAX (CASE WHEN COL = 1 THEN t.CURRENCY_SING ELSE '' END),
            AMOUNT1      = NULLIF(SUM (CASE WHEN COL = 1 THEN t.TOTAL_AMOUNT ELSE 0 END), 0),
            TYPE_ID2     = MAX (CASE WHEN COL = 0 THEN t.CUSTOMER_CONNECTION_TYPE_ID ELSE 0 END),
            TYPE_NAME2   = MAX (CASE WHEN COL = 0 THEN TYPE_NAME ELSE '' END),
            USAGE2       = NULLIF(SUM (CASE WHEN COL = 0 THEN t.TOTAL_USAGE ELSE 0 END), 0),
            PRICE2       = NULLIF(MAX (CASE WHEN COL = 0 THEN t.PRICE ELSE 0 END), 0),
            CURRENCY_ID2 = MAX (CASE WHEN COL = 0 THEN t.CURRENCY_ID ELSE 0 END),
            CURR_SIGN2   = MAX (CASE WHEN COL = 0 THEN t.CURRENCY_SING ELSE '' END),
            AMOUNT2      = NULLIF(SUM (CASE WHEN COL = 0 THEN t.TOTAL_AMOUNT ELSE 0 END), 0)
    FROM    #TMP1                t
            LEFT JOIN #DEF1      d ON d.CUSTOMER_CONNECTION_TYPE_ID = t.CUSTOMER_CONNECTION_TYPE_ID
            LEFT JOIN #TMP_TYPE1 c ON c.TYPE_ID = t.CUSTOMER_CONNECTION_TYPE_ID
    GROUP BY ROW,
             INVOICE_MONTH
    ORDER BY ROW,
             INVOICE_MONTH;
--END REPORT_QUARTER_SOLD_CONSUMER
END;
ELSE BEGIN
    SELECT  TYPE_ID = 1, TYPE_NAME = N'លំនៅដ្ឋាន'
    INTO    #TMP_TYPE UNION ALL
    SELECT  TYPE_ID = 2, TYPE_NAME = N'ឧស្សាហកម្ម MV'
    UNION ALL
    SELECT  TYPE_ID = 3, TYPE_NAME = N'ឧស្សាហកម្ម MV តាមពេលវេលា'
    UNION ALL
    SELECT  TYPE_ID = 4, TYPE_NAME = N'ឧស្សាហកម្ម MV ពន្លឺព្រះអាទិត្យ'
    UNION ALL
    SELECT  TYPE_ID = 5, TYPE_NAME = N'ឧស្សាហកម្ម LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID   = 6,
            TYPE_NAME = N'ឧស្សាហកម្ម LV ត្រង់ស្វូអ្នកលក់តាមពេលវេលា'
    UNION ALL
    SELECT  TYPE_ID   = 7,
            TYPE_NAME = N'ឧស្សាហកម្ម LV ត្រង់ស្វូអ្នកលក់ពន្លឺព្រះអាទិត្យ'
    UNION ALL
    SELECT  TYPE_ID = 8, TYPE_NAME = N'ឧស្សាហកម្ម LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID = 9, TYPE_NAME = N'ឧស្សាហកម្ម LV ត្រង់ស្វូអ្នកទិញតាមពេល'
    UNION ALL
    SELECT  TYPE_ID   = 10,
            TYPE_NAME = N'ឧស្សាហកម្ម LV ត្រង់ស្វូអ្នកទិញពន្លឺព្រះអាទិត្យ'
    UNION ALL
    SELECT  TYPE_ID = 11, TYPE_NAME = N'ឧស្សាហកម្ម LV សាធារណៈ'
    UNION ALL
    SELECT  TYPE_ID = 12, TYPE_NAME = N'ពាណិជ្ជកម្ម MV'
    UNION ALL
    SELECT  TYPE_ID = 13, TYPE_NAME = N'ពាណិជ្ជកម្ម MV តាមពេលវេលា'
    UNION ALL
    SELECT  TYPE_ID = 14, TYPE_NAME = N'ពាណិជ្ជកម្ម MV ពន្លឺព្រះអាទិត្យ'
    UNION ALL
    SELECT  TYPE_ID = 15, TYPE_NAME = N'ពាណិជ្ជកម្ម LV​ ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT  TYPE_ID = 16, TYPE_NAME = N'ពាណិជ្ជកម្ម LV ត្រង់ស្វូអ្នកលក់​តាមពេល'
    UNION ALL
    SELECT  TYPE_ID   = 17,
            TYPE_NAME = N'ពាណិជ្ជកម្ម LV ត្រង់ស្វូអ្នកលក់ពន្លឺព្រះអាទិត្យ'
    UNION ALL
    SELECT  TYPE_ID = 18, TYPE_NAME = N'ពាណិជ្ជកម្ម LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT  TYPE_ID   = 19,
            TYPE_NAME = N'ពាណិជ្ជកម្ម LV​ ត្រង់ស្វូអ្នកទិញតាមពេលវេលា'
    UNION ALL
    SELECT  TYPE_ID   = 20,
            TYPE_NAME = N'ពាណិជ្ជកម្ម LV ត្រង់ស្វូអ្នកទិញពន្លឺព្រះអាទិត្យ'
    UNION ALL
    SELECT  TYPE_ID = 21, TYPE_NAME = N'ពាណិជ្ជកម្ម LV សាធារណៈ'
    UNION ALL
    SELECT  TYPE_ID = 22, TYPE_NAME = N'អង្គភាពរដ្ឋ LV សាធារណៈ'
    UNION ALL
    SELECT  TYPE_ID = 23, TYPE_NAME = N'សាលារៀន, មន្ទីរពេទ្យ';

    -- BUILD DATA VALUE TABLE
    SELECT  i.INVOICE_MONTH,
            i.CURRENCY_ID,
            cc.CURRENCY_SING,
            CUSTOMER_CONNECTION_TYPE_ID = CASE WHEN i.CUSTOMER_CONNECTION_TYPE_ID = 1 THEN 1
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (4) THEN 2
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (7) THEN 3
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (21) THEN 4
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (2) THEN 5
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (18) THEN 6
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (0) THEN 7
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (3) THEN 8
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (17) THEN 9
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (0) THEN 10
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (14) THEN 11
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (8) THEN 12
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (10) THEN 13
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (0) THEN 14
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (5) THEN 15
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (20) THEN 16
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (0) THEN 17
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (6) THEN 18
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (19) THEN 19
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (0) THEN 20
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (15) THEN 21
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (16) THEN 22
                                          WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN (9) THEN 23 ELSE i.CUSTOMER_CONNECTION_TYPE_ID END,
            TOTAL_USAGE                 = i.TOTAL_USAGE + ISNULL (adj.ADJUST_USAGE, 0),
            d.PRICE,
            i.TOTAL_AMOUNT
    INTO    #TMP
    FROM    TBL_INVOICE              i
            INNER JOIN TBL_CUSTOMER  c ON c.CUSTOMER_ID = i.CUSTOMER_ID
            INNER JOIN TLKP_CURRENCY cc ON cc.CURRENCY_ID = i.CURRENCY_ID
            OUTER APPLY(
                       SELECT   TOP 1   PRICE
                       FROM TBL_INVOICE_DETAIL
                       WHERE   INVOICE_ID = i.INVOICE_ID
                       ORDER BY INVOICE_DETAIL_ID DESC
                       )             d
            OUTER APPLY(
                       SELECT   ADJUST_USAGE = SUM (ADJUST_USAGE)
                       FROM TBL_INVOICE_ADJUSTMENT
                       WHERE   INVOICE_ID = i.INVOICE_ID
                       ) adj
    WHERE   i.INVOICE_MONTH BETWEEN @D1 AND @D2
            --AND c.CUSTOMER_CONNECTION_TYPE_ID <> -1 -- USE IN PRODUCTION;
            AND i.IS_SERVICE_BILL = 0
            AND c.CUSTOMER_ID NOT IN(SELECT CUSTOMER_ID FROM    TBL_LICENSEE_CONNECTION WHERE  IS_ACTIVE = 1)
            AND c.IS_REACTIVE = 0
            AND i.INVOICE_STATUS NOT IN (3)
    UNION ALL
    SELECT  CONVERT (NVARCHAR (7), CREATE_ON, 126) + '-01',
            b.CURRENCY_ID,
            cc.CURRENCY_SING,
            CUSTOMER_CONNECTION_TYPE_ID = CASE WHEN CUSTOMER_CONNECTION_TYPE_ID = 1 THEN 1
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (4) THEN 2
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (7) THEN 3
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (21) THEN 4
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (2) THEN 5
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (18) THEN 6
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (0) THEN 7
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (3) THEN 8
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (17) THEN 9
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (0) THEN 10
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (14) THEN 11
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (8) THEN 12
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (10) THEN 13
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (0) THEN 14
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (5) THEN 15
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (20) THEN 16
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (0) THEN 17
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (6) THEN 18
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (19) THEN 19
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (0) THEN 20
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (15) THEN 21
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (16) THEN 22
                                          WHEN CUSTOMER_CONNECTION_TYPE_ID IN (9) THEN 23 ELSE CUSTOMER_CONNECTION_TYPE_ID END,
            b.BUY_QTY,
            b.PRICE,
            b.BUY_AMOUNT
    FROM    TBL_CUSTOMER_BUY                b
            INNER JOIN TBL_PREPAID_CUSTOMER p ON p.PREPAID_CUS_ID = b.PREPAID_CUS_ID
            INNER JOIN TBL_CUSTOMER         c ON c.CUSTOMER_ID = p.CUSTOMER_ID
            INNER JOIN TLKP_CURRENCY        cc ON cc.CURRENCY_ID = b.CURRENCY_ID
    WHERE   b.CREATE_ON BETWEEN @D1 AND @D2
            AND c.CUSTOMER_ID NOT IN(SELECT CUSTOMER_ID FROM    TBL_LICENSEE_CONNECTION WHERE  IS_ACTIVE = 1)
            AND c.IS_REACTIVE = 0
            AND CONVERT (NVARCHAR (7), CREATE_ON, 126) + '-01' <= '2016-02-01'
    UNION ALL
    SELECT  cr.CREDIT_MONTH,
            cr.CURRENCY_ID,
            cc.CURRENCY_SING,
            CUSTOMER_CONNECTION_TYPE_ID = CASE WHEN cr.CUSTOMER_CONNECTION_TYPE_ID = 1 THEN 1
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (4) THEN 2
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (7) THEN 3
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (21) THEN 4
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (2) THEN 5
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (18) THEN 6
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (0) THEN 7
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (3) THEN 8
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (17) THEN 9
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (0) THEN 10
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (14) THEN 11
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (8) THEN 12
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (10) THEN 13
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (0) THEN 14
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (5) THEN 15
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (20) THEN 16
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (0) THEN 17
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (6) THEN 18
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (19) THEN 19
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (0) THEN 20
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (15) THEN 21
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (16) THEN 22
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (9) THEN 23 ELSE cr.CUSTOMER_CONNECTION_TYPE_ID END,
            cr.USAGE,
            PRICE                       = 800,
            TOTAL_AMOUNT                = cr.USAGE * 800
    FROM    TBL_PREPAID_CREDIT       cr
            INNER JOIN TBL_CUSTOMER  c ON c.CUSTOMER_ID = cr.CUSTOMER_ID
            INNER JOIN TLKP_CURRENCY cc ON cc.CURRENCY_ID = cr.CURRENCY_ID
    WHERE   cr.CREDIT_MONTH BETWEEN @D1 AND @D2
            AND (cr.CREDIT_MONTH BETWEEN '2016-03-01' AND '2017-02-01')
    UNION ALL
    SELECT  cr.CREDIT_MONTH,
            cr.CURRENCY_ID,
            cc.CURRENCY_SING,
            CUSTOMER_CONNECTION_TYPE_ID = CASE WHEN cr.CUSTOMER_CONNECTION_TYPE_ID = 1 THEN 1
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (4) THEN 2
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (7) THEN 3
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (21) THEN 4
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (2) THEN 5
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (18) THEN 6
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (0) THEN 7
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (3) THEN 8
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (17) THEN 9
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (0) THEN 10
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (14) THEN 11
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (8) THEN 12
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (10) THEN 13
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (0) THEN 14
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (5) THEN 15
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (20) THEN 16
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (0) THEN 17
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (6) THEN 18
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (19) THEN 19
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (0) THEN 20
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (15) THEN 21
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (16) THEN 22
                                          WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN (9) THEN 23 ELSE cr.CUSTOMER_CONNECTION_TYPE_ID END,
            cr.USAGE,
            PRICE                       = cr.BASED_PRICE,
            TOTAL_AMOUNT                = cr.USAGE * BASED_PRICE
    FROM    TBL_PREPAID_CREDIT       cr
            INNER JOIN TBL_CUSTOMER  c ON c.CUSTOMER_ID = cr.CUSTOMER_ID
            INNER JOIN TLKP_CURRENCY cc ON cc.CURRENCY_ID = cr.CURRENCY_ID
    WHERE   cr.CREDIT_MONTH BETWEEN @D1 AND @D2;

    -- BUILD CROSS TAB DEFINITION
    SELECT  CUSTOMER_CONNECTION_TYPE_ID,
            COL = ROW_NUMBER () OVER (ORDER BY CUSTOMER_CONNECTION_TYPE_ID) % 2,
            ROW = (1 + ROW_NUMBER () OVER (ORDER BY CUSTOMER_CONNECTION_TYPE_ID)) / 2
    INTO    #DEF
    FROM(SELECT DISTINCT CUSTOMER_CONNECTION_TYPE_ID FROM   #TMP) x;

    -- TO FILL 12MONTHs DATA 
    INSERT INTO #TMP
    SELECT  m.M,
            t.CURRENCY_ID,
            t.CURRENCY_SING,
            t.CUSTOMER_CONNECTION_TYPE_ID,
            TOTAL_USAGE  = 0,
            PRICE        = 0,
            TOTAL_AMOUNT = 0
    FROM    #MONTHS m,
    (
    SELECT  DISTINCT CUSTOMER_CONNECTION_TYPE_ID,
                     CURRENCY_ID,
                     CURRENCY_SING
    FROM    #TMP
    ) t;

    -- IN CASE NO RECORD
    IF(SELECT   COUNT (*) FROM  #TMP) = 0 BEGIN
        INSERT INTO #TMP SELECT M, 0, 0, 0, 0, 0, 0 FROM    #MONTHS;
        INSERT INTO #DEF SELECT 0, 1, 1;
    END;
    -- RENDER RESULT
    SELECT  ROW,
            INVOICE_MONTH,
            TYPE_ID1     = MAX (CASE WHEN COL = 1 THEN t.CUSTOMER_CONNECTION_TYPE_ID ELSE 0 END),
            [TYPE_NAME]  = MAX (CASE WHEN COL = 1 THEN TYPE_NAME ELSE '' END),
            USAGE1       = NULLIF(SUM (CASE WHEN COL = 1 THEN t.TOTAL_USAGE ELSE 0 END), 0),
            PRICE1       = NULLIF(MAX (CASE WHEN COL = 1 THEN t.PRICE ELSE 0 END), 0),
            CURRENCY_ID1 = MAX (CASE WHEN COL = 1 THEN t.CURRENCY_ID ELSE 0 END),
            CURR_SIGN1   = MAX (CASE WHEN COL = 1 THEN t.CURRENCY_SING ELSE '' END),
            AMOUNT1      = NULLIF(SUM (CASE WHEN COL = 1 THEN t.TOTAL_AMOUNT ELSE 0 END), 0),
            TYPE_ID2     = MAX (CASE WHEN COL = 0 THEN t.CUSTOMER_CONNECTION_TYPE_ID ELSE 0 END),
            TYPE_NAME2   = MAX (CASE WHEN COL = 0 THEN TYPE_NAME ELSE '' END),
            USAGE2       = NULLIF(SUM (CASE WHEN COL = 0 THEN t.TOTAL_USAGE ELSE 0 END), 0),
            PRICE2       = NULLIF(MAX (CASE WHEN COL = 0 THEN t.PRICE ELSE 0 END), 0),
            CURRENCY_ID2 = MAX (CASE WHEN COL = 0 THEN t.CURRENCY_ID ELSE 0 END),
            CURR_SIGN2   = MAX (CASE WHEN COL = 0 THEN t.CURRENCY_SING ELSE '' END),
            AMOUNT2      = NULLIF(SUM (CASE WHEN COL = 0 THEN t.TOTAL_AMOUNT ELSE 0 END), 0)
    FROM    #TMP                t
            LEFT JOIN #DEF      d ON d.CUSTOMER_CONNECTION_TYPE_ID = t.CUSTOMER_CONNECTION_TYPE_ID
            LEFT JOIN #TMP_TYPE c ON c.TYPE_ID = t.CUSTOMER_CONNECTION_TYPE_ID
    GROUP BY ROW,
             INVOICE_MONTH
    ORDER BY ROW,
             INVOICE_MONTH;
--END REPORT_QUARTER_SOLD_CONSUMER
END;
GO
IF NOT EXISTS (SELECT   1 FROM  sys.objects WHERE   name = N'SEC.TBL_AUDITTRAIL')
    CREATE TABLE [dbo].[SEC.TBL_AUDITTRAIL]
    (
    [Id]         [BIGINT]         PRIMARY KEY IDENTITY (1, 1) NOT NULL,
    [UserId]     [INT]            NULL,
    [AuditDate]  [DATETIME]       NULL,
    [TableName]  [NVARCHAR] (200) NULL,
    [PrimaryKey] [INT]            NULL,
    [OldObject]  [NTEXT]          NULL,
    [NewObject]  [NTEXT]          NULL,
    [ChangeLog]  [NTEXT]          NULL,
    [Devivce]    [NVARCHAR] (200) NULL,
    [AppVersion] [NVARCHAR] (200) NULL,
    [Context]    [NVARCHAR] (500) NULL
    );
GO
UPDATE  dbo.TLKP_FILTER_OPTION
SET XML_FILE = 0xEFBBBF3C5874726153657269616C697A65722076657273696F6E3D22312E3022206170706C69636174696F6E3D2256696577223E0D0A20203C70726F7065727479206E616D653D22234C61796F757456657273696F6E22202F3E0D0A20203C70726F7065727479206E616D653D22234C61796F75745363616C65466163746F72223E40312C57696474683D3140312C4865696768743D313C2F70726F70657274793E0D0A20203C70726F7065727479206E616D653D224F7074696F6E7356696577222069736E756C6C3D2274727565222069736B65793D2274727565223E0D0A202020203C70726F7065727479206E616D653D2253686F77566572746963616C4C696E6573223E46616C73653C2F70726F70657274793E0D0A202020203C70726F7065727479206E616D653D2253686F77496E64696361746F72223E66616C73653C2F70726F70657274793E0D0A202020203C70726F7065727479206E616D653D22456E61626C65417070656172616E63654576656E526F77223E747275653C2F70726F70657274793E0D0A20203C2F70726F70657274793E0D0A20203C70726F7065727479206E616D653D2241637469766546696C746572456E61626C6564223E747275653C2F70726F70657274793E0D0A20203C70726F7065727479206E616D653D22436F6C756D6E73222069736B65793D2274727565222076616C75653D223138223E0D0A202020203C70726F7065727479206E616D653D224974656D31222069736E756C6C3D2274727565222069736B65793D2274727565223E0D0A2020202020203C70726F7065727479206E616D653D224E616D65223E636F6C435553544F4D45525F434F44453C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D2256697369626C65496E646578223E313C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D2256697369626C65223E747275653C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D225769647468223E3133363C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D696E5769647468223E35303C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D61785769647468223E303C2F70726F70657274793E0D0A202020203C2F70726F70657274793E0D0A202020203C70726F7065727479206E616D653D224974656D32222069736E756C6C3D2274727565222069736B65793D2274727565223E0D0A2020202020203C70726F7065727479206E616D653D224E616D65223E636F6C435553544F4D45525F4E414D453C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D2256697369626C65496E646578223E323C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D2256697369626C65223E747275653C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D225769647468223E3138353C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D696E5769647468223E32303C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D61785769647468223E303C2F70726F70657274793E0D0A202020203C2F70726F70657274793E0D0A202020203C70726F7065727479206E616D653D224974656D33222069736E756C6C3D2274727565222069736B65793D2274727565223E0D0A2020202020203C70726F7065727479206E616D653D224E616D65223E636F6C4D455445525F434F44453C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D2256697369626C65496E646578223E333C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D2256697369626C65223E747275653C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D225769647468223E3134383C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D696E5769647468223E3130303C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D61785769647468223E303C2F70726F70657274793E0D0A202020203C2F70726F70657274793E0D0A202020203C70726F7065727479206E616D653D224974656D34222069736E756C6C3D2274727565222069736B65793D2274727565223E0D0A2020202020203C70726F7065727479206E616D653D224E616D65223E636F6C435553544F4D45525F47524F55503C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D225769647468223E39393C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D696E5769647468223E32303C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D61785769647468223E303C2F70726F70657274793E0D0A202020203C2F70726F70657274793E0D0A202020203C70726F7065727479206E616D653D224974656D35222069736E756C6C3D2274727565222069736B65793D2274727565223E0D0A2020202020203C70726F7065727479206E616D653D224E616D65223E636F6C435553544F4D45525F434F4E4E454354494F4E5F545950453C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D225769647468223E39393C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D696E5769647468223E32303C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D61785769647468223E303C2F70726F70657274793E0D0A202020203C2F70726F70657274793E0D0A202020203C70726F7065727479206E616D653D224974656D36222069736E756C6C3D2274727565222069736B65793D2274727565223E0D0A2020202020203C70726F7065727479206E616D653D224E616D65223E636F6C415245413C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D2256697369626C65496E646578223E353C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D2256697369626C65223E747275653C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D225769647468223E3133303C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D696E5769647468223E32303C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D61785769647468223E303C2F70726F70657274793E0D0A202020203C2F70726F70657274793E0D0A202020203C70726F7065727479206E616D653D224974656D37222069736E756C6C3D2274727565222069736B65793D2274727565223E0D0A2020202020203C70726F7065727479206E616D653D224E616D65223E636F6C4359434C455F4E414D453C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D225769647468223E39393C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D696E5769647468223E32303C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D61785769647468223E303C2F70726F70657274793E0D0A202020203C2F70726F70657274793E0D0A202020203C70726F7065727479206E616D653D224974656D38222069736E756C6C3D2274727565222069736B65793D2274727565223E0D0A2020202020203C70726F7065727479206E616D653D224E616D65223E636F6C4455455F444154453C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D2256697369626C65496E646578223E373C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D2256697369626C65223E747275653C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D225769647468223E3135303C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D696E5769647468223E32303C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D61785769647468223E303C2F70726F70657274793E0D0A202020203C2F70726F70657274793E0D0A202020203C70726F7065727479206E616D653D224974656D39222069736E756C6C3D2274727565222069736B65793D2274727565223E0D0A2020202020203C70726F7065727479206E616D653D224E616D65223E636F6C4441593C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D2256697369626C65496E646578223E383C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D2256697369626C65223E747275653C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D225769647468223E3134333C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D696E5769647468223E32303C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D61785769647468223E303C2F70726F70657274793E0D0A202020203C2F70726F70657274793E0D0A202020203C70726F7065727479206E616D653D224974656D3130222069736E756C6C3D2274727565222069736B65793D2274727565223E0D0A2020202020203C70726F7065727479206E616D653D224E616D65223E636F6C5354415455533C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D2256697369626C65496E646578223E31303C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D2256697369626C65223E747275653C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D225769647468223E3132303C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D696E5769647468223E32303C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D61785769647468223E303C2F70726F70657274793E0D0A202020203C2F70726F70657274793E0D0A202020203C70726F7065727479206E616D653D224974656D3131222069736E756C6C3D2274727565222069736B65793D2274727565223E0D0A2020202020203C70726F7065727479206E616D653D224E616D65223E636F6C4455455F414D4F554E543C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D2256697369626C65496E646578223E393C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D2256697369626C65223E747275653C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D225769647468223E3235353C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D696E5769647468223E3130303C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D61785769647468223E303C2F70726F70657274793E0D0A202020203C2F70726F70657274793E0D0A202020203C70726F7065727479206E616D653D224974656D3132222069736E756C6C3D2274727565222069736B65793D2274727565223E0D0A2020202020203C70726F7065727479206E616D653D224E616D65223E636F6C43555252454E43593C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D2256697369626C65496E646578223E31313C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D2256697369626C65223E747275653C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D225769647468223E35303C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D696E5769647468223E35303C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D61785769647468223E303C2F70726F70657274793E0D0A202020203C2F70726F70657274793E0D0A202020203C70726F7065727479206E616D653D224974656D3133222069736E756C6C3D2274727565222069736B65793D2274727565223E0D0A2020202020203C70726F7065727479206E616D653D224E616D65223E636F6C43555252454E43595F5349474E3C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D2256697369626C65496E646578223E31303C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D2256697369626C65223E747275653C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D225769647468223E32303C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D696E5769647468223E31353C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D61785769647468223E32303C2F70726F70657274793E0D0A202020203C2F70726F70657274793E0D0A202020203C70726F7065727479206E616D653D224974656D3134222069736E756C6C3D2274727565222069736B65793D2274727565223E0D0A2020202020203C70726F7065727479206E616D653D224E616D65223E636F6C504F4C453C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D2256697369626C65496E646578223E353C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D2256697369626C65223E747275653C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D225769647468223E3132323C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D696E5769647468223E32303C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D61785769647468223E303C2F70726F70657274793E0D0A202020203C2F70726F70657274793E0D0A202020203C70726F7065727479206E616D653D224974656D3135222069736E756C6C3D2274727565222069736B65793D2274727565223E0D0A2020202020203C70726F7065727479206E616D653D224E616D65223E636F6C424F583C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D2256697369626C65496E646578223E363C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D2256697369626C65223E747275653C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D225769647468223E3132343C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D696E5769647468223E32303C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D61785769647468223E303C2F70726F70657274793E0D0A202020203C2F70726F70657274793E0D0A202020203C70726F7065727479206E616D653D224974656D3136222069736E756C6C3D2274727565222069736B65793D2274727565223E0D0A2020202020203C70726F7065727479206E616D653D224E616D65223E636F6C50484F4E453C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D2256697369626C65496E646578223E343C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D2256697369626C65223E747275653C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D225769647468223E3135333C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D696E5769647468223E32303C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D61785769647468223E303C2F70726F70657274793E0D0A202020203C2F70726F70657274793E0D0A202020203C70726F7065727479206E616D653D224974656D3137222069736E756C6C3D2274727565222069736B65793D2274727565223E0D0A2020202020203C70726F7065727479206E616D653D224E616D65223E636F6C50454E414C54593C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D2256697369626C65496E646578223E31323C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D2256697369626C65223E747275653C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D225769647468223E37353C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D696E5769647468223E32303C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D61785769647468223E303C2F70726F70657274793E0D0A202020203C2F70726F70657274793E0D0A202020203C70726F7065727479206E616D653D224974656D3138222069736E756C6C3D2274727565222069736B65793D2274727565223E0D0A2020202020203C70726F7065727479206E616D653D224E616D65223E636F6C466F726D617444696769743C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D225769647468223E37353C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D696E5769647468223E32303C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D224D61785769647468223E303C2F70726F70657274793E0D0A202020203C2F70726F70657274793E0D0A20203C2F70726F70657274793E0D0A20203C70726F7065727479206E616D653D2247726F757053756D6D617279222069736B65793D2274727565222076616C75653D223022202F3E0D0A20203C70726F7065727479206E616D653D2241637469766546696C746572537472696E67223E5B5354415455535D203D2027E19EA0E19EBDE19E9FE19E80E19EB6E19E9BE19E80E19F86E19E8EE19E8FE19F8B273C2F70726F70657274793E0D0A20203C70726F7065727479206E616D653D224D525546696C74657273496E666F222069736B65793D2274727565222076616C75653D2232223E0D0A202020203C70726F7065727479206E616D653D224974656D31222069736E756C6C3D2274727565222069736B65793D2274727565223E0D0A2020202020203C70726F7065727479206E616D653D224E6F6E436F6C756D6E46696C74657222202F3E0D0A2020202020203C70726F7065727479206E616D653D22436F6C756D6E7346696C746572222069736B65793D2274727565222076616C75653D2231223E0D0A20202020202020203C70726F7065727479206E616D653D224974656D31222069736E756C6C3D2274727565222069736B65793D2274727565223E0D0A202020202020202020203C70726F7065727479206E616D653D2254797065223E437573746F6D3C2F70726F70657274793E0D0A202020202020202020203C70726F7065727479206E616D653D2246696C746572537472696E67223E5B5354415455535D203D2027E19EA0E19EBDE19E9FE19E80E19EB6E19E9BE19E80E19F86E19E8EE19E8FE19F8B273C2F70726F70657274793E0D0A202020202020202020203C70726F7065727479206E616D653D22446973706C61795465787422202F3E0D0A202020202020202020203C70726F7065727479206E616D653D22436F6C756D6E4E616D65223E636F6C5354415455533C2F70726F70657274793E0D0A20202020202020203C2F70726F70657274793E0D0A2020202020203C2F70726F70657274793E0D0A202020203C2F70726F70657274793E0D0A202020203C70726F7065727479206E616D653D224974656D32222069736E756C6C3D2274727565222069736B65793D2274727565223E0D0A2020202020203C70726F7065727479206E616D653D224E6F6E436F6C756D6E46696C74657222202F3E0D0A2020202020203C70726F7065727479206E616D653D22436F6C756D6E7346696C746572222069736B65793D2274727565222076616C75653D2232223E0D0A20202020202020203C70726F7065727479206E616D653D224974656D31222069736E756C6C3D2274727565222069736B65793D2274727565223E0D0A202020202020202020203C70726F7065727479206E616D653D2254797065223E437573746F6D3C2F70726F70657274793E0D0A202020202020202020203C70726F7065727479206E616D653D2246696C746572537472696E67223E5B4455455F414D4F554E545D202667743B2031303030302E306D3C2F70726F70657274793E0D0A202020202020202020203C70726F7065727479206E616D653D22446973706C61795465787422202F3E0D0A202020202020202020203C70726F7065727479206E616D653D22436F6C756D6E4E616D65223E636F6C4455455F414D4F554E543C2F70726F70657274793E0D0A20202020202020203C2F70726F70657274793E0D0A20202020202020203C70726F7065727479206E616D653D224974656D32222069736E756C6C3D2274727565222069736B65793D2274727565223E0D0A202020202020202020203C70726F7065727479206E616D653D2254797065223E437573746F6D3C2F70726F70657274793E0D0A202020202020202020203C70726F7065727479206E616D653D2246696C746572537472696E67223E5B43555252454E43595D203D20313C2F70726F70657274793E0D0A202020202020202020203C70726F7065727479206E616D653D22446973706C61795465787422202F3E0D0A202020202020202020203C70726F7065727479206E616D653D22436F6C756D6E4E616D65223E636F6C43555252454E43593C2F70726F70657274793E0D0A20202020202020203C2F70726F70657274793E0D0A2020202020203C2F70726F70657274793E0D0A202020203C2F70726F70657274793E0D0A20203C2F70726F70657274793E0D0A20203C70726F7065727479206E616D653D22536F7274496E666F5374617465222069736B65793D2274727565222076616C75653D2235223E0D0A202020203C70726F7065727479206E616D653D224974656D31222069736E756C6C3D2274727565222069736B65793D2274727565223E0D0A2020202020203C70726F7065727479206E616D653D224F7264657273223E44657363656E64696E673C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D22436F6C756D6E73223E636F6C5354415455533C2F70726F70657274793E0D0A202020203C2F70726F70657274793E0D0A202020203C70726F7065727479206E616D653D224974656D32222069736E756C6C3D2274727565222069736B65793D2274727565223E0D0A2020202020203C70726F7065727479206E616D653D224F7264657273223E417363656E64696E673C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D22436F6C756D6E73223E636F6C50454E414C54593C2F70726F70657274793E0D0A202020203C2F70726F70657274793E0D0A202020203C70726F7065727479206E616D653D224974656D33222069736E756C6C3D2274727565222069736B65793D2274727565223E0D0A2020202020203C70726F7065727479206E616D653D224F7264657273223E44657363656E64696E673C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D22436F6C756D6E73223E636F6C43555252454E43593C2F70726F70657274793E0D0A202020203C2F70726F70657274793E0D0A202020203C70726F7065727479206E616D653D224974656D34222069736E756C6C3D2274727565222069736B65793D2274727565223E0D0A2020202020203C70726F7065727479206E616D653D224F7264657273223E417363656E64696E673C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D22436F6C756D6E73223E636F6C415245413C2F70726F70657274793E0D0A202020203C2F70726F70657274793E0D0A202020203C70726F7065727479206E616D653D224974656D35222069736E756C6C3D2274727565222069736B65793D2274727565223E0D0A2020202020203C70726F7065727479206E616D653D224F7264657273223E417363656E64696E673C2F70726F70657274793E0D0A2020202020203C70726F7065727479206E616D653D22436F6C756D6E73223E636F6C435553544F4D45525F434F44453C2F70726F70657274793E0D0A202020203C2F70726F70657274793E0D0A202020203C70726F7065727479206E616D653D22436F6E74656E74222069736E756C6C3D2274727565222069736B65793D2274727565223E0D0A2020202020203C70726F7065727479206E616D653D2247726F7570436F756E74223E343C2F70726F70657274793E0D0A202020203C2F70726F70657274793E0D0A20203C2F70726F70657274793E0D0A20203C70726F7065727479206E616D653D2247726F757053756D6D617279536F7274496E666F537461746522202F3E0D0A20203C70726F7065727479206E616D653D2246696E6446696C7465725465787422202F3E0D0A20203C70726F7065727479206E616D653D2246696E6450616E656C56697369626C65223E66616C73653C2F70726F70657274793E0D0A3C2F5874726153657269616C697A65723E
WHERE   FILTER_NAME = N'សម្រាប់បោះពុម្ភទម្រង់ចាស់'
        AND GRID_NAME = 'PAGECUSTOMERBLOCK'
        AND CREATE_BY = N'ប្រព័ន្ធ​';
GO

---update old permission accounting
DELETE  FROM TBL_PERMISSION WHERE   PERMISSION_ID = 162;
UPDATE  TBL_PERMISSION
SET PERMISSION_PARENT_ID = 179
WHERE   PERMISSION_ID BETWEEN 163 AND 167;
UPDATE  TBL_PERMISSION SET  PERMISSION_ORDER = 3 WHERE  PERMISSION_ID = 163;
UPDATE  TBL_PERMISSION SET  PERMISSION_ORDER = 4 WHERE  PERMISSION_ID = 164;
UPDATE  TBL_PERMISSION SET  PERMISSION_ORDER = 5 WHERE  PERMISSION_ID = 165;
UPDATE  TBL_PERMISSION SET  PERMISSION_ORDER = 6 WHERE  PERMISSION_ID = 166;
UPDATE  TBL_PERMISSION SET  PERMISSION_ORDER = 7 WHERE  PERMISSION_ID = 167;
UPDATE  TBL_PERMISSION
SET PERMISSION_PARENT_ID = 132,
    PERMISSION_ORDER = 7
WHERE   PERMISSION_ID = 159;
UPDATE  TBL_PERMISSION
SET PERMISSION_PARENT_ID = 132,
    PERMISSION_ORDER = 8
WHERE   PERMISSION_ID = 156;
DELETE  FROM TBL_PERMISSION WHERE   PERMISSION_ID = 158;
DELETE  FROM TBL_PERMISSION WHERE   PERMISSION_ID IN (154, 196, 197);
UPDATE  TBL_PERMISSION
SET PERMISSION_DISPLAY = N'ចំណូល'
WHERE   PERMISSION_ID = 153;
UPDATE  TBL_PERMISSION
SET PERMISSION_PARENT_ID = 132,
    PERMISSION_ORDER = 9
WHERE   PERMISSION_ID = 152;
GO
---update add new permission accounting

IF NOT EXISTS (SELECT   * FROM  dbo.TBL_PERMISSION WHERE PERMISSION_ID = 218)BEGIN
    SET IDENTITY_INSERT dbo.TBL_PERMISSION ON;
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(218, N'ផ្នែគណនេយ្យ', 'ACCOUNTING_POINTER', 1, 151, 0);

    --Parent Permission(218)
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(219, N'គណនី', 'ACCOUNT_POINTER', 1, 218, 0);
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(220, N'ប្រតិបត្តិការប្រចាំថ្ងៃ', 'TRANSACTION_DATE_POINTER', 2, 218, 0);
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(221, N'ប្រតិបត្តិការ', 'TRANSACTION_POINTER', 3, 218, 0);
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(222, N'បញ្ជីទិនានុប្បវត្តិ', 'JOURNAL_POINTER', 4, 218, 0);
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(223, N'សមតុល្យដើមគ្រា', 'OPENING_BALANCE_POINTER', 5, 218, 0);
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(224, N'ចំណេញខាតលើអត្រាប្តូរប្រាក់', 'EXCHANE_RATE_LOSSES_POINTER', 6, 218, 0);
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(225, N'ផ្ទេរសមតុល្យ', 'TRANSFER_BALANCE_POINTER', 7, 218, 0);
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(226, N'ការប្តូរប្រាក់', 'EXCHANG_RATE_POINTER', 8, 218, 0);
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(227, N'ការលក់', 'SALE_POINTER', 2, 151, 0);

    --Parent Permission(227)
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(228, N'អតិថិជន', 'CUSTOMER_POINTER', 1, 227, 0);
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(229, N'វិក្កយបត្រលក់', 'SALE_BILL_POINTER', 2, 227, 0);
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(230, N'ការបង់ប្រាក់', 'SALE_PAYMENT_POINTER', 3, 227, 0);
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(231, N'វិក្កយបត្រឥណទាន', 'CREDIT_INVOCIE_POINTER', 4, 227, 0);
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(232, N'ការទិញ', 'PURCHASE_POINTER', 3, 151, 0);

    --Parent Permission(232)
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(233, N'អ្នកផ្គត់ផ្គង់', 'SUPPLIER_POINTER', 1, 232, 0);
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(234, N'បញ្ជាទិញ', 'PURCHASE_ORDER_POINTER', 2, 232, 0);
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(235, N'វិក្កយបត្រទិញ', 'PURCHASE_BILL_POINTER', 3, 232, 0);
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(236, N'ការបង់ប្រាក់', 'PURCHASE_PAYMENT_POINTER', 4, 232, 0);
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(237, N'អចលនទ្រព្យ', 'REAL_ESTATE_POINTER', 4, 151, 0);

    --Parent Permission(237)
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(238, N'អចលនទ្រព្យ', 'FIX_ASSET_POINTER', 1, 237, 0);
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(239, N'ប្រភេទអចលនទ្រព្យ', 'FIX_ASSET_TYPE_POINTER', 2, 237, 0);
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(240, N'សន្និធិ', 'INVENTORY_POINTER', 5, 151, 0);

    --Parent Permission(240)
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(241, N'ទទួលទំនិញ', 'RECEVIE_PRODUCT_POINTER', 1, 240, 0);
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(242, N'ដឹកទំនិញ', 'SHIPPING_PRODUCT_POINTER', 2, 240, 0);
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(243, N'ប្រភេទទំនិញ', 'PRODUCT_TYPE_POINTER', 3, 240, 0);
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(244, N'ទំនិញ', 'PRODUCT_POINTER', 4, 240, 0);
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(245, N'ខ្នាត', 'SCALE_POINTER', 5, 240, 0);
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(246, N'របាយការណ៍', 'REPORT_POINTER', 6, 151, 0);

    --Parent Permission(246)
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(247, N'របាយការណ៍គណនេយ្យ', 'REPORT_ACCOUNTING_POINTER', 1, 246, 0);
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(248, N'ការកំណត់', 'SETTINGS_POINTER', 7, 151, 0);

    --Parent Permission(248)
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(249, N'ការកំណត់', 'SETTING_POINTER', 1, 248, 0);
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(250, N'បើកប្រព័ន្ធគណនេយ្យ', 'OPEN_SYSTEM_POINTER', 2, 248, 0);
    INSERT INTO dbo.TBL_PERMISSION(
                                  PERMISSION_ID, PERMISSION_DISPLAY, PERMISSION_NAME, PERMISSION_ORDER, PERMISSION_PARENT_ID, PERMISSION_LEVEL
                                  )
    VALUES(251, N'EAC App', 'EAC App', 5, 203, 0);
    SET IDENTITY_INSERT dbo.TBL_PERMISSION OFF;
END;
GO
IF NOT EXISTS (SELECT   * FROM  dbo.TBL_ROLE_PERMISSION WHERE   PERMISSION_ID >= 218)BEGIN
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 218, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 219, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 220, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 221, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 222, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 223, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 224, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 225, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 226, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 227, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 228, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 229, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 230, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 231, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 232, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 233, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 234, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 235, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 236, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 237, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 238, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 239, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 240, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 241, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 242, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 243, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 244, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 245, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 246, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 247, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 248, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 249, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 250, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 218, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 219, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 220, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 221, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 222, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 223, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 224, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 225, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 226, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 227, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 228, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 229, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 230, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 231, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 232, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 233, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 234, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 235, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 236, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 237, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 238, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 239, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 240, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 241, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 242, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 243, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 244, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 245, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 246, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 247, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 248, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 249, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 250, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 218, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 219, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 220, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 221, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 222, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 223, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 224, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 225, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 226, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 227, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 228, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 229, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 230, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 231, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 232, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 233, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 234, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 235, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 236, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 237, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 238, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 239, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 240, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 241, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 242, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 243, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 244, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 245, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 246, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 247, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 248, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 249, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 250, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 218, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 219, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 220, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 221, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 222, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 223, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 224, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 225, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 226, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 227, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 228, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 229, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 230, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 231, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 232, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 233, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 234, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 235, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 236, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 237, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 238, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 239, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 240, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 241, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 242, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 243, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 244, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 245, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 246, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 247, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 248, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 249, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 250, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(1, 251, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(2, 251, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(3, 251, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(4, 251, 1);
    INSERT INTO dbo.TBL_ROLE_PERMISSION(ROLE_ID, PERMISSION_ID, IS_ALLOWED)
    VALUES(5, 251, 1);
END;
GO
UPDATE  dbo.TBL_ROLE SET ROLE_NAME = N'គណនេយ្យករ' WHERE ROLE_ID = 3;
GO
IF OBJECT_ID ('REPORT_CUSTOMERS') IS NOT NULL DROP PROC dbo.REPORT_CUSTOMERS;
GO
CREATE PROC [dbo].[REPORT_CUSTOMERS]
    @AREA_ID                     INT      = 0,
    @PRICE_ID                    INT      = 0,
    @CYCLE_ID                    INT      = 0,
    @CUSTOMER_CONNECTION_TYPE_ID INT      = 0,
    @MONTH                       DATETIME = '2018-06-01',
    @CUS_STATUS_ID               INT      = -2,
    @IS_MARKET_VENDOR            BIT      = 0,
    @AMPERE_ID                   INT      = 0,
    @CUSTOMER_GROUP_TYPE_ID      INT      = 0
AS
-- @SINCE 2011-03-30 Update customer as reading list
-- @SINCE 2011-04-20 Trim Meter Code.
-- @SINCE 2011-11-04 EXCLUDE CLOSE AND CANCEL CUSTOMER 
-- @SINCE 2012-01-14 ADD AMPARE NAME
-- @SINCE 2014-04-28 ORDER CUSTOMER BY POSITION_IN_BOX
-- @SINCE 2016-12-12 ADD FILTER CUS_STATUS_ID
-- @SINCE 2020-06-24 ADD PARAMETER IS_MARKET_VENDOR
-- @SINCE 2020-07-23 ADD AMPERE PARAMETER
-- @SINCE 2020-12-08 FIX CUSTOMERS STOP & DELETE , Show Meter & Box
DECLARE @CMD NVARCHAR (MAX);
DECLARE @ID NVARCHAR (MAX);
WITH c AS (
          SELECT    RowNumber = ROW_NUMBER () OVER (PARTITION BY CUSTOMER_ID ORDER BY CUS_METER_ID DESC),
                    *
          FROM  TBL_CUSTOMER_METER
          )
SELECT  * INTO  #TempCusMeter FROM  c WHERE c.RowNumber = 1;
SELECT  CUSTOMER_CODE = c.CUSTOMER_CODE,
        CUSTOMER_NAME = c.LAST_NAME_KH + ' ' + c.FIRST_NAME_KH + ' ' + c.FIRST_NAME + ' ' + c.LAST_NAME,
        PHONE         = c.PHONE_1 + ' ' + c.PHONE_2,
        a.AREA_ID,
        a.AREA_CODE,
        AREA_NAME     = ISNULL (a.AREA_NAME, '-'),
        POLE_NAME     = ISNULL (p.POLE_CODE, '-'),
        BOX_CODE      = ISNULL (b.BOX_CODE, '-'),
        cm.POSITION_IN_BOX,
        METER_CODE    = REPLACE (LTRIM (REPLACE (ISNULL (m.METER_CODE, '-'), '0', ' ')), ' ', '0'),
        START_USAGE   = COALESCE (u.START_USAGE, o.END_USAGE, 0),
        END_USAGE     = COALESCE (u.END_USAGE, 0),
        AMPARE_NAME   = ISNULL (amp.AMPARE_NAME, '-'),
        c.STATUS_ID
INTO    #TEMP
FROM    TBL_CUSTOMER                                 c
        INNER JOIN dbo.TLKP_CUSTOMER_CONNECTION_TYPE ct ON c.CUSTOMER_CONNECTION_TYPE_ID = ct.CUSTOMER_CONNECTION_TYPE_ID
        LEFT JOIN #TempCusMeter                      cm ON cm.CUSTOMER_ID = c.CUSTOMER_ID
        LEFT JOIN TBL_METER                          m ON cm.METER_ID = m.METER_ID
        LEFT JOIN TBL_AREA                           a ON a.AREA_ID = c.AREA_ID
        LEFT JOIN TBL_BOX                            b ON b.BOX_ID = cm.BOX_ID
        LEFT JOIN TBL_POLE                           p ON p.POLE_ID = b.POLE_ID
        LEFT JOIN TBL_AMPARE                         amp ON amp.AMPARE_ID = c.AMP_ID
        OUTER APPLY(
                   SELECT   TOP 1   START_USAGE,
                                    END_USAGE
                   FROM TBL_USAGE
                   WHERE   CUSTOMER_ID = c.CUSTOMER_ID
                           AND USAGE_MONTH = @MONTH
                   ORDER BY USAGE_ID DESC
                   )                                 u
        OUTER APPLY(
                   SELECT   TOP 1   START_USAGE,
                                    END_USAGE
                   FROM TBL_USAGE
                   WHERE   CUSTOMER_ID = c.CUSTOMER_ID
                           AND USAGE_MONTH = DATEADD (MONTH, -1, @MONTH)
                   ORDER BY USAGE_ID DESC
                   ) o
WHERE(@AREA_ID = 0 OR   a.AREA_ID = @AREA_ID)
     AND (@CYCLE_ID = 0 OR  c.BILLING_CYCLE_ID = @CYCLE_ID)
     AND (@PRICE_ID = 0 OR  c.PRICE_ID = @PRICE_ID)
     AND (
         @CUSTOMER_CONNECTION_TYPE_ID = 0
         OR c.CUSTOMER_CONNECTION_TYPE_ID = @CUSTOMER_CONNECTION_TYPE_ID
         )
     AND (@IS_MARKET_VENDOR = 0 OR  c.CUSTOMER_TYPE_ID = -2)
     AND (@AMPERE_ID = 0 OR c.AMP_ID = @AMPERE_ID)
     AND (
         @CUSTOMER_GROUP_TYPE_ID = 0
         OR ct.NONLICENSE_CUSTOMER_GROUP_ID = @CUSTOMER_GROUP_TYPE_ID
         )
ORDER BY a.AREA_CODE,
         p.POLE_CODE,
         b.BOX_CODE,
         cm.POSITION_IN_BOX,
         c.CUSTOMER_CODE ASC;

--SET @ID=CASE WHEN @CUS_STATUS_ID=-1 THEN '1,2,3' WHEN @CUS_STATUS_ID=-2 THEN '4,5' ELSE @CUS_STATUS_ID END;
IF @CUS_STATUS_ID = -1 BEGIN
    SELECT  * FROM  #TEMP WHERE STATUS_ID IN (1, 2, 3);
END;
ELSE IF @CUS_STATUS_ID = -2 BEGIN
         SELECT * FROM  #TEMP WHERE STATUS_ID IN (4, 5);
END;
ELSE IF @CUS_STATUS_ID = 0 BEGIN
         SELECT * FROM  #TEMP;
END;
ELSE BEGIN
         SELECT * FROM  #TEMP WHERE STATUS_ID = @CUS_STATUS_ID;
END;
--END OF REPORT_CUSTOMERS
GO
IF OBJECT_ID ('REPORT_QUARTER_CUSTOMER_LICENSEE') IS NOT NULL
    DROP PROC REPORT_QUARTER_CUSTOMER_LICENSEE;
GO
CREATE PROC dbo.REPORT_QUARTER_CUSTOMER_LICENSEE
    @DATE DATETIME = '2022-01-01'
AS
DECLARE @D1 DATETIME,
        @D2 DATETIME;
SET @DATE = DATEADD (D, 0, DATEDIFF (D, 0, @DATE));
SET @D1 = @DATE;
SET @D2 = DATEADD (S, -1, DATEADD (M, 3, @DATE));
SELECT  LICENSE_NAME           = LICENSEE_NAME,
        NUMER_OF_CONNECTION    = COUNT (*),
        LOCATION_OF_CONNECTION = l.LOCATION,
        TARIFF                 = TARIFF,
        VOLTAGE                = VOLTAGE_NAME,
        cu.CURRENCY_ID,
        CURRENCY_SING
INTO    #tmp
FROM    TBL_LICENSEE_CONNECTION  c
        INNER JOIN TBL_LICENSEE  l ON l.LICENSEE_ID = c.LICENSEE_ID
        INNER JOIN TBL_VOLTAGE   v ON v.VOLTAGE_ID = l.VOLTAGE_ID
        INNER JOIN TLKP_CURRENCY cu ON l.CURRENCY_ID = cu.CURRENCY_ID
WHERE   l.IS_ACTIVE = 1
        AND c.IS_ACTIVE = 1
        AND (START_DATE <= @D2)
        AND (IS_INUSED = 1 OR   END_DATE > @D1)
GROUP BY LICENSEE_NAME,
         l.LOCATION,
         TARIFF,
         VOLTAGE_NAME,
         cu.CURRENCY_ID,
         CURRENCY_SING;
IF((SELECT  COUNT (*) FROM  #tmp) < 12)
    SELECT  TOP 12  *
    FROM(
        SELECT  *
        FROM    #tmp
        UNION ALL
        SELECT  NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL
        UNION ALL
        SELECT  NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL
        UNION ALL
        SELECT  NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL
        UNION ALL
        SELECT  NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL
        UNION ALL
        SELECT  NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL
        UNION ALL
        SELECT  NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL
        UNION ALL
        SELECT  NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL
        UNION ALL
        SELECT  NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL
        UNION ALL
        SELECT  NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL
        UNION ALL
        SELECT  NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL
        UNION ALL
        SELECT  NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL
        UNION ALL
        SELECT  NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL
        ) x;
ELSE SELECT * FROM  #tmp;
---END OF REPORT_QUARTER_CUSTOMER_LICENSEE
GO
IF OBJECT_ID ('REPORT_PAYMENT_SUMMARY') IS NOT NULL
    DROP PROC dbo.REPORT_PAYMENT_SUMMARY;
GO
CREATE PROC [dbo].[REPORT_PAYMENT_SUMMARY]
    @D1               DATETIME = '2022-06-22',
    @D2               DATETIME = '2022-06-22',
    @ITEM_TYPE_ID     INT      = 0,
    @ITEM_ID          INT      = 0,
    @CURRENCY_ID      INT      = 1,
    @AREA_ID          INT      = 0,
    @BILLING_CYCLE_ID INT      = 0
AS
/*
@2015-Jun-10 by Morm Raksmey
	1. Upate ON Payment take Payment that VOID
@2017-04-04 by Sieng Sotheara
	1. Verfiy @PARAMETER Unused
@2020-06-05 by HOR DARA
	1. Avoid Outer Apply
*/
DECLARE @DATE DATETIME;
SET @DATE = @D1;
SELECT  DISTINCT INVOICE_ID = inv.INVOICE_ID
INTO    #InvoiceIds
FROM    dbo.TBL_INVOICE                   inv
        INNER JOIN dbo.TBL_INVOICE_DETAIL invd ON inv.INVOICE_ID = invd.INVOICE_ID
        INNER JOIN dbo.TBL_INVOICE_ITEM   invt ON invt.INVOICE_ITEM_ID = invd.INVOICE_ITEM_ID
WHERE(@ITEM_ID = 0 OR   invt.INVOICE_ITEM_ID = @ITEM_ID)
     AND (@ITEM_TYPE_ID = 0 OR  invt.INVOICE_ITEM_TYPE_ID = @ITEM_TYPE_ID);

-- PAYMENT
SELECT  p.CURRENCY_ID,
        INVOICE_MONTH,
        TYPE   = 'PAYMENT',
        AMOUNT = SUM (ISNULL (pd.PAY_AMOUNT, 0))
INTO    #TMP
FROM    TBL_PAYMENT_DETAIL      pd
        INNER JOIN TBL_PAYMENT  p ON p.PAYMENT_ID = pd.PAYMENT_ID
        INNER JOIN TBL_INVOICE  i ON i.INVOICE_ID = pd.INVOICE_ID
        INNER JOIN #InvoiceIds  invId ON invId.INVOICE_ID = i.INVOICE_ID
        INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = i.CUSTOMER_ID
WHERE(p.PAY_DATE BETWEEN @D1 AND @D2)
     AND (@AREA_ID = 0 OR   c.AREA_ID = @AREA_ID)
     AND (@BILLING_CYCLE_ID = 0 OR  c.BILLING_CYCLE_ID = @BILLING_CYCLE_ID)
     AND i.IS_SERVICE_BILL = 0
--AND p.USER_CASH_DRAWER_ID!=0
--AND pd.PAY_AMOUNT>0
GROUP BY INVOICE_MONTH,
         p.CURRENCY_ID

-- NEW INVOICE
UNION ALL
SELECT  CURRENCY_ID,
        INVOICE_MONTH,
        TYPE          = 'NEW_INVOICE',
        BEFORE_ADJUST = SUM (i.SETTLE_AMOUNT - ISNULL (adj.ALL_ADJUST, 0))
--ADJUST_AMOUNT = ISNULL(adj.ALL_ADJUST,0),
--AFTER_ADJUST = i.SETTLE_AMOUNT
FROM    TBL_INVOICE  i
        OUTER APPLY(
                   SELECT   ALL_ADJUST = SUM (ADJUST_AMOUNT)
                   FROM TBL_INVOICE_ADJUSTMENT
                   WHERE   INVOICE_ID = i.INVOICE_ID
                   ) adj
        INNER JOIN #InvoiceIds  invId ON invId.INVOICE_ID = i.INVOICE_ID
        INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = i.CUSTOMER_ID
WHERE   INVOICE_DATE BETWEEN @D1 AND @D2
        AND (@AREA_ID = 0 OR c.AREA_ID = @AREA_ID)
        AND (@BILLING_CYCLE_ID = 0 OR   c.BILLING_CYCLE_ID = @BILLING_CYCLE_ID)
        AND i.IS_SERVICE_BILL = 0
GROUP BY INVOICE_MONTH,
         CURRENCY_ID

-- ADJUSTMENT
UNION ALL
SELECT  CURRENCY_ID,
        INVOICE_MONTH,
        TYPE   = 'ADJUST_INVOICE',
        AMOUNT = SUM (ADJUST_AMOUNT)
FROM    TBL_INVOICE_ADJUSTMENT  a
        INNER JOIN TBL_INVOICE  i ON a.INVOICE_ID = i.INVOICE_ID
        INNER JOIN #InvoiceIds  invId ON invId.INVOICE_ID = i.INVOICE_ID
        INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = i.CUSTOMER_ID
WHERE(a.CREATE_ON BETWEEN @D1 AND @D2)
     AND (@AREA_ID = 0 OR   c.AREA_ID = @AREA_ID)
     AND (@BILLING_CYCLE_ID = 0 OR  c.BILLING_CYCLE_ID = @BILLING_CYCLE_ID)
     AND i.IS_SERVICE_BILL = 0
GROUP BY INVOICE_MONTH,
         CURRENCY_ID

-- CANCEL PAYMENT 
UNION ALL
SELECT  p.CURRENCY_ID,
        INVOICE_MONTH,
        TYPE   = 'CANCEL_PAYMENT',
        AMOUNT = SUM (ISNULL (pd.PAY_AMOUNT, 0) * -1)
FROM    TBL_PAYMENT_DETAIL      pd
        INNER JOIN TBL_PAYMENT  p ON p.PAYMENT_ID = pd.PAYMENT_ID
        INNER JOIN TBL_INVOICE  i ON i.INVOICE_ID = pd.INVOICE_ID
        INNER JOIN #InvoiceIds  invId ON invId.INVOICE_ID = i.INVOICE_ID
        INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = i.CUSTOMER_ID
WHERE(p.PAY_DATE BETWEEN @D1 AND @D2)
     AND (@AREA_ID = 0 OR   c.AREA_ID = @AREA_ID)
     AND (@BILLING_CYCLE_ID = 0 OR  c.BILLING_CYCLE_ID = @BILLING_CYCLE_ID)
     AND p.PAY_AMOUNT < 0
     AND i.IS_SERVICE_BILL = 0
     AND p.USER_CASH_DRAWER_ID != 0
GROUP BY INVOICE_MONTH,
         p.CURRENCY_ID
-- REPORT PAYMENT CANCEL

-- BEGIN AR
UNION ALL
SELECT  CURRENCY_ID,
        INVOICE_MONTH,
        TYPE    = 'BEGIN_AR',
        BALANCE = SUM (i.SETTLE_AMOUNT - ISNULL (adj.ALL_ADJUST, 0) + ISNULL (adj.ADJUST, 0) - ISNULL (pay.PAID_AMOUNT, 0))
FROM    TBL_INVOICE  i
        OUTER APPLY(
                   SELECT   PAID_AMOUNT = SUM (dd.PAY_AMOUNT)
                   FROM TBL_PAYMENT                   dp
                        INNER JOIN TBL_PAYMENT_DETAIL dd ON dd.PAYMENT_ID = dp.PAYMENT_ID
                   WHERE   dd.INVOICE_ID = i.INVOICE_ID
                           AND dp.PAY_DATE <= @D1
                   ) pay
        OUTER APPLY(
                   SELECT   ADJUST     = SUM (CASE WHEN CREATE_ON <= @D1 THEN ADJUST_AMOUNT ELSE 0 END),
                            ALL_ADJUST = SUM (ADJUST_AMOUNT)
                   FROM TBL_INVOICE_ADJUSTMENT
                   WHERE   INVOICE_ID = i.INVOICE_ID
                   ) adj
        INNER JOIN #InvoiceIds  invId ON invId.INVOICE_ID = i.INVOICE_ID
        INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = i.CUSTOMER_ID
WHERE   INVOICE_DATE < @D1
        AND i.SETTLE_AMOUNT - ISNULL (adj.ALL_ADJUST, 0) + ISNULL (adj.ADJUST, 0) - ISNULL (pay.PAID_AMOUNT, 0) > 0
        AND (@AREA_ID = 0 OR c.AREA_ID = @AREA_ID)
        AND (@BILLING_CYCLE_ID = 0 OR   c.BILLING_CYCLE_ID = @BILLING_CYCLE_ID)
        AND i.IS_SERVICE_BILL = 0
GROUP BY INVOICE_MONTH,
         CURRENCY_ID

-- ENDING_AR 
UNION ALL
SELECT  CURRENCY_ID,
        INVOICE_MONTH,
        TYPE    = 'ENDING_AR',
        BALANCE = SUM (i.SETTLE_AMOUNT - ISNULL (adj.ALL_ADJUST, 0) + ISNULL (adj.ADJUST, 0) - ISNULL (pay.PAID_AMOUNT, 0))
FROM    TBL_INVOICE  i
        OUTER APPLY(
                   SELECT   PAID_AMOUNT = SUM (dd.PAY_AMOUNT)
                   FROM TBL_PAYMENT                   dp
                        INNER JOIN TBL_PAYMENT_DETAIL dd ON dd.PAYMENT_ID = dp.PAYMENT_ID
                   WHERE   dd.INVOICE_ID = i.INVOICE_ID
                           AND dp.PAY_DATE <= @D2
                   ) pay
        OUTER APPLY(
                   SELECT   ADJUST     = SUM (CASE WHEN CREATE_ON <= @D2 THEN ADJUST_AMOUNT ELSE 0 END),
                            ALL_ADJUST = SUM (ADJUST_AMOUNT)
                   FROM TBL_INVOICE_ADJUSTMENT
                   WHERE   INVOICE_ID = i.INVOICE_ID
                   ) adj
        INNER JOIN #InvoiceIds  invId ON invId.INVOICE_ID = i.INVOICE_ID
        INNER JOIN TBL_CUSTOMER c ON i.CUSTOMER_ID = c.CUSTOMER_ID
WHERE   INVOICE_DATE < @D2
        AND i.SETTLE_AMOUNT - ISNULL (adj.ALL_ADJUST, 0) + ISNULL (adj.ADJUST, 0) - ISNULL (pay.PAID_AMOUNT, 0) > 0
        AND (@AREA_ID = 0 OR c.AREA_ID = @AREA_ID)
        AND (@BILLING_CYCLE_ID = 0 OR   c.BILLING_CYCLE_ID = @BILLING_CYCLE_ID)
        AND i.IS_SERVICE_BILL = 0
GROUP BY INVOICE_MONTH,
         CURRENCY_ID;
-- END OF REPORT_AGING_DETAIL
SELECT  cx.CURRENCY_ID,
        cx.CURRENCY_NAME,
        cx.CURRENCY_SING,
        INVOICE_MONTH,
        BEGIN_AR       = SUM (CASE WHEN TYPE = 'BEGIN_AR' THEN AMOUNT ELSE 0 END),
        NEW_INVOICE    = SUM (CASE WHEN TYPE = 'NEW_INVOICE' THEN AMOUNT ELSE 0 END),
        ADJUST_INVOICE = SUM (CASE WHEN TYPE = 'ADJUST_INVOICE' THEN AMOUNT ELSE 0 END),
        PAYMENT        = SUM (CASE WHEN TYPE = 'PAYMENT' THEN AMOUNT ELSE 0 END),
        CANCEL_PAYMENT = SUM (CASE WHEN TYPE = 'CANCEL_PAYMENT' THEN AMOUNT ELSE 0 END),
        ENDING_AR      = SUM (CASE WHEN TYPE = 'ENDING_AR' THEN AMOUNT ELSE 0 END)
FROM    #TMP                     t
        INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID = t.CURRENCY_ID
GROUP BY INVOICE_MONTH,
         cx.CURRENCY_ID,
         cx.CURRENCY_NAME,
         cx.CURRENCY_SING;
-- END OF REPORT_PAYMENT_SUMMARY
GO
IF OBJECT_ID ('REPORT_TRIAL_BALANCE_CUSTOMER_DETAIL') IS NOT NULL
    DROP PROC REPORT_TRIAL_BALANCE_CUSTOMER_DETAIL;
GO
CREATE PROC [dbo].[REPORT_TRIAL_BALANCE_CUSTOMER_DETAIL]
    @D1          DATETIME = '2014-06-21',
    @D2          DATETIME = '2014-12-31',
    @CURRENCY_ID INT      = 0,
    @AREA_ID     INT      = 0,
    @CYCLE_ID    INT      = 0
AS
-- 2014-07-23 Use AREA_CODE instead of AREA_NAME for Crystal+Unicode Error 
-- 2014-12-11 SMEY : ADD PARAMETER @AREA_ID
-- 2014-12-29 SMEY : SHOW CUSTOMER ADDRESSS
-- 2016-12-20 SOTHERA : ADD FILTER BILLING CYCLE ID
SET @D1 = DATEADD (D, 0, DATEDIFF (D, 0, @D1));
SET @D2 = DATEADD (ms, -1, DATEADD (D, 1, DATEDIFF (D, 0, @D2)));
SELECT  cx.CURRENCY_ID,
        cx.CURRENCY_NAME,
        cx.CURRENCY_SING,
        c.CUSTOMER_ID,
        c.CUSTOMER_CODE,
        CUSTOMER_NAME  = c.LAST_NAME_KH + ' ' + c.FIRST_NAME_KH,
        AREA_NAME      = ISNULL (a.AREA_CODE, '-'),
        BOX_CODE       = ISNULL (b.BOX_CODE, '-'),
        METER_CODE     = ISNULL (REPLACE (LTRIM (REPLACE (m.METER_CODE, '0', ' ')), ' ', '0'), '-'),
        x.ITEM,
        x.ITEM_EN,
        x.ITEM_ID,
        x.[CREATE_ON],
        PHONE          = c.PHONE_1,
        AMPARE_NAME    = ISNULL (AMPARE_NAME, '-'),
        DEPOSIT        = ISNULL (d.DEPOSIT, 0),
        CONNECTION_FEE = ISNULL (f.CONNECTION_FEE, 0),
        RECONNECT      = ISNULL (r.RECONNECT, 0),
        v.VILLAGE_NAME,
        co.COMMUNE_NAME,
        dis.DISTRICT_NAME,
        p.PROVINCE_NAME
FROM(
    SELECT  ITEM       = N'អតិថិជនត្រូវភ្ជាប់',
            ITEM_EN    = N'Pending Customer',
            1          AS [ITEM_ID],
            CREATED_ON AS [CREATE_ON],
            CUSTOMER_ID
    FROM    TBL_CUSTOMER
    WHERE   STATUS_ID = 1
            AND (CREATED_ON BETWEEN @D1 AND @D2)
    UNION ALL
    SELECT  ITEM        = N'ចាប់ផ្តើមប្រើ',
            ITEM_EN     = N'New Activated Customers',
            2           AS [ITEM_ID],
            CHNAGE_DATE AS [CREATE_ON],
            CUSTOMER_ID
    FROM    TBL_CUS_STATUS_CHANGE
    WHERE   NEW_STATUS_ID = 2
            AND OLD_STATUS_ID = 1
            AND (CHNAGE_DATE BETWEEN @D1 AND @D2)
    UNION ALL
    SELECT  ITEM        = N'ប្រើឡើងវិញ',
            ITEM_EN     = N'Re-activated Customers',
            6           AS [ITEM_ID],
            CHNAGE_DATE AS [CREATE_ON],
            CUSTOMER_ID
    FROM    TBL_CUS_STATUS_CHANGE
    WHERE   NEW_STATUS_ID = 2
            AND OLD_STATUS_ID = 4
            AND (CHNAGE_DATE BETWEEN @D1 AND @D2)
    UNION ALL
    SELECT  ITEM        = N'ផ្តាច់ចរន្ត',
            ITEM_EN     = N'Blocked Customers',
            3           AS [ITEM_ID],
            CHNAGE_DATE AS [CREATE_ON],
            CUSTOMER_ID
    FROM    TBL_CUS_STATUS_CHANGE
    WHERE   NEW_STATUS_ID = 3
            AND OLD_STATUS_ID = 2
            AND (CHNAGE_DATE BETWEEN @D1 AND @D2)
    UNION ALL
    SELECT  ITEM        = N'ភ្ជាប់ចរន្តឡើងវិញ',
            ITEM_EN     = N'Unblocked Customers',
            4           AS [ITEM_ID],
            CHNAGE_DATE AS [CREATE_ON],
            CUSTOMER_ID
    FROM    TBL_CUS_STATUS_CHANGE
    WHERE   NEW_STATUS_ID = 2
            AND OLD_STATUS_ID = 3
            AND (CHNAGE_DATE BETWEEN @D1 AND @D2)
    UNION ALL
    SELECT  ITEM        = N'ឈប់ប្រើប្រាស់',
            ITEM_EN     = N'Closed Customers',
            5           AS [ITEM_ID],
            CHNAGE_DATE AS [CREATE_ON],
            CUSTOMER_ID
    FROM    TBL_CUS_STATUS_CHANGE
    WHERE   NEW_STATUS_ID = 4
            AND (CHNAGE_DATE BETWEEN @D1 AND @D2)
    )                            x
    INNER JOIN TBL_CUSTOMER      c ON c.CUSTOMER_ID = x.CUSTOMER_ID
    INNER JOIN TBL_PRICE         px ON px.PRICE_ID = c.PRICE_ID
    LEFT JOIN TBL_CUSTOMER_METER cm ON cm.CUSTOMER_ID = c.CUSTOMER_ID
                                       AND  cm.IS_ACTIVE = 1
    LEFT JOIN TBL_AREA           a ON a.AREA_ID = c.AREA_ID
    LEFT JOIN TBL_BOX            b ON b.BOX_ID = cm.BOX_ID
    LEFT JOIN TBL_METER          m ON m.METER_ID = cm.METER_ID
    LEFT JOIN TBL_AMPARE         am ON am.AMPARE_ID = c.AMP_ID
    CROSS JOIN TLKP_CURRENCY     cx
    OUTER APPLY(
               SELECT   DEPOSIT = SUM (AMOUNT)
               FROM TBL_CUS_DEPOSIT
               WHERE CUSTOMER_ID = c.CUSTOMER_ID
                     AND IS_PAID = 1
                     AND cx.CURRENCY_ID = CURRENCY_ID
               ) d
    OUTER APPLY(
               SELECT   CONNECTION_FEE = SUM (pd.PAY_AMOUNT)
               FROM TBL_PAYMENT                   p
                    INNER JOIN TBL_PAYMENT_DETAIL pd ON pd.PAYMENT_ID = p.PAYMENT_ID
                    INNER JOIN TBL_INVOICE        i ON i.INVOICE_ID = pd.INVOICE_ID
               WHERE EXISTS (
                            SELECT  INVOICE_DETAIL_ID
                            FROM    TBL_INVOICE_DETAIL
                            WHERE   INVOICE_ID = i.INVOICE_ID
                                    AND INVOICE_ITEM_ID = 3
                            )
                     AND (p.PAY_DATE BETWEEN @D1 AND @D2)
                     AND i.CUSTOMER_ID = c.CUSTOMER_ID
                     AND cx.CURRENCY_ID = p.CURRENCY_ID
               ) f
    OUTER APPLY(
               SELECT   RECONNECT = SUM (pd.PAY_AMOUNT)
               FROM TBL_PAYMENT                   p
                    INNER JOIN TBL_PAYMENT_DETAIL pd ON pd.PAYMENT_ID = p.PAYMENT_ID
                    INNER JOIN TBL_INVOICE        i ON i.INVOICE_ID = pd.INVOICE_ID
               WHERE EXISTS (
                            SELECT  INVOICE_DETAIL_ID
                            FROM    TBL_INVOICE_DETAIL
                            WHERE   INVOICE_ID = i.INVOICE_ID
                                    AND INVOICE_ITEM_ID = -5
                            )
                     AND (p.PAY_DATE BETWEEN @D1 AND @D2)
                     AND i.CUSTOMER_ID = c.CUSTOMER_ID
                     AND cx.CURRENCY_ID = p.CURRENCY_ID
               ) r

    -- SELECT ADDRESS OF CUSTOMER
    INNER JOIN TLKP_VILLAGE  v ON v.VILLAGE_CODE = c.VILLAGE_CODE
    INNER JOIN TLKP_COMMUNE  co ON co.COMMUNE_CODE = v.COMMUNE_CODE
    INNER JOIN TLKP_DISTRICT dis ON dis.DISTRICT_CODE = co.DISTRICT_CODE
    INNER JOIN TLKP_PROVINCE p ON p.PROVINCE_CODE = dis.PROVINCE_CODE
WHERE(
     -- have deposit or connection
     ISNULL (d.DEPOSIT, 0) + ISNULL (f.CONNECTION_FEE, 0) + ISNULL (r.RECONNECT, 0) > 0
     -- if don't have deposit or connection free, use price currency to display
     OR cx.CURRENCY_ID = px.CURRENCY_ID
     )
     AND (@CURRENCY_ID = 0 OR   cx.CURRENCY_ID = @CURRENCY_ID)
     AND (@AREA_ID = 0 OR   a.AREA_ID = @AREA_ID)
     AND (@CYCLE_ID = 0 OR  c.BILLING_CYCLE_ID = @CYCLE_ID);
-- END OF REPORT_TRIAL_BALANCE_CUSTOMER_DETAIL
GO
IF OBJECT_ID ('REPORT_CUSTOMERS_DEPOSIT_CONNECTION_FEE') IS NOT NULL
    DROP PROC dbo.REPORT_CUSTOMERS_DEPOSIT_CONNECTION_FEE;
GO
CREATE PROC [dbo].[REPORT_CUSTOMERS_DEPOSIT_CONNECTION_FEE]
    @AREA_ID     INT      = 0,
    @D1          DATETIME = '2020-01-01',
    @D2          DATETIME = '2020-05-31',
    @CURRENCY_ID INT      = 0,
    @CYCLE_ID    INT      = 0
AS

-- 2014-12-29 SMEY : SHOW CUSTOMER ADDRESSS
-- 2016-12-29 SOTHEARA: ADD FILTER BILLING CYCLE ID
-- 2020-06-05 HOR DARA: AVOID OUTER APPLY
-- 2021-01-06 VONN KIMPUTHMUNYVORN : Add POLE CODE
SET @D1 = DATEADD (D, 0, DATEDIFF (D, 0, @D1));
SET @D2 = DATEADD (ms, -1, DATEADD (D, 1, DATEDIFF (D, 0, @D2)));
SELECT  CUSTOMER_ID,
        CURRENCY_ID,
        AMOUNT = SUM (AMOUNT)
INTO    #Dtmp
FROM    TBL_CUS_DEPOSIT
GROUP BY CUSTOMER_ID,
         CURRENCY_ID;
SELECT  cx.CURRENCY_ID,
        cx.CURRENCY_NAME,
        cx.CURRENCY_SING,
        c.CUSTOMER_ID,
        c.CUSTOMER_CODE,
        CUSTOMER_NAME      = c.LAST_NAME_KH + ' ' + c.FIRST_NAME_KH,
        AREA_NAME          = ISNULL (AREA_NAME, '-'),
        CUSTOMER_TYPE_NAME = ISNULL (ct.CUSTOMER_CONNECTION_TYPE_NAME, '-'),
        BOX_CODE           = ISNULL (b.BOX_CODE, '-'),
        METER_CODE         = ISNULL (REPLACE (LTRIM (REPLACE (m.METER_CODE, '0', ' ')), ' ', '0'), '-'),
        METER_NUMBER       = 1,
        PHONE              = c.PHONE_1,
        AMPARE_NAME        = ISNULL (AMPARE_NAME, '-'),
        DEPOSIT            = ISNULL (d.AMOUNT, 0),
        CONNECTION_FEE     = ISNULL (f.CONNECTION_FEE, 0),
        v.VILLAGE_NAME,
        co.COMMUNE_NAME,
        dis.DISTRICT_NAME,
        p.PROVINCE_NAME,
        POLE_CODE          = ISNULL (pp.POLE_CODE, '-')
FROM(
    SELECT  ITEM        = N'ចាប់ផ្តើមប្រើ',
            ITEM_EN     = N'New Activated Customers',
            2           AS [ITEM_ID],
            CHNAGE_DATE AS [CREATE_ON],
            CUSTOMER_ID
    FROM    TBL_CUS_STATUS_CHANGE
    WHERE   NEW_STATUS_ID = 2
            AND OLD_STATUS_ID = 1
            AND (CHNAGE_DATE BETWEEN @D1 AND @D2)
    )                                       x
    INNER JOIN TBL_CUSTOMER                 c ON c.CUSTOMER_ID = x.CUSTOMER_ID
                                                 AND (@AREA_ID = 0 OR   c.AREA_ID = @AREA_ID)
    INNER JOIN TBL_PRICE                    px ON px.PRICE_ID = c.PRICE_ID
    LEFT JOIN TBL_CUSTOMER_METER            cm ON cm.CUSTOMER_ID = c.CUSTOMER_ID
                                                  AND  cm.IS_ACTIVE = 1
    LEFT JOIN TBL_AREA                      a ON a.AREA_ID = c.AREA_ID
    LEFT JOIN TBL_BOX                       b ON b.BOX_ID = cm.BOX_ID
    LEFT JOIN TBL_POLE                      pp ON pp.POLE_ID = b.POLE_ID
    LEFT JOIN TBL_METER                     m ON m.METER_ID = cm.METER_ID
    LEFT JOIN TBL_AMPARE                    am ON am.AMPARE_ID = c.AMP_ID
    --LEFT JOIN TBL_CUSTOMER_TYPE ct ON ct.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
    LEFT JOIN TLKP_CUSTOMER_CONNECTION_TYPE ct ON ct.CUSTOMER_CONNECTION_TYPE_ID = c.CUSTOMER_CONNECTION_TYPE_ID
    CROSS JOIN TLKP_CURRENCY                cx
    LEFT JOIN #Dtmp d ON c.CUSTOMER_ID = d.CUSTOMER_ID
                         AND cx.CURRENCY_ID = d.CURRENCY_ID
    OUTER APPLY(
               SELECT   CONNECTION_FEE = SUM (pd.PAY_AMOUNT)
               FROM TBL_PAYMENT                   p
                    INNER JOIN TBL_PAYMENT_DETAIL pd ON pd.PAYMENT_ID = p.PAYMENT_ID
                    INNER JOIN TBL_INVOICE        i ON i.INVOICE_ID = pd.INVOICE_ID
               WHERE EXISTS (
                            SELECT  INVOICE_DETAIL_ID
                            FROM    TBL_INVOICE_DETAIL
                            WHERE   INVOICE_ID = i.INVOICE_ID
                                    AND INVOICE_ITEM_ID = 3
                            )
                     AND p.PAY_DATE BETWEEN @D1 AND @D2
                     AND i.CUSTOMER_ID = c.CUSTOMER_ID
                     AND cx.CURRENCY_ID = p.CURRENCY_ID
               )    f

    -- SELECT ADDRESS OF CUSTOMER
    INNER JOIN TLKP_VILLAGE  v ON v.VILLAGE_CODE = c.VILLAGE_CODE
    INNER JOIN TLKP_COMMUNE  co ON co.COMMUNE_CODE = v.COMMUNE_CODE
    INNER JOIN TLKP_DISTRICT dis ON dis.DISTRICT_CODE = co.DISTRICT_CODE
    INNER JOIN TLKP_PROVINCE p ON p.PROVINCE_CODE = dis.PROVINCE_CODE
WHERE(
     -- have deposit or connection
     ISNULL (d.AMOUNT, 0) + ISNULL (f.CONNECTION_FEE, 0) > 0
     -- if don't have deposit or connection free, use price currency to display
     OR cx.CURRENCY_ID = px.CURRENCY_ID
     )
     AND (@CURRENCY_ID = 0 OR   cx.CURRENCY_ID = @CURRENCY_ID)
     AND (@CYCLE_ID = 0 OR  c.BILLING_CYCLE_ID = @CYCLE_ID);
-- END REPORT_CUSTOMERS_DEPOSIT_CONNECTION_FEE
GO
IF OBJECT_ID ('REPORT_CUSTOMER_CHANGE_AMPARE') IS NOT NULL
    DROP PROC REPORT_CUSTOMER_CHANGE_AMPARE;
GO
CREATE PROC [dbo].[REPORT_CUSTOMER_CHANGE_AMPARE]
    @D1          DATETIME = '2019-01-01',
    @D2          DATETIME = '2021-09-15',
    @AREA_ID     INT      = 0,
    @CURRENCY_ID INT      = 0,
    @CYCLE_ID    INT      = 0
AS
/*
@2016-12-20 SIENG SOTHEARA
  1.ADD FILTER BILLING CYCLE
@2017-Jun-21 By Morm Raksmey
  1.Add Column CHANGE_DATE
*/
DECLARE @TODAY DATETIME;
SET @D2 = DATEADD (S, -1, DATEADD (DAY, 1, @D2));
SET @TODAY = GETDATE ();
SELECT  ch.REASON_ID,
        ch.CREATE_BY,
        ch.CREATE_ON,
        ch.CUSTOMER_CHANGE_AMPARE_ID,
        ch.NOTE,
        c.CUSTOMER_CODE,
        c.FIRST_NAME_KH,
        c.LAST_NAME_KH,
        AREA_NAME          = ISNULL (a.AREA_NAME, '-'),
        AREA_CODE          = ISNULL (a.AREA_CODE, '-'),
        CUSTOMER_TYPE_NAME = ISNULL (ct.CUSTOMER_TYPE_NAME, '-'),
        p.CURRENCY_ID,
        BOX_CODE           = ISNULL (b.BOX_CODE, '-'),
        METER_CODE         = ISNULL (REPLACE (LTRIM (REPLACE (ISNULL (m.METER_CODE, (SELECT METER_CODE FROM dbo.TBL_METER WHERE METER_ID = cm.METER_ID)), '0', ' ')), ' ', '0'), '-'),
        OLD_AMPARE         = ISNULL (old.AMPARE_NAME, '-'),
        NEW_AMPARE         = ISNULL (new.AMPARE_NAME, '-'),
        VILLAGE_NAME       = ISNULL (vil.VILLAGE_NAME, '-'),
        COMMUNE_NAME       = ISNULL (com.COMMUNE_NAME, '-'),
        DISTRICT_NAME      = ISNULL (dis.DISTRICT_NAME, '-'),
        PROVINCE_NAME      = ISNULL (pro.PROVINCE_NAME, '-'),
        USED_DATE          = ISNULL (ch.USED_DATE, ISNULL ((
                                                           SELECT   TOP(1) USED_DATE
                                                           FROM     dbo.TBL_CUSTOMER_METER
                                                           WHERE METER_ID = cm.METER_ID
                                                           ), '1990-01-01'
                                                          )
                                    ),
        DURATION_OF_USED   = ISNULL (DATEDIFF (MONTH, ISNULL (ch.USED_DATE, (
                                                                            SELECT  TOP(1) USED_DATE
                                                                            FROM    dbo.TBL_CUSTOMER_METER
                                                                            WHERE METER_ID = cm.METER_ID
                                                                            )
                                                             ), @TODAY
                                              ), 0
                                    )
FROM    TBL_CUSTOMER_CHANGE_AMPARE   ch
        INNER JOIN TBL_CUSTOMER      c ON ch.CUSTOMER_ID = c.CUSTOMER_ID
        INNER JOIN TBL_AREA          a ON c.AREA_ID = a.AREA_ID
        INNER JOIN TBL_CUSTOMER_TYPE ct ON c.CUSTOMER_TYPE_ID = ct.CUSTOMER_TYPE_ID
        INNER JOIN TBL_PRICE         p ON c.PRICE_ID = p.PRICE_ID
        LEFT JOIN TBL_CUSTOMER_METER cm ON c.CUSTOMER_ID = cm.CUSTOMER_ID
                                           AND cm.IS_ACTIVE = 1
        LEFT JOIN TBL_METER          m ON ch.METER_ID = m.METER_ID
        LEFT JOIN TBL_BOX            b ON b.BOX_ID = cm.BOX_ID
        LEFT JOIN TBL_AMPARE         old ON ch.OLD_AMPARE_ID = old.AMPARE_ID
        LEFT JOIN TBL_AMPARE         new ON ch.NEW_AMPARE_ID = new.AMPARE_ID
        LEFT JOIN TLKP_VILLAGE       vil ON c.VILLAGE_CODE = vil.VILLAGE_CODE
        LEFT JOIN TLKP_COMMUNE       com ON vil.COMMUNE_CODE = com.COMMUNE_CODE
        LEFT JOIN TLKP_DISTRICT      dis ON com.DISTRICT_CODE = dis.DISTRICT_CODE
        LEFT JOIN TLKP_PROVINCE      pro ON dis.PROVINCE_CODE = pro.PROVINCE_CODE
WHERE   ch.IS_ACTIVE = 1
        AND (ch.CREATE_ON BETWEEN @D1 AND @D2)
        AND (@AREA_ID = 0 OR c.AREA_ID = @AREA_ID)
        AND (@CURRENCY_ID = 0 OR p.CURRENCY_ID = @CURRENCY_ID)
        AND (@CYCLE_ID = 0 OR   c.BILLING_CYCLE_ID = @CYCLE_ID)
ORDER BY ch.CREATE_ON;
GO
IF OBJECT_ID ('REPORT_CUSTOMER_CHANGE_METER') IS NOT NULL
    DROP PROC dbo.REPORT_CUSTOMER_CHANGE_METER;
GO
CREATE PROC [dbo].[REPORT_CUSTOMER_CHANGE_METER]
    @D1            DATETIME = '2021-09-15',
    @D2            DATETIME = '2021-09-15',
    @AREA_ID       INT      = 0,
    @CURRENCY_ID   INT      = 0,
    @CYCLE_ID      INT      = 0,
    @METER_TYPE_ID INT      = 0
AS
/*
@2016-12-29 SIENG SOTHEARA
	1.ADD FILTER BILLING CYCLE
@2017-06-20 by Morm Raksmey
	1. change filter date to column CHANGE_DATE
*/
DECLARE @TODAY DATETIME;
SET @D2 = DATEADD (S, -1, DATEADD (DAY, 1, @D2));
SET @TODAY = GETDATE ();
SELECT  ch.*,
        c.CUSTOMER_CODE,
        c.FIRST_NAME_KH,
        c.LAST_NAME_KH,
        p.CURRENCY_ID,
        -- OLD METER --
        OLD_METER_CODE   = REPLACE (LTRIM (REPLACE (ISNULL (oldMETER.METER_CODE, '-'), '0', ' ')), ' ', '0'),
        -- NEW METER --
        NEW_METER_CODE   = REPLACE (LTRIM (REPLACE (ISNULL (newMETER.METER_CODE, '-'), '0', ' ')), ' ', '0'),
        VILLAGE_NAME     = ISNULL (vil.VILLAGE_NAME, '-'),
        COMMUNE_NAME     = ISNULL (com.COMMUNE_NAME, '-'),
        DISTRICT_NAME    = ISNULL (dis.DISTRICT_NAME, '-'),
        PROVINCE_NAME    = ISNULL (pro.PROVINCE_NAME, '-'),
        METER_TYPE_NAME  = ISNULL (mt.METER_TYPE_NAME, '-'),
        USED_DATE        = ISNULL ((
                                   SELECT   TOP(1)  cm.USED_DATE
                                   FROM dbo.TBL_CUSTOMER_METER cm
                                   WHERE   cm.METER_ID = oldMETER.METER_ID
                                   ), '1990-01-01'
                                  ),
        DURATION_OF_USED = ISNULL (DATEDIFF (MONTH, (
                                                    SELECT  TOP(1)  cm.USED_DATE
                                                    FROM    dbo.TBL_CUSTOMER_METER cm
                                                    WHERE   cm.METER_ID = oldMETER.METER_ID
                                                    ), @TODAY
                                            ), 0
                                  ),
        REASON           = ISNULL (ch.NOTE, '-')
FROM    TBL_CUSTOMER_CHANGE_METER    ch
        INNER JOIN TBL_CUSTOMER      c ON ch.CUSTOMER_ID = c.CUSTOMER_ID
        INNER JOIN TBL_CUSTOMER_TYPE ct ON c.CUSTOMER_TYPE_ID = ct.CUSTOMER_TYPE_ID
        LEFT JOIN TBL_AMPARE         amp ON c.AMP_ID = amp.AMPARE_ID
        INNER JOIN TBL_PRICE         p ON c.PRICE_ID = p.PRICE_ID
        -- OLD METER --
        LEFT JOIN TBL_METER          oldMETER ON ch.OLD_METER_ID = oldMETER.METER_ID
        LEFT JOIN dbo.TBL_METER_TYPE mt ON mt.METER_TYPE_ID = oldMETER.METER_TYPE_ID
        -- NEW METER --
        LEFT JOIN TBL_METER          newMETER ON ch.NEW_METER_ID = newMETER.METER_ID
        -- ADDRESS ---
        LEFT JOIN TLKP_VILLAGE       vil ON c.VILLAGE_CODE = vil.VILLAGE_CODE
        LEFT JOIN TLKP_COMMUNE       com ON vil.COMMUNE_CODE = com.COMMUNE_CODE
        LEFT JOIN TLKP_DISTRICT      dis ON com.DISTRICT_CODE = dis.DISTRICT_CODE
        LEFT JOIN TLKP_PROVINCE      pro ON dis.PROVINCE_CODE = pro.PROVINCE_CODE
WHERE   ch.IS_ACTIVE = 1
        --AND (ch.CREATE_ON BETWEEN @D1 AND @D2)
        AND (ch.CHANGE_DATE BETWEEN @D1 AND @D2)
        AND (@AREA_ID = 0 OR c.AREA_ID = @AREA_ID)
        AND (@CURRENCY_ID = 0 OR p.CURRENCY_ID = @CURRENCY_ID)
        AND (@CYCLE_ID = 0 OR   c.BILLING_CYCLE_ID = @CYCLE_ID)
        AND (@METER_TYPE_ID = 0 OR  mt.METER_TYPE_ID = @METER_TYPE_ID);
GO
IF OBJECT_ID ('REPORT_CHANGE_SERIAL_METER') IS NOT NULL
    DROP PROC REPORT_CHANGE_SERIAL_METER;
GO
CREATE PROC [dbo].[REPORT_CHANGE_SERIAL_METER]
    @CURRENCY_ID INT      = 0,
    @AREA_ID     INT      = 0,
    @CYCLE_ID    INT      = 0,
    @D1          DATETIME = '2019-01-01',
    @D2          DATETIME = '2022-06-24'
AS
SET @D2 = DATEADD (S, -1, DATEADD (DAY, 1, @D2));
SELECT  CUSTOMER_CODE = ISNULL (c.CUSTOMER_CODE, '-'),
        CUSTOMER_NAME = ISNULL ((c.LAST_NAME_KH + ' ' + c.FIRST_NAME_KH), '-'),
        AREA_NAME     = ISNULL (a.AREA_NAME, '-'),
        POLE_CODE     = ISNULL (p.POLE_CODE, '-'),
        BOX_CODE      = ISNULL (b.BOX_CODE, '-'),
        d.CHANGE_LOG_ID,
        OPERATOR      = ISNULL (cl.OPERATOR, '-'),
        cl.CHANGE_DATE,
        VILLAGE_NAME  = ISNULL (vi.VILLAGE_NAME, '-'),
        COMMUNE_NAME  = ISNULL (co.COMMUNE_NAME, '-'),
        DISTRICT_NAME = ISNULL (di.DISTRICT_NAME, '-'),
        PROVINCE_NAME = ISNULL (pr.PROVINCE_NAME, '-'),
        OLD_SERIAL    = REPLACE (LTRIM (REPLACE (ISNULL (d.OLD_VALUE, '-'), '0', ' ')), ' ', '0'),
        NEW_SERIAL    = REPLACE (LTRIM (REPLACE (ISNULL (d.NEW_VALUE, '-'), '0', ' ')), ' ', '0'),
        NEW_VALUE     = ISNULL (d.NEW_VALUE, '-'),
        REASON_ID     = ISNULL ((
                                SELECT  TOP(1)  NEW_VALUE
                                FROM    dbo.TBL_CHANGE_LOG_DETAIL
                                WHERE   CHANGE_LOG_ID = cl.CHANG_LOG_ID
                                        AND FIELD_NAME = 'REASON_ID'
                                ), '-'
                               )
FROM    TBL_CHANGE_LOG                  cl
        LEFT JOIN TBL_CHANGE_LOG_DETAIL d ON cl.CHANG_LOG_ID = d.CHANGE_LOG_ID
        LEFT JOIN TBL_CUSTOMER_METER    cm ON cm.METER_ID = cl.PRIMARY_KEY_VALUE
                                              AND cm.IS_ACTIVE = 1
        LEFT JOIN TBL_CUSTOMER          c ON c.CUSTOMER_ID = cm.CUSTOMER_ID
        LEFT JOIN TBL_AREA              a ON a.AREA_ID = c.AREA_ID
        LEFT JOIN TBL_POLE              p ON p.POLE_ID = cm.POLE_ID
        LEFT JOIN TBL_BOX               b ON b.BOX_ID = cm.BOX_ID
        LEFT JOIN TBL_PRICE             pi ON pi.PRICE_ID = c.PRICE_ID
        LEFT JOIN TLKP_VILLAGE          vi ON vi.VILLAGE_CODE = c.VILLAGE_CODE
        LEFT JOIN TLKP_COMMUNE          co ON co.COMMUNE_CODE = vi.COMMUNE_CODE
        LEFT JOIN TLKP_DISTRICT         di ON di.DISTRICT_CODE = co.DISTRICT_CODE
        LEFT JOIN TLKP_PROVINCE         pr ON pr.PROVINCE_CODE = di.PROVINCE_CODE
WHERE   cl.TABLE_ID = 28
        AND (FIELD_ID = 167)
        AND (d.OLD_VALUE <> '' AND  d.OLD_VALUE <> d.NEW_VALUE)
        AND cl.CHANGE_DATE BETWEEN @D1 AND @D2
        AND (a.AREA_ID = @AREA_ID OR @AREA_ID = 0)
        AND (pi.CURRENCY_ID = @CURRENCY_ID OR   @CURRENCY_ID = 0)
        AND (c.BILLING_CYCLE_ID = @CYCLE_ID OR  @CYCLE_ID = 0)
ORDER BY cl.CHANGE_DATE;
GO

IF OBJECT_ID ('REPORT_TRIAL_BALANCE_DEPOSIT_DETAIL') IS NOT NULL
    DROP PROC REPORT_TRIAL_BALANCE_DEPOSIT_DETAIL;
GO
CREATE PROC [dbo].[REPORT_TRIAL_BALANCE_DEPOSIT_DETAIL]
    @END_DATE    DATETIME = '2014-06-01',
    @START_DATE  DATETIME = '2014-05-01',
    @CURRENCY_ID INT      = 0,
    @CYCLE_ID    INT      = 0,
    @AREA        INT      = 0
AS
--SET in order to fast compare in query
SET @END_DATE = DATEADD (D, 0, DATEDIFF (D, 0, @END_DATE));
SET @END_DATE = DATEADD (S, -1, DATEADD (D, 1, @END_DATE));
SELECT  cx.CURRENCY_ID,
        cx.CURRENCY_NAME,
        cx.CURRENCY_SING,
        ITEM                               = N'ប្រាក់កក់បានសងអោយអតិថិជនវិញ',
        ITEM_EN                            = N'Deposit Refund to Customer',
        2                                  AS [ITEM_ID],
        DEPOSIT_DATE                       AS [CREATE_ON],
        AREA_NAME                          = ISNULL (AREA_NAME, '-'),
        AMPARE_NAME                        = ISNULL (AP.AMPARE_NAME, '-'),
        DEPOSIT_NO                         = ISNULL (DEP.DEPOSIT_NO, '-'),
        LAST_NAME_KH + ' ' + FIRST_NAME_KH AS [CUSTOMER_NAME],
        BOX_CODE                           = ISNULL (BOX_CODE, '-'),
        METER_CODE                         = ISNULL (REPLACE (LTRIM (REPLACE (METER_CODE, '0', ' ')), ' ', '0'), '-'),
        [AMOUNT]
FROM    TBL_CUS_DEPOSIT              DEP
        INNER JOIN TBL_CUSTOMER      C ON C.CUSTOMER_ID = DEP.CUSTOMER_ID
        INNER JOIN TBL_AMPARE        AP ON AP.AMPARE_ID = C.AMP_ID
        INNER JOIN TBL_AREA          A ON A.AREA_ID = C.AREA_ID
        LEFT JOIN TBL_CUSTOMER_METER CM ON CM.CUSTOMER_ID = C.CUSTOMER_ID
                                           AND CM.IS_ACTIVE = 1
        LEFT JOIN TBL_BOX            B ON B.BOX_ID = CM.BOX_ID
        LEFT JOIN TBL_METER          M ON M.METER_ID = CM.METER_ID
        INNER JOIN TLKP_CURRENCY     cx ON cx.CURRENCY_ID = DEP.CURRENCY_ID
WHERE   DEPOSIT_ACTION_ID = 3
        AND DEP.IS_PAID = 1
        AND (DEPOSIT_DATE BETWEEN @START_DATE AND @END_DATE)
        AND (@CURRENCY_ID = 0 OR cx.CURRENCY_ID = @CURRENCY_ID)
        AND (@CYCLE_ID = 0 OR   C.BILLING_CYCLE_ID = @CYCLE_ID)
        AND (@AREA = 0 OR   A.AREA_ID = @AREA)
UNION ALL
SELECT  cx.CURRENCY_ID,
        cx.CURRENCY_NAME,
        cx.CURRENCY_SING,
        ITEM                               = N'ប្រាក់កក់កាត់ទៅទូទាត់វិក័យប័ត្រ',
        ITEM_EN                            = N'Deposit Applied to Invoice Payment',
        3                                  AS [ITEM_ID],
        DEPOSIT_DATE                       AS [CREATE_ON],
        AREA_NAME                          = ISNULL (AREA_NAME, '-'),
        AMPARE_NAME                        = ISNULL (AP.AMPARE_NAME, '-'),
        DEPOSIT_NO                         = ISNULL (DEP.DEPOSIT_NO, '-'),
        LAST_NAME_KH + ' ' + FIRST_NAME_KH AS [CUSTOMER_NAME],
        BOX_CODE                           = ISNULL (BOX_CODE, '-'),
        METER_CODE                         = ISNULL (REPLACE (LTRIM (REPLACE (METER_CODE, '0', ' ')), ' ', '0'), '-'),
        [AMOUNT]
FROM    TBL_CUS_DEPOSIT              DEP
        INNER JOIN TBL_CUSTOMER      C ON C.CUSTOMER_ID = DEP.CUSTOMER_ID
        INNER JOIN TBL_AMPARE        AP ON AP.AMPARE_ID = C.AMP_ID
        INNER JOIN TBL_AREA          A ON A.AREA_ID = C.AREA_ID
        LEFT JOIN TBL_CUSTOMER_METER CM ON CM.CUSTOMER_ID = C.CUSTOMER_ID
        LEFT JOIN TBL_BOX            B ON B.BOX_ID = CM.BOX_ID
        LEFT JOIN TBL_METER          M ON M.METER_ID = CM.METER_ID
        INNER JOIN TLKP_CURRENCY     cx ON cx.CURRENCY_ID = DEP.CURRENCY_ID
WHERE   DEPOSIT_ACTION_ID = 4
        AND DEP.IS_PAID = 1
        AND (DEPOSIT_DATE BETWEEN @START_DATE AND @END_DATE)
        AND (@CURRENCY_ID = 0 OR cx.CURRENCY_ID = @CURRENCY_ID)
        AND (@CYCLE_ID = 0 OR   C.BILLING_CYCLE_ID = @CYCLE_ID)
        AND (@AREA = 0 OR   A.AREA_ID = @AREA)
UNION ALL
SELECT  cx.CURRENCY_ID,
        cx.CURRENCY_NAME,
        cx.CURRENCY_SING,
        ITEM                               = N'ប្រាក់កក់ត្រូវបានកែតំរូវ',
        ITEM_EN                            = N'Deposit Adjustment',
        4                                  AS [ITEM_ID],
        DEPOSIT_DATE                       AS [CREATE_ON],
        AREA_NAME                          = ISNULL (AREA_NAME, '-'),
        AMPARE_NAME                        = ISNULL (AP.AMPARE_NAME, '-'),
        DEPOSIT_NO                         = ISNULL (DEP.DEPOSIT_NO, '-'),
        LAST_NAME_KH + ' ' + FIRST_NAME_KH AS [CUSTOMER_NAME],
        BOX_CODE                           = ISNULL (BOX_CODE, '-'),
        METER_CODE                         = ISNULL (REPLACE (LTRIM (REPLACE (METER_CODE, '0', ' ')), ' ', '0'), '-'),
        [AMOUNT]
FROM    TBL_CUS_DEPOSIT              DEP
        INNER JOIN TBL_CUSTOMER      C ON C.CUSTOMER_ID = DEP.CUSTOMER_ID
        INNER JOIN TBL_AMPARE        AP ON AP.AMPARE_ID = C.AMP_ID
        INNER JOIN TBL_AREA          A ON A.AREA_ID = C.AREA_ID
        LEFT JOIN TBL_CUSTOMER_METER CM ON CM.CUSTOMER_ID = C.CUSTOMER_ID
                                           AND CM.IS_ACTIVE = 1
        LEFT JOIN TBL_BOX            B ON B.BOX_ID = CM.BOX_ID
        LEFT JOIN TBL_METER          M ON M.METER_ID = CM.METER_ID
        INNER JOIN TLKP_CURRENCY     cx ON cx.CURRENCY_ID = DEP.CURRENCY_ID
WHERE   DEPOSIT_ACTION_ID = 2
        AND DEP.IS_PAID = 1
        AND (DEPOSIT_DATE BETWEEN @START_DATE AND @END_DATE)
        AND (@CURRENCY_ID = 0 OR cx.CURRENCY_ID = @CURRENCY_ID)
        AND (@CYCLE_ID = 0 OR   C.BILLING_CYCLE_ID = @CYCLE_ID)
        AND (@AREA = 0 OR   A.AREA_ID = @AREA)
UNION ALL
SELECT  cx.CURRENCY_ID,
        cx.CURRENCY_NAME,
        cx.CURRENCY_SING,
        ITEM                               = N'ប្រាក់កក់បន្ថែមទទួលបាន',
        ITEM_EN                            = N'Deposit From Customer',
        -1                                 AS [ITEM_ID],
        DEPOSIT_DATE                       AS [CREATE_ON],
        AREA_NAME                          = ISNULL (AREA_NAME, '-'),
        AMPARE_NAME                        = ISNULL (AP.AMPARE_NAME, '-'),
        DEPOSIT_NO                         = ISNULL (DEP.DEPOSIT_NO, '-'),
        LAST_NAME_KH + ' ' + FIRST_NAME_KH AS [CUSTOMER_NAME],
        BOX_CODE                           = ISNULL (BOX_CODE, '-'),
        METER_CODE                         = ISNULL (REPLACE (LTRIM (REPLACE (METER_CODE, '0', ' ')), ' ', '0'), '-'),
        [AMOUNT]
FROM    TBL_CUS_DEPOSIT              DEP
        INNER JOIN TBL_CUSTOMER      C ON C.CUSTOMER_ID = DEP.CUSTOMER_ID
        INNER JOIN TBL_AMPARE        AP ON AP.AMPARE_ID = C.AMP_ID
        INNER JOIN TBL_AREA          A ON A.AREA_ID = C.AREA_ID
        LEFT JOIN TBL_CUSTOMER_METER CM ON CM.CUSTOMER_ID = C.CUSTOMER_ID
                                           AND CM.IS_ACTIVE = 1
        LEFT JOIN TBL_BOX            B ON B.BOX_ID = CM.BOX_ID
        LEFT JOIN TBL_METER          M ON M.METER_ID = CM.METER_ID
        INNER JOIN TLKP_CURRENCY     cx ON cx.CURRENCY_ID = DEP.CURRENCY_ID
WHERE   DEPOSIT_ACTION_ID = 1
        AND DEP.IS_PAID = 1
        AND (DEPOSIT_DATE BETWEEN @START_DATE AND @END_DATE)
        AND (@CURRENCY_ID = 0 OR cx.CURRENCY_ID = @CURRENCY_ID)
        AND (@CYCLE_ID = 0 OR   C.BILLING_CYCLE_ID = @CYCLE_ID)
        AND (@AREA = 0 OR   A.AREA_ID = @AREA)
UNION ALL
SELECT  cx.CURRENCY_ID,
        cx.CURRENCY_NAME,
        cx.CURRENCY_SING,
        ITEM                               = N'ប្រាក់កក់មិនទាន់បានទទួល',
        ITEM_EN                            = N'Deposit Not Yet Paid',
        1                                  AS [ITEM_ID],
        DEPOSIT_DATE                       AS [CREATE_ON],
        AREA_NAME                          = ISNULL (AREA_NAME, '-'),
        AMPARE_NAME                        = ISNULL (AP.AMPARE_NAME, '-'),
        DEPOSIT_NO                         = ISNULL (DEP.DEPOSIT_NO, '-'),
        LAST_NAME_KH + ' ' + FIRST_NAME_KH AS [CUSTOMER_NAME],
        BOX_CODE                           = ISNULL (BOX_CODE, '-'),
        METER_CODE                         = ISNULL (REPLACE (LTRIM (REPLACE (METER_CODE, '0', ' ')), ' ', '0'), '-'),
        [AMOUNT]
FROM    TBL_CUS_DEPOSIT              DEP
        INNER JOIN TBL_CUSTOMER      C ON C.CUSTOMER_ID = DEP.CUSTOMER_ID
        INNER JOIN TBL_AMPARE        AP ON AP.AMPARE_ID = C.AMP_ID
        INNER JOIN TBL_AREA          A ON A.AREA_ID = C.AREA_ID
        LEFT JOIN TBL_CUSTOMER_METER CM ON CM.CUSTOMER_ID = C.CUSTOMER_ID
                                           AND CM.IS_ACTIVE = 1
        LEFT JOIN TBL_BOX            B ON B.BOX_ID = CM.BOX_ID
        LEFT JOIN TBL_METER          M ON M.METER_ID = CM.METER_ID
        INNER JOIN TLKP_CURRENCY     cx ON cx.CURRENCY_ID = DEP.CURRENCY_ID
WHERE   DEPOSIT_ACTION_ID = 1
        AND DEP.IS_PAID = 0
        AND (DEPOSIT_DATE BETWEEN @START_DATE AND @END_DATE)
        AND (@CURRENCY_ID = 0 OR cx.CURRENCY_ID = @CURRENCY_ID)
        AND (@CYCLE_ID = 0 OR   C.BILLING_CYCLE_ID = @CYCLE_ID)
        AND (@AREA = 0 OR   A.AREA_ID = @AREA);
--- END OF REPORT_TRIAL_BALANCE_DEPOSIT_DETAIL
GO

IF OBJECT_ID ('REPORT_CUSTOMER_VERIFY_USAGE') IS NOT NULL
    DROP PROC dbo.REPORT_CUSTOMER_VERIFY_USAGE;
GO
CREATE PROC [dbo].[REPORT_CUSTOMER_VERIFY_USAGE]
    @MONTH    DATETIME       = '2016-12-01',
    @M1       DATETIME       = '2016-11-01',
    @M2       DATETIME       = '2016-10-01',
    @M3       DATETIME       = '2016-09-01',
    @CYCLE_ID INT            = 0,
    @AREA_ID  INT            = 0,
    @FILTER   NVARCHAR (MAX) = '  WHERE r.MONTH_TOTAL_USAGE = 0 ',
    @ORDER_BY NVARCHAR (MAX) = ' ORDER BY a.AREA_CODE, p.POLE_CODE,b.BOX_CODE,cm.POSITION_IN_BOX,c.CUSTOMER_CODE'
/*
		@2020-09-12 by Kheang Kimkhorn
			1. Exclude INVOICE_STATUS = 3
	*/
AS
SELECT  r.CUSTOMER_ID,
        MONTH_START_USAGE = ISNULL(u.START_USAGE,0),
        MONTH_END_USAGE   = ISNULL(u.END_USAGE,0),
        MONTH_TOTAL_USAGE = ISNULL(r.M0,0),
        u.IS_METER_RENEW_CYCLE,
        M1_TOTAL_USAGE    = r.M1,
        M1_PERCENTAGE     = 100.0 * COALESCE ((r.M0 - r.M1) / NULLIF(r.M1, 0), CASE WHEN r.M0 > 0 THEN 1 ELSE 0 END),
        M2_TOTAL_USAGE    = r.M2,
        M2_PERCENTAGE     = 100.0 * COALESCE ((r.M0 - r.M2) / NULLIF(r.M2, 0), CASE WHEN r.M0 > 0 THEN 1 ELSE 0 END),
        M3_TOTAL_USAGE    = r.M3,
        M3_PERCENTAGE     = 100.0 * COALESCE ((r.M0 - r.M3) / NULLIF(r.M3, 0), CASE WHEN r.M0 > 0 THEN 1 ELSE 0 END)
INTO    #RESULT
FROM(
    SELECT  x.CUSTOMER_ID,
            M0 = ISNULL (SUM (CASE WHEN USAGE_MONTH = @MONTH THEN TOTAL_USAGE ELSE 0 END), 0),
            M1 = ISNULL (SUM (CASE WHEN USAGE_MONTH = @M1 THEN TOTAL_USAGE ELSE 0 END), 0),
            M2 = ISNULL (SUM (CASE WHEN USAGE_MONTH = @M2 THEN TOTAL_USAGE ELSE 0 END), 0),
            M3 = ISNULL (SUM (CASE WHEN USAGE_MONTH = @M3 THEN TOTAL_USAGE ELSE 0 END), 0)
    FROM(
        -- CURRENT MONTH
        SELECT  c.CUSTOMER_ID,
                u.USAGE_MONTH,
                TOTAL_USAGE = u.MULTIPLIER * (CASE WHEN IS_METER_RENEW_CYCLE = 0 THEN END_USAGE - START_USAGE ELSE (
                                                                                                                   SELECT   CONVERT (INT, REPLICATE ('9', LEN (CONVERT (NVARCHAR, CONVERT (INT, START_USAGE)))))
                                                                                                                   ) - START_USAGE + END_USAGE + 1 END
                                             )
        FROM    TBL_USAGE               u
                INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = u.CUSTOMER_ID
        WHERE   c.CUSTOMER_ID = u.CUSTOMER_ID
                AND c.IS_POST_PAID = 1
                AND (@CYCLE_ID = 0 OR   c.BILLING_CYCLE_ID = @CYCLE_ID)
                AND (@AREA_ID = 0 OR c.AREA_ID = @AREA_ID)
                AND u.USAGE_MONTH = @MONTH
        UNION ALL
        -- MONTH 1
        SELECT  c.CUSTOMER_ID,
                USAGE_MONTH = i.INVOICE_MONTH,
                TOTAL_USAGE = i.TOTAL_USAGE + ISNULL (adj.ADJUST_USAGE, 0)
        FROM    TBL_INVOICE             i
                INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = i.CUSTOMER_ID
                OUTER APPLY(
                           SELECT   ADJUST_USAGE = SUM (ADJUST_USAGE)
                           FROM TBL_INVOICE_ADJUSTMENT
                           WHERE   INVOICE_ID = i.INVOICE_ID
                           )            adj
        WHERE   c.CUSTOMER_ID = i.CUSTOMER_ID
                AND c.IS_POST_PAID = 1
                AND (@CYCLE_ID = 0 OR   c.BILLING_CYCLE_ID = @CYCLE_ID)
                AND (@AREA_ID = 0 OR c.AREA_ID = @AREA_ID)
                AND i.INVOICE_MONTH = @M1
                AND i.INVOICE_STATUS NOT IN (3)
        UNION ALL
        -- MONTH 2
        SELECT  c.CUSTOMER_ID,
                USAGE_MONTH = i.INVOICE_MONTH,
                TOTAL_USAGE = i.TOTAL_USAGE + ISNULL (adj.ADJUST_USAGE, 0)
        FROM    TBL_INVOICE             i
                INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = i.CUSTOMER_ID
                OUTER APPLY(
                           SELECT   ADJUST_USAGE = SUM (ADJUST_USAGE)
                           FROM TBL_INVOICE_ADJUSTMENT
                           WHERE   INVOICE_ID = i.INVOICE_ID
                           )            adj
        WHERE   c.CUSTOMER_ID = i.CUSTOMER_ID
                AND c.IS_POST_PAID = 1
                AND (@CYCLE_ID = 0 OR   c.BILLING_CYCLE_ID = @CYCLE_ID)
                AND (@AREA_ID = 0 OR c.AREA_ID = @AREA_ID)
                AND i.INVOICE_MONTH = @M2
                AND i.INVOICE_STATUS NOT IN (3)
        UNION ALL
        -- MONTH 3
        SELECT  c.CUSTOMER_ID,
                USAGE_MONTH = i.INVOICE_MONTH,
                TOTAL_USAGE = i.TOTAL_USAGE + ISNULL (adj.ADJUST_USAGE, 0)
        FROM    TBL_INVOICE             i
                INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = i.CUSTOMER_ID
                OUTER APPLY(
                           SELECT   ADJUST_USAGE = SUM (ADJUST_USAGE)
                           FROM TBL_INVOICE_ADJUSTMENT
                           WHERE   INVOICE_ID = i.INVOICE_ID
                           )            adj
        WHERE   c.CUSTOMER_ID = i.CUSTOMER_ID
                AND c.IS_POST_PAID = 1
                AND (@CYCLE_ID = 0 OR   c.BILLING_CYCLE_ID = @CYCLE_ID)
                AND (@AREA_ID = 0 OR c.AREA_ID = @AREA_ID)
                AND i.INVOICE_MONTH = @M3
                AND i.INVOICE_STATUS NOT IN (3)
        ) x
    GROUP BY x.CUSTOMER_ID
    )            r
    OUTER APPLY(
               SELECT   TOP(1)  *
               FROM TBL_USAGE
               WHERE CUSTOMER_ID = r.CUSTOMER_ID
                     AND USAGE_MONTH = @MONTH
               ORDER BY USAGE_ID DESC
               ) u;
DECLARE @CMD NVARCHAR (MAX);
SET @CMD = N'
SELECT r.*
		,c.CUSTOMER_CODE
		,c.LAST_NAME_KH
		,c.FIRST_NAME_KH
		,AREA_NAME = ISNULL(a.AREA_NAME,''-'')
		,BOX_CODE = ISNULL(b.BOX_CODE,''-'')
		,METER_CODE=REPLACE(LTRIM(REPLACE(ISNULL(m.METER_CODE,''-''),''0'','' '')),'' '',''0'')
FROM #RESULT r
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=r.CUSTOMER_ID
INNER JOIN TBL_AREA a ON a.AREA_ID=c.AREA_ID
LEFT JOIN TBL_CUSTOMER_METER cm ON cm.CUSTOMER_ID=c.CUSTOMER_ID AND cm.IS_ACTIVE=1
LEFT JOIN TBL_POLE p ON cm.POLE_ID=p.POLE_ID
LEFT JOIN TBL_BOX b ON cm.BOX_ID=b.BOX_ID
LEFT JOIN TBL_METER m ON m.METER_ID=cm.METER_ID 
' + @FILTER + N'
' + @ORDER_BY + N'
';
EXEC sp_executesql @CMD,
                   N'@FILTER NVARCHAR(MAX),@ORDER_BY NVARCHAR(MAX)',
                   @FILTER = @FILTER,
                   @ORDER_BY = @ORDER_BY;
--END REPORT_CUSTOMER_VERIFY_USAGE

GO

IF OBJECT_ID ('REPORT_USAGE_MONTHLY') IS NOT NULL
    DROP PROC dbo.REPORT_USAGE_MONTHLY;
GO
CREATE PROC [dbo].[REPORT_USAGE_MONTHLY]
    @MONTH                       DATETIME       = '2017-03-01',
    @M1                          DATETIME       = '2017-02-01',
    @M2                          DATETIME       = '2017-01-01',
    @M3                          DATETIME       = '2016-12-01',
    @CUS_STATUS_ID               INT            = 0,
    @CUSTOMER_CONNECTION_TYPE_ID INT            = 0,
    @CYCLE_ID                    INT            = 0,
    @AREA_ID                     INT            = 0,
    @PRICE_ID                    INT            = 0,
    @FILTER                      NVARCHAR (MAX) = '  WHERE r.MONTH_TOTAL_USAGE >= 0 ',
    @ORDER_BY                    NVARCHAR (MAX) = ' ORDER BY a.AREA_CODE, p.POLE_CODE,b.BOX_CODE,cm.POSITION_IN_BOX,c.CUSTOMER_CODE',
    @CUSTOMER_GROUP_TYPE_ID      INT            = 0
AS
/*
@2016-12-31 Sieng Sotheara
	1.Fix wrong total usage compare to other reports
@2017-03-11 Sieng Sotheara
	1.Add Pre Paid usage
    2.Add filter CUS_STATUS_ID
	3.Add filter CUSTOMER_TYPE_ID
@2017-03-30 Sieng SOtheara
	1.Add filter PRICE_ID
*/
SELECT  r.CUSTOMER_ID,
        MONTH_START_USAGE = ISNULL(u.START_USAGE,0),
        MONTH_END_USAGE   = ISNULL(u.END_USAGE,0),
        MONTH_TOTAL_USAGE = ISNULL(r.M0,0),
        u.IS_METER_RENEW_CYCLE,
        M1_TOTAL_USAGE    = r.M1,
        M1_PERCENTAGE     = 100.0 * COALESCE ((r.M0 - r.M1) / NULLIF(r.M1, 0), CASE WHEN r.M0 > 0 THEN 1 ELSE 0 END),
        M2_TOTAL_USAGE    = r.M2,
        M2_PERCENTAGE     = 100.0 * COALESCE ((r.M0 - r.M2) / NULLIF(r.M2, 0), CASE WHEN r.M0 > 0 THEN 1 ELSE 0 END),
        M3_TOTAL_USAGE    = r.M3,
        M3_PERCENTAGE     = 100.0 * COALESCE ((r.M0 - r.M3) / NULLIF(r.M3, 0), CASE WHEN r.M0 > 0 THEN 1 ELSE 0 END)
INTO    #RESULT
FROM(
    SELECT  x.CUSTOMER_ID,
            M0 = ISNULL (SUM (CASE WHEN INVOICE_MONTH = @MONTH THEN TOTAL_USAGE ELSE 0 END), 0),
            M1 = ISNULL (SUM (CASE WHEN INVOICE_MONTH = @M1 THEN TOTAL_USAGE ELSE 0 END), 0),
            M2 = ISNULL (SUM (CASE WHEN INVOICE_MONTH = @M2 THEN TOTAL_USAGE ELSE 0 END), 0),
            M3 = ISNULL (SUM (CASE WHEN INVOICE_MONTH = @M3 THEN TOTAL_USAGE ELSE 0 END), 0)
    FROM(
        --POST_PAID_USAGE
        SELECT  c.CUSTOMER_ID,
                i.INVOICE_MONTH,
                TOTAL_USAGE = i.TOTAL_USAGE + ISNULL (adj.ADJUST_USAGE, 0)
        FROM    TBL_INVOICE             i
                INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = i.CUSTOMER_ID
                                             AND   c.IS_POST_PAID = 1
                OUTER APPLY(
                           SELECT   ADJUST_USAGE = SUM (ADJUST_USAGE)
                           FROM TBL_INVOICE_ADJUSTMENT
                           WHERE   INVOICE_ID = i.INVOICE_ID
                           )            adj
        WHERE   i.INVOICE_MONTH = @MONTH
                AND i.INVOICE_STATUS NOT IN (3)
        UNION ALL
        SELECT  c.CUSTOMER_ID,
                i.INVOICE_MONTH,
                TOTAL_USAGE = i.TOTAL_USAGE + ISNULL (adj.ADJUST_USAGE, 0)
        FROM    TBL_INVOICE             i
                INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = i.CUSTOMER_ID
                                             AND   c.IS_POST_PAID = 1
                OUTER APPLY(
                           SELECT   ADJUST_USAGE = SUM (ADJUST_USAGE)
                           FROM TBL_INVOICE_ADJUSTMENT
                           WHERE   INVOICE_ID = i.INVOICE_ID
                           )            adj
        WHERE   i.INVOICE_MONTH = @M1
                AND i.INVOICE_STATUS NOT IN (3)
        UNION ALL
        SELECT  c.CUSTOMER_ID,
                i.INVOICE_MONTH,
                TOTAL_USAGE = i.TOTAL_USAGE + ISNULL (adj.ADJUST_USAGE, 0)
        FROM    TBL_INVOICE             i
                INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = i.CUSTOMER_ID
                                             AND   c.IS_POST_PAID = 1
                OUTER APPLY(
                           SELECT   ADJUST_USAGE = SUM (ADJUST_USAGE)
                           FROM TBL_INVOICE_ADJUSTMENT
                           WHERE   INVOICE_ID = i.INVOICE_ID
                           )            adj
        WHERE   i.INVOICE_MONTH = @M2
                AND i.INVOICE_STATUS NOT IN (3)
        UNION ALL
        SELECT  c.CUSTOMER_ID,
                i.INVOICE_MONTH,
                TOTAL_USAGE = i.TOTAL_USAGE + ISNULL (adj.ADJUST_USAGE, 0)
        FROM    TBL_INVOICE             i
                INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = i.CUSTOMER_ID
                                             AND   c.IS_POST_PAID = 1
                OUTER APPLY(
                           SELECT   ADJUST_USAGE = SUM (ADJUST_USAGE)
                           FROM TBL_INVOICE_ADJUSTMENT
                           WHERE   INVOICE_ID = i.INVOICE_ID
                           )            adj
        WHERE   i.INVOICE_MONTH = @M3
                AND i.INVOICE_STATUS NOT IN (3)
        UNION ALL
        --PRE_PAID_USAGE
        SELECT  c.CUSTOMER_ID,
                INVOICE_MONTH = CREDIT_MONTH,
                TOTAL_USAGE   = USAGE
        FROM    TBL_PREPAID_CREDIT      pc
                INNER JOIN TBL_CUSTOMER c ON pc.CUSTOMER_ID = c.CUSTOMER_ID
                                             AND   IS_POST_PAID = 0
        WHERE   pc.CREDIT_MONTH = @MONTH
        UNION ALL
        SELECT  c.CUSTOMER_ID,
                INVOICE_MONTH = CREDIT_MONTH,
                TOTAL_USAGE   = USAGE
        FROM    TBL_PREPAID_CREDIT      pc
                INNER JOIN TBL_CUSTOMER c ON pc.CUSTOMER_ID = c.CUSTOMER_ID
                                             AND   IS_POST_PAID = 0
        WHERE   pc.CREDIT_MONTH = @M1
        UNION ALL
        SELECT  c.CUSTOMER_ID,
                INVOICE_MONTH = CREDIT_MONTH,
                TOTAL_USAGE   = USAGE
        FROM    TBL_PREPAID_CREDIT      pc
                INNER JOIN TBL_CUSTOMER c ON pc.CUSTOMER_ID = c.CUSTOMER_ID
                                             AND   IS_POST_PAID = 0
        WHERE   pc.CREDIT_MONTH = @M2
        UNION ALL
        SELECT  c.CUSTOMER_ID,
                INVOICE_MONTH = CREDIT_MONTH,
                TOTAL_USAGE   = USAGE
        FROM    TBL_PREPAID_CREDIT      pc
                INNER JOIN TBL_CUSTOMER c ON pc.CUSTOMER_ID = c.CUSTOMER_ID
                                             AND   IS_POST_PAID = 0
        WHERE   pc.CREDIT_MONTH = @M3
        ) x
    GROUP BY x.CUSTOMER_ID
    )            r
    OUTER APPLY(
               SELECT   TOP(1)  *
               FROM TBL_USAGE
               WHERE CUSTOMER_ID = r.CUSTOMER_ID
                     AND USAGE_MONTH = @MONTH
               ORDER BY USAGE_ID DESC
               ) u
    INNER JOIN TBL_CUSTOMER                      c ON r.CUSTOMER_ID = c.CUSTOMER_ID
    INNER JOIN dbo.TLKP_CUSTOMER_CONNECTION_TYPE ct ON c.CUSTOMER_CONNECTION_TYPE_ID = ct.CUSTOMER_CONNECTION_TYPE_ID
WHERE(@CYCLE_ID = 0 OR  c.BILLING_CYCLE_ID = @CYCLE_ID)
     AND (@PRICE_ID = 0 OR  c.PRICE_ID = @PRICE_ID)
     AND (@AREA_ID = 0 OR   c.AREA_ID = @AREA_ID)
     AND (
         @CUSTOMER_CONNECTION_TYPE_ID = 0
         OR c.CUSTOMER_TYPE_ID = @CUSTOMER_CONNECTION_TYPE_ID
         )
     AND (@CUS_STATUS_ID = 0 OR c.STATUS_ID = @CUS_STATUS_ID)
     AND (
         @CUSTOMER_GROUP_TYPE_ID = 0
         OR ct.NONLICENSE_CUSTOMER_GROUP_ID = @CUSTOMER_GROUP_TYPE_ID
         );
DECLARE @CMD NVARCHAR (MAX);
SET @CMD = N'
SELECT r.*
		,c.CUSTOMER_CODE
		,c.LAST_NAME_KH
		,c.FIRST_NAME_KH
		,AREA_NAME = ISNULL(a.AREA_NAME,''-'')
		,BOX_CODE = ISNULL(b.BOX_CODE,''-'')
		,METER_CODE=REPLACE(LTRIM(REPLACE(ISNULL(m.METER_CODE,''-''),''0'','' '')),'' '',''0'')
FROM #RESULT r
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=r.CUSTOMER_ID
INNER JOIN TBL_AREA a ON a.AREA_ID=c.AREA_ID
LEFT JOIN TBL_CUSTOMER_METER cm ON cm.CUSTOMER_ID=c.CUSTOMER_ID AND cm.IS_ACTIVE=1
LEFT JOIN TBL_POLE p ON cm.POLE_ID=p.POLE_ID
LEFT JOIN TBL_BOX b ON cm.BOX_ID=b.BOX_ID
LEFT JOIN TBL_METER m ON m.METER_ID=cm.METER_ID 
' + @FILTER + N'
' + @ORDER_BY + N'
';
EXEC sp_executesql @CMD,
                   N'@FILTER NVARCHAR(MAX),@ORDER_BY NVARCHAR(MAX)',
                   @FILTER = @FILTER,
                   @ORDER_BY = @ORDER_BY;
--END REPORT_USAGE_MONTHLY
GO

IF OBJECT_ID('REPORT_CASH_RECEIVE_DEPOSIT_CONNECTION_FEE')IS NOT NULL
	DROP PROC dbo.REPORT_CASH_RECEIVE_DEPOSIT_CONNECTION_FEE

GO

CREATE PROC [dbo].[REPORT_CASH_RECEIVE_DEPOSIT_CONNECTION_FEE]
	@D1 DATETIME='2012-07-01',
	@D2 DATETIME='2015-07-31',
	@ITEM_TYPE_ID INT=0,
	@ITEM_ID INT =0,
	@CURRENCY_ID INT=0,
	@AREA_ID INT=0,
	@BILLING_CYCLE_ID INT=0
AS   
SET @D1 = DATEADD(D,0,DATEDIFF(D,0,@D1));
SET @D2 = DATEADD(S,-1,DATEADD(D,1,DATEDIFF(D,0,@D2)));

SELECT  cd.CUSTOMER_ID,
		cd.CURRENCY_ID,
		DEPOSIT_AMOUNT=SUM(AMOUNT)
INTO #TMP_DEPOSIT
FROM TBL_CUS_DEPOSIT cd
WHERE cd.IS_PAID=1
	  AND (@CURRENCY_ID=0 OR cd.CURRENCY_ID=@CURRENCY_ID)
	  AND (DEPOSIT_DATE BETWEEN @D1 AND @D2)
GROUP BY cd.CUSTOMER_ID,cd.CURRENCY_ID

SELECT p.CUSTOMER_ID,
	   p.CURRENCY_ID,
	   CONNECTION_FEE_AMOUNT=SUM(pd.PAY_AMOUNT)
INTO #TMP_CONNECTION_FEE
FROM TBL_PAYMENT p 
INNER JOIN TBL_PAYMENT_DETAIL pd ON pd.PAYMENT_ID=p.PAYMENT_ID
INNER JOIN TBL_INVOICE i ON i.INVOICE_ID = pd.INVOICE_ID
WHERE	EXISTS(
			SELECT INVOICE_DETAIL_ID 
			FROM TBL_INVOICE_DETAIL
			WHERE INVOICE_ID=i.INVOICE_ID AND INVOICE_ITEM_ID = 3
		) 
		AND (@CURRENCY_ID=0 OR p.CURRENCY_ID=@CURRENCY_ID) 
		AND p.PAY_DATE BETWEEN @D1 AND @D2
GROUP BY p.CUSTOMER_ID,p.CURRENCY_ID

SELECT	cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		c.CUSTOMER_ID,
		CUSTOMER_CODE,
		CUSTOMER_NAME = c.LAST_NAME_KH+' '+ c.FIRST_NAME_KH,
		AREA_NAME=ISNULL(a.AREA_CODE,'-'),
		CUSTOMER_TYPE_NAME=ISNULL(ct.CUSTOMER_CONNECTION_TYPE_NAME,'-'),
		BOX_CODE = ISNULL(b.BOX_CODE,'-'),
		METER_CODE =  ISNULL(REPLACE(LTRIM(REPLACE(m.METER_CODE,'0',' ')),' ','0'),'-'),
		METER_NUMBER=1,	  
		PHONE = c.PHONE_1,
		AMPARE_NAME = ISNULL(AMPARE_NAME,'-'),

		DEPOSIT=ISNULL(d.DEPOSIT_AMOUNT,0),
		CONNECTION_FEE=ISNULL(f.CONNECTION_FEE_AMOUNT,0)
FROM TBL_CUSTOMER c
INNER JOIN TBL_PRICE px ON px.PRICE_ID=c.PRICE_ID
LEFT JOIN TBL_CUSTOMER_METER cm ON cm.CUSTOMER_ID = c.CUSTOMER_ID AND cm.IS_ACTIVE = 1
LEFT JOIN TBL_AREA a ON a.AREA_ID = c.AREA_ID
LEFT JOIN TBL_BOX b ON b.BOX_ID = cm.BOX_ID
LEFT JOIN TBL_METER m ON m.METER_ID = cm.METER_ID
LEFT JOIN TBL_AMPARE am ON am.AMPARE_ID = c.AMP_ID
--LEFT JOIN TBL_CUSTOMER_TYPE ct ON ct.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
LEFT JOIN TLKP_CUSTOMER_CONNECTION_TYPE ct ON ct.CUSTOMER_CONNECTION_TYPE_ID=c.CUSTOMER_CONNECTION_TYPE_ID
CROSS JOIN TLKP_CURRENCY cx 
OUTER APPLY(
	SELECT DEPOSIT_AMOUNT
	FROM #TMP_DEPOSIT
	WHERE CUSTOMER_ID=c.CUSTOMER_ID
		  AND cx.CURRENCY_ID=CURRENCY_ID
)d
OUTER APPLY(
	SELECT CONNECTION_FEE_AMOUNT
	FROM #TMP_CONNECTION_FEE
	WHERE CUSTOMER_ID=c.CUSTOMER_ID
		  AND cx.CURRENCY_ID=CURRENCY_ID 
)f
  
WHERE  (@AREA_ID=0 OR c.AREA_ID=@AREA_ID)
		AND (@BILLING_CYCLE_ID=0 OR c.BILLING_CYCLE_ID=@BILLING_CYCLE_ID)
		AND ISNULL(f.CONNECTION_FEE_AMOUNT,0)+ISNULL(d.DEPOSIT_AMOUNT,0)>0
-- END OF REPORT_CASH_RECEIVE_DEPOSIT_CONNECTION_FEE

GO

IF OBJECT_ID('REPORT_OTHER_INCOME')IS NOT NULL
	DROP PROC dbo.REPORT_OTHER_INCOME

GO

CREATE PROC [dbo].[REPORT_OTHER_INCOME]
    @MONTH DATETIME = '2012-08-01',
    @AREA_ID INT = 0,
    @ITEM_TYPE_ID INT = 0,
    @ITEM_ID INT = 0,
    @CURRENCY_ID INT = 0
AS
SET @MONTH = CONVERT(NVARCHAR(7), @MONTH, 126) + '-01';
SELECT cx.CURRENCY_ID,
       cx.CURRENCY_NAME,
       cx.CURRENCY_SING,
       c.CUSTOMER_CODE,
       CUSTOMER_NAME = c.LAST_NAME_KH + ' ' + c.FIRST_NAME_KH,
       POLE_CODE = ISNULL(p.POLE_CODE,'-'),
       BOX_CODE = ISNULL(b.BOX_CODE,'-'),
       i.INVOICE_NO,
       i.INVOICE_MONTH,
       i.INVOICE_DATE,
       t.INVOICE_ITEM_ID,
       t.INVOICE_ITEM_NAME,
       tt.INVOICE_ITEM_TYPE_ID,
       tt.INVOICE_ITEM_TYPE_NAME,
       i.SETTLE_AMOUNT,
       i.PAID_AMOUNT
FROM TBL_INVOICE i
    CROSS APPLY
(
    SELECT TOP 1
           INVOICE_ITEM_ID
    FROM TBL_INVOICE_DETAIL
    WHERE INVOICE_ID = i.INVOICE_ID
) d
    INNER JOIN TBL_INVOICE_ITEM t
        ON t.INVOICE_ITEM_ID = d.INVOICE_ITEM_ID
    INNER JOIN TBL_INVOICE_ITEM_TYPE tt
        ON tt.INVOICE_ITEM_TYPE_ID = t.INVOICE_ITEM_TYPE_ID
    INNER JOIN TBL_CUSTOMER c
        ON c.CUSTOMER_ID = i.CUSTOMER_ID
    LEFT JOIN TBL_CUSTOMER_METER cm
        ON cm.CUSTOMER_ID = c.CUSTOMER_ID
           AND cm.IS_ACTIVE = 1
    LEFT JOIN TBL_POLE p
        ON p.POLE_ID = cm.POLE_ID
    LEFT JOIN TBL_BOX b
        ON b.BOX_ID = cm.BOX_ID
    INNER JOIN TLKP_CURRENCY cx
        ON cx.CURRENCY_ID = i.CURRENCY_ID
WHERE INVOICE_MONTH = @MONTH
      AND i.INVOICE_STATUS NOT IN ( 3 )
      AND
      (
          @AREA_ID = 0
          OR c.AREA_ID = @AREA_ID
      )
      AND
      (
          @ITEM_TYPE_ID = 0
          OR t.INVOICE_ITEM_TYPE_ID = @ITEM_TYPE_ID
      )
      AND
      (
          @ITEM_ID = 0
          OR t.INVOICE_ITEM_ID = @ITEM_ID
      )
      AND
      (
          @CURRENCY_ID = 0
          OR cx.CURRENCY_ID = @CURRENCY_ID
      )
ORDER BY i.INVOICE_NO ASC
--- END OF REPORT_OTHER_INCOME


GO

IF OBJECT_ID('REPORT_BLOCK_AND_RECONNECT')IS NOT NULL
	DROP PROC REPORT_BLOCK_AND_RECONNECT
GO

CREATE PROC [dbo].[REPORT_BLOCK_AND_RECONNECT]
	@D1 DATETIME = '2019-11-01',
	@D2 DATETIME = '2019-12-30',
	@CURRENCY_ID INT = 0
AS
SET @D1 = DATEADD(D,0,DATEDIFF(D,0,@D1));
SET @D2 = DATEADD(S,-1,DATEADD(D,1,DATEDIFF(D,0,@D2)));
SELECT	CURRENCY_ID = ISNULL(cx.CURRENCY_ID,cx2.CURRENCY_ID),
		CURRENCY_NAME = ISNULL(cx.CURRENCY_NAME,cx2.CURRENCY_NAME),
		CURRENCY_SING = ISNULL(cx.CURRENCY_SING,cx2.CURRENCY_SING),
		c.CUSTOMER_ID,
		c.CUSTOMER_CODE,
		FULL_NAME=c.LAST_NAME_KH+' '+c.FIRST_NAME_KH,
		CUT_DATE=ISNULL(csc.CHNAGE_DATE,'1990-01-01'),
		RECONNECT_DATE=ISNULL(t.CHNAGE_DATE,'1990-01-01'),
		RECONNECT_FEE=r.SETTLE_AMOUNT,
		c.STATUS_ID,
		s.CUS_STATUS_NAME
FROM TBL_CUS_STATUS_CHANGE csc 
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = csc.CUSTOMER_ID
INNER JOIN TBL_PRICE p ON p.PRICE_ID = c.PRICE_ID
INNER JOIN TLKP_CUS_STATUS s ON s.CUS_STATUS_ID = c.STATUS_ID
OUTER APPLY (
	SELECT top 1 CHNAGE_DATE,iv.SETTLE_AMOUNT,iv.CURRENCY_ID
	FROM TBL_CUS_STATUS_CHANGE cs
	INNER join TBL_INVOICE iv ON cs.INVOICE_ID=iv.INVOICE_ID
	WHERE cs.CUSTOMER_ID = csc.CUSTOMER_ID
			AND cs.CHNAGE_ID>csc.CHNAGE_ID
			AND cs.OLD_STATUS_ID=3 AND cs.NEW_STATUS_ID=2
) r
OUTER APPLY (
	SELECT top 1 CHNAGE_DATE,iv.SETTLE_AMOUNT,iv.CURRENCY_ID
	FROM TBL_CUS_STATUS_CHANGE cs
	LEFT join TBL_INVOICE iv ON cs.INVOICE_ID=iv.INVOICE_ID
	WHERE cs.CUSTOMER_ID = csc.CUSTOMER_ID
			AND cs.CHNAGE_ID>csc.CHNAGE_ID
			AND cs.OLD_STATUS_ID=3 AND cs.NEW_STATUS_ID=2
) t
LEFT JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID = r.CURRENCY_ID
LEFT JOIN TLKP_CURRENCY cx2 ON cx2.CURRENCY_ID = p.CURRENCY_ID
WHERE  csc.OLD_STATUS_ID=2 AND csc.NEW_STATUS_ID=3 
AND(csc.CHNAGE_DATE BETWEEN @D1 AND @D2 OR r.CHNAGE_DATE BETWEEN @D1 AND @D2)
AND(@CURRENCY_ID=0 OR ISNULL(cx.CURRENCY_ID,0)=@CURRENCY_ID) 
-- END OF REPORT_BLOCK_AND_RECONNECT
            
GO

IF NOT EXISTS (
              SELECT    *
              FROM  INFORMATION_SCHEMA.COLUMNS
              WHERE TABLE_NAME = 'TLKP_FILTER_OPTION'
                    AND COLUMN_NAME = 'IS_SYSTEM'
              )
    ALTER TABLE TLKP_FILTER_OPTION ADD IS_SYSTEM BIT;
GO

UPDATE TLKP_FILTER_OPTION
SET IS_SYSTEM = 0;

GO

IF NOT EXISTS (SELECT   * FROM  dbo.TLKP_FILTER_OPTION WHERE FILTER_NAME = N'បំណុលតាមកាលសង្ខេប' AND FILTER_NAME = N'បំណុលតាមកាលលម្អិត' AND FILTER_NAME = N'ទម្រង់ដើម')
BEGIN
INSERT INTO dbo.TLKP_FILTER_OPTION
(
    FILTER_NAME,
    GRID_NAME,
    FILTER_OPTION,
    XML_FILE,
    IS_ACTIVE,
    CREATE_BY,
    CREATE_ON,
    IS_SYSTEM
)
SELECT V.* FROM 
(
	SELECT  FILTER_NAME =  N'បំណុលតាមកាលសង្ខេប', GRID_NAME = 'PAGEREPORTAGING', FILTER_OPTION ='',XML_FILE = 0xefbbbf3c5874726153657269616c697a65722076657273696f6e3d22312e3022206170706c69636174696f6e3d225069766f7447726964223e0d0a20203c70726f7065727479206e616d653d22234c61796f757456657273696f6e22202f3e0d0a20203c70726f7065727479206e616d653d22234c61796f75745363616c65466163746f72223e40312c57696474683d3140312c4865696768743d313c2f70726f70657274793e0d0a20203c70726f7065727479206e616d653d224669786564436f6c756d6e4669656c6456616c756573222069736b65793d2274727565222076616c75653d223022202f3e0d0a20203c70726f7065727479206e616d653d224669656c6473222069736b65793d2274727565222076616c75653d223238223e0d0a202020203c70726f7065727479206e616d653d224974656d31222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77536f7274223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77457870616e64223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77536f7274427953756d6d617279223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c435553544f4d45525f434f44453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561223e526f77417265613c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e435553544f4d45525f434f44453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e323c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241637475616c536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e34303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3135303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d32222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77536f7274223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77457870616e64223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77536f7274427953756d6d617279223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c435553544f4d45525f4e414d453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561223e526f77417265613c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e435553544f4d45525f4e414d453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f74616c735669736962696c697479223e4e6f6e653c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e333c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241637475616c536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3135303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d33222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77536f7274223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77457870616e64223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77536f7274427953756d6d617279223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c50484f4e455f4e554d4245523c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561223e526f77417265613c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e50484f4e453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f74616c735669736962696c697479223e4e6f6e653c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e323c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e343c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241637475616c536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3134303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d34222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77536f7274223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77457870616e64223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77536f7274427953756d6d617279223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c415245413c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561223e526f77417265613c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e415245415f4e414d453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e333c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e353c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241637475616c536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3132303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d35222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77536f7274223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77457870616e64223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77536f7274427953756d6d617279223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c504f4c453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561223e526f77417265613c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e504f4c455f434f44453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e343c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e363c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241637475616c536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3132303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d36222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c424f583c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561223e526f77417265613c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e424f585f434f44453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e353c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e373c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3132303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d37222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77457870616e64223e547275653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c4e4f545f4f4e5f4455455f444154453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561223e44617461417265613c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e50303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2243656c6c466f726d6174222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d6174537472696e67223e232c2323302e303023233b2d232c2323302e303023233b302e30303c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d617454797065223e4e756d657269633c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e363c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241637475616c536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e35303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3135303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d38222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77457870616e64223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e5f636f6c50313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561223e44617461417265613c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e50313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2243656c6c466f726d6174222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d6174537472696e67223e232c2323302e303023233b2d232c2323302e303023233b302e30303c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d617454797065223e4e756d657269633c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e373c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241637475616c536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3135303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d39222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c7565466f726d6174222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a202020202020202020203c70726f7065727479206e616d653d22466f726d6174537472696e67223e643c2f70726f70657274793e0d0a202020202020202020203c70726f7065727479206e616d653d22466f726d617454797065223e4461746554696d653c2f70726f70657274793e0d0a20202020202020203c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e5f636f6c50323c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561223e44617461417265613c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e50323c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2243656c6c466f726d6174222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d6174537472696e67223e232c2323302e303023233b2d232c2323302e303023233b302e30303c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d617454797065223e4e756d657269633c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e383c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e323c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241637475616c536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3135303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3130222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e5f636f6c50333c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561223e44617461417265613c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e50333c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2243656c6c466f726d6174222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d6174537472696e67223e232c2323302e303023233b2d232c2323302e303023233b302e30303c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d617454797065223e4e756d657269633c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e393c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e333c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241637475616c536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3135303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3131222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e5f636f6c50343c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561223e44617461417265613c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e50343c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2243656c6c466f726d6174222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d6174537472696e67223e232c2323302e303023233b2d232c2323302e303023233b302e30303c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d617454797065223e4e756d657269633c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e31303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e343c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241637475616c536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3135303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3132222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e5f636f6c50353c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561223e44617461417265613c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e50353c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2243656c6c466f726d6174222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d6174537472696e67223e232c2323302e303023233b2d232c2323302e303023233b302e30303c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d617454797065223e4e756d657269633c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e31313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e353c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241637475616c536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3135303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3133222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77457870616e64223e547275653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c414d4f554e543c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e534554544c455f414d4f554e543c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2243656c6c466f726d6174222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d6174537472696e67223e232c2323302e303023233b2d232c2323302e303023233b302e30303c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d617454797065223e4e756d657269633c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e31323c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e363c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3135303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3134222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77457870616e64223e547275653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c544f54414c5f4455455f414d4f554e543c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561223e44617461417265613c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e544f54414c3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2243656c6c466f726d6174222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d6174537472696e67223e232c2323302e303023233b2d232c2323302e303023233b302e30303c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d617454797065223e4e756d657269633c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e31333c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e363c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241637475616c536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3130303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3135222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77457870616e64223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c504149445f414d4f554e543c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e504149445f414d4f554e543c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2243656c6c466f726d6174222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d6174537472696e67223e232c2323302e303023233b2d232c2323302e303023233b302e30303c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d617454797065223e4e756d657269633c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e31343c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e31323c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3135303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3136222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77536f7274223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f7744726167223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77536f7274427953756d6d617279223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c43555252454e43593c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561223e526f77417265613c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e43555252454e43595f434f44453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e31353c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e39303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3137222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77457870616e64223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c494e564f4943455f4e4f3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e494e564f4943455f4e4f3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e31363c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e31303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3133303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3138222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77457870616e64223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c494e564f4943455f444154455f42494c4c494e473c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e494e564f4943455f444154453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2256616c7565466f726d6174222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d6174537472696e67223e64642d4d4d2d797979793c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d617454797065223e4461746554696d653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e31373c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e31313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3131303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3139222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77457870616e64223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c494e564f4943455f5449544c453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e494e564f4943455f5449544c453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f74616c735669736962696c697479223e4e6f6e653c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e31383c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e373c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3230303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3230222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77457870616e64223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c4d455445525f434f44453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e4d455445525f434f44453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e31393c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e333c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3130303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3231222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c5354415455533c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e5354415455533c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e393c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3130303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3232222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77457870616e64223e547275653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c4f50454e5f444159533c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e4f50454e5f444159533c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2243656c6c466f726d6174222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d6174537472696e67223e232c2323303b2d232c2323303b303c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d617454797065223e4e756d657269633c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e32313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3135303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3233222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c435553544f4d45525f434f4e4e454354494f4e5f545950453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e435553544f4d45525f434f4e4e454354494f4e5f545950453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f74616c735669736962696c697479223e4e6f6e653c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e32323c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e353c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3230303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3234222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c435553544f4d45525f47524f55503c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e435553544f4d45525f47524f55503c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e32333c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e343c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3230303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3235222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c4359434c455f4e414d453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e4359434c455f4e414d453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e32343c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e383c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3130303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3236222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c494e564f4943455f4954454d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e494e564f4943455f4954454d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e32353c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e323c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3130303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3237222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c494e564f4943455f4954454d5f545950453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e494e564f4943455f4954454d5f545950453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e32363c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3130303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3238222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f7748696465223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77536f7274223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f7746696c746572223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f7744726167223e547275653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f7744726167496e437573746f6d697a6174696f6e466f726d223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77457870616e64223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c524f575f4e4f3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561223e526f77417265613c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e524f575f4e4f3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e32373c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241637475616c536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e37303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a20203c2f70726f70657274793e0d0a20203c70726f7065727479206e616d653d2250726566696c746572222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a202020203c70726f7065727479206e616d653d224372697465726961222069736e756c6c3d227472756522202f3e0d0a202020203c70726f7065727479206e616d653d2253686f774f706572616e645479706549636f6e223e66616c73653c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d22456e61626c6564223e747275653c2f70726f70657274793e0d0a20203c2f70726f70657274793e0d0a20203c70726f7065727479206e616d653d224f7074696f6e734265686176696f72222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a202020203c70726f7065727479206e616d653d22486f72697a6f6e74616c5363726f6c6c696e67223e436f6e74726f6c3c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d22536f7274427953756d6d61727944656661756c744f72646572223e417363656e64696e673c2f70726f70657274793e0d0a20203c2f70726f70657274793e0d0a20203c70726f7065727479206e616d653d224f7074696f6e7346696c746572506f707570222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a202020203c70726f7065727479206e616d653d224175746f46696c74657254797065223e547275653c2f70726f70657274793e0d0a20203c2f70726f70657274793e0d0a20203c70726f7065727479206e616d653d224f7074696f6e73446174614669656c64222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a202020203c70726f7065727479206e616d653d2241726561496e646578223e303c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d2243617074696f6e223ee19e91e19eb6e19e89e19e91e19e98e19f92e19e9be19eb6e19e80e19f8be19e91e19eb8e19e93e19f81e19f873c2f70726f70657274793e0d0a20203c2f70726f70657274793e0d0a20203c70726f7065727479206e616d653d224f7074696f6e7348696e74222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a202020203c70726f7065727479206e616d653d2253686f7748656164657248696e7473223e66616c73653c2f70726f70657274793e0d0a20203c2f70726f70657274793e0d0a20203c70726f7065727479206e616d653d224f7074696f6e735072696e74222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a202020203c70726f7065727479206e616d653d225072696e74566572744c696e6573223e547275653c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d225072696e744461746148656164657273223e46616c73653c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d225072696e74556e7573656446696c7465724669656c6473223e66616c73653c2f70726f70657274793e0d0a20203c2f70726f70657274793e0d0a20203c70726f7065727479206e616d653d224f7074696f6e7356696577222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a202020203c70726f7065727479206e616d653d2253686f77436f6c756d6e546f74616c73223e66616c73653c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d2253686f77436f6c756d6e4772616e64546f74616c73223e66616c73653c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d2253686f77526f774772616e64546f74616c73223e66616c73653c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d2253686f77546f74616c73466f7253696e676c6556616c756573223e747275653c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d2253686f77436f6c756d6e4772616e64546f74616c486561646572223e66616c73653c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d2253686f77526f774772616e64546f74616c486561646572223e66616c73653c2f70726f70657274793e0d0a20203c2f70726f70657274793e0d0a20203c70726f7065727479206e616d653d224f7074696f6e734f4c4150222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a202020203c70726f7065727479206e616d653d22536f72744279437573746f6d4669656c6456616c7565446973706c617954657874223e747275653c2f70726f70657274793e0d0a20203c2f70726f70657274793e0d0a20203c70726f7065727479206e616d653d224f7074696f6e7346696c746572222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a202020203c70726f7065727479206e616d653d2244656661756c7446696c746572456469746f7256696577223e56697375616c416e64546578743c2f70726f70657274793e0d0a20203c2f70726f70657274793e0d0a20203c70726f7065727479206e616d653d2246697865644c696e655769647468223e323c2f70726f70657274793e0d0a20203c70726f7065727479206e616d653d2247726f757073222069736b65793d2274727565222076616c75653d223022202f3e0d0a3c2f5874726153657269616c697a65723e,IS_ACTIVE = 1, CREATE_BY =  N'ប្រព័ន្ធ​',CREATE_ON = GETDATE(),IS_SYSTEM = 1
		UNION ALL
	SELECT   FILTER_NAME =  N'បំណុលតាមកាលលម្អិត', GRID_NAME = 'PAGEREPORTAGING',FILTER_OPTION = '',XML_FILE = 0xefbbbf3c5874726153657269616c697a65722076657273696f6e3d22312e3022206170706c69636174696f6e3d225069766f7447726964223e0d0a20203c70726f7065727479206e616d653d22234c61796f757456657273696f6e22202f3e0d0a20203c70726f7065727479206e616d653d22234c61796f75745363616c65466163746f72223e40312c57696474683d3140312c4865696768743d313c2f70726f70657274793e0d0a20203c70726f7065727479206e616d653d224669786564436f6c756d6e4669656c6456616c756573222069736b65793d2274727565222076616c75653d223022202f3e0d0a20203c70726f7065727479206e616d653d224669656c6473222069736b65793d2274727565222076616c75653d223238223e0d0a202020203c70726f7065727479206e616d653d224974656d31222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77536f7274223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77457870616e64223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77536f7274427953756d6d617279223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c435553544f4d45525f434f44453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561223e526f77417265613c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e435553544f4d45525f434f44453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e323c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241637475616c536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e34303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3135303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d32222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77536f7274223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77457870616e64223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77536f7274427953756d6d617279223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c435553544f4d45525f4e414d453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561223e526f77417265613c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e435553544f4d45525f4e414d453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f74616c735669736962696c697479223e4e6f6e653c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e333c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241637475616c536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3135303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d33222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77536f7274223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77457870616e64223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77536f7274427953756d6d617279223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c50484f4e455f4e554d4245523c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e50484f4e453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f74616c735669736962696c697479223e4e6f6e653c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e323c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e393c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241637475616c536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3134303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d34222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77536f7274223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77457870616e64223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77536f7274427953756d6d617279223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c415245413c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e415245415f4e414d453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e333c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e373c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241637475616c536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3132303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d35222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77536f7274223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77457870616e64223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77536f7274427953756d6d617279223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c504f4c453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e504f4c455f434f44453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e343c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e383c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241637475616c536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3132303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d36222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c424f583c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561223e526f77417265613c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e424f585f434f44453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e353c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e343c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3132303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d37222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77457870616e64223e547275653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c4e4f545f4f4e5f4455455f444154453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e50303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2243656c6c466f726d6174222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d6174537472696e67223e232c2323302e303023233b2d232c2323302e303023233b302e30303c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d617454797065223e4e756d657269633c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e363c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e31313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241637475616c536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e35303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3135303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d38222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77457870616e64223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e5f636f6c50313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e50313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2243656c6c466f726d6174222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d6174537472696e67223e232c2323302e303023233b2d232c2323302e303023233b302e30303c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d617454797065223e4e756d657269633c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e373c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e31323c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241637475616c536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3135303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d39222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c7565466f726d6174222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a202020202020202020203c70726f7065727479206e616d653d22466f726d6174537472696e67223e643c2f70726f70657274793e0d0a202020202020202020203c70726f7065727479206e616d653d22466f726d617454797065223e4461746554696d653c2f70726f70657274793e0d0a20202020202020203c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e5f636f6c50323c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e50323c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2243656c6c466f726d6174222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d6174537472696e67223e232c2323302e303023233b2d232c2323302e303023233b302e30303c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d617454797065223e4e756d657269633c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e383c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e31333c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241637475616c536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3135303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3130222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e5f636f6c50333c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e50333c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2243656c6c466f726d6174222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d6174537472696e67223e232c2323302e303023233b2d232c2323302e303023233b302e30303c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d617454797065223e4e756d657269633c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e393c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e31343c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241637475616c536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3135303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3131222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e5f636f6c50343c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e50343c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2243656c6c466f726d6174222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d6174537472696e67223e232c2323302e303023233b2d232c2323302e303023233b302e30303c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d617454797065223e4e756d657269633c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e31303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e31353c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241637475616c536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3135303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3132222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e5f636f6c50353c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e50353c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2243656c6c466f726d6174222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d6174537472696e67223e232c2323302e303023233b2d232c2323302e303023233b302e30303c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d617454797065223e4e756d657269633c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e31313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e31363c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241637475616c536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3135303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3133222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77457870616e64223e547275653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c414d4f554e543c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561223e44617461417265613c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e534554544c455f414d4f554e543c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2243656c6c466f726d6174222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d6174537472696e67223e232c2323302e303023233b2d232c2323302e303023233b302e30303c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d617454797065223e4e756d657269633c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e31323c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3135303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3134222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77457870616e64223e547275653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c544f54414c5f4455455f414d4f554e543c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561223e44617461417265613c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e544f54414c3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2243656c6c466f726d6174222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d6174537472696e67223e232c2323302e303023233b2d232c2323302e303023233b302e30303c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d617454797065223e4e756d657269633c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e31333c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e333c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241637475616c536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3130303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3135222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77457870616e64223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c504149445f414d4f554e543c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561223e44617461417265613c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e504149445f414d4f554e543c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2243656c6c466f726d6174222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d6174537472696e67223e232c2323302e303023233b2d232c2323302e303023233b302e30303c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d617454797065223e4e756d657269633c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e31343c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e323c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3135303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3136222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77536f7274223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f7744726167223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77536f7274427953756d6d617279223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c43555252454e43593c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561223e526f77417265613c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e43555252454e43595f434f44453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e31353c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e39303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3137222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77457870616e64223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c494e564f4943455f4e4f3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e494e564f4943455f4e4f3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e31363c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e31303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3133303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3138222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77457870616e64223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c494e564f4943455f444154455f42494c4c494e473c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561223e526f77417265613c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e494e564f4943455f444154453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2256616c7565466f726d6174222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d6174537472696e67223e64642d4d4d2d797979793c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d617454797065223e4461746554696d653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e31373c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e363c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3133363c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3139222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77457870616e64223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c494e564f4943455f5449544c453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561223e526f77417265613c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e494e564f4943455f5449544c453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f74616c735669736962696c697479223e4e6f6e653c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e31383c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e353c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3230303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3230222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77457870616e64223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c4d455445525f434f44453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e4d455445525f434f44453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e31393c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e323c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3130303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3231222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c5354415455533c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e5354415455533c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e363c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3130303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3232222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77457870616e64223e547275653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c4f50454e5f444159533c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561223e44617461417265613c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e4f50454e5f444159533c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2243656c6c466f726d6174222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d6174537472696e67223e232c2323303b2d232c2323303b303c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22466f726d617454797065223e4e756d657269633c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e32313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3135303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3233222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c435553544f4d45525f434f4e4e454354494f4e5f545950453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e435553544f4d45525f434f4e4e454354494f4e5f545950453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f74616c735669736962696c697479223e4e6f6e653c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e32323c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e343c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3230303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3234222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c435553544f4d45525f47524f55503c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e435553544f4d45525f47524f55503c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e32333c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e333c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3230303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3235222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c4359434c455f4e414d453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e4359434c455f4e414d453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e32343c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e353c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3130303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3236222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c494e564f4943455f4954454d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e494e564f4943455f4954454d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e32353c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3130303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3237222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c494e564f4943455f4954454d5f545950453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e494e564f4943455f4954454d5f545950453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e32363c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3130303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3238222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7074696f6e73222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f7748696465223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77536f7274223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f7746696c746572223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f7744726167223e547275653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f7744726167496e437573746f6d697a6174696f6e466f726d223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d22416c6c6f77457870616e64223e46616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f774772616e64546f74616c223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77546f74616c73223e66616c73653c2f70726f70657274793e0d0a20202020202020203c70726f7065727479206e616d653d2253686f77437573746f6d546f74616c73223e66616c73653c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496d616765496e646578223e2d313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22546f6f6c54697073222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a20202020202020203c70726f7065727479206e616d653d224865616465725465787422202f3e0d0a20202020202020203c70726f7065727479206e616d653d2256616c75655465787422202f3e0d0a2020202020203c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c64456469744e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c524f575f4e4f3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561223e526f77417265613c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e524f575f4e4f5f313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22496e646578223e32373c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241726561496e646578223e313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2241637475616c536f72744d6f6465223e437573746f6d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e37303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a20203c2f70726f70657274793e0d0a20203c70726f7065727479206e616d653d2250726566696c746572222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a202020203c70726f7065727479206e616d653d224372697465726961222069736e756c6c3d227472756522202f3e0d0a202020203c70726f7065727479206e616d653d2253686f774f706572616e645479706549636f6e223e66616c73653c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d22456e61626c6564223e747275653c2f70726f70657274793e0d0a20203c2f70726f70657274793e0d0a20203c70726f7065727479206e616d653d224f7074696f6e734265686176696f72222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a202020203c70726f7065727479206e616d653d22486f72697a6f6e74616c5363726f6c6c696e67223e436f6e74726f6c3c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d22536f7274427953756d6d61727944656661756c744f72646572223e417363656e64696e673c2f70726f70657274793e0d0a20203c2f70726f70657274793e0d0a20203c70726f7065727479206e616d653d224f7074696f6e7346696c746572506f707570222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a202020203c70726f7065727479206e616d653d224175746f46696c74657254797065223e547275653c2f70726f70657274793e0d0a20203c2f70726f70657274793e0d0a20203c70726f7065727479206e616d653d224f7074696f6e73446174614669656c64222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a202020203c70726f7065727479206e616d653d2241726561496e646578223e303c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d2243617074696f6e223ee19e91e19eb6e19e89e19e91e19e98e19f92e19e9be19eb6e19e80e19f8be19e91e19eb8e19e93e19f81e19f873c2f70726f70657274793e0d0a20203c2f70726f70657274793e0d0a20203c70726f7065727479206e616d653d224f7074696f6e7348696e74222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a202020203c70726f7065727479206e616d653d2253686f7748656164657248696e7473223e66616c73653c2f70726f70657274793e0d0a20203c2f70726f70657274793e0d0a20203c70726f7065727479206e616d653d224f7074696f6e735072696e74222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a202020203c70726f7065727479206e616d653d225072696e74566572744c696e6573223e547275653c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d225072696e744461746148656164657273223e46616c73653c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d225072696e74556e7573656446696c7465724669656c6473223e66616c73653c2f70726f70657274793e0d0a20203c2f70726f70657274793e0d0a20203c70726f7065727479206e616d653d224f7074696f6e7356696577222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a202020203c70726f7065727479206e616d653d2253686f77436f6c756d6e546f74616c73223e66616c73653c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d2253686f77436f6c756d6e4772616e64546f74616c73223e66616c73653c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d2253686f77526f774772616e64546f74616c73223e66616c73653c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d2253686f77546f74616c73466f7253696e676c6556616c756573223e747275653c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d2253686f77436f6c756d6e4772616e64546f74616c486561646572223e66616c73653c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d2253686f77526f774772616e64546f74616c486561646572223e66616c73653c2f70726f70657274793e0d0a20203c2f70726f70657274793e0d0a20203c70726f7065727479206e616d653d224f7074696f6e734f4c4150222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a202020203c70726f7065727479206e616d653d22536f72744279437573746f6d4669656c6456616c7565446973706c617954657874223e747275653c2f70726f70657274793e0d0a20203c2f70726f70657274793e0d0a20203c70726f7065727479206e616d653d224f7074696f6e7346696c746572222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a202020203c70726f7065727479206e616d653d2244656661756c7446696c746572456469746f7256696577223e56697375616c416e64546578743c2f70726f70657274793e0d0a20203c2f70726f70657274793e0d0a20203c70726f7065727479206e616d653d2246697865644c696e655769647468223e323c2f70726f70657274793e0d0a20203c70726f7065727479206e616d653d2247726f757073222069736b65793d2274727565222076616c75653d223022202f3e0d0a3c2f5874726153657269616c697a65723e, IS_ACTIVE = 1,CREATE_BY = N'ប្រព័ន្ធ​',CREATE_ON= GETDATE() ,IS_SYSTEM = 1
	    UNION ALL
	SELECT   FILTER_NAME =  N'ទម្រង់ដើម', GRID_NAME = 'PAGECUSTOMERBLOCK',FILTER_OPTION = '',XML_FILE = 0xefbbbf3c5874726153657269616c697a65722076657273696f6e3d22312e3022206170706c69636174696f6e3d2256696577223e0d0a20203c70726f7065727479206e616d653d22234c61796f757456657273696f6e22202f3e0d0a20203c70726f7065727479206e616d653d22234c61796f75745363616c65466163746f72223e40312c57696474683d3140312c4865696768743d313c2f70726f70657274793e0d0a20203c70726f7065727479206e616d653d224f7074696f6e7356696577222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a202020203c70726f7065727479206e616d653d22426573744669744d6f6465223e46756c6c3c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d2247726f7570466f6f74657253686f774d6f6465223e48696464656e3c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d22436f6c756d6e4865616465724175746f486569676874223e547275653c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d22526f774175746f486569676874223e747275653c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d2253686f7747726f757050616e656c223e66616c73653c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d2253686f77566572746963616c4c696e6573223e46616c73653c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d2253686f77496e64696361746f72223e66616c73653c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d22456e61626c65417070656172616e63654576656e526f77223e747275653c2f70726f70657274793e0d0a20203c2f70726f70657274793e0d0a20203c70726f7065727479206e616d653d2241637469766546696c746572456e61626c6564223e747275653c2f70726f70657274793e0d0a20203c70726f7065727479206e616d653d22436f6c756d6e73222069736b65793d2274727565222076616c75653d223139223e0d0a202020203c70726f7065727479206e616d653d224974656d31222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c435553544f4d45525f434f44453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2256697369626c65496e646578223e323c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2256697369626c65223e747275653c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3131363c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e35303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d61785769647468223e303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2253756d6d617279222069736b65793d2274727565222076616c75653d2231223e0d0a20202020202020203c70726f7065727479206e616d653d224974656d31222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a202020202020202020203c70726f7065727479206e616d653d22546167222069736e756c6c3d227472756522202f3e0d0a202020202020202020203c70726f7065727479206e616d653d224669656c644e616d65223e435553544f4d45525f434f44453c2f70726f70657274793e0d0a202020202020202020203c70726f7065727479206e616d653d2253756d6d61727954797065223e436f756e743c2f70726f70657274793e0d0a202020202020202020203c70726f7065727479206e616d653d224d6f6465223e416c6c526f77733c2f70726f70657274793e0d0a202020202020202020203c70726f7065727479206e616d653d22446973706c6179466f726d6174223e7b307d3c2f70726f70657274793e0d0a20202020202020203c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d32222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c435553544f4d45525f4e414d453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2256697369626c65496e646578223e333c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2256697369626c65223e747275653c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3130363c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d61785769647468223e303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d33222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c4d455445525f434f44453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2256697369626c65496e646578223e373c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2256697369626c65223e747275653c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3130303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e3130303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d61785769647468223e303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d34222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c435553544f4d45525f47524f55503c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e39313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d61785769647468223e303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d35222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c435553544f4d45525f434f4e4e454354494f4e5f545950453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e39313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d61785769647468223e303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d36222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c415245413c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2256697369626c65496e646578223e313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2256697369626c65223e747275653c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e39393c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d61785769647468223e303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d37222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c4359434c455f4e414d453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e38373c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d61785769647468223e303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d38222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c4455455f444154453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2256697369626c65496e646578223e383c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2256697369626c65223e747275653c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e36373c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d61785769647468223e303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d39222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c4441593c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2256697369626c65496e646578223e393c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2256697369626c65223e747275653c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e37343c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d61785769647468223e303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3130222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c5354415455533c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3132303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d61785769647468223e303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3131222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c4455455f414d4f554e543c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2256697369626c65496e646578223e31303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2256697369626c65223e747275653c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e3138313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e3130303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d61785769647468223e303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2253756d6d617279222069736b65793d2274727565222076616c75653d2231223e0d0a20202020202020203c70726f7065727479206e616d653d224974656d31222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a202020202020202020203c70726f7065727479206e616d653d225461672220747970653d2253797374656d2e537472696e6722202f3e0d0a202020202020202020203c70726f7065727479206e616d653d224669656c644e616d65223e4455455f414d4f554e543c2f70726f70657274793e0d0a202020202020202020203c70726f7065727479206e616d653d2253756d6d61727954797065223e53756d3c2f70726f70657274793e0d0a202020202020202020203c70726f7065727479206e616d653d224d6f6465223e416c6c526f77733c2f70726f70657274793e0d0a202020202020202020203c70726f7065727479206e616d653d22446973706c6179466f726d6174223e53554d3d7b303a232c2323302e232323237d3c2f70726f70657274793e0d0a20202020202020203c2f70726f70657274793e0d0a2020202020203c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3132222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c43555252454e43593c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2256697369626c65496e646578223e313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2256697369626c65223e747275653c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e35303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e35303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d61785769647468223e303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3133222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c43555252454e43595f5349474e3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2256697369626c65496e646578223e31313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2256697369626c65223e747275653c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e31353c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d61785769647468223e32303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3134222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c504f4c453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2256697369626c65496e646578223e353c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2256697369626c65223e747275653c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e37353c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d61785769647468223e303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3135222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c424f583c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2256697369626c65496e646578223e363c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2256697369626c65223e747275653c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e37353c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d61785769647468223e303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3136222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c50484f4e453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2256697369626c65496e646578223e343c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2256697369626c65223e747275653c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e37353c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d61785769647468223e303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3137222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c50454e414c54593c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e37353c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d61785769647468223e303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3138222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c524f575f4e4f3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2256697369626c65496e646578223e313c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2256697369626c65223e747275653c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2246696c7465724d6f6465223e446973706c6179546578743c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e34353c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e34353c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d61785769647468223e34353c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d3139222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224e616d65223e636f6c466f726d617444696769743c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d225769647468223e37353c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d696e5769647468223e32303c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d224d61785769647468223e303c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a20203c2f70726f70657274793e0d0a20203c70726f7065727479206e616d653d2247726f757053756d6d617279222069736b65793d2274727565222076616c75653d2232223e0d0a202020203c70726f7065727479206e616d653d224974656d31222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224d6f6465223e416c6c526f77733c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2253686f77496e47726f7570436f6c756d6e466f6f7465724e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d225461672220747970653d2253797374656d2e537472696e6722202f3e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e435553544f4d45525f434f44453c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2253756d6d61727954797065223e436f756e743c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22446973706c6179466f726d6174223ee19e85e19f86e19e93e19ebde19e93e19ea2e19e8fe19eb7e19e90e19eb7e19e87e19e93e19e9fe19e9ae19ebbe19e943a207b307d3c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d32222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224d6f6465223e416c6c526f77733c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2253686f77496e47726f7570436f6c756d6e466f6f7465724e616d6522202f3e0d0a2020202020203c70726f7065727479206e616d653d225461672220747970653d2253797374656d2e537472696e6722202f3e0d0a2020202020203c70726f7065727479206e616d653d224669656c644e616d65223e4455455f414d4f554e543c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d2253756d6d61727954797065223e53756d3c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22446973706c6179466f726d6174223ee19e94e19f92e19e9ae19eb6e19e80e19f8be19e87e19f86e19e96e19eb6e19e80e19f8be19e9fe19e9ae19ebbe19e943a207b303a232c2323302e30307d3c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a20203c2f70726f70657274793e0d0a20203c70726f7065727479206e616d653d2241637469766546696c746572537472696e6722202f3e0d0a20203c70726f7065727479206e616d653d22536f7274496e666f5374617465222069736b65793d2274727565222076616c75653d2232223e0d0a202020203c70726f7065727479206e616d653d224974656d31222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7264657273223e44657363656e64696e673c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22436f6c756d6e73223e636f6c43555252454e43593c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d224974656d32222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d224f7264657273223e417363656e64696e673c2f70726f70657274793e0d0a2020202020203c70726f7065727479206e616d653d22436f6c756d6e73223e636f6c415245413c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a202020203c70726f7065727479206e616d653d22436f6e74656e74222069736e756c6c3d2274727565222069736b65793d2274727565223e0d0a2020202020203c70726f7065727479206e616d653d2247726f7570436f756e74223e323c2f70726f70657274793e0d0a202020203c2f70726f70657274793e0d0a20203c2f70726f70657274793e0d0a20203c70726f7065727479206e616d653d2247726f757053756d6d617279536f7274496e666f537461746522202f3e0d0a20203c70726f7065727479206e616d653d2246696e6446696c7465725465787422202f3e0d0a20203c70726f7065727479206e616d653d2246696e6450616e656c56697369626c65223e66616c73653c2f70726f70657274793e0d0a3c2f5874726153657269616c697a65723e, IS_ACTIVE = 1,CREATE_BY = N'ប្រព័ន្ធ​',CREATE_ON= GETDATE() ,IS_SYSTEM = 1
) AS V 
LEFT JOIN dbo.TLKP_FILTER_OPTION F ON F.GRID_NAME = V.GRID_NAME AND F.FILTER_NAME = V.FILTER_NAME   AND F.IS_ACTIVE = 1
WHERE F.FILTER_NAME IS NULL 

END

GO

UPDATE dbo.TLKP_FILTER_OPTION
SET IS_SYSTEM = 1
WHERE FILTER_ID IN (4,3,2,1) OR FILTER_NAME IN (N'បំណុលតាមកាលសង្ខេប',N'បំណុលតាមកាលលម្អិត',N'ទម្រង់ដើម')

GO

IF OBJECT_ID('REPORT_REF_02')IS NOT NULL
	DROP PROC REPORT_REF_02

GO

CREATE PROC [dbo].[REPORT_REF_02]
	@DATA_MONTH DATETIME ='2020-07-01'
AS  

DECLARE @LICENSE_TYPE_ID INT
,@BASED_TARIFF DECIMAL(18,5)
,@SUBSIDY_TARIFF DECIMAL(18,5)

SELECT @LICENSE_TYPE_ID=LICENSE_TYPE_ID
,@BASED_TARIFF=BASED_TARIFF
,@SUBSIDY_TARIFF=SUBSIDY_TARIFF
FROM dbo.TBL_TARIFF
WHERE DATA_MONTH=@DATA_MONTH

SELECT ID
	  ,DATA_MONTH
	  ,TYPE_ID
	  ,TYPE_NAME
	  ,TYPE_OF_SALE_ID
	  ,TYPE_OF_SALE_NAME
	  ,TOTAL_CUSTOMER = ISNULL(TOTAL_CUSTOMER,0)
      ,TOTAL_POWER_SOLD = ISNULL(TOTAL_POWER_SOLD,0)
	  ,RATE
      ,CURRENCY_ID
      ,CURRENCY_NAME
      ,CURRENCY_SIGN
      ,IS_ACTIVE
	  ,LICENSE_TYPE_ID=@LICENSE_TYPE_ID
	  ,SUBSIDY_RATE=@BASED_TARIFF-@SUBSIDY_TARIFF
FROM TBL_REF_02 
WHERE DATA_MONTH=@DATA_MONTH
--END REPORT_REF_02

GO

IF OBJECT_ID('REPORT_AGING_NEW')IS NOT NULL
	DROP PROC REPORT_AGING_NEW

GO

CREATE PROC [dbo].[REPORT_AGING_NEW]
    @DATE DATETIME = '2022-07-07',
    @MONTH DATETIME = '1900-01-01'
AS
--START REPORT_AGING_DETAIL
SET @MONTH = CONVERT(NVARCHAR(7), @MONTH, 126) + '-01'
-- DEFAULT DATETIME FOR NULL.
IF @MONTH = '1900-01-01'
    SET @MONTH = NULL

DECLARE @Year AS INT = YEAR(@DATE);
DECLARE @_Month AS INT = MONTH(@DATE);
DECLARE @Day AS INT = DAY(@DATE);

SET @DATE =CAST( CAST(@Year AS NVARCHAR(20)) + '-' + CAST(@_Month  AS NVARCHAR(20))+ '-' +  CAST(@Day AS NVARCHAR(20)) + ' 23:59:59' AS DATETIME);

--old adjustment by date 
SELECT adj.INVOICE_ID,
       TOAL_ADJUSTMENT = SUM(adj.ADJUST_AMOUNT)
INTO #OLD_INVOICE_ADJUSTMENT_BY_DATE
FROM dbo.TBL_INVOICE_ADJUSTMENT adj
WHERE adj.CREATE_ON <= @DATE
GROUP BY adj.INVOICE_ID

SELECT INVOICE_ID = i.INVOICE_ID,
       i.INVOICE_NO,
       AMOUNT = SUM(d.AMOUNT)
INTO #INVOICE_AMOUNT
FROM dbo.TBL_INVOICE_DETAIL d
    INNER JOIN dbo.TBL_INVOICE i
        ON i.INVOICE_ID = d.INVOICE_ID
WHERE d.INVOICE_ITEM_ID != -1
      AND i.INVOICE_STATUS NOT IN ( 3 )
      AND d.TRAN_DATE <= @DATE

GROUP BY i.INVOICE_ID,
         i.INVOICE_NO

SELECT INVOICE_ID = i.INVOICE_ID,
       i.INVOICE_NO,
       AMOUNT = d.AMOUNT + ISNULL(fadj.TOAL_ADJUSTMENT, 0)
INTO #invs
FROM #INVOICE_AMOUNT d
    INNER JOIN dbo.TBL_INVOICE i
        ON i.INVOICE_ID = d.INVOICE_ID
    LEFT JOIN #OLD_INVOICE_ADJUSTMENT_BY_DATE fadj
        ON fadj.INVOICE_ID = i.INVOICE_ID

--INVOCIE_ITEM
SELECT ROWNUMBER = ROW_NUMBER() OVER (PARTITION BY INV.INVOICE_ID ORDER BY INVD.INVOICE_DETAIL_ID),
       INV.INVOICE_ID,
       INVT.INVOICE_ITEM_NAME,
       invType.INVOICE_ITEM_TYPE_NAME
INTO #TMP_INVOICE_ITEM_DETAIL
FROM TBL_INVOICE INV
    INNER JOIN TBL_INVOICE_DETAIL INVD
        ON INV.INVOICE_ID = INVD.INVOICE_ID
    INNER JOIN TBL_INVOICE_ITEM INVT
        ON INVT.INVOICE_ITEM_ID = INVD.INVOICE_ITEM_ID
    LEFT JOIN dbo.TBL_INVOICE_ITEM_TYPE invType
        ON invType.INVOICE_ITEM_TYPE_ID = INVT.INVOICE_ITEM_TYPE_ID
WHERE INV.INVOICE_DATE < @DATE

--Payment 
SELECT INVOICE_ID = dd.INVOICE_ID,
       PAID_AMOUNT = SUM(dd.PAY_AMOUNT)
INTO #Payment
FROM TBL_PAYMENT dp
    INNER JOIN TBL_PAYMENT_DETAIL dd
        ON dd.PAYMENT_ID = dp.PAYMENT_ID
WHERE dp.PAY_DATE <= @DATE

GROUP BY dd.INVOICE_ID

SELECT cx.DIGITS_AFTER_DECIMAL,
       cx.CURRENCY_ID,
       cx.CURRENCY_NAME,
       cx.CURRENCY_SING,
       cx.CURRENCY_CODE,
       i.INVOICE_ID,
       c.CUSTOMER_CODE,
	   METER_CODE = ISNULL(m.METER_CODE,''),
       CUSTOMER_NAME = c.LAST_NAME_KH + ' ' + c.FIRST_NAME_KH,
       c.STATUS_ID,
       r.AREA_NAME,
       p.POLE_CODE,
       b.BOX_CODE,
       CUSTOMER_GROUP = g.CUSTOMER_GROUP_NAME,
       CUSTOMER_CONNECTION_TYPE = ct.CUSTOMER_CONNECTION_TYPE_NAME,
       i.INVOICE_NO,
       i.INVOICE_DATE,
       i.INVOICE_MONTH,
       i.INVOICE_TITLE,
       --SETTLE_AMOUNT= CASE WHEN ISNULL(f.FORWARD_AMOUNT,0) =0 THEN  i.SETTLE_AMOUNT ELSE i.TOTAL_AMOUNT END  - ISNULL(old_adj.TOAL_ADJUSTMENT,0) + ISNULL(old_adj_by_date.TOAL_ADJUSTMENT,0),
       SETTLE_AMOUNT = invoice_amount.AMOUNT,
       PAID_AMOUNT = ISNULL(pay.PAID_AMOUNT, 0),
       OPEN_DAYS = DATEDIFF(D, INVOICE_DATE, @DATE),
       DUE_AMOUNT = ISNULL(invoice_amount.AMOUNT, 0) - ISNULL(pay.PAID_AMOUNT, 0),
       STATUS = cs.CUS_STATUS_NAME,
       bc.CYCLE_NAME,
       PHONE = c.PHONE_1,
       INVOICE_ITEM_TYPE = ISNULL(id.INVOICE_ITEM_TYPE_NAME, N'ថ្លៃអគ្គិសនី'),
       INVOICE_ITEM = id.INVOICE_ITEM_NAME
INTO #AgeDebt
FROM #invs invoice_amount
    INNER JOIN TBL_INVOICE i ON i.INVOICE_ID = invoice_amount.INVOICE_ID
    LEFT JOIN #Payment pay ON i.INVOICE_ID = pay.INVOICE_ID
    INNER JOIN TBL_CUSTOMER c ON i.CUSTOMER_ID = c.CUSTOMER_ID
    INNER JOIN dbo.TLKP_CUSTOMER_CONNECTION_TYPE ct ON c.CUSTOMER_CONNECTION_TYPE_ID = ct.CUSTOMER_CONNECTION_TYPE_ID
    INNER JOIN dbo.TLKP_CUSTOMER_GROUP g ON g.CUSTOMER_GROUP_ID = ct.NONLICENSE_CUSTOMER_GROUP_ID
    INNER JOIN TBL_AREA r ON r.AREA_ID = c.AREA_ID
    LEFT JOIN TBL_CUSTOMER_METER cm ON cm.CUSTOMER_ID = c.CUSTOMER_ID AND cm.IS_ACTIVE = 1
	LEFT JOIN dbo.TBL_METER  m ON m.METER_ID = cm.METER_ID
    LEFT JOIN TBL_BOX b ON b.BOX_ID = cm.BOX_ID
    LEFT JOIN TBL_POLE p ON p.POLE_ID = b.POLE_ID
    INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID = i.CURRENCY_ID
    INNER JOIN dbo.TLKP_CUS_STATUS cs ON cs.CUS_STATUS_ID = c.STATUS_ID
    INNER JOIN dbo.TBL_BILLING_CYCLE bc ON bc.CYCLE_ID = c.BILLING_CYCLE_ID
    LEFT JOIN #TMP_INVOICE_ITEM_DETAIL id ON id.INVOICE_ID = i.INVOICE_ID AND id.ROWNUMBER = 1 AND (@MONTH IS NULL OR INVOICE_MONTH = @MONTH)
           --  AND (invoice_amount.AMOUNT -  ISNULL(pay.PAID_AMOUNT, 0)) >0
           
SELECT cx.CURRENCY_ID,
       cx.CURRENCY_NAME,
       cx.CURRENCY_SING,
       cx.CURRENCY_CODE,
       INVOICE_ID = ISNULL(i.INVOICE_ID, 0),
       c.CUSTOMER_CODE,
	   METER_CODE = ISNULL(m.METER_CODE,''),
       CUSTOMER_NAME = c.LAST_NAME_KH + ' ' + c.FIRST_NAME_KH,
       c.STATUS_ID,
       r.AREA_NAME,
       p.POLE_CODE,
       b.BOX_CODE,
       CUSTOMER_GROUP = g.CUSTOMER_GROUP_NAME,
       CUSTOMER_CONNECTION_TYPE = ct.CUSTOMER_CONNECTION_TYPE_NAME,
       INVOICE_NO = c.CUSTOMER_CODE,
       i.INVOICE_DATE,
       i.INVOICE_MONTH,
       INVOICE_TITLE = N'ប្រាក់សល់ពីធនាគារ',
       -- SETTLE AMOUTN AS OF @DATE
       SETTLE_AMOUNT = 0,
       -- SETTLE AMOUTN AS OF @DATE
       PAID_AMOUNT = bpd.PAY_AMOUNT - bpd.PAID_AMOUNT + ISNULL(pd.PAY_AMOUNT, 0),
       OPEN_DAYS = ISNULL(DATEDIFF(D, INVOICE_DATE, @DATE), 0),
       STATUS = cs.CUS_STATUS_NAME,
       bc.CYCLE_NAME,
       PHONE = c.PHONE_1,
       INVOICE_ITEM_TYPE = ISNULL(id.INVOICE_ITEM_TYPE_NAME, N'ថ្លៃអគ្គិសនី'),
       INVOICE_ITEM = id.INVOICE_ITEM_NAME
INTO #BankTransaction
FROM dbo.TBL_BANK_PAYMENT_DETAIL bpd
    LEFT JOIN dbo.TBL_PAYMENT pa ON pa.BANK_PAYMENT_DETAIL_ID = bpd.BANK_PAYMENT_DETAIL_ID
    LEFT JOIN dbo.TBL_PAYMENT_DETAIL pd ON pd.PAYMENT_ID = pa.PAYMENT_ID
    LEFT JOIN dbo.TBL_INVOICE i ON i.INVOICE_ID = pd.INVOICE_ID
    INNER JOIN TBL_CUSTOMER c ON bpd.CUSTOMER_ID = c.CUSTOMER_ID
    INNER JOIN dbo.TLKP_CUSTOMER_CONNECTION_TYPE ct ON c.CUSTOMER_CONNECTION_TYPE_ID = ct.CUSTOMER_CONNECTION_TYPE_ID
    INNER JOIN dbo.TLKP_CUSTOMER_GROUP g ON g.CUSTOMER_GROUP_ID = ct.NONLICENSE_CUSTOMER_GROUP_ID
    INNER JOIN TBL_AREA r ON r.AREA_ID = c.AREA_ID
    LEFT JOIN TBL_CUSTOMER_METER cm ON cm.CUSTOMER_ID = c.CUSTOMER_ID AND cm.IS_ACTIVE = 1
	LEFT JOIN dbo.TBL_METER m ON m.METER_ID = cm.METER_ID
    LEFT JOIN TBL_BOX b ON b.BOX_ID = cm.BOX_ID
    LEFT JOIN TBL_POLE p ON p.POLE_ID = b.POLE_ID
    INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID = bpd.CURRENCY_ID
    INNER JOIN dbo.TLKP_CUS_STATUS cs ON cs.CUS_STATUS_ID = c.STATUS_ID
    INNER JOIN dbo.TBL_BILLING_CYCLE bc ON bc.CYCLE_ID = c.BILLING_CYCLE_ID
    LEFT JOIN #TMP_INVOICE_ITEM_DETAIL id ON id.INVOICE_ID = i.INVOICE_ID AND id.ROWNUMBER = 1
WHERE (i.INVOICE_DATE IS NULL OR i.INVOICE_DATE > @DATE)
      AND bpd.PAY_DATE <= @DATE
      AND (i.INVOICE_STATUS IS NULL OR i.INVOICE_STATUS NOT IN ( 3 ))
      AND bpd.PAY_AMOUNT - bpd.PAID_AMOUNT + ISNULL(pd.PAY_AMOUNT, 0) > 0
      AND (@MONTH IS NULL OR INVOICE_MONTH = @MONTH)

--GainLoss forward amount 
SELECT d.CURRENCY_ID,
       d.CURRENCY_NAME,
       d.CURRENCY_SING,
       d.CURRENCY_CODE,
       INVOICE_ID = 0,
       CUSTOMER_CODE = '',
	   METER_CODE = '',
       CUSTOMER_NAME = N'សរុបទឹកប្រាក់លម្អៀងទៅខែបន្ទាប់',
       STATUS_ID = 0,
       AREA_NAME = '',
       POLE_CODE = '',
       BOX_CODE = '',
       CUSTOMER_GROUP = N'សរុបទឹកប្រាក់លម្អៀងទៅខែបន្ទាប់',
       CUSTOMER_CONNECTION_TYPE = N'សរុបទឹកប្រាក់លម្អៀងទៅខែបន្ទាប់',
       INVOICE_NO = '',
       INVOICE_DATE = @DATE,
       INVOICE_MONTH = @DATE,
       INVOICE_TITLE = N'សរុបទឹកប្រាក់លម្អៀងទៅខែបន្ទាប់',
       SETTLE_AMOUNT = SUM(d.SETTLE_AMOUNT - d.PAID_AMOUNT),
       PAID_AMOUNT = 0,
       OPEN_DAYS = 0,
       STATUS = '',
       CYCLE_NAME = '',
       PHONE = '',
       INVOICE_ITEM_TYPE = '',
       INVOICE_ITEM = ''
INTO #FORWARD_AMOUNT
FROM #AgeDebt d
WHERE d.DUE_AMOUNT < dbo.GET_SMALLEST_VALUE(d.DIGITS_AFTER_DECIMAL)
GROUP BY d.CURRENCY_ID,
         d.CURRENCY_NAME,
         d.CURRENCY_SING,
         d.CURRENCY_CODE

SELECT *
INTO #RESULT
FROM
(
    SELECT d.CURRENCY_ID,
           d.CURRENCY_NAME,
           d.CURRENCY_SING,
           d.CURRENCY_CODE,
           d.INVOICE_ID,
           d.CUSTOMER_CODE,
		   d.METER_CODE,
           d.CUSTOMER_NAME,
           d.STATUS_ID,
           d.AREA_NAME,
           d.POLE_CODE,
           d.BOX_CODE,
           d.CUSTOMER_GROUP,
           d.CUSTOMER_CONNECTION_TYPE,
           d.INVOICE_NO,
           d.INVOICE_DATE,
           d.INVOICE_MONTH,
           d.INVOICE_TITLE,
           d.SETTLE_AMOUNT,
           d.PAID_AMOUNT,
           d.OPEN_DAYS,
           d.CYCLE_NAME,
           d.INVOICE_ITEM,
           d.INVOICE_ITEM_TYPE,
           d.STATUS,
		   d.PHONE
    FROM #AgeDebt d
    WHERE d.DUE_AMOUNT >= (dbo.GET_SMALLEST_VALUE(d.DIGITS_AFTER_DECIMAL))
    UNION ALL
    SELECT b.CURRENCY_ID,
           b.CURRENCY_NAME,
           b.CURRENCY_SING,
           b.CURRENCY_CODE,
           b.INVOICE_ID,
           b.CUSTOMER_CODE,
		   b.METER_CODE,
           b.CUSTOMER_NAME,
           b.STATUS_ID,
           b.AREA_NAME,
           b.POLE_CODE,
           b.BOX_CODE,
           b.CUSTOMER_GROUP,
           b.CUSTOMER_CONNECTION_TYPE,
           b.INVOICE_NO,
           b.INVOICE_DATE,
           b.INVOICE_MONTH,
           b.INVOICE_TITLE,
           b.SETTLE_AMOUNT,
           b.PAID_AMOUNT,
           b.OPEN_DAYS,
           b.CYCLE_NAME,
           b.INVOICE_ITEM,
           b.INVOICE_ITEM_TYPE,
           b.STATUS,
		   b.PHONE
    FROM #BankTransaction b
    UNION ALL
    SELECT f.CURRENCY_ID,
           f.CURRENCY_NAME,
           f.CURRENCY_SING,
           f.CURRENCY_CODE,
           f.INVOICE_ID,
           f.CUSTOMER_CODE,
		   f.METER_CODE,
           f.CUSTOMER_NAME,
           f.STATUS_ID,
           f.AREA_NAME,
           f.POLE_CODE,
           f.BOX_CODE,
           f.CUSTOMER_GROUP,
           f.CUSTOMER_CONNECTION_TYPE,
           f.INVOICE_NO,
           f.INVOICE_DATE,
           f.INVOICE_MONTH,
           f.INVOICE_TITLE,
           f.SETTLE_AMOUNT,
           f.PAID_AMOUNT,
           f.OPEN_DAYS,
           f.CYCLE_NAME,
           f.INVOICE_ITEM,
           f.INVOICE_ITEM_TYPE,
           f.STATUS,
		   f.PHONE
    FROM #FORWARD_AMOUNT f
) AS result

SELECT *
FROM #RESULT
--END OF REPORT_AGING_NEW

GO


