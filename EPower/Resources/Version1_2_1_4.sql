IF OBJECT_ID('EBL_RUN_PRE_UPLOAD_INVOICE')IS NOT NULL
	DROP PROC EBL_RUN_PRE_UPLOAD_INVOICE
GO

CREATE PROC EBL_RUN_PRE_UPLOAD_INVOICE
	@OPERATION_CODE NVARCHAR(50)='',
    @D1 DATETIME='2015-11-1',
	@D2 DATETIME='2015-12-05',
	@FORCE_ALL BIT = 0,
    @TASK_SIZE INT = 1000
AS 

IF NOT EXISTS(SELECT * FROM TBL_EBL_PROCESS WHERE OPERATION_NAME='3002')
    SET @FORCE_ALL=1;

CREATE TABLE #INVOICES(ID INT);
IF @FORCE_ALL=1		
	INSERT #INVOICES 
    SELECT INVOICE_ID 
    FROM TBL_INVOICE
    WHERE INVOICE_MONTH>='2015-1-1' OR PAID_AMOUNT<SETTLE_AMOUNT
ELSE 
	INSERT #INVOICES
	SELECT INVOICE_ID
	FROM TBL_INVOICE  l
	WHERE INVOICE_DATE BETWEEN @D1 AND @D2
	UNION ALL 
	SELECT INVOICE_ID 
	FROM  TBL_INVOICE_ADJUSTMENT 
	WHERE CREATE_ON BETWEEN @D1 AND @D2
    UNION ALL
    SELECT INVOICE_ID
    FROM TBL_PAYMENT p
    INNER JOIN TBL_PAYMENT_DETAIL d ON d.PAYMENT_ID = p.PAYMENT_ID
    WHERE p.CREATE_ON BETWEEN @D1 AND @D2;
-- bill_date,currency_id,bill_title,total_amount,paid_amount,last_paid_date,ext_no,ext_id,ext_consumer_id,
-- description,quantity,price,amount,ext_detail_id

SELECT  ROW_NO = ROW_NUMBER() OVER(ORDER BY i.INVOICE_ID),
        LINE_NO = 0,
        EXT_ID = i.INVOICE_ID,
        EXT_NO = i.INVOICE_NO,
        EXT_CUSTOMER_ID = i.CUSTOMER_ID,
        INVOICE_MONTH,
        INVOICE_DATE,
        CURRENCY_ID,
        INVOICE_TITLE,
        FORWARD_AMOUNT,
        TOTAL_AMOUNT,
        SETTLE_AMOUNT,
        PAID_AMOUNT,
        LAST_PAID_DATE =ISNULL(lp.PAY_DATE,INVOICE_DATE),
        START_PAY_DATE,
        DUE_DATE,
        DISCOUNT_USAGE,
        DISCOUNT_USAGE_NAME,
        METER_CODE = METER_CODE,
        START_DATE = START_DATE,
        END_DATE = END_DATE,
        START_USAGE = START_USAGE,
        END_USAGE = END_USAGE,
        TOTAL_USAGE = TOTAL_USAGE+ISNULL(adj.ADJUST_USAGE,0),
        IS_SERVICE_BILL= IS_SERVICE_BILL,
        PRICE,
        BASED_PRICE,
        INVOICE_STATUS, 
        DETAIL_DATA = '',
        ADJUST_DATA = ISNULL(CONVERT(NVARCHAR,adj.ADJUST_COUNT),''),
        EXT_DATA = '',
		CUSTOMER_CONNECTION_TYPE_ID
INTO #TMP
FROM TBL_INVOICE i
OUTER APPLY(
    SELECT TOP 1 PAY_DATE 
    FROM TBL_PAYMENT_DETAIL d
    INNER JOIN TBL_PAYMENT p ON d.PAYMENT_ID = p.PAYMENT_ID 
    WHERE d.INVOICE_ID=i.INVOICE_ID AND IS_VOID = 0
    ORDER BY PAY_DATE DESC
) lp
OUTER APPLY(
    SELECT ADJUST_USAGE=SUM(ADJUST_USAGE),ADJUST_COUNT=COUNT(*)
    FROM TBL_INVOICE_ADJUSTMENT a
    WHERE a.INVOICE_ID = i.INVOICE_ID
) adj
WHERE i.INVOICE_ID IN(
	SELECT ID FROM #INVOICES
)
ORDER BY i.INVOICE_ID

DECLARE @ROWS INT,@N INT
SET @ROWS=@@ROWCOUNT

-- NO, PROCESS TO ADD.
IF @ROWS=0 RETURN 0;

DECLARE @PROCESS_ID INT,@FIRST_TASK_ID INT
INSERT INTO TBL_EBL_PROCESS 
SELECT  PROCESS_DATE=GETDATE(),
        FROM_DATE=@D1,TO_DATE=@D2,
        OPERATION_CODE=@OPERATION_CODE,
        OPERATION_NAME='3002',
        OPERATION_FORMAT='csv',
        IS_COMPLETED=0,
        RESULT='',
        NOTE='',
        OPERATION_SCOPE='INVOICE'
SET @PROCESS_ID = @@IDENTITY;
SET @FIRST_TASK_ID=ISNULL(IDENT_CURRENT('TBL_EBL_TASK'),1);
IF EXISTS(SELECT* FROM  TBL_EBL_TASK) SET @FIRST_TASK_ID=@FIRST_TASK_ID+1;

SET @N=0; 
WHILE @ROWS>@N 
BEGIN
    INSERT INTO TBL_EBL_TASK 
    SELECT START_DATE=NULL,END_DATE=NULL,PROCESS_ID=@PROCESS_ID,IS_COMPLETED=0,RESULT='',
    SCOPE='INVOICE',
    HEADER='ext_id,ext_no,ext_customer_id,invoice_month,invoice_date,currency_id,invoice_title,forward_amount,total_amount,settle_amount,paid_amount,last_paid_date,start_pay_date,due_date,discount_usage,discount_usage_name,meter_code,start_date,end_date,start_usage,end_usage,total_usage,is_service_bill,price,based_price,invoice_status,detail_data,adjust_data,ext_data,customer_connection_type_id'
    SET @N = @N+@TASK_SIZE
END
-- bill_date,currency_id,bill_title,total_amount,paid_amount,last_paid_date,ext_no,ext_id,ext_consumer_id,
-- description,quantity,priceer_id,bill_date,issue_date,currency_id,bill_title,total_amount,paid_amount,last_paid_date,start_pay_date,
   
-- ext_id,ext_no,ext_customer_id,invoice_month,invoice_date,currency_id,invoice_title,forward_amount,total_amount,settle_amount,paid_amount,last_paid_date,start_pay_date,due_date,discount_usage,discount_usage_name,meter_code,start_date,end_date,start_usage,end_usage,total_usage,is_service_bill,price,based_price,invoice_status,detail_data,adjust_data,ext_data
INSERT INTO TBL_EBL_TASK_DETAIL
SELECT  TASK_ID = @FIRST_TASK_ID+(ROW_NO-1)/@TASK_SIZE,
        PK_ID = EXT_ID,
        DATA =    '"'+  REPLACE(EXT_ID,'"','')  +'",'
                + '"'+  REPLACE(EXT_NO,'"','')  +'",'
                + '"'+  REPLACE(EXT_CUSTOMER_ID,'"','')  +'",'
                + '"'+  REPLACE(CONVERT(NVARCHAR(10),INVOICE_MONTH,126),'"','')  +'",'
                + '"'+  REPLACE(CONVERT(NVARCHAR(10),INVOICE_DATE,126),'"','')  +'",'
                + '"'+  REPLACE(CURRENCY_ID,'"','')  +'",'
                + '"'+  REPLACE(INVOICE_TITLE,'"','')  +'",'
                + '"'+  REPLACE(FORWARD_AMOUNT,'"','')  +'",'
                + '"'+  REPLACE(TOTAL_AMOUNT,'"','')  +'",'
                + '"'+  REPLACE(SETTLE_AMOUNT,'"','')  +'",'
                + '"'+  REPLACE(PAID_AMOUNT,'"','')  +'",'
                + '"'+  REPLACE(CONVERT(NVARCHAR(10),LAST_PAID_DATE,126),'"','')  +'",'
                + '"'+  REPLACE(CONVERT(NVARCHAR(10),START_PAY_DATE,126),'"','')  +'",'
                + '"'+  REPLACE(CONVERT(NVARCHAR(10),DUE_DATE,126),'"','')  +'",'
                + '"'+  REPLACE(DISCOUNT_USAGE,'"','')  +'",'
                + '"'+  REPLACE(DISCOUNT_USAGE_NAME,'"','')  +'",'
                + '"'+  REPLACE(METER_CODE,'"','')  +'",'
                + '"'+  REPLACE(CONVERT(NVARCHAR(10),START_DATE,126),'"','')  +'",'
                + '"'+  REPLACE(CONVERT(NVARCHAR(10),END_DATE,126),'"','')  +'",'
                + '"'+  REPLACE(START_USAGE,'"','')  +'",'
                + '"'+  REPLACE(END_USAGE,'"','')  +'",'
                + '"'+  REPLACE(TOTAL_USAGE,'"','')  +'",'
                + '"'+  REPLACE(IS_SERVICE_BILL,'"','')  +'",'
                + '"'+  REPLACE(PRICE,'"','')  +'",'
                + '"'+  REPLACE(BASED_PRICE,'"','')  +'",'
                + '"'+  REPLACE(INVOICE_STATUS,'"','')  +'",'
                + '"'+  REPLACE(DETAIL_DATA,'"','')  +'",'
                + '"'+  REPLACE(ADJUST_DATA,'"','')  +'",'
                + '"'+  REPLACE(EXT_DATA,'"','')+'",'
				+ '"'+  REPLACE(CUSTOMER_CONNECTION_TYPE_ID,'"','')+'"'
FROM #TMP
ORDER BY ROW_NO
RETURN @ROWS