﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogConnectionFee : ExDialog
    {
        GeneralProcess _flag;
        TBL_CONNECTION_FEE _objDeposit = new TBL_CONNECTION_FEE();

        public TBL_CONNECTION_FEE Deposit
        {
            get { return _objDeposit; }
        }
        TBL_CONNECTION_FEE _oldObjDeposit = new TBL_CONNECTION_FEE();

        #region Constructor
        public DialogConnectionFee(GeneralProcess flag, TBL_CONNECTION_FEE objDeposit)
        {
            InitializeComponent();

            UIHelper.SetDataSourceToComboBox(cboAmpere, Lookup.GetPowerAmpare());
            UIHelper.SetDataSourceToComboBox(cboPhase, Lookup.GetPowerPhases());
            UIHelper.SetDataSourceToComboBox(cboCurrency, Lookup.GetCurrencies());

            _flag = flag;
            objDeposit._CopyTo(_objDeposit);
            objDeposit._CopyTo(_oldObjDeposit);

            if (flag == GeneralProcess.Insert)
            {
                _objDeposit.IS_ACTIVE = true;
                this.Text = string.Concat(Resources.INSERT, this.Text);
            }
            else if (flag == GeneralProcess.Update)
            {
                this.Text = string.Concat(Resources.UPDATE, this.Text);
            }
            else if (flag == GeneralProcess.Delete)
            {
                this.Text = string.Concat(Resources.DELETE, this.Text);
                UIHelper.SetEnabled(this, false);
            }

            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;

            read();
        }
        #endregion

        #region Operation
        /// <summary>
        /// Data transaction
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (this.inValid())
            {
                return;
            }

            this.write();


            //if duplicate record.
            this.ClearAllValidation();
            var lstDeposit = from d in DBDataContext.Db.TBL_CONNECTION_FEEs
                             where d.AMPARE_ID == _objDeposit.AMPARE_ID
                             && d.PHASE_ID == _objDeposit.PHASE_ID
                             && d.CURRENCY_ID == _objDeposit.CURRENCY_ID
                             && d.IS_ACTIVE
                             && d.DEPOSIT_ID != _objDeposit.DEPOSIT_ID
                             select d;
            if (lstDeposit.Count() > 0 && _flag != GeneralProcess.Delete)
            {
                this.cboAmpere.SetValidation(string.Format(Resources.MS_IS_EXISTS, this.lblAMPARE.Text));
                this.cboPhase.SetValidation(string.Format(Resources.MS_IS_EXISTS, this.lblPHASE.Text));
                this.cboCurrency.SetValidation(string.Format(Resources.MS_IS_EXISTS, this.lblCURRENCY.Text));
                return;
            }
            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objDeposit);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_oldObjDeposit, _objDeposit);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objDeposit);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        #endregion

        #region Method

        /// <summary>
        /// Load data from object.
        /// </summary>
        private void read()
        {
            this.cboAmpere.SelectedValue = _objDeposit.AMPARE_ID;
            this.cboPhase.SelectedValue = _objDeposit.PHASE_ID;
            this.cboCurrency.SelectedValue = _objDeposit.CURRENCY_ID;
            this.txtNewConnection.Text = _objDeposit.NEW_CONECTION.ToString(DataHelper.MoneyFormat);
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            _objDeposit.AMPARE_ID = (int)this.cboAmpere.SelectedValue;
            _objDeposit.IS_ACTIVE = true;
            _objDeposit.CURRENCY_ID = (int)cboCurrency.SelectedValue;
            _objDeposit.PHASE_ID = (int)this.cboPhase.SelectedValue;
            _objDeposit.NEW_CONECTION = UIHelper.Round(DataHelper.ParseToDecimal(txtNewConnection.Text.Trim()), _objDeposit.CURRENCY_ID);
        }

        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();
            if (this.cboAmpere.SelectedIndex == -1)
            {
                cboAmpere.SetValidation(string.Format(Resources.REQUIRED, lblAMPARE.Text));
                val = true;
            }
            if (this.cboPhase.SelectedIndex == -1)
            {
                cboPhase.SetValidation(string.Format(Resources.REQUIRED, lblPHASE.Text));
                val = true;
            }
            if (cboCurrency.SelectedIndex == -1)
            {
                cboCurrency.SetValidation(string.Format(Resources.REQUIRED, lblCURRENCY.Text));
                val = true;
            }
            if (txtNewConnection.Text.Trim() == "")
            {
                txtNewConnection.SetValidation(string.Format(Resources.REQUIRED, lblCONNECTION_FEE.Text));
                val = true;
            }

            if (DataHelper.ParseToDecimal(txtNewConnection.Text.Trim()) < 0)
            {
                txtNewConnection.SetValidation(Resources.MS_AMOUNT_DEPOSIT_MUST_BE_GREATER_THAN_ZERO);
                val = true;
            }

            return val;
        }
        #endregion        

        #region Event

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objDeposit);
        }

        private void txtDepositAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputNumberOnly(sender, e);
        }

        #endregion Event




    }
}