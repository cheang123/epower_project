﻿using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;

namespace EPower.Interface
{
    public partial class DialogCustomerChangeAmpareHistory : ExDialog
    {
        TBL_CUSTOMER _objCustomer = new TBL_CUSTOMER();
        #region Constructor
        public DialogCustomerChangeAmpareHistory(int customerId)
        {
            InitializeComponent();
            _objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == customerId);
            UIHelper.DataGridViewProperties(dgvHistory);

            read();
            loadHistory();
        }

        #endregion Constructor

        #region Method

        private void read()
        {
            txtCustomerCode.Text = _objCustomer.CUSTOMER_CODE;
            txtCustomerName.Text = _objCustomer.LAST_NAME_KH + " " + _objCustomer.FIRST_NAME_KH;
            txtCustomerType.Text = DBDataContext.Db.TBL_CUSTOMER_TYPEs.FirstOrDefault(x => x.CUSTOMER_TYPE_ID == _objCustomer.CUSTOMER_TYPE_ID).CUSTOMER_TYPE_NAME;
            txtArea.Text = DBDataContext.Db.TBL_AREAs.FirstOrDefault(x => x.AREA_ID == _objCustomer.AREA_ID).AREA_CODE;
        }
        private void loadHistory()
        {
            //Display History in data grid
            var db = from h in DBDataContext.Db.TBL_CUSTOMER_CHANGE_AMPAREs
                     join c in DBDataContext.Db.TBL_CUSTOMERs on h.CUSTOMER_ID equals c.CUSTOMER_ID
                     join oldAmp in DBDataContext.Db.TBL_AMPAREs on h.OLD_AMPARE_ID equals oldAmp.AMPARE_ID
                     join newAmp in DBDataContext.Db.TBL_AMPAREs on h.NEW_AMPARE_ID equals newAmp.AMPARE_ID
                     where h.CUSTOMER_ID == _objCustomer.CUSTOMER_ID
                         && h.IS_ACTIVE
                     orderby h.CUSTOMER_CHANGE_AMPARE_ID descending
                     select new
                     {
                         OLD_AMPARE = oldAmp.AMPARE_NAME,
                         NEW_AMPARE = newAmp.AMPARE_NAME,
                         h.CREATE_BY,
                         h.CHANGE_DATE,
                         h.NOTE
                     };
            dgvHistory.DataSource = db;
        }

        #endregion Method


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
