﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;


namespace EPower.Interface
{
    public partial class DialogCustomerActivate : SoftTech.Component.ExDialog
    {
        #region Data
        TBL_CUSTOMER _objCustomerNew = new TBL_CUSTOMER();
        TBL_CUSTOMER _objCustomerOld = new TBL_CUSTOMER();
        TBL_CUSTOMER_METER _objCustomerMeterNew = new TBL_CUSTOMER_METER();
        TBL_CUS_STATUS_CHANGE _objCustomerStatusChange = new TBL_CUS_STATUS_CHANGE();


        TBL_BOX _objBoxNew = null;
        TBL_BOX _objBoxOld = null;
        TBL_METER _objMeterNew = null;
        TBL_METER _objMeterOld = null;
        TBL_USAGE _objUsageNew = null;
        TBL_CIRCUIT_BREAKER _objBreakerNew = null;
        TBL_CIRCUIT_BREAKER _objBreakerOld = null;
        TBL_POLE _objPole = null;
        #endregion Data

        #region Constructor
        public DialogCustomerActivate(TBL_CUSTOMER objCustomer)
        {
            InitializeComponent();

            // module "POSITION_IN_BOX_ENABLE"
            lblPOSITION_IN_BOX.Visible =
                txtPositionInBox.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.POSITION_IN_BOX_ENABLE]);

            objCustomer._CopyTo(_objCustomerNew);
            objCustomer._CopyTo(_objCustomerOld);
            read();
        }
        #endregion Constructor

        #region Method  
        bool invalid()
        {
            bool blnReturn = false;
            if (cboBillingCycle.SelectedIndex == -1)
            {
                cboBillingCycle.SetValidation(string.Format(Properties.Resources.REQUIRED_SELECT_VALUE, lblCYCLE_NAME.Text));
                blnReturn = true;
            }
            //if(cboPrice.SelectedIndex == -1)
            //{
            //    cboPrice.SetValidation(string.Format(Properties.Resources.RequestSeletedValue, lblPrice.Text));
            //    blnReturn = true;
            //}
            if (txtMeterType.Text.Trim() == "")
            {
                txtMeter.SetValidation(string.Format(Properties.Resources.REQUIRED, lblMETER_CODE.Text));
                blnReturn = true;
            }
            if (cboMeterShield.SelectedIndex == -1)
            {
                cboMeterShield.SetValidation(string.Format(Properties.Resources.REQUIRED_SELECT_VALUE, lblSHIELD.Text));
                blnReturn = true;
            }
            if (cboCableShield.SelectedIndex == -1)
            {
                cboCableShield.SetValidation(string.Format(Properties.Resources.REQUIRED, lblCABLE_SHIELD.Text));
                blnReturn = true;
            }
            if (txtBreakerType.Text.Trim() == "")
            {
                txtBreaker.SetValidation(string.Format(Properties.Resources.REQUIRED, lblBREAKER_CODE.Text));
                blnReturn = true;
            }
            if (txtPoleCode.Text.Trim() == "")
            {
                txtBox.SetValidation(string.Format(Properties.Resources.REQUIRED, lblBOX.Text));
                blnReturn = true;
            }
            if (!DataHelper.IsNumber(this.txtSTART_USAGE.Text))
            {
                txtSTART_USAGE.SetValidation(string.Format(Properties.Resources.REQUIRED, lblSTART_USAGE.Text));
                blnReturn = true;
            }

            //if (txtPositionInBox.Visible && !DataHelper.IsInteger(txtNEWPositionInBox.Text))
            //{
            //    txtPositionInBox.SetValidation(string.Format(Properties.Resources.RequestInputNumber, this.lblPOSITION_IN_BOX.Text));
            //    result = true;
            //}

            return blnReturn;
        }

        void bind()
        {
            //UIHelper.SetDataSourceToComboBox(cboPrice, DBDataContext.Db.TBL_PRICEs.Where(p=>p.IS_ACTIVE));
            UIHelper.SetDataSourceToComboBox(cboBillingCycle, DBDataContext.Db.TBL_BILLING_CYCLEs.Where(bc => bc.IS_ACTIVE));
            UIHelper.SetDataSourceToComboBox(cboMeterShield, DBDataContext.Db.TBL_SEALs.Where(s => s.IS_ACTIVE));
            UIHelper.SetDataSourceToComboBox(cboCableShield, DBDataContext.Db.TBL_SEALs.Where(s => s.IS_ACTIVE));
        }

        void read()
        {
            bind();

            this.txtLastNamekh.Text = _objCustomerNew.LAST_NAME_KH + " " + _objCustomerNew.FIRST_NAME_KH;
            this.txtCustomerType.Text = DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs.Single(ct => ct.CUSTOMER_CONNECTION_TYPE_ID == _objCustomerNew.CUSTOMER_CONNECTION_TYPE_ID).CUSTOMER_CONNECTION_TYPE_NAME;
            this.dtpActivateDate.Value = DBDataContext.Db.GetSystemDate();
            this.cboBillingCycle.SelectedValue = _objCustomerNew.BILLING_CYCLE_ID;

            //Default Cutoff days
            this.nudCutoffDays.Value = DataHelper.ParseToDecimal(DBDataContext.Db.TBL_UTILITies.FirstOrDefault(row => row.UTILITY_ID == (int)Utility.DEFAULT_CUT_OFF_DAY).UTILITY_VALUE);

            // reactivate
            if (this._objCustomerNew.STATUS_ID == (int)CustomerStatus.Closed)
            {
                var objLastCusMeter = DBDataContext.Db.TBL_CUSTOMER_METERs
                                                      .Where(x => x.CUSTOMER_ID == this._objCustomerNew.CUSTOMER_ID)
                                                      .OrderByDescending(x => x.CUS_METER_ID)
                                                      .FirstOrDefault();
                var objMeter = DBDataContext.Db.TBL_METERs.FirstOrDefault(x => x.METER_ID == objLastCusMeter.METER_ID);
                var objBreaker = DBDataContext.Db.TBL_CIRCUIT_BREAKERs.FirstOrDefault(x => x.BREAKER_ID == objLastCusMeter.BREAKER_ID);
                var objBox = DBDataContext.Db.TBL_BOXes.FirstOrDefault(x => x.BOX_ID == objLastCusMeter.BOX_ID);

                if (objMeter != null)
                {
                    this.txtMeter.Text = objMeter.METER_CODE;
                    this.txtMeter.AcceptSearch(true);
                }
                if (objBreaker != null)
                {
                    this.txtBreaker.Text = objBreaker.BREAKER_CODE;
                    this.txtBreaker.AcceptSearch(true);
                }
                if (objBox != null)
                {
                    this.txtBox.Text = objBox.BOX_CODE;
                    this.txtBox.AcceptSearch(true);
                }

                //find last usage
                var objUsage = DBDataContext.Db.TBL_USAGEs.Where(x => x.CUSTOMER_ID == this._objCustomerNew.CUSTOMER_ID)
                                                        .OrderByDescending(x => x.USAGE_MONTH)
                                                        .ThenByDescending(x => x.POSTING_DATE)
                                                        .FirstOrDefault();
                if (objUsage != null)
                {
                    this.txtSTART_USAGE.Text = objUsage.END_USAGE.ToString("#");
                }
                this.txtPositionInBox.Text = objLastCusMeter.POSITION_IN_BOX.ToString("#");
            }
        }


        #endregion Method

        #region Event
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (invalid())
            {
                return;
            }
            if (dtpActivateDate.Value > DBDataContext.Db.GetSystemDate())
            {
                dtpActivateDate.Focus();
                if (MsgBox.ShowQuestion(string.Format(Resources.MSG_WARRING_CUSTOMER_ACTIVATE_DATE_GREATER_THEN__NOW), Resources.WARNING) == DialogResult.Yes)
                {
                    this.saveData();
                }
                else
                {
                    return;
                }
            }
            else
            {
                this.saveData();
            }
        }

        private void saveData()
        {
            try
            {
                var cycleId = DataHelper.ParseToInt(cboBillingCycle.SelectedValue.ToString());
                var cableShieldId = DataHelper.ParseToInt(cboCableShield.SelectedValue.ToString());
                var meterShieldId = DataHelper.ParseToInt(cboMeterShield.SelectedValue.ToString());
                Runner.RunNewThread(() =>
                {
                    using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                    {
                        // update customer.
                        _objCustomerNew.AREA_ID = _objPole.AREA_ID;
                        _objCustomerNew.ACTIVATE_DATE = dtpActivateDate.Value;
                        _objCustomerNew.BILLING_CYCLE_ID = cycleId;
                        _objCustomerNew.CUT_OFF_DAY = (int)nudCutoffDays.Value;
                        _objCustomerNew.STATUS_ID = (int)CustomerStatus.Active;
                        TBL_CHANGE_LOG objLog = DBDataContext.Db.Update(_objCustomerOld, _objCustomerNew);

                        // insert customer meter.
                        _objCustomerMeterNew.BOX_ID = _objBoxNew.BOX_ID;
                        _objCustomerMeterNew.BREAKER_ID = _objBreakerNew.BREAKER_ID;
                        _objCustomerMeterNew.CABLE_SHIELD_ID = cableShieldId;
                        _objCustomerMeterNew.CUSTOMER_ID = _objCustomerNew.CUSTOMER_ID;
                        _objCustomerMeterNew.METER_ID = _objMeterNew.METER_ID;
                        _objCustomerMeterNew.METER_SHIELD_ID = meterShieldId;
                        _objCustomerMeterNew.POLE_ID = _objPole.POLE_ID;
                        _objCustomerMeterNew.IS_ACTIVE = true;
                        _objCustomerMeterNew.USED_DATE = _objCustomerNew.ACTIVATE_DATE;
                        _objCustomerMeterNew.REMAIN_USAGE = 0;
                        _objCustomerMeterNew.POSITION_IN_BOX = DataHelper.ParseToInt(txtPositionInBox.Text);
                        DBDataContext.Db.InsertChild(_objCustomerMeterNew, _objCustomerOld, ref objLog);

                    // inser start usage
                    DateTime datStart = UIHelper._DefaultDate;
                    DateTime datEnd = UIHelper._DefaultDate;
                    DateTime datMonth = Method.GetNextBillingMonth(this._objCustomerNew.BILLING_CYCLE_ID, ref datStart, ref datEnd);
                    TBL_USAGE _objUsage = DBDataContext.Db.TBL_USAGEs.Where(r => r.CUSTOMER_ID == _objCustomerNew.CUSTOMER_ID && r.METER_ID == _objMeterNew.METER_ID && r.USAGE_MONTH == datMonth).OrderByDescending(r => r.USAGE_ID).FirstOrDefault();

                    if (_objUsage == null)
                    {
                        _objUsageNew = new TBL_USAGE();
                        _objUsageNew.COLLECTOR_ID = 0;
                        _objUsageNew.CREATE_BY = Login.CurrentLogin.LOGIN_NAME;
                        _objUsageNew.CUSTOMER_ID = _objCustomerNew.CUSTOMER_ID;
                        _objUsageNew.DEVICE_ID = 0;
                        _objUsageNew.END_USAGE = DataHelper.ParseToDecimal(this.txtSTART_USAGE.Text);
                        _objUsageNew.END_USE_DATE = datEnd;
                        _objUsageNew.IS_METER_RENEW_CYCLE = false;
                        _objUsageNew.METER_ID = _objMeterNew.METER_ID;
                        _objUsageNew.POSTING_DATE = DBDataContext.Db.GetSystemDate();
                        _objUsageNew.START_USAGE = DataHelper.ParseToDecimal(this.txtSTART_USAGE.Text);
                        _objUsageNew.START_USE_DATE = datStart;
                        _objUsageNew.USAGE_MONTH = datMonth;
                        _objUsageNew.MULTIPLIER = _objMeterNew.MULTIPLIER;
                        DBDataContext.Db.TBL_USAGEs.InsertOnSubmit(_objUsageNew);
                    }
                    else
                    {
                        TBL_USAGE _objUsageOld = new TBL_USAGE();
                        _objUsage._CopyTo(_objUsageOld);

                        _objUsage.CREATE_BY = Login.CurrentLogin.LOGIN_NAME;
                        _objUsage.END_USAGE = DataHelper.ParseToDecimal(this.txtSTART_USAGE.Text);
                        _objUsage.START_USAGE = DataHelper.ParseToDecimal(this.txtSTART_USAGE.Text);
                        _objUsage.MULTIPLIER = _objMeterNew.MULTIPLIER;
                        _objUsage.POSTING_DATE = DBDataContext.Db.GetSystemDate();
                        DBDataContext.Db.Update(_objUsageOld, _objUsage);
                    }
                    // insert customer status change
                    _objCustomerStatusChange.CUSTOMER_ID = _objCustomerNew.CUSTOMER_ID;
                    _objCustomerStatusChange.CHNAGE_DATE = _objCustomerNew.ACTIVATE_DATE;
                    _objCustomerStatusChange.CREATE_BY = Login.CurrentLogin.LOGIN_NAME;
                    _objCustomerStatusChange.NEW_STATUS_ID = _objCustomerNew.STATUS_ID;
                    _objCustomerStatusChange.OLD_STATUS_ID = _objCustomerOld.STATUS_ID;
                    _objCustomerStatusChange.INVOICE_ID = 0;
                    DBDataContext.Db.TBL_CUS_STATUS_CHANGEs.InsertOnSubmit(_objCustomerStatusChange);

                        // update meter status
                        _objBoxNew.STATUS_ID = (int)BoxStatus.Used;
                        _objMeterNew.STATUS_ID = (int)MeterStatus.Used;
                        _objBreakerNew.STATUS_ID = (int)BreakerStatus.Used;
                        DBDataContext.Db.UpdateChild(_objMeterOld, _objMeterNew, _objCustomerOld, ref objLog);
                        DBDataContext.Db.UpdateChild(_objBreakerOld, _objBreakerNew, _objCustomerOld, ref objLog);
                        DBDataContext.Db.UpdateChild(_objBoxOld, _objBoxNew, _objCustomerOld, ref objLog);

                    // log mete status change
                    DBDataContext.Db.TBL_STOCK_TRANs.InsertOnSubmit(new TBL_STOCK_TRAN()
                    {
                        STOCK_TRAN_TYPE_ID = (int)StockTranType.Use,
                        FROM_STOCK_TYPE_ID = (int)StockType.Stock,
                        TO_STOCK_TYPE_ID = (int)StockType.Used,
                        QTY = 1,
                        CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                        CREATE_ON = DBDataContext.Db.GetSystemDate(),
                        ITEM_ID = _objMeterNew.METER_TYPE_ID,
                        ITEM_TYPE_ID = (int)StockItemType.Meter,
                        REMARK = _objMeterNew.METER_CODE,
                        UNIT_PRICE = 0
                    });
                    DBDataContext.Db.SubmitChanges();

                    //remove meter from TBL_METER_UNKNOWN
                    Method.RemoveUnknownMeter(_objMeterNew.METER_CODE);

                        tran.Complete();

                        //refresh LINQ DataContext
                        // DBDataContext.Db = null;
                        this.DialogResult = DialogResult.OK;
                    }
                });
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void txtMeter_AdvanceSearch(object sender, EventArgs e)
        {
            // if not entry.
            if (txtMeter.Text.Trim() == "")
            {
                txtMeter.CancelSearch();
                return;
            }

            string strMeterCode = Method.FormatMeterCode(this.txtMeter.Text);
            TBL_METER tmp = DBDataContext.Db.TBL_METERs.FirstOrDefault(m => m.METER_CODE == strMeterCode);

            // if not contain in database.
            if (tmp == null)
            {
                //if user have permission to add new meter
                if (SoftTech.Security.Logic.Login.IsAuthorized(Permission.ADMIN_METER))
                {
                    //if user agree to add new meter
                    if (MsgBox.ShowQuestion(string.Format(Resources.MSQ_CREATE_METER, strMeterCode), "") == DialogResult.Yes)
                    {
                        DialogMeter diagMeter = new DialogMeter(GeneralProcess.Insert, new TBL_METER() { METER_CODE = strMeterCode });
                        diagMeter.ShowDialog();
                        if (diagMeter.DialogResult == DialogResult.OK)
                        {
                            tmp = diagMeter.Meter;
                        }
                        else
                        {
                            this.txtMeter.CancelSearch();
                            return;
                        }
                    }
                    else
                    {
                        this.txtMeter.CancelSearch();
                        return;
                    }
                }
                else
                {
                    MsgBox.ShowInformation(Resources.MS_METER_NOT_FOUND);
                    this.txtMeter.CancelSearch();
                    return;
                }
            }

            // if meter is inuse.
            if (tmp.STATUS_ID == (int)MeterStatus.Used)
            {
                MsgBox.ShowInformation(Resources.MS_METER_STATUS_IN_USE);
                this.txtMeter.CancelSearch();
                return;
            }

            if (tmp.STATUS_ID == (int)MeterStatus.Unavailable)
            {
                MsgBox.ShowInformation(Resources.MS_METER_IS_UNAVAILABLE);
                this.txtMeter.CancelSearch();
                return;
            }

            _objMeterNew = new TBL_METER();
            _objMeterOld = new TBL_METER();
            tmp._CopyTo(_objMeterNew);
            tmp._CopyTo(_objMeterOld);

            TBL_METER_TYPE objMeterType = DBDataContext.Db.TBL_METER_TYPEs.FirstOrDefault(mt => mt.METER_TYPE_ID == _objMeterNew.METER_TYPE_ID);
            txtMeter.Text = tmp.METER_CODE;
            txtMeterType.Text = objMeterType.METER_TYPE_NAME;
            txtMeterAmp.Text = DBDataContext.Db.TBL_AMPAREs.FirstOrDefault(a => a.AMPARE_ID == objMeterType.METER_AMP_ID).AMPARE_NAME;
            txtMeterPhase.Text = DBDataContext.Db.TBL_PHASEs.FirstOrDefault(p => p.PHASE_ID == objMeterType.METER_PHASE_ID).PHASE_NAME;
            txtMeterVol.Text = DBDataContext.Db.TBL_VOLTAGEs.FirstOrDefault(v => v.VOLTAGE_ID == objMeterType.METER_VOL_ID).VOLTAGE_NAME;

            this.txtSTART_USAGE.Focus();
            //this.txtBreaker.Focus();
            txtBreaker.Text = tmp.METER_CODE;
        }

        private void txtBreaker_AdvanceSearch(object sender, EventArgs e)
        {
            if (txtBreaker.Text.Trim() == "")
            {
                txtBreaker.CancelSearch();
                return;
            }

            string strBreakerCode = txtBreaker.Text;
            TBL_CIRCUIT_BREAKER tmp = DBDataContext.Db.TBL_CIRCUIT_BREAKERs.FirstOrDefault(b => b.BREAKER_CODE == strBreakerCode);

            if (tmp == null)
            {
                //if user have permission to add new breaker
                if (SoftTech.Security.Logic.Login.IsAuthorized(Permission.ADMIN_CIRCUITBREAKER))
                {
                    //if user agree to add new breaker
                    if (MsgBox.ShowQuestion(string.Format(Resources.MSQ_CREATE_BREAKER, strBreakerCode), "") == DialogResult.Yes)
                    {
                        DialogCircuitBreaker diagBreaker = new DialogCircuitBreaker(GeneralProcess.Insert, new TBL_CIRCUIT_BREAKER() { BREAKER_CODE = strBreakerCode });
                        diagBreaker.ShowDialog();
                        if (diagBreaker.DialogResult == DialogResult.OK)
                        {
                            tmp = diagBreaker.CircuitBraker;
                        }
                        else
                        {
                            this.txtBreaker.CancelSearch();
                            return;
                        }
                    }
                    else
                    {
                        this.txtBreaker.CancelSearch();
                        return;
                    }
                }
                else
                {
                    MsgBox.ShowInformation(Resources.MS_BREAKER_NOT_FOUND);
                    this.txtBreaker.CancelSearch();
                    return;
                }
            }

            if (tmp.STATUS_ID == (int)BreakerStatus.Used)
            {
                MsgBox.ShowInformation(Resources.MS_BREAKER_IS_IN_USE);
                this.txtBreaker.CancelSearch();
                return;
            }

            if (tmp.STATUS_ID == (int)BreakerStatus.Unavailable)
            {
                MsgBox.ShowInformation(Resources.MS_BREAKER_IS_UNAVAILABLE);
                this.txtBreaker.CancelSearch();
                return;
            }
            _objBreakerNew = new TBL_CIRCUIT_BREAKER();
            _objBreakerOld = new TBL_CIRCUIT_BREAKER();
            tmp._CopyTo(_objBreakerOld);
            tmp._CopyTo(_objBreakerNew);

            TBL_CIRCUIT_BREAKER_TYPE objBreakerType = DBDataContext.Db.TBL_CIRCUIT_BREAKER_TYPEs.FirstOrDefault(bt => bt.BREAKER_TYPE_ID == _objBreakerNew.BREAKER_TYPE_ID);
            txtBreaker.Text = tmp.BREAKER_CODE;
            txtBreakerType.Text = objBreakerType.BREAKER_TYPE_NAME;
            txtBreakerAmp.Text = DBDataContext.Db.TBL_AMPAREs.FirstOrDefault(a => a.AMPARE_ID == objBreakerType.BREAKER_AMP_ID).AMPARE_NAME;
            txtBreakerPhase.Text = DBDataContext.Db.TBL_PHASEs.FirstOrDefault(p => p.PHASE_ID == objBreakerType.BREAKER_PHASE_ID).PHASE_NAME;
            txtBreakerVol.Text = DBDataContext.Db.TBL_VOLTAGEs.FirstOrDefault(v => v.VOLTAGE_ID == objBreakerType.BREAKER_VOL_ID).VOLTAGE_NAME;

            this.txtBox.Focus();
        }

        private void txtBox_AdvanceSearch(object sender, EventArgs e)
        {
            if (txtBox.Text.Trim() == "")
            {
                txtBox.CancelSearch();
                return;
            }

            string strBox = txtBox.Text;
            TBL_BOX tmp = (from b in DBDataContext.Db.TBL_BOXes
                           where b.BOX_CODE.ToLower().Trim() == strBox.ToLower().Trim()
                           select b).FirstOrDefault();

            if (tmp == null)
            {
                //if user have permission to add new breaker
                if (SoftTech.Security.Logic.Login.IsAuthorized(Permission.ADMIN_POLEANDBOX))
                {
                    //if user agree to add new breaker
                    if (MsgBox.ShowQuestion(string.Format(Resources.MSQ_CREATE_BOX, strBox), "") == DialogResult.Yes)
                    {
                        DialogBox diagBox = new DialogBox(GeneralProcess.Insert, new TBL_BOX() { BOX_CODE = strBox, STARTED_DATE = DBDataContext.Db.GetSystemDate(), END_DATE = UIHelper._DefaultDate });
                        diagBox.ShowDialog();
                        if (diagBox.DialogResult == DialogResult.OK)
                        {
                            tmp = diagBox.Box;
                            this.txtBox.Text = tmp.BOX_CODE;
                        }
                        else
                        {
                            this.txtBox.CancelSearch();
                            return;
                        }
                    }
                    else
                    {
                        this.txtBox.CancelSearch();
                        return;
                    }
                }
                else
                {
                    MsgBox.ShowInformation(Resources.MS_BOX_NOT_FOUND);
                    this.txtBox.CancelSearch();
                    return;
                }
            }

            if (tmp.STATUS_ID == (int)BoxStatus.Unavailable)
            {
                MsgBox.ShowInformation(Resources.MS_BOX_IS_UNAVAILABLE);
                this.txtBox.CancelSearch();
                return;
            }

            _objBoxNew = new TBL_BOX();
            _objBoxOld = new TBL_BOX();
            tmp._CopyTo(_objBoxOld);
            tmp._CopyTo(_objBoxNew);

            _objPole = DBDataContext.Db.TBL_POLEs.FirstOrDefault(p => p.POLE_ID == _objBoxNew.POLE_ID);

            if (_objCustomerNew.AREA_ID != _objPole.AREA_ID)
            {
                if (MsgBox.ShowQuestion(Resources.MSQ_NEW_BOX_NOT_IN_AREA_OF_CUSTOMER, string.Empty) != DialogResult.Yes)
                {
                    this.txtBox.CancelSearch();
                    return;
                }
            }

            txtPoleCode.Text = _objPole.POLE_CODE;
            txtCollector.Text = DBDataContext.Db.TBL_EMPLOYEEs.FirstOrDefault(emp => emp.EMPLOYEE_ID == _objPole.COLLECTOR_ID).EMPLOYEE_NAME;
            txtBiller.Text = DBDataContext.Db.TBL_EMPLOYEEs.FirstOrDefault(emp => emp.EMPLOYEE_ID == _objPole.BILLER_ID).EMPLOYEE_NAME;
            txtCutter.Text = DBDataContext.Db.TBL_EMPLOYEEs.FirstOrDefault(emp => emp.EMPLOYEE_ID == _objPole.CUTTER_ID).EMPLOYEE_NAME;

            txtPositionInBox.Text = "";
            if (txtPositionInBox.Visible)
            {
                txtPositionInBox.ReadOnly = false;
                txtPositionInBox.Focus();
                return;
            }
            this.btnOK.Focus();
        }

        private void txtMeter_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void txtMeter_CancelAdvanceSearch(object sender, EventArgs e)
        {
            _objMeterNew = null;
            _objMeterOld = null;

            txtMeterType.Text = "";
            txtMeterAmp.Text = "";
            txtMeterPhase.Text = "";
            txtMeterVol.Text = "";
        }

        private void txtBreaker_CancelAdvanceSearch(object sender, EventArgs e)
        {
            _objBreakerNew = null;
            _objBreakerOld = null;

            txtBreakerType.Text = "";
            txtBreakerAmp.Text = "";
            txtBreakerPhase.Text = "";
            txtBreakerVol.Text = "";
        }

        private void txtBox_CancelAdvanceSearch(object sender, EventArgs e)
        {
            _objPole = null;
            _objBoxNew = null;
            _objBoxOld = null;
            txtPoleCode.Text = "";
            txtCollector.Text = "";
            txtBiller.Text = "";
            txtCutter.Text = "";
            txtPositionInBox.Text = "";
        }

        private void txtSTART_USAGE_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.txtBreaker.Focus();
            }
        }

        private void txtPositionInBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputNumberOnly(sender, e);
            if (e.KeyChar == 13)
            {
                btnOK.Focus();
            }
        }

        private void lblViewBoxCustomers_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (this._objBoxNew == null)
            {
                return;
            }
            var diag = new DialogCustomerInBox(this._objBoxNew.BOX_ID);
            diag.ShowDialog();
        }
        #endregion Event
    }
}
