﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogDeposit : ExDialog
    {
        GeneralProcess _flag;

        TBL_DEPOSIT _objDeposit = new TBL_DEPOSIT();

        public TBL_DEPOSIT Deposit
        {
            get { return _objDeposit; }
        }
        TBL_DEPOSIT _oldObjDeposit = new TBL_DEPOSIT();

        #region Constructor
        public DialogDeposit(GeneralProcess flag, TBL_DEPOSIT objDeposit)
        {
            InitializeComponent();

            UIHelper.SetDataSourceToComboBox(cboCustomerGroup, Lookup.GetCustomerGroup());
            UIHelper.SetDataSourceToComboBox(cboAmpere, Lookup.GetPowerAmpare());
            UIHelper.SetDataSourceToComboBox(cboPhase, Lookup.GetPowerPhases());
            UIHelper.SetDataSourceToComboBox(cboCurrency, Lookup.GetCurrencies());

            _flag = flag;
            objDeposit._CopyTo(_objDeposit);
            objDeposit._CopyTo(_oldObjDeposit);

            if (flag == GeneralProcess.Insert)
            {
                _objDeposit.IS_ACTIVE = true;
                this.Text = string.Concat(Resources.INSERT, this.Text);
            }
            else if (flag == GeneralProcess.Update)
            {
                this.Text = string.Concat(Resources.UPDATE, this.Text);
            }
            else if (flag == GeneralProcess.Delete)
            {
                this.Text = string.Concat(Resources.DELETE, this.Text);
                UIHelper.SetEnabled(this, false);
            }

            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;

            read();
        }
        #endregion

        #region Operation
        /// <summary>
        /// Data transaction
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (this.inValid())
            {
                return;
            }

            this.write();

            //if duplicate record.
            this.ClearAllValidation();
            var lstDeposit = from d in DBDataContext.Db.TBL_DEPOSITs
                             where
                             d.AMPARE_ID == _objDeposit.AMPARE_ID
                             && d.PHASE_ID == _objDeposit.PHASE_ID
                             && d.CUSTOMER_TYPE_ID == _objDeposit.CUSTOMER_TYPE_ID
                             && d.CURRENCY_ID == _objDeposit.CURRENCY_ID
                             where d.IS_ACTIVE == true && d.DEPOSIT_AMOUNT_ID != _objDeposit.DEPOSIT_AMOUNT_ID
                             select d;
            if (lstDeposit.Count() > 0 && _flag != GeneralProcess.Delete)
            {
                this.cboAmpere.SetValidation(string.Format(Resources.MS_IS_EXISTS, this.lblAMPARE.Text));
                this.cboPhase.SetValidation(string.Format(Resources.MS_IS_EXISTS, this.lblPHASE.Text));
                this.cboCustomerGroup.SetValidation(string.Format(Resources.MS_IS_EXISTS, this.lblCUSTOMER_GROUP.Text));
                this.cboCurrency.SetValidation(string.Format(Resources.MS_IS_EXISTS, this.lblCURRENCY.Text));
                return;
            }

            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objDeposit);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_oldObjDeposit, _objDeposit);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objDeposit);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        #endregion

        #region Method

        /// <summary>
        /// Load data from object.
        /// </summary>
        private void read()
        {
            var groupId = DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs.FirstOrDefault(x => x.CUSTOMER_CONNECTION_TYPE_ID == _objDeposit.CUSTOMER_TYPE_ID);
            this.cboCustomerGroup.SelectedValue = groupId != null ? groupId.NONLICENSE_CUSTOMER_GROUP_ID : 0;
            this.cboCustomerConnectionType.SelectedValue = _objDeposit.CUSTOMER_TYPE_ID;
            this.cboAmpere.SelectedValue = _objDeposit.AMPARE_ID;
            this.cboPhase.SelectedValue = _objDeposit.PHASE_ID;
            this.cboCurrency.SelectedValue = _objDeposit.CURRENCY_ID;
            this.txtNewConnection.Text = _objDeposit.AMOUNT.ToString(DataHelper.MoneyFormat);
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            _objDeposit.CUSTOMER_TYPE_ID = (int)this.cboCustomerConnectionType.SelectedValue;
            _objDeposit.CURRENCY_ID = (int)cboCurrency.SelectedValue;
            _objDeposit.AMPARE_ID = (int)this.cboAmpere.SelectedValue;
            _objDeposit.IS_ACTIVE = true;
            _objDeposit.PHASE_ID = (int)this.cboPhase.SelectedValue;
            _objDeposit.AMOUNT = UIHelper.Round(DataHelper.ParseToDecimal(txtNewConnection.Text.Trim()), _objDeposit.CURRENCY_ID);
        }

        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();
            if (cboCustomerGroup.SelectedIndex == -1)
            {
                cboCustomerGroup.SetValidation(string.Format(Resources.REQUIRED, lblCUSTOMER_GROUP.Text));
                val = true;
            }
            if (cboCustomerConnectionType.SelectedIndex == -1)
            {
                cboCustomerConnectionType.SetValidation(string.Format(Resources.REQUIRED, lblCUSTOMER_CONNECTION_TYPE.Text));
                val = true;
            }
            if (this.cboAmpere.SelectedIndex == -1)
            {
                cboAmpere.SetValidation(string.Format(Resources.REQUIRED, lblAMPARE.Text));
                val = true;
            }
            if (this.cboPhase.SelectedIndex == -1)
            {
                cboPhase.SetValidation(string.Format(Resources.REQUIRED, lblPHASE.Text));
                val = true;
            }
            if (cboCurrency.SelectedIndex == -1)
            {
                cboCurrency.SetValidation(string.Format(Resources.REQUIRED, lblCURRENCY.Text));
                val = true;
            }
            if (txtNewConnection.Text.Trim() == "")
            {
                txtNewConnection.SetValidation(string.Format(Resources.REQUIRED, lblDEPOSIT.Text));
                val = true;
            }
            if (DataHelper.ParseToDecimal(txtNewConnection.Text.Trim()) < 0)
            {
                txtNewConnection.SetValidation(Resources.MS_AMOUNT_DEPOSIT_MUST_BE_GREATER_THAN_ZERO);
                val = true;
            }

            return val;
        }
        #endregion        

        #region Event

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objDeposit);
        }

        private void txtDepositAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputNumberOnly(sender, e);
        }

        #endregion Event

        private void cboCustomerGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboCustomerGroup.SelectedValue == null)
            {
                return;
            }
            int groupId = DataHelper.ParseToInt(cboCustomerGroup.SelectedValue.ToString());
            var dt = DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs
                       .OrderBy(x => x.DESCRIPTION)
                       .Where(x => x.NONLICENSE_CUSTOMER_GROUP_ID == groupId);
            UIHelper.SetDataSourceToComboBox(cboCustomerConnectionType, dt);

            if (cboCustomerConnectionType.DataSource != null)
            {
                cboCustomerConnectionType.SelectedIndex = 0;
            }
        }
    }
}