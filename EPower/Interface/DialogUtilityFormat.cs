﻿using System;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class DialogUtilityFormat : ExDialog
    {
        bool _loading = false;
        public DialogUtilityFormat(Formatting formatting)
        { 
            InitializeComponent();
            this.Formatting = formatting; 
        } 
       
        public Formatting Formatting
        {
            get
            {
                return new Formatting()
                {
                    DIGIT = DataHelper.ParseToInt(this.txtDIGIIT.Text),
                    START_NUMBER = DataHelper.ParseToInt(this.txtSTART_NUMBER.Text),
                    HEADER = this.txtHEADER.Text,
                    DISPLAY_YEAR = this.chkDISPLAY_YEAR.Checked
                }; 
            }
            set
            {
                this._loading = true;
                
                this.txtDIGIIT.Text = value.DIGIT.ToString();
                this.txtSTART_NUMBER.Text = value.START_NUMBER.ToString();
                this.txtHEADER.Text = value.HEADER;
                this.chkDISPLAY_YEAR.Checked = value.DISPLAY_YEAR;

                this._loading = false;
                updateSample();
            }
        }

        private void updateSample()
        {
            if (!_loading)
            {
                this.txtSAMPLE.Text = Formatting.ToString();
            }
        }

        private void txtSTART_TextChanged(object sender, EventArgs e)
        {
            updateSample();
        }

        private void txtDIGIIT_TextChanged(object sender, EventArgs e)
        {
            updateSample();
        }

        private void txtHEADER_TextChanged(object sender, EventArgs e)
        {
            updateSample(); 
        }

        private void chkSHOWYEAR_CheckedChanged(object sender, EventArgs e)
        {
            updateSample();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (this.txtHEADER.Text == "")
            {
                this.txtHEADER.SetValidation(string.Format(Resources.REQUIRED, this.lblHEADER.Text));
                return;
            }
            if (!DataHelper.IsInteger(this.txtDIGIIT.Text))
            {
                this.txtDIGIIT.SetValidation(string.Format(Resources.REQUIRED_INPUT_NUMBER, this.lblDIGIT.Text));
                return;
            }
            this.DialogResult = DialogResult.OK;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            
            this.DialogResult = DialogResult.Cancel;
        }

        private void txtSAMPLE_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
    } 
    public class Formatting
    {
        public int DIGIT;
        public int START_NUMBER;
        public bool DISPLAY_YEAR;
        public string HEADER;

        public override string ToString()
        {
            return HEADER
                   + (DISPLAY_YEAR ? DateTime.Now.Year.ToString().Substring(2) : "")
                   + "-"
                   + 123.ToString(new string('0', DIGIT));
        }
    }
}
