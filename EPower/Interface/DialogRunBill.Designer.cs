﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogRunBill
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMONTH = new System.Windows.Forms.Label();
            this.lblDUE_DATE = new System.Windows.Forms.Label();
            this.lblSTART_DATE = new System.Windows.Forms.Label();
            this.dtpRunMonth = new System.Windows.Forms.DateTimePicker();
            this.dtpFrom = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpTo = new System.Windows.Forms.DateTimePicker();
            this.dtpDueDate = new System.Windows.Forms.DateTimePicker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnRUN = new SoftTech.Component.ExButton();
            this.lblCYCLE_NAME = new System.Windows.Forms.Label();
            this.lblPAY_DATE = new System.Windows.Forms.Label();
            this.dtpSTART_PAY_DATE = new System.Windows.Forms.DateTimePicker();
            this.lblNOTE = new System.Windows.Forms.Label();
            this.lblTOTAL_WORK_DAY = new System.Windows.Forms.Label();
            this.lblTOTAL_HOLIDAY = new System.Windows.Forms.Label();
            this.dtpInvoice_Date = new System.Windows.Forms.DateTimePicker();
            this.lblINVOICE_DATE_BILLING = new System.Windows.Forms.Label();
            this.lblEXCHANGE_RATE = new System.Windows.Forms.Label();
            this.txtCYCLE_NAME = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.txtCYCLE_NAME);
            this.content.Controls.Add(this.lblEXCHANGE_RATE);
            this.content.Controls.Add(this.dtpInvoice_Date);
            this.content.Controls.Add(this.lblINVOICE_DATE_BILLING);
            this.content.Controls.Add(this.dtpSTART_PAY_DATE);
            this.content.Controls.Add(this.lblTOTAL_HOLIDAY);
            this.content.Controls.Add(this.lblTOTAL_WORK_DAY);
            this.content.Controls.Add(this.lblNOTE);
            this.content.Controls.Add(this.lblPAY_DATE);
            this.content.Controls.Add(this.lblCYCLE_NAME);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnRUN);
            this.content.Controls.Add(this.dtpDueDate);
            this.content.Controls.Add(this.dtpTo);
            this.content.Controls.Add(this.dtpFrom);
            this.content.Controls.Add(this.dtpRunMonth);
            this.content.Controls.Add(this.lblSTART_DATE);
            this.content.Controls.Add(this.lblDUE_DATE);
            this.content.Controls.Add(this.lblMONTH);
            this.content.Controls.Add(this.label3);
            this.content.Size = new System.Drawing.Size(410, 310);
            this.content.TabIndex = 0;
            this.content.Text = "\'CONTENT\'";
            this.content.Controls.SetChildIndex(this.label3, 0);
            this.content.Controls.SetChildIndex(this.lblMONTH, 0);
            this.content.Controls.SetChildIndex(this.lblDUE_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblSTART_DATE, 0);
            this.content.Controls.SetChildIndex(this.dtpRunMonth, 0);
            this.content.Controls.SetChildIndex(this.dtpFrom, 0);
            this.content.Controls.SetChildIndex(this.dtpTo, 0);
            this.content.Controls.SetChildIndex(this.dtpDueDate, 0);
            this.content.Controls.SetChildIndex(this.btnRUN, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.lblCYCLE_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblPAY_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblNOTE, 0);
            this.content.Controls.SetChildIndex(this.lblTOTAL_WORK_DAY, 0);
            this.content.Controls.SetChildIndex(this.lblTOTAL_HOLIDAY, 0);
            this.content.Controls.SetChildIndex(this.dtpSTART_PAY_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblINVOICE_DATE_BILLING, 0);
            this.content.Controls.SetChildIndex(this.dtpInvoice_Date, 0);
            this.content.Controls.SetChildIndex(this.lblEXCHANGE_RATE, 0);
            this.content.Controls.SetChildIndex(this.txtCYCLE_NAME, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            // 
            // lblMONTH
            // 
            this.lblMONTH.AutoSize = true;
            this.lblMONTH.Location = new System.Drawing.Point(9, 27);
            this.lblMONTH.Margin = new System.Windows.Forms.Padding(2);
            this.lblMONTH.Name = "lblMONTH";
            this.lblMONTH.Size = new System.Drawing.Size(45, 19);
            this.lblMONTH.TabIndex = 6;
            this.lblMONTH.Text = "ប្រចាំខែ";
            // 
            // lblDUE_DATE
            // 
            this.lblDUE_DATE.AutoSize = true;
            this.lblDUE_DATE.Location = new System.Drawing.Point(9, 151);
            this.lblDUE_DATE.Margin = new System.Windows.Forms.Padding(2);
            this.lblDUE_DATE.Name = "lblDUE_DATE";
            this.lblDUE_DATE.Size = new System.Drawing.Size(70, 19);
            this.lblDUE_DATE.TabIndex = 7;
            this.lblDUE_DATE.Text = "ថ្ងៃផុតកំណត់";
            // 
            // lblSTART_DATE
            // 
            this.lblSTART_DATE.AutoSize = true;
            this.lblSTART_DATE.Location = new System.Drawing.Point(9, 89);
            this.lblSTART_DATE.Margin = new System.Windows.Forms.Padding(2);
            this.lblSTART_DATE.Name = "lblSTART_DATE";
            this.lblSTART_DATE.Size = new System.Drawing.Size(49, 19);
            this.lblSTART_DATE.TabIndex = 8;
            this.lblSTART_DATE.Text = "ចាប់ពីថ្ងៃ";
            // 
            // dtpRunMonth
            // 
            this.dtpRunMonth.CustomFormat = "MM-yyyy";
            this.dtpRunMonth.Enabled = false;
            this.dtpRunMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpRunMonth.Location = new System.Drawing.Point(158, 23);
            this.dtpRunMonth.Margin = new System.Windows.Forms.Padding(2);
            this.dtpRunMonth.Name = "dtpRunMonth";
            this.dtpRunMonth.Size = new System.Drawing.Size(233, 27);
            this.dtpRunMonth.TabIndex = 0;
            // 
            // dtpFrom
            // 
            this.dtpFrom.CustomFormat = "dd-MM-yyyy";
            this.dtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFrom.Location = new System.Drawing.Point(158, 85);
            this.dtpFrom.Margin = new System.Windows.Forms.Padding(2);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(102, 27);
            this.dtpFrom.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(158, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 19);
            this.label3.TabIndex = 15;
            this.label3.Text = "ដល់";
            // 
            // dtpTo
            // 
            this.dtpTo.CustomFormat = "dd-MM-yyyy";
            this.dtpTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTo.Location = new System.Drawing.Point(289, 85);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(102, 27);
            this.dtpTo.TabIndex = 3;
            // 
            // dtpDueDate
            // 
            this.dtpDueDate.CustomFormat = "dd-MM-yyyy";
            this.dtpDueDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDueDate.Location = new System.Drawing.Point(158, 147);
            this.dtpDueDate.Margin = new System.Windows.Forms.Padding(2);
            this.dtpDueDate.Name = "dtpDueDate";
            this.dtpDueDate.Size = new System.Drawing.Size(233, 27);
            this.dtpDueDate.TabIndex = 5;
            this.dtpDueDate.ValueChanged += new System.EventHandler(this.dtpDueDate_ValueChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Location = new System.Drawing.Point(1, 268);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(409, 1);
            this.panel1.TabIndex = 24;
            // 
            // btnCLOSE
            // 
            this.btnCLOSE.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnCLOSE.Location = new System.Drawing.Point(329, 275);
            this.btnCLOSE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.Size = new System.Drawing.Size(75, 23);
            this.btnCLOSE.TabIndex = 9;
            this.btnCLOSE.Text = "បោះបង់";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnRUN
            // 
            this.btnRUN.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnRUN.Location = new System.Drawing.Point(250, 275);
            this.btnRUN.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRUN.Name = "btnRUN";
            this.btnRUN.Size = new System.Drawing.Size(75, 23);
            this.btnRUN.TabIndex = 8;
            this.btnRUN.Text = "តំណើរការ";
            this.btnRUN.UseVisualStyleBackColor = true;
            this.btnRUN.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblCYCLE_NAME
            // 
            this.lblCYCLE_NAME.AutoSize = true;
            this.lblCYCLE_NAME.Location = new System.Drawing.Point(9, 58);
            this.lblCYCLE_NAME.Margin = new System.Windows.Forms.Padding(2);
            this.lblCYCLE_NAME.Name = "lblCYCLE_NAME";
            this.lblCYCLE_NAME.Size = new System.Drawing.Size(45, 19);
            this.lblCYCLE_NAME.TabIndex = 25;
            this.lblCYCLE_NAME.Text = "ជុំទូទាត់";
            // 
            // lblPAY_DATE
            // 
            this.lblPAY_DATE.AutoSize = true;
            this.lblPAY_DATE.Location = new System.Drawing.Point(9, 182);
            this.lblPAY_DATE.Margin = new System.Windows.Forms.Padding(2);
            this.lblPAY_DATE.Name = "lblPAY_DATE";
            this.lblPAY_DATE.Size = new System.Drawing.Size(113, 19);
            this.lblPAY_DATE.TabIndex = 27;
            this.lblPAY_DATE.Text = "ថ្ងៃចាប់ផ្តើមទទួលប្រាក់";
            // 
            // dtpSTART_PAY_DATE
            // 
            this.dtpSTART_PAY_DATE.CustomFormat = "dd-MM-yyyy";
            this.dtpSTART_PAY_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSTART_PAY_DATE.Location = new System.Drawing.Point(158, 178);
            this.dtpSTART_PAY_DATE.Margin = new System.Windows.Forms.Padding(2);
            this.dtpSTART_PAY_DATE.Name = "dtpSTART_PAY_DATE";
            this.dtpSTART_PAY_DATE.Size = new System.Drawing.Size(233, 27);
            this.dtpSTART_PAY_DATE.TabIndex = 6;
            this.dtpSTART_PAY_DATE.ValueChanged += new System.EventHandler(this.dtpDueDate_ValueChanged);
            // 
            // lblNOTE
            // 
            this.lblNOTE.AutoSize = true;
            this.lblNOTE.Location = new System.Drawing.Point(9, 209);
            this.lblNOTE.Margin = new System.Windows.Forms.Padding(2);
            this.lblNOTE.Name = "lblNOTE";
            this.lblNOTE.Size = new System.Drawing.Size(45, 19);
            this.lblNOTE.TabIndex = 27;
            this.lblNOTE.Text = "សំគាល់";
            // 
            // lblTOTAL_WORK_DAY
            // 
            this.lblTOTAL_WORK_DAY.AutoSize = true;
            this.lblTOTAL_WORK_DAY.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.lblTOTAL_WORK_DAY.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblTOTAL_WORK_DAY.Location = new System.Drawing.Point(158, 209);
            this.lblTOTAL_WORK_DAY.Margin = new System.Windows.Forms.Padding(2);
            this.lblTOTAL_WORK_DAY.Name = "lblTOTAL_WORK_DAY";
            this.lblTOTAL_WORK_DAY.Size = new System.Drawing.Size(73, 19);
            this.lblTOTAL_WORK_DAY.TabIndex = 28;
            this.lblTOTAL_WORK_DAY.Text = "ចំនួនថ្ងៃធ្វើការ";
            // 
            // lblTOTAL_HOLIDAY
            // 
            this.lblTOTAL_HOLIDAY.AutoSize = true;
            this.lblTOTAL_HOLIDAY.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblTOTAL_HOLIDAY.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Underline);
            this.lblTOTAL_HOLIDAY.Location = new System.Drawing.Point(158, 232);
            this.lblTOTAL_HOLIDAY.Margin = new System.Windows.Forms.Padding(2);
            this.lblTOTAL_HOLIDAY.Name = "lblTOTAL_HOLIDAY";
            this.lblTOTAL_HOLIDAY.Size = new System.Drawing.Size(98, 19);
            this.lblTOTAL_HOLIDAY.TabIndex = 7;
            this.lblTOTAL_HOLIDAY.Text = "ចំនួនថ្ងៃឈប់សំរាក";
            this.lblTOTAL_HOLIDAY.Click += new System.EventHandler(this.lblTotalHoliday_Click);
            // 
            // dtpInvoice_Date
            // 
            this.dtpInvoice_Date.CustomFormat = "dd-MM-yyyy";
            this.dtpInvoice_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpInvoice_Date.Location = new System.Drawing.Point(158, 116);
            this.dtpInvoice_Date.Margin = new System.Windows.Forms.Padding(2);
            this.dtpInvoice_Date.Name = "dtpInvoice_Date";
            this.dtpInvoice_Date.Size = new System.Drawing.Size(233, 27);
            this.dtpInvoice_Date.TabIndex = 4;
            this.dtpInvoice_Date.ValueChanged += new System.EventHandler(this.dtpInvoice_Date_ValueChanged);
            this.dtpInvoice_Date.Enter += new System.EventHandler(this.dtpInvoice_Date_Enter);
            // 
            // lblINVOICE_DATE_BILLING
            // 
            this.lblINVOICE_DATE_BILLING.AutoSize = true;
            this.lblINVOICE_DATE_BILLING.Location = new System.Drawing.Point(9, 120);
            this.lblINVOICE_DATE_BILLING.Margin = new System.Windows.Forms.Padding(2);
            this.lblINVOICE_DATE_BILLING.Name = "lblINVOICE_DATE_BILLING";
            this.lblINVOICE_DATE_BILLING.Size = new System.Drawing.Size(89, 19);
            this.lblINVOICE_DATE_BILLING.TabIndex = 31;
            this.lblINVOICE_DATE_BILLING.Text = "ថ្ងៃចេញវិក្កយបត្រ";
            // 
            // lblEXCHANGE_RATE
            // 
            this.lblEXCHANGE_RATE.AutoSize = true;
            this.lblEXCHANGE_RATE.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblEXCHANGE_RATE.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Underline);
            this.lblEXCHANGE_RATE.Location = new System.Drawing.Point(306, 232);
            this.lblEXCHANGE_RATE.Margin = new System.Windows.Forms.Padding(2);
            this.lblEXCHANGE_RATE.Name = "lblEXCHANGE_RATE";
            this.lblEXCHANGE_RATE.Size = new System.Drawing.Size(69, 19);
            this.lblEXCHANGE_RATE.TabIndex = 32;
            this.lblEXCHANGE_RATE.Text = "អត្រាប្តូរប្រាក់";
            this.lblEXCHANGE_RATE.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblEXCHANGE_RATE.Click += new System.EventHandler(this.lblEXCHANGE_RATE_Click);
            // 
            // txtCYCLE_NAME
            // 
            this.txtCYCLE_NAME.Enabled = false;
            this.txtCYCLE_NAME.Location = new System.Drawing.Point(158, 54);
            this.txtCYCLE_NAME.Name = "txtCYCLE_NAME";
            this.txtCYCLE_NAME.Size = new System.Drawing.Size(233, 27);
            this.txtCYCLE_NAME.TabIndex = 33;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label9.Location = new System.Drawing.Point(95, 119);
            this.label9.Margin = new System.Windows.Forms.Padding(1);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(15, 20);
            this.label9.TabIndex = 70;
            this.label9.Text = "*";
            // 
            // DialogRunBill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(410, 333);
            this.Name = "DialogRunBill";
            this.Text = "ចេញវិក្កយបត្រ";
            this.Load += new System.EventHandler(this.DialogRunBill_Load);
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Label lblSTART_DATE;
        private Label lblDUE_DATE;
        private Label lblMONTH;
        private Label label3;
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnRUN;
        public DateTimePicker dtpDueDate;
        public DateTimePicker dtpTo;
        public DateTimePicker dtpFrom;
        public DateTimePicker dtpRunMonth;
        private Label lblCYCLE_NAME;
        public DateTimePicker dtpSTART_PAY_DATE;
        private Label lblPAY_DATE;
        private Label lblNOTE;
        private Label lblTOTAL_HOLIDAY;
        private Label lblTOTAL_WORK_DAY;
        public DateTimePicker dtpInvoice_Date;
        private Label lblINVOICE_DATE_BILLING;
        private Label lblEXCHANGE_RATE;
        public TextBox txtCYCLE_NAME;
        private Label label9;
    }
}