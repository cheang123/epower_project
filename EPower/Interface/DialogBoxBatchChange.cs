﻿using EPower.Base.Logic;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using Login = SoftTech.Security.Logic.Login;

namespace EPower.Interface
{
    public partial class DialogBoxBatchChange : ExDialog
    {

        List<int> boxs = new List<int>();

        #region Constructor
        public DialogBoxBatchChange()
        {
            InitializeComponent();
            btnAddPole.Enabled = Login.IsAuthorized(Permission.ADMIN_POLEANDBOX);
            dataLockup();
        }
        #endregion

        #region Method
        private void dataLockup()
        {
            try
            {
                UIHelper.SetDataSourceToComboBox(cboPole, Lookup.GetPoles(0), "");
                UIHelper.SetDataSourceToComboBox(cboStatus, Lookup.GetBoxStatus(), "");
                UIHelper.SetDataSourceToComboBox(cboBoxType, Lookup.GetBoxType(), "");
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }
        public void SetBox(List<int> box)
        {
            this.boxs = box;
            this.lblPole_.Text = boxs.Count.ToString("0");
        }
        private bool invalid()
        {
            bool val = false;

            if ((int)cboStatus.SelectedValue != (int)BoxStatus.Stock)
            {

            }
            return val;
        }

        private void updateInfo()
        {
            using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
            {
                int i = 0;
                foreach (var boxId in this.boxs)
                {
                    TBL_BOX objBox = DBDataContext.Db.TBL_BOXes.FirstOrDefault(x => x.BOX_ID == boxId);
                    TBL_BOX objOldBox = new TBL_BOX();
                    objBox._CopyTo(objOldBox);

                    if ((int)cboPole.SelectedValue != 0)
                    {
                        objBox.POLE_ID = (int)cboPole.SelectedValue;
                    }
                    if ((int)cboBoxType.SelectedValue != 0)
                    {
                        objBox.BOX_TYPE_ID = (int)cboBoxType.SelectedValue;
                    }
                    if (dtpStartDate.Checked)
                    {
                        objBox.STARTED_DATE = dtpStartDate.Value;
                    }
                    if (dtpEndDate.Checked)
                    {
                        objBox.END_DATE = dtpEndDate.Value;
                    }
                    if ((int)cboStatus.SelectedValue != 0)
                    {
                        objBox.STATUS_ID = (int)cboStatus.SelectedValue;
                    }

                    DBDataContext.Db.Update(objOldBox, objBox);
                    Application.DoEvents();
                    Runner.Instance.Text = string.Format("កំពុងកែប្រែព័ត៌មាន....{0}%", (100 * i++ / boxs.Count));
                }
                success = true;
                tran.Complete();
            }
        }
        #endregion
        bool success = false;
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (invalid()) return;
            success = false;
            Runner.Run(updateInfo);
            if (success)
            {
                this.DialogResult = DialogResult.OK;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void btnAddPole_AddItem(object sender, EventArgs e)
        {
            DialogPole dig = new DialogPole(GeneralProcess.Insert, new TBL_POLE() { POLE_CODE = "", STARTED_DATE = DBDataContext.Db.GetSystemDate(), END_DATE = UIHelper._DefaultDate });
            if (dig.ShowDialog() == DialogResult.OK)
            {
                UIHelper.SetDataSourceToComboBox(cboPole,
                    from p in DBDataContext.Db.TBL_POLEs
                    where p.IS_ACTIVE
                    select new
                    {
                        p.POLE_ID,
                        p.POLE_CODE
                    });
                cboPole.SelectedValue = dig.Pole.POLE_ID;
            }
        }

        private void dtpEndDate_MouseDown(object sender, MouseEventArgs e)
        {
            if (dtpEndDate.Checked)
            {
                if (dtpEndDate.Value.Date.Equals(UIHelper._DefaultDate))
                {
                    dtpEndDate.SetValue(DBDataContext.Db.GetSystemDate());
                }
                else
                {
                    dtpEndDate.CustomFormat = UIHelper._DefaultDateFormat;
                }
            }
            else
            {
                dtpEndDate.ClearValue();
            }
        }

        private void btnBrowse__Click(object sender, EventArgs e)
        {
            new DialogPageBoxType().ShowDialog();
        }

    }
}
