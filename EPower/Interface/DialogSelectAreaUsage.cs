﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class DialogSelectAreaUsage : ExDialog
    {

        private List<TBL_AREA> mSelectedAreas = new List<TBL_AREA>();
        public List<TBL_AREA> SelectedAreas
        {
            get { return mSelectedAreas; }
            private set { mSelectedAreas = value; }
        }

        public int TotalCustomers { get; private set; }

        public string DisplaySelected
        {
            get
            {
                if (SelectedAreas.Count == 0)
                {
                    return "";
                }
                else if (SelectedAreas.Count == dgv.Rows.Count)
                {
                    return Resources.ALL_AREA;
                }
                else
                {
                    return string.Join(", ", SelectedAreas.Select(x => x.AREA_CODE).ToArray());
                }
            }
        }

        DataTable dtCusArea = null;
        #region Constructor
        public DialogSelectAreaUsage(List<TBL_AREA> areaId)
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);

            dgv.ReadOnly = false;
            AREA.ReadOnly = true;
            TOTAL_CUSTOMER.ReadOnly = true;
            SELECT_.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            mSelectedAreas = areaId;
            lblCount.TextChanged += lblCount_TextChanged;

        }

        void lblCount_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(lblCount.Text))
            {
                return;
            }
            TotalCustomers = DataHelper.ParseToInt(lblCount.Text);
        }
        #endregion Constructor

        #region Method
        CheckBox ckBox = null;

        void addCheckBoxHeader()
        {
            ckBox = new CheckBox();
            //Get the column header cell bounds
            Rectangle rect = this.dgv.GetCellDisplayRectangle(0, -1, false);
            ckBox.Size = new Size(16, 16);
            Point oPoint = new Point();
            oPoint.X = rect.Location.X + 2;
            oPoint.Y = rect.Location.Y + (rect.Height - ckBox.Height) / 2 + 1;
            if (oPoint.X < rect.X)
            {
                ckBox.Visible = false;
            }
            else
            {
                ckBox.Visible = true;
            }
            //Change the location of the CheckBox to make it stay on the header
            ckBox.Location = oPoint;
            ckBox.CheckedChanged += new EventHandler(ckBox_CheckedChanged);

            dgv.Controls.Add(ckBox);
        }

        void ckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ckBox.CheckState == CheckState.Indeterminate)
            {
                return;
            }

            foreach (DataRow dr in this.dtCusArea.Rows)
            {
               dr[SELECT_.Name] = ckBox.Checked;   
            }
            dgv.EndEdit();
            dgv.NotifyCurrentCellDirty(true);
            dgv.Refresh();

            //dgv.NotifyCurrentCellDirty(false);
            dgv_DataSourceChanged(dgv, EventArgs.Empty);


        }
        void Bind()
        {
            dtCusArea = (from a in DBDataContext.Db.TBL_AREAs
                         join c in DBDataContext.Db.TBL_CUSTOMERs on a.AREA_ID equals c.AREA_ID
                         where (c.STATUS_ID == (int)CustomerStatus.Active || c.STATUS_ID == (int)CustomerStatus.Blocked) && a.IS_ACTIVE
                         group c by new { a.AREA_ID, a.AREA_CODE } into gArea
                         select new
                         {
                             SELECT_ = true,
                             gArea.Key.AREA_ID,
                             gArea.Key.AREA_CODE,
                             TOTAL_CUSTOMER = gArea.Count()
                         })._ToDataTable();
            if (mSelectedAreas.Count > 0)
            {
                foreach (DataRow item in dtCusArea.Rows)
                {
                    item[SELECT_.Name] = mSelectedAreas.FirstOrDefault(x => x.AREA_ID == (int)item[AREA_ID.Name]) != null;
                }
            }
            dgv.DataSource = dtCusArea;
        }

        #endregion Method 

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgv_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            //if (e.ColumnIndex != SELECT_.Index || e.RowIndex == -1)
            //{
            //    return;
            //}
            //dgv_DataSourceChanged(dgv, e);
        }

        private void dgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1 || e.ColumnIndex == -1)
            {
                return;
            }

            var rowView = dgv.Rows[e.RowIndex].DataBoundItem as DataRowView;
            rowView.Row[SELECT_.Name] = !(bool)rowView.Row[SELECT_.Name];


            //if (e.ColumnIndex != SELECT_.Index || e.RowIndex == -1)
            //{
            //    return;
            //}
            //DataGridViewCell cell = dgv[e.ColumnIndex, e.RowIndex];
            //cell.Value = ! (bool)cell.Value;
            dgv.EndEdit();
            dgv.NotifyCurrentCellDirty(true);
            dgv.Refresh();

            dgv_DataSourceChanged(dgv, EventArgs.Empty);
        }

        private void dgv_DataSourceChanged(object sender, EventArgs e)
        {
            int nCus = 0;
            int nRecord = 0;
            foreach (var item in dtCusArea.Select().Where(x => (bool)x[SELECT_.Name] == true))
            {
                nCus += (int)item[TOTAL_CUSTOMER.Name];
                nRecord++;
            }
            lblCount.Text = nCus.ToString();
            if (ckBox == null)
            {
                return;
            }
            if (nRecord == dtCusArea.Rows.Count)
            {
                ckBox.CheckState = CheckState.Checked;
            }
            else if (nRecord > 0)
            {
                ckBox.CheckState = CheckState.Indeterminate;
            }
            else
            {
                ckBox.CheckState = CheckState.Unchecked;
            }
        }

        private void DialogSelectAreaUsage_FormClosing(object sender, FormClosingEventArgs e)
        {
            mSelectedAreas = new List<TBL_AREA>();
            foreach (var item in dtCusArea.Select().Where(x => (bool)x[SELECT_.Name]))
            {
                mSelectedAreas.Add(DBDataContext.Db.TBL_AREAs.FirstOrDefault(x => x.AREA_ID == (int)item[AREA_ID.Name]));
            }

            if (mSelectedAreas.Count == 0)
            {
                MsgBox.ShowWarning(Resources.MS_PLEASE_SELECT_ONE_AREA_FOR_COLLECT_CUSTOMER_USAGE, this.Text);
                e.Cancel = true;
            }



        }

        private void DialogSelectAreaUsage_Load(object sender, EventArgs e)
        {
            Bind();
            addCheckBoxHeader();
            int nRecord = dtCusArea.Select().Where(x => (bool)x[SELECT_.Name]).Count();
            if (ckBox == null)
            {
                return;
            }
            if (nRecord == dtCusArea.Rows.Count)
            {
                ckBox.CheckState = CheckState.Checked;
            }
            else if (nRecord > 0)
            {
                ckBox.CheckState = CheckState.Indeterminate;
            }
            else
            {
                ckBox.CheckState = CheckState.Unchecked;
            }
        }

        private void TxtSearch_QuickSearch(object sender, EventArgs e)
        {

            string search = txtSearch.Text.ToLower();
            if (search == null || search == "")
            {
                ckBox.Visible = true;
                foreach (DataGridViewRow row in this.dgv.Rows)
                {
                    CurrencyManager currencyManager1 = (CurrencyManager)BindingContext[dgv.DataSource];
                    currencyManager1.SuspendBinding();
                    row.Visible = true;
                    currencyManager1.ResumeBinding();
                    
                }
            }
            else
            {
                ckBox.Visible = false;
                foreach (DataGridViewRow row in this.dgv.Rows)
                {
                    //row.Visible = row.Cells[this.AREA.Name].Value.ToString().ToLower().Contains(search);
                    
                        if (!row.Cells[this.AREA.Name].Value.ToString().ToLower().Contains(search))
                    {
                        if (ckBox.CheckState == CheckState.Checked)
                        {
                            foreach (DataRow dr in this.dtCusArea.Rows)
                            {
                                dr[SELECT_.Name] = ckBox.Checked = false;
                            }
                        }
                        CurrencyManager currencyManager1 = (CurrencyManager)BindingContext[dgv.DataSource];
                        currencyManager1.SuspendBinding();
                        row.Visible = false;
                        currencyManager1.ResumeBinding();
                    }

                }
            }
        }
    }
}
