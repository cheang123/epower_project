﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogStaffExpense
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogStaffExpense));
            this.lblMONTH = new System.Windows.Forms.Label();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.lblAMOUNT = new System.Windows.Forms.Label();
            this.lblPAYMENT_ACCOUNT = new System.Windows.Forms.Label();
            this.dtpExpenseDate = new System.Windows.Forms.DateTimePicker();
            this.cboAccountPayment = new SoftTech.Component.TreeComboBox();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.dgvStaffExpense = new System.Windows.Forms.DataGridView();
            this.EXPENSE_DETAIL_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_ACTIVE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_PAYROLL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.POSITION_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DIVISION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.POSITION_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_STAFF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BASE_SALARY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OVER_TIME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SOCAIL_SECURITY_EXPENSE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BENEFIT_AND_BONUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FOOD_AND_OTHER_ALLOWANCE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OTHER_BONUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStaffExpense)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.cboCurrency);
            this.content.Controls.Add(this.dgvStaffExpense);
            this.content.Controls.Add(this.cboAccountPayment);
            this.content.Controls.Add(this.dtpExpenseDate);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.txtTotal);
            this.content.Controls.Add(this.lblPAYMENT_ACCOUNT);
            this.content.Controls.Add(this.lblAMOUNT);
            this.content.Controls.Add(this.lblMONTH);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblMONTH, 0);
            this.content.Controls.SetChildIndex(this.lblAMOUNT, 0);
            this.content.Controls.SetChildIndex(this.lblPAYMENT_ACCOUNT, 0);
            this.content.Controls.SetChildIndex(this.txtTotal, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.dtpExpenseDate, 0);
            this.content.Controls.SetChildIndex(this.cboAccountPayment, 0);
            this.content.Controls.SetChildIndex(this.dgvStaffExpense, 0);
            this.content.Controls.SetChildIndex(this.cboCurrency, 0);
            // 
            // lblMONTH
            // 
            resources.ApplyResources(this.lblMONTH, "lblMONTH");
            this.lblMONTH.Name = "lblMONTH";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // lblAMOUNT
            // 
            resources.ApplyResources(this.lblAMOUNT, "lblAMOUNT");
            this.lblAMOUNT.Name = "lblAMOUNT";
            // 
            // lblPAYMENT_ACCOUNT
            // 
            resources.ApplyResources(this.lblPAYMENT_ACCOUNT, "lblPAYMENT_ACCOUNT");
            this.lblPAYMENT_ACCOUNT.Name = "lblPAYMENT_ACCOUNT";
            // 
            // dtpExpenseDate
            // 
            resources.ApplyResources(this.dtpExpenseDate, "dtpExpenseDate");
            this.dtpExpenseDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpExpenseDate.Name = "dtpExpenseDate";
            // 
            // cboAccountPayment
            // 
            this.cboAccountPayment.AbsoluteChildrenSelectableOnly = false;
            this.cboAccountPayment.BranchSeparator = "/";
            this.cboAccountPayment.Imagelist = null;
            resources.ApplyResources(this.cboAccountPayment, "cboAccountPayment");
            this.cboAccountPayment.Name = "cboAccountPayment";
            this.cboAccountPayment.PopupHeight = 250;
            this.cboAccountPayment.PopupWidth = 200;
            this.cboAccountPayment.SelectedNode = null;
            // 
            // txtTotal
            // 
            resources.ApplyResources(this.txtTotal, "txtTotal");
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.ReadOnly = true;
            this.txtTotal.Enter += new System.EventHandler(this.txtCAPACITY_Enter);
            // 
            // dgvStaffExpense
            // 
            this.dgvStaffExpense.AllowUserToAddRows = false;
            this.dgvStaffExpense.AllowUserToDeleteRows = false;
            this.dgvStaffExpense.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvStaffExpense.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvStaffExpense.BackgroundColor = System.Drawing.Color.White;
            this.dgvStaffExpense.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvStaffExpense.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvStaffExpense.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvStaffExpense.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EXPENSE_DETAIL_ID,
            this.IS_ACTIVE,
            this.IS_PAYROLL,
            this.POSITION_ID,
            this.DIVISION,
            this.POSITION_NAME,
            this.TOTAL_STAFF,
            this.BASE_SALARY,
            this.OVER_TIME,
            this.SOCAIL_SECURITY_EXPENSE,
            this.BENEFIT_AND_BONUS,
            this.FOOD_AND_OTHER_ALLOWANCE,
            this.OTHER_BONUS,
            this.TOTAL});
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvStaffExpense.DefaultCellStyle = dataGridViewCellStyle10;
            this.dgvStaffExpense.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvStaffExpense.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgvStaffExpense, "dgvStaffExpense");
            this.dgvStaffExpense.MultiSelect = false;
            this.dgvStaffExpense.Name = "dgvStaffExpense";
            this.dgvStaffExpense.RowHeadersVisible = false;
            this.dgvStaffExpense.RowTemplate.Height = 25;
            this.dgvStaffExpense.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvStaffExpense.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvStaffExpense_CellEndEdit);
            this.dgvStaffExpense.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvStaffExpense_CellEnter);
            this.dgvStaffExpense.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvStaffExpense_DataError);
            // 
            // EXPENSE_DETAIL_ID
            // 
            this.EXPENSE_DETAIL_ID.DataPropertyName = "EXPENSE_DETAIL_ID";
            resources.ApplyResources(this.EXPENSE_DETAIL_ID, "EXPENSE_DETAIL_ID");
            this.EXPENSE_DETAIL_ID.Name = "EXPENSE_DETAIL_ID";
            // 
            // IS_ACTIVE
            // 
            this.IS_ACTIVE.DataPropertyName = "IS_ACTIVE";
            resources.ApplyResources(this.IS_ACTIVE, "IS_ACTIVE");
            this.IS_ACTIVE.Name = "IS_ACTIVE";
            // 
            // IS_PAYROLL
            // 
            this.IS_PAYROLL.DataPropertyName = "IS_PAYROLL";
            resources.ApplyResources(this.IS_PAYROLL, "IS_PAYROLL");
            this.IS_PAYROLL.Name = "IS_PAYROLL";
            // 
            // POSITION_ID
            // 
            this.POSITION_ID.DataPropertyName = "POSITION_ID";
            resources.ApplyResources(this.POSITION_ID, "POSITION_ID");
            this.POSITION_ID.Name = "POSITION_ID";
            // 
            // DIVISION
            // 
            this.DIVISION.DataPropertyName = "DIVISION";
            resources.ApplyResources(this.DIVISION, "DIVISION");
            this.DIVISION.Name = "DIVISION";
            this.DIVISION.ReadOnly = true;
            // 
            // POSITION_NAME
            // 
            this.POSITION_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.POSITION_NAME.DataPropertyName = "POSITION_NAME";
            resources.ApplyResources(this.POSITION_NAME, "POSITION_NAME");
            this.POSITION_NAME.Name = "POSITION_NAME";
            this.POSITION_NAME.ReadOnly = true;
            // 
            // TOTAL_STAFF
            // 
            this.TOTAL_STAFF.DataPropertyName = "TOTAL_STAFF";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Format = "#;-#; ;";
            dataGridViewCellStyle2.NullValue = null;
            this.TOTAL_STAFF.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.TOTAL_STAFF, "TOTAL_STAFF");
            this.TOTAL_STAFF.Name = "TOTAL_STAFF";
            // 
            // BASE_SALARY
            // 
            this.BASE_SALARY.DataPropertyName = "S6010";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "#,###.####";
            dataGridViewCellStyle3.NullValue = null;
            this.BASE_SALARY.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.BASE_SALARY, "BASE_SALARY");
            this.BASE_SALARY.Name = "BASE_SALARY";
            // 
            // OVER_TIME
            // 
            this.OVER_TIME.DataPropertyName = "S6020";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "#,###.####";
            dataGridViewCellStyle4.NullValue = null;
            this.OVER_TIME.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.OVER_TIME, "OVER_TIME");
            this.OVER_TIME.Name = "OVER_TIME";
            // 
            // SOCAIL_SECURITY_EXPENSE
            // 
            this.SOCAIL_SECURITY_EXPENSE.DataPropertyName = "S6040";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "#,###.####";
            dataGridViewCellStyle5.NullValue = null;
            this.SOCAIL_SECURITY_EXPENSE.DefaultCellStyle = dataGridViewCellStyle5;
            resources.ApplyResources(this.SOCAIL_SECURITY_EXPENSE, "SOCAIL_SECURITY_EXPENSE");
            this.SOCAIL_SECURITY_EXPENSE.Name = "SOCAIL_SECURITY_EXPENSE";
            // 
            // BENEFIT_AND_BONUS
            // 
            this.BENEFIT_AND_BONUS.DataPropertyName = "S6050";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "#,###.####";
            dataGridViewCellStyle6.NullValue = null;
            this.BENEFIT_AND_BONUS.DefaultCellStyle = dataGridViewCellStyle6;
            resources.ApplyResources(this.BENEFIT_AND_BONUS, "BENEFIT_AND_BONUS");
            this.BENEFIT_AND_BONUS.Name = "BENEFIT_AND_BONUS";
            // 
            // FOOD_AND_OTHER_ALLOWANCE
            // 
            this.FOOD_AND_OTHER_ALLOWANCE.DataPropertyName = "S6060";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "#,###.####";
            dataGridViewCellStyle7.NullValue = null;
            this.FOOD_AND_OTHER_ALLOWANCE.DefaultCellStyle = dataGridViewCellStyle7;
            resources.ApplyResources(this.FOOD_AND_OTHER_ALLOWANCE, "FOOD_AND_OTHER_ALLOWANCE");
            this.FOOD_AND_OTHER_ALLOWANCE.Name = "FOOD_AND_OTHER_ALLOWANCE";
            // 
            // OTHER_BONUS
            // 
            this.OTHER_BONUS.DataPropertyName = "S6070";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "#,###.####";
            dataGridViewCellStyle8.NullValue = null;
            this.OTHER_BONUS.DefaultCellStyle = dataGridViewCellStyle8;
            resources.ApplyResources(this.OTHER_BONUS, "OTHER_BONUS");
            this.OTHER_BONUS.Name = "OTHER_BONUS";
            // 
            // TOTAL
            // 
            this.TOTAL.DataPropertyName = "TOTAL";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.Format = "#,##0.####";
            dataGridViewCellStyle9.NullValue = null;
            this.TOTAL.DefaultCellStyle = dataGridViewCellStyle9;
            resources.ApplyResources(this.TOTAL, "TOTAL");
            this.TOTAL.Name = "TOTAL";
            this.TOTAL.ReadOnly = true;
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            // 
            // DialogStaffExpense
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogStaffExpense";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStaffExpense)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Label lblMONTH;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private ExButton btnCHANGE_LOG;
        private DateTimePicker dtpExpenseDate;
        private Label lblPAYMENT_ACCOUNT;
        private Label lblAMOUNT;
        private TreeComboBox cboAccountPayment;
        private TextBox txtTotal;
        private DataGridView dgvStaffExpense;
        private ComboBox cboCurrency;
        private DataGridViewTextBoxColumn EXPENSE_DETAIL_ID;
        private DataGridViewTextBoxColumn IS_ACTIVE;
        private DataGridViewTextBoxColumn IS_PAYROLL;
        private DataGridViewTextBoxColumn POSITION_ID;
        private DataGridViewTextBoxColumn DIVISION;
        private DataGridViewTextBoxColumn POSITION_NAME;
        private DataGridViewTextBoxColumn TOTAL_STAFF;
        private DataGridViewTextBoxColumn BASE_SALARY;
        private DataGridViewTextBoxColumn OVER_TIME;
        private DataGridViewTextBoxColumn SOCAIL_SECURITY_EXPENSE;
        private DataGridViewTextBoxColumn BENEFIT_AND_BONUS;
        private DataGridViewTextBoxColumn FOOD_AND_OTHER_ALLOWANCE;
        private DataGridViewTextBoxColumn OTHER_BONUS;
        private DataGridViewTextBoxColumn TOTAL;
    }
}