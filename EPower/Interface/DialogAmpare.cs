﻿using System;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;

namespace EPower.Interface
{
    public partial class DialogAmpare: ExDialog
    {
        GeneralProcess _flag;
        TBL_AMPARE _objAmpare = new TBL_AMPARE();
        public TBL_AMPARE Ampare
        {
            get { return _objAmpare; }
        }
        TBL_AMPARE _oldObjAmpare = new TBL_AMPARE();

        #region Constructor
        public DialogAmpare(GeneralProcess flag, TBL_AMPARE objAmpare)
        {
            InitializeComponent();
            _flag = flag;
            objAmpare._CopyTo(_objAmpare);
            objAmpare._CopyTo(_oldObjAmpare);

            if (flag == GeneralProcess.Insert)
            {
                _objAmpare.IS_ACTIVE = true;
                this.Text = string.Concat(Resources.INSERT, this.Text);
            }
            else if (flag == GeneralProcess.Update)
            {
                this.Text = string.Concat(Resources.UPDATE, this.Text);
            }
            else if (flag == GeneralProcess.Delete)
            {
                this.Text = string.Concat(Resources.DELETE, this.Text);
                UIHelper.SetEnabled(this, false);
            }

            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
            
            read();
        }
        #endregion

        #region Operation
        /// <summary>
        /// Data transaction
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
            //if duplicate record.
            txtAmpareName.ClearAllValidation();
            if (DBDataContext.Db.IsExits(_objAmpare, "AMPARE_NAME"))
            {
                txtAmpareName.SetValidation(string.Format(Resources.MS_IS_EXISTS, lblAMPARE.Text));
                return;
            }
            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objAmpare); 
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_oldObjAmpare, _objAmpare);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objAmpare);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
            
        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
        
        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        #endregion

        #region Method
        /// <summary>
        /// Load data from object.
        /// </summary>
        private void read()
        {
            txtAmpareName.Text = _objAmpare.AMPARE_NAME;
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            _objAmpare.AMPARE_NAME = txtAmpareName.Text.Trim();
        }

        private bool inValid()
        {
            bool val = false;
            txtAmpareName.ClearValidation();
            //System.Text.RegularExpressions.Regex regex = null;
            //regex = new System.Text.RegularExpressions.Regex("^([0-9][a][A]?)*$");
            if (!DataHelper.IsPositiveNumber(txtAmpareName.Text.Trim().ToUpper().Replace("A", "")))
            {
                txtAmpareName.SetValidation(string.Format(Resources.REQUEST_GRATER_THAN_ZERO, lblAMPARE.Text));
                val = true;
            }
            //if (!regex.IsMatch(txtAmpareName.Text))
            //{
            //    txtAmpareName.SetValidation(string.Format(Resources.REQUEST_GRATER_THAN_ZERO, lblAMPERE.Text));
            //    val = true;
            //}
            return val;
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objAmpare);
        }

        private void txtAmpareName_KeyPress(object sender, KeyPressEventArgs e)
        {
            int number = 0;

            switch (Convert.ToInt32(e.KeyChar))
            {
                default:
                    e.Handled = !int.TryParse((e.KeyChar.ToString()), out number);
                    break;
                case (int)Keys.Back:
                    e.Handled = false;
                    break;
                case (int)Keys.A:
                    e.Handled=false;
                    break;
                case (int)Keys.Insert:
                    if (((TextBox)sender).Text.IndexOf('-') == 0)
                        e.Handled = true;
                    else
                        e.Handled = false;
                    break;
            }
        }
        #endregion        

       
    }
}