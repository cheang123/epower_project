﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogPaymentHistoryCancel : ExDialog
    {
        string paymentNo = "";

        #region Constructor
        public DialogPaymentHistoryCancel(string _paymentNo, bool showDate = true)
        {
            InitializeComponent();
            paymentNo = _paymentNo;
            this.dtpPayDate.Visible =
            this.lblDATE.Visible = (showDate && DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_EDIT_PAYMENT_DATE]) && SoftTech.Security.Logic.Login.IsAuthorized(Permission.POSTPAID_PAYMENT_DATE));
            this.dtpPayDate.Value = DBDataContext.Db.GetSystemDate();
        }
        #endregion

        private void btnOK_Click(object sender, EventArgs e)
        {
            //if (string.IsNullOrEmpty(txtREASON.Text.Trim()))
            //{
            //    this.txtREASON.SetValidation(string.Format(Resources.PLEASE_INPUT, lblREASON.Text));
            //    return;
            //}

            this.DialogResult = DialogResult.Yes;
        }

        private void btnCLOSE_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
        }

        private void DialogPaymentHistoryCancel_Load(object sender, EventArgs e)
        {
            lblWarning.Text = paymentNo;
        }
    }
}