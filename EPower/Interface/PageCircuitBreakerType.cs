﻿using System;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PageCircuitBreakerType : Form
    {
        #region Data
        public TBL_CIRCUIT_BREAKER_TYPE CircuitBrakerType
        {
            get
            {
                TBL_CIRCUIT_BREAKER_TYPE objCircuitBrakerType = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    try
                    {
                        int intBrakerTypeID = (int)dgv.SelectedRows[0].Cells["BREAKER_TYPE_ID"].Value;
                        objCircuitBrakerType = DBDataContext.Db.TBL_CIRCUIT_BREAKER_TYPEs.FirstOrDefault(x => x.BREAKER_TYPE_ID == intBrakerTypeID);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objCircuitBrakerType;
            }
        }
        #endregion

        #region Constructor
        public PageCircuitBreakerType()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            txt_QuickSearch(null, null);
        }
        #endregion

        #region Operation
        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }
        private void btnNew_Click(object sender, EventArgs e)
        {
            DialogCircuitBreakerType dig = new DialogCircuitBreakerType(GeneralProcess.Insert, new TBL_CIRCUIT_BREAKER_TYPE() { BREAKER_TYPE_CODE = "" });
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.CircuitBrakerType.BREAKER_TYPE_ID);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                DialogCircuitBreakerType dig = new DialogCircuitBreakerType(GeneralProcess.Update, CircuitBrakerType);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.CircuitBrakerType.BREAKER_TYPE_ID);
                }
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (IsDeletable())
            {
                DialogCircuitBreakerType dig = new DialogCircuitBreakerType(GeneralProcess.Delete, CircuitBrakerType);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.CircuitBrakerType.BREAKER_TYPE_ID - 1);
                }
            }
        }

        private void txt_QuickSearch(object sender, EventArgs e)
        {
            try
            {
                dgv.DataSource = from cb in DBDataContext.Db.TBL_CIRCUIT_BREAKER_TYPEs
                                 where cb.IS_ACTIVE &&
                                 cb.BREAKER_TYPE_CODE.ToLower().Contains(txtQuickSearch.Text.ToLower().Trim()) &&
                                 cb.BREAKER_TYPE_NAME.ToLower().Contains(txtQuickSearch.Text.ToLower().Trim())
                                 select new
                                 {
                                     cb.BREAKER_TYPE_ID,
                                     cb.BREAKER_TYPE_CODE,
                                     cb.BREAKER_TYPE_NAME,
                                 };
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        } 

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        #endregion

        #region Method
        private bool IsDeletable()
        {
            //TODO: can delete record.
            bool val = false;
            if (dgv.SelectedRows.Count>0)
            {
                val = true;
                if (DBDataContext.Db.TBL_CIRCUIT_BREAKERs.Where(x=>x.BREAKER_TYPE_ID==CircuitBrakerType.BREAKER_TYPE_ID).Count()>0)
                {
                    MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE);
                    val = false;
                }
            }
            return val;
        }
        #endregion

        
    }
}
