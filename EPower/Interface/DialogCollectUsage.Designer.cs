﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogCollectUsage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCollectUsage));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.cboCycle = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpMonth = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.cboCollector = new System.Windows.Forms.ComboBox();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.cboArea = new System.Windows.Forms.ComboBox();
            this.txtSearch = new SoftTech.Component.ExTextbox();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.USAGE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.METER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.START_USAGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.END_USAGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_METER_RENEW_CYCLE = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._DB = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._EDITED = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rdoShowAll = new System.Windows.Forms.RadioButton();
            this.rdoShowNotComplete = new System.Windows.Forms.RadioButton();
            this.rdoShowInvalid = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.txtSearch);
            this.content.Controls.Add(this.label4);
            this.content.Controls.Add(this.rdoShowInvalid);
            this.content.Controls.Add(this.rdoShowNotComplete);
            this.content.Controls.Add(this.rdoShowAll);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.dgv);
            this.content.Controls.Add(this.panel4);
            this.content.Controls.Add(this.panel1);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.panel4, 0);
            this.content.Controls.SetChildIndex(this.dgv, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.rdoShowAll, 0);
            this.content.Controls.SetChildIndex(this.rdoShowNotComplete, 0);
            this.content.Controls.SetChildIndex(this.rdoShowInvalid, 0);
            this.content.Controls.SetChildIndex(this.label4, 0);
            this.content.Controls.SetChildIndex(this.txtSearch, 0);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel4.Controls.Add(this.cboCycle);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.dtpMonth);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.cboCollector);
            this.panel4.Controls.Add(this.dtpEndDate);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.dtpStartDate);
            this.panel4.Controls.Add(this.cboArea);
            resources.ApplyResources(this.panel4, "panel4");
            this.panel4.Name = "panel4";
            // 
            // cboCycle
            // 
            this.cboCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCycle.FormattingEnabled = true;
            resources.ApplyResources(this.cboCycle, "cboCycle");
            this.cboCycle.Name = "cboCycle";
            this.cboCycle.SelectedIndexChanged += new System.EventHandler(this.cboBillingCycle_SelectedIndexChanged);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // dtpMonth
            // 
            resources.ApplyResources(this.dtpMonth, "dtpMonth");
            this.dtpMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMonth.Name = "dtpMonth";
            this.dtpMonth.ShowUpDown = true;
            this.dtpMonth.ValueChanged += new System.EventHandler(this.dtpCollectMonth_ValueChanged);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // cboCollector
            // 
            this.cboCollector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCollector.FormattingEnabled = true;
            this.cboCollector.Items.AddRange(new object[] {
            resources.GetString("cboCollector.Items"),
            resources.GetString("cboCollector.Items1"),
            resources.GetString("cboCollector.Items2")});
            resources.ApplyResources(this.cboCollector, "cboCollector");
            this.cboCollector.Name = "cboCollector";
            this.cboCollector.SelectedIndexChanged += new System.EventHandler(this.txtCollector_SelectedIndexChanged);
            // 
            // dtpEndDate
            // 
            resources.ApplyResources(this.dtpEndDate, "dtpEndDate");
            this.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndDate.Name = "dtpEndDate";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // dtpStartDate
            // 
            resources.ApplyResources(this.dtpStartDate, "dtpStartDate");
            this.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartDate.Name = "dtpStartDate";
            // 
            // cboArea
            // 
            this.cboArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboArea.FormattingEnabled = true;
            this.cboArea.Items.AddRange(new object[] {
            resources.GetString("cboArea.Items"),
            resources.GetString("cboArea.Items1"),
            resources.GetString("cboArea.Items2"),
            resources.GetString("cboArea.Items3"),
            resources.GetString("cboArea.Items4"),
            resources.GetString("cboArea.Items5")});
            resources.ApplyResources(this.cboArea, "cboArea");
            this.cboArea.Name = "cboArea";
            this.cboArea.SelectedIndexChanged += new System.EventHandler(this.txtArea_SelectedIndexChanged);
            // 
            // txtSearch
            // 
            this.txtSearch.BackColor = System.Drawing.Color.White;
            this.txtSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtSearch, "txtSearch");
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtSearch.QuickSearch += new System.EventHandler(this.txtSearch_QuickSearch);
            this.txtSearch.Enter += new System.EventHandler(this.txtSearch_Enter);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToOrderColumns = true;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.USAGE_ID,
            this.CUSTOMER_ID,
            this.METER_ID,
            this.CUSTOMER_CODE,
            this.CUSTOMER_NAME,
            this.START_USAGE,
            this.END_USAGE,
            this.IS_METER_RENEW_CYCLE,
            this._DB,
            this._EDITED});
            this.dgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgv.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellValueChanged);
            this.dgv.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgv_DataError);
            // 
            // USAGE_ID
            // 
            this.USAGE_ID.DataPropertyName = "USAGE_ID";
            resources.ApplyResources(this.USAGE_ID, "USAGE_ID");
            this.USAGE_ID.Name = "USAGE_ID";
            this.USAGE_ID.ReadOnly = true;
            // 
            // CUSTOMER_ID
            // 
            this.CUSTOMER_ID.DataPropertyName = "CUSTOMER_ID";
            resources.ApplyResources(this.CUSTOMER_ID, "CUSTOMER_ID");
            this.CUSTOMER_ID.Name = "CUSTOMER_ID";
            this.CUSTOMER_ID.ReadOnly = true;
            // 
            // METER_ID
            // 
            this.METER_ID.DataPropertyName = "METER_ID";
            resources.ApplyResources(this.METER_ID, "METER_ID");
            this.METER_ID.Name = "METER_ID";
            // 
            // CUSTOMER_CODE
            // 
            this.CUSTOMER_CODE.DataPropertyName = "CUSTOMER_CODE";
            resources.ApplyResources(this.CUSTOMER_CODE, "CUSTOMER_CODE");
            this.CUSTOMER_CODE.Name = "CUSTOMER_CODE";
            this.CUSTOMER_CODE.ReadOnly = true;
            // 
            // CUSTOMER_NAME
            // 
            this.CUSTOMER_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CUSTOMER_NAME.DataPropertyName = "CUSTOMER_NAME";
            resources.ApplyResources(this.CUSTOMER_NAME, "CUSTOMER_NAME");
            this.CUSTOMER_NAME.Name = "CUSTOMER_NAME";
            this.CUSTOMER_NAME.ReadOnly = true;
            // 
            // START_USAGE
            // 
            this.START_USAGE.DataPropertyName = "START_USAGE";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "#";
            this.START_USAGE.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.START_USAGE, "START_USAGE");
            this.START_USAGE.Name = "START_USAGE";
            this.START_USAGE.ReadOnly = true;
            // 
            // END_USAGE
            // 
            this.END_USAGE.DataPropertyName = "END_USAGE";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "#";
            this.END_USAGE.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.END_USAGE, "END_USAGE");
            this.END_USAGE.Name = "END_USAGE";
            // 
            // IS_METER_RENEW_CYCLE
            // 
            this.IS_METER_RENEW_CYCLE.DataPropertyName = "IS_METER_RENEW_CYCLE";
            resources.ApplyResources(this.IS_METER_RENEW_CYCLE, "IS_METER_RENEW_CYCLE");
            this.IS_METER_RENEW_CYCLE.Name = "IS_METER_RENEW_CYCLE";
            // 
            // _DB
            // 
            this._DB.DataPropertyName = "_DB";
            resources.ApplyResources(this._DB, "_DB");
            this._DB.Name = "_DB";
            this._DB.ReadOnly = true;
            // 
            // _EDITED
            // 
            resources.ApplyResources(this._EDITED, "_EDITED");
            this._EDITED.Name = "_EDITED";
            // 
            // rdoShowAll
            // 
            resources.ApplyResources(this.rdoShowAll, "rdoShowAll");
            this.rdoShowAll.Checked = true;
            this.rdoShowAll.Name = "rdoShowAll";
            this.rdoShowAll.TabStop = true;
            this.rdoShowAll.UseVisualStyleBackColor = true;
            this.rdoShowAll.CheckedChanged += new System.EventHandler(this.rdoShowAll_CheckedChanged);
            // 
            // rdoShowNotComplete
            // 
            resources.ApplyResources(this.rdoShowNotComplete, "rdoShowNotComplete");
            this.rdoShowNotComplete.Name = "rdoShowNotComplete";
            this.rdoShowNotComplete.UseVisualStyleBackColor = true;
            this.rdoShowNotComplete.CheckedChanged += new System.EventHandler(this.rdoShowNotComplete_CheckedChanged);
            // 
            // rdoShowInvalid
            // 
            resources.ApplyResources(this.rdoShowInvalid, "rdoShowInvalid");
            this.rdoShowInvalid.Name = "rdoShowInvalid";
            this.rdoShowInvalid.UseVisualStyleBackColor = true;
            this.rdoShowInvalid.CheckedChanged += new System.EventHandler(this.rdoShowInvalid_CheckedChanged);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // DialogCollectUsage
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogCollectUsage";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DialogCollectUsage_FormClosing);
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Panel panel1;
        private Panel panel4;
        private DataGridView dgv;
        private DateTimePicker dtpStartDate;
        private ComboBox cboArea;
        private DateTimePicker dtpEndDate;
        private Label label1;
        private ComboBox cboCollector;
        private Label label3;
        private DateTimePicker dtpMonth;
        private Label label2;
        private ComboBox cboCycle;
        private Label label4;
        private RadioButton rdoShowInvalid;
        private RadioButton rdoShowNotComplete;
        private RadioButton rdoShowAll;
        private ExTextbox txtSearch;
        private DataGridViewTextBoxColumn USAGE_ID;
        private DataGridViewTextBoxColumn CUSTOMER_ID;
        private DataGridViewTextBoxColumn METER_ID;
        private DataGridViewTextBoxColumn CUSTOMER_CODE;
        private DataGridViewTextBoxColumn CUSTOMER_NAME;
        private DataGridViewTextBoxColumn START_USAGE;
        private DataGridViewTextBoxColumn END_USAGE;
        private DataGridViewCheckBoxColumn IS_METER_RENEW_CYCLE;
        private DataGridViewCheckBoxColumn _DB;
        private DataGridViewTextBoxColumn _EDITED;
    }
}