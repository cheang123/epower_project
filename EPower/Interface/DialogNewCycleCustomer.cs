﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogNewCycleCustomer : ExDialog
    {
        #region Data 
        bool _blnStart = false;
        #endregion Data

        #region Constructor

        public DialogNewCycleCustomer()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            bingBillingCycle();
            _blnStart = true;
            bind();
        }

        #endregion Constructor
        private void bingBillingCycle()
        {
            //Billing Cycle
            DataTable dtCycle = DBDataContext.Db.TBL_BILLING_CYCLEs.Where(b => b.IS_ACTIVE)._ToDataTable();
            DataRow row = dtCycle.NewRow();
            row["CYCLE_ID"] = 0;
            row["CYCLE_NAME"] = Resources.SELECT_BILLING_CYCLE;
            dtCycle.Rows.InsertAt(row, 0);
            UIHelper.SetDataSourceToComboBox(this.cboCycle, dtCycle);

            if (dtCycle.Rows.Count > 1)
            {
                this.cboCycle.SelectedIndex = 1;
                int intCycleID = DataHelper.ParseToInt(cboCycle.SelectedValue.ToString());
                dtpMonth.Value = Method.GetNextBillingMonth(intCycleID);
            }
        }

        #region Method

        void bind()
        {
            dgv.Rows.Clear();
            if (cboCycle.SelectedIndex > 0 && _blnStart)
            {
                int intCycleID = DataHelper.ParseToInt(cboCycle.SelectedValue.ToString());
                DateTime datMonth = dtpMonth.Value.Date;
                var lstNewCycle = from cus in DBDataContext.Db.TBL_CUSTOMERs
                                  join area in DBDataContext.Db.TBL_AREAs on cus.AREA_ID equals area.AREA_ID
                                  join usage in DBDataContext.Db.TBL_USAGEs on cus.CUSTOMER_ID equals usage.CUSTOMER_ID
                                  join meter in DBDataContext.Db.TBL_METERs on usage.METER_ID equals meter.METER_ID
                                  join metertype in DBDataContext.Db.TBL_METER_TYPEs on meter.METER_TYPE_ID equals metertype.METER_TYPE_ID
                                  join emp in DBDataContext.Db.TBL_EMPLOYEEs on usage.COLLECTOR_ID equals emp.EMPLOYEE_ID
                                  where usage.USAGE_MONTH.Date == datMonth.Date
                                  && (cus.STATUS_ID == (int)CustomerStatus.Active || cus.STATUS_ID == (int)CustomerStatus.Blocked)
                                  && usage.IS_METER_RENEW_CYCLE
                                  select new
                                  {
                                      cus.CUSTOMER_ID,
                                      cus.CUSTOMER_CODE,
                                      CUSTOMER_NAME = cus.LAST_NAME_KH + " " + cus.FIRST_NAME_KH,
                                      meter.METER_CODE,
                                      emp.EMPLOYEE_NAME,
                                      area.AREA_NAME,
                                      usage.START_USAGE,
                                      usage.END_USAGE,
                                      TOTAL_USAGE = 0 //
                                  };
                foreach (var objCus in lstNewCycle)
                {
                    decimal decTotalUsage = objCus.END_USAGE + (Method.GetMaxNumber((int)objCus.START_USAGE) - objCus.START_USAGE) + 1;
                    dgv.Rows.Add(objCus.CUSTOMER_ID, objCus.CUSTOMER_CODE, objCus.CUSTOMER_NAME, objCus.METER_CODE, objCus.EMPLOYEE_NAME,
                        objCus.AREA_NAME, objCus.START_USAGE, objCus.END_USAGE, decTotalUsage);
                }
            }
        }
        #endregion Method

        #region Event
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;



        }

        private void btnInputUsage_Click(object sender, EventArgs e)
        {
            if (this.dgv.SelectedRows.Count > 0)
            {
                int CustomerID = (int)this.dgv.SelectedRows[0].Cells["CUSTOMER_ID"].Value;
                if (new DialogCollectUsageManually(CustomerID).ShowDialog() == DialogResult.OK)
                {
                    this.bind();
                }
            }
        }

        private void cboCycle_SelectedIndexChanged(object sender, EventArgs e)
        {
            bind();
        }

        private void dtpMonth_ValueChanged(object sender, EventArgs e)
        {
            bind();
        }
        #endregion Event


    }
}
