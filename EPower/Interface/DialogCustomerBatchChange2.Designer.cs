﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogCustomerBatchChange_2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCustomerBatchChange_2));
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.lblUPDATE_BILL = new System.Windows.Forms.Label();
            this.lblPRICE = new System.Windows.Forms.Label();
            this.lblCYCLE_NAME = new System.Windows.Forms.Label();
            this.cboPrice = new System.Windows.Forms.ComboBox();
            this.cboCycle = new System.Windows.Forms.ComboBox();
            this.lblCOMMUNE = new System.Windows.Forms.Label();
            this.lblTOTAL_CUSTOMER_UPDATE = new System.Windows.Forms.Label();
            this.lblCustomer_ = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cboCommune = new System.Windows.Forms.ComboBox();
            this.cboVillage = new System.Windows.Forms.ComboBox();
            this.lblVILLAGE = new System.Windows.Forms.Label();
            this.cboDistrict = new System.Windows.Forms.ComboBox();
            this.lblDISTRICT = new System.Windows.Forms.Label();
            this.txtVillageText = new System.Windows.Forms.TextBox();
            this.lblUPDATE_ADDRESS = new System.Windows.Forms.Label();
            this.chkIsmarketVendor = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cboCustomerGroup = new System.Windows.Forms.ComboBox();
            this.cboConnectionType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cboVoltage = new System.Windows.Forms.ComboBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.cboVoltage);
            this.content.Controls.Add(this.label4);
            this.content.Controls.Add(this.label3);
            this.content.Controls.Add(this.label2);
            this.content.Controls.Add(this.cboCustomerGroup);
            this.content.Controls.Add(this.cboConnectionType);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.chkIsmarketVendor);
            this.content.Controls.Add(this.lblUPDATE_ADDRESS);
            this.content.Controls.Add(this.txtVillageText);
            this.content.Controls.Add(this.lblDISTRICT);
            this.content.Controls.Add(this.cboDistrict);
            this.content.Controls.Add(this.lblVILLAGE);
            this.content.Controls.Add(this.cboVillage);
            this.content.Controls.Add(this.cboCommune);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.lblCustomer_);
            this.content.Controls.Add(this.lblTOTAL_CUSTOMER_UPDATE);
            this.content.Controls.Add(this.lblCOMMUNE);
            this.content.Controls.Add(this.cboCycle);
            this.content.Controls.Add(this.cboPrice);
            this.content.Controls.Add(this.lblCYCLE_NAME);
            this.content.Controls.Add(this.lblPRICE);
            this.content.Controls.Add(this.lblUPDATE_BILL);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.btnCLOSE);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.lblUPDATE_BILL, 0);
            this.content.Controls.SetChildIndex(this.lblPRICE, 0);
            this.content.Controls.SetChildIndex(this.lblCYCLE_NAME, 0);
            this.content.Controls.SetChildIndex(this.cboPrice, 0);
            this.content.Controls.SetChildIndex(this.cboCycle, 0);
            this.content.Controls.SetChildIndex(this.lblCOMMUNE, 0);
            this.content.Controls.SetChildIndex(this.lblTOTAL_CUSTOMER_UPDATE, 0);
            this.content.Controls.SetChildIndex(this.lblCustomer_, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.cboCommune, 0);
            this.content.Controls.SetChildIndex(this.cboVillage, 0);
            this.content.Controls.SetChildIndex(this.lblVILLAGE, 0);
            this.content.Controls.SetChildIndex(this.cboDistrict, 0);
            this.content.Controls.SetChildIndex(this.lblDISTRICT, 0);
            this.content.Controls.SetChildIndex(this.txtVillageText, 0);
            this.content.Controls.SetChildIndex(this.lblUPDATE_ADDRESS, 0);
            this.content.Controls.SetChildIndex(this.chkIsmarketVendor, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.cboConnectionType, 0);
            this.content.Controls.SetChildIndex(this.cboCustomerGroup, 0);
            this.content.Controls.SetChildIndex(this.label2, 0);
            this.content.Controls.SetChildIndex(this.label3, 0);
            this.content.Controls.SetChildIndex(this.label4, 0);
            this.content.Controls.SetChildIndex(this.cboVoltage, 0);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblUPDATE_BILL
            // 
            resources.ApplyResources(this.lblUPDATE_BILL, "lblUPDATE_BILL");
            this.lblUPDATE_BILL.Name = "lblUPDATE_BILL";
            // 
            // lblPRICE
            // 
            resources.ApplyResources(this.lblPRICE, "lblPRICE");
            this.lblPRICE.Name = "lblPRICE";
            // 
            // lblCYCLE_NAME
            // 
            resources.ApplyResources(this.lblCYCLE_NAME, "lblCYCLE_NAME");
            this.lblCYCLE_NAME.Name = "lblCYCLE_NAME";
            // 
            // cboPrice
            // 
            this.cboPrice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPrice.DropDownWidth = 350;
            this.cboPrice.FormattingEnabled = true;
            resources.ApplyResources(this.cboPrice, "cboPrice");
            this.cboPrice.Name = "cboPrice";
            // 
            // cboCycle
            // 
            this.cboCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCycle.FormattingEnabled = true;
            resources.ApplyResources(this.cboCycle, "cboCycle");
            this.cboCycle.Name = "cboCycle";
            // 
            // lblCOMMUNE
            // 
            resources.ApplyResources(this.lblCOMMUNE, "lblCOMMUNE");
            this.lblCOMMUNE.Name = "lblCOMMUNE";
            // 
            // lblTOTAL_CUSTOMER_UPDATE
            // 
            resources.ApplyResources(this.lblTOTAL_CUSTOMER_UPDATE, "lblTOTAL_CUSTOMER_UPDATE");
            this.lblTOTAL_CUSTOMER_UPDATE.Name = "lblTOTAL_CUSTOMER_UPDATE";
            // 
            // lblCustomer_
            // 
            resources.ApplyResources(this.lblCustomer_, "lblCustomer_");
            this.lblCustomer_.Name = "lblCustomer_";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // cboCommune
            // 
            this.cboCommune.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCommune.FormattingEnabled = true;
            resources.ApplyResources(this.cboCommune, "cboCommune");
            this.cboCommune.Name = "cboCommune";
            this.cboCommune.SelectedIndexChanged += new System.EventHandler(this.cboCommune_SelectedIndexChanged);
            // 
            // cboVillage
            // 
            this.cboVillage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboVillage.FormattingEnabled = true;
            resources.ApplyResources(this.cboVillage, "cboVillage");
            this.cboVillage.Name = "cboVillage";
            this.cboVillage.SelectedIndexChanged += new System.EventHandler(this.cboVillage_SelectedIndexChanged);
            // 
            // lblVILLAGE
            // 
            resources.ApplyResources(this.lblVILLAGE, "lblVILLAGE");
            this.lblVILLAGE.Name = "lblVILLAGE";
            // 
            // cboDistrict
            // 
            this.cboDistrict.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDistrict.FormattingEnabled = true;
            resources.ApplyResources(this.cboDistrict, "cboDistrict");
            this.cboDistrict.Name = "cboDistrict";
            this.cboDistrict.SelectedIndexChanged += new System.EventHandler(this.cboDistrict_SelectedIndexChanged);
            // 
            // lblDISTRICT
            // 
            resources.ApplyResources(this.lblDISTRICT, "lblDISTRICT");
            this.lblDISTRICT.Name = "lblDISTRICT";
            // 
            // txtVillageText
            // 
            resources.ApplyResources(this.txtVillageText, "txtVillageText");
            this.txtVillageText.Name = "txtVillageText";
            // 
            // lblUPDATE_ADDRESS
            // 
            resources.ApplyResources(this.lblUPDATE_ADDRESS, "lblUPDATE_ADDRESS");
            this.lblUPDATE_ADDRESS.Name = "lblUPDATE_ADDRESS";
            // 
            // chkIsmarketVendor
            // 
            resources.ApplyResources(this.chkIsmarketVendor, "chkIsmarketVendor");
            this.chkIsmarketVendor.Name = "chkIsmarketVendor";
            this.chkIsmarketVendor.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // cboCustomerGroup
            // 
            this.cboCustomerGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomerGroup.DropDownWidth = 350;
            this.cboCustomerGroup.FormattingEnabled = true;
            resources.ApplyResources(this.cboCustomerGroup, "cboCustomerGroup");
            this.cboCustomerGroup.Name = "cboCustomerGroup";
            this.cboCustomerGroup.SelectedIndexChanged += new System.EventHandler(this.cboCustomerGroup_SelectedIndexChanged);
            // 
            // cboConnectionType
            // 
            this.cboConnectionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboConnectionType.DropDownWidth = 300;
            this.cboConnectionType.FormattingEnabled = true;
            resources.ApplyResources(this.cboConnectionType, "cboConnectionType");
            this.cboConnectionType.Name = "cboConnectionType";
            this.cboConnectionType.SelectedIndexChanged += new System.EventHandler(this.cboConnectionType_SelectedIndexChanged);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // cboVoltage
            // 
            this.cboVoltage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboVoltage.DropDownWidth = 224;
            this.cboVoltage.FormattingEnabled = true;
            resources.ApplyResources(this.cboVoltage, "cboVoltage");
            this.cboVoltage.Name = "cboVoltage";
            // 
            // DialogCustomerBatchChange_2
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogCustomerBatchChange_2";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label lblCYCLE_NAME;
        private Label lblPRICE;
        private Label lblUPDATE_BILL;
        private ComboBox cboCycle;
        private ComboBox cboPrice;
        private Label lblCOMMUNE;
        private Label lblCustomer_;
        private Label lblTOTAL_CUSTOMER_UPDATE;
        private Panel panel1;
        private Label lblVILLAGE;
        private ComboBox cboCommune;
        private ComboBox cboDistrict;
        private Label lblDISTRICT;
        private TextBox txtVillageText;
        private Label lblUPDATE_ADDRESS;
        public ComboBox cboVillage;
        private CheckBox chkIsmarketVendor;
        private Label label3;
        private Label label2;
        private ComboBox cboCustomerGroup;
        private ComboBox cboConnectionType;
        private Label label1;
        private Label label4;
        private ComboBox cboVoltage;
    }
}