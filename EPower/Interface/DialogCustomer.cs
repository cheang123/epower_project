﻿using dotnetCHARTING.WinForms;
using EPower.Base.Logic;
using EPower.Interface.BankPayment;
using EPower.Logic;
using EPower.Base.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using Security = SoftTech.Security.Logic.Login;

namespace EPower.Interface
{
    public partial class DialogCustomer : ExDialog
    {

        #region Private Data 
        GeneralProcess _flag = GeneralProcess.Insert;
        TBL_CUSTOMER _objNew = new TBL_CUSTOMER();
        TBL_CUSTOMER _objOld = new TBL_CUSTOMER();

        List<TLKP_CUSTOMER_CONNECTION_TYPE> lstConnectionType = new List<TLKP_CUSTOMER_CONNECTION_TYPE>();
        List<TLKP_TITLE> listTitles = DBDataContext.Db.TLKP_TITLEs.Where(x => x.IS_ACTIVE).ToList();

        TBL_METER _objMeter = null;
        TBL_CIRCUIT_BREAKER _objBreaker = null;
        TBL_BOX _objBox = null;
        TBL_CUSTOMER_METER _objCustomerMeter = null;
        TBL_POLE _objPole = null;

        private List<TBL_APPLY_PAYMENT_HISTORY> _prepayment_histories = new List<TBL_APPLY_PAYMENT_HISTORY>();

        public bool CustomerIsActivated = false;
        bool _chartWasShown = false;
        bool _loading = false;
        bool printingInvoice = false;

        #endregion Private Data

        #region Constructor
        public DialogCustomer(GeneralProcess flag, TBL_CUSTOMER obj)
        {
            InitializeComponent();
            printingInvoice = DataHelper.ParseToBoolean(Method.Utilities[Utility.INVOICE_CONCURRENT_PRINTING]);
            this._loading = true;

            this.txtCustomerCode.MaxLength = Properties.Settings.Default.CUSTOMER_DIGIT;

            this.btnActivate.Enabled = Security.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_ACTIVATE);
            this.btnCHANGE_METER.Enabled = Security.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_REGISTER_UPDATE_CHANGEMETER);
            this.btnCHANGE_BREAKER.Enabled = Security.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_REGISTER_UPDATE_CHANGECIRCUITBREAKER);
            this.btnCHANGE_BOX.Enabled = Security.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_REGISTER_UPDATE_CHANGEBOX);
            this.btnADJUST.Enabled = Security.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_REGISTER_UPDATE_ADJUSTUSAGE);
            //this.btnADJUST_AMOUNT.Enabled = Security.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_REGISTER_UPDATE_ADJUSTAMOUNT);
            this.btnPRINT_1.Enabled = Security.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_REGISTER_UPDATE_PRINT);
            this.lblCHANGE_AMPARE.Enabled = Security.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_REGISTER_UPDATE_CHANGE_AMPARE);

            this.btnAPPLY_PAYMENT_1.Enabled = Security.IsAuthorized(Permission.POSTPAID_PAYMENT);
            this.btnADD_SERVICE.Enabled = Security.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_REGISTER_FIXCHARGE);

            // module 
            this.lblPOSITION_IN_BOX.Visible =
            this.txtPositionInBox.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.POSITION_IN_BOX_ENABLE]);
            this.btnREPORT_DEPOSIT_AND_CONNECTION_FEE.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.BANK_PAYMENT_INCLUDE_DEPOSIT]);


            UIHelper.DataGridViewProperties(this.dgvEquitment, this.dgvInvoiceList, this.dgvDeposit, this.dgvPrepayment);

            SETTLE_AMOUNT.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            DUE_AMOUNT.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            DateTime sysDate = DBDataContext.Db.GetSystemDate();
            dtpYear.Value = new DateTime(sysDate.Year, sysDate.Month, 1);

            this._flag = flag;

            if (!DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_RECURRING_SERVICE]))
            {
                this.tab.TabPages.Remove(this.tabRECURRING_SERVICE);
            }
            if (!DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_PREPAYMENT_SERVICE]))
            {
                this.tab.TabPages.Remove(this.tabPREPAYMENT);
            }

            this.bind();

            this._objNew = obj;

            this._objNew._CopyTo(this._objOld);
            if (this._flag == GeneralProcess.Insert)
            {
                this.Text = string.Concat(Resources.INSERT, this.Text);
                this.tab.TabPages.Remove(this.tabBILLING_AND_METER);
                this.tab.TabPages.Remove(this.tabInvoice);
                this.tab.TabPages.Remove(this.tabUSAGE);
                this.tab.TabPages.Remove(this.tabDeposit);
                this.tab.TabPages.Remove(this.tabRECURRING_SERVICE);
                this.tab.TabPages.Remove(this.tabPREPAYMENT);
                this.btnADD_SERVICE.Visible = false;
                this.btnREPORT_CONNECTION.Visible = false;
                this.btnREPORT_AGREEMENT.Visible = false;
                this.btnREPORT_DEPOSIT_AND_CONNECTION_FEE.Visible = false;
                this.btnAPPLY_PAYMENT_1.Visible = false;
                this.lblCHANGE_AMPARE.Visible = false;
                this.newCus();
            }
            else if (this._flag == GeneralProcess.Update)
            {
                this.btnActivate.Visible = false;

                // if user want to change customer type, phase and ampere
                // they need to change with deposit.
                //this.cboCustomerType.Enabled = false;
                //this.cboPhase.Enabled = false;
                //this.cboAMP.Enabled = false;
                //this.linkEditDeposit.Visible = true;
                //this.cboPrice.Enabled = false;
                this.Text = string.Concat(Resources.UPDATE, this.Text);
                this.cboAMP.Enabled = false;
                this.read();

                this.bindAttachment();
                this.bindDeposit();
                this.bindPrepayment();
                this.bindService();

                // when pending.
                if (this._objOld.STATUS_ID == (int)CustomerStatus.Pending)
                {
                    this.tab.TabPages.Remove(this.tabBILLING_AND_METER);
                    this.tab.TabPages.Remove(this.tabUSAGE);
                }
                cboArea.Enabled = !(this._objOld.STATUS_ID == (int)CustomerStatus.Active);

                //bind invoice status
                List<int> StatusInv = new List<int>()
                {
                    (int)InvoiceStatus.Open,
                    (int)InvoiceStatus.Close,
                    (int)InvoiceStatus.Cancel
                };
                UIHelper.SetDataSourceToComboBox(cboInvoiceStatus, DBDataContext.Db.TLKP_INVOICE_STATUS.Where(x => StatusInv.Contains(x.INVOICE_STATUS_ID)), Resources.ALL_STATUS);
                cboInvoiceStatus.SelectedValue = (int)InvoiceStatus.Open;
                loadTotalDueAmount();
            }
            else if (this._flag == GeneralProcess.Delete)
            {
                this.Text = string.Concat(Resources.DELETE, this.Text);
                this.btnActivate.Visible = false;
                this.btnSAVE.Text = Resources.CLOSE;

                this.tab.TabPages.Remove(this.tabBILLING_AND_METER);
                this.tab.TabPages.Remove(this.tabInvoice);
                this.tab.TabPages.Remove(this.tabUSAGE);

                this.btnAPPLY_PAYMENT_1.Visible = false;
                this.btnADD_SERVICE.Visible = false;
                this.read();
            }
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
            this.btnREACTIVATE.Visible = this._objNew.STATUS_ID == (int)CustomerStatus.Closed;
            this.btnRE_ACTIVE.Visible = this._objNew.STATUS_ID == (int)CustomerStatus.Cancelled;
            //this.dtpActivateDate.Enabled = this._objNew.STATUS_ID != (int)CustomerStatus.Closed;
            this._loading = false;
            txtLastNameKH.Select();

            //Events
            this.btnADD_1.LinkClicked += btnAddService_LinkClicked;
            this.btnEDIT_1.LinkClicked += btnEditService_LinkClicked;
            this.btnREMOVE_1.LinkClicked += btnRemoveService_LinkClicked;
        }


        #endregion Constructor

        #region Public Data
        public TBL_CUSTOMER Object
        {
            get
            {
                return _objNew;
            }
        }
        public TBL_CUSTOMER_ATTACHMENT _objCusAttach
        {
            get
            {
                TBL_CUSTOMER_ATTACHMENT objCusAttach = null;
                if (dgvAttachment.SelectedRows.Count > 0)
                {
                    int cus_attach_id = (int)dgvAttachment.SelectedRows[0].Cells[CUSTOMER_ATTACHMENT_ID.Name].Value;
                    try
                    {
                        objCusAttach = DBDataContext.Db.TBL_CUSTOMER_ATTACHMENTs.FirstOrDefault(x => x.CUSTOMER_ATTACHMENT_ID == cus_attach_id);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objCusAttach;
            }
        }

        #endregion Public Data

        #region Method

        TabPage selectedPage = null;
        private void selectTab(TabPage page)
        {
            if (selectedPage == null)
            {
                this.selectedPage = page;
                this.tab.SelectedTab = page;
            }
        }

        private bool invalid()
        {
            bool blnReturn = false;

            this.selectedPage = null;

            this.ClearAllValidation();


            if (cboTitles.SelectedIndex == -1)
            {
                this.selectTab(tabGENERAL_INFORMATION);
                cboTitles.SetValidation(string.Format(Resources.REQUIRED, lblTITLES.Text));
                blnReturn = true;
            }


            if (txtLastNameKH.Text.Trim() == "")
            {
                this.selectTab(tabGENERAL_INFORMATION);
                txtLastNameKH.SetValidation(string.Format(Resources.REQUIRED, lblLAST_NAME_KH.Text));
                blnReturn = true;
            }

            //if (txtFirstNameKH.Text.Trim() == "")
            //{
            //    this.selectTab(tabGeneralInfo);
            //    txtFirstNameKH.SetValidation(string.Format(Properties.Resources.RequestInput, lblFirstNameKH.Text));
            //    blnReturn = true;
            //}

            if (txtCustomerCode.Text.Trim() == "")
            {
                this.selectTab(tabGENERAL_INFORMATION);
                txtCustomerCode.SetValidation(string.Format(Resources.REQUIRED, lblCUSTOMER_CODE.Text));
                blnReturn = true;
            }

            if (cboSex.SelectedIndex == -1)
            {
                this.selectTab(tabGENERAL_INFORMATION);
                cboSex.SetValidation(string.Format(Resources.REQUIRED, lblSEX.Text));
                blnReturn = true;
            }

            if (!DataHelper.IsInteger(this.txtTotalFamily.Text))
            {
                this.selectTab(tabGENERAL_INFORMATION);
                txtTotalFamily.SetValidation(string.Format(Resources.REQUIRED_INPUT_NUMBER, lblTOTAL_FAMILY.Text));
                blnReturn = true;
            }

            if (cboArea.SelectedIndex == -1)
            {
                this.selectTab(tabGENERAL_INFORMATION);
                cboArea.SetValidation(string.Format(Resources.REQUIRED, lblAREA.Text));
                blnReturn = true;
            }
            if (cboProvince.SelectedIndex == -1)
            {
                this.selectTab(tabGENERAL_INFORMATION);
                cboProvince.SetValidation(string.Format(Resources.REQUIRED, lblPROVINCE.Text));
                blnReturn = true;
            }
            if (cboDistrict.SelectedIndex == -1)
            {
                this.selectTab(tabGENERAL_INFORMATION);
                cboDistrict.SetValidation(string.Format(Resources.REQUIRED, lblDISTRICT.Text));
                blnReturn = true;
            }
            if (cboCommune.SelectedIndex == -1)
            {
                this.selectTab(tabGENERAL_INFORMATION);
                cboCommune.SetValidation(string.Format(Resources.REQUIRED, lblCOMMUNE.Text));
                blnReturn = true;
            }
            if (cboVillage.SelectedIndex == -1)
            {
                this.selectTab(tabGENERAL_INFORMATION);
                cboVillage.SetValidation(string.Format(Resources.REQUIRED, lblVILLAGE.Text));
                blnReturn = true;
            }
            if (!DataHelper.IsPhone(txtPhone1.Text))
            {
                this.selectTab(tabGENERAL_INFORMATION);
                this.txtPhone1.SetValidation(Resources.InvalidPhoneNumber);
                blnReturn = true;
            }
            if (txtIdNumber.Text.Trim() == "")
            {
                this.selectTab(tabGENERAL_INFORMATION);
                txtIdNumber.SetValidation(string.Format(Resources.REQUIRED, lblID_CARD_NUMBER.Text));
                blnReturn = true;
            }
            if (cboIdCardType.SelectedIndex == -1)
            {
                this.cboIdCardType.SetValidation(string.Format(Resources.REQUIRED, lblID_CARD_NUMBER_TYPE.Text));
                blnReturn = true;
            }

            if (cboIdCardType.Text.Trim() == "")
            {
                this.selectTab(tabGENERAL_INFORMATION);
                cboIdCardType.SetValidation(string.Format(Resources.REQUIRED, lblID_CARD_NUMBER_TYPE.Text));
                blnReturn = true;
            }
            if (cboCustomerGroup.SelectedIndex == -1)
            {
                this.selectTab(tabUSAGE_INFORMATION);
                this.cboCustomerGroup.SetValidation(string.Format(Resources.REQUIRED, lblCONNECTION.Text));
                blnReturn = true;
            }

            if (cboPrice.SelectedIndex == -1)
            {
                this.selectTab(tabUSAGE_INFORMATION);
                cboPrice.SetValidation(string.Format(Resources.REQUIRED, Resources.PRICE));
                blnReturn = true;
            }
            if (cboBillingCycle.SelectedIndex == -1)
            {
                this.selectTab(tabUSAGE_INFORMATION);
                cboBillingCycle.SetValidation(Resources.SELECT_BILLING_CYCLE);
                blnReturn = true;
            }
            if (cboPhase.SelectedIndex == -1)
            {
                this.selectTab(tabUSAGE_INFORMATION);
                cboPhase.SetValidation(string.Format(Resources.REQUIRED, lblPHASE.Text));
                blnReturn = true;
            }
            if (cboAMP.SelectedIndex == -1)
            {
                this.selectTab(tabUSAGE_INFORMATION);
                cboAMP.SetValidation(string.Format(Resources.REQUIRED, lblAMPARE.Text));
                blnReturn = true;
            }
            if (cboVol.SelectedIndex == -1)
            {
                this.selectTab(tabUSAGE_INFORMATION);
                cboVol.SetValidation(string.Format(Resources.REQUIRED, lblVOLTAGE.Text));
                blnReturn = true;
            }
            if (!string.IsNullOrEmpty(txtPOWER_KW.Text.Trim()) && !DataHelper.IsPositiveNumber(txtPOWER_KW.Text))
            {
                this.selectTab(tabUSAGE_INFORMATION);
                txtPOWER_KW.SetValidation(string.Format(Resources.REQUIRED, lblPOWER_KW.Text));
                blnReturn = true;
            }
            if (chkCAPACITY_CHARGE.Checked && !DataHelper.IsPositiveNumber(txtPOWER_CAPACITY.Text))
            {
                this.selectTab(tabUSAGE_INFORMATION);
                txtPOWER_CAPACITY.SetValidation(Resources.REQUIRED_POSITIVE_NUMBER);
                blnReturn = true;
            }

            // merge customer , non-base customer 
            // change billing cycle is not allows.
            if (this._flag == GeneralProcess.Update)
            {
                // billing cycle was change.
                if (this._objOld.BILLING_CYCLE_ID != (int)cboBillingCycle.SelectedValue)
                {
                    var basedCustomer = new TBL_CUSTOMER();
                    // for merge usage.
                    if (this._objOld.USAGE_CUSTOMER_ID != 0 && this._objOld.USAGE_CUSTOMER_ID != this._objOld.CUSTOMER_ID)
                    {
                        basedCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == this._objOld.USAGE_CUSTOMER_ID);
                        MsgBox.ShowInformation(string.Format(Resources.MS_CANNOT_CHANGE_BILLING_CYCLE_MERGED_CUSTOMER, basedCustomer.CUSTOMER_CODE + " " + basedCustomer.LAST_NAME_KH + " " + basedCustomer.FIRST_NAME_KH), "");
                        return true;
                    }
                    // for merge invoice
                    if (this._objOld.INVOICE_CUSTOMER_ID != 0 && this._objOld.INVOICE_CUSTOMER_ID != this._objOld.CUSTOMER_ID)
                    {
                        basedCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == this._objOld.INVOICE_CUSTOMER_ID);
                        MsgBox.ShowInformation(string.Format(Resources.MS_CANNOT_CHANGE_BILLING_CYCLE_MERGED_CUSTOMER, basedCustomer.CUSTOMER_CODE + " " + basedCustomer.LAST_NAME_KH + " " + basedCustomer.FIRST_NAME_KH), "");
                        return true;
                    }
                }
            }

            if (new SupportTaker(SupportTaker.Process.AddCustomer).Check())
            {
                return true;
            }

            return blnReturn;
        }

        private void newCus()
        {
            this.bind();
            lstConnectionType = DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs.Where(x=>x.IS_ACTIVE).ToList();
            UIHelper.SetDataSourceToComboBox(this.cboConnectionType, this.lstConnectionType.Where(x => x.NONLICENSE_CUSTOMER_GROUP_ID == 4));
            this.cboConnectionType.SelectedValue = DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs.FirstOrDefault(x => x.NONLICENSE_CUSTOMER_GROUP_ID == 4).CUSTOMER_CONNECTION_TYPE_ID;

            this.dtpDOB.ClearValue();
            this.txtCustomerCode.Text = getNextCustomerID();

            TBL_COMPANY objCompany = DBDataContext.Db.TBL_COMPANies.FirstOrDefault(c => c.COMPANY_ID == 1);
            this.loadVillage(objCompany.VILLAGE_CODE);
            this.txtLastNameKH.Focus();

            // clear data
            this.cboArea.SelectedIndex = -1;

            this.cboAMP.SelectedIndex = -1;

            this.cboCustomerType.SelectedIndex = 1;

            if (this.cboPrice.Items.Count > 1)
            {
                this.cboPrice.SelectedIndex = -1;
            }
            if (this.cboBillingCycle.Items.Count > 1)
            {
                this.cboBillingCycle.SelectedIndex = -1;
            }
        }

        private void read()
        {
            this.dtpActivateDate.SetValue(this._objNew.ACTIVATE_DATE);
            this.txtAddress.Text = this._objNew.ADDRESS;
            this.cboAMP.SelectedValue = this._objNew.AMP_ID;
            this.cboArea.SelectedValue = this._objNew.AREA_ID;
            this.cboBillingCycle.SelectedValue = this._objNew.BILLING_CYCLE_ID;
            this.txtCheckDigit.Text = this._objNew.CHECK_DIGIT;
            // _objNew.CLOSED_DATE 

            lstConnectionType = DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs.Where(x=>x.IS_ACTIVE).ToList();

            int groupId = (from g in DBDataContext.Db.TLKP_CUSTOMER_GROUPs
                           join t in DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs.Where(x => x.CUSTOMER_CONNECTION_TYPE_ID == _objNew.CUSTOMER_CONNECTION_TYPE_ID)
                           on g.CUSTOMER_GROUP_ID equals t.NONLICENSE_CUSTOMER_GROUP_ID
                           select g.CUSTOMER_GROUP_ID).FirstOrDefault();
            this.cboCustomerGroup.SelectedValue = groupId;
            UIHelper.SetDataSourceToComboBox(cboConnectionType, lstConnectionType.Where(x => x.NONLICENSE_CUSTOMER_GROUP_ID == groupId).OrderBy(x => x.DESCRIPTION));
            this.cboConnectionType.SelectedValue = this._objNew.CUSTOMER_CONNECTION_TYPE_ID;


            this.txtCompanyName.Text = this._objNew.COMPANY_NAME;
            this.txtCustomerCode.Text = this._objNew.CUSTOMER_CODE;
            //this.cboCustomerType.SelectedValue = this._objNew.CUSTOMER_TYPE_ID;
            this.chkIsmarketVendor.Checked = _objNew.CUSTOMER_TYPE_ID == -2 ? true : false;
            this.chkCAPACITY_CHARGE.Checked = _objNew.POWER_CAPACITY > 0;
            this.txtPOWER_CAPACITY.Enabled = chkCAPACITY_CHARGE.Checked;
            this.nudCutoffDays.Value = this._objNew.CUT_OFF_DAY;
            this.txtFirstName.Text = this._objNew.FIRST_NAME;

            this.txtFirstNameKH.Text = this._objNew.FIRST_NAME_KH;
            this.cboSex.SelectedValue = this._objNew.GENDER_ID;
            this.txtHouseNo.Text = this._objNew.HOUSE_NO;
            this.txtIdNumber.Text = this._objNew.ID_CARD_NO;
            this.txtJob.Text = this._objNew.JOB;
            this.txtLastName.Text = this._objNew.LAST_NAME;
            this.txtLastNameKH.Text = this._objNew.LAST_NAME_KH;
            this.txtTotalFamily.Text = this._objNew.TOTAL_FAMILY.ToString();

            this.cboPhase.SelectedValue = this._objNew.PHASE_ID;
            this.txtPhone1.Text = this._objNew.PHONE_1;
            this.txtPhone2.Text = this._objNew.PHONE_2;
            this.picPhoto.Image = UIHelper.ConvertBinaryToImage(this._objNew.PHOTO);
            this.cboPrice.SelectedValue = this._objNew.PRICE_ID;

            //_objNew.STATUS_ID; 
            this.txtStreetNo.Text = this._objNew.STREET_NO;
            this.loadVillage(this._objNew.VILLAGE_CODE);
            this.cboVol.SelectedValue = this._objNew.VOL_ID;
            this.dtpDOB.SetValue(this._objNew.BIRTH_DATE);
            this.txtPlaceofBirth.Text = this._objNew.BIRTH_PLACE;
            this.dtpREQUEST_CONNECTION_DATE.Value = this._objNew.REQUEST_CONNECTION_DATE;

            //_objCustomer Title
            this.cboTitles.SelectedValue = this._objNew.TITLE_ID;

            //_objID Card Type
            this.cboIdCardType.SelectedValue = this._objNew.ID_TYPE;

            //_objCustomer Tax Type
            this.cboCustomerType.SelectedValue = this._objNew.TAX_CUSTOMER_TYPE_ID;

            // read customer equipment.
            readCustomerEquipment();

            // read customer meter.
            readCustomerMeter();

            //read customer power_kw
            this.txtPOWER_KW.Text = this._objNew.POWER_KW.ToString("#.####");
            this.txtPOWER_CAPACITY.Text = this._objNew.POWER_CAPACITY.ToString("#.####");
            this.chkCAPACITY_CHARGE.Checked = this._objNew.POWER_CAPACITY > 0;

        }

        private void readCustomerMeter()
        {
            this._objCustomerMeter = DBDataContext.Db.TBL_CUSTOMER_METERs.FirstOrDefault(r => r.CUSTOMER_ID == this._objNew.CUSTOMER_ID && r.IS_ACTIVE);
            if (this._objCustomerMeter != null)
            {
                this.cboCableShield.SelectedValue = this._objCustomerMeter.CABLE_SHIELD_ID;
                this.cboMeterShield.SelectedValue = this._objCustomerMeter.METER_SHIELD_ID;
                this._objMeter = DBDataContext.Db.TBL_METERs.FirstOrDefault(m => m.METER_ID == this._objCustomerMeter.METER_ID);
                this.txtMeter.Text = this._objMeter.METER_CODE;
                this.txtMeter.AcceptSearch();


                this._objBreaker = DBDataContext.Db.TBL_CIRCUIT_BREAKERs.FirstOrDefault(b => b.BREAKER_ID == this._objCustomerMeter.BREAKER_ID);
                this.txtBreaker.Text = this._objBreaker.BREAKER_CODE;
                this.txtBreaker.AcceptSearch();

                this._objBox = DBDataContext.Db.TBL_BOXes.FirstOrDefault(b => b.BOX_ID == this._objCustomerMeter.BOX_ID);
                this.txtBox.Text = this._objBox.BOX_CODE;
                this.txtBox.AcceptSearch();

                this.txtPositionInBox.Text = _objCustomerMeter.POSITION_IN_BOX.ToString();
            }
        }

        private void readCustomerEquipment()
        {
            var qry = from ce in DBDataContext.Db.TBL_CUSTOMER_EQUIPMENTs
                      join e in DBDataContext.Db.TBL_EQUIPMENTs on ce.EQUIPMENT_ID equals e.EQUIPMENT_ID
                      where ce.CUSTOMER_ID == this._objNew.CUSTOMER_ID
                      select new
                      {
                          CUS_EQUIPMENT_ID = ce.CUS_EQUIPMENT_ID,
                          EQUIPMENT_ID = e.EQUIPMENT_ID,
                          EQUIPMENT_NAME = e.EQUIPMENT_NAME,
                          QUANTITY = ce.QUANTITY,
                          POWER = ce.POWER,
                          TOTAL_POWER = ce.TOTAL_POWER,
                          COMMENT = ce.COMMENT,
                          _DB = true,   // dummy field.
                          _EDITED = false  // dummy field.
                      };
            foreach (DataRow row in qry._ToDataTable().Rows)
            {
                this.dgvEquitment.Rows.Add(row.ItemArray);
            }
            this.calculateTotalPower();
        }

        private void bind()
        {
            //Sex
            UIHelper.SetDataSourceToComboBox(cboSex, DBDataContext.Db.TLKP_SEXes);

            //Customertype
            //UIHelper.SetDataSourceToComboBox(cboCustomerType, Lookup.GetCustomerTypes());

            //Phase
            UIHelper.SetDataSourceToComboBox(cboPhase, Lookup.GetPowerPhases());

            //Amp
            UIHelper.SetDataSourceToComboBox(cboAMP, Lookup.GetPowerAmpare());

            //Vol
            UIHelper.SetDataSourceToComboBox(cboVol, Lookup.GetPowerVoltage());

            //province
            UIHelper.SetDataSourceToComboBox(cboProvince, DBDataContext.Db.TLKP_PROVINCEs);

            //Area
            UIHelper.SetDataSourceToComboBox(cboArea, Lookup.GetAreas());

            //Price
            UIHelper.SetDataSourceToComboBox(cboPrice, Lookup.GetPrices());

            //Billing cycle
            UIHelper.SetDataSourceToComboBox(cboBillingCycle, Lookup.GetBillingCycles());

            // Seal
            UIHelper.SetDataSourceToComboBox(cboMeterShield, Lookup.GetSeals());
            UIHelper.SetDataSourceToComboBox(cboCableShield, Lookup.GetSeals());

            //Customer Group
            UIHelper.SetDataSourceToComboBox(cboCustomerGroup, DBDataContext.Db.TLKP_CUSTOMER_GROUPs.Where(x => x.IS_ACTIVE == true).OrderBy(x => x.DESCRIPTION));

            //Customer Connection Type 
            UIHelper.SetDataSourceToComboBox(cboConnectionType, lstConnectionType);

            //Cutomer Title
            UIHelper.SetDataSourceToComboBox(cboTitles, DBDataContext.Db.TLKP_TITLEs.ToList());

            //Customer Indentification
            UIHelper.SetDataSourceToComboBox(cboIdCardType, DBDataContext.Db.TLKP_IDENTIFICATION_TYPEs.ToList());

            //Customer Tax Type 
            UIHelper.SetDataSourceToComboBox(cboCustomerType, DBDataContext.Db.TLKP_TAX_CUSTOMER_TYPEs.ToList());

            //Currency
            UIHelper.SetDataSourceToComboBox(cboCurrency, DBDataContext.Db.TLKP_CURRENCies.Select(x => new { x.CURRENCY_ID, x.CURRENCY_NAME })._ToDataTable(), Resources.ALL_CURRENCY);
        }

        private void write()
        {
            _objNew.ADDRESS = txtAddress.Text;
            _objNew.AMP_ID = int.Parse(cboAMP.SelectedValue.ToString());
            _objNew.AREA_ID = int.Parse(cboArea.SelectedValue.ToString());

            _objNew.CHECK_DIGIT = txtCheckDigit.Text;
            _objNew.CLOSED_DATE = UIHelper._DefaultDate;
            _objNew.COMPANY_NAME = txtCompanyName.Text;
            _objNew.CREATED_BY = Login.CurrentLogin.LOGIN_NAME;
            // insert 1 to customer type, because It's not allow to insert null
            //_objNew.CUSTOMER_TYPE_ID = _objOld.CUSTOMER_TYPE_ID == 0 ? 1 : _objOld.CUSTOMER_TYPE_ID;

            _objNew.CUSTOMER_TYPE_ID = chkIsmarketVendor.Checked == true ? -2 : 1;
            _objNew.CUSTOMER_CONNECTION_TYPE_ID = int.Parse(cboConnectionType.SelectedValue.ToString());

            _objNew.FIRST_NAME = txtFirstName.Text;
            _objNew.FIRST_NAME_KH = txtFirstNameKH.Text;
            _objNew.GENDER_ID = int.Parse(cboSex.SelectedValue.ToString());
            _objNew.HOUSE_NO = txtHouseNo.Text;
            _objNew.ID_CARD_NO = txtIdNumber.Text;
            _objNew.JOB = txtJob.Text;
            _objNew.LAST_NAME = txtLastName.Text;
            _objNew.LAST_NAME_KH = txtLastNameKH.Text;
            _objNew.PHASE_ID = int.Parse(cboPhase.SelectedValue.ToString());
            _objNew.PHONE_1 = txtPhone1.Text;
            _objNew.PHONE_2 = txtPhone2.Text;
            _objNew.PHOTO = UIHelper.ConvertImageToBinary(picPhoto.Image);
            _objNew.TOTAL_FAMILY = DataHelper.ParseToInt(this.txtTotalFamily.Text);


            _objNew.STREET_NO = txtStreetNo.Text;
            _objNew.VILLAGE_CODE = cboVillage.SelectedValue.ToString();
            _objNew.VOL_ID = int.Parse(cboVol.SelectedValue.ToString());
            _objNew.POWER_KW = DataHelper.ParseToDecimal(txtPOWER_KW.Text);
            _objNew.POWER_CAPACITY = chkCAPACITY_CHARGE.Checked == false ? 0 : DataHelper.ParseToDecimal(txtPOWER_CAPACITY.Text);

            _objNew.BIRTH_DATE = dtpDOB.Value;
            _objNew.BIRTH_PLACE = this.txtPlaceofBirth.Text;
            _objNew.IS_POST_PAID = true;
            _objNew.REQUEST_CONNECTION_DATE = dtpREQUEST_CONNECTION_DATE.Value;

            // billing cycle
            _objNew.BILLING_CYCLE_ID = (int)this.cboBillingCycle.SelectedValue;
            _objNew.PRICE_ID = (int)this.cboPrice.SelectedValue;

            //_objCustomer Title
            _objNew.TITLE_ID = (int)this.cboTitles.SelectedValue;

            //_objID Card Type
            _objNew.ID_TYPE = (int)this.cboIdCardType.SelectedValue;

            //_objCustomer Tax Type
            _objNew.TAX_CUSTOMER_TYPE_ID = (int)this.cboCustomerType.SelectedValue;

            //MV Customer
            if (DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.BusinessMVCustomer
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.IndustryMVCustomer
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.BusinessTOU_MVCustomer
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.IndustryTOU_MVCustomer
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.LicenseeMVCustomer
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.MINING_INDUSTRY_MV
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.MANUFACTURING_INDUSTRY_MV
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.TEXTILE_INDUSTRY_MV
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.AGRICULTURE_MV
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.ACTIVITIES_MV
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.AGRICULTURAL_MV
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.BUSINESS_MV
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.PUBLIC_ADMINISTRATION_MV
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.OTHER_SERVICES_MV
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.AGRICULTURAL_WATER_9_TO_7
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.MINING_INDUSTRY_BUY_MV_7_TO_9
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.MINING_INDUSTRY_BUY_MV_9_TO_7
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.MINING_INDUSTRY_BUY_MV_SUN
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.MANUFACTURIING_INDUSTRY_BUY_MV_7_TO_9
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.MANUFACTURIING_INDUSTRY_BUY_MV_9_TO_7
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.MANUFACTURIING_INDUSTRY_BUY_MV_SUN
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.TXTTILE_INDUSTRY_BUY_MV_7_TO_9
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.TXTTILE_INDUSTRY_BUY_MV_9_TO_7
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.TXTTILE_INDUSTRY_BUY_MV_SUN
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.AGRICULTURE_BUY_MV_7_TO_9
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.AGRICULTURE_BUY_MV_9_TO_7
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.AGRICULTURE_BUY_MV_SUN
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.ACTIVITIES_BUY_MV_7_TO_9
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.ACTIVITIES_BUY_MV_9_TO_7
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.ACTIVITIES_BUY_MV_SUN
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.AGRICULTURAL_BUY_MV_7_TO_9
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.AGRICULTURAL_BUY_MV_9_TO_7
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.AGRICULTURAL_BUY_MV_SUN
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.BUSINESS_BUY_MV_7_TO_9
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.BUSINESS_BUY_MV_9_TO_7
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.BUSINESS_BUY_MV_SUN
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.PUBLIC_ADMINISTRATION_BUY_MV_7_TO_9
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.PUBLIC_ADMINISTRATION_BUY_MV_9_TO_7
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.PUBLIC_ADMINISTRATION_BUY_MV_SUN
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.OTHER_SERVICES_BUY_MV_7_TO_9
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.OTHER_SERVICES_BUY_MV_9_TO_7
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.OTHER_SERVICES_BUY_MV_SUN
                )
            {
                _objNew.IS_MV = true;
            }
            else
            {
                _objNew.IS_MV = false;
            }

            if (this._flag == GeneralProcess.Insert)
            {
                _objNew.CREATED_ON = DBDataContext.Db.GetSystemDate();
                _objNew.STATUS_ID = (int)CustomerStatus.Pending;
                _objNew.ACTIVATE_DATE = UIHelper._DefaultDate;
                _objNew.CUT_OFF_DAY = 0;

            }
            else if (this._flag == GeneralProcess.Update)
            {
                _objNew.ACTIVATE_DATE = this.dtpActivateDate.Value;
                _objNew.CUT_OFF_DAY = (int)this.nudCutoffDays.Value;

                //UPDATE POSITION_IN_BOX
                if (_objNew.STATUS_ID == (int)CustomerStatus.Active)
                {
                    TBL_CUSTOMER_METER objCustomerMeter = DBDataContext.Db.TBL_CUSTOMER_METERs.FirstOrDefault(x => x.CUSTOMER_ID == _objNew.CUSTOMER_ID && x.IS_ACTIVE);
                    objCustomerMeter.POSITION_IN_BOX = DataHelper.ParseToInt(txtPositionInBox.Text);
                }
            }
            else if (this._flag == GeneralProcess.Delete)
            {
                _objNew.STATUS_ID = (int)CustomerStatus.Cancelled;
                // do not update activate,billing cycle,price, cutoffday
            }
        }

        private string getNextCustomerID()
        {
            //string CUSTOMER_FORMAT = new string('0', Properties.Settings.Default.CUSTOMER_DIGIT);
            //if (DBDataContext.Db.TBL_CUSTOMERs.Count()> 0)
            //{
            //    // get new id. 
            //    int newID = DBDataContext.Db.TBL_CUSTOMERs.Max(c => Convert.ToInt32( c.CUSTOMER_CODE)) + 1; 
            //    return newID.ToString(CUSTOMER_FORMAT);
            //}
            //else
            //{

            //    return  1.ToString(CUSTOMER_FORMAT);
            //}
            return Method.GetNextSequence(Sequence.Customer, false);
        }
        private void calculateTotalPower()
        {
            decimal total = 0.0m;
            foreach (DataGridViewRow row in this.dgvEquitment.Rows)
            {
                if (row.Visible)
                {
                    total += (decimal)row.Cells[TOTAL_WATT.Name].Value;
                }
            }
            this.txtTotalPower.Text = total.ToString("N2");
        }

        /// <summary>
        /// display total due amount.
        /// </summary>
        private void loadTotalDueAmount()
        {
            var totalAmount = from i in DBDataContext.Db.TBL_INVOICEs
                              join c in DBDataContext.Db.TLKP_CURRENCies on i.CURRENCY_ID equals c.CURRENCY_ID
                              where i.CUSTOMER_ID == _objNew.CUSTOMER_ID && i.INVOICE_STATUS != (int)InvoiceStatus.Cancel
                              group i by new { c.CURRENCY_ID, c.CURRENCY_SING, c.FORMAT } into tmpBalance

                              select new
                              {
                                  tmpBalance.Key.CURRENCY_ID,
                                  tmpBalance.Key.CURRENCY_SING,
                                  tmpBalance.Key.FORMAT,
                                  TOTAL_BALANCE = tmpBalance.Sum(x => x.SETTLE_AMOUNT - x.PAID_AMOUNT)
                              };
            dgvTotalBalance.DataSource = totalAmount;
            //if (totalAmount.Count() > 0)
            //{
            //    decimal TotaldueAmount = totalAmount.Sum(x => x.SETTLE_AMOUNT - x.PAID_AMOUNT);
            //    txtBalance.Text = UIHelper.FormatCurrency(TotaldueAmount);
            //}
        }

        private void bindDeposit()
        {
            var lstDeposit = from de in DBDataContext.Db.TBL_CUS_DEPOSITs
                             join ac in DBDataContext.Db.TLKP_DEPOSIT_ACTIONs on de.DEPOSIT_ACTION_ID equals ac.DEPOSIT_ACTION_ID
                             join c in DBDataContext.Db.TLKP_CURRENCies on de.CURRENCY_ID equals c.CURRENCY_ID
                             where de.CUSTOMER_ID == _objNew.CUSTOMER_ID
                             orderby de.CURRENCY_ID, de.CUS_DEPOSIT_ID
                             select new
                             {
                                 de.CUS_DEPOSIT_ID,
                                 de.DEPOSIT_NO,
                                 de.DEPOSIT_DATE,
                                 de.CREATE_BY,
                                 ac.DEPOSIT_ACTION_NAME_KH,
                                 de.AMOUNT,
                                 de.BALANCE,
                                 c.CURRENCY_SING,
                                 de.IS_PAID
                             };
            dgvDeposit.DataSource = lstDeposit;

            var dBalance = from de in DBDataContext.Db.TBL_CUS_DEPOSITs
                           join c in DBDataContext.Db.TLKP_CURRENCies on de.CURRENCY_ID equals c.CURRENCY_ID
                           where de.CUSTOMER_ID == _objNew.CUSTOMER_ID && de.IS_PAID
                           group de by new { c.CURRENCY_ID, c.CURRENCY_SING } into tmpBalance
                           select new
                           {
                               tmpBalance.Key.CURRENCY_ID,
                               tmpBalance.Key.CURRENCY_SING,
                               tmpBalance.OrderByDescending(x => x.CUS_DEPOSIT_ID).FirstOrDefault().BALANCE
                           };
            dgvBalanceDeposit.DataSource = dBalance;

            if (dBalance.Count() > 0)
            {
                btnAPPLY_PAYMENT.Enabled = true;
                btnREFUND_DEPOSIT.Enabled = true;
                btnADJUST_DEPOSIT.Enabled = true;
            }
            else
            {
                btnAPPLY_PAYMENT.Enabled = false;
                btnREFUND_DEPOSIT.Enabled = false;
                btnADJUST_DEPOSIT.Enabled = false;
            }
        }

        private void bindService()
        {
            this.dgvService.DataSource = from cs in DBDataContext.Db.TBL_CUSTOMER_SERVICEs
                                         join s in DBDataContext.Db.TBL_INVOICE_ITEMs on cs.INVOICE_ITEM_ID equals s.INVOICE_ITEM_ID
                                         where cs.CUSTOMER_ID == this._objNew.CUSTOMER_ID && cs.IS_ACTIVE
                                         select new
                                         {
                                             cs.CUSTOMER_SERVICE_ID,
                                             s.INVOICE_ITEM_NAME,
                                             cs.QTY,
                                             s.PRICE,
                                             cs.LAST_BILLING_MONTH,
                                             s.RECURRING_MONTH,
                                             cs.NEXT_BILLING_MONTH
                                         };
        }

        /// <summary>
        /// 1 New Customer 
        /// 2 OldBillingMonth==NewBillingMonth
        /// 3 Customer Never Issue Invoice Power
        /// 4 Cycle Never Run and No Customer
        /// </summary>
        /// <returns></returns>
        private bool IsInvalidBillingCycle()
        {
            bool invalid = false;
            DateTime oldBillMonnth = Method.GetNextBillingMonth(_objOld.BILLING_CYCLE_ID);
            DateTime newBillMonth = Method.GetNextBillingMonth(_objNew.BILLING_CYCLE_ID);
            int totalInvoice = DBDataContext.Db.TBL_INVOICEs.Count(x => x.CUSTOMER_ID == _objNew.CUSTOMER_ID && !x.IS_SERVICE_BILL);
            int totalCycleRun = DBDataContext.Db.TBL_RUN_BILLs.Count(x => x.CYCLE_ID == _objNew.BILLING_CYCLE_ID);
            int totalCustomerInCycle = DBDataContext.Db.TBL_CUSTOMERs.Count(x => x.BILLING_CYCLE_ID == _objNew.BILLING_CYCLE_ID && x.STATUS_ID == (int)CustomerStatus.Active);
            if (_flag == GeneralProcess.Insert || oldBillMonnth == newBillMonth || totalInvoice <= 0 || (totalCycleRun <= 0 && totalCustomerInCycle <= 0))
            {
                invalid = false;
            }
            else
            {
                invalid = true;
                MsgBox.ShowInformation(string.Format(Resources.MS_CUTOMER_CANNOT_CHANGE_BILLING_CYCLE, oldBillMonnth.ToString("MM-yyyy"), newBillMonth.ToString("MM-yyyy")));
            }
            return invalid;
        }

        private bool saveCustomer()
        {
            if (invalid())
            {
                return false;
            }

            this.write();

            if (IsInvalidBillingCycle())
            {
                cboBillingCycle.SelectedValue = _objOld.BILLING_CYCLE_ID;
                _objNew.BILLING_CYCLE_ID = (int)cboBillingCycle.SelectedValue;
                return false;
            }


            try
            {
                Runner.RunNewThread(() =>
                {
                    using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                    {
                        //renew customer code & update sequance for each save
                        if (_flag == GeneralProcess.Insert)
                        {
                            _objNew.CUSTOMER_CODE = Method.GetNextSequence(Sequence.Customer, true);
                        }

                        TBL_CHANGE_LOG objChangeLog = new TBL_CHANGE_LOG();

                        // save general infomation.
                        if (this._flag == GeneralProcess.Insert)
                        {
                            objChangeLog = DBDataContext.Db.Insert(_objNew);
                        }
                        else // both delete and edit are just update
                        {
                            objChangeLog = DBDataContext.Db.Update(this._objOld, this._objNew);
                        }

                        // save customer equipment. 
                        foreach (DataGridViewRow row in this.dgvEquitment.Rows)
                        {
                            TBL_CUSTOMER_EQUIPMENT objENew = new TBL_CUSTOMER_EQUIPMENT()
                            {
                                CUS_EQUIPMENT_ID = (int)row.Cells[this.CUS_EQUIPMENT_ID.Name].Value,
                                CUSTOMER_ID = this._objNew.CUSTOMER_ID,
                                EQUIPMENT_ID = (int)row.Cells[this.EQUIPMENT_ID.Name].Value,
                                POWER = (decimal)row.Cells[this.WATT.Name].Value,
                                QUANTITY = (decimal)row.Cells[this.QUANTITY.Name].Value,
                                TOTAL_POWER = (decimal)row.Cells[this.TOTAL_WATT.Name].Value,
                                COMMENT = row.Cells[this.NOTE.Name].Value.ToString()
                            };
                            if (row.Visible)
                            {
                                if ((bool)row.Cells["_DB"].Value)
                                {
                                    // all data from db
                                    // update only field that face update.
                                    if ((bool)row.Cells["_EDITED"].Value)
                                    {
                                        TBL_CUSTOMER_EQUIPMENT tmp = DBDataContext.Db.TBL_CUSTOMER_EQUIPMENTs.FirstOrDefault(r => r.CUS_EQUIPMENT_ID == objENew.CUS_EQUIPMENT_ID);
                                        if (tmp != null)
                                        {
                                            TBL_CUSTOMER_EQUIPMENT objEOld = new TBL_CUSTOMER_EQUIPMENT();
                                            tmp._CopyTo(objEOld);
                                            DBDataContext.Db.UpdateChild(objEOld, objENew, this._objNew, ref objChangeLog);
                                        }
                                    }
                                }
                                else
                                {
                                    DBDataContext.Db.InsertChild(objENew, this._objNew, ref objChangeLog);
                                }
                            }
                            else
                            {
                                if ((bool)row.Cells["_DB"].Value)
                                {
                                    DBDataContext.Db.DeleteChild(objENew, this._objNew, ref objChangeLog);
                                }
                            }
                        }

                        // update billing cycle for merge customer
                        if (_flag == GeneralProcess.Update && _objOld.BILLING_CYCLE_ID != _objNew.BILLING_CYCLE_ID)
                        {
                            var relatedCustomers = DBDataContext.Db.TBL_CUSTOMERs.Where(x =>
                                                    (x.INVOICE_CUSTOMER_ID == _objNew.CUSTOMER_ID || x.USAGE_CUSTOMER_ID == _objNew.CUSTOMER_ID)
                                                     && x.CUSTOMER_ID != _objNew.CUSTOMER_ID);
                            foreach (var relatedCustomer in relatedCustomers)
                            {
                                relatedCustomer.BILLING_CYCLE_ID = _objNew.BILLING_CYCLE_ID;
                            }
                            DBDataContext.Db.SubmitChanges();
                        }

                        // update ROW_DATE of TBL_B24_CUSTOMER
                        if (_flag == GeneralProcess.Update && (_objOld.FIRST_NAME_KH != _objNew.FIRST_NAME_KH || _objOld.LAST_NAME_KH != _objNew.LAST_NAME_KH ||
                                                            _objOld.PHONE_1 != _objNew.PHONE_1 || _objOld.PHONE_2 != _objNew.PHONE_2))
                        {
                            B24_Integration.UpdateCustomerRowDate(_objNew.CUSTOMER_ID, true);
                        }

                        //if(_flag == GeneralProcess.Update && _objNew.ACTIVATE_DATE != _objOld.ACTIVATE_DATE)
                        //{
                        //    var k = DBDataContext.Db.TBL_CUSTOMER_METERs.OrderBy(x => x.CUS_METER_ID).FirstOrDefault(s => s.CUSTOMER_ID == _objNew.CUSTOMER_ID);
                        //    var kk = DBDataContext.Db.TBL_CUS_STATUS_CHANGEs.OrderBy(x => x.CHNAGE_ID).FirstOrDefault(s => s.CUSTOMER_ID == _objNew.CUSTOMER_ID);
                        //    k.USED_DATE = kk.CHNAGE_DATE = this.dtpActivateDate.Value;
                        //    DBDataContext.Db.SubmitChanges();
                        //}

                        tran.Complete();
                        this.DialogResult = DialogResult.OK;
                    }
                });
                return true;
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
                return false;
            }
        }

        #endregion Method

        #region Event

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.saveCustomer();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnActivate_Click(object sender, EventArgs e)
        {
            if (saveCustomer())
            {
                DialogCustomerActivate diag = new DialogCustomerActivate(this._objNew);
                if (diag.ShowDialog() == DialogResult.OK)
                {
                    this.CustomerIsActivated = true;
                }
                this.DialogResult = DialogResult.OK;
            }
        }

        private void loadVillage(string villageCode)
        {
            this.cboProvince.SelectedIndex = -1;
            this.cboDistrict.SelectedIndex = -1;
            this.cboCommune.SelectedIndex = -1;
            this.cboVillage.SelectedIndex = -1;

            if (DBDataContext.Db.TLKP_VILLAGEs.FirstOrDefault(row => row.VILLAGE_CODE == villageCode) != null)
            {
                string communeCode = DBDataContext.Db.TLKP_VILLAGEs.FirstOrDefault(row => row.VILLAGE_CODE == villageCode).COMMUNE_CODE;
                string districCode = DBDataContext.Db.TLKP_COMMUNEs.FirstOrDefault(row => row.COMMUNE_CODE == communeCode).DISTRICT_CODE;
                string province = DBDataContext.Db.TLKP_DISTRICTs.FirstOrDefault(row => row.DISTRICT_CODE == districCode).PROVINCE_CODE;

                UIHelper.SetDataSourceToComboBox(this.cboDistrict, DBDataContext.Db.TLKP_DISTRICTs.Where(row => row.PROVINCE_CODE == province));
                UIHelper.SetDataSourceToComboBox(this.cboCommune, DBDataContext.Db.TLKP_COMMUNEs.Where(row => row.DISTRICT_CODE == districCode));
                UIHelper.SetDataSourceToComboBox(this.cboVillage, DBDataContext.Db.TLKP_VILLAGEs.Where(row => row.COMMUNE_CODE == communeCode));

                this.cboProvince.SelectedValue = province;
                this.cboDistrict.SelectedValue = districCode;
                this.cboCommune.SelectedValue = communeCode;
                this.cboVillage.SelectedValue = villageCode;
            }
        }

        private void cboProvince_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!this._loading && cboProvince.SelectedIndex != -1)
            {
                string strProvinceCode = cboProvince.SelectedValue.ToString();
                // distict
                UIHelper.SetDataSourceToComboBox(cboDistrict, DBDataContext.Db.TLKP_DISTRICTs.Where(d => d.PROVINCE_CODE == strProvinceCode));
            }
        }

        private void cboDistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!this._loading && cboProvince.SelectedIndex != -1)
            {
                string strDisCode = cboDistrict.SelectedValue.ToString();
                // communte
                UIHelper.SetDataSourceToComboBox(cboCommune, DBDataContext.Db.TLKP_COMMUNEs.Where(c => c.DISTRICT_CODE == strDisCode));
            }
        }

        private void cboCommune_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!this._loading && cboCommune.SelectedIndex != -1)
            {
                string strComCode = cboCommune.SelectedValue.ToString();
                // village
                UIHelper.SetDataSourceToComboBox(cboVillage, DBDataContext.Db.TLKP_VILLAGEs.Where(v => v.COMMUNE_CODE == strComCode));
            }
        }

        private void LinkPhoto_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            OpenFileDialog objDialog = new OpenFileDialog();
            objDialog.Filter = "(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|All files (*.*)|*.*";
            objDialog.ShowDialog();
            if (objDialog.FileName != "")
            {
                picPhoto.ImageLocation = objDialog.FileName;
            }
        }

        private void dtpDOB_MouseDown(object sender, MouseEventArgs e)
        {
            if (dtpDOB.Checked)
            {
                if (dtpDOB.Value.Date.Equals(UIHelper._DefaultDate))
                {
                    dtpDOB.SetValue(DateTime.Now.Date);
                }
                else
                {
                    dtpDOB.CustomFormat = UIHelper._DefaultDateFormat;
                }
            }
            else
            {
                dtpDOB.ClearValue();
            }
        }

        private void btnAdd_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DialogCustomerEquipment diag = new DialogCustomerEquipment(GeneralProcess.Insert, new TBL_CUSTOMER_EQUIPMENT() { CUSTOMER_ID = this._objNew.CUSTOMER_ID });
            if (diag.ShowDialog() == DialogResult.OK)
            {
                int newIndex = this.dgvEquitment.Rows.Add();
                DataGridViewRow row = this.dgvEquitment.Rows[newIndex];
                TBL_CUSTOMER_EQUIPMENT obj = diag.Object;
                row.Cells[CUS_EQUIPMENT_ID.Name].Value = obj.CUS_EQUIPMENT_ID;
                row.Cells[EQUIPMENT_ID.Name].Value = obj.EQUIPMENT_ID;
                row.Cells[EQUIPMENT_NAME.Name].Value = DBDataContext.Db.TBL_EQUIPMENTs.FirstOrDefault(r => r.EQUIPMENT_ID == obj.EQUIPMENT_ID).EQUIPMENT_NAME;
                row.Cells[QUANTITY.Name].Value = obj.QUANTITY;
                row.Cells[WATT.Name].Value = obj.POWER;
                row.Cells[TOTAL_WATT.Name].Value = obj.TOTAL_POWER;
                row.Cells[NOTE.Name].Value = obj.COMMENT;

                // make sure need to insert to db.
                row.Cells["_DB"].Value = false;
                // makse sure it's not update.
                // useful for update operation
                row.Cells["_EDITED"].Value = false;

                calculateTotalPower();
            }
        }
        private int getVisibleRows(DataGridView dgv)
        {
            int tmp = 0;
            foreach (DataGridViewRow row in dgv.Rows)
            {
                if (row.Visible)
                {
                    tmp++;
                }
            }
            return tmp;
        }
        private void btnEdit_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // since some row are hidden 
            // we need to check only number of visible 
            // because when all row in data grid view are 
            // hidden it select the last selected row 
            if (this.getVisibleRows(this.dgvEquitment) == 0)
            {
                return;
            }

            if (this.dgvEquitment.SelectedRows.Count == 0)
            {
                return;
            }

            DataGridViewRow row = this.dgvEquitment.SelectedRows[0];

            DialogCustomerEquipment diag = new DialogCustomerEquipment(
                GeneralProcess.Update,
                new TBL_CUSTOMER_EQUIPMENT()
                {
                    CUS_EQUIPMENT_ID = (int)row.Cells[CUS_EQUIPMENT_ID.Name].Value,
                    EQUIPMENT_ID = (int)row.Cells[EQUIPMENT_ID.Name].Value,
                    QUANTITY = (decimal)row.Cells[QUANTITY.Name].Value,
                    POWER = (decimal)row.Cells[WATT.Name].Value,
                    TOTAL_POWER = (decimal)row.Cells[TOTAL_WATT.Name].Value,
                    COMMENT = row.Cells[NOTE.Name].Value.ToString()
                }
            );

            if (diag.ShowDialog() == DialogResult.OK)
            {
                TBL_CUSTOMER_EQUIPMENT obj = diag.Object;
                row.Cells[CUS_EQUIPMENT_ID.Name].Value = obj.CUS_EQUIPMENT_ID;
                row.Cells[EQUIPMENT_ID.Name].Value = obj.EQUIPMENT_ID;
                row.Cells[EQUIPMENT_NAME.Name].Value = DBDataContext.Db.TBL_EQUIPMENTs.FirstOrDefault(r => r.EQUIPMENT_ID == obj.EQUIPMENT_ID).EQUIPMENT_NAME;
                row.Cells[QUANTITY.Name].Value = obj.QUANTITY;
                row.Cells[WATT.Name].Value = obj.POWER;
                row.Cells[TOTAL_WATT.Name].Value = obj.TOTAL_POWER;
                row.Cells[NOTE.Name].Value = obj.COMMENT;

                // flag that this row is update 
                // if it's from DB so update database.
                row.Cells[_EDITED.Name].Value = true;

                // update total power to display
                calculateTotalPower();
            }

        }

        private void btnRemove_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // since some row are hidden 
            // we need to check only number of visible 
            // because when all row in data grid view are 
            // hidden it select the last selected row 
            if (this.getVisibleRows(this.dgvEquitment) == 0)
            {
                return;
            }

            if (this.dgvEquitment.SelectedRows.Count == 0)
            {
                return;
            }
            DataGridViewRow row = this.dgvEquitment.SelectedRows[0];
            if (MsgBox.ShowQuestion(Resources.MSQ_REMOVE_DEVICE, Resources.REMOVE_DEVICE) == DialogResult.Yes)
            {
                row.Visible = false;
                // update total power.
                this.calculateTotalPower();
            };
        }

        private void InputEnglish(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void InputKhmer(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void txtMeter_AdvanceSearch(object sender, EventArgs e)
        {
            if (txtMeter.Text.Trim() != "")
            {
                string strMeterCode = txtMeter.Text;
                _objMeter = DBDataContext.Db.TBL_METERs.FirstOrDefault(m => m.METER_CODE == strMeterCode);
                if (_objMeter != null)
                {
                    TBL_METER_TYPE objMeterType = DBDataContext.Db.TBL_METER_TYPEs.FirstOrDefault(mt => mt.METER_TYPE_ID == _objMeter.METER_TYPE_ID);
                    txtMeterType.Text = objMeterType.METER_TYPE_NAME;
                    txtMeterAmp.Text = DBDataContext.Db.TBL_AMPAREs.FirstOrDefault(a => a.AMPARE_ID == objMeterType.METER_AMP_ID).AMPARE_NAME;
                    txtMeterPhase.Text = DBDataContext.Db.TBL_PHASEs.FirstOrDefault(p => p.PHASE_ID == objMeterType.METER_PHASE_ID).PHASE_NAME;
                    txtMeterVol.Text = DBDataContext.Db.TBL_VOLTAGEs.FirstOrDefault(v => v.VOLTAGE_ID == objMeterType.METER_VOL_ID).VOLTAGE_NAME;
                    txtMeterConstant.Text = DBDataContext.Db.TBL_CONSTANTs.FirstOrDefault(c => c.CONSTANT_ID == objMeterType.METER_CONST_ID).CONSTANT_NAME;
                    this.txtBreaker.Focus();
                }
                else
                {
                    this.txtMeter.CancelSearch();
                    MsgBox.ShowInformation(Resources.MS_METER_NOT_FOUND);
                }
            }
            else
            {
                txtMeter.CancelSearch();
            }
        }

        private void txtMeter_CancelAdvanceSearch(object sender, EventArgs e)
        {
            this._objMeter = null;
            txtMeterType.Text = "";
            txtMeterAmp.Text = "";
            txtMeterPhase.Text = "";
            txtMeterVol.Text = "";
            txtMeterConstant.Text = "";
        }

        private void txtBreaker_AdvanceSearch(object sender, EventArgs e)
        {
            if (txtBreaker.Text.Trim() != "")
            {
                string strBreakerCode = txtBreaker.Text;
                _objBreaker = DBDataContext.Db.TBL_CIRCUIT_BREAKERs.FirstOrDefault(b => b.BREAKER_CODE == strBreakerCode);
                if (_objBreaker != null)
                {
                    TBL_CIRCUIT_BREAKER_TYPE objBreakerType = DBDataContext.Db.TBL_CIRCUIT_BREAKER_TYPEs.FirstOrDefault(bt => bt.BREAKER_TYPE_ID == _objBreaker.BREAKER_TYPE_ID);
                    txtBreakerType.Text = objBreakerType.BREAKER_TYPE_NAME;
                    txtBreakerAmp.Text = DBDataContext.Db.TBL_AMPAREs.FirstOrDefault(a => a.AMPARE_ID == objBreakerType.BREAKER_AMP_ID).AMPARE_NAME;
                    txtBreakerPhase.Text = DBDataContext.Db.TBL_PHASEs.FirstOrDefault(p => p.PHASE_ID == objBreakerType.BREAKER_PHASE_ID).PHASE_NAME;
                    txtBreakerVol.Text = DBDataContext.Db.TBL_VOLTAGEs.FirstOrDefault(v => v.VOLTAGE_ID == objBreakerType.BREAKER_VOL_ID).VOLTAGE_NAME;
                    txtBreakerConstant.Text = DBDataContext.Db.TBL_CONSTANTs.FirstOrDefault(c => c.CONSTANT_ID == objBreakerType.BREAKER_CONST_ID).CONSTANT_NAME;
                    this.txtBox.Focus();
                }
                else
                {
                    txtBreaker.CancelSearch();
                    MsgBox.ShowInformation(Resources.MS_BREAKER_NOT_FOUND);
                }
            }
            else
            {
                txtBreaker.CancelSearch();
            }
        }

        private void txtBreaker_CancelAdvanceSearch(object sender, EventArgs e)
        {
            this._objBreaker = null;
            txtBreakerType.Text = "";
            txtBreakerAmp.Text = "";
            txtBreakerPhase.Text = "";
            txtBreakerVol.Text = "";
            txtBreakerConstant.Text = "";
        }

        private void txtBox_AdvanceSearch(object sender, EventArgs e)
        {
            if (txtBox.Text.Trim() != "")
            {
                string strBox = txtBox.Text;
                _objBox = DBDataContext.Db.TBL_BOXes.FirstOrDefault(b => b.BOX_CODE == strBox);
                if (_objBox != null)
                {
                    _objPole = DBDataContext.Db.TBL_POLEs.FirstOrDefault(p => p.POLE_ID == _objBox.POLE_ID);
                    txtPoleCode.Text = _objPole.POLE_CODE;
                    txtCollector.Text = DBDataContext.Db.TBL_EMPLOYEEs.FirstOrDefault(emp => emp.EMPLOYEE_ID == _objPole.COLLECTOR_ID).EMPLOYEE_NAME;
                    txtBiller.Text = DBDataContext.Db.TBL_EMPLOYEEs.FirstOrDefault(emp => emp.EMPLOYEE_ID == _objPole.BILLER_ID).EMPLOYEE_NAME;
                    txtCutter.Text = DBDataContext.Db.TBL_EMPLOYEEs.FirstOrDefault(emp => emp.EMPLOYEE_ID == _objPole.CUTTER_ID).EMPLOYEE_NAME;
                }
                else
                {
                    txtBox.CancelSearch();
                    MsgBox.ShowInformation(Resources.MS_BOX_NOT_FOUND);
                }
            }
            else
            {
                txtBox.CancelSearch();
            }
        }

        private void txtBox_CancelAdvanceSearch(object sender, EventArgs e)
        {
            _objPole = null;
            _objBox = null;
            txtPoleCode.Text = "";
            txtCollector.Text = "";
            txtBiller.Text = "";
            txtCutter.Text = "";
        }

        private void btnChangeMeter_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (_objNew.STATUS_ID == (int)CustomerStatus.Active)
            {
                DialogCustomerChangeMeter diag = new DialogCustomerChangeMeter(this._objNew);
                if (diag.ShowDialog() == DialogResult.OK)
                {
                    // read customer meter.
                    this.readCustomerMeter();
                }
            }

        }

        private void btnChangeBreaker_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (_objNew.STATUS_ID == (int)CustomerStatus.Active)
            {
                DialogCustomerChangeBreaker diag = new DialogCustomerChangeBreaker(this._objNew);
                if (diag.ShowDialog() == DialogResult.OK)
                {
                    // read customer meter.
                    this.readCustomerMeter();
                }
            }
        }

        private void btnChangeBox_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (_objNew.STATUS_ID == (int)CustomerStatus.Active)
            {
                DialogCustomerChangeBox diag = new DialogCustomerChangeBox(this._objNew);
                int oldBoxId = _objCustomerMeter.BOX_ID;
                if (diag.ShowDialog() == DialogResult.OK)
                {
                    // read customer meter.
                    this.readCustomerMeter();
                    cboArea.SelectedValue = (int)diag.Customer.AREA_ID;
                    this.btnSAVE.Focus();
                }
            }
        }

        private void cboInvoiceStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadInvoice();
        }

        private void loadInvoice()
        {
            if (cboInvoiceStatus.SelectedIndex != -1 && _flag == GeneralProcess.Update)
            {
                int intStatus = (int)cboInvoiceStatus.SelectedValue;
                try
                {
                    var invioceInfo = from i in DBDataContext.Db.TBL_INVOICEs
                                      join cc in DBDataContext.Db.TBL_CUSTOMERs on i.CUSTOMER_ID equals cc.CUSTOMER_ID
                                      join c in DBDataContext.Db.TLKP_CURRENCies on i.CURRENCY_ID equals c.CURRENCY_ID
                                      where
                                      i.CUSTOMER_ID == _objNew.CUSTOMER_ID
                                      &&
                                      (i.INVOICE_STATUS == intStatus || intStatus == 0)
                                      &&
                                      (!dtpYear.Checked || i.INVOICE_MONTH.Year == dtpYear.Value.Year)
                                      select new
                                      {
                                          i.INVOICE_ID,
                                          i.INVOICE_DATE,
                                          i.INVOICE_NO,
                                          i.INVOICE_TITLE,
                                          i.SETTLE_AMOUNT,
                                          i.PAID_AMOUNT,
                                          DUE_AMOUNT = i.SETTLE_AMOUNT - i.PAID_AMOUNT,
                                          c.CURRENCY_ID,
                                          c.CURRENCY_SING,
                                          DUE_DATE = i.DUE_DATE,//.AddDays(cc.CUT_OFF_DAY),
                                          DUE_DAY = (int)((DateTime.Now - i.DUE_DATE.AddDays(cc.CUT_OFF_DAY)).TotalDays > 0 && i.INVOICE_STATUS == (int)InvoiceStatus.Open ? (DateTime.Now - i.DUE_DATE).TotalDays : 0),
                                          i.IS_SERVICE_BILL,
                                          INVOICE_STATUS = i.INVOICE_STATUS,
                                          c.FORMAT
                                      };
                    dgvInvoiceList.DataSource = invioceInfo.OrderByDescending(x => x.INVOICE_DATE);
                    loadTotalDueAmount();

                }
                catch (Exception exp)
                {
                    MsgBox.ShowError(exp);
                }
            }
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }

        #endregion Event

        #region Adjust Invoice Event

        bool isNotValidToAdjust()
        {
            bool blnReturn = false;


            if (dgvInvoiceList.SelectedRows.Count == 0)
            {
                blnReturn = true;
            }
            else
            {

                TBL_INVOICE objInvoice = DBDataContext.Db.TBL_INVOICEs.FirstOrDefault(x => x.INVOICE_ID == Int32.Parse(dgvInvoiceList.SelectedRows[0].Cells[INVOICE_ID.Name].Value.ToString()));
                if (objInvoice.INVOICE_STATUS == (int)InvoiceStatus.Cancel)
                {
                    MsgBox.ShowInformation(Resources.MSG_INVOICE_REVERSED_CANNOT_ADJUST);
                    return true;
                }

                //can adust when the invoice is in last cycle
                if (objInvoice.IS_SERVICE_BILL == false)
                {
                    var enableAdjust = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_ADJUST_OLD_INVOICE]);
                    if (!enableAdjust)
                    {
                        DateTime datNextBillingMonth = Method.GetNextBillingMonth(objInvoice.CYCLE_ID);
                        if (datNextBillingMonth.AddMonths(-1).Date != objInvoice.INVOICE_MONTH.Date)
                        {
                            MsgBox.ShowInformation(Resources.MS_INVALID_ADJUST_INVOICE);
                            blnReturn = true;
                        }
                    }
                }

                //can adjust when
                //Invoice status == Open
                //OR Closeed Invoice that have settle amount == 0 
                if (objInvoice.PAID_AMOUNT > 0)
                {
                    MsgBox.ShowInformation(Resources.MS_CANNOT_ADJUST_PAID_INVOICE);
                    blnReturn = true;
                }
            }
            return blnReturn;
        }
        bool isNotValidToAdjustBasePrice()
        {
            bool blnReturn = false;


            if (dgvInvoiceList.SelectedRows.Count == 0)
            {
                blnReturn = true;
            }
            else
            {

                TBL_INVOICE objInvoice = DBDataContext.Db.TBL_INVOICEs.FirstOrDefault(x => x.INVOICE_ID == Int32.Parse(dgvInvoiceList.SelectedRows[0].Cells["INVOICE_ID"].Value.ToString()));

                //can adust when the invoice is in last cycle
                if (objInvoice.IS_SERVICE_BILL == false)
                {
                    var enableAdjust = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_ADJUST_OLD_INVOICE]);
                    if (!enableAdjust)
                    {
                        DateTime datNextBillingMonth = Method.GetNextBillingMonth(objInvoice.CYCLE_ID);
                        if (datNextBillingMonth.AddMonths(-1).Date != objInvoice.INVOICE_MONTH.Date)
                        {
                            MsgBox.ShowInformation(Resources.MS_INVALID_ADJUST_INVOICE);
                            blnReturn = true;
                        }
                    }
                }

                //can adjust when
                //Invoice status == Open
                //OR Closeed Invoice that have settle amount == 0 
            }
            return blnReturn;
        }

        private void linkAdjustUsage_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (isNotValidToAdjust())
            {
                return;
            }
            //if user not yet open cash drawer
            //not allow to adjust invoice
            if (Login.CurrentCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_ADJUST_INVOICE, string.Empty) == DialogResult.Yes)
                    new DialogOpenCashDrawer().ShowDialog();
                if (Login.CurrentCashDrawer == null)
                    return;
            }

            TBL_INVOICE objInvoice = DBDataContext.Db.TBL_INVOICEs.FirstOrDefault(x => x.INVOICE_ID == Int32.Parse(dgvInvoiceList.SelectedRows[0].Cells[INVOICE_ID.Name].Value.ToString()));

            if (objInvoice.IS_SERVICE_BILL)
            {
                DialogCustomerCharge diag = new DialogCustomerCharge(_objOld, GeneralProcess.Update, objInvoice);
                if (diag.ShowDialog() == DialogResult.OK)
                {
                    loadInvoice();
                }
                return;
            }

            //allow user adjust usage only 3 times or below for each invoice
            if (DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_EDIT_INVOICE_ADJUSTMENT_OVER3]) != true)
            {
                var objADJ = DBDataContext.Db.TBL_INVOICE_ADJUSTMENTs.Where(x => x.INVOICE_ID == objInvoice.INVOICE_ID).ToList();
                if (objADJ.Count() >= 3)
                {
                    if (MsgBox.ShowQuestion(Resources.MS_INVOICE_ADJUSTMENT_OVER_3TIMES, Resources.WARNING) != DialogResult.Yes)
                    {
                        return;
                    }

                }
            }
            //check if adjust covid subsidy invoice
            TBL_INVOICE_ADJUSTMENT objAdjustment = DBDataContext.Db.TBL_INVOICE_ADJUSTMENTs.FirstOrDefault(x => x.INVOICE_ID == objInvoice.INVOICE_ID && (x.ADJUST_REASON == "ចុះថ្លៃ25%​ (COVID-19)" || x.ADJUST_REASON == "កែសម្រួល​ (COVID-19)" || x.ADJUST_REASON == "កែសម្រួល​ ថ្លៃអានុភាព (COVID-19) "));
            TMP_CUSTOMER_AVERAGE_USAGE customerAverage = DBDataContext.Db.TMP_CUSTOMER_AVERAGE_USAGEs.FirstOrDefault(x => x.CUSTOMER_ID == objInvoice.CUSTOMER_ID);
            DialogAdjustment dig = new DialogAdjustment(_objNew, objInvoice, _objOld.PRICE_ID);
            //if ((objAdjustment != null && customerAverage != null) || (objAdjustment == null && customerAverage != null))
            //{
            //    dig.cboCurrency.Enabled = false;
            //}
            if (dig.ShowDialog() == DialogResult.OK)
            {
                loadInvoice();
                if (DialogRemindSendData.IsRequiredReminder())
                {
                    new DialogRemindSendData().ShowDialog();
                }
            }
        }

        private void linkAdjustAmount_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (isNotValidToAdjust())
            {
                return;
            }
            //if user not yet open cash drawer
            //not allow to adjust invoice
            if (Login.CurrentCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_ADJUST_INVOICE, string.Empty) == DialogResult.Yes)
                    new DialogOpenCashDrawer().ShowDialog();
                if (Login.CurrentCashDrawer == null)
                    return;
            }
            TBL_INVOICE objInvoice = DBDataContext.Db.TBL_INVOICEs.FirstOrDefault(x => x.INVOICE_ID == Int32.Parse(dgvInvoiceList.SelectedRows[0].Cells[INVOICE_ID.Name].Value.ToString()));

            //allow user adjust usage only 3 times or below for each invoice
            if (DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_EDIT_INVOICE_ADJUSTMENT_OVER3]) != true)
            {
                var objADJ = DBDataContext.Db.TBL_INVOICE_ADJUSTMENTs.Where(x => x.INVOICE_ID == objInvoice.INVOICE_ID).ToList();
                if (objADJ.Count() >= 3)
                {
                    if (MsgBox.ShowQuestion(Resources.MS_INVOICE_ADJUSTMENT_OVER_3TIMES, Resources.WARNING) != DialogResult.Yes)
                    {
                        return;
                    }
                }
            }
            DialogAdjustmentAmount dig = new DialogAdjustmentAmount(objInvoice, _objOld.PRICE_ID);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                loadInvoice();
                if (DialogRemindSendData.IsRequiredReminder())
                {
                    new DialogRemindSendData().ShowDialog();
                }
            }
        }

        private void linkDeleteInvoice_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (dgvInvoiceList.SelectedRows.Count == 0)
            {
                return;
            }

            TBL_INVOICE objInvoice = DBDataContext.Db.TBL_INVOICEs.FirstOrDefault(x => x.INVOICE_ID == Int32.Parse(dgvInvoiceList.SelectedRows[0].Cells[INVOICE_ID.Name].Value.ToString()));
            if (!objInvoice.IS_SERVICE_BILL)
            {
                MsgBox.ShowInformation(Resources.MS_CANNOT_VOID_POWER_INVOICE);
                return;
            }

            if(objInvoice.INVOICE_STATUS == (int)InvoiceStatus.Cancel)
            {
                MsgBox.ShowInformation(Resources.MSG_INVOICE_VOIDED_CANNOT_VOID);
                return;
            }

            if (objInvoice.PAID_AMOUNT > 0)
            {
                MsgBox.ShowInformation(Resources.MS_CANNOT_VOID_PAID_INVOICE);
                return;
            }

            DialogCustomerCharge diag = new DialogCustomerCharge(_objOld, GeneralProcess.Delete, objInvoice);
            if (diag.ShowDialog() == DialogResult.OK)
            {
                loadInvoice();
            }
        }
        private void linkPrintInvoice_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (dgvInvoiceList.SelectedRows.Count > 0)
            {
                if ((int)dgvInvoiceList.SelectedRows[0].Cells[INVOICE_STATUS.Name].Value == (int)InvoiceStatus.Cancel)
                {
                    return;
                }

                SupportTaker objSt = new SupportTaker(SupportTaker.Process.ViewInvoice);
                if (objSt.Check())
                {
                    return;
                }

                //if not exists report invoice 
                string path = string.Format(Properties.Settings.Default.PATH_REPORT, Properties.Settings.Default.LANGUAGE == "en-US" ? "" : "" + Properties.Settings.Default.LANGUAGE);
                path = Path.Combine(Directory.GetCurrentDirectory(), path);
                string PathCustomized = path + "Customized\\" + Properties.Settings.Default.REPORT_INVOICE;
                if (!File.Exists(PathCustomized))
                {
                    if (!File.Exists(path + Properties.Settings.Default.REPORT_INVOICE))
                    {
                        MsgBox.ShowInformation(string.Format(Resources.NOT_FOUND_INVOICE, "", Properties.Settings.Default.REPORT_INVOICE), Resources.INFORMATION);
                        return;
                    }
                }

                if (printingInvoice)
                {
                    TBL_PRINT_INVOICE objPrintInvoice = new TBL_PRINT_INVOICE()
                    {
                        PRINT_DATE = DBDataContext.Db.GetSystemDate(),
                        LOGIN_ID = Login.CurrentLogin.LOGIN_ID
                    };
                    using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                    {

                        DBDataContext.Db.TBL_PRINT_INVOICEs.InsertOnSubmit(objPrintInvoice);
                        DBDataContext.Db.SubmitChanges();
                        int invoiceID = int.Parse(this.dgvInvoiceList.SelectedRows[0].Cells[INVOICE_ID.Name].Value.ToString());
                        DBDataContext.Db.TBL_PRINT_INVOICE_DETAILs.InsertOnSubmit(new TBL_PRINT_INVOICE_DETAIL()
                        {
                            PRINT_INVOICE_ID = objPrintInvoice.PRINT_INVOICE_ID,
                            INVOICE_ID = invoiceID,
                            PRINT_ORDER = 0
                        });
                        DBDataContext.Db.SubmitChanges();
                        tran.Complete();
                    }
                    new DialogCustomerPrintInvoice(true, objPrintInvoice.PRINT_INVOICE_ID).ShowDialog();
                }
                else
                {
                    using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                    {
                        long invoiceID = (long)this.dgvInvoiceList.SelectedRows[0].Cells[INVOICE_ID.Name].Value;
                        DBDataContext.Db.TBL_INVOICE_TO_PRINTs.DeleteAllOnSubmit(DBDataContext.Db.TBL_INVOICE_TO_PRINTs);
                        DBDataContext.Db.TBL_INVOICE_TO_PRINTs.InsertOnSubmit(new TBL_INVOICE_TO_PRINT() { INVOICE_ID = invoiceID });
                        DBDataContext.Db.SubmitChanges();
                        tran.Complete();
                    }
                    new DialogCustomerPrintInvoice(false, 0).ShowDialog();
                }

            }
        }

        #endregion Adjust Invoice Event

        private void btnPay_Click(object sender, EventArgs e)
        {
            if (Login.CurrentCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_RECEIVE_PAYMENT, string.Empty) == DialogResult.Yes)
                    new DialogOpenCashDrawer().ShowDialog();
                if (Login.CurrentCashDrawer == null)
                    return;
            }
            int invoiceOpen = DBDataContext.Db.TBL_INVOICEs.Where(x => x.CUSTOMER_ID == _objNew.CUSTOMER_ID && x.INVOICE_STATUS == (int)InvoiceStatus.Open).Count();
            if (invoiceOpen == 0)
            {
                loadInvoice();
                return;
            }

            DialogReceivePayment dig = new DialogReceivePayment(_objNew, true);
            if (!dig.IsValidShow())
            {
                loadInvoice();
                return;
            }
            if (dig.ShowDialog() == DialogResult.OK)
            {
                bindPrepayment();
            }
            bindPrepayment();
            loadInvoice();
        }

        private void btnCharge_Click(object sender, EventArgs e)
        {
            if (Login.CurrentCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER, string.Empty) == DialogResult.Yes)
                    new DialogOpenCashDrawer().ShowDialog();
                if (Login.CurrentCashDrawer == null)
                    return;
            }
            if (_objNew.STATUS_ID == (int)CustomerStatus.Closed)
            {
                MsgBox.ShowInformation(Resources.MS_CANNOT_ADD_CHARGE_TO_CLOSE_CUSTOMER);
                return;
            }
            if (_objNew.STATUS_ID == (int)CustomerStatus.Cancelled)
            {
                MsgBox.ShowInformation(Resources.MS_CANNOT_ADD_CHARGE_TO_CANCEL_CUSTOMER);
                return;
            }
            DialogCustomerCharge dig = new DialogCustomerCharge(_objNew, GeneralProcess.Insert);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                loadInvoice();
                //loadTotalDueAmount();
            }

        }

        private void tab_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.tab.SelectedTab == this.tabUSAGE && this._chartWasShown == false)
            {

                // calculate usage by months!
                var lst = (from u in DBDataContext.Db.TBL_USAGEs
                           where u.CUSTOMER_ID == this._objNew.CUSTOMER_ID
                           group u by new DateTime(u.USAGE_MONTH.Year, u.USAGE_MONTH.Month, 1) into tu
                           orderby tu.Key ascending
                           select new
                           {
                               MONTH = tu.Key,
                               USAGE = tu.Sum(row => row.MULTIPLIER * (row.IS_METER_RENEW_CYCLE
                                                             ? (Convert.ToInt32(new string('9', ((int)row.START_USAGE).ToString().Length)) - row.START_USAGE + 1 + row.END_USAGE)
                                                             : (row.END_USAGE - row.START_USAGE)))
                           });

                DataTable dt = new DataTable();
                dt.Columns.Add("Month");
                dt.Columns.Add("Usage", typeof(decimal));
                foreach (var obj in lst)
                {
                    DataRow row = dt.NewRow();
                    row[0] = obj.MONTH.ToString("MMM yy");
                    row[1] = obj.USAGE;
                    dt.Rows.Add(row);
                }
                // fill up chart for 2 year (24 months)
                // to make sure chart size remain the same
                while (dt.Rows.Count < 24)
                {
                    DataRow row = dt.NewRow();
                    row[0] = "";
                    row[1] = 0;
                    dt.Rows.Add(row);
                }
                //styling chart
                chart1.TempDirectory = "temp";
                chart1.Type = ChartType.Combo;
                chart1.Palette = new Color[] { Color.FromArgb(49, 255, 49), Color.FromArgb(0, 156, 255), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) };
                chart1.ChartArea.Background.Color = Color.FromArgb(255, 250, 250, 250);
                chart1.YAxis.AlternateGridBackground.Color = Color.FromArgb(100, 235, 235, 235);
                chart1.ShadingEffectMode = ShadingEffectMode.Two;
                //chart1.DefaultElement.ShowValue = true;  
                //set global properties
                chart1.Title = "Customer Usage";
                // Set the x axis label
                chart1.ChartArea.XAxis.Label.Text = "Month";
                // Set the y axis label
                chart1.ChartArea.YAxis.Label.Text = "Usage(kWh)";

                //Adding series programatically
                chart1.Series.Name = "Usage(kWh)";
                chart1.LegendBox.Visible = false;
                chart1.Series.Data = dt;
                chart1.SeriesCollection.Add();

                // mark that chart was shown
                this._chartWasShown = true;
            }
            else if (this.tab.SelectedTab == tabPREPAYMENT)
            {
                bindPrepayment();
            }
            else if (this.tab.SelectedTab == tabInvoice)
            {
                loadInvoice();
            }

        }

        private void btnPrintUsageReport_Click(object sender, EventArgs e)
        {
            try
            {
                CrystalReportHelper ch = new CrystalReportHelper("ReportCustomerUsage.rpt");
                ch.SetParameter("@CUSTOMER_ID", this._objNew.CUSTOMER_ID);
                ch.SetParameter("@CUSTOMER_CODE", this._objNew.CUSTOMER_CODE);
                ch.SetParameter("@CUSTOMER_NAME", this._objNew.LAST_NAME_KH + " " + this._objNew.FIRST_NAME_KH);
                ch.ViewReport("");
            }
            catch(Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_VIEW_REPORT, Base.Properties.Resources.WARNING);
            }

        }

        private void lblAddDeposit_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //GET DEPOSIT THAT NOT YET PAID
            TBL_CUS_DEPOSIT objDeposit = DBDataContext.Db.TBL_CUS_DEPOSITs.FirstOrDefault(row => !row.IS_PAID && row.CUSTOMER_ID == _objNew.CUSTOMER_ID);
            DialogDepositAdd dialog = null;
            if (objDeposit != null)
            {
                dialog = new DialogDepositAdd(_objNew, DepositAction.AddDeposit, GeneralProcess.Update, objDeposit);
            }
            else
            {
                dialog = new DialogDepositAdd(_objNew, DepositAction.AddDeposit, GeneralProcess.Insert, objDeposit);
            }

            dialog.ShowDialog();
            if (dialog.DialogResult == DialogResult.OK)
            {
                bindDeposit();
            }
        }

        private void linkAdjustDeposit_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (Login.CurrentCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_RECEIVE_PAYMENT, string.Empty) == DialogResult.Yes)
                    new DialogOpenCashDrawer().ShowDialog();
                if (Login.CurrentCashDrawer == null)
                    return;
            }
            TBL_CUS_DEPOSIT objDeposit = DBDataContext.Db.TBL_CUS_DEPOSITs.FirstOrDefault(row => !row.IS_PAID && row.CUSTOMER_ID == _objNew.CUSTOMER_ID);
            if (objDeposit != null)
            {
                MsgBox.ShowInformation(Resources.MS_CUSTOMER_NO_DEPOSIT);
                return;
            }
            DialogDepositAdd dialog = new DialogDepositAdd(_objNew, DepositAction.AdjustDeposit, GeneralProcess.Insert, null);
            dialog.ShowDialog();
            if (dialog.DialogResult == DialogResult.OK)
            {
                bindDeposit();
            }
        }

        private void linkApplyPayment_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (Login.CurrentCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_RECEIVE_PAYMENT, string.Empty) == DialogResult.Yes)
                    new DialogOpenCashDrawer().ShowDialog();
                if (Login.CurrentCashDrawer == null)
                    return;
            }
            TBL_CUS_DEPOSIT objDeposit = DBDataContext.Db.TBL_CUS_DEPOSITs.FirstOrDefault(row => !row.IS_PAID && row.CUSTOMER_ID == _objNew.CUSTOMER_ID);
            if (objDeposit != null)
            {
                MsgBox.ShowInformation(Resources.MS_CUSTOMER_NO_DEPOSIT);
                return;
            }
            DialogDepositApplyPayment dialog = new DialogDepositApplyPayment(_objNew);
            dialog.ShowDialog();
            if (dialog.DialogResult == DialogResult.OK)
            {
                bindDeposit();
            }
        }

        private void linkRefundDeposit_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (Login.CurrentCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_RECEIVE_PAYMENT, string.Empty) == DialogResult.Yes)
                    new DialogOpenCashDrawer().ShowDialog();
                if (Login.CurrentCashDrawer == null)
                    return;
            }
            TBL_CUS_DEPOSIT objDeposit = DBDataContext.Db.TBL_CUS_DEPOSITs.FirstOrDefault(row => !row.IS_PAID && row.CUSTOMER_ID == _objNew.CUSTOMER_ID);
            if (objDeposit != null)
            {
                MsgBox.ShowInformation(Resources.MS_CUSTOMER_NO_DEPOSIT);
                return;
            }
            DialogDepositAdd dialog = new DialogDepositAdd(_objNew, DepositAction.RefundDeposit, GeneralProcess.Insert, null);
            dialog.ShowDialog();
            if (dialog.DialogResult == DialogResult.OK)
            {
                bindDeposit();
            }
        }

        private void btnPrintReceipt_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (this.dgvDeposit.SelectedRows.Count > 0)
            {
                try
                {
                    int intCusDeposit = (int)this.dgvDeposit.SelectedRows[0].Cells[this.CUS_DEPOSIT_ID.Name].Value;
                    TBL_CUS_DEPOSIT objDeposit = DBDataContext.Db.TBL_CUS_DEPOSITs.FirstOrDefault(row => row.CUS_DEPOSIT_ID == intCusDeposit);
                    CrystalReportHelper ch;
                    if (objDeposit.IS_PAID)
                    {
                        ch = new CrystalReportHelper("ReportDepositReceipt.rpt");
                    }
                    else
                    {
                        ch = new CrystalReportHelper("ReportDepositInvoice.rpt");
                    }
                    ch.SetParameter("@CUS_DEPOSIT_ID", intCusDeposit);
                    ch.ViewReport("");
                }
                catch(Exception ex)
                {
                    MsgBox.LogError(ex);
                    MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_VIEW_REPORT, Base.Properties.Resources.WARNING);
                }
                
            }
        }

        private void cboVillage_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_loading && this._objNew.ADDRESS != "") return;
            this.txtAddress.Text = String.Format(Resources.CUSTOMER_ADDRESS, this.cboVillage.Text, this.cboCommune.Text);
        }


        private void btnAddService_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var diag = new DialogCustomerService(GeneralProcess.Insert, new TBL_CUSTOMER_SERVICE() { CUSTOMER_ID = this._objNew.CUSTOMER_ID });
            if (diag.ShowDialog() == DialogResult.OK)
            {
                this.bindService();
            }
        }

        private void btnEditService_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (this.dgvService.SelectedRows.Count == 0) return;
            var obj = DBDataContext.Db.TBL_CUSTOMER_SERVICEs.FirstOrDefault(row => row.CUSTOMER_SERVICE_ID == (int)this.dgvService.SelectedRows[0].Cells[this.CUSTOMER_SERVICE_ID.Name].Value);
            if (obj == null) return;

            var diag = new DialogCustomerService(GeneralProcess.Update, obj);
            if (diag.ShowDialog() == DialogResult.OK)
            {
                this.bindService();
            }
        }

        private void btnRemoveService_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (this.dgvService.SelectedRows.Count == 0) return;
            var obj = DBDataContext.Db.TBL_CUSTOMER_SERVICEs.FirstOrDefault(row => row.CUSTOMER_SERVICE_ID == (int)this.dgvService.SelectedRows[0].Cells[this.CUSTOMER_SERVICE_ID.Name].Value);
            if (obj == null) return;

            var diag = new DialogCustomerService(GeneralProcess.Delete, obj);
            if (diag.ShowDialog() == DialogResult.OK)
            {
                this.bindService();
            }
        }

        private void btnConnection_Click(object sender, EventArgs e)
        {
            CrystalReportHelper ch = new CrystalReportHelper("ReportCustomerConnection.rpt");
            ch.SetParameter("@CUSTOMER_ID", this._objNew.CUSTOMER_ID);
            ch.ViewReport("");
        }

        private void btnAgreement_Click(object sender, EventArgs e)
        {
            CrystalReportHelper ch = new CrystalReportHelper("ReportCustomerAgreement.rpt");
            ch.SetParameter("@CUSTOMER_ID", this._objNew.CUSTOMER_ID);
            ch.ViewReport("");
        }

        private void NumberOnly(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputNumberOnly(sender, e);
        }

        private void btnDepositConnectionFeePayslip_Click(object sender, EventArgs e)
        {
            CrystalReportHelper ch = new CrystalReportHelper("ReportDepositConnectionFeePayslip.rpt");
            ch.SetParameter("@CUSTOMER_ID", _objNew.CUSTOMER_ID);
            ch.ViewReport("");
            ch.Dispose();
        }

        private void btnReactivate_Click(object sender, EventArgs e)
        {
            if (_objNew.INVOICE_CUSTOMER_ID != 0 && _objNew.INVOICE_CUSTOMER_ID != _objNew.CUSTOMER_ID)
            {
                if (DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == _objNew.INVOICE_CUSTOMER_ID).STATUS_ID != (int)CustomerStatus.Active)
                {
                    string cust = DBDataContext.Db.TBL_CUSTOMERs.Where(x => x.CUSTOMER_ID == _objNew.INVOICE_CUSTOMER_ID).Select(c => new { s = c.CUSTOMER_CODE + " " + c.LAST_NAME_KH + " " + c.FIRST_NAME_KH }).FirstOrDefault().s;
                    MsgBox.ShowInformation(string.Format(Resources.PLEASE_ACTIVE_MAIN_CUSTOMER_FIRST, cust), Resources.WARNING);
                    return;
                }
            }
            var diag = new DialogCustomerActivate(this._objNew);
            if (diag.ShowDialog() == DialogResult.OK)
            {
                this.DialogResult = DialogResult.OK;
            }
        }

        private void btnCustomerAR_Click(object sender, EventArgs e)
        {
            CrystalReportHelper ch = new CrystalReportHelper("ReportCustomerAR.rpt");
            ch.SetParameter("@CUSTOMER_ID", _objNew.CUSTOMER_ID);
            ch.SetParameter("@INVOICE_STATUS", (int)cboInvoiceStatus.SelectedValue);
            ch.SetParameter("@INVOICE_STATUS_TEXT", cboInvoiceStatus.Text);
            ch.ViewReport(Resources.VIEW_INVOICE);
            ch.Dispose();
        }

        private void dgvInvoiceList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != -1 && e.RowIndex != -1)
            {
                long invoiceId = (long)dgvInvoiceList.SelectedRows[0].Cells[INVOICE_ID.Name].Value;
                if (this.dgvInvoiceList.Columns[e.ColumnIndex].Name == this.PAID_AMOUNT.Name)
                {
                    DialogPaymentHistory diag = new DialogPaymentHistory(invoiceId);
                    diag.ShowDialog();
                    loadInvoice();
                    bindPrepayment();
                    dgvBalancePrepayment.Refresh();
                }
                else if (this.dgvInvoiceList.Columns[e.ColumnIndex].Name == this.SETTLE_AMOUNT.Name)
                {
                    new Interface.DialogAdjustmentHistory(DBDataContext.Db.TBL_INVOICEs.FirstOrDefault(x => x.INVOICE_ID == invoiceId)).ShowDialog();
                }
            }

        }
        #region PrePayment

        private void bindPrepayment()
        {
            int CurrencyId = 0;
            if (cboCurrency.SelectedIndex != -1)
            {
                CurrencyId = (int)cboCurrency.SelectedValue;
            }

            var lstPrepayment = from de in DBDataContext.Db.TBL_CUS_PREPAYMENTs
                                join c in DBDataContext.Db.TLKP_CURRENCies on de.CURRENCY_ID equals c.CURRENCY_ID
                                where (de.CUSTOMER_ID == _objNew.CUSTOMER_ID)
                                && (c.CURRENCY_ID == CurrencyId || CurrencyId == 0)
                                //&& (!dtpPpMonth.Checked || (de.CREATE_ON <= dtpPpMonth.Value.Date.AddMonths(1).AddMilliseconds(-1)))
                                orderby de.CURRENCY_ID, de.CUS_PREPAYMENT_ID
                                select new
                                {
                                    de.CUS_PREPAYMENT_ID,
                                    de.PREPAYMENT_NO,
                                    de.CREATE_ON,
                                    de.CREATE_BY,
                                    AMOUNT = de.AMOUNT,
                                    BALANCE = de.BALANCE,
                                    c.CURRENCY_SING,
                                    c.CURRENCY_ID,
                                    c.FORMAT,
                                    de.NOTE,
                                    de.PAYMENT_ID,
                                    ACTION = (int)de.ACTION_ID
                                };

            dgvPrepayment.DataSource = lstPrepayment.OrderBy(x => x.CURRENCY_ID).ThenByDescending(x => x.CREATE_ON).ThenByDescending(x => x.PREPAYMENT_NO);


            var allIds = lstPrepayment.Select(x => x.CUS_PREPAYMENT_ID).ToList();
            allIds = allIds ?? new List<int>();
            _prepayment_histories = DBDataContext.Db.TBL_APPLY_PAYMENT_HISTORies.Where(x => x.IS_ACTIVE && allIds.Contains(x.PREPAYMENT_ID)).ToList();

            var dBalance = from de in DBDataContext.Db.TBL_CUS_PREPAYMENTs
                           join c in DBDataContext.Db.TLKP_CURRENCies on de.CURRENCY_ID equals c.CURRENCY_ID
                           where de.CUSTOMER_ID == _objNew.CUSTOMER_ID
                           && (c.CURRENCY_ID == CurrencyId || CurrencyId == 0)
                           group de by new { c.CURRENCY_ID, c.CURRENCY_SING, c.FORMAT } into tmpBalance
                           select new
                           {
                               tmpBalance.Key.CURRENCY_ID,
                               tmpBalance.Key.CURRENCY_SING,
                               tmpBalance.Key.FORMAT,
                               tmpBalance.OrderByDescending(x => x.CUS_PREPAYMENT_ID).FirstOrDefault().BALANCE
                           };
            dgvBalancePrepayment.DataSource = dBalance.OrderBy(x => x.CURRENCY_ID).ToList();

            if (dBalance.Count() > 0)
            {
                btnSETTLE_INVOICE.Enabled = true;
                btnRETURN_PREPAYMENT.Enabled = true;
                //linkAdjustDeposit.Enabled = true;
            }
            else
            {
                btnSETTLE_INVOICE.Enabled = false;
                btnRETURN_PREPAYMENT.Enabled = false;
                //linkAdjustDeposit.Enabled = false;
            }
        }

        private void btnADD_PREPAYMENT_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var isValid = true;
            if (Login.CurrentCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_RECEIVE_PAYMENT, string.Empty) == DialogResult.Yes)
                {
                    new DialogOpenCashDrawer().ShowDialog();
                }
                else
                {
                    return;
                }
                isValid = (Login.CurrentCashDrawer != null);
            }
            if (isValid)
            {
                int currency = 0;
                if (dgvPrepayment.CurrentRow != null)
                {
                    currency = (int)dgvPrepayment.SelectedRows[0].Cells[CURRENCY_ID_P.Name].Value;
                }
                DialogReceivePayment dialog = new DialogReceivePayment(_objNew, true);
                if (!dialog.IsValidShow())
                {
                    bindPrepayment();
                    return;
                }
                dialog.ShowDialog();
                if (dialog.DialogResult == DialogResult.OK)
                {
                    loadInvoice();
                    dgvBalancePrepayment.Refresh();
                }
                bindPrepayment();
            }
        }

        private void btnRETURN_PREPAYMENT_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (dgvPrepayment.Rows.Count < 1)
            {
                MsgBox.ShowInformation(Resources.CUSTOMER_NO_PREPAYMENT);
                return;
            }

            if (Login.CurrentCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Properties.Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_RECEIVE_PAYMENT, string.Empty) == DialogResult.Yes)
                {
                    new DialogOpenCashDrawer().ShowDialog();
                }
                return;
            }

            var currency = (int)dgvPrepayment.SelectedRows[0].Cells[CURRENCY_ID_P.Name].Value;
            TBL_CUS_PREPAYMENT objPrepay = DBDataContext.Db.TBL_CUS_PREPAYMENTs.OrderByDescending(x => x.CREATE_ON).FirstOrDefault(row => row.CURRENCY_ID == currency && row.CUSTOMER_ID == _objNew.CUSTOMER_ID);
            if (objPrepay == null || objPrepay.BALANCE == 0)
            {
                MsgBox.ShowInformation(Resources.CUSTOMER_NO_PREPAYMENT);
                return;
            }
            DialogPrepaymentAdd dialog = new DialogPrepaymentAdd(_objNew, PrepaymentAction.RefundPrepayment, GeneralProcess.Insert, currency);
            dialog.ShowDialog();
            if (dialog.DialogResult == DialogResult.OK)
            {
                bindPrepayment();
            }
        }
        private void btnSETTLE_INVOICE_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            TBL_CUS_PREPAYMENT objPrepayment = DBDataContext.Db.TBL_CUS_PREPAYMENTs.FirstOrDefault(row => row.CUSTOMER_ID == _objNew.CUSTOMER_ID);
            if (objPrepayment == null)
            {
                MsgBox.ShowInformation(Resources.CUSTOMER_NO_PREPAYMENT);
                return;
            }

            //if (Logic.Login.CurrentCashDrawer == null)
            //{
            //    if (MsgBox.ShowQuestion(Properties.Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_RECEIVE_PAYMENT, string.Empty) == DialogResult.Yes)
            //    {
            //        new DialogOpenCashDrawer().ShowDialog();
            //    }
            //    return;
            //}

            //int currency = (int)dgvPrepayment.SelectedRows[0].Cells[CURRENCY_ID_P.Name].Value;
            //DialogPrepaymentApplyPayment dialog = new DialogPrepaymentApplyPayment(_objNew, currency);
            //dialog.ShowDialog();
            //if (dialog.DialogResult == DialogResult.OK)
            //{
            //    loadInvoice();
            //    bindPrepayment();
            //    dgvBalancePrepayment.Refresh();
            //}
        }
        #endregion

        private void lblCHANGE_AMPARE_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DialogCustomerChangeAmpare diag = new DialogCustomerChangeAmpare(_objNew);
            if (diag.ShowDialog() == DialogResult.OK)
            {
                cboAMP.SelectedValue = diag._objCustomer.AMP_ID;
            }
        }

        private void cboCustomerGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboCustomerGroup.SelectedIndex != -1 && !_loading)
            {
                int groupId = (int)cboCustomerGroup.SelectedValue;
                var objs = lstConnectionType.OrderBy(x => x.DESCRIPTION).Where(x => x.NONLICENSE_CUSTOMER_GROUP_ID == groupId);
                UIHelper.SetDataSourceToComboBox(cboConnectionType, objs);

                var str = objs.OrderByDescending(x => x.CUSTOMER_CONNECTION_TYPE_NAME.Length).FirstOrDefault().CUSTOMER_CONNECTION_TYPE_NAME;
                var w = TextRenderer.MeasureText(str.ToString(), cboConnectionType.Font).Width;
                this.cboConnectionType.DropDownWidth = w;
            }
        }
        private void ChangeConnectionType_Click(object sender, EventArgs e)
        {
            try
            {
                long invId = (long)dgvInvoiceList.CurrentRow.Cells[INVOICE_ID.Name].Value;
                new DialogChangeConnectionType(invId).ShowDialog();
            }
            catch (Exception)
            {

                throw;
            }
        }
        private void ChangeBasePrice_Click(object sender, EventArgs e)
        {
            if (isNotValidToAdjustBasePrice())
            {
                return;
            }
            TBL_INVOICE objInvoice = DBDataContext.Db.TBL_INVOICEs.FirstOrDefault(x => x.INVOICE_ID == Int32.Parse(dgvInvoiceList.SelectedRows[0].Cells["INVOICE_ID"].Value.ToString()));

            //if invoice is service bill 
            //not allow to adust usage
            if (objInvoice.IS_SERVICE_BILL == true)
            {
                MsgBox.ShowInformation(Resources.MS_CANNOT_ADJUST_SERVICE_INVOICE);
                return;
            }
            DialogAdjustment dig = new DialogAdjustment(_objNew, objInvoice, _objOld.PRICE_ID);
            if (objInvoice.PAID_AMOUNT > 0)
            {
                dig.cboCurrency.Enabled = false;
            }
            dig.cboMeterCode.Enabled = dig.txtStartUsage.Enabled = dig.txtEndUsage.Enabled = dig.txtMultiplier.Enabled = dig.chkIS_NEW_CYCLE.Enabled = dig.txtPrice.Enabled = dig.txtTotalAmountNew.Enabled = false;
            dig.lblSUBSIDY_TARIFF.Visible = dig.label2.Visible = dig.lblStar_.Visible = dig.lblCURRENCY.Visible = dig.txtBASED_PRICE.Visible = dig.cboCurrency.Visible = true;
            dig.txtBASED_PRICE.Focus();
            if (dig.ShowDialog() == DialogResult.OK)
            {
                loadInvoice();
                if (DialogRemindSendData.IsRequiredReminder())
                {
                    new DialogRemindSendData().ShowDialog();
                }
            }
        }

        private void cmsStrip_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }

        private void btnRE_ACTIVE_Click(object sender, EventArgs e)
        {
            if (MsgBox.ShowQuestion(Resources.MSG_REQUEST_TO_REACTIVE_CANCELLED_CUSTOMER, Resources.NOTE_REASON) == DialogResult.Yes)
            {
                this._objNew.STATUS_ID = (int)CustomerStatus.Pending;
                DBDataContext.Db.SubmitChanges();
                this.btnRE_ACTIVE.Visible = false;
            }
        }

        private void cboConnectionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (cboConnectionType.SelectedIndex == -1)
            //{
            //    return;
            //}
            if (cboConnectionType.SelectedIndex != -1 && !_loading)
            {
                var connType = lstConnectionType.FirstOrDefault(x => x.CUSTOMER_CONNECTION_TYPE_ID == (int)this.cboConnectionType.SelectedValue);
                cboPrice.SelectedValue = connType.PRICE_ID;
            }
        }

        private void cboTitles_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            UIHelper.SetDataSourceToComboBox(cboIdCardType, DBDataContext.Db.TLKP_IDENTIFICATION_TYPEs.ToList());
            UIHelper.SetDataSourceToComboBox(cboCustomerType, DBDataContext.Db.TLKP_TAX_CUSTOMER_TYPEs.ToList());
            if (cboTitles.SelectedIndex == 5)
            {
                this.cboIdCardType.SelectedIndex = 2;
                this.cboCustomerType.SelectedIndex = 0;
                txtFirstNameKH.Enabled = false;
                lblLAST_NAME_KH.Text = Resources.NAME_COMPANY_KHMER;
                txtFirstName.Enabled = false;
                lblLAST_NAME.Text = Resources.NAME_COMPANY_ENGLISH;
                this.cboIdCardType.Enabled = false;
            }

            if (cboTitles.SelectedIndex != 5)
            {
                this.cboIdCardType.SelectedIndex = 0;
                this.cboCustomerType.SelectedIndex = 1;
                txtFirstNameKH.Enabled = true;
                lblLAST_NAME_KH.Text = "គោត្តនាម";
                txtFirstName.Enabled = true;
                lblLAST_NAME.Text = "គោត្តនាមឡាតាំង";
                this.cboIdCardType.Enabled = true;
            }
        }

        private void txtPhone1_KeyPress(object sender, KeyPressEventArgs e)
        {
            var arrayString = new char[] { (char)Keys.Space, (char)Keys.Insert, (char)Keys.Help, (char)Keys.Execute };
            if (e.KeyChar == (char)Keys.Back)
            {
                e.Handled = false;
            }
            else if (arrayString.Contains(e.KeyChar) || char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void dgvInvoiceList_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            var selectedRow = dgvInvoiceList.Rows[e.RowIndex];
            if (selectedRow.Index == -1)
            {
                return;
            }
            var invoicId = int.Parse(selectedRow.Cells["INVOICE_ID"].Value?.ToString() ?? "0");
            var objInvoice = DBDataContext.Db.TBL_INVOICEs.FirstOrDefault(x => x.INVOICE_ID == invoicId);
            if (objInvoice == null)
            {
                return;
            }
            // TBL_INVOICE objInvoice = DBDataContext.Db.TBL_INVOICEs.FirstOrDefault(x => x.INVOICE_ID == Int32.Parse(dgvInvoiceList.SelectedRows[0].Cells["INVOICE_ID"].Value.ToString()));
            if (objInvoice.IS_SERVICE_BILL)
            {
                return;
            }
            if (objInvoice.INVOICE_STATUS == 3)
            {
                return;
            }
            if (e.Button == MouseButtons.Right)
            {
                dgvInvoiceList.CurrentCell = dgvInvoiceList.Rows[e.RowIndex].Cells[1];
                cmsStrip.Show();
                cmsStrip.Left = Cursor.Position.X;
                cmsStrip.Top = Cursor.Position.Y;
            }
        }

        private void dgvPrepayment_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            var formatAmount = dgvPrepayment.Rows[e.RowIndex].Cells[FORMAT.Name].Value?.ToString() ?? "";
            if (e.ColumnIndex != dgvPrepayment.Columns[AMOUNT_PREPAYMENT.Name].Index && e.ColumnIndex != dgvPrepayment.Columns[BALANCE_PREPAYMENT.Name].Index)
            {
                return;
            }
            e.CellStyle.Format = formatAmount;

            int cus_prepaymentId = (int)dgvPrepayment.Rows[e.RowIndex].Cells[CUS_PREPAYMENT_ID.Name].Value;
            int action = (int)dgvPrepayment.Rows[e.RowIndex].Cells[colACTION.Name].Value;
            if (action == (int)PrepaymentAction.RefundPrepayment)
            {
                return;
            }
            //change it to load when bind prepayment => make performance better.  

            //var objPrepayment =    DBDataContext.Db.TBL_CUS_PREPAYMENTs.FirstOrDefault(x => x.PAYMENT_ID == intPrepaymentID);
            var objPreHistory = _prepayment_histories.FirstOrDefault(x => x.PREPAYMENT_ID == cus_prepaymentId);  //DBDataContext.Db.TBL_APPLY_PAYMENT_HISTORies.FirstOrDefault(x => x.PREPAYMENT_ID == objPrepayment.CUS_PREPAYMENT_ID && x.IS_ACTIVE == true);
            if (objPreHistory == null)
            {

                dgvPrepayment.Rows[e.RowIndex].Cells[PREPAYMENT_NO.Name].Style.ForeColor = Color.Black;
            }
            else
            {
                dgvPrepayment.Rows[e.RowIndex].Cells[PREPAYMENT_NO.Name].Style.ForeColor = Color.Blue;
                dgvPrepayment.Rows[e.RowIndex].Cells[PREPAYMENT_NO.Name].Style.SelectionForeColor = Color.Blue;
                dgvPrepayment.Rows[e.RowIndex].Cells[PREPAYMENT_NO.Name].Style.Font = new Font("Khmer OS System", 8, FontStyle.Regular | FontStyle.Underline);
            }
        }


        private void dgvInvoiceList_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            var formatAmount = dgvInvoiceList.Rows[e.RowIndex].Cells[FORMAT_INVOICE.Name].Value?.ToString() ?? "";
            if (e.ColumnIndex != dgvInvoiceList.Columns[SETTLE_AMOUNT.Name].Index && e.ColumnIndex != dgvInvoiceList.Columns[PAID_AMOUNT.Name].Index && e.ColumnIndex != dgvInvoiceList.Columns[DUE_AMOUNT.Name].Index)
            {
                return;
            }
            e.CellStyle.Format = formatAmount;
        }

        private void dgvTotalBalance_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            var formatAmount = dgvTotalBalance.Rows[e.RowIndex].Cells[FORMAT_BALNCE_INVOICE.Name].Value?.ToString() ?? "";
            if (e.ColumnIndex != dgvTotalBalance.Columns[INVOICE_BALANCE.Name].Index)
            {
                return;
            }
            e.CellStyle.Format = formatAmount;
        }

        private void dgvBalancePrepayment_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            var formatAmount = dgvBalancePrepayment.Rows[e.RowIndex].Cells[FORMAT_BALANCE_PREPAYMENT.Name].Value?.ToString() ?? "";
            if (e.ColumnIndex != dgvBalancePrepayment.Columns[PREPAYMENT_BALANCE.Name].Index)
            {
                return;
            }
            e.CellStyle.Format = formatAmount;
        }

        private void cboCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindPrepayment();
        }

        private void btnPRINT_2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //if (this.dgvPrepayment.SelectedRows.Count > 0)
            //{
            //    int intCusPrepayment = (int)this.dgvPrepayment.SelectedRows[0].Cells[this.CUS_PREPAYMENT_ID.Name].Value;
            //    TBL_CUS_PREPAYMENT objPrepayment = DBDataContext.Db.TBL_CUS_PREPAYMENTs.FirstOrDefault(row => row.CUS_PREPAYMENT_ID == intCusPrepayment);
            //    CrystalReportHelper ch;
            //    ch = new CrystalReportHelper("ReportReceiptPrepayment.rpt");
            //    ch.SetParameter("@CUS_PREPAYMENT_ID", intCusPrepayment);
            //    ch.ViewReport("");
            //}
        }

        private void dgvPrepayment_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != -1 && e.RowIndex != -1)
            {
                if (this.dgvPrepayment.Columns[e.ColumnIndex].Name == this.PREPAYMENT_NO.Name)
                {
                    int intPrepaymentID = (int)dgvPrepayment.SelectedRows[0].Cells[PAYMENT_ID.Name].Value;
                    var objPrepayment = DBDataContext.Db.TBL_CUS_PREPAYMENTs.FirstOrDefault(x => x.PAYMENT_ID == intPrepaymentID);
                    var objPreHistory = DBDataContext.Db.TBL_APPLY_PAYMENT_HISTORies.FirstOrDefault(x => x.PREPAYMENT_ID == objPrepayment.CUS_PREPAYMENT_ID && x.IS_ACTIVE == true);

                    if (objPreHistory == null)
                    {
                        return;
                    }
                    else
                    {
                        new DialogPaymentDetailCancel(intPrepaymentID).ShowDialog();
                    }
                }
            }
        }

        private void linkAddNewAttach_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DialogCustomerAttachment dig = new DialogCustomerAttachment(GeneralProcess.Insert, new TBL_CUSTOMER_ATTACHMENT(), this._objNew);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                bindAttachment();
            }
        }
        private void bindAttachment()
        {
            this.dgvAttachment.DataSource = (from att in DBDataContext.Db.TBL_CUSTOMER_ATTACHMENTs
                                             join fy in DBDataContext.Db.TLKP_FILE_TYPEs on att.FILE_TYPE_ID equals fy.FILE_TYPE_ID
                                             where att.CUSTOMER_ID == _objNew.CUSTOMER_ID
                                             && att.IS_ACTIVE
                                             && att.ATTACHMENT_NAME.ToLower().Contains(txtSearchAttachment.Text.ToLower().Trim())
                                             select new
                                             {
                                                 att.CUSTOMER_ATTACHMENT_ID,
                                                 att.ATTACHMENT_NAME,
                                                 att.NOTE,
                                                 DOWNLOAD = "Download",
                                                 FILE_NAME_TYPE = fy.FILE_TYPE_NAME,
                                                 SIZE = Method.SizeSuffix(att.FILE.Length),
                                                 att.CREATED_ON,
                                                 att.CREATED_BY,
                                                 att.IS_ACTIVE,
                                             }).Take(20);
        }

        private void linkPrintAttachment_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (_objCusAttach != null)
            {
                Runner.RunNewThread(openAttachment, Properties.Resources.DISPLAYINPROGRESS);
            }
        }
        private void openAttachment()
        {
            try
            {
                string fileName = System.IO.Path.Combine(Application.StartupPath + "\\TEMP", _objCusAttach.FILE_NAME);
                using (FileStream stream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.Read))
                {
                    stream.Write(_objCusAttach.FILE.ToArray(), 0, _objCusAttach.FILE.Length);
                }
                // try opening newfile after complete  
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo(fileName);
                System.Diagnostics.Process.Start(startInfo);
            }
            catch { }
        }

        private void linkRemoveAttach_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (_objCusAttach != null)
            {
                DialogCustomerAttachment dig = new DialogCustomerAttachment(GeneralProcess.Delete, _objCusAttach, this._objNew);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    bindAttachment();
                }
            }
        }

        private void linkAdjustAttachment_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (_objCusAttach == null)
            {
                return;
            }
            int id = _objCusAttach.CUSTOMER_ATTACHMENT_ID;
            DialogCustomerAttachment dig = new DialogCustomerAttachment(GeneralProcess.Update, _objCusAttach, this._objNew);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                bindAttachment();
                UIHelper.SelectRow(dgvAttachment, id);
            }
        }

        private void downloadAttachment()
        {
            string fileName = System.IO.Path.Combine(Application.StartupPath + "\\TEMP", _objCusAttach.FILE_NAME);
            using (FileStream stream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.Read))
            {
                stream.Write(_objCusAttach.FILE.ToArray(), 0, _objCusAttach.FILE.Length);
            }
        }

        private void dgvAttachment_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != -1 && e.RowIndex != -1)
            {
                if (this.dgvAttachment.Columns[e.ColumnIndex].Name == this.DOWNLOAD_ATTACHMENT.Name)
                {
                    if (_objCusAttach != null)
                    {
                        Runner.RunNewThread(downloadAttachment, Properties.Resources.DISPLAYINPROGRESS);
                        System.Diagnostics.Process.Start("explorer.exe", string.Format("/select,\"{0}\"", System.IO.Path.Combine(Application.StartupPath + "\\TEMP", _objCusAttach.FILE_NAME)));
                    }
                }
                if (this.dgvAttachment.Columns[e.ColumnIndex].Name == this.ATTACHMENT_NAME.Name)
                {
                    Runner.RunNewThread(openAttachment, Properties.Resources.DISPLAYINPROGRESS);
                }
            }
        }

        private void txtSearchAttachment_QuickSearch(object sender, EventArgs e)
        {
            bindAttachment();
        }

        private void chkCAPACITY_CHARGE_CheckedChanged(object sender, EventArgs e)
        {
            this.txtPOWER_CAPACITY.Enabled = this.chkCAPACITY_CHARGE.Checked;
        }

        private void DialogCustomer_Load(object sender, EventArgs e)
        {

        }
    }
}