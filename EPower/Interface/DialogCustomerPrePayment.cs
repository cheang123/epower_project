﻿using EPower.Base.Logic;
using EPower.Interface.BankPayment;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogCustomerPrePayment : ExDialog
    {
        bool _loading = false;
        DateTime datNow = DBDataContext.Db.GetSystemDate().AddDays(double.Parse(DBDataContext.Db.TBL_UTILITies.FirstOrDefault(u => u.UTILITY_ID == (int)Utility.INVOICE_DUE_DATE).UTILITY_VALUE));

        #region Constructor
        public DialogCustomerPrePayment()
        {
            InitializeComponent();
            lookup();
            bind();
        }
        #endregion

        #region Method
        private void lookup()
        {
            _loading = true;
            UIHelper.SetDataSourceToComboBox(this.cboCustomerType, Lookup.GetCustomerTypes(), Resources.ALL_TYPE);
            UIHelper.SetDataSourceToComboBox(this.cboArea, Lookup.GetAreas(), Resources.ALL_AREA);
            UIHelper.SetDataSourceToComboBox(this.cboBillingCycle, Lookup.GetBillingCycles(), Resources.ALL_CYCLE);
            UIHelper.SetDataSourceToComboBox(this.cboCurrencyId, Lookup.GetCurrencies(), Resources.ALL_CURRENCY);
            cboStatus.SelectedIndex = 0;

            _loading = false;
        }

        private void bind()
        {
            if (_loading) return;

            int customerTypeId = 0;
            int areaID = 0;
            int billingCycleId = 0;
            int currencyId = 0;
            int statusId = cboStatus.SelectedIndex;

            if (cboCustomerType.SelectedIndex != -1)
            {
                customerTypeId = (int)this.cboCustomerType.SelectedValue;
            }
            if (cboArea.SelectedIndex != -1)
            {
                areaID = (int)this.cboArea.SelectedValue;
            }
            if (cboBillingCycle.SelectedIndex != -1)
            {
                billingCycleId = (int)cboBillingCycle.SelectedValue;
            }
            if (cboCurrencyId.SelectedIndex != -1)
            {
                currencyId = (int)cboCurrencyId.SelectedValue;
            }
            string strSearch = this.txtSearchCus.Text.ToLower();

            btnPENALTY.Enabled = statusId == 0 ? true : false;
            chkCheckAll_.Checked = false;

            //get cut of amount.
            decimal decCutAmount = Convert.ToDecimal(DBDataContext.Db.TBL_UTILITies
                                .FirstOrDefault(u => u.UTILITY_ID == (int)Utility.CUT_OFF_AMOUNT)
                                .UTILITY_VALUE);



            bool onlyPower = DataHelper.ParseToBoolean(Method.Utilities[Utility.USE_ONLY_POWER_INVOICE_TO_BLOCK_CUSTOMER]);

            DateTime datCurrentDate = DBDataContext.Db.GetSystemDate();
            Cursor.Current = Cursors.WaitCursor;

            DataTable dt = new DataTable();

            Runner.RunNewThread(delegate ()
            {
                if (statusId == 0)
                {
                    var data = (from c in DBDataContext.Db.TBL_CUSTOMERs
                                join ct in DBDataContext.Db.TBL_CUSTOMER_TYPEs on c.CUSTOMER_TYPE_ID equals ct.CUSTOMER_TYPE_ID
                                join a in DBDataContext.Db.TBL_AREAs on c.AREA_ID equals a.AREA_ID
                                join inv in
                                    (from i in DBDataContext.Db.TBL_INVOICEs
                                     join cur in DBDataContext.Db.TLKP_CURRENCies on i.CURRENCY_ID equals cur.CURRENCY_ID
                                     where
                                          (i.INVOICE_STATUS == (int)InvoiceStatus.Open
                                                  && (!onlyPower || i.IS_SERVICE_BILL == false))
                                     group i by new { i.CUSTOMER_ID, cur.CURRENCY_ID, cur.CURRENCY_SING } into invoice
                                     select (from row in invoice
                                             orderby row.DUE_DATE
                                             select new
                                             {
                                                 row.CUSTOMER_ID,
                                                 invoice.Key.CURRENCY_ID,
                                                 invoice.Key.CURRENCY_SING,
                                                 BALANCE_DUE = invoice.Sum(row1 => row1.SETTLE_AMOUNT - row1.PAID_AMOUNT),
                                                 row.DUE_DATE,
                                                 LAST_DUE_DATE = invoice.Max(x => x.DUE_DATE)//invoice.OrderByDescending(x=>x.DUE_DATE).FirstOrDefault().DUE_DATE
                                             }).First()
                                           ) on c.CUSTOMER_ID equals inv.CUSTOMER_ID
                                orderby inv.BALANCE_DUE descending
                                where c.STATUS_ID == (int)CustomerStatus.Active
                                && c.IS_POST_PAID
                                && ((c.CUSTOMER_CODE + " " + c.LAST_NAME_KH + " " + c.FIRST_NAME_KH + " " + c.PHONE_1).ToLower().Contains(strSearch))
                                && inv.BALANCE_DUE >= decCutAmount
                                && inv.DUE_DATE.AddDays(c.CUT_OFF_DAY).Date <= datCurrentDate.Date
                                && (customerTypeId == 0 || c.CUSTOMER_TYPE_ID == customerTypeId)
                                && (areaID == 0 || c.AREA_ID == areaID)
                                && (billingCycleId == 0 || c.BILLING_CYCLE_ID == billingCycleId)
                                && (currencyId == 0 || inv.CURRENCY_ID == currencyId)
                                && !DBDataContext.Db.TBL_INVOICEs.Any(x => x.IS_SERVICE_BILL && x.DUE_DATE.AddDays(c.CUT_OFF_DAY).Date > datCurrentDate.Date && x.INVOICE_STATUS == (int)InvoiceStatus.Open && x.CUSTOMER_ID == c.CUSTOMER_ID)
                                select new
                                {
                                    IS_CHECK = false,
                                    c.CUSTOMER_CODE,
                                    c.CUSTOMER_ID,
                                    ct.CUSTOMER_TYPE_NAME,
                                    a.AREA_NAME,
                                    CUSTOMER_NAME = c.LAST_NAME_KH + " " + c.FIRST_NAME_KH,
                                    DUE_DATE = inv.DUE_DATE.AddDays(c.CUT_OFF_DAY),
                                    Day = (datCurrentDate.Date - inv.DUE_DATE.Date.AddDays(c.CUT_OFF_DAY)).TotalDays,
                                    BALANCE_DUE = inv.BALANCE_DUE,
                                    inv.CURRENCY_SING
                                });

                    dt = data._ToDataTable();
                }
                else
                {
                    var data = (from c in DBDataContext.Db.TBL_CUSTOMERs
                                join ct in DBDataContext.Db.TBL_CUSTOMER_TYPEs on c.CUSTOMER_TYPE_ID equals ct.CUSTOMER_TYPE_ID
                                join a in DBDataContext.Db.TBL_AREAs on c.AREA_ID equals a.AREA_ID
                                join inv in
                                    (from i in DBDataContext.Db.TBL_INVOICEs
                                     join cur in DBDataContext.Db.TLKP_CURRENCies on i.CURRENCY_ID equals cur.CURRENCY_ID
                                     where
                                          (i.INVOICE_STATUS == (int)InvoiceStatus.Open
                                                  && (!onlyPower || i.IS_SERVICE_BILL == false))
                                     group i by new { i.CUSTOMER_ID, cur.CURRENCY_ID, cur.CURRENCY_SING } into invoice
                                     select (from row in invoice
                                             orderby row.DUE_DATE
                                             select new
                                             {
                                                 row.CUSTOMER_ID,
                                                 invoice.Key.CURRENCY_ID,
                                                 invoice.Key.CURRENCY_SING,
                                                 BALANCE_DUE = invoice.Sum(row1 => row1.SETTLE_AMOUNT - row1.PAID_AMOUNT),
                                                 row.DUE_DATE,
                                                 LAST_DUE_DATE = invoice.Max(x => x.DUE_DATE)//invoice.OrderByDescending(x=>x.DUE_DATE).FirstOrDefault().DUE_DATE
                                             }).First()
                                           ) on c.CUSTOMER_ID equals inv.CUSTOMER_ID
                                orderby inv.BALANCE_DUE descending
                                where c.STATUS_ID == (int)CustomerStatus.Active
                                && c.IS_POST_PAID
                                && ((c.CUSTOMER_CODE + " " + c.LAST_NAME_KH + " " + c.FIRST_NAME_KH + " " + c.PHONE_1).ToLower().Contains(strSearch))
                                && inv.BALANCE_DUE >= decCutAmount
                                && inv.DUE_DATE.AddDays(c.CUT_OFF_DAY).Date <= datCurrentDate.Date
                                && (customerTypeId == 0 || c.CUSTOMER_TYPE_ID == customerTypeId)
                                && (areaID == 0 || c.AREA_ID == areaID)
                                && (billingCycleId == 0 || c.BILLING_CYCLE_ID == billingCycleId)
                                && (currencyId == 0 || inv.CURRENCY_ID == currencyId)
                                && DBDataContext.Db.TBL_INVOICEs.Any(x => x.IS_SERVICE_BILL && x.DUE_DATE.AddDays(c.CUT_OFF_DAY).Date > datCurrentDate.Date && x.INVOICE_STATUS == (int)InvoiceStatus.Open && x.CUSTOMER_ID == c.CUSTOMER_ID)
                                select new
                                {
                                    IS_CHECK = false,
                                    c.CUSTOMER_CODE,
                                    c.CUSTOMER_ID,
                                    ct.CUSTOMER_TYPE_NAME,
                                    a.AREA_NAME,
                                    CUSTOMER_NAME = c.LAST_NAME_KH + " " + c.FIRST_NAME_KH,
                                    DUE_DATE = inv.DUE_DATE.AddDays(c.CUT_OFF_DAY),
                                    Day = (datCurrentDate.Date - inv.DUE_DATE.Date.AddDays(c.CUT_OFF_DAY)).TotalDays,
                                    BALANCE_DUE = inv.BALANCE_DUE,
                                    inv.CURRENCY_SING
                                });

                    dt = data._ToDataTable();
                }


                //if (cboStatus.SelectedIndex == 0)
                //{
                //    data = data.Where(d => !DBDataContext.Db.TBL_INVOICEs.Any(i => i.IS_SERVICE_BILL && d.DUE_DATE.Date > datCurrentDate.Date && i.INVOICE_STATUS == (int)InvoiceStatus.Open && i.CUSTOMER_ID == d.CUSTOMER_ID));
                //}
                //else if (cboStatus.SelectedIndex == 1)
                //{
                //    data = data.Where(d => !DBDataContext.Db.TBL_INVOICEs.Any(i => i.IS_SERVICE_BILL && d.DUE_DATE.Date > datCurrentDate.Date && i.INVOICE_STATUS == (int)InvoiceStatus.Open && i.CUSTOMER_ID == d.CUSTOMER_ID));
                //}

                //dt=data._ToDataTable();

            }, Resources.PROCESSING);

            dgv.DataSource = dt;
            getTotalCustomer();
            Cursor.Current = Cursors.Default;
        }

        private void getTotalCustomer()
        {
            int totalCustomer = 0;
            foreach (DataGridViewRow row in this.dgv.Rows)
            {
                if (DataHelper.ParseToBoolean(row.Cells[this.IS_CHECK_.Name].Value.ToString()))
                {
                    totalCustomer += 1;
                }
            }
            lblTotalSelectCustomer_.Text = totalCustomer.ToString();
        }

        private void CreateNewInvoice(TBL_CUSTOMER _objCustomer, TBL_INVOICE_ITEM objInvoiceItem, DateTime dueDate)
        {
            DateTime dtInvoiceMonth = new DateTime();
            var exchangeRate = new ExchangeRateLogic().findLastExchangeRate(DBDataContext.Db.GetSystemDate(), objInvoiceItem.CURRENCY_ID);
            //select all customer that in this cycle 
            var CusInfo = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == _objCustomer.CUSTOMER_ID);
            if (!CusInfo.IS_POST_PAID)
            {
                dtInvoiceMonth = DBDataContext.Db.GetSystemDate();
                dtInvoiceMonth = new DateTime(dtInvoiceMonth.Year, dtInvoiceMonth.Month, 1);
            }
            else
            {
                dtInvoiceMonth = Method.GetNextBillingMonth(_objCustomer.BILLING_CYCLE_ID);
            }

            TBL_INVOICE objInv = new TBL_INVOICE();
            objInv = Method.NewInvoice(true, objInvoiceItem.INVOICE_ITEM_NAME, _objCustomer.CUSTOMER_ID);
            objInv.CURRENCY_ID = objInvoiceItem.CURRENCY_ID;
            objInv.CUSTOMER_ID = _objCustomer.CUSTOMER_ID;
            objInv.DUE_DATE = dueDate;
            objInv.INVOICE_DATE = DBDataContext.Db.GetSystemDate();
            objInv.INVOICE_MONTH = dtInvoiceMonth;
            objInv.METER_CODE = "";
            objInv.START_DATE = datNow;
            objInv.END_DATE = datNow;
            objInv.START_PAY_DATE = datNow;
            //objInv.EXCHANGE_RATE = exchangeRate.EXCHANGE_RATE;
            //objInv.EXCHANGE_RATE_DATE = exchangeRate.CREATE_ON;
            //insert invoice to db in order to get primary key
            DBDataContext.Db.TBL_INVOICEs.InsertOnSubmit(objInv);
            DBDataContext.Db.SubmitChanges();

            TBL_INVOICE_DETAIL obj = new TBL_INVOICE_DETAIL();
            obj.AMOUNT = objInvoiceItem.PRICE;
            obj.END_USAGE = 0;
            obj.INVOICE_ID = objInv.INVOICE_ID;
            obj.INVOICE_ITEM_ID = objInvoiceItem.INVOICE_ITEM_ID;
            obj.PRICE = objInvoiceItem.PRICE;
            obj.START_USAGE = 0;
            obj.USAGE = 1;
            obj.CHARGE_DESCRIPTION = objInvoiceItem.INVOICE_ITEM_NAME;
            obj.REF_NO = objInv.INVOICE_NO;
            obj.TRAN_DATE = objInv.INVOICE_DATE;
            obj.EXCHANGE_RATE = exchangeRate.EXCHANGE_RATE;
            obj.EXCHANGE_RATE_DATE = exchangeRate.CREATE_ON;
            DBDataContext.Db.TBL_INVOICE_DETAILs.InsertOnSubmit(obj);

            //Update Total Amount to Invoice;
            objInv.TOTAL_AMOUNT += obj.AMOUNT;
            objInv.SETTLE_AMOUNT = UIHelper.Round(objInv.TOTAL_AMOUNT, objInv.CURRENCY_ID);
            DBDataContext.Db.SubmitChanges();
        }

        #endregion

        private void cboCustomerType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_loading == false)
            {
                bind();
            }
        }


        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1 && e.ColumnIndex == dgv.Columns[this.IS_CHECK_.Name].Index)
            {
                dgv.Rows[e.RowIndex].Cells[this.IS_CHECK_.Name].Value = !(bool)dgv.Rows[e.RowIndex].Cells[IS_CHECK_.Name].Value;
                getTotalCustomer();
            }
        }

        private void chkCheckAll_CheckedChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in this.dgv.Rows)
            {
                row.Cells[IS_CHECK_.Name].Value = this.chkCheckAll_.Checked;
            }
            getTotalCustomer();
        }

        private void txtAmount_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void txtAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        }

        private void btnPenalty_Click(object sender, EventArgs e)
        {
            if (lblTotalSelectCustomer_.Text == "0")
            {
                MsgBox.ShowInformation(Resources.MS_NO_SELECT_CUSTOMER);
                return;
            }
            DialogCustomersCharge dig = new DialogCustomersCharge(lblTotalSelectCustomer_.Text, datNow);

            if (dig.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    Runner.RunNewThread(delegate ()
                    {
                        using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                        {
                            foreach (DataGridViewRow item in dgv.Rows)
                            {
                                if ((bool)item.Cells[IS_CHECK_.Name].Value)
                                {
                                    TBL_CUSTOMER objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == (int)item.Cells[CUSTOMER_ID.Name].Value);
                                    CreateNewInvoice(objCustomer, new TBL_INVOICE_ITEM { PRICE = dig.amount, INVOICE_ITEM_ID = dig.invoiceItemId, INVOICE_ITEM_NAME = dig.invoiceItemName, CURRENCY_ID = dig.currencyId }, dig.date);
                                }
                            }
                            tran.Complete();
                        }

                    }, Resources.PROCESSING);
                    bind();

                    if (DialogRemindSendData.IsRequiredReminder())
                    {
                        new DialogRemindSendData().ShowDialog();
                    }
                }
                catch (Exception ex)
                {
                    MsgBox.ShowError(ex);
                }
            }
        }
    }
}