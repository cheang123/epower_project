﻿using EPower.Base.Logic;
using EPower.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using Login = SoftTech.Security.Logic.Login;

namespace EPower.Interface
{
    public partial class DialogPrintInvoice : ExDialog
    {
        bool isConcurrentPrinting = false;
        public DialogPrintInvoice(DataTable source)
        {
            InitializeComponent();
            isConcurrentPrinting = DataHelper.ParseToBoolean(Method.Utilities[Utility.INVOICE_CONCURRENT_PRINTING]);
            foreach (DataRow row in source.Rows)
            {
                this.dgv.Rows.Add(row.ItemArray);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            if (this.dgv.SelectedRows.Count > 0 )
            {
                SupportTaker objSt = new SupportTaker(SupportTaker.Process.ViewInvoice);
                if (objSt.Check())
                {
                    return;
                }

                //if not exists report invoice 
                string path = string.Format(Settings.Default.PATH_REPORT, Settings.Default.LANGUAGE == "en-US" ? "" : "" + Settings.Default.LANGUAGE);
                path = Path.Combine(Directory.GetCurrentDirectory(), path);
                string PathCustomized = path + "Customized\\" + Settings.Default.REPORT_INVOICE;
                if (!File.Exists(PathCustomized))
                {
                    if (!File.Exists(path + Settings.Default.REPORT_INVOICE))
                    {
                        MsgBox.ShowInformation(string.Format(Resources.NOT_FOUND_INVOICE, "", Settings.Default.REPORT_INVOICE), Resources.INFORMATION);
                        return;
                    }
                }

                if (isConcurrentPrinting)
                {
                    TBL_PRINT_INVOICE objPrintInvoice = new TBL_PRINT_INVOICE()
                    {
                        PRINT_DATE = DBDataContext.Db.GetSystemDate(),
                        LOGIN_ID = Login.CurrentLogin.LOGIN_ID
                    };
                    using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                    {
                        DBDataContext.Db.TBL_PRINT_INVOICEs.InsertOnSubmit(objPrintInvoice);
                        DBDataContext.Db.SubmitChanges();
                        int i = 1;
                        foreach (DataGridViewRow row in this.dgv.SelectedRows)
                        {
                            if (row.Visible == true)
                            {
                                DBDataContext.Db.TBL_PRINT_INVOICE_DETAILs.InsertOnSubmit(new TBL_PRINT_INVOICE_DETAIL()
                                {
                                    PRINT_INVOICE_ID = objPrintInvoice.PRINT_INVOICE_ID,
                                    INVOICE_ID = int.Parse(row.Cells[INVOICE_ID.Name].Value.ToString()),
                                    PRINT_ORDER = i++
                                });
                            }
                        }
                        DBDataContext.Db.SubmitChanges();
                        tran.Complete();
                    }
                    new DialogCustomerPrintInvoice(true, objPrintInvoice.PRINT_INVOICE_ID).ShowDialog();
                }
                else
                {
                    using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                    {
                        DBDataContext.Db.TBL_INVOICE_TO_PRINTs.DeleteAllOnSubmit(DBDataContext.Db.TBL_INVOICE_TO_PRINTs);
                        int i = 1;
                        foreach (DataGridViewRow row in this.dgv.SelectedRows)
                        {
                            DBDataContext.Db.TBL_INVOICE_TO_PRINTs.InsertOnSubmit(new TBL_INVOICE_TO_PRINT()
                            {
                                INVOICE_ID = (long)row.Cells[this.INVOICE_ID.Name].Value,
                                PRINT_ORDER = i++
                            });
                        }
                        DBDataContext.Db.SubmitChanges();
                        tran.Complete();
                    }
                    new DialogCustomerPrintInvoice(false, 0).ShowDialog();
                }
                     
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (this.dgv.SelectedRows.Count > 0)
            {
                SupportTaker objSt = new SupportTaker(SupportTaker.Process.PrintInvoice);
                if (objSt.Check())
                {
                    return;
                }

                //if not exists report invoice 
                string path = string.Format(Settings.Default.PATH_REPORT, Settings.Default.LANGUAGE == "en-US" ? "" : "" + Settings.Default.LANGUAGE);
                path = Path.Combine(Directory.GetCurrentDirectory(), path);
                string PathCustomized = path + "Customized\\" + Settings.Default.REPORT_INVOICE;
                var PintInvoieID = 0;
                if (!File.Exists(PathCustomized))
                {
                    if (!File.Exists(path + Settings.Default.REPORT_INVOICE))
                    {
                        MsgBox.ShowInformation(string.Format(Resources.NOT_FOUND_INVOICE, "", Settings.Default.REPORT_INVOICE), Resources.INFORMATION);
                        return;
                    }
                }

                try
                {
                    if (isConcurrentPrinting)
                    {
                        TBL_PRINT_INVOICE objPrintInvoice = new TBL_PRINT_INVOICE()
                        {
                            PRINT_DATE = DBDataContext.Db.GetSystemDate(),
                            LOGIN_ID = Login.CurrentLogin.LOGIN_ID
                        };

                        using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                        {
                            DBDataContext.Db.TBL_PRINT_INVOICEs.InsertOnSubmit(objPrintInvoice);
                            DBDataContext.Db.SubmitChanges();
                            int i = 1;
                            foreach (DataGridViewRow row in this.dgv.SelectedRows)
                            {
                                DBDataContext.Db.TBL_PRINT_INVOICE_DETAILs.InsertOnSubmit(new TBL_PRINT_INVOICE_DETAIL()
                                {
                                    PRINT_INVOICE_ID = objPrintInvoice.PRINT_INVOICE_ID,
                                    INVOICE_ID = int.Parse(row.Cells[INVOICE_ID.Name].Value.ToString()),
                                    PRINT_ORDER = i++
                                });
                            }
                            DBDataContext.Db.SubmitChanges();
                            tran.Complete();
                        }
                        PintInvoieID = objPrintInvoice.PRINT_INVOICE_ID;
                    }
                    else
                    {
                        using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                        {
                            DBDataContext.Db.TBL_INVOICE_TO_PRINTs.DeleteAllOnSubmit(DBDataContext.Db.TBL_INVOICE_TO_PRINTs);
                            int i = 1;
                            foreach (DataGridViewRow row in this.dgv.SelectedRows)
                            {
                                DBDataContext.Db.TBL_INVOICE_TO_PRINTs.InsertOnSubmit(new TBL_INVOICE_TO_PRINT()
                                {
                                    INVOICE_ID = (long)row.Cells[this.INVOICE_ID.Name].Value,
                                    PRINT_ORDER = i++
                                });
                            }
                            DBDataContext.Db.SubmitChanges();
                            tran.Complete();
                        }

                        CrystalReportHelper cr = Method.GetInvoiceReport();
                        cr.PrintReport(Settings.Default.PRINTER_INVOICE);
                        cr.Dispose();
                    }

                    //Check Print New and Old Invocie
                    var InvoiceName = Settings.Default.REPORT_INVOICE;
                    var PinterInvoiceName = Settings.Default.PRINTER_INVOICE;
                    var TemplateString = DBDataContext.Db.TBL_UTILITies.FirstOrDefault(x => x.UTILITY_ID == 40).UTILITY_VALUE.ToString();
                    var Templates = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BillPrintingListModel>>(TemplateString);
                    var template = Templates.FirstOrDefault(x => x.TemplateName == InvoiceName);
                    Runner.Run(() =>
                    {
                        this.BeginInvoke(new MethodInvoker(delegate
                        {
                            if (!template.IsOld)
                            {
                                ReportLogic.Instance.GetPrintNewInvoice(InvoiceName, PinterInvoiceName, PintInvoieID);
                            }
                            else
                            {
                                CrystalReportHelper cr = Method.GetInvoiceReport(InvoiceName);
                                cr.SetParameter("@PRINT_INVOICE_ID", PintInvoieID);
                                cr.PrintReport(PinterInvoiceName);
                                cr.Dispose();
                            }
                        }));
                    });
                }
                catch(Exception ex)
                {
                    MsgBox.LogError(ex);
                    MsgBox.ShowWarning(string.Format(Base.Properties.Resources.YOU_CANNOT_PROCESS, "", Base.Properties.Resources.PRINT_INVOICE), Base.Properties.Resources.WARNING);
                }                
            }
        }

        private void txtSearch_QuickSearch(object sender, EventArgs e)
        {
            string search = this.txtSearch.Text.ToLower();
            foreach (DataGridViewRow row in this.dgv.Rows)
            {
                row.Visible = (row.Cells[this.CUSTOMER_CODE.Name].Value.ToString() + row.Cells[this.CUSTOMER_NAME.Name].Value.ToString() + row.Cells[this.BOX.Name].Value.ToString()).ToLower().Contains(search);
            }
        }

        private void txtSearch_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }     
    }
}