﻿using DevExpress.Utils.Win;
using DevExpress.XtraEditors.Popup;
using EPower.Interface.BankPayment;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogEmailToBank : ExDialog
    {
        private enum BANK_NAME
        {
            WING=0,
            AMRET=1,
            ACLEDA=2,
            SATHAPANA=3,
            AMK=4,
            HKL=5,
            EMONEY=6
        }

        #region Constructor
        string bankRegistration = "";
        TBL_COMPANY objCompany = DBDataContext.Db.TBL_COMPANies.FirstOrDefault(x => x.COMPANY_ID == 1);
        decimal totalFileSize = 0;
        public DialogEmailToBank()
        {
            InitializeComponent();
            TBL_UTILITY objU = DBDataContext.Db.TBL_UTILITies.FirstOrDefault(x => x.UTILITY_ID == (int)Utility.NOTE_BANK_REGISTRATION);
            cboBank.Properties.LookAndFeel.SkinName = "DevExpress Style";
            cboBank.Properties.Popup += Properties_Popup;
            BindAcitavtedBank();
            //foreach (var x in objU.UTILITY_VALUE.Split(','))
            //{
            //    if (x.ToUpper() == BANK_NAME.WING.ToString().ToUpper()) cbWING.Checked = true;
            //    else if (x.ToUpper() == BANK_NAME.AMRET.ToString().ToUpper()) cbAMRET.Checked = true;
            //    else if (x.ToUpper() == BANK_NAME.ACLEDA.ToString().ToUpper()) cbACLEDA.Checked = true;
            //    else if (x.ToUpper() == BANK_NAME.SATHAPANA.ToString().ToUpper()) cbSATHAPANA.Checked = true;
            //    else if (x.ToUpper() == BANK_NAME.AMK.ToString().ToUpper()) cbAMK.Checked = true;
            //    else if (x.ToUpper() == BANK_NAME.HKL.ToString().ToUpper()) cbHATTHA.Checked = true;
            //    else if (x.ToUpper() == BANK_NAME.EMONEY.ToString().ToUpper()) cbEMONEY.Checked = true; 
            //}
            txtAddress.Text = objCompany.LICENSE_NUMBER + " " + objCompany.LICENSE_NAME_KH + System.Environment.NewLine + objCompany.ADDRESS;
        
        }

        private void Properties_Popup(object sender, EventArgs e)
        {
            CheckedPopupContainerForm f = (sender as IPopupControl).PopupWindow as CheckedPopupContainerForm;
            f.OkButton.Text = Resources.OK;
            f.CloseButton.Text = Resources.CLOSE;
        }

        #endregion

        private void btnCLOSE_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DialogEmailToBank_Load(object sender, EventArgs e)
        {
            foreach (Control item in content.Controls)
            { 
                if (item is CheckBox)
                {
                    item.Text = string.Empty; 
                }
            }
        }  
        private void BindAcitavtedBank()
        {
            var paymentOperation = new PaymentOperation() { };
            var banks =  paymentOperation.GetActivatedBank();
            cboBank.Properties.DataSource = banks.Where(x=>x.ACTIVATED).ToList(); 
            cboBank.Properties.DisplayMember = nameof(LICENSE_BANK_STATUS.BANK_NAME); 
            cboBank.Properties.ValueMember = nameof(LICENSE_BANK_STATUS.BANK_CODE);
            cboBank.Properties.SelectAllItemCaption = Resources.ALL_BANK;
        }

        private void lblADD_Click(object sender, EventArgs e)
        {
            OpenFileDialog diag = new OpenFileDialog();
            diag.Filter = "All File (*.*)|*.*";
            diag.Multiselect = false;
            if (diag.ShowDialog() == DialogResult.OK)
            {
                System.IO.FileInfo fileInfo=new System.IO.FileInfo (diag.FileName);
                decimal fileSize=fileInfo.Length/1024;
                if (fileSize > 25600) // 25MB
                {
                    MsgBox.ShowWarning(string.Format(Resources.MSG_FILE_SIZE_OVER_LIMITED, (fileSize/1024).ToString("N0")+"MB"),Resources.WARNING);
                    return;
                }
                if (totalFileSize + (fileSize/1024) > 25)
                {
                    MsgBox.ShowWarning(string.Format(Resources.MSG_TOTAL_FILE_SIZE_OVER_LIMITED,(totalFileSize+(fileSize/1024)).ToString("N0")), Resources.WARNING);
                    return;
                }
                totalFileSize += (fileSize/1024);
                dgv.Rows.Add(diag.FileName, fileSize);              
            }
        }

        private void lblREMOVE_Click(object sender, EventArgs e)
        {
            if (dgv.Rows.Count > 0)
            {
                dgv.Rows.RemoveAt(dgv.SelectedRows[0].Index);
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
          //  string sendTo = "thunnynt@gmail.com";
            string sendTo = "system@ubill24.com";
            string subject = objCompany.LICENSE_NUMBER + " " + objCompany.LICENSE_NAME_KH + " want to use Bank Payment";
            string body = "Dear Sir" + Environment.NewLine + "This email from " + txtAddress.Text + " " + Environment.NewLine + "He/She want to use Bank Payment with "; 
            int count = 0;
            bankRegistration = cboBank.EditValue.ToString();
            body += " " +  cboBank.Text;

            //foreach (var item in )
            //{

            //}
            //foreach (Control item in content.Controls)
            //{
            //    if (item is CheckBox)
            //    {
            //        CheckBox cb = (CheckBox)item;
            //        if (cb.Checked)
            //        {
            //            bankRegistration += cb.Tag + ",";
            //            body += cb.Name.Replace("cb", "") + ", ";
            //            count++;
            //        }
            //    }
            //}

            body += Environment.NewLine + "Best Regard. ";
            if (string.IsNullOrEmpty(cboBank.Text))
            {
                MsgBox.ShowInformation(Resources.MSG_PLS_SELECT_BANK_BEFORE_SEND_EMAIL, Resources.WARNING);
                return;
            }
            var attachments = this.dgv.Rows.Cast<DataGridViewRow>().Select(x => x.Cells[this.FILE_PATH.Name].Value.ToString()).ToArray();
            bool success = false;

            Runner.RunNewThread(delegate()
            {
                WebHelper.SendMail(sendTo, subject, body, true, false, attachments);
                success = true;
            }, Resources.PROCESSING);

            if (success)
            {
                TBL_UTILITY objU = DBDataContext.Db.TBL_UTILITies.FirstOrDefault(x => x.UTILITY_ID == (int)Utility.NOTE_BANK_REGISTRATION);
                objU.UTILITY_VALUE = bankRegistration;
                DBDataContext.Db.SubmitChanges();
                this.Close();
            }
        }
         
    }
}