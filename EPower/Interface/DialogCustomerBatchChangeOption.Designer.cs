﻿namespace EPower.Interface
{
    partial class DialogCustomerBatchChangeOption
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCUSTOMER_CONNECTION_TYPE = new System.Windows.Forms.Label();
            this.lblCUSTOMER_GROUP = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClose = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.cboCustomerGroup = new System.Windows.Forms.ComboBox();
            this.lblAREA = new System.Windows.Forms.Label();
            this.cboArea = new System.Windows.Forms.ComboBox();
            this.cboCustomerConnectionType = new System.Windows.Forms.ComboBox();
            this.lblCUSTOMER_STATUS = new System.Windows.Forms.Label();
            this.lblVILLAGE = new System.Windows.Forms.Label();
            this.cboCustomerStatus = new System.Windows.Forms.ComboBox();
            this.cboVillage = new System.Windows.Forms.ComboBox();
            this.lblCOMMUNE = new System.Windows.Forms.Label();
            this.lblPOLE = new System.Windows.Forms.Label();
            this.cboCommune = new System.Windows.Forms.ComboBox();
            this.cboPole = new System.Windows.Forms.ComboBox();
            this.chkIsmarketVendor = new System.Windows.Forms.CheckBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.chkIsmarketVendor);
            this.content.Controls.Add(this.lblCUSTOMER_STATUS);
            this.content.Controls.Add(this.lblVILLAGE);
            this.content.Controls.Add(this.cboCustomerStatus);
            this.content.Controls.Add(this.cboVillage);
            this.content.Controls.Add(this.lblCOMMUNE);
            this.content.Controls.Add(this.lblPOLE);
            this.content.Controls.Add(this.cboCommune);
            this.content.Controls.Add(this.cboPole);
            this.content.Controls.Add(this.lblAREA);
            this.content.Controls.Add(this.cboArea);
            this.content.Controls.Add(this.lblCUSTOMER_CONNECTION_TYPE);
            this.content.Controls.Add(this.lblCUSTOMER_GROUP);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnClose);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.cboCustomerConnectionType);
            this.content.Controls.Add(this.cboCustomerGroup);
            this.content.Size = new System.Drawing.Size(343, 350);
            this.content.Text = "CONTENT";
            this.content.Controls.SetChildIndex(this.cboCustomerGroup, 0);
            this.content.Controls.SetChildIndex(this.cboCustomerConnectionType, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnClose, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_GROUP, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_CONNECTION_TYPE, 0);
            this.content.Controls.SetChildIndex(this.cboArea, 0);
            this.content.Controls.SetChildIndex(this.lblAREA, 0);
            this.content.Controls.SetChildIndex(this.cboPole, 0);
            this.content.Controls.SetChildIndex(this.cboCommune, 0);
            this.content.Controls.SetChildIndex(this.lblPOLE, 0);
            this.content.Controls.SetChildIndex(this.lblCOMMUNE, 0);
            this.content.Controls.SetChildIndex(this.cboVillage, 0);
            this.content.Controls.SetChildIndex(this.cboCustomerStatus, 0);
            this.content.Controls.SetChildIndex(this.lblVILLAGE, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_STATUS, 0);
            this.content.Controls.SetChildIndex(this.chkIsmarketVendor, 0);
            // 
            // lblCUSTOMER_CONNECTION_TYPE
            // 
            this.lblCUSTOMER_CONNECTION_TYPE.AutoSize = true;
            this.lblCUSTOMER_CONNECTION_TYPE.Location = new System.Drawing.Point(22, 221);
            this.lblCUSTOMER_CONNECTION_TYPE.Name = "lblCUSTOMER_CONNECTION_TYPE";
            this.lblCUSTOMER_CONNECTION_TYPE.Size = new System.Drawing.Size(87, 25);
            this.lblCUSTOMER_CONNECTION_TYPE.TabIndex = 42;
            this.lblCUSTOMER_CONNECTION_TYPE.Text = "ការភ្ជាប់ចរន្ត";
            // 
            // lblCUSTOMER_GROUP
            // 
            this.lblCUSTOMER_GROUP.AutoSize = true;
            this.lblCUSTOMER_GROUP.Location = new System.Drawing.Point(22, 188);
            this.lblCUSTOMER_GROUP.Name = "lblCUSTOMER_GROUP";
            this.lblCUSTOMER_GROUP.Size = new System.Drawing.Size(55, 25);
            this.lblCUSTOMER_GROUP.TabIndex = 41;
            this.lblCUSTOMER_GROUP.Text = "ប្រភេទ";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Location = new System.Drawing.Point(2, 297);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(343, 1);
            this.panel1.TabIndex = 40;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnClose.Location = new System.Drawing.Point(249, 313);
            this.btnClose.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 39;
            this.btnClose.Text = "បិទ";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnOK.Location = new System.Drawing.Point(169, 313);
            this.btnOK.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 38;
            this.btnOK.Text = "យល់ព្រម";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // cboCustomerGroup
            // 
            this.cboCustomerGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomerGroup.DropDownWidth = 350;
            this.cboCustomerGroup.FormattingEnabled = true;
            this.cboCustomerGroup.Location = new System.Drawing.Point(131, 185);
            this.cboCustomerGroup.Name = "cboCustomerGroup";
            this.cboCustomerGroup.Size = new System.Drawing.Size(200, 33);
            this.cboCustomerGroup.TabIndex = 33;
            this.cboCustomerGroup.SelectedIndexChanged += new System.EventHandler(this.cboCustomerGroup_SelectedIndexChanged);
            // 
            // lblAREA
            // 
            this.lblAREA.AutoSize = true;
            this.lblAREA.Location = new System.Drawing.Point(23, 23);
            this.lblAREA.Name = "lblAREA";
            this.lblAREA.Size = new System.Drawing.Size(45, 25);
            this.lblAREA.TabIndex = 51;
            this.lblAREA.Text = "តំបន់";
            // 
            // cboArea
            // 
            this.cboArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboArea.FormattingEnabled = true;
            this.cboArea.Location = new System.Drawing.Point(131, 20);
            this.cboArea.Name = "cboArea";
            this.cboArea.Size = new System.Drawing.Size(200, 33);
            this.cboArea.TabIndex = 50;
            this.cboArea.SelectedIndexChanged += new System.EventHandler(this.cboArea_SelectedIndexChanged);
            // 
            // cboCustomerConnectionType
            // 
            this.cboCustomerConnectionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomerConnectionType.DropDownWidth = 350;
            this.cboCustomerConnectionType.FormattingEnabled = true;
            this.cboCustomerConnectionType.Location = new System.Drawing.Point(131, 218);
            this.cboCustomerConnectionType.Name = "cboCustomerConnectionType";
            this.cboCustomerConnectionType.Size = new System.Drawing.Size(200, 33);
            this.cboCustomerConnectionType.TabIndex = 34;
            // 
            // lblCUSTOMER_STATUS
            // 
            this.lblCUSTOMER_STATUS.AutoSize = true;
            this.lblCUSTOMER_STATUS.Location = new System.Drawing.Point(23, 155);
            this.lblCUSTOMER_STATUS.Name = "lblCUSTOMER_STATUS";
            this.lblCUSTOMER_STATUS.Size = new System.Drawing.Size(71, 25);
            this.lblCUSTOMER_STATUS.TabIndex = 59;
            this.lblCUSTOMER_STATUS.Text = "ស្ថានភាព";
            // 
            // lblVILLAGE
            // 
            this.lblVILLAGE.AutoSize = true;
            this.lblVILLAGE.Location = new System.Drawing.Point(23, 122);
            this.lblVILLAGE.Name = "lblVILLAGE";
            this.lblVILLAGE.Size = new System.Drawing.Size(34, 25);
            this.lblVILLAGE.TabIndex = 58;
            this.lblVILLAGE.Text = "ភូមិ";
            // 
            // cboCustomerStatus
            // 
            this.cboCustomerStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomerStatus.FormattingEnabled = true;
            this.cboCustomerStatus.Location = new System.Drawing.Point(131, 152);
            this.cboCustomerStatus.Name = "cboCustomerStatus";
            this.cboCustomerStatus.Size = new System.Drawing.Size(200, 33);
            this.cboCustomerStatus.TabIndex = 57;
            // 
            // cboVillage
            // 
            this.cboVillage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboVillage.FormattingEnabled = true;
            this.cboVillage.Location = new System.Drawing.Point(131, 119);
            this.cboVillage.Name = "cboVillage";
            this.cboVillage.Size = new System.Drawing.Size(200, 33);
            this.cboVillage.TabIndex = 56;
            // 
            // lblCOMMUNE
            // 
            this.lblCOMMUNE.AutoSize = true;
            this.lblCOMMUNE.Location = new System.Drawing.Point(22, 89);
            this.lblCOMMUNE.Name = "lblCOMMUNE";
            this.lblCOMMUNE.Size = new System.Drawing.Size(75, 25);
            this.lblCOMMUNE.TabIndex = 55;
            this.lblCOMMUNE.Text = "ឃុំ/សង្កាត់";
            // 
            // lblPOLE
            // 
            this.lblPOLE.AutoSize = true;
            this.lblPOLE.Location = new System.Drawing.Point(22, 56);
            this.lblPOLE.Name = "lblPOLE";
            this.lblPOLE.Size = new System.Drawing.Size(60, 25);
            this.lblPOLE.TabIndex = 54;
            this.lblPOLE.Text = "បង្គោល";
            // 
            // cboCommune
            // 
            this.cboCommune.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCommune.FormattingEnabled = true;
            this.cboCommune.Location = new System.Drawing.Point(131, 86);
            this.cboCommune.Name = "cboCommune";
            this.cboCommune.Size = new System.Drawing.Size(200, 33);
            this.cboCommune.TabIndex = 53;
            this.cboCommune.SelectedIndexChanged += new System.EventHandler(this.cboCommune_SelectedIndexChanged);
            // 
            // cboPole
            // 
            this.cboPole.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPole.FormattingEnabled = true;
            this.cboPole.Location = new System.Drawing.Point(131, 53);
            this.cboPole.Name = "cboPole";
            this.cboPole.Size = new System.Drawing.Size(200, 33);
            this.cboPole.TabIndex = 52;
            // 
            // chkIsmarketVendor
            // 
            this.chkIsmarketVendor.AutoSize = true;
            this.chkIsmarketVendor.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.chkIsmarketVendor.Location = new System.Drawing.Point(131, 257);
            this.chkIsmarketVendor.Name = "chkIsmarketVendor";
            this.chkIsmarketVendor.Size = new System.Drawing.Size(196, 29);
            this.chkIsmarketVendor.TabIndex = 415;
            this.chkIsmarketVendor.Text = "អាជីវករតូបលក់ដូរក្នុងផ្សារ";
            this.chkIsmarketVendor.UseVisualStyleBackColor = true;
            // 
            // DialogCustomerBatchChangeOption
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(343, 373);
            this.Name = "DialogCustomerBatchChangeOption";
            this.Text = "កំណត់";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblCUSTOMER_CONNECTION_TYPE;
        private System.Windows.Forms.Label lblCUSTOMER_GROUP;
        private System.Windows.Forms.Panel panel1;
        private SoftTech.Component.ExButton btnClose;
        private SoftTech.Component.ExButton btnOK;
        public System.Windows.Forms.ComboBox cboCustomerGroup;
        private System.Windows.Forms.Label lblAREA;
        public System.Windows.Forms.ComboBox cboArea;
        private System.Windows.Forms.Label lblCUSTOMER_STATUS;
        private System.Windows.Forms.Label lblVILLAGE;
        public System.Windows.Forms.ComboBox cboCustomerStatus;
        public System.Windows.Forms.ComboBox cboVillage;
        private System.Windows.Forms.Label lblCOMMUNE;
        private System.Windows.Forms.Label lblPOLE;
        public System.Windows.Forms.ComboBox cboCommune;
        public System.Windows.Forms.ComboBox cboPole;
        public System.Windows.Forms.ComboBox cboCustomerConnectionType;
        public System.Windows.Forms.CheckBox chkIsmarketVendor;
    }
}