﻿using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using p= OpenNETCF.Desktop.Communication;
using Process = System.Diagnostics.Process;

namespace EPower.Interface
{
    public partial class DialogDevice : ExDialog
    {
        GeneralProcess _flag;
        TBL_DEVICE _objDevice = new TBL_DEVICE();
        public TBL_DEVICE Device
        {
            get { return _objDevice; }
        }
        TBL_DEVICE _oldObjDevice = new TBL_DEVICE();

        #region Constructor
        public DialogDevice()
        {
            InitializeComponent();
        }

        public DialogDevice(GeneralProcess flag, TBL_DEVICE objDevice)
        {
            InitializeComponent();
            _flag = flag;
            dataLookUp();
            objDevice._CopyTo(_objDevice);
            objDevice._CopyTo(_oldObjDevice);


            if (flag == GeneralProcess.Insert)
            {
                pbSync.Visible=
                btnCONNECT_TO_DEVICE.Enabled = true;
                this.Text = string.Concat(Resources.INSERT, this.Text);
            }
            else if (flag == GeneralProcess.Update)
            {
                this.Text = string.Concat(Resources.UPDATE, this.Text);
                chkREINSTALL_APPLICATION_TO_DEVICE.Visible = true;
            }
            else if (flag == GeneralProcess.Delete)
            {
                this.Text = string.Concat(Resources.DELETE, this.txtCode);
                UIHelper.SetEnabled(this, false);
            }
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
            read();
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

       
        #endregion

        #region Method
        private void read()
        {
            txtCode.Text = _objDevice.DEVICE_CODE;
            cboStatus.SelectedValue = _objDevice.STATUS_ID;
            lblDeviceOemInfo_.Text = _objDevice.DEVICE_OEM_INFO;
            lblDeviceType_.Text = _objDevice.DEVICE_TYPE;
        }

        private void write()
        {
            _objDevice.DEVICE_CODE = txtCode.Text.Trim();
            _objDevice.STATUS_ID = (int)cboStatus.SelectedValue;
            if (_flag== GeneralProcess.Insert)
            {
                _objDevice.DEVICE_TYPE = Devices.MyDevice[DeviceInfo.DeviceType];
                _objDevice.DEVICE_OEM_INFO = Devices.MyDevice[DeviceInfo.DeviceOemInfo];
                _objDevice.DEVICE_HARDWARE_ID = Devices.MyDevice[DeviceInfo.DeviceHardwareId];
                _objDevice.DEVICE_TYPE_ID = (int)DeviceType.Mobile;
                _objDevice.DEVICE_CODE = "";
            }
            
        }

        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();
            if (txtCode.Text.Trim() == string.Empty)
            {
                txtCode.SetValidation(string.Format(Resources.REQUIRED, lblDEVICE_CODE.Text));
                val = true;
            }

            if (cboStatus.SelectedIndex==-1)
            {
                cboStatus.SetValidation(string.Format(Resources.REQUIRED, lblSTATUS.Text));
                val = true;
            }

            if (_flag== GeneralProcess.Insert)
            {
                if (Devices.Connect()!=true)
                {
                    val = true;
                    btnCONNECT_TO_DEVICE.SetValidation("");
                }
            }           

            return val;
        }

        private void dataLookUp()
        {
            try
            {
                UIHelper.SetDataSourceToComboBox(cboStatus,
                    from s in DBDataContext.Db.TLKP_DEVICE_STATUS
                    select s);
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }
        #endregion

        #region Operation
        private void btnAdd_Click(object sender, EventArgs e)
        { 
            if (inValid())
            {
                return;
            }
             
            write();

            txtCode.ClearValidation();

            if (DBDataContext.Db.IsExits(_objDevice, "DEVICE_CODE"))
            {
                txtCode.SetValidation(string.Format(Resources.MS_IS_EXISTS, lblDEVICE_CODE.Text));
                return;
            }

            if (DBDataContext.Db.TBL_DEVICEs.FirstOrDefault(
                x=> x.DEVICE_HARDWARE_ID.ToLower()==_objDevice.DEVICE_HARDWARE_ID.ToLower()
                    && x.STATUS_ID==(int)DeviceStatus.Used && x.DEVICE_ID!=_objDevice.DEVICE_ID )!=null)
            {
                MsgBox.ShowInformation(string.Format(Resources.MS_IS_EXISTS, Resources.DEVICE), Resources.INFORMATION);
                return;
            }                       
            
            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {                        
                        pbSync.Enabled = true;
                        //Not Connect to device
                        if (!InstallDeviceApplication())
                        {
                            return;
                        }                        
                        DBDataContext.Db.Insert(_objDevice);
                        
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        if (chkREINSTALL_APPLICATION_TO_DEVICE.Checked)
                        {
                            if (MsgBox.ShowQuestion(Resources.MS_REINSTALL_DEVICE_APPLICATION, Resources.MS_CONFIRM) == DialogResult.No)
                            {
                                return;
                            }
                            //Not Connect to device
                            if (!InstallDeviceApplication())
                            {
                                return;
                            }                    
                        }                        
                        DBDataContext.Db.Update(_oldObjDevice, _objDevice);
                        
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objDevice);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            } 
        }

        /// <summary>
        /// Install E-Mobile application to device.
        /// </summary>
        private bool InstallDeviceApplication()
        {
            if (Devices.Connect()!=true)
            {
                MsgBox.ShowInformation(Resources.REQUIRED_CONNECT_TO_DEVICE);
                return false;
            }

            Process dApp = new Process();
            dApp.StartInfo.FileName = string.Format(@"{0}\\{1}", Application.StartupPath, Settings.Default.DeviceApplication);
            dApp.Start();
            dApp.WaitForExit();
            return true;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void timeConnection_Tick(object sender, EventArgs e)
        {
            timeConnection.Enabled = false;

            bool? conn = Devices.Connect();
            
            if (conn==true)
            {
                lblDeviceOemInfo_.Text = Devices.MyDevice[DeviceInfo.DeviceOemInfo];
                lblDeviceType_.Text = Devices.MyDevice[DeviceInfo.DeviceType];
                resetSyn();
                lblConnectStatus_.Text = Resources.CONNECT_SUCCESS;
            }
            else if (conn==false)
            {
                lblConnectStatus_.Text = Resources.REQUIRED_CONNECT_TO_DEVICE;
                timeConnection.Enabled = true;
            }
            else //if null
            {
                resetSyn();
                lblConnectStatus_.Text = "";
            }
        }

        private bool resetSyn()
        {
            return  pbSync.Enabled =
                    timeConnection.Enabled = false;
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            pbSync.Enabled = true;
            lblConnectStatus_.Text = Resources.CONNECTING;
            timeConnection.Enabled = true;            
        }

        
        #endregion

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objDevice);
        }
    }
}
