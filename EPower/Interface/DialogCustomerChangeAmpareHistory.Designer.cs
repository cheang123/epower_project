﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogCustomerChangeAmpareHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCustomerChangeAmpareHistory));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvHistory = new System.Windows.Forms.DataGridView();
            this.txtCustomerType = new System.Windows.Forms.TextBox();
            this.lblCUSTOMER_TYPE = new System.Windows.Forms.Label();
            this.lblCUSTOMER_CHANGE_AMPARE_HISTORY = new System.Windows.Forms.Label();
            this.lblCUSTOMER_INFORMATION = new System.Windows.Forms.Label();
            this.lblCUSTOMER_NAME = new System.Windows.Forms.Label();
            this.lblAREA = new System.Windows.Forms.Label();
            this.lblCUSTOMER_CODE = new System.Windows.Forms.Label();
            this.txtCustomerCode = new System.Windows.Forms.TextBox();
            this.txtCustomerName = new System.Windows.Forms.TextBox();
            this.txtArea = new System.Windows.Forms.TextBox();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.OLD_AMPARE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NEW_AMPARE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_BY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CHANGE_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NOTE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHistory)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.dgvHistory);
            this.content.Controls.Add(this.txtArea);
            this.content.Controls.Add(this.txtCustomerType);
            this.content.Controls.Add(this.lblCUSTOMER_TYPE);
            this.content.Controls.Add(this.lblCUSTOMER_CHANGE_AMPARE_HISTORY);
            this.content.Controls.Add(this.lblCUSTOMER_INFORMATION);
            this.content.Controls.Add(this.txtCustomerCode);
            this.content.Controls.Add(this.txtCustomerName);
            this.content.Controls.Add(this.lblCUSTOMER_NAME);
            this.content.Controls.Add(this.lblAREA);
            this.content.Controls.Add(this.lblCUSTOMER_CODE);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_CODE, 0);
            this.content.Controls.SetChildIndex(this.lblAREA, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_NAME, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerName, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerCode, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_INFORMATION, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_CHANGE_AMPARE_HISTORY, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_TYPE, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerType, 0);
            this.content.Controls.SetChildIndex(this.txtArea, 0);
            this.content.Controls.SetChildIndex(this.dgvHistory, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            // 
            // dgvHistory
            // 
            this.dgvHistory.AllowUserToAddRows = false;
            this.dgvHistory.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvHistory.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvHistory.BackgroundColor = System.Drawing.Color.White;
            this.dgvHistory.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvHistory.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHistory.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.OLD_AMPARE,
            this.NEW_AMPARE,
            this.CREATE_BY,
            this.CHANGE_DATE,
            this.NOTE});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvHistory.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvHistory.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgvHistory, "dgvHistory");
            this.dgvHistory.Name = "dgvHistory";
            this.dgvHistory.ReadOnly = true;
            this.dgvHistory.RowHeadersVisible = false;
            // 
            // txtCustomerType
            // 
            resources.ApplyResources(this.txtCustomerType, "txtCustomerType");
            this.txtCustomerType.Name = "txtCustomerType";
            this.txtCustomerType.ReadOnly = true;
            // 
            // lblCUSTOMER_TYPE
            // 
            resources.ApplyResources(this.lblCUSTOMER_TYPE, "lblCUSTOMER_TYPE");
            this.lblCUSTOMER_TYPE.Name = "lblCUSTOMER_TYPE";
            // 
            // lblCUSTOMER_CHANGE_AMPARE_HISTORY
            // 
            resources.ApplyResources(this.lblCUSTOMER_CHANGE_AMPARE_HISTORY, "lblCUSTOMER_CHANGE_AMPARE_HISTORY");
            this.lblCUSTOMER_CHANGE_AMPARE_HISTORY.Name = "lblCUSTOMER_CHANGE_AMPARE_HISTORY";
            // 
            // lblCUSTOMER_INFORMATION
            // 
            resources.ApplyResources(this.lblCUSTOMER_INFORMATION, "lblCUSTOMER_INFORMATION");
            this.lblCUSTOMER_INFORMATION.Name = "lblCUSTOMER_INFORMATION";
            // 
            // lblCUSTOMER_NAME
            // 
            resources.ApplyResources(this.lblCUSTOMER_NAME, "lblCUSTOMER_NAME");
            this.lblCUSTOMER_NAME.Name = "lblCUSTOMER_NAME";
            // 
            // lblAREA
            // 
            resources.ApplyResources(this.lblAREA, "lblAREA");
            this.lblAREA.Name = "lblAREA";
            // 
            // lblCUSTOMER_CODE
            // 
            resources.ApplyResources(this.lblCUSTOMER_CODE, "lblCUSTOMER_CODE");
            this.lblCUSTOMER_CODE.Name = "lblCUSTOMER_CODE";
            // 
            // txtCustomerCode
            // 
            resources.ApplyResources(this.txtCustomerCode, "txtCustomerCode");
            this.txtCustomerCode.Name = "txtCustomerCode";
            this.txtCustomerCode.ReadOnly = true;
            // 
            // txtCustomerName
            // 
            resources.ApplyResources(this.txtCustomerName, "txtCustomerName");
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.ReadOnly = true;
            // 
            // txtArea
            // 
            resources.ApplyResources(this.txtArea, "txtArea");
            this.txtArea.Name = "txtArea";
            this.txtArea.ReadOnly = true;
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // OLD_AMPARE
            // 
            this.OLD_AMPARE.DataPropertyName = "OLD_AMPARE";
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.OLD_AMPARE.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.OLD_AMPARE, "OLD_AMPARE");
            this.OLD_AMPARE.Name = "OLD_AMPARE";
            this.OLD_AMPARE.ReadOnly = true;
            // 
            // NEW_AMPARE
            // 
            this.NEW_AMPARE.DataPropertyName = "NEW_AMPARE";
            resources.ApplyResources(this.NEW_AMPARE, "NEW_AMPARE");
            this.NEW_AMPARE.Name = "NEW_AMPARE";
            this.NEW_AMPARE.ReadOnly = true;
            // 
            // CREATE_BY
            // 
            this.CREATE_BY.DataPropertyName = "CREATE_BY";
            resources.ApplyResources(this.CREATE_BY, "CREATE_BY");
            this.CREATE_BY.Name = "CREATE_BY";
            this.CREATE_BY.ReadOnly = true;
            // 
            // CHANGE_DATE
            // 
            this.CHANGE_DATE.DataPropertyName = "CHANGE_DATE";
            dataGridViewCellStyle3.Format = "dd-MM-yyyy";
            dataGridViewCellStyle3.NullValue = null;
            this.CHANGE_DATE.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.CHANGE_DATE, "CHANGE_DATE");
            this.CHANGE_DATE.Name = "CHANGE_DATE";
            this.CHANGE_DATE.ReadOnly = true;
            // 
            // NOTE
            // 
            this.NOTE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NOTE.DataPropertyName = "NOTE";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.NOTE.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.NOTE, "NOTE");
            this.NOTE.Name = "NOTE";
            this.NOTE.ReadOnly = true;
            // 
            // DialogCustomerChangeAmpareHistory
            // 
            resources.ApplyResources(this, "$this");
            this.Name = "DialogCustomerChangeAmpareHistory";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHistory)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataGridView dgvHistory;
        private TextBox txtCustomerType;
        private Label lblCUSTOMER_TYPE;
        private Label lblCUSTOMER_CHANGE_AMPARE_HISTORY;
        private Label lblCUSTOMER_INFORMATION;
        private Label lblCUSTOMER_NAME;
        private Label lblAREA;
        private Label lblCUSTOMER_CODE;
        private TextBox txtArea;
        private TextBox txtCustomerCode;
        private TextBox txtCustomerName;
        private ExButton btnCLOSE;
        private DataGridViewTextBoxColumn OLD_AMPARE;
        private DataGridViewTextBoxColumn NEW_AMPARE;
        private DataGridViewTextBoxColumn CREATE_BY;
        private DataGridViewTextBoxColumn CHANGE_DATE;
        private DataGridViewTextBoxColumn NOTE;
    }
}
