﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogCustomerAttachment : ExDialog
    {
        #region Private Data
        private byte[] file;
        private string deniedAttachment = ".exe;.ini;.config;.dll;.ttf;.pdb;.bak;.mdf;.ldf;.sql;.lnk;.bat;.json;.xml;.inf;.rll;.rpt;.jar;.py;.pyc;.pif;.msi;.com;.scr;.gadget";
        private OpenFileDialog openfileDiaglog = new OpenFileDialog();
        private GeneralProcess _flag;
        private TBL_CUSTOMER_ATTACHMENT _objNew = new TBL_CUSTOMER_ATTACHMENT();
        private TBL_CUSTOMER_ATTACHMENT _objOld = new TBL_CUSTOMER_ATTACHMENT();
        private TBL_CUSTOMER _objCustomer = new TBL_CUSTOMER();
        #endregion Private Data

        #region Constructor
        public DialogCustomerAttachment(GeneralProcess flag, TBL_CUSTOMER_ATTACHMENT objAttachment, TBL_CUSTOMER objCustomer)
        {
            InitializeComponent();
            this._flag = flag;
            objAttachment._CopyTo(this._objNew);
            objAttachment._CopyTo(this._objOld);
            objCustomer._CopyTo(this._objCustomer);
            if (flag == GeneralProcess.Insert)
            {
                this.Text = string.Concat(Resources.ADD, this.Text);
            }
            else if (flag == GeneralProcess.Update)
            {
                this.Text = string.Concat(Resources.UPDATE, this.Text);
            }
            else if (flag == GeneralProcess.Delete)
            {
                this.Text = string.Concat(Resources.DELETE, this.Text);
            }
            if (flag == GeneralProcess.Delete)
            {
                UIHelper.SetEnabled(this, false);
                _btnBrowse.Enabled = false;
            }
            //File Type Name
            UIHelper.SetDataSourceToComboBox(cboFILE_TYPE_NAME, DBDataContext.Db.TLKP_FILE_TYPEs.ToList());
            read();
        }
        #endregion Constructor

        #region Method
        private void read()
        {
            this.txtAttachmentName.Text = _objNew.ATTACHMENT_NAME;
            file = _objNew.FILE == null ? new byte[0] : _objNew.FILE.ToArray();
            this.txtFILE_NAME.Text = _objNew.FILE_NAME;
            this.dtpCreatedOn.Value = _objNew.CREATED_BY == null ? DateTime.Now : DateTime.Parse(_objNew.CREATED_ON.ToString());
            this.txtNote.Text = _objNew.NOTE;
            this.cboFILE_TYPE_NAME.SelectedValue = _objNew.FILE_TYPE_ID;
        }

        private void save()
        {
            this._objNew.CUSTOMER_ID = _objCustomer.CUSTOMER_ID;
            this._objNew.ATTACHMENT_NAME = txtAttachmentName.Text;
            if (_flag != GeneralProcess.Delete)
            {
                this._objNew.FILE = file;
            }
            this._objNew.FILE_NAME = txtFILE_NAME.Text;
            this._objNew.CREATED_ON = dtpCreatedOn.Value;
            this._objNew.NOTE = txtNote.Text.Trim();
            this._objNew.CREATED_BY = Login.CurrentLogin.LOGIN_NAME;
            this._objNew.FILE_TYPE_ID = (int)cboFILE_TYPE_NAME.SelectedValue;
        }

        private bool invalid()
        {
            bool blnReturn = false;
            this.ClearAllValidation();
            if (txtAttachmentName.Text == string.Empty)
            {
                txtAttachmentName.SetValidation(string.Format(Resources.REQUIRED, lblATTACHMENT_NAME.Text));
                return true;
            }
            if (txtFILE_NAME.Text == string.Empty)
            {
                //btnBrowse.SetValidation(string.Format("Resources.RequestChoose", Resources.Attachment));
                _btnBrowse.SetValidation(string.Format(Resources.REQUIRED, lblFILE_LOCATION.Text));
                return true;
            }
            if (cboFILE_TYPE_NAME.SelectedIndex == -1)
            {
                this.cboFILE_TYPE_NAME.SetValidation(string.Format(Resources.REQUIRED, lblFILE_NAME_TYPE.Text));
                return true;
            }
            return blnReturn;
        }

        #endregion Method

        #region Event

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {

            if (invalid())
            {
                return;
            }
            save();
            //if (DBDataContext.Db.IsExits(_objNew, "ATTACHMENT_NAME"))
            //{
            //    txtAttachmentName.SetValidation(string.Format(Resources.MS_IS_EXISTS, lblATTACHMENT_NAME.Text));
            //    return;
            //}
            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objNew);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objNew);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            _btnBrowse.ClearValidation();
            openfileDiaglog.Filter = Properties.Resources.FILTER_ATTACHMENT;
            if (openfileDiaglog.ShowDialog() == DialogResult.OK)
            {
                var size = new FileInfo(openfileDiaglog.FileName);
                foreach (var dined in deniedAttachment.Split(';').Select(x => x.ToString()))
                {
                    if (dined.Equals(Path.GetExtension(openfileDiaglog.FileName).ToLower()))
                    {
                        txtFILE_NAME.Text = "";
                        MsgBox.ShowWarning(string.Format(Properties.Resources.WARNING_WRONG_SELECTEDFILE, dined), Properties.Resources.WARNING);
                        return;
                    }
                }
                if (size.Length > 26214400) // limit size <25MB
                {
                    txtFILE_NAME.Text = "";
                    MsgBox.ShowWarning(Resources.WARNING_FILESIZE_TOOLARGE, Resources.WARNING);
                    return;
                }
                if (string.IsNullOrEmpty(txtAttachmentName.Text))
                {
                    txtAttachmentName.Text = Path.GetFileName(openfileDiaglog.FileName);

                }
                txtFILE_NAME.Text = Path.GetFileName(openfileDiaglog.FileName);
                file = File.ReadAllBytes(openfileDiaglog.FileName);
            }
        }

        private void txtAttachmentName_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void txtNote_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        private void btnChangeLog_Click(object sender, EventArgs e)
        {
            SoftTech.Security.Interface.DialogChangeLog.ShowChangeLog(this._objNew);
        }
        #endregion Event 
    }
}
