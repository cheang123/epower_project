﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageAmpere : Form
    {
        public TBL_AMPARE Ampare
        {
            get
            {
                TBL_AMPARE objAmpare = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    try
                    {
                        int ampareID = (int)dgv.SelectedRows[0].Cells[AMPARE_ID.Name].Value;
                        objAmpare = DBDataContext.Db.TBL_AMPAREs.FirstOrDefault(x => x.AMPARE_ID == ampareID);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objAmpare;
            }
        }

        #region Constructor
        public PageAmpere()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            txt_QuickSearch(null, null);
        }
        #endregion

        #region Operation
        /// <summary>
        /// Add new ampare.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNew_Click(object sender, EventArgs e)
        {
            DialogAmpare dig = new DialogAmpare(GeneralProcess.Insert, new TBL_AMPARE() { AMPARE_NAME = "" });
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.Ampare.AMPARE_ID);
            }
        }

        /// <summary>
        /// Edit ampare
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                DialogAmpare dig = new DialogAmpare(GeneralProcess.Update, Ampare);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.Ampare.AMPARE_ID);
                }
            }
        }

        /// <summary>
        /// Remove ampare
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exButton1_Click(object sender, EventArgs e)
        {
            if (IsDeletable())
            {
                DialogAmpare dig = new DialogAmpare(GeneralProcess.Delete, Ampare);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.Ampare.AMPARE_ID - 1);
                }
            }
        }

        /// <summary>
        /// Load data from database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            try
            {
                dgv.DataSource = (from ap in DBDataContext.Db.TBL_AMPAREs
                                 where ap.IS_ACTIVE &&
                                 ap.AMPARE_NAME.ToLower().Contains(txtQuickSearch.Text.ToLower().Trim())
                                 select ap)
                                 .ToList()
                                 .OrderBy(x=>Lookup.ParseAmpare(x.AMPARE_NAME))
                                 .ToList();
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
            
        }
        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }
        #endregion

        #region Method
        private bool IsDeletable()
        {
            //TODO: can delete record.
            bool val = false;
            if (dgv.SelectedRows.Count > 0)
            {
                val = true;
                if (DBDataContext.Db.TBL_METER_TYPEs.Where(x=>x.METER_AMP_ID==Ampare.AMPARE_ID && x.IS_ACTIVE).Count()>0||
                    DBDataContext.Db.TBL_CIRCUIT_BREAKER_TYPEs.Where(x=>x.BREAKER_AMP_ID==Ampare.AMPARE_ID &&x.IS_ACTIVE).Count()>0||
                    DBDataContext.Db.TBL_CUSTOMERs.Where(x => x.AMP_ID == Ampare.AMPARE_ID && x.STATUS_ID != (int)CustomerStatus.Cancelled).Count() > 0)
                {
                    MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE);
                    val = false;
                }
            }
            return val;
        }
        #endregion

        
    }
}