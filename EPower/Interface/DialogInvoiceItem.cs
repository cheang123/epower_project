﻿using EPower.Base.Helper;
using EPower.Base.Logic;
using EPower.Properties;
using HB01.Domain.Enums;
using HB01.Domain.ListModels;
using HB01.Helpers.DevExpressCustomize;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogInvoiceItem : ExDialog
    {
        GeneralProcess _flag;
        TBL_INVOICE_ITEM _objNew = new TBL_INVOICE_ITEM();
        TBL_INVOICE_ITEM _objOld = new TBL_INVOICE_ITEM();

        public TBL_INVOICE_ITEM InvioceItem
        {
            get { return _objNew; }
        }

        #region Constructor
        public DialogInvoiceItem(GeneralProcess flag, TBL_INVOICE_ITEM objItem)
        {
            InitializeComponent();
            _flag = flag;

            dataLookup();
            objItem._CopyTo(_objNew);
            objItem._CopyTo(_objOld);

            if (flag == GeneralProcess.Insert)
            {
                this.Text = string.Concat(Resources.INSERT, this.Text);
                _objNew.IS_ACTIVE = true;
                _objNew.RECURRING_MONTH = 1;

            }
            else if (flag == GeneralProcess.Update)
            {
                this.Text = string.Concat(Resources.UPDATE, this.Text);
            }
            else if (flag == GeneralProcess.Delete)
            {
                this.Text = string.Concat(Resources.DELETE, this.Text);
                UIHelper.SetEnabled(this, false);
            }
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;

            this.chkIS_RECURRING_SERVICE.Visible =
                this.lblRECURRING_MONTH.Visible =
                this.txtRecuringMonth.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_RECURRING_SERVICE]);

            this.lblAR_KHR.Visible = this.cboAR_KHR.Visible = this._lblREQ_AR_KHR.Visible = this.cboAR_KHR.Enabled = SettingLogic.Currencies.Any(x => x.CURRENCY_ID == (int)Currency.KHR);
            this.lblAR_USD.Visible = this.cboAR_USD.Visible = this._lblREQ_AR_USD.Visible = this.cboAR_USD.Enabled = SettingLogic.Currencies.Any(x => x.CURRENCY_ID == (int)Currency.USD);
            this.lblAR_THB.Visible = this.cboAR_THB.Visible = this._lblREQ_AR_THB.Visible = this.cboAR_THB.Enabled = SettingLogic.Currencies.Any(x => x.CURRENCY_ID == (int)Currency.THB);
            this.lblAR_VND.Visible = this.cboAR_VND.Visible = this._lblREQ_AR_VND.Visible = this.cboAR_VND.Enabled = SettingLogic.Currencies.Any(x => x.CURRENCY_ID == (int)Currency.VND);
            read();
        }

        #endregion

        #region Opeartion
        /// <summary>
        /// Data transaction.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
            txtInvoiceItemName.ClearValidation();
            if (DBDataContext.Db.IsExits(_objNew, "INVOICE_ITEM_NAME"))
            {
                txtInvoiceItemName.SetValidation(string.Format(Resources.MS_IS_EXISTS, lblSERVICE.Text));
                return;
            }
            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objNew);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objNew);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }

        private void chkIS_RECURRING_SERVICE_CheckedChanged(object sender, EventArgs e)
        {
            this.txtRecuringMonth.Visible = this.chkIS_RECURRING_SERVICE.Checked;
            this.lblRECURRING_MONTH.Visible = this.chkIS_RECURRING_SERVICE.Checked;
            if (this.chkIS_RECURRING_SERVICE.Checked)
            {
                if (DataHelper.ParseToInt(this.txtRecuringMonth.Text) == 0)
                {
                    this.txtRecuringMonth.Text = "1";
                }
            }
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        /// <summary>
        /// Change current keyboard layout to khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        /// <summary>
        /// Input number Decimal only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNumber(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        }
        #endregion

        #region Method
        private void dataLookup()
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                //Get invioce item type.
                UIHelper.SetDataSourceToComboBox(cboInviceItemType,
                    from it in DBDataContext.Db.TBL_INVOICE_ITEM_TYPEs
                    where it.IS_ACTIVE
                    select new
                    {
                        it.INVOICE_ITEM_TYPE_ID,
                        it.INVOICE_ITEM_TYPE_NAME
                    });

                //Get currency
                UIHelper.SetDataSourceToComboBox(cboCurrency, DBDataContext.Db.TLKP_CURRENCies.Select(x => new { x.CURRENCY_ID, x.CURRENCY_NAME }));

                //Get account
                //UIHelper.SetDataSourceToComboBox(cboIncomeAccount, AccountChartHelper.GetAccounts(AccountConfig.INVOICE_ITEM_INCOME_ACCOUNTS));
                //UIHelper.SetDataSourceToComboBox(cboARAccount, AccountChartHelper.GetAccounts(AccountConfig.RECEIVABLE_ACCOUNT));
                var icAcc = AccountChartHelper.GetAccounts(new AccountSearchParam { Natures = new List<AccountNatures> { AccountNatures.Income } });
                var arAcc = AccountChartHelper.GetAccounts(new AccountSearchParam { Natures = new List<AccountNatures> { AccountNatures.Asset } });
                this.cboAccountIncome.SetDatasourceList(icAcc, nameof(AccountListModel.DisplayAccountName));
                this.cboAR_KHR.SetDatasourceList(arAcc, nameof(AccountListModel.DisplayAccountName));
                this.cboAR_USD.SetDatasourceList(arAcc, nameof(AccountListModel.DisplayAccountName));
                this.cboAR_THB.SetDatasourceList(arAcc, nameof(AccountListModel.DisplayAccountName));
                this.cboAR_VND.SetDatasourceList(arAcc, nameof(AccountListModel.DisplayAccountName));
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
            this.Cursor = Cursors.Default;
        }

        private void read()
        {
            txtInvoiceItemName.Text = _objNew.INVOICE_ITEM_NAME;
            cboInviceItemType.SelectedValue = _objNew.INVOICE_ITEM_TYPE_ID;
            cboCurrency.SelectedValue = _objNew.CURRENCY_ID;
            txtPrice.Text = UIHelper.FormatCurrency(_objNew.PRICE, _objNew.CURRENCY_ID);
            chkIS_EDITABLE.Checked = _objNew.IS_EDITABLE;
            chkIS_RECURRING_SERVICE.Checked = _objNew.IS_RECURRING_SERVICE;
            txtRecuringMonth.Text = _objNew.RECURRING_MONTH.ToString("0");
            cboAccountIncome.EditValue = _objNew.INCOME_ACCOUNT_ID;
            cboAR_KHR.EditValue = _objNew.AR_KHR;
            cboAR_USD.EditValue = _objNew.AR_USD;
            cboAR_THB.EditValue = _objNew.AR_THB;
            cboAR_VND.EditValue = _objNew.AR_VND;
        }

        private void write()
        {
            _objNew.INVOICE_ITEM_NAME = txtInvoiceItemName.Text.Trim();
            _objNew.INVOICE_ITEM_TYPE_ID = (int)cboInviceItemType.SelectedValue;
            _objNew.CURRENCY_ID = (int)cboCurrency.SelectedValue;
            _objNew.PRICE = DataHelper.ParseToDecimal(txtPrice.Text);
            _objNew.IS_EDITABLE = chkIS_EDITABLE.Checked;
            _objNew.IS_RECURRING_SERVICE = chkIS_RECURRING_SERVICE.Checked;
            _objNew.RECURRING_MONTH = this.chkIS_RECURRING_SERVICE.Checked ? DataHelper.ParseToInt(txtRecuringMonth.Text) : 0;
            _objNew.INCOME_ACCOUNT_ID = (int)cboAccountIncome.EditValue;
            _objNew.AR_KHR = (int)cboAR_KHR.EditValue;
            _objNew.AR_USD = (int)cboAR_USD.EditValue;
            _objNew.AR_THB = (int)cboAR_THB.EditValue;
            _objNew.AR_VND = (int)cboAR_VND.EditValue;
        }

        TabPage selectedPage = null;
        private void selectTab(TabPage page)
        {
            if (selectedPage == null)
            {
                this.selectedPage = page;
                this.tabControl.SelectedTab = page;
            }
        }
        private bool inValid()
        {
            bool val = false;
            this.selectedPage = null;
            this.ClearAllValidation();

            //Accounting
            if ((int)cboAccountIncome.EditValue == 0 && _flag != GeneralProcess.Delete)
            {
                this.selectTab(tabACCOUNTING);
                cboAccountIncome.SetValidation(string.Format(Resources.REQUIRED_SELECT_VALUE, lblINCOME_ACCOUNT.Text));
                val = true;
            }
            if (PointerLogic.isConnectedPointer)
            {
                if (this.cboAR_KHR.Enabled && (int)this.cboAR_KHR.EditValue == 0 && _flag != GeneralProcess.Delete)
                {
                    this.selectTab(tabACCOUNTING);
                    cboAR_KHR.SetValidation(string.Format(Resources.REQUIRED_SELECT_VALUE, lblAR_KHR.Text));
                    val = true;
                }
                if (this.cboAR_USD.Enabled && (int)this.cboAR_USD.EditValue == 0 && _flag != GeneralProcess.Delete)
                {
                    this.selectTab(tabACCOUNTING);
                    cboAR_USD.SetValidation(string.Format(Resources.REQUIRED_SELECT_VALUE, lblAR_USD.Text));
                    val = true;
                }
                if (this.cboAR_THB.Enabled && (int)this.cboAR_THB.EditValue == 0 && _flag != GeneralProcess.Delete)
                {
                    this.selectTab(tabACCOUNTING);
                    cboAR_THB.SetValidation(string.Format(Resources.REQUIRED_SELECT_VALUE, lblAR_THB.Text));
                    val = true;
                }
                if (this.cboAR_VND.Enabled && (int)this.cboAR_VND.EditValue == 0 && _flag != GeneralProcess.Delete)
                {
                    this.selectTab(tabACCOUNTING);
                    cboAR_VND.SetValidation(string.Format(Resources.REQUIRED_SELECT_VALUE, lblAR_VND.Text));
                    val = true;
                }
            }

            //General
            if (txtInvoiceItemName.Text.Trim() == string.Empty)
            {
                this.tabGENERAL.Select();
                txtInvoiceItemName.SetValidation(string.Format(Resources.REQUIRED, lblSERVICE.Text));
                val = true;
            }
            if (cboInviceItemType.SelectedIndex == -1)
            {
                this.tabGENERAL.Select();
                cboInviceItemType.SetValidation(string.Format(Resources.REQUIRED_SELECT_VALUE, lblSERVICE_TYPE.Text));
                val = true;
            }
            if (cboCurrency.SelectedIndex == -1)
            {
                this.tabGENERAL.Select();
                cboCurrency.SetValidation(string.Format(Resources.REQUIRED_SELECT_VALUE, lblCURRENCY.Text));
                val = false;
            }
            if (!DataHelper.IsNumber(txtPrice.Text) && _flag != GeneralProcess.Delete)
            {
                this.tabGENERAL.Select();
                txtPrice.SetValidation(Resources.REQUIRED_INPUT_NUMBER);
                val = true;
            }
            if (this.chkIS_RECURRING_SERVICE.Checked)
            {
                if (this.txtRecuringMonth.Text.Trim() == string.Empty)
                {
                    this.tabGENERAL.Select();
                    txtRecuringMonth.SetValidation(string.Format(Resources.REQUIRED, ""));
                    val = true;
                }
            }

            return val;
        }
        #endregion

    }
}
