﻿using EPower.Base.Helper;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Logic;
using System;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageStaffExpense : Form
    { 
        public TBL_STAFF_EXPENSE StaffExpense
        {
            get
            {
                TBL_STAFF_EXPENSE objExpense = null;
                if (dgvStaffExpense.SelectedRows.Count > 0)
                {
                    int expenseId = (int)dgvStaffExpense.SelectedRows[0].Cells[EXPENSE_ID.Name].Value;
                    try
                    {
                        objExpense = DBDataContext.Db.TBL_STAFF_EXPENSEs.FirstOrDefault(x => x.EXPENSE_ID == expenseId);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objExpense;
            }
        }

        #region Constructor
        public PageStaffExpense()
        {
            InitializeComponent();
            txt_QuickSearch(null, null);
            UIHelper.DataGridViewProperties(dgvStaffExpense);
        }
        #endregion

        #region Operation
        /// <summary>
        /// Load data from database.
        /// </summary>
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            try
            {
                dgvStaffExpense.DataSource = from s in DBDataContext.Db.TBL_STAFF_EXPENSEs
                                             join c in DBDataContext.Db.TLKP_CURRENCies on s.CURRENCY_ID equals c.CURRENCY_ID
                                             join a in DBDataContext.Db.TBL_ACCOUNT_CHARTs on s.PAYMENT_ACCOUNT_ID equals a.ACCOUNT_ID
                                             where s.EXPENSE_DATE.Year == dtpYear.Value.Year
                                                    && s.IS_ACTIVE
                                            orderby s.EXPENSE_DATE
                                             select new
                                             {
                                                 s.EXPENSE_ID,
                                                 s.EXPENSE_DATE,
                                                 s.CREATE_BY,
                                                 s.CREATE_ON,
                                                 a.ACCOUNT_NAME,
                                                 s.TOTAL_AMOUNT,
                                                 c.CURRENCY_SING
                                             }; 

            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
            
        }

        

        /// <summary>
        /// Add new area.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNew_Click(object sender, EventArgs e)
        {
            TBL_STAFF_EXPENSE _objStaff=DBDataContext.Db.TBL_STAFF_EXPENSEs.OrderByDescending(x=>x.EXPENSE_DATE).FirstOrDefault(x=>x.IS_ACTIVE);

            TBL_STAFF_EXPENSE objStaffExpense = new TBL_STAFF_EXPENSE()
            {
                CURRENCY_ID = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.IS_DEFAULT_CURRENCY).CURRENCY_ID,
                PAYMENT_ACCOUNT_ID = AccountChartHelper.GetAccounts(AccountConfig.PAYMENT_DEFAULT_ACCOUNTS).FirstOrDefault().ACCOUNT_ID,
                EXPENSE_DATE = _objStaff == null ? DBDataContext.Db.GetSystemDate() : _objStaff.EXPENSE_DATE.AddMonths(1),
                CREATE_BY=Login.CurrentLogin.LOGIN_NAME
            };
            DialogStaffExpense dig = new DialogStaffExpense(GeneralProcess.Insert, objStaffExpense);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgvStaffExpense, dig.StaffExpense.EXPENSE_ID);
            }
        }

        /// <summary>
        /// Edit area.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgvStaffExpense.SelectedRows.Count > 0)
            {
                DialogStaffExpense dig = new DialogStaffExpense(GeneralProcess.Update, StaffExpense);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgvStaffExpense, dig.StaffExpense.EXPENSE_ID);
                }
            }
        }

        /// <summary>
        /// Remove area.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (dgvStaffExpense.Rows.Count > 0)
            {
                DialogStaffExpense dig = new DialogStaffExpense(GeneralProcess.Delete, StaffExpense);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgvStaffExpense, dig.StaffExpense.EXPENSE_ID - 1);
                }
            }
            
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        private void dgvStaffExpense_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }
        #endregion

        #region Method               
         
        #endregion 
    }
}
