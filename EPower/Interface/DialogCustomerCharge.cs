﻿using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using EPower.Base.Helper;
using EPower.Base.Helper.DevExpressCustomize;
using EPower.Base.Logic;
using EPower.Base.Properties;
using EPower.Logic;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogCustomerCharge : ExDialog
    {
        #region Data
        private TBL_CUSTOMER _objCustomer = new TBL_CUSTOMER();
        private TBL_INVOICE _oldObj;
        private TBL_INVOICE _newObj;
        private TBL_EXCHANGE_RATE exchangeRate = new TBL_EXCHANGE_RATE();
        private List<TBL_INVOICE_ITEM> invoiceItems = new List<TBL_INVOICE_ITEM>();
        private List<TBL_TAX> taxes = new List<TBL_TAX>();
        private BindingList<InviceItemList> lines;
        private BindingList<InviceItemList> _oldLines = new BindingList<InviceItemList>();

        Base.Logic.Currency mCurrency = null;
        int? _accountId = 0;
        TBL_PAYMENT_METHOD payMethod = new TBL_PAYMENT_METHOD();
        GeneralProcess _flag;
        #endregion Data

        #region Constructor
        public DialogCustomerCharge(TBL_CUSTOMER objCus, GeneralProcess flag, TBL_INVOICE objInv = null)
        {
            InitializeComponent();
            _flag = flag;
            objCus.CopyTo(_objCustomer);
            if (objInv != null)
            {
                _oldObj = new TBL_INVOICE();
                _newObj = new TBL_INVOICE();
                objInv.CopyTo(_oldObj);
                objInv.CopyTo(_newObj);
            }

            this.txtCustomer.Text = objCus.CUSTOMER_CODE + "-" + objCus.LAST_NAME_KH + " " + objCus.FIRST_NAME_KH;
            mCurrency = SettingLogic.Currencies.FirstOrDefault(x => x.IS_DEFAULT_CURRENCY);
            dtpDueDate.FormatSortDate();
            dtpInvoiceDate.FormatSortDate();

            bind();
            load();

            //Events
            this.btnOK.Click += btnOk_Click;
            this.btnCLOSE.Click += btnClose_Click;
            this.cboCurrency.EditValueChanged += cboCurrency_EditValueChanged;
            this.dtpInvoiceDate.EditValueChanged += dtpInvoiceDate_EditValueChanged;
            this.dgvServiceList.RowCellClick += dgvServiceList_RowCellClick;
            this.dgvServiceList.CellValueChanged += dgvServiceList_CellValueChanged;
            this.repInvoiceItem.Closed += repInvoiceItem_Closed;

            this.KeyDown += DialogCustomerCharge_KeyDown; ;

            this.cboCurrency.EditValue = mCurrency.CURRENCY_ID;
            this.ActiveControl = cboCurrency;
        }


        #endregion Constructor

        #region Method

        private void bind()
        {
            //currency
            this.cboCurrency.SetDataSourceToComboBox(SettingLogic.Currencies, "CURRENCY_ID", "CURRENCY_NAME");

            // Payment method 
            var paymentMethods = DBDataContext.Db.TBL_PAYMENT_METHODs.Where(x => x.IS_ACTIVE).ToList();
            this.cboPaymentMethod.SetDataSourceToComboBox(paymentMethods, nameof(TBL_PAYMENT_METHOD.PAYMENT_METHOD_ID), nameof(TBL_PAYMENT_METHOD.PAYMENT_METHOD_NAME));
            this.cboPaymentMethod.EditValue = paymentMethods.FirstOrDefault().PAYMENT_METHOD_ID;

            //Invoice Item
            invoiceItems = DBDataContext.Db.TBL_INVOICE_ITEMs.Where(x => x.IS_ACTIVE && x.INVOICE_ITEM_TYPE_ID != 0).ToList();
            repInvoiceItem.DataSource = invoiceItems;
            foreach (LookUpColumnInfo item in repInvoiceItem.Columns)
            {
                item.Caption = ResourceHelper.Translate(item.Caption);
            }

            //Tax
            taxes = DBDataContext.Db.TBL_TAXes.Where(x => x.IS_ACTIVE).ToList();
            repTax.DataSource = taxes;
            foreach (LookUpColumnInfo item in repTax.Columns)
            {
                item.Caption = ResourceHelper.Translate(item.Caption);
            }
        }

        private void load()
        {
            List<InviceItemList> objDetails = new List<InviceItemList>();
            //List<TBL_INVOICE_ADJUSTMENT> objAdjs = new List<TBL_INVOICE_ADJUSTMENT>();
            if (_oldObj != null)
            {
                mCurrency = SettingLogic.Currencies.FirstOrDefault(x => x.CURRENCY_ID == _oldObj.CURRENCY_ID);
                dtpInvoiceDate.EditValue = _oldObj.INVOICE_DATE;
                dtpDueDate.EditValue = _oldObj.DUE_DATE;
                objDetails = (from d in DBDataContext.Db.TBL_INVOICE_DETAILs
                              where d.INVOICE_ID == _oldObj.INVOICE_ID && d.INVOICE_ITEM_ID != (int)InvoiceItem.Adjustment
                              select new InviceItemList
                              {
                                  INVOICE_DETAIL_ID = d.INVOICE_DETAIL_ID,
                                  INVOICE_ID = d.INVOICE_ID,
                                  INVOICE_ITEM_ID = d.INVOICE_ITEM_ID,
                                  AMOUNT = d.AMOUNT,
                                  TAX_AMOUNT = d.TAX_AMOUNT,
                                  TAX_ID = d.TAX_ID,
                                  PRICE = d.PRICE,
                                  CHARGE_DESCRIPTION = d.CHARGE_DESCRIPTION,
                                  EXCHANGE_RATE = d.EXCHANGE_RATE,
                                  EXCHANGE_RATE_DATE = d.EXCHANGE_RATE_DATE
                              }).ToList();
                var objTaxs = DBDataContext.Db.TBL_INVOICE_TAXes.Where(x => objDetails.Select(i => i.INVOICE_DETAIL_ID).Contains(x.INVOICE_DETAIL_ID) && x.IS_ACTIVE).ToList();
                var objAdjs = DBDataContext.Db.TBL_INVOICE_ADJUSTMENTs.Where(x => x.INVOICE_ID == _oldObj.INVOICE_ID).ToList();
                if (objAdjs != null)
                {
                    foreach (var item in objDetails)
                    {
                        //var tax = taxes.FirstOrDefault(x=>x.TAX_ID == item.TAX_ID)
                        var taxId = objTaxs.FirstOrDefault(x => x.INVOICE_DETAIL_ID == item.INVOICE_DETAIL_ID)?.TAX_ID;

                        item.TAX_ID = taxId ?? 0;

                        var adjustedAmount = objAdjs.Where(x => x.INVOICE_ITEM_ID == item.INVOICE_ITEM_ID).Sum(x => x.ADJUST_AMOUNT);
                        var adjustedTaxAmount = objAdjs.Where(x => x.INVOICE_ITEM_ID == item.INVOICE_ITEM_ID).Sum(x => x.TAX_ADJUST_AMOUNT);
                        item.AMOUNT = item.AMOUNT + adjustedAmount;
                        item.TAX_AMOUNT = item.TAX_AMOUNT + adjustedTaxAmount;
                        item.PRICE = item.AMOUNT - item.TAX_AMOUNT;
                        if (item.TAX_ID != 0 && taxes.FirstOrDefault(x => x.TAX_ID == taxId).COMPUTATION == (int)TaxComputations.PERCENTAGE_INCLUDED_TAX)
                        {
                            item.PRICE = item.AMOUNT;
                        }
                    }
                }
            }
            lines = new BindingList<InviceItemList>(objDetails);
            if (_flag == GeneralProcess.Update)
            {
                txtExchangeRate.Text = objDetails.FirstOrDefault().EXCHANGE_RATE.ToString(UIHelper._DefaultPriceFormat);
            }

            objDetails.ForEach(x =>
            {
                var copiedLine = new InviceItemList();
                Copier.CopyTo<InviceItemList>(x, copiedLine);
                _oldLines.Add(copiedLine);
            });

            lines.AllowNew = true;
            lines.AllowEdit = true;
            lines.AllowRemove = true;
            dgv.DataSource = lines;
            calculateAmount();

            if (_flag == GeneralProcess.Insert)
            {
                this._lblNote.Text = Resources.DESCRIPTION;
                this.dtpInvoiceDate.EditValue = DateTime.Now;
                dtpInvoiceDate_EditValueChanged(null, null);
            }
            else if (_flag == GeneralProcess.Update)
            {
                this.dgvServiceList.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.None;
                this.colService.OptionsColumn.AllowEdit = false;
                this.colTax.OptionsColumn.AllowEdit = false;
                this.cboCurrency.Enabled = false;
                this.dtpInvoiceDate.Enabled = false;
                this.txtExchangeRate.Enabled = false;
                this.chkIS_PAID.Visible = this.chkPRINT.Visible = false;
                this._lblNote.Text = Resources.ADJUST_REASON;
            }
            else if (_flag == GeneralProcess.Delete)
            {
                UIHelper.SetEnabled(this, false);
                this.cboCurrency.Enabled = false;
                this.cboPaymentMethod.Enabled = false;
                this.colService.OptionsColumn.AllowEdit = false;
                this.chkIS_PAID.Visible = this.chkPRINT.Visible = false;
                this.dgvServiceList.AllowEdit(false);
                this.dgvServiceList.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.None;

                this.txtNote.Enabled = true;
                this._lblNote.Text = Resources.DELETE_REASON;
            }
        }

        private bool invalid()
        {
            bool blnReturn = false;
            this.ClearAllValidation();

            if (lines == null || lines.Count == 0)
            {
                MsgBox.ShowInformation(String.Format(Resources.REQUIRED, Resources.TABLE));
                blnReturn = true;
            }

            //if (lines.Sum(x => x.AMOUNT) == 0 || ((int)cboCurrency.EditValue == (int)Currency.KHR && lines.Sum(x => x.AMOUNT) < 100))
            //{
            //    MsgBox.ShowInformation(String.Format(Resources.REQUIRED, Resources.TABLE));
            //    blnReturn = true;
            //}
            //else
            //{
            foreach (var line in lines)
            {
                if (line.INVOICE_ITEM_ID == 0)
                {
                    MsgBox.ShowInformation(String.Format(Resources.REQUIRED, Resources.TABLE));
                    blnReturn = true;
                    break;
                }
            }
            //}

            if (string.IsNullOrEmpty(txtNote.Text.Trim()))
            {
                this.txtNote.SetValidation(string.Format(Resources.REQUIRED, _lblNote.Text));
                blnReturn = true;
            }

            if (chkIS_PAID.Checked)
            {
                payMethod = (TBL_PAYMENT_METHOD)cboPaymentMethod.GetSelectedDataRow();
                if (payMethod == null)
                {
                    this.cboPaymentMethod.SetValidation(string.Format(Resources.REQUIRED, lblPAYMENT_METHOD.Text));
                    blnReturn = true;
                }
                _accountId = mCurrency.CURRENCY_ID == (int)Currency.KHR ? payMethod?.ACCOUNT_ID_KHR : mCurrency.CURRENCY_ID == (int)Currency.USD ? payMethod?.ACCOUNT_ID_USD :
                           mCurrency.CURRENCY_ID == (int)Currency.THB ? payMethod?.ACCOUNT_ID_THB : mCurrency.CURRENCY_ID == (int)Currency.VND ? payMethod?.ACCOUNT_ID_VND : 0;
                if (_accountId <= 0)
                {
                    if (MsgBox.ShowQuestion(String.Format(Resources.MSG_PAYMENT_METHOD_NOT_SET_ACCOUNT, cboCurrency.Text), Resources.INFORMATION) == DialogResult.Yes)
                    {
                        new DialogPaymentMethod(payMethod, GeneralProcess.Update).ShowDialog();
                    }
                    blnReturn = true;
                }
            }

            return blnReturn;
        }

        private TBL_INVOICE CreateNewInvoice()
        {
            //

            //get lastest service invoice
            var lstInvoice = DBDataContext.Db.TBL_INVOICEs.OrderByDescending(x => x.INVOICE_ID).FirstOrDefault(x => x.CUSTOMER_ID == _objCustomer.CUSTOMER_ID && x.CURRENCY_ID == mCurrency.CURRENCY_ID && x.IS_SERVICE_BILL);
            decimal forwardAmount = lstInvoice == null ? 0 : lstInvoice.TOTAL_AMOUNT - lstInvoice.SETTLE_AMOUNT;
            TBL_INVOICE objInv = Method.NewInvoice(true, txtNote.Text, _objCustomer.CUSTOMER_ID);

            objInv.CURRENCY_ID = mCurrency.CURRENCY_ID;
            objInv.CUSTOMER_ID = _objCustomer.CUSTOMER_ID;
            objInv.DUE_DATE = dtpDueDate.DateTime;
            objInv.INVOICE_DATE = dtpInvoiceDate.DateTime;
            objInv.INVOICE_MONTH = new DateTime(dtpInvoiceDate.DateTime.Year, dtpInvoiceDate.DateTime.Month, 1);
            objInv.METER_CODE = "";
            objInv.START_DATE = dtpInvoiceDate.DateTime;
            objInv.END_DATE = dtpInvoiceDate.DateTime;
            objInv.START_PAY_DATE = dtpInvoiceDate.DateTime;
            objInv.PRICE_ID = 0;
            objInv.BASED_PRICE = 0;
            objInv.PRICE = 0;
            objInv.FORWARD_AMOUNT = forwardAmount;
            objInv.TOTAL_AMOUNT = (decimal)txtTotalAmount.EditValue + forwardAmount;
            objInv.SETTLE_AMOUNT = UIHelper.Round(((decimal)txtTotalAmount.EditValue + forwardAmount), mCurrency.CURRENCY_ID);
            objInv.INVOICE_STATUS = objInv.SETTLE_AMOUNT == 0 ? (int)InvoiceStatus.Close : (int)InvoiceStatus.Open;
            DBDataContext.Db.TBL_INVOICEs.InsertOnSubmit(objInv);
            DBDataContext.Db.SubmitChanges();

            foreach (var line in lines)
            {
                TBL_INVOICE_DETAIL obj = new TBL_INVOICE_DETAIL()
                {
                    AMOUNT = line.AMOUNT,
                    END_USAGE = 0,
                    INVOICE_ID = objInv.INVOICE_ID,
                    INVOICE_ITEM_ID = line.INVOICE_ITEM_ID,
                    PRICE = line.PRICE,
                    START_USAGE = 0,
                    USAGE = 1,
                    CHARGE_DESCRIPTION = line.CHARGE_DESCRIPTION,
                    REF_NO = objInv.INVOICE_NO,
                    TRAN_DATE = objInv.INVOICE_DATE,
                    EXCHANGE_RATE = exchangeRate.EXCHANGE_RATE,
                    EXCHANGE_RATE_DATE = exchangeRate.CREATE_ON,
                    TAX_AMOUNT = line.TAX_AMOUNT,
                    TAX_ID = line.TAX_ID
                };
                DBDataContext.Db.TBL_INVOICE_DETAILs.InsertOnSubmit(obj);
                DBDataContext.Db.SubmitChanges();

                if (line.TAX_ID != 0)
                {
                    TBL_INVOICE_TAX objInvTax = new TBL_INVOICE_TAX
                    {
                        INVOICE_DETAIL_ID = obj.INVOICE_DETAIL_ID,
                        TAX_ID = line.TAX_ID,
                        TAX_AMOUNT = line.TAX_AMOUNT,
                        TAX_TITLE = taxes.FirstOrDefault(x => x.TAX_ID == line.TAX_ID).TAX_NAME,
                        ROW_DATE = obj.TRAN_DATE,
                        IS_ACTIVE = true
                    };
                    DBDataContext.Db.TBL_INVOICE_TAXes.InsertOnSubmit(objInvTax);
                    DBDataContext.Db.SubmitChanges();
                }
            }
            return objInv;
        }

        private TBL_PAYMENT CreatePayment(TBL_INVOICE objInvoice)
        {
            TBL_PAYMENT objPayment = new TBL_PAYMENT()
            {
                CUSTOMER_ID = _objCustomer.CUSTOMER_ID,
                CURRENCY_ID = objInvoice.CURRENCY_ID,
                CREATE_ON = DBDataContext.Db.GetSystemDate(),
                CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                PAY_DATE = DBDataContext.Db.GetSystemDate(),
                PAYMENT_NO = "",
                USER_CASH_DRAWER_ID = Login.CurrentCashDrawer.USER_CASH_DRAWER_ID,
                PAY_AMOUNT = objInvoice.SETTLE_AMOUNT,
                DUE_AMOUNT = objInvoice.SETTLE_AMOUNT,
                PAYMENT_ACCOUNT_ID = _accountId ?? 0,
                EXCHANGE_RATE = exchangeRate.EXCHANGE_RATE,
                EXCHANGE_RATE_DATE = exchangeRate.CREATE_ON,
                IS_ACTIVE = true,
                PAYMENT_METHOD_ID = payMethod.PAYMENT_METHOD_ID
            };
            DBDataContext.Db.TBL_PAYMENTs.InsertOnSubmit(objPayment);
            //update paid amount in invoice
            objInvoice.PAID_AMOUNT = objPayment.PAY_AMOUNT;
            objInvoice.INVOICE_STATUS = (int)InvoiceStatus.Close;
            objPayment.PAYMENT_NO = Method.GetNextSequence(Sequence.Receipt, true);
            DBDataContext.Db.SubmitChanges();

            //add payment detail
            TBL_PAYMENT_DETAIL objPaymentDetail = new TBL_PAYMENT_DETAIL()
            {
                PAYMENT_ID = objPayment.PAYMENT_ID,
                INVOICE_ID = objInvoice.INVOICE_ID,
                PAY_AMOUNT = objInvoice.PAID_AMOUNT,
                DUE_AMOUNT = objInvoice.SETTLE_AMOUNT
            };
            DBDataContext.Db.TBL_PAYMENT_DETAILs.InsertOnSubmit(objPaymentDetail);
            DBDataContext.Db.SubmitChanges();
            return objPayment;
        }

        private void printInvoice(long invoiceID)
        {
            var PintInvoieID = 0;
            TBL_PRINT_INVOICE objPrintInvoice = new TBL_PRINT_INVOICE()
            {
                PRINT_DATE = DBDataContext.Db.GetSystemDate(),
                LOGIN_ID = Login.CurrentLogin.LOGIN_ID
            };
            using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
            {
                DBDataContext.Db.TBL_PRINT_INVOICEs.InsertOnSubmit(objPrintInvoice);
                DBDataContext.Db.SubmitChanges();
                int i = 1;
                DBDataContext.Db.TBL_PRINT_INVOICE_DETAILs.InsertOnSubmit(new TBL_PRINT_INVOICE_DETAIL()
                {
                    PRINT_INVOICE_ID = objPrintInvoice.PRINT_INVOICE_ID,
                    INVOICE_ID = DataHelper.ParseToInt(invoiceID.ToString()),
                    PRINT_ORDER = i++
                });
                DBDataContext.Db.SubmitChanges();
                tran.Complete();
            }
            PintInvoieID = objPrintInvoice.PRINT_INVOICE_ID;
            //Check Print New and Old Invocie
            var InvoiceName = Properties.Settings.Default.REPORT_INVOICE;
            var PinterInvoiceName = Properties.Settings.Default.PRINTER_INVOICE;
            var TemplateString = DBDataContext.Db.TBL_UTILITies.FirstOrDefault(x => x.UTILITY_ID == 40).UTILITY_VALUE.ToString();
            var Templates = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BillPrintingListModel>>(TemplateString);
            var template = Templates.FirstOrDefault(x => x.TemplateName == InvoiceName);

            if (!template.IsOld)
            {
                ReportLogic.Instance.GetPrintNewInvoice(InvoiceName, PinterInvoiceName, PintInvoieID);
            }
            else
            {
                CrystalReportHelper cr = Method.GetInvoiceReport(InvoiceName);
                cr.SetParameter("@PRINT_INVOICE_ID", PintInvoieID);
                cr.PrintReport(PinterInvoiceName);
                cr.Dispose();
            }
        }

        public void InvoiceItemChanged(int invoiceItemId = 0)
        {
            var rowHandle = dgvServiceList.FocusedRowHandle;
            decimal price = 0;
            TBL_INVOICE_ITEM item;
            InviceItemList line = dgvServiceList.GetFocusedRow() as InviceItemList;
            if (line == null)
            {
                return;
            }

            if (invoiceItemId != 0)
            {
                item = invoiceItems.FirstOrDefault(x => x.INVOICE_ITEM_ID == invoiceItemId);
                if (lines.Select(x => x.INVOICE_ITEM_ID).Contains(invoiceItemId))
                {
                    lines.Remove(line);
                    MsgBox.ShowInformation(string.Format(Resources.MS_IS_EXISTS, item.INVOICE_ITEM_NAME));
                    return;
                }
                line.INVOICE_ITEM_ID = invoiceItemId;
            }
            item = invoiceItems.FirstOrDefault(x => x.INVOICE_ITEM_ID == line.INVOICE_ITEM_ID);

            if (_flag == GeneralProcess.Insert && item.INVOICE_ITEM_ID == (int)InvoiceItem.NewConnection)
            {
                price = Method.GetNewConnectionAmount(_objCustomer.CUSTOMER_ID, mCurrency.CURRENCY_ID);
            }
            else
            {
                price = item.PRICE;
            }
            if (string.IsNullOrEmpty(txtNote.Text.Trim()))
            {
                txtNote.Text = item.INVOICE_ITEM_NAME;
            }

            line.CHARGE_DESCRIPTION = item.INVOICE_ITEM_NAME;
            line.PRICE = price;
            line.TAX_ID = item.TAX_ID;
            dgvServiceList.SetRowCellValue(rowHandle, colTax, line.TAX_ID);
            dgvServiceList.UpdateCurrentRow();
            dgvServiceList.RefreshData();
            calculateAmount();
        }

        private void adjustment()
        {
            var isChanged = false;
            var now = DBDataContext.Db.GetSystemDate();
            if (lines.Count() == _oldLines.Count())
            {
                foreach (var line in lines)
                {
                    var tObj = _oldLines.FirstOrDefault(x => x.INVOICE_ITEM_ID == line.INVOICE_ITEM_ID);
                    if (tObj == null)
                    {
                        isChanged = true; break;
                    }
                    else if (!line._Compare(tObj))
                    {
                        isChanged = true; break;
                    }
                }
            }
            if (!isChanged)
            {
                _newObj.DUE_DATE = dtpDueDate.DateTime;
                _newObj.ROW_DATE = now; // update ROW_DATE for B24 Intergration
                DBDataContext.Db.Update(_oldObj, _newObj);
                DBDataContext.Db.SubmitChanges();
                this.DialogResult = DialogResult.Cancel;
                return;
            }

            decimal beforeAdj = _oldObj.TOTAL_AMOUNT;
            decimal adjAmountTax = 0;
            decimal adjAmount = (decimal)txtTotalAmount.EditValue - (_oldObj.TOTAL_AMOUNT - _oldObj.FORWARD_AMOUNT);

            if (adjAmount < 0)
            {
                exchangeRate = new TBL_EXCHANGE_RATE() { CREATE_ON = _oldLines.FirstOrDefault().EXCHANGE_RATE_DATE, EXCHANGE_RATE = _oldLines.FirstOrDefault().EXCHANGE_RATE };
            }
            using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
            {
                ///Add adjustment record
                var objInvTax = DBDataContext.Db.TBL_INVOICE_TAXes.Where(x => (_oldLines.Select(d => d.INVOICE_DETAIL_ID)).Contains(x.INVOICE_DETAIL_ID) && x.IS_ACTIVE).ToList();

                //Adjust removed item
                var objRemoved = from o in _oldLines
                                 where !lines.Select(x => x.INVOICE_ITEM_ID).Contains(o.INVOICE_ITEM_ID)
                                 select o;
                foreach (var item in objRemoved)
                {
                    TBL_INVOICE_ADJUSTMENT objInvoiceAdjust = new TBL_INVOICE_ADJUSTMENT()
                    {
                        INVOICE_ID = item.INVOICE_ID,
                        CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                        CREATE_ON = now,
                        USER_CASH_DRAWER_ID = Login.CurrentCashDrawer.USER_CASH_DRAWER_ID,
                        ADJUST_REASON = txtNote.Text.Trim(),
                        BEFORE_ADJUST_AMOUNT = beforeAdj,
                        ADJUST_AMOUNT = -item.AMOUNT,
                        TAX_ADJUST_AMOUNT = -item.TAX_AMOUNT,
                        METER_ID = 0,
                        BEFORE_ADJUST_USAGE = 0,
                        ADJUST_USAGE = 0,
                        INVOICE_ITEM_ID = item.INVOICE_ITEM_ID,
                        EXCHANGE_RATE = exchangeRate.EXCHANGE_RATE,
                        EXCHANGE_RATE_DATE = exchangeRate.CREATE_ON
                    };
                    beforeAdj -= item.AMOUNT;
                    adjAmountTax -= item.TAX_AMOUNT;
                    DBDataContext.Db.TBL_INVOICE_ADJUSTMENTs.InsertOnSubmit(objInvoiceAdjust);

                    if (item.TAX_ID != 0)
                    {
                        var oldObj = objInvTax.FirstOrDefault(x => x.INVOICE_DETAIL_ID == item.INVOICE_DETAIL_ID);
                        var newObj = oldObj;
                        newObj.IS_ACTIVE = false;
                        DBDataContext.Db.Update(oldObj, newObj);
                    }
                }
                DBDataContext.Db.SubmitChanges();

                //Adjust existing item
                var objItem = from n in lines
                              join o in _oldLines on n.INVOICE_ITEM_ID equals o.INVOICE_ITEM_ID
                              select new
                              {
                                  INVOICE_DETAIL_ID = o.INVOICE_DETAIL_ID,
                                  INVOICE_ITEM_ID = n.INVOICE_ITEM_ID,
                                  INVOICE_ID = n.INVOICE_ID,
                                  ADJUST_AMOUNT = n.AMOUNT - o.AMOUNT,
                                  TAX_ADJUST_AMOUNT = n.TAX_AMOUNT - o.TAX_AMOUNT,
                                  TAX_ID = n.TAX_ID
                              };
                foreach (var item in objItem)
                {
                    TBL_INVOICE_ADJUSTMENT objInvoiceAdjust = new TBL_INVOICE_ADJUSTMENT()
                    {
                        INVOICE_ID = item.INVOICE_ID,
                        CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                        CREATE_ON = now,
                        USER_CASH_DRAWER_ID = Login.CurrentCashDrawer.USER_CASH_DRAWER_ID,
                        ADJUST_REASON = txtNote.Text.Trim(),
                        BEFORE_ADJUST_AMOUNT = beforeAdj,
                        ADJUST_AMOUNT = item.ADJUST_AMOUNT,
                        TAX_ADJUST_AMOUNT = item.TAX_ADJUST_AMOUNT,
                        METER_ID = 0,
                        BEFORE_ADJUST_USAGE = 0,
                        ADJUST_USAGE = 0,
                        EXCHANGE_RATE = exchangeRate.EXCHANGE_RATE,
                        EXCHANGE_RATE_DATE = exchangeRate.CREATE_ON,
                        INVOICE_ITEM_ID = item.INVOICE_ITEM_ID
                    };
                    beforeAdj += item.ADJUST_AMOUNT;
                    adjAmountTax += item.TAX_ADJUST_AMOUNT;
                    DBDataContext.Db.TBL_INVOICE_ADJUSTMENTs.InsertOnSubmit(objInvoiceAdjust);

                    var objOld = objInvTax.FirstOrDefault(x => x.INVOICE_DETAIL_ID == item.INVOICE_DETAIL_ID);
                    //var objNew = new TBL_INVOICE_TAX();
                    if (objOld != null && item.TAX_ID != objOld.TAX_ID)
                    {
                        var objNew = objOld;
                        objNew.IS_ACTIVE = false;
                        DBDataContext.Db.Update(objOld, objNew);
                    }

                    if (item.TAX_ID != 0)
                    {
                        TBL_INVOICE_TAX objNewInvTax = new TBL_INVOICE_TAX
                        {
                            INVOICE_DETAIL_ID = item.INVOICE_DETAIL_ID,
                            TAX_ID = item.TAX_ID,
                            TAX_AMOUNT = item.TAX_ADJUST_AMOUNT,
                            TAX_TITLE = taxes.FirstOrDefault(x => x.TAX_ID == item.TAX_ID).TAX_NAME,
                            ROW_DATE = now,
                            IS_ACTIVE = true
                        };
                        DBDataContext.Db.TBL_INVOICE_TAXes.InsertOnSubmit(objNewInvTax);
                    }
                }
                DBDataContext.Db.SubmitChanges();

                //Add adjustment line in invoice detail
                TBL_INVOICE_DETAIL objDetail = new TBL_INVOICE_DETAIL()
                {
                    AMOUNT = adjAmount,
                    END_USAGE = 0,
                    INVOICE_ID = _oldObj.INVOICE_ID,
                    INVOICE_ITEM_ID = (int)InvoiceItem.Adjustment,
                    PRICE = adjAmount - adjAmountTax,
                    START_USAGE = 0,
                    USAGE = 1,
                    CHARGE_DESCRIPTION = txtNote.Text.Trim(),
                    REF_NO = Method.GetNextSequence(Sequence.Adjustment, true),
                    TRAN_DATE = now,
                    EXCHANGE_RATE = exchangeRate.EXCHANGE_RATE,
                    EXCHANGE_RATE_DATE = exchangeRate.CREATE_ON,
                    TAX_AMOUNT = adjAmountTax,
                    TAX_ID = 0
                };
                DBDataContext.Db.TBL_INVOICE_DETAILs.InsertOnSubmit(objDetail);
                DBDataContext.Db.SubmitChanges();

                ///Update invoice
                _newObj.TOTAL_AMOUNT = _newObj.TOTAL_AMOUNT + adjAmount;
                _newObj.SETTLE_AMOUNT = UIHelper.Round(_newObj.TOTAL_AMOUNT, _newObj.CURRENCY_ID);
                _newObj.INVOICE_STATUS = (_newObj.SETTLE_AMOUNT == 0) ? (int)InvoiceStatus.Close : (int)InvoiceStatus.Open;
                _newObj.DUE_DATE = dtpDueDate.DateTime;
                _newObj.ROW_DATE = now; // update ROW_DATE for B24 Intergration
                DBDataContext.Db.Update(_oldObj, _newObj);
                DBDataContext.Db.SubmitChanges();

                tran.Complete();
                this.DialogResult = DialogResult.OK;
            }
        }

        private void calculateAmount()
        {
            InviceItemList line = dgvServiceList.GetFocusedRow() as InviceItemList;
            decimal taxAmount = 0;
            decimal amount = 0;
            decimal totalTaxAmount = 0;
            decimal totalAmount = 0;
            decimal totalUnTaxAmount = 0;
            if (line != null)
            {
                TBL_TAX tax = taxes.FirstOrDefault(x => x.IS_ACTIVE && x.TAX_ID == line.TAX_ID);
                taxAmount = Method.CalculateTaxAmount(tax, line.PRICE, mCurrency);
                amount = line.PRICE + taxAmount;
                if (tax != null && tax.COMPUTATION == (int)TaxComputations.PERCENTAGE_INCLUDED_TAX)
                {
                    amount = line.PRICE;
                }
                line.TAX_AMOUNT = taxAmount;
                line.AMOUNT = amount;
                dgvServiceList.UpdateCurrentRow();
                dgvServiceList.RefreshData();
            }

            var lines = dgvServiceList.ToListModel<InviceItemList>();
            if (lines.Count > 0)
            {
                totalTaxAmount = lines.Sum(x => x.TAX_AMOUNT);
                totalAmount = lines.Sum(x => x.AMOUNT);
                totalUnTaxAmount = totalAmount - totalTaxAmount;
            }

            this.txtUnTaxAmount.EditValue = totalUnTaxAmount;
            this.txtTaxAmount.EditValue = totalTaxAmount;
            this.txtTotalAmount.EditValue = totalAmount;
            
            /*read khmer number */
            _lblReadKhmerNumber.Text = ReadNumber.Read(totalAmount, true, mCurrency.CURRENCY_NAME);
        }

        #endregion Method

        #region Event

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (invalid())
            {
                return;
            }
            var tranOption = new TransactionOptions() { IsolationLevel = IsolationLevel.ReadUncommitted, Timeout = TimeSpan.MaxValue };
            if (_flag == GeneralProcess.Insert)
            {
                try
                {
                    TBL_INVOICE objInvoice;
                    TBL_PAYMENT objPayment;
                    
                    Runner.RunNewThread(() =>
                    {
                        using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, tranOption))
                        {
                            objInvoice = CreateNewInvoice();
                            tran.Complete();
                        }
                        if (chkIS_PAID.Checked)
                        {
                            using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, tranOption))
                            {
                                objPayment = CreatePayment(objInvoice);
                                tran.Complete();
                            }
                            if (this.chkPRINT.Checked)
                            {
                                Printing.ReceiptPayment(objPayment.PAYMENT_ID);
                            }
                        }
                        else if (this.chkPRINT.Checked)
                        {
                            printInvoice(objInvoice.INVOICE_ID);
                        }
                        this.DialogResult = DialogResult.OK;
                    });
                }
                catch (Exception ex)
                {
                    MsgBox.ShowError(ex);
                }
            }
            else if (_flag == GeneralProcess.Update)
            {
                Runner.RunNewThread(adjustment, Resources.PROCESSING);
            }
            else if (_flag == GeneralProcess.Delete)
            {
                if (MsgBox.ShowQuestion(Resources.MSG_DO_YOU_WANT_TO_VOID_THIS_INVOICE, Resources.WARNING) == DialogResult.Yes)
                {
                    using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, tranOption))
                    {
                        Runner.RunNewThread(() =>
                        {
                            new BillLogic().VoidInvoice(_oldObj.INVOICE_ID);
                        }, Resources.PROCESSING);
                        tran.Complete();
                        this.DialogResult = DialogResult.OK;
                    }
                }
            }
        }

        private void repInvoiceItem_Closed(object sender, ClosedEventArgs e)
        {
            var repInvoiceItem = sender as LookUpEdit;
            if (repInvoiceItem.EditValue != null)
            {
                var itemId = DataHelper.ParseToInt(repInvoiceItem.EditValue.ToString());
                InvoiceItemChanged(itemId);
            }

        }

        private void dgvServiceList_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (e.Column == _colRemove && _flag == GeneralProcess.Insert)
            {
                dgvServiceList.DeleteRow(e.RowHandle);
                calculateAmount();
            }
        }

        private void dtpInvoiceDate_EditValueChanged(object sender, EventArgs e)
        {
            this.dtpDueDate.EditValue = ((DateTime)dtpInvoiceDate.EditValue).AddDays(DataHelper.ParseToInt(Method.GetUtilityValue(Utility.INVOICE_DUE_DATE)));
            RefreshExchangeRate();
        }

        private void cboCurrency_EditValueChanged(object sender, EventArgs e)
        {
            RefreshExchangeRate();
        }

        private void RefreshExchangeRate()
        {
            if (_flag != GeneralProcess.Delete)
            {
                if (cboCurrency.EditValue == null)
                {
                    return;
                }
                mCurrency = SettingLogic.Currencies.FirstOrDefault(x => x.CURRENCY_ID == (int)cboCurrency.EditValue);
                exchangeRate = new ExchangeRateLogic().findLastExchangeRate(dtpInvoiceDate.DateTime, mCurrency.CURRENCY_ID);
                if (mCurrency != null)
                {
                    this.txtExchangeRate.Text = exchangeRate.EXCHANGE_RATE.ToString(UIHelper._DefaultPriceFormat);
                    this.txtExchangeRate.Enabled = mCurrency.CURRENCY_ID != (int)Currency.KHR;
                    this.lblUntaxCurrency.Text = this.lblTaxCurrency.Text = this.lblTotalAmountCurrency.Text = mCurrency.CURRENCY_CODE;
                }
                colPRICE_I.FormatEditAndDisplay(DevExpress.Utils.FormatType.Numeric, mCurrency.FORMAT);
                colTAX_AMOUNT.FormatEditAndDisplay(DevExpress.Utils.FormatType.Numeric, mCurrency.FORMAT);
                colTotal.FormatEditAndDisplay(DevExpress.Utils.FormatType.Numeric, mCurrency.FORMAT);
                txtUnTaxAmount.FormatEditAndDisplay(DevExpress.Utils.FormatType.Numeric, mCurrency.FORMAT);
                txtTaxAmount.FormatEditAndDisplay(DevExpress.Utils.FormatType.Numeric, mCurrency.FORMAT);
                txtTotalAmount.FormatEditAndDisplay(DevExpress.Utils.FormatType.Numeric, mCurrency.FORMAT);
                lblExchangeCurrency.Text = $"= 1 {mCurrency.CURRENCY_NAME}";
            }
        }

        private void dgvServiceList_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column == colService)
            {
                InvoiceItemChanged();
                calculateAmount();
            }
            else if (e.Column == colPRICE_I || e.Column == colTax)
            {
                calculateAmount();
            }
        }
        
        private void DialogCustomerCharge_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.ActiveControl != btnOK)
            {
                this.btnOk_Click(sender, e);
            }
        }

        #endregion Event 

    }

    class InviceItemList
    {
        public long INVOICE_DETAIL_ID { get; set; }
        public long INVOICE_ID { get; set; }
        public int INVOICE_ITEM_ID { get; set; }
        public decimal PRICE { get; set; }
        public decimal AMOUNT { get; set; }
        public decimal TAX_AMOUNT { get; set; }
        public int TAX_ID { get; set; }
        public string CHARGE_DESCRIPTION { get; set; }
        public decimal EXCHANGE_RATE { get; set; }
        public DateTime EXCHANGE_RATE_DATE { get; set; }
    }
}
