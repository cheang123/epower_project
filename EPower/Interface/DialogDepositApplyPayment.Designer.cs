﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogDepositApplyPayment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogDepositApplyPayment));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.txtTotalBalance = new System.Windows.Forms.TextBox();
            this.lblDUE_AMOUNT = new System.Windows.Forms.Label();
            this.txtTotalDue = new System.Windows.Forms.TextBox();
            this.txtTotalPay = new System.Windows.Forms.TextBox();
            this.lblSETTLE_AMOUNT = new System.Windows.Forms.Label();
            this.lblCUSTOMER_NAME = new System.Windows.Forms.Label();
            this.lblCUSTOMER_CODE = new System.Windows.Forms.Label();
            this.lblAREA = new System.Windows.Forms.Label();
            this.txtAreaName = new System.Windows.Forms.TextBox();
            this.txtCustomerCode = new SoftTech.Component.ExTextbox();
            this.lblMETER_CODE = new System.Windows.Forms.Label();
            this.lblCUSTOMER_INFORMATION = new System.Windows.Forms.Label();
            this.lblINVOICE_TO_PAID = new System.Windows.Forms.Label();
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.lblPAY_DATE = new System.Windows.Forms.Label();
            this.lblTOTAL_DUE_AMOUNT = new System.Windows.Forms.Label();
            this.txtPayDate = new System.Windows.Forms.DateTimePicker();
            this.txtCustomerName = new SoftTech.Component.ExTextbox();
            this.txtMeterCode = new SoftTech.Component.ExTextbox();
            this.txtPole = new System.Windows.Forms.TextBox();
            this.lblPOLE = new System.Windows.Forms.Label();
            this.txtBox = new System.Windows.Forms.TextBox();
            this.lblBOX = new System.Windows.Forms.Label();
            this.lblCASHIER = new System.Windows.Forms.Label();
            this.txtLogin = new System.Windows.Forms.TextBox();
            this.chkPARTIAL_PAYMENT = new System.Windows.Forms.CheckBox();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkPRINT_RECEIPT = new System.Windows.Forms.CheckBox();
            this.lblText_ = new System.Windows.Forms.Label();
            this.lblDEPOSIT_AMOUNT = new System.Windows.Forms.Label();
            this.txtDepositBalance = new System.Windows.Forms.TextBox();
            this.lblREMAIN_DEPOSIT = new System.Windows.Forms.Label();
            this.txtRemainDeposit = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.lblCURRENCY = new System.Windows.Forms.Label();
            this.txtSignDepositBalance = new System.Windows.Forms.TextBox();
            this.txtSignTotalDue = new System.Windows.Forms.TextBox();
            this.txtSignTotalBalance = new System.Windows.Forms.TextBox();
            this.txtSignTotalPay = new System.Windows.Forms.TextBox();
            this.txtSignRemainDeposit = new System.Windows.Forms.TextBox();
            this.INVOICE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICE_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICE_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICE_TITLE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SETTLE_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAID_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DUE_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_SING_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.txtSignRemainDeposit);
            this.content.Controls.Add(this.txtSignTotalBalance);
            this.content.Controls.Add(this.txtSignTotalPay);
            this.content.Controls.Add(this.txtSignTotalDue);
            this.content.Controls.Add(this.txtSignDepositBalance);
            this.content.Controls.Add(this.cboCurrency);
            this.content.Controls.Add(this.lblCURRENCY);
            this.content.Controls.Add(this.txtRemainDeposit);
            this.content.Controls.Add(this.lblREMAIN_DEPOSIT);
            this.content.Controls.Add(this.txtDepositBalance);
            this.content.Controls.Add(this.lblDEPOSIT_AMOUNT);
            this.content.Controls.Add(this.chkPRINT_RECEIPT);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.dgv);
            this.content.Controls.Add(this.lblText_);
            this.content.Controls.Add(this.chkPARTIAL_PAYMENT);
            this.content.Controls.Add(this.txtLogin);
            this.content.Controls.Add(this.lblCASHIER);
            this.content.Controls.Add(this.txtBox);
            this.content.Controls.Add(this.lblBOX);
            this.content.Controls.Add(this.txtPole);
            this.content.Controls.Add(this.lblPOLE);
            this.content.Controls.Add(this.txtMeterCode);
            this.content.Controls.Add(this.txtCustomerName);
            this.content.Controls.Add(this.txtPayDate);
            this.content.Controls.Add(this.lblPAY_DATE);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.lblINVOICE_TO_PAID);
            this.content.Controls.Add(this.lblCUSTOMER_INFORMATION);
            this.content.Controls.Add(this.lblMETER_CODE);
            this.content.Controls.Add(this.txtCustomerCode);
            this.content.Controls.Add(this.txtAreaName);
            this.content.Controls.Add(this.lblAREA);
            this.content.Controls.Add(this.txtTotalBalance);
            this.content.Controls.Add(this.lblDUE_AMOUNT);
            this.content.Controls.Add(this.txtTotalDue);
            this.content.Controls.Add(this.txtTotalPay);
            this.content.Controls.Add(this.lblSETTLE_AMOUNT);
            this.content.Controls.Add(this.lblTOTAL_DUE_AMOUNT);
            this.content.Controls.Add(this.lblCUSTOMER_NAME);
            this.content.Controls.Add(this.lblCUSTOMER_CODE);
            this.content.Controls.Add(this.label13);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.label13, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_CODE, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblTOTAL_DUE_AMOUNT, 0);
            this.content.Controls.SetChildIndex(this.lblSETTLE_AMOUNT, 0);
            this.content.Controls.SetChildIndex(this.txtTotalPay, 0);
            this.content.Controls.SetChildIndex(this.txtTotalDue, 0);
            this.content.Controls.SetChildIndex(this.lblDUE_AMOUNT, 0);
            this.content.Controls.SetChildIndex(this.txtTotalBalance, 0);
            this.content.Controls.SetChildIndex(this.lblAREA, 0);
            this.content.Controls.SetChildIndex(this.txtAreaName, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerCode, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_CODE, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_INFORMATION, 0);
            this.content.Controls.SetChildIndex(this.lblINVOICE_TO_PAID, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.lblPAY_DATE, 0);
            this.content.Controls.SetChildIndex(this.txtPayDate, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerName, 0);
            this.content.Controls.SetChildIndex(this.txtMeterCode, 0);
            this.content.Controls.SetChildIndex(this.lblPOLE, 0);
            this.content.Controls.SetChildIndex(this.txtPole, 0);
            this.content.Controls.SetChildIndex(this.lblBOX, 0);
            this.content.Controls.SetChildIndex(this.txtBox, 0);
            this.content.Controls.SetChildIndex(this.lblCASHIER, 0);
            this.content.Controls.SetChildIndex(this.txtLogin, 0);
            this.content.Controls.SetChildIndex(this.chkPARTIAL_PAYMENT, 0);
            this.content.Controls.SetChildIndex(this.lblText_, 0);
            this.content.Controls.SetChildIndex(this.dgv, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.chkPRINT_RECEIPT, 0);
            this.content.Controls.SetChildIndex(this.lblDEPOSIT_AMOUNT, 0);
            this.content.Controls.SetChildIndex(this.txtDepositBalance, 0);
            this.content.Controls.SetChildIndex(this.lblREMAIN_DEPOSIT, 0);
            this.content.Controls.SetChildIndex(this.txtRemainDeposit, 0);
            this.content.Controls.SetChildIndex(this.lblCURRENCY, 0);
            this.content.Controls.SetChildIndex(this.cboCurrency, 0);
            this.content.Controls.SetChildIndex(this.txtSignDepositBalance, 0);
            this.content.Controls.SetChildIndex(this.txtSignTotalDue, 0);
            this.content.Controls.SetChildIndex(this.txtSignTotalPay, 0);
            this.content.Controls.SetChildIndex(this.txtSignTotalBalance, 0);
            this.content.Controls.SetChildIndex(this.txtSignRemainDeposit, 0);
            // 
            // txtTotalBalance
            // 
            resources.ApplyResources(this.txtTotalBalance, "txtTotalBalance");
            this.txtTotalBalance.Name = "txtTotalBalance";
            this.txtTotalBalance.ReadOnly = true;
            this.txtTotalBalance.TabStop = false;
            // 
            // lblDUE_AMOUNT
            // 
            resources.ApplyResources(this.lblDUE_AMOUNT, "lblDUE_AMOUNT");
            this.lblDUE_AMOUNT.Name = "lblDUE_AMOUNT";
            // 
            // txtTotalDue
            // 
            resources.ApplyResources(this.txtTotalDue, "txtTotalDue");
            this.txtTotalDue.Name = "txtTotalDue";
            this.txtTotalDue.ReadOnly = true;
            this.txtTotalDue.TabStop = false;
            // 
            // txtTotalPay
            // 
            resources.ApplyResources(this.txtTotalPay, "txtTotalPay");
            this.txtTotalPay.Name = "txtTotalPay";
            this.txtTotalPay.TextChanged += new System.EventHandler(this.txtPayAmount_TextChanged);
            this.txtTotalPay.Enter += new System.EventHandler(this.InputEnglish);
            this.txtTotalPay.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPayAmount_KeyDown);
            // 
            // lblSETTLE_AMOUNT
            // 
            resources.ApplyResources(this.lblSETTLE_AMOUNT, "lblSETTLE_AMOUNT");
            this.lblSETTLE_AMOUNT.Name = "lblSETTLE_AMOUNT";
            // 
            // lblCUSTOMER_NAME
            // 
            resources.ApplyResources(this.lblCUSTOMER_NAME, "lblCUSTOMER_NAME");
            this.lblCUSTOMER_NAME.Name = "lblCUSTOMER_NAME";
            // 
            // lblCUSTOMER_CODE
            // 
            resources.ApplyResources(this.lblCUSTOMER_CODE, "lblCUSTOMER_CODE");
            this.lblCUSTOMER_CODE.Name = "lblCUSTOMER_CODE";
            // 
            // lblAREA
            // 
            resources.ApplyResources(this.lblAREA, "lblAREA");
            this.lblAREA.Name = "lblAREA";
            // 
            // txtAreaName
            // 
            resources.ApplyResources(this.txtAreaName, "txtAreaName");
            this.txtAreaName.Name = "txtAreaName";
            // 
            // txtCustomerCode
            // 
            this.txtCustomerCode.BackColor = System.Drawing.Color.White;
            this.txtCustomerCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtCustomerCode, "txtCustomerCode");
            this.txtCustomerCode.Name = "txtCustomerCode";
            this.txtCustomerCode.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtCustomerCode.AdvanceSearch += new System.EventHandler(this.txtCustomerCode_AdvanceSearch);
            this.txtCustomerCode.CancelAdvanceSearch += new System.EventHandler(this.txtMeterCode_CancelAdvanceSearch);
            this.txtCustomerCode.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // lblMETER_CODE
            // 
            resources.ApplyResources(this.lblMETER_CODE, "lblMETER_CODE");
            this.lblMETER_CODE.Name = "lblMETER_CODE";
            // 
            // lblCUSTOMER_INFORMATION
            // 
            resources.ApplyResources(this.lblCUSTOMER_INFORMATION, "lblCUSTOMER_INFORMATION");
            this.lblCUSTOMER_INFORMATION.Name = "lblCUSTOMER_INFORMATION";
            // 
            // lblINVOICE_TO_PAID
            // 
            resources.ApplyResources(this.lblINVOICE_TO_PAID, "lblINVOICE_TO_PAID");
            this.lblINVOICE_TO_PAID.Name = "lblINVOICE_TO_PAID";
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblPAY_DATE
            // 
            resources.ApplyResources(this.lblPAY_DATE, "lblPAY_DATE");
            this.lblPAY_DATE.Name = "lblPAY_DATE";
            // 
            // lblTOTAL_DUE_AMOUNT
            // 
            resources.ApplyResources(this.lblTOTAL_DUE_AMOUNT, "lblTOTAL_DUE_AMOUNT");
            this.lblTOTAL_DUE_AMOUNT.Name = "lblTOTAL_DUE_AMOUNT";
            // 
            // txtPayDate
            // 
            resources.ApplyResources(this.txtPayDate, "txtPayDate");
            this.txtPayDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtPayDate.Name = "txtPayDate";
            this.txtPayDate.ShowUpDown = true;
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.BackColor = System.Drawing.Color.White;
            this.txtCustomerName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtCustomerName, "txtCustomerName");
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtCustomerName.AdvanceSearch += new System.EventHandler(this.txtCustomerName_AdvanceSearch);
            this.txtCustomerName.CancelAdvanceSearch += new System.EventHandler(this.txtMeterCode_CancelAdvanceSearch);
            this.txtCustomerName.Enter += new System.EventHandler(this.InputKhmer);
            // 
            // txtMeterCode
            // 
            this.txtMeterCode.BackColor = System.Drawing.Color.White;
            this.txtMeterCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtMeterCode, "txtMeterCode");
            this.txtMeterCode.Name = "txtMeterCode";
            this.txtMeterCode.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtMeterCode.AdvanceSearch += new System.EventHandler(this.txtMeterCode_AdvanceSearch);
            this.txtMeterCode.CancelAdvanceSearch += new System.EventHandler(this.txtMeterCode_CancelAdvanceSearch);
            this.txtMeterCode.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // txtPole
            // 
            resources.ApplyResources(this.txtPole, "txtPole");
            this.txtPole.Name = "txtPole";
            // 
            // lblPOLE
            // 
            resources.ApplyResources(this.lblPOLE, "lblPOLE");
            this.lblPOLE.Name = "lblPOLE";
            // 
            // txtBox
            // 
            resources.ApplyResources(this.txtBox, "txtBox");
            this.txtBox.Name = "txtBox";
            // 
            // lblBOX
            // 
            resources.ApplyResources(this.lblBOX, "lblBOX");
            this.lblBOX.Name = "lblBOX";
            // 
            // lblCASHIER
            // 
            resources.ApplyResources(this.lblCASHIER, "lblCASHIER");
            this.lblCASHIER.Name = "lblCASHIER";
            // 
            // txtLogin
            // 
            resources.ApplyResources(this.txtLogin, "txtLogin");
            this.txtLogin.Name = "txtLogin";
            this.txtLogin.ReadOnly = true;
            this.txtLogin.TabStop = false;
            // 
            // chkPARTIAL_PAYMENT
            // 
            resources.ApplyResources(this.chkPARTIAL_PAYMENT, "chkPARTIAL_PAYMENT");
            this.chkPARTIAL_PAYMENT.Name = "chkPARTIAL_PAYMENT";
            this.chkPARTIAL_PAYMENT.UseVisualStyleBackColor = true;
            // 
            // dgv
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.INVOICE_ID,
            this.INVOICE_NO,
            this.INVOICE_DATE,
            this.INVOICE_TITLE,
            this.SETTLE_AMOUNT,
            this.PAID_AMOUNT,
            this.DUE_AMOUNT,
            this.CURRENCY_SING_});
            this.dgv.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // chkPRINT_RECEIPT
            // 
            resources.ApplyResources(this.chkPRINT_RECEIPT, "chkPRINT_RECEIPT");
            this.chkPRINT_RECEIPT.Checked = true;
            this.chkPRINT_RECEIPT.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkPRINT_RECEIPT.Name = "chkPRINT_RECEIPT";
            this.chkPRINT_RECEIPT.UseVisualStyleBackColor = true;
            // 
            // lblText_
            // 
            resources.ApplyResources(this.lblText_, "lblText_");
            this.lblText_.Name = "lblText_";
            // 
            // lblDEPOSIT_AMOUNT
            // 
            resources.ApplyResources(this.lblDEPOSIT_AMOUNT, "lblDEPOSIT_AMOUNT");
            this.lblDEPOSIT_AMOUNT.Name = "lblDEPOSIT_AMOUNT";
            // 
            // txtDepositBalance
            // 
            resources.ApplyResources(this.txtDepositBalance, "txtDepositBalance");
            this.txtDepositBalance.Name = "txtDepositBalance";
            this.txtDepositBalance.ReadOnly = true;
            this.txtDepositBalance.TabStop = false;
            // 
            // lblREMAIN_DEPOSIT
            // 
            resources.ApplyResources(this.lblREMAIN_DEPOSIT, "lblREMAIN_DEPOSIT");
            this.lblREMAIN_DEPOSIT.Name = "lblREMAIN_DEPOSIT";
            // 
            // txtRemainDeposit
            // 
            resources.ApplyResources(this.txtRemainDeposit, "txtRemainDeposit");
            this.txtRemainDeposit.Name = "txtRemainDeposit";
            this.txtRemainDeposit.ReadOnly = true;
            this.txtRemainDeposit.TabStop = false;
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Name = "label13";
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.SelectedIndexChanged += new System.EventHandler(this.cboCurrency_SelectedIndexChanged);
            // 
            // lblCURRENCY
            // 
            resources.ApplyResources(this.lblCURRENCY, "lblCURRENCY");
            this.lblCURRENCY.Name = "lblCURRENCY";
            // 
            // txtSignDepositBalance
            // 
            resources.ApplyResources(this.txtSignDepositBalance, "txtSignDepositBalance");
            this.txtSignDepositBalance.Name = "txtSignDepositBalance";
            this.txtSignDepositBalance.ReadOnly = true;
            this.txtSignDepositBalance.TabStop = false;
            // 
            // txtSignTotalDue
            // 
            resources.ApplyResources(this.txtSignTotalDue, "txtSignTotalDue");
            this.txtSignTotalDue.Name = "txtSignTotalDue";
            this.txtSignTotalDue.ReadOnly = true;
            this.txtSignTotalDue.TabStop = false;
            // 
            // txtSignTotalBalance
            // 
            resources.ApplyResources(this.txtSignTotalBalance, "txtSignTotalBalance");
            this.txtSignTotalBalance.Name = "txtSignTotalBalance";
            this.txtSignTotalBalance.ReadOnly = true;
            this.txtSignTotalBalance.TabStop = false;
            // 
            // txtSignTotalPay
            // 
            resources.ApplyResources(this.txtSignTotalPay, "txtSignTotalPay");
            this.txtSignTotalPay.Name = "txtSignTotalPay";
            this.txtSignTotalPay.ReadOnly = true;
            this.txtSignTotalPay.TabStop = false;
            // 
            // txtSignRemainDeposit
            // 
            resources.ApplyResources(this.txtSignRemainDeposit, "txtSignRemainDeposit");
            this.txtSignRemainDeposit.Name = "txtSignRemainDeposit";
            this.txtSignRemainDeposit.ReadOnly = true;
            this.txtSignRemainDeposit.TabStop = false;
            // 
            // INVOICE_ID
            // 
            this.INVOICE_ID.DataPropertyName = "INVOICE_ID";
            resources.ApplyResources(this.INVOICE_ID, "INVOICE_ID");
            this.INVOICE_ID.Name = "INVOICE_ID";
            // 
            // INVOICE_NO
            // 
            this.INVOICE_NO.DataPropertyName = "INVOICE_NO";
            resources.ApplyResources(this.INVOICE_NO, "INVOICE_NO");
            this.INVOICE_NO.Name = "INVOICE_NO";
            // 
            // INVOICE_DATE
            // 
            this.INVOICE_DATE.DataPropertyName = "INVOICE_DATE";
            dataGridViewCellStyle2.Format = "yyyy-MM-dd";
            this.INVOICE_DATE.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.INVOICE_DATE, "INVOICE_DATE");
            this.INVOICE_DATE.Name = "INVOICE_DATE";
            // 
            // INVOICE_TITLE
            // 
            this.INVOICE_TITLE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.INVOICE_TITLE.DataPropertyName = "INVOICE_TITLE";
            resources.ApplyResources(this.INVOICE_TITLE, "INVOICE_TITLE");
            this.INVOICE_TITLE.Name = "INVOICE_TITLE";
            // 
            // SETTLE_AMOUNT
            // 
            this.SETTLE_AMOUNT.DataPropertyName = "SETTLE_AMOUNT";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "#,##0.####";
            this.SETTLE_AMOUNT.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.SETTLE_AMOUNT, "SETTLE_AMOUNT");
            this.SETTLE_AMOUNT.Name = "SETTLE_AMOUNT";
            // 
            // PAID_AMOUNT
            // 
            this.PAID_AMOUNT.DataPropertyName = "PAID_AMOUNT";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "#,##0.####";
            this.PAID_AMOUNT.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.PAID_AMOUNT, "PAID_AMOUNT");
            this.PAID_AMOUNT.Name = "PAID_AMOUNT";
            // 
            // DUE_AMOUNT
            // 
            this.DUE_AMOUNT.DataPropertyName = "DUE_AMOUNT";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "#,##0.####";
            this.DUE_AMOUNT.DefaultCellStyle = dataGridViewCellStyle5;
            resources.ApplyResources(this.DUE_AMOUNT, "DUE_AMOUNT");
            this.DUE_AMOUNT.Name = "DUE_AMOUNT";
            // 
            // CURRENCY_SING_
            // 
            this.CURRENCY_SING_.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.CURRENCY_SING_.DataPropertyName = "CURRENCY_SING";
            this.CURRENCY_SING_.FillWeight = 65F;
            resources.ApplyResources(this.CURRENCY_SING_, "CURRENCY_SING_");
            this.CURRENCY_SING_.Name = "CURRENCY_SING_";
            // 
            // DialogDepositApplyPayment
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Image = global::EPower.Properties.Resources.icon_payment;
            this.Name = "DialogDepositApplyPayment";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBox txtTotalBalance;
        private Label lblDUE_AMOUNT;
        private TextBox txtTotalDue;
        private TextBox txtTotalPay;
        private Label lblSETTLE_AMOUNT;
        private Label lblCUSTOMER_NAME;
        private Label lblCUSTOMER_CODE;
        private Label lblAREA;
        private TextBox txtAreaName;
        private ExTextbox txtCustomerCode;
        private Label lblINVOICE_TO_PAID;
        private Label lblCUSTOMER_INFORMATION;
        private Label lblMETER_CODE;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label lblPAY_DATE;
        private DateTimePicker txtPayDate;
        private Label lblTOTAL_DUE_AMOUNT;
        private TextBox txtBox;
        private Label lblBOX;
        private TextBox txtPole;
        private Label lblPOLE;
        private ExTextbox txtMeterCode;
        private ExTextbox txtCustomerName;
        private TextBox txtLogin;
        private Label lblCASHIER;
        private CheckBox chkPARTIAL_PAYMENT;
        private DataGridView dgv;
        private Panel panel1;
        private CheckBox chkPRINT_RECEIPT;
        private Label lblText_;
        private TextBox txtDepositBalance;
        private Label lblDEPOSIT_AMOUNT;
        private TextBox txtRemainDeposit;
        private Label lblREMAIN_DEPOSIT;
        private Label label13;
        private ComboBox cboCurrency;
        private Label lblCURRENCY;
        private TextBox txtSignRemainDeposit;
        private TextBox txtSignTotalBalance;
        private TextBox txtSignTotalPay;
        private TextBox txtSignTotalDue;
        private TextBox txtSignDepositBalance;
        private DataGridViewTextBoxColumn INVOICE_ID;
        private DataGridViewTextBoxColumn INVOICE_NO;
        private DataGridViewTextBoxColumn INVOICE_DATE;
        private DataGridViewTextBoxColumn INVOICE_TITLE;
        private DataGridViewTextBoxColumn SETTLE_AMOUNT;
        private DataGridViewTextBoxColumn PAID_AMOUNT;
        private DataGridViewTextBoxColumn DUE_AMOUNT;
        private DataGridViewTextBoxColumn CURRENCY_SING_;
    }
}