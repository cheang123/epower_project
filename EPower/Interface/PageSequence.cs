﻿using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;

namespace EPower.Interface
{
    public partial class PageSequence : Form
    {

        #region Constructor
         public PageSequence()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            txt_QuickSearch(null, null); 
            this.VisibleChanged += PageSequence_VisibleChanged;
        }

        private void PageSequence_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible) 
            {
                Reload();
            } 
        } 


        #endregion

        #region Operation

        private void btnNew_Click(object sender, EventArgs e)
        {            
            
        }
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                TBL_SEQUENCE objSeq = DBDataContext.Db.TBL_SEQUENCEs.FirstOrDefault(x => x.SEQUENCE_ID == (int)dgv.SelectedRows[0].Cells[SEQUENCE_ID.Name].Value);
                DialogSequence diag = new DialogSequence(GeneralProcess.Update, objSeq);
                if (diag.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, diag.Sequence.SEQUENCE_ID);
                }
            }
        }

        
        #endregion

        #region Method
       
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            try
            {
                Reload();
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
            
        }

        private void Reload()
        {
            dgv.DataSource = from ep in DBDataContext.Db.TBL_SEQUENCEs
                             where ep.IS_ACTIVE
                             select new
                             {
                                 ep.SEQUENCE_ID,
                                 ep.SEQUENCE_NAME_KH,
                                 ep.FORMAT,
                                 ep.VALUE
                             };
        }

        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        #endregion

        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            
            btnEdit_Click(null, null);
        }
    }
}
