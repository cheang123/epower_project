﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogInvoiceItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogInvoiceItem));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabGENERAL = new System.Windows.Forms.TabPage();
            this.lblRECURRING_MONTH = new System.Windows.Forms.Label();
            this.txtRecuringMonth = new System.Windows.Forms.TextBox();
            this.chkIS_RECURRING_SERVICE = new System.Windows.Forms.CheckBox();
            this.chkIS_EDITABLE = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblCURRENCY = new System.Windows.Forms.Label();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblUNIT_PRICE = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.lblSERVICE_TYPE = new System.Windows.Forms.Label();
            this.cboInviceItemType = new System.Windows.Forms.ComboBox();
            this.txtInvoiceItemName = new System.Windows.Forms.TextBox();
            this.lblSERVICE = new System.Windows.Forms.Label();
            this.tabACCOUNTING = new System.Windows.Forms.TabPage();
            this.cboAR_VND = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit();
            this._lblREQ_AR_VND = new System.Windows.Forms.Label();
            this.lblAR_VND = new System.Windows.Forms.Label();
            this.cboAR_THB = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit();
            this._lblREQ_AR_THB = new System.Windows.Forms.Label();
            this.lblAR_THB = new System.Windows.Forms.Label();
            this.cboAR_USD = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit();
            this._lblREQ_AR_USD = new System.Windows.Forms.Label();
            this.lblAR_USD = new System.Windows.Forms.Label();
            this.cboAR_KHR = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit();
            this.cboAccountIncome = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit();
            this._lblREQ_AR_KHR = new System.Windows.Forms.Label();
            this.lblAR_KHR = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblINCOME_ACCOUNT = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabGENERAL.SuspendLayout();
            this.tabACCOUNTING.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboAR_VND.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboAR_THB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboAR_USD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboAR_KHR.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboAccountIncome.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.tabControl);
            this.content.Controls.Add(this.panel1);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.tabControl, 0);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.btnCHANGE_LOG);
            this.panel1.Controls.Add(this.btnCLOSE);
            this.panel1.Controls.Add(this.btnOK);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabGENERAL);
            this.tabControl.Controls.Add(this.tabACCOUNTING);
            resources.ApplyResources(this.tabControl, "tabControl");
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            // 
            // tabGENERAL
            // 
            this.tabGENERAL.Controls.Add(this.lblRECURRING_MONTH);
            this.tabGENERAL.Controls.Add(this.txtRecuringMonth);
            this.tabGENERAL.Controls.Add(this.chkIS_RECURRING_SERVICE);
            this.tabGENERAL.Controls.Add(this.chkIS_EDITABLE);
            this.tabGENERAL.Controls.Add(this.label3);
            this.tabGENERAL.Controls.Add(this.lblCURRENCY);
            this.tabGENERAL.Controls.Add(this.cboCurrency);
            this.tabGENERAL.Controls.Add(this.label6);
            this.tabGENERAL.Controls.Add(this.label4);
            this.tabGENERAL.Controls.Add(this.label9);
            this.tabGENERAL.Controls.Add(this.lblUNIT_PRICE);
            this.tabGENERAL.Controls.Add(this.txtPrice);
            this.tabGENERAL.Controls.Add(this.lblSERVICE_TYPE);
            this.tabGENERAL.Controls.Add(this.cboInviceItemType);
            this.tabGENERAL.Controls.Add(this.txtInvoiceItemName);
            this.tabGENERAL.Controls.Add(this.lblSERVICE);
            resources.ApplyResources(this.tabGENERAL, "tabGENERAL");
            this.tabGENERAL.Name = "tabGENERAL";
            this.tabGENERAL.UseVisualStyleBackColor = true;
            // 
            // lblRECURRING_MONTH
            // 
            resources.ApplyResources(this.lblRECURRING_MONTH, "lblRECURRING_MONTH");
            this.lblRECURRING_MONTH.Name = "lblRECURRING_MONTH";
            // 
            // txtRecuringMonth
            // 
            resources.ApplyResources(this.txtRecuringMonth, "txtRecuringMonth");
            this.txtRecuringMonth.Name = "txtRecuringMonth";
            // 
            // chkIS_RECURRING_SERVICE
            // 
            resources.ApplyResources(this.chkIS_RECURRING_SERVICE, "chkIS_RECURRING_SERVICE");
            this.chkIS_RECURRING_SERVICE.Name = "chkIS_RECURRING_SERVICE";
            this.chkIS_RECURRING_SERVICE.UseVisualStyleBackColor = true;
            this.chkIS_RECURRING_SERVICE.CheckedChanged += new System.EventHandler(this.chkIS_RECURRING_SERVICE_CheckedChanged);
            // 
            // chkIS_EDITABLE
            // 
            resources.ApplyResources(this.chkIS_EDITABLE, "chkIS_EDITABLE");
            this.chkIS_EDITABLE.Name = "chkIS_EDITABLE";
            this.chkIS_EDITABLE.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Name = "label3";
            // 
            // lblCURRENCY
            // 
            resources.ApplyResources(this.lblCURRENCY, "lblCURRENCY");
            this.lblCURRENCY.Name = "lblCURRENCY";
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            this.cboCurrency.Items.AddRange(new object[] {
            resources.GetString("cboCurrency.Items"),
            resources.GetString("cboCurrency.Items1"),
            resources.GetString("cboCurrency.Items2"),
            resources.GetString("cboCurrency.Items3")});
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Name = "label6";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Name = "label4";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // lblUNIT_PRICE
            // 
            resources.ApplyResources(this.lblUNIT_PRICE, "lblUNIT_PRICE");
            this.lblUNIT_PRICE.Name = "lblUNIT_PRICE";
            // 
            // txtPrice
            // 
            resources.ApplyResources(this.txtPrice, "txtPrice");
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            this.txtPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumber);
            // 
            // lblSERVICE_TYPE
            // 
            resources.ApplyResources(this.lblSERVICE_TYPE, "lblSERVICE_TYPE");
            this.lblSERVICE_TYPE.Name = "lblSERVICE_TYPE";
            // 
            // cboInviceItemType
            // 
            this.cboInviceItemType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboInviceItemType.FormattingEnabled = true;
            this.cboInviceItemType.Items.AddRange(new object[] {
            resources.GetString("cboInviceItemType.Items"),
            resources.GetString("cboInviceItemType.Items1"),
            resources.GetString("cboInviceItemType.Items2"),
            resources.GetString("cboInviceItemType.Items3")});
            resources.ApplyResources(this.cboInviceItemType, "cboInviceItemType");
            this.cboInviceItemType.Name = "cboInviceItemType";
            // 
            // txtInvoiceItemName
            // 
            resources.ApplyResources(this.txtInvoiceItemName, "txtInvoiceItemName");
            this.txtInvoiceItemName.Name = "txtInvoiceItemName";
            this.txtInvoiceItemName.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // lblSERVICE
            // 
            resources.ApplyResources(this.lblSERVICE, "lblSERVICE");
            this.lblSERVICE.Name = "lblSERVICE";
            // 
            // tabACCOUNTING
            // 
            this.tabACCOUNTING.Controls.Add(this.cboAR_VND);
            this.tabACCOUNTING.Controls.Add(this._lblREQ_AR_VND);
            this.tabACCOUNTING.Controls.Add(this.lblAR_VND);
            this.tabACCOUNTING.Controls.Add(this.cboAR_THB);
            this.tabACCOUNTING.Controls.Add(this._lblREQ_AR_THB);
            this.tabACCOUNTING.Controls.Add(this.lblAR_THB);
            this.tabACCOUNTING.Controls.Add(this.cboAR_USD);
            this.tabACCOUNTING.Controls.Add(this._lblREQ_AR_USD);
            this.tabACCOUNTING.Controls.Add(this.lblAR_USD);
            this.tabACCOUNTING.Controls.Add(this.cboAR_KHR);
            this.tabACCOUNTING.Controls.Add(this.cboAccountIncome);
            this.tabACCOUNTING.Controls.Add(this._lblREQ_AR_KHR);
            this.tabACCOUNTING.Controls.Add(this.lblAR_KHR);
            this.tabACCOUNTING.Controls.Add(this.label11);
            this.tabACCOUNTING.Controls.Add(this.lblINCOME_ACCOUNT);
            resources.ApplyResources(this.tabACCOUNTING, "tabACCOUNTING");
            this.tabACCOUNTING.Name = "tabACCOUNTING";
            this.tabACCOUNTING.UseVisualStyleBackColor = true;
            // 
            // cboAR_VND
            // 
            resources.ApplyResources(this.cboAR_VND, "cboAR_VND");
            this.cboAR_VND.Name = "cboAR_VND";
            this.cboAR_VND.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("dLookUpEdit3.Properties.Appearance.Font")));
            this.cboAR_VND.Properties.Appearance.Options.UseFont = true;
            this.cboAR_VND.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("dLookUpEdit3.Properties.Buttons"))))});
            this.cboAR_VND.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("dLookUpEdit3.Properties.Columns"), resources.GetString("dLookUpEdit3.Properties.Columns1"), ((int)(resources.GetObject("dLookUpEdit3.Properties.Columns2"))), ((DevExpress.Utils.FormatType)(resources.GetObject("dLookUpEdit3.Properties.Columns3"))), resources.GetString("dLookUpEdit3.Properties.Columns4"), ((bool)(resources.GetObject("dLookUpEdit3.Properties.Columns5"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("dLookUpEdit3.Properties.Columns6"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("dLookUpEdit3.Properties.Columns7"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("dLookUpEdit3.Properties.Columns8")))),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("dLookUpEdit3.Properties.Columns9"), resources.GetString("dLookUpEdit3.Properties.Columns10"), ((int)(resources.GetObject("dLookUpEdit3.Properties.Columns11"))), ((DevExpress.Utils.FormatType)(resources.GetObject("dLookUpEdit3.Properties.Columns12"))), resources.GetString("dLookUpEdit3.Properties.Columns13"), ((bool)(resources.GetObject("dLookUpEdit3.Properties.Columns14"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("dLookUpEdit3.Properties.Columns15"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("dLookUpEdit3.Properties.Columns16"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("dLookUpEdit3.Properties.Columns17")))),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("dLookUpEdit3.Properties.Columns18"), resources.GetString("dLookUpEdit3.Properties.Columns19"), ((int)(resources.GetObject("dLookUpEdit3.Properties.Columns20"))), ((DevExpress.Utils.FormatType)(resources.GetObject("dLookUpEdit3.Properties.Columns21"))), resources.GetString("dLookUpEdit3.Properties.Columns22"), ((bool)(resources.GetObject("dLookUpEdit3.Properties.Columns23"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("dLookUpEdit3.Properties.Columns24"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("dLookUpEdit3.Properties.Columns25"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("dLookUpEdit3.Properties.Columns26"))))});
            this.cboAR_VND.Properties.NullText = resources.GetString("dLookUpEdit3.Properties.NullText");
            this.cboAR_VND.Properties.PopupWidth = 500;
            this.cboAR_VND.Properties.PopupWidthMode = DevExpress.XtraEditors.PopupWidthMode.ContentWidth;
            this.cboAR_VND.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.cboAR_VND.Required = false;
            // 
            // _lblREQ_AR_VND
            // 
            resources.ApplyResources(this._lblREQ_AR_VND, "_lblREQ_AR_VND");
            this._lblREQ_AR_VND.ForeColor = System.Drawing.Color.Red;
            this._lblREQ_AR_VND.Name = "_lblREQ_AR_VND";
            // 
            // lblAR_VND
            // 
            resources.ApplyResources(this.lblAR_VND, "lblAR_VND");
            this.lblAR_VND.Name = "lblAR_VND";
            // 
            // cboAR_THB
            // 
            resources.ApplyResources(this.cboAR_THB, "cboAR_THB");
            this.cboAR_THB.Name = "cboAR_THB";
            this.cboAR_THB.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("dLookUpEdit2.Properties.Appearance.Font")));
            this.cboAR_THB.Properties.Appearance.Options.UseFont = true;
            this.cboAR_THB.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("dLookUpEdit2.Properties.Buttons"))))});
            this.cboAR_THB.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("dLookUpEdit2.Properties.Columns"), resources.GetString("dLookUpEdit2.Properties.Columns1"), ((int)(resources.GetObject("dLookUpEdit2.Properties.Columns2"))), ((DevExpress.Utils.FormatType)(resources.GetObject("dLookUpEdit2.Properties.Columns3"))), resources.GetString("dLookUpEdit2.Properties.Columns4"), ((bool)(resources.GetObject("dLookUpEdit2.Properties.Columns5"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("dLookUpEdit2.Properties.Columns6"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("dLookUpEdit2.Properties.Columns7"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("dLookUpEdit2.Properties.Columns8")))),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("dLookUpEdit2.Properties.Columns9"), resources.GetString("dLookUpEdit2.Properties.Columns10"), ((int)(resources.GetObject("dLookUpEdit2.Properties.Columns11"))), ((DevExpress.Utils.FormatType)(resources.GetObject("dLookUpEdit2.Properties.Columns12"))), resources.GetString("dLookUpEdit2.Properties.Columns13"), ((bool)(resources.GetObject("dLookUpEdit2.Properties.Columns14"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("dLookUpEdit2.Properties.Columns15"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("dLookUpEdit2.Properties.Columns16"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("dLookUpEdit2.Properties.Columns17")))),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("dLookUpEdit2.Properties.Columns18"), resources.GetString("dLookUpEdit2.Properties.Columns19"), ((int)(resources.GetObject("dLookUpEdit2.Properties.Columns20"))), ((DevExpress.Utils.FormatType)(resources.GetObject("dLookUpEdit2.Properties.Columns21"))), resources.GetString("dLookUpEdit2.Properties.Columns22"), ((bool)(resources.GetObject("dLookUpEdit2.Properties.Columns23"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("dLookUpEdit2.Properties.Columns24"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("dLookUpEdit2.Properties.Columns25"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("dLookUpEdit2.Properties.Columns26"))))});
            this.cboAR_THB.Properties.NullText = resources.GetString("dLookUpEdit2.Properties.NullText");
            this.cboAR_THB.Properties.PopupWidth = 500;
            this.cboAR_THB.Properties.PopupWidthMode = DevExpress.XtraEditors.PopupWidthMode.ContentWidth;
            this.cboAR_THB.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.cboAR_THB.Required = false;
            // 
            // _lblREQ_AR_THB
            // 
            resources.ApplyResources(this._lblREQ_AR_THB, "_lblREQ_AR_THB");
            this._lblREQ_AR_THB.ForeColor = System.Drawing.Color.Red;
            this._lblREQ_AR_THB.Name = "_lblREQ_AR_THB";
            // 
            // lblAR_THB
            // 
            resources.ApplyResources(this.lblAR_THB, "lblAR_THB");
            this.lblAR_THB.Name = "lblAR_THB";
            // 
            // cboAR_USD
            // 
            resources.ApplyResources(this.cboAR_USD, "cboAR_USD");
            this.cboAR_USD.Name = "cboAR_USD";
            this.cboAR_USD.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("dLookUpEdit1.Properties.Appearance.Font")));
            this.cboAR_USD.Properties.Appearance.Options.UseFont = true;
            this.cboAR_USD.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("dLookUpEdit1.Properties.Buttons"))))});
            this.cboAR_USD.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("dLookUpEdit1.Properties.Columns"), resources.GetString("dLookUpEdit1.Properties.Columns1"), ((int)(resources.GetObject("dLookUpEdit1.Properties.Columns2"))), ((DevExpress.Utils.FormatType)(resources.GetObject("dLookUpEdit1.Properties.Columns3"))), resources.GetString("dLookUpEdit1.Properties.Columns4"), ((bool)(resources.GetObject("dLookUpEdit1.Properties.Columns5"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("dLookUpEdit1.Properties.Columns6"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("dLookUpEdit1.Properties.Columns7"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("dLookUpEdit1.Properties.Columns8")))),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("dLookUpEdit1.Properties.Columns9"), resources.GetString("dLookUpEdit1.Properties.Columns10"), ((int)(resources.GetObject("dLookUpEdit1.Properties.Columns11"))), ((DevExpress.Utils.FormatType)(resources.GetObject("dLookUpEdit1.Properties.Columns12"))), resources.GetString("dLookUpEdit1.Properties.Columns13"), ((bool)(resources.GetObject("dLookUpEdit1.Properties.Columns14"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("dLookUpEdit1.Properties.Columns15"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("dLookUpEdit1.Properties.Columns16"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("dLookUpEdit1.Properties.Columns17")))),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("dLookUpEdit1.Properties.Columns18"), resources.GetString("dLookUpEdit1.Properties.Columns19"), ((int)(resources.GetObject("dLookUpEdit1.Properties.Columns20"))), ((DevExpress.Utils.FormatType)(resources.GetObject("dLookUpEdit1.Properties.Columns21"))), resources.GetString("dLookUpEdit1.Properties.Columns22"), ((bool)(resources.GetObject("dLookUpEdit1.Properties.Columns23"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("dLookUpEdit1.Properties.Columns24"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("dLookUpEdit1.Properties.Columns25"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("dLookUpEdit1.Properties.Columns26"))))});
            this.cboAR_USD.Properties.NullText = resources.GetString("dLookUpEdit1.Properties.NullText");
            this.cboAR_USD.Properties.PopupWidth = 500;
            this.cboAR_USD.Properties.PopupWidthMode = DevExpress.XtraEditors.PopupWidthMode.ContentWidth;
            this.cboAR_USD.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.cboAR_USD.Required = false;
            // 
            // _lblREQ_AR_USD
            // 
            resources.ApplyResources(this._lblREQ_AR_USD, "_lblREQ_AR_USD");
            this._lblREQ_AR_USD.ForeColor = System.Drawing.Color.Red;
            this._lblREQ_AR_USD.Name = "_lblREQ_AR_USD";
            // 
            // lblAR_USD
            // 
            resources.ApplyResources(this.lblAR_USD, "lblAR_USD");
            this.lblAR_USD.Name = "lblAR_USD";
            // 
            // cboAR_KHR
            // 
            resources.ApplyResources(this.cboAR_KHR, "cboAR_KHR");
            this.cboAR_KHR.Name = "cboAR_KHR";
            this.cboAR_KHR.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("cboAR_KHR.Properties.Appearance.Font")));
            this.cboAR_KHR.Properties.Appearance.Options.UseFont = true;
            this.cboAR_KHR.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cboAR_KHR.Properties.Buttons"))))});
            this.cboAR_KHR.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("cboAR_KHR.Properties.Columns"), resources.GetString("cboAR_KHR.Properties.Columns1"), ((int)(resources.GetObject("cboAR_KHR.Properties.Columns2"))), ((DevExpress.Utils.FormatType)(resources.GetObject("cboAR_KHR.Properties.Columns3"))), resources.GetString("cboAR_KHR.Properties.Columns4"), ((bool)(resources.GetObject("cboAR_KHR.Properties.Columns5"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("cboAR_KHR.Properties.Columns6"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("cboAR_KHR.Properties.Columns7"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("cboAR_KHR.Properties.Columns8")))),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("cboAR_KHR.Properties.Columns9"), resources.GetString("cboAR_KHR.Properties.Columns10"), ((int)(resources.GetObject("cboAR_KHR.Properties.Columns11"))), ((DevExpress.Utils.FormatType)(resources.GetObject("cboAR_KHR.Properties.Columns12"))), resources.GetString("cboAR_KHR.Properties.Columns13"), ((bool)(resources.GetObject("cboAR_KHR.Properties.Columns14"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("cboAR_KHR.Properties.Columns15"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("cboAR_KHR.Properties.Columns16"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("cboAR_KHR.Properties.Columns17")))),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("cboAR_KHR.Properties.Columns18"), resources.GetString("cboAR_KHR.Properties.Columns19"), ((int)(resources.GetObject("cboAR_KHR.Properties.Columns20"))), ((DevExpress.Utils.FormatType)(resources.GetObject("cboAR_KHR.Properties.Columns21"))), resources.GetString("cboAR_KHR.Properties.Columns22"), ((bool)(resources.GetObject("cboAR_KHR.Properties.Columns23"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("cboAR_KHR.Properties.Columns24"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("cboAR_KHR.Properties.Columns25"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("cboAR_KHR.Properties.Columns26"))))});
            this.cboAR_KHR.Properties.NullText = resources.GetString("cboAR_KHR.Properties.NullText");
            this.cboAR_KHR.Properties.PopupWidth = 500;
            this.cboAR_KHR.Properties.PopupWidthMode = DevExpress.XtraEditors.PopupWidthMode.ContentWidth;
            this.cboAR_KHR.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.cboAR_KHR.Required = false;
            // 
            // cboAccountIncome
            // 
            resources.ApplyResources(this.cboAccountIncome, "cboAccountIncome");
            this.cboAccountIncome.Name = "cboAccountIncome";
            this.cboAccountIncome.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("cboAccountIncome.Properties.Appearance.Font")));
            this.cboAccountIncome.Properties.Appearance.Options.UseFont = true;
            this.cboAccountIncome.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cboAccountIncome.Properties.Buttons"))))});
            this.cboAccountIncome.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("cboAccountIncome.Properties.Columns"), resources.GetString("cboAccountIncome.Properties.Columns1"), ((int)(resources.GetObject("cboAccountIncome.Properties.Columns2"))), ((DevExpress.Utils.FormatType)(resources.GetObject("cboAccountIncome.Properties.Columns3"))), resources.GetString("cboAccountIncome.Properties.Columns4"), ((bool)(resources.GetObject("cboAccountIncome.Properties.Columns5"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("cboAccountIncome.Properties.Columns6"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("cboAccountIncome.Properties.Columns7"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("cboAccountIncome.Properties.Columns8")))),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("cboAccountIncome.Properties.Columns9"), resources.GetString("cboAccountIncome.Properties.Columns10"), ((int)(resources.GetObject("cboAccountIncome.Properties.Columns11"))), ((DevExpress.Utils.FormatType)(resources.GetObject("cboAccountIncome.Properties.Columns12"))), resources.GetString("cboAccountIncome.Properties.Columns13"), ((bool)(resources.GetObject("cboAccountIncome.Properties.Columns14"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("cboAccountIncome.Properties.Columns15"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("cboAccountIncome.Properties.Columns16"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("cboAccountIncome.Properties.Columns17")))),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("cboAccountIncome.Properties.Columns18"), resources.GetString("cboAccountIncome.Properties.Columns19"), ((int)(resources.GetObject("cboAccountIncome.Properties.Columns20"))), ((DevExpress.Utils.FormatType)(resources.GetObject("cboAccountIncome.Properties.Columns21"))), resources.GetString("cboAccountIncome.Properties.Columns22"), ((bool)(resources.GetObject("cboAccountIncome.Properties.Columns23"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("cboAccountIncome.Properties.Columns24"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("cboAccountIncome.Properties.Columns25"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("cboAccountIncome.Properties.Columns26"))))});
            this.cboAccountIncome.Properties.NullText = resources.GetString("cboAccountIncome.Properties.NullText");
            this.cboAccountIncome.Properties.PopupWidth = 500;
            this.cboAccountIncome.Properties.PopupWidthMode = DevExpress.XtraEditors.PopupWidthMode.ContentWidth;
            this.cboAccountIncome.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.cboAccountIncome.Required = false;
            // 
            // _lblREQ_AR_KHR
            // 
            resources.ApplyResources(this._lblREQ_AR_KHR, "_lblREQ_AR_KHR");
            this._lblREQ_AR_KHR.ForeColor = System.Drawing.Color.Red;
            this._lblREQ_AR_KHR.Name = "_lblREQ_AR_KHR";
            // 
            // lblAR_KHR
            // 
            resources.ApplyResources(this.lblAR_KHR, "lblAR_KHR");
            this.lblAR_KHR.Name = "lblAR_KHR";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Name = "label11";
            // 
            // lblINCOME_ACCOUNT
            // 
            resources.ApplyResources(this.lblINCOME_ACCOUNT, "lblINCOME_ACCOUNT");
            this.lblINCOME_ACCOUNT.Name = "lblINCOME_ACCOUNT";
            // 
            // DialogInvoiceItem
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogInvoiceItem";
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.tabGENERAL.ResumeLayout(false);
            this.tabGENERAL.PerformLayout();
            this.tabACCOUNTING.ResumeLayout(false);
            this.tabACCOUNTING.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboAR_VND.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboAR_THB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboAR_USD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboAR_KHR.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboAccountIncome.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Panel panel1;
        private ExButton btnCHANGE_LOG;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Panel panel2;
        private TabControl tabControl;
        private TabPage tabGENERAL;
        private Label lblRECURRING_MONTH;
        private TextBox txtRecuringMonth;
        private CheckBox chkIS_RECURRING_SERVICE;
        private CheckBox chkIS_EDITABLE;
        private Label label3;
        private Label lblCURRENCY;
        private ComboBox cboCurrency;
        private Label label6;
        private Label label4;
        private Label label9;
        private Label lblUNIT_PRICE;
        private TextBox txtPrice;
        private Label lblSERVICE_TYPE;
        private ComboBox cboInviceItemType;
        private TextBox txtInvoiceItemName;
        private Label lblSERVICE;
        private TabPage tabACCOUNTING;
        private HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit cboAR_KHR;
        private HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit cboAccountIncome;
        private Label _lblREQ_AR_KHR;
        private Label lblAR_KHR;
        private Label label11;
        private Label lblINCOME_ACCOUNT;
        private HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit cboAR_VND;
        private Label _lblREQ_AR_VND;
        private Label lblAR_VND;
        private HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit cboAR_THB;
        private Label _lblREQ_AR_THB;
        private Label lblAR_THB;
        private HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit cboAR_USD;
        private Label _lblREQ_AR_USD;
        private Label lblAR_USD;
    }
}