﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using Login = SoftTech.Security.Logic.Login;

namespace EPower.Interface
{
    public partial class DialogCollectUsageManually : ExDialog
    {
        TBL_METER _objMeter = null;
        TBL_CUSTOMER _objCustomer = null;
        TBL_CUSTOMER_METER _objCustomerMeter = null;
        TBL_USAGE _objUsage = null;

        private bool _loading = false;
        private bool _closeAfterSaveSuccess = false;

        private Guid session_id;

        //private bool _isPrepaidUsage = false;

        public DialogCollectUsageManually()
        {
            InitializeComponent();
            session_id = Guid.NewGuid();
            //_isPrepaidUsage = isPrepaidUsage;
            // enable auto move to next customer
            this.chkAUTO_MOVE_TO_NEXT_CUSTOMER.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_AUTO_MOVE_TO_NEXT_CUSTOMER]);
            this.chkAUTO_MOVE_TO_NEXT_CUSTOMER.Checked = Settings.Default.AUTO_MOVE_TO_NEXT_CUSTOMER;

            // initial data
            UIHelper.SetDataSourceToComboBox(this.cboCycle, DBDataContext.Db.TBL_BILLING_CYCLEs.Where(row => row.IS_ACTIVE));
            UIHelper.SetDataSourceToComboBox(this.cboCollector, (from e in DBDataContext.Db.TBL_EMPLOYEEs
                                                                 join p in DBDataContext.Db.TBL_EMPLOYEE_POSITIONs on e.EMPLOYEE_ID equals p.EMPLOYEE_ID
                                                                 where p.POSITION_ID == (int)EmpPosition.Collector && e.IS_ACTIVE
                                                                 select new { e.EMPLOYEE_ID, e.EMPLOYEE_NAME }));

            this.FormClosing += DialogCollectUsageManually_FormClosing;

            // begin with new entry point
            this.newEntry();
        }

        private void DialogCollectUsageManually_FormClosing(object sender, FormClosingEventArgs e)
        {
            SubmitUsage();
        }

        private void SubmitUsage()
        {
            string ConnectionString = Method.GetConnectionString(Settings.Default.CONNECTION);
            using (var cnn = new SqlConnection(ConnectionString))
            {
                if (cnn.State == ConnectionState.Closed)
                {
                    cnn.Open();
                }
                using (var cmd = new SqlCommand("dbo.SUBMIT_USAGE", cnn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;
                    cmd.Parameters.AddWithValue("@SESSION_ID", session_id);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public DialogCollectUsageManually(int CUSTOMER_ID) : this()
        {

            // mark close after save 
            // when we call from other interface!
            this._closeAfterSaveSuccess = true;

            _objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == CUSTOMER_ID);
            if (_objCustomer == null)
            {
                this.txtCustomerCode.CancelSearch(false);
                MsgBox.ShowInformation(Resources.MS_CUSTOMER_NOT_EXISTS);
                return;
            }
            _objCustomerMeter = DBDataContext.Db.TBL_CUSTOMER_METERs.FirstOrDefault(row => row.CUSTOMER_ID == this._objCustomer.CUSTOMER_ID && row.IS_ACTIVE);
            if (_objCustomerMeter == null)
            {
                this.txtCustomerCode.CancelSearch(false);
                MsgBox.ShowInformation(Resources.MS_CUSTOMER_NOT_ACTIVATE);
                return;
            }
            _objMeter = DBDataContext.Db.TBL_METERs.FirstOrDefault(row => row.METER_ID == this._objCustomerMeter.METER_ID);
            if (_objMeter == null)
            {
                this.txtCustomerCode.CancelSearch(false);
                MsgBox.ShowInformation(Resources.MS_CUSTOMER_NOT_ACTIVATE);
                return;
            }
            this.read();
        }

        private void read()
        {
            this._loading = true;
            DBDataContext.Db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues);
            // accpet entry.
            this.txtCustomerCode.AcceptSearch(false);
            this.txtCustomerName.AcceptSearch(false);
            this.txtMeter.AcceptSearch(false);


            // show customer information.
            this.txtCustomerName.Text = this._objCustomer.LAST_NAME_KH + " " + this._objCustomer.FIRST_NAME_KH;
            this.txtCustomerCode.Text = this._objCustomer.CUSTOMER_CODE;
            this.txtMeter.Text = this._objMeter.METER_CODE;
            this.txtArea.Text = DBDataContext.Db.TBL_AREAs.FirstOrDefault(row => row.AREA_ID == this._objCustomer.AREA_ID).AREA_CODE;
            this.txtPole.Text = DBDataContext.Db.TBL_POLEs.FirstOrDefault(row => row.POLE_ID == this._objCustomerMeter.POLE_ID).POLE_CODE;
            this.txtBox.Text = DBDataContext.Db.TBL_BOXes.FirstOrDefault(row => row.BOX_ID == this._objCustomerMeter.BOX_ID).BOX_CODE;

            // cycle
            this.cboCycle.SelectedValue = _objCustomer.BILLING_CYCLE_ID;

            // months, start and end
            DateTime dateMonth = UIHelper._DefaultDate,
                        dateStart = UIHelper._DefaultDate,
                        dateEnd = UIHelper._DefaultDate;
            dateMonth = Method.GetNextBillingMonth(_objCustomer.BILLING_CYCLE_ID, ref dateStart, ref dateEnd);
            this.dtpMonth.Value = dateMonth;
            this.dtpStart.SetValue(dateStart);
            this.dtpEnd.SetValue(dateEnd);

            // collector
            TBL_POLE objPole = DBDataContext.Db.TBL_POLEs.FirstOrDefault(row => row.POLE_ID == this._objCustomerMeter.POLE_ID);
            this.cboCollector.SelectedValue = objPole.COLLECTOR_ID;

            // load meter and select last meter usage
            this.loadMeters(dateMonth);

            // load Usage as of meter and month
            this.loadUsage(dateMonth, (int)cboMeter.SelectedValue);

            //Load Usage History
            this.LoadUsageHistory();

            // sending focus to end usage
            this.txtEndUsage.SelectAll();
            this.txtEndUsage.Focus();

            this._loading = false;
        }

        /// <summary>
        /// Load all meters and select last meter usage
        /// </summary>
        /// <param name="month"></param>
        private void loadMeters(DateTime dateMonth)
        {
            DataTable meters = (from u in DBDataContext.Db.TBL_USAGEs
                                join m in DBDataContext.Db.TBL_METERs on u.METER_ID equals m.METER_ID
                                where u.CUSTOMER_ID == this._objCustomer.CUSTOMER_ID
                                      && u.USAGE_MONTH == dateMonth
                                orderby u.USAGE_ID
                                select new { m.METER_ID, m.METER_CODE }).Distinct()._ToDataTable();

            //var DD = from u in DBDataContext.Db.TBL_USAGEs
            //          join m in DBDataContext.Db.TBL_METERs on u.METER_ID equals m.METER_ID
            //          where u.CUSTOMER_ID == this._objCustomer.CUSTOMER_ID
            //                && u.USAGE_MONTH == dateMonth
            //          orderby u.USAGE_ID
            //          select new { m.METER_ID, m.METER_CODE };

            if (meters.Rows.Count == 0)
            {
                DataRow row = meters.NewRow();
                row[0] = this._objMeter.METER_ID;
                row[1] = this._objMeter.METER_CODE;
                meters.Rows.Add(row);
            }
            // bind avialable meters found in current month usage
            UIHelper.SetDataSourceToComboBox(this.cboMeter, meters);

            // select last usage meter for the current month
            //this.cboMeter.SelectedIndex = this.cboMeter.Items.Count - 1;
            this.cboMeter.SelectedValue = this._objCustomerMeter.METER_ID;
        }

        private void loadUsage(DateTime datMonth, int METER_ID)
        {
            // check if usage already run
            if (DBDataContext.Db.TBL_RUN_BILLs.Count(row => row.CYCLE_ID == (int)this.cboCycle.SelectedValue && row.BILLING_MONTH == datMonth && row.IS_ACTIVE) > 0)
            {
                this.txtEndUsage.ReadOnly = true;
                this.txtStartUsage.ReadOnly = true;
                this.chkIS_NEW_CYCLE.Enabled = false;
                this.btnSAVE.Enabled = false;
            }
            else
            {
                this.txtMultiplier.Text = DBDataContext.Db.TBL_METERs.FirstOrDefault(row => row.METER_ID == METER_ID).MULTIPLIER.ToString();
                this.txtEndUsage.ReadOnly = !Login.IsAuthorized(Permission.POSTPAID_INPUTUSAGE_ENDUSAGE);
                this.txtStartUsage.ReadOnly = !Login.IsAuthorized(Permission.POSTPAID_INPUTUSAGE_STARTUSAGE);
                this.chkIS_NEW_CYCLE.Enabled = true;
                this.btnSAVE.Enabled = true;
            }

            this._objUsage = DBDataContext.Db.TBL_USAGEs.Where(row => row.CUSTOMER_ID == this._objCustomer.CUSTOMER_ID
                                                                            && row.METER_ID == METER_ID
                                                                            && row.USAGE_MONTH == datMonth)
                                                                            .OrderByDescending(row => row.USAGE_ID).FirstOrDefault();

            var isPendingInTempTable = false;
            //load from temp usage
            if (this._objUsage == null)
            {
                var old_temp_usage = DBDataContext.Db.TEMP_INSERT_USAGEs.Where(row => row.CUSTOMER_ID == this._objCustomer.CUSTOMER_ID
                                                                           && row.METER_ID == METER_ID
                                                                           && row.USAGE_MONTH == datMonth)
                                                                           .OrderByDescending(row => row.ID).FirstOrDefault();

                if (old_temp_usage != null)
                {
                    isPendingInTempTable = true;
                    this.txtStartUsage.Text = old_temp_usage.START_USAGE > 0 ? old_temp_usage.START_USAGE.ToString(UIHelper._DefaultUsageFormat) : "0";
                    this.txtEndUsage.Text = old_temp_usage.END_USAGE.ToString(UIHelper._DefaultUsageFormat);
                    this.txtMultiplier.Text = old_temp_usage.MULTIPLIER.ToString();
                    this.chkIS_NEW_CYCLE.Checked = old_temp_usage.IS_METER_RENEW_CYCLE;
                    this.txtTotalUsage.Text = this.calcUsage().ToString(UIHelper._DefaultUsageFormat);
                }
            }

            if (this._objUsage != null)
            {
                this.txtStartUsage.Text = this._objUsage.START_USAGE > 0 ? this._objUsage.START_USAGE.ToString(UIHelper._DefaultUsageFormat) : "0";
                this.txtEndUsage.Text = this._objUsage.END_USAGE.ToString(UIHelper._DefaultUsageFormat);
                this.txtMultiplier.Text = _objUsage.MULTIPLIER.ToString();
                this.chkIS_NEW_CYCLE.Checked = this._objUsage.IS_METER_RENEW_CYCLE;
                this.txtTotalUsage.Text = this.calcUsage().ToString(UIHelper._DefaultUsageFormat);
            }
            else
            {
                if (!isPendingInTempTable)
                {
                    TBL_USAGE objLastUsage = (from u in DBDataContext.Db.TBL_USAGEs
                                              where u.METER_ID == METER_ID
                                                     && u.USAGE_MONTH == datMonth.AddMonths(-1)
                                                     && u.CUSTOMER_ID == this._objCustomer.CUSTOMER_ID
                                              orderby u.USAGE_ID descending
                                              select u).FirstOrDefault();
                    if (objLastUsage != null)
                    {
                        this.txtStartUsage.Text = objLastUsage.END_USAGE > 0 ? objLastUsage.END_USAGE.ToString(UIHelper._DefaultUsageFormat) : "0";
                    }
                    else
                    {
                        this.txtStartUsage.Text = "0";
                    }
                    this.txtEndUsage.Text = "";
                    this.txtTotalUsage.Text = "";
                    this.chkIS_NEW_CYCLE.Checked = false;
                }
            }
            // if there are no collector
            // or not input usage then 
            // load default collector
            if (this.cboCollector.SelectedIndex != -1)
            {
                if (this._objCustomerMeter != null)
                {
                    TBL_POLE objPole = DBDataContext.Db.TBL_POLEs.FirstOrDefault(row => row.POLE_ID == this._objCustomerMeter.POLE_ID);
                    if (objPole != null)
                    {
                        this.cboCollector.SelectedValue = objPole.COLLECTOR_ID;
                    }
                }
            }
        }

        private void LoadUsageHistory()
        {
            if (chk12_MONTHS_USAGE_HISTORY.Checked && _objCustomer != null)
            {
                lbl12_MONTHS_USAGE_HISTORY0.Text = string.Format(DBDataContext.Db.GetHistoryUsage(_objCustomer.CUSTOMER_ID), Environment.NewLine);
            }
        }


        private decimal calcUsage()
        {
            if (cboMeter.SelectedIndex == -1)
            {
                return 0;
            }
            decimal start = DataHelper.ParseToDecimal(this.txtStartUsage.Text);
            decimal end = DataHelper.ParseToDecimal(this.txtEndUsage.Text);
            decimal mult = DataHelper.ParseToDecimal(this.txtMultiplier.Text);
            bool isMeterRenewCycle = this.chkIS_NEW_CYCLE.Checked;
            return Method.GetTotalUsage(start, end, mult, isMeterRenewCycle);
        }


        private void newEntry()
        {
            this.txtCustomerCode.Select();
            this._objCustomer = null;
            this._objCustomerMeter = null;
            this._objMeter = null;
            this._objUsage = null;

            this.txtCustomerCode.CancelSearch(false);
            this.txtCustomerName.CancelSearch(false);
            this.txtMeter.CancelSearch(false);

            this.txtCustomerCode.Text = "";
            this.txtCustomerName.Text = "";
            this.txtMeter.Text = "";
            this.txtArea.Text = "";
            this.txtPole.Text = "";
            this.txtBox.Text = "";

            this.dtpEnd.ClearValue();
            this.dtpStart.ClearValue();

            this.txtStartUsage.Text = "";
            this.txtEndUsage.Text = "";
            this.txtMultiplier.Text = "";
            this.txtTotalUsage.Text = "";
            this.chkIS_NEW_CYCLE.Checked = false;

            this.cboCollector.SelectedIndex = -1;
            this.cboCycle.SelectedIndex = -1;
            this.cboMeter.DataSource = null;

            this.txtCustomerCode.Focus();

            this.lbl12_MONTHS_USAGE_HISTORY0.Text = "";
        }
        private void nextCustomer()
        {
            string currentCustomer = this.txtArea.Text + " " + this.txtPole.Text + " " + this.txtBox.Text + " " + _objCustomerMeter.POSITION_IN_BOX.ToString().PadLeft(6, '0') + " " + this.txtCustomerCode.Text;
            var objCustomer = (from c in DBDataContext.Db.TBL_CUSTOMERs
                               join cm in DBDataContext.Db.TBL_CUSTOMER_METERs on c.CUSTOMER_ID equals cm.CUSTOMER_ID
                               join b in DBDataContext.Db.TBL_BOXes on cm.BOX_ID equals b.BOX_ID
                               join p in DBDataContext.Db.TBL_POLEs on b.POLE_ID equals p.POLE_ID
                               join a in DBDataContext.Db.TBL_AREAs on p.AREA_ID equals a.AREA_ID
                               where cm.IS_ACTIVE
                                     && (c.STATUS_ID == (int)CustomerStatus.Active || c.STATUS_ID == (int)CustomerStatus.Blocked)
                                     && (a.AREA_CODE + " " + p.POLE_CODE + " " + b.BOX_CODE + " " + cm.POSITION_IN_BOX.ToString().PadLeft(6, '0') + " " + c.CUSTOMER_CODE).CompareTo(currentCustomer) > 0
                               orderby a.AREA_CODE + " " + p.POLE_CODE + " " + b.BOX_CODE + " " + cm.POSITION_IN_BOX.ToString().PadLeft(6, '0') + " " + c.CUSTOMER_CODE
                               select new
                               {
                                   c.CUSTOMER_ID
                               }).FirstOrDefault();
            if (objCustomer != null)
            {
                _objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == objCustomer.CUSTOMER_ID);
                if (_objCustomer == null)
                {
                    this.txtCustomerCode.CancelSearch(false);
                    MsgBox.ShowInformation(Resources.MS_CUSTOMER_NOT_EXISTS);
                    return;
                }
                _objCustomerMeter = DBDataContext.Db.TBL_CUSTOMER_METERs.FirstOrDefault(row => row.CUSTOMER_ID == this._objCustomer.CUSTOMER_ID && row.IS_ACTIVE);
                if (_objCustomerMeter == null)
                {
                    this.txtCustomerCode.CancelSearch(false);
                    MsgBox.ShowInformation(Resources.MS_CUSTOMER_NOT_ACTIVATE);
                    return;
                }
                _objMeter = DBDataContext.Db.TBL_METERs.FirstOrDefault(row => row.METER_ID == this._objCustomerMeter.METER_ID);
                if (_objMeter == null)
                {
                    this.txtCustomerCode.CancelSearch(false);
                    MsgBox.ShowInformation(Resources.MS_CUSTOMER_NOT_ACTIVATE);
                    return;
                }
                this.read();
            }
            else
            {
                this.newEntry();
            }
        }

        private void txtMeter_AdvanceSearch(object sender, EventArgs e)
        {
            // if not entry.
            if (txtMeter.Text.Trim() == "")
            {
                this.txtMeter.CancelSearch(false);
                return;
            }

            string strMeterCode = Method.FormatMeterCode(this.txtMeter.Text);
            this._objMeter = DBDataContext.Db.TBL_METERs.FirstOrDefault(m => m.METER_CODE == strMeterCode);


            // if not contain in database.
            if (this._objMeter == null)
            {
                MsgBox.ShowInformation(Resources.MS_METER_CODE_NOT_FOUND);
                this.txtMeter.CancelSearch(false);
                return;
            }

            // if meter is inuse.
            if (this._objMeter.STATUS_ID != (int)MeterStatus.Used)
            {
                MsgBox.ShowInformation(Resources.MS_METER_IS_UNAVAILABLE);
                this.txtMeter.CancelSearch(false);
                return;
            }

            this._objCustomerMeter = DBDataContext.Db.TBL_CUSTOMER_METERs.FirstOrDefault(row => row.IS_ACTIVE && row.METER_ID == this._objMeter.METER_ID);

            this._objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == this._objCustomerMeter.CUSTOMER_ID && row.IS_POST_PAID);

            this.read();
        }
        private void txtCustomerCode_AdvanceSearch(object sender, EventArgs e)
        {
            using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
            {
                if (this.txtCustomerCode.Text == "")
                {
                    this.txtCustomerCode.CancelSearch(false);
                    return;
                }
                _objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_CODE == Method.FormatCustomerCode(this.txtCustomerCode.Text));


                if (_objCustomer == null)
                {
                    this.txtCustomerCode.CancelSearch(false);
                    MsgBox.ShowInformation(Resources.MS_CUSTOMER_NOT_EXISTS);
                    return;
                }

                _objCustomerMeter = DBDataContext.Db.TBL_CUSTOMER_METERs.FirstOrDefault(row => row.CUSTOMER_ID == this._objCustomer.CUSTOMER_ID && row.IS_ACTIVE);
                if (_objCustomerMeter == null)
                {
                    this.txtCustomerCode.CancelSearch(false);
                    MsgBox.ShowInformation(Resources.MS_CUSTOMER_NOT_ACTIVATE);
                    return;
                }
                _objMeter = DBDataContext.Db.TBL_METERs.FirstOrDefault(row => row.METER_ID == this._objCustomerMeter.METER_ID);
                if (_objMeter == null)
                {
                    this.txtCustomerCode.CancelSearch(false);
                    MsgBox.ShowInformation(Resources.MS_CUSTOMER_NOT_ACTIVATE);
                    return;
                }

                this.read();
                tran.Complete();
            }
        }

        private void txtCustomerName_AdvanceSearch(object sender, EventArgs e)
        {
            DialogCustomerSearch diag = new DialogCustomerSearch(this.txtCustomerName.Text, DialogCustomerSearch.PowerType.AllType);
            if (diag.ShowDialog() != DialogResult.OK)
            {
                this.txtCustomerName.CancelSearch(false);
                return;
            }
            this._objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == diag.CustomerID);
            if (_objCustomer == null)
            {
                this.txtCustomerName.CancelSearch(false);
                return;
            }
            _objCustomerMeter = DBDataContext.Db.TBL_CUSTOMER_METERs.FirstOrDefault(row => row.CUSTOMER_ID == this._objCustomer.CUSTOMER_ID && row.IS_ACTIVE);
            if (_objCustomerMeter == null)
            {
                this.txtCustomerName.CancelSearch(false);
                MsgBox.ShowInformation(Resources.MS_CUSTOMER_NOT_ACTIVATE);
                return;
            }
            _objMeter = DBDataContext.Db.TBL_METERs.FirstOrDefault(row => row.METER_ID == this._objCustomerMeter.METER_ID);
            if (_objMeter == null)
            {
                this.txtCustomerName.CancelSearch(false);
                MsgBox.ShowInformation(Resources.MS_CUSTOMER_NOT_ACTIVATE);
                return;
            }
            this.read();
        }

        private void txtMeter_CancelAdvanceSearch(object sender, EventArgs e)
        {
            this.newEntry();
        }

        private void txtCustomerCode_CancelAdvanceSearch(object sender, EventArgs e)
        {
            this.newEntry();
        }

        private void txtCustomerName_CancelAdvanceSearch(object sender, EventArgs e)
        {
            this.newEntry();
        }

        private void dtpMonth_ValueChanged(object sender, EventArgs e)
        {
            if (!_loading && cboCycle.SelectedIndex != -1)
            {
                TBL_BILLING_CYCLE objCycle = DBDataContext.Db.TBL_BILLING_CYCLEs.FirstOrDefault(b => b.CYCLE_ID == (int)cboCycle.SelectedValue);
                if (objCycle != null)
                {
                    DateTime datStart,
                             datEnd,
                             datMonth = new DateTime(this.dtpMonth.Value.Year, this.dtpMonth.Value.Month, 1);

                    //if run end of month
                    if (objCycle.IS_END_MONTH)
                    {
                        datStart = new DateTime(dtpMonth.Value.Year, dtpMonth.Value.Month, 1);
                        datEnd = datStart.AddMonths(1).AddDays(-1);
                    }
                    else
                    {
                        datStart = new DateTime(dtpMonth.Value.Year, dtpMonth.Value.Month, objCycle.END_DAY).AddDays(1);
                        datEnd = new DateTime(datStart.AddMonths(1).Year, datStart.AddMonths(1).Month, objCycle.END_DAY);
                    }

                    this.dtpStart.SetValue(datStart);
                    this.dtpEnd.SetValue(datEnd);

                    // load meter and select last meter usage
                    this.loadMeters(datMonth);

                    // load Usage as of meter and month
                    if (this.cboMeter.SelectedIndex != -1)
                    {
                        this.loadUsage(datMonth, (int)cboMeter.SelectedValue);
                    }
                }

            }
        }

        private void cboMeter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_loading && this.cboMeter.SelectedIndex != -1)
            {
                // load Usage as of meter and month
                DateTime datMonth = new DateTime(this.dtpMonth.Value.Year, this.dtpMonth.Value.Month, 1);
                this.loadUsage(datMonth, (int)cboMeter.SelectedValue);
            }
        }

        private void txtEndUsage_TextChanged(object sender, EventArgs e)
        {
            this.txtTotalUsage.Text = this.calcUsage().ToString(UIHelper._DefaultUsageFormat);
        }

        private void chkNewMeterCycle_CheckedChanged(object sender, EventArgs e)
        {
            this.txtTotalUsage.Text = this.calcUsage().ToString(UIHelper._DefaultUsageFormat);
        }

        TextBox textBoxToRead = null;
        private void txtStartUsage_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;

            // focus control will read
            this.textBoxToRead = (TextBox)sender;

            // read once!
            this.lblReader_.Text = DataHelper.NumberToWord(DataHelper.ParseToDecimal(this.textBoxToRead.Text));

            // repeate read every changed!
            this.textBoxToRead.TextChanged += delegate (object obj, EventArgs evt)
            {
                this.lblReader_.Text = DataHelper.NumberToWord(DataHelper.ParseToDecimal(this.textBoxToRead.Text));
            };
        }

        private void InputEnglish(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        public void InputKhmer(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void DialogCollectUsageManually_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                // if user select a customer than
                if (this._objCustomer != null)
                {
                    this.newEntry();
                }
                else
                {
                    this.Close();
                }
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.save();
        }

        private void save()
        {

            this.ClearAllValidation();

            if (this._objCustomer == null)
            {
                this.txtCustomerCode.SetValidation(string.Format(Resources.REQUIRED, this.lblCUSTOMER_CODE.Text));
                return;
            }
            if (this._objMeter == null)
            {
                this.txtCustomerCode.SetValidation(string.Format(Resources.REQUIRED, this.lblMETER_CODE.Text));
                return;
            }
            if (!DataHelper.IsNumber(this.txtStartUsage.Text))
            {
                this.txtStartUsage.SetValidation(string.Format(Resources.REQUIRED, this.lblSTART_USAGE.Text));
                return;
            }

            if (!DataHelper.IsNumber(this.txtEndUsage.Text))
            {
                this.txtEndUsage.SetValidation(string.Format(Resources.REQUIRED, this.lblEND_USAGE.Text));
                return;
            }

            if (DataHelper.ParseToDecimal(txtStartUsage.Text) < 0)
            {
                this.txtStartUsage.SetValidation(Resources.REQUIRED_POSITIVE_NUMBER);
                return;
            }
            if (DataHelper.ParseToDecimal(txtEndUsage.Text) < 0)
            {
                this.txtEndUsage.SetValidation(Resources.REQUIRED_POSITIVE_NUMBER);
                return;
            }

            if (DataHelper.ParseToDecimal(this.txtStartUsage.Text) > DataHelper.ParseToDecimal(this.txtEndUsage.Text) != this.chkIS_NEW_CYCLE.Checked)
            {
                this.txtEndUsage.SetValidation(Resources.MS_METER_IS_NEW_CYCLE);
                return;
            }

            if (cboCollector.SelectedIndex == -1)
            {
                this.cboCollector.SetValidation(string.Format(Resources.REQUIRED, this.lblCOLLECTOR.Text));
                return;
            }

            var MeterId = (int)cboMeter.SelectedValue;
            var CollectorId = (int)cboCollector.SelectedValue;
            var StartUsage = DataHelper.ParseToDecimal(this.txtStartUsage.Text);
            var EndUsage = DataHelper.ParseToDecimal(this.txtEndUsage.Text);
            var Multiplier = DataHelper.ParseToInt(this.txtMultiplier.Text);

            try
            {
                Runner.RunNewThread(() =>
                {
                    using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted, Timeout = TimeSpan.MaxValue }))
                    {
                        DateTime now = DBDataContext.Db.GetSystemDate();
                        DateTime month = new DateTime(this.dtpMonth.Value.Year, this.dtpMonth.Value.Month, 1);

                        TBL_USAGE objUsage = DBDataContext.Db.TBL_USAGEs.Where(row => row.CUSTOMER_ID == this._objCustomer.CUSTOMER_ID
                                                                                       && row.METER_ID == MeterId
                                                                                       && row.USAGE_MONTH == dtpMonth.Value).FirstOrDefault();

                        TEMP_INSERT_USAGE old_temp_usage = null;
                        if (objUsage == null)
                        {

                            old_temp_usage = DBDataContext.Db.TEMP_INSERT_USAGEs.Where(row => row.CUSTOMER_ID == this._objCustomer.CUSTOMER_ID
                                                                                      && row.METER_ID == MeterId
                                                                                      && row.USAGE_MONTH == dtpMonth.Value)
                                                                                       .OrderByDescending(row => row.ID).FirstOrDefault();
                            if (old_temp_usage != null)
                            {
                                // backup old object.  
                                // usage on temp table.
                                TEMP_INSERT_USAGE objOld = new TEMP_INSERT_USAGE();
                                old_temp_usage._CopyTo(objOld);
                                old_temp_usage.START_USAGE = StartUsage;
                                old_temp_usage.END_USAGE = EndUsage;
                                old_temp_usage.IS_METER_RENEW_CYCLE = this.chkIS_NEW_CYCLE.Checked;
                                old_temp_usage.MULTIPLIER = Multiplier;
                                old_temp_usage.COLLECTOR_ID = CollectorId;
                                old_temp_usage.CREATE_BY = Login.CurrentLogin.LOGIN_NAME;
                                old_temp_usage.POSTING_DATE = now;
                                DBDataContext.Db.UpdateWithoutChangeLog(objOld, old_temp_usage);
                            }
                            else
                            {
                                DBDataContext.Db.InsertWithoutChangeLog(new TEMP_INSERT_USAGE()
                                {
                                    COLLECTOR_ID = CollectorId,
                                    CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                                    CUSTOMER_ID = this._objCustomer.CUSTOMER_ID,
                                    DEVICE_ID = 0,
                                    END_USAGE = EndUsage,
                                    END_USE_DATE = this.dtpEnd.Value,
                                    IS_METER_RENEW_CYCLE = this.chkIS_NEW_CYCLE.Checked,
                                    METER_ID = this._objMeter.METER_ID,
                                    POSTING_DATE = now,
                                    START_USAGE = StartUsage,
                                    START_USE_DATE = this.dtpStart.Value,
                                    SESSION_ID = session_id,
                                    // USAGE_ID = 0,
                                    USAGE_MONTH = month,
                                    MULTIPLIER = Multiplier
                                });
                            }
                        }
                        else
                        {
                            // backup old object.
                            TBL_USAGE objOld = new TBL_USAGE();
                            objUsage._CopyTo(objOld);
                            // update endusage and new meter cycle
                            objUsage.START_USAGE = StartUsage;
                            objUsage.END_USAGE = EndUsage;
                            objUsage.IS_METER_RENEW_CYCLE = this.chkIS_NEW_CYCLE.Checked;
                            objUsage.MULTIPLIER = Multiplier;
                            objUsage.COLLECTOR_ID = CollectorId;
                            objUsage.CREATE_BY = Login.CurrentLogin.LOGIN_NAME;
                            objUsage.POSTING_DATE = now;
                            //DBDataContext.Db.ChangeConflicts.ResolveAll(System.Data.Linq.RefreshMode.OverwriteCurrentValues);
                            DBDataContext.Db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues);
                            DBDataContext.Db.Update(objOld, objUsage);
                        }
                        DBDataContext.Db.SubmitChanges();                        
                        tran.Complete();
                    }
                });
            }
            catch (Exception ex)
            {
                MsgBox.LogError(ex);
                throw new Exception(string.Format(Base.Properties.Resources.YOU_CANNOT_PROCESS, "", Base.Properties.Resources.COLLECT_USAGE_MANUALLY));
            }

            if (this._closeAfterSaveSuccess)
            {
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                if (this.chkAUTO_MOVE_TO_NEXT_CUSTOMER.Checked && this.chkAUTO_MOVE_TO_NEXT_CUSTOMER.Visible)
                {
                    this.nextCustomer();
                }
                else
                {
                    this.newEntry();
                }
            }
        }

        private void txtEndUsage_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                this.save();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnNoUsage_Click(object sender, EventArgs e)
        {
            // get now usage
            new DialogCustomerNoUsage(0).ShowDialog();
        }

        private void DialogCollectUsageManually_Activated(object sender, EventArgs e)
        {
            if (this._objCustomer != null)
            {
                this.txtEndUsage.Focus();
            }
        }

        private void bntSpecialUsage_Click(object sender, EventArgs e)
        {
            //After input it all user may view usage => submit to make view usage correct.
            SubmitUsage();
            var diag = new DialogCustomerSpecialUsage();
            diag.ShowDialog();
        }

        private void cboAUTO_MOVE_TO_NEXT_CUSTOMER_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Default.AUTO_MOVE_TO_NEXT_CUSTOMER = this.chkAUTO_MOVE_TO_NEXT_CUSTOMER.Checked;
            Settings.Default.Save();
        }

        private void txtStartUsage_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        }

        private void txtEndUsage_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        }

        private void chk12_MONTHS_USAGE_HISTORY_CheckedChanged(object sender, EventArgs e)
        {
            grp12_MONTHS_USAGE_HISTORY.Visible = chk12_MONTHS_USAGE_HISTORY.Checked;
            LoadUsageHistory();
        }

        private void btnUSAGE_HISTORY_Click(object sender, EventArgs e)
        {
            if (_objCustomer == null)
            {
                this.txtCustomerCode.CancelSearch(false);
                MsgBox.ShowInformation(Resources.MS_NO_SELECT_CUSTOMER);
                return;
            }

            TBL_USAGE objUsage = DBDataContext.Db.TBL_USAGEs.Where(row => row.CUSTOMER_ID == this._objCustomer.CUSTOMER_ID
                                                                                   && row.METER_ID == (int)cboMeter.SelectedValue
                                                                                   && row.USAGE_MONTH == dtpMonth.Value)
                                                                                   .OrderByDescending(row => row.USAGE_ID).FirstOrDefault();
            if (objUsage == null)
            {
                MsgBox.ShowInformation(Resources.NO_USAGE_HISTORY_FOUND, Resources.NO_USAGE);
                return;
            }
            else
            {
                new DialogAdjustUsageHistory(DBDataContext.Db.TBL_USAGEs.FirstOrDefault(x => x.USAGE_ID == objUsage.USAGE_ID)).ShowDialog();
            }
        }
    }
}
