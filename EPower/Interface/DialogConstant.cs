﻿using System;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;

namespace EPower.Interface
{
    public partial class DialogConstant : ExDialog
    {
        GeneralProcess _flag ;
        TBL_CONSTANT _objConstant = new TBL_CONSTANT();
        public TBL_CONSTANT Constant
        {
            get { return _objConstant; }
        }
        TBL_CONSTANT _oldObjContant = new TBL_CONSTANT();

        #region Constructor
        public DialogConstant(GeneralProcess flag, TBL_CONSTANT objConstant)
        {
            InitializeComponent();
            _flag = flag;
            objConstant._CopyTo(_objConstant);
            objConstant._CopyTo(_oldObjContant);
            if (flag == GeneralProcess.Insert)
            {
                _objConstant.IS_ACTIVE = true;
                this.Text = string.Concat(Resources.INSERT, this.Text);
            }
            else if (flag == GeneralProcess.Update)
            {
                this.Text = string.Concat(Resources.UPDATE, this.Text);
            }
            else if (flag == GeneralProcess.Delete)
            {
                this.Text = string.Concat(Resources.DELETE, this.Text);
                UIHelper.SetEnabled(this, false);
            }

            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
            read();
        }
        #endregion

        #region Method
        /// <summary>
        /// Read data from object.
        /// </summary>
        private void read()
        {
            txtContant.Text = _objConstant.CONSTANT_NAME;
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            _objConstant.CONSTANT_NAME = txtContant.Text.Trim();
        }

        private bool invalid()
        {
            bool val = false;
            txtContant.ClearValidation();

            if (txtContant.Text.Trim()==string.Empty)
            {
                txtContant.SetValidation(string.Format(Resources.REQUIRED, lblCONSTANT.Text));
                val = true;
            }

            return val;
        } 
        #endregion

        #region Operation
        /// <summary>
        /// Data transaction
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (invalid())
            {
                return;
            }
            write();
            //If record is duplicate.
            txtContant.ClearValidation();
            if (DBDataContext.Db.IsExits(_objConstant, "CONSTANT_NAME"))
            {
                txtContant.SetValidation(string.Format(Resources.MS_IS_EXISTS,lblCONSTANT.Text));
                return;
            }
            try
            {
                using (TransactionScope tran=new TransactionScope( TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objConstant);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_oldObjContant, _objConstant);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objConstant);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }                                                
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }            
        }

        /// <summary>
        /// User read only
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        #endregion

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objConstant);
        }
    }
}