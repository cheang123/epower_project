﻿using SoftTech.Component;
using System;

namespace EPower.Interface.BankPayment
{
    public partial class DialogRemindPrePayment : ExDialog
    {
        public DialogRemindPrePayment()
        {
            InitializeComponent();
        }

        private void btnCLOSE_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnPRE_PAYMENT_Click(object sender, EventArgs e)
        {
            DialogCustomerPrePayment dig = new DialogCustomerPrePayment();
            var result = dig.ShowDialog();
        }
    }
}
