﻿using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;

namespace EPower.Interface
{
    public partial class DialogRunBillHistory : ExDialog
    {
        TBL_RUN_BILL objRun = new TBL_RUN_BILL();
        DateTime _invoiceDate,
                 _startDate,
                 _endDate,
                 _dueDate,
                 _startPayDate = new DateTime();
        public DialogRunBillHistory(int runId)
        {
            InitializeComponent();

            this.dtpInvoiceDate.Enabled = false;
            UIHelper.SetDataSourceToComboBox(this.cboBillingCycle, DBDataContext.Db.TBL_BILLING_CYCLEs.Where(row => row.IS_ACTIVE));
            
            this.objRun = DBDataContext.Db.TBL_RUN_BILLs.FirstOrDefault(row => row.RUN_ID == runId);

            var objInvoice = DBDataContext.Db.TBL_INVOICEs.FirstOrDefault(row => row.RUN_ID == objRun.RUN_ID && !row.IS_SERVICE_BILL);
            if (objInvoice != null)
            {
                this.dtpDueDate.Value = objInvoice.DUE_DATE;
                this.dtpFrom.Value = objInvoice.START_DATE;
                this.dtpTo.Value = objInvoice.END_DATE;
                this.dtpSTART_PAY_DATE.Value = objInvoice.START_PAY_DATE;
                this.dtpInvoiceDate.Value = objInvoice.INVOICE_DATE;
                _invoiceDate = objInvoice.INVOICE_DATE;
                _startDate = objInvoice.START_DATE;
                _endDate = objInvoice.END_DATE;
                _startPayDate = objInvoice.START_PAY_DATE;
                _dueDate = objInvoice.DUE_DATE;
            }
            this.dtpRunMonth.Value = objRun.BILLING_MONTH;
            this.cboBillingCycle.SelectedValue = objRun.CYCLE_ID;
        } 

        private void btnOK_Click(object sender, EventArgs e)
        {
            DateTime dueDate = this.dtpDueDate.Value,
                     fromDate = this.dtpFrom.Value,
                     toDate = this.dtpTo.Value,
                     invoiceDate=this.dtpInvoiceDate.Value,
                     startPayDate = this.dtpSTART_PAY_DATE.Value;

            Runner.RunNewThread(delegate()
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    foreach (var objInv in DBDataContext.Db.TBL_INVOICEs.Where(row => row.RUN_ID == objRun.RUN_ID))
                    {
                        objInv.DUE_DATE = dueDate;
                        objInv.START_DATE = fromDate;
                        objInv.END_DATE = toDate;
                        objInv.START_PAY_DATE=startPayDate;
                        objInv.INVOICE_DATE = invoiceDate;

                    }
                    DBDataContext.Db.SubmitChanges();

                    TBL_RUN_BILL_HISTORY_CHANGE objRunBillHistoryChange = DBDataContext.Db.TBL_RUN_BILL_HISTORY_CHANGEs.FirstOrDefault(x => x.RUN_ID == objRun.RUN_ID);
                    if (objRunBillHistoryChange == null)
                    {
                        TBL_RUN_BILL_HISTORY_CHANGE _objRunBillHistory = new TBL_RUN_BILL_HISTORY_CHANGE()
                        {
                            RUN_ID=objRun.RUN_ID, 
                            DUE_DATE = _dueDate,
                            START_DATE = _startDate,
                            END_DATE = _endDate,
                            START_PAY_DATE = _startPayDate,
                            INVOICE_DATE = _invoiceDate
                        };
                        DBDataContext.Db.TBL_RUN_BILL_HISTORY_CHANGEs.InsertOnSubmit(_objRunBillHistory);
                        DBDataContext.Db.SubmitChanges(); 
                    }

                    TBL_RUN_BILL_HISTORY_CHANGE objNew = DBDataContext.Db.TBL_RUN_BILL_HISTORY_CHANGEs.FirstOrDefault(x => x.RUN_ID == objRun.RUN_ID);
                    TBL_RUN_BILL_HISTORY_CHANGE objOld = new TBL_RUN_BILL_HISTORY_CHANGE();
                    objNew._CopyTo(objOld);

                    objNew.DUE_DATE = dueDate;
                    objNew.START_DATE = fromDate;
                    objNew.END_DATE = toDate;
                    objNew.START_PAY_DATE = startPayDate;
                    objNew.INVOICE_DATE = invoiceDate;
                    DBDataContext.Db.Update(objOld, objNew);
                    DBDataContext.Db.SubmitChanges();

                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            });
        }
          

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnHistory_Click(object sender, EventArgs e)
        {
            TBL_RUN_BILL_HISTORY_CHANGE objNew = DBDataContext.Db.TBL_RUN_BILL_HISTORY_CHANGEs.FirstOrDefault(x => x.RUN_ID == objRun.RUN_ID);
            if (objNew != null)
            {
                DialogChangeLog.ShowChangeLog(objNew);
            }
            else
            {
                MsgBox.ShowInformation(Resources.MS_NO_INFORMATION_CHANGE);
            }
            
        }
         
 
    }
}
