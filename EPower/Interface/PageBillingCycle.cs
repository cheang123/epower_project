﻿using System;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PageBillingCycle : Form
    {        
        public TBL_BILLING_CYCLE BillingCycle
        {
            get
            {
                TBL_BILLING_CYCLE objCycle = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    try
                    {
                        int cycleID = (int)dgv.SelectedRows[0].Cells[CYCLE_ID.Name].Value;
                        objCycle = DBDataContext.Db.TBL_BILLING_CYCLEs.FirstOrDefault(x => x.CYCLE_ID == cycleID);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objCycle;
            }
        }

        #region Constructor
        public PageBillingCycle()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            txt_QuickSearch(null, null);
        }
        #endregion

        #region Operation
        /// <summary>
        /// Add new ampare.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNew_Click(object sender, EventArgs e)
        {            
            DialogBillingCycle dig = new DialogBillingCycle(GeneralProcess.Insert, new TBL_BILLING_CYCLE() {  CYCLE_NAME = "", COLLECT_DAY=30,IS_END_MONTH=true });
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.BillingCycle.CYCLE_ID);
            }
        }

        /// <summary>
        /// Edit ampare
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                DialogBillingCycle dig = new DialogBillingCycle(GeneralProcess.Update, BillingCycle);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.BillingCycle.CYCLE_ID);
                }
            }
        }

        /// <summary>
        /// Remove ampare
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exButton1_Click(object sender, EventArgs e)
        {
            if (IsDeletable())
            {
                DialogBillingCycle dig = new DialogBillingCycle(GeneralProcess.Delete, BillingCycle);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.BillingCycle.CYCLE_ID - 1);
                }
            }
        }

        /// <summary>
        /// Load data from database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            try
            {
                dgv.DataSource = (from cy in DBDataContext.Db.TBL_BILLING_CYCLEs
                                 where cy.IS_ACTIVE &&
                                 cy.CYCLE_NAME.ToLower().Contains(txtQuickSearch.Text.ToLower().Trim())
                                 orderby cy.CYCLE_NAME
                                 select new
                                 {
                                     cy.CYCLE_ID,
                                     cy.CYCLE_NAME
                                 }).ToList();
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }
        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }
        #endregion

        #region Method
        private bool IsDeletable()
        {
            bool val = false;
            if (dgv.SelectedRows.Count > 0)
            {
                val = true;   
                if (DBDataContext.Db.TBL_CUSTOMERs.Count(c => c.BILLING_CYCLE_ID == BillingCycle.CYCLE_ID) > 0)
                {
                    MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE, Resources.INFORMATION);
                    val = false;
                }             
            }
            return val;
        }
        #endregion

        
    }
}
