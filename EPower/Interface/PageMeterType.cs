﻿using System;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PageMeterType : Form
    {
        public TBL_METER_TYPE MeterType
        {
            get
            {
                TBL_METER_TYPE objMeterType = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    try
                    {
                        int meterTypeID = (int)dgv.SelectedRows[0].Cells["METER_TYPE_ID"].Value;
                        objMeterType = DBDataContext.Db.TBL_METER_TYPEs.FirstOrDefault(x => x.METER_TYPE_ID == meterTypeID);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objMeterType;
            }
        }

        #region Constructor
        public PageMeterType()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            txt_QuickSearch(null, null);
        }
        #endregion

        #region Operation
        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }
        private void btnNew_Click(object sender, EventArgs e)
        {
            DialogMeterType dig = new DialogMeterType(GeneralProcess.Insert, new TBL_METER_TYPE() { METER_TYPE_CODE = "" });
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.MeterType.METER_TYPE_ID);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                DialogMeterType dig = new DialogMeterType(GeneralProcess.Update, MeterType);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.MeterType.METER_TYPE_ID);
                }
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (IsDeletable())
            {
                DialogMeterType dig = new DialogMeterType(GeneralProcess.Delete, MeterType);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.MeterType.METER_TYPE_ID-1);
                }
            }
        }

        private void txt_QuickSearch(object sender, EventArgs e)
        {
            try
            {
                dgv.DataSource = from mt in DBDataContext.Db.TBL_METER_TYPEs
                                 where mt.IS_ACTIVE &&
                                 (mt.METER_TYPE_CODE.ToLower().Contains(txtQuickSearch.Text.ToLower().Trim()) ||
                                  mt.METER_TYPE_NAME.ToLower().Contains(txtQuickSearch.Text.ToLower().Trim()))
                                 select new
                                 {
                                     mt.METER_TYPE_ID,
                                     mt.METER_TYPE_CODE,
                                     mt.METER_TYPE_NAME
                                 };
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }        
        #endregion

        #region Method
        private bool IsDeletable()
        {
  
            bool val = false;
            if (dgv.SelectedRows.Count > 0)
            {
                val = true;
                if (DBDataContext.Db.TBL_METERs.Where(x=>x.METER_TYPE_ID==MeterType.METER_TYPE_ID).Count()>0)
                {
                    val = false;
                    MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE);
                }
            }
            return val;
        }
        #endregion

        
    }
}