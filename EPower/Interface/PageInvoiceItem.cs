﻿using System;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PageInvoiceItem : Form
    {
        int currencyId = 0;
        public int CurrencyID
        {
            get { return currencyId; }
            private  set { currencyId = value; }
        }

        int itemType = 0;

        public int ItemTypeID
        {
            get { return itemType; }
            private set { itemType = value; }
        }
        public TBL_INVOICE_ITEM InvoiceItem
        {
            get
            {
                TBL_INVOICE_ITEM objItem = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    try
                    {
                        int itemID = (int)dgv.SelectedRows[0].Cells[INVOICE_ITEM_ID.Name].Value;
                        objItem = DBDataContext.Db.TBL_INVOICE_ITEMs.FirstOrDefault(x => x.INVOICE_ITEM_ID == itemID);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objItem;
            }
        }

        #region Constructor
        public PageInvoiceItem()
        {
            InitializeComponent();
            VALUE.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            UIHelper.DataGridViewProperties(dgv);

            txt_QuickSearch(null, null);
        }
        #endregion

        #region Operation
        /// <summary>
        /// Add new ampare.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNew_Click(object sender, EventArgs e)
        {
            TBL_INVOICE_ITEM objItem = new TBL_INVOICE_ITEM() { INVOICE_ITEM_NAME = "" };
            if (cboItemType.SelectedIndex!=-1)
            {
                objItem.INVOICE_ITEM_TYPE_ID = (int)cboItemType.SelectedValue;
            }
            if (cboItemType.SelectedIndex!=-1)
            {
                objItem.CURRENCY_ID = (int)cboCurrency.SelectedValue;
            }

            DialogInvoiceItem dig = new DialogInvoiceItem(GeneralProcess.Insert, objItem);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                SetSelectedRow(dig.InvioceItem.INVOICE_ITEM_ID);
               // UIHelper.SelectRow(dgv, dig.InvioceItem.INVOICE_ITEM_ID);
            } 
        }

        private void SetSelectedRow(int Id)
        {
            foreach (DataGridViewRow row in dgv.Rows)
            {
                // 0 is the column index
                if (DataHelper.ParseToInt(row.Cells[INVOICE_ITEM_ID.Name].Value?.ToString()??"0") == Id)
                {
                    row.Selected = true;
                    break;
                }
            }
        }

        /// <summary>
        /// Edit ampare
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                DialogInvoiceItem dig = new DialogInvoiceItem(GeneralProcess.Update, InvoiceItem);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    SetSelectedRow(dig.InvioceItem.INVOICE_ITEM_ID);
                    //  UIHelper.SelectRow(dgv, dig.InvioceItem.INVOICE_ITEM_ID);
                }
            }
        }

        /// <summary>
        /// Remove ampare
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (IsDeletable())
            {
                if (InvoiceItem.INVOICE_ITEM_ID<0 || InvoiceItem.INVOICE_ITEM_ID==3)
                {
                    MsgBox.ShowInformation(Resources.MS_IS_DEFAULT_INVOICE_SERVICE);
                    return;
                }
                DialogInvoiceItem dig = new DialogInvoiceItem(GeneralProcess.Delete, InvoiceItem);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.InvioceItem.INVOICE_ITEM_ID - 1);
                }
            }
        }

        /// <summary>
        /// Load data from database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            try
            {                
                if (cboCurrency.SelectedIndex!=-1)
                {
                    currencyId = (int)cboCurrency.SelectedValue;
                }
                if (cboItemType.SelectedIndex!=-1)
                {
                    itemType = (int)cboItemType.SelectedValue;
                }
                dgv.DataSource = (from it in DBDataContext.Db.TBL_INVOICE_ITEMs
                                 join ivt in DBDataContext.Db.TBL_INVOICE_ITEM_TYPEs on it.INVOICE_ITEM_TYPE_ID equals ivt.INVOICE_ITEM_TYPE_ID
                                 join c in DBDataContext.Db.TLKP_CURRENCies on it.CURRENCY_ID equals c.CURRENCY_ID
                                 join a in DBDataContext.Db.TBL_ACCOUNT_CHARTs on it.INCOME_ACCOUNT_ID equals a.ACCOUNT_ID
                                 where (it.IS_ACTIVE && !it.IS_ONLY_IN_BILLING && ivt.IS_ACTIVE) &&
                                 (itemType == 0 || it.INVOICE_ITEM_TYPE_ID == itemType) &&
                                 (currencyId == 0 || it.CURRENCY_ID == currencyId) &&
                                 (it.INVOICE_ITEM_NAME.ToLower().Contains(txtQuickSearch.Text.ToLower().Trim()))
                                 select new
                                 {
                                     it.INVOICE_ITEM_TYPE_ID,
                                     it.INVOICE_ITEM_ID,
                                     it.INVOICE_ITEM_NAME,
                                     it.PRICE,
                                     ivt.INVOICE_ITEM_TYPE_NAME,  
                                     c.CURRENCY_SING,
                                     INCOME_ACCOUNT_NAME = a.ACCOUNT_CODE+" "+a.ACCOUNT_NAME,
                                     it.IS_RECURRING_SERVICE,
                                 }).OrderBy(x=>x.INVOICE_ITEM_TYPE_ID).ThenBy(x=>x.INVOICE_ITEM_ID).ToList();                 
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }         
        }
        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }
        #endregion

        #region Method
        private bool IsDeletable()
        {
            //TODO: can delete record.
            bool val = false;
            if (dgv.SelectedRows.Count > 0)
            {
                val = true;
                
            }
            return val;
        }

        public void loadCurrency()
        {
            int tempCurrency =  CurrencyID;
            try
            {
                UIHelper.SetDataSourceToComboBox(cboCurrency,
                from c in DBDataContext.Db.TLKP_CURRENCies
                orderby c.IS_DEFAULT_CURRENCY
                select new
                {
                    c.CURRENCY_ID,
                    c.CURRENCY_NAME
                },Resources.ALL_CURRENCY);

                cboCurrency.SelectedValue = tempCurrency;
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        public void loadInvoiceItemType()
        {
            int tempItemType = ItemTypeID;
            try
            {
                UIHelper.SetDataSourceToComboBox(cboItemType,
                    from t in DBDataContext.Db.TBL_INVOICE_ITEM_TYPEs
                    where t.IS_ACTIVE
                    select new
                    {
                        t.INVOICE_ITEM_TYPE_ID,
                        t.INVOICE_ITEM_TYPE_NAME
                    }, Resources.ALL_TYPE);
                cboItemType.SelectedValue = tempItemType;
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }
        #endregion

        

       
    }
}
