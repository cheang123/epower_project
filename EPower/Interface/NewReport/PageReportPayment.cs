﻿using DevExpress.XtraBars;
using DevExpress.XtraPivotGrid;
using EPower.Base.Helper;
using EPower.Base.Helper.DevExpressCustomize;
using EPower.Base.Logic;
using EPower.Base.Models;
using EPower.Base.Properties;
using EPower.Logic;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageReportPayment : Form
    {
        List<TLKP_FILTER_OPTION> options = new List<TLKP_FILTER_OPTION>();
        private List<TLKP_CURRENCY> _Currencies;
        private bool IsLoading;
        private string _title = "";
        private string title = "ការទទួលប្រាក់";
        private string _TemplateDetail = "ការទទួលប្រាក់លម្អិត";
        private string _TemplateCashDrawer = "តាមកេះដាក់ប្រាក់";
        private string _TemplateTypeCustomer = "តាមប្រភេទអតិ -សេវា";
        private string _TemplatePaymentNo = "តាមបង្កាន់ដៃ";
        DataTable _data;
        List<ReportPaymentModel> _reportData = new List<ReportPaymentModel>();

        public PageReportPayment()
        {
            InitializeComponent();
            ResourceHelper.ApplyResource(this);
            this.IsLoading = true;
            //Reload();
            ApplySetting();
            _Currencies = DBDataContext.Db.TLKP_CURRENCies.ToList();
            this.Load += PageReportPayment_Load;
            pgvPaymentDetail.CustomCellValue += PgvPaymentDetail_CustomCellValue;
            pgvPaymentDetail.FieldFilterChanged += PgvPaymentDetail_FieldFilterChanged;
            pgvPaymentDetail.CustomCellDisplayText += PgvPaymentDetail_CustomCellDisplayText;
            pgvPaymentDetail.FieldValueDisplayText += PgvPaymentDetail_FieldValueDisplayText;
            this.btnSAVE_TEMPLATE.ItemClick += btnSAVE_TEMPLATE_ItemClick;
            this.VisibleChanged += PageReportPayment_VisibleChanged; ;
            pgvPaymentDetail.CustomFieldSort += PgvPaymentDetail_CustomFieldSort;
            pgvPaymentDetail.EndRefresh += PgvPaymentDetail_EndRefresh;
            this.RestoreUserConfigure();
            colTYPE_OF_TRANSACTION.TotalsVisibility = PivotTotalsVisibility.AutomaticTotals;
            CustomValueTotal();
            CustomBind();
            _title = title;
        }

        private void CustomValueTotal()
        {
            colROW_NO.Appearance.Cell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            colROW_NO.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            colROW_NO.Appearance.Value.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

            colCUSTOMER_CODE.Appearance.Cell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            colCUSTOMER_CODE.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            colCUSTOMER_CODE.Appearance.Value.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

            colCUSTOMER_NAME.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

            colDESCRIPTION.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

            colREFERENCE.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

            colINVOICE_DATE_BILLING.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            colINVOICE_DATE_BILLING.Appearance.Value.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

            colPAID_DATE.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            colPAID_DATE.Appearance.Value.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

            colRECEIVE_BY.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            colRECEIVE_BY.Appearance.Value.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

            colAREA.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

            colPAID_TIME.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            colPAID_TIME.Appearance.Value.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

            colPAYMENT_NO.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            colPAYMENT_NO.Appearance.Value.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

           
            colCurrency.Appearance.ValueTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            colCurrency.Appearance.ValueTotal.Font = new Font("Khmer OS System", 9f, FontStyle.Bold);

            colTYPE_OF_TRANSACTION.Appearance.ValueTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            colTYPE_OF_TRANSACTION.Appearance.ValueTotal.Font = new Font("Khmer OS System", 9f, FontStyle.Bold);

            colCASH_DRAWER_NAME.Appearance.ValueTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            colCASH_DRAWER_NAME.Appearance.ValueTotal.Font = new Font("Khmer OS System", 9f, FontStyle.Bold);

            colCUSTOMER_TYPE.Appearance.ValueTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            colCUSTOMER_TYPE.Appearance.ValueTotal.Font = new Font("Khmer OS System", 9f, FontStyle.Bold);

            colINVOICE_ITEM.Appearance.ValueTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            colINVOICE_ITEM.Appearance.ValueTotal.Font = new Font("Khmer OS System", 9f, FontStyle.Bold);

            colTOTAL_PAY.Appearance.Value.Font = new Font("Khmer OS System", 8.25f, FontStyle.Regular);
            colTOTAL_PAY.Appearance.ValueGrandTotal.Font = new Font("Khmer OS System", 8.25f, FontStyle.Regular);
            colTOTAL_PAY.Appearance.Header.Font = new Font("Khmer OS System", 8.25f, FontStyle.Regular);
        }

        private void PgvPaymentDetail_CustomFieldSort(object sender, PivotGridCustomFieldSortEventArgs e)
        {
            int value1 = Convert.ToInt32(e.GetListSourceColumnValue(e.ListSourceRowIndex1, "ID"));
            int value2 = Convert.ToInt32(e.GetListSourceColumnValue(e.ListSourceRowIndex2, "ID"));
            e.Result = Comparer.Default.Compare(value1, value2);
            e.Handled = true;
        }

        private int groupIndex = 0;
        private void PgvPaymentDetail_FieldValueDisplayText(object sender, PivotFieldDisplayTextEventArgs e)
        {
            if (e.DataField == null)
            {
                return;
            }
            if (e.Field == null)
            {
                return;
            }

            if (e.MaxIndex == 0)
            {
                groupIndex = 0;
            }
            if (e.ValueType == PivotGridValueType.GrandTotal || e.ValueType == PivotGridValueType.CustomTotal || e.ValueType == PivotGridValueType.Total)
            {
                if (e.Value != null)
                {
                    var newGroup = e.Value.ToString();
                    groupIndex += 1;
                }
            }
            if (e.Field.Name == colROW_NO.Name)
            {
                if (e.MinIndex >= 0 && e.MinIndex == e.MaxIndex)
                {

                    e.DisplayText = (e.MinIndex + 1 - groupIndex).ToString();
                }
            }
        }

        private void PageReportPayment_VisibleChanged(object sender, EventArgs e)
        {
            if (!this.Visible)
            {
                this.SaveUserConfigure();
            }
        }

        private void btnSAVE_TEMPLATE_ItemClick(object sender, ItemClickEventArgs e)
        {
            DialogTemplate dialog = new DialogTemplate(pgvPaymentDetail.ActiveFilterString, this.Name.ToUpper());
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                var file = pgvPaymentDetail.SaveUserConfigureToXml(dialog.txtTemplateName.Text);
                TLKP_FILTER_OPTION obj = dialog._objFilter;
                obj.XML_FILE = File.ReadAllBytes(file);
                DBDataContext.Db.SubmitChanges();
                BindAction();
            }
        }
        public void BindAction()
        {
            options = DBDataContext.Db.TLKP_FILTER_OPTIONs.Where(x => x.IS_ACTIVE && x.GRID_NAME == this.Name.ToUpper()).ToList();
            var font = new Font("Khmer OS System", 8.24f);
            foreach (var item in options)
            {
                if (!popActions.ItemLinks.Where(x => x.Caption == $"បង្ហាញទម្រង់៖ {item.FILTER_NAME}").Any())
                {
                    BarButtonItem btnRemove = new BarButtonItem();
                    btnRemove.Caption = Resources.REMOVE;
                    btnRemove.Tag = item.FILTER_ID;
                    btnRemove.ItemClick += btnRemove_ItemClick;


                    var btnFilter = new BarSubItem();
                    btnFilter.Caption = $"បង្ហាញទម្រង់៖ {item.FILTER_NAME}";
                    btnFilter.Tag = item.FILTER_ID;
                    if (!item.IS_SYSTEM)
                    {
                        barManager1.Items.Add(btnRemove);
                        btnFilter.AddItem(btnRemove);
                    }
                    popActions.AddItem(btnFilter);
                    btnFilter.ItemClick += btnFilter_ItemClick;
                    btnFilter.MenuAppearance.AppearanceMenu.SetFont(font);
                }
            }
            barManager1.DockWindowTabFont = font;
            popActions.MenuAppearance.AppearanceMenu.SetFont(font);
        }
        private void btnFilter_ItemClick(object sender, ItemClickEventArgs e)
        {
            var filter = options.FirstOrDefault(x => x.FILTER_ID == (int)e.Item.Tag);
            if (filter == null)
            {
                return;
            }

            pgvPaymentDetail.RefreshData();
            pgvPaymentDetail.Refresh();
            pgvPaymentDetail.DataSource.Refresh();
            this.pgvPaymentDetail.ActiveFilterString = filter.FILTER_OPTION;
            this.popActions.HidePopup();

            var file = pgvPaymentDetail.RestoreUserConfigureFromXml(filter.FILTER_NAME);
            if (!File.Exists(file) && filter.XML_FILE != null)
            {
                var baseDirectory = Path.Combine(Application.StartupPath, "UserConfigures");
                file = Path.Combine(baseDirectory, $"{pgvPaymentDetail.Name}.{filter.FILTER_NAME}.xml");
                File.WriteAllBytes(file, filter.XML_FILE.ToArray());
                pgvPaymentDetail.RestoreUserConfigureFromXml(filter.FILTER_NAME);

            }
            if (filter.FILTER_NAME == _TemplateDetail)
            {
                colTYPE_OF_TRANSACTION.TotalsVisibility = PivotTotalsVisibility.AutomaticTotals;
                colCUSTOMER_TYPE.TotalsVisibility = PivotTotalsVisibility.None;
                colINVOICE_ITEM.TotalsVisibility = PivotTotalsVisibility.None;
                colCASH_DRAWER_NAME.TotalsVisibility = PivotTotalsVisibility.None;
                _title = _TemplateDetail;
            }
            else if (filter.FILTER_NAME == _TemplateCashDrawer)
            {
                colCASH_DRAWER_NAME.TotalsVisibility = PivotTotalsVisibility.AutomaticTotals;
                colCUSTOMER_TYPE.TotalsVisibility = PivotTotalsVisibility.None;
                colINVOICE_ITEM.TotalsVisibility = PivotTotalsVisibility.None;
                colTYPE_OF_TRANSACTION.TotalsVisibility = PivotTotalsVisibility.None;
                _title = title +  _TemplateCashDrawer;
            }
            else if (filter.FILTER_NAME == _TemplateTypeCustomer)
            {
                colCUSTOMER_TYPE.TotalsVisibility = PivotTotalsVisibility.AutomaticTotals;
                colINVOICE_ITEM.TotalsVisibility = PivotTotalsVisibility.AutomaticTotals;
                colTYPE_OF_TRANSACTION.TotalsVisibility = PivotTotalsVisibility.None;
                colCASH_DRAWER_NAME.TotalsVisibility = PivotTotalsVisibility.None;
                _title = title + _TemplateTypeCustomer;
            }
            else if (filter.FILTER_NAME == _TemplatePaymentNo)
            {
                colTYPE_OF_TRANSACTION.TotalsVisibility = PivotTotalsVisibility.AutomaticTotals;
                colCUSTOMER_TYPE.TotalsVisibility = PivotTotalsVisibility.None;
                colINVOICE_ITEM.TotalsVisibility = PivotTotalsVisibility.None;
                colCASH_DRAWER_NAME.TotalsVisibility = PivotTotalsVisibility.None;
                _title = title + _TemplatePaymentNo;
            }
            GetReportName();
            _lblTitle.Text = _title;
            pgvPaymentDetail.ExpandAll();
            CustomValueTotal();
        }
        private void btnRemove_ItemClick(object sender, ItemClickEventArgs e)
        {
            var filter = options.FirstOrDefault(x => x.FILTER_ID == (int)e.Item.Tag);
            if (filter == null)
            {
                return;
            }
            filter.IS_ACTIVE = false;
            DBDataContext.Db.SubmitChanges();
            popActions.RemoveLink(popActions.ItemLinks.ToList().FirstOrDefault(x => x.Caption == $"បង្ហាញទម្រង់៖ {filter.FILTER_NAME}"));
        }


        private void ApplySetting()
        {
            var companyInfo = DBDataContext.Db.TBL_COMPANies.FirstOrDefault();
            _lblCompany.Text = companyInfo.COMPANY_NAME;
            _lblAddress.Text = companyInfo.ADDRESS;
            _picLogo.Image = UIHelper.ConvertBinaryToImage(companyInfo.COMPANY_LOGO);
            _lblCompany.AutoSize = false;
            _lblAddress.AutoSize = false;
            _lblCompany.Size = new System.Drawing.Size(300, 30);
            _lblAddress.Size = new System.Drawing.Size(300, 50);
            _lblTitle.Text = _TemplateDetail;
            _lblSubTitle1.Text = $"{Resources.START_DATE} : {rdpDate.FromDate.ToShortDate()} {Resources.TO} : {rdpDate.ToDate.ToShortDate()} ";
            pgvPaymentDetail.SetDefaultPivotview();
            dpExport.ToolTip = Resources.ShowAsExportFuction;
            dpAction.ToolTip = Resources.ShowAsResetTemplatesFunction;
            this.RestoreUserConfigure();
            colPAID_DATE.FormatShortDate();
            colDUE_DATE.FormatShortDate();
            colBANK_CUT_OFF_DATE.FormatShortDate();
            colTRANSACTION_DATE.FormatShortDate();
            colINVOICE_DATE_BILLING.FormatShortDate();
            colPAID_TIME.FormatTime();
            colTOTAL_PAID.FormatViewNumeric();
            colTOTAL_PAY.FormatViewNumeric();
            colTOTAL_VOID.FormatViewNumeric();
            colNUMBER_OF_ROW.FormatNumeric();
        }

        private void rdpDate_ValueChanged(object sender, EventArgs e)
        {
            Reload();
            colPAYMENT_METHOD.CollapseAll();
        }
        private void PgvPaymentDetail_EndRefresh(object sender, EventArgs e)
        {
            this.SaveUserConfigure();
        }


        private void PgvPaymentDetail_CustomCellDisplayText(object sender, PivotCellDisplayTextEventArgs e)
        {

            var value = e.GetFieldValue(colCurrency)?.ToString() ?? "";
            if (string.IsNullOrEmpty(value))
            {
                return;
            }
            var currency = _Currencies.FirstOrDefault(x => x.CURRENCY_CODE == value);
            if (currency == null)
            {
                return;
            }
            if (
                  e.DataField.FieldName == colTOTAL_PAY.FieldName || e.DataField.FieldName == colTOTAL_PAID.FieldName ||
                  e.DataField.FieldName == colTOTAL_VOID.FieldName || e.RowField.FieldName == colPAYMENT_TYPE_GROUP.FieldName ||
                  e.DataField.FieldName == colPAYMENT_TYPE_GROUP.FieldName || e.RowValueType == PivotGridValueType.Total ||
                  e.RowValueType == PivotGridValueType.GrandTotal)
            {
                e.DisplayText = Convert.ToDecimal(e.Value).ToString(currency.FORMAT);
            }
        }

        private void PgvPaymentDetail_FieldFilterChanged(object sender, PivotFieldEventArgs e)
        {
            if (e.Field.FieldName == colCurrency.FieldName)
            {
               // VisibleTotalRecord();
            }
        }
        private void VisibleTotalRecord()
        {
            var checkCurrency = pgvPaymentDetail.Fields[colCurrency.FieldName].FilterValues.Values.Count();
            if (checkCurrency == 1)
            {
                pgvPaymentDetail.Fields[colPAYMENT_TYPE_GROUP.FieldName].Options.ShowTotals = true;
                pgvPaymentDetail.Fields[colPAYMENT_TYPE_GROUP.FieldName].Options.ShowGrandTotal = true;

                pgvPaymentDetail.Fields[colCurrency.FieldName].Options.ShowTotals = true;
                pgvPaymentDetail.Fields[colCurrency.FieldName].Options.ShowGrandTotal = true;

                pgvPaymentDetail.Fields[colPAID_DATE.FieldName].Options.ShowTotals = false;
                pgvPaymentDetail.Fields[colPAID_DATE.FieldName].Options.ShowGrandTotal = false;

                pgvPaymentDetail.Fields[colPAYMENT_METHOD.FieldName].Options.ShowTotals = false;
                pgvPaymentDetail.Fields[colPAYMENT_METHOD.FieldName].Options.ShowGrandTotal = false;

                pgvPaymentDetail.Fields[colTOTAL_PAY.FieldName].Options.ShowTotals = true;
                pgvPaymentDetail.Fields[colTOTAL_PAY.FieldName].Options.ShowGrandTotal = true;

                pgvPaymentDetail.Fields[colTOTAL_PAID.FieldName].Options.ShowTotals = true;
                pgvPaymentDetail.Fields[colTOTAL_PAID.FieldName].Options.ShowGrandTotal = true;

                pgvPaymentDetail.Fields[colTOTAL_VOID.FieldName].Options.ShowTotals = true;
                pgvPaymentDetail.Fields[colTOTAL_VOID.FieldName].Options.ShowGrandTotal = true;
            }
            else
            {
                pgvPaymentDetail.Fields[colPAYMENT_TYPE_GROUP.FieldName].Options.ShowTotals = false;
                pgvPaymentDetail.Fields[colPAYMENT_TYPE_GROUP.FieldName].Options.ShowGrandTotal = false;

                pgvPaymentDetail.Fields[colCurrency.FieldName].Options.ShowTotals = false;
                pgvPaymentDetail.Fields[colCurrency.FieldName].Options.ShowGrandTotal = false;

                pgvPaymentDetail.Fields[colPAID_DATE.FieldName].Options.ShowTotals = false;
                pgvPaymentDetail.Fields[colPAID_DATE.FieldName].Options.ShowGrandTotal = false;

                pgvPaymentDetail.Fields[colPAYMENT_METHOD.FieldName].Options.ShowTotals = false;
                pgvPaymentDetail.Fields[colPAYMENT_METHOD.FieldName].Options.ShowGrandTotal = false;

                pgvPaymentDetail.Fields[colTOTAL_PAY.FieldName].Options.ShowTotals = false;
                pgvPaymentDetail.Fields[colTOTAL_PAY.FieldName].Options.ShowGrandTotal = false;

                pgvPaymentDetail.Fields[colTOTAL_PAID.FieldName].Options.ShowTotals = false;
                pgvPaymentDetail.Fields[colTOTAL_PAID.FieldName].Options.ShowGrandTotal = false;

                pgvPaymentDetail.Fields[colTOTAL_VOID.FieldName].Options.ShowTotals = false;
                pgvPaymentDetail.Fields[colTOTAL_VOID.FieldName].Options.ShowGrandTotal = false;
            }
        }

        private void PgvPaymentDetail_CustomCellValue(object sender, PivotCellValueEventArgs e)
        {
            //if (pgvPaymentDetail.Fields[colCurrency.FieldName].FilterValues.ValuesIncluded.Count() > 1)
            //{
            //    if (e.RowValueType == PivotGridValueType.Total || e.RowValueType == PivotGridValueType.GrandTotal)
            //    {
            //        e.Value = null;
            //    }
            //}
        }

        private void SaveUserConfigure()
        {
            pgvPaymentDetail.SaveUserConfigureToXml();
        }
        private void RestoreUserConfigure()
        {
            pgvPaymentDetail.RestoreUserConfigureFromXml();
        }
        private void Reload()
        {
            if (this.IsLoading) return;
            var d1 = rdpDate.FromDate.Date;
            var d2 = rdpDate.ToDate.Date.AddDays(1).AddMilliseconds(-1);
            var param = new ReportPaymentSearchParam()
            {
                DATE_PAID_FROM = d1,
                DATE_PAID_TO = d2,
            };
           
            _lblSubTitle1.Text = $"{Resources.START_DATE} : {d1.ToShortDate()} {Resources.TO} : {d2.ToShortDate()} ";

            Runner.RunNewThread(delegate ()
            {
                try
                {
                    var dcResult = ReportLogic.getPaymentReportData(param).FirstOrDefault();
                    _data = dcResult.Value;
                    _reportData = dcResult.Key;
                    pgvPaymentDetail.DataSource = _reportData;
                }
                catch(Exception ex)
                {
                    MsgBox.LogError(ex);
                    throw new Exception(Resources.YOU_CANNOT_VIEW_REPORT);
                }
            });
        }
        private string GetReportName()
        {
            var dateExport = DateTime.Now.ToString("_yyyyMMdd");
            var defaultFileName = $"{_title}{dateExport}";
            return defaultFileName;
        }
        private void CustomBind()
        {
            //When View
            colROW_NO.Width = 45;
            colCurrency.Width = 100;
            colCUSTOMER_CODE.Width = 100;
            colCUSTOMER_NAME.Width = 150;
            colTYPE_OF_TRANSACTION.Width = 150;
            colCUSTOMER_TYPE.Width = 150;
            colINVOICE_ITEM.Width = 100;
            colINVOICE_DATE_BILLING.Width = 150;
            colREFERENCE.Width = 120;
            colPAID_DATE.Width = 130;
            colRECEIVE_BY.Width = 120;
            colPAID_TIME.Width = 120;
            colDESCRIPTION.Width = 150;
            colCASH_DRAWER_NAME.Width = 120;
            colAREA.Width = 100;
            colTOTAL_PAY.Width = 130;
            colPAYMENT_NO.Width = 120;
        }

        private void CustomPrint()
        {
            //When print
            colROW_NO.Width = 35;
            colCurrency.Width = 30;
            colCUSTOMER_CODE.Width = 50;
            colCUSTOMER_NAME.Width = 80;
            colTYPE_OF_TRANSACTION.Width = 70;
            colCUSTOMER_TYPE.Width = 80;
            colINVOICE_ITEM.Width = 80;
            colINVOICE_DATE_BILLING.Width = 70;
            colREFERENCE.Width = 90;
            colPAID_DATE.Width = 70;
            colRECEIVE_BY.Width = 50;
            colPAID_TIME.Width = 70;
            colDESCRIPTION.Width = 120;
            colCASH_DRAWER_NAME.Width = 150;
            colAREA.Width = 130;
            colTOTAL_PAY.Width = 90;
            colPAYMENT_NO.Width = 80;
        }
        private void btnPRINT_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            CustomPrint();
            DevExtension.ShowGridPreview(pgvPaymentDetail, _title, GetReportDescription(), false);
            CustomBind();
        }
        private void btnExcel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DevExtension.ToExcel(pgvPaymentDetail, _title, GetReportDescription(), GetReportName(), false);
        }
        private void btnCsv_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DevExtension.ToCsv(pgvPaymentDetail, _title, GetReportDescription(), GetReportName(), false);
        }

        private void btnPdf_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            CustomPrint();
            DevExtension.ToPdf(pgvPaymentDetail, _title, GetReportDescription(), GetReportName(), false);
            CustomBind();
        }
        private string GetReportDescription()
        {
            var description = $"{Resources.FROMDATE} {rdpDate.FromDate.ToDevShortDate()} {Resources.TODATE} {rdpDate.ToDate.ToDevShortDate()} ";
            return description;
        }

        private void pgvPaymentDetail_CellDoubleClick(object sender, PivotCellEventArgs e)
        {
            var payAtBank = e.GetFieldValue(colPAYMENT_TYPE_GROUP)?.ToString() ?? "";
            if (payAtBank == Resources.PAY_AT_BANK)
            {
                var paymentMethod = e.GetFieldValue(colPAY_AT)?.ToString() ?? "";
                new DialogPaymentSetting(paymentMethod).ShowDialog();
            }
        }

        private void PageReportPayment_Load(object sender, EventArgs e)
        {
            var timer = new Timer();
            timer.Interval = 500;
            timer.Start();
            timer.Tick += (object sender1, EventArgs e1) =>
            {
                timer.Stop();
                this.IsLoading = !this.Created;
                Reload();
            };
            BindAction();
        }

        private void pgvPaymentDetail_DragDrop(object sender, DragEventArgs e)
        {
            groupIndex = 0;
        }
    }
}
