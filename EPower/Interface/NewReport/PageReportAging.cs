﻿using DevExpress.XtraBars;
using DevExpress.XtraPivotGrid;
using EPower.Base.Helper;
using EPower.Base.Helper.Component;
using EPower.Base.Helper.DevExpressCustomize;
using EPower.Base.Logic;
using EPower.Base.Models;
using EPower.Base.Properties;
using EPower.Logic;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EPower.Interface.NewReport
{
    public partial class PageReportAging : Form
    { 
        private string _DefaultTemplateView = "DefaultTemplateAgingReport.xml";
        private string _defaultSummary = "បំណុលតាមកាលសង្ខេប";
        private string _defaultDetail = "បំណុលតាមកាលលម្អិត";
        private string _title = Resources.REPORT_AGING;

        private List<TLKP_CURRENCY> _Currencies;
        private bool IsLoading;
        private string Filed;
        DataTable _data;
        List<ReportAgingModel> _reportData = new List<ReportAgingModel>();
        List<TLKP_FILTER_OPTION> options = new List<TLKP_FILTER_OPTION>();

        public PageReportAging()
        {
            InitializeComponent();
            this.IsLoading = true;
            ApplySetting();
            _Currencies = DBDataContext.Db.TLKP_CURRENCies.ToList();
            this.Load += PageReportAging_Load;
            pgvAgingReport.CustomAppearance += PgvPaymentDetail_CustomAppearance;
            pgvAgingReport.CustomCellValue += PgvPaymentDetail_CustomCellValue;
            pgvAgingReport.FieldFilterChanged += PgvPaymentDetail_FieldFilterChanged;
            pgvAgingReport.CustomCellDisplayText += PgvPaymentDetail_CustomCellDisplayText;
            pgvAgingReport.BeginRefresh += PgvAgingReport_BeginRefresh;
            this.rdpDate.ValueChanged += rdpDate_ValueChanged;
            pgvAgingReport.CellDoubleClick += PgvAgingReport_CellDoubleClick;
            pgvAgingReport.KeyUp += PgvAgingReport_KeyUp;
            pgvAgingReport.CellClick += PgvAgingReport_CellClick;
            pgvAgingReport.FieldValueDisplayText += PgvAgingReport_FieldValueDisplayText;
            this.btnSAVE_TEMPLATE.ItemClick += btnSAVE_TEMPLATE_ItemClick;
            this.VisibleChanged += PageReportAging_VisibleChanged;
            this.pgvAgingReport.CustomFieldSort += PgvAgingReport_CustomFieldSort;
            pgvAgingReport.EndRefresh += PgvAgingReport_EndRefresh1;
            this.RestoreUserConfigure();
            //overide restore when row no not in mid 
            colROW_NO.Appearance.Cell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            colROW_NO.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            colROW_NO.Appearance.Value.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            FormatDataHelper.SetDefaultFontEPower();
            CustomBind();
        }

        private void PgvAgingReport_CustomFieldSort(object sender, PivotGridCustomFieldSortEventArgs e)
        { 
            int value1 = Convert.ToInt32(e.GetListSourceColumnValue(e.ListSourceRowIndex1, "ID"));
            int value2 = Convert.ToInt32(e.GetListSourceColumnValue(e.ListSourceRowIndex2, "ID"));
            e.Result = Comparer.Default.Compare(value1, value2); 
            e.Handled = true;
        }

        private void PgvAgingReport_EndRefresh1(object sender, EventArgs e)
        {
            this.SaveUserConfigure();
        }

        private void PageReportAging_VisibleChanged(object sender, EventArgs e)
        {
            if (!this.Visible)
            {
                this.SaveUserConfigure();
            }
        }

        private int groupIndex = 0; 
        private void PgvAgingReport_FieldValueDisplayText(object sender, PivotFieldDisplayTextEventArgs e)
        {
            if (e.DataField == null)
            {
                return;
            } 
            if (e.Field == null)
            {
                return;
            } 
            
            if(e.MaxIndex == 0)
            {
                groupIndex = 0;
            }
            if (e.ValueType == PivotGridValueType.GrandTotal || e.ValueType == PivotGridValueType.CustomTotal || e.ValueType == PivotGridValueType.Total)
            {
                if (e.Value != null)
                {
                    var newGroup = e.Value.ToString();
                    groupIndex += 1;
                }
            }
            //var fieldName =  ( colROW_NO.Tag == null)?  colROW_NO.FieldName : colROW_NO.Tag.ToString();
            if (e.Field.Name == colROW_NO.Name)
            {
                if (e.MinIndex >= 0 && e.MinIndex == e.MaxIndex)
                {

                    e.DisplayText = (e.MinIndex + 1 - groupIndex).ToString();
                } 
            } 
        }

        private void PgvAgingReport_EndRefresh(object sender, EventArgs e)
        {
            this.SaveUserConfigure();
        }

        public void SaveUserConfigure()
        {
            pgvAgingReport.SaveUserConfigureToXml();
        }
        private void PgvAgingReport_CellClick(object sender, PivotCellEventArgs e)
        {
            var CellFiled = e.DataField.FieldName;
            Filed = CellFiled;
        } 
        private void PgvAgingReport_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                var CustomerCode = pgvAgingReport.Cells.GetFocusedCellInfo().GetFieldValue(colCUSTOMER_CODE) ?? "";
                var objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_CODE == CustomerCode);
                if (objCustomer != null)
                {
                    if (Filed == colAMOUNT.FieldName || Filed == colTOTAL_DUE_AMOUNT.FieldName)
                    {
                        if (!isOpen())
                        {
                            return;
                        }

                        DialogReceivePayment dialog = new DialogReceivePayment(objCustomer);
                        if (dialog.ShowDialog() == DialogResult.OK)
                        {
                            Reload();
                        }
                    }
                }
            }
        }  

        private bool isOpen()
        {
            bool valid = true;
            if (Login.CurrentCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_RECEIVE_PAYMENT, string.Empty) == DialogResult.Yes)
                {
                    new DialogOpenCashDrawer().ShowDialog();
                    if (Login.CurrentCashDrawer != null)
                    {
                        valid = true;
                    }
                    else
                    {
                        valid = false;
                    }
                }
                else
                {
                    valid = false;
                }
            }
            return valid;
        }

        private void PgvAgingReport_CellDoubleClick(object sender, PivotCellEventArgs e)
        {
            var CustomerCode = e.GetFieldValue(colCUSTOMER_CODE) ?? "";
            var objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_CODE == CustomerCode);
            if (objCustomer != null)
            {
                if (e.DataField.FieldName == colAMOUNT.FieldName || e.DataField.FieldName == colTOTAL_DUE_AMOUNT.FieldName)
                {
                    if (!isOpen())
                    {
                        return;
                    }

                    DialogReceivePayment dialog = new DialogReceivePayment(objCustomer);
                    if (dialog.ShowDialog() == DialogResult.OK)
                    {
                        Reload();
                    }
                }
            }
        }

        private void PgvPaymentDetail_CustomAppearance(object sender, PivotCustomAppearanceEventArgs e)
        {
            if (Convert.ToString(e.GetFieldValue(colSTATUS)) == "ឈប់ប្រើប្រាស់" || Convert.ToString(e.GetFieldValue(colSTATUS)) == "លុប")
            {
                e.Appearance.ForeColor = Color.Red;
            }
            else if (Convert.ToString(e.GetFieldValue(colSTATUS)) == "ផ្តាច់")
            {
                e.Appearance.ForeColor = Color.DarkOrange;
            }
            else if (Convert.ToString(e.GetFieldValue(colSTATUS)) == "មិនទាន់ចាប់ផ្តើមប្រើ")
            {
                e.Appearance.ForeColor = Color.Blue;
            }
        }
        private void ApplySetting()
        {
            ResourceHelper.ApplyResource(this);
            rdpDate.DateFilterTypes = ReportDatePicker.DateFilterType.AsOfDate;
            var companyInfo = DBDataContext.Db.TBL_COMPANies.FirstOrDefault();
            _lblCompany.Text = companyInfo.COMPANY_NAME;
            _lblAddress.Text = companyInfo.ADDRESS;
            _lblCompany.AutoSize = false;
            _lblAddress.AutoSize = false;
            _lblCompany.Size = new Size(300, 30);
            _lblAddress.Size = new Size(300, 50);
            _lblCompany.Size = new System.Drawing.Size(300, 30);
            _lblAddress.Size = new Size(300, 50);
            pgvAgingReport.SetDefaultPivotview();
            colOPEN_DAYS.FormatNumeric();
            colINVOICE_DATE_BILLING.FormatShortDate();
            //VisibleTotalRecord();
            dpExport.ToolTip = Resources.ShowAsExportFuction;
            dpAction.ToolTip = Resources.ShowAsResetTemplatesFunction;
          // this.RestoreUserConfigure();
        }
        private string GetReportName()
        {
            var dateExport = DateTime.Now.ToString("_yyyyMMdd");
            var defaultFileName = $"{_title}{dateExport}";
            return defaultFileName;
        }
        private void GetReportHeader()
        {
            _lblTitle.Text = $"{_title}";
            _lblSubTitle1.Text = $"{Resources.AsOfDate} : {rdpDate.ToDate.ToShortDate()} - {Resources.DAY} : {(int)nudDays.Value}";
            _colP1.Caption = $"{0} - {(int)nudDays.Value}";
            _colP2.Caption = $"{(int)nudDays.Value + 1} - {(int)nudDays.Value * 2}";
            _colP3.Caption = $"{(int)nudDays.Value * 2 + 1} - {(int)nudDays.Value * 3}";
            _colP4.Caption = $"{(int)nudDays.Value * 3 + 1} - {(int)nudDays.Value * 4}";
            _colP5.Caption = $"{Resources.LongThan} {(int)nudDays.Value * 4}";
        }

        private void PgvAgingReport_BeginRefresh(object sender, EventArgs e)
        {
            colNOT_ON_DUE_DATE.FormatViewNumeric();
            _colP1.FormatViewNumeric();
            _colP2.FormatViewNumeric();
            _colP3.FormatViewNumeric();
            _colP4.FormatViewNumeric();
            _colP5.FormatViewNumeric();
            colPAID_AMOUNT.FormatViewNumeric();
            colAMOUNT.FormatViewNumeric();
            colTOTAL_DUE_AMOUNT.FormatViewNumeric();
            colOPEN_DAYS.FormatNumeric();
        }
        private void PgvPaymentDetail_CustomCellDisplayText(object sender, PivotCellDisplayTextEventArgs e)
        {
            var value = e.GetFieldValue(colCURRENCY)?.ToString() ?? "";
            if (string.IsNullOrEmpty(value))
            {
                return;
            }
            var currency = _Currencies.FirstOrDefault(x => x.CURRENCY_CODE == value);
            if (currency == null)
            {
                return;
            }
            if (
                  e.DataField.FieldName == colAMOUNT.FieldName || e.DataField.FieldName == _colP1.FieldName ||
                  e.DataField.FieldName == _colP2.FieldName || e.DataField.FieldName == _colP3.FieldName ||
                  e.DataField.FieldName == _colP4.FieldName || e.DataField.FieldName == _colP5.FieldName ||
                  e.DataField.FieldName == colNOT_ON_DUE_DATE.FieldName || e.DataField.FieldName == colPAID_AMOUNT.FieldName ||
                  e.DataField.FieldName == colTOTAL_DUE_AMOUNT.FieldName ||
                  e.RowValueType == PivotGridValueType.Total ||
                  e.RowValueType == PivotGridValueType.GrandTotal
                )
            {
                e.DisplayText = Convert.ToDecimal(e.Value).ToString(currency.FORMAT);
            }
        }

        private void PgvPaymentDetail_FieldFilterChanged(object sender, PivotFieldEventArgs e)
        {
            if (e.Field.FieldName == colCURRENCY.FieldName)
            {

               // VisibleTotalRecord();
            }
        }
        private void VisibleTotalRecord()
        {
            var checkCurrency = pgvAgingReport.Fields[colCURRENCY.FieldName].FilterValues.Values.Count();
            if (checkCurrency == 1)
            {
                pgvAgingReport.Fields[colCURRENCY.FieldName].Options.ShowTotals = true;
                pgvAgingReport.Fields[colCURRENCY.FieldName].Options.ShowGrandTotal = true;

                pgvAgingReport.Fields[colAMOUNT.FieldName].Options.ShowTotals = true;
                pgvAgingReport.Fields[colAMOUNT.FieldName].Options.ShowGrandTotal = true;
            }
            else
            {
                pgvAgingReport.Fields[colCURRENCY.FieldName].Options.ShowTotals = false;
                pgvAgingReport.Fields[colCURRENCY.FieldName].Options.ShowGrandTotal = false;

                pgvAgingReport.Fields[colAMOUNT.FieldName].Options.ShowTotals = false;
                pgvAgingReport.Fields[colAMOUNT.FieldName].Options.ShowGrandTotal = false; 
            }
        }

        private void PgvPaymentDetail_CustomCellValue(object sender, PivotCellValueEventArgs e)
        {
            if (pgvAgingReport.Fields[colCURRENCY.FieldName].FilterValues.ValuesIncluded.Count() > 1)
            {
                if (e.RowValueType == PivotGridValueType.GrandTotal)
                {
                    e.Value = null;
                }
            }
        }
        private void rdpDate_ValueChanged(object sender, EventArgs e)
        {
            Reload();
        }
        private void Reload()
        {
            if (this.IsLoading) return;
            var d1 = rdpDate.ToDate;
            int interval = (int)nudDays.Value;
            GetReportHeader();
            var param = new ReportAgingSearchParam()
            {
                DATE = d1,
                INTERVAL = interval,
            };
            Runner.RunNewThread(() =>
            {
                try
                {
                    var dcResult = ReportLogic.getAgingReportData(param).FirstOrDefault();
                    _data = dcResult.Value;
                    _reportData = dcResult.Key;
                    pgvAgingReport.DataSource = _reportData;
                }
                catch(Exception ex)
                {
                    if (ex.Message.Contains("One or more errors occurred."))
                    {
                        MsgBox.LogError(ex);
                        throw new Exception(Resources.OUT_OF_MEMORY);
                    }
                    else
                    {
                        MsgBox.LogError(ex);
                        throw new Exception(Resources.YOU_CANNOT_VIEW_REPORT);
                    }
                    if (this.IsLoading) return;
                }
            });
            //pgvAgingReport.RestoreUserConfigureFromXml(_DefaultTemplateView);
        }

        private void PageReportAging_Load(object sender, EventArgs e)
        {
            var timer = new Timer();
            timer.Interval = 500;
            timer.Start();
            timer.Tick += (object sender1, EventArgs e1) =>
            {
                timer.Stop();
                this.IsLoading = !this.Created;
                Reload();
            };
            BindAction();
        }
        private void CustomBind()
        {
            //When View
            colROW_NO.Width = 70;
            colCURRENCY.Width = 90;
            colCUSTOMER_CODE.Width = 150;
            colCUSTOMER_NAME.Width = 150;
            colBOX.Width = 120;
            colINVOICE_TITLE.Width = 150;
            colINVOICE_DATE_BILLING.Width = 120;
            colNOT_ON_DUE_DATE.Width = 150;
            colAMOUNT.Width = 150;
            colPAID_AMOUNT.Width = 150;
            colTOTAL_DUE_AMOUNT.Width = 100;
            _colP1.Width = 150;
            _colP2.Width = 150;
            _colP2.Width = 150;
            _colP3.Width = 150;
            _colP4.Width = 150;
            _colP5.Width = 150;
            colPHONE_NUMBER.Width = 140;
            colPOLE.Width = 120;
            colAREA.Width = 120;
        }

        private void CustomPrint()
        {
            //When print
            colROW_NO.Width = 45;
            colCURRENCY.Width = 50;
            colCUSTOMER_CODE.Width = 50;
            colCUSTOMER_NAME.Width = 50;
            colBOX.Width = 50;
            colINVOICE_TITLE.Width = 120;
            colINVOICE_DATE_BILLING.Width = 60;
            colNOT_ON_DUE_DATE.Width = 60;
            colAMOUNT.Width = 80;
            colPAID_AMOUNT.Width = 80;
            colTOTAL_DUE_AMOUNT.Width = 80;
            colOPEN_DAYS.Width = 90;
            _colP1.Width = 40;
            _colP2.Width = 40;
            _colP2.Width = 40;
            _colP3.Width = 40;
            _colP4.Width = 40;
            _colP5.Width = 40;
            colPHONE_NUMBER.Width = 50;
            colPOLE.Width = 60;
            colAREA.Width = 60;
        }
        private void btnPRINT_ItemClick(object sender, ItemClickEventArgs e)
        {
            CustomPrint();
            EPower.Base.Helper.DevExpressCustomize.DevExtension.ShowGridPreview(pgvAgingReport, _title, _lblSubTitle1.Text,false);
            CustomBind();
        }
        private void btnExcel_ItemClick(object sender, ItemClickEventArgs e)
        {
            EPower.Base.Helper.DevExpressCustomize.DevExtension.ToExcel(pgvAgingReport, _title, _lblSubTitle1.Text, GetReportName(), false);
        }
        private void btnCsv_ItemClick(object sender, ItemClickEventArgs e)
        {
            EPower.Base.Helper.DevExpressCustomize.DevExtension.ToCsv(pgvAgingReport, _title, _lblSubTitle1.Text, GetReportName(), false);
        }

        private void btnPdf_ItemClick(object sender, ItemClickEventArgs e)
        {
            CustomPrint();
            EPower.Base.Helper.DevExpressCustomize.DevExtension.ToPdf(pgvAgingReport, _title, _lblSubTitle1.Text, GetReportName(), false);
            CustomBind();
        }

        private void bntRaw_Data_ItemClick(object sender, ItemClickEventArgs e)
        {
            var i = 1;
            var localReportResult = _reportData.OrderBy(x => x.AREA_NAME).ThenBy(x => x.POLE_CODE).ThenBy(x => x.BOX_CODE).ThenBy(x => x.CUSTOMER_CODE).ToList();
            var qExportModel = (from x in localReportResult
                                group x by new
                                {
                                    x.CUSTOMER_CODE,
                                    x.CUSTOMER_NAME,
                                    x.AREA_NAME,
                                    x.BOX_CODE,
                                    x.STATUS,
                                    x.CURRENCY_SING,
                                    x.PHONE
                                } into g
                                select new ExportingAgingModel()
                                {
                                    CUSTOMER_CODE = (g.Key.CUSTOMER_CODE == "") ? "សរុបទឹកប្រាក់លម្អៀងទៅខែបន្ទាប់" : g.Key.CUSTOMER_CODE,
                                    CUSTOMER_NAME = g.Key.CUSTOMER_NAME,
                                    PHONE_NUMBER = g.Key.PHONE,
                                    AREA_NAME = g.Key.AREA_NAME,
                                    BOX = g.Key.BOX_CODE,
                                    STATUS = g.Key.STATUS,
                                    P1 = g.Sum(x => x.P1),
                                    P2 = g.Sum(x => x.P2),
                                    P3 = g.Sum(x => x.P3),
                                    P4 = g.Sum(x => x.P4),
                                    P5 = g.Sum(x => x.P5),
                                    TOTAL_DUE_AMOUNT = g.Sum(s => s.SETTLE_AMOUNT - s.PAID_AMOUNT),
                                    CURRENCY_SIGN = (g.Key.CURRENCY_SING)
                                }).Where(x => x.TOTAL_DUE_AMOUNT != 0).ToList();
            var totalRecord = qExportModel.GroupBy(x => x.CURRENCY_SIGN).Select(x => new ExportingAgingModel()
            {
                CUSTOMER_CODE = "សរុបទឹកប្រាក់",
                CURRENCY_SIGN = x.Key,
                P1 = x.Sum(s => s.P1),
                P2 = x.Sum(s => s.P2),
                P3 = x.Sum(s => s.P3),
                P4 = x.Sum(s => s.P4),
                P5 = x.Sum(s => s.P5),
                TOTAL_DUE_AMOUNT = x.Sum(s => s.TOTAL_DUE_AMOUNT),
            }).ToList();

            qExportModel.AddRange(totalRecord.Where(x => x.TOTAL_DUE_AMOUNT != 0).ToList());
            var exportModel = qExportModel.Where(x => x.TOTAL_DUE_AMOUNT != 0)._ToDataTable();
            exportModel.Columns["P1"].Caption = $"{0} - {(int)nudDays.Value}";
            exportModel.Columns["P2"].Caption = $"{(int)nudDays.Value} - {(int)nudDays.Value * 2}";
            exportModel.Columns["P3"].Caption = $"{(int)nudDays.Value * 2} - {(int)nudDays.Value * 3}";
            exportModel.Columns["P4"].Caption = $"{(int)nudDays.Value * 3} - {(int)nudDays.Value * 4}";
            exportModel.Columns["P5"].Caption = $"{Resources.LongThan} {(int)nudDays.Value * 4}";

            var notTranslateColumn = new List<string>()
            {
                "P1",
                "P2",
                "P3",
                "P4",
                "P5",
            };

            foreach (DataColumn column in exportModel.Columns)
            {
                if (notTranslateColumn.Contains(column.ColumnName))
                {
                    continue;
                }
                if (column.ColumnName == "CURRENCY_SIGN")
                {
                    column.Caption = Resources.CURRENCY;
                    continue;
                }
                column.Caption = ResourceHelper.Translate(column.ColumnName);
            }
            ExportData.ToExcel(exportModel, Resources.REPORT_AGING, "", "");
        }

        private void btnResetToDefault_ItemClick(object sender, ItemClickEventArgs e)
        {
            pgvAgingReport.RestoreUserConfigureFromXml(_DefaultTemplateView);
         
        }

        private void btnSAVE_TEMPLATE_ItemClick(object sender, ItemClickEventArgs e)
        {
            DialogTemplate dialog = new DialogTemplate(pgvAgingReport.ActiveFilterString, this.Name.ToUpper());
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                var file = pgvAgingReport.SaveUserConfigureToXml(dialog.txtTemplateName.Text);
                TLKP_FILTER_OPTION obj = dialog._objFilter;
                obj.XML_FILE = File.ReadAllBytes(file);
                DBDataContext.Db.SubmitChanges();
                BindAction();
            }
        }
        private void btnRemove_ItemClick(object sender, ItemClickEventArgs e)
        {
            var filter = options.FirstOrDefault(x => x.FILTER_ID == (int)e.Item.Tag);
            if (filter == null)
            {
                return;
            }
            filter.IS_ACTIVE = false;
            DBDataContext.Db.SubmitChanges();
            popActions.RemoveLink(popActions.ItemLinks.ToList().FirstOrDefault(x => x.Caption == $"បង្ហាញទម្រង់៖ {filter.FILTER_NAME}"));
        }
        private void btnFilter_ItemClick(object sender, ItemClickEventArgs e)
        {
            var filter = options.FirstOrDefault(x => x.FILTER_ID == (int)e.Item.Tag);
            if (filter == null)
            {
                return;
            }
            _title = filter.FILTER_NAME;
            pgvAgingReport.RefreshData();
            pgvAgingReport.Refresh();
            pgvAgingReport.DataSource.Refresh();
            this.pgvAgingReport.ActiveFilterString = filter.FILTER_OPTION;
            this.popActions.HidePopup();

            var file = pgvAgingReport.RestoreUserConfigureFromXml(filter.FILTER_NAME);
            if (!File.Exists(file) && filter.XML_FILE != null)
            {
                var baseDirectory = Path.Combine(Application.StartupPath, "UserConfigures");
                file = Path.Combine(baseDirectory, $"{pgvAgingReport.Name}.{filter.FILTER_NAME}.xml");
                File.WriteAllBytes(file, filter.XML_FILE.ToArray());
                pgvAgingReport.RestoreUserConfigureFromXml(filter.FILTER_NAME);
            }
            GetReportHeader();
            pgvAgingReport.ExpandAll();
            colROW_NO.Appearance.Value.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        }
        public void BindAction()
        {
            options = DBDataContext.Db.TLKP_FILTER_OPTIONs.Where(x => x.IS_ACTIVE && x.GRID_NAME == this.Name.ToUpper()).ToList();
            var font = new Font("Khmer OS System", 8.24f);
            foreach (var item in options)
            {
                if (!popActions.ItemLinks.Where(x => x.Caption == $"បង្ហាញទម្រង់៖ {item.FILTER_NAME}").Any())
                {
                    BarButtonItem btnRemove = new BarButtonItem();
                    btnRemove.Caption = Resources.REMOVE;
                    btnRemove.Tag = item.FILTER_ID;
                    btnRemove.ItemClick += btnRemove_ItemClick;
                   

                    var btnFilter = new BarSubItem();
                    btnFilter.Caption = $"បង្ហាញទម្រង់៖ {item.FILTER_NAME}";
                    btnFilter.Tag = item.FILTER_ID;
                    if (!item.IS_SYSTEM)
                    {
                        barManager1.Items.Add(btnRemove);
                        btnFilter.AddItem(btnRemove);
                    }
                    popActions.AddItem(btnFilter);
                    btnFilter.ItemClick += btnFilter_ItemClick;
                    btnFilter.MenuAppearance.AppearanceMenu.SetFont(font);
                }
            }
            barManager1.DockWindowTabFont = font;
            popActions.MenuAppearance.AppearanceMenu.SetFont(font);
        } 
        public void RestoreUserConfigure()
        {
            pgvAgingReport.RestoreUserConfigureFromXml();
        } 
        private void pgvAgingReport_CustomUnboundFieldData(object sender, CustomFieldDataEventArgs e)
        {
            //e.Value = e.ListSourceRowIndex + 1; 
            //e.Value = e.GetListSourceColumnValue(colCUSTOMER_CODE.Name);
        }  
        private void btnAgingSummary_ItemClick(object sender, ItemClickEventArgs e)
        {

        }
        protected override void OnClosed(EventArgs e)
        {
           
            base.OnClosed(e);
        }
        protected override void OnVisibleChanged(EventArgs e)
        {
            if (!this.Visible)
            {
                this.SaveUserConfigure();
            }
            base.OnVisibleChanged(e);
        }

        private void pgvAgingReport_DragDrop(object sender, DragEventArgs e)
        {
            groupIndex = 0;
        }

        private void pgvAgingReport_Click(object sender, EventArgs e)
        {

        }
    }

    public class ExportingAgingModel
    {
        public string CUSTOMER_CODE { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string PHONE_NUMBER { get; set; }
        public string AREA_NAME { get; set; }
        public string BOX { get; set; }
        public string STATUS { get; set; }
        public decimal P1 { get; set; }
        public decimal P2 { get; set; }
        public decimal P3 { get; set; }
        public decimal P4 { get; set; }
        public decimal P5 { get; set; }
        public decimal TOTAL_DUE_AMOUNT { get; set; }
        public string CURRENCY_SIGN { get; set; }

    }
}
