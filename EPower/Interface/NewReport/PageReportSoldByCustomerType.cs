﻿using DevExpress.Utils;
using DevExpress.XtraBars;
using EPower.Base.Helper;
using EPower.Base.Helper.Component;
using EPower.Base.Helper.DevExpressCustomize;
using EPower.Base.Logic;
using EPower.Base.Models;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageReportSoldByCustomerType : Form
    {
        private bool isLoading = true;
        public PageReportSoldByCustomerType()
        {
            InitializeComponent();
            ApplySetting();
            UIHelper.SetDataSourceToComboBox(this.cboBillingCycle, Lookup.GetBillingCycles(), Resources.ALL_CYCLE);
            this.cboBillingCycle.SelectedIndex = 0;
            RegisterExportEvent();
            //isLoading = false;
            rdpDate.DefaultValue = ReportDatePicker.DateRanges.LastMonth;
            this.tgvCustomerType.ActiveFilterString = "[TOTAL_CUSTOMER] >= 1";
            this.Shown += PageReportSoldByCustomerType_Shown;
            ///this.rdpDate.ValueChanged += rdpDate_ValueChanged;
            tgvCustomerType.CustomDrawNodeCell += TgvCustomerType_CustomDrawNodeCell;
        }

        private void PageReportSoldByCustomerType_Shown(object sender, EventArgs e)
        {
            if (this.IsHandleCreated)
            {
                isLoading = false;
                Bind();
            }
        }

        private void ApplySetting()
        {
            var companyInfo = DBDataContext.Db.TBL_COMPANies.FirstOrDefault();
            _lblCompany.Text = companyInfo.COMPANY_NAME;
            _lblAddress.Text = companyInfo.ADDRESS;
            _picLogo.Image = UIHelper.ConvertBinaryToImage(companyInfo.COMPANY_LOGO);
            _lblCompany.AutoSize = false;
            _lblAddress.AutoSize = false;
            _lblCompany.Size = new System.Drawing.Size(300, 30);
            _lblAddress.Size = new System.Drawing.Size(300, 50);

            tgvCustomerType.OptionsView.ShowIndentAsRowStyle = false;
            //tgvCustomerType.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            tgvCustomerType.OptionsView.ShowVertLines = false;
            tgvCustomerType.OptionsView.ShowHorzLines = false;
            tgvCustomerType.OptionsFilter.AllowAutoFilterConditionChange = DefaultBoolean.False;
            tgvCustomerType.OptionsFilter.AllowFilterEditor = false;
            tgvCustomerType.OptionsFilter.AllowMRUFilterList = false;
            tgvCustomerType.Cursor = Cursors.Arrow;

            tgvCustomerType.OptionsPrint.PrintCheckBoxes = false;
            tgvCustomerType.OptionsPrint.PrintTree = false;
            tgvCustomerType.OptionsPrint.PrintTreeButtons = false;
            tgvCustomerType.AppearancePrint.GroupFooter.Font = tgvCustomerType.Appearance.GroupFooter.Font;
            tgvCustomerType.AppearancePrint.HeaderPanel.Font = tgvCustomerType.Appearance.HeaderPanel.Font;
            tgvCustomerType.AppearancePrint.Row.Font = tgvCustomerType.Appearance.Row.Font;
        }

        private void ReloadFilter()
        {
            _lblSubTitle1.Text = $"{Resources.START_DATE} : {rdpDate.FromDate.ToShortDate()} {Resources.TO} : {rdpDate.ToDate.ToShortDate()} ";
            //+ $"{Properties.Resources.CYCLE_NAME}{"៖"} {diag.cboBillingCycle.Text} {Properties.Resources.AREA_NAME}{"៖"} {diag.cboArea.Text}";
            _lblSubTitle2.Text = "";// $"{Properties.Resources.CURRENCY_NAME}{"៖"} {diag.cboCurrency.Text} {Properties.Resources.CUSTOMER_GROUP}{"៖"} {diag.cboCustomerGroup.Text} {Properties.Resources.CUSTOMER_CONNECTION_TYPE}{"៖"} {diag.cboCustomerConnectionType.Text}";
            _lblTitle.AutoSize = false;
        }

        private void Bind()
        {
            if (this.isLoading)
            {
                return;
            }
            var CycleId = (int)cboBillingCycle.SelectedValue;
            var renderId = 1;
            var results = new List<GroupCustomerType>();
            Runner.RunNewThread(delegate ()
            {
                try
                {
                    var q = DBDataContext.Db.ExecuteQuery<GroupCustomerType>(@"EXEC dbo.REPORT_SOLD_BY_CUSTOMER_TYPE @p0, @p1, @p2", rdpDate.FromDate, rdpDate.ToDate, CycleId).ToList();
                    //q = q.Where(x => x.CUSTOMER_CONNECTION_TYPE_NAME != "").ToList();
                    var groups = q.Select(x => x.TYPE_ID).Distinct().ToList();

                    foreach (var group in groups)
                    {
                        var dataGroupbyType = q.Where(x => x.TYPE_ID == group).ToList();
                        var t = new GroupCustomerType()
                        {

                            RenderId = -1 * group,
                            ParentId = 0,
                            CUSTOMER_CONNECTION_TYPE_NAME = $"{ResourceHelper.Translate(dataGroupbyType.FirstOrDefault().TYPE_NAME)}",
                            CURRENCY_ID = 0,
                            TOTAL_CUSTOMER = dataGroupbyType.Sum(x => x.TOTAL_CUSTOMER),
                            KWH_USAGE = dataGroupbyType.Sum(x => x.KWH_USAGE),
                            KVAR_USAGE = dataGroupbyType.Sum(x => x.KVAR_USAGE),
                            KWH_AMOUNT = 0,
                            KVAR_AMOUNT = 0,
                            TOTAL_AMOUNT = 0,
                            FORMAT = dataGroupbyType.FirstOrDefault().FORMAT
                        };
                        q.Add(t);
                        var currencyIds = dataGroupbyType.Select(x => x.CURRENCY_ID).Distinct().ToList();
                        foreach (var currency in currencyIds)
                        {
                            var parentRenderId = 0;
                            parentRenderId = renderId;
                            var currecny = dataGroupbyType.Where(x => x.CURRENCY_ID == currency).ToList();
                            var groupCustomer = currecny.Select(x => x.CUSTOMER_GROUP_NAME).Distinct().ToList();
                            var parent = new GroupCustomerType()
                            {
                                RenderId = renderId,
                                ParentId = dataGroupbyType.FirstOrDefault().TYPE_ID * -1,
                                CUSTOMER_CONNECTION_TYPE_NAME = $"{ Resources.CURRENCY} ៖ {currecny.FirstOrDefault().CURRENCY_CODE}",
                                CURRENCY_ID = currency,
                                TOTAL_CUSTOMER = currecny.Sum(x => x.TOTAL_CUSTOMER),
                                KWH_USAGE = currecny.Sum(x => x.KWH_USAGE),
                                KVAR_USAGE = currecny.Sum(x => x.KVAR_USAGE),
                                KWH_AMOUNT = currecny.Sum(x => x.KWH_AMOUNT),
                                KVAR_AMOUNT = currecny.Sum(x => x.KVAR_AMOUNT),
                                TOTAL_AMOUNT = currecny.Sum(x => x.TOTAL_AMOUNT),
                                FORMAT = currecny.FirstOrDefault().FORMAT
                            };
                            q.Add(parent);
                            renderId++;
                            foreach (var type in groupCustomer)
                            {
                                var groupType = currecny.Where(x => x.CUSTOMER_GROUP_NAME == type).ToList();
                                var connType = groupType.Select(x => x.CUSTOMER_CONNECTION_TYPE_NAME).Distinct().ToList();
                                var subparent = new GroupCustomerType()
                                {
                                    RenderId = renderId,
                                    ParentId = parentRenderId,
                                    CUSTOMER_CONNECTION_TYPE_NAME = $"{groupType.FirstOrDefault().CUSTOMER_GROUP_NAME}",
                                    CURRENCY_ID = currency,
                                    TOTAL_CUSTOMER = groupType.Sum(x => x.TOTAL_CUSTOMER),
                                    KWH_USAGE = groupType.Sum(x => x.KWH_USAGE),
                                    KVAR_USAGE = groupType.Sum(x => x.KVAR_USAGE),
                                    KWH_AMOUNT = groupType.Sum(x => x.KWH_AMOUNT),
                                    KVAR_AMOUNT = groupType.Sum(x => x.KVAR_AMOUNT),
                                    TOTAL_AMOUNT = groupType.Sum(x => x.TOTAL_AMOUNT),
                                    FORMAT = currecny.FirstOrDefault().FORMAT
                                };
                                q.Add(subparent);
                                var subparentRenderId = renderId;
                                renderId++;
                                foreach (var typeName in connType)
                                {
                                    var conn = groupType.Where(x => x.CUSTOMER_CONNECTION_TYPE_NAME == typeName).ToList();
                                    var subsubparent = new GroupCustomerType()
                                    {
                                        RenderId = renderId,
                                        ParentId = subparentRenderId,
                                        CUSTOMER_CONNECTION_TYPE_NAME = $"{conn.FirstOrDefault().CUSTOMER_CONNECTION_TYPE_NAME}",
                                        CURRENCY_ID = currency,
                                        TOTAL_CUSTOMER = conn.Sum(x => x.TOTAL_CUSTOMER),
                                        KWH_USAGE = conn.Sum(x => x.KWH_USAGE),
                                        KVAR_USAGE = conn.Sum(x => x.KVAR_USAGE),
                                        KWH_AMOUNT = conn.Sum(x => x.KWH_AMOUNT),
                                        KVAR_AMOUNT = conn.Sum(x => x.KVAR_AMOUNT),
                                        TOTAL_AMOUNT = conn.Sum(x => x.TOTAL_AMOUNT),
                                        FORMAT = currecny.FirstOrDefault().FORMAT
                                    };
                                    q.Add(subsubparent);
                                    var subParentId = renderId;
                                    renderId++;
                                    foreach (var name in conn)
                                    {
                                        name.CUSTOMER_CONNECTION_TYPE_NAME = name.CUSTOMER_NAME;
                                        name.RenderId = renderId;
                                        name.ParentId = subParentId;
                                        renderId++;
                                    }
                                }
                            }
                        }
                    }
                    var rm = q.Where(x => string.IsNullOrEmpty(x.CUSTOMER_CONNECTION_TYPE_NAME)).ToList();
                    foreach (var k in rm)
                    {
                        q.Where(x => x.ParentId == k.RenderId).ToList().ForEach(x => x.ParentId = k.ParentId);
                    }
                    results = q.Where(x => !string.IsNullOrEmpty(x.CUSTOMER_CONNECTION_TYPE_NAME)).ToList();
                }
                catch(Exception ex)
                {
                    MsgBox.LogError(ex);
                    throw new Exception(Resources.YOU_CANNOT_VIEW_REPORT);
                }    
            });
            tgvCustomerType.DataSource = results;
            tgvCustomerType.ExpandAll();
            ReloadFilter();
        }

        private string GetReportName()
        {
            var dateExport = DateTime.Now.ToString("yyyy-MM-dd");
            var defaultFileName = $"{_lblTitle.Text} {dateExport}";
            return defaultFileName;
        }

        private string GetReportDescription()
        {
            var description = $"{_lblSubTitle1.Text} {_lblSubTitle2.Text}";
            return description;
        }

        public void ExportToPdf()
        {
            DevExtension.ToPdf(tgvCustomerType, _lblTitle.Text, GetReportDescription(), GetReportName(), true);
        }

        public void Print()
        {
            DevExtension.ShowGridPreview(tgvCustomerType, _lblTitle.Text, GetReportDescription(), true);
        }

        public void ExportToCsv()
        {
            DevExtension.ToCsv(tgvCustomerType, _lblTitle.Text, GetReportDescription(), GetReportName(), true);
        }

        public void ExportToExcel()
        {
            DevExtension.ToExcel(tgvCustomerType, _lblTitle.Text, GetReportDescription(), "", GetReportName(), false, false);
        }

        public void RegisterExportEvent()
        {
            btnExcel.ItemClick += (object sender, ItemClickEventArgs e) => { ExportToExcel(); };
            btnPrint.ItemClick += (object sender, ItemClickEventArgs e) => { Print(); };
            btnCsv.ItemClick += (object sender, ItemClickEventArgs e) => { ExportToCsv(); };
            btnPdf.ItemClick += (object sender, ItemClickEventArgs e) => { ExportToPdf(); };
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            //Bind();
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            try
            {
                string Path = Settings.Default.PATH_TEMP;
                DevExtension.ToExcel(tgvCustomerType, _lblTitle.Text, GetReportDescription(), Path, GetReportName(), false, true);
                string exportPath = new System.IO.FileInfo(Settings.Default.PATH_TEMP + GetReportName() + ".xlsx").FullName;
                DialogSendMail.Instance.Add(_lblTitle.Text, exportPath);
                DialogSendMail.Instance.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void btnSetup_Click(object sender, EventArgs e)
        {
            //if (diag.ShowDialog() == DialogResult.OK)
            //{
            //    Bind();
            //}
        }

        private void rdpDate_ValueChanged(object sender, EventArgs e)
        {

            Bind();
        }

        private void chkAMOUNT_ZERO_CheckedChanged(object sender, EventArgs e)
        {
            //Bind();
        }

        private void TgvCustomerType_CustomDrawNodeCell(object sender, DevExpress.XtraTreeList.CustomDrawNodeCellEventArgs e)
        {
            var format = "#,##0.####;-#,##0.####;-";
            if (e.Column == null)
            {
                return;
            }
            else
            {
                colTOTAL_CUSTOMER.Format.FormatType = FormatType.Numeric;
                colTOTAL_CUSTOMER.Format.FormatString = format;

                colKWH_POWER.Format.FormatType = FormatType.Numeric;
                colKWH_POWER.Format.FormatString = format;

                colKVAR_POWER.Format.FormatType = FormatType.Numeric;
                colKVAR_POWER.Format.FormatString = format;

                colKWH_AMOUNT.Format.FormatType = FormatType.Numeric;
                colKWH_AMOUNT.Format.FormatString = format;

                colKVAR_AMOUNT.Format.FormatType = FormatType.Numeric;
                colKVAR_AMOUNT.Format.FormatString = format;

                colCAPACITY_CHARGE_AMOUNT.Format.FormatType = FormatType.Numeric;
                colCAPACITY_CHARGE_AMOUNT.Format.FormatString = format;

                colTOTAL_AMOUNT.Format.FormatType = FormatType.Numeric;
                //colTOTAL_AMOUNT.Format.FormatString = e.Node.GetDisplayText(colFORMAT);
                colTOTAL_AMOUNT.Format.FormatString = format;
            }
        }

        private void cboBillingCycle_SelectedIndexChanged(object sender, EventArgs e)
        {
            Bind();
        }
    }
}
