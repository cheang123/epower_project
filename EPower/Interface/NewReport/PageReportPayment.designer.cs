﻿using EPower.Base.Helper.Component;
using System.ComponentModel;
using System.Windows.Forms;

namespace EPower.Interface
{
    partial class PageReportPayment
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageReportPayment));
            this.panel1 = new System.Windows.Forms.Panel();
            this.rdpDate = new EPower.Base.Helper.Component.ReportDatePicker();
            this.popupExport = new DevExpress.XtraBars.PopupMenu(this.components);
            this.btnExcel = new DevExpress.XtraBars.BarButtonItem();
            this.btnCsv = new DevExpress.XtraBars.BarButtonItem();
            this.btnPdf = new DevExpress.XtraBars.BarButtonItem();
            this.btnPRINT = new DevExpress.XtraBars.BarButtonItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnSAVE_TEMPLATE = new DevExpress.XtraBars.BarButtonItem();
            this.popActions = new DevExpress.XtraBars.PopupMenu(this.components);
            this.pivotGridField1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField4 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField5 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField6 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField7 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField8 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField9 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField10 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField11 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField12 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField13 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField14 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField15 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField16 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField17 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField18 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField19 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField20 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField21 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField22 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField23 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField24 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField25 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this._lblSubTitle1 = new System.Windows.Forms.Label();
            this._lblTitle = new System.Windows.Forms.Label();
            this._lblAddress = new System.Windows.Forms.Label();
            this._lblCompany = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.dpExport = new DevExpress.XtraEditors.DropDownButton();
            this.dpAction = new DevExpress.XtraEditors.DropDownButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this._picLogo = new System.Windows.Forms.PictureBox();
            this.pgvPaymentDetail = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.colCUSTOMER_CODE = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colCUSTOMER_NAME = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colPHONE_NUMBER = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colCUSTOMER_TYPE = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colADDRESS = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colMETER_CODE = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colAMPARE_NAME = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colAREA = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colPOLE = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colBOX = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colTRANSACTION_DATE = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colINVOICE_DATE_BILLING = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colBANK_CUT_OFF_DATE = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colPAID_DATE = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colDUE_DATE = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colREFERENCE = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colDESCRIPTION = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colPAYMENT_METHOD = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colPAYMENT_ACCOUNT = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colPAY_AT = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colRECEIVE_BY = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colTOTAL_PAY = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colTOTAL_VOID = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colTOTAL_PAID = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colSTATUS = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colNOTE = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colPAYMENT_TYPE_GROUP = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colCurrency = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colPAID_TIME = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colNUMBER_OF_ROW = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colCASH_DRAWER_NAME = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colPAYMENT_NO = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colBILLING_CYCLE = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colPRICE_NAME = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colINVOICE_ITEM = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colINVOICE_ITEM_TYPE = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colPLAN = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colROW_NO = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colTYPE_OF_TRANSACTION = new DevExpress.XtraPivotGrid.PivotGridField();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupExport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popActions)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._picLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pgvPaymentDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.rdpDate);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // rdpDate
            // 
            this.rdpDate.AutoSave = true;
            resources.ApplyResources(this.rdpDate, "rdpDate");
            this.rdpDate.DateFilterTypes = EPower.Base.Helper.Component.ReportDatePicker.DateFilterType.Period;
            this.rdpDate.DefaultValue = EPower.Base.Helper.Component.ReportDatePicker.DateRanges.Today;
            this.rdpDate.FromDate = new System.DateTime(2022, 7, 29, 0, 0, 0, 0);
            this.rdpDate.LastCustomDays = 30;
            this.rdpDate.LastFinancialYear = true;
            this.rdpDate.LastMonth = true;
            this.rdpDate.LastQuarter = true;
            this.rdpDate.LastWeek = true;
            this.rdpDate.Name = "rdpDate";
            this.rdpDate.ThisFinancialYear = true;
            this.rdpDate.ThisMonth = true;
            this.rdpDate.ThisQuarter = true;
            this.rdpDate.ThisWeek = true;
            this.rdpDate.ToDate = new System.DateTime(2022, 7, 29, 23, 59, 59, 0);
            this.rdpDate.Today = true;
            this.rdpDate.Value = EPower.Base.Helper.Component.ReportDatePicker.DateRanges.Today;
            this.rdpDate.Yesterday = true;
            this.rdpDate.ValueChanged += new System.EventHandler(this.rdpDate_ValueChanged);
            // 
            // popupExport
            // 
            this.popupExport.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExcel),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnCsv),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnPdf),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnPRINT, true)});
            this.popupExport.Manager = this.barManager1;
            this.popupExport.Name = "popupExport";
            // 
            // btnExcel
            // 
            resources.ApplyResources(this.btnExcel, "btnExcel");
            this.btnExcel.Id = 0;
            this.btnExcel.Name = "btnExcel";
            this.btnExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExcel_ItemClick);
            // 
            // btnCsv
            // 
            resources.ApplyResources(this.btnCsv, "btnCsv");
            this.btnCsv.Id = 1;
            this.btnCsv.Name = "btnCsv";
            this.btnCsv.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCsv_ItemClick);
            // 
            // btnPdf
            // 
            resources.ApplyResources(this.btnPdf, "btnPdf");
            this.btnPdf.Id = 2;
            this.btnPdf.Name = "btnPdf";
            this.btnPdf.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnPdf_ItemClick);
            // 
            // btnPRINT
            // 
            resources.ApplyResources(this.btnPRINT, "btnPRINT");
            this.btnPRINT.Id = 3;
            this.btnPRINT.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnPRINT.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnPRINT.ItemAppearance.Normal.Options.UseFont = true;
            this.btnPRINT.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnPRINT.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.btnPRINT.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.btnPRINT.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnPRINT.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.btnPRINT.Name = "btnPRINT";
            this.btnPRINT.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnPRINT_ItemClick);
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnExcel,
            this.btnCsv,
            this.btnPdf,
            this.btnPRINT,
            this.btnSAVE_TEMPLATE});
            this.barManager1.MaxItemId = 11;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            resources.ApplyResources(this.barDockControlTop, "barDockControlTop");
            this.barDockControlTop.Manager = this.barManager1;
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            resources.ApplyResources(this.barDockControlBottom, "barDockControlBottom");
            this.barDockControlBottom.Manager = this.barManager1;
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            resources.ApplyResources(this.barDockControlLeft, "barDockControlLeft");
            this.barDockControlLeft.Manager = this.barManager1;
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            resources.ApplyResources(this.barDockControlRight, "barDockControlRight");
            this.barDockControlRight.Manager = this.barManager1;
            // 
            // btnSAVE_TEMPLATE
            // 
            resources.ApplyResources(this.btnSAVE_TEMPLATE, "btnSAVE_TEMPLATE");
            this.btnSAVE_TEMPLATE.Id = 10;
            this.btnSAVE_TEMPLATE.Name = "btnSAVE_TEMPLATE";
            // 
            // popActions
            // 
            this.popActions.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSAVE_TEMPLATE)});
            this.popActions.Manager = this.barManager1;
            this.popActions.Name = "popActions";
            // 
            // pivotGridField1
            // 
            this.pivotGridField1.AreaIndex = 13;
            this.pivotGridField1.FieldName = "CUSTOMER_CODE";
            this.pivotGridField1.Name = "pivotGridField1";
            // 
            // pivotGridField2
            // 
            this.pivotGridField2.AreaIndex = 14;
            this.pivotGridField2.FieldName = "CUSTOMER_NAME";
            this.pivotGridField2.Name = "pivotGridField2";
            // 
            // pivotGridField3
            // 
            this.pivotGridField3.AreaIndex = 15;
            this.pivotGridField3.FieldName = "PHONE_NUMBER";
            this.pivotGridField3.Name = "pivotGridField3";
            // 
            // pivotGridField4
            // 
            this.pivotGridField4.AreaIndex = 16;
            this.pivotGridField4.FieldName = "CUSTOMER_TYPE";
            this.pivotGridField4.Name = "pivotGridField4";
            // 
            // pivotGridField5
            // 
            this.pivotGridField5.AreaIndex = 17;
            this.pivotGridField5.FieldName = "ADDRESS_CUSTOMER";
            this.pivotGridField5.Name = "pivotGridField5";
            // 
            // pivotGridField6
            // 
            this.pivotGridField6.AreaIndex = 18;
            this.pivotGridField6.FieldName = "METER_CODE";
            this.pivotGridField6.Name = "pivotGridField6";
            // 
            // pivotGridField7
            // 
            this.pivotGridField7.AreaIndex = 0;
            this.pivotGridField7.FieldName = "AMPARE_NAME";
            this.pivotGridField7.Name = "pivotGridField7";
            // 
            // pivotGridField8
            // 
            this.pivotGridField8.AreaIndex = 1;
            this.pivotGridField8.FieldName = "AREA_NAME";
            this.pivotGridField8.Name = "pivotGridField8";
            // 
            // pivotGridField9
            // 
            this.pivotGridField9.AreaIndex = 2;
            this.pivotGridField9.FieldName = "POLE_NAME";
            this.pivotGridField9.Name = "pivotGridField9";
            // 
            // pivotGridField10
            // 
            this.pivotGridField10.AreaIndex = 3;
            this.pivotGridField10.FieldName = "BOX_CODE";
            this.pivotGridField10.Name = "pivotGridField10";
            // 
            // pivotGridField11
            // 
            this.pivotGridField11.AreaIndex = 4;
            this.pivotGridField11.FieldName = "CREATE_ON";
            this.pivotGridField11.Name = "pivotGridField11";
            // 
            // pivotGridField12
            // 
            this.pivotGridField12.AreaIndex = 5;
            this.pivotGridField12.FieldName = "INVOICE_DATE";
            this.pivotGridField12.Name = "pivotGridField12";
            // 
            // pivotGridField13
            // 
            this.pivotGridField13.AreaIndex = 6;
            this.pivotGridField13.FieldName = "BANK_CUT_OFF_DATE";
            this.pivotGridField13.Name = "pivotGridField13";
            // 
            // pivotGridField14
            // 
            this.pivotGridField14.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridField14.AreaIndex = 0;
            this.pivotGridField14.FieldName = "PAID_DATE";
            this.pivotGridField14.Name = "pivotGridField14";
            // 
            // pivotGridField15
            // 
            this.pivotGridField15.AreaIndex = 7;
            this.pivotGridField15.FieldName = "DUE_DATE";
            this.pivotGridField15.Name = "pivotGridField15";
            // 
            // pivotGridField16
            // 
            this.pivotGridField16.AreaIndex = 8;
            this.pivotGridField16.FieldName = "REFERENCE";
            this.pivotGridField16.Name = "pivotGridField16";
            // 
            // pivotGridField17
            // 
            this.pivotGridField17.AreaIndex = 9;
            this.pivotGridField17.FieldName = "DESCRIPTION";
            this.pivotGridField17.Name = "pivotGridField17";
            // 
            // pivotGridField18
            // 
            this.pivotGridField18.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridField18.AreaIndex = 0;
            this.pivotGridField18.FieldName = "PAYMENT_METHOD";
            this.pivotGridField18.Name = "pivotGridField18";
            resources.ApplyResources(this.pivotGridField18, "pivotGridField18");
            // 
            // pivotGridField19
            // 
            this.pivotGridField19.AreaIndex = 19;
            this.pivotGridField19.FieldName = "PAY_AT_BANK";
            this.pivotGridField19.Name = "pivotGridField19";
            // 
            // pivotGridField20
            // 
            this.pivotGridField20.AreaIndex = 10;
            this.pivotGridField20.FieldName = "RECEIVE_BY";
            this.pivotGridField20.Name = "pivotGridField20";
            // 
            // pivotGridField21
            // 
            this.pivotGridField21.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridField21.AreaIndex = 1;
            this.pivotGridField21.FieldName = "TOTAL_PAY";
            this.pivotGridField21.Name = "pivotGridField21";
            // 
            // pivotGridField22
            // 
            this.pivotGridField22.AreaIndex = 20;
            this.pivotGridField22.FieldName = "TOTAL_VOID";
            this.pivotGridField22.Name = "pivotGridField22";
            // 
            // pivotGridField23
            // 
            this.pivotGridField23.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridField23.AreaIndex = 2;
            this.pivotGridField23.FieldName = "TOTAL_PAID";
            this.pivotGridField23.Name = "pivotGridField23";
            // 
            // pivotGridField24
            // 
            this.pivotGridField24.AreaIndex = 11;
            this.pivotGridField24.FieldName = "STATUS";
            this.pivotGridField24.Name = "pivotGridField24";
            // 
            // pivotGridField25
            // 
            this.pivotGridField25.AreaIndex = 12;
            this.pivotGridField25.FieldName = "NOTE";
            this.pivotGridField25.Name = "pivotGridField25";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this._lblSubTitle1);
            this.panel2.Controls.Add(this._lblTitle);
            this.panel2.Controls.Add(this._lblAddress);
            this.panel2.Controls.Add(this._lblCompany);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // panel5
            // 
            resources.ApplyResources(this.panel5, "panel5");
            this.panel5.Name = "panel5";
            // 
            // _lblSubTitle1
            // 
            this._lblSubTitle1.BackColor = System.Drawing.Color.White;
            this._lblSubTitle1.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this._lblSubTitle1, "_lblSubTitle1");
            this._lblSubTitle1.Name = "_lblSubTitle1";
            // 
            // _lblTitle
            // 
            this._lblTitle.BackColor = System.Drawing.Color.White;
            this._lblTitle.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this._lblTitle, "_lblTitle");
            this._lblTitle.Name = "_lblTitle";
            // 
            // _lblAddress
            // 
            this._lblAddress.BackColor = System.Drawing.Color.White;
            this._lblAddress.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this._lblAddress, "_lblAddress");
            this._lblAddress.Name = "_lblAddress";
            // 
            // _lblCompany
            // 
            this._lblCompany.BackColor = System.Drawing.Color.White;
            this._lblCompany.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this._lblCompany, "_lblCompany");
            this._lblCompany.Name = "_lblCompany";
            // 
            // panel4
            // 
            resources.ApplyResources(this.panel4, "panel4");
            this.panel4.Controls.Add(this.flowLayoutPanel3);
            this.panel4.Name = "panel4";
            // 
            // flowLayoutPanel3
            // 
            resources.ApplyResources(this.flowLayoutPanel3, "flowLayoutPanel3");
            this.flowLayoutPanel3.BackColor = System.Drawing.Color.Transparent;
            this.flowLayoutPanel3.Controls.Add(this.dpExport);
            this.flowLayoutPanel3.Controls.Add(this.dpAction);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            // 
            // dpExport
            // 
            resources.ApplyResources(this.dpExport, "dpExport");
            this.dpExport.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.dpExport.Appearance.BorderColor = System.Drawing.Color.Transparent;
            this.dpExport.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("dpExport.Appearance.Font")));
            this.dpExport.Appearance.Options.UseBackColor = true;
            this.dpExport.Appearance.Options.UseBorderColor = true;
            this.dpExport.Appearance.Options.UseFont = true;
            this.dpExport.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            this.dpExport.DropDownControl = this.popupExport;
            this.dpExport.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("dpExport.ImageOptions.Image")));
            this.dpExport.ImageOptions.ImageUri.Uri = "SendCSV;Size32x32;GrayScaled";
            this.dpExport.ImageOptions.SvgImageSize = new System.Drawing.Size(10, 10);
            this.dpExport.LookAndFeel.SkinMaskColor = System.Drawing.Color.Transparent;
            this.dpExport.LookAndFeel.SkinMaskColor2 = System.Drawing.Color.Transparent;
            this.dpExport.LookAndFeel.UseDefaultLookAndFeel = false;
            this.dpExport.Name = "dpExport";
            this.dpExport.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            // 
            // dpAction
            // 
            resources.ApplyResources(this.dpAction, "dpAction");
            this.dpAction.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.dpAction.Appearance.BorderColor = System.Drawing.Color.Transparent;
            this.dpAction.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("dpAction.Appearance.Font")));
            this.dpAction.Appearance.Options.UseBackColor = true;
            this.dpAction.Appearance.Options.UseBorderColor = true;
            this.dpAction.Appearance.Options.UseFont = true;
            this.dpAction.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dpAction.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            this.dpAction.DropDownControl = this.popActions;
            this.dpAction.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("dpAction.ImageOptions.Image")));
            this.dpAction.ImageOptions.ImageUri.Uri = "WrapText;Size32x32;GrayScaled";
            this.dpAction.ImageOptions.SvgImageColorizationMode = DevExpress.Utils.SvgImageColorizationMode.Full;
            this.dpAction.LookAndFeel.SkinMaskColor = System.Drawing.Color.Transparent;
            this.dpAction.LookAndFeel.SkinMaskColor2 = System.Drawing.Color.Transparent;
            this.dpAction.LookAndFeel.UseDefaultLookAndFeel = false;
            this.dpAction.Name = "dpAction";
            this.dpAction.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            // 
            // panel3
            // 
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Controls.Add(this._picLogo);
            this.panel3.Name = "panel3";
            // 
            // _picLogo
            // 
            resources.ApplyResources(this._picLogo, "_picLogo");
            this._picLogo.Name = "_picLogo";
            this._picLogo.TabStop = false;
            // 
            // pgvPaymentDetail
            // 
            this.pgvPaymentDetail.Appearance.ColumnHeaderArea.BackColor = System.Drawing.Color.Transparent;
            this.pgvPaymentDetail.Appearance.ColumnHeaderArea.BackColor2 = ((System.Drawing.Color)(resources.GetObject("pgvPaymentDetail.Appearance.ColumnHeaderArea.BackColor2")));
            this.pgvPaymentDetail.Appearance.ColumnHeaderArea.BorderColor = System.Drawing.Color.Transparent;
            this.pgvPaymentDetail.Appearance.ColumnHeaderArea.Options.UseBackColor = true;
            this.pgvPaymentDetail.Appearance.ColumnHeaderArea.Options.UseBorderColor = true;
            this.pgvPaymentDetail.Appearance.DataHeaderArea.BackColor = System.Drawing.Color.Silver;
            this.pgvPaymentDetail.Appearance.DataHeaderArea.BackColor2 = ((System.Drawing.Color)(resources.GetObject("pgvPaymentDetail.Appearance.DataHeaderArea.BackColor2")));
            this.pgvPaymentDetail.Appearance.DataHeaderArea.BorderColor = System.Drawing.Color.Silver;
            this.pgvPaymentDetail.Appearance.DataHeaderArea.Font = ((System.Drawing.Font)(resources.GetObject("pgvPaymentDetail.Appearance.DataHeaderArea.Font")));
            this.pgvPaymentDetail.Appearance.DataHeaderArea.Options.UseBackColor = true;
            this.pgvPaymentDetail.Appearance.DataHeaderArea.Options.UseBorderColor = true;
            this.pgvPaymentDetail.Appearance.DataHeaderArea.Options.UseFont = true;
            this.pgvPaymentDetail.Appearance.FieldHeader.BackColor = System.Drawing.Color.SkyBlue;
            this.pgvPaymentDetail.Appearance.FieldHeader.BackColor2 = ((System.Drawing.Color)(resources.GetObject("pgvPaymentDetail.Appearance.FieldHeader.BackColor2")));
            this.pgvPaymentDetail.Appearance.FieldHeader.BorderColor = System.Drawing.Color.Silver;
            this.pgvPaymentDetail.Appearance.FieldHeader.Font = ((System.Drawing.Font)(resources.GetObject("pgvPaymentDetail.Appearance.FieldHeader.Font")));
            this.pgvPaymentDetail.Appearance.FieldHeader.Options.UseBackColor = true;
            this.pgvPaymentDetail.Appearance.FieldHeader.Options.UseBorderColor = true;
            this.pgvPaymentDetail.Appearance.FieldHeader.Options.UseFont = true;
            this.pgvPaymentDetail.Appearance.FieldValue.Font = ((System.Drawing.Font)(resources.GetObject("pgvPaymentDetail.Appearance.FieldValue.Font")));
            this.pgvPaymentDetail.Appearance.FieldValue.Options.UseFont = true;
            this.pgvPaymentDetail.Appearance.FieldValueTotal.Font = ((System.Drawing.Font)(resources.GetObject("pgvPaymentDetail.Appearance.FieldValueTotal.Font")));
            this.pgvPaymentDetail.Appearance.FieldValueTotal.Options.UseFont = true;
            this.pgvPaymentDetail.Appearance.GrandTotalCell.Options.UseTextOptions = true;
            this.pgvPaymentDetail.Appearance.GrandTotalCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.pgvPaymentDetail.Appearance.HeaderArea.BackColor = System.Drawing.Color.Silver;
            this.pgvPaymentDetail.Appearance.HeaderArea.BackColor2 = ((System.Drawing.Color)(resources.GetObject("pgvPaymentDetail.Appearance.HeaderArea.BackColor2")));
            this.pgvPaymentDetail.Appearance.HeaderArea.BorderColor = System.Drawing.Color.Silver;
            this.pgvPaymentDetail.Appearance.HeaderArea.Font = ((System.Drawing.Font)(resources.GetObject("pgvPaymentDetail.Appearance.HeaderArea.Font")));
            this.pgvPaymentDetail.Appearance.HeaderArea.Options.UseBackColor = true;
            this.pgvPaymentDetail.Appearance.HeaderArea.Options.UseBorderColor = true;
            this.pgvPaymentDetail.Appearance.HeaderArea.Options.UseFont = true;
            this.pgvPaymentDetail.Appearance.RowHeaderArea.BackColor = System.Drawing.Color.Silver;
            this.pgvPaymentDetail.Appearance.RowHeaderArea.BackColor2 = ((System.Drawing.Color)(resources.GetObject("pgvPaymentDetail.Appearance.RowHeaderArea.BackColor2")));
            this.pgvPaymentDetail.Appearance.RowHeaderArea.BorderColor = System.Drawing.Color.Silver;
            this.pgvPaymentDetail.Appearance.RowHeaderArea.Font = ((System.Drawing.Font)(resources.GetObject("pgvPaymentDetail.Appearance.RowHeaderArea.Font")));
            this.pgvPaymentDetail.Appearance.RowHeaderArea.Options.UseBackColor = true;
            this.pgvPaymentDetail.Appearance.RowHeaderArea.Options.UseBorderColor = true;
            this.pgvPaymentDetail.Appearance.RowHeaderArea.Options.UseFont = true;
            this.pgvPaymentDetail.Appearance.TotalCell.Options.UseTextOptions = true;
            this.pgvPaymentDetail.Appearance.TotalCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.pgvPaymentDetail.AppearancePrint.FieldHeader.BackColor = System.Drawing.Color.Silver;
            this.pgvPaymentDetail.AppearancePrint.FieldHeader.BackColor2 = ((System.Drawing.Color)(resources.GetObject("pgvPaymentDetail.AppearancePrint.FieldHeader.BackColor2")));
            this.pgvPaymentDetail.AppearancePrint.FieldHeader.BorderColor = System.Drawing.Color.Silver;
            this.pgvPaymentDetail.AppearancePrint.FieldHeader.Options.UseBackColor = true;
            this.pgvPaymentDetail.AppearancePrint.FieldHeader.Options.UseBorderColor = true;
            this.pgvPaymentDetail.AppearancePrint.FieldValue.BackColor = System.Drawing.Color.Transparent;
            this.pgvPaymentDetail.AppearancePrint.FieldValue.BackColor2 = ((System.Drawing.Color)(resources.GetObject("pgvPaymentDetail.AppearancePrint.FieldValue.BackColor2")));
            this.pgvPaymentDetail.AppearancePrint.FieldValue.BorderColor = System.Drawing.Color.Transparent;
            this.pgvPaymentDetail.AppearancePrint.FieldValue.Options.UseBackColor = true;
            this.pgvPaymentDetail.AppearancePrint.FieldValue.Options.UseBorderColor = true;
            this.pgvPaymentDetail.AppearancePrint.FieldValueTotal.BackColor = System.Drawing.Color.Silver;
            this.pgvPaymentDetail.AppearancePrint.FieldValueTotal.BackColor2 = ((System.Drawing.Color)(resources.GetObject("pgvPaymentDetail.AppearancePrint.FieldValueTotal.BackColor2")));
            this.pgvPaymentDetail.AppearancePrint.FieldValueTotal.BorderColor = System.Drawing.Color.Silver;
            this.pgvPaymentDetail.AppearancePrint.FieldValueTotal.Options.UseBackColor = true;
            this.pgvPaymentDetail.AppearancePrint.FieldValueTotal.Options.UseBorderColor = true;
            this.pgvPaymentDetail.AppearancePrint.FieldValueTotal.Options.UseTextOptions = true;
            this.pgvPaymentDetail.AppearancePrint.FieldValueTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pgvPaymentDetail.AppearancePrint.FieldValueTotal.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            resources.ApplyResources(this.pgvPaymentDetail, "pgvPaymentDetail");
            this.pgvPaymentDetail.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.colCUSTOMER_CODE,
            this.colCUSTOMER_NAME,
            this.colPHONE_NUMBER,
            this.colCUSTOMER_TYPE,
            this.colADDRESS,
            this.colMETER_CODE,
            this.colAMPARE_NAME,
            this.colAREA,
            this.colPOLE,
            this.colBOX,
            this.colTRANSACTION_DATE,
            this.colINVOICE_DATE_BILLING,
            this.colBANK_CUT_OFF_DATE,
            this.colPAID_DATE,
            this.colDUE_DATE,
            this.colREFERENCE,
            this.colDESCRIPTION,
            this.colPAYMENT_METHOD,
            this.colPAYMENT_ACCOUNT,
            this.colPAY_AT,
            this.colRECEIVE_BY,
            this.colTOTAL_PAY,
            this.colTOTAL_VOID,
            this.colTOTAL_PAID,
            this.colSTATUS,
            this.colNOTE,
            this.colPAYMENT_TYPE_GROUP,
            this.colCurrency,
            this.colPAID_TIME,
            this.colNUMBER_OF_ROW,
            this.colCASH_DRAWER_NAME,
            this.colPAYMENT_NO,
            this.colBILLING_CYCLE,
            this.colPRICE_NAME,
            this.colINVOICE_ITEM,
            this.colINVOICE_ITEM_TYPE,
            this.colPLAN,
            this.colROW_NO,
            this.colTYPE_OF_TRANSACTION});
            this.pgvPaymentDetail.Name = "pgvPaymentDetail";
            this.pgvPaymentDetail.OptionsBehavior.HorizontalScrolling = DevExpress.XtraPivotGrid.PivotGridScrolling.Control;
            this.pgvPaymentDetail.OptionsDataField.AreaIndex = 0;
            this.pgvPaymentDetail.OptionsDataField.Caption = resources.GetString("pgvPaymentDetail.OptionsDataField.Caption");
            this.pgvPaymentDetail.OptionsFilter.DefaultFilterEditorView = DevExpress.XtraEditors.FilterEditorViewMode.VisualAndText;
            this.pgvPaymentDetail.OptionsFilterPopup.AutoFilterType = DevExpress.Utils.DefaultBoolean.True;
            this.pgvPaymentDetail.OptionsPrint.PrintDataHeaders = DevExpress.Utils.DefaultBoolean.False;
            this.pgvPaymentDetail.OptionsPrint.PrintFilterHeaders = DevExpress.Utils.DefaultBoolean.False;
            this.pgvPaymentDetail.OptionsPrint.PrintUnusedFilterFields = false;
            this.pgvPaymentDetail.OptionsPrint.PrintVertLines = DevExpress.Utils.DefaultBoolean.True;
            this.pgvPaymentDetail.OptionsPrint.UsePrintAppearance = true;
            this.pgvPaymentDetail.OptionsView.ShowColumnGrandTotalHeader = false;
            this.pgvPaymentDetail.OptionsView.ShowColumnGrandTotals = false;
            this.pgvPaymentDetail.OptionsView.ShowColumnTotals = false;
            this.pgvPaymentDetail.OptionsView.ShowRowGrandTotalHeader = false;
            this.pgvPaymentDetail.OptionsView.ShowRowGrandTotals = false;
            this.pgvPaymentDetail.OptionsView.ShowTotalsForSingleValues = true;
            this.pgvPaymentDetail.DragDrop += new System.Windows.Forms.DragEventHandler(this.pgvPaymentDetail_DragDrop);
            // 
            // colCUSTOMER_CODE
            // 
            this.colCUSTOMER_CODE.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.colCUSTOMER_CODE.AreaIndex = 3;
            this.colCUSTOMER_CODE.FieldName = "CUSTOMER_CODE";
            this.colCUSTOMER_CODE.Name = "colCUSTOMER_CODE";
            this.colCUSTOMER_CODE.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.False;
            this.colCUSTOMER_CODE.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colCUSTOMER_CODE.Options.ShowCustomTotals = false;
            this.colCUSTOMER_CODE.Options.ShowGrandTotal = false;
            this.colCUSTOMER_CODE.Options.ShowTotals = false;
            this.colCUSTOMER_CODE.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colCUSTOMER_CODE.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            // 
            // colCUSTOMER_NAME
            // 
            this.colCUSTOMER_NAME.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.colCUSTOMER_NAME.AreaIndex = 4;
            this.colCUSTOMER_NAME.FieldName = "CUSTOMER_NAME";
            this.colCUSTOMER_NAME.Name = "colCUSTOMER_NAME";
            this.colCUSTOMER_NAME.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.False;
            this.colCUSTOMER_NAME.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colCUSTOMER_NAME.Options.ShowCustomTotals = false;
            this.colCUSTOMER_NAME.Options.ShowGrandTotal = false;
            this.colCUSTOMER_NAME.Options.ShowTotals = false;
            this.colCUSTOMER_NAME.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colCUSTOMER_NAME.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            resources.ApplyResources(this.colCUSTOMER_NAME, "colCUSTOMER_NAME");
            // 
            // colPHONE_NUMBER
            // 
            this.colPHONE_NUMBER.AreaIndex = 23;
            this.colPHONE_NUMBER.FieldName = "PHONE_NUMBER";
            this.colPHONE_NUMBER.Name = "colPHONE_NUMBER";
            this.colPHONE_NUMBER.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.False;
            this.colPHONE_NUMBER.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colPHONE_NUMBER.Options.ShowCustomTotals = false;
            this.colPHONE_NUMBER.Options.ShowGrandTotal = false;
            this.colPHONE_NUMBER.Options.ShowTotals = false;
            this.colPHONE_NUMBER.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colPHONE_NUMBER.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            resources.ApplyResources(this.colPHONE_NUMBER, "colPHONE_NUMBER");
            // 
            // colCUSTOMER_TYPE
            // 
            this.colCUSTOMER_TYPE.Appearance.ValueTotal.Font = ((System.Drawing.Font)(resources.GetObject("colCUSTOMER_TYPE.Appearance.ValueTotal.Font")));
            this.colCUSTOMER_TYPE.Appearance.ValueTotal.Options.UseFont = true;
            this.colCUSTOMER_TYPE.Appearance.ValueTotal.Options.UseTextOptions = true;
            this.colCUSTOMER_TYPE.Appearance.ValueTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCUSTOMER_TYPE.Appearance.ValueTotal.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCUSTOMER_TYPE.AreaIndex = 22;
            this.colCUSTOMER_TYPE.FieldName = "CUSTOMER_TYPE";
            this.colCUSTOMER_TYPE.Name = "colCUSTOMER_TYPE";
            this.colCUSTOMER_TYPE.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.False;
            this.colCUSTOMER_TYPE.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colCUSTOMER_TYPE.Options.AllowSortBySummary = DevExpress.Utils.DefaultBoolean.False;
            this.colCUSTOMER_TYPE.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colCUSTOMER_TYPE.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            resources.ApplyResources(this.colCUSTOMER_TYPE, "colCUSTOMER_TYPE");
            // 
            // colADDRESS
            // 
            this.colADDRESS.AreaIndex = 7;
            this.colADDRESS.FieldName = "ADDRESS_CUSTOMER";
            this.colADDRESS.Name = "colADDRESS";
            this.colADDRESS.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.False;
            this.colADDRESS.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colADDRESS.Options.ShowCustomTotals = false;
            this.colADDRESS.Options.ShowGrandTotal = false;
            this.colADDRESS.Options.ShowTotals = false;
            this.colADDRESS.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colADDRESS.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            // 
            // colMETER_CODE
            // 
            this.colMETER_CODE.AreaIndex = 8;
            this.colMETER_CODE.FieldName = "METER_CODE";
            this.colMETER_CODE.Name = "colMETER_CODE";
            this.colMETER_CODE.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.False;
            this.colMETER_CODE.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colMETER_CODE.Options.ShowCustomTotals = false;
            this.colMETER_CODE.Options.ShowGrandTotal = false;
            this.colMETER_CODE.Options.ShowTotals = false;
            this.colMETER_CODE.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colMETER_CODE.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            // 
            // colAMPARE_NAME
            // 
            this.colAMPARE_NAME.AreaIndex = 0;
            this.colAMPARE_NAME.FieldName = "AMPARE_NAME";
            this.colAMPARE_NAME.Name = "colAMPARE_NAME";
            this.colAMPARE_NAME.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.False;
            this.colAMPARE_NAME.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colAMPARE_NAME.Options.ShowCustomTotals = false;
            this.colAMPARE_NAME.Options.ShowGrandTotal = false;
            this.colAMPARE_NAME.Options.ShowTotals = false;
            this.colAMPARE_NAME.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colAMPARE_NAME.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            // 
            // colAREA
            // 
            this.colAREA.AreaIndex = 1;
            this.colAREA.FieldName = "AREA_NAME";
            this.colAREA.Name = "colAREA";
            this.colAREA.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.colAREA.Options.AllowDragInCustomizationForm = DevExpress.Utils.DefaultBoolean.False;
            this.colAREA.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.False;
            this.colAREA.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.False;
            this.colAREA.Options.AllowHide = DevExpress.Utils.DefaultBoolean.False;
            this.colAREA.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colAREA.Options.ShowCustomTotals = false;
            this.colAREA.Options.ShowGrandTotal = false;
            this.colAREA.Options.ShowTotals = false;
            this.colAREA.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colAREA.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            // 
            // colPOLE
            // 
            this.colPOLE.AreaIndex = 2;
            this.colPOLE.FieldName = "POLE_NAME";
            this.colPOLE.Name = "colPOLE";
            this.colPOLE.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.False;
            this.colPOLE.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colPOLE.Options.ShowCustomTotals = false;
            this.colPOLE.Options.ShowGrandTotal = false;
            this.colPOLE.Options.ShowTotals = false;
            this.colPOLE.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colPOLE.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            // 
            // colBOX
            // 
            this.colBOX.AreaIndex = 3;
            this.colBOX.FieldName = "BOX_CODE";
            this.colBOX.Name = "colBOX";
            this.colBOX.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.False;
            this.colBOX.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colBOX.Options.ShowCustomTotals = false;
            this.colBOX.Options.ShowGrandTotal = false;
            this.colBOX.Options.ShowTotals = false;
            this.colBOX.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colBOX.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            // 
            // colTRANSACTION_DATE
            // 
            this.colTRANSACTION_DATE.AreaIndex = 4;
            this.colTRANSACTION_DATE.FieldName = "CREATE_ON";
            this.colTRANSACTION_DATE.Name = "colTRANSACTION_DATE";
            this.colTRANSACTION_DATE.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colTRANSACTION_DATE.Options.ShowCustomTotals = false;
            this.colTRANSACTION_DATE.Options.ShowGrandTotal = false;
            this.colTRANSACTION_DATE.Options.ShowTotals = false;
            this.colTRANSACTION_DATE.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colTRANSACTION_DATE.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            resources.ApplyResources(this.colTRANSACTION_DATE, "colTRANSACTION_DATE");
            // 
            // colINVOICE_DATE_BILLING
            // 
            this.colINVOICE_DATE_BILLING.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.colINVOICE_DATE_BILLING.AreaIndex = 6;
            this.colINVOICE_DATE_BILLING.FieldName = "INVOICE_DATE";
            this.colINVOICE_DATE_BILLING.Name = "colINVOICE_DATE_BILLING";
            this.colINVOICE_DATE_BILLING.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.False;
            this.colINVOICE_DATE_BILLING.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colINVOICE_DATE_BILLING.Options.ShowCustomTotals = false;
            this.colINVOICE_DATE_BILLING.Options.ShowGrandTotal = false;
            this.colINVOICE_DATE_BILLING.Options.ShowTotals = false;
            this.colINVOICE_DATE_BILLING.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colINVOICE_DATE_BILLING.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            resources.ApplyResources(this.colINVOICE_DATE_BILLING, "colINVOICE_DATE_BILLING");
            // 
            // colBANK_CUT_OFF_DATE
            // 
            this.colBANK_CUT_OFF_DATE.AreaIndex = 21;
            this.colBANK_CUT_OFF_DATE.CellFormat.FormatString = "DD-MM-YYYYY";
            this.colBANK_CUT_OFF_DATE.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colBANK_CUT_OFF_DATE.FieldName = "BANK_CUT_OFF_DATE";
            this.colBANK_CUT_OFF_DATE.Name = "colBANK_CUT_OFF_DATE";
            this.colBANK_CUT_OFF_DATE.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colBANK_CUT_OFF_DATE.Options.ShowCustomTotals = false;
            this.colBANK_CUT_OFF_DATE.Options.ShowGrandTotal = false;
            this.colBANK_CUT_OFF_DATE.Options.ShowTotals = false;
            this.colBANK_CUT_OFF_DATE.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colBANK_CUT_OFF_DATE.ToolTips.ValueFormat.FormatString = "d";
            this.colBANK_CUT_OFF_DATE.ToolTips.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colBANK_CUT_OFF_DATE.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            this.colBANK_CUT_OFF_DATE.ValueFormat.FormatString = "DD-MM-YYYYY";
            this.colBANK_CUT_OFF_DATE.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            resources.ApplyResources(this.colBANK_CUT_OFF_DATE, "colBANK_CUT_OFF_DATE");
            // 
            // colPAID_DATE
            // 
            this.colPAID_DATE.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.colPAID_DATE.AreaIndex = 8;
            this.colPAID_DATE.CellFormat.FormatString = "DD-MM-YYYYY";
            this.colPAID_DATE.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colPAID_DATE.FieldName = "SHORT_PAID_DATE";
            this.colPAID_DATE.Name = "colPAID_DATE";
            this.colPAID_DATE.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colPAID_DATE.Options.ShowCustomTotals = false;
            this.colPAID_DATE.Options.ShowGrandTotal = false;
            this.colPAID_DATE.Options.ShowTotals = false;
            this.colPAID_DATE.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colPAID_DATE.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            resources.ApplyResources(this.colPAID_DATE, "colPAID_DATE");
            // 
            // colDUE_DATE
            // 
            this.colDUE_DATE.AreaIndex = 5;
            this.colDUE_DATE.FieldName = "DUE_DATE";
            this.colDUE_DATE.Name = "colDUE_DATE";
            this.colDUE_DATE.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colDUE_DATE.Options.ShowCustomTotals = false;
            this.colDUE_DATE.Options.ShowGrandTotal = false;
            this.colDUE_DATE.Options.ShowTotals = false;
            this.colDUE_DATE.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colDUE_DATE.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            // 
            // colREFERENCE
            // 
            this.colREFERENCE.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.colREFERENCE.AreaIndex = 5;
            this.colREFERENCE.FieldName = "REFERENCE";
            this.colREFERENCE.Name = "colREFERENCE";
            this.colREFERENCE.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.colREFERENCE.Options.AllowDragInCustomizationForm = DevExpress.Utils.DefaultBoolean.False;
            this.colREFERENCE.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.False;
            this.colREFERENCE.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.False;
            this.colREFERENCE.Options.AllowHide = DevExpress.Utils.DefaultBoolean.False;
            this.colREFERENCE.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colREFERENCE.Options.ShowCustomTotals = false;
            this.colREFERENCE.Options.ShowGrandTotal = false;
            this.colREFERENCE.Options.ShowTotals = false;
            this.colREFERENCE.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colREFERENCE.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            resources.ApplyResources(this.colREFERENCE, "colREFERENCE");
            // 
            // colDESCRIPTION
            // 
            this.colDESCRIPTION.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.colDESCRIPTION.AreaIndex = 7;
            this.colDESCRIPTION.FieldName = "DESCRIPTION";
            this.colDESCRIPTION.Name = "colDESCRIPTION";
            this.colDESCRIPTION.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.colDESCRIPTION.Options.AllowDragInCustomizationForm = DevExpress.Utils.DefaultBoolean.False;
            this.colDESCRIPTION.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.False;
            this.colDESCRIPTION.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.False;
            this.colDESCRIPTION.Options.AllowHide = DevExpress.Utils.DefaultBoolean.False;
            this.colDESCRIPTION.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colDESCRIPTION.Options.ShowCustomTotals = false;
            this.colDESCRIPTION.Options.ShowGrandTotal = false;
            this.colDESCRIPTION.Options.ShowTotals = false;
            this.colDESCRIPTION.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colDESCRIPTION.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            resources.ApplyResources(this.colDESCRIPTION, "colDESCRIPTION");
            // 
            // colPAYMENT_METHOD
            // 
            this.colPAYMENT_METHOD.AreaIndex = 18;
            this.colPAYMENT_METHOD.FieldName = "PAYMENT_METHOD";
            this.colPAYMENT_METHOD.Name = "colPAYMENT_METHOD";
            this.colPAYMENT_METHOD.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colPAYMENT_METHOD.Options.ShowCustomTotals = false;
            this.colPAYMENT_METHOD.Options.ShowGrandTotal = false;
            this.colPAYMENT_METHOD.Options.ShowTotals = false;
            this.colPAYMENT_METHOD.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colPAYMENT_METHOD.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            resources.ApplyResources(this.colPAYMENT_METHOD, "colPAYMENT_METHOD");
            // 
            // colPAYMENT_ACCOUNT
            // 
            this.colPAYMENT_ACCOUNT.AreaIndex = 15;
            this.colPAYMENT_ACCOUNT.FieldName = "PAYMENT_ACCOUNT";
            this.colPAYMENT_ACCOUNT.Name = "colPAYMENT_ACCOUNT";
            this.colPAYMENT_ACCOUNT.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colPAYMENT_ACCOUNT.Options.ShowCustomTotals = false;
            this.colPAYMENT_ACCOUNT.Options.ShowGrandTotal = false;
            this.colPAYMENT_ACCOUNT.Options.ShowTotals = false;
            this.colPAYMENT_ACCOUNT.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colPAYMENT_ACCOUNT.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            resources.ApplyResources(this.colPAYMENT_ACCOUNT, "colPAYMENT_ACCOUNT");
            // 
            // colPAY_AT
            // 
            this.colPAY_AT.AreaIndex = 19;
            this.colPAY_AT.FieldName = "PAY_AT_BANK";
            this.colPAY_AT.Name = "colPAY_AT";
            this.colPAY_AT.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colPAY_AT.Options.ShowCustomTotals = false;
            this.colPAY_AT.Options.ShowGrandTotal = false;
            this.colPAY_AT.Options.ShowTotals = false;
            this.colPAY_AT.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colPAY_AT.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            resources.ApplyResources(this.colPAY_AT, "colPAY_AT");
            // 
            // colRECEIVE_BY
            // 
            this.colRECEIVE_BY.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.colRECEIVE_BY.AreaIndex = 9;
            this.colRECEIVE_BY.FieldName = "RECEIVE_BY";
            this.colRECEIVE_BY.Name = "colRECEIVE_BY";
            this.colRECEIVE_BY.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.colRECEIVE_BY.Options.AllowDragInCustomizationForm = DevExpress.Utils.DefaultBoolean.False;
            this.colRECEIVE_BY.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.False;
            this.colRECEIVE_BY.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.False;
            this.colRECEIVE_BY.Options.AllowHide = DevExpress.Utils.DefaultBoolean.False;
            this.colRECEIVE_BY.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colRECEIVE_BY.Options.ShowCustomTotals = false;
            this.colRECEIVE_BY.Options.ShowGrandTotal = false;
            this.colRECEIVE_BY.Options.ShowTotals = false;
            this.colRECEIVE_BY.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colRECEIVE_BY.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            resources.ApplyResources(this.colRECEIVE_BY, "colRECEIVE_BY");
            // 
            // colTOTAL_PAY
            // 
            this.colTOTAL_PAY.Appearance.Header.Font = ((System.Drawing.Font)(resources.GetObject("colTOTAL_PAY.Appearance.Header.Font")));
            this.colTOTAL_PAY.Appearance.Header.Options.UseFont = true;
            this.colTOTAL_PAY.Appearance.Value.Font = ((System.Drawing.Font)(resources.GetObject("colTOTAL_PAY.Appearance.Value.Font")));
            this.colTOTAL_PAY.Appearance.Value.Options.UseFont = true;
            this.colTOTAL_PAY.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.colTOTAL_PAY.AreaIndex = 0;
            this.colTOTAL_PAY.FieldName = "TOTAL_PAY";
            this.colTOTAL_PAY.Name = "colTOTAL_PAY";
            this.colTOTAL_PAY.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colTOTAL_PAY.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            resources.ApplyResources(this.colTOTAL_PAY, "colTOTAL_PAY");
            // 
            // colTOTAL_VOID
            // 
            this.colTOTAL_VOID.Appearance.Header.BackColor = System.Drawing.Color.Transparent;
            this.colTOTAL_VOID.Appearance.Header.BackColor2 = ((System.Drawing.Color)(resources.GetObject("colTOTAL_VOID.Appearance.Header.BackColor2")));
            this.colTOTAL_VOID.Appearance.Header.BorderColor = System.Drawing.Color.Transparent;
            this.colTOTAL_VOID.Appearance.Header.Options.UseBackColor = true;
            this.colTOTAL_VOID.Appearance.Header.Options.UseBorderColor = true;
            this.colTOTAL_VOID.AreaIndex = 27;
            this.colTOTAL_VOID.FieldName = "TOTAL_VOID";
            this.colTOTAL_VOID.Name = "colTOTAL_VOID";
            this.colTOTAL_VOID.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colTOTAL_VOID.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            resources.ApplyResources(this.colTOTAL_VOID, "colTOTAL_VOID");
            // 
            // colTOTAL_PAID
            // 
            this.colTOTAL_PAID.Appearance.Header.BackColor = System.Drawing.Color.Transparent;
            this.colTOTAL_PAID.Appearance.Header.BackColor2 = ((System.Drawing.Color)(resources.GetObject("colTOTAL_PAID.Appearance.Header.BackColor2")));
            this.colTOTAL_PAID.Appearance.Header.BorderColor = System.Drawing.Color.Transparent;
            this.colTOTAL_PAID.Appearance.Header.Options.UseBackColor = true;
            this.colTOTAL_PAID.Appearance.Header.Options.UseBorderColor = true;
            this.colTOTAL_PAID.AreaIndex = 26;
            this.colTOTAL_PAID.FieldName = "TOTAL_PAID";
            this.colTOTAL_PAID.Name = "colTOTAL_PAID";
            this.colTOTAL_PAID.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colTOTAL_PAID.Options.ShowCustomTotals = false;
            this.colTOTAL_PAID.Options.ShowGrandTotal = false;
            this.colTOTAL_PAID.Options.ShowTotals = false;
            this.colTOTAL_PAID.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            resources.ApplyResources(this.colTOTAL_PAID, "colTOTAL_PAID");
            // 
            // colSTATUS
            // 
            this.colSTATUS.AreaIndex = 16;
            this.colSTATUS.FieldName = "STATUS";
            this.colSTATUS.Name = "colSTATUS";
            this.colSTATUS.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.False;
            this.colSTATUS.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colSTATUS.Options.ShowCustomTotals = false;
            this.colSTATUS.Options.ShowGrandTotal = false;
            this.colSTATUS.Options.ShowTotals = false;
            this.colSTATUS.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            // 
            // colNOTE
            // 
            this.colNOTE.AreaIndex = 6;
            this.colNOTE.FieldName = "NOTE";
            this.colNOTE.Name = "colNOTE";
            this.colNOTE.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.False;
            this.colNOTE.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colNOTE.Options.ShowCustomTotals = false;
            this.colNOTE.Options.ShowGrandTotal = false;
            this.colNOTE.Options.ShowTotals = false;
            this.colNOTE.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colNOTE.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            // 
            // colPAYMENT_TYPE_GROUP
            // 
            this.colPAYMENT_TYPE_GROUP.AreaIndex = 17;
            this.colPAYMENT_TYPE_GROUP.FieldName = "PAYMENT_TYPE_GROUP";
            this.colPAYMENT_TYPE_GROUP.Name = "colPAYMENT_TYPE_GROUP";
            this.colPAYMENT_TYPE_GROUP.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colPAYMENT_TYPE_GROUP.Options.ShowCustomTotals = false;
            this.colPAYMENT_TYPE_GROUP.Options.ShowGrandTotal = false;
            this.colPAYMENT_TYPE_GROUP.Options.ShowTotals = false;
            this.colPAYMENT_TYPE_GROUP.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colPAYMENT_TYPE_GROUP.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            resources.ApplyResources(this.colPAYMENT_TYPE_GROUP, "colPAYMENT_TYPE_GROUP");
            // 
            // colCurrency
            // 
            this.colCurrency.Appearance.ValueTotal.Font = ((System.Drawing.Font)(resources.GetObject("colCurrency.Appearance.ValueTotal.Font")));
            this.colCurrency.Appearance.ValueTotal.Options.UseFont = true;
            this.colCurrency.Appearance.ValueTotal.Options.UseTextOptions = true;
            this.colCurrency.Appearance.ValueTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCurrency.Appearance.ValueTotal.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCurrency.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.colCurrency.AreaIndex = 0;
            this.colCurrency.FieldName = "CURRENCY";
            this.colCurrency.Name = "colCurrency";
            this.colCurrency.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.False;
            this.colCurrency.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colCurrency.Options.AllowSortBySummary = DevExpress.Utils.DefaultBoolean.False;
            this.colCurrency.Options.ShowCustomTotals = false;
            this.colCurrency.Options.ShowGrandTotal = false;
            this.colCurrency.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            // 
            // colPAID_TIME
            // 
            this.colPAID_TIME.AreaIndex = 20;
            this.colPAID_TIME.CellFormat.FormatString = "hh:mm:ss tt";
            this.colPAID_TIME.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colPAID_TIME.FieldName = "EXACT_PAID_DATE";
            this.colPAID_TIME.Name = "colPAID_TIME";
            this.colPAID_TIME.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colPAID_TIME.Options.ShowCustomTotals = false;
            this.colPAID_TIME.Options.ShowGrandTotal = false;
            this.colPAID_TIME.Options.ShowTotals = false;
            this.colPAID_TIME.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colPAID_TIME.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            this.colPAID_TIME.ValueFormat.FormatString = "hh:mm:ss tt";
            this.colPAID_TIME.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            resources.ApplyResources(this.colPAID_TIME, "colPAID_TIME");
            // 
            // colNUMBER_OF_ROW
            // 
            this.colNUMBER_OF_ROW.Appearance.Header.BackColor = System.Drawing.Color.Transparent;
            this.colNUMBER_OF_ROW.Appearance.Header.BackColor2 = ((System.Drawing.Color)(resources.GetObject("colNUMBER_OF_ROW.Appearance.Header.BackColor2")));
            this.colNUMBER_OF_ROW.Appearance.Header.BorderColor = System.Drawing.Color.Transparent;
            this.colNUMBER_OF_ROW.Appearance.Header.Options.UseBackColor = true;
            this.colNUMBER_OF_ROW.Appearance.Header.Options.UseBorderColor = true;
            this.colNUMBER_OF_ROW.AreaIndex = 24;
            this.colNUMBER_OF_ROW.FieldName = "NUMBER_OF_INVOICE";
            this.colNUMBER_OF_ROW.Name = "colNUMBER_OF_ROW";
            this.colNUMBER_OF_ROW.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colNUMBER_OF_ROW.Options.ShowCustomTotals = false;
            this.colNUMBER_OF_ROW.Options.ShowGrandTotal = false;
            this.colNUMBER_OF_ROW.Options.ShowTotals = false;
            this.colNUMBER_OF_ROW.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colNUMBER_OF_ROW.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Count;
            this.colNUMBER_OF_ROW.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            // 
            // colCASH_DRAWER_NAME
            // 
            this.colCASH_DRAWER_NAME.Appearance.ValueTotal.Font = ((System.Drawing.Font)(resources.GetObject("colCASH_DRAWER_NAME.Appearance.ValueTotal.Font")));
            this.colCASH_DRAWER_NAME.Appearance.ValueTotal.Options.UseFont = true;
            this.colCASH_DRAWER_NAME.Appearance.ValueTotal.Options.UseTextOptions = true;
            this.colCASH_DRAWER_NAME.Appearance.ValueTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCASH_DRAWER_NAME.Appearance.ValueTotal.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCASH_DRAWER_NAME.AreaIndex = 9;
            resources.ApplyResources(this.colCASH_DRAWER_NAME, "colCASH_DRAWER_NAME");
            this.colCASH_DRAWER_NAME.FieldName = "CASH_DRAWER_NAME";
            this.colCASH_DRAWER_NAME.Name = "colCASH_DRAWER_NAME";
            this.colCASH_DRAWER_NAME.Options.AllowSortBySummary = DevExpress.Utils.DefaultBoolean.False;
            this.colCASH_DRAWER_NAME.Options.ShowCustomTotals = false;
            this.colCASH_DRAWER_NAME.Options.ShowGrandTotal = false;
            this.colCASH_DRAWER_NAME.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colCASH_DRAWER_NAME.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            // 
            // colPAYMENT_NO
            // 
            this.colPAYMENT_NO.AreaIndex = 11;
            resources.ApplyResources(this.colPAYMENT_NO, "colPAYMENT_NO");
            this.colPAYMENT_NO.FieldName = "PAYMENT_NO";
            this.colPAYMENT_NO.Name = "colPAYMENT_NO";
            this.colPAYMENT_NO.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.colPAYMENT_NO.Options.AllowDragInCustomizationForm = DevExpress.Utils.DefaultBoolean.False;
            this.colPAYMENT_NO.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.False;
            this.colPAYMENT_NO.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.False;
            this.colPAYMENT_NO.Options.AllowHide = DevExpress.Utils.DefaultBoolean.False;
            this.colPAYMENT_NO.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colPAYMENT_NO.Options.ShowCustomTotals = false;
            this.colPAYMENT_NO.Options.ShowGrandTotal = false;
            this.colPAYMENT_NO.Options.ShowTotals = false;
            this.colPAYMENT_NO.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colPAYMENT_NO.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            // 
            // colBILLING_CYCLE
            // 
            this.colBILLING_CYCLE.AreaIndex = 10;
            resources.ApplyResources(this.colBILLING_CYCLE, "colBILLING_CYCLE");
            this.colBILLING_CYCLE.FieldName = "BILLING_CYCLE";
            this.colBILLING_CYCLE.Name = "colBILLING_CYCLE";
            this.colBILLING_CYCLE.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colBILLING_CYCLE.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            // 
            // colPRICE_NAME
            // 
            this.colPRICE_NAME.AreaIndex = 12;
            resources.ApplyResources(this.colPRICE_NAME, "colPRICE_NAME");
            this.colPRICE_NAME.FieldName = "PRICE_NAME";
            this.colPRICE_NAME.Name = "colPRICE_NAME";
            this.colPRICE_NAME.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colPRICE_NAME.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            // 
            // colINVOICE_ITEM
            // 
            this.colINVOICE_ITEM.Appearance.ValueTotal.Font = ((System.Drawing.Font)(resources.GetObject("colINVOICE_ITEM.Appearance.ValueTotal.Font")));
            this.colINVOICE_ITEM.Appearance.ValueTotal.Options.UseFont = true;
            this.colINVOICE_ITEM.Appearance.ValueTotal.Options.UseTextOptions = true;
            this.colINVOICE_ITEM.Appearance.ValueTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colINVOICE_ITEM.Appearance.ValueTotal.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colINVOICE_ITEM.AreaIndex = 25;
            resources.ApplyResources(this.colINVOICE_ITEM, "colINVOICE_ITEM");
            this.colINVOICE_ITEM.FieldName = "INVOICE_ITEM_NAME";
            this.colINVOICE_ITEM.Name = "colINVOICE_ITEM";
            this.colINVOICE_ITEM.Options.AllowSortBySummary = DevExpress.Utils.DefaultBoolean.False;
            this.colINVOICE_ITEM.Options.ShowCustomTotals = false;
            this.colINVOICE_ITEM.Options.ShowGrandTotal = false;
            this.colINVOICE_ITEM.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colINVOICE_ITEM.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            // 
            // colINVOICE_ITEM_TYPE
            // 
            this.colINVOICE_ITEM_TYPE.Appearance.ValueTotal.Font = ((System.Drawing.Font)(resources.GetObject("colINVOICE_ITEM_TYPE.Appearance.ValueTotal.Font")));
            this.colINVOICE_ITEM_TYPE.Appearance.ValueTotal.Options.UseFont = true;
            this.colINVOICE_ITEM_TYPE.Appearance.ValueTotal.Options.UseTextOptions = true;
            this.colINVOICE_ITEM_TYPE.Appearance.ValueTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colINVOICE_ITEM_TYPE.Appearance.ValueTotal.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colINVOICE_ITEM_TYPE.AreaIndex = 13;
            resources.ApplyResources(this.colINVOICE_ITEM_TYPE, "colINVOICE_ITEM_TYPE");
            this.colINVOICE_ITEM_TYPE.FieldName = "INVOICE_ITEM_TYPE_NAME";
            this.colINVOICE_ITEM_TYPE.Name = "colINVOICE_ITEM_TYPE";
            this.colINVOICE_ITEM_TYPE.Options.AllowSortBySummary = DevExpress.Utils.DefaultBoolean.False;
            this.colINVOICE_ITEM_TYPE.Options.ShowCustomTotals = false;
            this.colINVOICE_ITEM_TYPE.Options.ShowGrandTotal = false;
            this.colINVOICE_ITEM_TYPE.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colINVOICE_ITEM_TYPE.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            // 
            // colPLAN
            // 
            this.colPLAN.AreaIndex = 14;
            resources.ApplyResources(this.colPLAN, "colPLAN");
            this.colPLAN.FieldName = "PLAN";
            this.colPLAN.Name = "colPLAN";
            this.colPLAN.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colPLAN.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            // 
            // colROW_NO
            // 
            this.colROW_NO.Appearance.Header.Options.UseTextOptions = true;
            this.colROW_NO.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colROW_NO.Appearance.Header.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colROW_NO.Appearance.Value.Options.UseTextOptions = true;
            this.colROW_NO.Appearance.Value.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colROW_NO.Appearance.Value.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colROW_NO.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.colROW_NO.AreaIndex = 2;
            this.colROW_NO.FieldName = "ROW_NO";
            this.colROW_NO.Name = "colROW_NO";
            this.colROW_NO.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.colROW_NO.Options.AllowDragInCustomizationForm = DevExpress.Utils.DefaultBoolean.False;
            this.colROW_NO.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.False;
            this.colROW_NO.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.False;
            this.colROW_NO.Options.AllowHide = DevExpress.Utils.DefaultBoolean.False;
            this.colROW_NO.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colROW_NO.Options.ShowCustomTotals = false;
            this.colROW_NO.Options.ShowGrandTotal = false;
            this.colROW_NO.Options.ShowTotals = false;
            this.colROW_NO.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colROW_NO.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            resources.ApplyResources(this.colROW_NO, "colROW_NO");
            // 
            // colTYPE_OF_TRANSACTION
            // 
            this.colTYPE_OF_TRANSACTION.Appearance.ValueTotal.Font = ((System.Drawing.Font)(resources.GetObject("colTYPE_OF_TRANSACTION.Appearance.ValueTotal.Font")));
            this.colTYPE_OF_TRANSACTION.Appearance.ValueTotal.Options.UseFont = true;
            this.colTYPE_OF_TRANSACTION.Appearance.ValueTotal.Options.UseTextOptions = true;
            this.colTYPE_OF_TRANSACTION.Appearance.ValueTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTYPE_OF_TRANSACTION.Appearance.ValueTotal.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTYPE_OF_TRANSACTION.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.colTYPE_OF_TRANSACTION.AreaIndex = 1;
            this.colTYPE_OF_TRANSACTION.FieldName = "TYPE_OF_TRANSACTION";
            this.colTYPE_OF_TRANSACTION.Name = "colTYPE_OF_TRANSACTION";
            this.colTYPE_OF_TRANSACTION.Options.AllowSortBySummary = DevExpress.Utils.DefaultBoolean.False;
            this.colTYPE_OF_TRANSACTION.Options.ShowCustomTotals = false;
            this.colTYPE_OF_TRANSACTION.Options.ShowGrandTotal = false;
            this.colTYPE_OF_TRANSACTION.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            resources.ApplyResources(this.colTYPE_OF_TRANSACTION, "colTYPE_OF_TRANSACTION");
            // 
            // PageReportPayment
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.pgvPaymentDetail);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "PageReportPayment";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupExport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popActions)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.flowLayoutPanel3.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._picLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pgvPaymentDetail)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Panel panel1;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField1;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField2;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField3;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField4;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField5;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField6;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField7;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField8;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField9;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField10;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField11;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField12;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField13;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField14;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField15;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField16;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField17;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField18;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField19;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField20;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField21;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField22;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField23;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField24;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField25;
        private DevExpress.XtraBars.PopupMenu popupExport;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnExcel;
        private DevExpress.XtraBars.BarButtonItem btnCsv;
        private DevExpress.XtraBars.BarButtonItem btnPdf;
        private DevExpress.XtraBars.BarButtonItem btnPRINT;
        private ReportDatePicker rdpDate;
        private DevExpress.XtraBars.PopupMenu popActions;
        private DevExpress.XtraPivotGrid.PivotGridControl pgvPaymentDetail;
        private DevExpress.XtraPivotGrid.PivotGridField colCUSTOMER_CODE;
        private DevExpress.XtraPivotGrid.PivotGridField colCUSTOMER_NAME;
        private DevExpress.XtraPivotGrid.PivotGridField colPHONE_NUMBER;
        private DevExpress.XtraPivotGrid.PivotGridField colCUSTOMER_TYPE;
        private DevExpress.XtraPivotGrid.PivotGridField colADDRESS;
        private DevExpress.XtraPivotGrid.PivotGridField colMETER_CODE;
        private DevExpress.XtraPivotGrid.PivotGridField colAMPARE_NAME;
        private DevExpress.XtraPivotGrid.PivotGridField colAREA;
        private DevExpress.XtraPivotGrid.PivotGridField colPOLE;
        private DevExpress.XtraPivotGrid.PivotGridField colBOX;
        private DevExpress.XtraPivotGrid.PivotGridField colTRANSACTION_DATE;
        private DevExpress.XtraPivotGrid.PivotGridField colINVOICE_DATE_BILLING;
        private DevExpress.XtraPivotGrid.PivotGridField colBANK_CUT_OFF_DATE;
        private DevExpress.XtraPivotGrid.PivotGridField colPAID_DATE;
        private DevExpress.XtraPivotGrid.PivotGridField colDUE_DATE;
        private DevExpress.XtraPivotGrid.PivotGridField colREFERENCE;
        private DevExpress.XtraPivotGrid.PivotGridField colDESCRIPTION;
        private DevExpress.XtraPivotGrid.PivotGridField colPAYMENT_METHOD;
        private DevExpress.XtraPivotGrid.PivotGridField colPAY_AT;
        private DevExpress.XtraPivotGrid.PivotGridField colRECEIVE_BY;
        private DevExpress.XtraPivotGrid.PivotGridField colTOTAL_PAY;
        private DevExpress.XtraPivotGrid.PivotGridField colTOTAL_VOID;
        private DevExpress.XtraPivotGrid.PivotGridField colTOTAL_PAID;
        private DevExpress.XtraPivotGrid.PivotGridField colSTATUS;
        private DevExpress.XtraPivotGrid.PivotGridField colNOTE;
        private DevExpress.XtraPivotGrid.PivotGridField colPAYMENT_TYPE_GROUP;
        private DevExpress.XtraPivotGrid.PivotGridField colCurrency;
        private DevExpress.XtraPivotGrid.PivotGridField colPAID_TIME;
        private DevExpress.XtraPivotGrid.PivotGridField colNUMBER_OF_ROW;
        private DevExpress.XtraPivotGrid.PivotGridField colCASH_DRAWER_NAME;
        private DevExpress.XtraPivotGrid.PivotGridField colPAYMENT_NO;
        private DevExpress.XtraPivotGrid.PivotGridField colBILLING_CYCLE;
        private DevExpress.XtraPivotGrid.PivotGridField colPRICE_NAME;
        private DevExpress.XtraPivotGrid.PivotGridField colINVOICE_ITEM;
        private DevExpress.XtraPivotGrid.PivotGridField colINVOICE_ITEM_TYPE;
        private DevExpress.XtraPivotGrid.PivotGridField colPLAN;
        private Panel panel2;
        private Label _lblSubTitle1;
        private Label _lblTitle;
        private Label _lblAddress;
        private Label _lblCompany;
        private Panel panel4;
        private FlowLayoutPanel flowLayoutPanel3;
        private DevExpress.XtraEditors.DropDownButton dpExport;
        private DevExpress.XtraEditors.DropDownButton dpAction;
        private Panel panel3;
        private Panel panel5;
        private PictureBox _picLogo;
        private DevExpress.XtraPivotGrid.PivotGridField colPAYMENT_ACCOUNT;
        private DevExpress.XtraPivotGrid.PivotGridField colROW_NO;
        private DevExpress.XtraBars.BarButtonItem btnSAVE_TEMPLATE;
        private DevExpress.XtraPivotGrid.PivotGridField colTYPE_OF_TRANSACTION;
    }
}
