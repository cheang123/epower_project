﻿using EPower.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EPower.Interface
{
    public partial class DialogUploadUsage : ExDialog
    {

        public DialogUploadUsage()
        {
            InitializeComponent();



            txtArea.Text = Resources.ALL_AREA;
            txtCycle.Text = Resources.ALL_CYCLE;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        List<TBL_BILLING_CYCLE> mCycles = DBDataContext.Db.TBL_BILLING_CYCLEs.Where(x => x.IS_ACTIVE).ToList();
        List<TBL_AREA> mAreas = DBDataContext.Db.TBL_AREAs.Where(x => x.IS_ACTIVE).ToList();

        private void btnSelectCycle_Click(object sender, EventArgs e)
        {
            DialogSelectCycleUsage dig = new DialogSelectCycleUsage(mCycles);
            dig.ShowDialog();
            mCycles = dig.SelectCycles;
            txtCycle.Text = dig.DisplaySelect;
        }

        private void btnSelectArea_Click(object sender, EventArgs e)
        {
            DialogSelectAreaUsage dig = new DialogSelectAreaUsage(mAreas);
            dig.ShowDialog();
            mAreas = dig.SelectedAreas;
            txtArea.Text = dig.DisplaySelected;
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            int nCus = 0;
            Runner.RunNewThread(delegate ()
            {
                nCus = ServiceUsage.Intance.UploadUsage(mCycles, mAreas);
            }, Resources.MS_SEND_DATA_USAGE_TO_SERVER);
            txtTotalCustomer.Text = nCus.ToString("N0");
        }
    }
}