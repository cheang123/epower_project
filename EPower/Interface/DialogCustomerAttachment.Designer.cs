﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogCustomerAttachment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCustomerAttachment));
            this._btnBrowse = new System.Windows.Forms.Button();
            this.dtpCreatedOn = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.lblCREATED_ON = new System.Windows.Forms.Label();
            this.txtFILE_NAME = new System.Windows.Forms.TextBox();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.lblNote = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtAttachmentName = new System.Windows.Forms.TextBox();
            this.lblATTACHMENT_NAME = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblFILE_LOCATION = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.cboFILE_TYPE_NAME = new System.Windows.Forms.ComboBox();
            this.lblFILE_NAME_TYPE = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.lblFILE_NAME_TYPE);
            this.content.Controls.Add(this.cboFILE_TYPE_NAME);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this._btnBrowse);
            this.content.Controls.Add(this.dtpCreatedOn);
            this.content.Controls.Add(this.label4);
            this.content.Controls.Add(this.lblCREATED_ON);
            this.content.Controls.Add(this.txtFILE_NAME);
            this.content.Controls.Add(this.txtNote);
            this.content.Controls.Add(this.lblNote);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.label5);
            this.content.Controls.Add(this.txtAttachmentName);
            this.content.Controls.Add(this.lblATTACHMENT_NAME);
            this.content.Controls.Add(this.label2);
            this.content.Controls.Add(this.lblFILE_LOCATION);
            this.content.Controls.Add(this.label16);
            this.content.Controls.Add(this.label12);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.label12, 0);
            this.content.Controls.SetChildIndex(this.label16, 0);
            this.content.Controls.SetChildIndex(this.lblFILE_LOCATION, 0);
            this.content.Controls.SetChildIndex(this.label2, 0);
            this.content.Controls.SetChildIndex(this.lblATTACHMENT_NAME, 0);
            this.content.Controls.SetChildIndex(this.txtAttachmentName, 0);
            this.content.Controls.SetChildIndex(this.label5, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.lblNote, 0);
            this.content.Controls.SetChildIndex(this.txtNote, 0);
            this.content.Controls.SetChildIndex(this.txtFILE_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblCREATED_ON, 0);
            this.content.Controls.SetChildIndex(this.label4, 0);
            this.content.Controls.SetChildIndex(this.dtpCreatedOn, 0);
            this.content.Controls.SetChildIndex(this._btnBrowse, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.cboFILE_TYPE_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblFILE_NAME_TYPE, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            // 
            // _btnBrowse
            // 
            resources.ApplyResources(this._btnBrowse, "_btnBrowse");
            this._btnBrowse.Name = "_btnBrowse";
            this._btnBrowse.UseVisualStyleBackColor = true;
            this._btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // dtpCreatedOn
            // 
            resources.ApplyResources(this.dtpCreatedOn, "dtpCreatedOn");
            this.dtpCreatedOn.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpCreatedOn.Name = "dtpCreatedOn";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // lblCREATED_ON
            // 
            resources.ApplyResources(this.lblCREATED_ON, "lblCREATED_ON");
            this.lblCREATED_ON.Name = "lblCREATED_ON";
            // 
            // txtFILE_NAME
            // 
            resources.ApplyResources(this.txtFILE_NAME, "txtFILE_NAME");
            this.txtFILE_NAME.Name = "txtFILE_NAME";
            this.txtFILE_NAME.ReadOnly = true;
            // 
            // txtNote
            // 
            resources.ApplyResources(this.txtNote, "txtNote");
            this.txtNote.Name = "txtNote";
            // 
            // lblNote
            // 
            resources.ApplyResources(this.lblNote, "lblNote");
            this.lblNote.Name = "lblNote";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // txtAttachmentName
            // 
            resources.ApplyResources(this.txtAttachmentName, "txtAttachmentName");
            this.txtAttachmentName.Name = "txtAttachmentName";
            // 
            // lblATTACHMENT_NAME
            // 
            resources.ApplyResources(this.lblATTACHMENT_NAME, "lblATTACHMENT_NAME");
            this.lblATTACHMENT_NAME.Name = "lblATTACHMENT_NAME";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // lblFILE_LOCATION
            // 
            resources.ApplyResources(this.lblFILE_LOCATION, "lblFILE_LOCATION");
            this.lblFILE_LOCATION.Name = "lblFILE_LOCATION";
            // 
            // label16
            // 
            resources.ApplyResources(this.label16, "label16");
            this.label16.Name = "label16";
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Name = "label12";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // cboFILE_TYPE_NAME
            // 
            this.cboFILE_TYPE_NAME.FormattingEnabled = true;
            resources.ApplyResources(this.cboFILE_TYPE_NAME, "cboFILE_TYPE_NAME");
            this.cboFILE_TYPE_NAME.Name = "cboFILE_TYPE_NAME";
            // 
            // lblFILE_NAME_TYPE
            // 
            resources.ApplyResources(this.lblFILE_NAME_TYPE, "lblFILE_NAME_TYPE");
            this.lblFILE_NAME_TYPE.Name = "lblFILE_NAME_TYPE";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // DialogCustomerAttachment
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogCustomerAttachment";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Button _btnBrowse;
        private DateTimePicker dtpCreatedOn;
        private Label label4;
        private Label lblCREATED_ON;
        private TextBox txtFILE_NAME;
        private TextBox txtNote;
        private Label lblNote;
        private Label label9;
        private Label label5;
        private TextBox txtAttachmentName;
        private Label lblATTACHMENT_NAME;
        private Label label2;
        private Label lblFILE_LOCATION;
        private Label label16;
        private Label label12;
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private ComboBox cboFILE_TYPE_NAME;
        private Label lblFILE_NAME_TYPE;
        private Label label1;
    }
}