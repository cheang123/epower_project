﻿using System;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System.Linq;

namespace EPower.Interface
{
    public partial class DialogPageBoxType : ExDialog
    {
        

        #region Constructor
        public DialogPageBoxType()
        {
            InitializeComponent();
            load();
        }

        #endregion

        #region Method

        private void load()
        {
            dgvBoxType.DataSource = DBDataContext.Db.TLKP_BOX_TYPEs.Where(x => x.BOX_TYPE_NAME.ToUpper().Contains(txtQuickSearch.Text.ToUpper()));
        }

        private bool IsDeletable()
        {
            bool val = false;
            if (dgvBoxType.SelectedRows.Count > 0)
            {
                val = true;
                int boxTypeId = (int)dgvBoxType.SelectedRows[0].Cells[BOX_TYPE_ID.Name].Value;
                if (DBDataContext.Db.TBL_BOXes.Where(x => x.BOX_TYPE_ID == boxTypeId && x.STATUS_ID!=(int)BoxStatus.Unavailable).Count() > 0)
                {
                    MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE, Resources.INFORMATION);
                    val = false;
                }
            }
            return val;
        }

        #endregion

        private void btnADD_Click(object sender, EventArgs e)
        {
            DialogBoxType dig = new DialogBoxType(GeneralProcess.Insert, new TLKP_BOX_TYPE() { BOX_TYPE_NAME = "" });
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txtQuickSearch_QuickSearch(null, null);
                UIHelper.SelectRow(dgvBoxType, dig.BoxType.BOX_TYPE_ID);
            }
        }

        private void btnEDIT_Click(object sender, EventArgs e)
        {
            if (dgvBoxType.Rows.Count > 0)
            {
                int boxTypeId = (int)dgvBoxType.SelectedRows[0].Cells[BOX_TYPE_ID.Name].Value;
                TLKP_BOX_TYPE objBoxType = DBDataContext.Db.TLKP_BOX_TYPEs.FirstOrDefault(x => x.BOX_TYPE_ID ==boxTypeId );
                DialogBoxType dig = new DialogBoxType(GeneralProcess.Update, objBoxType);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txtQuickSearch_QuickSearch(null, null);
                    UIHelper.SelectRow(dgvBoxType, dig.BoxType.BOX_TYPE_ID);
                }
            }
        }

        private void btnREMOVE_Click(object sender, EventArgs e)
        {
            if (IsDeletable())
            {
                int boxTypeId = (int)dgvBoxType.SelectedRows[0].Cells[BOX_TYPE_ID.Name].Value;
                TLKP_BOX_TYPE objBoxType = DBDataContext.Db.TLKP_BOX_TYPEs.FirstOrDefault(x => x.BOX_TYPE_ID == boxTypeId);
                DialogBoxType dig = new DialogBoxType(GeneralProcess.Delete, objBoxType);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txtQuickSearch_QuickSearch(null, null);
                    UIHelper.SelectRow(dgvBoxType, dig.BoxType.BOX_TYPE_ID);
                }
            }
        }

        private void txtQuickSearch_QuickSearch(object sender, EventArgs e)
        {
            load();
        }

        private void btnBox_Click(object sender, EventArgs e)
        {
            if (dgvBoxType.SelectedRows.Count> 0)
            {
                new DialogBoxTypeList((int)dgvBoxType.SelectedRows[0].Cells[BOX_TYPE_ID.Name].Value).ShowDialog();
            }
            
        }
        private void ChangeKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer; 
        }
    }
}