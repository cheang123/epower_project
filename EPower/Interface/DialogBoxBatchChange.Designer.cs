﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogBoxBatchChange
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogBoxBatchChange));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.lblPOLE = new System.Windows.Forms.Label();
            this.cboPole = new System.Windows.Forms.ComboBox();
            this.cboStatus = new System.Windows.Forms.ComboBox();
            this.lblSTATUS = new System.Windows.Forms.Label();
            this.btnAddPole = new SoftTech.Component.ExAddItem();
            this.lblBOX_TYPE = new System.Windows.Forms.Label();
            this.cboBoxType = new System.Windows.Forms.ComboBox();
            this.lblEND_USE_DATE = new System.Windows.Forms.Label();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.lblSTART_USE_DATE = new System.Windows.Forms.Label();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.btnBrowse_ = new SoftTech.Component.ExButton();
            this.lblPole_ = new System.Windows.Forms.Label();
            this.lblTOTAL_BOX_UPDATE = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.lblPole_);
            this.content.Controls.Add(this.lblTOTAL_BOX_UPDATE);
            this.content.Controls.Add(this.btnBrowse_);
            this.content.Controls.Add(this.lblEND_USE_DATE);
            this.content.Controls.Add(this.dtpEndDate);
            this.content.Controls.Add(this.lblSTART_USE_DATE);
            this.content.Controls.Add(this.dtpStartDate);
            this.content.Controls.Add(this.btnAddPole);
            this.content.Controls.Add(this.cboStatus);
            this.content.Controls.Add(this.lblSTATUS);
            this.content.Controls.Add(this.cboBoxType);
            this.content.Controls.Add(this.lblBOX_TYPE);
            this.content.Controls.Add(this.cboPole);
            this.content.Controls.Add(this.lblPOLE);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.lblPOLE, 0);
            this.content.Controls.SetChildIndex(this.cboPole, 0);
            this.content.Controls.SetChildIndex(this.lblBOX_TYPE, 0);
            this.content.Controls.SetChildIndex(this.cboBoxType, 0);
            this.content.Controls.SetChildIndex(this.lblSTATUS, 0);
            this.content.Controls.SetChildIndex(this.cboStatus, 0);
            this.content.Controls.SetChildIndex(this.btnAddPole, 0);
            this.content.Controls.SetChildIndex(this.dtpStartDate, 0);
            this.content.Controls.SetChildIndex(this.lblSTART_USE_DATE, 0);
            this.content.Controls.SetChildIndex(this.dtpEndDate, 0);
            this.content.Controls.SetChildIndex(this.lblEND_USE_DATE, 0);
            this.content.Controls.SetChildIndex(this.btnBrowse_, 0);
            this.content.Controls.SetChildIndex(this.lblTOTAL_BOX_UPDATE, 0);
            this.content.Controls.SetChildIndex(this.lblPole_, 0);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lblPOLE
            // 
            resources.ApplyResources(this.lblPOLE, "lblPOLE");
            this.lblPOLE.Name = "lblPOLE";
            // 
            // cboPole
            // 
            this.cboPole.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPole.FormattingEnabled = true;
            resources.ApplyResources(this.cboPole, "cboPole");
            this.cboPole.Name = "cboPole";
            // 
            // cboStatus
            // 
            this.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboStatus.FormattingEnabled = true;
            resources.ApplyResources(this.cboStatus, "cboStatus");
            this.cboStatus.Name = "cboStatus";
            // 
            // lblSTATUS
            // 
            resources.ApplyResources(this.lblSTATUS, "lblSTATUS");
            this.lblSTATUS.Name = "lblSTATUS";
            // 
            // btnAddPole
            // 
            this.btnAddPole.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnAddPole, "btnAddPole");
            this.btnAddPole.Name = "btnAddPole";
            this.btnAddPole.AddItem += new System.EventHandler(this.btnAddPole_AddItem);
            // 
            // lblBOX_TYPE
            // 
            resources.ApplyResources(this.lblBOX_TYPE, "lblBOX_TYPE");
            this.lblBOX_TYPE.Name = "lblBOX_TYPE";
            // 
            // cboBoxType
            // 
            this.cboBoxType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBoxType.FormattingEnabled = true;
            resources.ApplyResources(this.cboBoxType, "cboBoxType");
            this.cboBoxType.Name = "cboBoxType";
            // 
            // lblEND_USE_DATE
            // 
            resources.ApplyResources(this.lblEND_USE_DATE, "lblEND_USE_DATE");
            this.lblEND_USE_DATE.Name = "lblEND_USE_DATE";
            // 
            // dtpEndDate
            // 
            this.dtpEndDate.Checked = false;
            resources.ApplyResources(this.dtpEndDate, "dtpEndDate");
            this.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.ShowCheckBox = true;
            this.dtpEndDate.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            this.dtpEndDate.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dtpEndDate_MouseDown);
            // 
            // lblSTART_USE_DATE
            // 
            resources.ApplyResources(this.lblSTART_USE_DATE, "lblSTART_USE_DATE");
            this.lblSTART_USE_DATE.Name = "lblSTART_USE_DATE";
            // 
            // dtpStartDate
            // 
            this.dtpStartDate.Checked = false;
            resources.ApplyResources(this.dtpStartDate, "dtpStartDate");
            this.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.ShowCheckBox = true;
            this.dtpStartDate.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // btnBrowse_
            // 
            resources.ApplyResources(this.btnBrowse_, "btnBrowse_");
            this.btnBrowse_.Name = "btnBrowse_";
            this.btnBrowse_.UseVisualStyleBackColor = true;
            this.btnBrowse_.Click += new System.EventHandler(this.btnBrowse__Click);
            // 
            // lblPole_
            // 
            resources.ApplyResources(this.lblPole_, "lblPole_");
            this.lblPole_.Name = "lblPole_";
            // 
            // lblTOTAL_BOX_UPDATE
            // 
            resources.ApplyResources(this.lblTOTAL_BOX_UPDATE, "lblTOTAL_BOX_UPDATE");
            this.lblTOTAL_BOX_UPDATE.Name = "lblTOTAL_BOX_UPDATE";
            // 
            // DialogBoxBatchChange
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogBoxBatchChange";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label lblPOLE;
        private ComboBox cboPole;
        private ComboBox cboStatus;
        private Label lblSTATUS;
        private ExAddItem btnAddPole;
        private ComboBox cboBoxType;
        private Label lblBOX_TYPE;
        private Label lblEND_USE_DATE;
        private DateTimePicker dtpEndDate;
        private Label lblSTART_USE_DATE;
        private DateTimePicker dtpStartDate;
        private ExButton btnBrowse_;
        private Label lblPole_;
        private Label lblTOTAL_BOX_UPDATE;
    }
}