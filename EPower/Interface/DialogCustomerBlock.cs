﻿using EPower.Base.Logic;
using EPower.Base.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogCustomerBlock : ExDialog
    {
        #region Data
        CustomerOperation _flag;
        TBL_CUSTOMER _objCustomer = new TBL_CUSTOMER();
        TBL_CUSTOMER _objOldCustomer = new TBL_CUSTOMER();
        TBL_METER _objMeter = new TBL_METER();
        TBL_CUS_STATUS_CHANGE _objCusStatus = new TBL_CUS_STATUS_CHANGE();
        TBL_CHANGE_LOG _objChangeParent;
        TBL_INVOICE _objInvoice;
        TBL_INVOICE_DETAIL _objInvoiceDetail;
        TBL_PAYMENT _objPayment;
        TBL_PAYMENT_DETAIL _objPaymentDetail;
        TBL_INVOICE_ITEM _objInvoiceItem;
        int _intCurrencyId = 0;
        int _accountId = 0;
        TBL_PAYMENT_METHOD payMethod = new TBL_PAYMENT_METHOD();
        #endregion 

        #region Constructor
        public DialogCustomerBlock(CustomerOperation flag, TBL_CUSTOMER objCustomer)
        {
            InitializeComponent();
            chkPrint.Checked = Properties.Settings.Default.PRINT_RECIEPT;
            _flag = flag;
            bindCurrency();
            objCustomer._CopyTo(_objCustomer);
            objCustomer._CopyTo(_objOldCustomer);
            if (flag == CustomerOperation.ACTIVATE_CUSTOMER)
            {
                //Assign permission
                dtpBlockDate.Enabled = SoftTech.Security.Logic.Login.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_UNBLOCK_DATE);
                this.Text = string.Format(Properties.Resources.CUSTOMER_BLOCK, Properties.Resources.UNBLOCK);
                lblBLOCK_DATE.Text = string.Format(Properties.Resources.BLOCK_DATE, Properties.Resources.UNBLOCK);
            }
            else if (_flag == CustomerOperation.BLOCK_CUSTOMER)
            {
                //Assign permission
                dtpBlockDate.Enabled = SoftTech.Security.Logic.Login.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_BLOCK_DATE);
                this.Text = string.Format(Properties.Resources.CUSTOMER_BLOCK, Properties.Resources.BLOCK);
                lblBLOCK_DATE.Text = string.Format(Properties.Resources.BLOCK_DATE, Properties.Resources.BLOCK);
            }
            dtpBlockDate.Value = DBDataContext.Db.GetSystemDate();
            dtpInvoice.Value = dtpBlockDate.Value;
            // Payment method 
            UIHelper.SetDataSourceToComboBox(cboPaymentMethod, DBDataContext.Db.TBL_PAYMENT_METHODs.Where(x => x.IS_ACTIVE).Select(x => new { x.PAYMENT_METHOD_ID, x.PAYMENT_METHOD_NAME }));
            read();
        }
        #endregion

        #region Method

        private void bindCurrency()
        {
            UIHelper.SetDataSourceToComboBox(cboCurrency, DBDataContext.Db.TLKP_CURRENCies.Select(x => new { x.CURRENCY_ID, x.CURRENCY_NAME }));
        }

        void read()
        {
            _intCurrencyId = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(row => row.IS_DEFAULT_CURRENCY).CURRENCY_ID;
            panelService.Visible = false;
            if (_flag == CustomerOperation.ACTIVATE_CUSTOMER)
            {
                panelService.Visible = true;
                txtCashDrawerName.Text = DBDataContext.Db.TBL_CASH_DRAWERs.FirstOrDefault(x => x.CASH_DRAWER_ID == Login.CurrentCashDrawer.CASH_DRAWER_ID).CASH_DRAWER_NAME;
                txtUserCashDrawerName.Text = Login.CurrentLogin.LOGIN_NAME;

                //Todo:Add customer enum
                var invoiceItem = from invItem in DBDataContext.Db.TBL_INVOICE_ITEMs
                                  where invItem.INVOICE_ITEM_ID == (int)InvoiceItem.Reconnect
                                  select invItem;
                _objInvoiceItem = invoiceItem.FirstOrDefault();
                UIHelper.SetDataSourceToComboBox(cboInvoiceItem, invoiceItem, "INVOICE_ITEM_ID", "INVOICE_ITEM_NAME");
                cboInvoiceItem.SelectedIndex = 0;
                cboCurrency.SelectedValue = _objInvoiceItem.CURRENCY_ID;
                txtPrice.Text = UIHelper.FormatCurrency(_objInvoiceItem.PRICE);
                txtPayment.Text = UIHelper.FormatCurrency(_objInvoiceItem.PRICE);
            }

            var cusBlockInfo = (from c in DBDataContext.Db.TBL_CUSTOMERs
                                join i in DBDataContext.Db.TBL_INVOICEs on c.CUSTOMER_ID equals i.CUSTOMER_ID
                                join ct in DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs on c.CUSTOMER_CONNECTION_TYPE_ID equals ct.CUSTOMER_CONNECTION_TYPE_ID
                                join cg in DBDataContext.Db.TLKP_CUSTOMER_GROUPs on ct.NONLICENSE_CUSTOMER_GROUP_ID equals cg.CUSTOMER_GROUP_ID
                                where c.CUSTOMER_ID == _objCustomer.CUSTOMER_ID
                                orderby i.DUE_DATE descending
                                select new
                                {
                                    c.CUSTOMER_ID,
                                    FULL_NAME = string.Concat(c.LAST_NAME_KH, " ", c.FIRST_NAME_KH),
                                    cg.CUSTOMER_GROUP_NAME,
                                    ct.CUSTOMER_CONNECTION_TYPE_NAME,
                                    DUE_DATE = i.DUE_DATE.AddDays(c.CUT_OFF_DAY),
                                }).FirstOrDefault();

            if (cusBlockInfo != null)
            {
                var meterInfo = (from m in DBDataContext.Db.TBL_METERs
                                 join mt in DBDataContext.Db.TBL_METER_TYPEs
                                 on m.METER_TYPE_ID equals mt.METER_TYPE_ID
                                 join cm in DBDataContext.Db.TBL_CUSTOMER_METERs
                                 on m.METER_ID equals cm.METER_ID

                                 join con in DBDataContext.Db.TBL_CONSTANTs
                                 on mt.METER_CONST_ID equals con.CONSTANT_ID

                                 join ph in DBDataContext.Db.TBL_PHASEs
                                 on mt.METER_PHASE_ID equals ph.PHASE_ID

                                 join amp in DBDataContext.Db.TBL_AMPAREs
                                 on mt.METER_AMP_ID equals amp.AMPARE_ID

                                 join v in DBDataContext.Db.TBL_VOLTAGEs
                                 on mt.METER_VOL_ID equals v.VOLTAGE_ID

                                 where cm.CUSTOMER_ID == cusBlockInfo.CUSTOMER_ID
                                 select new
                                 {
                                     m,
                                     m.METER_CODE,
                                     mt.METER_TYPE_NAME,
                                     con.CONSTANT_NAME,
                                     ph.PHASE_NAME,
                                     amp.AMPARE_NAME,
                                     v.VOLTAGE_NAME
                                 }).FirstOrDefault();
                var area = (from b in DBDataContext.Db.TBL_BOXes
                            join cm in DBDataContext.Db.TBL_CUSTOMER_METERs
                            on b.BOX_ID equals cm.BOX_ID
                            join p in DBDataContext.Db.TBL_POLEs
                            on cm.POLE_ID equals p.POLE_ID
                            select new
                            {
                                b.BOX_CODE,
                                p.POLE_CODE
                            }).FirstOrDefault();
                _objMeter = meterInfo.m;
                txtFullName.Text = cusBlockInfo.FULL_NAME;
                txtCustomerGroup.Text = cusBlockInfo.CUSTOMER_GROUP_NAME;
                txtCustomerConnectionType.Text = cusBlockInfo.CUSTOMER_CONNECTION_TYPE_NAME;
                dtpDueDate.Value = cusBlockInfo.DUE_DATE;
                txtMeterCode.Text = meterInfo.METER_CODE;
                txtMeterType.Text = meterInfo.METER_TYPE_NAME;
                txtMeterAmp.Text = meterInfo.AMPARE_NAME;
                txtMeterPhase.Text = meterInfo.PHASE_NAME;
                txtMeterVol.Text = meterInfo.VOLTAGE_NAME;
                txtMeterConstant.Text = meterInfo.CONSTANT_NAME;

                txtPoleCode.Text = area.POLE_CODE;
                txtBoxCode.Text = area.BOX_CODE;

                //dtpBlockDate.MinDate = DBDataContext.Db.GetSystemDate();
            }
        }

        void write()
        {
            _objCusStatus = new TBL_CUS_STATUS_CHANGE();
            _objCusStatus.CHNAGE_DATE = dtpBlockDate.Value;
            _objCusStatus.CREATE_BY = SoftTech.Security.Logic.Login.CurrentLogin.LOGIN_NAME;
            _objCusStatus.CUSTOMER_ID = _objCustomer.CUSTOMER_ID;
            _objCusStatus.OLD_STATUS_ID = _objOldCustomer.STATUS_ID;
            _objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == _objOldCustomer.CUSTOMER_ID);
            if (_flag == CustomerOperation.BLOCK_CUSTOMER)
            {
                _objCustomer.STATUS_ID =
                _objCusStatus.NEW_STATUS_ID = (int)CustomerStatus.Blocked;
                _objCusStatus.INVOICE_ID = 0;
            }
            else if (_flag == CustomerOperation.ACTIVATE_CUSTOMER)
            {
                _objCustomer.STATUS_ID =
                _objCusStatus.NEW_STATUS_ID = (int)CustomerStatus.Active;
            }
        }

        public decimal getTotalPay()
        {
            return UIHelper.Round(DataHelper.ParseToDecimal(this.txtPayment.Text), (int)cboCurrency.SelectedValue);
        }

        private decimal getTotalBalance()
        {
            decimal decPrice = 0, decPayment = 0;

            decPrice = UIHelper.Round(DataHelper.ParseToDecimal(this.txtPrice.Text), (int)cboCurrency.SelectedValue);
            decPayment = UIHelper.Round(DataHelper.ParseToDecimal(this.txtPayment.Text), (int)cboCurrency.SelectedValue);

            return decPrice - decPayment;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (invalid())
                {
                    return;
                }
                bool blnPrint = false;
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    write();
                    _objChangeParent = DBDataContext.Db.Update(_objOldCustomer, _objCustomer);
                    if (_flag == CustomerOperation.ACTIVATE_CUSTOMER)
                    {
                        if (DataHelper.ParseToDecimal(txtPrice.Text) > 0)
                        {
                            saveInvoice();
                            blnPrint = chkPrint.Checked;
                        }

                    }
                    DBDataContext.Db.InsertChild(_objCusStatus, _objCustomer, ref _objChangeParent);
                    tran.Complete();

                }
                if (blnPrint && _objPayment != null)
                {
                    CrystalReportHelper cr = new CrystalReportHelper("ReportReceiptPayment.rpt");
                    cr.SetParameter("PAYMENT_ID", (int)_objPayment.PAYMENT_ID);
                    cr.PrintReport(Properties.Settings.Default.PRINTER_RECIEPT);
                    cr.Dispose();
                }
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp);
            }

            PostingLogic.Instance.PostToPointerBg(DateTime.Now.Date, DateTime.Now.Date.AddDays(1).AddSeconds(-1));
        }

        private bool invalid()
        {
            this.ClearAllValidation();
            bool blnVal = false;
            payMethod = DBDataContext.Db.TBL_PAYMENT_METHODs.FirstOrDefault(x => x.PAYMENT_METHOD_ID == (int)cboPaymentMethod.SelectedValue);
            if (_flag != CustomerOperation.ACTIVATE_CUSTOMER)
            {
                return blnVal;
            }

            if (cboCurrency.SelectedIndex == -1)
            {
                cboCurrency.SetValidation(string.Format(Resources.REQUIRED, lblCURRENCY.Text));
                blnVal = true;
            }

            if (getTotalBalance() < 0)
            {
                this.txtPayment.SetValidation(Resources.MS_PAYMENT_OVER_AMOUNT);
                blnVal = true;
            }
            //if (getTotalBalance() > 0)
            //{
            //    this.txtPayment.SetValidation(Resources.MS_PAID_FULL_AMOUNT);
            //    blnVal = true;
            //}

            if (this.cboPaymentMethod.SelectedIndex == -1 || payMethod == null)
            {
                this.cboPaymentMethod.SetValidation(string.Format(Resources.REQUIRED, lblPAYMENT_METHOD.Text));
                blnVal = true;
            }

            _accountId = _intCurrencyId == (int)Currency.KHR ? payMethod.ACCOUNT_ID_KHR : _intCurrencyId == (int)Currency.USD ? payMethod.ACCOUNT_ID_USD :
                            _intCurrencyId == (int)Currency.THB ? payMethod.ACCOUNT_ID_THB : _intCurrencyId == (int)Currency.VND ? payMethod.ACCOUNT_ID_VND : 0;
            if (_accountId <= 0)
            {
                if (MsgBox.ShowQuestion(String.Format(Resources.MSG_PAYMENT_METHOD_NOT_SET_ACCOUNT, cboCurrency.Text), Resources.INFORMATION) == DialogResult.Yes)
                {
                    new DialogPaymentMethod(payMethod, GeneralProcess.Update).ShowDialog();
                }
                blnVal = true;
            }
            return blnVal;

        }

        private void saveInvoice()
        {
            Sequence sequence = Method.GetSequenceId(SequenceType.InvoiceService);
            DateTime dtNow = DBDataContext.Db.GetSystemDate();
            var exchangeRate = new ExchangeRateLogic().findLastExchangeRate(dtNow, (int)cboCurrency.SelectedValue);
            decimal decAmount = 0;
            int intDueDay = DataHelper.ParseToInt(DBDataContext.Db.TBL_UTILITies.FirstOrDefault(x => x.UTILITY_ID == (int)Utility.INVOICE_DUE_DATE).UTILITY_VALUE);
            decAmount = DataHelper.ParseToDecimal(txtPrice.Text);

            _objInvoice = Method.NewInvoice(true, _objInvoiceItem.INVOICE_ITEM_NAME, _objCustomer.CUSTOMER_ID);
            //_objInvoice.INVOICE_NO = Method.GetNextSequence(sequence, true);
            _objInvoice.CUSTOMER_ID = _objCustomer.CUSTOMER_ID;
            _objInvoice.CURRENCY_ID = (int)cboCurrency.SelectedValue;
            _objInvoice.CYCLE_ID = _objCustomer.BILLING_CYCLE_ID;
            _objInvoice.PRINT_COUNT = 0;
            _objInvoice.RUN_ID = 0;
            _objInvoice.DISCOUNT_AMOUNT = 0;
            _objInvoice.DISCOUNT_AMOUNT_NAME = string.Empty;
            _objInvoice.DISCOUNT_USAGE = 0;
            _objInvoice.DISCOUNT_USAGE_NAME = string.Empty;
            _objInvoice.START_DATE = dtpInvoice.Value;
            _objInvoice.START_USAGE = 0;
            _objInvoice.END_DATE = dtpInvoice.Value;
            _objInvoice.END_USAGE = 0;
            _objInvoice.TOTAL_USAGE = 0;
            _objInvoice.INVOICE_DATE = dtpInvoice.Value;
            _objInvoice.DUE_DATE = dtpInvoice.Value.AddDays(intDueDay);
            _objInvoice.INVOICE_MONTH = new DateTime(dtpInvoice.Value.Year, dtpInvoice.Value.Month, 1); //Method.GetNextBillingMonth(_objCustomer.BILLING_CYCLE_ID); //new DateTime(dtNow.Year, dtNow.Month, 1).AddMonths(1).AddDays(-1);
            _objInvoice.INVOICE_TITLE = _objInvoiceItem.INVOICE_ITEM_NAME;
            _objInvoice.IS_SERVICE_BILL = true;
            _objInvoice.METER_CODE = _objMeter.METER_CODE;
            _objInvoice.TOTAL_AMOUNT = decAmount;
            _objInvoice.SETTLE_AMOUNT = DataHelper.ParseToDecimal(UIHelper.FormatCurrency(decAmount, _objInvoice.CURRENCY_ID));
            _objInvoice.PAID_AMOUNT = getTotalPay();
            _objInvoice.FORWARD_AMOUNT = _objInvoice.TOTAL_AMOUNT - _objInvoice.PAID_AMOUNT;
            _objInvoice.INVOICE_STATUS = (int)InvoiceStatus.Open;
            _objInvoice.START_PAY_DATE = _objInvoice.INVOICE_DATE;
            //_objInvoice.EXCHANGE_RATE = exchangeRate.EXCHANGE_RATE;
            //_objInvoice.EXCHANGE_RATE_DATE = exchangeRate.CREATE_ON;
            DBDataContext.Db.InsertChild(_objInvoice, _objCustomer, ref _objChangeParent);
            _objCusStatus.INVOICE_ID = _objInvoice.INVOICE_ID;

            _objInvoiceDetail = new TBL_INVOICE_DETAIL()
            {
                INVOICE_ID = _objInvoice.INVOICE_ID,
                INVOICE_ITEM_ID = _objInvoiceItem.INVOICE_ITEM_ID,
                PRICE = _objInvoiceItem.PRICE,
                START_USAGE = 0,
                END_USAGE = 0,
                USAGE = 0,
                AMOUNT = _objInvoice.TOTAL_AMOUNT,
                CHARGE_DESCRIPTION = _objInvoiceItem.INVOICE_ITEM_NAME,
                REF_NO = _objInvoice.INVOICE_NO,
                TRAN_DATE = dtNow,
                EXCHANGE_RATE = exchangeRate.EXCHANGE_RATE,
                EXCHANGE_RATE_DATE = exchangeRate.CREATE_ON
            };
            DBDataContext.Db.InsertChild(_objInvoiceDetail, _objCustomer, ref _objChangeParent);

            if (DataHelper.ParseToDecimal(txtPayment.Text) > 0)
            {
                _objPayment = new TBL_PAYMENT()
                {
                    PAYMENT_NO = Method.GetNextSequence(Sequence.Receipt, true),
                    CURRENCY_ID = _objInvoice.CURRENCY_ID,
                    CUSTOMER_ID = _objCustomer.CUSTOMER_ID,
                    CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                    CREATE_ON = dtNow,
                    IS_ACTIVE = true,
                    PAY_AMOUNT = getTotalPay(),
                    PAY_DATE = dtpInvoice.Value,
                    USER_CASH_DRAWER_ID = Login.CurrentCashDrawer.USER_CASH_DRAWER_ID,
                    DUE_AMOUNT = _objInvoice.SETTLE_AMOUNT,
                    PAYMENT_ACCOUNT_ID = _accountId,
                    //PAYMENT_TYPE_ID = 1,
                    EXCHANGE_RATE = exchangeRate.EXCHANGE_RATE,
                    EXCHANGE_RATE_DATE = exchangeRate.CREATE_ON,
                    PAYMENT_METHOD_ID = payMethod.PAYMENT_METHOD_ID
                };
                DBDataContext.Db.InsertChild(_objPayment, _objCustomer, ref _objChangeParent);

                _objPaymentDetail = new TBL_PAYMENT_DETAIL();
                _objPaymentDetail.INVOICE_ID = _objInvoice.INVOICE_ID;
                _objPaymentDetail.PAY_AMOUNT = getTotalPay();
                _objPaymentDetail.PAYMENT_ID = _objPayment.PAYMENT_ID;
                DBDataContext.Db.InsertChild(_objPaymentDetail, _objCustomer, ref _objChangeParent);
            }

            //Close invoice
            if (getTotalPay() == _objInvoice.SETTLE_AMOUNT)
            {
                TBL_INVOICE objOld = new TBL_INVOICE();
                _objInvoice._CopyTo(objOld);
                _objInvoice.INVOICE_STATUS = (int)InvoiceStatus.Close;
                DBDataContext.Db.Update(objOld, _objInvoice);
            }

            //if(_objInvoice.INVOICE_STATUS != (int)InvoiceStatus.Close)
            //{
            //    var balance = PrepaymentLogic.Instance.GetCustomerPrepayment(_objCustomer.CUSTOMER_ID, _objInvoice.CURRENCY_ID);
            //    if (balance <= 0)
            //    {
            //        return;
            //    }
            //    var invoices = new List<TBL_INVOICE>() { _objInvoice };
            //    PrepaymentLogic.Instance.ClearPaymentWithPrepay(invoices);
            //} 
        }
        #endregion Method

        #region Event
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void EnterEnglishKey(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }



        private void txtPayment_TextChanged(object sender, EventArgs e)
        {
            decimal decPrice = 0,
               decPayment = 0,
               decDueAmount = 0;
            if (string.IsNullOrEmpty(txtPrice.Text))
            {
                txtPrice.Text = "0";
                txtPrice.SelectAll();
            }
            if (string.IsNullOrEmpty(txtPayment.Text))
            {
                txtPayment.Text = "0";
                txtPayment.SelectAll();
            }
            decPrice = DataHelper.ParseToDecimal(txtPrice.Text);
            decPayment = DataHelper.ParseToDecimal(UIHelper.FormatCurrency(DataHelper.ParseToDecimal(txtPayment.Text)));
            decDueAmount = decPrice - decPayment;
            txtDueAmount.Text = UIHelper.FormatCurrency(decDueAmount);

            if (((TextBox)sender).Name.Contains(txtPayment.Name))
            {
                lblReadAmount_.Text = decPayment <= 0 ? string.Empty : DataHelper.NumberToWord(DataHelper.ParseToDecimal(txtPayment.Text));
            }
            else if (((TextBox)sender).Name.Contains(txtPrice.Name))
            {
                lblReadAmount_.Text = decPrice <= 0 ? string.Empty : DataHelper.NumberToWord(DataHelper.ParseToDecimal(txtPrice.Text));
            }
            else
            {
                lblReadAmount_.Text = decDueAmount <= 0 ? string.Empty : DataHelper.NumberToWord(DataHelper.ParseToDecimal(txtDueAmount.Text));
            }


        }

        private void chkPrint_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.PRINT_RECIEPT = chkPrint.Checked;
            Properties.Settings.Default.Save();
        }

        private void DialogCustomerBlock_Load(object sender, EventArgs e)
        {
            if (_flag == CustomerOperation.ACTIVATE_CUSTOMER)
            {
                this.Text = string.Format(Resources.CUSTOMER_BLOCK, Resources.UNBLOCK);
                lblBLOCK_DATE.Text = string.Format(Resources.BLOCK_DATE, Resources.UNBLOCK);
            }
            else if (_flag == CustomerOperation.BLOCK_CUSTOMER)
            {
                this.Text = string.Format(Resources.CUSTOMER_BLOCK, Resources.BLOCK);
                lblBLOCK_DATE.Text = string.Format(Resources.BLOCK_DATE, Resources.BLOCK);
            }
        }
        private void dtpBlockDate_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        #endregion Event

        private void txtPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        }


    }
}