﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogCustomerListPenalty
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCustomerListPenalty));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.cboCurrencyId = new System.Windows.Forms.ComboBox();
            this.cboBillingCycle = new System.Windows.Forms.ComboBox();
            this.cboArea = new System.Windows.Forms.ComboBox();
            this.cboCustomerType = new System.Windows.Forms.ComboBox();
            this.lblCUSTOMER_TYPE = new System.Windows.Forms.Label();
            this.lblAREA = new System.Windows.Forms.Label();
            this.lblCYCLE_NAME = new System.Windows.Forms.Label();
            this.lblCURRENCY = new System.Windows.Forms.Label();
            this.lblSEARCH = new System.Windows.Forms.Label();
            this.txtSearchCus = new SoftTech.Component.ExTextbox();
            this.cboStatus = new System.Windows.Forms.ComboBox();
            this.lblSTATUS = new System.Windows.Forms.Label();
            this.chkCheckAll_ = new System.Windows.Forms.CheckBox();
            this.lblTOTAL_CUSTOMER = new System.Windows.Forms.Label();
            this.lblTotalSelectCustomer_ = new System.Windows.Forms.Label();
            this.btnPENALTY = new SoftTech.Component.ExButton();
            this.CUSTOMER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_CHECK_ = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.CUSTOMER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AREA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DUE_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUT_DAY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_DUE_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_SING_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.btnPENALTY);
            this.content.Controls.Add(this.chkCheckAll_);
            this.content.Controls.Add(this.txtSearchCus);
            this.content.Controls.Add(this.lblTotalSelectCustomer_);
            this.content.Controls.Add(this.lblTOTAL_CUSTOMER);
            this.content.Controls.Add(this.lblCURRENCY);
            this.content.Controls.Add(this.lblSEARCH);
            this.content.Controls.Add(this.lblCYCLE_NAME);
            this.content.Controls.Add(this.lblSTATUS);
            this.content.Controls.Add(this.lblAREA);
            this.content.Controls.Add(this.lblCUSTOMER_TYPE);
            this.content.Controls.Add(this.cboCurrencyId);
            this.content.Controls.Add(this.cboBillingCycle);
            this.content.Controls.Add(this.cboStatus);
            this.content.Controls.Add(this.cboArea);
            this.content.Controls.Add(this.cboCustomerType);
            this.content.Controls.Add(this.dgv);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.dgv, 0);
            this.content.Controls.SetChildIndex(this.cboCustomerType, 0);
            this.content.Controls.SetChildIndex(this.cboArea, 0);
            this.content.Controls.SetChildIndex(this.cboStatus, 0);
            this.content.Controls.SetChildIndex(this.cboBillingCycle, 0);
            this.content.Controls.SetChildIndex(this.cboCurrencyId, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_TYPE, 0);
            this.content.Controls.SetChildIndex(this.lblAREA, 0);
            this.content.Controls.SetChildIndex(this.lblSTATUS, 0);
            this.content.Controls.SetChildIndex(this.lblCYCLE_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblSEARCH, 0);
            this.content.Controls.SetChildIndex(this.lblCURRENCY, 0);
            this.content.Controls.SetChildIndex(this.lblTOTAL_CUSTOMER, 0);
            this.content.Controls.SetChildIndex(this.lblTotalSelectCustomer_, 0);
            this.content.Controls.SetChildIndex(this.txtSearchCus, 0);
            this.content.Controls.SetChildIndex(this.chkCheckAll_, 0);
            this.content.Controls.SetChildIndex(this.btnPENALTY, 0);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CUSTOMER_ID,
            this.IS_CHECK_,
            this.CUSTOMER_CODE,
            this.CUSTOMER_NAME,
            this.CUSTOMER_TYPE,
            this.AREA,
            this.DUE_DATE,
            this.CUT_DAY,
            this.TOTAL_DUE_AMOUNT,
            this.CURRENCY_SING_});
            this.dgv.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellContentClick);
            // 
            // cboCurrencyId
            // 
            this.cboCurrencyId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrencyId.DropDownWidth = 200;
            this.cboCurrencyId.FormattingEnabled = true;
            this.cboCurrencyId.Items.AddRange(new object[] {
            resources.GetString("cboCurrencyId.Items"),
            resources.GetString("cboCurrencyId.Items1"),
            resources.GetString("cboCurrencyId.Items2"),
            resources.GetString("cboCurrencyId.Items3"),
            resources.GetString("cboCurrencyId.Items4")});
            resources.ApplyResources(this.cboCurrencyId, "cboCurrencyId");
            this.cboCurrencyId.Name = "cboCurrencyId";
            this.cboCurrencyId.Tag = "0";
            this.cboCurrencyId.SelectedIndexChanged += new System.EventHandler(this.cboCustomerType_SelectedIndexChanged);
            // 
            // cboBillingCycle
            // 
            this.cboBillingCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBillingCycle.DropDownWidth = 200;
            this.cboBillingCycle.FormattingEnabled = true;
            this.cboBillingCycle.Items.AddRange(new object[] {
            resources.GetString("cboBillingCycle.Items"),
            resources.GetString("cboBillingCycle.Items1"),
            resources.GetString("cboBillingCycle.Items2"),
            resources.GetString("cboBillingCycle.Items3"),
            resources.GetString("cboBillingCycle.Items4")});
            resources.ApplyResources(this.cboBillingCycle, "cboBillingCycle");
            this.cboBillingCycle.Name = "cboBillingCycle";
            this.cboBillingCycle.Tag = "0";
            this.cboBillingCycle.SelectedIndexChanged += new System.EventHandler(this.cboCustomerType_SelectedIndexChanged);
            // 
            // cboArea
            // 
            this.cboArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboArea.DropDownWidth = 200;
            this.cboArea.FormattingEnabled = true;
            this.cboArea.Items.AddRange(new object[] {
            resources.GetString("cboArea.Items"),
            resources.GetString("cboArea.Items1"),
            resources.GetString("cboArea.Items2"),
            resources.GetString("cboArea.Items3"),
            resources.GetString("cboArea.Items4")});
            resources.ApplyResources(this.cboArea, "cboArea");
            this.cboArea.Name = "cboArea";
            this.cboArea.Tag = "0";
            this.cboArea.SelectedIndexChanged += new System.EventHandler(this.cboCustomerType_SelectedIndexChanged);
            // 
            // cboCustomerType
            // 
            this.cboCustomerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomerType.DropDownWidth = 200;
            this.cboCustomerType.FormattingEnabled = true;
            this.cboCustomerType.Items.AddRange(new object[] {
            resources.GetString("cboCustomerType.Items"),
            resources.GetString("cboCustomerType.Items1"),
            resources.GetString("cboCustomerType.Items2"),
            resources.GetString("cboCustomerType.Items3"),
            resources.GetString("cboCustomerType.Items4")});
            resources.ApplyResources(this.cboCustomerType, "cboCustomerType");
            this.cboCustomerType.Name = "cboCustomerType";
            this.cboCustomerType.SelectedIndexChanged += new System.EventHandler(this.cboCustomerType_SelectedIndexChanged);
            // 
            // lblCUSTOMER_TYPE
            // 
            resources.ApplyResources(this.lblCUSTOMER_TYPE, "lblCUSTOMER_TYPE");
            this.lblCUSTOMER_TYPE.Name = "lblCUSTOMER_TYPE";
            // 
            // lblAREA
            // 
            resources.ApplyResources(this.lblAREA, "lblAREA");
            this.lblAREA.Name = "lblAREA";
            // 
            // lblCYCLE_NAME
            // 
            resources.ApplyResources(this.lblCYCLE_NAME, "lblCYCLE_NAME");
            this.lblCYCLE_NAME.Name = "lblCYCLE_NAME";
            // 
            // lblCURRENCY
            // 
            resources.ApplyResources(this.lblCURRENCY, "lblCURRENCY");
            this.lblCURRENCY.Name = "lblCURRENCY";
            // 
            // lblSEARCH
            // 
            resources.ApplyResources(this.lblSEARCH, "lblSEARCH");
            this.lblSEARCH.Name = "lblSEARCH";
            // 
            // txtSearchCus
            // 
            this.txtSearchCus.BackColor = System.Drawing.Color.White;
            this.txtSearchCus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtSearchCus, "txtSearchCus");
            this.txtSearchCus.Name = "txtSearchCus";
            this.txtSearchCus.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtSearchCus.QuickSearch += new System.EventHandler(this.cboCustomerType_SelectedIndexChanged);
            // 
            // cboStatus
            // 
            this.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboStatus.DropDownWidth = 200;
            this.cboStatus.FormattingEnabled = true;
            this.cboStatus.Items.AddRange(new object[] {
            resources.GetString("cboStatus.Items"),
            resources.GetString("cboStatus.Items1")});
            resources.ApplyResources(this.cboStatus, "cboStatus");
            this.cboStatus.Name = "cboStatus";
            this.cboStatus.Tag = "0";
            this.cboStatus.SelectedIndexChanged += new System.EventHandler(this.cboCustomerType_SelectedIndexChanged);
            // 
            // lblSTATUS
            // 
            resources.ApplyResources(this.lblSTATUS, "lblSTATUS");
            this.lblSTATUS.Name = "lblSTATUS";
            // 
            // chkCheckAll_
            // 
            resources.ApplyResources(this.chkCheckAll_, "chkCheckAll_");
            this.chkCheckAll_.Name = "chkCheckAll_";
            this.chkCheckAll_.UseVisualStyleBackColor = true;
            this.chkCheckAll_.CheckedChanged += new System.EventHandler(this.chkCheckAll_CheckedChanged);
            // 
            // lblTOTAL_CUSTOMER
            // 
            resources.ApplyResources(this.lblTOTAL_CUSTOMER, "lblTOTAL_CUSTOMER");
            this.lblTOTAL_CUSTOMER.Name = "lblTOTAL_CUSTOMER";
            // 
            // lblTotalSelectCustomer_
            // 
            resources.ApplyResources(this.lblTotalSelectCustomer_, "lblTotalSelectCustomer_");
            this.lblTotalSelectCustomer_.Name = "lblTotalSelectCustomer_";
            // 
            // btnPENALTY
            // 
            resources.ApplyResources(this.btnPENALTY, "btnPENALTY");
            this.btnPENALTY.Name = "btnPENALTY";
            this.btnPENALTY.UseVisualStyleBackColor = true;
            this.btnPENALTY.Click += new System.EventHandler(this.btnPenalty_Click);
            // 
            // CUSTOMER_ID
            // 
            this.CUSTOMER_ID.DataPropertyName = "CUSTOMER_ID";
            resources.ApplyResources(this.CUSTOMER_ID, "CUSTOMER_ID");
            this.CUSTOMER_ID.Name = "CUSTOMER_ID";
            this.CUSTOMER_ID.ReadOnly = true;
            // 
            // IS_CHECK_
            // 
            this.IS_CHECK_.DataPropertyName = "IS_CHECK";
            resources.ApplyResources(this.IS_CHECK_, "IS_CHECK_");
            this.IS_CHECK_.Name = "IS_CHECK_";
            this.IS_CHECK_.ReadOnly = true;
            this.IS_CHECK_.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.IS_CHECK_.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // CUSTOMER_CODE
            // 
            this.CUSTOMER_CODE.DataPropertyName = "CUSTOMER_CODE";
            resources.ApplyResources(this.CUSTOMER_CODE, "CUSTOMER_CODE");
            this.CUSTOMER_CODE.Name = "CUSTOMER_CODE";
            this.CUSTOMER_CODE.ReadOnly = true;
            // 
            // CUSTOMER_NAME
            // 
            this.CUSTOMER_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CUSTOMER_NAME.DataPropertyName = "CUSTOMER_NAME";
            resources.ApplyResources(this.CUSTOMER_NAME, "CUSTOMER_NAME");
            this.CUSTOMER_NAME.Name = "CUSTOMER_NAME";
            this.CUSTOMER_NAME.ReadOnly = true;
            // 
            // CUSTOMER_TYPE
            // 
            this.CUSTOMER_TYPE.DataPropertyName = "CUSTOMER_TYPE_NAME";
            resources.ApplyResources(this.CUSTOMER_TYPE, "CUSTOMER_TYPE");
            this.CUSTOMER_TYPE.Name = "CUSTOMER_TYPE";
            this.CUSTOMER_TYPE.ReadOnly = true;
            // 
            // AREA
            // 
            this.AREA.DataPropertyName = "AREA_NAME";
            resources.ApplyResources(this.AREA, "AREA");
            this.AREA.Name = "AREA";
            this.AREA.ReadOnly = true;
            // 
            // DUE_DATE
            // 
            this.DUE_DATE.DataPropertyName = "DUE_DATE";
            dataGridViewCellStyle2.Format = "dd-MM-yyyy";
            this.DUE_DATE.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.DUE_DATE, "DUE_DATE");
            this.DUE_DATE.Name = "DUE_DATE";
            this.DUE_DATE.ReadOnly = true;
            // 
            // CUT_DAY
            // 
            this.CUT_DAY.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.CUT_DAY.DataPropertyName = "Day";
            resources.ApplyResources(this.CUT_DAY, "CUT_DAY");
            this.CUT_DAY.Name = "CUT_DAY";
            this.CUT_DAY.ReadOnly = true;
            // 
            // TOTAL_DUE_AMOUNT
            // 
            this.TOTAL_DUE_AMOUNT.DataPropertyName = "BALANCE_DUE";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "#,##0.####";
            this.TOTAL_DUE_AMOUNT.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.TOTAL_DUE_AMOUNT, "TOTAL_DUE_AMOUNT");
            this.TOTAL_DUE_AMOUNT.Name = "TOTAL_DUE_AMOUNT";
            this.TOTAL_DUE_AMOUNT.ReadOnly = true;
            // 
            // CURRENCY_SING_
            // 
            this.CURRENCY_SING_.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.CURRENCY_SING_.DataPropertyName = "CURRENCY_SING";
            resources.ApplyResources(this.CURRENCY_SING_, "CURRENCY_SING_");
            this.CURRENCY_SING_.Name = "CURRENCY_SING_";
            this.CURRENCY_SING_.ReadOnly = true;
            // 
            // DialogCustomerListPenalty
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogCustomerListPenalty";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataGridView dgv;
        public ComboBox cboCurrencyId;
        public ComboBox cboBillingCycle;
        public ComboBox cboArea;
        public ComboBox cboCustomerType;
        private Label lblCURRENCY;
        private Label lblCYCLE_NAME;
        private Label lblAREA;
        private Label lblCUSTOMER_TYPE;
        private Label lblSEARCH;
        private ExTextbox txtSearchCus;
        private Label lblSTATUS;
        public ComboBox cboStatus;
        private CheckBox chkCheckAll_;
        private Label lblTotalSelectCustomer_;
        private Label lblTOTAL_CUSTOMER;
        private ExButton btnPENALTY;
        private DataGridViewTextBoxColumn CUSTOMER_ID;
        private DataGridViewCheckBoxColumn IS_CHECK_;
        private DataGridViewTextBoxColumn CUSTOMER_CODE;
        private DataGridViewTextBoxColumn CUSTOMER_NAME;
        private DataGridViewTextBoxColumn CUSTOMER_TYPE;
        private DataGridViewTextBoxColumn AREA;
        private DataGridViewTextBoxColumn DUE_DATE;
        private DataGridViewTextBoxColumn CUT_DAY;
        private DataGridViewTextBoxColumn TOTAL_DUE_AMOUNT;
        private DataGridViewTextBoxColumn CURRENCY_SING_;
    }
}