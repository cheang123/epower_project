﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PageCashDrawer : Form
    {
        #region Data
        int intCurrencyID = 0;
        public int CurrencyID
        {
            get { return intCurrencyID; }
            private set { intCurrencyID = value; }
        }
        
        public TBL_CASH_DRAWER CashDrawer
        {
            get
            {
                TBL_CASH_DRAWER objCashDrawer = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    try
                    {
                        int intCashDrawerID = (int)dgv.SelectedRows[0].Cells["CASH_DRAWER_ID"].Value;
                        objCashDrawer = DBDataContext.Db.TBL_CASH_DRAWERs.FirstOrDefault(x => x.CASH_DRAWER_ID == intCashDrawerID && x.IS_ACTIVE);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objCashDrawer;
            }
        }
        #endregion

        #region Constructor
        public PageCashDrawer()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            DEFAULT_FLOATING_CASH.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            txt_QuickSearch(null, null);            
        }
        #endregion

        #region Operation
        /// <summary>
        /// Load data from database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_QuickSearch(object sender, EventArgs e)
        {                        
            if (cboCurrency.SelectedIndex !=-1)
            {
                intCurrencyID = (int)cboCurrency.SelectedValue;
            }

            dgv.DataSource = from cd in DBDataContext.Db.TBL_CASH_DRAWERs
                           join c in DBDataContext.Db.TLKP_CURRENCies
                           on cd.CURRENCY_ID equals c.CURRENCY_ID
                           where (cd.IS_ACTIVE && (intCurrencyID == 0 || c.CURRENCY_ID == intCurrencyID)) &&
                           (cd.CASH_DRAWER_NAME.ToLower().Contains(txtQuickSearch.Text.ToLower().Trim()))
                           select new
                           {
                               cd.CASH_DRAWER_ID,
                               cd.CASH_DRAWER_NAME,
                               cd.DEFAULT_FLOATING_CASH,
                               c.CURRENCY_NAME                         
                           };
        }

        /// <summary>
        /// Add new pole.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNew_Click(object sender, EventArgs e)
        {
            TBL_CASH_DRAWER objCashDrawer = new TBL_CASH_DRAWER() { CASH_DRAWER_NAME = ""};
            if (cboCurrency.SelectedIndex!=-1)
            {
                objCashDrawer.CURRENCY_ID = (int)cboCurrency.SelectedValue;
            }            
            DialogCashDrawer dig = new DialogCashDrawer(GeneralProcess.Insert, objCashDrawer);
            if (dig.ShowDialog() == DialogResult.OK)
            {                
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.CashDrawer.CASH_DRAWER_ID);
            }
        }

        /// <summary>
        /// Edit pole.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                DialogCashDrawer dig = new DialogCashDrawer(GeneralProcess.Update, CashDrawer);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.CashDrawer.CASH_DRAWER_ID);
                }
            }
        }

        /// <summary>
        /// Remove pole.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (IsDeletable())
            {
                DialogCashDrawer dig = new DialogCashDrawer(GeneralProcess.Delete, CashDrawer);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.CashDrawer.CASH_DRAWER_ID - 1);
                }
            }

        }

        /// <summary>
        /// Change current keyboard layout to khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }
        #endregion

        #region Method
        public bool IsDeletable()
        { 
            bool val = false;
            if (dgv.SelectedRows.Count > 0)
            {
                val = true;
                if (DBDataContext.Db.TBL_USER_CASH_DRAWERs.Count(uc => uc.IS_ACTIVE && uc.CASH_DRAWER_ID == (int)this.dgv.SelectedRows[0].Cells["CASH_DRAWER_ID"].Value) > 0)
                {
                    MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE, Resources.INFORMATION);
                    val = false;
                }
            }
            return val;
        }

        public void loadCurrency()
        {
            int tempID = cboCurrency.SelectedValue != null ?
                         (int)cboCurrency.SelectedValue : 0;
            DataTable dt = (from a in DBDataContext.Db.TLKP_CURRENCies
                            select a)._ToDataTable();
            //Create Row Templet
            DataRow dr = dt.NewRow();
            dr["CURRENCY_ID"] = 0;
            dr["CURRENCY_NAME"] = Resources.ALL_CURRENCY;
            dt.Rows.InsertAt(dr, 0);
            UIHelper.SetDataSourceToComboBox(cboCurrency, dt, "CURRENCY_ID", "CURRENCY_NAME");
            cboCurrency.SelectedValue = tempID;
        }
        #endregion

        

       
    }
}
