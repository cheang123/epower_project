﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogIRRegistration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogIRRegistration));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnOK = new SoftTech.Component.ExButton();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.btnBrowse_ = new System.Windows.Forms.Button();
            this.lblFILE_NAME = new System.Windows.Forms.Label();
            this.txtFileName = new System.Windows.Forms.TextBox();
            this.DEVICE_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DEVICE_OEM_INFO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.STATUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.btnBrowse_);
            this.content.Controls.Add(this.lblFILE_NAME);
            this.content.Controls.Add(this.txtFileName);
            this.content.Controls.Add(this.dgv);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnOK);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.dgv, 0);
            this.content.Controls.SetChildIndex(this.txtFileName, 0);
            this.content.Controls.SetChildIndex(this.lblFILE_NAME, 0);
            this.content.Controls.SetChildIndex(this.btnBrowse_, 0);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // dgv
            // 
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DEVICE_CODE,
            this.DEVICE_OEM_INFO,
            this.STATUS});
            this.dgv.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // btnBrowse_
            // 
            resources.ApplyResources(this.btnBrowse_, "btnBrowse_");
            this.btnBrowse_.Name = "btnBrowse_";
            this.btnBrowse_.UseVisualStyleBackColor = true;
            this.btnBrowse_.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // lblFILE_NAME
            // 
            resources.ApplyResources(this.lblFILE_NAME, "lblFILE_NAME");
            this.lblFILE_NAME.Name = "lblFILE_NAME";
            // 
            // txtFileName
            // 
            this.txtFileName.BackColor = System.Drawing.Color.LightYellow;
            resources.ApplyResources(this.txtFileName, "txtFileName");
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.ReadOnly = true;
            // 
            // DEVICE_CODE
            // 
            this.DEVICE_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DEVICE_CODE.DataPropertyName = "DEVICE_CODE";
            resources.ApplyResources(this.DEVICE_CODE, "DEVICE_CODE");
            this.DEVICE_CODE.Name = "DEVICE_CODE";
            // 
            // DEVICE_OEM_INFO
            // 
            this.DEVICE_OEM_INFO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DEVICE_OEM_INFO.DataPropertyName = "DEVICE_OEM_INFO";
            resources.ApplyResources(this.DEVICE_OEM_INFO, "DEVICE_OEM_INFO");
            this.DEVICE_OEM_INFO.Name = "DEVICE_OEM_INFO";
            // 
            // STATUS
            // 
            this.STATUS.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.STATUS.DataPropertyName = "DEVICE_TYPE_NAME";
            resources.ApplyResources(this.STATUS, "STATUS");
            this.STATUS.Name = "STATUS";
            // 
            // DialogIRRegistration
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogIRRegistration";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnOK;
        private DataGridView dgv;
        private Button btnBrowse_;
        private Label lblFILE_NAME;
        private TextBox txtFileName;
        private DataGridViewTextBoxColumn DEVICE_CODE;
        private DataGridViewTextBoxColumn DEVICE_OEM_INFO;
        private DataGridViewTextBoxColumn STATUS;
    }
}