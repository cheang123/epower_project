﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogAdjustmentAmount : ExDialog
    {
        TBL_INVOICE _objInvoice = new TBL_INVOICE();
        TBL_INVOICE _objOldInvoice = new TBL_INVOICE();
        //TBL_INVOICE_DETAIL _objInvoiceDetail = new TBL_INVOICE_DETAIL();
        //private int _intPriceId;
        decimal oldusage = 0;
        public DialogAdjustmentAmount(TBL_INVOICE objInvoice, int intPriceId)
        {
            InitializeComponent();
            objInvoice._CopyTo(_objInvoice);
            objInvoice._CopyTo(_objOldInvoice);
            //_intPriceId = intPriceId;
            read();
        }

        private void read()
        {
            dtpStartUsage.Value = _objInvoice.START_DATE;
            dtpEndUsage.Value = _objInvoice.END_DATE;
            txtTotalAmount.Text = UIHelper.FormatCurrency(_objInvoice.TOTAL_AMOUNT, _objInvoice.CURRENCY_ID);
        }

        private bool invalid()
        {
            bool val = false;
            this.ClearAllValidation();
            if (!DataHelper.IsNumber(txtPayAmount.Text))
            {
                txtPayAmount.SetValidation(string.Format(Resources.REQUIRED, lblADJUST_AMOUNT.Text));
                val = true;
            }
            if (txtDescription.Text.Trim() == string.Empty)
            {
                txtDescription.SetValidation(string.Format(Resources.REQUIRED, lblNOTE.Text));
                val = true;
            }
            if (DataHelper.ParseToDecimal(txtPayAmount.Text) < 0)
            {
                txtPayAmount.SetValidation(string.Format(Resources.REQUEST_GRATER_THAN_ZERO));
                val = true;
            }
            txtPayAmount.ClearAllValidation();
            if (DataHelper.ParseToDecimal(txtTotalAmount.Text) == DataHelper.ParseToDecimal(txtPayAmount.Text))
            {
                txtPayAmount.SetValidation(string.Format(Resources.MSG_TOTAL_AMOUNT_ADJ));
                val = true;
            }
            return val;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (invalid())
            {
                return;
            }
            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {

                    decimal adjustAmount = DataHelper.ParseToDecimal(txtPayAmount.Text) - _objInvoice.TOTAL_AMOUNT;

                    //Adjust invoice amount
                    Method.AdjustInvoiceAmount(adjustAmount, 0, 0, _objInvoice, txtDescription.Text, _objInvoice.CURRENCY_ID, _objInvoice.CURRENCY_ID, oldusage);

                    tran.Complete();
                }
                this.DialogResult = DialogResult.OK;

            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp);
            }
        }

        private void txtNumber(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Enter_EnglishKey(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void Enter_KhmerKey(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void btnCHANGE_LOG_Click(object sender, EventArgs e)
        {
            new DialogAdjustmentHistory(_objInvoice).ShowDialog();
        }
    }

}







