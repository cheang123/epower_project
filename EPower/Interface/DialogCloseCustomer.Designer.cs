﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogCloseCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCloseCustomer));
            this.txtRefundDeposit = new System.Windows.Forms.TextBox();
            this.lblREMAIN_DEPOSIT = new System.Windows.Forms.Label();
            this.txtTotalDue = new System.Windows.Forms.TextBox();
            this.txtDeposit = new System.Windows.Forms.TextBox();
            this.lblDEPOSIT = new System.Windows.Forms.Label();
            this.lblCUSTOMER_NAME = new System.Windows.Forms.Label();
            this.lblCUSTOMER_CODE = new System.Windows.Forms.Label();
            this.lblAREA = new System.Windows.Forms.Label();
            this.txtAreaName = new System.Windows.Forms.TextBox();
            this.lblMETER_CODE = new System.Windows.Forms.Label();
            this.lblCUSTOMER_INFORMATION = new System.Windows.Forms.Label();
            this.lblBILL_INFORMATION = new System.Windows.Forms.Label();
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.lblCLOSE_DATE = new System.Windows.Forms.Label();
            this.lblTOTAL_DUE_AMOUNT = new System.Windows.Forms.Label();
            this.txtPayDate = new System.Windows.Forms.DateTimePicker();
            this.txtPole = new System.Windows.Forms.TextBox();
            this.lblPOLE = new System.Windows.Forms.Label();
            this.txtBox = new System.Windows.Forms.TextBox();
            this.lblBOX = new System.Windows.Forms.Label();
            this.lblCASH_DRAWER = new System.Windows.Forms.Label();
            this.txtCashDrawer = new System.Windows.Forms.TextBox();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.INVOICE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICE_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICE_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICE_TITLE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SETTLE_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAID_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DUE_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_SING_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtCustomerCode = new System.Windows.Forms.TextBox();
            this.txtCustomerName = new System.Windows.Forms.TextBox();
            this.txtMeterCode = new System.Windows.Forms.TextBox();
            this.lblMETER_STATUS = new System.Windows.Forms.Label();
            this.cboStatus = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.lblCURRENCY = new System.Windows.Forms.Label();
            this.txtSignTotalDue = new System.Windows.Forms.TextBox();
            this.txtSignDeposit = new System.Windows.Forms.TextBox();
            this.txtSignRefundDeposit = new System.Windows.Forms.TextBox();
            this.lblBREAKER_STATUS = new System.Windows.Forms.Label();
            this.cboBreakerStatus = new System.Windows.Forms.ComboBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.cboBreakerStatus);
            this.content.Controls.Add(this.lblBREAKER_STATUS);
            this.content.Controls.Add(this.txtSignRefundDeposit);
            this.content.Controls.Add(this.txtSignDeposit);
            this.content.Controls.Add(this.txtSignTotalDue);
            this.content.Controls.Add(this.label12);
            this.content.Controls.Add(this.cboCurrency);
            this.content.Controls.Add(this.lblCURRENCY);
            this.content.Controls.Add(this.cboStatus);
            this.content.Controls.Add(this.lblMETER_STATUS);
            this.content.Controls.Add(this.txtMeterCode);
            this.content.Controls.Add(this.txtCustomerName);
            this.content.Controls.Add(this.txtCustomerCode);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.dgv);
            this.content.Controls.Add(this.txtCashDrawer);
            this.content.Controls.Add(this.lblCASH_DRAWER);
            this.content.Controls.Add(this.txtBox);
            this.content.Controls.Add(this.lblBOX);
            this.content.Controls.Add(this.txtPole);
            this.content.Controls.Add(this.lblPOLE);
            this.content.Controls.Add(this.txtPayDate);
            this.content.Controls.Add(this.lblCLOSE_DATE);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.lblBILL_INFORMATION);
            this.content.Controls.Add(this.lblCUSTOMER_INFORMATION);
            this.content.Controls.Add(this.lblMETER_CODE);
            this.content.Controls.Add(this.txtAreaName);
            this.content.Controls.Add(this.lblAREA);
            this.content.Controls.Add(this.txtRefundDeposit);
            this.content.Controls.Add(this.lblREMAIN_DEPOSIT);
            this.content.Controls.Add(this.txtTotalDue);
            this.content.Controls.Add(this.txtDeposit);
            this.content.Controls.Add(this.lblDEPOSIT);
            this.content.Controls.Add(this.lblTOTAL_DUE_AMOUNT);
            this.content.Controls.Add(this.lblCUSTOMER_NAME);
            this.content.Controls.Add(this.lblCUSTOMER_CODE);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_CODE, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblTOTAL_DUE_AMOUNT, 0);
            this.content.Controls.SetChildIndex(this.lblDEPOSIT, 0);
            this.content.Controls.SetChildIndex(this.txtDeposit, 0);
            this.content.Controls.SetChildIndex(this.txtTotalDue, 0);
            this.content.Controls.SetChildIndex(this.lblREMAIN_DEPOSIT, 0);
            this.content.Controls.SetChildIndex(this.txtRefundDeposit, 0);
            this.content.Controls.SetChildIndex(this.lblAREA, 0);
            this.content.Controls.SetChildIndex(this.txtAreaName, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_CODE, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_INFORMATION, 0);
            this.content.Controls.SetChildIndex(this.lblBILL_INFORMATION, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.lblCLOSE_DATE, 0);
            this.content.Controls.SetChildIndex(this.txtPayDate, 0);
            this.content.Controls.SetChildIndex(this.lblPOLE, 0);
            this.content.Controls.SetChildIndex(this.txtPole, 0);
            this.content.Controls.SetChildIndex(this.lblBOX, 0);
            this.content.Controls.SetChildIndex(this.txtBox, 0);
            this.content.Controls.SetChildIndex(this.lblCASH_DRAWER, 0);
            this.content.Controls.SetChildIndex(this.txtCashDrawer, 0);
            this.content.Controls.SetChildIndex(this.dgv, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerCode, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerName, 0);
            this.content.Controls.SetChildIndex(this.txtMeterCode, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_STATUS, 0);
            this.content.Controls.SetChildIndex(this.cboStatus, 0);
            this.content.Controls.SetChildIndex(this.lblCURRENCY, 0);
            this.content.Controls.SetChildIndex(this.cboCurrency, 0);
            this.content.Controls.SetChildIndex(this.label12, 0);
            this.content.Controls.SetChildIndex(this.txtSignTotalDue, 0);
            this.content.Controls.SetChildIndex(this.txtSignDeposit, 0);
            this.content.Controls.SetChildIndex(this.txtSignRefundDeposit, 0);
            this.content.Controls.SetChildIndex(this.lblBREAKER_STATUS, 0);
            this.content.Controls.SetChildIndex(this.cboBreakerStatus, 0);
            // 
            // txtRefundDeposit
            // 
            resources.ApplyResources(this.txtRefundDeposit, "txtRefundDeposit");
            this.txtRefundDeposit.Name = "txtRefundDeposit";
            this.txtRefundDeposit.ReadOnly = true;
            this.txtRefundDeposit.TabStop = false;
            // 
            // lblREMAIN_DEPOSIT
            // 
            resources.ApplyResources(this.lblREMAIN_DEPOSIT, "lblREMAIN_DEPOSIT");
            this.lblREMAIN_DEPOSIT.Name = "lblREMAIN_DEPOSIT";
            // 
            // txtTotalDue
            // 
            resources.ApplyResources(this.txtTotalDue, "txtTotalDue");
            this.txtTotalDue.Name = "txtTotalDue";
            this.txtTotalDue.ReadOnly = true;
            this.txtTotalDue.TabStop = false;
            // 
            // txtDeposit
            // 
            resources.ApplyResources(this.txtDeposit, "txtDeposit");
            this.txtDeposit.Name = "txtDeposit";
            this.txtDeposit.ReadOnly = true;
            this.txtDeposit.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // lblDEPOSIT
            // 
            resources.ApplyResources(this.lblDEPOSIT, "lblDEPOSIT");
            this.lblDEPOSIT.Name = "lblDEPOSIT";
            // 
            // lblCUSTOMER_NAME
            // 
            resources.ApplyResources(this.lblCUSTOMER_NAME, "lblCUSTOMER_NAME");
            this.lblCUSTOMER_NAME.Name = "lblCUSTOMER_NAME";
            // 
            // lblCUSTOMER_CODE
            // 
            resources.ApplyResources(this.lblCUSTOMER_CODE, "lblCUSTOMER_CODE");
            this.lblCUSTOMER_CODE.Name = "lblCUSTOMER_CODE";
            // 
            // lblAREA
            // 
            resources.ApplyResources(this.lblAREA, "lblAREA");
            this.lblAREA.Name = "lblAREA";
            // 
            // txtAreaName
            // 
            resources.ApplyResources(this.txtAreaName, "txtAreaName");
            this.txtAreaName.Name = "txtAreaName";
            // 
            // lblMETER_CODE
            // 
            resources.ApplyResources(this.lblMETER_CODE, "lblMETER_CODE");
            this.lblMETER_CODE.Name = "lblMETER_CODE";
            // 
            // lblCUSTOMER_INFORMATION
            // 
            resources.ApplyResources(this.lblCUSTOMER_INFORMATION, "lblCUSTOMER_INFORMATION");
            this.lblCUSTOMER_INFORMATION.Name = "lblCUSTOMER_INFORMATION";
            // 
            // lblBILL_INFORMATION
            // 
            resources.ApplyResources(this.lblBILL_INFORMATION, "lblBILL_INFORMATION");
            this.lblBILL_INFORMATION.Name = "lblBILL_INFORMATION";
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblCLOSE_DATE
            // 
            resources.ApplyResources(this.lblCLOSE_DATE, "lblCLOSE_DATE");
            this.lblCLOSE_DATE.Name = "lblCLOSE_DATE";
            // 
            // lblTOTAL_DUE_AMOUNT
            // 
            resources.ApplyResources(this.lblTOTAL_DUE_AMOUNT, "lblTOTAL_DUE_AMOUNT");
            this.lblTOTAL_DUE_AMOUNT.Name = "lblTOTAL_DUE_AMOUNT";
            // 
            // txtPayDate
            // 
            resources.ApplyResources(this.txtPayDate, "txtPayDate");
            this.txtPayDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtPayDate.Name = "txtPayDate";
            this.txtPayDate.ShowUpDown = true;
            // 
            // txtPole
            // 
            resources.ApplyResources(this.txtPole, "txtPole");
            this.txtPole.Name = "txtPole";
            // 
            // lblPOLE
            // 
            resources.ApplyResources(this.lblPOLE, "lblPOLE");
            this.lblPOLE.Name = "lblPOLE";
            // 
            // txtBox
            // 
            resources.ApplyResources(this.txtBox, "txtBox");
            this.txtBox.Name = "txtBox";
            // 
            // lblBOX
            // 
            resources.ApplyResources(this.lblBOX, "lblBOX");
            this.lblBOX.Name = "lblBOX";
            // 
            // lblCASH_DRAWER
            // 
            resources.ApplyResources(this.lblCASH_DRAWER, "lblCASH_DRAWER");
            this.lblCASH_DRAWER.Name = "lblCASH_DRAWER";
            // 
            // txtCashDrawer
            // 
            resources.ApplyResources(this.txtCashDrawer, "txtCashDrawer");
            this.txtCashDrawer.Name = "txtCashDrawer";
            this.txtCashDrawer.ReadOnly = true;
            this.txtCashDrawer.TabStop = false;
            // 
            // dgv
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.INVOICE_ID,
            this.INVOICE_NO,
            this.INVOICE_DATE,
            this.INVOICE_TITLE,
            this.SETTLE_AMOUNT,
            this.PAID_AMOUNT,
            this.DUE_AMOUNT,
            this.CURRENCY_SING_});
            this.dgv.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            // 
            // INVOICE_ID
            // 
            this.INVOICE_ID.DataPropertyName = "INVOICE_ID";
            resources.ApplyResources(this.INVOICE_ID, "INVOICE_ID");
            this.INVOICE_ID.Name = "INVOICE_ID";
            // 
            // INVOICE_NO
            // 
            this.INVOICE_NO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.INVOICE_NO.DataPropertyName = "INVOICE_NO";
            resources.ApplyResources(this.INVOICE_NO, "INVOICE_NO");
            this.INVOICE_NO.Name = "INVOICE_NO";
            // 
            // INVOICE_DATE
            // 
            this.INVOICE_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.INVOICE_DATE.DataPropertyName = "INVOICE_DATE";
            dataGridViewCellStyle2.Format = "yyyy-MM-dd";
            this.INVOICE_DATE.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.INVOICE_DATE, "INVOICE_DATE");
            this.INVOICE_DATE.Name = "INVOICE_DATE";
            // 
            // INVOICE_TITLE
            // 
            this.INVOICE_TITLE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.INVOICE_TITLE.DataPropertyName = "INVOICE_TITLE";
            resources.ApplyResources(this.INVOICE_TITLE, "INVOICE_TITLE");
            this.INVOICE_TITLE.Name = "INVOICE_TITLE";
            // 
            // SETTLE_AMOUNT
            // 
            this.SETTLE_AMOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.SETTLE_AMOUNT.DataPropertyName = "SETTLE_AMOUNT";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "#,##0.####";
            this.SETTLE_AMOUNT.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.SETTLE_AMOUNT, "SETTLE_AMOUNT");
            this.SETTLE_AMOUNT.Name = "SETTLE_AMOUNT";
            // 
            // PAID_AMOUNT
            // 
            this.PAID_AMOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.PAID_AMOUNT.DataPropertyName = "PAID_AMOUNT";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "#,##0.####";
            this.PAID_AMOUNT.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.PAID_AMOUNT, "PAID_AMOUNT");
            this.PAID_AMOUNT.Name = "PAID_AMOUNT";
            // 
            // DUE_AMOUNT
            // 
            this.DUE_AMOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.DUE_AMOUNT.DataPropertyName = "DUE_AMOUNT";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "#,##0.####";
            this.DUE_AMOUNT.DefaultCellStyle = dataGridViewCellStyle5;
            resources.ApplyResources(this.DUE_AMOUNT, "DUE_AMOUNT");
            this.DUE_AMOUNT.Name = "DUE_AMOUNT";
            // 
            // CURRENCY_SING_
            // 
            this.CURRENCY_SING_.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.CURRENCY_SING_.DataPropertyName = "CURRENCY_SING";
            resources.ApplyResources(this.CURRENCY_SING_, "CURRENCY_SING_");
            this.CURRENCY_SING_.Name = "CURRENCY_SING_";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // txtCustomerCode
            // 
            resources.ApplyResources(this.txtCustomerCode, "txtCustomerCode");
            this.txtCustomerCode.Name = "txtCustomerCode";
            // 
            // txtCustomerName
            // 
            resources.ApplyResources(this.txtCustomerName, "txtCustomerName");
            this.txtCustomerName.Name = "txtCustomerName";
            // 
            // txtMeterCode
            // 
            resources.ApplyResources(this.txtMeterCode, "txtMeterCode");
            this.txtMeterCode.Name = "txtMeterCode";
            // 
            // lblMETER_STATUS
            // 
            resources.ApplyResources(this.lblMETER_STATUS, "lblMETER_STATUS");
            this.lblMETER_STATUS.Name = "lblMETER_STATUS";
            // 
            // cboStatus
            // 
            this.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboStatus.FormattingEnabled = true;
            resources.ApplyResources(this.cboStatus, "cboStatus");
            this.cboStatus.Name = "cboStatus";
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Name = "label12";
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.SelectedIndexChanged += new System.EventHandler(this.cboCurrency_SelectedIndexChanged);
            // 
            // lblCURRENCY
            // 
            resources.ApplyResources(this.lblCURRENCY, "lblCURRENCY");
            this.lblCURRENCY.Name = "lblCURRENCY";
            // 
            // txtSignTotalDue
            // 
            resources.ApplyResources(this.txtSignTotalDue, "txtSignTotalDue");
            this.txtSignTotalDue.Name = "txtSignTotalDue";
            this.txtSignTotalDue.ReadOnly = true;
            this.txtSignTotalDue.TabStop = false;
            // 
            // txtSignDeposit
            // 
            resources.ApplyResources(this.txtSignDeposit, "txtSignDeposit");
            this.txtSignDeposit.Name = "txtSignDeposit";
            this.txtSignDeposit.ReadOnly = true;
            this.txtSignDeposit.TabStop = false;
            // 
            // txtSignRefundDeposit
            // 
            resources.ApplyResources(this.txtSignRefundDeposit, "txtSignRefundDeposit");
            this.txtSignRefundDeposit.Name = "txtSignRefundDeposit";
            this.txtSignRefundDeposit.ReadOnly = true;
            this.txtSignRefundDeposit.TabStop = false;
            // 
            // lblBREAKER_STATUS
            // 
            resources.ApplyResources(this.lblBREAKER_STATUS, "lblBREAKER_STATUS");
            this.lblBREAKER_STATUS.Name = "lblBREAKER_STATUS";
            // 
            // cboBreakerStatus
            // 
            this.cboBreakerStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBreakerStatus.FormattingEnabled = true;
            resources.ApplyResources(this.cboBreakerStatus, "cboBreakerStatus");
            this.cboBreakerStatus.Name = "cboBreakerStatus";
            // 
            // DialogCloseCustomer
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Image = global::EPower.Properties.Resources.icon_payment;
            this.Name = "DialogCloseCustomer";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBox txtRefundDeposit;
        private Label lblREMAIN_DEPOSIT;
        private TextBox txtTotalDue;
        private TextBox txtDeposit;
        private Label lblDEPOSIT;
        private Label lblCUSTOMER_NAME;
        private Label lblCUSTOMER_CODE;
        private Label lblAREA;
        private TextBox txtAreaName;
        private Label lblBILL_INFORMATION;
        private Label lblCUSTOMER_INFORMATION;
        private Label lblMETER_CODE;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label lblCLOSE_DATE;
        private DateTimePicker txtPayDate;
        private Label lblTOTAL_DUE_AMOUNT;
        private TextBox txtBox;
        private Label lblBOX;
        private TextBox txtPole;
        private Label lblPOLE;
        private TextBox txtCashDrawer;
        private Label lblCASH_DRAWER;
        private DataGridView dgv;
        private Panel panel1;
        private TextBox txtMeterCode;
        private TextBox txtCustomerName;
        private TextBox txtCustomerCode;
        private Label lblMETER_STATUS;
        private ComboBox cboStatus;
        private Label label12;
        private ComboBox cboCurrency;
        private Label lblCURRENCY;
        private TextBox txtSignRefundDeposit;
        private TextBox txtSignDeposit;
        private TextBox txtSignTotalDue;
        private Label lblBREAKER_STATUS;
        private ComboBox cboBreakerStatus;
        private DataGridViewTextBoxColumn INVOICE_ID;
        private DataGridViewTextBoxColumn INVOICE_NO;
        private DataGridViewTextBoxColumn INVOICE_DATE;
        private DataGridViewTextBoxColumn INVOICE_TITLE;
        private DataGridViewTextBoxColumn SETTLE_AMOUNT;
        private DataGridViewTextBoxColumn PAID_AMOUNT;
        private DataGridViewTextBoxColumn DUE_AMOUNT;
        private DataGridViewTextBoxColumn CURRENCY_SING_;
    }
}