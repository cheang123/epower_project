﻿using System;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PagePhase : Form
    {
        public TBL_PHASE Phase
        {
            get
            {
                TBL_PHASE objPhase = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    try
                    {
                        int phaseId = (int)dgv.SelectedRows[0].Cells["PHASE_ID"].Value;
                        objPhase = DBDataContext.Db.TBL_PHASEs.FirstOrDefault(x => x.PHASE_ID == phaseId);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objPhase;
            }
        }

        #region Constructor
        public PagePhase()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            txt_QuickSearch(null, null);
        }
        #endregion

        #region Operation
        /// <summary>
        /// Add new phase.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNew_Click(object sender, EventArgs e)
        {
            DialogPhase dig = new DialogPhase(GeneralProcess.Insert, new TBL_PHASE() { PHASE_NAME = "" });
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.Phase.PHASE_ID);
            }
        }

        /// <summary>
        /// Edit phase.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                DialogPhase dig = new DialogPhase(GeneralProcess.Update, Phase);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.Phase.PHASE_ID);
                }
            }
        }

        /// <summary>
        /// Remove phase.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (IsDeletable())
            {
                DialogPhase dig = new DialogPhase(GeneralProcess.Delete, Phase);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.Phase.PHASE_ID - 1);
                }
            }
        }

        private void panel1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                btnADD.Visible = !btnADD.Visible;
                btnEDIT.Visible = !btnEDIT.Visible;
                btnREMOVE.Visible = !btnREMOVE.Visible;
            }
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //if (e.RowIndex == -1)
            //{
            //    return;
            //}
            //btnEdit_Click(null, null);
        }
        #endregion

        #region Method
        private bool IsDeletable()
        {
            //TODO: can delete record.
            bool val = false;
            if (dgv.SelectedRows.Count > 0)
            {
                val = true;
                if (DBDataContext.Db.TBL_METER_TYPEs.Where(x => x.METER_PHASE_ID == Phase.PHASE_ID && x.IS_ACTIVE).Count() > 0 ||
                    DBDataContext.Db.TBL_CIRCUIT_BREAKER_TYPEs.Where(x => x.BREAKER_PHASE_ID == Phase.PHASE_ID && x.IS_ACTIVE).Count() > 0)
                {
                    MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE);
                    val = false;
                }
            }
            return val;
        }

        /// <summary>
        /// Load data form database and display.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            try
            {
                dgv.DataSource = (from ps in DBDataContext.Db.TBL_PHASEs
                                  where ps.IS_ACTIVE &&

                                  ps.PHASE_NAME.ToLower().Contains(txtQuickSearch.Text.ToLower().Trim())
                                  orderby ps.PHASE_NAME
                                  select ps);
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }

        }

        #endregion
    }
}