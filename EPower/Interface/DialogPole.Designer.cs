﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogPole
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogPole));
            this.lblPOLE_CODE = new System.Windows.Forms.Label();
            this.txtPoleCode = new System.Windows.Forms.TextBox();
            this.lblAREA = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblCOLLECTOR = new System.Windows.Forms.Label();
            this.lblBILLER = new System.Windows.Forms.Label();
            this.lblCUTTER = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.cboAreaName = new System.Windows.Forms.ComboBox();
            this.btnAddArea = new SoftTech.Component.ExAddItem();
            this.cboCollector = new System.Windows.Forms.ComboBox();
            this.btnAddCollector = new SoftTech.Component.ExAddItem();
            this.cboBiller = new System.Windows.Forms.ComboBox();
            this.btnAddBiller = new SoftTech.Component.ExAddItem();
            this.cboCutter = new System.Windows.Forms.ComboBox();
            this.btnAddCutter = new SoftTech.Component.ExAddItem();
            this.label15 = new System.Windows.Forms.Label();
            this.lblTRANSFORMER = new System.Windows.Forms.Label();
            this.cboTransfo = new System.Windows.Forms.ComboBox();
            this.btnAddTransfo = new SoftTech.Component.ExAddItem();
            this.lblSTART_USE_DATE = new System.Windows.Forms.Label();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.lblOWN = new System.Windows.Forms.CheckBox();
            this.lblSTATUS = new System.Windows.Forms.Label();
            this.lblEND_USE_DATE = new System.Windows.Forms.Label();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.cboProvince = new System.Windows.Forms.ComboBox();
            this.lblPROVINCE = new System.Windows.Forms.Label();
            this.lblDISTRICT = new System.Windows.Forms.Label();
            this.lblCOMMUNE = new System.Windows.Forms.Label();
            this.lblVILLAGE = new System.Windows.Forms.Label();
            this.cboVillage = new System.Windows.Forms.ComboBox();
            this.cboCommune = new System.Windows.Forms.ComboBox();
            this.cboDistrict = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.cboVillage);
            this.content.Controls.Add(this.cboCommune);
            this.content.Controls.Add(this.cboDistrict);
            this.content.Controls.Add(this.lblVILLAGE);
            this.content.Controls.Add(this.lblCOMMUNE);
            this.content.Controls.Add(this.lblDISTRICT);
            this.content.Controls.Add(this.cboProvince);
            this.content.Controls.Add(this.lblPROVINCE);
            this.content.Controls.Add(this.lblEND_USE_DATE);
            this.content.Controls.Add(this.dtpEndDate);
            this.content.Controls.Add(this.lblOWN);
            this.content.Controls.Add(this.lblSTATUS);
            this.content.Controls.Add(this.lblSTART_USE_DATE);
            this.content.Controls.Add(this.dtpStartDate);
            this.content.Controls.Add(this.btnAddTransfo);
            this.content.Controls.Add(this.cboTransfo);
            this.content.Controls.Add(this.label15);
            this.content.Controls.Add(this.lblTRANSFORMER);
            this.content.Controls.Add(this.btnAddCutter);
            this.content.Controls.Add(this.cboCutter);
            this.content.Controls.Add(this.btnAddBiller);
            this.content.Controls.Add(this.btnAddCollector);
            this.content.Controls.Add(this.cboCollector);
            this.content.Controls.Add(this.btnAddArea);
            this.content.Controls.Add(this.cboAreaName);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.cboBiller);
            this.content.Controls.Add(this.label14);
            this.content.Controls.Add(this.label12);
            this.content.Controls.Add(this.label13);
            this.content.Controls.Add(this.lblCUTTER);
            this.content.Controls.Add(this.lblBILLER);
            this.content.Controls.Add(this.lblCOLLECTOR);
            this.content.Controls.Add(this.label6);
            this.content.Controls.Add(this.label5);
            this.content.Controls.Add(this.label4);
            this.content.Controls.Add(this.label2);
            this.content.Controls.Add(this.label3);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.lblAREA);
            this.content.Controls.Add(this.txtPoleCode);
            this.content.Controls.Add(this.lblPOLE_CODE);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblPOLE_CODE, 0);
            this.content.Controls.SetChildIndex(this.txtPoleCode, 0);
            this.content.Controls.SetChildIndex(this.lblAREA, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.label3, 0);
            this.content.Controls.SetChildIndex(this.label2, 0);
            this.content.Controls.SetChildIndex(this.label4, 0);
            this.content.Controls.SetChildIndex(this.label5, 0);
            this.content.Controls.SetChildIndex(this.label6, 0);
            this.content.Controls.SetChildIndex(this.lblCOLLECTOR, 0);
            this.content.Controls.SetChildIndex(this.lblBILLER, 0);
            this.content.Controls.SetChildIndex(this.lblCUTTER, 0);
            this.content.Controls.SetChildIndex(this.label13, 0);
            this.content.Controls.SetChildIndex(this.label12, 0);
            this.content.Controls.SetChildIndex(this.label14, 0);
            this.content.Controls.SetChildIndex(this.cboBiller, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.cboAreaName, 0);
            this.content.Controls.SetChildIndex(this.btnAddArea, 0);
            this.content.Controls.SetChildIndex(this.cboCollector, 0);
            this.content.Controls.SetChildIndex(this.btnAddCollector, 0);
            this.content.Controls.SetChildIndex(this.btnAddBiller, 0);
            this.content.Controls.SetChildIndex(this.cboCutter, 0);
            this.content.Controls.SetChildIndex(this.btnAddCutter, 0);
            this.content.Controls.SetChildIndex(this.lblTRANSFORMER, 0);
            this.content.Controls.SetChildIndex(this.label15, 0);
            this.content.Controls.SetChildIndex(this.cboTransfo, 0);
            this.content.Controls.SetChildIndex(this.btnAddTransfo, 0);
            this.content.Controls.SetChildIndex(this.dtpStartDate, 0);
            this.content.Controls.SetChildIndex(this.lblSTART_USE_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblSTATUS, 0);
            this.content.Controls.SetChildIndex(this.lblOWN, 0);
            this.content.Controls.SetChildIndex(this.dtpEndDate, 0);
            this.content.Controls.SetChildIndex(this.lblEND_USE_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblPROVINCE, 0);
            this.content.Controls.SetChildIndex(this.cboProvince, 0);
            this.content.Controls.SetChildIndex(this.lblDISTRICT, 0);
            this.content.Controls.SetChildIndex(this.lblCOMMUNE, 0);
            this.content.Controls.SetChildIndex(this.lblVILLAGE, 0);
            this.content.Controls.SetChildIndex(this.cboDistrict, 0);
            this.content.Controls.SetChildIndex(this.cboCommune, 0);
            this.content.Controls.SetChildIndex(this.cboVillage, 0);
            // 
            // lblPOLE_CODE
            // 
            resources.ApplyResources(this.lblPOLE_CODE, "lblPOLE_CODE");
            this.lblPOLE_CODE.Name = "lblPOLE_CODE";
            // 
            // txtPoleCode
            // 
            resources.ApplyResources(this.txtPoleCode, "txtPoleCode");
            this.txtPoleCode.Name = "txtPoleCode";
            this.txtPoleCode.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // lblAREA
            // 
            resources.ApplyResources(this.lblAREA, "lblAREA");
            this.lblAREA.Name = "lblAREA";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Name = "label3";
            // 
            // lblCOLLECTOR
            // 
            resources.ApplyResources(this.lblCOLLECTOR, "lblCOLLECTOR");
            this.lblCOLLECTOR.Name = "lblCOLLECTOR";
            // 
            // lblBILLER
            // 
            resources.ApplyResources(this.lblBILLER, "lblBILLER");
            this.lblBILLER.Name = "lblBILLER";
            // 
            // lblCUTTER
            // 
            resources.ApplyResources(this.lblCUTTER, "lblCUTTER");
            this.lblCUTTER.Name = "lblCUTTER";
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Name = "label12";
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Name = "label13";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Name = "label14";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // cboAreaName
            // 
            this.cboAreaName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAreaName.FormattingEnabled = true;
            resources.ApplyResources(this.cboAreaName, "cboAreaName");
            this.cboAreaName.Name = "cboAreaName";
            // 
            // btnAddArea
            // 
            this.btnAddArea.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnAddArea, "btnAddArea");
            this.btnAddArea.Name = "btnAddArea";
            this.btnAddArea.AddItem += new System.EventHandler(this.btnAddArea_AddItem);
            // 
            // cboCollector
            // 
            this.cboCollector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCollector.FormattingEnabled = true;
            resources.ApplyResources(this.cboCollector, "cboCollector");
            this.cboCollector.Name = "cboCollector";
            // 
            // btnAddCollector
            // 
            this.btnAddCollector.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnAddCollector, "btnAddCollector");
            this.btnAddCollector.Name = "btnAddCollector";
            this.btnAddCollector.AddItem += new System.EventHandler(this.btnAddCollector_AddItem);
            // 
            // cboBiller
            // 
            this.cboBiller.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBiller.FormattingEnabled = true;
            resources.ApplyResources(this.cboBiller, "cboBiller");
            this.cboBiller.Name = "cboBiller";
            // 
            // btnAddBiller
            // 
            this.btnAddBiller.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnAddBiller, "btnAddBiller");
            this.btnAddBiller.Name = "btnAddBiller";
            this.btnAddBiller.AddItem += new System.EventHandler(this.btnAddBiller_AddItem);
            // 
            // cboCutter
            // 
            this.cboCutter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCutter.FormattingEnabled = true;
            resources.ApplyResources(this.cboCutter, "cboCutter");
            this.cboCutter.Name = "cboCutter";
            // 
            // btnAddCutter
            // 
            this.btnAddCutter.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnAddCutter, "btnAddCutter");
            this.btnAddCutter.Name = "btnAddCutter";
            this.btnAddCutter.AddItem += new System.EventHandler(this.btnAddCutter_AddItem);
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Name = "label15";
            // 
            // lblTRANSFORMER
            // 
            resources.ApplyResources(this.lblTRANSFORMER, "lblTRANSFORMER");
            this.lblTRANSFORMER.Name = "lblTRANSFORMER";
            // 
            // cboTransfo
            // 
            this.cboTransfo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTransfo.FormattingEnabled = true;
            resources.ApplyResources(this.cboTransfo, "cboTransfo");
            this.cboTransfo.Name = "cboTransfo";
            // 
            // btnAddTransfo
            // 
            this.btnAddTransfo.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnAddTransfo, "btnAddTransfo");
            this.btnAddTransfo.Name = "btnAddTransfo";
            this.btnAddTransfo.AddItem += new System.EventHandler(this.btnAddTransfo_AddItem);
            // 
            // lblSTART_USE_DATE
            // 
            resources.ApplyResources(this.lblSTART_USE_DATE, "lblSTART_USE_DATE");
            this.lblSTART_USE_DATE.Name = "lblSTART_USE_DATE";
            // 
            // dtpStartDate
            // 
            resources.ApplyResources(this.dtpStartDate, "dtpStartDate");
            this.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // lblOWN
            // 
            resources.ApplyResources(this.lblOWN, "lblOWN");
            this.lblOWN.Checked = true;
            this.lblOWN.CheckState = System.Windows.Forms.CheckState.Checked;
            this.lblOWN.Name = "lblOWN";
            this.lblOWN.UseVisualStyleBackColor = true;
            // 
            // lblSTATUS
            // 
            resources.ApplyResources(this.lblSTATUS, "lblSTATUS");
            this.lblSTATUS.Name = "lblSTATUS";
            // 
            // lblEND_USE_DATE
            // 
            resources.ApplyResources(this.lblEND_USE_DATE, "lblEND_USE_DATE");
            this.lblEND_USE_DATE.Name = "lblEND_USE_DATE";
            // 
            // dtpEndDate
            // 
            this.dtpEndDate.Checked = false;
            resources.ApplyResources(this.dtpEndDate, "dtpEndDate");
            this.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.ShowCheckBox = true;
            this.dtpEndDate.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            this.dtpEndDate.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dtpEndDate_MouseDown);
            // 
            // cboProvince
            // 
            this.cboProvince.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboProvince, "cboProvince");
            this.cboProvince.FormattingEnabled = true;
            this.cboProvince.Name = "cboProvince";
            this.cboProvince.SelectedIndexChanged += new System.EventHandler(this.cboProvince_SelectedIndexChanged);
            // 
            // lblPROVINCE
            // 
            resources.ApplyResources(this.lblPROVINCE, "lblPROVINCE");
            this.lblPROVINCE.Name = "lblPROVINCE";
            // 
            // lblDISTRICT
            // 
            resources.ApplyResources(this.lblDISTRICT, "lblDISTRICT");
            this.lblDISTRICT.Name = "lblDISTRICT";
            // 
            // lblCOMMUNE
            // 
            resources.ApplyResources(this.lblCOMMUNE, "lblCOMMUNE");
            this.lblCOMMUNE.Name = "lblCOMMUNE";
            // 
            // lblVILLAGE
            // 
            resources.ApplyResources(this.lblVILLAGE, "lblVILLAGE");
            this.lblVILLAGE.Name = "lblVILLAGE";
            // 
            // cboVillage
            // 
            this.cboVillage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboVillage, "cboVillage");
            this.cboVillage.FormattingEnabled = true;
            this.cboVillage.Name = "cboVillage";
            // 
            // cboCommune
            // 
            this.cboCommune.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboCommune, "cboCommune");
            this.cboCommune.FormattingEnabled = true;
            this.cboCommune.Name = "cboCommune";
            this.cboCommune.SelectedIndexChanged += new System.EventHandler(this.cboCommune_SelectedIndexChanged);
            // 
            // cboDistrict
            // 
            this.cboDistrict.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboDistrict, "cboDistrict");
            this.cboDistrict.FormattingEnabled = true;
            this.cboDistrict.Name = "cboDistrict";
            this.cboDistrict.SelectedIndexChanged += new System.EventHandler(this.cboDistrict_SelectedIndexChanged);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Name = "label2";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Name = "label4";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Name = "label5";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Name = "label6";
            // 
            // DialogPole
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogPole";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Label lblAREA;
        private TextBox txtPoleCode;
        private Label lblPOLE_CODE;
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label label3;
        private Label label1;
        private Label lblCUTTER;
        private Label lblBILLER;
        private Label lblCOLLECTOR;
        private Label label14;
        private Label label12;
        private Label label13;
        private ExButton btnCHANGE_LOG;
        private ComboBox cboAreaName;
        private ExAddItem btnAddArea;
        private ComboBox cboCollector;
        private ExAddItem btnAddCollector;
        private ComboBox cboBiller;
        private ComboBox cboCutter;
        private ExAddItem btnAddBiller;
        private ExAddItem btnAddCutter;
        private ExAddItem btnAddTransfo;
        private ComboBox cboTransfo;
        private Label label15;
        private Label lblTRANSFORMER;
        private Label lblSTART_USE_DATE;
        private DateTimePicker dtpStartDate;
        private CheckBox lblOWN;
        private Label lblSTATUS;
        private Label lblEND_USE_DATE;
        private DateTimePicker dtpEndDate;
        private ComboBox cboProvince;
        private Label lblPROVINCE;
        private Label lblDISTRICT;
        private Label lblCOMMUNE;
        private Label lblVILLAGE;
        private ComboBox cboVillage;
        private ComboBox cboCommune;
        private ComboBox cboDistrict;
        private Label label6;
        private Label label5;
        private Label label4;
        private Label label2;
    }
}