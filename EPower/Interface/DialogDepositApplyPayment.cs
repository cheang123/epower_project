﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogDepositApplyPayment : ExDialog
    {
        TBL_CUSTOMER _objCustomer = null;
        TBL_CUSTOMER_METER _objCustomerMeter = null;
        TBL_METER _objMeter = null;
        TBL_USER_CASH_DRAWER _objUserCashDrawer = null;
        TBL_CASH_DRAWER _objCashDrawer = null;
        DataTable source = new DataTable();

        DataTable _dtInvoice = new DataTable();
        int _intCurrencyId = 0;

        bool _blnIsPayByInvoice = false;

        public DialogDepositApplyPayment()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(this.dgv);
            this._objUserCashDrawer = Login.CurrentCashDrawer;
            if (this._objUserCashDrawer != null)
            {
                this._objCashDrawer = DBDataContext.Db.TBL_CASH_DRAWERs.FirstOrDefault(row => row.CASH_DRAWER_ID == this._objUserCashDrawer.CASH_DRAWER_ID);
            }
            // if no cashdrawer is open then lock user edit.
            if (this._objCashDrawer != null)
            {
                this.txtLogin.Text = Login.CurrentLogin.LOGIN_NAME;
            }
            else
            {
                UIHelper.SetEnabled(this, false);
                this.btnOK.Enabled = false;
            }
            bindCurrency();
        }

        private void bindCurrency()
        {
            UIHelper.SetDataSourceToComboBox(cboCurrency, DBDataContext.Db.TLKP_CURRENCies.Select(x => new { x.CURRENCY_ID, x.CURRENCY_NAME }));
        }

        public DialogDepositApplyPayment(TBL_CUSTOMER objCustomer)
            : this()
        {
            _blnIsPayByInvoice = true;
            _objCustomer = objCustomer;
            txtCustomerCode.Text = _objCustomer.CUSTOMER_CODE;
            txtCustomerCode_AdvanceSearch(null, null);
            txtCustomerCode.Enabled =
            txtMeterCode.Enabled =
            txtCustomerName.Enabled = false;
        }

        private void newPayment()
        {
            this._objCustomer = null;
            this._objCustomerMeter = null;
            this._objMeter = null;

            this.txtCustomerCode.CancelSearch(false);
            this.txtCustomerName.CancelSearch(false);
            this.txtMeterCode.CancelSearch(false);

            this.txtCustomerCode.Text = "";
            this.txtCustomerName.Text = "";
            this.txtMeterCode.Text = "";
            this.txtAreaName.Text = "";
            this.txtPole.Text = "";
            this.txtBox.Text = "";

            this.txtPayDate.ClearValue();

            this.txtTotalDue.Text = "";
            this.txtTotalPay.Text = "";
            this.txtTotalBalance.Text = "";
            this.source.Rows.Clear();
            this.chkPARTIAL_PAYMENT.Checked = false;
            this.txtCustomerCode.Focus();

            this.Refresh();
        }

        /// <summary>
        /// Reads object to display to the form.
        /// This form require tree objects to be not null
        /// this._objCustomer,
        /// this._objCustomerMeter, and 
        /// this._objMeter
        /// </summary>
        private void read()
        {
            // accpet entry.
            this.txtCustomerCode.AcceptSearch(false);
            this.txtCustomerName.AcceptSearch(false);
            this.txtMeterCode.AcceptSearch(false);

            //get customer currency
            _intCurrencyId = (int)cboCurrency.SelectedValue;

            // show customer information.
            this.txtCustomerName.Text = this._objCustomer.LAST_NAME_KH + " " + this._objCustomer.FIRST_NAME_KH;
            this.txtCustomerCode.Text = this._objCustomer.CUSTOMER_CODE;
            this.txtAreaName.Text = DBDataContext.Db.TBL_AREAs.FirstOrDefault(row => row.AREA_ID == this._objCustomer.AREA_ID).AREA_NAME;

            if (this._objMeter != null)
            {
                this.txtMeterCode.Text = this._objMeter.METER_CODE;
                this.txtPole.Text = DBDataContext.Db.TBL_POLEs.FirstOrDefault(row => row.POLE_ID == this._objCustomerMeter.POLE_ID).POLE_CODE;
                this.txtBox.Text = DBDataContext.Db.TBL_BOXes.FirstOrDefault(row => row.BOX_ID == this._objCustomerMeter.BOX_ID).BOX_CODE;
            }

            // invoice.
            var query = from inv in DBDataContext.Db.TBL_INVOICEs
                        join c in DBDataContext.Db.TLKP_CURRENCies on inv.CURRENCY_ID equals c.CURRENCY_ID
                        where inv.CUSTOMER_ID == this._objCustomer.CUSTOMER_ID
                              && (inv.INVOICE_STATUS == (int)InvoiceStatus.Open || inv.INVOICE_STATUS == (int)InvoiceStatus.Pay)
                              && c.CURRENCY_ID == _intCurrencyId
                        select new
                        {
                            INVOICE_ID = inv.INVOICE_ID,
                            INVOICE_NO = inv.INVOICE_NO,
                            INVOICE_DATE = inv.INVOICE_DATE,
                            INVOICE_TITLE = inv.INVOICE_TITLE,
                            SETTLE_AMOUNT = inv.SETTLE_AMOUNT,
                            PAID_AMOUNT = inv.PAID_AMOUNT,
                            DUE_AMOUNT = inv.SETTLE_AMOUNT - inv.PAID_AMOUNT,
                            c.CURRENCY_SING,
                        };

            _dtInvoice = query._ToDataTable();
            this.dgv.DataSource = _dtInvoice;
            this.txtTotalDue.Text = UIHelper.FormatCurrency(this.getTotalDue(false), _intCurrencyId);
            this.txtTotalBalance.Text = UIHelper.FormatCurrency(this.getTotalDue(false), _intCurrencyId);
            this.txtTotalPay.Focus();
            this.txtPayDate.SetValue(DBDataContext.Db.GetSystemDate());
            this.txtDepositBalance.Text = Method.GetCustomerDeposit(_objCustomer.CUSTOMER_ID, _intCurrencyId).ToString(DataHelper.MoneyFormat);
            if (this.dgv.Rows.Count == 0)
            {
                MsgBox.ShowInformation(Resources.MS_CUSTOMER_NO_INVOICE);
                return;
            }

            this.btnOK.Enabled = true;
        }

        public decimal getTotalDue(bool blnInvoiceOnly)
        {
            decimal tmp = 0.0m;
            foreach (DataGridViewRow row in this.dgv.Rows)
            {
                tmp += (decimal)row.Cells["DUE_AMOUNT"].Value;
            }
            return tmp;
        }
        public decimal getTotalPay()
        {
            return UIHelper.Round(DataHelper.ParseToDecimal(this.txtTotalPay.Text), this._intCurrencyId);
        }
        public decimal getTotalBalance()
        {
            return this.getTotalDue(false) - getTotalPay();
        }
        private bool invalid()
        {
            bool result = false;
            this.ClearAllValidation();

            if (this._objCustomer == null)
            {
                this.txtCustomerCode.SetValidation(string.Format(Resources.REQUIRED, this.lblCUSTOMER_CODE.Text));
                return true;
            }

            if (!DataHelper.IsNumber(this.txtTotalPay.Text))
            {
                this.txtTotalPay.SetValidation(string.Format(Resources.REQUIRED, this.lblSETTLE_AMOUNT.Text));
                result = true;
            }

            if (this.getTotalPay() == 0)
            {
                this.txtTotalPay.SetValidation(string.Format(Resources.REQUIRED, this.lblSETTLE_AMOUNT.Text));
                result = true;
            }
            if (this.getTotalBalance() < 0)
            {
                this.txtTotalPay.SetValidation(Resources.MS_PAYMENT_OVER_AMOUNT);
                result = true;
            }

            if (this.getTotalPay() > DataHelper.ParseToDecimal(txtDepositBalance.Text))
            {
                this.txtTotalPay.SetValidation(Resources.MS_DEPOSIT_NOT_ENOUGH_TO_PAY);
                result = true;
            }
            if (cboCurrency.SelectedIndex == -1)
            {
                cboCurrency.SetValidation(string.Format(Resources.REQUIRED, lblCURRENCY.Text));
                result = true;
            }

            if (!this.chkPARTIAL_PAYMENT.Checked
                && this.getTotalBalance() != 0)
            {
                MsgBox.ShowInformation(Resources.MS_PAID_FULL_AMOUNT);
                result = true;
            }
            return result;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtCustomerCode_AdvanceSearch(object sender, EventArgs e)
        {
            if (this.txtCustomerCode.Text == "")
            {
                this.txtCustomerCode.CancelSearch(false);
                return;
            }
            _objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_CODE == Method.FormatCustomerCode(this.txtCustomerCode.Text));
            if (_objCustomer == null)
            {
                this.txtCustomerCode.CancelSearch(false);
                MsgBox.ShowInformation(Resources.MS_CUSTOMER_NOT_EXISTS);
                return;
            }

            _objCustomerMeter = DBDataContext.Db.TBL_CUSTOMER_METERs.FirstOrDefault(row => row.CUSTOMER_ID == this._objCustomer.CUSTOMER_ID && row.IS_ACTIVE);
            if (_objCustomerMeter == null)
            {
                this.txtCustomerCode.CancelSearch(false);
            }
            else
            {
                _objMeter = DBDataContext.Db.TBL_METERs.FirstOrDefault(row => row.METER_ID == this._objCustomerMeter.METER_ID);
                if (_objMeter == null)
                {
                    this.txtCustomerCode.CancelSearch(false);
                }
            }
            this.read();
        }

        private void txtMeterCode_CancelAdvanceSearch(object sender, EventArgs e)
        {
            newPayment();
        }

        private void txtPayAmount_TextChanged(object sender, EventArgs e)
        {
            decimal dueAmount = DataHelper.ParseToDecimal(this.txtTotalDue.Text) - DataHelper.ParseToDecimal(this.txtTotalPay.Text);
            this.txtTotalBalance.Text = UIHelper.FormatCurrency(dueAmount, _intCurrencyId);
            this.lblText_.Text = DataHelper.NumberToWord(DataHelper.ParseToDecimal(this.txtTotalPay.Text));
            txtRemainDeposit.Text = (DataHelper.ParseToDecimal(txtDepositBalance.Text) - DataHelper.ParseToDecimal(txtTotalPay.Text)).ToString(DataHelper.MoneyFormat);
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.savePayment();
        }

        private void savePayment()
        {
            if (this.dgv.Rows.Count == 0)
            {
                return;
            }

            if (invalid())
            {
                return;
            }

            // PROCESS PAYMENT
            // ========================
            // 1. SETTLE DEPOSIT FIRST
            // 2. ADD A RECORD PAYMENT.
            // 3. FOR EACH INVOICE.
            //    >> ADD PAYMENT_DETAIL.
            //    >> UPDATE INVOICE. 

            // amoun to pay FIFO.
            try
            {
                btnOK.Enabled = false;
                decimal remainAmount = this.getTotalPay();
                DateTime now = DBDataContext.Db.GetSystemDate();
                TBL_PAYMENT objPayment = new TBL_PAYMENT();
                TBL_CUS_DEPOSIT objCusDeposit = new TBL_CUS_DEPOSIT();
                var exchangeRate = new ExchangeRateLogic().findLastExchangeRate(this.txtPayDate.Value, (int)cboCurrency.SelectedValue);

                using (TransactionScope tran = new TransactionScope())
                {
                    if (_dtInvoice.Rows.Count > 0)
                    {
                        // 1. ADD PAYMENT.
                        TBL_CHANGE_LOG log = new TBL_CHANGE_LOG();
                        objPayment = new TBL_PAYMENT()
                        {
                            CREATE_BY = SoftTech.Security.Logic.Login.CurrentLogin.LOGIN_NAME,
                            CREATE_ON = now,
                            CURRENCY_ID = (int)cboCurrency.SelectedValue,
                            PAY_DATE = this.txtPayDate.Value,
                            CUSTOMER_ID = this._objCustomer.CUSTOMER_ID,
                            IS_ACTIVE = true,
                            PAY_AMOUNT = remainAmount,
                            DUE_AMOUNT = this.getTotalDue(true),
                            PAYMENT_ID = 0,
                            PAYMENT_NO = Method.GetNextSequence(Sequence.Receipt, true),
                            USER_CASH_DRAWER_ID = this._objUserCashDrawer.USER_CASH_DRAWER_ID,
                            EXCHANGE_RATE = exchangeRate.EXCHANGE_RATE,
                            EXCHANGE_RATE_DATE = exchangeRate.CREATE_ON
                        };
                        log = DBDataContext.Db.Insert(objPayment);


                        //SETTLE FOR DEPOSIT
                        DateTime datNow = DBDataContext.Db.GetSystemDate();
                        string strDepositNO = Method.GetNextSequence(Sequence.Receipt, true);
                        bool blnIsPaid = true;

                        objCusDeposit = Method.NewCustomerDeposit(_objCustomer.CUSTOMER_ID, -remainAmount, DataHelper.ParseToDecimal(txtRemainDeposit.Text), objPayment.CURRENCY_ID, DepositAction.AppyPayment, objPayment.PAYMENT_NO, datNow, blnIsPaid, datNow);
                        objCusDeposit.DEPOSIT_NO = strDepositNO;
                        DBDataContext.Db.TBL_CUS_DEPOSITs.InsertOnSubmit(objCusDeposit);
                        DBDataContext.Db.SubmitChanges();


                        //Payment Detail
                        foreach (DataRow row in _dtInvoice.Rows)
                        {
                            TBL_INVOICE objInvoice = DBDataContext.Db.TBL_INVOICEs.FirstOrDefault(inv => inv.INVOICE_ID == (long)row["INVOICE_ID"]);

                            // calculate due amount and pay amount
                            decimal dueAmount = (decimal)row["DUE_AMOUNT"];
                            decimal payAmount;
                            if (remainAmount >= dueAmount)
                            {
                                payAmount = dueAmount;
                                remainAmount -= dueAmount;
                            }
                            else
                            {
                                payAmount = remainAmount;
                                remainAmount = 0;
                            }

                            // add payment detail.
                            TBL_PAYMENT_DETAIL objPaymentDetail = new TBL_PAYMENT_DETAIL()
                            {
                                PAYMENT_DETAIL_ID = 0,
                                PAYMENT_ID = objPayment.PAYMENT_ID,
                                INVOICE_ID = objInvoice.INVOICE_ID,
                                DUE_AMOUNT = dueAmount,
                                PAY_AMOUNT = payAmount
                            };
                            DBDataContext.Db.InsertChild(objPaymentDetail, objPayment, ref log);

                            // update invoice.
                            TBL_INVOICE objInvoiceBackup = new TBL_INVOICE();
                            objInvoice._CopyTo(objInvoiceBackup);
                            objInvoice.PAID_AMOUNT += payAmount;
                            objInvoice.INVOICE_STATUS = objInvoice.SETTLE_AMOUNT == objInvoice.PAID_AMOUNT ? (int)InvoiceStatus.Close : (int)InvoiceStatus.Open;
                            DBDataContext.Db.UpdateChild(objInvoiceBackup, objInvoice, objPayment, ref log);

                            // if there are no other amount
                            // to pay to other invoice break loop
                            if (remainAmount == 0)
                            {
                                break;
                            }
                        }
                    }
                    tran.Complete();
                }
                if (chkPRINT_RECEIPT.Checked)
                {
                    if (objPayment.PAYMENT_ID > 0)
                    {
                        CrystalReportHelper ch = new CrystalReportHelper("ReportReceiptPayment.rpt");
                        ch.SetParameter("PAYMENT_ID", (int)objPayment.PAYMENT_ID);
                        ch.PrintReport(Settings.Default.PRINTER_RECIEPT);
                    }
                }
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                this.btnOK.Enabled = false;
                MsgBox.ShowError(ex);
            }
        }

        private void txtPayAmount_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                this.savePayment();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                this.newPayment();
            }
        }

        private void txtMeterCode_AdvanceSearch(object sender, EventArgs e)
        {
            // if not entry.
            if (txtMeterCode.Text.Trim() == "")
            {
                this.txtMeterCode.CancelSearch(false);
                return;
            }

            string strMeterCode = Method.FormatMeterCode(this.txtMeterCode.Text);
            this._objMeter = DBDataContext.Db.TBL_METERs.FirstOrDefault(m => m.METER_CODE == strMeterCode);

            // if not contain in database.
            if (this._objMeter == null)
            {
                MsgBox.ShowInformation(Resources.MS_METER_CODE_NOT_FOUND);
                this.txtMeterCode.CancelSearch(false);
                return;
            }

            // if meter is inuse.
            if (this._objMeter.STATUS_ID != (int)MeterStatus.Used)
            {
                MsgBox.ShowInformation(Resources.MS_METER_STATUS_IN_USE);
                this.txtMeterCode.CancelSearch(false);
                return;
            }

            this._objCustomerMeter = DBDataContext.Db.TBL_CUSTOMER_METERs.FirstOrDefault(row => row.IS_ACTIVE && row.METER_ID == this._objMeter.METER_ID);

            this._objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == this._objCustomerMeter.CUSTOMER_ID);

            this.read();

        }

        private void txtCustomerName_AdvanceSearch(object sender, EventArgs e)
        {
            DialogCustomerSearch diag = new DialogCustomerSearch(this.txtCustomerName.Text, DialogCustomerSearch.PowerType.AllType);
            if (diag.ShowDialog() != DialogResult.OK)
            {
                this.txtCustomerName.CancelSearch(false);
                return;
            }
            this._objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == diag.CustomerID);
            if (_objCustomer == null)
            {
                this.txtCustomerName.CancelSearch(false);
                return;
            }
            _objCustomerMeter = DBDataContext.Db.TBL_CUSTOMER_METERs.FirstOrDefault(row => row.CUSTOMER_ID == this._objCustomer.CUSTOMER_ID && row.IS_ACTIVE);
            if (_objCustomerMeter == null)
            {
                this.txtCustomerName.CancelSearch(false);
            }
            else
            {
                _objMeter = DBDataContext.Db.TBL_METERs.FirstOrDefault(row => row.METER_ID == this._objCustomerMeter.METER_ID);
                if (_objMeter == null)
                {
                    this.txtCustomerName.CancelSearch(false);
                }
            }
            this.read();
        }

        private void InputEnglish(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void InputKhmer(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void cboCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboCurrency.SelectedIndex == -1)
            {
                return;
            }
            var c = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.CURRENCY_ID == (int)cboCurrency.SelectedValue);

            txtSignDepositBalance.Text =
                txtSignRemainDeposit.Text =
                txtSignTotalBalance.Text =
                txtSignTotalDue.Text =
                txtSignTotalPay.Text = c.CURRENCY_SING;

            if (_objCustomer != null)
            {
                read();
            }
        }



    }
}
