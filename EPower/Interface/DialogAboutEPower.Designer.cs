﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogAboutEPower
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogAboutEPower));
            this.btnOK = new SoftTech.Component.ExButton();
            this.picICON_APP = new System.Windows.Forms.PictureBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabINFORMATION = new System.Windows.Forms.TabPage();
            this.picIe = new System.Windows.Forms.PictureBox();
            this.picFb = new System.Windows.Forms.PictureBox();
            this.lblPROGRAM = new System.Windows.Forms.Label();
            this.lblVERSION = new System.Windows.Forms.Label();
            this.lblCOPY_RIGHT = new System.Windows.Forms.Label();
            this.lblPHONE_CONTACT = new System.Windows.Forms.Label();
            this.lblPN_ = new System.Windows.Forms.Label();
            this.lblCOMPANY = new System.Windows.Forms.Label();
            this.lblV_ = new System.Windows.Forms.Label();
            this.lblCopy_ = new System.Windows.Forms.Label();
            this.lblCom_ = new System.Windows.Forms.Label();
            this.lblPhone_ = new System.Windows.Forms.Label();
            this.lblLINK_EPOWER_ = new SoftTech.Component.ExLinkLabel(this.components);
            this.tabMAINTENANCE = new System.Windows.Forms.TabPage();
            this.btnCONTINUE_LICENSE = new SoftTech.Component.ExButton();
            this.txtSerial = new System.Windows.Forms.TextBox();
            this.lblTOTAL_CUSTOMER = new System.Windows.Forms.Label();
            this.lblTotalCus_ = new System.Windows.Forms.Label();
            this.lblEXPIRE_DATE = new System.Windows.Forms.Label();
            this.txtDueDate_ = new System.Windows.Forms.Label();
            this.lblACTIVATE_MONTH = new System.Windows.Forms.Label();
            this.txtMonth_ = new System.Windows.Forms.Label();
            this.lblACTIVATE_DATE = new System.Windows.Forms.Label();
            this.txtActivateDate_ = new System.Windows.Forms.Label();
            this.pbSync = new System.Windows.Forms.PictureBox();
            this.lblVersioning_ = new System.Windows.Forms.Label();
            this.btnUPDATE_ = new SoftTech.Component.ExLinkLabel(this.components);
            this.toolTipLink = new System.Windows.Forms.ToolTip(this.components);
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picICON_APP)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabINFORMATION.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picIe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFb)).BeginInit();
            this.tabMAINTENANCE.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSync)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.btnUPDATE_);
            this.content.Controls.Add(this.lblVersioning_);
            this.content.Controls.Add(this.pbSync);
            this.content.Controls.Add(this.tabControl1);
            this.content.Controls.Add(this.btnOK);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.tabControl1, 0);
            this.content.Controls.SetChildIndex(this.pbSync, 0);
            this.content.Controls.SetChildIndex(this.lblVersioning_, 0);
            this.content.Controls.SetChildIndex(this.btnUPDATE_, 0);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // picICON_APP
            // 
            resources.ApplyResources(this.picICON_APP, "picICON_APP");
            this.picICON_APP.Name = "picICON_APP";
            this.picICON_APP.TabStop = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabINFORMATION);
            this.tabControl1.Controls.Add(this.tabMAINTENANCE);
            resources.ApplyResources(this.tabControl1, "tabControl1");
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            // 
            // tabINFORMATION
            // 
            this.tabINFORMATION.Controls.Add(this.picIe);
            this.tabINFORMATION.Controls.Add(this.picFb);
            this.tabINFORMATION.Controls.Add(this.lblPROGRAM);
            this.tabINFORMATION.Controls.Add(this.picICON_APP);
            this.tabINFORMATION.Controls.Add(this.lblVERSION);
            this.tabINFORMATION.Controls.Add(this.lblCOPY_RIGHT);
            this.tabINFORMATION.Controls.Add(this.lblPHONE_CONTACT);
            this.tabINFORMATION.Controls.Add(this.lblPN_);
            this.tabINFORMATION.Controls.Add(this.lblCOMPANY);
            this.tabINFORMATION.Controls.Add(this.lblV_);
            this.tabINFORMATION.Controls.Add(this.lblCopy_);
            this.tabINFORMATION.Controls.Add(this.lblCom_);
            this.tabINFORMATION.Controls.Add(this.lblPhone_);
            this.tabINFORMATION.Controls.Add(this.lblLINK_EPOWER_);
            resources.ApplyResources(this.tabINFORMATION, "tabINFORMATION");
            this.tabINFORMATION.Name = "tabINFORMATION";
            this.tabINFORMATION.UseVisualStyleBackColor = true;
            // 
            // picIe
            // 
            this.picIe.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picIe.Image = global::EPower.Properties.Resources.internet_ic;
            resources.ApplyResources(this.picIe, "picIe");
            this.picIe.Name = "picIe";
            this.picIe.TabStop = false;
            this.toolTipLink.SetToolTip(this.picIe, resources.GetString("picIe.ToolTip"));
            this.picIe.MouseClick += new System.Windows.Forms.MouseEventHandler(this.picIe_MouseClick);
            // 
            // picFb
            // 
            this.picFb.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picFb.Image = global::EPower.Properties.Resources.fb_ic1;
            resources.ApplyResources(this.picFb, "picFb");
            this.picFb.Name = "picFb";
            this.picFb.TabStop = false;
            this.toolTipLink.SetToolTip(this.picFb, resources.GetString("picFb.ToolTip"));
            this.picFb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.picFb_MouseClick);
            // 
            // lblPROGRAM
            // 
            resources.ApplyResources(this.lblPROGRAM, "lblPROGRAM");
            this.lblPROGRAM.Name = "lblPROGRAM";
            // 
            // lblVERSION
            // 
            resources.ApplyResources(this.lblVERSION, "lblVERSION");
            this.lblVERSION.Name = "lblVERSION";
            // 
            // lblCOPY_RIGHT
            // 
            resources.ApplyResources(this.lblCOPY_RIGHT, "lblCOPY_RIGHT");
            this.lblCOPY_RIGHT.Name = "lblCOPY_RIGHT";
            // 
            // lblPHONE_CONTACT
            // 
            resources.ApplyResources(this.lblPHONE_CONTACT, "lblPHONE_CONTACT");
            this.lblPHONE_CONTACT.Name = "lblPHONE_CONTACT";
            // 
            // lblPN_
            // 
            resources.ApplyResources(this.lblPN_, "lblPN_");
            this.lblPN_.Name = "lblPN_";
            // 
            // lblCOMPANY
            // 
            resources.ApplyResources(this.lblCOMPANY, "lblCOMPANY");
            this.lblCOMPANY.Name = "lblCOMPANY";
            // 
            // lblV_
            // 
            resources.ApplyResources(this.lblV_, "lblV_");
            this.lblV_.Name = "lblV_";
            // 
            // lblCopy_
            // 
            resources.ApplyResources(this.lblCopy_, "lblCopy_");
            this.lblCopy_.Name = "lblCopy_";
            // 
            // lblCom_
            // 
            resources.ApplyResources(this.lblCom_, "lblCom_");
            this.lblCom_.Name = "lblCom_";
            // 
            // lblPhone_
            // 
            resources.ApplyResources(this.lblPhone_, "lblPhone_");
            this.lblPhone_.Name = "lblPhone_";
            // 
            // lblLINK_EPOWER_
            // 
            resources.ApplyResources(this.lblLINK_EPOWER_, "lblLINK_EPOWER_");
            this.lblLINK_EPOWER_.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblLINK_EPOWER_.Name = "lblLINK_EPOWER_";
            this.lblLINK_EPOWER_.TabStop = true;
            this.toolTipLink.SetToolTip(this.lblLINK_EPOWER_, resources.GetString("lblLINK_EPOWER_.ToolTip"));
            this.lblLINK_EPOWER_.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblLINK_EPOWER__LinkClicked);
            // 
            // tabMAINTENANCE
            // 
            this.tabMAINTENANCE.Controls.Add(this.btnCONTINUE_LICENSE);
            this.tabMAINTENANCE.Controls.Add(this.txtSerial);
            this.tabMAINTENANCE.Controls.Add(this.lblTOTAL_CUSTOMER);
            this.tabMAINTENANCE.Controls.Add(this.lblTotalCus_);
            this.tabMAINTENANCE.Controls.Add(this.lblEXPIRE_DATE);
            this.tabMAINTENANCE.Controls.Add(this.txtDueDate_);
            this.tabMAINTENANCE.Controls.Add(this.lblACTIVATE_MONTH);
            this.tabMAINTENANCE.Controls.Add(this.txtMonth_);
            this.tabMAINTENANCE.Controls.Add(this.lblACTIVATE_DATE);
            this.tabMAINTENANCE.Controls.Add(this.txtActivateDate_);
            resources.ApplyResources(this.tabMAINTENANCE, "tabMAINTENANCE");
            this.tabMAINTENANCE.Name = "tabMAINTENANCE";
            this.tabMAINTENANCE.UseVisualStyleBackColor = true;
            // 
            // btnCONTINUE_LICENSE
            // 
            resources.ApplyResources(this.btnCONTINUE_LICENSE, "btnCONTINUE_LICENSE");
            this.btnCONTINUE_LICENSE.Name = "btnCONTINUE_LICENSE";
            this.btnCONTINUE_LICENSE.UseVisualStyleBackColor = true;
            this.btnCONTINUE_LICENSE.Click += new System.EventHandler(this.btnUpdateSupport_Click);
            // 
            // txtSerial
            // 
            resources.ApplyResources(this.txtSerial, "txtSerial");
            this.txtSerial.Name = "txtSerial";
            // 
            // lblTOTAL_CUSTOMER
            // 
            resources.ApplyResources(this.lblTOTAL_CUSTOMER, "lblTOTAL_CUSTOMER");
            this.lblTOTAL_CUSTOMER.Name = "lblTOTAL_CUSTOMER";
            // 
            // lblTotalCus_
            // 
            resources.ApplyResources(this.lblTotalCus_, "lblTotalCus_");
            this.lblTotalCus_.Name = "lblTotalCus_";
            // 
            // lblEXPIRE_DATE
            // 
            resources.ApplyResources(this.lblEXPIRE_DATE, "lblEXPIRE_DATE");
            this.lblEXPIRE_DATE.Name = "lblEXPIRE_DATE";
            // 
            // txtDueDate_
            // 
            resources.ApplyResources(this.txtDueDate_, "txtDueDate_");
            this.txtDueDate_.Name = "txtDueDate_";
            // 
            // lblACTIVATE_MONTH
            // 
            resources.ApplyResources(this.lblACTIVATE_MONTH, "lblACTIVATE_MONTH");
            this.lblACTIVATE_MONTH.Name = "lblACTIVATE_MONTH";
            // 
            // txtMonth_
            // 
            resources.ApplyResources(this.txtMonth_, "txtMonth_");
            this.txtMonth_.Name = "txtMonth_";
            // 
            // lblACTIVATE_DATE
            // 
            resources.ApplyResources(this.lblACTIVATE_DATE, "lblACTIVATE_DATE");
            this.lblACTIVATE_DATE.Name = "lblACTIVATE_DATE";
            // 
            // txtActivateDate_
            // 
            resources.ApplyResources(this.txtActivateDate_, "txtActivateDate_");
            this.txtActivateDate_.Name = "txtActivateDate_";
            // 
            // pbSync
            // 
            this.pbSync.BackColor = System.Drawing.Color.Transparent;
            this.pbSync.Image = global::EPower.Properties.Resources.Sync;
            resources.ApplyResources(this.pbSync, "pbSync");
            this.pbSync.Name = "pbSync";
            this.pbSync.TabStop = false;
            // 
            // lblVersioning_
            // 
            resources.ApplyResources(this.lblVersioning_, "lblVersioning_");
            this.lblVersioning_.Name = "lblVersioning_";
            // 
            // btnUPDATE_
            // 
            resources.ApplyResources(this.btnUPDATE_, "btnUPDATE_");
            this.btnUPDATE_.LinkColor = System.Drawing.Color.Blue;
            this.btnUPDATE_.Name = "btnUPDATE_";
            this.btnUPDATE_.TabStop = true;
            this.btnUPDATE_.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnInstall_LinkClicked);
            // 
            // DialogAboutEPower
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogAboutEPower";
            this.Load += new System.EventHandler(this.DialogAboutEPower_Load);
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picICON_APP)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabINFORMATION.ResumeLayout(false);
            this.tabINFORMATION.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picIe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFb)).EndInit();
            this.tabMAINTENANCE.ResumeLayout(false);
            this.tabMAINTENANCE.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSync)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnOK;
        private PictureBox picICON_APP;
        private TabControl tabControl1;
        private TabPage tabINFORMATION;
        private TabPage tabMAINTENANCE;
        private Label lblPROGRAM;
        private Label lblVERSION;
        private Label lblCOPY_RIGHT;
        private Label lblPHONE_CONTACT;
        private Label lblPN_;
        private Label lblCOMPANY;
        private Label lblV_;
        private Label lblCopy_;
        private Label lblCom_;
        private Label lblPhone_;
        private Label lblEXPIRE_DATE;
        private Label txtDueDate_;
        private Label lblACTIVATE_MONTH;
        private Label txtMonth_;
        private Label lblACTIVATE_DATE;
        private Label txtActivateDate_;
        private TextBox txtSerial;
        private ExButton btnCONTINUE_LICENSE;
        private Label lblTOTAL_CUSTOMER;
        private Label lblTotalCus_;
        private Label lblVersioning_;
        private PictureBox pbSync;
        private ExLinkLabel btnUPDATE_;
        private PictureBox picIe;
        private PictureBox picFb;
        private ExLinkLabel lblLINK_EPOWER_;
        private ToolTip toolTipLink;
    }
}