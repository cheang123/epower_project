﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogCustomerBlockOption : ExDialog
    {
        public DialogCustomerBlockOption()
        {
            InitializeComponent();
            bind();
        }

        #region Method
        private void bind()
        {
            UIHelper.SetDataSourceToComboBox(cboCustomerGroup, Lookup.GetCustomerGroup(), Resources.All_CUSTOMER_GROUP);
            UIHelper.SetDataSourceToComboBox(cboArea, Lookup.GetAreas(), Resources.ALL_AREA);
            UIHelper.SetDataSourceToComboBox(cboBilling, Lookup.GetBillingCycles(), Resources.ALL_CYCLE);
            UIHelper.SetDataSourceToComboBox(cboCurrency, Lookup.GetCurrencies(), Resources.ALL_CURRENCY);

            txtDayStart.Items.Add(Resources.UNLIMITED);
            txtDayStart.SelectedIndex = 0;
            txtDayEnd.Items.Add(Resources.UNLIMITED);
            txtDayEnd.SelectedIndex = 0;
        }
        #endregion

        private void cboCustomerGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            int groupId = DataHelper.ParseToInt(cboCustomerGroup.SelectedValue.ToString());
            var dt = DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs
                    .OrderBy(x => x.DESCRIPTION)
                    .Where(x => x.NONLICENSE_CUSTOMER_GROUP_ID == groupId);
            if (cboCustomerGroup.SelectedIndex != -1)
            {
                UIHelper.SetDataSourceToComboBox(cboCustomerConnectionType, dt, Resources.All_CUSTOMER_CUSTOMER_TYPE);
                if (cboCustomerConnectionType.DataSource != null)
                {
                    cboCustomerConnectionType.SelectedIndex = 0;
                }
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
