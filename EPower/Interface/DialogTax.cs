﻿using DevExpress.XtraEditors.Controls;
using EPower.Base.Helper;
using EPower.Base.Helper.DevExpressCustomize;
using EPower.Base.Logic;
using EPower.Base.Properties;
using HB01.Domain.ListModels;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Collections.Generic;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogTax : ExDialog
    {
        #region Data
        GeneralProcess _flag = GeneralProcess.Insert;
        TBL_TAX _objOld = new TBL_TAX();
        TBL_TAX _objNew = new TBL_TAX();
        List<Base.Logic.Currency> currencies = new List<Base.Logic.Currency>();
        List<PaymentMethodAccountList> lst = new List<PaymentMethodAccountList>();

        #endregion Data

        #region Constructor

        public DialogTax(TBL_TAX obj, GeneralProcess flag)
        {
            InitializeComponent();
            this._flag = flag;
            currencies = SettingLogic.Currencies;
            obj._CopyTo(this._objNew);
            obj._CopyTo(this._objOld);
            this.bind();
            this.read();
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
            if (flag == GeneralProcess.Delete)
            {
                UIHelper.SetEnabled(this, false);
                this.cboAccount.Enabled = false;
                this.cboComputation.Enabled = false;
            }
            this.btnCLOSE.Click += btnClose_Click;
            this.btnOK.Click += btnOK_Click;
            this.btnCHANGE_LOG.Click += btnChangelog_Click;
            this.txtAmount.KeyPress += txtAmount_KeyPress;
        }


        #endregion Constructor

        #region Method

        public bool invalid()
        {
            bool val = false;
            this.ClearAllValidation();

            if (string.IsNullOrEmpty(this.cboAccount.Text.Trim()))
            {
                this.cboAccount.SetValidation(string.Format(Resources.REQUIRED_SELECT_VALUE, this.lblTAX_EXPENSE_ACCOUNT.Text));
                val = true;
            }

            if (string.IsNullOrEmpty(this.txtAmount.Text.Trim()))
            {
                this.txtAmount.SetValidation(string.Format(Resources.REQUIRED, this.lblTAX_AMOUNT.Text));
                this.txtAmount.Focus();
                val = true;
            }

            if (string.IsNullOrEmpty(this.cboComputation.Text.Trim()))
            {
                cboComputation.SetValidation(string.Format(Resources.REQUIRED_SELECT_VALUE, this.lblCOMPUTATION.Text));
                val = true;
            }

            if (string.IsNullOrEmpty(this.txtTaxName.Text.Trim()))
            {
                this.txtTaxName.SetValidation(string.Format(Resources.REQUIRED, this.lblTAX_NAME.Text));
                this.txtTaxName.Focus();
                val = true;
            }

            return val;
        }

        private void bind()
        {
            cboAccount.SetDatasourceList(AccountChartHelper.GetAccounts(new AccountSearchParam { }), nameof(AccountListModel.DisplayAccountName));
            cboComputation.SetEnumSource(typeof(TaxComputations));
            foreach (LookUpColumnInfo item in cboAccount.Properties.Columns)
            {
                item.Caption = ResourceHelper.Translate(item.Caption);
            }
            foreach (LookUpColumnInfo item in cboComputation.Properties.Columns)
            {
                item.Caption = ResourceHelper.Translate(item.Caption);
            }
        }

        public void read()
        {
            this.txtTaxName.Text = _objOld.TAX_NAME;
            this.cboComputation.EditValue = _objOld.COMPUTATION;
            this.txtAmount.Text = _objOld.TAX_AMOUNT.ToString(UIHelper._DefaultDecimal2);
            this.cboAccount.EditValue = _objOld.ACCOUNT_ID;
        }

        public void write()
        {
            this._objNew.TAX_NAME = this.txtTaxName.Text;
            this._objNew.COMPUTATION = (int)this.cboComputation.EditValue;
            this._objNew.TAX_AMOUNT = DataHelper.ParseToDecimal(txtAmount.Text);
            this._objNew.ACCOUNT_ID = (int)this.cboAccount.EditValue;
        }

        #endregion Method

        #region Event

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                btnOK.Focus();
                if (this.invalid())
                {
                    return;
                }

                this.write();
                //txt.ClearValidation();
                //if (DBDataContext.Db.IsExits(this._objNew, "PAYMENT_METHOD_NAME"))
                //{
                //    txtPaymentMethod.SetValidation(string.Format(Resources.MS_IS_EXISTS, this.lblNAME.Text));
                //    return;
                //}

                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (this._flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(this._objNew);
                    }
                    else if (this._flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(this._objOld, this._objNew);
                    }
                    else if (this._flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(this._objNew);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp);
            }
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }

        private void txtAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        }

        #endregion Event
    }
}
