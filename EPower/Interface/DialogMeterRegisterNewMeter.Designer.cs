﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogMeterRegisterNewMeter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogMeterRegisterNewMeter));
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.METER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.METER_TYPE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.METER_TYPE_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.METER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cboMeterType = new System.Windows.Forms.ComboBox();
            this.lblMeterType = new System.Windows.Forms.Label();
            this.btnREGISTER = new SoftTech.Component.ExButton();
            this.lblCOUNT = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.lblCOUNT);
            this.content.Controls.Add(this.btnREGISTER);
            this.content.Controls.Add(this.label2);
            this.content.Controls.Add(this.cboMeterType);
            this.content.Controls.Add(this.lblMeterType);
            this.content.Controls.Add(this.label4);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.dgv);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.dgv, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.label4, 0);
            this.content.Controls.SetChildIndex(this.lblMeterType, 0);
            this.content.Controls.SetChildIndex(this.cboMeterType, 0);
            this.content.Controls.SetChildIndex(this.label2, 0);
            this.content.Controls.SetChildIndex(this.btnREGISTER, 0);
            this.content.Controls.SetChildIndex(this.lblCOUNT, 0);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.METER_ID,
            this.METER_TYPE_ID,
            this.METER_TYPE_NAME,
            this.METER_CODE});
            this.dgv.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // METER_ID
            // 
            this.METER_ID.DataPropertyName = "METER_ID";
            resources.ApplyResources(this.METER_ID, "METER_ID");
            this.METER_ID.Name = "METER_ID";
            this.METER_ID.ReadOnly = true;
            // 
            // METER_TYPE_ID
            // 
            this.METER_TYPE_ID.DataPropertyName = "METER_TYPE_ID";
            resources.ApplyResources(this.METER_TYPE_ID, "METER_TYPE_ID");
            this.METER_TYPE_ID.Name = "METER_TYPE_ID";
            this.METER_TYPE_ID.ReadOnly = true;
            // 
            // METER_TYPE_NAME
            // 
            this.METER_TYPE_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.METER_TYPE_NAME.DataPropertyName = "METER_TYPE_NAME";
            resources.ApplyResources(this.METER_TYPE_NAME, "METER_TYPE_NAME");
            this.METER_TYPE_NAME.Name = "METER_TYPE_NAME";
            this.METER_TYPE_NAME.ReadOnly = true;
            this.METER_TYPE_NAME.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // METER_CODE
            // 
            this.METER_CODE.DataPropertyName = "METER_CODE";
            resources.ApplyResources(this.METER_CODE, "METER_CODE");
            this.METER_CODE.Name = "METER_CODE";
            this.METER_CODE.ReadOnly = true;
            this.METER_CODE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Name = "label2";
            // 
            // cboMeterType
            // 
            this.cboMeterType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMeterType.FormattingEnabled = true;
            resources.ApplyResources(this.cboMeterType, "cboMeterType");
            this.cboMeterType.Name = "cboMeterType";
            this.cboMeterType.SelectedIndexChanged += new System.EventHandler(this.cboMeterType_SelectedIndexChanged);
            // 
            // lblMeterType
            // 
            resources.ApplyResources(this.lblMeterType, "lblMeterType");
            this.lblMeterType.Name = "lblMeterType";
            // 
            // btnREGISTER
            // 
            resources.ApplyResources(this.btnREGISTER, "btnREGISTER");
            this.btnREGISTER.Name = "btnREGISTER";
            this.btnREGISTER.UseVisualStyleBackColor = true;
            this.btnREGISTER.Click += new System.EventHandler(this.btnRegister_Click);
            // 
            // lblCOUNT
            // 
            resources.ApplyResources(this.lblCOUNT, "lblCOUNT");
            this.lblCOUNT.Name = "lblCOUNT";
            // 
            // DialogMeterRegisterNewMeter
            // 
            resources.ApplyResources(this, "$this");
            this.Name = "DialogMeterRegisterNewMeter";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnCLOSE;
        private DataGridView dgv;
        private Label label4;
        private Label label2;
        private ComboBox cboMeterType;
        private Label lblMeterType;
        private ExButton btnREGISTER;
        private Label lblCOUNT;
        private DataGridViewTextBoxColumn METER_ID;
        private DataGridViewTextBoxColumn METER_TYPE_ID;
        private DataGridViewTextBoxColumn METER_TYPE_NAME;
        private DataGridViewTextBoxColumn METER_CODE;
    }
}
