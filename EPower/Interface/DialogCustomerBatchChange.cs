﻿using System;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class DialogCustomerBatchChange : ExDialog
    { 
        public DialogCustomerBatchChange()
        {
            InitializeComponent();

            UIHelper.SetDataSourceToComboBox(this.cboArea, DBDataContext.Db.TBL_AREAs.Where(row => row.IS_ACTIVE).OrderBy(row => row.AREA_CODE), Resources.ALL_AREA);
            UIHelper.SetDataSourceToComboBox(this.cboPrice, DBDataContext.Db.TBL_PRICEs.Where(row => row.IS_ACTIVE),"");
            UIHelper.SetDataSourceToComboBox(this.cboCycle, DBDataContext.Db.TBL_BILLING_CYCLEs.Where(row => row.IS_ACTIVE), "");
            this.cboPrice.SelectedIndex = 0;
            this.cboCycle.SelectedIndex = 0;
        }
          
        private void btnAdd_Click(object sender, EventArgs e)
        {
            this.Close();
        }
          
        private void cboArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            int areaId=(int)this.cboArea.SelectedValue;
            UIHelper.SetDataSourceToComboBox(cboPole, DBDataContext.Db.TBL_POLEs.Where(row => row.AREA_ID  == areaId && row.IS_ACTIVE).Select(row=>new {row.POLE_ID,row.POLE_CODE}).OrderBy(row => row.POLE_CODE), Resources.ALL_POLE);
            calCustomer();
        }

        private void cboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            calCustomer();
        }
       
        private void btnOK_Click(object sender, EventArgs e)
        {
            int priceId = (int)this.cboPrice.SelectedValue;
            int cycleId = (int)this.cboCycle.SelectedValue;
            if (priceId == 0 && cycleId == 0 && villageCode == "")
            {
                return;
            }
            success = false;
            Runner.Run(updateCustomerInfo);
            if (success)
            {
                MsgBox.ShowInformation(Resources.SUCCESS,"");
            }
        }
        bool success = false;
        private void updateCustomerInfo()
        {
            int areaId = (int)cboArea.SelectedValue;
            int poleId = cboPole.SelectedIndex == -1?0:(int)cboPole.SelectedValue;
            int boxId =  cboBox.SelectedIndex==-1?0:(int)cboBox.SelectedValue;
            var qry = from c in DBDataContext.Db.TBL_CUSTOMERs
                      join m in DBDataContext.Db.TBL_CUSTOMER_METERs.Where(row => row.IS_ACTIVE) on c.CUSTOMER_ID equals m.CUSTOMER_ID
                      join b in DBDataContext.Db.TBL_BOXes on m.BOX_ID equals b.BOX_ID
                      join p in DBDataContext.Db.TBL_POLEs on b.POLE_ID equals p.POLE_ID
                      where (areaId == 0 || c.AREA_ID == areaId)
                           && (poleId == 0 || p.POLE_ID == poleId)
                           && (boxId == 0 || b.BOX_ID == boxId)
                      select c;
            int priceId = (int)this.cboPrice.SelectedValue;
            int cycleId = (int)this.cboCycle.SelectedValue;
            foreach (var c in qry)
            {
                if (priceId != 0) c.PRICE_ID = priceId;
                if (cycleId != 0) c.BILLING_CYCLE_ID = cycleId;
                if (villageCode != "")
                {
                    c.VILLAGE_CODE = villageCode;
                    c.ADDRESS = this.txtVillage.Text;
                }
                DBDataContext.Db.SubmitChanges();
                Application.DoEvents();
            }
            success = true;
        }

        private void calCustomer()
        {
            int areaId = (int)cboArea.SelectedValue;
            int poleId = cboPole.SelectedIndex == -1 ? 0 : (int)cboPole.SelectedValue;
            int boxId = cboBox.SelectedIndex == -1 ? 0 : (int)cboBox.SelectedValue;
            this.lblCustomer.Text =(from c in DBDataContext.Db.TBL_CUSTOMERs
                                    join m in DBDataContext.Db.TBL_CUSTOMER_METERs.Where(row => row.IS_ACTIVE) on c.CUSTOMER_ID equals m.CUSTOMER_ID
                                    join b in DBDataContext.Db.TBL_BOXes on m.BOX_ID equals b.BOX_ID
                                    join p in DBDataContext.Db.TBL_POLEs on b.POLE_ID equals p.POLE_ID
                                    where (areaId == 0 || c.AREA_ID == areaId)
                                       && (poleId == 0 || p.POLE_ID == poleId)
                                       && (boxId == 0 || b.BOX_ID == boxId)
                                    select c.CUSTOMER_ID).Count().ToString();
        }
        string villageCode = "";
        private void btnChooseVillage_Click(object sender, EventArgs e)
        {
            if (villageCode == "")
            {
                DialogChooseVillage diag = new DialogChooseVillage();
                if (diag.ShowDialog() == DialogResult.OK)
                {
                    this.txtVillage.Text = diag.lblVillage.Text;
                    villageCode = diag.cboVillage.SelectedValue.ToString();
                    this.btnChooseVillage.Text = "X";
                }
            }
            else
            {
                this.villageCode = "";
                this.txtVillage.Text = "";
                this.btnChooseVillage.Text = "...";
            }
        }

        private void cboPole_SelectedIndexChanged(object sender, EventArgs e)
        {
            int poleId = (int)this.cboPole.SelectedValue;
            UIHelper.SetDataSourceToComboBox(cboBox, DBDataContext.Db.TBL_BOXes.Where(row => row.POLE_ID == poleId).Select(row => new { row.BOX_ID, row.BOX_CODE }).OrderBy(row => row.BOX_CODE), Resources.ALL_BOX);
            calCustomer();
        } 
    }
}