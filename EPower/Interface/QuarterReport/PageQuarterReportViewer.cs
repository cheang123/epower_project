﻿using EPower.Base.Logic;
using EPower.Interface.OPSReport;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageQuarterReportViewer : Form
    {
        CrystalReportHelper ch = null;
        DialogReportQuarterOptions diagOptions = new DialogReportQuarterOptions(true);

        #region Constructor

        public PageQuarterReportViewer()
        {
            InitializeComponent();
            viewer.DefaultView();

            this.FormBorderStyle = FormBorderStyle.None;
            this.Dock = DockStyle.Fill;   
            ResourceHelper.ApplyResource(this);

            // BIND DATA
            UIHelper.SetDataSourceToComboBox(cboYear, PageOPSReportSetup.DataYear());
            UIHelper.SetDataSourceToComboBox(cboQuarter, PageOPSReportSetup.DataQuarter());

            // SET DEFAUL VALUE
            var now = DBDataContext.Db.GetSystemDate();
            cboYear.SelectedValue = now.Year;
            cboQuarter.SelectedValue = ((int)((now.Month - 1) / 3) + 1);
        }
        
        #endregion

        #region Method

        private void viewReport()
        {
            this.ch = PageReportQuaterly.OpenReport(diagOptions);
            ch.SetParameter("@DATE", new DateTime((int)cboYear.SelectedValue, 1, 1).AddMonths(((int)cboQuarter.SelectedValue - 1) * 3));
            this.viewer.ReportSource = ch.Report;
        }

        private void sendMail()
        {
            try
            {
                if (ch == null)
                {
                    this.viewReport();
                }
                TBL_COMPANY com = null;
                com = DBDataContext.Db.TBL_COMPANies.FirstOrDefault();
                string licensename = com.LICENSE_NUMBER + " - " + com.COMPANY_NAME;
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + this.ch.ReportName + ".pdf").FullName;
                Runner.RunNewThread(delegate () { this.ch.ExportToPDF(exportPath); });
                DialogSendMail diag = new DialogSendMail();
                diag.chkMERG_FILE.Checked = false;
                diag.chkMERG_FILE.Visible = false;
                diag.txtTo.Text = "eackh.report@gmail.com";
                diag.txtSubject.Text = licensename + " - " + Resources.QUARTER_REPORT + Resources.QUARTER_NO + cboQuarter.SelectedValue.ToString() + " " + Resources.YEAR + cboYear.SelectedValue;
                diag.Add(this.ch.ReportName, exportPath);
                diag.ShowDialog();
            }
            catch(Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_SEND_EMAIL, Base.Properties.Resources.WARNING);
            }
        }

        #endregion

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.cboYear.SelectedIndex == -1)
                {
                    MsgBox.ShowInformation(Resources.MS_SELECT_YEAR);
                    this.cboYear.Focus();
                    return;
                }
                Runner.Run(this.viewReport);
            }
            catch (Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_VIEW_REPORT, Base.Properties.Resources.WARNING);
            }          
        } 
       
        private void btnSetup_Click(object sender, EventArgs e)
        {
            diagOptions.cboYear.SelectedValue = this.cboYear.SelectedValue;
            diagOptions.cboQuarter.Text = this.cboQuarter.Text;
            if (diagOptions.ShowDialog() == DialogResult.OK)
            {
                this.cboYear.SelectedValue = diagOptions.cboYear.SelectedValue;
                this.cboQuarter.Text = diagOptions.cboQuarter.Text; 
                viewReport();
            }
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            this.btnView_Click(sender, EventArgs.Empty);
            this.sendMail();
        }
    }
}
