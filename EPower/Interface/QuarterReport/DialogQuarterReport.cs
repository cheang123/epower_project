﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class DialogQuarterReport : ExDialog
    {
        public DialogQuarterReport()
        {
            InitializeComponent();

            // resize screen
            WindowState = FormWindowState.Normal;
            StartPosition = FormStartPosition.Manual;
            Top = 0;
            Left = 0;
            Height = Screen.GetWorkingArea(this).Height;
            Width = Screen.GetWorkingArea(this).Width; 

            var root = new TreeNode
            {
                Tag = "",
                Text = Text,
                ImageKey = "FOLDER",
                SelectedImageKey = "FOLDER"
            };

            this.tvw.Nodes.Add(root);

            foreach (var obj1 in DBDataContext.Db.TBL_TREE_QUARTERs.OrderBy(x => x.TABLE_CODE).Where(x=>x.PARENT_ID==0))
            {
                var n1 = new TreeNode
                {
                    Tag = obj1,
                    Text = obj1.TABLE_NAME,
                    ImageKey = obj1.ICON_KEY,
                    SelectedImageKey = obj1.ICON_KEY
                };
                root.Nodes.Add(n1); 
            }
            root.Expand();

        }
        
        private void tvw_AfterSelect(object sender, TreeViewEventArgs e)
        {
            var obj = e.Node.Tag as TBL_TREE_QUARTER;
            if (obj == null)
            {
                return;
            } 
            if (string.IsNullOrEmpty(obj.CLASS))
            {
                return;
            }
            if (obj.CLASS.ToLower().Contains("dialog"))
            {
                var type = Type.GetType(obj.CLASS);
                var diag = Activator.CreateInstance(type) as Form;
                diag.ShowDialog(); 
            }
            else
            {
                var type = Type.GetType(obj.CLASS);
                showPage(type,e.Node); 
            }
        }


        #region showPage
        private Control currentPage = null;
        private Dictionary<Type, Control> pages = new Dictionary<Type, Control>();
        private void showPage(Type type, object sender)
        {
            if (!this.pages.ContainsKey(type))
            {
                this.pages[type] = (Control)Activator.CreateInstance(type);
                this.pages[type].Size = this.panelContainer.Size;
                if (this.pages[type] is Form)
                {
                    Form frm = (Form)this.pages[type];
                    frm.TopLevel = false;
                    frm.Visible = true;
                    frm.FormBorderStyle = FormBorderStyle.None;
                }
                this.panelContainer.Controls.Add(this.pages[type]);
                this.pages[type].Dock = DockStyle.Fill;
                ResourceHelper.ApplyResource(this.pages[type]);
            }
            if (this.currentPage != this.pages[type])
            {
                this.pages[type].Show();
                if (this.currentPage != null)
                {
                    this.currentPage.Hide();
                }
                this.currentPage = this.pages[type];
            }
            //this.title.Text = ((Control)sender).Text;
        }
        #endregion ShowPage()
    }
}
