﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogCollectUsageManually
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCollectUsageManually));
            this.lblCUSTOMER_1 = new System.Windows.Forms.Label();
            this.lblMETER_CODE = new System.Windows.Forms.Label();
            this.lblCUSTOMER_CODE = new System.Windows.Forms.Label();
            this.lblCUSTOMER_NAME = new System.Windows.Forms.Label();
            this.lblCUSTOMER_USAGE = new System.Windows.Forms.Label();
            this.txtMeter = new SoftTech.Component.ExTextbox();
            this.txtCustomerCode = new SoftTech.Component.ExTextbox();
            this.txtCustomerName = new SoftTech.Component.ExTextbox();
            this.lblAREA = new System.Windows.Forms.Label();
            this.lblPOLE = new System.Windows.Forms.Label();
            this.lblBOX = new System.Windows.Forms.Label();
            this.txtArea = new System.Windows.Forms.TextBox();
            this.txtPole = new System.Windows.Forms.TextBox();
            this.txtBox = new System.Windows.Forms.TextBox();
            this.lblCYCLE_NAME = new System.Windows.Forms.Label();
            this.lblMONTH = new System.Windows.Forms.Label();
            this.cboCycle = new System.Windows.Forms.ComboBox();
            this.dtpMonth = new System.Windows.Forms.DateTimePicker();
            this.dtpStart = new System.Windows.Forms.DateTimePicker();
            this.dtpEnd = new System.Windows.Forms.DateTimePicker();
            this.lblSTART_DATE = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.lblCOLLECTOR = new System.Windows.Forms.Label();
            this.cboCollector = new System.Windows.Forms.ComboBox();
            this.lblMETER_CODE_1 = new System.Windows.Forms.Label();
            this.cboMeter = new System.Windows.Forms.ComboBox();
            this.lblSTART_USAGE = new System.Windows.Forms.Label();
            this.lblEND_USAGE = new System.Windows.Forms.Label();
            this.txtStartUsage = new System.Windows.Forms.TextBox();
            this.txtEndUsage = new System.Windows.Forms.TextBox();
            this.txtTotalUsage = new System.Windows.Forms.TextBox();
            this.lblUSAGE = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnSAVE = new SoftTech.Component.ExButton();
            this.chkIS_NEW_CYCLE = new System.Windows.Forms.CheckBox();
            this.lblReader_ = new System.Windows.Forms.Label();
            this.btnCUSTOMER_NO_USAGE = new SoftTech.Component.ExButton();
            this.btnCHECK_USAGE = new SoftTech.Component.ExButton();
            this.lblMULTIPLIER = new System.Windows.Forms.Label();
            this.txtMultiplier = new System.Windows.Forms.TextBox();
            this.chkAUTO_MOVE_TO_NEXT_CUSTOMER = new System.Windows.Forms.CheckBox();
            this.lbl12_MONTHS_USAGE_HISTORY0 = new System.Windows.Forms.Label();
            this.grp12_MONTHS_USAGE_HISTORY = new System.Windows.Forms.GroupBox();
            this.chk12_MONTHS_USAGE_HISTORY = new System.Windows.Forms.CheckBox();
            this.btnUSAGE_HISTORY = new SoftTech.Component.ExButton();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.grp12_MONTHS_USAGE_HISTORY.SuspendLayout();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.btnUSAGE_HISTORY);
            this.content.Controls.Add(this.chk12_MONTHS_USAGE_HISTORY);
            this.content.Controls.Add(this.grp12_MONTHS_USAGE_HISTORY);
            this.content.Controls.Add(this.chkAUTO_MOVE_TO_NEXT_CUSTOMER);
            this.content.Controls.Add(this.txtMultiplier);
            this.content.Controls.Add(this.lblMULTIPLIER);
            this.content.Controls.Add(this.btnCHECK_USAGE);
            this.content.Controls.Add(this.btnCUSTOMER_NO_USAGE);
            this.content.Controls.Add(this.lblReader_);
            this.content.Controls.Add(this.chkIS_NEW_CYCLE);
            this.content.Controls.Add(this.panel2);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnSAVE);
            this.content.Controls.Add(this.txtTotalUsage);
            this.content.Controls.Add(this.lblUSAGE);
            this.content.Controls.Add(this.txtEndUsage);
            this.content.Controls.Add(this.txtStartUsage);
            this.content.Controls.Add(this.lblEND_USAGE);
            this.content.Controls.Add(this.lblSTART_USAGE);
            this.content.Controls.Add(this.cboMeter);
            this.content.Controls.Add(this.lblMETER_CODE_1);
            this.content.Controls.Add(this.cboCollector);
            this.content.Controls.Add(this.lblCOLLECTOR);
            this.content.Controls.Add(this.label21);
            this.content.Controls.Add(this.lblSTART_DATE);
            this.content.Controls.Add(this.dtpEnd);
            this.content.Controls.Add(this.dtpStart);
            this.content.Controls.Add(this.dtpMonth);
            this.content.Controls.Add(this.cboCycle);
            this.content.Controls.Add(this.lblMONTH);
            this.content.Controls.Add(this.lblCYCLE_NAME);
            this.content.Controls.Add(this.txtBox);
            this.content.Controls.Add(this.txtPole);
            this.content.Controls.Add(this.txtArea);
            this.content.Controls.Add(this.lblBOX);
            this.content.Controls.Add(this.lblPOLE);
            this.content.Controls.Add(this.lblAREA);
            this.content.Controls.Add(this.txtCustomerName);
            this.content.Controls.Add(this.txtCustomerCode);
            this.content.Controls.Add(this.txtMeter);
            this.content.Controls.Add(this.lblCUSTOMER_USAGE);
            this.content.Controls.Add(this.lblCUSTOMER_NAME);
            this.content.Controls.Add(this.lblCUSTOMER_CODE);
            this.content.Controls.Add(this.lblCUSTOMER_1);
            this.content.Controls.Add(this.lblMETER_CODE);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblMETER_CODE, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_1, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_CODE, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_USAGE, 0);
            this.content.Controls.SetChildIndex(this.txtMeter, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerCode, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerName, 0);
            this.content.Controls.SetChildIndex(this.lblAREA, 0);
            this.content.Controls.SetChildIndex(this.lblPOLE, 0);
            this.content.Controls.SetChildIndex(this.lblBOX, 0);
            this.content.Controls.SetChildIndex(this.txtArea, 0);
            this.content.Controls.SetChildIndex(this.txtPole, 0);
            this.content.Controls.SetChildIndex(this.txtBox, 0);
            this.content.Controls.SetChildIndex(this.lblCYCLE_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblMONTH, 0);
            this.content.Controls.SetChildIndex(this.cboCycle, 0);
            this.content.Controls.SetChildIndex(this.dtpMonth, 0);
            this.content.Controls.SetChildIndex(this.dtpStart, 0);
            this.content.Controls.SetChildIndex(this.dtpEnd, 0);
            this.content.Controls.SetChildIndex(this.lblSTART_DATE, 0);
            this.content.Controls.SetChildIndex(this.label21, 0);
            this.content.Controls.SetChildIndex(this.lblCOLLECTOR, 0);
            this.content.Controls.SetChildIndex(this.cboCollector, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_CODE_1, 0);
            this.content.Controls.SetChildIndex(this.cboMeter, 0);
            this.content.Controls.SetChildIndex(this.lblSTART_USAGE, 0);
            this.content.Controls.SetChildIndex(this.lblEND_USAGE, 0);
            this.content.Controls.SetChildIndex(this.txtStartUsage, 0);
            this.content.Controls.SetChildIndex(this.txtEndUsage, 0);
            this.content.Controls.SetChildIndex(this.lblUSAGE, 0);
            this.content.Controls.SetChildIndex(this.txtTotalUsage, 0);
            this.content.Controls.SetChildIndex(this.btnSAVE, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel2, 0);
            this.content.Controls.SetChildIndex(this.chkIS_NEW_CYCLE, 0);
            this.content.Controls.SetChildIndex(this.lblReader_, 0);
            this.content.Controls.SetChildIndex(this.btnCUSTOMER_NO_USAGE, 0);
            this.content.Controls.SetChildIndex(this.btnCHECK_USAGE, 0);
            this.content.Controls.SetChildIndex(this.lblMULTIPLIER, 0);
            this.content.Controls.SetChildIndex(this.txtMultiplier, 0);
            this.content.Controls.SetChildIndex(this.chkAUTO_MOVE_TO_NEXT_CUSTOMER, 0);
            this.content.Controls.SetChildIndex(this.grp12_MONTHS_USAGE_HISTORY, 0);
            this.content.Controls.SetChildIndex(this.chk12_MONTHS_USAGE_HISTORY, 0);
            this.content.Controls.SetChildIndex(this.btnUSAGE_HISTORY, 0);
            // 
            // lblCUSTOMER_1
            // 
            resources.ApplyResources(this.lblCUSTOMER_1, "lblCUSTOMER_1");
            this.lblCUSTOMER_1.Name = "lblCUSTOMER_1";
            // 
            // lblMETER_CODE
            // 
            resources.ApplyResources(this.lblMETER_CODE, "lblMETER_CODE");
            this.lblMETER_CODE.Name = "lblMETER_CODE";
            // 
            // lblCUSTOMER_CODE
            // 
            resources.ApplyResources(this.lblCUSTOMER_CODE, "lblCUSTOMER_CODE");
            this.lblCUSTOMER_CODE.Name = "lblCUSTOMER_CODE";
            // 
            // lblCUSTOMER_NAME
            // 
            resources.ApplyResources(this.lblCUSTOMER_NAME, "lblCUSTOMER_NAME");
            this.lblCUSTOMER_NAME.Name = "lblCUSTOMER_NAME";
            // 
            // lblCUSTOMER_USAGE
            // 
            resources.ApplyResources(this.lblCUSTOMER_USAGE, "lblCUSTOMER_USAGE");
            this.lblCUSTOMER_USAGE.Name = "lblCUSTOMER_USAGE";
            // 
            // txtMeter
            // 
            this.txtMeter.BackColor = System.Drawing.Color.White;
            this.txtMeter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtMeter, "txtMeter");
            this.txtMeter.Name = "txtMeter";
            this.txtMeter.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtMeter.AdvanceSearch += new System.EventHandler(this.txtMeter_AdvanceSearch);
            this.txtMeter.CancelAdvanceSearch += new System.EventHandler(this.txtMeter_CancelAdvanceSearch);
            this.txtMeter.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // txtCustomerCode
            // 
            this.txtCustomerCode.BackColor = System.Drawing.Color.White;
            this.txtCustomerCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtCustomerCode, "txtCustomerCode");
            this.txtCustomerCode.Name = "txtCustomerCode";
            this.txtCustomerCode.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtCustomerCode.AdvanceSearch += new System.EventHandler(this.txtCustomerCode_AdvanceSearch);
            this.txtCustomerCode.CancelAdvanceSearch += new System.EventHandler(this.txtCustomerCode_CancelAdvanceSearch);
            this.txtCustomerCode.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.BackColor = System.Drawing.Color.White;
            this.txtCustomerName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtCustomerName, "txtCustomerName");
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtCustomerName.AdvanceSearch += new System.EventHandler(this.txtCustomerName_AdvanceSearch);
            this.txtCustomerName.CancelAdvanceSearch += new System.EventHandler(this.txtCustomerName_CancelAdvanceSearch);
            this.txtCustomerName.Enter += new System.EventHandler(this.InputKhmer);
            // 
            // lblAREA
            // 
            resources.ApplyResources(this.lblAREA, "lblAREA");
            this.lblAREA.Name = "lblAREA";
            // 
            // lblPOLE
            // 
            resources.ApplyResources(this.lblPOLE, "lblPOLE");
            this.lblPOLE.Name = "lblPOLE";
            // 
            // lblBOX
            // 
            resources.ApplyResources(this.lblBOX, "lblBOX");
            this.lblBOX.Name = "lblBOX";
            // 
            // txtArea
            // 
            resources.ApplyResources(this.txtArea, "txtArea");
            this.txtArea.Name = "txtArea";
            this.txtArea.ReadOnly = true;
            // 
            // txtPole
            // 
            resources.ApplyResources(this.txtPole, "txtPole");
            this.txtPole.Name = "txtPole";
            this.txtPole.ReadOnly = true;
            // 
            // txtBox
            // 
            resources.ApplyResources(this.txtBox, "txtBox");
            this.txtBox.Name = "txtBox";
            this.txtBox.ReadOnly = true;
            // 
            // lblCYCLE_NAME
            // 
            resources.ApplyResources(this.lblCYCLE_NAME, "lblCYCLE_NAME");
            this.lblCYCLE_NAME.Name = "lblCYCLE_NAME";
            // 
            // lblMONTH
            // 
            resources.ApplyResources(this.lblMONTH, "lblMONTH");
            this.lblMONTH.Name = "lblMONTH";
            // 
            // cboCycle
            // 
            this.cboCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
            resources.ApplyResources(this.cboCycle, "cboCycle");
            this.cboCycle.FormattingEnabled = true;
            this.cboCycle.Name = "cboCycle";
            this.cboCycle.Enter += new System.EventHandler(this.InputKhmer);
            // 
            // dtpMonth
            // 
            resources.ApplyResources(this.dtpMonth, "dtpMonth");
            this.dtpMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMonth.Name = "dtpMonth";
            this.dtpMonth.ValueChanged += new System.EventHandler(this.dtpMonth_ValueChanged);
            this.dtpMonth.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // dtpStart
            // 
            resources.ApplyResources(this.dtpStart, "dtpStart");
            this.dtpStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStart.Name = "dtpStart";
            this.dtpStart.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // dtpEnd
            // 
            resources.ApplyResources(this.dtpEnd, "dtpEnd");
            this.dtpEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEnd.Name = "dtpEnd";
            this.dtpEnd.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // lblSTART_DATE
            // 
            resources.ApplyResources(this.lblSTART_DATE, "lblSTART_DATE");
            this.lblSTART_DATE.Name = "lblSTART_DATE";
            // 
            // label21
            // 
            resources.ApplyResources(this.label21, "label21");
            this.label21.Name = "label21";
            // 
            // lblCOLLECTOR
            // 
            resources.ApplyResources(this.lblCOLLECTOR, "lblCOLLECTOR");
            this.lblCOLLECTOR.Name = "lblCOLLECTOR";
            // 
            // cboCollector
            // 
            this.cboCollector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCollector.FormattingEnabled = true;
            resources.ApplyResources(this.cboCollector, "cboCollector");
            this.cboCollector.Name = "cboCollector";
            this.cboCollector.Enter += new System.EventHandler(this.InputKhmer);
            // 
            // lblMETER_CODE_1
            // 
            resources.ApplyResources(this.lblMETER_CODE_1, "lblMETER_CODE_1");
            this.lblMETER_CODE_1.Name = "lblMETER_CODE_1";
            // 
            // cboMeter
            // 
            this.cboMeter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboMeter, "cboMeter");
            this.cboMeter.FormattingEnabled = true;
            this.cboMeter.Name = "cboMeter";
            this.cboMeter.SelectedIndexChanged += new System.EventHandler(this.cboMeter_SelectedIndexChanged);
            this.cboMeter.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // lblSTART_USAGE
            // 
            resources.ApplyResources(this.lblSTART_USAGE, "lblSTART_USAGE");
            this.lblSTART_USAGE.Name = "lblSTART_USAGE";
            // 
            // lblEND_USAGE
            // 
            resources.ApplyResources(this.lblEND_USAGE, "lblEND_USAGE");
            this.lblEND_USAGE.Name = "lblEND_USAGE";
            // 
            // txtStartUsage
            // 
            resources.ApplyResources(this.txtStartUsage, "txtStartUsage");
            this.txtStartUsage.Name = "txtStartUsage";
            this.txtStartUsage.ReadOnly = true;
            this.txtStartUsage.TextChanged += new System.EventHandler(this.txtEndUsage_TextChanged);
            this.txtStartUsage.Enter += new System.EventHandler(this.txtStartUsage_Enter);
            this.txtStartUsage.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtStartUsage_KeyPress);
            // 
            // txtEndUsage
            // 
            resources.ApplyResources(this.txtEndUsage, "txtEndUsage");
            this.txtEndUsage.Name = "txtEndUsage";
            this.txtEndUsage.TextChanged += new System.EventHandler(this.txtEndUsage_TextChanged);
            this.txtEndUsage.Enter += new System.EventHandler(this.txtStartUsage_Enter);
            this.txtEndUsage.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtEndUsage_KeyDown);
            this.txtEndUsage.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEndUsage_KeyPress);
            // 
            // txtTotalUsage
            // 
            resources.ApplyResources(this.txtTotalUsage, "txtTotalUsage");
            this.txtTotalUsage.Name = "txtTotalUsage";
            this.txtTotalUsage.ReadOnly = true;
            this.txtTotalUsage.Enter += new System.EventHandler(this.txtStartUsage_Enter);
            // 
            // lblUSAGE
            // 
            resources.ApplyResources(this.lblUSAGE, "lblUSAGE");
            this.lblUSAGE.Name = "lblUSAGE";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSAVE
            // 
            resources.ApplyResources(this.btnSAVE, "btnSAVE");
            this.btnSAVE.Name = "btnSAVE";
            this.btnSAVE.UseVisualStyleBackColor = true;
            this.btnSAVE.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // chkIS_NEW_CYCLE
            // 
            resources.ApplyResources(this.chkIS_NEW_CYCLE, "chkIS_NEW_CYCLE");
            this.chkIS_NEW_CYCLE.Name = "chkIS_NEW_CYCLE";
            this.chkIS_NEW_CYCLE.UseVisualStyleBackColor = true;
            this.chkIS_NEW_CYCLE.CheckedChanged += new System.EventHandler(this.chkNewMeterCycle_CheckedChanged);
            this.chkIS_NEW_CYCLE.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // lblReader_
            // 
            resources.ApplyResources(this.lblReader_, "lblReader_");
            this.lblReader_.Name = "lblReader_";
            // 
            // btnCUSTOMER_NO_USAGE
            // 
            resources.ApplyResources(this.btnCUSTOMER_NO_USAGE, "btnCUSTOMER_NO_USAGE");
            this.btnCUSTOMER_NO_USAGE.Name = "btnCUSTOMER_NO_USAGE";
            this.btnCUSTOMER_NO_USAGE.UseVisualStyleBackColor = true;
            this.btnCUSTOMER_NO_USAGE.Click += new System.EventHandler(this.btnNoUsage_Click);
            // 
            // btnCHECK_USAGE
            // 
            resources.ApplyResources(this.btnCHECK_USAGE, "btnCHECK_USAGE");
            this.btnCHECK_USAGE.Name = "btnCHECK_USAGE";
            this.btnCHECK_USAGE.UseVisualStyleBackColor = true;
            this.btnCHECK_USAGE.Click += new System.EventHandler(this.bntSpecialUsage_Click);
            // 
            // lblMULTIPLIER
            // 
            resources.ApplyResources(this.lblMULTIPLIER, "lblMULTIPLIER");
            this.lblMULTIPLIER.Name = "lblMULTIPLIER";
            // 
            // txtMultiplier
            // 
            resources.ApplyResources(this.txtMultiplier, "txtMultiplier");
            this.txtMultiplier.Name = "txtMultiplier";
            this.txtMultiplier.ReadOnly = true;
            // 
            // chkAUTO_MOVE_TO_NEXT_CUSTOMER
            // 
            resources.ApplyResources(this.chkAUTO_MOVE_TO_NEXT_CUSTOMER, "chkAUTO_MOVE_TO_NEXT_CUSTOMER");
            this.chkAUTO_MOVE_TO_NEXT_CUSTOMER.Checked = true;
            this.chkAUTO_MOVE_TO_NEXT_CUSTOMER.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAUTO_MOVE_TO_NEXT_CUSTOMER.Name = "chkAUTO_MOVE_TO_NEXT_CUSTOMER";
            this.chkAUTO_MOVE_TO_NEXT_CUSTOMER.UseVisualStyleBackColor = true;
            this.chkAUTO_MOVE_TO_NEXT_CUSTOMER.CheckedChanged += new System.EventHandler(this.cboAUTO_MOVE_TO_NEXT_CUSTOMER_CheckedChanged);
            // 
            // lbl12_MONTHS_USAGE_HISTORY0
            // 
            this.lbl12_MONTHS_USAGE_HISTORY0.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lbl12_MONTHS_USAGE_HISTORY0, "lbl12_MONTHS_USAGE_HISTORY0");
            this.lbl12_MONTHS_USAGE_HISTORY0.Name = "lbl12_MONTHS_USAGE_HISTORY0";
            this.lbl12_MONTHS_USAGE_HISTORY0.UseCompatibleTextRendering = true;
            // 
            // grp12_MONTHS_USAGE_HISTORY
            // 
            this.grp12_MONTHS_USAGE_HISTORY.Controls.Add(this.lbl12_MONTHS_USAGE_HISTORY0);
            resources.ApplyResources(this.grp12_MONTHS_USAGE_HISTORY, "grp12_MONTHS_USAGE_HISTORY");
            this.grp12_MONTHS_USAGE_HISTORY.Name = "grp12_MONTHS_USAGE_HISTORY";
            this.grp12_MONTHS_USAGE_HISTORY.TabStop = false;
            // 
            // chk12_MONTHS_USAGE_HISTORY
            // 
            resources.ApplyResources(this.chk12_MONTHS_USAGE_HISTORY, "chk12_MONTHS_USAGE_HISTORY");
            this.chk12_MONTHS_USAGE_HISTORY.Name = "chk12_MONTHS_USAGE_HISTORY";
            this.chk12_MONTHS_USAGE_HISTORY.UseVisualStyleBackColor = true;
            this.chk12_MONTHS_USAGE_HISTORY.CheckedChanged += new System.EventHandler(this.chk12_MONTHS_USAGE_HISTORY_CheckedChanged);
            // 
            // btnUSAGE_HISTORY
            // 
            resources.ApplyResources(this.btnUSAGE_HISTORY, "btnUSAGE_HISTORY");
            this.btnUSAGE_HISTORY.Name = "btnUSAGE_HISTORY";
            this.btnUSAGE_HISTORY.UseVisualStyleBackColor = true;
            this.btnUSAGE_HISTORY.Click += new System.EventHandler(this.btnUSAGE_HISTORY_Click);
            // 
            // DialogCollectUsageManually
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.KeyPreview = true;
            this.Name = "DialogCollectUsageManually";
            this.Activated += new System.EventHandler(this.DialogCollectUsageManually_Activated);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DialogCollectUsageManually_KeyDown);
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.grp12_MONTHS_USAGE_HISTORY.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Label lblCUSTOMER_1;
        private Label lblCUSTOMER_USAGE;
        private Label lblCUSTOMER_NAME;
        private Label lblCUSTOMER_CODE;
        private Label lblMETER_CODE;
        private TextBox txtBox;
        private TextBox txtPole;
        private TextBox txtArea;
        private Label lblBOX;
        private Label lblPOLE;
        private Label lblAREA;
        private ExTextbox txtCustomerName;
        private ExTextbox txtCustomerCode;
        private ExTextbox txtMeter;
        private Label lblMONTH;
        private Label lblCYCLE_NAME;
        private ComboBox cboCollector;
        private Label lblCOLLECTOR;
        private Label label21;
        private Label lblSTART_DATE;
        private DateTimePicker dtpEnd;
        private DateTimePicker dtpStart;
        private DateTimePicker dtpMonth;
        private ComboBox cboCycle;
        private Label lblEND_USAGE;
        private Label lblSTART_USAGE;
        private ComboBox cboMeter;
        private Label lblMETER_CODE_1;
        private TextBox txtTotalUsage;
        private Label lblUSAGE;
        private TextBox txtEndUsage;
        private TextBox txtStartUsage;
        private Panel panel2;
        private ExButton btnCLOSE;
        private ExButton btnSAVE;
        private Label lblReader_;
        private CheckBox chkIS_NEW_CYCLE;
        private ExButton btnCUSTOMER_NO_USAGE;
        private ExButton btnCHECK_USAGE;
        private TextBox txtMultiplier;
        private Label lblMULTIPLIER;
        private CheckBox chkAUTO_MOVE_TO_NEXT_CUSTOMER;
        private Label lbl12_MONTHS_USAGE_HISTORY0;
        private GroupBox grp12_MONTHS_USAGE_HISTORY;
        private CheckBox chk12_MONTHS_USAGE_HISTORY;
        private ExButton btnUSAGE_HISTORY;
    }
}