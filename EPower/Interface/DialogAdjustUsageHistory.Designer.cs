﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogAdjustUsageHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogAdjustUsageHistory));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.ADJUST_USAGE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.USAGE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATED_ON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OLD_START_USAGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OLD_END_USAGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NEW_START_USAGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NEW_END_USAGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_BY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.dgv);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.panel1);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.dgv, 0);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToOrderColumns = true;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ADJUST_USAGE_ID,
            this.USAGE_ID,
            this.CREATED_ON,
            this.OLD_START_USAGE,
            this.OLD_END_USAGE,
            this.NEW_START_USAGE,
            this.NEW_END_USAGE,
            this.CREATE_BY});
            this.dgv.Cursor = System.Windows.Forms.Cursors.Arrow;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv.DefaultCellStyle = dataGridViewCellStyle10;
            this.dgv.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgv.RowsDefaultCellStyle = dataGridViewCellStyle11;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // ADJUST_USAGE_ID
            // 
            this.ADJUST_USAGE_ID.DataPropertyName = "ADJUST_USAGE_ID";
            dataGridViewCellStyle3.Format = "dd-MM-yyyy";
            this.ADJUST_USAGE_ID.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.ADJUST_USAGE_ID, "ADJUST_USAGE_ID");
            this.ADJUST_USAGE_ID.Name = "ADJUST_USAGE_ID";
            this.ADJUST_USAGE_ID.ReadOnly = true;
            // 
            // USAGE_ID
            // 
            this.USAGE_ID.DataPropertyName = "USAGE_ID";
            resources.ApplyResources(this.USAGE_ID, "USAGE_ID");
            this.USAGE_ID.Name = "USAGE_ID";
            this.USAGE_ID.ReadOnly = true;
            // 
            // CREATED_ON
            // 
            this.CREATED_ON.DataPropertyName = "CREATED_ON";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.Format = "dd-MM-yyyy hh:mm tt";
            this.CREATED_ON.DefaultCellStyle = dataGridViewCellStyle4;
            this.CREATED_ON.FillWeight = 60F;
            resources.ApplyResources(this.CREATED_ON, "CREATED_ON");
            this.CREATED_ON.Name = "CREATED_ON";
            this.CREATED_ON.ReadOnly = true;
            // 
            // OLD_START_USAGE
            // 
            this.OLD_START_USAGE.DataPropertyName = "OLD_START_USAGE";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.Format = "N0";
            dataGridViewCellStyle5.NullValue = null;
            this.OLD_START_USAGE.DefaultCellStyle = dataGridViewCellStyle5;
            this.OLD_START_USAGE.FillWeight = 75F;
            resources.ApplyResources(this.OLD_START_USAGE, "OLD_START_USAGE");
            this.OLD_START_USAGE.Name = "OLD_START_USAGE";
            this.OLD_START_USAGE.ReadOnly = true;
            // 
            // OLD_END_USAGE
            // 
            this.OLD_END_USAGE.DataPropertyName = "OLD_END_USAGE";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "#,##0.####";
            this.OLD_END_USAGE.DefaultCellStyle = dataGridViewCellStyle6;
            this.OLD_END_USAGE.FillWeight = 75F;
            resources.ApplyResources(this.OLD_END_USAGE, "OLD_END_USAGE");
            this.OLD_END_USAGE.Name = "OLD_END_USAGE";
            this.OLD_END_USAGE.ReadOnly = true;
            // 
            // NEW_START_USAGE
            // 
            this.NEW_START_USAGE.DataPropertyName = "NEW_START_USAGE";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.Format = "N0";
            this.NEW_START_USAGE.DefaultCellStyle = dataGridViewCellStyle7;
            this.NEW_START_USAGE.FillWeight = 80F;
            resources.ApplyResources(this.NEW_START_USAGE, "NEW_START_USAGE");
            this.NEW_START_USAGE.Name = "NEW_START_USAGE";
            this.NEW_START_USAGE.ReadOnly = true;
            // 
            // NEW_END_USAGE
            // 
            this.NEW_END_USAGE.DataPropertyName = "NEW_END_USAGE";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "#,##0.####";
            this.NEW_END_USAGE.DefaultCellStyle = dataGridViewCellStyle8;
            this.NEW_END_USAGE.FillWeight = 75F;
            resources.ApplyResources(this.NEW_END_USAGE, "NEW_END_USAGE");
            this.NEW_END_USAGE.Name = "NEW_END_USAGE";
            this.NEW_END_USAGE.ReadOnly = true;
            // 
            // CREATE_BY
            // 
            this.CREATE_BY.DataPropertyName = "CREATE_BY";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.CREATE_BY.DefaultCellStyle = dataGridViewCellStyle9;
            this.CREATE_BY.FillWeight = 50F;
            resources.ApplyResources(this.CREATE_BY, "CREATE_BY");
            this.CREATE_BY.Name = "CREATE_BY";
            this.CREATE_BY.ReadOnly = true;
            // 
            // DialogAdjustUsageHistory
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogAdjustUsageHistory";
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Panel panel1;
        private DataGridView dgv;
        private DataGridViewTextBoxColumn ADJUST_USAGE_ID;
        private DataGridViewTextBoxColumn USAGE_ID;
        private DataGridViewTextBoxColumn CREATED_ON;
        private DataGridViewTextBoxColumn OLD_START_USAGE;
        private DataGridViewTextBoxColumn OLD_END_USAGE;
        private DataGridViewTextBoxColumn NEW_START_USAGE;
        private DataGridViewTextBoxColumn NEW_END_USAGE;
        private DataGridViewTextBoxColumn CREATE_BY;
    }
}