﻿using System;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;

namespace EPower.Interface
{
    public partial class DialogStockItem : ExDialog
    {
        GeneralProcess _flag;
        
        TBL_STOCK_ITEM _objNew = new TBL_STOCK_ITEM();

        public TBL_STOCK_ITEM StockItem
        {
          get { return _objNew; }
        }
         
        TBL_STOCK_ITEM _objOld = new TBL_STOCK_ITEM();

        #region Constructor
        public DialogStockItem(GeneralProcess flag, TBL_STOCK_ITEM objStockItem)
        {
            InitializeComponent();

            _flag = flag;
            objStockItem._CopyTo(_objNew);
            objStockItem._CopyTo(_objOld);
            flag.GetText(this.Text);
            UIHelper.SetEnabled(this, flag != GeneralProcess.Delete);
            this.btnChangelog.Visible = this._flag != GeneralProcess.Insert;
            read();
        }        
        #endregion

        #region Operation
        /// <summary>
        /// Change Keyboard to Khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        } 

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        /// <summary>
        /// Data transaction.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
             //If record is duplicate.
            txtItemName.ClearValidation();
            if (DBDataContext.Db.IsExits(_objNew, "ITEM_NAME"))
            {
                txtItemName.SetValidation(string.Format( Resources.MS_IS_EXISTS,lblAreaCode.Text));
                return;
            }

            try
            {
                using (TransactionScope tran=new TransactionScope( TransactionScopeOption.Required,TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objNew);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objNew);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }                                               
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }            
        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
        #endregion

        #region Method
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();

            if (txtItemName.Text.Trim().Length <= 0)
            {
                txtItemName.SetValidation(string.Format(Resources.REQUIRED, lblAreaCode.Text));
                val = true;
            }

            if (txtUnit.Text.Trim().Length <= 0)
            {
                txtUnit.SetValidation(string.Format(Resources.REQUIRED, lblAreaName.Text));
                val = true;
            }            
            return val;
        }

        /// <summary>
        /// Read data from object.
        /// </summary>
        private void read()
        {
            txtItemName.Text = _objNew.ITEM_NAME;
            txtUnit.Text = _objNew.UNIT;
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            _objNew.ITEM_NAME = txtItemName.Text;
            _objNew.UNIT = txtUnit.Text;
        }
        #endregion

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }

       
    }
}