﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageMeter
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageMeter));
            this.dgv = new System.Windows.Forms.DataGridView();
            this.METER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.METER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.METER_TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MULTIPLIER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.STATUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCUSTOMER_METER_USAGE = new SoftTech.Component.ExButton();
            this.cboStatus = new System.Windows.Forms.ComboBox();
            this.cboMeterType = new System.Windows.Forms.ComboBox();
            this.btnADD = new SoftTech.Component.ExButton();
            this.btnEDIT = new SoftTech.Component.ExButton();
            this.txtQuickSearch = new SoftTech.Component.ExTextbox();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.METER_ID,
            this.METER_CODE,
            this.CUSTOMER_NAME,
            this.METER_TYPE,
            this.MULTIPLIER,
            this.STATUS});
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellDoubleClick);
            // 
            // METER_ID
            // 
            this.METER_ID.DataPropertyName = "METER_ID";
            resources.ApplyResources(this.METER_ID, "METER_ID");
            this.METER_ID.Name = "METER_ID";
            // 
            // METER_CODE
            // 
            this.METER_CODE.DataPropertyName = "METER_CODE";
            resources.ApplyResources(this.METER_CODE, "METER_CODE");
            this.METER_CODE.Name = "METER_CODE";
            // 
            // CUSTOMER_NAME
            // 
            this.CUSTOMER_NAME.DataPropertyName = "CUSTOMER_NAME";
            resources.ApplyResources(this.CUSTOMER_NAME, "CUSTOMER_NAME");
            this.CUSTOMER_NAME.Name = "CUSTOMER_NAME";
            // 
            // METER_TYPE
            // 
            this.METER_TYPE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.METER_TYPE.DataPropertyName = "METER_TYPE_NAME";
            resources.ApplyResources(this.METER_TYPE, "METER_TYPE");
            this.METER_TYPE.Name = "METER_TYPE";
            // 
            // MULTIPLIER
            // 
            this.MULTIPLIER.DataPropertyName = "MULTIPLIER";
            resources.ApplyResources(this.MULTIPLIER, "MULTIPLIER");
            this.MULTIPLIER.Name = "MULTIPLIER";
            // 
            // STATUS
            // 
            this.STATUS.DataPropertyName = "STATUS_NAME";
            resources.ApplyResources(this.STATUS, "STATUS");
            this.STATUS.Name = "STATUS";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.btnCUSTOMER_METER_USAGE);
            this.panel1.Controls.Add(this.cboStatus);
            this.panel1.Controls.Add(this.cboMeterType);
            this.panel1.Controls.Add(this.btnADD);
            this.panel1.Controls.Add(this.btnEDIT);
            this.panel1.Controls.Add(this.txtQuickSearch);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCUSTOMER_METER_USAGE
            // 
            resources.ApplyResources(this.btnCUSTOMER_METER_USAGE, "btnCUSTOMER_METER_USAGE");
            this.btnCUSTOMER_METER_USAGE.Name = "btnCUSTOMER_METER_USAGE";
            this.btnCUSTOMER_METER_USAGE.UseVisualStyleBackColor = true;
            this.btnCUSTOMER_METER_USAGE.Click += new System.EventHandler(this.btnCUS_METER_USAGE_Click);
            // 
            // cboStatus
            // 
            this.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboStatus.FormattingEnabled = true;
            this.cboStatus.Items.AddRange(new object[] {
            resources.GetString("cboStatus.Items"),
            resources.GetString("cboStatus.Items1"),
            resources.GetString("cboStatus.Items2"),
            resources.GetString("cboStatus.Items3"),
            resources.GetString("cboStatus.Items4")});
            resources.ApplyResources(this.cboStatus, "cboStatus");
            this.cboStatus.Name = "cboStatus";
            this.cboStatus.SelectedIndexChanged += new System.EventHandler(this.txt_QuickSearch);
            this.cboStatus.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // cboMeterType
            // 
            this.cboMeterType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMeterType.FormattingEnabled = true;
            this.cboMeterType.Items.AddRange(new object[] {
            resources.GetString("cboMeterType.Items"),
            resources.GetString("cboMeterType.Items1"),
            resources.GetString("cboMeterType.Items2"),
            resources.GetString("cboMeterType.Items3"),
            resources.GetString("cboMeterType.Items4")});
            resources.ApplyResources(this.cboMeterType, "cboMeterType");
            this.cboMeterType.Name = "cboMeterType";
            this.cboMeterType.SelectedIndexChanged += new System.EventHandler(this.txt_QuickSearch);
            this.cboMeterType.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // btnADD
            // 
            resources.ApplyResources(this.btnADD, "btnADD");
            this.btnADD.Name = "btnADD";
            this.btnADD.UseVisualStyleBackColor = true;
            this.btnADD.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnEDIT
            // 
            resources.ApplyResources(this.btnEDIT, "btnEDIT");
            this.btnEDIT.Name = "btnEDIT";
            this.btnEDIT.UseVisualStyleBackColor = true;
            this.btnEDIT.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // txtQuickSearch
            // 
            this.txtQuickSearch.BackColor = System.Drawing.Color.White;
            this.txtQuickSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtQuickSearch, "txtQuickSearch");
            this.txtQuickSearch.Name = "txtQuickSearch";
            this.txtQuickSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtQuickSearch.QuickSearch += new System.EventHandler(this.txt_QuickSearch);
            this.txtQuickSearch.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // PageMeter
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.panel1);
            this.Name = "PageMeter";
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExTextbox txtQuickSearch;
        private ExButton btnADD;
        private ExButton btnEDIT;
        private DataGridView dgv;
        public ComboBox cboMeterType;
        public ComboBox cboStatus;
        private DataGridViewTextBoxColumn METER_ID;
        private DataGridViewTextBoxColumn METER_CODE;
        private DataGridViewTextBoxColumn CUSTOMER_NAME;
        private DataGridViewTextBoxColumn METER_TYPE;
        private DataGridViewTextBoxColumn MULTIPLIER;
        private DataGridViewTextBoxColumn STATUS;
        private ExButton btnCUSTOMER_METER_USAGE;
    }
}
