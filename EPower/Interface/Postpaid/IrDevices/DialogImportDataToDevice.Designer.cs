﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogImportDataToDevice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnClose = new SoftTech.Component.ExButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.lblTOTAL_CUSTOMER = new System.Windows.Forms.Label();
            this.lblMONTH = new System.Windows.Forms.Label();
            this.txtTotalCustomer = new System.Windows.Forms.TextBox();
            this.txtMonth = new System.Windows.Forms.TextBox();
            this.lblDEVICE_CODE = new System.Windows.Forms.Label();
            this.btnSEND_DATA_TO_DEVICE = new SoftTech.Component.ExButton();
            this.lblDEVICE_NAME = new System.Windows.Forms.Label();
            this.txtDeviceCode = new System.Windows.Forms.TextBox();
            this.txtDeviceName = new System.Windows.Forms.TextBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.btnClose);
            this.content.Controls.Add(this.txtDeviceName);
            this.content.Controls.Add(this.txtDeviceCode);
            this.content.Controls.Add(this.btnSEND_DATA_TO_DEVICE);
            this.content.Controls.Add(this.txtMonth);
            this.content.Controls.Add(this.lblDEVICE_CODE);
            this.content.Controls.Add(this.lblDEVICE_NAME);
            this.content.Controls.Add(this.txtTotalCustomer);
            this.content.Controls.Add(this.lblMONTH);
            this.content.Controls.Add(this.lblTOTAL_CUSTOMER);
            this.content.Controls.Add(this.panel1);
            this.content.Margin = new System.Windows.Forms.Padding(1);
            this.content.Size = new System.Drawing.Size(374, 234);
            this.content.Text = "\'CONTENT\'";
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.lblTOTAL_CUSTOMER, 0);
            this.content.Controls.SetChildIndex(this.lblMONTH, 0);
            this.content.Controls.SetChildIndex(this.txtTotalCustomer, 0);
            this.content.Controls.SetChildIndex(this.lblDEVICE_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblDEVICE_CODE, 0);
            this.content.Controls.SetChildIndex(this.txtMonth, 0);
            this.content.Controls.SetChildIndex(this.btnSEND_DATA_TO_DEVICE, 0);
            this.content.Controls.SetChildIndex(this.txtDeviceCode, 0);
            this.content.Controls.SetChildIndex(this.txtDeviceName, 0);
            this.content.Controls.SetChildIndex(this.btnClose, 0);
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnClose.Location = new System.Drawing.Point(294, 205);
            this.btnClose.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "បិទ";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Location = new System.Drawing.Point(2, 200);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(371, 1);
            this.panel1.TabIndex = 223;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(1, 0);
            this.panel4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(768, 34);
            this.panel4.TabIndex = 225;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 19);
            this.label1.TabIndex = 3;
            this.label1.Text = "សំរាប់ខែ ៖";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(228, 31);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(104, 23);
            this.checkBox1.TabIndex = 4;
            this.checkBox1.Text = "បង្ហាញក្នុងតារាង";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // lblTOTAL_CUSTOMER
            // 
            this.lblTOTAL_CUSTOMER.AutoSize = true;
            this.lblTOTAL_CUSTOMER.Location = new System.Drawing.Point(17, 123);
            this.lblTOTAL_CUSTOMER.Margin = new System.Windows.Forms.Padding(2);
            this.lblTOTAL_CUSTOMER.Name = "lblTOTAL_CUSTOMER";
            this.lblTOTAL_CUSTOMER.Size = new System.Drawing.Size(97, 19);
            this.lblTOTAL_CUSTOMER.TabIndex = 227;
            this.lblTOTAL_CUSTOMER.Text = "ចំនួនអតិថិជនសរុប";
            // 
            // lblMONTH
            // 
            this.lblMONTH.AutoSize = true;
            this.lblMONTH.Location = new System.Drawing.Point(17, 92);
            this.lblMONTH.Margin = new System.Windows.Forms.Padding(2);
            this.lblMONTH.Name = "lblMONTH";
            this.lblMONTH.Size = new System.Drawing.Size(45, 19);
            this.lblMONTH.TabIndex = 228;
            this.lblMONTH.Text = "ប្រចាំខែ";
            // 
            // txtTotalCustomer
            // 
            this.txtTotalCustomer.Location = new System.Drawing.Point(164, 119);
            this.txtTotalCustomer.Margin = new System.Windows.Forms.Padding(2);
            this.txtTotalCustomer.Name = "txtTotalCustomer";
            this.txtTotalCustomer.ReadOnly = true;
            this.txtTotalCustomer.Size = new System.Drawing.Size(185, 27);
            this.txtTotalCustomer.TabIndex = 283;
            // 
            // txtMonth
            // 
            this.txtMonth.Location = new System.Drawing.Point(164, 88);
            this.txtMonth.Margin = new System.Windows.Forms.Padding(2);
            this.txtMonth.Name = "txtMonth";
            this.txtMonth.ReadOnly = true;
            this.txtMonth.Size = new System.Drawing.Size(185, 27);
            this.txtMonth.TabIndex = 300;
            // 
            // lblDEVICE_CODE
            // 
            this.lblDEVICE_CODE.AutoSize = true;
            this.lblDEVICE_CODE.BackColor = System.Drawing.Color.Transparent;
            this.lblDEVICE_CODE.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblDEVICE_CODE.Location = new System.Drawing.Point(17, 30);
            this.lblDEVICE_CODE.Margin = new System.Windows.Forms.Padding(2);
            this.lblDEVICE_CODE.Name = "lblDEVICE_CODE";
            this.lblDEVICE_CODE.Size = new System.Drawing.Size(52, 19);
            this.lblDEVICE_CODE.TabIndex = 80;
            this.lblDEVICE_CODE.Text = "លេខកូដ ";
            // 
            // btnSEND_DATA_TO_DEVICE
            // 
            this.btnSEND_DATA_TO_DEVICE.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnSEND_DATA_TO_DEVICE.Location = new System.Drawing.Point(145, 205);
            this.btnSEND_DATA_TO_DEVICE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSEND_DATA_TO_DEVICE.Name = "btnSEND_DATA_TO_DEVICE";
            this.btnSEND_DATA_TO_DEVICE.Size = new System.Drawing.Size(143, 23);
            this.btnSEND_DATA_TO_DEVICE.TabIndex = 79;
            this.btnSEND_DATA_TO_DEVICE.Text = "បញ្ចូនទៅកាន់ឧបករណ័";
            this.btnSEND_DATA_TO_DEVICE.UseVisualStyleBackColor = true;
            this.btnSEND_DATA_TO_DEVICE.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // lblDEVICE_NAME
            // 
            this.lblDEVICE_NAME.AutoSize = true;
            this.lblDEVICE_NAME.BackColor = System.Drawing.Color.Transparent;
            this.lblDEVICE_NAME.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblDEVICE_NAME.Location = new System.Drawing.Point(17, 61);
            this.lblDEVICE_NAME.Margin = new System.Windows.Forms.Padding(2);
            this.lblDEVICE_NAME.Name = "lblDEVICE_NAME";
            this.lblDEVICE_NAME.Size = new System.Drawing.Size(40, 19);
            this.lblDEVICE_NAME.TabIndex = 81;
            this.lblDEVICE_NAME.Text = "ឈ្មោះ";
            this.lblDEVICE_NAME.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDeviceCode
            // 
            this.txtDeviceCode.Location = new System.Drawing.Point(164, 26);
            this.txtDeviceCode.Margin = new System.Windows.Forms.Padding(2);
            this.txtDeviceCode.Name = "txtDeviceCode";
            this.txtDeviceCode.ReadOnly = true;
            this.txtDeviceCode.Size = new System.Drawing.Size(185, 27);
            this.txtDeviceCode.TabIndex = 303;
            // 
            // txtDeviceName
            // 
            this.txtDeviceName.Location = new System.Drawing.Point(164, 57);
            this.txtDeviceName.Margin = new System.Windows.Forms.Padding(2);
            this.txtDeviceName.Name = "txtDeviceName";
            this.txtDeviceName.ReadOnly = true;
            this.txtDeviceName.Size = new System.Drawing.Size(185, 27);
            this.txtDeviceName.TabIndex = 304;
            // 
            // DialogImportDataToDevice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(374, 257);
            this.Name = "DialogImportDataToDevice";
            this.Text = "បញ្ចូលព៌តមានថ្មីៗទៅឧបករណ៍";
            this.TopMost = true;
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnClose;
        private Panel panel1;
        private Panel panel4;
        
        private Label label1;
        private CheckBox checkBox1;
    
        private Label lblMONTH;
        private Label lblTOTAL_CUSTOMER;
        private TextBox txtTotalCustomer;
        private TextBox txtMonth;
        private Label lblDEVICE_CODE;
        private ExButton btnSEND_DATA_TO_DEVICE;
        private Label lblDEVICE_NAME;
        private TextBox txtDeviceName;
        private TextBox txtDeviceCode;
    }
}