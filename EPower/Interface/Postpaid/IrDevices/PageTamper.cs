﻿using EPower.Base.Logic;
using EPower.Logic.IRDevice;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageTamper : Form
    {
        private bool _binding = false;
        private DataTable _ds =null;
        private List<TMP_IR_TAMPER_METER> rows = new List<TMP_IR_TAMPER_METER>();
        //private List<TBL_CUSTOMER> crows = new List<TBL_CUSTOMER>();

        public PageTamper()
        {
            InitializeComponent();

            UIHelper.DataGridViewProperties(this.dgvListCustomer, this.dgvClear);
            this.dgvListCustomer.MultiSelect
                = this.dgvClear.MultiSelect
                = true;
            this.Dock = DockStyle.Fill;
            BindCustomerType();
            BindCycle();
            this.txtCycle.SelectedValue = 0;
            //BindData();
        }

        public void BindCustomerType()
        {
            _binding = true;
            UIHelper.SetDataSourceToComboBox(this.cboCustomerGroup, Lookup.GetCustomerGroup(), Resources.All_CUSTOMER_GROUP);
           _binding = false;
        }

        public void BindCycle()
        {
            _binding = true;
            UIHelper.SetDataSourceToComboBox(this.txtCycle, Lookup.GetBillingCycles(), Resources.ALL_CYCLE);
            _binding = false;
        }

        public void BindData()
        {
            if (this._binding)
            { 
                return;
            } 

            this._binding = true;
            
            int cycleID=(int) this.txtCycle.SelectedValue;
            int typeID = cboConnectionType.SelectedValue == null ? 0 : (int)cboConnectionType.SelectedValue;
            int cusGroupId = (int)cboCustomerGroup.SelectedValue;

            this._ds = (from t in DBDataContext.Db.TBL_METER_TAMPERs
                     join c in DBDataContext.Db.TBL_CUSTOMERs on t.CUSTOMER_ID equals c.CUSTOMER_ID
                     join m in DBDataContext.Db.TBL_METERs on t.METER_ID equals m.METER_ID
                     join ct in DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs on c.CUSTOMER_CONNECTION_TYPE_ID equals ct.CUSTOMER_CONNECTION_TYPE_ID
                     join g in DBDataContext.Db.TLKP_CUSTOMER_GROUPs on ct.NONLICENSE_CUSTOMER_GROUP_ID equals g.CUSTOMER_GROUP_ID
                     join area in DBDataContext.Db.TBL_AREAs on c.AREA_ID equals area.AREA_ID
                     join b in DBDataContext.Db.TBL_BILLING_CYCLEs on c.BILLING_CYCLE_ID equals b.CYCLE_ID                     
                     join cm in (
                                from m in DBDataContext.Db.TBL_METERs
                                join cm in DBDataContext.Db.TBL_CUSTOMER_METERs on m.METER_ID equals cm.METER_ID
                                join b in DBDataContext.Db.TBL_BOXes on cm.BOX_ID equals b.BOX_ID
                                join p in DBDataContext.Db.TBL_POLEs on b.POLE_ID equals p.POLE_ID
                                where cm.IS_ACTIVE
                                select new { cm.CUSTOMER_ID, m.METER_CODE, p.POLE_CODE, b.BOX_CODE }
                             ) on c.CUSTOMER_ID equals cm.CUSTOMER_ID into cmTmp
                     join k in (from s in DBDataContext.Db.TBL_METER_TAMPERs group s by new { s.CUSTOMER_ID, s.METER_ID } into g select new { MaxID = g.Max(x => x.METER_TAMPER_ID) }) on t.METER_TAMPER_ID equals k.MaxID
                     join ts in DBDataContext.Db.TLKP_TAMPER_STATUS on t.TAMPER_STATUS equals ts.TAMPER_STATUS_CODE
                     from cmt in cmTmp.DefaultIfEmpty()
                     where (cusGroupId == 0 || ct.NONLICENSE_CUSTOMER_GROUP_ID == cusGroupId)
                        && (typeID == 0 || c.CUSTOMER_CONNECTION_TYPE_ID == typeID)
                        && (cycleID == 0 || c.BILLING_CYCLE_ID == cycleID)
                        && ((c.CUSTOMER_CODE + " " + c.LAST_NAME_KH + " " + c.FIRST_NAME_KH + " " + c.LAST_NAME + " " + c.FIRST_NAME + " " + m.METER_CODE).ToLower().Contains(this.txtSearch.Text.ToLower()))
                        && ts.TAMPER_STATUS_NAME != "Normal"
                     select new
                     {
                         METER_TAMPER_ID = t.METER_TAMPER_ID,
                         METER_ID = m.METER_ID,
                         CUSTOMER_ID = c.CUSTOMER_ID,
                         CUSTOMER_CODE = c.CUSTOMER_CODE,
                         FULL_NAME = c.LAST_NAME_KH + " " + c.FIRST_NAME_KH,
                         METER_CODE = m.METER_CODE,
                         CUSTOMER_TYPE_NAME = g.CUSTOMER_GROUP_NAME,
                         AREA_CODE = area.AREA_NAME,
                         POLE_CODE = cmt == null ? "" : cmt.POLE_CODE,
                         BOX_CODE = cmt == null ? "" : cmt.BOX_CODE,
                         CYCLE_NAME = b.CYCLE_NAME,
                         TAMPER_STATUS_CODE = ts.TAMPER_STATUS_CODE,
                         TAMPER_STATUS = ts.TAMPER_STATUS_NAME_KH,
                         CREATE_BY = t.CREATE_BY,
                         CREATE_ON = t.CREATE_ON,
                         DEVICE_ID = t.DEVICE_ID,
                         COLLECTOR_ID = t.COLLECTOR_ID,
                         IS_BLUETOOTH = m.IS_BLUETOOTH
                     })._ToDataTable();

            this.dgvListCustomer.DataSource =this._ds;
                    
            this._binding = false;

        }

        private void txtCustomerType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

        private void txtSearch_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void cboCustomerGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            int groupId = DataHelper.ParseToInt(cboCustomerGroup.SelectedValue.ToString());
            UIHelper.SetDataSourceToComboBox(cboConnectionType, DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs
                                                                .Where(x => x.NONLICENSE_CUSTOMER_GROUP_ID == groupId)
                                                                .OrderBy(x => x.DESCRIPTION)
                                                                , Resources.ALL_CONNECTION_TYPE);
            if (cboConnectionType.DataSource != null)
            {
                cboConnectionType.SelectedIndex = 0;
            }
        }

        private void txtSearch_QuickSearch(object sender, EventArgs e)
        {
            BindData();
        }

        private void txtCycle_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

        private void dgvListCustomer_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                List<DataGridViewRow> rows = new List<DataGridViewRow>();
                foreach (var item in dgvListCustomer.SelectedRows)
                {
                    rows.Add((DataGridViewRow)item);
                }
                dgvListCustomer.DoDragDrop(rows, DragDropEffects.Copy);
            }
        }

        private void dgvClear_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }

        private void dgvClear_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(List<DataGridViewRow>)))
            {
                var data = e.Data.GetData(typeof(List<DataGridViewRow>));
                var id = 1;

                foreach (var item in (List<DataGridViewRow>)data)
                {
                    if (rows.Select(x => x.METER_CODE).Contains((string)item.Cells[METER_CODE.Name].Value))
                    {
                        continue;
                    }
                    rows.Add(new TMP_IR_TAMPER_METER()
                    {
                        METER_CODE = (string)item.Cells[METER_CODE.Name].Value,
                        FULL_NAME = (string)item.Cells[CUSTOMER_NAME.Name].Value,
                        AREA_CODE = (string)item.Cells[AREA.Name].Value,
                        POLE_CODE = (string)item.Cells[POLE.Name].Value,
                        BOX_CODE = (string)item.Cells[BOX.Name].Value,
                        TAMPER_STATUS = (string)item.Cells[TAMPER_STATUS_CODE.Name].Value,
                        CREATE_ON = (DateTime)item.Cells[CREATE_ON.Name].Value,
                        IS_BLUETOOTH = (bool)item.Cells[IS_BLUETOOTH.Name].Value,
                        ID = id
                    });
                    id++;
                }
                dgvClear.DataSource = rows;
                
                lblCUSTOMER_TO_CLEAR.Visible = dgvClear.Rows.Count == 0;
            }
        }

        private void dgvClear_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                foreach (DataGridViewRow item in dgvClear.SelectedRows)
                {
                    var cId = (int)item.Cells[METER_TAMPER_ID_1.Name].Value;
                    rows.Remove(rows.FirstOrDefault(x => x.ID == cId));
                    //crows.Remove(crows.FirstOrDefault(x => x.ID == cId));
                }
                dgvClear.DataSource = rows.ToList();
            }
        }

        private void btnSEND_DATA_Click(object sender, EventArgs e)
        {
            string deviceCode = "";
            bool isSend = false;
            if (MsgBox.ShowQuestion(Resources.MSG_CONFIRM_BEFORE_SEND_DATA_TO_DEVICE, Resources.MSG_VERIFY_BEFORE_CONFIRM) != DialogResult.Yes)
            {
                return;
            }
            if (rows.Count() == 0)
            {
                MsgBox.ShowInformation(Resources.MS_NO_CUSTOMER_TO_BLOCK_AND_UNBLOCK);
                return;
            }
            try
            {
                Runner.RunNewThread(() =>
                {
                    // Connect & check device identify 
                    var d = IRDevice.Connect();

                    if (!(d is Android))
                    {
                        throw new MsgBox.MessageException(Resources.MSG_NO_DATA_TO_SEND);
                    }
                    IRDevice.Validate(d);
                    deviceCode = d.ProductID;
                    // Get data from GF1100
                    Runner.Instance.Text = string.Format(Resources.MS_BACKUP_DATA_IN_DEVICE, string.Empty, string.Empty);
                    d.GetFile(IRDevice.BackupType.BeforeSFTD);
                    // Get Order relay meter
                    Runner.Instance.Text = Resources.MS_PREPARING_DATA;
                    ((Android)d).GetTamperMeter(rows);

                    // Send Data
                    Runner.Instance.Text = string.Format(Resources.MS_SEND_FILE_TO_DEVICE, string.Empty, string.Empty);
                    isSend = d.SendFile();                    
                });
                MsgBox.ShowInformation(Resources.UPLOAD_DATA_SUCCESS);
            }
            catch (Exception)
            {

            }            
        }

        private void btnRECEIVE_DATA_Click(object sender, EventArgs e)
        {
            try
            {
                Runner.RunNewThread(() =>
                {
                    // Connect & check device identify 
                    var d = IRDevice.Connect();

                    if (!(d is Android))
                    {
                        throw new MsgBox.MessageException(Resources.MS_DEVICE_NO_CUSTOMER_BLOCK_AND_UNBLOCK);
                    }
                    IRDevice.Validate(d);
                    var objDevice = DBDataContext.Db.TBL_DEVICEs.FirstOrDefault(x => x.DEVICE_CODE == d.ProductID);
                    
                    Runner.Instance.Text = string.Format(Resources.MS_BACKUP_DATA_IN_DEVICE, objDevice.DEVICE_CODE, string.Empty);

                    d.GetTamperData(IRDevice.BackupType.AfterCPFD);
                    Runner.Instance.Text = Resources.SUCCESS;
                    ((Android)d).SaveTamperMeter(-1, objDevice.DEVICE_ID);
                    
                });
                MsgBox.ShowInformation(Resources.DOWNLOAD_DATA_SUCCESS);
                rows = new List<TMP_IR_TAMPER_METER>();
                dgvClear.DataSource = rows;
                BindData();
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp);
            }            
        }
    }
}
