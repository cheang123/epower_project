﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class DialogOrderRelayMeterLogDetail : ExDialog
    {
        int orderId;
        Dictionary<int, string> OrderRelay = new Dictionary<int, string>()
        {
            {(int)RelayControl.Connect,Resources.UNBLOCK_CUSTOMER},
            {(int)RelayControl.Disconnect,Resources.BLOCK_CUSTOMER}
        };
        public DialogOrderRelayMeterLogDetail(int Id)
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            orderId = Id;
        }

        private void DialogOrderRelayMeterLogDetail_Load(object sender, EventArgs e)
        {
            var rowSend = from s in DBDataContext.Db.TBL_RELAY_METERs.Where(x => x.SEND_ORDER_RELAY_METER_ID == orderId)
                          select new
                          {
                              s.CUSTOMER_CODE,
                              s.FULL_NAME,
                              s.METER_CODE,
                              s.AREA_CODE,
                              s.POLE_CODE,
                              s.BOX_CODE,
                              s.ORDER_RELAY_STATUS
                          };
            var rowReceive = from r in DBDataContext.Db.TBL_RELAY_METERs.Where(x => x.GET_ORDER_RELAY_METER_ID == orderId)
                             select new
                             {
                                 r.CUSTOMER_CODE,
                                 r.FULL_NAME,
                                 r.METER_CODE,
                                 r.AREA_CODE,
                                 r.POLE_CODE,
                                 r.BOX_CODE,
                                 r.ORDER_RELAY_STATUS
                             };
            dgv.DataSource = from row in rowSend.Union(rowReceive)
                     orderby row.ORDER_RELAY_STATUS, row.AREA_CODE, row.POLE_CODE, row.BOX_CODE, row.CUSTOMER_CODE
                     select new
                     {
                         row.CUSTOMER_CODE,
                         row.FULL_NAME,
                         row.METER_CODE,
                         row.AREA_CODE,
                         row.POLE_CODE,
                         row.BOX_CODE,
                         ORDER_RELAY_STATUS = OrderRelay[row.ORDER_RELAY_STATUS]
                     };
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            var obj = DBDataContext.Db.TBL_ORDER_RELAY_METERs.FirstOrDefault(x => x.ORDER_RELAY_METER_ID == orderId);

            CrystalReportHelper ch = new CrystalReportHelper("ReportOrderRelayMeter.rpt");
            ch.SetParameter("@ORDER_RELAY_METER_ID", obj.ORDER_RELAY_METER_ID);
            ch.SetParameter("@DEVICE_CODE", DBDataContext.Db.TBL_DEVICEs.First(x=>x.DEVICE_ID == obj.DEVICE_ID).DEVICE_CODE);
            ch.SetParameter("@CREATE_ON", obj.CREATE_ON);
            ch.SetParameter("@CREATE_BY", obj.CREATE_BY);
            ch.SetParameter("@OPERATION", this.Text);
            DialogReportViewer dig = new DialogReportViewer(ch.Report, this.Text);
            //dig.viewer.EnableExportButton = true;
            dig.ShowDialog();
        }
    }
}
