﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogImportDataToIRReader
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogImportDataToIRReader));
            this.btnClose = new SoftTech.Component.ExButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.lblTOTAL_CUSTOMER = new System.Windows.Forms.Label();
            this.lblMONTH = new System.Windows.Forms.Label();
            this.txtTotalCustomer = new System.Windows.Forms.TextBox();
            this.txtMonth = new System.Windows.Forms.TextBox();
            this.lblDEVICE_CODE = new System.Windows.Forms.Label();
            this.btnSEND_DATA = new SoftTech.Component.ExButton();
            this.lblDEVICE_NAME = new System.Windows.Forms.Label();
            this.txtDeviceCode = new System.Windows.Forms.TextBox();
            this.txtDeviceName = new System.Windows.Forms.TextBox();
            this.lblCYCLE_NAME = new System.Windows.Forms.Label();
            this.lblAREA = new System.Windows.Forms.Label();
            this.txtCycle = new System.Windows.Forms.TextBox();
            this.txtArea = new System.Windows.Forms.TextBox();
            this.btnSelectCycle_ = new SoftTech.Component.ExButton();
            this.btnSelectArea_ = new SoftTech.Component.ExButton();
            this.chkNEW_DB_IN_IR = new System.Windows.Forms.CheckBox();
            this.btnBROWS_SD_PATH0 = new SoftTech.Component.ExButton();
            this.txtSD_PATH = new System.Windows.Forms.TextBox();
            this.lblSD_PATH = new System.Windows.Forms.Label();
            this.picHelp = new System.Windows.Forms.PictureBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picHelp)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.picHelp);
            this.content.Controls.Add(this.btnBROWS_SD_PATH0);
            this.content.Controls.Add(this.txtSD_PATH);
            this.content.Controls.Add(this.lblSD_PATH);
            this.content.Controls.Add(this.chkNEW_DB_IN_IR);
            this.content.Controls.Add(this.btnSelectArea_);
            this.content.Controls.Add(this.btnSelectCycle_);
            this.content.Controls.Add(this.txtCycle);
            this.content.Controls.Add(this.txtArea);
            this.content.Controls.Add(this.lblAREA);
            this.content.Controls.Add(this.lblCYCLE_NAME);
            this.content.Controls.Add(this.btnClose);
            this.content.Controls.Add(this.btnSEND_DATA);
            this.content.Controls.Add(this.txtDeviceCode);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.txtDeviceName);
            this.content.Controls.Add(this.lblTOTAL_CUSTOMER);
            this.content.Controls.Add(this.lblMONTH);
            this.content.Controls.Add(this.txtMonth);
            this.content.Controls.Add(this.lblDEVICE_CODE);
            this.content.Controls.Add(this.lblDEVICE_NAME);
            this.content.Controls.Add(this.txtTotalCustomer);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.txtTotalCustomer, 0);
            this.content.Controls.SetChildIndex(this.lblDEVICE_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblDEVICE_CODE, 0);
            this.content.Controls.SetChildIndex(this.txtMonth, 0);
            this.content.Controls.SetChildIndex(this.lblMONTH, 0);
            this.content.Controls.SetChildIndex(this.lblTOTAL_CUSTOMER, 0);
            this.content.Controls.SetChildIndex(this.txtDeviceName, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.txtDeviceCode, 0);
            this.content.Controls.SetChildIndex(this.btnSEND_DATA, 0);
            this.content.Controls.SetChildIndex(this.btnClose, 0);
            this.content.Controls.SetChildIndex(this.lblCYCLE_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblAREA, 0);
            this.content.Controls.SetChildIndex(this.txtArea, 0);
            this.content.Controls.SetChildIndex(this.txtCycle, 0);
            this.content.Controls.SetChildIndex(this.btnSelectCycle_, 0);
            this.content.Controls.SetChildIndex(this.btnSelectArea_, 0);
            this.content.Controls.SetChildIndex(this.chkNEW_DB_IN_IR, 0);
            this.content.Controls.SetChildIndex(this.lblSD_PATH, 0);
            this.content.Controls.SetChildIndex(this.txtSD_PATH, 0);
            this.content.Controls.SetChildIndex(this.btnBROWS_SD_PATH0, 0);
            this.content.Controls.SetChildIndex(this.picHelp, 0);
            // 
            // btnClose
            // 
            resources.ApplyResources(this.btnClose, "btnClose");
            this.btnClose.Name = "btnClose";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.BackgroundImage = global::EPower.Properties.Resources.dock;
            resources.ApplyResources(this.panel4, "panel4");
            this.panel4.Name = "panel4";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // checkBox1
            // 
            resources.ApplyResources(this.checkBox1, "checkBox1");
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // lblTOTAL_CUSTOMER
            // 
            resources.ApplyResources(this.lblTOTAL_CUSTOMER, "lblTOTAL_CUSTOMER");
            this.lblTOTAL_CUSTOMER.Name = "lblTOTAL_CUSTOMER";
            // 
            // lblMONTH
            // 
            resources.ApplyResources(this.lblMONTH, "lblMONTH");
            this.lblMONTH.Name = "lblMONTH";
            // 
            // txtTotalCustomer
            // 
            resources.ApplyResources(this.txtTotalCustomer, "txtTotalCustomer");
            this.txtTotalCustomer.Name = "txtTotalCustomer";
            this.txtTotalCustomer.ReadOnly = true;
            // 
            // txtMonth
            // 
            resources.ApplyResources(this.txtMonth, "txtMonth");
            this.txtMonth.Name = "txtMonth";
            this.txtMonth.ReadOnly = true;
            // 
            // lblDEVICE_CODE
            // 
            resources.ApplyResources(this.lblDEVICE_CODE, "lblDEVICE_CODE");
            this.lblDEVICE_CODE.BackColor = System.Drawing.Color.Transparent;
            this.lblDEVICE_CODE.Name = "lblDEVICE_CODE";
            // 
            // btnSEND_DATA
            // 
            resources.ApplyResources(this.btnSEND_DATA, "btnSEND_DATA");
            this.btnSEND_DATA.Name = "btnSEND_DATA";
            this.btnSEND_DATA.UseVisualStyleBackColor = true;
            this.btnSEND_DATA.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // lblDEVICE_NAME
            // 
            resources.ApplyResources(this.lblDEVICE_NAME, "lblDEVICE_NAME");
            this.lblDEVICE_NAME.BackColor = System.Drawing.Color.Transparent;
            this.lblDEVICE_NAME.Name = "lblDEVICE_NAME";
            // 
            // txtDeviceCode
            // 
            resources.ApplyResources(this.txtDeviceCode, "txtDeviceCode");
            this.txtDeviceCode.Name = "txtDeviceCode";
            this.txtDeviceCode.ReadOnly = true;
            // 
            // txtDeviceName
            // 
            resources.ApplyResources(this.txtDeviceName, "txtDeviceName");
            this.txtDeviceName.Name = "txtDeviceName";
            this.txtDeviceName.ReadOnly = true;
            // 
            // lblCYCLE_NAME
            // 
            resources.ApplyResources(this.lblCYCLE_NAME, "lblCYCLE_NAME");
            this.lblCYCLE_NAME.BackColor = System.Drawing.Color.Transparent;
            this.lblCYCLE_NAME.Name = "lblCYCLE_NAME";
            // 
            // lblAREA
            // 
            resources.ApplyResources(this.lblAREA, "lblAREA");
            this.lblAREA.BackColor = System.Drawing.Color.Transparent;
            this.lblAREA.Name = "lblAREA";
            // 
            // txtCycle
            // 
            resources.ApplyResources(this.txtCycle, "txtCycle");
            this.txtCycle.Name = "txtCycle";
            this.txtCycle.ReadOnly = true;
            // 
            // txtArea
            // 
            resources.ApplyResources(this.txtArea, "txtArea");
            this.txtArea.Name = "txtArea";
            this.txtArea.ReadOnly = true;
            // 
            // btnSelectCycle_
            // 
            resources.ApplyResources(this.btnSelectCycle_, "btnSelectCycle_");
            this.btnSelectCycle_.Name = "btnSelectCycle_";
            this.btnSelectCycle_.UseVisualStyleBackColor = true;
            this.btnSelectCycle_.Click += new System.EventHandler(this.btnSelectCycle_Click);
            // 
            // btnSelectArea_
            // 
            resources.ApplyResources(this.btnSelectArea_, "btnSelectArea_");
            this.btnSelectArea_.Name = "btnSelectArea_";
            this.btnSelectArea_.UseVisualStyleBackColor = true;
            this.btnSelectArea_.Click += new System.EventHandler(this.btnSelectArea_Click);
            // 
            // chkNEW_DB_IN_IR
            // 
            resources.ApplyResources(this.chkNEW_DB_IN_IR, "chkNEW_DB_IN_IR");
            this.chkNEW_DB_IN_IR.Name = "chkNEW_DB_IN_IR";
            this.chkNEW_DB_IN_IR.UseVisualStyleBackColor = true;
            // 
            // btnBROWS_SD_PATH0
            // 
            resources.ApplyResources(this.btnBROWS_SD_PATH0, "btnBROWS_SD_PATH0");
            this.btnBROWS_SD_PATH0.Name = "btnBROWS_SD_PATH0";
            this.btnBROWS_SD_PATH0.UseVisualStyleBackColor = true;
            this.btnBROWS_SD_PATH0.Click += new System.EventHandler(this.btnBROWS_SD_PATH_Click);
            // 
            // txtSD_PATH
            // 
            resources.ApplyResources(this.txtSD_PATH, "txtSD_PATH");
            this.txtSD_PATH.Name = "txtSD_PATH";
            this.txtSD_PATH.ReadOnly = true;
            // 
            // lblSD_PATH
            // 
            resources.ApplyResources(this.lblSD_PATH, "lblSD_PATH");
            this.lblSD_PATH.BackColor = System.Drawing.Color.Transparent;
            this.lblSD_PATH.Name = "lblSD_PATH";
            // 
            // picHelp
            // 
            this.picHelp.Cursor = System.Windows.Forms.Cursors.Default;
            this.picHelp.Image = global::EPower.Properties.Resources.question_mark;
            resources.ApplyResources(this.picHelp, "picHelp");
            this.picHelp.Name = "picHelp";
            this.picHelp.TabStop = false;
            this.picHelp.MouseEnter += new System.EventHandler(this.picHelp_MouseEnter);
            // 
            // DialogImportDataToIRReader
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogImportDataToIRReader";
            this.TopMost = true;
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picHelp)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnClose;
        private Panel panel1;
        private Panel panel4;
        
        private Label label1;
        private CheckBox checkBox1;
    
        private Label lblMONTH;
        private Label lblTOTAL_CUSTOMER;
        private TextBox txtTotalCustomer;
        private TextBox txtMonth;
        private Label lblDEVICE_CODE;
        private ExButton btnSEND_DATA;
        private Label lblDEVICE_NAME;
        private TextBox txtDeviceName;
        private TextBox txtDeviceCode;
        private Label lblCYCLE_NAME;
        private Label lblAREA;
        private TextBox txtCycle;
        private TextBox txtArea;
        private ExButton btnSelectArea_;
        private ExButton btnSelectCycle_;
        private CheckBox chkNEW_DB_IN_IR;
        private ExButton btnBROWS_SD_PATH0;
        private TextBox txtSD_PATH;
        private Label lblSD_PATH;
        private PictureBox picHelp;
    }
}