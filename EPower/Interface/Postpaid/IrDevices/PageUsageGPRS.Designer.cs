﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageUsageGPRS
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageUsageGPRS));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cboGroup = new System.Windows.Forms.ComboBox();
            this.dtp2 = new System.Windows.Forms.DateTimePicker();
            this.dtp1 = new System.Windows.Forms.DateTimePicker();
            this.btnSEND_DATA = new SoftTech.Component.ExButton();
            this.btnMETER_UNREGISTER = new SoftTech.Component.ExButton();
            this.btnRECEIVE = new SoftTech.Component.ExButton();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.AUDIT_TRIAL_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CONTEXT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DATE_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CONTEXT_GROUP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LOGIN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.cboGroup);
            this.panel1.Controls.Add(this.dtp2);
            this.panel1.Controls.Add(this.dtp1);
            this.panel1.Controls.Add(this.btnSEND_DATA);
            this.panel1.Controls.Add(this.btnMETER_UNREGISTER);
            this.panel1.Controls.Add(this.btnRECEIVE);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // cboGroup
            // 
            this.cboGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboGroup.FormattingEnabled = true;
            resources.ApplyResources(this.cboGroup, "cboGroup");
            this.cboGroup.Name = "cboGroup";
            this.cboGroup.SelectedIndexChanged += new System.EventHandler(this.txtQuickSearch_QuickSearch);
            // 
            // dtp2
            // 
            resources.ApplyResources(this.dtp2, "dtp2");
            this.dtp2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp2.Name = "dtp2";
            this.dtp2.ValueChanged += new System.EventHandler(this.txtQuickSearch_QuickSearch);
            // 
            // dtp1
            // 
            resources.ApplyResources(this.dtp1, "dtp1");
            this.dtp1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp1.Name = "dtp1";
            this.dtp1.ValueChanged += new System.EventHandler(this.txtQuickSearch_QuickSearch);
            // 
            // btnSEND_DATA
            // 
            resources.ApplyResources(this.btnSEND_DATA, "btnSEND_DATA");
            this.btnSEND_DATA.Name = "btnSEND_DATA";
            this.btnSEND_DATA.UseVisualStyleBackColor = true;
            this.btnSEND_DATA.Click += new System.EventHandler(this.btnUploadUsage_Click);
            // 
            // btnMETER_UNREGISTER
            // 
            resources.ApplyResources(this.btnMETER_UNREGISTER, "btnMETER_UNREGISTER");
            this.btnMETER_UNREGISTER.Name = "btnMETER_UNREGISTER";
            this.btnMETER_UNREGISTER.UseVisualStyleBackColor = true;
            this.btnMETER_UNREGISTER.Click += new System.EventHandler(this.btnRegisterMeter_Click);
            // 
            // btnRECEIVE
            // 
            resources.ApplyResources(this.btnRECEIVE, "btnRECEIVE");
            this.btnRECEIVE.Name = "btnRECEIVE";
            this.btnRECEIVE.UseVisualStyleBackColor = true;
            this.btnRECEIVE.Click += new System.EventHandler(this.btnDownloadUsage_Click);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AUDIT_TRIAL_ID,
            this.CONTEXT,
            this.DATE_1,
            this.CONTEXT_GROUP,
            this.LOGIN});
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // AUDIT_TRIAL_ID
            // 
            this.AUDIT_TRIAL_ID.DataPropertyName = "AUDIT_TRIAL_ID";
            resources.ApplyResources(this.AUDIT_TRIAL_ID, "AUDIT_TRIAL_ID");
            this.AUDIT_TRIAL_ID.Name = "AUDIT_TRIAL_ID";
            this.AUDIT_TRIAL_ID.ReadOnly = true;
            // 
            // CONTEXT
            // 
            this.CONTEXT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CONTEXT.DataPropertyName = "CONTEXT";
            resources.ApplyResources(this.CONTEXT, "CONTEXT");
            this.CONTEXT.Name = "CONTEXT";
            this.CONTEXT.ReadOnly = true;
            // 
            // DATE_1
            // 
            this.DATE_1.DataPropertyName = "DATE";
            dataGridViewCellStyle4.Format = "dd-MM-yyyy  hh:mm tt";
            this.DATE_1.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.DATE_1, "DATE_1");
            this.DATE_1.Name = "DATE_1";
            this.DATE_1.ReadOnly = true;
            // 
            // CONTEXT_GROUP
            // 
            this.CONTEXT_GROUP.DataPropertyName = "GROUP";
            resources.ApplyResources(this.CONTEXT_GROUP, "CONTEXT_GROUP");
            this.CONTEXT_GROUP.Name = "CONTEXT_GROUP";
            this.CONTEXT_GROUP.ReadOnly = true;
            // 
            // LOGIN
            // 
            this.LOGIN.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.LOGIN.DataPropertyName = "LOGIN_NAME";
            resources.ApplyResources(this.LOGIN, "LOGIN");
            this.LOGIN.Name = "LOGIN";
            this.LOGIN.ReadOnly = true;
            // 
            // PageUsageGPRS
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.panel1);
            this.Name = "PageUsageGPRS";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnMETER_UNREGISTER;
        private ExButton btnRECEIVE;
        private ExButton btnSEND_DATA;
        private DateTimePicker dtp2;
        private DateTimePicker dtp1;
        private DataGridView dgv;
        private ComboBox cboGroup;
        private DataGridViewTextBoxColumn AUDIT_TRIAL_ID;
        private DataGridViewTextBoxColumn CONTEXT;
        private DataGridViewTextBoxColumn DATE_1;
        private DataGridViewTextBoxColumn CONTEXT_GROUP;
        private DataGridViewTextBoxColumn LOGIN;
    }
}
