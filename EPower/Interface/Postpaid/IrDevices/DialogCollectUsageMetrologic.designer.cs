﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogCollectUsageMetrologic
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCollectUsageMetrologic));
            this.btnOk = new SoftTech.Component.ExButton();
            this.btnClose = new SoftTech.Component.ExButton();
            this.lblCYCLE_NAME = new System.Windows.Forms.Label();
            this.lblMONTH = new System.Windows.Forms.Label();
            this.lblTOTAL_CUSTOMER = new System.Windows.Forms.Label();
            this.lblCUSTOMER_USAGE = new System.Windows.Forms.Label();
            this.lblCUSTOMER_NO_USAGE = new System.Windows.Forms.Label();
            this.cboBillingCycle = new System.Windows.Forms.ComboBox();
            this.dtpMonth = new System.Windows.Forms.DateTimePicker();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.lblSTART_DATE = new System.Windows.Forms.Label();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.txtAllCustomer = new System.Windows.Forms.TextBox();
            this.txtHaveUsageCustomer = new System.Windows.Forms.TextBox();
            this.txtNoUsageCustomer = new System.Windows.Forms.TextBox();
            this.btnCHECK_1 = new SoftTech.Component.ExButton();
            this.lblEND_DATE = new System.Windows.Forms.Label();
            this.lblMETER_UNKNOWN = new System.Windows.Forms.Label();
            this.btnCHECK_2 = new SoftTech.Component.ExButton();
            this.txtUnknownMeter = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnCHECK_3 = new SoftTech.Component.ExButton();
            this.txtUnRegister = new System.Windows.Forms.TextBox();
            this.lblMETER_UNREGISTER = new System.Windows.Forms.Label();
            this.cboDevice = new System.Windows.Forms.ComboBox();
            this.lblDEVICE_CODE = new System.Windows.Forms.Label();
            this.btnSHOW_DETAIL = new SoftTech.Component.ExLinkLabel(this.components);
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.btnSHOW_DETAIL);
            this.content.Controls.Add(this.cboDevice);
            this.content.Controls.Add(this.lblDEVICE_CODE);
            this.content.Controls.Add(this.btnCHECK_3);
            this.content.Controls.Add(this.txtUnRegister);
            this.content.Controls.Add(this.lblMETER_UNREGISTER);
            this.content.Controls.Add(this.panel2);
            this.content.Controls.Add(this.btnCHECK_2);
            this.content.Controls.Add(this.txtUnknownMeter);
            this.content.Controls.Add(this.lblMETER_UNKNOWN);
            this.content.Controls.Add(this.lblEND_DATE);
            this.content.Controls.Add(this.btnCHECK_1);
            this.content.Controls.Add(this.txtNoUsageCustomer);
            this.content.Controls.Add(this.txtHaveUsageCustomer);
            this.content.Controls.Add(this.txtAllCustomer);
            this.content.Controls.Add(this.dtpMonth);
            this.content.Controls.Add(this.dtpEndDate);
            this.content.Controls.Add(this.lblSTART_DATE);
            this.content.Controls.Add(this.dtpStartDate);
            this.content.Controls.Add(this.cboBillingCycle);
            this.content.Controls.Add(this.lblCUSTOMER_NO_USAGE);
            this.content.Controls.Add(this.lblCUSTOMER_USAGE);
            this.content.Controls.Add(this.lblTOTAL_CUSTOMER);
            this.content.Controls.Add(this.lblMONTH);
            this.content.Controls.Add(this.lblCYCLE_NAME);
            this.content.Controls.Add(this.btnClose);
            this.content.Controls.Add(this.btnOk);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnOk, 0);
            this.content.Controls.SetChildIndex(this.btnClose, 0);
            this.content.Controls.SetChildIndex(this.lblCYCLE_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblMONTH, 0);
            this.content.Controls.SetChildIndex(this.lblTOTAL_CUSTOMER, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_USAGE, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_NO_USAGE, 0);
            this.content.Controls.SetChildIndex(this.cboBillingCycle, 0);
            this.content.Controls.SetChildIndex(this.dtpStartDate, 0);
            this.content.Controls.SetChildIndex(this.lblSTART_DATE, 0);
            this.content.Controls.SetChildIndex(this.dtpEndDate, 0);
            this.content.Controls.SetChildIndex(this.dtpMonth, 0);
            this.content.Controls.SetChildIndex(this.txtAllCustomer, 0);
            this.content.Controls.SetChildIndex(this.txtHaveUsageCustomer, 0);
            this.content.Controls.SetChildIndex(this.txtNoUsageCustomer, 0);
            this.content.Controls.SetChildIndex(this.btnCHECK_1, 0);
            this.content.Controls.SetChildIndex(this.lblEND_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_UNKNOWN, 0);
            this.content.Controls.SetChildIndex(this.txtUnknownMeter, 0);
            this.content.Controls.SetChildIndex(this.btnCHECK_2, 0);
            this.content.Controls.SetChildIndex(this.panel2, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_UNREGISTER, 0);
            this.content.Controls.SetChildIndex(this.txtUnRegister, 0);
            this.content.Controls.SetChildIndex(this.btnCHECK_3, 0);
            this.content.Controls.SetChildIndex(this.lblDEVICE_CODE, 0);
            this.content.Controls.SetChildIndex(this.cboDevice, 0);
            this.content.Controls.SetChildIndex(this.btnSHOW_DETAIL, 0);
            // 
            // btnOk
            // 
            resources.ApplyResources(this.btnOk, "btnOk");
            this.btnOk.Name = "btnOk";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnClose
            // 
            resources.ApplyResources(this.btnClose, "btnClose");
            this.btnClose.Name = "btnClose";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblCYCLE_NAME
            // 
            resources.ApplyResources(this.lblCYCLE_NAME, "lblCYCLE_NAME");
            this.lblCYCLE_NAME.BackColor = System.Drawing.Color.Transparent;
            this.lblCYCLE_NAME.Name = "lblCYCLE_NAME";
            // 
            // lblMONTH
            // 
            resources.ApplyResources(this.lblMONTH, "lblMONTH");
            this.lblMONTH.BackColor = System.Drawing.Color.Transparent;
            this.lblMONTH.Name = "lblMONTH";
            // 
            // lblTOTAL_CUSTOMER
            // 
            resources.ApplyResources(this.lblTOTAL_CUSTOMER, "lblTOTAL_CUSTOMER");
            this.lblTOTAL_CUSTOMER.BackColor = System.Drawing.Color.Transparent;
            this.lblTOTAL_CUSTOMER.Name = "lblTOTAL_CUSTOMER";
            // 
            // lblCUSTOMER_USAGE
            // 
            resources.ApplyResources(this.lblCUSTOMER_USAGE, "lblCUSTOMER_USAGE");
            this.lblCUSTOMER_USAGE.BackColor = System.Drawing.Color.Transparent;
            this.lblCUSTOMER_USAGE.Name = "lblCUSTOMER_USAGE";
            // 
            // lblCUSTOMER_NO_USAGE
            // 
            resources.ApplyResources(this.lblCUSTOMER_NO_USAGE, "lblCUSTOMER_NO_USAGE");
            this.lblCUSTOMER_NO_USAGE.BackColor = System.Drawing.Color.Transparent;
            this.lblCUSTOMER_NO_USAGE.Name = "lblCUSTOMER_NO_USAGE";
            // 
            // cboBillingCycle
            // 
            this.cboBillingCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBillingCycle.FormattingEnabled = true;
            resources.ApplyResources(this.cboBillingCycle, "cboBillingCycle");
            this.cboBillingCycle.Name = "cboBillingCycle";
            this.cboBillingCycle.SelectedIndexChanged += new System.EventHandler(this.cboBillingCycle_SelectedIndexChanged);
            // 
            // dtpMonth
            // 
            resources.ApplyResources(this.dtpMonth, "dtpMonth");
            this.dtpMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMonth.Name = "dtpMonth";
            this.dtpMonth.ShowUpDown = true;
            // 
            // dtpEndDate
            // 
            resources.ApplyResources(this.dtpEndDate, "dtpEndDate");
            this.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndDate.Name = "dtpEndDate";
            // 
            // lblSTART_DATE
            // 
            resources.ApplyResources(this.lblSTART_DATE, "lblSTART_DATE");
            this.lblSTART_DATE.Name = "lblSTART_DATE";
            // 
            // dtpStartDate
            // 
            resources.ApplyResources(this.dtpStartDate, "dtpStartDate");
            this.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartDate.Name = "dtpStartDate";
            // 
            // txtAllCustomer
            // 
            resources.ApplyResources(this.txtAllCustomer, "txtAllCustomer");
            this.txtAllCustomer.Name = "txtAllCustomer";
            this.txtAllCustomer.ReadOnly = true;
            // 
            // txtHaveUsageCustomer
            // 
            resources.ApplyResources(this.txtHaveUsageCustomer, "txtHaveUsageCustomer");
            this.txtHaveUsageCustomer.Name = "txtHaveUsageCustomer";
            this.txtHaveUsageCustomer.ReadOnly = true;
            // 
            // txtNoUsageCustomer
            // 
            resources.ApplyResources(this.txtNoUsageCustomer, "txtNoUsageCustomer");
            this.txtNoUsageCustomer.Name = "txtNoUsageCustomer";
            this.txtNoUsageCustomer.ReadOnly = true;
            // 
            // btnCHECK_1
            // 
            resources.ApplyResources(this.btnCHECK_1, "btnCHECK_1");
            this.btnCHECK_1.Name = "btnCHECK_1";
            this.btnCHECK_1.UseVisualStyleBackColor = true;
            this.btnCHECK_1.Click += new System.EventHandler(this.btnCheckNoUsage_Click);
            // 
            // lblEND_DATE
            // 
            resources.ApplyResources(this.lblEND_DATE, "lblEND_DATE");
            this.lblEND_DATE.Name = "lblEND_DATE";
            // 
            // lblMETER_UNKNOWN
            // 
            resources.ApplyResources(this.lblMETER_UNKNOWN, "lblMETER_UNKNOWN");
            this.lblMETER_UNKNOWN.BackColor = System.Drawing.Color.Transparent;
            this.lblMETER_UNKNOWN.Name = "lblMETER_UNKNOWN";
            // 
            // btnCHECK_2
            // 
            resources.ApplyResources(this.btnCHECK_2, "btnCHECK_2");
            this.btnCHECK_2.Name = "btnCHECK_2";
            this.btnCHECK_2.UseVisualStyleBackColor = true;
            this.btnCHECK_2.Click += new System.EventHandler(this.btnCheckUnknown_Click);
            // 
            // txtUnknownMeter
            // 
            resources.ApplyResources(this.txtUnknownMeter, "txtUnknownMeter");
            this.txtUnknownMeter.Name = "txtUnknownMeter";
            this.txtUnknownMeter.ReadOnly = true;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // btnCHECK_3
            // 
            resources.ApplyResources(this.btnCHECK_3, "btnCHECK_3");
            this.btnCHECK_3.Name = "btnCHECK_3";
            this.btnCHECK_3.UseVisualStyleBackColor = true;
            this.btnCHECK_3.Click += new System.EventHandler(this.btnCheckUnregister_Click);
            // 
            // txtUnRegister
            // 
            resources.ApplyResources(this.txtUnRegister, "txtUnRegister");
            this.txtUnRegister.Name = "txtUnRegister";
            this.txtUnRegister.ReadOnly = true;
            // 
            // lblMETER_UNREGISTER
            // 
            resources.ApplyResources(this.lblMETER_UNREGISTER, "lblMETER_UNREGISTER");
            this.lblMETER_UNREGISTER.BackColor = System.Drawing.Color.Transparent;
            this.lblMETER_UNREGISTER.Name = "lblMETER_UNREGISTER";
            // 
            // cboDevice
            // 
            this.cboDevice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDevice.FormattingEnabled = true;
            resources.ApplyResources(this.cboDevice, "cboDevice");
            this.cboDevice.Name = "cboDevice";
            // 
            // lblDEVICE_CODE
            // 
            resources.ApplyResources(this.lblDEVICE_CODE, "lblDEVICE_CODE");
            this.lblDEVICE_CODE.BackColor = System.Drawing.Color.Transparent;
            this.lblDEVICE_CODE.Name = "lblDEVICE_CODE";
            // 
            // btnSHOW_DETAIL
            // 
            resources.ApplyResources(this.btnSHOW_DETAIL, "btnSHOW_DETAIL");
            this.btnSHOW_DETAIL.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnSHOW_DETAIL.Name = "btnSHOW_DETAIL";
            this.btnSHOW_DETAIL.TabStop = true;
            // 
            // DialogCollectUsageMetrologic
            // 
            resources.ApplyResources(this, "$this");
            this.Name = "DialogCollectUsageMetrologic";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnOk;
        private ExButton btnClose;
        private Label lblCYCLE_NAME;
        private Label lblTOTAL_CUSTOMER;
        private Label lblMONTH;
        private Label lblCUSTOMER_NO_USAGE;
        private Label lblCUSTOMER_USAGE;
        private ComboBox cboBillingCycle;
        private DateTimePicker dtpMonth;
        private DateTimePicker dtpEndDate;
        private Label lblSTART_DATE;
        private DateTimePicker dtpStartDate;
        private TextBox txtAllCustomer;
        private TextBox txtHaveUsageCustomer;
        private TextBox txtNoUsageCustomer;
        private ExButton btnCHECK_1;
        private Label lblEND_DATE;
        private ExButton btnCHECK_2;
        private TextBox txtUnknownMeter;
        private Label lblMETER_UNKNOWN;
        private Panel panel2;
        private ExButton btnCHECK_3;
        private TextBox txtUnRegister;
        private Label lblMETER_UNREGISTER;
        private ComboBox cboDevice;
        private Label lblDEVICE_CODE;
        private ExLinkLabel btnSHOW_DETAIL;
    }
}
