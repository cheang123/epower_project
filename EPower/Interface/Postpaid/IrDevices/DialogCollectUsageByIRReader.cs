﻿using EPower.HB02.REST;
using EPower.Logic;
using EPower.Logic.IRDevice;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogCollectUsageByIRReader : ExDialog
    {
        TransmissionOption _tranType;

        #region Private Data
        TBL_USAGE_DEVICE_COLLECTION objLog = null;
        string _strDeviceCode = "";
        #endregion Private Data 
        #region Constructor
        public DialogCollectUsageByIRReader(TransmissionOption tranType = TransmissionOption.WIRE)
        {
            InitializeComponent();
            _tranType = tranType;
            DBDataContext.Db.CommandTimeout = 3 * 60;
        }
        #endregion Constructor

        #region Event
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (_tranType == TransmissionOption.WIRE && txtSD_PATH.Text == "")
                wire();
            else if (_tranType == TransmissionOption.WIRE && txtSD_PATH.Text != "")
            {
                CollectSDCard();
            }
            else
                internet();
        }

        private void wire()
        {
            int totalCus = 0;
            int totalUnknown = 0;
            int totalUnregister = 0;
            int totalTampermeter = 0;
            var deviceCode = string.Empty;
            var deviceName = string.Empty;

            bool blnSuccess = false;
            Runner.RunNewThread(() =>
            {
                // connect to device 
                IRDevice objd = IRDevice.Connect();
                deviceCode = objd.ProductID;
                deviceName = objd.DeviceOEM;

                //validate device registration & employee
                IRDevice.Validate(objd);

                //Get file from device and backup
                Runner.Instance.Text = string.Format(Resources.MS_RECEIVING_FILE_FROM_DEVICE, string.Empty, string.Empty);
                objd.GetFile(IRDevice.BackupType.AfterCPFD);
                //Load data from device and save to database
                Runner.Instance.Text = Resources.MS_SAVE_USAGE;
                if (objd.HasSerial)
                {
                    var DeviceInfo = (from d in DBDataContext.Db.TBL_DEVICEs
                                      join ed in DBDataContext.Db.TBL_EMPLOYEE_DEVICEs on d.DEVICE_ID equals ed.DEVICE_ID
                                      where d.DEVICE_CODE == deviceCode
                                      select new { d.DEVICE_ID, ed.EMPLOYEE_ID }).FirstOrDefault();
                    objd.SaveUsage(DeviceInfo.DEVICE_ID, DeviceInfo.EMPLOYEE_ID);
                }
                else
                {
                    objd.SaveUsage(-1, -1);
                }

                totalCus = objd.TotalCollectCustomer;
                totalUnknown = objd.TotalUnknown;
                totalUnregister = objd.TotalUnregistered;
                totalTampermeter = objd.TotalTamperMeter;

                /*
                 * Log download usage transaction 
                 */
                var usageDeviceCollection = objd.UsageDeviceCollection;
                usageDeviceCollection.AREA = "";
                usageDeviceCollection.CYCLE = "";
                usageDeviceCollection.MONTH = "";
                usageDeviceCollection.DEVICE_CODE = deviceCode;
                usageDeviceCollection.DEVICE_NAME = deviceName;
                usageDeviceCollection.RECORD = totalCus;
                usageDeviceCollection.TRANSMISSION_TYPE_ID = (int)TransmissionOption.WIRE;
                usageDeviceCollection.DATA_BACKUP = Encoding.Unicode.GetBytes(new List<TMP_IR_USAGE>()._ToJson());
                objd.LogDownloadUsage(usageDeviceCollection);
                objLog = usageDeviceCollection;
                blnSuccess = true;
            });

            txtDeviceCode.Text = deviceCode;
            txtDeviceName.Text = deviceName;

            txtHaveUsageCustomer.Text = totalCus.ToString();
            txtUnknownMeter.Text = totalUnknown.ToString();
            txtUnRegister.Text = totalUnregister.ToString();
            txtMeterTamper.Text = totalTampermeter.ToString();

            //Successfully save data to db
            if (blnSuccess)
            {
                MsgBox.ShowInformation(Resources.DOWNLOAD_DATA_SUCCESS);
                var diag = new DialogCustomerSpecialUsage();
                diag.ShowDialog();
            }
        }

        private void internet()
        {
            var blnSuccess = false;
            var customer = 0;
            var tamper = 0;
            var android = new Android();
            var deviceName = "Online";
            var deviceCode = deviceName;
            Runner.RunNewThread(() =>
            {
                var pendingDownload = HB02_API.Instance.PendingDownload();
                using (var tran = new TransactionScope(TransactionScopeOption.Required, DBDataContext.Db.TransactionOption()))
                {
                    DBDataContext.Db.ExecuteCommand("TRUNCATE TABLE TMP_IR_USAGE");
                    foreach (var upload_id in pendingDownload)
                    {
                        Runner.Instance.Text = "Downloading...";
                        var json = HB02_API.Instance.Download(upload_id);
                        Runner.Instance.Text = "Importing...";
                        var usages = android.ToIrUsage(json);
                        var iUsage = usages.AsEnumerable<TMP_IR_USAGE>();
                        //var dt = usages._ToDataTable();
                        DBDataContext.Db.TMP_IR_USAGEs.InsertAllOnSubmit(iUsage);
                        DBDataContext.Db.SubmitChanges();
                        //DBDataContext.Db.Insert(dt, "dbo.TMP_IR_USAGE");
                        customer += usages.Count;
                        tamper += usages.Where(x => x.TAMPER_STATUS != "" && x.PHASE_ID == 3).Count();
                    }
                    android.RunIRUsage(-1, -1);
                    Runner.Instance.Text = "Validating...";
                    pendingDownload.ForEach(x => HB02_API.Instance.Success(x));
                    tran.Complete();
                }
                /*
                 * Log download usage transaction 
                 */
                var usageDeviceCollection = android.UsageDeviceCollection;
                usageDeviceCollection.AREA = "";
                usageDeviceCollection.CYCLE = "";
                usageDeviceCollection.MONTH = "";
                usageDeviceCollection.DEVICE_CODE = deviceCode;
                usageDeviceCollection.DEVICE_NAME = deviceName;
                usageDeviceCollection.RECORD = customer;
                usageDeviceCollection.TRANSMISSION_TYPE_ID = (int)TransmissionOption.Internet;
                usageDeviceCollection.DATA_BACKUP = Encoding.Unicode.GetBytes(new List<TMP_IR_USAGE>()._ToJson());
                android.LogDownloadUsage(usageDeviceCollection);
                objLog = usageDeviceCollection;
                blnSuccess = true;
            }, "Downloading...");
            txtDeviceCode.Text = "Online";
            txtDeviceName.Text = txtDeviceCode.Text;
            txtHaveUsageCustomer.Text = customer.ToString();
            txtUnknownMeter.Text = android.TotalUnknown.ToString();
            txtUnRegister.Text = android.TotalUnregistered.ToString();
            txtMeterTamper.Text = tamper.ToString();

            //Successfully save data to db
            if (blnSuccess)
            {
                MsgBox.ShowInformation(Resources.DOWNLOAD_DATA_SUCCESS);
                var diag = new DialogCustomerSpecialUsage();
                diag.ShowDialog();
            }
        }

        private void btnCheckUnknown_Click(object sender, EventArgs e)
        {
            DialogMeterUnknown objDialog = new DialogMeterUnknown();
            objDialog.ShowDialog();
        }

        private void btnCheckUnregister_Click(object sender, EventArgs e)
        {
            DialogMeterUnregister objDialog = new DialogMeterUnregister();
            objDialog.ShowDialog();
        }
        private void btnCheckTamper_Click(object sender, EventArgs e)
        {
            if (objLog != null)
            {
                DialogMeterTamper objDialog = new DialogMeterTamper(objLog);
                objDialog.ShowDialog();
            }
        }
        #endregion Event

        private void btnViewNotCollectCustomer_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var cycles = DBDataContext.Db.TBL_BILLING_CYCLEs.Where(x => x.IS_ACTIVE);
            int cycleId = cycles.Count() > 1 ? 0 : cycles.FirstOrDefault().CYCLE_ID;
            var diag = new DialogCustomerNoUsage(cycleId);
            diag.ShowDialog();
        }

        private void CollectSDCard()
        {
            System.Data.DataTable dt = new System.Data.DataTable();
            WinCE Win = new WinCE();
            bool blnSuccess = false;
            Runner.RunNewThread(() =>
            {
                int DeviceId = 1;
                int CollectorId = 1;
                Runner.Instance.Text = Resources.DOWNLOADING;
                if (!CopyFile())
                {
                    throw new Exception(Resources.MS_CAN_NOT_FIND_FILE_FOR_RECIEVE);
                }

                Runner.Instance.Text = Resources.MS_RUN_BILL_BACKUP;
                var DataBackUp = Win.BackUp(Application.StartupPath + @"\MobileDB\MobileDB.sdf", IRDevice.BackupType.AfterCPFD);

                Runner.Instance.Text = Resources.MS_PREPARING_DATA;
                CollectorId = DBDataContext.Db.TBL_EMPLOYEE_DEVICEs.Where(x => x.DEVICE_ID == DeviceId).Select(x => x.EMPLOYEE_ID).FirstOrDefault();
                dt = Win.SaveUsageFromSD(CollectorId == null ? 1 : CollectorId);
                blnSuccess = true;
            });

            /*
            * Log download usage transaction 
            */
            if (!blnSuccess) return;
            var usageDeviceCollection = Win.UsageDeviceCollection;
            usageDeviceCollection.AREA = "";
            usageDeviceCollection.CYCLE = "";
            usageDeviceCollection.MONTH = "";
            usageDeviceCollection.DEVICE_CODE = dt.Rows[0][2].ToString();
            usageDeviceCollection.DEVICE_NAME = Win.DeviceOEM ?? "";
            usageDeviceCollection.RECORD = Win.TotalCollectCustomer;
            usageDeviceCollection.TRANSMISSION_TYPE_ID = (int)TransmissionOption.SD;
            usageDeviceCollection.DATA_BACKUP = Encoding.Unicode.GetBytes(new List<TMP_IR_USAGE>()._ToJson());
            Win.LogDownloadUsage(usageDeviceCollection);
            Win.Complete();


            txtHaveUsageCustomer.Text = Win.TotalCollectCustomer.ToString();
            txtUnknownMeter.Text = Win.TotalUnknown.ToString();
            txtUnRegister.Text = Win.TotalUnregistered.ToString();
            txtMeterTamper.Text = Win.TotalTamperMeter.ToString();

            txtDeviceCode.Text = dt.Rows[0][2].ToString();
            txtDeviceName.Text = Win.DeviceOEM;

            //Successfully save data to db
            if (blnSuccess)
            {
                MsgBox.ShowInformation(Resources.DOWNLOAD_DATA_SUCCESS);
                var diag = new DialogCustomerSpecialUsage();
                diag.ShowDialog();
            }
        }

        private void btnBROWS_SD_PATH0_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog frm = new FolderBrowserDialog();
            if (frm.ShowDialog() == DialogResult.OK)
            {
                txtSD_PATH.Text = frm.SelectedPath;
            }
        }

        private bool CopyFile()
        {
            bool Result = false;
            string sourcePath = txtSD_PATH.Text;
            string targetPath = Application.StartupPath + @"\MobileDB\";
            if (!Directory.Exists(targetPath))
            {
                Directory.CreateDirectory(targetPath);
            }
            if (File.Exists(sourcePath + @"\EPower.IrMobile\MobileDB.sdf"))
            {
                File.Copy(sourcePath + @"\EPower.IrMobile\MobileDB.sdf", targetPath + @"\MobileDB.sdf", true);
                Result = true;
            }
            else if (File.Exists(sourcePath + @"\MobileDB.sdf"))
            {
                File.Copy(sourcePath + @"\MobileDB.sdf", targetPath + @"\MobileDB.sdf", true);
                Result = true;
            }
            else
            {
                Result = false;
            }
            return Result;
        }

        private void picHelp_MouseEnter(object sender, EventArgs e)
        {
            ToolTip tp = new ToolTip
            {
                AutoPopDelay = 15000,
                ShowAlways = true,
                OwnerDraw = true,
            };
            tp.SetToolTip(picHelp, Resources.MS_SD_PATH_HELPER);
            tp.Draw += new DrawToolTipEventHandler(toolTip1_Draw);
            tp.Popup += new PopupEventHandler(toolTip1_Popup);
        }

        void toolTip1_Popup(object sender, PopupEventArgs e)
        {
            e.ToolTipSize = TextRenderer.MeasureText(Resources.MS_SD_PATH_HELPER, new Font("Khmer OS System", 8.5f));
        }

        void toolTip1_Draw(object sender, DrawToolTipEventArgs e)
        {
            using (e.Graphics)
            {
                Font f = new Font("Khmer OS System", 8.5f);
                e.DrawBackground();
                e.DrawBorder();
                e.Graphics.DrawString(e.ToolTipText, f, Brushes.Black, new PointF(2, 2));

            }

        }

    }
}
