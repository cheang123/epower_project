﻿using EPower.HB02.Model;
using EPower.HB02.REST;
using EPower.Logic.IRDevice;
using SoftTech.Component;
using System;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogH203RegisterDevice : ExDialog
    {
        public DialogH203RegisterDevice()
        {
            InitializeComponent();
        }

        private void DialogH203RegisterDevice_Load(object sender, EventArgs e)
        {
            DatabaseInfor db = null;
            Runner.RunNewThread(() =>
            {
                db = HB02_API.Instance.DatabaseInformation();

            });
            if (db == null)
            {
                MsgBox.ShowWarning("ទិន្នន័យរបស់អ្នកមិនទាន់ត្រូវបានចុះបញ្ជីប្រើប្រាស់ជោគជ័យទេ សូមទំនាក់ទំនងអ្នកបច្ចេកទេស!", this.Text);
                return;
            }
            var android = new Android();

            lblREGISTER_CODE1.Text = db.access_key;
            if (db.code.ToUpper().Contains("DEMO"))
            {
                lblREGISTER_CODE1.Text = db.access_key + " " + db.code;
            }
            imgREGISTER.Image = android.RegisterQrCode(lblREGISTER_CODE1.Text);
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}