﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogCollectUsageByIRReader
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCollectUsageByIRReader));
            this.btnRECEIVE_DATA = new SoftTech.Component.ExButton();
            this.btnClose = new SoftTech.Component.ExButton();
            this.lblDEVICE_CODE = new System.Windows.Forms.Label();
            this.timeConnection = new System.Windows.Forms.Timer(this.components);
            this.lblDEVICE_NAME = new System.Windows.Forms.Label();
            this.lblCUSTOMER_USAGE = new System.Windows.Forms.Label();
            this.txtDeviceName = new System.Windows.Forms.TextBox();
            this.txtHaveUsageCustomer = new System.Windows.Forms.TextBox();
            this.lblMETER_UNKNOWN = new System.Windows.Forms.Label();
            this.btnCHECK = new SoftTech.Component.ExButton();
            this.txtUnknownMeter = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnCHECK_1 = new SoftTech.Component.ExButton();
            this.txtUnRegister = new System.Windows.Forms.TextBox();
            this.lblMETER_UNREGISTER = new System.Windows.Forms.Label();
            this.txtDeviceCode = new System.Windows.Forms.TextBox();
            this.btnCUSTOMER_NO_USAGE = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnBROWS_SD_PATH0 = new SoftTech.Component.ExButton();
            this.txtSD_PATH = new System.Windows.Forms.TextBox();
            this.lblSD_PATH = new System.Windows.Forms.Label();
            this.picHelp = new System.Windows.Forms.PictureBox();
            this.btnCHECK_2 = new SoftTech.Component.ExButton();
            this.txtMeterTamper = new System.Windows.Forms.TextBox();
            this.lblMETER_TAMPER = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picHelp)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.btnCHECK_2);
            this.content.Controls.Add(this.txtMeterTamper);
            this.content.Controls.Add(this.lblMETER_TAMPER);
            this.content.Controls.Add(this.picHelp);
            this.content.Controls.Add(this.btnBROWS_SD_PATH0);
            this.content.Controls.Add(this.txtSD_PATH);
            this.content.Controls.Add(this.lblSD_PATH);
            this.content.Controls.Add(this.btnCUSTOMER_NO_USAGE);
            this.content.Controls.Add(this.txtDeviceCode);
            this.content.Controls.Add(this.btnCHECK_1);
            this.content.Controls.Add(this.lblDEVICE_CODE);
            this.content.Controls.Add(this.txtUnRegister);
            this.content.Controls.Add(this.lblMETER_UNREGISTER);
            this.content.Controls.Add(this.panel2);
            this.content.Controls.Add(this.btnCHECK);
            this.content.Controls.Add(this.txtUnknownMeter);
            this.content.Controls.Add(this.lblMETER_UNKNOWN);
            this.content.Controls.Add(this.txtHaveUsageCustomer);
            this.content.Controls.Add(this.txtDeviceName);
            this.content.Controls.Add(this.lblCUSTOMER_USAGE);
            this.content.Controls.Add(this.lblDEVICE_NAME);
            this.content.Controls.Add(this.btnClose);
            this.content.Controls.Add(this.btnRECEIVE_DATA);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnRECEIVE_DATA, 0);
            this.content.Controls.SetChildIndex(this.btnClose, 0);
            this.content.Controls.SetChildIndex(this.lblDEVICE_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_USAGE, 0);
            this.content.Controls.SetChildIndex(this.txtDeviceName, 0);
            this.content.Controls.SetChildIndex(this.txtHaveUsageCustomer, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_UNKNOWN, 0);
            this.content.Controls.SetChildIndex(this.txtUnknownMeter, 0);
            this.content.Controls.SetChildIndex(this.btnCHECK, 0);
            this.content.Controls.SetChildIndex(this.panel2, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_UNREGISTER, 0);
            this.content.Controls.SetChildIndex(this.txtUnRegister, 0);
            this.content.Controls.SetChildIndex(this.lblDEVICE_CODE, 0);
            this.content.Controls.SetChildIndex(this.btnCHECK_1, 0);
            this.content.Controls.SetChildIndex(this.txtDeviceCode, 0);
            this.content.Controls.SetChildIndex(this.btnCUSTOMER_NO_USAGE, 0);
            this.content.Controls.SetChildIndex(this.lblSD_PATH, 0);
            this.content.Controls.SetChildIndex(this.txtSD_PATH, 0);
            this.content.Controls.SetChildIndex(this.btnBROWS_SD_PATH0, 0);
            this.content.Controls.SetChildIndex(this.picHelp, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_TAMPER, 0);
            this.content.Controls.SetChildIndex(this.txtMeterTamper, 0);
            this.content.Controls.SetChildIndex(this.btnCHECK_2, 0);
            // 
            // btnRECEIVE_DATA
            // 
            resources.ApplyResources(this.btnRECEIVE_DATA, "btnRECEIVE_DATA");
            this.btnRECEIVE_DATA.Name = "btnRECEIVE_DATA";
            this.btnRECEIVE_DATA.UseVisualStyleBackColor = true;
            this.btnRECEIVE_DATA.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnClose
            // 
            resources.ApplyResources(this.btnClose, "btnClose");
            this.btnClose.Name = "btnClose";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblDEVICE_CODE
            // 
            resources.ApplyResources(this.lblDEVICE_CODE, "lblDEVICE_CODE");
            this.lblDEVICE_CODE.BackColor = System.Drawing.Color.Transparent;
            this.lblDEVICE_CODE.Name = "lblDEVICE_CODE";
            // 
            // timeConnection
            // 
            this.timeConnection.Interval = 3000;
            // 
            // lblDEVICE_NAME
            // 
            resources.ApplyResources(this.lblDEVICE_NAME, "lblDEVICE_NAME");
            this.lblDEVICE_NAME.BackColor = System.Drawing.Color.Transparent;
            this.lblDEVICE_NAME.Name = "lblDEVICE_NAME";
            // 
            // lblCUSTOMER_USAGE
            // 
            resources.ApplyResources(this.lblCUSTOMER_USAGE, "lblCUSTOMER_USAGE");
            this.lblCUSTOMER_USAGE.BackColor = System.Drawing.Color.Transparent;
            this.lblCUSTOMER_USAGE.Name = "lblCUSTOMER_USAGE";
            // 
            // txtDeviceName
            // 
            resources.ApplyResources(this.txtDeviceName, "txtDeviceName");
            this.txtDeviceName.Name = "txtDeviceName";
            this.txtDeviceName.ReadOnly = true;
            // 
            // txtHaveUsageCustomer
            // 
            resources.ApplyResources(this.txtHaveUsageCustomer, "txtHaveUsageCustomer");
            this.txtHaveUsageCustomer.Name = "txtHaveUsageCustomer";
            this.txtHaveUsageCustomer.ReadOnly = true;
            // 
            // lblMETER_UNKNOWN
            // 
            resources.ApplyResources(this.lblMETER_UNKNOWN, "lblMETER_UNKNOWN");
            this.lblMETER_UNKNOWN.BackColor = System.Drawing.Color.Transparent;
            this.lblMETER_UNKNOWN.Name = "lblMETER_UNKNOWN";
            // 
            // btnCHECK
            // 
            resources.ApplyResources(this.btnCHECK, "btnCHECK");
            this.btnCHECK.Name = "btnCHECK";
            this.btnCHECK.UseVisualStyleBackColor = true;
            this.btnCHECK.Click += new System.EventHandler(this.btnCheckUnknown_Click);
            // 
            // txtUnknownMeter
            // 
            resources.ApplyResources(this.txtUnknownMeter, "txtUnknownMeter");
            this.txtUnknownMeter.Name = "txtUnknownMeter";
            this.txtUnknownMeter.ReadOnly = true;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // btnCHECK_1
            // 
            resources.ApplyResources(this.btnCHECK_1, "btnCHECK_1");
            this.btnCHECK_1.Name = "btnCHECK_1";
            this.btnCHECK_1.UseVisualStyleBackColor = true;
            this.btnCHECK_1.Click += new System.EventHandler(this.btnCheckUnregister_Click);
            // 
            // txtUnRegister
            // 
            resources.ApplyResources(this.txtUnRegister, "txtUnRegister");
            this.txtUnRegister.Name = "txtUnRegister";
            this.txtUnRegister.ReadOnly = true;
            // 
            // lblMETER_UNREGISTER
            // 
            resources.ApplyResources(this.lblMETER_UNREGISTER, "lblMETER_UNREGISTER");
            this.lblMETER_UNREGISTER.BackColor = System.Drawing.Color.Transparent;
            this.lblMETER_UNREGISTER.Name = "lblMETER_UNREGISTER";
            // 
            // txtDeviceCode
            // 
            resources.ApplyResources(this.txtDeviceCode, "txtDeviceCode");
            this.txtDeviceCode.Name = "txtDeviceCode";
            this.txtDeviceCode.ReadOnly = true;
            // 
            // btnCUSTOMER_NO_USAGE
            // 
            resources.ApplyResources(this.btnCUSTOMER_NO_USAGE, "btnCUSTOMER_NO_USAGE");
            this.btnCUSTOMER_NO_USAGE.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCUSTOMER_NO_USAGE.Name = "btnCUSTOMER_NO_USAGE";
            this.btnCUSTOMER_NO_USAGE.TabStop = true;
            this.btnCUSTOMER_NO_USAGE.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnViewNotCollectCustomer_LinkClicked);
            // 
            // btnBROWS_SD_PATH0
            // 
            resources.ApplyResources(this.btnBROWS_SD_PATH0, "btnBROWS_SD_PATH0");
            this.btnBROWS_SD_PATH0.Name = "btnBROWS_SD_PATH0";
            this.btnBROWS_SD_PATH0.UseVisualStyleBackColor = true;
            this.btnBROWS_SD_PATH0.Click += new System.EventHandler(this.btnBROWS_SD_PATH0_Click);
            // 
            // txtSD_PATH
            // 
            resources.ApplyResources(this.txtSD_PATH, "txtSD_PATH");
            this.txtSD_PATH.Name = "txtSD_PATH";
            this.txtSD_PATH.ReadOnly = true;
            // 
            // lblSD_PATH
            // 
            resources.ApplyResources(this.lblSD_PATH, "lblSD_PATH");
            this.lblSD_PATH.BackColor = System.Drawing.Color.Transparent;
            this.lblSD_PATH.Name = "lblSD_PATH";
            // 
            // picHelp
            // 
            this.picHelp.Cursor = System.Windows.Forms.Cursors.Default;
            this.picHelp.Image = global::EPower.Properties.Resources.question_mark;
            resources.ApplyResources(this.picHelp, "picHelp");
            this.picHelp.Name = "picHelp";
            this.picHelp.TabStop = false;
            this.picHelp.MouseEnter += new System.EventHandler(this.picHelp_MouseEnter);
            // 
            // btnCHECK_2
            // 
            resources.ApplyResources(this.btnCHECK_2, "btnCHECK_2");
            this.btnCHECK_2.Name = "btnCHECK_2";
            this.btnCHECK_2.UseVisualStyleBackColor = true;
            this.btnCHECK_2.Click += new System.EventHandler(this.btnCheckTamper_Click);
            // 
            // txtMeterTamper
            // 
            resources.ApplyResources(this.txtMeterTamper, "txtMeterTamper");
            this.txtMeterTamper.Name = "txtMeterTamper";
            this.txtMeterTamper.ReadOnly = true;
            // 
            // lblMETER_TAMPER
            // 
            resources.ApplyResources(this.lblMETER_TAMPER, "lblMETER_TAMPER");
            this.lblMETER_TAMPER.BackColor = System.Drawing.Color.Transparent;
            this.lblMETER_TAMPER.Name = "lblMETER_TAMPER";
            // 
            // DialogCollectUsageByIRReader
            // 
            resources.ApplyResources(this, "$this");
            this.Name = "DialogCollectUsageByIRReader";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picHelp)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnRECEIVE_DATA;
        private ExButton btnClose;
        private Timer timeConnection;
        private Label lblDEVICE_NAME;
        private Label lblCUSTOMER_USAGE;
        private TextBox txtDeviceName;
        private TextBox txtHaveUsageCustomer;
        private ExButton btnCHECK;
        private TextBox txtUnknownMeter;
        private Label lblMETER_UNKNOWN;
        private Panel panel2;
        private Label lblDEVICE_CODE;
        private ExButton btnCHECK_1;
        private TextBox txtUnRegister;
        private Label lblMETER_UNREGISTER;
        private TextBox txtDeviceCode;
        private ExLinkLabel btnCUSTOMER_NO_USAGE;
        private ExButton btnBROWS_SD_PATH0;
        private TextBox txtSD_PATH;
        private Label lblSD_PATH;
        private PictureBox picHelp;
        private ExButton btnCHECK_2;
        private TextBox txtMeterTamper;
        private Label lblMETER_TAMPER;
    }
}
