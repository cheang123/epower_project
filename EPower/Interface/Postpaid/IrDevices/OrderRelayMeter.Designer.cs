﻿using System.ComponentModel;
using System.Windows.Forms;

namespace EPower.Interface.Postpaid.IrDevices
{
    partial class OrderRelayMeter
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dgvListCustomer = new System.Windows.Forms.DataGridView();
            this.CUSTOMER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.METER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.METER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AREA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.POLE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BOX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DUE_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DUE_DAY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_DUE_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_SING_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.lblCUSTOMER_TO_BLOCK = new System.Windows.Forms.Label();
            this.dgvClose = new System.Windows.Forms.DataGridView();
            this.CLOSE_CUSTOMER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RELAY_METER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_ON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_CODE_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_NAME_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.METER_CODE_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AREA_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.POLE_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BOX_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblCUSTOMER_TO_OPEN = new System.Windows.Forms.Label();
            this.dgvOpen = new System.Windows.Forms.DataGridView();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OPEN_CUSTOMER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_CODE_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_NAME_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.METER_CODE_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AREA_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.POLE_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BOX_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblTotalCloseCustomer_ = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblTOTAL_CUSTOMER_TO_OPEN = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblTotalOpenCustomer_ = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblTOTAL_CUSTOMER_TO_CLOSE = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListCustomer)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOpen)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dgvListCustomer);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(699, 371);
            this.splitContainer1.SplitterDistance = 462;
            this.splitContainer1.TabIndex = 0;
            // 
            // dgvListCustomer
            // 
            this.dgvListCustomer.AllowDrop = true;
            this.dgvListCustomer.AllowUserToAddRows = false;
            this.dgvListCustomer.AllowUserToDeleteRows = false;
            this.dgvListCustomer.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvListCustomer.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvListCustomer.BackgroundColor = System.Drawing.Color.White;
            this.dgvListCustomer.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvListCustomer.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvListCustomer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListCustomer.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CUSTOMER_ID,
            this.METER_ID,
            this.CUSTOMER_CODE,
            this.CUSTOMER_NAME,
            this.METER_CODE,
            this.CUSTOMER_TYPE,
            this.AREA,
            this.POLE,
            this.BOX,
            this.DUE_DATE,
            this.DUE_DAY,
            this.TOTAL_DUE_AMOUNT,
            this.CURRENCY_SING_});
            this.dgvListCustomer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvListCustomer.EnableHeadersVisualStyles = false;
            this.dgvListCustomer.Location = new System.Drawing.Point(0, 0);
            this.dgvListCustomer.Name = "dgvListCustomer";
            this.dgvListCustomer.ReadOnly = true;
            this.dgvListCustomer.RowHeadersVisible = false;
            this.dgvListCustomer.RowHeadersWidth = 51;
            this.dgvListCustomer.RowTemplate.Height = 25;
            this.dgvListCustomer.Size = new System.Drawing.Size(462, 371);
            this.dgvListCustomer.TabIndex = 2;
            this.dgvListCustomer.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgvListCustomer_MouseDown);
            // 
            // CUSTOMER_ID
            // 
            this.CUSTOMER_ID.DataPropertyName = "CUSTOMER_ID";
            this.CUSTOMER_ID.HeaderText = "CUSTOMER_ID";
            this.CUSTOMER_ID.MinimumWidth = 6;
            this.CUSTOMER_ID.Name = "CUSTOMER_ID";
            this.CUSTOMER_ID.ReadOnly = true;
            this.CUSTOMER_ID.Visible = false;
            this.CUSTOMER_ID.Width = 125;
            // 
            // METER_ID
            // 
            this.METER_ID.DataPropertyName = "METER_ID";
            this.METER_ID.HeaderText = "METER_ID";
            this.METER_ID.MinimumWidth = 6;
            this.METER_ID.Name = "METER_ID";
            this.METER_ID.ReadOnly = true;
            this.METER_ID.Visible = false;
            this.METER_ID.Width = 125;
            // 
            // CUSTOMER_CODE
            // 
            this.CUSTOMER_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.CUSTOMER_CODE.DataPropertyName = "CUSTOMER_CODE";
            this.CUSTOMER_CODE.HeaderText = "លេខកូដ";
            this.CUSTOMER_CODE.MinimumWidth = 74;
            this.CUSTOMER_CODE.Name = "CUSTOMER_CODE";
            this.CUSTOMER_CODE.ReadOnly = true;
            this.CUSTOMER_CODE.Width = 95;
            // 
            // CUSTOMER_NAME
            // 
            this.CUSTOMER_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CUSTOMER_NAME.DataPropertyName = "FULL_NAME";
            this.CUSTOMER_NAME.HeaderText = "ឈ្មោះអតិថិជន";
            this.CUSTOMER_NAME.MinimumWidth = 150;
            this.CUSTOMER_NAME.Name = "CUSTOMER_NAME";
            this.CUSTOMER_NAME.ReadOnly = true;
            // 
            // METER_CODE
            // 
            this.METER_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.METER_CODE.DataPropertyName = "METER_CODE";
            this.METER_CODE.HeaderText = "លេខកុងទ័រ";
            this.METER_CODE.MinimumWidth = 86;
            this.METER_CODE.Name = "METER_CODE";
            this.METER_CODE.ReadOnly = true;
            this.METER_CODE.Width = 111;
            // 
            // CUSTOMER_TYPE
            // 
            this.CUSTOMER_TYPE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.CUSTOMER_TYPE.DataPropertyName = "CUSTOMER_TYPE_NAME";
            this.CUSTOMER_TYPE.HeaderText = "ប្រភេទ";
            this.CUSTOMER_TYPE.MinimumWidth = 66;
            this.CUSTOMER_TYPE.Name = "CUSTOMER_TYPE";
            this.CUSTOMER_TYPE.ReadOnly = true;
            this.CUSTOMER_TYPE.Width = 84;
            // 
            // AREA
            // 
            this.AREA.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.AREA.DataPropertyName = "AREA_CODE";
            this.AREA.HeaderText = "តំបន់";
            this.AREA.MinimumWidth = 58;
            this.AREA.Name = "AREA";
            this.AREA.ReadOnly = true;
            this.AREA.Width = 74;
            // 
            // POLE
            // 
            this.POLE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.POLE.DataPropertyName = "POLE_CODE";
            this.POLE.HeaderText = "បង្គោល";
            this.POLE.MinimumWidth = 70;
            this.POLE.Name = "POLE";
            this.POLE.ReadOnly = true;
            this.POLE.Width = 89;
            // 
            // BOX
            // 
            this.BOX.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.BOX.DataPropertyName = "BOX_CODE";
            this.BOX.HeaderText = "ប្រអប់";
            this.BOX.MinimumWidth = 62;
            this.BOX.Name = "BOX";
            this.BOX.ReadOnly = true;
            this.BOX.Width = 79;
            // 
            // DUE_DATE
            // 
            this.DUE_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.DUE_DATE.DataPropertyName = "DUE_DATE";
            dataGridViewCellStyle2.Format = "dd-MMM-yyyy";
            this.DUE_DATE.DefaultCellStyle = dataGridViewCellStyle2;
            this.DUE_DATE.HeaderText = "ថ្ងៃផុតកំណត់";
            this.DUE_DATE.MinimumWidth = 95;
            this.DUE_DATE.Name = "DUE_DATE";
            this.DUE_DATE.ReadOnly = true;
            this.DUE_DATE.Width = 122;
            // 
            // DUE_DAY
            // 
            this.DUE_DAY.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.DUE_DAY.DataPropertyName = "Day";
            this.DUE_DAY.HeaderText = "ចំនួន(ថ្ងៃ)";
            this.DUE_DAY.MinimumWidth = 78;
            this.DUE_DAY.Name = "DUE_DAY";
            this.DUE_DAY.ReadOnly = true;
            // 
            // TOTAL_DUE_AMOUNT
            // 
            this.TOTAL_DUE_AMOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.TOTAL_DUE_AMOUNT.DataPropertyName = "BALANCE_DUE";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N2";
            this.TOTAL_DUE_AMOUNT.DefaultCellStyle = dataGridViewCellStyle3;
            this.TOTAL_DUE_AMOUNT.HeaderText = "បំណុលសរុប";
            this.TOTAL_DUE_AMOUNT.MinimumWidth = 95;
            this.TOTAL_DUE_AMOUNT.Name = "TOTAL_DUE_AMOUNT";
            this.TOTAL_DUE_AMOUNT.ReadOnly = true;
            this.TOTAL_DUE_AMOUNT.Width = 121;
            // 
            // CURRENCY_SING_
            // 
            this.CURRENCY_SING_.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.CURRENCY_SING_.DataPropertyName = "CURRENCY_SING";
            this.CURRENCY_SING_.HeaderText = "";
            this.CURRENCY_SING_.MinimumWidth = 6;
            this.CURRENCY_SING_.Name = "CURRENCY_SING_";
            this.CURRENCY_SING_.ReadOnly = true;
            this.CURRENCY_SING_.Width = 6;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.lblCUSTOMER_TO_BLOCK);
            this.splitContainer2.Panel1.Controls.Add(this.dgvClose);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.lblCUSTOMER_TO_OPEN);
            this.splitContainer2.Panel2.Controls.Add(this.dgvOpen);
            this.splitContainer2.Size = new System.Drawing.Size(233, 371);
            this.splitContainer2.SplitterDistance = 175;
            this.splitContainer2.TabIndex = 0;
            // 
            // lblCUSTOMER_TO_BLOCK
            // 
            this.lblCUSTOMER_TO_BLOCK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCUSTOMER_TO_BLOCK.BackColor = System.Drawing.Color.White;
            this.lblCUSTOMER_TO_BLOCK.ForeColor = System.Drawing.Color.Black;
            this.lblCUSTOMER_TO_BLOCK.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblCUSTOMER_TO_BLOCK.Location = new System.Drawing.Point(0, 35);
            this.lblCUSTOMER_TO_BLOCK.Name = "lblCUSTOMER_TO_BLOCK";
            this.lblCUSTOMER_TO_BLOCK.Size = new System.Drawing.Size(233, 25);
            this.lblCUSTOMER_TO_BLOCK.TabIndex = 4;
            this.lblCUSTOMER_TO_BLOCK.Text = "អតិថិជនដែលត្រូវផ្តាច់ចរន្ត";
            this.lblCUSTOMER_TO_BLOCK.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dgvClose
            // 
            this.dgvClose.AllowDrop = true;
            this.dgvClose.AllowUserToAddRows = false;
            this.dgvClose.AllowUserToDeleteRows = false;
            this.dgvClose.AllowUserToResizeRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvClose.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvClose.BackgroundColor = System.Drawing.Color.White;
            this.dgvClose.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvClose.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvClose.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvClose.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CLOSE_CUSTOMER_ID,
            this.RELAY_METER_ID,
            this.Column1,
            this.CREATE_ON,
            this.Column2,
            this.Column4,
            this.Column3,
            this.Column5,
            this.Column6,
            this.CUSTOMER_CODE_1,
            this.CUSTOMER_NAME_1,
            this.METER_CODE_1,
            this.AREA_1,
            this.POLE_1,
            this.BOX_1});
            this.dgvClose.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvClose.EnableHeadersVisualStyles = false;
            this.dgvClose.Location = new System.Drawing.Point(0, 0);
            this.dgvClose.Name = "dgvClose";
            this.dgvClose.ReadOnly = true;
            this.dgvClose.RowHeadersVisible = false;
            this.dgvClose.RowHeadersWidth = 51;
            this.dgvClose.RowTemplate.Height = 25;
            this.dgvClose.Size = new System.Drawing.Size(233, 175);
            this.dgvClose.TabIndex = 3;
            this.dgvClose.DataSourceChanged += new System.EventHandler(this.dgvClose_DataSourceChanged);
            this.dgvClose.DragDrop += new System.Windows.Forms.DragEventHandler(this.dgvClose_DragDrop);
            this.dgvClose.DragEnter += new System.Windows.Forms.DragEventHandler(this.dgvClose_DragEnter);
            this.dgvClose.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvClose_KeyDown);
            // 
            // CLOSE_CUSTOMER_ID
            // 
            this.CLOSE_CUSTOMER_ID.DataPropertyName = "CUSTOMER_ID";
            this.CLOSE_CUSTOMER_ID.HeaderText = "CUSTOMER_ID";
            this.CLOSE_CUSTOMER_ID.MinimumWidth = 6;
            this.CLOSE_CUSTOMER_ID.Name = "CLOSE_CUSTOMER_ID";
            this.CLOSE_CUSTOMER_ID.ReadOnly = true;
            this.CLOSE_CUSTOMER_ID.Visible = false;
            this.CLOSE_CUSTOMER_ID.Width = 125;
            // 
            // RELAY_METER_ID
            // 
            this.RELAY_METER_ID.DataPropertyName = "RELAY_METER_ID";
            this.RELAY_METER_ID.HeaderText = "RELAY_METER_ID";
            this.RELAY_METER_ID.MinimumWidth = 6;
            this.RELAY_METER_ID.Name = "RELAY_METER_ID";
            this.RELAY_METER_ID.ReadOnly = true;
            this.RELAY_METER_ID.Visible = false;
            this.RELAY_METER_ID.Width = 125;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "METER_ID";
            this.Column1.HeaderText = "METER_ID";
            this.Column1.MinimumWidth = 6;
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Visible = false;
            this.Column1.Width = 125;
            // 
            // CREATE_ON
            // 
            this.CREATE_ON.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.CREATE_ON.DataPropertyName = "CREATE_ON";
            this.CREATE_ON.HeaderText = "CREATE_ON";
            this.CREATE_ON.MinimumWidth = 6;
            this.CREATE_ON.Name = "CREATE_ON";
            this.CREATE_ON.ReadOnly = true;
            this.CREATE_ON.Visible = false;
            this.CREATE_ON.Width = 125;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column2.DataPropertyName = "ORDER_RELAY_STATUS";
            this.Column2.HeaderText = "ORDER_RELAY_STATUS";
            this.Column2.MinimumWidth = 6;
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Visible = false;
            this.Column2.Width = 125;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column4.DataPropertyName = "SEND_ORDER_RELAY_METER_ID";
            this.Column4.HeaderText = "SEND_ORDER_RELAY_METER_ID";
            this.Column4.MinimumWidth = 6;
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Visible = false;
            this.Column4.Width = 125;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column3.DataPropertyName = "METER_RELAY_STATUS";
            this.Column3.HeaderText = "METER_RELAY_STATUS";
            this.Column3.MinimumWidth = 6;
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Visible = false;
            this.Column3.Width = 125;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "GET_ORDER_RELAY_METER_ID";
            this.Column5.HeaderText = "GET_ORDER_RELAY_METER_ID";
            this.Column5.MinimumWidth = 6;
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Visible = false;
            this.Column5.Width = 125;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "CHNAGE_ID";
            this.Column6.HeaderText = "CHNAGE_ID";
            this.Column6.MinimumWidth = 6;
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Visible = false;
            this.Column6.Width = 125;
            // 
            // CUSTOMER_CODE_1
            // 
            this.CUSTOMER_CODE_1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.CUSTOMER_CODE_1.DataPropertyName = "CUSTOMER_CODE";
            this.CUSTOMER_CODE_1.HeaderText = "លេខកូដ";
            this.CUSTOMER_CODE_1.MinimumWidth = 74;
            this.CUSTOMER_CODE_1.Name = "CUSTOMER_CODE_1";
            this.CUSTOMER_CODE_1.ReadOnly = true;
            this.CUSTOMER_CODE_1.Width = 95;
            // 
            // CUSTOMER_NAME_1
            // 
            this.CUSTOMER_NAME_1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CUSTOMER_NAME_1.DataPropertyName = "FULL_NAME";
            this.CUSTOMER_NAME_1.HeaderText = "ឈ្មោះអតិថិជន";
            this.CUSTOMER_NAME_1.MinimumWidth = 150;
            this.CUSTOMER_NAME_1.Name = "CUSTOMER_NAME_1";
            this.CUSTOMER_NAME_1.ReadOnly = true;
            // 
            // METER_CODE_1
            // 
            this.METER_CODE_1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.METER_CODE_1.DataPropertyName = "METER_CODE";
            this.METER_CODE_1.HeaderText = "លេខកុងទ័រ";
            this.METER_CODE_1.MinimumWidth = 85;
            this.METER_CODE_1.Name = "METER_CODE_1";
            this.METER_CODE_1.ReadOnly = true;
            this.METER_CODE_1.Width = 111;
            // 
            // AREA_1
            // 
            this.AREA_1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.AREA_1.DataPropertyName = "AREA_CODE";
            this.AREA_1.HeaderText = "តំបន់";
            this.AREA_1.MinimumWidth = 59;
            this.AREA_1.Name = "AREA_1";
            this.AREA_1.ReadOnly = true;
            this.AREA_1.Width = 74;
            // 
            // POLE_1
            // 
            this.POLE_1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.POLE_1.DataPropertyName = "POLE_CODE";
            this.POLE_1.HeaderText = "បង្គោល";
            this.POLE_1.MinimumWidth = 68;
            this.POLE_1.Name = "POLE_1";
            this.POLE_1.ReadOnly = true;
            this.POLE_1.Width = 89;
            // 
            // BOX_1
            // 
            this.BOX_1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.BOX_1.DataPropertyName = "BOX_CODE";
            this.BOX_1.HeaderText = "ប្រអប់";
            this.BOX_1.MinimumWidth = 62;
            this.BOX_1.Name = "BOX_1";
            this.BOX_1.ReadOnly = true;
            this.BOX_1.Width = 79;
            // 
            // lblCUSTOMER_TO_OPEN
            // 
            this.lblCUSTOMER_TO_OPEN.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCUSTOMER_TO_OPEN.BackColor = System.Drawing.Color.White;
            this.lblCUSTOMER_TO_OPEN.ForeColor = System.Drawing.Color.Black;
            this.lblCUSTOMER_TO_OPEN.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblCUSTOMER_TO_OPEN.Location = new System.Drawing.Point(3, 39);
            this.lblCUSTOMER_TO_OPEN.Name = "lblCUSTOMER_TO_OPEN";
            this.lblCUSTOMER_TO_OPEN.Size = new System.Drawing.Size(233, 25);
            this.lblCUSTOMER_TO_OPEN.TabIndex = 5;
            this.lblCUSTOMER_TO_OPEN.Text = "អតិថិជនដែលត្រូវភ្ជាប់ចរន្ត";
            this.lblCUSTOMER_TO_OPEN.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dgvOpen
            // 
            this.dgvOpen.AllowDrop = true;
            this.dgvOpen.AllowUserToAddRows = false;
            this.dgvOpen.AllowUserToDeleteRows = false;
            this.dgvOpen.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvOpen.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvOpen.BackgroundColor = System.Drawing.Color.White;
            this.dgvOpen.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvOpen.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvOpen.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOpen.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column14,
            this.OPEN_CUSTOMER_ID,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10,
            this.Column11,
            this.Column12,
            this.Column13,
            this.CUSTOMER_CODE_2,
            this.CUSTOMER_NAME_2,
            this.METER_CODE_2,
            this.AREA_2,
            this.POLE_2,
            this.BOX_2});
            this.dgvOpen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvOpen.EnableHeadersVisualStyles = false;
            this.dgvOpen.Location = new System.Drawing.Point(0, 0);
            this.dgvOpen.Name = "dgvOpen";
            this.dgvOpen.ReadOnly = true;
            this.dgvOpen.RowHeadersVisible = false;
            this.dgvOpen.RowHeadersWidth = 51;
            this.dgvOpen.RowTemplate.Height = 25;
            this.dgvOpen.Size = new System.Drawing.Size(233, 192);
            this.dgvOpen.TabIndex = 3;
            this.dgvOpen.DataSourceChanged += new System.EventHandler(this.dgvOpen_DataSourceChanged);
            this.dgvOpen.DragDrop += new System.Windows.Forms.DragEventHandler(this.dgvOpen_DragDrop);
            this.dgvOpen.DragEnter += new System.Windows.Forms.DragEventHandler(this.dgvClose_DragEnter);
            this.dgvOpen.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvOpen_KeyDown);
            // 
            // Column14
            // 
            this.Column14.DataPropertyName = "RELAY_METER_ID";
            this.Column14.HeaderText = "RELAY_METER_ID";
            this.Column14.MinimumWidth = 6;
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Visible = false;
            this.Column14.Width = 125;
            // 
            // OPEN_CUSTOMER_ID
            // 
            this.OPEN_CUSTOMER_ID.DataPropertyName = "CUSTOMER_ID";
            this.OPEN_CUSTOMER_ID.HeaderText = "CUSTOMER_ID";
            this.OPEN_CUSTOMER_ID.MinimumWidth = 6;
            this.OPEN_CUSTOMER_ID.Name = "OPEN_CUSTOMER_ID";
            this.OPEN_CUSTOMER_ID.ReadOnly = true;
            this.OPEN_CUSTOMER_ID.Visible = false;
            this.OPEN_CUSTOMER_ID.Width = 125;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "METER_ID";
            this.Column7.HeaderText = "METER_ID";
            this.Column7.MinimumWidth = 6;
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Visible = false;
            this.Column7.Width = 125;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "CREATE_ON";
            this.Column8.HeaderText = "CREATE_ON";
            this.Column8.MinimumWidth = 6;
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Visible = false;
            this.Column8.Width = 125;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "ORDER_RELAY_STATUS";
            this.Column9.HeaderText = "ORDER_RELAY_STATUS";
            this.Column9.MinimumWidth = 6;
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Visible = false;
            this.Column9.Width = 125;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "METER_RELAY_STATUS";
            this.Column10.HeaderText = "METER_RELAY_STATUS";
            this.Column10.MinimumWidth = 6;
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Visible = false;
            this.Column10.Width = 125;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "SEND_ORDER_RELAY_METER_ID";
            this.Column11.HeaderText = "SEND_ORDER_RELAY_METER_ID";
            this.Column11.MinimumWidth = 6;
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Visible = false;
            this.Column11.Width = 125;
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "GET_ORDER_RELAY_METER_ID";
            this.Column12.HeaderText = "GET_ORDER_RELAY_METER_ID";
            this.Column12.MinimumWidth = 6;
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Visible = false;
            this.Column12.Width = 125;
            // 
            // Column13
            // 
            this.Column13.DataPropertyName = "CHNAGE_ID";
            this.Column13.HeaderText = "CHNAGE_ID";
            this.Column13.MinimumWidth = 6;
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            this.Column13.Visible = false;
            this.Column13.Width = 125;
            // 
            // CUSTOMER_CODE_2
            // 
            this.CUSTOMER_CODE_2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.CUSTOMER_CODE_2.DataPropertyName = "CUSTOMER_CODE";
            this.CUSTOMER_CODE_2.HeaderText = "លេខកូដ";
            this.CUSTOMER_CODE_2.MinimumWidth = 74;
            this.CUSTOMER_CODE_2.Name = "CUSTOMER_CODE_2";
            this.CUSTOMER_CODE_2.ReadOnly = true;
            this.CUSTOMER_CODE_2.Width = 95;
            // 
            // CUSTOMER_NAME_2
            // 
            this.CUSTOMER_NAME_2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CUSTOMER_NAME_2.DataPropertyName = "FULL_NAME";
            this.CUSTOMER_NAME_2.HeaderText = "ឈ្មោះអតិថិជន";
            this.CUSTOMER_NAME_2.MinimumWidth = 150;
            this.CUSTOMER_NAME_2.Name = "CUSTOMER_NAME_2";
            this.CUSTOMER_NAME_2.ReadOnly = true;
            // 
            // METER_CODE_2
            // 
            this.METER_CODE_2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.METER_CODE_2.DataPropertyName = "METER_CODE";
            this.METER_CODE_2.HeaderText = "លេខកុងទ័រ";
            this.METER_CODE_2.MinimumWidth = 85;
            this.METER_CODE_2.Name = "METER_CODE_2";
            this.METER_CODE_2.ReadOnly = true;
            this.METER_CODE_2.Width = 111;
            // 
            // AREA_2
            // 
            this.AREA_2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.AREA_2.DataPropertyName = "AREA_CODE";
            this.AREA_2.HeaderText = "តំបន់";
            this.AREA_2.MinimumWidth = 59;
            this.AREA_2.Name = "AREA_2";
            this.AREA_2.ReadOnly = true;
            this.AREA_2.Width = 74;
            // 
            // POLE_2
            // 
            this.POLE_2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.POLE_2.DataPropertyName = "POLE_CODE";
            this.POLE_2.HeaderText = "បង្គោល";
            this.POLE_2.MinimumWidth = 68;
            this.POLE_2.Name = "POLE_2";
            this.POLE_2.ReadOnly = true;
            this.POLE_2.Width = 89;
            // 
            // BOX_2
            // 
            this.BOX_2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.BOX_2.DataPropertyName = "BOX_CODE";
            this.BOX_2.HeaderText = "ប្រអប់";
            this.BOX_2.MinimumWidth = 62;
            this.BOX_2.Name = "BOX_2";
            this.BOX_2.ReadOnly = true;
            this.BOX_2.Width = 79;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblTotalCloseCustomer_,
            this.lblTOTAL_CUSTOMER_TO_OPEN,
            this.toolStripStatusLabel3,
            this.lblTotalOpenCustomer_,
            this.lblTOTAL_CUSTOMER_TO_CLOSE});
            this.statusStrip1.Location = new System.Drawing.Point(0, 371);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.statusStrip1.Size = new System.Drawing.Size(699, 31);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblTotalCloseCustomer_
            // 
            this.lblTotalCloseCustomer_.BackColor = System.Drawing.Color.Transparent;
            this.lblTotalCloseCustomer_.ForeColor = System.Drawing.Color.Red;
            this.lblTotalCloseCustomer_.Name = "lblTotalCloseCustomer_";
            this.lblTotalCloseCustomer_.Size = new System.Drawing.Size(20, 25);
            this.lblTotalCloseCustomer_.Text = "0";
            // 
            // lblTOTAL_CUSTOMER_TO_OPEN
            // 
            this.lblTOTAL_CUSTOMER_TO_OPEN.BackColor = System.Drawing.Color.Transparent;
            this.lblTOTAL_CUSTOMER_TO_OPEN.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.lblTOTAL_CUSTOMER_TO_OPEN.Name = "lblTOTAL_CUSTOMER_TO_OPEN";
            this.lblTOTAL_CUSTOMER_TO_OPEN.Size = new System.Drawing.Size(181, 25);
            this.lblTOTAL_CUSTOMER_TO_OPEN.Text = "សរុបអតិថិជនត្រូវផ្តាច់ចរន្ត៖";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(15, 25);
            this.toolStripStatusLabel3.Text = "|";
            // 
            // lblTotalOpenCustomer_
            // 
            this.lblTotalOpenCustomer_.BackColor = System.Drawing.Color.Transparent;
            this.lblTotalOpenCustomer_.ForeColor = System.Drawing.Color.Blue;
            this.lblTotalOpenCustomer_.Name = "lblTotalOpenCustomer_";
            this.lblTotalOpenCustomer_.Size = new System.Drawing.Size(20, 25);
            this.lblTotalOpenCustomer_.Text = "0";
            // 
            // lblTOTAL_CUSTOMER_TO_CLOSE
            // 
            this.lblTOTAL_CUSTOMER_TO_CLOSE.BackColor = System.Drawing.Color.Transparent;
            this.lblTOTAL_CUSTOMER_TO_CLOSE.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.lblTOTAL_CUSTOMER_TO_CLOSE.Name = "lblTOTAL_CUSTOMER_TO_CLOSE";
            this.lblTOTAL_CUSTOMER_TO_CLOSE.Size = new System.Drawing.Size(181, 25);
            this.lblTOTAL_CUSTOMER_TO_CLOSE.Text = "សរុបអតិថិជនត្រូវភ្ជាប់ចរន្ត៖";
            // 
            // OrderRelayMeter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.statusStrip1);
            this.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "OrderRelayMeter";
            this.Size = new System.Drawing.Size(699, 402);
            this.Load += new System.EventHandler(this.OrderRelayMeter_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListCustomer)).EndInit();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOpen)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private SplitContainer splitContainer1;
        private Label lblCUSTOMER_TO_BLOCK;
        private DataGridView dgvListCustomer;
        private SplitContainer splitContainer2;
        private DataGridView dgvClose;
        private Label lblCUSTOMER_TO_OPEN;
        private DataGridView dgvOpen;
        private StatusStrip statusStrip1;
        private ToolStripStatusLabel lblTOTAL_CUSTOMER_TO_OPEN;
        private ToolStripStatusLabel lblTotalCloseCustomer_;
        private ToolStripStatusLabel toolStripStatusLabel3;
        private ToolStripStatusLabel lblTOTAL_CUSTOMER_TO_CLOSE;
        private ToolStripStatusLabel lblTotalOpenCustomer_;
        private DataGridViewTextBoxColumn CUSTOMER_ID;
        private DataGridViewTextBoxColumn METER_ID;
        private DataGridViewTextBoxColumn CUSTOMER_CODE;
        private DataGridViewTextBoxColumn CUSTOMER_NAME;
        private DataGridViewTextBoxColumn METER_CODE;
        private DataGridViewTextBoxColumn CUSTOMER_TYPE;
        private DataGridViewTextBoxColumn AREA;
        private DataGridViewTextBoxColumn POLE;
        private DataGridViewTextBoxColumn BOX;
        private DataGridViewTextBoxColumn DUE_DATE;
        private DataGridViewTextBoxColumn DUE_DAY;
        private DataGridViewTextBoxColumn TOTAL_DUE_AMOUNT;
        private DataGridViewTextBoxColumn CURRENCY_SING_;
        private DataGridViewTextBoxColumn CLOSE_CUSTOMER_ID;
        private DataGridViewTextBoxColumn RELAY_METER_ID;
        private DataGridViewTextBoxColumn Column1;
        private DataGridViewTextBoxColumn CREATE_ON;
        private DataGridViewTextBoxColumn Column2;
        private DataGridViewTextBoxColumn Column4;
        private DataGridViewTextBoxColumn Column3;
        private DataGridViewTextBoxColumn Column5;
        private DataGridViewTextBoxColumn Column6;
        private DataGridViewTextBoxColumn CUSTOMER_CODE_1;
        private DataGridViewTextBoxColumn CUSTOMER_NAME_1;
        private DataGridViewTextBoxColumn METER_CODE_1;
        private DataGridViewTextBoxColumn AREA_1;
        private DataGridViewTextBoxColumn POLE_1;
        private DataGridViewTextBoxColumn BOX_1;
        private DataGridViewTextBoxColumn Column14;
        private DataGridViewTextBoxColumn OPEN_CUSTOMER_ID;
        private DataGridViewTextBoxColumn Column7;
        private DataGridViewTextBoxColumn Column8;
        private DataGridViewTextBoxColumn Column9;
        private DataGridViewTextBoxColumn Column10;
        private DataGridViewTextBoxColumn Column11;
        private DataGridViewTextBoxColumn Column12;
        private DataGridViewTextBoxColumn Column13;
        private DataGridViewTextBoxColumn CUSTOMER_CODE_2;
        private DataGridViewTextBoxColumn CUSTOMER_NAME_2;
        private DataGridViewTextBoxColumn METER_CODE_2;
        private DataGridViewTextBoxColumn AREA_2;
        private DataGridViewTextBoxColumn POLE_2;
        private DataGridViewTextBoxColumn BOX_2;

    }
}
