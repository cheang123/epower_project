﻿using EPower.Properties;
using SoftTech;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageUsageH203 : Form
    {
        Dictionary<int,string> transferType = new Dictionary<int, string>();
        Dictionary<int, string> transmissionType = new Dictionary<int, string>();
        public PageUsageH203()
        {
            InitializeComponent();
            ///*
            // * Register Service
            // */
            //new HB02_API("http://amr.e-power.com.kh:50005", "ep", Method.Utilities[Utility.IR_H203_ACTIVATE_CODE]);
            UIHelper.DataGridViewProperties(dgv);
            CREATE_ON.DefaultCellStyle.Format = UIHelper._DefaultShortDatetime;
            dtp1.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            dtp2.Value = dtp1.Value.AddMonths(1).AddDays(-1);
            transferType[(int)TransferType.UPLOAD_USAGE] = Resources.SEND_DATA;
            transferType[(int)TransferType.DOWNLOD_USAGE] = Resources.RECEIVE_DATA;
            transmissionType[(int)TransmissionOption.WIRE] = Resources.WIRE;
            transmissionType[(int)TransmissionOption.Internet] = Resources.INTERNET;
            transmissionType[(int)TransmissionOption.GPRS] = Resources.GPRS;
            UIHelper.SetDataSourceToComboBox(cboGroup,transferType.Select(x=>new { x.Key, x.Value }), "");
            bind();

            cboGroup.SelectedIndexChanged += txtQuickSearch_QuickSearch;
            dtp1.ValueChanged += txtQuickSearch_QuickSearch;
            dtp2.ValueChanged += txtQuickSearch_QuickSearch;
        }

        void bind()
        {
            var transfer = 0;
            if (cboGroup.SelectedIndex!=-1)
            {
                transfer = (int)cboGroup.SelectedValue;
            }

            var deviceCollections = from dc in DBDataContext.Db.TBL_USAGE_DEVICE_COLLECTIONs
                                    where (dc.TRANSFER_TYPE_ID == transfer || transfer == 0)
                                    && dc.CREATE_ON>=dtp1.Value
                                    && dc.CREATE_ON<=dtp2.Value
                                    && dc.TRANSMISSION_TYPE_ID == (int)TransmissionOption.Internet
                                    select new
                                    {
                                        dc.USAGE_DEVICE_COLLECTION_ID,
                                        TRANSFER_TYPE = transferType[dc.TRANSFER_TYPE_ID],
                                        TRANSMISSION_TYPE = transmissionType[ dc.TRANSMISSION_TYPE_ID],
                                        dc.CREATE_BY,
                                        dc.CREATE_ON,
                                        dc.DEVICE_CODE,
                                        dc.DEVICE_NAME,
                                        CYCLE_NAME= dc.CYCLE,
                                        AREA_NAME = dc.AREA,
                                        dc.MONTH,
                                        dc.RECORD
                                    };
            dgv.DataSource = deviceCollections;
        }

        private void btnUploadUsage_Click(object sender, EventArgs e)
        {
            var dig = new DialogImportDataToIRReader(TransmissionOption.Internet);
            dig.ShowDialog();
            bind();
        }

        private void btnDownloadUsage_Click(object sender, EventArgs e)
        {
            var dig = new DialogCollectUsageByIRReader(TransmissionOption.Internet);
            dig.ShowDialog();
            bind();
        }

        private void txtQuickSearch_QuickSearch(object sender, EventArgs e)
        {
            bind();
        }

        private void btnREGISTER_DEVICE_Click(object sender, EventArgs e)
        {
            var dig = new DialogH203RegisterDevice();
            dig.ShowDialog();
        }

        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgv.SelectedRows.Count ==0 || e.ColumnIndex != dgv.Columns[RECORD.Name].Index)
            {
                return;
            }

            var deviceUsage = DBDataContext.Db.TBL_USAGE_DEVICE_COLLECTIONs.FirstOrDefault(x => x.USAGE_DEVICE_COLLECTION_ID == (int)dgv.SelectedRows[0].Cells[USAGE_DEVICE_COLLECTION_ID.Name].Value);
            var dig = new DialogUsageDeviceCollection(deviceUsage);
            dig.ShowDialog();
        }
    }
}
