﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogDownloadRef
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogDownloadRef));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnClose1 = new SoftTech.Component.ExButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.txtCycle = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dgvService = new System.Windows.Forms.DataGridView();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.SERVICE1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QTY1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRICE1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LAST_BILLING_MONTH1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RECURRING_MONTH1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvService)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.linkLabel1);
            this.content.Controls.Add(this.dgvService);
            this.content.Controls.Add(this.label2);
            this.content.Controls.Add(this.txtCycle);
            this.content.Controls.Add(this.btnClose1);
            this.content.Controls.Add(this.panel1);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.btnClose1, 0);
            this.content.Controls.SetChildIndex(this.txtCycle, 0);
            this.content.Controls.SetChildIndex(this.label2, 0);
            this.content.Controls.SetChildIndex(this.dgvService, 0);
            this.content.Controls.SetChildIndex(this.linkLabel1, 0);
            // 
            // btnClose1
            // 
            resources.ApplyResources(this.btnClose1, "btnClose1");
            this.btnClose1.Name = "btnClose1";
            this.btnClose1.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.BackgroundImage = global::EPower.Properties.Resources.dock;
            resources.ApplyResources(this.panel4, "panel4");
            this.panel4.Name = "panel4";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // checkBox1
            // 
            resources.ApplyResources(this.checkBox1, "checkBox1");
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // txtCycle
            // 
            resources.ApplyResources(this.txtCycle, "txtCycle");
            this.txtCycle.Name = "txtCycle";
            this.txtCycle.ReadOnly = true;
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Name = "label2";
            // 
            // dgvService
            // 
            this.dgvService.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvService.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvService.BackgroundColor = System.Drawing.Color.White;
            this.dgvService.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvService.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvService.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvService.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SERVICE1,
            this.QTY1,
            this.PRICE1,
            this.LAST_BILLING_MONTH1,
            this.RECURRING_MONTH1});
            this.dgvService.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgvService, "dgvService");
            this.dgvService.Name = "dgvService";
            this.dgvService.RowHeadersVisible = false;
            this.dgvService.RowTemplate.Height = 25;
            this.dgvService.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // linkLabel1
            // 
            resources.ApplyResources(this.linkLabel1, "linkLabel1");
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.TabStop = true;
            // 
            // SERVICE1
            // 
            this.SERVICE1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.SERVICE1.DataPropertyName = "INVOICE_ITEM_NAME";
            resources.ApplyResources(this.SERVICE1, "SERVICE1");
            this.SERVICE1.Name = "SERVICE1";
            // 
            // QTY1
            // 
            this.QTY1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.QTY1.DataPropertyName = "QTY";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Format = "N2";
            this.QTY1.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.QTY1, "QTY1");
            this.QTY1.Name = "QTY1";
            // 
            // PRICE1
            // 
            this.PRICE1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PRICE1.DataPropertyName = "PRICE";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.PRICE1.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.PRICE1, "PRICE1");
            this.PRICE1.Name = "PRICE1";
            // 
            // LAST_BILLING_MONTH1
            // 
            this.LAST_BILLING_MONTH1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.LAST_BILLING_MONTH1.DataPropertyName = "LAST_BILLING_MONTH";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "MM-yyyy";
            this.LAST_BILLING_MONTH1.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.LAST_BILLING_MONTH1, "LAST_BILLING_MONTH1");
            this.LAST_BILLING_MONTH1.Name = "LAST_BILLING_MONTH1";
            // 
            // RECURRING_MONTH1
            // 
            this.RECURRING_MONTH1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.RECURRING_MONTH1.DataPropertyName = "RECURRING_MONTH";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.Format = "#";
            this.RECURRING_MONTH1.DefaultCellStyle = dataGridViewCellStyle5;
            resources.ApplyResources(this.RECURRING_MONTH1, "RECURRING_MONTH1");
            this.RECURRING_MONTH1.Name = "RECURRING_MONTH1";
            // 
            // DialogDownloadRef
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogDownloadRef";
            this.TopMost = true;
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvService)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnClose1;
        private Panel panel1;
        private Panel panel4;
        
        private Label label1;
        private CheckBox checkBox1;
        private TextBox txtCycle;
        private Label label2;
        private DataGridView dgvService;
        private LinkLabel linkLabel1;
        private DataGridViewTextBoxColumn SERVICE1;
        private DataGridViewTextBoxColumn QTY1;
        private DataGridViewTextBoxColumn PRICE1;
        private DataGridViewTextBoxColumn LAST_BILLING_MONTH1;
        private DataGridViewTextBoxColumn RECURRING_MONTH1;
    }
}