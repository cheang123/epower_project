﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Windows.Forms;

namespace EPower.Interface.Postpaid.IrDevices
{
    public partial class QueryOrderRelayMeter : UserControl
    {
        private static QueryOrderRelayMeter self = new QueryOrderRelayMeter();
        public static QueryOrderRelayMeter Instant { get { return self; } }

        string contentSearch = "";

        public QueryOrderRelayMeter()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
            UIHelper.SetDataSourceToComboBox(this.cboCustomerType, Lookup.GetCustomerTypes(), Resources.ALL_TYPE);
            UIHelper.SetDataSourceToComboBox(this.cboArea, Lookup.GetAreas(), Resources.ALL_AREA);
            UIHelper.SetDataSourceToComboBox(this.cboBillingCycle, Lookup.GetBillingCycles(), Resources.ALL_CYCLE);
            chkCUSTOMER_TO_BLOCK.Checked = true;
            this.btnSEND_DATA_TO_INTERNET.Visible =
            this.btnRECEIVE_DATA_INTERNET.Visible = !string.IsNullOrEmpty(Method.Utilities[Utility.IR_H203_ACTIVATE_CODE]);
        }

        public void search(string strSearch)
        {
            int ctype = cboCustomerType.SelectedIndex > -1 ? (int)cboCustomerType.SelectedValue : 0;
            int area = cboArea.SelectedIndex > -1 ? (int)cboArea.SelectedValue : 0;
            int cycle = cboBillingCycle.SelectedIndex > -1 ? (int)cboBillingCycle.SelectedValue : 0;
            int take = (int)txtTake.Value;
            bool all = !chkCUSTOMER_TO_BLOCK.Checked;
            OrderRelayMeter.Instant.Search(strSearch, area, ctype, cycle, take, all);
            contentSearch = strSearch;
        }

        private void cbo_SelectedIndexChanged(object sender, EventArgs e)
        {
            search(contentSearch);
        }

        private void chkCloseCus_CheckedChanged(object sender, EventArgs e)
        {
            search(contentSearch);
        }

        private void btnSendData_Click(object sender, EventArgs e)
        {
            try
            {
                OrderRelayMeter.Instant.SendOrderRelayToIR(false);
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp);
            }

        }

        private void btnSEND_DATA_TO_INTERNET_Click(object sender, EventArgs e)
        {
            try
            {
                OrderRelayMeter.Instant.SendOrderRelayToIR(true);
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp);
            }
        }

        private void btnRECEIVE_DATA_Click(object sender, EventArgs e)
        {
            OrderRelayMeter.Instant.ReceiveOrderRelayFromIR(false);
        }

        private void btnRECEIVE_DATA_INTERNET_Click(object sender, EventArgs e)
        {
            OrderRelayMeter.Instant.ReceiveOrderRelayFromIR(true);
        }
    }
}
