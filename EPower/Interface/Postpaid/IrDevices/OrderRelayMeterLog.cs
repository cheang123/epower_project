﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Helper;

namespace EPower.Interface.Postpaid.IrDevices
{
    public partial class OrderRelayMeterLog : UserControl
    {
        public enum RelayOperation
        {
            All = 0,
            Send = 1, Receive = 2
        }

        public Dictionary<RelayOperation, string> Operation = new Dictionary<RelayOperation, string>()
        {
            {RelayOperation.All, Resources.ALL_TRANSACTION},
            {RelayOperation.Send,Resources.SEND_DATA},
            {RelayOperation.Receive,Resources.RECEIVE_DATA}
        };

        private static OrderRelayMeterLog self = new OrderRelayMeterLog();
        public static OrderRelayMeterLog Instant { get { return self; } }

        private OrderRelayMeterLog()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
            UIHelper.DataGridViewProperties(this.dgv);
        }

        public void Search(RelayOperation ro = RelayOperation.All, DateTime d1 = new DateTime(), DateTime d2 = new DateTime())
        {
            if (d1 == new DateTime())
            {
                d1 = DateTime.Now;
            }
            if (d2 == new DateTime())
            {
                d2 = DateTime.Now;
            }

            var rowSend = from o in DBDataContext.Db.TBL_ORDER_RELAY_METERs
                          join d in DBDataContext.Db.TBL_DEVICEs on o.DEVICE_ID equals d.DEVICE_ID
                          join s in DBDataContext.Db.TBL_RELAY_METERs on o.ORDER_RELAY_METER_ID equals s.SEND_ORDER_RELAY_METER_ID
                          where o.CREATE_ON.Date.Date >= d1.Date && o.CREATE_ON.Date <= d2.Date
                          group s by new { o.ORDER_RELAY_METER_ID, d.DEVICE_CODE, o.CREATE_ON, o.CREATE_BY } into gs
                          select new
                          {
                              gs.Key.ORDER_RELAY_METER_ID,
                              gs.Key.DEVICE_CODE,
                              gs.Key.CREATE_ON,
                              gs.Key.CREATE_BY,
                              TOTAL_CUSTOMER = gs.Count(),
                              USER_OPERATION_ID = 1,
                              USER_OPERATION = Operation[RelayOperation.Send]
                          };
            var rowGet = from o in DBDataContext.Db.TBL_ORDER_RELAY_METERs
                         join d in DBDataContext.Db.TBL_DEVICEs on o.DEVICE_ID equals d.DEVICE_ID
                         join s in DBDataContext.Db.TBL_RELAY_METERs on o.ORDER_RELAY_METER_ID equals s.GET_ORDER_RELAY_METER_ID
                         where o.CREATE_ON.Date >= d1.Date && o.CREATE_ON.Date <= d2.Date
                         group s by new { o.ORDER_RELAY_METER_ID, d.DEVICE_CODE, o.CREATE_ON, o.CREATE_BY } into gs

                         select new
                         {
                             gs.Key.ORDER_RELAY_METER_ID,
                             gs.Key.DEVICE_CODE,
                             gs.Key.CREATE_ON,
                             gs.Key.CREATE_BY,
                             TOTAL_CUSTOMER = gs.Count(),
                             USER_OPERATION_ID = 2,
                             USER_OPERATION = Operation[RelayOperation.Receive]
                         };

            dgv.DataSource = from r in rowGet.Union(rowSend)
                             where r.USER_OPERATION_ID == (int)ro || ro == RelayOperation.All
                             select r;
        }

        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgv.SelectedRows.Count == 0)
            {
                return;
            }

            if (!(dgv[e.ColumnIndex, e.RowIndex] is DataGridViewLinkCell))
            {
                return;
            }
            var id = (int)dgv.SelectedRows[0].Cells[ORDER_RELAY_METER_ID.Name].Value;
            DialogOrderRelayMeterLogDetail dig = new DialogOrderRelayMeterLogDetail(id);
            dig.Text = dgv.SelectedRows[0].Cells[USER_OPERATION.Name].Value.ToString();
            dig.ShowDialog();
        }
    }
}
