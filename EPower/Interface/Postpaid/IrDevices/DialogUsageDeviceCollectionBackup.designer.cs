﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogUsageDeviceCollectionBackup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogUsageDeviceCollection));
            this.panel4 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.txtQuickSearch = new SoftTech.Component.ExTextbox();
            this.IS_DIGITAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REGISTER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_METER_REGISTER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_READ_IR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.USAGE_HISTORY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblRECORD = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.lblRECORD);
            this.content.Controls.Add(this.txtQuickSearch);
            this.content.Controls.Add(this.dgv);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.dgv, 0);
            this.content.Controls.SetChildIndex(this.txtQuickSearch, 0);
            this.content.Controls.SetChildIndex(this.lblRECORD, 0);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.BackgroundImage = global::EPower.Properties.Resources.dock;
            resources.ApplyResources(this.panel4, "panel4");
            this.panel4.Name = "panel4";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // checkBox1
            // 
            resources.ApplyResources(this.checkBox1, "checkBox1");
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IS_DIGITAL,
            this.ID,
            this.REGISTER_CODE,
            this.IS_METER_REGISTER,
            this.IS_READ_IR,
            this.USAGE_HISTORY});
            this.dgv.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // txtQuickSearch
            // 
            this.txtQuickSearch.BackColor = System.Drawing.Color.White;
            this.txtQuickSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtQuickSearch, "txtQuickSearch");
            this.txtQuickSearch.Name = "txtQuickSearch";
            this.txtQuickSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtQuickSearch.QuickSearch += new System.EventHandler(this.txtQuickSearch_QuickSearch);
            // 
            // IS_DIGITAL
            // 
            this.IS_DIGITAL.DataPropertyName = "IS_DIGITAL";
            resources.ApplyResources(this.IS_DIGITAL, "IS_DIGITAL");
            this.IS_DIGITAL.Name = "IS_DIGITAL";
            this.IS_DIGITAL.ReadOnly = true;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            resources.ApplyResources(this.ID, "ID");
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            // 
            // REGISTER_CODE
            // 
            this.REGISTER_CODE.DataPropertyName = "REGISTER_CODE";
            resources.ApplyResources(this.REGISTER_CODE, "REGISTER_CODE");
            this.REGISTER_CODE.Name = "REGISTER_CODE";
            this.REGISTER_CODE.ReadOnly = true;
            // 
            // IS_METER_REGISTER
            // 
            this.IS_METER_REGISTER.DataPropertyName = "IS_METER_REGISTER";
            resources.ApplyResources(this.IS_METER_REGISTER, "IS_METER_REGISTER");
            this.IS_METER_REGISTER.Name = "IS_METER_REGISTER";
            this.IS_METER_REGISTER.ReadOnly = true;
            // 
            // IS_READ_IR
            // 
            this.IS_READ_IR.DataPropertyName = "IS_READ_IR";
            resources.ApplyResources(this.IS_READ_IR, "IS_READ_IR");
            this.IS_READ_IR.Name = "IS_READ_IR";
            this.IS_READ_IR.ReadOnly = true;
            // 
            // USAGE_HISTORY
            // 
            this.USAGE_HISTORY.DataPropertyName = "USAGE_HISTORY";
            resources.ApplyResources(this.USAGE_HISTORY, "USAGE_HISTORY");
            this.USAGE_HISTORY.Name = "USAGE_HISTORY";
            this.USAGE_HISTORY.ReadOnly = true;
            // 
            // lblRECORD
            // 
            resources.ApplyResources(this.lblRECORD, "lblRECORD");
            this.lblRECORD.Name = "lblRECORD";
            // 
            // DialogUsageDeviceCollection
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogUsageDeviceCollection";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.DialogUsageDeviceCollection_Load);
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Panel panel4;
        
        private Label label1;
        private CheckBox checkBox1;
        private DataGridView dgv;
        private ExTextbox txtQuickSearch;
        private DataGridViewTextBoxColumn IS_DIGITAL;
        private DataGridViewTextBoxColumn ID;
        private DataGridViewTextBoxColumn REGISTER_CODE;
        private DataGridViewTextBoxColumn IS_METER_REGISTER;
        private DataGridViewTextBoxColumn IS_READ_IR;
        private DataGridViewTextBoxColumn USAGE_HISTORY;
        private Label lblRECORD;
    }
}