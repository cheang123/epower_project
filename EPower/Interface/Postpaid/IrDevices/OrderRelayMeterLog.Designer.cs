﻿using System.ComponentModel;
using System.Windows.Forms;

namespace EPower.Interface.Postpaid.IrDevices
{
    partial class OrderRelayMeterLog
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.ORDER_RELAY_METER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.USER_OPERATION_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DEVICE_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.USER_OPERATION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_ON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_BY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_CUSTOMER = new System.Windows.Forms.DataGridViewLinkColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ORDER_RELAY_METER_ID,
            this.USER_OPERATION_ID,
            this.DEVICE_CODE,
            this.USER_OPERATION,
            this.CREATE_ON,
            this.CREATE_BY,
            this.TOTAL_CUSTOMER});
            this.dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.Location = new System.Drawing.Point(0, 0);
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.Size = new System.Drawing.Size(699, 402);
            this.dgv.TabIndex = 3;
            this.dgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellContentClick);
            // 
            // ORDER_RELAY_METER_ID
            // 
            this.ORDER_RELAY_METER_ID.DataPropertyName = "ORDER_RELAY_METER_ID";
            this.ORDER_RELAY_METER_ID.HeaderText = "ORDER_RELAY_METER_ID";
            this.ORDER_RELAY_METER_ID.Name = "ORDER_RELAY_METER_ID";
            this.ORDER_RELAY_METER_ID.ReadOnly = true;
            this.ORDER_RELAY_METER_ID.Visible = false;
            // 
            // USER_OPERATION_ID
            // 
            this.USER_OPERATION_ID.DataPropertyName = "USER_OPERATION_ID";
            this.USER_OPERATION_ID.HeaderText = "USER_OPERATION_ID";
            this.USER_OPERATION_ID.Name = "USER_OPERATION_ID";
            this.USER_OPERATION_ID.ReadOnly = true;
            this.USER_OPERATION_ID.Visible = false;
            // 
            // DEVICE_CODE
            // 
            this.DEVICE_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.DEVICE_CODE.DataPropertyName = "DEVICE_CODE";
            this.DEVICE_CODE.HeaderText = "ឧបករណ៍";
            this.DEVICE_CODE.Name = "DEVICE_CODE";
            this.DEVICE_CODE.ReadOnly = true;
            this.DEVICE_CODE.Width = 79;
            // 
            // USER_OPERATION
            // 
            this.USER_OPERATION.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.USER_OPERATION.DataPropertyName = "USER_OPERATION";
            this.USER_OPERATION.HeaderText = "ដំណើរការ";
            this.USER_OPERATION.Name = "USER_OPERATION";
            this.USER_OPERATION.ReadOnly = true;
            // 
            // CREATE_ON
            // 
            this.CREATE_ON.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.CREATE_ON.DataPropertyName = "CREATE_ON";
            dataGridViewCellStyle2.Format = "dd-MM-yyyy hh:mm:ss tt";
            this.CREATE_ON.DefaultCellStyle = dataGridViewCellStyle2;
            this.CREATE_ON.HeaderText = "កាលបរិច្ឆេទ";
            this.CREATE_ON.MinimumWidth = 135;
            this.CREATE_ON.Name = "CREATE_ON";
            this.CREATE_ON.ReadOnly = true;
            this.CREATE_ON.Width = 135;
            // 
            // CREATE_BY
            // 
            this.CREATE_BY.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.CREATE_BY.DataPropertyName = "CREATE_BY";
            this.CREATE_BY.HeaderText = "ធ្វើដោយ";
            this.CREATE_BY.Name = "CREATE_BY";
            this.CREATE_BY.ReadOnly = true;
            this.CREATE_BY.Width = 74;
            // 
            // TOTAL_CUSTOMER
            // 
            this.TOTAL_CUSTOMER.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.TOTAL_CUSTOMER.DataPropertyName = "TOTAL_CUSTOMER";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.TOTAL_CUSTOMER.DefaultCellStyle = dataGridViewCellStyle3;
            this.TOTAL_CUSTOMER.HeaderText = "ចំនួនអតិថិជន";
            this.TOTAL_CUSTOMER.Name = "TOTAL_CUSTOMER";
            this.TOTAL_CUSTOMER.ReadOnly = true;
            this.TOTAL_CUSTOMER.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.TOTAL_CUSTOMER.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.TOTAL_CUSTOMER.TrackVisitedState = false;
            this.TOTAL_CUSTOMER.Width = 98;
            // 
            // OrderRelayMeterLog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dgv);
            this.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "OrderRelayMeterLog";
            this.Size = new System.Drawing.Size(699, 402);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataGridView dgv;
        private DataGridViewTextBoxColumn ORDER_RELAY_METER_ID;
        private DataGridViewTextBoxColumn USER_OPERATION_ID;
        private DataGridViewTextBoxColumn DEVICE_CODE;
        private DataGridViewTextBoxColumn USER_OPERATION;
        private DataGridViewTextBoxColumn CREATE_ON;
        private DataGridViewTextBoxColumn CREATE_BY;
        private DataGridViewLinkColumn TOTAL_CUSTOMER;


    }
}
