﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageUsageWire
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageUsageWire));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cboGroup = new System.Windows.Forms.ComboBox();
            this.dtp2 = new System.Windows.Forms.DateTimePicker();
            this.dtp1 = new System.Windows.Forms.DateTimePicker();
            this.btnSEND_DATA = new SoftTech.Component.ExButton();
            this.btnRECEIVE_DATA = new SoftTech.Component.ExButton();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.USAGE_DEVICE_COLLECTION_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TRANSFER_TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TRANSMISSION_TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_BY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_ON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DEVICE_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DEVICE_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CYCLE_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AREA_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MONTH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RECORD = new System.Windows.Forms.DataGridViewLinkColumn();
            this.DATA_BACKUP = new System.Windows.Forms.DataGridViewLinkColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.cboGroup);
            this.panel1.Controls.Add(this.dtp2);
            this.panel1.Controls.Add(this.dtp1);
            this.panel1.Controls.Add(this.btnSEND_DATA);
            this.panel1.Controls.Add(this.btnRECEIVE_DATA);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // cboGroup
            // 
            this.cboGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboGroup.FormattingEnabled = true;
            resources.ApplyResources(this.cboGroup, "cboGroup");
            this.cboGroup.Name = "cboGroup";
            // 
            // dtp2
            // 
            resources.ApplyResources(this.dtp2, "dtp2");
            this.dtp2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp2.Name = "dtp2";
            // 
            // dtp1
            // 
            resources.ApplyResources(this.dtp1, "dtp1");
            this.dtp1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp1.Name = "dtp1";
            // 
            // btnSEND_DATA
            // 
            resources.ApplyResources(this.btnSEND_DATA, "btnSEND_DATA");
            this.btnSEND_DATA.Name = "btnSEND_DATA";
            this.btnSEND_DATA.UseVisualStyleBackColor = true;
            this.btnSEND_DATA.Click += new System.EventHandler(this.btnUploadUsage_Click);
            // 
            // btnRECEIVE_DATA
            // 
            resources.ApplyResources(this.btnRECEIVE_DATA, "btnRECEIVE_DATA");
            this.btnRECEIVE_DATA.Name = "btnRECEIVE_DATA";
            this.btnRECEIVE_DATA.UseVisualStyleBackColor = true;
            this.btnRECEIVE_DATA.Click += new System.EventHandler(this.btnDownloadUsage_Click);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.USAGE_DEVICE_COLLECTION_ID,
            this.TRANSFER_TYPE,
            this.TRANSMISSION_TYPE,
            this.CREATE_BY,
            this.CREATE_ON,
            this.DEVICE_CODE,
            this.DEVICE_NAME,
            this.CYCLE_NAME,
            this.AREA_NAME,
            this.MONTH,
            this.RECORD,
            this.DATA_BACKUP});
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellContentClick);
            // 
            // USAGE_DEVICE_COLLECTION_ID
            // 
            this.USAGE_DEVICE_COLLECTION_ID.DataPropertyName = "USAGE_DEVICE_COLLECTION_ID";
            resources.ApplyResources(this.USAGE_DEVICE_COLLECTION_ID, "USAGE_DEVICE_COLLECTION_ID");
            this.USAGE_DEVICE_COLLECTION_ID.Name = "USAGE_DEVICE_COLLECTION_ID";
            this.USAGE_DEVICE_COLLECTION_ID.ReadOnly = true;
            // 
            // TRANSFER_TYPE
            // 
            this.TRANSFER_TYPE.DataPropertyName = "TRANSFER_TYPE";
            resources.ApplyResources(this.TRANSFER_TYPE, "TRANSFER_TYPE");
            this.TRANSFER_TYPE.Name = "TRANSFER_TYPE";
            this.TRANSFER_TYPE.ReadOnly = true;
            // 
            // TRANSMISSION_TYPE
            // 
            this.TRANSMISSION_TYPE.DataPropertyName = "TRANSMISSION_TYPE";
            resources.ApplyResources(this.TRANSMISSION_TYPE, "TRANSMISSION_TYPE");
            this.TRANSMISSION_TYPE.Name = "TRANSMISSION_TYPE";
            this.TRANSMISSION_TYPE.ReadOnly = true;
            // 
            // CREATE_BY
            // 
            this.CREATE_BY.DataPropertyName = "CREATE_BY";
            resources.ApplyResources(this.CREATE_BY, "CREATE_BY");
            this.CREATE_BY.Name = "CREATE_BY";
            this.CREATE_BY.ReadOnly = true;
            // 
            // CREATE_ON
            // 
            this.CREATE_ON.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.CREATE_ON.DataPropertyName = "CREATE_ON";
            resources.ApplyResources(this.CREATE_ON, "CREATE_ON");
            this.CREATE_ON.Name = "CREATE_ON";
            this.CREATE_ON.ReadOnly = true;
            // 
            // DEVICE_CODE
            // 
            this.DEVICE_CODE.DataPropertyName = "DEVICE_CODE";
            resources.ApplyResources(this.DEVICE_CODE, "DEVICE_CODE");
            this.DEVICE_CODE.Name = "DEVICE_CODE";
            this.DEVICE_CODE.ReadOnly = true;
            // 
            // DEVICE_NAME
            // 
            this.DEVICE_NAME.DataPropertyName = "DEVICE_NAME";
            resources.ApplyResources(this.DEVICE_NAME, "DEVICE_NAME");
            this.DEVICE_NAME.Name = "DEVICE_NAME";
            this.DEVICE_NAME.ReadOnly = true;
            // 
            // CYCLE_NAME
            // 
            this.CYCLE_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CYCLE_NAME.DataPropertyName = "CYCLE_NAME";
            resources.ApplyResources(this.CYCLE_NAME, "CYCLE_NAME");
            this.CYCLE_NAME.Name = "CYCLE_NAME";
            this.CYCLE_NAME.ReadOnly = true;
            // 
            // AREA_NAME
            // 
            this.AREA_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AREA_NAME.DataPropertyName = "AREA_NAME";
            resources.ApplyResources(this.AREA_NAME, "AREA_NAME");
            this.AREA_NAME.Name = "AREA_NAME";
            this.AREA_NAME.ReadOnly = true;
            // 
            // MONTH
            // 
            this.MONTH.DataPropertyName = "MONTH";
            resources.ApplyResources(this.MONTH, "MONTH");
            this.MONTH.Name = "MONTH";
            this.MONTH.ReadOnly = true;
            // 
            // RECORD
            // 
            this.RECORD.DataPropertyName = "RECORD";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.RECORD.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.RECORD, "RECORD");
            this.RECORD.Name = "RECORD";
            this.RECORD.ReadOnly = true;
            this.RECORD.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.RECORD.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.RECORD.VisitedLinkColor = System.Drawing.Color.Blue;
            // 
            // DATA_BACKUP
            // 
            this.DATA_BACKUP.DataPropertyName = "DATA_BACKUP";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DATA_BACKUP.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.DATA_BACKUP, "DATA_BACKUP");
            this.DATA_BACKUP.Name = "DATA_BACKUP";
            this.DATA_BACKUP.ReadOnly = true;
            this.DATA_BACKUP.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DATA_BACKUP.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.DATA_BACKUP.VisitedLinkColor = System.Drawing.Color.Blue;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "USAGE_DEVICE_COLLECTION_ID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn1, "dataGridViewTextBoxColumn1");
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "TRANSFER_TYPE";
            resources.ApplyResources(this.dataGridViewTextBoxColumn2, "dataGridViewTextBoxColumn2");
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "TRANSMISSION_TYPE";
            resources.ApplyResources(this.dataGridViewTextBoxColumn3, "dataGridViewTextBoxColumn3");
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "CREATE_BY";
            resources.ApplyResources(this.dataGridViewTextBoxColumn4, "dataGridViewTextBoxColumn4");
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "CREATE_ON";
            resources.ApplyResources(this.dataGridViewTextBoxColumn5, "dataGridViewTextBoxColumn5");
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "DEVICE_CODE";
            resources.ApplyResources(this.dataGridViewTextBoxColumn6, "dataGridViewTextBoxColumn6");
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "DEVICE_NAME";
            resources.ApplyResources(this.dataGridViewTextBoxColumn7, "dataGridViewTextBoxColumn7");
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "CYCLE_NAME";
            resources.ApplyResources(this.dataGridViewTextBoxColumn8, "dataGridViewTextBoxColumn8");
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "MONTH";
            resources.ApplyResources(this.dataGridViewTextBoxColumn9, "dataGridViewTextBoxColumn9");
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "RECORD";
            resources.ApplyResources(this.dataGridViewTextBoxColumn10, "dataGridViewTextBoxColumn10");
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            // 
            // PageUsageWire
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.panel1);
            this.Name = "PageUsageWire";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnRECEIVE_DATA;
        private ExButton btnSEND_DATA;
        private DateTimePicker dtp2;
        private DateTimePicker dtp1;
        private DataGridView dgv;
        private ComboBox cboGroup;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private DataGridViewTextBoxColumn USAGE_DEVICE_COLLECTION_ID;
        private DataGridViewTextBoxColumn TRANSFER_TYPE;
        private DataGridViewTextBoxColumn TRANSMISSION_TYPE;
        private DataGridViewTextBoxColumn CREATE_BY;
        private DataGridViewTextBoxColumn CREATE_ON;
        private DataGridViewTextBoxColumn DEVICE_CODE;
        private DataGridViewTextBoxColumn DEVICE_NAME;
        private DataGridViewTextBoxColumn CYCLE_NAME;
        private DataGridViewTextBoxColumn AREA_NAME;
        private DataGridViewTextBoxColumn MONTH;
        private DataGridViewLinkColumn RECORD;
        private DataGridViewLinkColumn DATA_BACKUP;
    }
}
