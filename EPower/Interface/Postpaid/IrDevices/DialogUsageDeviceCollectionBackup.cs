﻿using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogUsageDeviceCollectionBackup : ExDialog
    {

        List<TMP_IR_USAGE> _usage = new List<TMP_IR_USAGE>();
        public DialogUsageDeviceCollectionBackup(List<TMP_IR_USAGE> usage)
        {
            InitializeComponent();
            _usage = usage;
            load();
        }

        void load()
        {
            dgv.DataSource = _usage;
            dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgv.Columns["PHASE_ID"].Visible = false;
        }

        private void txtQuickSearch_QuickSearch(object sender, EventArgs e)
        {

            dgv.DataSource = (from u in _usage
                              where (u.METER_CODE + " " + u.BOX_CODE).ToLower()
                              .Contains(txtQuickSearch.Text.ToLower())
                              orderby u.METER_CODE
                              select u)._ToDataTable();
            dgv.Columns["PHASE_ID"].Visible = false;
        }

        private void DialogUsageDeviceCollection_Load(object sender, EventArgs e)
        {
            this.Text = Resources.RECEIVE_DATA;
            lblRECORD.Text = string.Format("{0}:{1}", Resources.RECORD, _usage.Count);
        }
    }
}