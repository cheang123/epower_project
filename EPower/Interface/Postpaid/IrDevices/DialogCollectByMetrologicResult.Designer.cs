﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogCollectByMetrologicResult
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCollectByMetrologicResult));
            this.btnClose = new SoftTech.Component.ExButton();
            this.dgvInvoiceItem = new System.Windows.Forms.DataGridView();
            this.METER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FULL_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.START_USAGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.END_USAGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblCount = new System.Windows.Forms.Label();
            this.rdoAll = new System.Windows.Forms.RadioButton();
            this.rdoSuccess = new System.Windows.Forms.RadioButton();
            this.rdoNewCycle = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInvoiceItem)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.radioButton1);
            this.content.Controls.Add(this.rdoNewCycle);
            this.content.Controls.Add(this.rdoSuccess);
            this.content.Controls.Add(this.rdoAll);
            this.content.Controls.Add(this.dgvInvoiceItem);
            this.content.Controls.Add(this.btnClose);
            this.content.Controls.Add(this.label4);
            this.content.Controls.Add(this.label3);
            this.content.Controls.Add(this.lblCount);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblCount, 0);
            this.content.Controls.SetChildIndex(this.label3, 0);
            this.content.Controls.SetChildIndex(this.label4, 0);
            this.content.Controls.SetChildIndex(this.btnClose, 0);
            this.content.Controls.SetChildIndex(this.dgvInvoiceItem, 0);
            this.content.Controls.SetChildIndex(this.rdoAll, 0);
            this.content.Controls.SetChildIndex(this.rdoSuccess, 0);
            this.content.Controls.SetChildIndex(this.rdoNewCycle, 0);
            this.content.Controls.SetChildIndex(this.radioButton1, 0);
            // 
            // btnClose
            // 
            resources.ApplyResources(this.btnClose, "btnClose");
            this.btnClose.Name = "btnClose";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // dgvInvoiceItem
            // 
            this.dgvInvoiceItem.AllowUserToAddRows = false;
            this.dgvInvoiceItem.AllowUserToDeleteRows = false;
            this.dgvInvoiceItem.AllowUserToResizeRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvInvoiceItem.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvInvoiceItem.BackgroundColor = System.Drawing.Color.White;
            this.dgvInvoiceItem.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvInvoiceItem.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvInvoiceItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInvoiceItem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.METER_CODE,
            this.CUSTOMER_ID,
            this.CUSTOMER_CODE,
            this.FULL_NAME,
            this.START_USAGE,
            this.END_USAGE});
            resources.ApplyResources(this.dgvInvoiceItem, "dgvInvoiceItem");
            this.dgvInvoiceItem.Name = "dgvInvoiceItem";
            this.dgvInvoiceItem.ReadOnly = true;
            this.dgvInvoiceItem.RowHeadersVisible = false;
            this.dgvInvoiceItem.RowTemplate.Height = 25;
            this.dgvInvoiceItem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // METER_CODE
            // 
            this.METER_CODE.DataPropertyName = "METER_CODE";
            resources.ApplyResources(this.METER_CODE, "METER_CODE");
            this.METER_CODE.Name = "METER_CODE";
            this.METER_CODE.ReadOnly = true;
            // 
            // CUSTOMER_ID
            // 
            this.CUSTOMER_ID.DataPropertyName = "CUSTOMER_ID";
            resources.ApplyResources(this.CUSTOMER_ID, "CUSTOMER_ID");
            this.CUSTOMER_ID.Name = "CUSTOMER_ID";
            this.CUSTOMER_ID.ReadOnly = true;
            // 
            // CUSTOMER_CODE
            // 
            this.CUSTOMER_CODE.DataPropertyName = "CUSTOMER_CODE";
            resources.ApplyResources(this.CUSTOMER_CODE, "CUSTOMER_CODE");
            this.CUSTOMER_CODE.Name = "CUSTOMER_CODE";
            this.CUSTOMER_CODE.ReadOnly = true;
            // 
            // FULL_NAME
            // 
            this.FULL_NAME.DataPropertyName = "FULL_NAME";
            resources.ApplyResources(this.FULL_NAME, "FULL_NAME");
            this.FULL_NAME.Name = "FULL_NAME";
            this.FULL_NAME.ReadOnly = true;
            // 
            // START_USAGE
            // 
            this.START_USAGE.DataPropertyName = "START_USAGE";
            resources.ApplyResources(this.START_USAGE, "START_USAGE");
            this.START_USAGE.Name = "START_USAGE";
            this.START_USAGE.ReadOnly = true;
            // 
            // END_USAGE
            // 
            this.END_USAGE.DataPropertyName = "END_USAGE";
            resources.ApplyResources(this.END_USAGE, "END_USAGE");
            this.END_USAGE.Name = "END_USAGE";
            this.END_USAGE.ReadOnly = true;
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // lblCount
            // 
            resources.ApplyResources(this.lblCount, "lblCount");
            this.lblCount.Name = "lblCount";
            // 
            // rdoAll
            // 
            resources.ApplyResources(this.rdoAll, "rdoAll");
            this.rdoAll.Name = "rdoAll";
            this.rdoAll.UseVisualStyleBackColor = true;
            // 
            // rdoSuccess
            // 
            resources.ApplyResources(this.rdoSuccess, "rdoSuccess");
            this.rdoSuccess.Name = "rdoSuccess";
            this.rdoSuccess.UseVisualStyleBackColor = true;
            // 
            // rdoNewCycle
            // 
            resources.ApplyResources(this.rdoNewCycle, "rdoNewCycle");
            this.rdoNewCycle.Checked = true;
            this.rdoNewCycle.Name = "rdoNewCycle";
            this.rdoNewCycle.TabStop = true;
            this.rdoNewCycle.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            resources.ApplyResources(this.radioButton1, "radioButton1");
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // DialogCollectByMetrologicResult
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogCollectByMetrologicResult";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInvoiceItem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnClose;
        private DataGridView dgvInvoiceItem;
        private Label lblCount;
        private Label label3;
        private Label label4;
        private RadioButton rdoNewCycle;
        private RadioButton rdoSuccess;
        private RadioButton rdoAll;
        private RadioButton radioButton1;
        private DataGridViewTextBoxColumn METER_CODE;
        private DataGridViewTextBoxColumn CUSTOMER_ID;
        private DataGridViewTextBoxColumn CUSTOMER_CODE;
        private DataGridViewTextBoxColumn FULL_NAME;
        private DataGridViewTextBoxColumn START_USAGE;
        private DataGridViewTextBoxColumn END_USAGE;
    }
}