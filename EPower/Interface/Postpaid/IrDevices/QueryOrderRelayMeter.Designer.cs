﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.Postpaid.IrDevices
{
    partial class QueryOrderRelayMeter
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cboBillingCycle = new System.Windows.Forms.ComboBox();
            this.cboArea = new System.Windows.Forms.ComboBox();
            this.cboCustomerType = new System.Windows.Forms.ComboBox();
            this.chkCUSTOMER_TO_BLOCK = new System.Windows.Forms.CheckBox();
            this.btnSEND_DATA = new SoftTech.Component.ExButton();
            this.btnRECEIVE_DATA = new SoftTech.Component.ExButton();
            this.txtTake = new System.Windows.Forms.NumericUpDown();
            this.btnSEND_DATA_TO_INTERNET = new SoftTech.Component.ExButton();
            this.btnRECEIVE_DATA_INTERNET = new SoftTech.Component.ExButton();
            ((System.ComponentModel.ISupportInitialize)(this.txtTake)).BeginInit();
            this.SuspendLayout();
            // 
            // cboBillingCycle
            // 
            this.cboBillingCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBillingCycle.DropDownWidth = 200;
            this.cboBillingCycle.FormattingEnabled = true;
            this.cboBillingCycle.Items.AddRange(new object[] {
            "គ្រប់តំបន់ទាំងអស់",
            "ក១",
            "ក២",
            "ក៣",
            "ក៤"});
            this.cboBillingCycle.Location = new System.Drawing.Point(244, 3);
            this.cboBillingCycle.Margin = new System.Windows.Forms.Padding(1);
            this.cboBillingCycle.Name = "cboBillingCycle";
            this.cboBillingCycle.Size = new System.Drawing.Size(114, 27);
            this.cboBillingCycle.TabIndex = 20;
            this.cboBillingCycle.Tag = "0";
            this.cboBillingCycle.SelectedIndexChanged += new System.EventHandler(this.cbo_SelectedIndexChanged);
            // 
            // cboArea
            // 
            this.cboArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboArea.DropDownWidth = 200;
            this.cboArea.FormattingEnabled = true;
            this.cboArea.Items.AddRange(new object[] {
            "គ្រប់តំបន់ទាំងអស់",
            "ក១",
            "ក២",
            "ក៣",
            "ក៤"});
            this.cboArea.Location = new System.Drawing.Point(117, 3);
            this.cboArea.Margin = new System.Windows.Forms.Padding(1);
            this.cboArea.Name = "cboArea";
            this.cboArea.Size = new System.Drawing.Size(124, 27);
            this.cboArea.TabIndex = 21;
            this.cboArea.Tag = "0";
            this.cboArea.SelectedIndexChanged += new System.EventHandler(this.cbo_SelectedIndexChanged);
            // 
            // cboCustomerType
            // 
            this.cboCustomerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomerType.DropDownWidth = 200;
            this.cboCustomerType.FormattingEnabled = true;
            this.cboCustomerType.Items.AddRange(new object[] {
            "គ្រប់តំបន់ទាំងអស់",
            "ក១",
            "ក២",
            "ក៣",
            "ក៤"});
            this.cboCustomerType.Location = new System.Drawing.Point(0, 3);
            this.cboCustomerType.Name = "cboCustomerType";
            this.cboCustomerType.Size = new System.Drawing.Size(114, 27);
            this.cboCustomerType.TabIndex = 18;
            this.cboCustomerType.SelectedIndexChanged += new System.EventHandler(this.cbo_SelectedIndexChanged);
            // 
            // chkCUSTOMER_TO_BLOCK
            // 
            this.chkCUSTOMER_TO_BLOCK.AutoSize = true;
            this.chkCUSTOMER_TO_BLOCK.Location = new System.Drawing.Point(425, 5);
            this.chkCUSTOMER_TO_BLOCK.Name = "chkCUSTOMER_TO_BLOCK";
            this.chkCUSTOMER_TO_BLOCK.Size = new System.Drawing.Size(124, 23);
            this.chkCUSTOMER_TO_BLOCK.TabIndex = 22;
            this.chkCUSTOMER_TO_BLOCK.Text = "អតិថិជនដែលត្រូវបិទ";
            this.chkCUSTOMER_TO_BLOCK.UseVisualStyleBackColor = true;
            this.chkCUSTOMER_TO_BLOCK.CheckedChanged += new System.EventHandler(this.chkCloseCus_CheckedChanged);
            // 
            // btnSEND_DATA
            // 
            this.btnSEND_DATA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSEND_DATA.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnSEND_DATA.Location = new System.Drawing.Point(978, 5);
            this.btnSEND_DATA.Margin = new System.Windows.Forms.Padding(2);
            this.btnSEND_DATA.Name = "btnSEND_DATA";
            this.btnSEND_DATA.Size = new System.Drawing.Size(127, 23);
            this.btnSEND_DATA.TabIndex = 24;
            this.btnSEND_DATA.Text = "បញ្ចូនទិន្នន័យតាមខ្សែ";
            this.btnSEND_DATA.UseVisualStyleBackColor = true;
            this.btnSEND_DATA.Click += new System.EventHandler(this.btnSendData_Click);
            // 
            // btnRECEIVE_DATA
            // 
            this.btnRECEIVE_DATA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRECEIVE_DATA.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnRECEIVE_DATA.Location = new System.Drawing.Point(1109, 5);
            this.btnRECEIVE_DATA.Margin = new System.Windows.Forms.Padding(2);
            this.btnRECEIVE_DATA.Name = "btnRECEIVE_DATA";
            this.btnRECEIVE_DATA.Size = new System.Drawing.Size(99, 23);
            this.btnRECEIVE_DATA.TabIndex = 23;
            this.btnRECEIVE_DATA.Text = "ទាញយកទិន្នន័យ";
            this.btnRECEIVE_DATA.UseVisualStyleBackColor = true;
            this.btnRECEIVE_DATA.Click += new System.EventHandler(this.btnRECEIVE_DATA_Click);
            // 
            // txtTake
            // 
            this.txtTake.Location = new System.Drawing.Point(361, 3);
            this.txtTake.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.txtTake.Minimum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.txtTake.Name = "txtTake";
            this.txtTake.Size = new System.Drawing.Size(60, 27);
            this.txtTake.TabIndex = 25;
            this.txtTake.Value = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.txtTake.ValueChanged += new System.EventHandler(this.cbo_SelectedIndexChanged);
            // 
            // btnSEND_DATA_TO_INTERNET
            // 
            this.btnSEND_DATA_TO_INTERNET.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSEND_DATA_TO_INTERNET.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnSEND_DATA_TO_INTERNET.Location = new System.Drawing.Point(665, 5);
            this.btnSEND_DATA_TO_INTERNET.Margin = new System.Windows.Forms.Padding(2);
            this.btnSEND_DATA_TO_INTERNET.Name = "btnSEND_DATA_TO_INTERNET";
            this.btnSEND_DATA_TO_INTERNET.Size = new System.Drawing.Size(151, 23);
            this.btnSEND_DATA_TO_INTERNET.TabIndex = 26;
            this.btnSEND_DATA_TO_INTERNET.Text = "បញ្ចូនទិន្នន័យតាម Internet";
            this.btnSEND_DATA_TO_INTERNET.UseVisualStyleBackColor = true;
            this.btnSEND_DATA_TO_INTERNET.Visible = false;
            this.btnSEND_DATA_TO_INTERNET.Click += new System.EventHandler(this.btnSEND_DATA_TO_INTERNET_Click);
            // 
            // btnRECEIVE_DATA_INTERNET
            // 
            this.btnRECEIVE_DATA_INTERNET.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRECEIVE_DATA_INTERNET.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnRECEIVE_DATA_INTERNET.Location = new System.Drawing.Point(822, 5);
            this.btnRECEIVE_DATA_INTERNET.Margin = new System.Windows.Forms.Padding(2);
            this.btnRECEIVE_DATA_INTERNET.Name = "btnRECEIVE_DATA_INTERNET";
            this.btnRECEIVE_DATA_INTERNET.Size = new System.Drawing.Size(151, 23);
            this.btnRECEIVE_DATA_INTERNET.TabIndex = 27;
            this.btnRECEIVE_DATA_INTERNET.Text = "ទាញទិន្នន័យតាម Internet";
            this.btnRECEIVE_DATA_INTERNET.UseVisualStyleBackColor = true;
            this.btnRECEIVE_DATA_INTERNET.Click += new System.EventHandler(this.btnRECEIVE_DATA_INTERNET_Click);
            // 
            // QueryOrderRelayMeter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnRECEIVE_DATA_INTERNET);
            this.Controls.Add(this.btnSEND_DATA_TO_INTERNET);
            this.Controls.Add(this.txtTake);
            this.Controls.Add(this.btnSEND_DATA);
            this.Controls.Add(this.btnRECEIVE_DATA);
            this.Controls.Add(this.chkCUSTOMER_TO_BLOCK);
            this.Controls.Add(this.cboBillingCycle);
            this.Controls.Add(this.cboArea);
            this.Controls.Add(this.cboCustomerType);
            this.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "QueryOrderRelayMeter";
            this.Size = new System.Drawing.Size(1213, 32);
            ((System.ComponentModel.ISupportInitialize)(this.txtTake)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public ComboBox cboBillingCycle;
        public ComboBox cboArea;
        public ComboBox cboCustomerType;
        private CheckBox chkCUSTOMER_TO_BLOCK;
        private ExButton btnSEND_DATA;
        private ExButton btnRECEIVE_DATA;
        private NumericUpDown txtTake;
        private ExButton btnSEND_DATA_TO_INTERNET;
        private ExButton btnRECEIVE_DATA_INTERNET;
    }
}
