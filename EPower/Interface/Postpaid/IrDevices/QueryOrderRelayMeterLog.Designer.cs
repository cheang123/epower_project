﻿using System.ComponentModel;
using System.Windows.Forms;

namespace EPower.Interface.Postpaid.IrDevices
{
    partial class QueryOrderRelayMeterLog
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cboOperation = new System.Windows.Forms.ComboBox();
            this.dtp2 = new System.Windows.Forms.DateTimePicker();
            this.dtp1 = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // cboOperation
            // 
            this.cboOperation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboOperation.DropDownWidth = 200;
            this.cboOperation.FormattingEnabled = true;
            this.cboOperation.Items.AddRange(new object[] {
            "គ្រប់តំបន់ទាំងអស់",
            "ក១",
            "ក២",
            "ក៣",
            "ក៤"});
            this.cboOperation.Location = new System.Drawing.Point(0, 3);
            this.cboOperation.Name = "cboOperation";
            this.cboOperation.Size = new System.Drawing.Size(114, 27);
            this.cboOperation.TabIndex = 18;
            this.cboOperation.SelectedIndexChanged += new System.EventHandler(this.cboOperation_SelectedIndexChanged);
            // 
            // dtp2
            // 
            this.dtp2.CustomFormat = "dd-MM-yyyy";
            this.dtp2.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.dtp2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp2.Location = new System.Drawing.Point(223, 3);
            this.dtp2.Name = "dtp2";
            this.dtp2.Size = new System.Drawing.Size(103, 27);
            this.dtp2.TabIndex = 31;
            this.dtp2.ValueChanged += new System.EventHandler(this.dtp_ValueChanged);
            // 
            // dtp1
            // 
            this.dtp1.CustomFormat = "dd-MM-yyyy";
            this.dtp1.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.dtp1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp1.Location = new System.Drawing.Point(117, 3);
            this.dtp1.Name = "dtp1";
            this.dtp1.Size = new System.Drawing.Size(103, 27);
            this.dtp1.TabIndex = 30;
            this.dtp1.ValueChanged += new System.EventHandler(this.dtp_ValueChanged);
            // 
            // QueryOrderRelayMeterLog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dtp2);
            this.Controls.Add(this.dtp1);
            this.Controls.Add(this.cboOperation);
            this.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "QueryOrderRelayMeterLog";
            this.Size = new System.Drawing.Size(760, 32);
            this.Load += new System.EventHandler(this.QueryOrderRelayMeterLog_Load);
            this.ResumeLayout(false);

        }

        #endregion

        public ComboBox cboOperation;
        private DateTimePicker dtp2;
        private DateTimePicker dtp1;

    }
}
