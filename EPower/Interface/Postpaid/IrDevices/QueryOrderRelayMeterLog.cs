﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface.Postpaid.IrDevices
{
    public partial class QueryOrderRelayMeterLog : UserControl
    {
        private static QueryOrderRelayMeterLog self = new QueryOrderRelayMeterLog();
        public static QueryOrderRelayMeterLog Instant { get { return self; } }
        public QueryOrderRelayMeterLog()
        {

            InitializeComponent();
            this.Dock = DockStyle.Fill;
            cboOperation.ValueMember = "Key";
            cboOperation.DisplayMember = "Value";
            cboOperation.DataSource = OrderRelayMeterLog.Instant.Operation.ToList();
            dtp2.Value = DateTime.Now;
            dtp1.Value = new DateTime(dtp2.Value.Year, dtp2.Value.Month, 1);
        }

        private void cboOperation_SelectedIndexChanged(object sender, EventArgs e)
        {
            search();
        }

        public void search()
        {
            if (cboOperation.SelectedValue == null)
            {
                return;
            }

            OrderRelayMeterLog.Instant.Search((OrderRelayMeterLog.RelayOperation)cboOperation.SelectedValue, dtp1.Value, dtp2.Value);
        }

        private void dtp_ValueChanged(object sender, EventArgs e)
        {
            search();
        }

        private void QueryOrderRelayMeterLog_Load(object sender, EventArgs e)
        {
            this.search();
        }
    }
}
