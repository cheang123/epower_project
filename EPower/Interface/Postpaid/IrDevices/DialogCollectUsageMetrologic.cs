﻿using EPower.Base.Logic;
using EPower.Logic;
using EPower.Logic.IRDevice;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogCollectUsageMetrologic : ExDialog
    {
        #region Private Data
        string _strDeviceCode = "";
        DataTable _dtMetrolData = new DataTable();
        Metrologic metro = new Metrologic();
        #endregion Private Data

        #region Constructor
        public DialogCollectUsageMetrologic()
        {
            InitializeComponent();
            Bind();
        }
        #endregion Constructor

        #region Method
        private void LoadStartEndDate()
        {
            if (cboBillingCycle.SelectedIndex != -1)
            {
                int intBillingCycleId = int.Parse(cboBillingCycle.SelectedValue.ToString());
                if (intBillingCycleId > 0)
                {
                    TBL_BILLING_CYCLE objCycle = DBDataContext.Db.TBL_BILLING_CYCLEs.FirstOrDefault(b => b.CYCLE_ID == intBillingCycleId);
                    DateTime datStart = new DateTime(), datEnd = new DateTime();
                    //DateTime datMonth = new DateTime(this.dtpMonth.Value.Year, this.dtpMonth.Value.Month, 1);

                    this.dtpMonth.Value = Method.GetNextBillingMonth(intBillingCycleId, ref datStart, ref datEnd);

                    dtpStartDate.Value = datStart;
                    dtpEndDate.Value = datEnd;


                    //get total customer
                    int intTotalCustomer = (from cus in DBDataContext.Db.TBL_CUSTOMERs
                                            where cus.STATUS_ID == (int)CustomerStatus.Active
                                            || cus.STATUS_ID == (int)CustomerStatus.Blocked
                                            select cus).Count();

                    txtAllCustomer.Text = intTotalCustomer.ToString();

                    //load Total of UnusageCustomer , UnknownMeter and UnRegisterMeter
                    LoadUnusalCustomer(intBillingCycleId, _dtMetrolData.Rows.Count);
                }
            }

        }

        private void Bind()
        {
            //Billing Cycle
            DataTable dtCycle = DBDataContext.Db.TBL_BILLING_CYCLEs.Where(b => b.IS_ACTIVE)._ToDataTable();
            DataRow row = dtCycle.NewRow();
            row["CYCLE_ID"] = 0;
            row["CYCLE_NAME"] = Resources.SELECT_BILLING_CYCLE;
            dtCycle.Rows.InsertAt(row, 0);
            UIHelper.SetDataSourceToComboBox(this.cboBillingCycle, dtCycle);
            if (dtCycle.Rows.Count > 1)
            {
                this.cboBillingCycle.SelectedIndex = 1;
            }

            var device = from d in DBDataContext.Db.TBL_DEVICEs
                         where d.DEVICE_TYPE_ID == (int)DeviceType.Metrologic && d.STATUS_ID == (int)DeviceStatus.Used
                         select new
                         {
                             d.DEVICE_ID,
                             d.DEVICE_CODE
                         };
            UIHelper.SetDataSourceToComboBox(cboDevice, device);
        }

        /// <summary>
        /// Save  usage 
        /// </summary>
        private bool SaveUsage(int intBillingCycle, int intCollectID)
        {
            bool blnReturn = false;
            try
            {
                //Insert new data to temp table                
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    Runner.Instance.Text = string.Format(Resources.MS_SAVING_DATA, "");
                    Application.DoEvents();

                    DateTime datBillingMonth = dtpMonth.Value.Date;
                    int intBillingCycleId = DataHelper.ParseToInt(cboBillingCycle.SelectedValue.ToString());
                    bool isDigital = false;
                    DateTime datCurrentDate = DBDataContext.Db.GetSystemDate();
                    string strSQL = string.Empty;

                    #region GET USAGE FROM DIGITAL METER RIGESTER
                    //Delete temp table with the current connected device
                    strSQL = "DELETE FROM TBL_TMP_USAGE";
                    DBDataContext.Db.ExecuteCommand(strSQL, metro.DeviceID);


                    //Insert all usage from Metrologic data collector to TBL_TMP_USAGE with meter register
                    foreach (DataRow drTmpUage in _dtMetrolData.Rows)
                    {
                        TMP_IR_USAGE objTmp = new TMP_IR_USAGE();
                        string strMeterCode = drTmpUage["METER_CODE"].ToString();
                        objTmp.END_USAGE = UIHelper.Floor((decimal)drTmpUage["TOTAL_POWER"], 0);
                        objTmp.METER_CODE = Method.FormatMeterCode(strMeterCode);
                        objTmp.BOX_CODE = "";
                        TBL_METER objMeter = DBDataContext.Db.TBL_METERs.FirstOrDefault(x => x.METER_CODE.ToLower().Contains(objTmp.METER_CODE.ToLower()));
                        if (objMeter == null)
                        {
                            isDigital = false;
                        }
                        else
                        {
                            isDigital = objMeter.IS_DIGITAL;
                        }
                        objTmp.IS_DIGITAL = isDigital;
                        objTmp.REGISTER_CODE = Method.GetMETER_REGCODE(objTmp.METER_CODE, isDigital);
                        DBDataContext.Db.TMP_IR_USAGEs.InsertOnSubmit(objTmp);
                    }
                    DBDataContext.Db.SubmitChanges();

                    IRDevice objd = new IRDevice();
                    objd.ProductID = cboDevice.Text;
                    objd.RunIRUsage((int)cboDevice.SelectedValue, intCollectID);

                    #endregion
                    tran.Complete();
                    blnReturn = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return blnReturn;
        }


        /// <summary>
        /// Form Validation
        /// </summary>
        /// <returns></returns>
        private bool Invalid()
        {
            this.ClearAllValidation();
            if (cboBillingCycle.SelectedIndex < 1)
            {
                cboBillingCycle.SetValidation(string.Format(Resources.REQUIRED, lblCYCLE_NAME.Text));
                return true;
            }

            if (cboDevice.SelectedIndex == -1)
            {
                cboDevice.SetValidation(string.Format(Resources.REQUIRED, lblCYCLE_NAME.Text));
                return true;
            }

            return false;
        }

        /// <summary>
        /// Is device connected and valid
        /// </summary>
        /// <param name="strDevice"></param>
        /// <returns></returns>
        private bool IsValidDevice()
        {
            return true;
        }

        private void LoadUnusalCustomer(int intBillingCycleID, int intCustomerHaveUsage)
        {
            //get none usge customer
            txtNoUsageCustomer.Text = Method.GetNoneUsageCustomer(intBillingCycleID, this.dtpMonth.Value.Date).ToString();

            //get unkown customer
            txtUnknownMeter.Text = DBDataContext.Db.TBL_METER_UNKNOWNs.Count().ToString();

            //get unregister meter
            txtUnRegister.Text = DBDataContext.Db.TBL_METERs.Where(x => x.REGISTER_CODE == string.Empty).Count().ToString();

            txtHaveUsageCustomer.Text = intCustomerHaveUsage.ToString();
        }


        #endregion Method

        #region Event
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (Invalid())
            {
                return;
            }

            Runner.Run(Submit);

            if (blnSuccess)
            {
                LoadUnusalCustomer((int)cboBillingCycle.SelectedValue, _dtMetrolData.Rows.Count);
                var diag = new DialogCustomerSpecialUsage();
                diag.cboCycle.SelectedValue = (int)cboBillingCycle.SelectedValue;
                diag.ShowDialog();
            }
        }


        bool blnSuccess = false;
        /// <summary>
        /// Submit usage to database
        /// </summary>
        private void Submit()
        {
            try
            {
                blnSuccess = false;

                var DeviceInfo = (from d in DBDataContext.Db.TBL_DEVICEs
                                  join ed in DBDataContext.Db.TBL_EMPLOYEE_DEVICEs on d.DEVICE_ID equals ed.DEVICE_ID
                                  where d.DEVICE_ID == (int)cboDevice.SelectedValue
                                  select new { d.DEVICE_ID, ed.EMPLOYEE_ID }).FirstOrDefault();


                //if this device not yet assigned to any collector
                //then use the current user to be collector
                if (DeviceInfo == null)
                {
                    MsgBox.ShowInformation(Resources.MS_DEVICE_NO_COLLECTOR);
                    return;
                }
                FileInfo f = new FileInfo(Settings.Default.METROLOGIC_DATA_READ);
                string fullpath = f.DirectoryName;
                metro = new Metrologic()
                {
                    DeviceID = cboDevice.Text,
                    ExecuteDataRead = Settings.Default.METROLOGIC_DATA_READ,
                    PathBackup = Settings.Default.PATH_BACKUP,
                    FileName = Path.Combine(fullpath, Settings.Default.METROLOGIC_FILE),
                    ConnectionMode = Metrologic.ConnectMode.IR_Cradle,
                    PortNumber = Settings.Default.METROLOGIC_COM_PORT,
                    BaudRateTransmit = Settings.Default.METROLOGIC_BAUDRATE,
                    AddLineFeed = true,
                    AddReturnCharacter = true,
                    KeepOnline = false,
                    PollingTime = 2,
                    SaveMethod = Metrologic.SaveMode.Overwrite,
                    ShowDialog = false,
                    ShowError = true,
                    ViewData = false,
                };
                _dtMetrolData = new DataTable();
                _dtMetrolData = metro.ReceivedData(',');

                if (metro.HasError)
                {
                    MsgBox.ShowError(new Exception(metro.MessageError));
                    return;
                }

                if (_dtMetrolData.Rows.Count < 0)
                {
                    MsgBox.ShowInformation(Resources.MS_DEVICE_NO_CUSTOMER_COLLECT_USAGE);
                    return;
                }

                //Load data from device and save to database
                blnSuccess = SaveUsage((int)cboBillingCycle.SelectedValue, DeviceInfo.EMPLOYEE_ID);

            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void cboBillingCycle_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Load Runing Month 
            LoadStartEndDate();
        }

        private void btnCheckNoUsage_Click(object sender, EventArgs e)
        {
            DialogCustomerNoUsage objDialog = new DialogCustomerNoUsage((int)cboBillingCycle.SelectedValue);
            objDialog.ShowDialog();
            LoadUnusalCustomer((int)cboBillingCycle.SelectedValue, _dtMetrolData.Rows.Count);
        }

        private void btnCheckUnknown_Click(object sender, EventArgs e)
        {
            DialogMeterUnknown objDialog = new DialogMeterUnknown();
            objDialog.ShowDialog();
        }

        private void btnCheckUnregister_Click(object sender, EventArgs e)
        {
            DialogMeterUnregister objDialog = new DialogMeterUnregister();
            objDialog.ShowDialog();
        }
        #endregion Event


    }
}
