﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogH203RegisterDevice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogH203RegisterDevice));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnOK = new SoftTech.Component.ExButton();
            this.lblREGISTER_CODE1 = new System.Windows.Forms.Label();
            this.lblREGISTER_CODE = new System.Windows.Forms.Label();
            this.imgREGISTER = new System.Windows.Forms.PictureBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgREGISTER)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.lblREGISTER_CODE1);
            this.content.Controls.Add(this.lblREGISTER_CODE);
            this.content.Controls.Add(this.imgREGISTER);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.panel1);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.imgREGISTER, 0);
            this.content.Controls.SetChildIndex(this.lblREGISTER_CODE, 0);
            this.content.Controls.SetChildIndex(this.lblREGISTER_CODE1, 0);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblREGISTER_CODE1
            // 
            resources.ApplyResources(this.lblREGISTER_CODE1, "lblREGISTER_CODE1");
            this.lblREGISTER_CODE1.Name = "lblREGISTER_CODE1";
            // 
            // lblREGISTER_CODE
            // 
            resources.ApplyResources(this.lblREGISTER_CODE, "lblREGISTER_CODE");
            this.lblREGISTER_CODE.Name = "lblREGISTER_CODE";
            // 
            // imgREGISTER
            // 
            this.imgREGISTER.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.imgREGISTER, "imgREGISTER");
            this.imgREGISTER.Name = "imgREGISTER";
            this.imgREGISTER.TabStop = false;
            // 
            // DialogH203RegisterDevice
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogH203RegisterDevice";
            this.Load += new System.EventHandler(this.DialogH203RegisterDevice_Load);
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgREGISTER)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private ExButton btnOK;
        private Panel panel1;
        private Label lblREGISTER_CODE1;
        private Label lblREGISTER_CODE;
        private PictureBox imgREGISTER;
    }
}