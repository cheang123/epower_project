﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PageMeter : Form
    {
        int statusId = 0;
        public int StatusID
        {
            get { return statusId; }
            private set { statusId = value; }
        }

        int meterTypeId = 0;
        public int MeterTypeID
        {
            get { return meterTypeId; }
            set { meterTypeId = value; }
        }
        public TBL_METER Meter
        {
            get
            {
                TBL_METER objMeter = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    try
                    {
                        int intMeterID = (int)dgv.SelectedRows[0].Cells["METER_ID"].Value;
                        objMeter = DBDataContext.Db.TBL_METERs.FirstOrDefault(x => x.METER_ID == intMeterID);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objMeter;
            }
        }
        #region Constructor
        public PageMeter()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            txt_QuickSearch(null, null);            
        }
        #endregion

        #region Operation
        private void btnNew_Click(object sender, EventArgs e)
        {
            TBL_METER objMeter = new TBL_METER() { METER_CODE = "" };
            if (cboMeterType.SelectedIndex != -1)
            {
                objMeter.METER_TYPE_ID = (int)cboMeterType.SelectedValue;
            }
            if (cboStatus.SelectedIndex != -1)
            {
                objMeter.STATUS_ID = (int)cboStatus.SelectedValue;
            }

            DialogMeter dig = new DialogMeter(GeneralProcess.Insert, objMeter);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.Meter.METER_ID);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                DialogMeter dig = new DialogMeter(GeneralProcess.Update, Meter);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.Meter.METER_ID);
                }
            }
        }

        private void txt_QuickSearch(object sender, EventArgs e)
        {
            try
            {
               
                if (cboMeterType.SelectedIndex !=-1)
                {
                    meterTypeId = (int)cboMeterType.SelectedValue;
                }
                if (cboStatus.SelectedIndex != -1)
                {
                    statusId = (int)cboStatus.SelectedValue;
                }
                dgv.DataSource = (from m in DBDataContext.Db.TBL_METERs
                                 join t in DBDataContext.Db.TBL_METER_TYPEs on m.METER_TYPE_ID equals t.METER_TYPE_ID
                                 join s in DBDataContext.Db.TLKP_METER_STATUS on m.STATUS_ID equals s.STATUS_ID
                                 join cm in( from m in DBDataContext.Db.TBL_METERs
                                             join cm in DBDataContext.Db.TBL_CUSTOMER_METERs on m.METER_ID equals cm.METER_ID 
                                             join c in DBDataContext.Db.TBL_CUSTOMERs on cm.CUSTOMER_ID equals c.CUSTOMER_ID
                                             where cm.IS_ACTIVE
                                             select new {
                                                 m.METER_ID,CUSTOMER_NAME=c.LAST_NAME_KH+" "+c.FIRST_NAME_KH
                                             }) on m.METER_ID equals cm.METER_ID into cms
                                 from x in cms.DefaultIfEmpty()
                                 where  (meterTypeId == 0 || t.METER_TYPE_ID == meterTypeId) &&
                                         (statusId == 0 || s.STATUS_ID == statusId) &&
                                         ((x==null?"":x.CUSTOMER_NAME)+m.METER_CODE).ToLower().Contains(txtQuickSearch.Text.ToLower().Trim())
                                 select new
                                 {
                                     m.METER_ID,
                                     m.METER_CODE,
                                     t.METER_TYPE_NAME,
                                     m.MULTIPLIER,
                                     s.STATUS_NAME,
                                     CUSTOMER_NAME = x==null?"":x.CUSTOMER_NAME
                                 }).Take(200);
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        /// <summary>
        /// Change current keyboard layout to khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }       
        #endregion        

        public void loadMeterType()
        {
            int tempMeter = MeterTypeID;
            DataTable dt = (from a in DBDataContext.Db.TBL_METER_TYPEs
                            where a.IS_ACTIVE
                            select a)._ToDataTable();
            //Create Row Templet
            DataRow dr = dt.NewRow();
            dr["METER_TYPE_ID"] = 0;
            dr["METER_TYPE_CODE"] =Resources.ALL_TYPE;
            dt.Rows.InsertAt(dr, 0);
            UIHelper.SetDataSourceToComboBox(cboMeterType, dt, "METER_TYPE_ID", "METER_TYPE_CODE");
            cboMeterType.SelectedValue = tempMeter;
        }

        public void loadMeterStatus()
        {
            int tempStatus = StatusID;
            DataTable dt = (from a in DBDataContext.Db.TLKP_METER_STATUS
                            select a)._ToDataTable();
            //Create Row Templet
            DataRow dr = dt.NewRow();
            dr["STATUS_ID"] = 0;
            dr["STATUS_NAME"] = Resources.ALL_STATUS;
            dt.Rows.InsertAt(dr, 0);
            UIHelper.SetDataSourceToComboBox(cboStatus, dt, "STATUS_ID", "STATUS_NAME");
            cboStatus.SelectedValue = tempStatus;
        }

        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }

        private void btnCUS_METER_USAGE_Click(object sender, EventArgs e)
        {
            if(this.Meter == null)
            {
                return;
            }
            new DialogCustomerMeterUsage(this.Meter).ShowDialog();
        }
    }
}