﻿using System.ComponentModel;
using System.Windows.Forms;
using dotnetCHARTING.WinForms;
using SoftTech.Component;
using Label = System.Windows.Forms.Label;

namespace EPower.Interface
{
    partial class DialogCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCustomer));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            dotnetCHARTING.WinForms.ChartArea chartArea1 = new dotnetCHARTING.WinForms.ChartArea();
            dotnetCHARTING.WinForms.Background background1 = new dotnetCHARTING.WinForms.Background();
            dotnetCHARTING.WinForms.Element element1 = new dotnetCHARTING.WinForms.Element();
            dotnetCHARTING.WinForms.SubValue subValue1 = new dotnetCHARTING.WinForms.SubValue();
            dotnetCHARTING.WinForms.Line line1 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Hotspot hotspot1 = new dotnetCHARTING.WinForms.Hotspot();
            dotnetCHARTING.WinForms.LegendEntry legendEntry1 = new dotnetCHARTING.WinForms.LegendEntry();
            dotnetCHARTING.WinForms.Background background2 = new dotnetCHARTING.WinForms.Background();
            dotnetCHARTING.WinForms.Line line2 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Hotspot hotspot2 = new dotnetCHARTING.WinForms.Hotspot();
            dotnetCHARTING.WinForms.Line line3 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Label label7 = new dotnetCHARTING.WinForms.Label();
            dotnetCHARTING.WinForms.Hotspot hotspot3 = new dotnetCHARTING.WinForms.Hotspot();
            dotnetCHARTING.WinForms.Truncation truncation1 = new dotnetCHARTING.WinForms.Truncation();
            dotnetCHARTING.WinForms.LegendBox legendBox1 = new dotnetCHARTING.WinForms.LegendBox();
            dotnetCHARTING.WinForms.Background background3 = new dotnetCHARTING.WinForms.Background();
            dotnetCHARTING.WinForms.LegendEntry legendEntry2 = new dotnetCHARTING.WinForms.LegendEntry();
            dotnetCHARTING.WinForms.Background background4 = new dotnetCHARTING.WinForms.Background();
            dotnetCHARTING.WinForms.Line line4 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Hotspot hotspot4 = new dotnetCHARTING.WinForms.Hotspot();
            dotnetCHARTING.WinForms.Label label8 = new dotnetCHARTING.WinForms.Label();
            dotnetCHARTING.WinForms.Hotspot hotspot5 = new dotnetCHARTING.WinForms.Hotspot();
            dotnetCHARTING.WinForms.Truncation truncation2 = new dotnetCHARTING.WinForms.Truncation();
            dotnetCHARTING.WinForms.LegendEntry legendEntry3 = new dotnetCHARTING.WinForms.LegendEntry();
            dotnetCHARTING.WinForms.Background background5 = new dotnetCHARTING.WinForms.Background();
            dotnetCHARTING.WinForms.Line line5 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Hotspot hotspot6 = new dotnetCHARTING.WinForms.Hotspot();
            dotnetCHARTING.WinForms.Line line6 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Line line7 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Shadow shadow1 = new dotnetCHARTING.WinForms.Shadow();
            dotnetCHARTING.WinForms.Line line8 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Shadow shadow2 = new dotnetCHARTING.WinForms.Shadow();
            dotnetCHARTING.WinForms.Box box1 = new dotnetCHARTING.WinForms.Box();
            dotnetCHARTING.WinForms.Background background6 = new dotnetCHARTING.WinForms.Background();
            dotnetCHARTING.WinForms.Line line9 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Label label9 = new dotnetCHARTING.WinForms.Label();
            dotnetCHARTING.WinForms.Hotspot hotspot7 = new dotnetCHARTING.WinForms.Hotspot();
            dotnetCHARTING.WinForms.Truncation truncation3 = new dotnetCHARTING.WinForms.Truncation();
            dotnetCHARTING.WinForms.Line line10 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Shadow shadow3 = new dotnetCHARTING.WinForms.Shadow();
            dotnetCHARTING.WinForms.Axis axis1 = new dotnetCHARTING.WinForms.Axis();
            dotnetCHARTING.WinForms.AxisTick axisTick1 = new dotnetCHARTING.WinForms.AxisTick();
            dotnetCHARTING.WinForms.Line line11 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Line line12 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.TimeIntervalAdvanced timeIntervalAdvanced1 = new dotnetCHARTING.WinForms.TimeIntervalAdvanced();
            dotnetCHARTING.WinForms.ScaleRange scaleRange1 = new dotnetCHARTING.WinForms.ScaleRange();
            dotnetCHARTING.WinForms.Line line13 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.TimeIntervalAdvanced timeIntervalAdvanced2 = new dotnetCHARTING.WinForms.TimeIntervalAdvanced();
            dotnetCHARTING.WinForms.AxisTick axisTick2 = new dotnetCHARTING.WinForms.AxisTick();
            dotnetCHARTING.WinForms.Line line14 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Line line15 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Axis axis2 = new dotnetCHARTING.WinForms.Axis();
            dotnetCHARTING.WinForms.AxisTick axisTick3 = new dotnetCHARTING.WinForms.AxisTick();
            dotnetCHARTING.WinForms.Line line16 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Line line17 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.ScaleRange scaleRange2 = new dotnetCHARTING.WinForms.ScaleRange();
            dotnetCHARTING.WinForms.Line line18 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.TimeIntervalAdvanced timeIntervalAdvanced3 = new dotnetCHARTING.WinForms.TimeIntervalAdvanced();
            dotnetCHARTING.WinForms.AxisTick axisTick4 = new dotnetCHARTING.WinForms.AxisTick();
            dotnetCHARTING.WinForms.Line line19 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Line line20 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Axis axis3 = new dotnetCHARTING.WinForms.Axis();
            dotnetCHARTING.WinForms.AxisTick axisTick5 = new dotnetCHARTING.WinForms.AxisTick();
            dotnetCHARTING.WinForms.Line line21 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Line line22 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.ScaleRange scaleRange3 = new dotnetCHARTING.WinForms.ScaleRange();
            dotnetCHARTING.WinForms.Line line23 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.TimeIntervalAdvanced timeIntervalAdvanced4 = new dotnetCHARTING.WinForms.TimeIntervalAdvanced();
            dotnetCHARTING.WinForms.AxisTick axisTick6 = new dotnetCHARTING.WinForms.AxisTick();
            dotnetCHARTING.WinForms.Line line24 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Line line25 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Element element2 = new dotnetCHARTING.WinForms.Element();
            dotnetCHARTING.WinForms.SubValue subValue2 = new dotnetCHARTING.WinForms.SubValue();
            dotnetCHARTING.WinForms.Line line26 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Hotspot hotspot8 = new dotnetCHARTING.WinForms.Hotspot();
            dotnetCHARTING.WinForms.LegendEntry legendEntry4 = new dotnetCHARTING.WinForms.LegendEntry();
            dotnetCHARTING.WinForms.Background background7 = new dotnetCHARTING.WinForms.Background();
            dotnetCHARTING.WinForms.Line line27 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Hotspot hotspot9 = new dotnetCHARTING.WinForms.Hotspot();
            dotnetCHARTING.WinForms.SmartLabel smartLabel1 = new dotnetCHARTING.WinForms.SmartLabel();
            dotnetCHARTING.WinForms.Hotspot hotspot10 = new dotnetCHARTING.WinForms.Hotspot();
            dotnetCHARTING.WinForms.Line line28 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Truncation truncation4 = new dotnetCHARTING.WinForms.Truncation();
            dotnetCHARTING.WinForms.Shadow shadow4 = new dotnetCHARTING.WinForms.Shadow();
            dotnetCHARTING.WinForms.Hotspot hotspot11 = new dotnetCHARTING.WinForms.Hotspot();
            dotnetCHARTING.WinForms.Truncation truncation5 = new dotnetCHARTING.WinForms.Truncation();
            dotnetCHARTING.WinForms.ChartArea chartArea2 = new dotnetCHARTING.WinForms.ChartArea();
            dotnetCHARTING.WinForms.Background background8 = new dotnetCHARTING.WinForms.Background();
            dotnetCHARTING.WinForms.Element element3 = new dotnetCHARTING.WinForms.Element();
            dotnetCHARTING.WinForms.SubValue subValue3 = new dotnetCHARTING.WinForms.SubValue();
            dotnetCHARTING.WinForms.Line line29 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Hotspot hotspot12 = new dotnetCHARTING.WinForms.Hotspot();
            dotnetCHARTING.WinForms.LegendEntry legendEntry5 = new dotnetCHARTING.WinForms.LegendEntry();
            dotnetCHARTING.WinForms.Background background9 = new dotnetCHARTING.WinForms.Background();
            dotnetCHARTING.WinForms.Line line30 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Hotspot hotspot13 = new dotnetCHARTING.WinForms.Hotspot();
            dotnetCHARTING.WinForms.Line line31 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Label label10 = new dotnetCHARTING.WinForms.Label();
            dotnetCHARTING.WinForms.Hotspot hotspot14 = new dotnetCHARTING.WinForms.Hotspot();
            dotnetCHARTING.WinForms.Truncation truncation6 = new dotnetCHARTING.WinForms.Truncation();
            dotnetCHARTING.WinForms.Line line32 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Shadow shadow5 = new dotnetCHARTING.WinForms.Shadow();
            dotnetCHARTING.WinForms.Box box2 = new dotnetCHARTING.WinForms.Box();
            dotnetCHARTING.WinForms.Background background10 = new dotnetCHARTING.WinForms.Background();
            dotnetCHARTING.WinForms.Line line33 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Label label11 = new dotnetCHARTING.WinForms.Label();
            dotnetCHARTING.WinForms.Hotspot hotspot15 = new dotnetCHARTING.WinForms.Hotspot();
            dotnetCHARTING.WinForms.Truncation truncation7 = new dotnetCHARTING.WinForms.Truncation();
            dotnetCHARTING.WinForms.Line line34 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Shadow shadow6 = new dotnetCHARTING.WinForms.Shadow();
            dotnetCHARTING.WinForms.Axis axis4 = new dotnetCHARTING.WinForms.Axis();
            dotnetCHARTING.WinForms.AxisTick axisTick7 = new dotnetCHARTING.WinForms.AxisTick();
            dotnetCHARTING.WinForms.Line line35 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Line line36 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.TimeIntervalAdvanced timeIntervalAdvanced5 = new dotnetCHARTING.WinForms.TimeIntervalAdvanced();
            dotnetCHARTING.WinForms.ScaleRange scaleRange4 = new dotnetCHARTING.WinForms.ScaleRange();
            dotnetCHARTING.WinForms.Line line37 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.TimeIntervalAdvanced timeIntervalAdvanced6 = new dotnetCHARTING.WinForms.TimeIntervalAdvanced();
            dotnetCHARTING.WinForms.AxisTick axisTick8 = new dotnetCHARTING.WinForms.AxisTick();
            dotnetCHARTING.WinForms.Line line38 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Line line39 = new dotnetCHARTING.WinForms.Line();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnSAVE = new SoftTech.Component.ExButton();
            this.cboArea = new System.Windows.Forms.ComboBox();
            this.lblAREA = new System.Windows.Forms.Label();
            this.txtPlaceofBirth = new System.Windows.Forms.TextBox();
            this.lblPLACE_OF_BIRTH = new System.Windows.Forms.Label();
            this.txtCustomerCode = new System.Windows.Forms.TextBox();
            this.lblCUSTOMER_CODE = new System.Windows.Forms.Label();
            this.picPhoto = new System.Windows.Forms.PictureBox();
            this.dtpDOB = new System.Windows.Forms.DateTimePicker();
            this.lblDATE_OF_BIRTH = new System.Windows.Forms.Label();
            this.lblADDRESS_INFORMATION = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.lblADDRESS = new System.Windows.Forms.Label();
            this.cboVillage = new System.Windows.Forms.ComboBox();
            this.cboCommune = new System.Windows.Forms.ComboBox();
            this.cboDistrict = new System.Windows.Forms.ComboBox();
            this.cboProvince = new System.Windows.Forms.ComboBox();
            this.lblVILLAGE = new System.Windows.Forms.Label();
            this.lblCOMMUNE = new System.Windows.Forms.Label();
            this.lblDISTRICT = new System.Windows.Forms.Label();
            this.lblPROVINCE = new System.Windows.Forms.Label();
            this.txtPhone2 = new System.Windows.Forms.TextBox();
            this.txtPhone1 = new System.Windows.Forms.TextBox();
            this.txtCompanyName = new System.Windows.Forms.TextBox();
            this.txtIdNumber = new System.Windows.Forms.TextBox();
            this.cboSex = new System.Windows.Forms.ComboBox();
            this.txtLastNameKH = new System.Windows.Forms.TextBox();
            this.txtFirstNameKH = new System.Windows.Forms.TextBox();
            this.lblNICKNAME = new System.Windows.Forms.Label();
            this.lblID_CARD_NUMBER = new System.Windows.Forms.Label();
            this.lblPhone_II = new System.Windows.Forms.Label();
            this.lblPHONE_I = new System.Windows.Forms.Label();
            this.lblSEX = new System.Windows.Forms.Label();
            this.lblLAST_NAME_KH = new System.Windows.Forms.Label();
            this.lblFIRST_NAME_KH = new System.Windows.Forms.Label();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.lblHOUSE_NO = new System.Windows.Forms.Label();
            this.lblSTREET_NO = new System.Windows.Forms.Label();
            this.txtHouseNo = new System.Windows.Forms.TextBox();
            this.txtStreetNo = new System.Windows.Forms.TextBox();
            this.lblLAST_NAME = new System.Windows.Forms.Label();
            this.lblFIRST_NAME = new System.Windows.Forms.Label();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.btnPHOTO = new SoftTech.Component.ExLinkLabel(this.components);
            this.label25 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.lblJOB = new System.Windows.Forms.Label();
            this.txtCheckDigit = new System.Windows.Forms.TextBox();
            this.tab = new System.Windows.Forms.TabControl();
            this.tabGENERAL_INFORMATION = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cboCustomerType = new System.Windows.Forms.ComboBox();
            this.lblTYPE_OF_CUSTOMERS = new System.Windows.Forms.Label();
            this.cboIdCardType = new System.Windows.Forms.ComboBox();
            this.lblID_CARD_NUMBER_TYPE = new System.Windows.Forms.Label();
            this.cboTitles = new System.Windows.Forms.ComboBox();
            this.lblTITLES = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTotalFamily = new System.Windows.Forms.TextBox();
            this.lblTOTAL_FAMILY = new System.Windows.Forms.Label();
            this.txtJob = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.tabUSAGE_INFORMATION = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPOWER_CAPACITY = new System.Windows.Forms.TextBox();
            this.chkCAPACITY_CHARGE = new System.Windows.Forms.CheckBox();
            this.lblPOWER_KW = new System.Windows.Forms.Label();
            this.txtPOWER_KW = new System.Windows.Forms.TextBox();
            this.chkIsmarketVendor = new System.Windows.Forms.CheckBox();
            this.btnRE_ACTIVE = new SoftTech.Component.ExButton();
            this.cboCustomerGroup = new System.Windows.Forms.ComboBox();
            this.cboConnectionType = new System.Windows.Forms.ComboBox();
            this.lblCHANGE_AMPARE = new SoftTech.Component.ExLinkLabel(this.components);
            this.dtpREQUEST_CONNECTION_DATE = new System.Windows.Forms.DateTimePicker();
            this.lblREQUEST_CONNECTION_DATE = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnADD = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnEDIT = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnREMOVE = new SoftTech.Component.ExLinkLabel(this.components);
            this.lblCONNECTION = new System.Windows.Forms.Label();
            this.btnREPORT_CONNECTION = new SoftTech.Component.ExButton();
            this.btnREPORT_DEPOSIT_AND_CONNECTION_FEE = new SoftTech.Component.ExButton();
            this.btnREPORT_AGREEMENT = new SoftTech.Component.ExButton();
            this.label35 = new System.Windows.Forms.Label();
            this.cboPrice = new System.Windows.Forms.ComboBox();
            this.txtTotalPower = new System.Windows.Forms.TextBox();
            this.lblTOTAL_WATT = new System.Windows.Forms.Label();
            this.cboBillingCycle = new System.Windows.Forms.ComboBox();
            this.dgvEquitment = new System.Windows.Forms.DataGridView();
            this.CUS_EQUIPMENT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EQUIPMENT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EQUIPMENT_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QUANTITY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WATT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_WATT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NOTE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._DB = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._EDITED = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.lblAMPERE = new System.Windows.Forms.Label();
            this.lblPRICE_AND_BILLING_CYCLE = new System.Windows.Forms.Label();
            this.lblPHASE = new System.Windows.Forms.Label();
            this.lblVOLTAGE = new System.Windows.Forms.Label();
            this.lblEQUIPMENT = new System.Windows.Forms.Label();
            this.cboPhase = new System.Windows.Forms.ComboBox();
            this.cboAMP = new System.Windows.Forms.ComboBox();
            this.cboVol = new System.Windows.Forms.ComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.tabDeposit = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnADJUST_DEPOSIT = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnAPPLY_PAYMENT = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnADD_DEPOSIT = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnREFUND_DEPOSIT = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnPRINT = new SoftTech.Component.ExLinkLabel(this.components);
            this.dgvBalanceDeposit = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvDeposit = new System.Windows.Forms.DataGridView();
            this.CUS_DEPOSIT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DEPOSIT_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_ON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_BY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DEPOSIT_ACTION_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BALANCE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D_CURRENCY_SING_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_PAID = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.lblTOTAL_DEPOSIT_AMOUNT = new System.Windows.Forms.Label();
            this.tabBILLING_AND_METER = new System.Windows.Forms.TabPage();
            this.btnREACTIVATE = new SoftTech.Component.ExButton();
            this.txtPositionInBox = new System.Windows.Forms.TextBox();
            this.lblPOSITION_IN_BOX = new System.Windows.Forms.Label();
            this.btnCHANGE_BOX = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnCHANGE_BREAKER = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnCHANGE_METER = new SoftTech.Component.ExLinkLabel(this.components);
            this.txtPoleCode = new System.Windows.Forms.TextBox();
            this.lblPOLE_AND_BOX_INSTALLATION = new System.Windows.Forms.Label();
            this.txtCutter = new System.Windows.Forms.TextBox();
            this.txtBiller = new System.Windows.Forms.TextBox();
            this.txtCollector = new System.Windows.Forms.TextBox();
            this.label71 = new System.Windows.Forms.Label();
            this.lblCUTTER = new System.Windows.Forms.Label();
            this.lblBILLER = new System.Windows.Forms.Label();
            this.lblCOLLECTOR = new System.Windows.Forms.Label();
            this.txtBox = new SoftTech.Component.ExTextbox();
            this.lblBOX = new System.Windows.Forms.Label();
            this.lblPOLE = new System.Windows.Forms.Label();
            this.lblCONSTANT_1 = new System.Windows.Forms.Label();
            this.txtBreakerConstant = new System.Windows.Forms.TextBox();
            this.txtBreakerType = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.lblBREAKER_INSTALLATION = new System.Windows.Forms.Label();
            this.lblAMPARE_2 = new System.Windows.Forms.Label();
            this.lblVOLTAGE_2 = new System.Windows.Forms.Label();
            this.lblPHASE_2 = new System.Windows.Forms.Label();
            this.txtBreakerAmp = new System.Windows.Forms.TextBox();
            this.txtBreakerVol = new System.Windows.Forms.TextBox();
            this.txtBreakerPhase = new System.Windows.Forms.TextBox();
            this.lblBREAKER_CODE = new System.Windows.Forms.Label();
            this.txtBreaker = new SoftTech.Component.ExTextbox();
            this.lblBREAKER_TYPE = new System.Windows.Forms.Label();
            this.lblCONSTANT = new System.Windows.Forms.Label();
            this.txtMeterConstant = new System.Windows.Forms.TextBox();
            this.txtMeterType = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.lblAMPARE = new System.Windows.Forms.Label();
            this.lblVOLTAGE_1 = new System.Windows.Forms.Label();
            this.lblPHASE_1 = new System.Windows.Forms.Label();
            this.lblCABLE_SHIELD = new System.Windows.Forms.Label();
            this.lblSHIELD = new System.Windows.Forms.Label();
            this.txtMeterAmp = new System.Windows.Forms.TextBox();
            this.txtMeterVol = new System.Windows.Forms.TextBox();
            this.txtMeterPhase = new System.Windows.Forms.TextBox();
            this.lblMETER_CODE = new System.Windows.Forms.Label();
            this.lblMETER_INSTALLATION = new System.Windows.Forms.Label();
            this.lblMETER_TYPE = new System.Windows.Forms.Label();
            this.cboCableShield = new System.Windows.Forms.ComboBox();
            this.cboMeterShield = new System.Windows.Forms.ComboBox();
            this.txtMeter = new SoftTech.Component.ExTextbox();
            this.dtpActivateDate = new System.Windows.Forms.DateTimePicker();
            this.label45 = new System.Windows.Forms.Label();
            this.lblACTIVATE_DATE = new System.Windows.Forms.Label();
            this.nudCutoffDays = new System.Windows.Forms.NumericUpDown();
            this.lblCUT_DAY = new System.Windows.Forms.Label();
            this.lblUNLIMITE_CUT_DAY = new System.Windows.Forms.Label();
            this.tabRECURRING_SERVICE = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnADD_1 = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnEDIT_1 = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnREMOVE_1 = new SoftTech.Component.ExLinkLabel(this.components);
            this.dgvService = new System.Windows.Forms.DataGridView();
            this.CUSTOMER_SERVICE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SERVICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LAST_BILLING_MONTH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RECURRING_MONTH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NEXT_BILLING_MONTH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPREPAYMENT = new System.Windows.Forms.TabPage();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnSETTLE_INVOICE = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnAPPLY_PREPAYMENT = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnRETURN_PREPAYMENT = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnPRINT_2 = new SoftTech.Component.ExLinkLabel(this.components);
            this.dgvBalancePrepayment = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PREPAYMENT_BALANCE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FORMAT_BALANCE_PREPAYMENT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvPrepayment = new System.Windows.Forms.DataGridView();
            this.CUS_PREPAYMENT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAYMENT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PREPAYMENT_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PREPAYMENT_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PREPAYMENT_BY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_PREPAYMENT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BALANCE_PREPAYMENT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_ID_P = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PREPAYMENT_NOTE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FORMAT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colACTION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabInvoice = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnADJUST_AMOUNT = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnADJUST = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnDELETE = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnPRINT_1 = new SoftTech.Component.ExLinkLabel(this.components);
            this.lblTOTAL_DUE_AMOUNT = new System.Windows.Forms.Label();
            this.dgvTotalBalance = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICE_BALANCE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FORMAT_BALNCE_INVOICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnREPORT_CUSTOMER_AR = new SoftTech.Component.ExButton();
            this.btnADD_SERVICE = new SoftTech.Component.ExButton();
            this.btnAPPLY_PAYMENT_1 = new SoftTech.Component.ExButton();
            this.cboInvoiceStatus = new System.Windows.Forms.ComboBox();
            this.dgvInvoiceList = new System.Windows.Forms.DataGridView();
            this.INVOICE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICE_STATUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICE_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICE_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICE_TITLE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SETTLE_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAID_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DUE_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_SING_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DUE_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DUE_DAY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_SERVICE_BILL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FORMAT_INVOICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblDATE = new System.Windows.Forms.Label();
            this.dtpYear = new System.Windows.Forms.DateTimePicker();
            this.tabUSAGE = new System.Windows.Forms.TabPage();
            this.btnREPORT = new SoftTech.Component.ExButton();
            this.chart1 = new dotnetCHARTING.WinForms.Chart();
            this.tabAttachment = new System.Windows.Forms.TabPage();
            this.txtSearchAttachment = new SoftTech.Component.ExTextbox();
            this.btnREMOVE_ATTACHMENT = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnADDNEW_ATTCHMENT = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnVIEW_ATTACHMENT = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnADJUST_ATTACHMENT = new SoftTech.Component.ExLinkLabel(this.components);
            this.dgvAttachment = new System.Windows.Forms.DataGridView();
            this.CUSTOMER_ATTACHMENT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ATTACHMENT_NAME = new System.Windows.Forms.DataGridViewLinkColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DOWNLOAD_ATTACHMENT = new System.Windows.Forms.DataGridViewLinkColumn();
            this.FILE_NAME_TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SIZE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATED_ON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATED_BY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_ACTIVE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnActivate = new SoftTech.Component.ExButton();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.cmsStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ChangeConnectionType = new System.Windows.Forms.ToolStripMenuItem();
            this.ChangeBasePrice = new System.Windows.Forms.ToolStripMenuItem();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPhoto)).BeginInit();
            this.tab.SuspendLayout();
            this.tabGENERAL_INFORMATION.SuspendLayout();
            this.tabUSAGE_INFORMATION.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEquitment)).BeginInit();
            this.tabDeposit.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBalanceDeposit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeposit)).BeginInit();
            this.tabBILLING_AND_METER.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCutoffDays)).BeginInit();
            this.tabRECURRING_SERVICE.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvService)).BeginInit();
            this.tabPREPAYMENT.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBalancePrepayment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrepayment)).BeginInit();
            this.tabInvoice.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTotalBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInvoiceList)).BeginInit();
            this.tabUSAGE.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.tabAttachment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAttachment)).BeginInit();
            this.cmsStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.tab);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnSAVE);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.btnActivate);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnActivate, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.btnSAVE, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.tab, 0);
            // 
            // btnSAVE
            // 
            resources.ApplyResources(this.btnSAVE, "btnSAVE");
            this.btnSAVE.Name = "btnSAVE";
            this.btnSAVE.UseVisualStyleBackColor = true;
            this.btnSAVE.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cboArea
            // 
            this.cboArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboArea, "cboArea");
            this.cboArea.FormattingEnabled = true;
            this.cboArea.Name = "cboArea";
            // 
            // lblAREA
            // 
            resources.ApplyResources(this.lblAREA, "lblAREA");
            this.lblAREA.Name = "lblAREA";
            // 
            // txtPlaceofBirth
            // 
            resources.ApplyResources(this.txtPlaceofBirth, "txtPlaceofBirth");
            this.txtPlaceofBirth.Name = "txtPlaceofBirth";
            this.txtPlaceofBirth.Enter += new System.EventHandler(this.InputKhmer);
            // 
            // lblPLACE_OF_BIRTH
            // 
            resources.ApplyResources(this.lblPLACE_OF_BIRTH, "lblPLACE_OF_BIRTH");
            this.lblPLACE_OF_BIRTH.Name = "lblPLACE_OF_BIRTH";
            // 
            // txtCustomerCode
            // 
            resources.ApplyResources(this.txtCustomerCode, "txtCustomerCode");
            this.txtCustomerCode.Name = "txtCustomerCode";
            this.txtCustomerCode.ReadOnly = true;
            this.txtCustomerCode.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // lblCUSTOMER_CODE
            // 
            resources.ApplyResources(this.lblCUSTOMER_CODE, "lblCUSTOMER_CODE");
            this.lblCUSTOMER_CODE.Name = "lblCUSTOMER_CODE";
            // 
            // picPhoto
            // 
            this.picPhoto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            resources.ApplyResources(this.picPhoto, "picPhoto");
            this.picPhoto.Name = "picPhoto";
            this.picPhoto.TabStop = false;
            // 
            // dtpDOB
            // 
            this.dtpDOB.CausesValidation = false;
            this.dtpDOB.Checked = false;
            resources.ApplyResources(this.dtpDOB, "dtpDOB");
            this.dtpDOB.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDOB.Name = "dtpDOB";
            this.dtpDOB.ShowCheckBox = true;
            this.dtpDOB.Enter += new System.EventHandler(this.InputEnglish);
            this.dtpDOB.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dtpDOB_MouseDown);
            // 
            // lblDATE_OF_BIRTH
            // 
            resources.ApplyResources(this.lblDATE_OF_BIRTH, "lblDATE_OF_BIRTH");
            this.lblDATE_OF_BIRTH.Name = "lblDATE_OF_BIRTH";
            // 
            // lblADDRESS_INFORMATION
            // 
            resources.ApplyResources(this.lblADDRESS_INFORMATION, "lblADDRESS_INFORMATION");
            this.lblADDRESS_INFORMATION.Name = "lblADDRESS_INFORMATION";
            // 
            // txtAddress
            // 
            resources.ApplyResources(this.txtAddress, "txtAddress");
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Enter += new System.EventHandler(this.InputKhmer);
            // 
            // lblADDRESS
            // 
            resources.ApplyResources(this.lblADDRESS, "lblADDRESS");
            this.lblADDRESS.Name = "lblADDRESS";
            // 
            // cboVillage
            // 
            this.cboVillage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboVillage, "cboVillage");
            this.cboVillage.FormattingEnabled = true;
            this.cboVillage.Name = "cboVillage";
            this.cboVillage.SelectedIndexChanged += new System.EventHandler(this.cboVillage_SelectedIndexChanged);
            // 
            // cboCommune
            // 
            this.cboCommune.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboCommune, "cboCommune");
            this.cboCommune.FormattingEnabled = true;
            this.cboCommune.Name = "cboCommune";
            this.cboCommune.SelectedIndexChanged += new System.EventHandler(this.cboCommune_SelectedIndexChanged);
            // 
            // cboDistrict
            // 
            this.cboDistrict.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboDistrict, "cboDistrict");
            this.cboDistrict.FormattingEnabled = true;
            this.cboDistrict.Name = "cboDistrict";
            this.cboDistrict.SelectedIndexChanged += new System.EventHandler(this.cboDistrict_SelectedIndexChanged);
            // 
            // cboProvince
            // 
            this.cboProvince.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboProvince, "cboProvince");
            this.cboProvince.FormattingEnabled = true;
            this.cboProvince.Name = "cboProvince";
            this.cboProvince.SelectedIndexChanged += new System.EventHandler(this.cboProvince_SelectedIndexChanged);
            // 
            // lblVILLAGE
            // 
            resources.ApplyResources(this.lblVILLAGE, "lblVILLAGE");
            this.lblVILLAGE.Name = "lblVILLAGE";
            // 
            // lblCOMMUNE
            // 
            resources.ApplyResources(this.lblCOMMUNE, "lblCOMMUNE");
            this.lblCOMMUNE.Name = "lblCOMMUNE";
            // 
            // lblDISTRICT
            // 
            resources.ApplyResources(this.lblDISTRICT, "lblDISTRICT");
            this.lblDISTRICT.Name = "lblDISTRICT";
            // 
            // lblPROVINCE
            // 
            resources.ApplyResources(this.lblPROVINCE, "lblPROVINCE");
            this.lblPROVINCE.Name = "lblPROVINCE";
            // 
            // txtPhone2
            // 
            resources.ApplyResources(this.txtPhone2, "txtPhone2");
            this.txtPhone2.Name = "txtPhone2";
            this.txtPhone2.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // txtPhone1
            // 
            resources.ApplyResources(this.txtPhone1, "txtPhone1");
            this.txtPhone1.Name = "txtPhone1";
            this.txtPhone1.Enter += new System.EventHandler(this.InputEnglish);
            this.txtPhone1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPhone1_KeyPress);
            // 
            // txtCompanyName
            // 
            resources.ApplyResources(this.txtCompanyName, "txtCompanyName");
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Enter += new System.EventHandler(this.InputKhmer);
            // 
            // txtIdNumber
            // 
            resources.ApplyResources(this.txtIdNumber, "txtIdNumber");
            this.txtIdNumber.Name = "txtIdNumber";
            this.txtIdNumber.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // cboSex
            // 
            this.cboSex.AllowDrop = true;
            this.cboSex.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboSex, "cboSex");
            this.cboSex.FormattingEnabled = true;
            this.cboSex.Name = "cboSex";
            // 
            // txtLastNameKH
            // 
            resources.ApplyResources(this.txtLastNameKH, "txtLastNameKH");
            this.txtLastNameKH.Name = "txtLastNameKH";
            this.txtLastNameKH.Enter += new System.EventHandler(this.InputKhmer);
            // 
            // txtFirstNameKH
            // 
            resources.ApplyResources(this.txtFirstNameKH, "txtFirstNameKH");
            this.txtFirstNameKH.Name = "txtFirstNameKH";
            this.txtFirstNameKH.Enter += new System.EventHandler(this.InputKhmer);
            // 
            // lblNICKNAME
            // 
            resources.ApplyResources(this.lblNICKNAME, "lblNICKNAME");
            this.lblNICKNAME.Name = "lblNICKNAME";
            // 
            // lblID_CARD_NUMBER
            // 
            resources.ApplyResources(this.lblID_CARD_NUMBER, "lblID_CARD_NUMBER");
            this.lblID_CARD_NUMBER.Name = "lblID_CARD_NUMBER";
            // 
            // lblPhone_II
            // 
            resources.ApplyResources(this.lblPhone_II, "lblPhone_II");
            this.lblPhone_II.Name = "lblPhone_II";
            // 
            // lblPHONE_I
            // 
            resources.ApplyResources(this.lblPHONE_I, "lblPHONE_I");
            this.lblPHONE_I.Name = "lblPHONE_I";
            // 
            // lblSEX
            // 
            resources.ApplyResources(this.lblSEX, "lblSEX");
            this.lblSEX.Name = "lblSEX";
            // 
            // lblLAST_NAME_KH
            // 
            resources.ApplyResources(this.lblLAST_NAME_KH, "lblLAST_NAME_KH");
            this.lblLAST_NAME_KH.Name = "lblLAST_NAME_KH";
            // 
            // lblFIRST_NAME_KH
            // 
            resources.ApplyResources(this.lblFIRST_NAME_KH, "lblFIRST_NAME_KH");
            this.lblFIRST_NAME_KH.Name = "lblFIRST_NAME_KH";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblHOUSE_NO
            // 
            resources.ApplyResources(this.lblHOUSE_NO, "lblHOUSE_NO");
            this.lblHOUSE_NO.Name = "lblHOUSE_NO";
            // 
            // lblSTREET_NO
            // 
            resources.ApplyResources(this.lblSTREET_NO, "lblSTREET_NO");
            this.lblSTREET_NO.Name = "lblSTREET_NO";
            // 
            // txtHouseNo
            // 
            resources.ApplyResources(this.txtHouseNo, "txtHouseNo");
            this.txtHouseNo.Name = "txtHouseNo";
            this.txtHouseNo.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // txtStreetNo
            // 
            resources.ApplyResources(this.txtStreetNo, "txtStreetNo");
            this.txtStreetNo.Name = "txtStreetNo";
            this.txtStreetNo.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // lblLAST_NAME
            // 
            resources.ApplyResources(this.lblLAST_NAME, "lblLAST_NAME");
            this.lblLAST_NAME.Name = "lblLAST_NAME";
            // 
            // lblFIRST_NAME
            // 
            resources.ApplyResources(this.lblFIRST_NAME, "lblFIRST_NAME");
            this.lblFIRST_NAME.Name = "lblFIRST_NAME";
            // 
            // txtLastName
            // 
            resources.ApplyResources(this.txtLastName, "txtLastName");
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // txtFirstName
            // 
            resources.ApplyResources(this.txtFirstName, "txtFirstName");
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // btnPHOTO
            // 
            resources.ApplyResources(this.btnPHOTO, "btnPHOTO");
            this.btnPHOTO.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnPHOTO.Name = "btnPHOTO";
            this.btnPHOTO.TabStop = true;
            this.btnPHOTO.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkPhoto_LinkClicked);
            // 
            // label25
            // 
            resources.ApplyResources(this.label25, "label25");
            this.label25.ForeColor = System.Drawing.Color.Red;
            this.label25.Name = "label25";
            // 
            // label27
            // 
            resources.ApplyResources(this.label27, "label27");
            this.label27.ForeColor = System.Drawing.Color.Red;
            this.label27.Name = "label27";
            // 
            // label28
            // 
            resources.ApplyResources(this.label28, "label28");
            this.label28.ForeColor = System.Drawing.Color.Red;
            this.label28.Name = "label28";
            // 
            // label34
            // 
            resources.ApplyResources(this.label34, "label34");
            this.label34.ForeColor = System.Drawing.Color.Red;
            this.label34.Name = "label34";
            // 
            // label36
            // 
            resources.ApplyResources(this.label36, "label36");
            this.label36.ForeColor = System.Drawing.Color.Red;
            this.label36.Name = "label36";
            // 
            // label37
            // 
            resources.ApplyResources(this.label37, "label37");
            this.label37.ForeColor = System.Drawing.Color.Red;
            this.label37.Name = "label37";
            // 
            // label38
            // 
            resources.ApplyResources(this.label38, "label38");
            this.label38.ForeColor = System.Drawing.Color.Red;
            this.label38.Name = "label38";
            // 
            // label39
            // 
            resources.ApplyResources(this.label39, "label39");
            this.label39.ForeColor = System.Drawing.Color.Red;
            this.label39.Name = "label39";
            // 
            // lblJOB
            // 
            resources.ApplyResources(this.lblJOB, "lblJOB");
            this.lblJOB.Name = "lblJOB";
            // 
            // txtCheckDigit
            // 
            resources.ApplyResources(this.txtCheckDigit, "txtCheckDigit");
            this.txtCheckDigit.Name = "txtCheckDigit";
            this.txtCheckDigit.ReadOnly = true;
            this.txtCheckDigit.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // tab
            // 
            this.tab.Controls.Add(this.tabGENERAL_INFORMATION);
            this.tab.Controls.Add(this.tabUSAGE_INFORMATION);
            this.tab.Controls.Add(this.tabDeposit);
            this.tab.Controls.Add(this.tabBILLING_AND_METER);
            this.tab.Controls.Add(this.tabRECURRING_SERVICE);
            this.tab.Controls.Add(this.tabPREPAYMENT);
            this.tab.Controls.Add(this.tabInvoice);
            this.tab.Controls.Add(this.tabUSAGE);
            this.tab.Controls.Add(this.tabAttachment);
            resources.ApplyResources(this.tab, "tab");
            this.tab.Name = "tab";
            this.tab.SelectedIndex = 0;
            this.tab.SelectedIndexChanged += new System.EventHandler(this.tab_SelectedIndexChanged);
            // 
            // tabGENERAL_INFORMATION
            // 
            this.tabGENERAL_INFORMATION.Controls.Add(this.label5);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label4);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label2);
            this.tabGENERAL_INFORMATION.Controls.Add(this.cboCustomerType);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblTYPE_OF_CUSTOMERS);
            this.tabGENERAL_INFORMATION.Controls.Add(this.cboIdCardType);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblID_CARD_NUMBER_TYPE);
            this.tabGENERAL_INFORMATION.Controls.Add(this.cboTitles);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblTITLES);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label3);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtTotalFamily);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblTOTAL_FAMILY);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblFIRST_NAME_KH);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblLAST_NAME_KH);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtJob);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblSEX);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblPHONE_I);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblJOB);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblPhone_II);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblID_CARD_NUMBER);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblNICKNAME);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtFirstNameKH);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtLastNameKH);
            this.tabGENERAL_INFORMATION.Controls.Add(this.cboSex);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtIdNumber);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtCompanyName);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtPhone1);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtPhone2);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblPROVINCE);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblDISTRICT);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblCOMMUNE);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblVILLAGE);
            this.tabGENERAL_INFORMATION.Controls.Add(this.cboProvince);
            this.tabGENERAL_INFORMATION.Controls.Add(this.cboDistrict);
            this.tabGENERAL_INFORMATION.Controls.Add(this.cboCommune);
            this.tabGENERAL_INFORMATION.Controls.Add(this.cboVillage);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblADDRESS);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtAddress);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblADDRESS_INFORMATION);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblDATE_OF_BIRTH);
            this.tabGENERAL_INFORMATION.Controls.Add(this.dtpDOB);
            this.tabGENERAL_INFORMATION.Controls.Add(this.picPhoto);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblCUSTOMER_CODE);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtCustomerCode);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblPLACE_OF_BIRTH);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtPlaceofBirth);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblAREA);
            this.tabGENERAL_INFORMATION.Controls.Add(this.cboArea);
            this.tabGENERAL_INFORMATION.Controls.Add(this.btnPHOTO);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblHOUSE_NO);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtFirstName);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblSTREET_NO);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtLastName);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtHouseNo);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblFIRST_NAME);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtStreetNo);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblLAST_NAME);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtCheckDigit);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label46);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label38);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label27);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label25);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label39);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label37);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label36);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label34);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label28);
            resources.ApplyResources(this.tabGENERAL_INFORMATION, "tabGENERAL_INFORMATION");
            this.tabGENERAL_INFORMATION.Name = "tabGENERAL_INFORMATION";
            this.tabGENERAL_INFORMATION.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Name = "label5";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Name = "label4";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Name = "label2";
            // 
            // cboCustomerType
            // 
            this.cboCustomerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboCustomerType, "cboCustomerType");
            this.cboCustomerType.FormattingEnabled = true;
            this.cboCustomerType.Name = "cboCustomerType";
            // 
            // lblTYPE_OF_CUSTOMERS
            // 
            resources.ApplyResources(this.lblTYPE_OF_CUSTOMERS, "lblTYPE_OF_CUSTOMERS");
            this.lblTYPE_OF_CUSTOMERS.Name = "lblTYPE_OF_CUSTOMERS";
            // 
            // cboIdCardType
            // 
            this.cboIdCardType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboIdCardType, "cboIdCardType");
            this.cboIdCardType.FormattingEnabled = true;
            this.cboIdCardType.Name = "cboIdCardType";
            // 
            // lblID_CARD_NUMBER_TYPE
            // 
            resources.ApplyResources(this.lblID_CARD_NUMBER_TYPE, "lblID_CARD_NUMBER_TYPE");
            this.lblID_CARD_NUMBER_TYPE.Name = "lblID_CARD_NUMBER_TYPE";
            // 
            // cboTitles
            // 
            this.cboTitles.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboTitles, "cboTitles");
            this.cboTitles.FormattingEnabled = true;
            this.cboTitles.Name = "cboTitles";
            this.cboTitles.SelectedIndexChanged += new System.EventHandler(this.cboTitles_SelectedIndexChanged_1);
            // 
            // lblTITLES
            // 
            resources.ApplyResources(this.lblTITLES, "lblTITLES");
            this.lblTITLES.Name = "lblTITLES";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Name = "label3";
            // 
            // txtTotalFamily
            // 
            resources.ApplyResources(this.txtTotalFamily, "txtTotalFamily");
            this.txtTotalFamily.Name = "txtTotalFamily";
            this.txtTotalFamily.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NumberOnly);
            // 
            // lblTOTAL_FAMILY
            // 
            resources.ApplyResources(this.lblTOTAL_FAMILY, "lblTOTAL_FAMILY");
            this.lblTOTAL_FAMILY.Name = "lblTOTAL_FAMILY";
            // 
            // txtJob
            // 
            resources.ApplyResources(this.txtJob, "txtJob");
            this.txtJob.Name = "txtJob";
            this.txtJob.Enter += new System.EventHandler(this.InputKhmer);
            // 
            // label46
            // 
            resources.ApplyResources(this.label46, "label46");
            this.label46.ForeColor = System.Drawing.Color.Red;
            this.label46.Name = "label46";
            // 
            // tabUSAGE_INFORMATION
            // 
            this.tabUSAGE_INFORMATION.Controls.Add(this.label6);
            this.tabUSAGE_INFORMATION.Controls.Add(this.txtPOWER_CAPACITY);
            this.tabUSAGE_INFORMATION.Controls.Add(this.chkCAPACITY_CHARGE);
            this.tabUSAGE_INFORMATION.Controls.Add(this.lblPOWER_KW);
            this.tabUSAGE_INFORMATION.Controls.Add(this.txtPOWER_KW);
            this.tabUSAGE_INFORMATION.Controls.Add(this.chkIsmarketVendor);
            this.tabUSAGE_INFORMATION.Controls.Add(this.btnRE_ACTIVE);
            this.tabUSAGE_INFORMATION.Controls.Add(this.cboCustomerGroup);
            this.tabUSAGE_INFORMATION.Controls.Add(this.cboConnectionType);
            this.tabUSAGE_INFORMATION.Controls.Add(this.lblCHANGE_AMPARE);
            this.tabUSAGE_INFORMATION.Controls.Add(this.dtpREQUEST_CONNECTION_DATE);
            this.tabUSAGE_INFORMATION.Controls.Add(this.lblREQUEST_CONNECTION_DATE);
            this.tabUSAGE_INFORMATION.Controls.Add(this.panel1);
            this.tabUSAGE_INFORMATION.Controls.Add(this.lblCONNECTION);
            this.tabUSAGE_INFORMATION.Controls.Add(this.btnREPORT_CONNECTION);
            this.tabUSAGE_INFORMATION.Controls.Add(this.btnREPORT_DEPOSIT_AND_CONNECTION_FEE);
            this.tabUSAGE_INFORMATION.Controls.Add(this.btnREPORT_AGREEMENT);
            this.tabUSAGE_INFORMATION.Controls.Add(this.label35);
            this.tabUSAGE_INFORMATION.Controls.Add(this.cboPrice);
            this.tabUSAGE_INFORMATION.Controls.Add(this.txtTotalPower);
            this.tabUSAGE_INFORMATION.Controls.Add(this.lblTOTAL_WATT);
            this.tabUSAGE_INFORMATION.Controls.Add(this.cboBillingCycle);
            this.tabUSAGE_INFORMATION.Controls.Add(this.dgvEquitment);
            this.tabUSAGE_INFORMATION.Controls.Add(this.lblAMPERE);
            this.tabUSAGE_INFORMATION.Controls.Add(this.lblPRICE_AND_BILLING_CYCLE);
            this.tabUSAGE_INFORMATION.Controls.Add(this.lblPHASE);
            this.tabUSAGE_INFORMATION.Controls.Add(this.lblVOLTAGE);
            this.tabUSAGE_INFORMATION.Controls.Add(this.lblEQUIPMENT);
            this.tabUSAGE_INFORMATION.Controls.Add(this.cboPhase);
            this.tabUSAGE_INFORMATION.Controls.Add(this.cboAMP);
            this.tabUSAGE_INFORMATION.Controls.Add(this.cboVol);
            this.tabUSAGE_INFORMATION.Controls.Add(this.label32);
            this.tabUSAGE_INFORMATION.Controls.Add(this.label30);
            this.tabUSAGE_INFORMATION.Controls.Add(this.label33);
            this.tabUSAGE_INFORMATION.Controls.Add(this.label31);
            resources.ApplyResources(this.tabUSAGE_INFORMATION, "tabUSAGE_INFORMATION");
            this.tabUSAGE_INFORMATION.Name = "tabUSAGE_INFORMATION";
            this.tabUSAGE_INFORMATION.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // txtPOWER_CAPACITY
            // 
            resources.ApplyResources(this.txtPOWER_CAPACITY, "txtPOWER_CAPACITY");
            this.txtPOWER_CAPACITY.Name = "txtPOWER_CAPACITY";
            this.txtPOWER_CAPACITY.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // chkCAPACITY_CHARGE
            // 
            resources.ApplyResources(this.chkCAPACITY_CHARGE, "chkCAPACITY_CHARGE");
            this.chkCAPACITY_CHARGE.Name = "chkCAPACITY_CHARGE";
            this.chkCAPACITY_CHARGE.UseVisualStyleBackColor = true;
            this.chkCAPACITY_CHARGE.CheckedChanged += new System.EventHandler(this.chkCAPACITY_CHARGE_CheckedChanged);
            // 
            // lblPOWER_KW
            // 
            resources.ApplyResources(this.lblPOWER_KW, "lblPOWER_KW");
            this.lblPOWER_KW.Name = "lblPOWER_KW";
            // 
            // txtPOWER_KW
            // 
            resources.ApplyResources(this.txtPOWER_KW, "txtPOWER_KW");
            this.txtPOWER_KW.Name = "txtPOWER_KW";
            this.txtPOWER_KW.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // chkIsmarketVendor
            // 
            resources.ApplyResources(this.chkIsmarketVendor, "chkIsmarketVendor");
            this.chkIsmarketVendor.Name = "chkIsmarketVendor";
            this.chkIsmarketVendor.UseVisualStyleBackColor = true;
            // 
            // btnRE_ACTIVE
            // 
            resources.ApplyResources(this.btnRE_ACTIVE, "btnRE_ACTIVE");
            this.btnRE_ACTIVE.Name = "btnRE_ACTIVE";
            this.btnRE_ACTIVE.UseVisualStyleBackColor = true;
            this.btnRE_ACTIVE.Click += new System.EventHandler(this.btnRE_ACTIVE_Click);
            // 
            // cboCustomerGroup
            // 
            this.cboCustomerGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomerGroup.DropDownWidth = 350;
            this.cboCustomerGroup.FormattingEnabled = true;
            resources.ApplyResources(this.cboCustomerGroup, "cboCustomerGroup");
            this.cboCustomerGroup.Name = "cboCustomerGroup";
            this.cboCustomerGroup.SelectedIndexChanged += new System.EventHandler(this.cboCustomerGroup_SelectedIndexChanged);
            // 
            // cboConnectionType
            // 
            this.cboConnectionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboConnectionType.DropDownWidth = 300;
            this.cboConnectionType.FormattingEnabled = true;
            resources.ApplyResources(this.cboConnectionType, "cboConnectionType");
            this.cboConnectionType.Name = "cboConnectionType";
            this.cboConnectionType.SelectedIndexChanged += new System.EventHandler(this.cboConnectionType_SelectedIndexChanged);
            // 
            // lblCHANGE_AMPARE
            // 
            resources.ApplyResources(this.lblCHANGE_AMPARE, "lblCHANGE_AMPARE");
            this.lblCHANGE_AMPARE.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblCHANGE_AMPARE.Name = "lblCHANGE_AMPARE";
            this.lblCHANGE_AMPARE.TabStop = true;
            this.lblCHANGE_AMPARE.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblCHANGE_AMPARE_LinkClicked);
            // 
            // dtpREQUEST_CONNECTION_DATE
            // 
            resources.ApplyResources(this.dtpREQUEST_CONNECTION_DATE, "dtpREQUEST_CONNECTION_DATE");
            this.dtpREQUEST_CONNECTION_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpREQUEST_CONNECTION_DATE.Name = "dtpREQUEST_CONNECTION_DATE";
            this.dtpREQUEST_CONNECTION_DATE.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // lblREQUEST_CONNECTION_DATE
            // 
            resources.ApplyResources(this.lblREQUEST_CONNECTION_DATE, "lblREQUEST_CONNECTION_DATE");
            this.lblREQUEST_CONNECTION_DATE.Name = "lblREQUEST_CONNECTION_DATE";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnADD);
            this.panel1.Controls.Add(this.btnEDIT);
            this.panel1.Controls.Add(this.btnREMOVE);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnADD
            // 
            resources.ApplyResources(this.btnADD, "btnADD");
            this.btnADD.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnADD.Name = "btnADD";
            this.btnADD.TabStop = true;
            this.btnADD.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnAdd_LinkClicked);
            // 
            // btnEDIT
            // 
            resources.ApplyResources(this.btnEDIT, "btnEDIT");
            this.btnEDIT.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnEDIT.Name = "btnEDIT";
            this.btnEDIT.TabStop = true;
            this.btnEDIT.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnEdit_LinkClicked);
            // 
            // btnREMOVE
            // 
            resources.ApplyResources(this.btnREMOVE, "btnREMOVE");
            this.btnREMOVE.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnREMOVE.Name = "btnREMOVE";
            this.btnREMOVE.TabStop = true;
            this.btnREMOVE.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnRemove_LinkClicked);
            // 
            // lblCONNECTION
            // 
            resources.ApplyResources(this.lblCONNECTION, "lblCONNECTION");
            this.lblCONNECTION.Name = "lblCONNECTION";
            // 
            // btnREPORT_CONNECTION
            // 
            resources.ApplyResources(this.btnREPORT_CONNECTION, "btnREPORT_CONNECTION");
            this.btnREPORT_CONNECTION.Name = "btnREPORT_CONNECTION";
            this.btnREPORT_CONNECTION.UseVisualStyleBackColor = true;
            this.btnREPORT_CONNECTION.Click += new System.EventHandler(this.btnConnection_Click);
            // 
            // btnREPORT_DEPOSIT_AND_CONNECTION_FEE
            // 
            resources.ApplyResources(this.btnREPORT_DEPOSIT_AND_CONNECTION_FEE, "btnREPORT_DEPOSIT_AND_CONNECTION_FEE");
            this.btnREPORT_DEPOSIT_AND_CONNECTION_FEE.Name = "btnREPORT_DEPOSIT_AND_CONNECTION_FEE";
            this.btnREPORT_DEPOSIT_AND_CONNECTION_FEE.UseVisualStyleBackColor = true;
            this.btnREPORT_DEPOSIT_AND_CONNECTION_FEE.Click += new System.EventHandler(this.btnDepositConnectionFeePayslip_Click);
            // 
            // btnREPORT_AGREEMENT
            // 
            resources.ApplyResources(this.btnREPORT_AGREEMENT, "btnREPORT_AGREEMENT");
            this.btnREPORT_AGREEMENT.Name = "btnREPORT_AGREEMENT";
            this.btnREPORT_AGREEMENT.UseVisualStyleBackColor = true;
            this.btnREPORT_AGREEMENT.Click += new System.EventHandler(this.btnAgreement_Click);
            // 
            // label35
            // 
            resources.ApplyResources(this.label35, "label35");
            this.label35.Name = "label35";
            // 
            // cboPrice
            // 
            this.cboPrice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPrice.DropDownWidth = 350;
            this.cboPrice.FormattingEnabled = true;
            resources.ApplyResources(this.cboPrice, "cboPrice");
            this.cboPrice.Name = "cboPrice";
            // 
            // txtTotalPower
            // 
            resources.ApplyResources(this.txtTotalPower, "txtTotalPower");
            this.txtTotalPower.Name = "txtTotalPower";
            // 
            // lblTOTAL_WATT
            // 
            resources.ApplyResources(this.lblTOTAL_WATT, "lblTOTAL_WATT");
            this.lblTOTAL_WATT.Name = "lblTOTAL_WATT";
            // 
            // cboBillingCycle
            // 
            this.cboBillingCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBillingCycle.DropDownWidth = 200;
            this.cboBillingCycle.FormattingEnabled = true;
            resources.ApplyResources(this.cboBillingCycle, "cboBillingCycle");
            this.cboBillingCycle.Name = "cboBillingCycle";
            this.cboBillingCycle.Enter += new System.EventHandler(this.InputKhmer);
            // 
            // dgvEquitment
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvEquitment.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvEquitment.BackgroundColor = System.Drawing.Color.White;
            this.dgvEquitment.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvEquitment.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvEquitment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEquitment.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CUS_EQUIPMENT_ID,
            this.EQUIPMENT_ID,
            this.EQUIPMENT_NAME,
            this.QUANTITY,
            this.WATT,
            this.TOTAL_WATT,
            this.NOTE,
            this._DB,
            this._EDITED});
            this.dgvEquitment.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgvEquitment, "dgvEquitment");
            this.dgvEquitment.Name = "dgvEquitment";
            this.dgvEquitment.RowHeadersVisible = false;
            // 
            // CUS_EQUIPMENT_ID
            // 
            resources.ApplyResources(this.CUS_EQUIPMENT_ID, "CUS_EQUIPMENT_ID");
            this.CUS_EQUIPMENT_ID.Name = "CUS_EQUIPMENT_ID";
            // 
            // EQUIPMENT_ID
            // 
            this.EQUIPMENT_ID.DataPropertyName = "EQUIPMENT_ID";
            resources.ApplyResources(this.EQUIPMENT_ID, "EQUIPMENT_ID");
            this.EQUIPMENT_ID.Name = "EQUIPMENT_ID";
            // 
            // EQUIPMENT_NAME
            // 
            this.EQUIPMENT_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.EQUIPMENT_NAME.DataPropertyName = "EQUIPMENT_NAME";
            resources.ApplyResources(this.EQUIPMENT_NAME, "EQUIPMENT_NAME");
            this.EQUIPMENT_NAME.Name = "EQUIPMENT_NAME";
            // 
            // QUANTITY
            // 
            this.QUANTITY.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.QUANTITY.DataPropertyName = "QUANTITY";
            resources.ApplyResources(this.QUANTITY, "QUANTITY");
            this.QUANTITY.Name = "QUANTITY";
            // 
            // WATT
            // 
            this.WATT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.WATT.DataPropertyName = "POWER";
            resources.ApplyResources(this.WATT, "WATT");
            this.WATT.Name = "WATT";
            // 
            // TOTAL_WATT
            // 
            this.TOTAL_WATT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.TOTAL_WATT.DataPropertyName = "TOTAL_POWER";
            resources.ApplyResources(this.TOTAL_WATT, "TOTAL_WATT");
            this.TOTAL_WATT.Name = "TOTAL_WATT";
            // 
            // NOTE
            // 
            this.NOTE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NOTE.DataPropertyName = "COMMENT";
            resources.ApplyResources(this.NOTE, "NOTE");
            this.NOTE.Name = "NOTE";
            // 
            // _DB
            // 
            this._DB.DataPropertyName = "_DB";
            resources.ApplyResources(this._DB, "_DB");
            this._DB.Name = "_DB";
            this._DB.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._DB.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // _EDITED
            // 
            resources.ApplyResources(this._EDITED, "_EDITED");
            this._EDITED.Name = "_EDITED";
            // 
            // lblAMPERE
            // 
            resources.ApplyResources(this.lblAMPERE, "lblAMPERE");
            this.lblAMPERE.Name = "lblAMPERE";
            // 
            // lblPRICE_AND_BILLING_CYCLE
            // 
            resources.ApplyResources(this.lblPRICE_AND_BILLING_CYCLE, "lblPRICE_AND_BILLING_CYCLE");
            this.lblPRICE_AND_BILLING_CYCLE.Name = "lblPRICE_AND_BILLING_CYCLE";
            // 
            // lblPHASE
            // 
            resources.ApplyResources(this.lblPHASE, "lblPHASE");
            this.lblPHASE.Name = "lblPHASE";
            // 
            // lblVOLTAGE
            // 
            resources.ApplyResources(this.lblVOLTAGE, "lblVOLTAGE");
            this.lblVOLTAGE.Name = "lblVOLTAGE";
            // 
            // lblEQUIPMENT
            // 
            resources.ApplyResources(this.lblEQUIPMENT, "lblEQUIPMENT");
            this.lblEQUIPMENT.Name = "lblEQUIPMENT";
            // 
            // cboPhase
            // 
            this.cboPhase.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPhase.FormattingEnabled = true;
            resources.ApplyResources(this.cboPhase, "cboPhase");
            this.cboPhase.Name = "cboPhase";
            this.cboPhase.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // cboAMP
            // 
            this.cboAMP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAMP.FormattingEnabled = true;
            resources.ApplyResources(this.cboAMP, "cboAMP");
            this.cboAMP.Name = "cboAMP";
            this.cboAMP.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // cboVol
            // 
            this.cboVol.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboVol.FormattingEnabled = true;
            resources.ApplyResources(this.cboVol, "cboVol");
            this.cboVol.Name = "cboVol";
            this.cboVol.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // label32
            // 
            resources.ApplyResources(this.label32, "label32");
            this.label32.ForeColor = System.Drawing.Color.Red;
            this.label32.Name = "label32";
            // 
            // label30
            // 
            resources.ApplyResources(this.label30, "label30");
            this.label30.ForeColor = System.Drawing.Color.Red;
            this.label30.Name = "label30";
            // 
            // label33
            // 
            resources.ApplyResources(this.label33, "label33");
            this.label33.ForeColor = System.Drawing.Color.Red;
            this.label33.Name = "label33";
            // 
            // label31
            // 
            resources.ApplyResources(this.label31, "label31");
            this.label31.ForeColor = System.Drawing.Color.Red;
            this.label31.Name = "label31";
            // 
            // tabDeposit
            // 
            this.tabDeposit.Controls.Add(this.panel2);
            this.tabDeposit.Controls.Add(this.dgvBalanceDeposit);
            this.tabDeposit.Controls.Add(this.dgvDeposit);
            this.tabDeposit.Controls.Add(this.lblTOTAL_DEPOSIT_AMOUNT);
            resources.ApplyResources(this.tabDeposit, "tabDeposit");
            this.tabDeposit.Name = "tabDeposit";
            this.tabDeposit.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnADJUST_DEPOSIT);
            this.panel2.Controls.Add(this.btnAPPLY_PAYMENT);
            this.panel2.Controls.Add(this.btnADD_DEPOSIT);
            this.panel2.Controls.Add(this.btnREFUND_DEPOSIT);
            this.panel2.Controls.Add(this.btnPRINT);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // btnADJUST_DEPOSIT
            // 
            resources.ApplyResources(this.btnADJUST_DEPOSIT, "btnADJUST_DEPOSIT");
            this.btnADJUST_DEPOSIT.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnADJUST_DEPOSIT.Name = "btnADJUST_DEPOSIT";
            this.btnADJUST_DEPOSIT.TabStop = true;
            this.btnADJUST_DEPOSIT.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkAdjustDeposit_LinkClicked);
            // 
            // btnAPPLY_PAYMENT
            // 
            resources.ApplyResources(this.btnAPPLY_PAYMENT, "btnAPPLY_PAYMENT");
            this.btnAPPLY_PAYMENT.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnAPPLY_PAYMENT.Name = "btnAPPLY_PAYMENT";
            this.btnAPPLY_PAYMENT.TabStop = true;
            this.btnAPPLY_PAYMENT.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkApplyPayment_LinkClicked);
            // 
            // btnADD_DEPOSIT
            // 
            resources.ApplyResources(this.btnADD_DEPOSIT, "btnADD_DEPOSIT");
            this.btnADD_DEPOSIT.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnADD_DEPOSIT.Name = "btnADD_DEPOSIT";
            this.btnADD_DEPOSIT.TabStop = true;
            this.btnADD_DEPOSIT.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblAddDeposit_LinkClicked);
            // 
            // btnREFUND_DEPOSIT
            // 
            resources.ApplyResources(this.btnREFUND_DEPOSIT, "btnREFUND_DEPOSIT");
            this.btnREFUND_DEPOSIT.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnREFUND_DEPOSIT.Name = "btnREFUND_DEPOSIT";
            this.btnREFUND_DEPOSIT.TabStop = true;
            this.btnREFUND_DEPOSIT.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkRefundDeposit_LinkClicked);
            // 
            // btnPRINT
            // 
            resources.ApplyResources(this.btnPRINT, "btnPRINT");
            this.btnPRINT.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnPRINT.Name = "btnPRINT";
            this.btnPRINT.TabStop = true;
            this.btnPRINT.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnPrintReceipt_LinkClicked);
            // 
            // dgvBalanceDeposit
            // 
            this.dgvBalanceDeposit.BackgroundColor = System.Drawing.Color.White;
            this.dgvBalanceDeposit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvBalanceDeposit.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvBalanceDeposit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBalanceDeposit.ColumnHeadersVisible = false;
            this.dgvBalanceDeposit.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvBalanceDeposit.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvBalanceDeposit.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgvBalanceDeposit, "dgvBalanceDeposit");
            this.dgvBalanceDeposit.Name = "dgvBalanceDeposit";
            this.dgvBalanceDeposit.RowHeadersVisible = false;
            this.dgvBalanceDeposit.RowTemplate.Height = 25;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "CURRENCY_ID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn1, "dataGridViewTextBoxColumn1");
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "BALANCE";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "#,##0.####";
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.dataGridViewTextBoxColumn2, "dataGridViewTextBoxColumn2");
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "CURRENCY_SING";
            this.dataGridViewTextBoxColumn3.FillWeight = 65F;
            resources.ApplyResources(this.dataGridViewTextBoxColumn3, "dataGridViewTextBoxColumn3");
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dgvDeposit
            // 
            this.dgvDeposit.AllowUserToAddRows = false;
            this.dgvDeposit.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvDeposit.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvDeposit.BackgroundColor = System.Drawing.Color.White;
            this.dgvDeposit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvDeposit.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvDeposit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDeposit.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CUS_DEPOSIT_ID,
            this.DEPOSIT_NO,
            this.CREATE_ON,
            this.CREATE_BY,
            this.DEPOSIT_ACTION_NAME,
            this.AMOUNT,
            this.BALANCE,
            this.D_CURRENCY_SING_,
            this.IS_PAID});
            this.dgvDeposit.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgvDeposit, "dgvDeposit");
            this.dgvDeposit.Name = "dgvDeposit";
            this.dgvDeposit.ReadOnly = true;
            this.dgvDeposit.RowHeadersVisible = false;
            // 
            // CUS_DEPOSIT_ID
            // 
            this.CUS_DEPOSIT_ID.DataPropertyName = "CUS_DEPOSIT_ID";
            resources.ApplyResources(this.CUS_DEPOSIT_ID, "CUS_DEPOSIT_ID");
            this.CUS_DEPOSIT_ID.Name = "CUS_DEPOSIT_ID";
            this.CUS_DEPOSIT_ID.ReadOnly = true;
            // 
            // DEPOSIT_NO
            // 
            this.DEPOSIT_NO.DataPropertyName = "DEPOSIT_NO";
            resources.ApplyResources(this.DEPOSIT_NO, "DEPOSIT_NO");
            this.DEPOSIT_NO.Name = "DEPOSIT_NO";
            this.DEPOSIT_NO.ReadOnly = true;
            // 
            // CREATE_ON
            // 
            this.CREATE_ON.DataPropertyName = "DEPOSIT_DATE";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.Format = "dd-MM-yyyy";
            this.CREATE_ON.DefaultCellStyle = dataGridViewCellStyle5;
            resources.ApplyResources(this.CREATE_ON, "CREATE_ON");
            this.CREATE_ON.Name = "CREATE_ON";
            this.CREATE_ON.ReadOnly = true;
            // 
            // CREATE_BY
            // 
            this.CREATE_BY.DataPropertyName = "CREATE_BY";
            resources.ApplyResources(this.CREATE_BY, "CREATE_BY");
            this.CREATE_BY.Name = "CREATE_BY";
            this.CREATE_BY.ReadOnly = true;
            // 
            // DEPOSIT_ACTION_NAME
            // 
            this.DEPOSIT_ACTION_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DEPOSIT_ACTION_NAME.DataPropertyName = "DEPOSIT_ACTION_NAME_KH";
            resources.ApplyResources(this.DEPOSIT_ACTION_NAME, "DEPOSIT_ACTION_NAME");
            this.DEPOSIT_ACTION_NAME.Name = "DEPOSIT_ACTION_NAME";
            this.DEPOSIT_ACTION_NAME.ReadOnly = true;
            // 
            // AMOUNT
            // 
            this.AMOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AMOUNT.DataPropertyName = "AMOUNT";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "#,##0.####";
            this.AMOUNT.DefaultCellStyle = dataGridViewCellStyle6;
            resources.ApplyResources(this.AMOUNT, "AMOUNT");
            this.AMOUNT.Name = "AMOUNT";
            this.AMOUNT.ReadOnly = true;
            // 
            // BALANCE
            // 
            this.BALANCE.DataPropertyName = "BALANCE";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "#,##0.####";
            this.BALANCE.DefaultCellStyle = dataGridViewCellStyle7;
            resources.ApplyResources(this.BALANCE, "BALANCE");
            this.BALANCE.Name = "BALANCE";
            this.BALANCE.ReadOnly = true;
            // 
            // D_CURRENCY_SING_
            // 
            this.D_CURRENCY_SING_.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.D_CURRENCY_SING_.DataPropertyName = "CURRENCY_SING";
            this.D_CURRENCY_SING_.FillWeight = 65F;
            resources.ApplyResources(this.D_CURRENCY_SING_, "D_CURRENCY_SING_");
            this.D_CURRENCY_SING_.Name = "D_CURRENCY_SING_";
            this.D_CURRENCY_SING_.ReadOnly = true;
            // 
            // IS_PAID
            // 
            this.IS_PAID.DataPropertyName = "IS_PAID";
            resources.ApplyResources(this.IS_PAID, "IS_PAID");
            this.IS_PAID.Name = "IS_PAID";
            this.IS_PAID.ReadOnly = true;
            // 
            // lblTOTAL_DEPOSIT_AMOUNT
            // 
            resources.ApplyResources(this.lblTOTAL_DEPOSIT_AMOUNT, "lblTOTAL_DEPOSIT_AMOUNT");
            this.lblTOTAL_DEPOSIT_AMOUNT.Name = "lblTOTAL_DEPOSIT_AMOUNT";
            // 
            // tabBILLING_AND_METER
            // 
            this.tabBILLING_AND_METER.Controls.Add(this.btnREACTIVATE);
            this.tabBILLING_AND_METER.Controls.Add(this.txtPositionInBox);
            this.tabBILLING_AND_METER.Controls.Add(this.lblPOSITION_IN_BOX);
            this.tabBILLING_AND_METER.Controls.Add(this.btnCHANGE_BOX);
            this.tabBILLING_AND_METER.Controls.Add(this.btnCHANGE_BREAKER);
            this.tabBILLING_AND_METER.Controls.Add(this.btnCHANGE_METER);
            this.tabBILLING_AND_METER.Controls.Add(this.txtPoleCode);
            this.tabBILLING_AND_METER.Controls.Add(this.lblPOLE_AND_BOX_INSTALLATION);
            this.tabBILLING_AND_METER.Controls.Add(this.txtCutter);
            this.tabBILLING_AND_METER.Controls.Add(this.txtBiller);
            this.tabBILLING_AND_METER.Controls.Add(this.txtCollector);
            this.tabBILLING_AND_METER.Controls.Add(this.label71);
            this.tabBILLING_AND_METER.Controls.Add(this.lblCUTTER);
            this.tabBILLING_AND_METER.Controls.Add(this.lblBILLER);
            this.tabBILLING_AND_METER.Controls.Add(this.lblCOLLECTOR);
            this.tabBILLING_AND_METER.Controls.Add(this.txtBox);
            this.tabBILLING_AND_METER.Controls.Add(this.lblBOX);
            this.tabBILLING_AND_METER.Controls.Add(this.lblPOLE);
            this.tabBILLING_AND_METER.Controls.Add(this.lblCONSTANT_1);
            this.tabBILLING_AND_METER.Controls.Add(this.txtBreakerConstant);
            this.tabBILLING_AND_METER.Controls.Add(this.txtBreakerType);
            this.tabBILLING_AND_METER.Controls.Add(this.label53);
            this.tabBILLING_AND_METER.Controls.Add(this.lblBREAKER_INSTALLATION);
            this.tabBILLING_AND_METER.Controls.Add(this.lblAMPARE_2);
            this.tabBILLING_AND_METER.Controls.Add(this.lblVOLTAGE_2);
            this.tabBILLING_AND_METER.Controls.Add(this.lblPHASE_2);
            this.tabBILLING_AND_METER.Controls.Add(this.txtBreakerAmp);
            this.tabBILLING_AND_METER.Controls.Add(this.txtBreakerVol);
            this.tabBILLING_AND_METER.Controls.Add(this.txtBreakerPhase);
            this.tabBILLING_AND_METER.Controls.Add(this.lblBREAKER_CODE);
            this.tabBILLING_AND_METER.Controls.Add(this.txtBreaker);
            this.tabBILLING_AND_METER.Controls.Add(this.lblBREAKER_TYPE);
            this.tabBILLING_AND_METER.Controls.Add(this.lblCONSTANT);
            this.tabBILLING_AND_METER.Controls.Add(this.txtMeterConstant);
            this.tabBILLING_AND_METER.Controls.Add(this.txtMeterType);
            this.tabBILLING_AND_METER.Controls.Add(this.label50);
            this.tabBILLING_AND_METER.Controls.Add(this.lblAMPARE);
            this.tabBILLING_AND_METER.Controls.Add(this.lblVOLTAGE_1);
            this.tabBILLING_AND_METER.Controls.Add(this.lblPHASE_1);
            this.tabBILLING_AND_METER.Controls.Add(this.lblCABLE_SHIELD);
            this.tabBILLING_AND_METER.Controls.Add(this.lblSHIELD);
            this.tabBILLING_AND_METER.Controls.Add(this.txtMeterAmp);
            this.tabBILLING_AND_METER.Controls.Add(this.txtMeterVol);
            this.tabBILLING_AND_METER.Controls.Add(this.txtMeterPhase);
            this.tabBILLING_AND_METER.Controls.Add(this.lblMETER_CODE);
            this.tabBILLING_AND_METER.Controls.Add(this.lblMETER_INSTALLATION);
            this.tabBILLING_AND_METER.Controls.Add(this.lblMETER_TYPE);
            this.tabBILLING_AND_METER.Controls.Add(this.cboCableShield);
            this.tabBILLING_AND_METER.Controls.Add(this.cboMeterShield);
            this.tabBILLING_AND_METER.Controls.Add(this.txtMeter);
            this.tabBILLING_AND_METER.Controls.Add(this.dtpActivateDate);
            this.tabBILLING_AND_METER.Controls.Add(this.label45);
            this.tabBILLING_AND_METER.Controls.Add(this.lblACTIVATE_DATE);
            this.tabBILLING_AND_METER.Controls.Add(this.nudCutoffDays);
            this.tabBILLING_AND_METER.Controls.Add(this.lblCUT_DAY);
            this.tabBILLING_AND_METER.Controls.Add(this.lblUNLIMITE_CUT_DAY);
            resources.ApplyResources(this.tabBILLING_AND_METER, "tabBILLING_AND_METER");
            this.tabBILLING_AND_METER.Name = "tabBILLING_AND_METER";
            this.tabBILLING_AND_METER.UseVisualStyleBackColor = true;
            // 
            // btnREACTIVATE
            // 
            resources.ApplyResources(this.btnREACTIVATE, "btnREACTIVATE");
            this.btnREACTIVATE.Name = "btnREACTIVATE";
            this.btnREACTIVATE.UseVisualStyleBackColor = true;
            this.btnREACTIVATE.Click += new System.EventHandler(this.btnReactivate_Click);
            // 
            // txtPositionInBox
            // 
            resources.ApplyResources(this.txtPositionInBox, "txtPositionInBox");
            this.txtPositionInBox.Name = "txtPositionInBox";
            this.txtPositionInBox.TabStop = false;
            this.txtPositionInBox.Enter += new System.EventHandler(this.InputEnglish);
            this.txtPositionInBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NumberOnly);
            // 
            // lblPOSITION_IN_BOX
            // 
            resources.ApplyResources(this.lblPOSITION_IN_BOX, "lblPOSITION_IN_BOX");
            this.lblPOSITION_IN_BOX.Name = "lblPOSITION_IN_BOX";
            // 
            // btnCHANGE_BOX
            // 
            this.btnCHANGE_BOX.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            resources.ApplyResources(this.btnCHANGE_BOX, "btnCHANGE_BOX");
            this.btnCHANGE_BOX.Name = "btnCHANGE_BOX";
            this.btnCHANGE_BOX.TabStop = true;
            this.btnCHANGE_BOX.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnChangeBox_LinkClicked);
            // 
            // btnCHANGE_BREAKER
            // 
            this.btnCHANGE_BREAKER.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            resources.ApplyResources(this.btnCHANGE_BREAKER, "btnCHANGE_BREAKER");
            this.btnCHANGE_BREAKER.Name = "btnCHANGE_BREAKER";
            this.btnCHANGE_BREAKER.TabStop = true;
            this.btnCHANGE_BREAKER.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnChangeBreaker_LinkClicked);
            // 
            // btnCHANGE_METER
            // 
            this.btnCHANGE_METER.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            resources.ApplyResources(this.btnCHANGE_METER, "btnCHANGE_METER");
            this.btnCHANGE_METER.Name = "btnCHANGE_METER";
            this.btnCHANGE_METER.TabStop = true;
            this.btnCHANGE_METER.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnChangeMeter_LinkClicked);
            // 
            // txtPoleCode
            // 
            resources.ApplyResources(this.txtPoleCode, "txtPoleCode");
            this.txtPoleCode.Name = "txtPoleCode";
            this.txtPoleCode.ReadOnly = true;
            // 
            // lblPOLE_AND_BOX_INSTALLATION
            // 
            resources.ApplyResources(this.lblPOLE_AND_BOX_INSTALLATION, "lblPOLE_AND_BOX_INSTALLATION");
            this.lblPOLE_AND_BOX_INSTALLATION.Name = "lblPOLE_AND_BOX_INSTALLATION";
            // 
            // txtCutter
            // 
            resources.ApplyResources(this.txtCutter, "txtCutter");
            this.txtCutter.Name = "txtCutter";
            this.txtCutter.ReadOnly = true;
            // 
            // txtBiller
            // 
            resources.ApplyResources(this.txtBiller, "txtBiller");
            this.txtBiller.Name = "txtBiller";
            this.txtBiller.ReadOnly = true;
            // 
            // txtCollector
            // 
            resources.ApplyResources(this.txtCollector, "txtCollector");
            this.txtCollector.Name = "txtCollector";
            this.txtCollector.ReadOnly = true;
            // 
            // label71
            // 
            resources.ApplyResources(this.label71, "label71");
            this.label71.ForeColor = System.Drawing.Color.Red;
            this.label71.Name = "label71";
            // 
            // lblCUTTER
            // 
            resources.ApplyResources(this.lblCUTTER, "lblCUTTER");
            this.lblCUTTER.Name = "lblCUTTER";
            // 
            // lblBILLER
            // 
            resources.ApplyResources(this.lblBILLER, "lblBILLER");
            this.lblBILLER.Name = "lblBILLER";
            // 
            // lblCOLLECTOR
            // 
            resources.ApplyResources(this.lblCOLLECTOR, "lblCOLLECTOR");
            this.lblCOLLECTOR.Name = "lblCOLLECTOR";
            // 
            // txtBox
            // 
            this.txtBox.BackColor = System.Drawing.Color.White;
            this.txtBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtBox, "txtBox");
            this.txtBox.Name = "txtBox";
            this.txtBox.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtBox.AdvanceSearch += new System.EventHandler(this.txtBox_AdvanceSearch);
            this.txtBox.CancelAdvanceSearch += new System.EventHandler(this.txtBox_CancelAdvanceSearch);
            this.txtBox.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // lblBOX
            // 
            resources.ApplyResources(this.lblBOX, "lblBOX");
            this.lblBOX.Name = "lblBOX";
            // 
            // lblPOLE
            // 
            resources.ApplyResources(this.lblPOLE, "lblPOLE");
            this.lblPOLE.Name = "lblPOLE";
            // 
            // lblCONSTANT_1
            // 
            resources.ApplyResources(this.lblCONSTANT_1, "lblCONSTANT_1");
            this.lblCONSTANT_1.Name = "lblCONSTANT_1";
            // 
            // txtBreakerConstant
            // 
            resources.ApplyResources(this.txtBreakerConstant, "txtBreakerConstant");
            this.txtBreakerConstant.Name = "txtBreakerConstant";
            this.txtBreakerConstant.ReadOnly = true;
            // 
            // txtBreakerType
            // 
            resources.ApplyResources(this.txtBreakerType, "txtBreakerType");
            this.txtBreakerType.Name = "txtBreakerType";
            this.txtBreakerType.ReadOnly = true;
            // 
            // label53
            // 
            resources.ApplyResources(this.label53, "label53");
            this.label53.ForeColor = System.Drawing.Color.Red;
            this.label53.Name = "label53";
            // 
            // lblBREAKER_INSTALLATION
            // 
            resources.ApplyResources(this.lblBREAKER_INSTALLATION, "lblBREAKER_INSTALLATION");
            this.lblBREAKER_INSTALLATION.Name = "lblBREAKER_INSTALLATION";
            // 
            // lblAMPARE_2
            // 
            resources.ApplyResources(this.lblAMPARE_2, "lblAMPARE_2");
            this.lblAMPARE_2.Name = "lblAMPARE_2";
            // 
            // lblVOLTAGE_2
            // 
            resources.ApplyResources(this.lblVOLTAGE_2, "lblVOLTAGE_2");
            this.lblVOLTAGE_2.Name = "lblVOLTAGE_2";
            // 
            // lblPHASE_2
            // 
            resources.ApplyResources(this.lblPHASE_2, "lblPHASE_2");
            this.lblPHASE_2.Name = "lblPHASE_2";
            // 
            // txtBreakerAmp
            // 
            resources.ApplyResources(this.txtBreakerAmp, "txtBreakerAmp");
            this.txtBreakerAmp.Name = "txtBreakerAmp";
            this.txtBreakerAmp.ReadOnly = true;
            // 
            // txtBreakerVol
            // 
            resources.ApplyResources(this.txtBreakerVol, "txtBreakerVol");
            this.txtBreakerVol.Name = "txtBreakerVol";
            this.txtBreakerVol.ReadOnly = true;
            // 
            // txtBreakerPhase
            // 
            resources.ApplyResources(this.txtBreakerPhase, "txtBreakerPhase");
            this.txtBreakerPhase.Name = "txtBreakerPhase";
            this.txtBreakerPhase.ReadOnly = true;
            // 
            // lblBREAKER_CODE
            // 
            resources.ApplyResources(this.lblBREAKER_CODE, "lblBREAKER_CODE");
            this.lblBREAKER_CODE.Name = "lblBREAKER_CODE";
            // 
            // txtBreaker
            // 
            this.txtBreaker.BackColor = System.Drawing.Color.White;
            this.txtBreaker.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtBreaker, "txtBreaker");
            this.txtBreaker.Name = "txtBreaker";
            this.txtBreaker.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtBreaker.AdvanceSearch += new System.EventHandler(this.txtBreaker_AdvanceSearch);
            this.txtBreaker.CancelAdvanceSearch += new System.EventHandler(this.txtBreaker_CancelAdvanceSearch);
            this.txtBreaker.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // lblBREAKER_TYPE
            // 
            resources.ApplyResources(this.lblBREAKER_TYPE, "lblBREAKER_TYPE");
            this.lblBREAKER_TYPE.Name = "lblBREAKER_TYPE";
            // 
            // lblCONSTANT
            // 
            resources.ApplyResources(this.lblCONSTANT, "lblCONSTANT");
            this.lblCONSTANT.Name = "lblCONSTANT";
            // 
            // txtMeterConstant
            // 
            resources.ApplyResources(this.txtMeterConstant, "txtMeterConstant");
            this.txtMeterConstant.Name = "txtMeterConstant";
            this.txtMeterConstant.ReadOnly = true;
            // 
            // txtMeterType
            // 
            resources.ApplyResources(this.txtMeterType, "txtMeterType");
            this.txtMeterType.Name = "txtMeterType";
            this.txtMeterType.ReadOnly = true;
            // 
            // label50
            // 
            resources.ApplyResources(this.label50, "label50");
            this.label50.ForeColor = System.Drawing.Color.Red;
            this.label50.Name = "label50";
            // 
            // lblAMPARE
            // 
            resources.ApplyResources(this.lblAMPARE, "lblAMPARE");
            this.lblAMPARE.Name = "lblAMPARE";
            // 
            // lblVOLTAGE_1
            // 
            resources.ApplyResources(this.lblVOLTAGE_1, "lblVOLTAGE_1");
            this.lblVOLTAGE_1.Name = "lblVOLTAGE_1";
            // 
            // lblPHASE_1
            // 
            resources.ApplyResources(this.lblPHASE_1, "lblPHASE_1");
            this.lblPHASE_1.Name = "lblPHASE_1";
            // 
            // lblCABLE_SHIELD
            // 
            resources.ApplyResources(this.lblCABLE_SHIELD, "lblCABLE_SHIELD");
            this.lblCABLE_SHIELD.Name = "lblCABLE_SHIELD";
            // 
            // lblSHIELD
            // 
            resources.ApplyResources(this.lblSHIELD, "lblSHIELD");
            this.lblSHIELD.Name = "lblSHIELD";
            // 
            // txtMeterAmp
            // 
            resources.ApplyResources(this.txtMeterAmp, "txtMeterAmp");
            this.txtMeterAmp.Name = "txtMeterAmp";
            this.txtMeterAmp.ReadOnly = true;
            // 
            // txtMeterVol
            // 
            resources.ApplyResources(this.txtMeterVol, "txtMeterVol");
            this.txtMeterVol.Name = "txtMeterVol";
            this.txtMeterVol.ReadOnly = true;
            // 
            // txtMeterPhase
            // 
            resources.ApplyResources(this.txtMeterPhase, "txtMeterPhase");
            this.txtMeterPhase.Name = "txtMeterPhase";
            this.txtMeterPhase.ReadOnly = true;
            // 
            // lblMETER_CODE
            // 
            resources.ApplyResources(this.lblMETER_CODE, "lblMETER_CODE");
            this.lblMETER_CODE.Name = "lblMETER_CODE";
            // 
            // lblMETER_INSTALLATION
            // 
            resources.ApplyResources(this.lblMETER_INSTALLATION, "lblMETER_INSTALLATION");
            this.lblMETER_INSTALLATION.Name = "lblMETER_INSTALLATION";
            // 
            // lblMETER_TYPE
            // 
            resources.ApplyResources(this.lblMETER_TYPE, "lblMETER_TYPE");
            this.lblMETER_TYPE.Name = "lblMETER_TYPE";
            // 
            // cboCableShield
            // 
            this.cboCableShield.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboCableShield, "cboCableShield");
            this.cboCableShield.FormattingEnabled = true;
            this.cboCableShield.Name = "cboCableShield";
            this.cboCableShield.Enter += new System.EventHandler(this.InputKhmer);
            // 
            // cboMeterShield
            // 
            this.cboMeterShield.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboMeterShield, "cboMeterShield");
            this.cboMeterShield.FormattingEnabled = true;
            this.cboMeterShield.Name = "cboMeterShield";
            this.cboMeterShield.Enter += new System.EventHandler(this.InputKhmer);
            // 
            // txtMeter
            // 
            this.txtMeter.BackColor = System.Drawing.Color.White;
            this.txtMeter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtMeter, "txtMeter");
            this.txtMeter.Name = "txtMeter";
            this.txtMeter.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtMeter.AdvanceSearch += new System.EventHandler(this.txtMeter_AdvanceSearch);
            this.txtMeter.CancelAdvanceSearch += new System.EventHandler(this.txtMeter_CancelAdvanceSearch);
            this.txtMeter.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // dtpActivateDate
            // 
            resources.ApplyResources(this.dtpActivateDate, "dtpActivateDate");
            this.dtpActivateDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpActivateDate.Name = "dtpActivateDate";
            this.dtpActivateDate.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // label45
            // 
            resources.ApplyResources(this.label45, "label45");
            this.label45.ForeColor = System.Drawing.Color.Red;
            this.label45.Name = "label45";
            // 
            // lblACTIVATE_DATE
            // 
            resources.ApplyResources(this.lblACTIVATE_DATE, "lblACTIVATE_DATE");
            this.lblACTIVATE_DATE.Name = "lblACTIVATE_DATE";
            // 
            // nudCutoffDays
            // 
            resources.ApplyResources(this.nudCutoffDays, "nudCutoffDays");
            this.nudCutoffDays.Name = "nudCutoffDays";
            this.nudCutoffDays.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblCUT_DAY
            // 
            resources.ApplyResources(this.lblCUT_DAY, "lblCUT_DAY");
            this.lblCUT_DAY.Name = "lblCUT_DAY";
            // 
            // lblUNLIMITE_CUT_DAY
            // 
            resources.ApplyResources(this.lblUNLIMITE_CUT_DAY, "lblUNLIMITE_CUT_DAY");
            this.lblUNLIMITE_CUT_DAY.Name = "lblUNLIMITE_CUT_DAY";
            // 
            // tabRECURRING_SERVICE
            // 
            this.tabRECURRING_SERVICE.Controls.Add(this.panel3);
            this.tabRECURRING_SERVICE.Controls.Add(this.dgvService);
            resources.ApplyResources(this.tabRECURRING_SERVICE, "tabRECURRING_SERVICE");
            this.tabRECURRING_SERVICE.Name = "tabRECURRING_SERVICE";
            this.tabRECURRING_SERVICE.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnADD_1);
            this.panel3.Controls.Add(this.btnEDIT_1);
            this.panel3.Controls.Add(this.btnREMOVE_1);
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            // 
            // btnADD_1
            // 
            resources.ApplyResources(this.btnADD_1, "btnADD_1");
            this.btnADD_1.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnADD_1.Name = "btnADD_1";
            this.btnADD_1.TabStop = true;
            // 
            // btnEDIT_1
            // 
            resources.ApplyResources(this.btnEDIT_1, "btnEDIT_1");
            this.btnEDIT_1.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnEDIT_1.Name = "btnEDIT_1";
            this.btnEDIT_1.TabStop = true;
            // 
            // btnREMOVE_1
            // 
            resources.ApplyResources(this.btnREMOVE_1, "btnREMOVE_1");
            this.btnREMOVE_1.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnREMOVE_1.Name = "btnREMOVE_1";
            this.btnREMOVE_1.TabStop = true;
            // 
            // dgvService
            // 
            this.dgvService.AllowUserToResizeRows = false;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvService.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dgvService.BackgroundColor = System.Drawing.Color.White;
            this.dgvService.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvService.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvService.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvService.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CUSTOMER_SERVICE_ID,
            this.SERVICE,
            this.QTY,
            this.PRICE,
            this.LAST_BILLING_MONTH,
            this.RECURRING_MONTH,
            this.NEXT_BILLING_MONTH});
            this.dgvService.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgvService, "dgvService");
            this.dgvService.Name = "dgvService";
            this.dgvService.RowHeadersVisible = false;
            this.dgvService.RowTemplate.Height = 25;
            this.dgvService.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // CUSTOMER_SERVICE_ID
            // 
            this.CUSTOMER_SERVICE_ID.DataPropertyName = "CUSTOMER_SERVICE_ID";
            resources.ApplyResources(this.CUSTOMER_SERVICE_ID, "CUSTOMER_SERVICE_ID");
            this.CUSTOMER_SERVICE_ID.Name = "CUSTOMER_SERVICE_ID";
            // 
            // SERVICE
            // 
            this.SERVICE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.SERVICE.DataPropertyName = "INVOICE_ITEM_NAME";
            resources.ApplyResources(this.SERVICE, "SERVICE");
            this.SERVICE.Name = "SERVICE";
            // 
            // QTY
            // 
            this.QTY.DataPropertyName = "QTY";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.Format = "N2";
            this.QTY.DefaultCellStyle = dataGridViewCellStyle9;
            resources.ApplyResources(this.QTY, "QTY");
            this.QTY.Name = "QTY";
            // 
            // PRICE
            // 
            this.PRICE.DataPropertyName = "PRICE";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle10.Format = "#,##0.####";
            dataGridViewCellStyle10.NullValue = null;
            this.PRICE.DefaultCellStyle = dataGridViewCellStyle10;
            resources.ApplyResources(this.PRICE, "PRICE");
            this.PRICE.Name = "PRICE";
            // 
            // LAST_BILLING_MONTH
            // 
            this.LAST_BILLING_MONTH.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.LAST_BILLING_MONTH.DataPropertyName = "LAST_BILLING_MONTH";
            dataGridViewCellStyle11.Format = "MM-yyyy";
            this.LAST_BILLING_MONTH.DefaultCellStyle = dataGridViewCellStyle11;
            resources.ApplyResources(this.LAST_BILLING_MONTH, "LAST_BILLING_MONTH");
            this.LAST_BILLING_MONTH.Name = "LAST_BILLING_MONTH";
            // 
            // RECURRING_MONTH
            // 
            this.RECURRING_MONTH.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.RECURRING_MONTH.DataPropertyName = "RECURRING_MONTH";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.Format = "#";
            this.RECURRING_MONTH.DefaultCellStyle = dataGridViewCellStyle12;
            resources.ApplyResources(this.RECURRING_MONTH, "RECURRING_MONTH");
            this.RECURRING_MONTH.Name = "RECURRING_MONTH";
            // 
            // NEXT_BILLING_MONTH
            // 
            this.NEXT_BILLING_MONTH.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.NEXT_BILLING_MONTH.DataPropertyName = "NEXT_BILLING_MONTH";
            dataGridViewCellStyle13.Format = "MM-yyyy";
            this.NEXT_BILLING_MONTH.DefaultCellStyle = dataGridViewCellStyle13;
            resources.ApplyResources(this.NEXT_BILLING_MONTH, "NEXT_BILLING_MONTH");
            this.NEXT_BILLING_MONTH.Name = "NEXT_BILLING_MONTH";
            // 
            // tabPREPAYMENT
            // 
            this.tabPREPAYMENT.Controls.Add(this.cboCurrency);
            this.tabPREPAYMENT.Controls.Add(this.panel6);
            this.tabPREPAYMENT.Controls.Add(this.dgvBalancePrepayment);
            this.tabPREPAYMENT.Controls.Add(this.label1);
            this.tabPREPAYMENT.Controls.Add(this.dgvPrepayment);
            resources.ApplyResources(this.tabPREPAYMENT, "tabPREPAYMENT");
            this.tabPREPAYMENT.Name = "tabPREPAYMENT";
            this.tabPREPAYMENT.UseVisualStyleBackColor = true;
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.SelectedIndexChanged += new System.EventHandler(this.cboCurrency_SelectedIndexChanged);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.btnSETTLE_INVOICE);
            this.panel6.Controls.Add(this.btnAPPLY_PREPAYMENT);
            this.panel6.Controls.Add(this.btnRETURN_PREPAYMENT);
            this.panel6.Controls.Add(this.btnPRINT_2);
            resources.ApplyResources(this.panel6, "panel6");
            this.panel6.Name = "panel6";
            // 
            // btnSETTLE_INVOICE
            // 
            resources.ApplyResources(this.btnSETTLE_INVOICE, "btnSETTLE_INVOICE");
            this.btnSETTLE_INVOICE.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnSETTLE_INVOICE.Name = "btnSETTLE_INVOICE";
            this.btnSETTLE_INVOICE.TabStop = true;
            this.btnSETTLE_INVOICE.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnSETTLE_INVOICE_LinkClicked);
            // 
            // btnAPPLY_PREPAYMENT
            // 
            resources.ApplyResources(this.btnAPPLY_PREPAYMENT, "btnAPPLY_PREPAYMENT");
            this.btnAPPLY_PREPAYMENT.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnAPPLY_PREPAYMENT.Name = "btnAPPLY_PREPAYMENT";
            this.btnAPPLY_PREPAYMENT.TabStop = true;
            this.btnAPPLY_PREPAYMENT.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnADD_PREPAYMENT_LinkClicked);
            // 
            // btnRETURN_PREPAYMENT
            // 
            resources.ApplyResources(this.btnRETURN_PREPAYMENT, "btnRETURN_PREPAYMENT");
            this.btnRETURN_PREPAYMENT.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnRETURN_PREPAYMENT.Name = "btnRETURN_PREPAYMENT";
            this.btnRETURN_PREPAYMENT.TabStop = true;
            this.btnRETURN_PREPAYMENT.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnRETURN_PREPAYMENT_LinkClicked);
            // 
            // btnPRINT_2
            // 
            resources.ApplyResources(this.btnPRINT_2, "btnPRINT_2");
            this.btnPRINT_2.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnPRINT_2.Name = "btnPRINT_2";
            this.btnPRINT_2.TabStop = true;
            this.btnPRINT_2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnPRINT_2_LinkClicked);
            // 
            // dgvBalancePrepayment
            // 
            this.dgvBalancePrepayment.BackgroundColor = System.Drawing.Color.White;
            this.dgvBalancePrepayment.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvBalancePrepayment.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvBalancePrepayment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBalancePrepayment.ColumnHeadersVisible = false;
            this.dgvBalancePrepayment.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn14,
            this.PREPAYMENT_BALANCE,
            this.dataGridViewTextBoxColumn16,
            this.FORMAT_BALANCE_PREPAYMENT});
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvBalancePrepayment.DefaultCellStyle = dataGridViewCellStyle15;
            this.dgvBalancePrepayment.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgvBalancePrepayment, "dgvBalancePrepayment");
            this.dgvBalancePrepayment.Name = "dgvBalancePrepayment";
            this.dgvBalancePrepayment.RowHeadersVisible = false;
            this.dgvBalancePrepayment.RowTemplate.Height = 25;
            this.dgvBalancePrepayment.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvBalancePrepayment_CellFormatting);
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "CURRENCY_ID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn14, "dataGridViewTextBoxColumn14");
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            // 
            // PREPAYMENT_BALANCE
            // 
            this.PREPAYMENT_BALANCE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PREPAYMENT_BALANCE.DataPropertyName = "BALANCE";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PREPAYMENT_BALANCE.DefaultCellStyle = dataGridViewCellStyle14;
            resources.ApplyResources(this.PREPAYMENT_BALANCE, "PREPAYMENT_BALANCE");
            this.PREPAYMENT_BALANCE.Name = "PREPAYMENT_BALANCE";
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.dataGridViewTextBoxColumn16.DataPropertyName = "CURRENCY_SING";
            this.dataGridViewTextBoxColumn16.FillWeight = 65F;
            resources.ApplyResources(this.dataGridViewTextBoxColumn16, "dataGridViewTextBoxColumn16");
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            // 
            // FORMAT_BALANCE_PREPAYMENT
            // 
            this.FORMAT_BALANCE_PREPAYMENT.DataPropertyName = "FORMAT";
            resources.ApplyResources(this.FORMAT_BALANCE_PREPAYMENT, "FORMAT_BALANCE_PREPAYMENT");
            this.FORMAT_BALANCE_PREPAYMENT.Name = "FORMAT_BALANCE_PREPAYMENT";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // dgvPrepayment
            // 
            this.dgvPrepayment.AllowUserToAddRows = false;
            this.dgvPrepayment.AllowUserToDeleteRows = false;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvPrepayment.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle16;
            this.dgvPrepayment.BackgroundColor = System.Drawing.Color.White;
            this.dgvPrepayment.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvPrepayment.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvPrepayment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPrepayment.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CUS_PREPAYMENT_ID,
            this.PAYMENT_ID,
            this.PREPAYMENT_NO,
            this.PREPAYMENT_DATE,
            this.PREPAYMENT_BY,
            this.AMOUNT_PREPAYMENT,
            this.BALANCE_PREPAYMENT,
            this.dataGridViewTextBoxColumn13,
            this.CURRENCY_ID_P,
            this.PREPAYMENT_NOTE,
            this.FORMAT,
            this.colACTION});
            this.dgvPrepayment.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgvPrepayment, "dgvPrepayment");
            this.dgvPrepayment.Name = "dgvPrepayment";
            this.dgvPrepayment.ReadOnly = true;
            this.dgvPrepayment.RowHeadersVisible = false;
            this.dgvPrepayment.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPrepayment_CellContentClick);
            this.dgvPrepayment.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvPrepayment_CellFormatting);
            // 
            // CUS_PREPAYMENT_ID
            // 
            this.CUS_PREPAYMENT_ID.DataPropertyName = "CUS_PREPAYMENT_ID";
            resources.ApplyResources(this.CUS_PREPAYMENT_ID, "CUS_PREPAYMENT_ID");
            this.CUS_PREPAYMENT_ID.Name = "CUS_PREPAYMENT_ID";
            this.CUS_PREPAYMENT_ID.ReadOnly = true;
            // 
            // PAYMENT_ID
            // 
            this.PAYMENT_ID.DataPropertyName = "PAYMENT_ID";
            resources.ApplyResources(this.PAYMENT_ID, "PAYMENT_ID");
            this.PAYMENT_ID.Name = "PAYMENT_ID";
            this.PAYMENT_ID.ReadOnly = true;
            // 
            // PREPAYMENT_NO
            // 
            this.PREPAYMENT_NO.DataPropertyName = "PREPAYMENT_NO";
            resources.ApplyResources(this.PREPAYMENT_NO, "PREPAYMENT_NO");
            this.PREPAYMENT_NO.Name = "PREPAYMENT_NO";
            this.PREPAYMENT_NO.ReadOnly = true;
            // 
            // PREPAYMENT_DATE
            // 
            this.PREPAYMENT_DATE.DataPropertyName = "CREATE_ON";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle17.Format = "dd-MM-yyyy";
            this.PREPAYMENT_DATE.DefaultCellStyle = dataGridViewCellStyle17;
            resources.ApplyResources(this.PREPAYMENT_DATE, "PREPAYMENT_DATE");
            this.PREPAYMENT_DATE.Name = "PREPAYMENT_DATE";
            this.PREPAYMENT_DATE.ReadOnly = true;
            // 
            // PREPAYMENT_BY
            // 
            this.PREPAYMENT_BY.DataPropertyName = "CREATE_BY";
            resources.ApplyResources(this.PREPAYMENT_BY, "PREPAYMENT_BY");
            this.PREPAYMENT_BY.Name = "PREPAYMENT_BY";
            this.PREPAYMENT_BY.ReadOnly = true;
            // 
            // AMOUNT_PREPAYMENT
            // 
            this.AMOUNT_PREPAYMENT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AMOUNT_PREPAYMENT.DataPropertyName = "AMOUNT";
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.AMOUNT_PREPAYMENT.DefaultCellStyle = dataGridViewCellStyle18;
            resources.ApplyResources(this.AMOUNT_PREPAYMENT, "AMOUNT_PREPAYMENT");
            this.AMOUNT_PREPAYMENT.Name = "AMOUNT_PREPAYMENT";
            this.AMOUNT_PREPAYMENT.ReadOnly = true;
            // 
            // BALANCE_PREPAYMENT
            // 
            this.BALANCE_PREPAYMENT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.BALANCE_PREPAYMENT.DataPropertyName = "BALANCE";
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.BALANCE_PREPAYMENT.DefaultCellStyle = dataGridViewCellStyle19;
            resources.ApplyResources(this.BALANCE_PREPAYMENT, "BALANCE_PREPAYMENT");
            this.BALANCE_PREPAYMENT.Name = "BALANCE_PREPAYMENT";
            this.BALANCE_PREPAYMENT.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn13.DataPropertyName = "CURRENCY_SING";
            this.dataGridViewTextBoxColumn13.FillWeight = 65F;
            resources.ApplyResources(this.dataGridViewTextBoxColumn13, "dataGridViewTextBoxColumn13");
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            // 
            // CURRENCY_ID_P
            // 
            this.CURRENCY_ID_P.DataPropertyName = "CURRENCY_ID";
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle20.Format = "#,##0.####";
            this.CURRENCY_ID_P.DefaultCellStyle = dataGridViewCellStyle20;
            resources.ApplyResources(this.CURRENCY_ID_P, "CURRENCY_ID_P");
            this.CURRENCY_ID_P.Name = "CURRENCY_ID_P";
            this.CURRENCY_ID_P.ReadOnly = true;
            // 
            // PREPAYMENT_NOTE
            // 
            this.PREPAYMENT_NOTE.DataPropertyName = "NOTE";
            resources.ApplyResources(this.PREPAYMENT_NOTE, "PREPAYMENT_NOTE");
            this.PREPAYMENT_NOTE.Name = "PREPAYMENT_NOTE";
            this.PREPAYMENT_NOTE.ReadOnly = true;
            // 
            // FORMAT
            // 
            this.FORMAT.DataPropertyName = "FORMAT";
            resources.ApplyResources(this.FORMAT, "FORMAT");
            this.FORMAT.Name = "FORMAT";
            this.FORMAT.ReadOnly = true;
            // 
            // colACTION
            // 
            this.colACTION.DataPropertyName = "ACTION";
            resources.ApplyResources(this.colACTION, "colACTION");
            this.colACTION.Name = "colACTION";
            this.colACTION.ReadOnly = true;
            // 
            // tabInvoice
            // 
            this.tabInvoice.Controls.Add(this.panel5);
            this.tabInvoice.Controls.Add(this.lblTOTAL_DUE_AMOUNT);
            this.tabInvoice.Controls.Add(this.dgvTotalBalance);
            this.tabInvoice.Controls.Add(this.btnREPORT_CUSTOMER_AR);
            this.tabInvoice.Controls.Add(this.btnADD_SERVICE);
            this.tabInvoice.Controls.Add(this.btnAPPLY_PAYMENT_1);
            this.tabInvoice.Controls.Add(this.cboInvoiceStatus);
            this.tabInvoice.Controls.Add(this.dgvInvoiceList);
            this.tabInvoice.Controls.Add(this.lblDATE);
            this.tabInvoice.Controls.Add(this.dtpYear);
            resources.ApplyResources(this.tabInvoice, "tabInvoice");
            this.tabInvoice.Name = "tabInvoice";
            this.tabInvoice.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnADJUST_AMOUNT);
            this.panel5.Controls.Add(this.btnADJUST);
            this.panel5.Controls.Add(this.btnDELETE);
            this.panel5.Controls.Add(this.btnPRINT_1);
            resources.ApplyResources(this.panel5, "panel5");
            this.panel5.Name = "panel5";
            // 
            // btnADJUST_AMOUNT
            // 
            resources.ApplyResources(this.btnADJUST_AMOUNT, "btnADJUST_AMOUNT");
            this.btnADJUST_AMOUNT.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnADJUST_AMOUNT.Name = "btnADJUST_AMOUNT";
            this.btnADJUST_AMOUNT.TabStop = true;
            this.btnADJUST_AMOUNT.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkAdjustAmount_LinkClicked);
            // 
            // btnADJUST
            // 
            resources.ApplyResources(this.btnADJUST, "btnADJUST");
            this.btnADJUST.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnADJUST.Name = "btnADJUST";
            this.btnADJUST.TabStop = true;
            this.btnADJUST.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkAdjustUsage_LinkClicked);
            // 
            // btnDELETE
            // 
            resources.ApplyResources(this.btnDELETE, "btnDELETE");
            this.btnDELETE.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnDELETE.Name = "btnDELETE";
            this.btnDELETE.TabStop = true;
            this.btnDELETE.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkDeleteInvoice_LinkClicked);
            // 
            // btnPRINT_1
            // 
            resources.ApplyResources(this.btnPRINT_1, "btnPRINT_1");
            this.btnPRINT_1.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnPRINT_1.Name = "btnPRINT_1";
            this.btnPRINT_1.TabStop = true;
            this.btnPRINT_1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkPrintInvoice_LinkClicked);
            // 
            // lblTOTAL_DUE_AMOUNT
            // 
            resources.ApplyResources(this.lblTOTAL_DUE_AMOUNT, "lblTOTAL_DUE_AMOUNT");
            this.lblTOTAL_DUE_AMOUNT.Name = "lblTOTAL_DUE_AMOUNT";
            // 
            // dgvTotalBalance
            // 
            this.dgvTotalBalance.BackgroundColor = System.Drawing.Color.White;
            this.dgvTotalBalance.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvTotalBalance.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvTotalBalance.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTotalBalance.ColumnHeadersVisible = false;
            this.dgvTotalBalance.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn8,
            this.INVOICE_BALANCE,
            this.dataGridViewTextBoxColumn9,
            this.FORMAT_BALNCE_INVOICE});
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvTotalBalance.DefaultCellStyle = dataGridViewCellStyle22;
            this.dgvTotalBalance.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgvTotalBalance, "dgvTotalBalance");
            this.dgvTotalBalance.Name = "dgvTotalBalance";
            this.dgvTotalBalance.RowHeadersVisible = false;
            this.dgvTotalBalance.RowTemplate.Height = 25;
            this.dgvTotalBalance.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvTotalBalance_CellFormatting);
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "CURRENCY_ID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn8, "dataGridViewTextBoxColumn8");
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // INVOICE_BALANCE
            // 
            this.INVOICE_BALANCE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.INVOICE_BALANCE.DataPropertyName = "TOTAL_BALANCE";
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.INVOICE_BALANCE.DefaultCellStyle = dataGridViewCellStyle21;
            resources.ApplyResources(this.INVOICE_BALANCE, "INVOICE_BALANCE");
            this.INVOICE_BALANCE.Name = "INVOICE_BALANCE";
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.dataGridViewTextBoxColumn9.DataPropertyName = "CURRENCY_SING";
            this.dataGridViewTextBoxColumn9.FillWeight = 30F;
            resources.ApplyResources(this.dataGridViewTextBoxColumn9, "dataGridViewTextBoxColumn9");
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // FORMAT_BALNCE_INVOICE
            // 
            this.FORMAT_BALNCE_INVOICE.DataPropertyName = "FORMAT";
            resources.ApplyResources(this.FORMAT_BALNCE_INVOICE, "FORMAT_BALNCE_INVOICE");
            this.FORMAT_BALNCE_INVOICE.Name = "FORMAT_BALNCE_INVOICE";
            // 
            // btnREPORT_CUSTOMER_AR
            // 
            resources.ApplyResources(this.btnREPORT_CUSTOMER_AR, "btnREPORT_CUSTOMER_AR");
            this.btnREPORT_CUSTOMER_AR.Name = "btnREPORT_CUSTOMER_AR";
            this.btnREPORT_CUSTOMER_AR.UseVisualStyleBackColor = true;
            this.btnREPORT_CUSTOMER_AR.Click += new System.EventHandler(this.btnCustomerAR_Click);
            // 
            // btnADD_SERVICE
            // 
            resources.ApplyResources(this.btnADD_SERVICE, "btnADD_SERVICE");
            this.btnADD_SERVICE.Name = "btnADD_SERVICE";
            this.btnADD_SERVICE.UseVisualStyleBackColor = true;
            this.btnADD_SERVICE.Click += new System.EventHandler(this.btnCharge_Click);
            // 
            // btnAPPLY_PAYMENT_1
            // 
            resources.ApplyResources(this.btnAPPLY_PAYMENT_1, "btnAPPLY_PAYMENT_1");
            this.btnAPPLY_PAYMENT_1.Name = "btnAPPLY_PAYMENT_1";
            this.btnAPPLY_PAYMENT_1.UseVisualStyleBackColor = true;
            this.btnAPPLY_PAYMENT_1.Click += new System.EventHandler(this.btnPay_Click);
            // 
            // cboInvoiceStatus
            // 
            this.cboInvoiceStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboInvoiceStatus.FormattingEnabled = true;
            resources.ApplyResources(this.cboInvoiceStatus, "cboInvoiceStatus");
            this.cboInvoiceStatus.Name = "cboInvoiceStatus";
            this.cboInvoiceStatus.SelectedIndexChanged += new System.EventHandler(this.cboInvoiceStatus_SelectedIndexChanged);
            // 
            // dgvInvoiceList
            // 
            dataGridViewCellStyle23.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvInvoiceList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle23;
            this.dgvInvoiceList.BackgroundColor = System.Drawing.Color.White;
            this.dgvInvoiceList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvInvoiceList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvInvoiceList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInvoiceList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.INVOICE_ID,
            this.INVOICE_STATUS,
            this.CURRENCY_ID,
            this.INVOICE_DATE,
            this.INVOICE_NO,
            this.INVOICE_TITLE,
            this.SETTLE_AMOUNT,
            this.PAID_AMOUNT,
            this.DUE_AMOUNT,
            this.CURRENCY_SING_,
            this.DUE_DATE,
            this.DUE_DAY,
            this.IS_SERVICE_BILL,
            this.FORMAT_INVOICE});
            this.dgvInvoiceList.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgvInvoiceList, "dgvInvoiceList");
            this.dgvInvoiceList.Name = "dgvInvoiceList";
            this.dgvInvoiceList.RowHeadersVisible = false;
            this.dgvInvoiceList.RowTemplate.Height = 25;
            this.dgvInvoiceList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInvoiceList_CellContentClick);
            this.dgvInvoiceList.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvInvoiceList_CellFormatting);
            this.dgvInvoiceList.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvInvoiceList_CellMouseClick);
            // 
            // INVOICE_ID
            // 
            this.INVOICE_ID.DataPropertyName = "INVOICE_ID";
            resources.ApplyResources(this.INVOICE_ID, "INVOICE_ID");
            this.INVOICE_ID.Name = "INVOICE_ID";
            // 
            // INVOICE_STATUS
            // 
            this.INVOICE_STATUS.DataPropertyName = "INVOICE_STATUS";
            resources.ApplyResources(this.INVOICE_STATUS, "INVOICE_STATUS");
            this.INVOICE_STATUS.Name = "INVOICE_STATUS";
            this.INVOICE_STATUS.ReadOnly = true;
            // 
            // CURRENCY_ID
            // 
            this.CURRENCY_ID.DataPropertyName = "CURRENCY_ID";
            resources.ApplyResources(this.CURRENCY_ID, "CURRENCY_ID");
            this.CURRENCY_ID.Name = "CURRENCY_ID";
            // 
            // INVOICE_DATE
            // 
            this.INVOICE_DATE.DataPropertyName = "INVOICE_DATE";
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle24.Format = "dd-MM-yyyy";
            this.INVOICE_DATE.DefaultCellStyle = dataGridViewCellStyle24;
            resources.ApplyResources(this.INVOICE_DATE, "INVOICE_DATE");
            this.INVOICE_DATE.Name = "INVOICE_DATE";
            // 
            // INVOICE_NO
            // 
            this.INVOICE_NO.DataPropertyName = "INVOICE_NO";
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle25.Format = "dd-MM-yyyy";
            this.INVOICE_NO.DefaultCellStyle = dataGridViewCellStyle25;
            resources.ApplyResources(this.INVOICE_NO, "INVOICE_NO");
            this.INVOICE_NO.Name = "INVOICE_NO";
            // 
            // INVOICE_TITLE
            // 
            this.INVOICE_TITLE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.INVOICE_TITLE.DataPropertyName = "INVOICE_TITLE";
            resources.ApplyResources(this.INVOICE_TITLE, "INVOICE_TITLE");
            this.INVOICE_TITLE.Name = "INVOICE_TITLE";
            // 
            // SETTLE_AMOUNT
            // 
            this.SETTLE_AMOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.SETTLE_AMOUNT.DataPropertyName = "SETTLE_AMOUNT";
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Underline);
            dataGridViewCellStyle26.ForeColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle26.NullValue = null;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.Color.Blue;
            this.SETTLE_AMOUNT.DefaultCellStyle = dataGridViewCellStyle26;
            resources.ApplyResources(this.SETTLE_AMOUNT, "SETTLE_AMOUNT");
            this.SETTLE_AMOUNT.Name = "SETTLE_AMOUNT";
            // 
            // PAID_AMOUNT
            // 
            this.PAID_AMOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.PAID_AMOUNT.DataPropertyName = "PAID_AMOUNT";
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Underline);
            dataGridViewCellStyle27.ForeColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.Color.Blue;
            this.PAID_AMOUNT.DefaultCellStyle = dataGridViewCellStyle27;
            resources.ApplyResources(this.PAID_AMOUNT, "PAID_AMOUNT");
            this.PAID_AMOUNT.Name = "PAID_AMOUNT";
            // 
            // DUE_AMOUNT
            // 
            this.DUE_AMOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.DUE_AMOUNT.DataPropertyName = "DUE_AMOUNT";
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle28.NullValue = null;
            this.DUE_AMOUNT.DefaultCellStyle = dataGridViewCellStyle28;
            resources.ApplyResources(this.DUE_AMOUNT, "DUE_AMOUNT");
            this.DUE_AMOUNT.Name = "DUE_AMOUNT";
            // 
            // CURRENCY_SING_
            // 
            this.CURRENCY_SING_.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.CURRENCY_SING_.DataPropertyName = "CURRENCY_SING";
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.CURRENCY_SING_.DefaultCellStyle = dataGridViewCellStyle29;
            this.CURRENCY_SING_.FillWeight = 30F;
            resources.ApplyResources(this.CURRENCY_SING_, "CURRENCY_SING_");
            this.CURRENCY_SING_.Name = "CURRENCY_SING_";
            // 
            // DUE_DATE
            // 
            this.DUE_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.DUE_DATE.DataPropertyName = "DUE_DATE";
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle30.Format = "dd-MM-yyyy";
            this.DUE_DATE.DefaultCellStyle = dataGridViewCellStyle30;
            resources.ApplyResources(this.DUE_DATE, "DUE_DATE");
            this.DUE_DATE.Name = "DUE_DATE";
            // 
            // DUE_DAY
            // 
            this.DUE_DAY.DataPropertyName = "DUE_DAY";
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DUE_DAY.DefaultCellStyle = dataGridViewCellStyle31;
            resources.ApplyResources(this.DUE_DAY, "DUE_DAY");
            this.DUE_DAY.Name = "DUE_DAY";
            // 
            // IS_SERVICE_BILL
            // 
            this.IS_SERVICE_BILL.DataPropertyName = "IS_SERVICE_BILL";
            resources.ApplyResources(this.IS_SERVICE_BILL, "IS_SERVICE_BILL");
            this.IS_SERVICE_BILL.Name = "IS_SERVICE_BILL";
            // 
            // FORMAT_INVOICE
            // 
            this.FORMAT_INVOICE.DataPropertyName = "FORMAT";
            resources.ApplyResources(this.FORMAT_INVOICE, "FORMAT_INVOICE");
            this.FORMAT_INVOICE.Name = "FORMAT_INVOICE";
            // 
            // lblDATE
            // 
            resources.ApplyResources(this.lblDATE, "lblDATE");
            this.lblDATE.Name = "lblDATE";
            // 
            // dtpYear
            // 
            this.dtpYear.Checked = false;
            resources.ApplyResources(this.dtpYear, "dtpYear");
            this.dtpYear.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpYear.Name = "dtpYear";
            this.dtpYear.ShowCheckBox = true;
            this.dtpYear.ValueChanged += new System.EventHandler(this.cboInvoiceStatus_SelectedIndexChanged);
            // 
            // tabUSAGE
            // 
            this.tabUSAGE.Controls.Add(this.btnREPORT);
            this.tabUSAGE.Controls.Add(this.chart1);
            resources.ApplyResources(this.tabUSAGE, "tabUSAGE");
            this.tabUSAGE.Name = "tabUSAGE";
            this.tabUSAGE.UseVisualStyleBackColor = true;
            // 
            // btnREPORT
            // 
            resources.ApplyResources(this.btnREPORT, "btnREPORT");
            this.btnREPORT.Name = "btnREPORT";
            this.btnREPORT.UseVisualStyleBackColor = true;
            this.btnREPORT.Click += new System.EventHandler(this.btnPrintUsageReport_Click);
            // 
            // chart1
            // 
            this.chart1.Application = "";
            this.chart1.ApplicationDNC = "";
            this.chart1.Background.Color = System.Drawing.Color.White;
            background1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(235)))), ((int)(((byte)(238)))));
            chartArea1.Background = background1;
            line1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(28)))), ((int)(((byte)(59)))));
            subValue1.Line = line1;
            subValue1.Name = "";
            element1.DefaultSubValue = subValue1;
            element1.Hotspot = hotspot1;
            legendEntry1.Background = background2;
            line2.Color = System.Drawing.Color.Empty;
            legendEntry1.DividerLine = line2;
            legendEntry1.Hotspot = hotspot2;
            legendEntry1.ShapeType = dotnetCHARTING.WinForms.ShapeType.None;
            element1.LegendEntry = legendEntry1;
            element1.ShapeType = dotnetCHARTING.WinForms.ShapeType.None;
            element1.XAxisTick = null;
            element1.YAxisTick = null;
            chartArea1.DefaultElement = element1;
            line3.Color = System.Drawing.Color.LightGray;
            chartArea1.InteriorLine = line3;
            label7.Font = new System.Drawing.Font("Tahoma", 8F);
            label7.Hotspot = hotspot3;
            label7.Truncation = truncation1;
            chartArea1.Label = label7;
            background3.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(219)))));
            legendBox1.Background = background3;
            legendBox1.CornerBottomRight = dotnetCHARTING.WinForms.BoxCorner.Cut;
            legendEntry2.Background = background4;
            line4.Color = System.Drawing.Color.Empty;
            legendEntry2.DividerLine = line4;
            legendEntry2.Hotspot = hotspot4;
            label8.Hotspot = hotspot5;
            label8.Truncation = truncation2;
            legendEntry2.LabelStyle = label8;
            legendEntry2.ShapeType = dotnetCHARTING.WinForms.ShapeType.None;
            legendBox1.DefaultEntry = legendEntry2;
            legendEntry3.Background = background5;
            line5.Color = System.Drawing.Color.Gray;
            legendEntry3.DividerLine = line5;
            legendEntry3.Hotspot = hotspot6;
            legendEntry3.Name = "Name";
            legendEntry3.ShapeType = dotnetCHARTING.WinForms.ShapeType.None;
            legendEntry3.SortOrder = -1;
            legendEntry3.Value = "Value";
            legendEntry3.Visible = false;
            legendBox1.HeaderEntry = legendEntry3;
            line6.Color = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            legendBox1.InteriorLine = line6;
            legendBox1.LabelStyle = label8;
            line7.Color = System.Drawing.Color.Gray;
            legendBox1.Line = line7;
            legendBox1.Padding = 4;
            legendBox1.Position = dotnetCHARTING.WinForms.LegendBoxPosition.Top;
            legendBox1.Shadow = shadow1;
            legendBox1.Visible = true;
            chartArea1.LegendBox = legendBox1;
            line8.Color = System.Drawing.Color.Gray;
            chartArea1.Line = line8;
            chartArea1.Shadow = shadow2;
            chartArea1.StartDateOfYear = new System.DateTime(((long)(0)));
            background6.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(219)))));
            box1.Background = background6;
            line9.Color = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            box1.InteriorLine = line9;
            label9.Color = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(45)))), ((int)(((byte)(38)))));
            label9.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            label9.Hotspot = hotspot7;
            label9.Truncation = truncation3;
            box1.Label = label9;
            line10.Color = System.Drawing.Color.Gray;
            box1.Line = line10;
            box1.Position = null;
            box1.Shadow = shadow3;
            box1.Visible = true;
            chartArea1.TitleBox = box1;
            line11.Color = System.Drawing.Color.LightGray;
            axisTick1.GridLine = line11;
            line12.Length = 3;
            axisTick1.Line = line12;
            axis1.DefaultTick = axisTick1;
            axis1.GaugeLabelMode = dotnetCHARTING.WinForms.GaugeLabelMode.Default;
            axis1.GaugeNeedleType = dotnetCHARTING.WinForms.GaugeNeedleType.One;
            axis1.LabelMarker = null;
            timeIntervalAdvanced1.Start = new System.DateTime(((long)(0)));
            timeIntervalAdvanced1.TimeSpan = System.TimeSpan.Parse("00:00:00");
            axis1.MinorTimeIntervalAdvanced = timeIntervalAdvanced1;
            axis1.ScaleRange = scaleRange1;
            axis1.SmartScaleBreakLimit = 2;
            axis1.TickLabelSeparatorLine = line13;
            axis1.TimeInterval = dotnetCHARTING.WinForms.TimeInterval.Minutes;
            timeIntervalAdvanced2.Start = new System.DateTime(((long)(0)));
            timeIntervalAdvanced2.TimeSpan = System.TimeSpan.Parse("00:00:00");
            axis1.TimeIntervalAdvanced = timeIntervalAdvanced2;
            line14.Color = System.Drawing.Color.Red;
            axisTick2.GridLine = line14;
            line15.Length = 3;
            axisTick2.Line = line15;
            axis1.ZeroTick = axisTick2;
            chartArea1.XAxis = axis1;
            line16.Color = System.Drawing.Color.LightGray;
            axisTick3.GridLine = line16;
            line17.Length = 3;
            axisTick3.Line = line17;
            axis2.DefaultTick = axisTick3;
            axis2.GaugeLabelMode = dotnetCHARTING.WinForms.GaugeLabelMode.Default;
            axis2.GaugeNeedleType = dotnetCHARTING.WinForms.GaugeNeedleType.One;
            axis2.LabelMarker = null;
            axis2.MinorTimeIntervalAdvanced = timeIntervalAdvanced1;
            axis2.ScaleRange = scaleRange2;
            axis2.SmartScaleBreakLimit = 2;
            axis2.TickLabelSeparatorLine = line18;
            axis2.TimeInterval = dotnetCHARTING.WinForms.TimeInterval.Minutes;
            timeIntervalAdvanced3.Start = new System.DateTime(((long)(0)));
            timeIntervalAdvanced3.TimeSpan = System.TimeSpan.Parse("00:00:00");
            axis2.TimeIntervalAdvanced = timeIntervalAdvanced3;
            line19.Color = System.Drawing.Color.Red;
            axisTick4.GridLine = line19;
            line20.Length = 3;
            axisTick4.Line = line20;
            axis2.ZeroTick = axisTick4;
            chartArea1.YAxis = axis2;
            this.chart1.ChartArea = chartArea1;
            this.chart1.DataGrid = null;
            line21.Color = System.Drawing.Color.LightGray;
            axisTick5.GridLine = line21;
            line22.Length = 3;
            axisTick5.Line = line22;
            axis3.DefaultTick = axisTick5;
            axis3.GaugeLabelMode = dotnetCHARTING.WinForms.GaugeLabelMode.Default;
            axis3.GaugeNeedleType = dotnetCHARTING.WinForms.GaugeNeedleType.One;
            axis3.LabelMarker = null;
            axis3.MinorTimeIntervalAdvanced = timeIntervalAdvanced1;
            axis3.ScaleRange = scaleRange3;
            axis3.SmartScaleBreakLimit = 2;
            axis3.TickLabelSeparatorLine = line23;
            axis3.TimeInterval = dotnetCHARTING.WinForms.TimeInterval.Minutes;
            timeIntervalAdvanced4.Start = new System.DateTime(((long)(0)));
            timeIntervalAdvanced4.TimeSpan = System.TimeSpan.Parse("00:00:00");
            axis3.TimeIntervalAdvanced = timeIntervalAdvanced4;
            line24.Color = System.Drawing.Color.Red;
            axisTick6.GridLine = line24;
            line25.Length = 3;
            axisTick6.Line = line25;
            axis3.ZeroTick = axisTick6;
            this.chart1.DefaultAxis = axis3;
            this.chart1.DefaultCultureName = "";
            line26.Color = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(28)))), ((int)(((byte)(59)))));
            subValue2.Line = line26;
            subValue2.Name = "";
            element2.DefaultSubValue = subValue2;
            element2.Hotspot = hotspot8;
            legendEntry4.Background = background7;
            line27.Color = System.Drawing.Color.Empty;
            legendEntry4.DividerLine = line27;
            legendEntry4.Hotspot = hotspot9;
            legendEntry4.ShapeType = dotnetCHARTING.WinForms.ShapeType.None;
            element2.LegendEntry = legendEntry4;
            element2.ShapeType = dotnetCHARTING.WinForms.ShapeType.None;
            smartLabel1.Color = System.Drawing.Color.Empty;
            smartLabel1.Hotspot = hotspot10;
            smartLabel1.Line = line28;
            smartLabel1.Truncation = truncation4;
            element2.SmartLabel = smartLabel1;
            element2.XAxisTick = null;
            element2.YAxisTick = null;
            this.chart1.DefaultElement = element2;
            this.chart1.DefaultShadow = shadow4;
            this.chart1.FunnelNozzleWidthPercentage = 35;
            resources.ApplyResources(this.chart1, "chart1");
            this.chart1.Name = "chart1";
            this.chart1.NoDataLabel.Hotspot = hotspot11;
            this.chart1.NoDataLabel.Text = "No Data";
            this.chart1.NoDataLabel.Truncation = truncation5;
            this.chart1.Palette = new System.Drawing.Color[] {
        System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(154)))), ((int)(((byte)(255))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(48)))), ((int)(((byte)(99))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(206))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(255)))), ((int)(((byte)(255))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(0)))), ((int)(((byte)(99))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(130)))), ((int)(((byte)(132))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(101)))), ((int)(((byte)(206))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(207)))), ((int)(((byte)(255))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(132))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(255))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(0))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(0)))), ((int)(((byte)(132))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(0)))), ((int)(((byte)(0))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(130)))), ((int)(((byte)(132))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(207)))), ((int)(((byte)(255))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(255)))), ((int)(((byte)(255))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(255)))), ((int)(((byte)(206))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(156))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(207)))), ((int)(((byte)(255))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(154)))), ((int)(((byte)(206))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(154)))), ((int)(((byte)(255))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(207)))), ((int)(((byte)(156))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(101)))), ((int)(((byte)(255))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(207)))), ((int)(((byte)(206))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(207)))), ((int)(((byte)(0))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(207)))), ((int)(((byte)(0))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(154)))), ((int)(((byte)(0))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(101)))), ((int)(((byte)(0))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(101)))), ((int)(((byte)(156))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(150)))), ((int)(((byte)(148))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(48)))), ((int)(((byte)(99))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(154)))), ((int)(((byte)(99))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(48)))), ((int)(((byte)(0))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(48)))), ((int)(((byte)(0))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(48)))), ((int)(((byte)(0))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(48)))), ((int)(((byte)(99))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(48)))), ((int)(((byte)(156))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(48)))), ((int)(((byte)(49))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(0))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(0))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(255))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(0)))), ((int)(((byte)(0))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(130)))), ((int)(((byte)(0))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(132))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(130)))), ((int)(((byte)(0))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(0)))), ((int)(((byte)(132))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(130)))), ((int)(((byte)(132))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(195)))), ((int)(((byte)(198))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(130)))), ((int)(((byte)(132)))))};
            this.chart1.SmartLabelLine = line28;
            this.chart1.SpacingPercentageNested = 4;
            this.chart1.StartDateOfYear = new System.DateTime(((long)(0)));
            this.chart1.TempDirectory = "C:\\Users\\dev\\AppData\\Local\\Temp\\";
            this.chart1.ToolTipTemplate = "";
            background8.Color = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(235)))), ((int)(((byte)(238)))));
            chartArea2.Background = background8;
            subValue3.Line = line29;
            subValue3.Name = "";
            element3.DefaultSubValue = subValue3;
            element3.Hotspot = hotspot12;
            legendEntry5.Background = background9;
            line30.Color = System.Drawing.Color.Empty;
            legendEntry5.DividerLine = line30;
            legendEntry5.Hotspot = hotspot13;
            legendEntry5.ShapeType = dotnetCHARTING.WinForms.ShapeType.None;
            element3.LegendEntry = legendEntry5;
            element3.ShapeType = dotnetCHARTING.WinForms.ShapeType.None;
            element3.XAxisTick = null;
            element3.YAxisTick = null;
            chartArea2.DefaultElement = element3;
            line31.Color = System.Drawing.Color.LightGray;
            chartArea2.InteriorLine = line31;
            label10.Font = new System.Drawing.Font("Tahoma", 8F);
            label10.Hotspot = hotspot14;
            label10.Truncation = truncation6;
            chartArea2.Label = label10;
            chartArea2.LegendBox = null;
            line32.Color = System.Drawing.Color.Gray;
            chartArea2.Line = line32;
            chartArea2.Shadow = shadow5;
            chartArea2.StartDateOfYear = new System.DateTime(((long)(0)));
            background10.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(219)))));
            box2.Background = background10;
            line33.Color = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            box2.InteriorLine = line33;
            label11.Color = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(45)))), ((int)(((byte)(38)))));
            label11.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            label11.Hotspot = hotspot15;
            label11.Truncation = truncation7;
            box2.Label = label11;
            line34.Color = System.Drawing.Color.Gray;
            box2.Line = line34;
            box2.Position = null;
            box2.Shadow = shadow6;
            box2.Visible = true;
            chartArea2.TitleBox = box2;
            chartArea2.XAxis = axis1;
            line35.Color = System.Drawing.Color.LightGray;
            axisTick7.GridLine = line35;
            line36.Length = 3;
            axisTick7.Line = line36;
            axis4.DefaultTick = axisTick7;
            axis4.GaugeLabelMode = dotnetCHARTING.WinForms.GaugeLabelMode.Default;
            axis4.GaugeNeedleType = dotnetCHARTING.WinForms.GaugeNeedleType.One;
            axis4.LabelMarker = null;
            timeIntervalAdvanced5.Start = new System.DateTime(((long)(0)));
            timeIntervalAdvanced5.TimeSpan = System.TimeSpan.Parse("00:00:00");
            axis4.MinorTimeIntervalAdvanced = timeIntervalAdvanced5;
            axis4.ScaleRange = scaleRange4;
            axis4.SmartScaleBreakLimit = 2;
            axis4.TickLabelSeparatorLine = line37;
            axis4.TimeInterval = dotnetCHARTING.WinForms.TimeInterval.Minutes;
            timeIntervalAdvanced6.Start = new System.DateTime(((long)(0)));
            timeIntervalAdvanced6.TimeSpan = System.TimeSpan.Parse("00:00:00");
            axis4.TimeIntervalAdvanced = timeIntervalAdvanced6;
            line38.Color = System.Drawing.Color.Red;
            axisTick8.GridLine = line38;
            line39.Length = 3;
            axisTick8.Line = line39;
            axis4.ZeroTick = axisTick8;
            chartArea2.YAxis = axis4;
            this.chart1.VolumeArea = chartArea2;
            // 
            // tabAttachment
            // 
            this.tabAttachment.Controls.Add(this.txtSearchAttachment);
            this.tabAttachment.Controls.Add(this.btnREMOVE_ATTACHMENT);
            this.tabAttachment.Controls.Add(this.btnADDNEW_ATTCHMENT);
            this.tabAttachment.Controls.Add(this.btnVIEW_ATTACHMENT);
            this.tabAttachment.Controls.Add(this.btnADJUST_ATTACHMENT);
            this.tabAttachment.Controls.Add(this.dgvAttachment);
            resources.ApplyResources(this.tabAttachment, "tabAttachment");
            this.tabAttachment.Name = "tabAttachment";
            this.tabAttachment.UseVisualStyleBackColor = true;
            // 
            // txtSearchAttachment
            // 
            this.txtSearchAttachment.BackColor = System.Drawing.Color.White;
            this.txtSearchAttachment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtSearchAttachment, "txtSearchAttachment");
            this.txtSearchAttachment.Name = "txtSearchAttachment";
            this.txtSearchAttachment.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtSearchAttachment.QuickSearch += new System.EventHandler(this.txtSearchAttachment_QuickSearch);
            // 
            // btnREMOVE_ATTACHMENT
            // 
            resources.ApplyResources(this.btnREMOVE_ATTACHMENT, "btnREMOVE_ATTACHMENT");
            this.btnREMOVE_ATTACHMENT.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnREMOVE_ATTACHMENT.Name = "btnREMOVE_ATTACHMENT";
            this.btnREMOVE_ATTACHMENT.TabStop = true;
            this.btnREMOVE_ATTACHMENT.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkRemoveAttach_LinkClicked);
            // 
            // btnADDNEW_ATTCHMENT
            // 
            resources.ApplyResources(this.btnADDNEW_ATTCHMENT, "btnADDNEW_ATTCHMENT");
            this.btnADDNEW_ATTCHMENT.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnADDNEW_ATTCHMENT.Name = "btnADDNEW_ATTCHMENT";
            this.btnADDNEW_ATTCHMENT.TabStop = true;
            this.btnADDNEW_ATTCHMENT.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkAddNewAttach_LinkClicked);
            // 
            // btnVIEW_ATTACHMENT
            // 
            resources.ApplyResources(this.btnVIEW_ATTACHMENT, "btnVIEW_ATTACHMENT");
            this.btnVIEW_ATTACHMENT.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnVIEW_ATTACHMENT.Name = "btnVIEW_ATTACHMENT";
            this.btnVIEW_ATTACHMENT.TabStop = true;
            this.btnVIEW_ATTACHMENT.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkPrintAttachment_LinkClicked);
            // 
            // btnADJUST_ATTACHMENT
            // 
            resources.ApplyResources(this.btnADJUST_ATTACHMENT, "btnADJUST_ATTACHMENT");
            this.btnADJUST_ATTACHMENT.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnADJUST_ATTACHMENT.Name = "btnADJUST_ATTACHMENT";
            this.btnADJUST_ATTACHMENT.TabStop = true;
            this.btnADJUST_ATTACHMENT.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkAdjustAttachment_LinkClicked);
            // 
            // dgvAttachment
            // 
            this.dgvAttachment.AllowUserToAddRows = false;
            this.dgvAttachment.AllowUserToDeleteRows = false;
            this.dgvAttachment.AllowUserToResizeColumns = false;
            this.dgvAttachment.AllowUserToResizeRows = false;
            dataGridViewCellStyle32.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvAttachment.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle32;
            this.dgvAttachment.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvAttachment.BackgroundColor = System.Drawing.Color.White;
            this.dgvAttachment.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvAttachment.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvAttachment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAttachment.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CUSTOMER_ATTACHMENT_ID,
            this.ATTACHMENT_NAME,
            this.dataGridViewTextBoxColumn4,
            this.DOWNLOAD_ATTACHMENT,
            this.FILE_NAME_TYPE,
            this.SIZE,
            this.CREATED_ON,
            this.CREATED_BY,
            this.IS_ACTIVE});
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle37.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle37.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            dataGridViewCellStyle37.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle37.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle37.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle37.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAttachment.DefaultCellStyle = dataGridViewCellStyle37;
            this.dgvAttachment.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgvAttachment, "dgvAttachment");
            this.dgvAttachment.MultiSelect = false;
            this.dgvAttachment.Name = "dgvAttachment";
            this.dgvAttachment.ReadOnly = true;
            this.dgvAttachment.RowHeadersVisible = false;
            this.dgvAttachment.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAttachment.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAttachment_CellContentClick);
            // 
            // CUSTOMER_ATTACHMENT_ID
            // 
            this.CUSTOMER_ATTACHMENT_ID.DataPropertyName = "CUSTOMER_ATTACHMENT_ID";
            resources.ApplyResources(this.CUSTOMER_ATTACHMENT_ID, "CUSTOMER_ATTACHMENT_ID");
            this.CUSTOMER_ATTACHMENT_ID.Name = "CUSTOMER_ATTACHMENT_ID";
            this.CUSTOMER_ATTACHMENT_ID.ReadOnly = true;
            // 
            // ATTACHMENT_NAME
            // 
            this.ATTACHMENT_NAME.DataPropertyName = "ATTACHMENT_NAME";
            this.ATTACHMENT_NAME.FillWeight = 170.3232F;
            resources.ApplyResources(this.ATTACHMENT_NAME, "ATTACHMENT_NAME");
            this.ATTACHMENT_NAME.Name = "ATTACHMENT_NAME";
            this.ATTACHMENT_NAME.ReadOnly = true;
            this.ATTACHMENT_NAME.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ATTACHMENT_NAME.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ATTACHMENT_NAME.VisitedLinkColor = System.Drawing.Color.Blue;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "NOTE";
            this.dataGridViewTextBoxColumn4.FillWeight = 124.8421F;
            resources.ApplyResources(this.dataGridViewTextBoxColumn4, "dataGridViewTextBoxColumn4");
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // DOWNLOAD_ATTACHMENT
            // 
            this.DOWNLOAD_ATTACHMENT.DataPropertyName = "DOWNLOAD";
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DOWNLOAD_ATTACHMENT.DefaultCellStyle = dataGridViewCellStyle33;
            this.DOWNLOAD_ATTACHMENT.FillWeight = 48.47535F;
            resources.ApplyResources(this.DOWNLOAD_ATTACHMENT, "DOWNLOAD_ATTACHMENT");
            this.DOWNLOAD_ATTACHMENT.Name = "DOWNLOAD_ATTACHMENT";
            this.DOWNLOAD_ATTACHMENT.ReadOnly = true;
            this.DOWNLOAD_ATTACHMENT.VisitedLinkColor = System.Drawing.Color.Blue;
            // 
            // FILE_NAME_TYPE
            // 
            this.FILE_NAME_TYPE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.FILE_NAME_TYPE.DataPropertyName = "FILE_NAME_TYPE";
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.FILE_NAME_TYPE.DefaultCellStyle = dataGridViewCellStyle34;
            this.FILE_NAME_TYPE.FillWeight = 213.198F;
            resources.ApplyResources(this.FILE_NAME_TYPE, "FILE_NAME_TYPE");
            this.FILE_NAME_TYPE.Name = "FILE_NAME_TYPE";
            this.FILE_NAME_TYPE.ReadOnly = true;
            // 
            // SIZE
            // 
            this.SIZE.DataPropertyName = "SIZE";
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.SIZE.DefaultCellStyle = dataGridViewCellStyle35;
            this.SIZE.FillWeight = 34.36783F;
            resources.ApplyResources(this.SIZE, "SIZE");
            this.SIZE.Name = "SIZE";
            this.SIZE.ReadOnly = true;
            // 
            // CREATED_ON
            // 
            this.CREATED_ON.DataPropertyName = "CREATED_ON";
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle36.Format = "dd-MM-yyyy";
            this.CREATED_ON.DefaultCellStyle = dataGridViewCellStyle36;
            this.CREATED_ON.FillWeight = 50.01147F;
            resources.ApplyResources(this.CREATED_ON, "CREATED_ON");
            this.CREATED_ON.Name = "CREATED_ON";
            this.CREATED_ON.ReadOnly = true;
            // 
            // CREATED_BY
            // 
            this.CREATED_BY.DataPropertyName = "CREATED_BY";
            this.CREATED_BY.FillWeight = 58.7821F;
            resources.ApplyResources(this.CREATED_BY, "CREATED_BY");
            this.CREATED_BY.Name = "CREATED_BY";
            this.CREATED_BY.ReadOnly = true;
            // 
            // IS_ACTIVE
            // 
            this.IS_ACTIVE.DataPropertyName = "IS_ACTIVE";
            resources.ApplyResources(this.IS_ACTIVE, "IS_ACTIVE");
            this.IS_ACTIVE.Name = "IS_ACTIVE";
            this.IS_ACTIVE.ReadOnly = true;
            // 
            // btnActivate
            // 
            resources.ApplyResources(this.btnActivate, "btnActivate");
            this.btnActivate.Name = "btnActivate";
            this.btnActivate.UseVisualStyleBackColor = true;
            this.btnActivate.Click += new System.EventHandler(this.btnActivate_Click);
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // cmsStrip
            // 
            resources.ApplyResources(this.cmsStrip, "cmsStrip");
            this.cmsStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cmsStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ChangeConnectionType,
            this.ChangeBasePrice});
            this.cmsStrip.Name = "cmsStrip";
            this.cmsStrip.Opening += new System.ComponentModel.CancelEventHandler(this.cmsStrip_Opening);
            // 
            // ChangeConnectionType
            // 
            this.ChangeConnectionType.Name = "ChangeConnectionType";
            resources.ApplyResources(this.ChangeConnectionType, "ChangeConnectionType");
            this.ChangeConnectionType.Click += new System.EventHandler(this.ChangeConnectionType_Click);
            // 
            // ChangeBasePrice
            // 
            this.ChangeBasePrice.Name = "ChangeBasePrice";
            resources.ApplyResources(this.ChangeBasePrice, "ChangeBasePrice");
            this.ChangeBasePrice.Click += new System.EventHandler(this.ChangeBasePrice_Click);
            // 
            // DialogCustomer
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogCustomer";
            this.Load += new System.EventHandler(this.DialogCustomer_Load);
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPhoto)).EndInit();
            this.tab.ResumeLayout(false);
            this.tabGENERAL_INFORMATION.ResumeLayout(false);
            this.tabGENERAL_INFORMATION.PerformLayout();
            this.tabUSAGE_INFORMATION.ResumeLayout(false);
            this.tabUSAGE_INFORMATION.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEquitment)).EndInit();
            this.tabDeposit.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBalanceDeposit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeposit)).EndInit();
            this.tabBILLING_AND_METER.ResumeLayout(false);
            this.tabBILLING_AND_METER.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCutoffDays)).EndInit();
            this.tabRECURRING_SERVICE.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvService)).EndInit();
            this.tabPREPAYMENT.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBalancePrepayment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrepayment)).EndInit();
            this.tabInvoice.ResumeLayout(false);
            this.tabInvoice.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTotalBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInvoiceList)).EndInit();
            this.tabUSAGE.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.tabAttachment.ResumeLayout(false);
            this.tabAttachment.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAttachment)).EndInit();
            this.cmsStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnSAVE;
        private ComboBox cboArea;
        private Label lblAREA;
        private TextBox txtPlaceofBirth;
        private Label lblPLACE_OF_BIRTH;
        private TextBox txtCustomerCode;
        private Label lblCUSTOMER_CODE;
        private PictureBox picPhoto;
        private DateTimePicker dtpDOB;
        private Label lblDATE_OF_BIRTH;
        private Label lblADDRESS_INFORMATION;
        private TextBox txtAddress;
        private Label lblADDRESS;
        private ComboBox cboVillage;
        private ComboBox cboCommune;
        private ComboBox cboDistrict;
        private ComboBox cboProvince;
        private Label lblVILLAGE;
        private Label lblCOMMUNE;
        private Label lblDISTRICT;
        private Label lblPROVINCE;
        private TextBox txtPhone2;
        private TextBox txtPhone1;
        private TextBox txtCompanyName;
        private TextBox txtIdNumber;
        private ComboBox cboSex;
        private TextBox txtLastNameKH;
        private TextBox txtFirstNameKH;
        private Label lblNICKNAME;
        private Label lblID_CARD_NUMBER;
        private Label lblPhone_II;
        private Label lblPHONE_I;
        private Label lblSEX;
        private Label lblLAST_NAME_KH;
        private Label lblFIRST_NAME_KH;
        private ExButton btnCLOSE;
        private Label lblSTREET_NO;
        private Label lblHOUSE_NO;
        private TextBox txtStreetNo;
        private TextBox txtHouseNo;
        private Label lblLAST_NAME;
        private TextBox txtLastName;
        private Label lblFIRST_NAME;
        private TextBox txtFirstName;
        private ExLinkLabel btnPHOTO;
        private Label label28;
        private Label label27;
        private Label label25;
        private Label label39;
        private Label label38;
        private Label label37;
        private Label label36;
        private Label label34;
        private Label lblJOB;
        private TextBox txtCheckDigit;
        private TabControl tab;
        private TabPage tabGENERAL_INFORMATION;
        private TabPage tabUSAGE_INFORMATION;
        private Label label33;
        private Label label32;
        private Label label31;
        private Label label30;
        private Label lblAMPERE;
        private Label lblPRICE_AND_BILLING_CYCLE;
        private Label lblPHASE;
        private Label lblVOLTAGE;
        private Label lblEQUIPMENT;
        private ComboBox cboPhase;
        private ComboBox cboAMP;
        private ComboBox cboVol;
        private ExLinkLabel btnEDIT;
        private ExLinkLabel btnREMOVE;
        private ExLinkLabel btnADD;
        private TextBox txtTotalPower;
        private Label lblTOTAL_WATT;
        private DataGridView dgvEquitment;
        private TabPage tabBILLING_AND_METER;
        private DateTimePicker dtpActivateDate;
        private Label label45;
        private Label lblACTIVATE_DATE;
        private NumericUpDown nudCutoffDays;
        private Label lblCUT_DAY;
        private ComboBox cboBillingCycle;
        private Label lblUNLIMITE_CUT_DAY;
        private Label lblCONSTANT;
        private TextBox txtMeterConstant;
        private TextBox txtMeterType;
        private Label label50;
        private Label lblAMPARE;
        private Label lblVOLTAGE_1;
        private Label lblPHASE_1;
        private Label lblCABLE_SHIELD;
        private Label lblSHIELD;
        private TextBox txtMeterAmp;
        private TextBox txtMeterVol;
        private TextBox txtMeterPhase;
        private Label lblMETER_CODE;
        private Label lblMETER_INSTALLATION;
        private Label lblMETER_TYPE;
        private ComboBox cboCableShield;
        private ComboBox cboMeterShield;
        private TextBox txtBreakerType;
        private Label label53;
        private Label lblBREAKER_INSTALLATION;
        private Label lblAMPARE_2;
        private Label lblVOLTAGE_2;
        private Label lblPHASE_2;
        private TextBox txtBreakerAmp;
        private TextBox txtBreakerVol;
        private TextBox txtBreakerPhase;
        private Label lblBREAKER_CODE;
        private Label lblBREAKER_TYPE;
        private Label lblCONSTANT_1;
        private TextBox txtBreakerConstant;
        private TextBox txtPoleCode;
        private Label lblPOLE_AND_BOX_INSTALLATION;
        private TextBox txtCutter;
        private TextBox txtBiller;
        private TextBox txtCollector;
        private Label label71;
        private Label lblCUTTER;
        private Label lblBILLER;
        private Label lblCOLLECTOR;
        private Label lblBOX;
        private Label lblPOLE;
        private TabPage tabInvoice;
        private ComboBox cboInvoiceStatus;
        private DataGridView dgvInvoiceList;
        private DateTimePicker dtpYear;
        private ExButton btnActivate;
        private ExTextbox txtBox;
        private ExTextbox txtBreaker;
        private ExTextbox txtMeter;
        private ExLinkLabel btnCHANGE_BOX;
        private ExLinkLabel btnCHANGE_BREAKER;
        private ExLinkLabel btnCHANGE_METER;
        private ExButton btnCHANGE_LOG;
        private ExLinkLabel btnADJUST_AMOUNT;
        private ExLinkLabel btnADJUST;
        private ExLinkLabel btnPRINT_1;
        private ExButton btnAPPLY_PAYMENT_1;
        private ExButton btnADD_SERVICE;
        private TabPage tabUSAGE;
        private Chart chart1;
        private ExButton btnREPORT;
        private ComboBox cboPrice;
        private TabPage tabDeposit;
        private DataGridView dgvDeposit;
        private ExLinkLabel btnADD_DEPOSIT;
        private ExLinkLabel btnAPPLY_PAYMENT;
        private ExLinkLabel btnPRINT;
        private Label label35;
        private TextBox txtJob;
        private TabPage tabRECURRING_SERVICE;
        private ExLinkLabel btnREMOVE_1;
        private ExLinkLabel btnEDIT_1;
        private ExLinkLabel btnADD_1;
        private DataGridView dgvService;
        private ExButton btnREPORT_CONNECTION;
        private ExButton btnREPORT_AGREEMENT;
        private Label label46;
        private TextBox txtTotalFamily;
        private Label lblTOTAL_FAMILY;
        private TextBox txtPositionInBox;
        private Label lblPOSITION_IN_BOX;
        private DataGridView dgvTotalBalance;
        private DataGridView dgvBalanceDeposit;
        private Label lblTOTAL_DEPOSIT_AMOUNT;
        private Label lblTOTAL_DUE_AMOUNT;
        private ExButton btnREPORT_DEPOSIT_AND_CONNECTION_FEE;
        private ExButton btnREACTIVATE;
        private ExButton btnREPORT_CUSTOMER_AR;
        private Label lblDATE;
        private Label lblCONNECTION;
        private Panel panel1;
        private Panel panel2;
        private Panel panel3;
        private Panel panel5;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn CUSTOMER_SERVICE_ID;
        private DataGridViewTextBoxColumn SERVICE;
        private DataGridViewTextBoxColumn QTY;
        private DataGridViewTextBoxColumn PRICE;
        private DataGridViewTextBoxColumn LAST_BILLING_MONTH;
        private DataGridViewTextBoxColumn RECURRING_MONTH;
        private DataGridViewTextBoxColumn NEXT_BILLING_MONTH;
        private Label lblREQUEST_CONNECTION_DATE;
        private DateTimePicker dtpREQUEST_CONNECTION_DATE;
        private TabPage tabPREPAYMENT;
        private DataGridView dgvPrepayment;
        private DataGridView dgvBalancePrepayment;
        private Label label1;
        private ExLinkLabel btnADJUST_DEPOSIT;
        private ExLinkLabel btnREFUND_DEPOSIT;
        private Panel panel6;
        private ExLinkLabel btnSETTLE_INVOICE;
        private ExLinkLabel btnAPPLY_PREPAYMENT;
        private ExLinkLabel btnRETURN_PREPAYMENT;
        private ExLinkLabel lblCHANGE_AMPARE;
        private ComboBox cboConnectionType;
        private ComboBox cboCustomerGroup;
        private ContextMenuStrip cmsStrip;
        private ToolStripMenuItem ChangeConnectionType;
        private ToolStripMenuItem ChangeBasePrice;
        private ExButton btnRE_ACTIVE;
        private DataGridViewTextBoxColumn CUS_EQUIPMENT_ID;
        private DataGridViewTextBoxColumn EQUIPMENT_ID;
        private DataGridViewTextBoxColumn EQUIPMENT_NAME;
        private DataGridViewTextBoxColumn QUANTITY;
        private DataGridViewTextBoxColumn WATT;
        private DataGridViewTextBoxColumn TOTAL_WATT;
        private DataGridViewTextBoxColumn NOTE;
        private DataGridViewCheckBoxColumn _DB;
        private DataGridViewCheckBoxColumn _EDITED;
        private CheckBox chkIsmarketVendor;
        private ExLinkLabel btnPRINT_2;
        private DataGridViewTextBoxColumn CUS_DEPOSIT_ID;
        private DataGridViewTextBoxColumn DEPOSIT_NO;
        private DataGridViewTextBoxColumn CREATE_ON;
        private DataGridViewTextBoxColumn CREATE_BY;
        private DataGridViewTextBoxColumn DEPOSIT_ACTION_NAME;
        private DataGridViewTextBoxColumn AMOUNT;
        private DataGridViewTextBoxColumn BALANCE;
        private DataGridViewTextBoxColumn D_CURRENCY_SING_;
        private DataGridViewCheckBoxColumn IS_PAID;
        private ComboBox cboTitles;
        private Label lblTITLES;
        private Label label3;
        private ComboBox cboIdCardType;
        private Label lblID_CARD_NUMBER_TYPE;
        private ComboBox cboCustomerType;
        private Label lblTYPE_OF_CUSTOMERS;
        private Label label2;
        private Label label5;
        private Label label4;
        private DataGridViewTextBoxColumn INVOICE_ID;
        private DataGridViewTextBoxColumn INVOICE_STATUS;
        private DataGridViewTextBoxColumn CURRENCY_ID;
        private DataGridViewTextBoxColumn INVOICE_DATE;
        private DataGridViewTextBoxColumn INVOICE_NO;
        private DataGridViewTextBoxColumn INVOICE_TITLE;
        private DataGridViewTextBoxColumn SETTLE_AMOUNT;
        private DataGridViewTextBoxColumn PAID_AMOUNT;
        private DataGridViewTextBoxColumn DUE_AMOUNT;
        private DataGridViewTextBoxColumn CURRENCY_SING_;
        private DataGridViewTextBoxColumn DUE_DATE;
        private DataGridViewTextBoxColumn DUE_DAY;
        private DataGridViewTextBoxColumn IS_SERVICE_BILL;
        private DataGridViewTextBoxColumn FORMAT_INVOICE;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private DataGridViewTextBoxColumn PREPAYMENT_BALANCE;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private DataGridViewTextBoxColumn FORMAT_BALANCE_PREPAYMENT;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private DataGridViewTextBoxColumn INVOICE_BALANCE;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private DataGridViewTextBoxColumn FORMAT_BALNCE_INVOICE;
        private ComboBox cboCurrency;
        private TabPage tabAttachment;
        private ExTextbox txtSearchAttachment;
        private ExLinkLabel btnREMOVE_ATTACHMENT;
        private ExLinkLabel btnADDNEW_ATTCHMENT;
        private ExLinkLabel btnVIEW_ATTACHMENT;
        private ExLinkLabel btnADJUST_ATTACHMENT;
        private DataGridView dgvAttachment;
        private TextBox txtPOWER_KW;
        private Label lblPOWER_KW;
        private DataGridViewTextBoxColumn CUSTOMER_ATTACHMENT_ID;
        private DataGridViewLinkColumn ATTACHMENT_NAME;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewLinkColumn DOWNLOAD_ATTACHMENT;
        private DataGridViewTextBoxColumn FILE_NAME_TYPE;
        private DataGridViewTextBoxColumn SIZE;
        private DataGridViewTextBoxColumn CREATED_ON;
        private DataGridViewTextBoxColumn CREATED_BY;
        private DataGridViewTextBoxColumn IS_ACTIVE;
        private TextBox txtPOWER_CAPACITY;
        private CheckBox chkCAPACITY_CHARGE;
        private Label label6;
        private DataGridViewTextBoxColumn CUS_PREPAYMENT_ID;
        private DataGridViewTextBoxColumn PAYMENT_ID;
        private DataGridViewTextBoxColumn PREPAYMENT_NO;
        private DataGridViewTextBoxColumn PREPAYMENT_DATE;
        private DataGridViewTextBoxColumn PREPAYMENT_BY;
        private DataGridViewTextBoxColumn AMOUNT_PREPAYMENT;
        private DataGridViewTextBoxColumn BALANCE_PREPAYMENT;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private DataGridViewTextBoxColumn CURRENCY_ID_P;
        private DataGridViewTextBoxColumn PREPAYMENT_NOTE;
        private DataGridViewTextBoxColumn FORMAT;
        private DataGridViewTextBoxColumn colACTION;
        private ExLinkLabel btnDELETE;
    }
}