﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogCustomerService
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCustomerService));
            this.lblQTY = new System.Windows.Forms.Label();
            this.txtQTY = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.label9 = new System.Windows.Forms.Label();
            this.btnChange_log = new SoftTech.Component.ExButton();
            this.label1 = new System.Windows.Forms.Label();
            this.lblSERVICE = new System.Windows.Forms.Label();
            this.cboService = new System.Windows.Forms.ComboBox();
            this.lblSTART_MONTH = new System.Windows.Forms.Label();
            this.dtpACTIVATE_MONTH = new System.Windows.Forms.DateTimePicker();
            this.lblLAST_BILLING_MONTH = new System.Windows.Forms.Label();
            this.lblNEXT_BILLING_MONTH = new System.Windows.Forms.Label();
            this.dtpLAST_BILLING_MONTH = new System.Windows.Forms.DateTimePicker();
            this.dtpNEXT_BILLING_MONTH = new System.Windows.Forms.DateTimePicker();
            this.txtRECURRING_MONTH = new System.Windows.Forms.TextBox();
            this.lblRECURRING_MONTH = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.txtRECURRING_MONTH);
            this.content.Controls.Add(this.lblRECURRING_MONTH);
            this.content.Controls.Add(this.dtpNEXT_BILLING_MONTH);
            this.content.Controls.Add(this.dtpLAST_BILLING_MONTH);
            this.content.Controls.Add(this.lblNEXT_BILLING_MONTH);
            this.content.Controls.Add(this.lblLAST_BILLING_MONTH);
            this.content.Controls.Add(this.dtpACTIVATE_MONTH);
            this.content.Controls.Add(this.lblSTART_MONTH);
            this.content.Controls.Add(this.cboService);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.lblSERVICE);
            this.content.Controls.Add(this.btnChange_log);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.txtQTY);
            this.content.Controls.Add(this.lblQTY);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblQTY, 0);
            this.content.Controls.SetChildIndex(this.txtQTY, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.btnChange_log, 0);
            this.content.Controls.SetChildIndex(this.lblSERVICE, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.cboService, 0);
            this.content.Controls.SetChildIndex(this.lblSTART_MONTH, 0);
            this.content.Controls.SetChildIndex(this.dtpACTIVATE_MONTH, 0);
            this.content.Controls.SetChildIndex(this.lblLAST_BILLING_MONTH, 0);
            this.content.Controls.SetChildIndex(this.lblNEXT_BILLING_MONTH, 0);
            this.content.Controls.SetChildIndex(this.dtpLAST_BILLING_MONTH, 0);
            this.content.Controls.SetChildIndex(this.dtpNEXT_BILLING_MONTH, 0);
            this.content.Controls.SetChildIndex(this.lblRECURRING_MONTH, 0);
            this.content.Controls.SetChildIndex(this.txtRECURRING_MONTH, 0);
            // 
            // lblQTY
            // 
            resources.ApplyResources(this.lblQTY, "lblQTY");
            this.lblQTY.Name = "lblQTY";
            // 
            // txtQTY
            // 
            resources.ApplyResources(this.txtQTY, "txtQTY");
            this.txtQTY.Name = "txtQTY";
            this.txtQTY.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // btnChange_log
            // 
            resources.ApplyResources(this.btnChange_log, "btnChange_log");
            this.btnChange_log.Name = "btnChange_log";
            this.btnChange_log.UseVisualStyleBackColor = true;
            this.btnChange_log.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // lblSERVICE
            // 
            resources.ApplyResources(this.lblSERVICE, "lblSERVICE");
            this.lblSERVICE.Name = "lblSERVICE";
            // 
            // cboService
            // 
            this.cboService.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboService.FormattingEnabled = true;
            resources.ApplyResources(this.cboService, "cboService");
            this.cboService.Name = "cboService";
            this.cboService.SelectedIndexChanged += new System.EventHandler(this.cboService_SelectedIndexChanged);
            // 
            // lblSTART_MONTH
            // 
            resources.ApplyResources(this.lblSTART_MONTH, "lblSTART_MONTH");
            this.lblSTART_MONTH.Name = "lblSTART_MONTH";
            // 
            // dtpACTIVATE_MONTH
            // 
            resources.ApplyResources(this.dtpACTIVATE_MONTH, "dtpACTIVATE_MONTH");
            this.dtpACTIVATE_MONTH.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpACTIVATE_MONTH.Name = "dtpACTIVATE_MONTH";
            this.dtpACTIVATE_MONTH.ValueChanged += new System.EventHandler(this.dtpACTIVATE_MONTH_ValueChanged);
            // 
            // lblLAST_BILLING_MONTH
            // 
            resources.ApplyResources(this.lblLAST_BILLING_MONTH, "lblLAST_BILLING_MONTH");
            this.lblLAST_BILLING_MONTH.Name = "lblLAST_BILLING_MONTH";
            // 
            // lblNEXT_BILLING_MONTH
            // 
            resources.ApplyResources(this.lblNEXT_BILLING_MONTH, "lblNEXT_BILLING_MONTH");
            this.lblNEXT_BILLING_MONTH.Name = "lblNEXT_BILLING_MONTH";
            // 
            // dtpLAST_BILLING_MONTH
            // 
            resources.ApplyResources(this.dtpLAST_BILLING_MONTH, "dtpLAST_BILLING_MONTH");
            this.dtpLAST_BILLING_MONTH.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpLAST_BILLING_MONTH.Name = "dtpLAST_BILLING_MONTH";
            // 
            // dtpNEXT_BILLING_MONTH
            // 
            resources.ApplyResources(this.dtpNEXT_BILLING_MONTH, "dtpNEXT_BILLING_MONTH");
            this.dtpNEXT_BILLING_MONTH.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpNEXT_BILLING_MONTH.Name = "dtpNEXT_BILLING_MONTH";
            // 
            // txtRECURRING_MONTH
            // 
            resources.ApplyResources(this.txtRECURRING_MONTH, "txtRECURRING_MONTH");
            this.txtRECURRING_MONTH.Name = "txtRECURRING_MONTH";
            // 
            // lblRECURRING_MONTH
            // 
            resources.ApplyResources(this.lblRECURRING_MONTH, "lblRECURRING_MONTH");
            this.lblRECURRING_MONTH.Name = "lblRECURRING_MONTH";
            // 
            // DialogCustomerService
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogCustomerService";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBox txtQTY;
        private Label lblQTY;
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label label9;
        private ExButton btnChange_log;
        private DateTimePicker dtpACTIVATE_MONTH;
        private Label lblSTART_MONTH;
        private ComboBox cboService;
        private Label label1;
        private Label lblSERVICE;
        private DateTimePicker dtpNEXT_BILLING_MONTH;
        private DateTimePicker dtpLAST_BILLING_MONTH;
        private Label lblNEXT_BILLING_MONTH;
        private Label lblLAST_BILLING_MONTH;
        private TextBox txtRECURRING_MONTH;
        private Label lblRECURRING_MONTH;
    }
}