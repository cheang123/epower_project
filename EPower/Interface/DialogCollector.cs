﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using SoftTech.Security.Logic;

namespace EPower.Interface
{
    public partial class DialogCollector : ExDialog
    {
        GeneralProcess _flag;
        EmpPosition _pFlag;
        TBL_EMPLOYEE _objEmp = new TBL_EMPLOYEE();
        public TBL_EMPLOYEE Employee
        {
            get { return _objEmp; }
        }
        TBL_EMPLOYEE _oldEmp = new TBL_EMPLOYEE();

        private string passwordDisplay = "123456789abcdef";

        #region Contructor       
        public DialogCollector(GeneralProcess flag, TBL_EMPLOYEE objEmp)
        {
            InitializeComponent();
            _flag = flag;
            tapComponent.TabPages.Remove(tabDEVICE);
            

            objEmp._CopyTo(_objEmp);
            objEmp._CopyTo(_oldEmp);

            loadData();

            if (flag == GeneralProcess.Insert)
            {
                _objEmp.IS_ACTIVE = true;
                _objEmp.PASSWORD = "";
                this.Text = string.Concat(Resources.INSERT, this.Text);
            }
            else if (flag == GeneralProcess.Update)
            {
                this.Text = string.Concat(Resources.UPDATE, this.Text);
            }
            else if (flag == GeneralProcess.Delete)
            {
                this.Text = string.Concat(Resources.DELETE, this.Text);
                UIHelper.SetEnabled(this, false);                
            }
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
            read();

        }

        public  DialogCollector (TBL_EMPLOYEE objEmp,EmpPosition pFlag)
        {
            InitializeComponent();
            _pFlag = pFlag;
            _flag = GeneralProcess.Insert;
            tapComponent.TabPages.Remove(tabDEVICE);            

            objEmp._CopyTo(_objEmp);
            objEmp._CopyTo(_oldEmp);

            loadData();
             _objEmp.IS_ACTIVE = true;
             _objEmp.PASSWORD = "";
             this.Text = string.Concat(Resources.INSERT, this.Text);
             this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
             read();
             //dgvPosition.Enabled = false;
        }

        #endregion

        #region Method
        private void loadData()
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                //Get position.
                dgvPosition.DataSource = (from p in DBDataContext.Db.TBL_POSITIONs
                                          where p.IS_ACTIVE
                                          select new
                                          {
                                              p.POSITION_ID,
                                              p.POSITION_NAME,
                                              p.IS_USE_DEVICE,
                                              SELECT_POSITION = false
                                          })._ToDataTable();

                LoadDevice();
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }


            this.Cursor = Cursors.Default;
        }

        /// <summary>
        /// Load device every add tap.
        /// </summary>
        private void LoadDevice()
        {
            //Get device.
            dgvDevice.DataSource = (from d in DBDataContext.Db.TBL_DEVICEs
                                    join s in DBDataContext.Db.TLKP_DEVICE_STATUS on d.STATUS_ID equals s.STATUS_ID
                                    where d.STATUS_ID != (int)DeviceStatus.Unavailable
                                    select new
                                    {
                                        d.DEVICE_ID,
                                        d.DEVICE_CODE,
                                        d.DEVICE_OEM_INFO,
                                        SELECT_DEVICE = false
                                    })._ToDataTable();
        }

        private void read()
        {
            try
            {
                txtEmpName.Text = _objEmp.EMPLOYEE_NAME;
                txtPassword.Text = passwordDisplay;

               

                var empPosition = from ep in DBDataContext.Db.TBL_EMPLOYEE_POSITIONs
                                  join p in DBDataContext.Db.TBL_POSITIONs
                                  on ep.POSITION_ID equals p.POSITION_ID
                                  where ep.EMPLOYEE_ID == _objEmp.EMPLOYEE_ID && p.IS_ACTIVE

                                  select new
                                  {
                                      ep.EMPLOYEE_POSITION_ID,
                                      ep.EMPLOYEE_ID,
                                      ep.POSITION_ID,
                                  };
                if (_pFlag!=null)
                {
                    foreach (DataGridViewRow item in dgvPosition.Rows)
                    {
                        if ((int)item.Cells[this.POSITION_ID.Name].Value == (int)_pFlag)
                        {
                            item.Cells[this.SELECT_POSITION.Name].Value = true;
                            break;
                        }
                    }
                }

                foreach (var item in empPosition)
                {
                    foreach (DataGridViewRow row in dgvPosition.Rows)
                    {
                        if ((int)row.Cells[this.POSITION_ID.Name].Value == item.POSITION_ID)
                        {
                            row.Cells[this.SELECT_POSITION.Name].Value = true;
                            break;
                        }
                    }
                }

                CheckEnableDevice();

                var empDevice = from dv in DBDataContext.Db.TBL_EMPLOYEE_DEVICEs
                                join d in DBDataContext.Db.TBL_DEVICEs
                                on dv.DEVICE_ID equals d.DEVICE_ID
                                where dv.EMPLOYEE_ID == _objEmp.EMPLOYEE_ID &&
                                d.STATUS_ID==(int)DeviceStatus.Used
                                select new
                                {
                                    dv.EMPLOYEE_DEVICE_ID,
                                    dv.EMPLOYEE_ID,
                                    dv.DEVICE_ID
                                };

                if (empDevice.Count()>0)
                {
                    foreach (var item in empDevice)
                    {
                        foreach (DataGridViewRow row in dgvDevice.Rows)
                        {
                            if ((int)row.Cells[this.DEVICE_ID.Name].Value == item.DEVICE_ID)
                            {
                                row.Cells[this.SELECT_DEVICE_.Name].Value = true;
                                break;
                            }
                        }
                    }    
                }
                
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void write()
        {
            _objEmp.EMPLOYEE_NAME = txtEmpName.Text.Trim();

            if (this.txtPassword.Text != passwordDisplay && txtPassword.Text.Trim() != "")
            {
                // get real password.
                _objEmp.PASSWORD= txtPassword.Text;
                // encrypt password here.
                _objEmp.PASSWORD = Login.GetEncrypt( _objEmp.PASSWORD.ToUpper());
            }
        }

        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();

            if (txtEmpName.Text.Trim() == string.Empty)
            {
                txtEmpName.SetValidation(string.Format(Resources.REQUIRED, lblEMPLOYEE.Text));
                val = true;
            }
            if (tapComponent.Contains(tabDEVICE)&&txtPassword.Text.Trim()==string.Empty)
            {
                txtPassword.SetValidation(string.Format(Resources.REQUIRED, lblPASSWORD.Text));
                val = true;
            }

            if ( _objEmp.PASSWORD=="" && txtPassword.Text.ToLower().Contains(passwordDisplay) &&  txtPassword.Enabled)
            {
                txtPassword.SetValidation(string.Format(Resources.REQUIRED, lblPASSWORD.Text));
                val = true;
            }
            return val;
        } 

        private void ModifiesDevice(TBL_CHANGE_LOG objChangParent)
        {
            //Delete all record of device by employee.
            IEnumerable<TBL_EMPLOYEE_DEVICE> empDev = from ed in DBDataContext.Db.TBL_EMPLOYEE_DEVICEs
                                                      join d in DBDataContext.Db.TBL_DEVICEs
                                                      on ed.DEVICE_ID equals d.DEVICE_ID

                                                      where ed.EMPLOYEE_ID == _objEmp.EMPLOYEE_ID &&
                                                      d.STATUS_ID != (int)DeviceStatus.Unavailable
                                                      select ed;

            foreach (TBL_EMPLOYEE_DEVICE item in empDev)
            {
                DBDataContext.Db.Delete(item);
            }

            

            //Insert new device to employee.
            if (tapComponent.TabPages.Contains(tabDEVICE))
            {
                foreach (DataGridViewRow item in dgvDevice.Rows)
                {
                    if ((bool)item.Cells[this.SELECT_DEVICE_.Name].Value)
                    {
                        int intDeviceID=(int)item.Cells[this.DEVICE_ID.Name].Value;

                        //Query device by status equal stock.
                        TBL_DEVICE objDeviceUpdataStatus=DBDataContext.Db.TBL_DEVICEs.FirstOrDefault
                            (x=>x.DEVICE_ID==intDeviceID && x.STATUS_ID==(int)DeviceStatus.Stock);
                        //change device status to used.
                        if (objDeviceUpdataStatus!=null)
                        {
                            TBL_DEVICE objTempDevice=new TBL_DEVICE();
                            objDeviceUpdataStatus._CopyTo(objTempDevice);
                            objDeviceUpdataStatus.STATUS_ID=(int)DeviceStatus.Used;
                            DBDataContext.Db.Update(objTempDevice, objDeviceUpdataStatus);
                        }                        

                        DBDataContext.Db.InsertChild(new TBL_EMPLOYEE_DEVICE()
                        {
                            EMPLOYEE_ID = _objEmp.EMPLOYEE_ID,
                            DEVICE_ID = intDeviceID
                        },_objEmp,ref objChangParent);
                    }
                }    
            }            
        }

        private void ModifiesPosition(TBL_CHANGE_LOG objChangParent)
        {
            //Delete all record of position by employee.
            IEnumerable<TBL_EMPLOYEE_POSITION> empPos = from ep in DBDataContext.Db.TBL_EMPLOYEE_POSITIONs
                                                        join p in DBDataContext.Db.TBL_POSITIONs
                                                        on ep.POSITION_ID equals p.POSITION_ID
                                                        where p.IS_ACTIVE && ep.EMPLOYEE_ID == _objEmp.EMPLOYEE_ID
                                                        select ep;

            foreach (TBL_EMPLOYEE_POSITION item in empPos)
            {
                DBDataContext.Db.Delete(item);
            }

            //Insert new position to employee.
            foreach (DataGridViewRow item in dgvPosition.Rows)
            {
                if ((bool)item.Cells[this.SELECT_POSITION.Name].Value)
                {
                    DBDataContext.Db.InsertChild(new TBL_EMPLOYEE_POSITION()
                    {
                        EMPLOYEE_ID = _objEmp.EMPLOYEE_ID,
                        POSITION_ID = (int)item.Cells[this.POSITION_ID.Name].Value
                    },_objEmp,ref objChangParent);
                }
            }
        }

        /// <summary>
        /// Check enable tab device.
        /// </summary>
        private void CheckEnableDevice()
        {
            bool deviceEnable = false;
            foreach (DataGridViewRow item in dgvPosition.Rows)
            {
                if ((bool)item.Cells[this.SELECT_POSITION.Name].Value && (bool)item.Cells[this.IS_USE_DEVICE.Name].Value)
                {
                    deviceEnable = true;
                    break;
                }
            }
            if (deviceEnable)
            {
                if (!tapComponent.TabPages.Contains(tabDEVICE) )
                {
                    tapComponent.TabPages.Add(tabDEVICE);
                    txtPassword.Enabled = (true && _flag != GeneralProcess.Delete);
                }
            }
            else
            {
                tapComponent.TabPages.Remove(tabDEVICE);
                txtPassword.Enabled = false;
            }
        }
        #endregion

        #region Operation
        private void btnAdd_Click(object sender, EventArgs e)
        {            
            if (inValid())
            {
                return;
            }

            write();

            txtEmpName.ClearValidation();

            if (DBDataContext.Db.IsExits(_objEmp, "EMPLOYEE_NAME"))
            {
                txtEmpName.SetValidation(string.Format(Resources.MS_IS_EXISTS, lblEMPLOYEE.Text));
                return;
            }

            if (DBDataContext.Db.IsExits(_objEmp,"PASSWORD")&&tapComponent.Contains(tabDEVICE)&&!txtPassword.Text.ToLower().Contains(passwordDisplay.ToLower()))
            {
                MsgBox.ShowInformation(Resources.MS_INVALID_PASSWORD);
                return;                
            }

            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    TBL_CHANGE_LOG objChangParent = null;

                    if (_flag == GeneralProcess.Insert)
                    {
                        objChangParent= DBDataContext.Db.Insert(_objEmp);
                        ModifiesPosition(objChangParent);
                        ModifiesDevice(objChangParent);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        objChangParent= DBDataContext.Db.Update(_oldEmp, _objEmp);
                        ModifiesPosition(objChangParent);
                        ModifiesDevice(objChangParent);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objEmp);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        /// <summary>
        /// Change current keyboard layout to khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void dgvPosition_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && dgvPosition.SelectedRows.Count > 0)
            {                
                dgvPosition[this.SELECT_POSITION.Name, e.RowIndex].Value = !(bool)dgvPosition[this.SELECT_POSITION.Name, e.RowIndex].Value;
                CheckEnableDevice();
            }
        }

        

        private void dgvDevice_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && dgvDevice.SelectedRows.Count > 0)
            {
                dgvDevice[this.SELECT_DEVICE_.Name, e.RowIndex].Value = !(bool)dgvDevice[this.SELECT_DEVICE_.Name, e.RowIndex].Value;
            }
        }
        #endregion

        private void tapComponent_ControlAdded(object sender, ControlEventArgs e)
        {
            LoadDevice();
        }

        private void txtPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputNumberOnly(sender, e);
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objEmp);
        }
    }
}