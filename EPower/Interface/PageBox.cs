﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using EPower.Properties;

namespace EPower.Interface
{
    public partial class PageBox : Form
    {        
        public TBL_BOX Box
        {
            get
            {
                TBL_BOX objBox = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    try
                    {
                        int boxID = (int)dgv.SelectedRows[0].Cells["BOX_ID"].Value;
                        objBox = DBDataContext.Db.TBL_BOXes.FirstOrDefault(x => x.BOX_ID == boxID);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objBox;
            }
        }

        #region Constructor
        public PageBox()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            txt_QuickSearch(null, null);
        }
        #endregion

        #region Operation
         

        /// <summary>
        /// Load data from database and display
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            try
            {
                dgv.DataSource = from b in DBDataContext.Db.TBL_BOXes
                                 where b.STATUS_ID!=(int)BoxStatus.Unavailable 
                                 && b.BOX_CODE.ToLower().Contains(txtQuickSearch.Text.ToLower().Trim())
                                 select b;
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }            
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        #endregion

        #region Method
        private bool IsDeletable()
        {
            //TODO: can delte.
            bool val = false;
            if (dgv.SelectedRows.Count > 0)
            {
                val = true;
            }
            return val;
        }

        #endregion

        private void btnNew_Click(object sender, EventArgs e)
        {
            MsgBox.ShowInformation("sdddddddddddddddddddddddddddddddddddddddddddf;asdlkfjsdflkjsldfjak;ssssssssssssssssssss");
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {

        }

        private void btnEdit_Click(object sender, EventArgs e)
        {

        }

        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
