﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageFormulaCalculation : Form
    {
        bool _load = false;
       
        #region Constructor
        public PageFormulaCalculation()
        {
            InitializeComponent();
            _load = false;
            cboMVTransfomerType.SelectedIndex = 0;
            UIHelper.SetDataSourceToComboBox(cboPhase, Lookup.GetPowerPhases());
            _load = true;
        }

        #endregion

        private void calculateAmpareTransfo()
        {
            if (DataHelper.ParseToInt(cboPhase.Text) == 1)
            {
                lblMultiplier_.Text = "4.5";
                txtResultAmpareTransformer.Text = (DataHelper.ParseToDouble(txtTransfoCapacity.Text) * 4.5).ToString("N0")+" A";
            }
            else if (DataHelper.ParseToInt(cboPhase.Text) == 3)
            {
                lblMultiplier_.Text = "1.5";
                txtResultAmpareTransformer.Text = (DataHelper.ParseToDouble(txtTransfoCapacity.Text) * 1.5).ToString("N0")+" A 1P";
            }
        }

        private void cboMVKVA_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_load) return;
            int transfoType = cboMVTransfomerType.SelectedIndex == 0 ? 22 : 35; 
            double result = DataHelper.ParseToDouble(txtMVCapacity.Text) / (Math.Sqrt(3) * transfoType); 
            txtMVResult.Text = result.ToString("N2");
            TBL_RATIO_CT_VT obj = DBDataContext.Db.TBL_RATIO_CT_VTs.OrderBy(x => x.RATIO).FirstOrDefault(x => x.RATIO >= result && x.RATIO_TYPE_ID == (int)RationType.MV);
            if (obj != null)
            {
                txtMVCT_VT.Text = obj.RATIO.ToString("N0") + "/5";
            }
            else
            {
                TBL_RATIO_CT_VT objMax = DBDataContext.Db.TBL_RATIO_CT_VTs.OrderByDescending(x => x.RATIO).FirstOrDefault(x => x.RATIO_TYPE_ID == (int)RationType.MV);
                txtMVCT_VT.Text = string.Format(Resources.MAX_RATIO_CT_VT, objMax.RATIO.ToString("N0") + "/5");
            }
        }

        private void cboLVKVA_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_load) return; 
            int data = 400;
            double result = (DataHelper.ParseToDouble(txtLVCapacity.Text) * 1000) / DataHelper.ParseToDouble((Math.Sqrt(3) * data).ToString());
            txtLVResult.Text = result.ToString("N2");
            TBL_RATIO_CT_VT obj = DBDataContext.Db.TBL_RATIO_CT_VTs.OrderBy(x => x.RATIO).FirstOrDefault(x => x.RATIO >= result && x.RATIO_TYPE_ID == (int)RationType.LV);
            if (obj != null)
            {
                txtLVCT_VT.Text = obj.RATIO.ToString("N0") + "/5";
            }
            else
            {
                TBL_RATIO_CT_VT objMax = DBDataContext.Db.TBL_RATIO_CT_VTs.OrderByDescending(x => x.RATIO).FirstOrDefault(x => x.RATIO_TYPE_ID == (int)RationType.LV);
                txtLVCT_VT.Text = string.Format(Resources.MAX_RATIO_CT_VT, objMax.RATIO.ToString("N0")+"/5");
            }
        }

        private void txtMVCapacity_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void txtMVCapacity_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputNumberOnly(sender, e);
            if (e.KeyChar == 13)
            {
                cboMVKVA_SelectedIndexChanged(null, null);
            }
        }

        private void txtLVCapacity_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputNumberOnly(sender, e);
            if (e.KeyChar == 13)
            {
                cboLVKVA_SelectedIndexChanged(null, null);
            }
        }

        private void txtTransfoCapacity_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputNumberOnly(sender, e);
            if (e.KeyChar == 13)
            {
                calculateAmpareTransfo();
            }
        }

        private void cboPhase_SelectedIndexChanged(object sender, EventArgs e)
        {
            calculateAmpareTransfo();
        }

        
        
    }
}
