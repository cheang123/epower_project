﻿using EPower.Base.Helper;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogCustomerType : ExDialog
    {
        GeneralProcess _flag;        
        TBL_CUSTOMER_TYPE _objCustomerType = new TBL_CUSTOMER_TYPE();
        public TBL_CUSTOMER_TYPE CustomerType
        {
            get { return _objCustomerType; }
        }
        TBL_CUSTOMER_TYPE _oldCustomerType = new TBL_CUSTOMER_TYPE();

        #region Constructor
        public DialogCustomerType(GeneralProcess flag, TBL_CUSTOMER_TYPE objCustomerType)
        {
            InitializeComponent();

            _flag = flag;
            objCustomerType._CopyTo(_objCustomerType);
            objCustomerType._CopyTo(_oldCustomerType);
            if (flag == GeneralProcess.Insert)
            {
                _objCustomerType.IS_ACTIVE = true;
                this.Text = string.Concat(Resources.INSERT, this.Text);
            }
            else if (flag == GeneralProcess.Update)
            {
                this.Text = string.Concat(Resources.UPDATE, this.Text);
            }
            else if (flag == GeneralProcess.Delete)
            {
                this.Text = string.Concat(Resources.DELETE, this.Text);
                UIHelper.SetEnabled(this, false);
            }
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
            UIHelper.SetDataSourceToComboBox(cboIncomeAccount, AccountChartHelper.GetAccountsTable(AccountConfig.CUSTOMER_TYPE_INCOME_ACCOUNTS));
            read();            
        }

            

        #endregion

        #region Operation
        /// <summary>
        /// Change Keyboard to Khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InputKhmer(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        private void InputEnglish(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        } 
        /// <summary>
        /// Data transaction.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }

            write();

             //If record is duplicate.
            if (DBDataContext.Db.IsExits(_objCustomerType, "CUSTOMER_TYPE_NAME"))
            {
                MsgBox.ShowInformation(string.Format( Resources.MS_IS_EXISTS,lblCUSTOMER_TYPE.Text), Resources.INFORMATION);
                return;
            }

            try
            {
                using (TransactionScope tran=new TransactionScope( TransactionScopeOption.Required,TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objCustomerType);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_oldCustomerType, _objCustomerType);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objCustomerType);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }                                               
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }            
        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
        #endregion

        #region Method
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool inValid()
        {
            bool val = false;

            this.ClearAllValidation();            

            if (this.txtCustomerTypeName.Text.Trim()==string.Empty)
            {
                this.txtCustomerTypeName.SetValidation(string.Format(Resources.REQUIRED, this.lblCUSTOMER_TYPE.Text));
                val = true;
            }

            if (!DataHelper.IsNumber(this.txtDepositUsage.Text))
            {
                this.txtDepositUsage.SetValidation(string.Format(Resources.REQUIRED, this.lblDEPOSIT_USAGE.Text));
                val = true;
            }
            if (!DataHelper.IsNumber(txtDailySupplyHour.Text))
            {
                this.txtDailySupplyHour.SetValidation(string.Format(Resources.REQUIRED, this.lblDAILY_SUPPLY_HOUR.Text));
                val = true;
            }

            return val;
        }

        /// <summary>
        /// Read data from object.
        /// </summary>
        private void read()
        {            
            txtCustomerTypeName.Text = _objCustomerType.CUSTOMER_TYPE_NAME;
            txtDepositUsage.Text = _objCustomerType.DEPOSIT_USAGE.ToString("#");
            txtDailySupplyHour.Text = _objCustomerType.DAILY_SUPPLY_HOUR;
            txtDailySupplySchedule.Text = _objCustomerType.DAILY_SUPPLY_SCHEDULE;
            cboIncomeAccount.SelectedValue = _objCustomerType.INCOME_ACCOUNT_ID;
            txtNote.Text = _objCustomerType.NOTE;
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            _objCustomerType.CUSTOMER_TYPE_NAME = txtCustomerTypeName.Text.Trim();
            _objCustomerType.DEPOSIT_USAGE = DataHelper.ParseToInt(this.txtDepositUsage.Text);
            _objCustomerType.DAILY_SUPPLY_HOUR = this.txtDailySupplyHour.Text;
            _objCustomerType.DAILY_SUPPLY_SCHEDULE=txtDailySupplySchedule.Text;
            _objCustomerType.INCOME_ACCOUNT_ID = (int) cboIncomeAccount.SelectedValue;
            _objCustomerType.NOTE = txtNote.Text;
        }
        #endregion

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objCustomerType);
        }
    }
}
