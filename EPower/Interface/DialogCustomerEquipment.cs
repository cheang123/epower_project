﻿using System;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Logic;

namespace EPower.Interface
{
    public partial class DialogCustomerEquipment : ExDialog
    {
        GeneralProcess _flag = GeneralProcess.Insert;
        TBL_CUSTOMER_EQUIPMENT _obj = new TBL_CUSTOMER_EQUIPMENT();

        public DialogCustomerEquipment(GeneralProcess flag,TBL_CUSTOMER_EQUIPMENT obj)
        {
            InitializeComponent();
            btnAddEquipment.Enabled = Login.IsAuthorized(Permission.ADMIN_EQIPMENT);

            
            this.Text = this._flag.GetText(this.Text);

            this._obj = obj;

            this.bind();

            this.read();
        }

        public TBL_CUSTOMER_EQUIPMENT Object
        {
            get
            {
                return this._obj;
            }
        }

        private void bind()
        {
            UIHelper.SetDataSourceToComboBox(cboEquitment, DBDataContext.Db.TBL_EQUIPMENTs.Where(row => row.IS_ACTIVE));
            this.cboEquitment.SelectedIndex = -1;
        }

        private void read()
        {
            this.cboEquitment.SelectedValue = this._obj.EQUIPMENT_ID;
            this.txtQuantity.Text = this._obj.QUANTITY.ToString();
            this.txtPower.Text = this._obj.POWER.ToString(UIHelper._DefaultUsageFormat);
            this.txtTotalPower.Text = this._obj.POWER.ToString(UIHelper._DefaultUsageFormat);
            this.txtComment.Text = this._obj.COMMENT;
        }

        private void write()
        {
            this._obj.EQUIPMENT_ID = (int)this.cboEquitment.SelectedValue;
            this._obj.QUANTITY = DataHelper.ParseToDecimal(this.txtQuantity.Text);
            this._obj.POWER = DataHelper.ParseToDecimal(this.txtPower.Text);
            this._obj.TOTAL_POWER = DataHelper.ParseToDecimal(this.txtTotalPower.Text);
            this._obj.COMMENT = this.txtComment.Text;
        }

        private bool invalid()
        {
            bool result = false; 
            
            this.ClearAllValidation();

            if (this.cboEquitment.SelectedIndex == -1)
            {
                this.cboEquitment.SetValidation(string.Format(Resources.REQUIRED, this.lblEQUIPMENT.Text));
                result = true;
            }

            if (DataHelper.ParseToDecimal(this.txtQuantity.Text)<=0)
            {
                this.txtQuantity.SetValidation(string.Format(Resources.REQUIRED, this.lblQUANTITY.Text));
                result = true;
            }
            if (DataHelper.ParseToDecimal(this.txtPower.Text) <= 0)
            {
                this.txtPower.SetValidation(string.Format(Resources.REQUIRED, this.lblWATT.Text));
                result = true;
            }
 
            return result;
        }

        private bool duplicate()
        {
            return false;
        }
 
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (invalid())
            {
                return;
            }

            if (duplicate())
            {
                MsgBox.ShowInformation(Resources.MS_IS_EXISTS);
                this.cboEquitment.Focus();
                return;
            }

            this.write();

            this.DialogResult = DialogResult.OK;

        }

        private void txtQuantity_KeyUp(object sender, KeyEventArgs e)
        {
            this.txtTotalPower.Text = (DataHelper.ParseToDecimal(this.txtQuantity.Text) * DataHelper.ParseToDecimal(this.txtPower.Text)).ToString(UIHelper._DefaultUsageFormat);
        }

        private void txtPower_KeyUp(object sender, KeyEventArgs e)
        {
            this.txtTotalPower.Text = (DataHelper.ParseToDecimal(this.txtQuantity.Text) * DataHelper.ParseToDecimal(this.txtPower.Text)).ToString(UIHelper._DefaultUsageFormat);
        }

        private void txtQuantity_Enter(object sender, EventArgs e)
        {
            if (DataHelper.ParseToDecimal(((Control)sender).Text) == 0)
            {
                 ((Control)sender).Text = "";
            }
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void txtComment_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void cboEquitment_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.txtQuantity.Focus();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtNumberOnly(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputNumberOnly(sender, e);
        }

        private void btnAddEquipment_AddItem(object sender, EventArgs e)
        {
            DialogEquipment dig = new DialogEquipment(GeneralProcess.Insert, new TBL_EQUIPMENT());
            if (dig.ShowDialog()== DialogResult.OK)
            {
                UIHelper.SetDataSourceToComboBox(cboEquitment, DBDataContext.Db.TBL_EQUIPMENTs.Where(row => row.IS_ACTIVE));
                cboEquitment.SelectedValue = dig.Equipment.EQUIPMENT_ID;
            }
        }
    }
}
