﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogCustomerChangeAmpare
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCustomerChangeAmpare));
            this.lblOLD_AMPARE = new System.Windows.Forms.Label();
            this.txtCUSTOMER_NAME = new System.Windows.Forms.TextBox();
            this.lblCUSTOMER_NAME = new System.Windows.Forms.Label();
            this.txtCUSTOMER_CODE = new System.Windows.Forms.TextBox();
            this.lblCUSTOMER_CODE = new System.Windows.Forms.Label();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblCHANGE_DATE = new System.Windows.Forms.Label();
            this.dtpCHANGE_DATE = new System.Windows.Forms.DateTimePicker();
            this.cboOldAmpare = new System.Windows.Forms.ComboBox();
            this.lblNEW_AMPARE = new System.Windows.Forms.Label();
            this.cboNewAmpare = new System.Windows.Forms.ComboBox();
            this.lblREASON = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.cboReason = new System.Windows.Forms.ComboBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.cboReason);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.label13);
            this.content.Controls.Add(this.lblREASON);
            this.content.Controls.Add(this.cboNewAmpare);
            this.content.Controls.Add(this.cboOldAmpare);
            this.content.Controls.Add(this.dtpCHANGE_DATE);
            this.content.Controls.Add(this.lblCHANGE_DATE);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.txtCUSTOMER_CODE);
            this.content.Controls.Add(this.lblCUSTOMER_CODE);
            this.content.Controls.Add(this.txtCUSTOMER_NAME);
            this.content.Controls.Add(this.lblCUSTOMER_NAME);
            this.content.Controls.Add(this.lblNEW_AMPARE);
            this.content.Controls.Add(this.lblOLD_AMPARE);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblOLD_AMPARE, 0);
            this.content.Controls.SetChildIndex(this.lblNEW_AMPARE, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_NAME, 0);
            this.content.Controls.SetChildIndex(this.txtCUSTOMER_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_CODE, 0);
            this.content.Controls.SetChildIndex(this.txtCUSTOMER_CODE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.lblCHANGE_DATE, 0);
            this.content.Controls.SetChildIndex(this.dtpCHANGE_DATE, 0);
            this.content.Controls.SetChildIndex(this.cboOldAmpare, 0);
            this.content.Controls.SetChildIndex(this.cboNewAmpare, 0);
            this.content.Controls.SetChildIndex(this.lblREASON, 0);
            this.content.Controls.SetChildIndex(this.label13, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.cboReason, 0);
            // 
            // lblOLD_AMPARE
            // 
            resources.ApplyResources(this.lblOLD_AMPARE, "lblOLD_AMPARE");
            this.lblOLD_AMPARE.Name = "lblOLD_AMPARE";
            // 
            // txtCUSTOMER_NAME
            // 
            resources.ApplyResources(this.txtCUSTOMER_NAME, "txtCUSTOMER_NAME");
            this.txtCUSTOMER_NAME.Name = "txtCUSTOMER_NAME";
            this.txtCUSTOMER_NAME.ReadOnly = true;
            this.txtCUSTOMER_NAME.TabStop = false;
            // 
            // lblCUSTOMER_NAME
            // 
            resources.ApplyResources(this.lblCUSTOMER_NAME, "lblCUSTOMER_NAME");
            this.lblCUSTOMER_NAME.Name = "lblCUSTOMER_NAME";
            // 
            // txtCUSTOMER_CODE
            // 
            resources.ApplyResources(this.txtCUSTOMER_CODE, "txtCUSTOMER_CODE");
            this.txtCUSTOMER_CODE.Name = "txtCUSTOMER_CODE";
            this.txtCUSTOMER_CODE.ReadOnly = true;
            this.txtCUSTOMER_CODE.TabStop = false;
            // 
            // lblCUSTOMER_CODE
            // 
            resources.ApplyResources(this.lblCUSTOMER_CODE, "lblCUSTOMER_CODE");
            this.lblCUSTOMER_CODE.Name = "lblCUSTOMER_CODE";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // lblCHANGE_DATE
            // 
            resources.ApplyResources(this.lblCHANGE_DATE, "lblCHANGE_DATE");
            this.lblCHANGE_DATE.Name = "lblCHANGE_DATE";
            // 
            // dtpCHANGE_DATE
            // 
            resources.ApplyResources(this.dtpCHANGE_DATE, "dtpCHANGE_DATE");
            this.dtpCHANGE_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpCHANGE_DATE.Name = "dtpCHANGE_DATE";
            this.dtpCHANGE_DATE.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // cboOldAmpare
            // 
            this.cboOldAmpare.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboOldAmpare.FormattingEnabled = true;
            resources.ApplyResources(this.cboOldAmpare, "cboOldAmpare");
            this.cboOldAmpare.Name = "cboOldAmpare";
            // 
            // lblNEW_AMPARE
            // 
            resources.ApplyResources(this.lblNEW_AMPARE, "lblNEW_AMPARE");
            this.lblNEW_AMPARE.Name = "lblNEW_AMPARE";
            // 
            // cboNewAmpare
            // 
            this.cboNewAmpare.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboNewAmpare.FormattingEnabled = true;
            resources.ApplyResources(this.cboNewAmpare, "cboNewAmpare");
            this.cboNewAmpare.Name = "cboNewAmpare";
            // 
            // lblREASON
            // 
            resources.ApplyResources(this.lblREASON, "lblREASON");
            this.lblREASON.Name = "lblREASON";
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Name = "label13";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnCHANGE_LOG_Click);
            // 
            // cboReason
            // 
            this.cboReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboReason.FormattingEnabled = true;
            resources.ApplyResources(this.cboReason, "cboReason");
            this.cboReason.Name = "cboReason";
            // 
            // DialogCustomerChangeAmpare
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogCustomerChangeAmpare";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBox txtCUSTOMER_CODE;
        private Label lblCUSTOMER_CODE;
        private TextBox txtCUSTOMER_NAME;
        private Label lblCUSTOMER_NAME;
        private Label lblOLD_AMPARE;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Panel panel1;
        private DateTimePicker dtpCHANGE_DATE;
        private Label lblCHANGE_DATE;
        private ComboBox cboNewAmpare;
        private ComboBox cboOldAmpare;
        private Label lblNEW_AMPARE;
        private Label lblREASON;
        private Label label1;
        private Label label13;
        private ExButton btnCHANGE_LOG;
        private ComboBox cboReason;
    }
}