﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using Login = SoftTech.Security.Logic.Login;

namespace EPower.Interface
{
    public partial class DialogPaymentDetailCancel : ExDialog
    {
        #region Private 
        int _intPaymentID = 0;
        TBL_PAYMENT objPayment = new TBL_PAYMENT();
        TBL_CUSTOMER objCustomer = new TBL_CUSTOMER();
        TBL_ACCOUNT_CHART objAccountChart = new TBL_ACCOUNT_CHART();
        TLKP_CURRENCY objCurrency = new TLKP_CURRENCY();
        #endregion Private 

        #region Constructor
        public DialogPaymentDetailCancel(int intPaymentID)
        {
            InitializeComponent();
            _intPaymentID = intPaymentID;
            objPayment = DBDataContext.Db.TBL_PAYMENTs.FirstOrDefault(x => x.PAYMENT_ID == _intPaymentID);
            objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == objPayment.CUSTOMER_ID);
            objAccountChart = DBDataContext.Db.TBL_ACCOUNT_CHARTs.FirstOrDefault(x => x.ACCOUNT_ID == objPayment.PAYMENT_ACCOUNT_ID);
            objCurrency = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.CURRENCY_ID == objPayment.CURRENCY_ID);
            UIHelper.DataGridViewProperties(dgvPaymentDetail);
            Bind();
            read();
        }
        #endregion Constructor

        #region Method

        void Bind()
        {
            dgvPaymentDetail.DataSource = from inv in DBDataContext.Db.TBL_INVOICEs 
                                          join pd in DBDataContext.Db.TBL_PAYMENT_DETAILs on inv.INVOICE_ID equals pd.INVOICE_ID
                                          join c in DBDataContext.Db.TLKP_CURRENCies on inv.CURRENCY_ID equals c.CURRENCY_ID
                                          where pd.PAYMENT_ID == _intPaymentID
                                          select new
                                          {
                                              inv.INVOICE_ID,
                                              inv.INVOICE_NO,
                                              inv.INVOICE_TITLE,
                                              pd.DUE_AMOUNT,
                                              pd.PAY_AMOUNT,
                                              BALANCE = inv.SETTLE_AMOUNT - inv.PAID_AMOUNT ,
                                              c.CURRENCY_SING
                                          };
        }

        void read()
        {
            txtPaymentNo.Text = objPayment.PAYMENT_NO;
            txtCustomer.Text = objCustomer.LAST_NAME_KH + " " + objCustomer.FIRST_NAME_KH;
            txtPayDate.Text = objPayment.PAY_DATE.ToString("dd-MM-yyyy hh:mm tt");
            txtCreateBy.Text = objPayment.CREATE_BY;
            txtPaymentAccount.Text = objAccountChart != null ? objAccountChart.ACCOUNT_NAME : "";
            txtPayAmount.Text = UIHelper.FormatCurrency(objPayment.PAY_AMOUNT, objPayment.CURRENCY_ID)+" "+objCurrency.CURRENCY_SING;
        }

        #endregion Method

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvPaymentDetail_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != -1 && e.RowIndex != -1)
            {
                if (this.dgvPaymentDetail.Columns[e.ColumnIndex].Name == this.INVOICE_NO.Name)
                {
                    bool printingInvoice = DataHelper.ParseToBoolean(Method.Utilities[Utility.INVOICE_CONCURRENT_PRINTING]);
                    int invoiceId = int.Parse(dgvPaymentDetail.SelectedRows[0].Cells[INVOICE_ID.Name].Value.ToString());

                    //if not exists report invoice 
                    string path = string.Format(Settings.Default.PATH_REPORT, Settings.Default.LANGUAGE == "en-US" ? "" : "" + Settings.Default.LANGUAGE);
                    path = Path.Combine(Directory.GetCurrentDirectory(), path);
                    string PathCustomized = path + "Customized\\" + Settings.Default.REPORT_INVOICE;
                    if (!File.Exists(PathCustomized))
                    {
                        if (!File.Exists(path + Settings.Default.REPORT_INVOICE))
                        {
                            MsgBox.ShowInformation(string.Format(Resources.NOT_FOUND_INVOICE, "", Settings.Default.REPORT_INVOICE), Resources.INFORMATION);
                            return;
                        }
                    }

                    if (printingInvoice)
                    {
                        TBL_PRINT_INVOICE objPrintInvoice = new TBL_PRINT_INVOICE()
                        {
                            PRINT_DATE = DBDataContext.Db.GetSystemDate(),
                            LOGIN_ID = Login.CurrentLogin.LOGIN_ID
                        };
                        using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                        {

                            DBDataContext.Db.TBL_PRINT_INVOICEs.InsertOnSubmit(objPrintInvoice);
                            DBDataContext.Db.SubmitChanges();
                            DBDataContext.Db.TBL_PRINT_INVOICE_DETAILs.InsertOnSubmit(new TBL_PRINT_INVOICE_DETAIL()
                            {
                                PRINT_INVOICE_ID = objPrintInvoice.PRINT_INVOICE_ID,
                                INVOICE_ID = invoiceId,
                                PRINT_ORDER = 0
                            });
                            DBDataContext.Db.SubmitChanges();
                            tran.Complete();
                        }
                        new DialogCustomerPrintInvoice(true, objPrintInvoice.PRINT_INVOICE_ID).ShowDialog();
                    }
                    else
                    {
                        using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                        {
                            DBDataContext.Db.TBL_INVOICE_TO_PRINTs.DeleteAllOnSubmit(DBDataContext.Db.TBL_INVOICE_TO_PRINTs);
                            DBDataContext.Db.TBL_INVOICE_TO_PRINTs.InsertOnSubmit(new TBL_INVOICE_TO_PRINT() { INVOICE_ID = invoiceId });
                            DBDataContext.Db.SubmitChanges();
                            tran.Complete();
                        }
                        new DialogCustomerPrintInvoice(false, 0).ShowDialog();
                    } 
                }
            }
        }
    }
}
