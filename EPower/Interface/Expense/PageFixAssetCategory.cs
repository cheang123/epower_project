﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageFixAssetCategory : Form
    { 
        
        public TBL_FIX_ASSET_CATEGORY FixAssetCategory
        {
            get
            {
                TBL_FIX_ASSET_CATEGORY objFixAssetCategory = null;
                if (tvw.SelectedNode!=null )
                {
                    int categoryId = (int)tvw.SelectedNode.Tag ;
                    try
                    {
                        objFixAssetCategory = DBDataContext.Db.TBL_FIX_ASSET_CATEGORies.FirstOrDefault(x => x.CATEGORY_ID == categoryId);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objFixAssetCategory;
            }
        }

        #region Constructor
        public PageFixAssetCategory()
        {
            InitializeComponent(); 
            txt_QuickSearch(null, null);

            this.btnEDIT.Enabled =
                this.btnREMOVE.Enabled =
                this.btnADD.Enabled = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_EDIT_FIXED_ASSET_CATEGORY]);
        }
        #endregion

        #region Operation
        /// <summary>
        /// Load data from database.
        /// </summary>
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            try
            {
                new FixAssetCategoryPopulator().PopluateTree(tvw);
                this.tvw.ExpandAll();
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            } 
        }
        void SelectNode(int id){
          var node =  this.tvw.GetNodesIncludeAncestors()
                    .FirstOrDefault(x => (int)x.Tag == id);
          if (node != null)
          {
              this.tvw.SelectedNode = node;
          } 
        }

        /// <summary>
        /// Add new ExpenseCategory.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNew_Click(object sender, EventArgs e)
        {
            
            var parentId = 0;
            if (this.tvw.SelectedNode != null)
            {
                parentId = (int)this.tvw.SelectedNode.Tag;
            }
            var objParent = DBDataContext.Db.TBL_FIX_ASSET_CATEGORies.FirstOrDefault(x => x.CATEGORY_ID == parentId);
          
            var objCategory =  new TBL_FIX_ASSET_CATEGORY();
            objCategory.PARENT_ID = parentId;
            if (objParent != null)
            {
                objCategory.DEFAULT_USEFUL_LIFE = objParent.DEFAULT_USEFUL_LIFE;
                objCategory.ASSET_ACCOUNT_ID = objParent.ASSET_ACCOUNT_ID;
                objCategory.ACCUMULATED_DEPRECIATION_ACCOUNT_ID = objParent.ACCUMULATED_DEPRECIATION_ACCOUNT_ID;
                objCategory.DEPRECIATION_EXPENSE_ID = objParent.DEPRECIATION_EXPENSE_ID;
            }

            DialogFixAssetCategory dig = new DialogFixAssetCategory(GeneralProcess.Insert, objCategory);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                this.SelectNode( dig.FixAssetCategory.CATEGORY_ID);
            }
        }

        /// <summary>
        /// Edit ExpenseCategory.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (this.tvw.SelectedNode!=null && this.tvw.SelectedNode.Level >0 )
            {
                DialogFixAssetCategory dig = new DialogFixAssetCategory(GeneralProcess.Update, FixAssetCategory);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    this.SelectNode(  dig.FixAssetCategory.CATEGORY_ID);
                }
            }
        }

        /// <summary>
        /// Remove ExpenseCategory.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if ( IsDeletable())
            {
                DialogFixAssetCategory dig = new DialogFixAssetCategory(GeneralProcess.Delete, FixAssetCategory);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    this.SelectNode(dig.FixAssetCategory.CATEGORY_ID - 1);
                }
            }
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        #endregion

        #region Method               
        private bool IsDeletable()
        {
            bool val = false;
            if (this.tvw.SelectedNode!=null)
            {
                val = true;
                if (DBDataContext.Db.TBL_FIX_ASSET_CATEGORies.Where(x => x.PARENT_ID == FixAssetCategory.CATEGORY_ID && x.IS_ACTIVE).Count() > 0)
                {
                    MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE, Resources.INFORMATION);
                    val = false;
                }
            }            
            return val;
        }
        #endregion
    }
}
