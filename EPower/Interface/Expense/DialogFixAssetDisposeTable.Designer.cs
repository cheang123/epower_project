﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogFixAssetDisposeTable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogFixAssetDisposeTable));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dtpStartUse = new System.Windows.Forms.DateTimePicker();
            this.lblUseDate = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.lblDeleteDepose = new SoftTech.Component.ExLinkLabel(this.components);
            this.lblUpdateDepose = new SoftTech.Component.ExLinkLabel(this.components);
            this.lblAddDepose = new SoftTech.Component.ExLinkLabel(this.components);
            this.txtFixAssetName = new System.Windows.Forms.TextBox();
            this.lblQuantity = new System.Windows.Forms.Label();
            this.lblPrice = new System.Windows.Forms.Label();
            this.lblAmount = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.txtTotalAmount = new System.Windows.Forms.TextBox();
            this.txtQty = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.DISPOSE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TRAN_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_BY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRICE_I = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_SIGN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.lblAmount);
            this.content.Controls.Add(this.lblPrice);
            this.content.Controls.Add(this.lblQuantity);
            this.content.Controls.Add(this.txtQty);
            this.content.Controls.Add(this.txtTotal);
            this.content.Controls.Add(this.txtTotalAmount);
            this.content.Controls.Add(this.txtPrice);
            this.content.Controls.Add(this.txtFixAssetName);
            this.content.Controls.Add(this.lblAddDepose);
            this.content.Controls.Add(this.lblUpdateDepose);
            this.content.Controls.Add(this.lblDeleteDepose);
            this.content.Controls.Add(this.dgv);
            this.content.Controls.Add(this.label2);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.dtpStartUse);
            this.content.Controls.Add(this.label4);
            this.content.Controls.Add(this.label3);
            this.content.Controls.Add(this.lblUseDate);
            this.content.Controls.Add(this.label7);
            this.content.Controls.Add(this.label6);
            this.content.Controls.Add(this.label5);
            this.content.Controls.Add(this.label19);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.label19, 0);
            this.content.Controls.SetChildIndex(this.label5, 0);
            this.content.Controls.SetChildIndex(this.label6, 0);
            this.content.Controls.SetChildIndex(this.label7, 0);
            this.content.Controls.SetChildIndex(this.lblUseDate, 0);
            this.content.Controls.SetChildIndex(this.label3, 0);
            this.content.Controls.SetChildIndex(this.label4, 0);
            this.content.Controls.SetChildIndex(this.dtpStartUse, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.label2, 0);
            this.content.Controls.SetChildIndex(this.dgv, 0);
            this.content.Controls.SetChildIndex(this.lblDeleteDepose, 0);
            this.content.Controls.SetChildIndex(this.lblUpdateDepose, 0);
            this.content.Controls.SetChildIndex(this.lblAddDepose, 0);
            this.content.Controls.SetChildIndex(this.txtFixAssetName, 0);
            this.content.Controls.SetChildIndex(this.txtPrice, 0);
            this.content.Controls.SetChildIndex(this.txtTotalAmount, 0);
            this.content.Controls.SetChildIndex(this.txtTotal, 0);
            this.content.Controls.SetChildIndex(this.txtQty, 0);
            this.content.Controls.SetChildIndex(this.lblQuantity, 0);
            this.content.Controls.SetChildIndex(this.lblPrice, 0);
            this.content.Controls.SetChildIndex(this.lblAmount, 0);
            // 
            // dtpStartUse
            // 
            resources.ApplyResources(this.dtpStartUse, "dtpStartUse");
            this.dtpStartUse.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartUse.Name = "dtpStartUse";
            // 
            // lblUseDate
            // 
            resources.ApplyResources(this.lblUseDate, "lblUseDate");
            this.lblUseDate.Name = "lblUseDate";
            // 
            // label19
            // 
            resources.ApplyResources(this.label19, "label19");
            this.label19.Name = "label19";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DISPOSE_ID,
            this.TRAN_DATE,
            this.CREATE_BY,
            this.QTY,
            this.PRICE_I,
            this.AMOUNT,
            this.CURRENCY_SIGN,
            this.CURRENCY_ID});
            this.dgv.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // lblDeleteDepose
            // 
            resources.ApplyResources(this.lblDeleteDepose, "lblDeleteDepose");
            this.lblDeleteDepose.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblDeleteDepose.Name = "lblDeleteDepose";
            this.lblDeleteDepose.TabStop = true;
            this.lblDeleteDepose.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblDeleteDepose_LinkClicked);
            // 
            // lblUpdateDepose
            // 
            resources.ApplyResources(this.lblUpdateDepose, "lblUpdateDepose");
            this.lblUpdateDepose.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblUpdateDepose.Name = "lblUpdateDepose";
            this.lblUpdateDepose.TabStop = true;
            this.lblUpdateDepose.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblUpdateDepose_LinkClicked);
            // 
            // lblAddDepose
            // 
            resources.ApplyResources(this.lblAddDepose, "lblAddDepose");
            this.lblAddDepose.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblAddDepose.Name = "lblAddDepose";
            this.lblAddDepose.TabStop = true;
            this.lblAddDepose.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblAddDepose_LinkClicked);
            // 
            // txtFixAssetName
            // 
            resources.ApplyResources(this.txtFixAssetName, "txtFixAssetName");
            this.txtFixAssetName.Name = "txtFixAssetName";
            this.txtFixAssetName.ReadOnly = true;
            // 
            // lblQuantity
            // 
            resources.ApplyResources(this.lblQuantity, "lblQuantity");
            this.lblQuantity.Name = "lblQuantity";
            // 
            // lblPrice
            // 
            resources.ApplyResources(this.lblPrice, "lblPrice");
            this.lblPrice.Name = "lblPrice";
            // 
            // lblAmount
            // 
            resources.ApplyResources(this.lblAmount, "lblAmount");
            this.lblAmount.Name = "lblAmount";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // txtPrice
            // 
            resources.ApplyResources(this.txtPrice, "txtPrice");
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.ReadOnly = true;
            // 
            // txtTotalAmount
            // 
            resources.ApplyResources(this.txtTotalAmount, "txtTotalAmount");
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.ReadOnly = true;
            // 
            // txtQty
            // 
            resources.ApplyResources(this.txtQty, "txtQty");
            this.txtQty.Name = "txtQty";
            this.txtQty.ReadOnly = true;
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // txtTotal
            // 
            resources.ApplyResources(this.txtTotal, "txtTotal");
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.ReadOnly = true;
            // 
            // DISPOSE_ID
            // 
            this.DISPOSE_ID.DataPropertyName = "DISPOSE_ID";
            resources.ApplyResources(this.DISPOSE_ID, "DISPOSE_ID");
            this.DISPOSE_ID.Name = "DISPOSE_ID";
            this.DISPOSE_ID.ReadOnly = true;
            // 
            // TRAN_DATE
            // 
            this.TRAN_DATE.DataPropertyName = "TRAN_DATE";
            dataGridViewCellStyle2.Format = "MM-yyyy";
            dataGridViewCellStyle2.NullValue = null;
            this.TRAN_DATE.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.TRAN_DATE, "TRAN_DATE");
            this.TRAN_DATE.Name = "TRAN_DATE";
            this.TRAN_DATE.ReadOnly = true;
            // 
            // CREATE_BY
            // 
            this.CREATE_BY.DataPropertyName = "CREATE_BY";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N0";
            dataGridViewCellStyle3.NullValue = null;
            this.CREATE_BY.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.CREATE_BY, "CREATE_BY");
            this.CREATE_BY.Name = "CREATE_BY";
            this.CREATE_BY.ReadOnly = true;
            // 
            // QTY
            // 
            this.QTY.DataPropertyName = "QTY";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = null;
            this.QTY.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.QTY, "QTY");
            this.QTY.Name = "QTY";
            this.QTY.ReadOnly = true;
            // 
            // PRICE_I
            // 
            this.PRICE_I.DataPropertyName = "PRICE";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "#,##0.####";
            dataGridViewCellStyle5.NullValue = null;
            this.PRICE_I.DefaultCellStyle = dataGridViewCellStyle5;
            resources.ApplyResources(this.PRICE_I, "PRICE_I");
            this.PRICE_I.Name = "PRICE_I";
            this.PRICE_I.ReadOnly = true;
            // 
            // AMOUNT
            // 
            this.AMOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AMOUNT.DataPropertyName = "AMOUNT";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "#,##0.####";
            dataGridViewCellStyle6.NullValue = null;
            this.AMOUNT.DefaultCellStyle = dataGridViewCellStyle6;
            resources.ApplyResources(this.AMOUNT, "AMOUNT");
            this.AMOUNT.Name = "AMOUNT";
            this.AMOUNT.ReadOnly = true;
            // 
            // CURRENCY_SIGN
            // 
            this.CURRENCY_SIGN.DataPropertyName = "CURRENCY_SIGN";
            resources.ApplyResources(this.CURRENCY_SIGN, "CURRENCY_SIGN");
            this.CURRENCY_SIGN.Name = "CURRENCY_SIGN";
            this.CURRENCY_SIGN.ReadOnly = true;
            // 
            // CURRENCY_ID
            // 
            this.CURRENCY_ID.DataPropertyName = "CURRENCY_ID";
            resources.ApplyResources(this.CURRENCY_ID, "CURRENCY_ID");
            this.CURRENCY_ID.Name = "CURRENCY_ID";
            this.CURRENCY_ID.ReadOnly = true;
            // 
            // DialogFixAssetDisposeTable
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogFixAssetDisposeTable";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DateTimePicker dtpStartUse;
        private Label lblUseDate;
        private Label label19;
        private Label label2;
        private Label label1;
        private DataGridView dgv;
        private ExLinkLabel lblAddDepose;
        private ExLinkLabel lblUpdateDepose;
        private ExLinkLabel lblDeleteDepose;
        private TextBox txtFixAssetName;
        private Label lblQuantity;
        private Label lblPrice;
        private Label lblAmount;
        private TextBox txtTotalAmount;
        private TextBox txtPrice;
        private Label label7;
        private Label label6;
        private Label label5;
        private TextBox txtQty;
        private Label label4;
        private Label label3;
        private TextBox txtTotal;
        private DataGridViewTextBoxColumn DISPOSE_ID;
        private DataGridViewTextBoxColumn TRAN_DATE;
        private DataGridViewTextBoxColumn CREATE_BY;
        private DataGridViewTextBoxColumn QTY;
        private DataGridViewTextBoxColumn PRICE_I;
        private DataGridViewTextBoxColumn AMOUNT;
        private DataGridViewTextBoxColumn CURRENCY_SIGN;
        private DataGridViewTextBoxColumn CURRENCY_ID;
    }
}