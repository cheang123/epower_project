﻿using System;
using System.Linq;
using System.Windows.Forms;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using Login = SoftTech.Security.Logic.Login;

namespace EPower.Interface
{
    public partial class DialogExchangeRate: ExDialog
    {
        int exchageRateId = 0;
        private DateTime _defaultRateDate;

        public DialogExchangeRate()
        {
            InitializeComponent();
            _defaultRateDate = DBDataContext.Db.GetSystemDate(); 
            LoadCurrency();
            cboCurrencyFrom.SelectedValue = 2;
        }
        public DialogExchangeRate(DateTime rateDate,int currencyId)
        {
            InitializeComponent();
            LoadCurrency();
            _defaultRateDate = rateDate;
            dtpDate.Value = rateDate;
            //function currency if in accounting => so normally user will exchange from foriegn currency to function currency
            //=> function currency as ToCurrency;
            var defaultCurrency = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.IS_DEFAULT_CURRENCY);
            //=> forigenCurrency as FromCurrency
            var forigenCurrency = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => !x.IS_DEFAULT_CURRENCY);
            cboCurrencyFrom.SelectedValue = currencyId;
            //cboCurrencyFrom.SelectedValue = forigenCurrency?.CURRENCY_ID ?? 0;
            cboCurrencyTo.SelectedValue = defaultCurrency?.CURRENCY_ID ?? 0;
        } 

        public void LoadCurrency()
        {
            UIHelper.SetDataSourceToComboBox(cboCurrencyFrom, Lookup.GetCurrencies());
            UIHelper.SetDataSourceToComboBox(cboCurrencyTo, Lookup.GetCurrencies());
            cboCurrencyFrom.SelectedIndex = 1;
        }

        private void bindLastExchangeRate()
        {
            lblExchangeRate1.Text = "";
            btnSAVE.Enabled = false;
            txtExchangeRate.Enabled = false;
            int c1 = (int)(this.cboCurrencyFrom.SelectedValue ?? 0);
            int c2 = (int)(this.cboCurrencyTo.SelectedValue ?? 0);
            if (c1== 0 || c2 == 0 || c1==c2)
            {
                return;
            } 
            var objLastExchangeRate = DBDataContext.Db.TBL_EXCHANGE_RATEs
                                                      .OrderByDescending(row => row.CREATE_ON)
                                                      .FirstOrDefault(x => x.CURRENCY_ID == c1 && x.EXCHANGE_CURRENCY_ID == c2);

            if (objLastExchangeRate == null)
            {
                MsgBox.ShowWarning(Resources.MS_NO_DATA_ABOUT_EXCHANGE_RATE, this.Text);
                return;
            }
             
            var objCurrency1 = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.CURRENCY_ID == c1);
            var objCurrency2 = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.CURRENCY_ID == c2);


            if (objLastExchangeRate.MULTIPLIER_METHOD)
            {
                this.lblExchangeRate1.Text = string.Format("1 {0} = {1}", objCurrency1.CURRENCY_SING, objCurrency2.CURRENCY_SING);
            }
            else
            {
                this.lblExchangeRate1.Text = string.Format("1 {1} = {0}", objCurrency2.CURRENCY_SING, objCurrency1.CURRENCY_SING);
            }

            rMultiplier1.Checked =  objLastExchangeRate.MULTIPLIER_METHOD;
            rDivisor1.Checked = !rMultiplier1.Checked;


            btnSAVE.Enabled = true;
            txtExchangeRate.Enabled = true;
            txtExchangeRate.Focus();
        }

        private void bindHistory()
        {
            if (loading)
            {
                return;
            }

            int c1 = (int)(this.cboCurrencyFrom.SelectedValue ?? 0);
            int c2 = (int)(this.cboCurrencyTo.SelectedValue ?? 0);
            var q = from ex in DBDataContext.Db.TBL_EXCHANGE_RATEs
                    where ex.CREATE_ON <= this.dtpDate.Value
                     && ex.CURRENCY_ID == c1
                     && ex.EXCHANGE_CURRENCY_ID == c2
                    orderby ex.CREATE_ON descending
                    select new
                    {
                        ex.EXCHANGE_RATE_ID,
                        ex.EXCHANGE_RATE,
                        ex.MULTIPLIER_METHOD,
                        ex.CREATE_ON,
                        ex.CREATE_BY
                    };
            this.dgv.DataSource = q.Take(50);
        }

        private void cboCurrencyFrom_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindLastExchangeRate();
            bindHistory();
        }

        private void cboCurrencyTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindLastExchangeRate();
            bindHistory();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            save();
            btnREMOVE.Enabled = true;
        }

        private void save()
        {
            this.ClearAllValidation();

            if (DataHelper.ParseToDecimal(txtExchangeRate.Text) <= 0.0m)
            {
                txtExchangeRate.SetValidation(Resources.REQUEST_GRATER_THAN_ZERO);
                return;
            }

            int c1 = (int)(this.cboCurrencyFrom.SelectedValue ?? 0);
            int c2 = (int)(this.cboCurrencyTo.SelectedValue ?? 0);

            var obj = new TBL_EXCHANGE_RATE()
            {
                CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                CREATE_ON = dtpDate.Value,
                CURRENCY_ID = c1,
                EXCHANGE_CURRENCY_ID = c2,
                EXCHANGE_RATE = DataHelper.ParseToDecimal(this.txtExchangeRate.Text),
                MULTIPLIER_METHOD = this.rMultiplier1.Checked,
                EXCHANGE_RATE_ID = exchageRateId
            };
            if (obj.EXCHANGE_RATE_ID == 0)
            {
                DBDataContext.Db.Insert(obj);
            }
            else
            {
                var objOld = DBDataContext.Db.TBL_EXCHANGE_RATEs.FirstOrDefault(x => x.EXCHANGE_RATE_ID == obj.EXCHANGE_RATE_ID);
                DBDataContext.Db.Update(objOld, obj);
            }

            bindHistory();
            clearLine();
        }

        private void remove()
        {
            if (this.dgv.SelectedRows.Count == 0)
            {
                return;
            }
            var row = this.dgv.SelectedRows[0];
            if ((DateTime)row.Cells[this.CREATE_ON.Name].Value == new DateTime(1900, 1, 1))
            {
                MsgBox.ShowInformation(Resources.MS_CANNOT_DELETE_SYSTEM_RECORD,this.Text);
                return;
            }
            if (MsgBox.ShowQuestion(Resources.MSQ_ARE_YOU_SURE_WANT_TO_DELETE_RECORD,this.Text) == DialogResult.No)
            {
                return;
            }
            var id = (int)row.Cells[this.EXCHANGE_RATE_ID.Name].Value;
            var obj = DBDataContext.Db.TBL_EXCHANGE_RATEs.FirstOrDefault(x => x.EXCHANGE_RATE_ID == id);
            DBDataContext.Db.Delete(obj);

            bindHistory();
        }
        bool loading = false;
        private void edit()
        {
            if (this.dgv.SelectedRows.Count == 0)
            {
                return;
            }
            var row = this.dgv.SelectedRows[0];
            this.loading = true;
            this.exchageRateId = (int)row.Cells[this.EXCHANGE_RATE_ID.Name].Value;
            this.dtpDate.Value = (DateTime)row.Cells[this.CREATE_ON.Name].Value;
            this.txtExchangeRate.Text = ((decimal)row.Cells[this.EXCHANGE_RATE.Name].Value).ToString("#.######");
            this.rMultiplier1.Checked = (bool)row.Cells[this.MULTIPLIER_METHOD.Name].Value;
            this.rDivisor1.Checked = !this.rMultiplier1.Checked;
            this.loading = false;
            this.dtpDate.Enabled = false;
            this.cboCurrencyFrom.Enabled = false;
            this.cboCurrencyTo.Enabled = false;
        }
        private void clearLine()
        {
            this.exchageRateId = 0;
            this.dtpDate.Value = _defaultRateDate;
            this.txtExchangeRate.Text = "";
            this.dtpDate.Enabled = true;
            this.cboCurrencyFrom.Enabled = true;
            this.cboCurrencyTo.Enabled = true;
            this.txtExchangeRate.Focus();
        }

        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                edit();
            }
        }

       

        private void txtExchangeRate_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                save();
            }
        }

        private void dgv_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                remove();
            }
        }

        private void dtpDate_ValueChanged(object sender, EventArgs e)
        {
            bindHistory();
        }

        private void btnEdit_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            edit();
            btnREMOVE.Enabled = false;
        }

        private void btnRemove_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            remove();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
