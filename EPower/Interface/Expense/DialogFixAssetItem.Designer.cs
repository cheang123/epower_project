﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogFixAssetItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogFixAssetItem));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabGENERAL_INFORMATION = new System.Windows.Forms.TabPage();
            this.txtProductionYear = new System.Windows.Forms.TextBox();
            this.cboPaymentAccount = new SoftTech.Component.TreeComboBox();
            this.cboCategory = new SoftTech.Component.TreeComboBox();
            this.txtFixAssetName = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.lblPRICE_PER_UNIT = new System.Windows.Forms.Label();
            this.dtpUseDate = new System.Windows.Forms.DateTimePicker();
            this.txtBrand = new System.Windows.Forms.TextBox();
            this.lblBRAND = new System.Windows.Forms.Label();
            this.lblSTART_USE_DATE = new System.Windows.Forms.Label();
            this.lblSOURCE = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txtSource = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.lblUNIT = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.lblQUANTITY = new System.Windows.Forms.Label();
            this.lblACCUMULATE_DEPRECIATION = new System.Windows.Forms.Label();
            this.lblAMOUNT = new System.Windows.Forms.Label();
            this.lblANNUAL_DEPRECIATION_RATE = new System.Windows.Forms.Label();
            this.lblFIX_ASSET = new System.Windows.Forms.Label();
            this.txtUnit = new System.Windows.Forms.TextBox();
            this.txtQty = new System.Windows.Forms.TextBox();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.txtAccDepreciation = new System.Windows.Forms.TextBox();
            this.txtSalvageValue = new System.Windows.Forms.TextBox();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.txtUseFulLife = new System.Windows.Forms.TextBox();
            this.txtDepreciationRate = new System.Windows.Forms.TextBox();
            this.lblSALVAGE_VALUE = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.lblDEPRECIATION_PERIOD = new System.Windows.Forms.Label();
            this.txtProductStatus = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.lblPRODUCT_STATUS = new System.Windows.Forms.Label();
            this.lblYEAR_OF_PRODUCTION = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.lblCURRENCY = new System.Windows.Forms.Label();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.lblPAYMENT_ACCOUNT = new System.Windows.Forms.Label();
            this.lblFIX_ASSET_TYPE = new System.Windows.Forms.Label();
            this.lblNOTE = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.tabDEPRECIATION = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblREMOVE_DEPRECIATION = new SoftTech.Component.ExLinkLabel(this.components);
            this.lblDEPRECIATION = new SoftTech.Component.ExLinkLabel(this.components);
            this.chbCheckAll_ = new System.Windows.Forms.CheckBox();
            this.txtBookValue = new System.Windows.Forms.TextBox();
            this.lblBOOK_VALUE_1 = new System.Windows.Forms.Label();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.DEPRECIATION_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_CHECK_ = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ROW_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_DEPRECIATION = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.DEPRECIATION_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MONTH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_BY_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ACCUMULATE_DEPRECIATION_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ACCUMULATED_DEPRECIATION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BOOK_VALUE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.STATUS_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblACCUMULATE_DEPRECIATION_1 = new System.Windows.Forms.Label();
            this.txtTotalAccumulated = new System.Windows.Forms.TextBox();
            this.dtpSuggestionDate = new System.Windows.Forms.DateTimePicker();
            this.cboDepreciationStatus = new System.Windows.Forms.ComboBox();
            this.tabDISPOSE_DEPRECIATION = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnADD_2 = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnEDIT_2 = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnREMOVE_2 = new SoftTech.Component.ExLinkLabel(this.components);
            this.txtTotalDisposeSoldValue = new System.Windows.Forms.TextBox();
            this.txtTotalDisposeBookValue = new System.Windows.Forms.TextBox();
            this.txtTotalDisposeGainLoss = new System.Windows.Forms.TextBox();
            this.dgvDispose = new System.Windows.Forms.DataGridView();
            this.lblAMOUNT_2 = new System.Windows.Forms.Label();
            this.tabSERVICE_AND_MAINTENAINCE = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnADD_1 = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnEDIT_1 = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnDELETE_1 = new SoftTech.Component.ExLinkLabel(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.cboCurrencyMaintenance = new System.Windows.Forms.ComboBox();
            this.dgvTotalAmountMaintenance = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_BALANCE_MAINTENANCE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMaintenance = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FIX_ASSET_ITEM_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_BY_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REF_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAYMENT_ACCOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_SING_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DISPOSE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DISPOSE_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_BY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QUANTITY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRICE_I = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DISPOSE_SOLD_VALUE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BOOK_VALUE_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GAIN_LOSS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_SIGN_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DISPOSE_CURRENCY_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabControl.SuspendLayout();
            this.tabGENERAL_INFORMATION.SuspendLayout();
            this.tabDEPRECIATION.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.tabDISPOSE_DEPRECIATION.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDispose)).BeginInit();
            this.tabSERVICE_AND_MAINTENAINCE.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTotalAmountMaintenance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaintenance)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.tabControl);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.tabControl, 0);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabGENERAL_INFORMATION);
            this.tabControl.Controls.Add(this.tabDEPRECIATION);
            this.tabControl.Controls.Add(this.tabDISPOSE_DEPRECIATION);
            this.tabControl.Controls.Add(this.tabSERVICE_AND_MAINTENAINCE);
            resources.ApplyResources(this.tabControl, "tabControl");
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            // 
            // tabGENERAL_INFORMATION
            // 
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtProductionYear);
            this.tabGENERAL_INFORMATION.Controls.Add(this.cboPaymentAccount);
            this.tabGENERAL_INFORMATION.Controls.Add(this.cboCategory);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtFixAssetName);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label13);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblPRICE_PER_UNIT);
            this.tabGENERAL_INFORMATION.Controls.Add(this.dtpUseDate);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtBrand);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblBRAND);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblSTART_USE_DATE);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblSOURCE);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label27);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtSource);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label16);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblUNIT);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label38);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblQUANTITY);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblACCUMULATE_DEPRECIATION);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblAMOUNT);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblANNUAL_DEPRECIATION_RATE);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblFIX_ASSET);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtUnit);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtQty);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtPrice);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label39);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtAccDepreciation);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtSalvageValue);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtAmount);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtUseFulLife);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtDepreciationRate);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblSALVAGE_VALUE);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label28);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label33);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblDEPRECIATION_PERIOD);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtProductStatus);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label30);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblPRODUCT_STATUS);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblYEAR_OF_PRODUCTION);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label24);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblCURRENCY);
            this.tabGENERAL_INFORMATION.Controls.Add(this.cboCurrency);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblPAYMENT_ACCOUNT);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblFIX_ASSET_TYPE);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblNOTE);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label1);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label5);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label2);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtNote);
            resources.ApplyResources(this.tabGENERAL_INFORMATION, "tabGENERAL_INFORMATION");
            this.tabGENERAL_INFORMATION.Name = "tabGENERAL_INFORMATION";
            this.tabGENERAL_INFORMATION.UseVisualStyleBackColor = true;
            // 
            // txtProductionYear
            // 
            this.txtProductionYear.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtProductionYear.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            resources.ApplyResources(this.txtProductionYear, "txtProductionYear");
            this.txtProductionYear.Name = "txtProductionYear";
            this.txtProductionYear.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            this.txtProductionYear.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.InputerNumberOnly);
            // 
            // cboPaymentAccount
            // 
            this.cboPaymentAccount.AbsoluteChildrenSelectableOnly = true;
            this.cboPaymentAccount.BranchSeparator = "/";
            this.cboPaymentAccount.Imagelist = null;
            resources.ApplyResources(this.cboPaymentAccount, "cboPaymentAccount");
            this.cboPaymentAccount.Name = "cboPaymentAccount";
            this.cboPaymentAccount.PopupHeight = 250;
            this.cboPaymentAccount.PopupWidth = 350;
            this.cboPaymentAccount.SelectedNode = null;
            // 
            // cboCategory
            // 
            this.cboCategory.AbsoluteChildrenSelectableOnly = true;
            this.cboCategory.BranchSeparator = "/";
            this.cboCategory.Imagelist = null;
            resources.ApplyResources(this.cboCategory, "cboCategory");
            this.cboCategory.Name = "cboCategory";
            this.cboCategory.PopupHeight = 250;
            this.cboCategory.PopupWidth = 350;
            this.cboCategory.SelectedNode = null;
            // 
            // txtFixAssetName
            // 
            resources.ApplyResources(this.txtFixAssetName, "txtFixAssetName");
            this.txtFixAssetName.Name = "txtFixAssetName";
            this.txtFixAssetName.Enter += new System.EventHandler(this.ChangeKeyboardKhmer);
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Name = "label13";
            // 
            // lblPRICE_PER_UNIT
            // 
            resources.ApplyResources(this.lblPRICE_PER_UNIT, "lblPRICE_PER_UNIT");
            this.lblPRICE_PER_UNIT.Name = "lblPRICE_PER_UNIT";
            // 
            // dtpUseDate
            // 
            resources.ApplyResources(this.dtpUseDate, "dtpUseDate");
            this.dtpUseDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpUseDate.Name = "dtpUseDate";
            this.dtpUseDate.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            // 
            // txtBrand
            // 
            this.txtBrand.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtBrand.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            resources.ApplyResources(this.txtBrand, "txtBrand");
            this.txtBrand.Name = "txtBrand";
            this.txtBrand.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            // 
            // lblBRAND
            // 
            resources.ApplyResources(this.lblBRAND, "lblBRAND");
            this.lblBRAND.Name = "lblBRAND";
            // 
            // lblSTART_USE_DATE
            // 
            resources.ApplyResources(this.lblSTART_USE_DATE, "lblSTART_USE_DATE");
            this.lblSTART_USE_DATE.Name = "lblSTART_USE_DATE";
            // 
            // lblSOURCE
            // 
            resources.ApplyResources(this.lblSOURCE, "lblSOURCE");
            this.lblSOURCE.Name = "lblSOURCE";
            // 
            // label27
            // 
            resources.ApplyResources(this.label27, "label27");
            this.label27.ForeColor = System.Drawing.Color.Red;
            this.label27.Name = "label27";
            // 
            // txtSource
            // 
            this.txtSource.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtSource.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            resources.ApplyResources(this.txtSource, "txtSource");
            this.txtSource.Name = "txtSource";
            this.txtSource.Enter += new System.EventHandler(this.ChangeKeyboardKhmer);
            // 
            // label16
            // 
            resources.ApplyResources(this.label16, "label16");
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Name = "label16";
            // 
            // lblUNIT
            // 
            resources.ApplyResources(this.lblUNIT, "lblUNIT");
            this.lblUNIT.Name = "lblUNIT";
            // 
            // label38
            // 
            resources.ApplyResources(this.label38, "label38");
            this.label38.ForeColor = System.Drawing.Color.Red;
            this.label38.Name = "label38";
            // 
            // lblQUANTITY
            // 
            resources.ApplyResources(this.lblQUANTITY, "lblQUANTITY");
            this.lblQUANTITY.Name = "lblQUANTITY";
            // 
            // lblACCUMULATE_DEPRECIATION
            // 
            resources.ApplyResources(this.lblACCUMULATE_DEPRECIATION, "lblACCUMULATE_DEPRECIATION");
            this.lblACCUMULATE_DEPRECIATION.Name = "lblACCUMULATE_DEPRECIATION";
            // 
            // lblAMOUNT
            // 
            resources.ApplyResources(this.lblAMOUNT, "lblAMOUNT");
            this.lblAMOUNT.Name = "lblAMOUNT";
            // 
            // lblANNUAL_DEPRECIATION_RATE
            // 
            resources.ApplyResources(this.lblANNUAL_DEPRECIATION_RATE, "lblANNUAL_DEPRECIATION_RATE");
            this.lblANNUAL_DEPRECIATION_RATE.Name = "lblANNUAL_DEPRECIATION_RATE";
            // 
            // lblFIX_ASSET
            // 
            resources.ApplyResources(this.lblFIX_ASSET, "lblFIX_ASSET");
            this.lblFIX_ASSET.Name = "lblFIX_ASSET";
            // 
            // txtUnit
            // 
            this.txtUnit.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtUnit.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            resources.ApplyResources(this.txtUnit, "txtUnit");
            this.txtUnit.Name = "txtUnit";
            this.txtUnit.Enter += new System.EventHandler(this.ChangeKeyboardKhmer);
            // 
            // txtQty
            // 
            resources.ApplyResources(this.txtQty, "txtQty");
            this.txtQty.Name = "txtQty";
            this.txtQty.TextChanged += new System.EventHandler(this.txtPrice_TextChanged);
            this.txtQty.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            this.txtQty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtQty_KeyPress);
            // 
            // txtPrice
            // 
            resources.ApplyResources(this.txtPrice, "txtPrice");
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.TextChanged += new System.EventHandler(this.txtPrice_TextChanged);
            this.txtPrice.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            this.txtPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrice_KeyPress);
            // 
            // label39
            // 
            resources.ApplyResources(this.label39, "label39");
            this.label39.ForeColor = System.Drawing.Color.Red;
            this.label39.Name = "label39";
            // 
            // txtAccDepreciation
            // 
            resources.ApplyResources(this.txtAccDepreciation, "txtAccDepreciation");
            this.txtAccDepreciation.Name = "txtAccDepreciation";
            this.txtAccDepreciation.ReadOnly = true;
            this.txtAccDepreciation.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            // 
            // txtSalvageValue
            // 
            resources.ApplyResources(this.txtSalvageValue, "txtSalvageValue");
            this.txtSalvageValue.Name = "txtSalvageValue";
            this.txtSalvageValue.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            this.txtSalvageValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrice_KeyPress);
            // 
            // txtAmount
            // 
            resources.ApplyResources(this.txtAmount, "txtAmount");
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.ReadOnly = true;
            this.txtAmount.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            // 
            // txtUseFulLife
            // 
            resources.ApplyResources(this.txtUseFulLife, "txtUseFulLife");
            this.txtUseFulLife.Name = "txtUseFulLife";
            this.txtUseFulLife.TextChanged += new System.EventHandler(this.txtDeperciationPeriod_TextChanged);
            this.txtUseFulLife.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            this.txtUseFulLife.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtUseFulLife_KeyPress);
            // 
            // txtDepreciationRate
            // 
            resources.ApplyResources(this.txtDepreciationRate, "txtDepreciationRate");
            this.txtDepreciationRate.Name = "txtDepreciationRate";
            this.txtDepreciationRate.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            this.txtDepreciationRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrice_KeyPress);
            // 
            // lblSALVAGE_VALUE
            // 
            resources.ApplyResources(this.lblSALVAGE_VALUE, "lblSALVAGE_VALUE");
            this.lblSALVAGE_VALUE.Name = "lblSALVAGE_VALUE";
            // 
            // label28
            // 
            resources.ApplyResources(this.label28, "label28");
            this.label28.ForeColor = System.Drawing.Color.Red;
            this.label28.Name = "label28";
            // 
            // label33
            // 
            resources.ApplyResources(this.label33, "label33");
            this.label33.ForeColor = System.Drawing.Color.Red;
            this.label33.Name = "label33";
            // 
            // lblDEPRECIATION_PERIOD
            // 
            resources.ApplyResources(this.lblDEPRECIATION_PERIOD, "lblDEPRECIATION_PERIOD");
            this.lblDEPRECIATION_PERIOD.Name = "lblDEPRECIATION_PERIOD";
            // 
            // txtProductStatus
            // 
            this.txtProductStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtProductStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            resources.ApplyResources(this.txtProductStatus, "txtProductStatus");
            this.txtProductStatus.Name = "txtProductStatus";
            this.txtProductStatus.Enter += new System.EventHandler(this.ChangeKeyboardKhmer);
            // 
            // label30
            // 
            resources.ApplyResources(this.label30, "label30");
            this.label30.ForeColor = System.Drawing.Color.Red;
            this.label30.Name = "label30";
            // 
            // lblPRODUCT_STATUS
            // 
            resources.ApplyResources(this.lblPRODUCT_STATUS, "lblPRODUCT_STATUS");
            this.lblPRODUCT_STATUS.Name = "lblPRODUCT_STATUS";
            // 
            // lblYEAR_OF_PRODUCTION
            // 
            resources.ApplyResources(this.lblYEAR_OF_PRODUCTION, "lblYEAR_OF_PRODUCTION");
            this.lblYEAR_OF_PRODUCTION.Name = "lblYEAR_OF_PRODUCTION";
            // 
            // label24
            // 
            resources.ApplyResources(this.label24, "label24");
            this.label24.ForeColor = System.Drawing.Color.Red;
            this.label24.Name = "label24";
            // 
            // lblCURRENCY
            // 
            resources.ApplyResources(this.lblCURRENCY, "lblCURRENCY");
            this.lblCURRENCY.Name = "lblCURRENCY";
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.Enter += new System.EventHandler(this.ChangeKeyboardKhmer);
            // 
            // lblPAYMENT_ACCOUNT
            // 
            resources.ApplyResources(this.lblPAYMENT_ACCOUNT, "lblPAYMENT_ACCOUNT");
            this.lblPAYMENT_ACCOUNT.Name = "lblPAYMENT_ACCOUNT";
            // 
            // lblFIX_ASSET_TYPE
            // 
            resources.ApplyResources(this.lblFIX_ASSET_TYPE, "lblFIX_ASSET_TYPE");
            this.lblFIX_ASSET_TYPE.Name = "lblFIX_ASSET_TYPE";
            // 
            // lblNOTE
            // 
            resources.ApplyResources(this.lblNOTE, "lblNOTE");
            this.lblNOTE.Name = "lblNOTE";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Name = "label5";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Name = "label2";
            // 
            // txtNote
            // 
            resources.ApplyResources(this.txtNote, "txtNote");
            this.txtNote.Name = "txtNote";
            this.txtNote.Enter += new System.EventHandler(this.ChangeKeyboardKhmer);
            // 
            // tabDEPRECIATION
            // 
            this.tabDEPRECIATION.Controls.Add(this.panel1);
            this.tabDEPRECIATION.Controls.Add(this.chbCheckAll_);
            this.tabDEPRECIATION.Controls.Add(this.txtBookValue);
            this.tabDEPRECIATION.Controls.Add(this.lblBOOK_VALUE_1);
            this.tabDEPRECIATION.Controls.Add(this.dgv);
            this.tabDEPRECIATION.Controls.Add(this.lblACCUMULATE_DEPRECIATION_1);
            this.tabDEPRECIATION.Controls.Add(this.txtTotalAccumulated);
            this.tabDEPRECIATION.Controls.Add(this.dtpSuggestionDate);
            this.tabDEPRECIATION.Controls.Add(this.cboDepreciationStatus);
            resources.ApplyResources(this.tabDEPRECIATION, "tabDEPRECIATION");
            this.tabDEPRECIATION.Name = "tabDEPRECIATION";
            this.tabDEPRECIATION.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblREMOVE_DEPRECIATION);
            this.panel1.Controls.Add(this.lblDEPRECIATION);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // lblREMOVE_DEPRECIATION
            // 
            resources.ApplyResources(this.lblREMOVE_DEPRECIATION, "lblREMOVE_DEPRECIATION");
            this.lblREMOVE_DEPRECIATION.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblREMOVE_DEPRECIATION.Name = "lblREMOVE_DEPRECIATION";
            this.lblREMOVE_DEPRECIATION.TabStop = true;
            this.lblREMOVE_DEPRECIATION.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblDeleteDepreciation_LinkClicked);
            // 
            // lblDEPRECIATION
            // 
            resources.ApplyResources(this.lblDEPRECIATION, "lblDEPRECIATION");
            this.lblDEPRECIATION.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblDEPRECIATION.Name = "lblDEPRECIATION";
            this.lblDEPRECIATION.TabStop = true;
            this.lblDEPRECIATION.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblDepreciation_LinkClicked);
            // 
            // chbCheckAll_
            // 
            resources.ApplyResources(this.chbCheckAll_, "chbCheckAll_");
            this.chbCheckAll_.Name = "chbCheckAll_";
            this.chbCheckAll_.UseVisualStyleBackColor = true;
            this.chbCheckAll_.CheckedChanged += new System.EventHandler(this.cbCheckAll_CheckedChanged);
            // 
            // txtBookValue
            // 
            resources.ApplyResources(this.txtBookValue, "txtBookValue");
            this.txtBookValue.Name = "txtBookValue";
            this.txtBookValue.ReadOnly = true;
            // 
            // lblBOOK_VALUE_1
            // 
            resources.ApplyResources(this.lblBOOK_VALUE_1, "lblBOOK_VALUE_1");
            this.lblBOOK_VALUE_1.Name = "lblBOOK_VALUE_1";
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DEPRECIATION_ID,
            this.IS_CHECK_,
            this.ROW_NO,
            this.IS_DEPRECIATION,
            this.DEPRECIATION_DATE,
            this.MONTH,
            this.CREATE_BY_1,
            this.ACCUMULATE_DEPRECIATION_1,
            this.ACCUMULATED_DEPRECIATION,
            this.BOOK_VALUE,
            this.dataGridViewTextBoxColumn1,
            this.STATUS_ID});
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(205)))), ((int)(((byte)(239)))));
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv.DefaultCellStyle = dataGridViewCellStyle10;
            this.dgv.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgv_CellFormatting);
            // 
            // DEPRECIATION_ID
            // 
            this.DEPRECIATION_ID.DataPropertyName = "DEPRECIATION_ID";
            resources.ApplyResources(this.DEPRECIATION_ID, "DEPRECIATION_ID");
            this.DEPRECIATION_ID.Name = "DEPRECIATION_ID";
            // 
            // IS_CHECK_
            // 
            this.IS_CHECK_.DataPropertyName = "IS_CHECK";
            resources.ApplyResources(this.IS_CHECK_, "IS_CHECK_");
            this.IS_CHECK_.Name = "IS_CHECK_";
            this.IS_CHECK_.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.IS_CHECK_.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // ROW_NO
            // 
            this.ROW_NO.DataPropertyName = "ROW_NO";
            resources.ApplyResources(this.ROW_NO, "ROW_NO");
            this.ROW_NO.Name = "ROW_NO";
            this.ROW_NO.ReadOnly = true;
            // 
            // IS_DEPRECIATION
            // 
            this.IS_DEPRECIATION.DataPropertyName = "IS_DEPRECIATION";
            resources.ApplyResources(this.IS_DEPRECIATION, "IS_DEPRECIATION");
            this.IS_DEPRECIATION.Name = "IS_DEPRECIATION";
            this.IS_DEPRECIATION.ReadOnly = true;
            // 
            // DEPRECIATION_DATE
            // 
            this.DEPRECIATION_DATE.DataPropertyName = "DEPRECIATION_DATE";
            dataGridViewCellStyle2.Format = "hh:mm tt dd-MM-yyyy";
            this.DEPRECIATION_DATE.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.DEPRECIATION_DATE, "DEPRECIATION_DATE");
            this.DEPRECIATION_DATE.Name = "DEPRECIATION_DATE";
            // 
            // MONTH
            // 
            this.MONTH.DataPropertyName = "DEPRECIATION_MONTH";
            dataGridViewCellStyle3.Format = "MM-yyyy";
            dataGridViewCellStyle3.NullValue = null;
            this.MONTH.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.MONTH, "MONTH");
            this.MONTH.Name = "MONTH";
            this.MONTH.ReadOnly = true;
            // 
            // CREATE_BY_1
            // 
            this.CREATE_BY_1.DataPropertyName = "DEPRECIATION_BY";
            dataGridViewCellStyle4.Format = "yyyy-MM-dd";
            this.CREATE_BY_1.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.CREATE_BY_1, "CREATE_BY_1");
            this.CREATE_BY_1.Name = "CREATE_BY_1";
            this.CREATE_BY_1.ReadOnly = true;
            // 
            // ACCUMULATE_DEPRECIATION_1
            // 
            this.ACCUMULATE_DEPRECIATION_1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.ACCUMULATE_DEPRECIATION_1.DataPropertyName = "AMOUNT";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "#,##0.####";
            dataGridViewCellStyle5.NullValue = null;
            this.ACCUMULATE_DEPRECIATION_1.DefaultCellStyle = dataGridViewCellStyle5;
            resources.ApplyResources(this.ACCUMULATE_DEPRECIATION_1, "ACCUMULATE_DEPRECIATION_1");
            this.ACCUMULATE_DEPRECIATION_1.Name = "ACCUMULATE_DEPRECIATION_1";
            this.ACCUMULATE_DEPRECIATION_1.ReadOnly = true;
            // 
            // ACCUMULATED_DEPRECIATION
            // 
            this.ACCUMULATED_DEPRECIATION.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.ACCUMULATED_DEPRECIATION.DataPropertyName = "ACCUMULATED";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "#,##0.####";
            this.ACCUMULATED_DEPRECIATION.DefaultCellStyle = dataGridViewCellStyle6;
            resources.ApplyResources(this.ACCUMULATED_DEPRECIATION, "ACCUMULATED_DEPRECIATION");
            this.ACCUMULATED_DEPRECIATION.Name = "ACCUMULATED_DEPRECIATION";
            this.ACCUMULATED_DEPRECIATION.ReadOnly = true;
            // 
            // BOOK_VALUE
            // 
            this.BOOK_VALUE.DataPropertyName = "BOOK_VALUE";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "#,##0.####";
            this.BOOK_VALUE.DefaultCellStyle = dataGridViewCellStyle7;
            resources.ApplyResources(this.BOOK_VALUE, "BOOK_VALUE");
            this.BOOK_VALUE.Name = "BOOK_VALUE";
            this.BOOK_VALUE.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "CURRENCY_SING";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle8;
            resources.ApplyResources(this.dataGridViewTextBoxColumn1, "dataGridViewTextBoxColumn1");
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // STATUS_ID
            // 
            this.STATUS_ID.DataPropertyName = "STATUS_ID";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.Format = "N2";
            dataGridViewCellStyle9.NullValue = null;
            this.STATUS_ID.DefaultCellStyle = dataGridViewCellStyle9;
            resources.ApplyResources(this.STATUS_ID, "STATUS_ID");
            this.STATUS_ID.Name = "STATUS_ID";
            this.STATUS_ID.ReadOnly = true;
            // 
            // lblACCUMULATE_DEPRECIATION_1
            // 
            resources.ApplyResources(this.lblACCUMULATE_DEPRECIATION_1, "lblACCUMULATE_DEPRECIATION_1");
            this.lblACCUMULATE_DEPRECIATION_1.Name = "lblACCUMULATE_DEPRECIATION_1";
            // 
            // txtTotalAccumulated
            // 
            resources.ApplyResources(this.txtTotalAccumulated, "txtTotalAccumulated");
            this.txtTotalAccumulated.Name = "txtTotalAccumulated";
            this.txtTotalAccumulated.ReadOnly = true;
            // 
            // dtpSuggestionDate
            // 
            resources.ApplyResources(this.dtpSuggestionDate, "dtpSuggestionDate");
            this.dtpSuggestionDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSuggestionDate.Name = "dtpSuggestionDate";
            this.dtpSuggestionDate.ValueChanged += new System.EventHandler(this.cboStatus_SelectedIndexChanged);
            // 
            // cboDepreciationStatus
            // 
            this.cboDepreciationStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDepreciationStatus.FormattingEnabled = true;
            resources.ApplyResources(this.cboDepreciationStatus, "cboDepreciationStatus");
            this.cboDepreciationStatus.Name = "cboDepreciationStatus";
            this.cboDepreciationStatus.SelectedIndexChanged += new System.EventHandler(this.cboStatus_SelectedIndexChanged);
            // 
            // tabDISPOSE_DEPRECIATION
            // 
            this.tabDISPOSE_DEPRECIATION.Controls.Add(this.panel2);
            this.tabDISPOSE_DEPRECIATION.Controls.Add(this.txtTotalDisposeSoldValue);
            this.tabDISPOSE_DEPRECIATION.Controls.Add(this.txtTotalDisposeBookValue);
            this.tabDISPOSE_DEPRECIATION.Controls.Add(this.txtTotalDisposeGainLoss);
            this.tabDISPOSE_DEPRECIATION.Controls.Add(this.dgvDispose);
            this.tabDISPOSE_DEPRECIATION.Controls.Add(this.lblAMOUNT_2);
            resources.ApplyResources(this.tabDISPOSE_DEPRECIATION, "tabDISPOSE_DEPRECIATION");
            this.tabDISPOSE_DEPRECIATION.Name = "tabDISPOSE_DEPRECIATION";
            this.tabDISPOSE_DEPRECIATION.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnADD_2);
            this.panel2.Controls.Add(this.btnEDIT_2);
            this.panel2.Controls.Add(this.btnREMOVE_2);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // btnADD_2
            // 
            resources.ApplyResources(this.btnADD_2, "btnADD_2");
            this.btnADD_2.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnADD_2.Name = "btnADD_2";
            this.btnADD_2.TabStop = true;
            this.btnADD_2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblAddDepose_LinkClicked);
            // 
            // btnEDIT_2
            // 
            resources.ApplyResources(this.btnEDIT_2, "btnEDIT_2");
            this.btnEDIT_2.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnEDIT_2.Name = "btnEDIT_2";
            this.btnEDIT_2.TabStop = true;
            this.btnEDIT_2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblUpdateDepose_LinkClicked);
            // 
            // btnREMOVE_2
            // 
            resources.ApplyResources(this.btnREMOVE_2, "btnREMOVE_2");
            this.btnREMOVE_2.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnREMOVE_2.Name = "btnREMOVE_2";
            this.btnREMOVE_2.TabStop = true;
            this.btnREMOVE_2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblDeleteDepose_LinkClicked);
            // 
            // txtTotalDisposeSoldValue
            // 
            resources.ApplyResources(this.txtTotalDisposeSoldValue, "txtTotalDisposeSoldValue");
            this.txtTotalDisposeSoldValue.Name = "txtTotalDisposeSoldValue";
            this.txtTotalDisposeSoldValue.ReadOnly = true;
            // 
            // txtTotalDisposeBookValue
            // 
            resources.ApplyResources(this.txtTotalDisposeBookValue, "txtTotalDisposeBookValue");
            this.txtTotalDisposeBookValue.Name = "txtTotalDisposeBookValue";
            this.txtTotalDisposeBookValue.ReadOnly = true;
            // 
            // txtTotalDisposeGainLoss
            // 
            resources.ApplyResources(this.txtTotalDisposeGainLoss, "txtTotalDisposeGainLoss");
            this.txtTotalDisposeGainLoss.Name = "txtTotalDisposeGainLoss";
            this.txtTotalDisposeGainLoss.ReadOnly = true;
            // 
            // dgvDispose
            // 
            this.dgvDispose.AllowUserToAddRows = false;
            this.dgvDispose.AllowUserToDeleteRows = false;
            this.dgvDispose.AllowUserToResizeRows = false;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvDispose.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle11;
            this.dgvDispose.BackgroundColor = System.Drawing.Color.White;
            this.dgvDispose.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvDispose.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvDispose.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDispose.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DISPOSE_ID,
            this.DISPOSE_DATE,
            this.CREATE_BY,
            this.QUANTITY,
            this.PRICE_I,
            this.DISPOSE_SOLD_VALUE,
            this.BOOK_VALUE_2,
            this.GAIN_LOSS,
            this.CURRENCY_SIGN_,
            this.DISPOSE_CURRENCY_ID});
            this.dgvDispose.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgvDispose, "dgvDispose");
            this.dgvDispose.MultiSelect = false;
            this.dgvDispose.Name = "dgvDispose";
            this.dgvDispose.ReadOnly = true;
            this.dgvDispose.RowHeadersVisible = false;
            this.dgvDispose.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // lblAMOUNT_2
            // 
            resources.ApplyResources(this.lblAMOUNT_2, "lblAMOUNT_2");
            this.lblAMOUNT_2.Name = "lblAMOUNT_2";
            // 
            // tabSERVICE_AND_MAINTENAINCE
            // 
            this.tabSERVICE_AND_MAINTENAINCE.Controls.Add(this.panel3);
            this.tabSERVICE_AND_MAINTENAINCE.Controls.Add(this.label4);
            this.tabSERVICE_AND_MAINTENAINCE.Controls.Add(this.cboCurrencyMaintenance);
            this.tabSERVICE_AND_MAINTENAINCE.Controls.Add(this.dgvTotalAmountMaintenance);
            this.tabSERVICE_AND_MAINTENAINCE.Controls.Add(this.dgvMaintenance);
            resources.ApplyResources(this.tabSERVICE_AND_MAINTENAINCE, "tabSERVICE_AND_MAINTENAINCE");
            this.tabSERVICE_AND_MAINTENAINCE.Name = "tabSERVICE_AND_MAINTENAINCE";
            this.tabSERVICE_AND_MAINTENAINCE.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnADD_1);
            this.panel3.Controls.Add(this.btnEDIT_1);
            this.panel3.Controls.Add(this.btnDELETE_1);
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            // 
            // btnADD_1
            // 
            resources.ApplyResources(this.btnADD_1, "btnADD_1");
            this.btnADD_1.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnADD_1.Name = "btnADD_1";
            this.btnADD_1.TabStop = true;
            this.btnADD_1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblAddRepairMaintenance_LinkClicked);
            // 
            // btnEDIT_1
            // 
            resources.ApplyResources(this.btnEDIT_1, "btnEDIT_1");
            this.btnEDIT_1.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnEDIT_1.Name = "btnEDIT_1";
            this.btnEDIT_1.TabStop = true;
            this.btnEDIT_1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblEditRepairMaintenance_LinkClicked);
            // 
            // btnDELETE_1
            // 
            resources.ApplyResources(this.btnDELETE_1, "btnDELETE_1");
            this.btnDELETE_1.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnDELETE_1.Name = "btnDELETE_1";
            this.btnDELETE_1.TabStop = true;
            this.btnDELETE_1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblDeleteRepairMaintenance_LinkClicked);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // cboCurrencyMaintenance
            // 
            this.cboCurrencyMaintenance.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrencyMaintenance.FormattingEnabled = true;
            resources.ApplyResources(this.cboCurrencyMaintenance, "cboCurrencyMaintenance");
            this.cboCurrencyMaintenance.Name = "cboCurrencyMaintenance";
            this.cboCurrencyMaintenance.SelectedIndexChanged += new System.EventHandler(this.cboCurrencyMaintenance_SelectedIndexChanged);
            // 
            // dgvTotalAmountMaintenance
            // 
            this.dgvTotalAmountMaintenance.AllowUserToAddRows = false;
            this.dgvTotalAmountMaintenance.AllowUserToDeleteRows = false;
            this.dgvTotalAmountMaintenance.AllowUserToResizeColumns = false;
            this.dgvTotalAmountMaintenance.AllowUserToResizeRows = false;
            this.dgvTotalAmountMaintenance.BackgroundColor = System.Drawing.Color.White;
            this.dgvTotalAmountMaintenance.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvTotalAmountMaintenance.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvTotalAmountMaintenance.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTotalAmountMaintenance.ColumnHeadersVisible = false;
            this.dgvTotalAmountMaintenance.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn8,
            this.TOTAL_BALANCE_MAINTENANCE,
            this.dataGridViewTextBoxColumn9});
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            dataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvTotalAmountMaintenance.DefaultCellStyle = dataGridViewCellStyle20;
            this.dgvTotalAmountMaintenance.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgvTotalAmountMaintenance, "dgvTotalAmountMaintenance");
            this.dgvTotalAmountMaintenance.MultiSelect = false;
            this.dgvTotalAmountMaintenance.Name = "dgvTotalAmountMaintenance";
            this.dgvTotalAmountMaintenance.ReadOnly = true;
            this.dgvTotalAmountMaintenance.RowHeadersVisible = false;
            this.dgvTotalAmountMaintenance.RowTemplate.Height = 25;
            this.dgvTotalAmountMaintenance.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "CURRENCY_ID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn8, "dataGridViewTextBoxColumn8");
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // TOTAL_BALANCE_MAINTENANCE
            // 
            this.TOTAL_BALANCE_MAINTENANCE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TOTAL_BALANCE_MAINTENANCE.DataPropertyName = "TOTAL_BALANCE_MAINTENANCE";
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle19.Format = "#,##0.####";
            this.TOTAL_BALANCE_MAINTENANCE.DefaultCellStyle = dataGridViewCellStyle19;
            resources.ApplyResources(this.TOTAL_BALANCE_MAINTENANCE, "TOTAL_BALANCE_MAINTENANCE");
            this.TOTAL_BALANCE_MAINTENANCE.Name = "TOTAL_BALANCE_MAINTENANCE";
            this.TOTAL_BALANCE_MAINTENANCE.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.dataGridViewTextBoxColumn9.DataPropertyName = "CURRENCY_SING";
            this.dataGridViewTextBoxColumn9.FillWeight = 30F;
            resources.ApplyResources(this.dataGridViewTextBoxColumn9, "dataGridViewTextBoxColumn9");
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            // 
            // dgvMaintenance
            // 
            this.dgvMaintenance.AllowUserToAddRows = false;
            this.dgvMaintenance.AllowUserToDeleteRows = false;
            dataGridViewCellStyle21.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvMaintenance.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle21;
            this.dgvMaintenance.BackgroundColor = System.Drawing.Color.White;
            this.dgvMaintenance.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvMaintenance.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvMaintenance.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMaintenance.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.CURRENCY_ID,
            this.FIX_ASSET_ITEM_ID,
            this.DATE,
            this.CREATE_BY_2,
            this.REF_NO,
            this.PAYMENT_ACCOUNT,
            this.AMOUNT,
            this.CURRENCY_SING_});
            this.dgvMaintenance.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgvMaintenance, "dgvMaintenance");
            this.dgvMaintenance.Name = "dgvMaintenance";
            this.dgvMaintenance.ReadOnly = true;
            this.dgvMaintenance.RowHeadersVisible = false;
            this.dgvMaintenance.RowTemplate.Height = 25;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            resources.ApplyResources(this.ID, "ID");
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            // 
            // CURRENCY_ID
            // 
            this.CURRENCY_ID.DataPropertyName = "CURRENCY_ID";
            resources.ApplyResources(this.CURRENCY_ID, "CURRENCY_ID");
            this.CURRENCY_ID.Name = "CURRENCY_ID";
            this.CURRENCY_ID.ReadOnly = true;
            // 
            // FIX_ASSET_ITEM_ID
            // 
            this.FIX_ASSET_ITEM_ID.DataPropertyName = "FIX_ASSET_ITEM_ID";
            resources.ApplyResources(this.FIX_ASSET_ITEM_ID, "FIX_ASSET_ITEM_ID");
            this.FIX_ASSET_ITEM_ID.Name = "FIX_ASSET_ITEM_ID";
            this.FIX_ASSET_ITEM_ID.ReadOnly = true;
            // 
            // DATE
            // 
            this.DATE.DataPropertyName = "TRAN_DATE";
            dataGridViewCellStyle22.Format = "dd-MM-yyyy";
            dataGridViewCellStyle22.NullValue = null;
            this.DATE.DefaultCellStyle = dataGridViewCellStyle22;
            resources.ApplyResources(this.DATE, "DATE");
            this.DATE.Name = "DATE";
            this.DATE.ReadOnly = true;
            // 
            // CREATE_BY_2
            // 
            this.CREATE_BY_2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.CREATE_BY_2.DataPropertyName = "TRAN_BY";
            resources.ApplyResources(this.CREATE_BY_2, "CREATE_BY_2");
            this.CREATE_BY_2.Name = "CREATE_BY_2";
            this.CREATE_BY_2.ReadOnly = true;
            // 
            // REF_NO
            // 
            this.REF_NO.DataPropertyName = "REF_NO";
            resources.ApplyResources(this.REF_NO, "REF_NO");
            this.REF_NO.Name = "REF_NO";
            this.REF_NO.ReadOnly = true;
            // 
            // PAYMENT_ACCOUNT
            // 
            this.PAYMENT_ACCOUNT.DataPropertyName = "ACCOUNT_NAME";
            resources.ApplyResources(this.PAYMENT_ACCOUNT, "PAYMENT_ACCOUNT");
            this.PAYMENT_ACCOUNT.Name = "PAYMENT_ACCOUNT";
            this.PAYMENT_ACCOUNT.ReadOnly = true;
            // 
            // AMOUNT
            // 
            this.AMOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AMOUNT.DataPropertyName = "AMOUNT";
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle23.Format = "#,##0.####";
            dataGridViewCellStyle23.NullValue = null;
            this.AMOUNT.DefaultCellStyle = dataGridViewCellStyle23;
            resources.ApplyResources(this.AMOUNT, "AMOUNT");
            this.AMOUNT.Name = "AMOUNT";
            this.AMOUNT.ReadOnly = true;
            // 
            // CURRENCY_SING_
            // 
            this.CURRENCY_SING_.DataPropertyName = "CURRENCY_SING";
            resources.ApplyResources(this.CURRENCY_SING_, "CURRENCY_SING_");
            this.CURRENCY_SING_.Name = "CURRENCY_SING_";
            this.CURRENCY_SING_.ReadOnly = true;
            // 
            // DISPOSE_ID
            // 
            this.DISPOSE_ID.DataPropertyName = "DISPOSE_ID";
            resources.ApplyResources(this.DISPOSE_ID, "DISPOSE_ID");
            this.DISPOSE_ID.Name = "DISPOSE_ID";
            this.DISPOSE_ID.ReadOnly = true;
            // 
            // DISPOSE_DATE
            // 
            this.DISPOSE_DATE.DataPropertyName = "DISPOSE_TRAN_DATE";
            dataGridViewCellStyle12.Format = "hh:mm tt dd-MM-yyyy";
            dataGridViewCellStyle12.NullValue = null;
            this.DISPOSE_DATE.DefaultCellStyle = dataGridViewCellStyle12;
            resources.ApplyResources(this.DISPOSE_DATE, "DISPOSE_DATE");
            this.DISPOSE_DATE.Name = "DISPOSE_DATE";
            this.DISPOSE_DATE.ReadOnly = true;
            // 
            // CREATE_BY
            // 
            this.CREATE_BY.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.CREATE_BY.DataPropertyName = "CREATE_BY";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle13.Format = "N0";
            dataGridViewCellStyle13.NullValue = null;
            this.CREATE_BY.DefaultCellStyle = dataGridViewCellStyle13;
            resources.ApplyResources(this.CREATE_BY, "CREATE_BY");
            this.CREATE_BY.Name = "CREATE_BY";
            this.CREATE_BY.ReadOnly = true;
            // 
            // QUANTITY
            // 
            this.QUANTITY.DataPropertyName = "QTY";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle14.Format = "N2";
            dataGridViewCellStyle14.NullValue = null;
            this.QUANTITY.DefaultCellStyle = dataGridViewCellStyle14;
            resources.ApplyResources(this.QUANTITY, "QUANTITY");
            this.QUANTITY.Name = "QUANTITY";
            this.QUANTITY.ReadOnly = true;
            // 
            // PRICE_I
            // 
            this.PRICE_I.DataPropertyName = "PRICE";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle15.Format = "#,##0.####";
            dataGridViewCellStyle15.NullValue = null;
            this.PRICE_I.DefaultCellStyle = dataGridViewCellStyle15;
            resources.ApplyResources(this.PRICE_I, "PRICE_I");
            this.PRICE_I.Name = "PRICE_I";
            this.PRICE_I.ReadOnly = true;
            // 
            // DISPOSE_SOLD_VALUE
            // 
            this.DISPOSE_SOLD_VALUE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DISPOSE_SOLD_VALUE.DataPropertyName = "DISPOSE_SOLD_VALUE";
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle16.Format = "#,##0.####";
            dataGridViewCellStyle16.NullValue = null;
            this.DISPOSE_SOLD_VALUE.DefaultCellStyle = dataGridViewCellStyle16;
            resources.ApplyResources(this.DISPOSE_SOLD_VALUE, "DISPOSE_SOLD_VALUE");
            this.DISPOSE_SOLD_VALUE.Name = "DISPOSE_SOLD_VALUE";
            this.DISPOSE_SOLD_VALUE.ReadOnly = true;
            // 
            // BOOK_VALUE_2
            // 
            this.BOOK_VALUE_2.DataPropertyName = "DISPOSE_BOOK_VALUE";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle17.Format = "#,##0.####";
            this.BOOK_VALUE_2.DefaultCellStyle = dataGridViewCellStyle17;
            resources.ApplyResources(this.BOOK_VALUE_2, "BOOK_VALUE_2");
            this.BOOK_VALUE_2.Name = "BOOK_VALUE_2";
            this.BOOK_VALUE_2.ReadOnly = true;
            // 
            // GAIN_LOSS
            // 
            this.GAIN_LOSS.DataPropertyName = "DISPOSE_GAIN_LOSS";
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle18.Format = "#,##0.####";
            this.GAIN_LOSS.DefaultCellStyle = dataGridViewCellStyle18;
            resources.ApplyResources(this.GAIN_LOSS, "GAIN_LOSS");
            this.GAIN_LOSS.Name = "GAIN_LOSS";
            this.GAIN_LOSS.ReadOnly = true;
            // 
            // CURRENCY_SIGN_
            // 
            this.CURRENCY_SIGN_.DataPropertyName = "CURRENCY_SIGN";
            resources.ApplyResources(this.CURRENCY_SIGN_, "CURRENCY_SIGN_");
            this.CURRENCY_SIGN_.Name = "CURRENCY_SIGN_";
            this.CURRENCY_SIGN_.ReadOnly = true;
            // 
            // DISPOSE_CURRENCY_ID
            // 
            this.DISPOSE_CURRENCY_ID.DataPropertyName = "CURRENCY_ID";
            resources.ApplyResources(this.DISPOSE_CURRENCY_ID, "DISPOSE_CURRENCY_ID");
            this.DISPOSE_CURRENCY_ID.Name = "DISPOSE_CURRENCY_ID";
            this.DISPOSE_CURRENCY_ID.ReadOnly = true;
            // 
            // DialogFixAssetItem
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogFixAssetItem";
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.tabGENERAL_INFORMATION.ResumeLayout(false);
            this.tabGENERAL_INFORMATION.PerformLayout();
            this.tabDEPRECIATION.ResumeLayout(false);
            this.tabDEPRECIATION.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.tabDISPOSE_DEPRECIATION.ResumeLayout(false);
            this.tabDISPOSE_DEPRECIATION.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDispose)).EndInit();
            this.tabSERVICE_AND_MAINTENAINCE.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTotalAmountMaintenance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaintenance)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnCLOSE;
        private ExButton btnOK;
        private ExButton btnCHANGE_LOG;
        private TabControl tabControl;
        private TabPage tabSERVICE_AND_MAINTENAINCE;
        private TabPage tabDEPRECIATION;
        private ComboBox cboDepreciationStatus;
        private DateTimePicker dtpSuggestionDate;
        private DataGridView dgv;
        private Label lblACCUMULATE_DEPRECIATION_1;
        private TextBox txtTotalAccumulated;
        private TabPage tabGENERAL_INFORMATION;
        private TextBox txtFixAssetName;
        private Label label13;
        private Label lblPRICE_PER_UNIT;
        private DateTimePicker dtpUseDate;
        private TextBox txtBrand;
        private Label lblBRAND;
        private Label lblSTART_USE_DATE;
        private Label lblSOURCE;
        private Label label27;
        private TextBox txtSource;
        private Label label16;
        private Label lblUNIT;
        private Label label38;
        private Label lblQUANTITY;
        private Label lblACCUMULATE_DEPRECIATION;
        private Label lblAMOUNT;
        private Label lblANNUAL_DEPRECIATION_RATE;
        private Label lblFIX_ASSET;
        private TextBox txtUnit;
        private TextBox txtQty;
        private TextBox txtPrice;
        private Label label39;
        private TextBox txtAccDepreciation;
        private TextBox txtSalvageValue;
        private TextBox txtAmount;
        private TextBox txtUseFulLife;
        private TextBox txtDepreciationRate;
        private Label lblSALVAGE_VALUE;
        private Label label28;
        private Label label33;
        private Label lblDEPRECIATION_PERIOD;
        private TextBox txtProductStatus;
        private Label label30;
        private Label lblPRODUCT_STATUS;
        private Label lblYEAR_OF_PRODUCTION;
        private Label label24;
        private Label lblCURRENCY;
        public ComboBox cboCurrency;
        private Label lblFIX_ASSET_TYPE;
        private Label lblNOTE;
        private Label label1;
        private Label label2;
        private TextBox txtNote;
        private TreeComboBox cboCategory;
        private TreeComboBox cboPaymentAccount;
        private Label lblPAYMENT_ACCOUNT;
        private Label label5;
        private DataGridView dgvMaintenance;
        private ExLinkLabel btnADD_1;
        private ExLinkLabel btnEDIT_1;
        private ExLinkLabel btnDELETE_1;
        private DataGridView dgvTotalAmountMaintenance;
        private Label label4;
        public ComboBox cboCurrencyMaintenance;
        private TextBox txtProductionYear;
        private TextBox txtBookValue;
        private Label lblBOOK_VALUE_1;
        private ExLinkLabel lblREMOVE_DEPRECIATION;
        private ExLinkLabel lblDEPRECIATION;
        private CheckBox chbCheckAll_;
        private TabPage tabDISPOSE_DEPRECIATION;
        private TextBox txtTotalDisposeGainLoss;
        private ExLinkLabel btnADD_2;
        private ExLinkLabel btnEDIT_2;
        private ExLinkLabel btnREMOVE_2;
        private DataGridView dgvDispose;
        private Label lblAMOUNT_2;
        private TextBox txtTotalDisposeSoldValue;
        private TextBox txtTotalDisposeBookValue;
        private Panel panel1;
        private Panel panel2;
        private Panel panel3;
        private DataGridViewTextBoxColumn DEPRECIATION_ID;
        private DataGridViewCheckBoxColumn IS_CHECK_;
        private DataGridViewTextBoxColumn ROW_NO;
        private DataGridViewCheckBoxColumn IS_DEPRECIATION;
        private DataGridViewTextBoxColumn DEPRECIATION_DATE;
        private DataGridViewTextBoxColumn MONTH;
        private DataGridViewTextBoxColumn CREATE_BY_1;
        private DataGridViewTextBoxColumn ACCUMULATE_DEPRECIATION_1;
        private DataGridViewTextBoxColumn ACCUMULATED_DEPRECIATION;
        private DataGridViewTextBoxColumn BOOK_VALUE;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn STATUS_ID;
        private DataGridViewTextBoxColumn ID;
        private DataGridViewTextBoxColumn CURRENCY_ID;
        private DataGridViewTextBoxColumn FIX_ASSET_ITEM_ID;
        private DataGridViewTextBoxColumn DATE;
        private DataGridViewTextBoxColumn CREATE_BY_2;
        private DataGridViewTextBoxColumn REF_NO;
        private DataGridViewTextBoxColumn PAYMENT_ACCOUNT;
        private DataGridViewTextBoxColumn AMOUNT;
        private DataGridViewTextBoxColumn CURRENCY_SING_;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private DataGridViewTextBoxColumn TOTAL_BALANCE_MAINTENANCE;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private DataGridViewTextBoxColumn DISPOSE_ID;
        private DataGridViewTextBoxColumn DISPOSE_DATE;
        private DataGridViewTextBoxColumn CREATE_BY;
        private DataGridViewTextBoxColumn QUANTITY;
        private DataGridViewTextBoxColumn PRICE_I;
        private DataGridViewTextBoxColumn DISPOSE_SOLD_VALUE;
        private DataGridViewTextBoxColumn BOOK_VALUE_2;
        private DataGridViewTextBoxColumn GAIN_LOSS;
        private DataGridViewTextBoxColumn CURRENCY_SIGN_;
        private DataGridViewTextBoxColumn DISPOSE_CURRENCY_ID;
    }
}