﻿using EPower.Base.Helper;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using Login = SoftTech.Security.Logic.Login;

namespace EPower.Interface
{
    public partial class DialogFixAssetRepairMaintenance : ExDialog
    {
        GeneralProcess _flag;


        TBL_FIX_ASSET_ITEM _objFixAsset = new TBL_FIX_ASSET_ITEM();
        TBL_FIX_ASSET_ITEM _objOldFixAsset = new TBL_FIX_ASSET_ITEM();
        TBL_FIX_ASSET_MAINTENANCE _objNew = new TBL_FIX_ASSET_MAINTENANCE();
        TBL_FIX_ASSET_MAINTENANCE _objOld = new TBL_FIX_ASSET_MAINTENANCE();
        

        #region Constructor
        public DialogFixAssetRepairMaintenance(GeneralProcess flag, TBL_FIX_ASSET_ITEM objFixAsset, TBL_FIX_ASSET_MAINTENANCE objFixAssetRepairMaintenance)
        {
            InitializeComponent();

            _flag = flag;

            new AccountChartPopulator().PopluateTree(cboPaymentAccount.TreeView, AccountChartHelper.GetAccounts(AccountConfig.PAYMENT_ACCOUNTS));
            new AccountChartPopulator().PopluateTree(cboAccountExpense.TreeView, AccountChartHelper.GetAccounts(AccountConfig.EXPENSE_MAINTENANCE_ACCOUNTS));
            UIHelper.SetDataSourceToComboBox(cboCurrency, Lookup.GetCurrencies());
            var tranBy = from a in DBDataContext.Db.TBL_FIX_ASSET_MAINTENANCEs
                        where a.IS_ACTIVE
                        select new { a.TRAN_BY };
            foreach (var item in tranBy.Distinct())
            {
                txtTranBy.AutoCompleteCustomSource.Add(item.TRAN_BY);
            }

            objFixAsset._CopyTo(_objFixAsset);
            objFixAsset._CopyTo(_objOldFixAsset);
            objFixAssetRepairMaintenance._CopyTo(_objNew);
            objFixAssetRepairMaintenance._CopyTo(_objOld);
            read();
            UIHelper.SetEnabled(this, !(flag == GeneralProcess.Delete));
        }

       

        #endregion

        #region Operation
        /// <summary>
        /// Change Keyboard to Khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKeyboardKhmer(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        private void ChangeKeyboardEnglish(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
         

        /// <summary>
        /// Data transaction.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
             //If record is duplicate.
            this.ClearAllValidation(); 

            try
            {
                using (TransactionScope tran=new TransactionScope( TransactionScopeOption.Required,TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objNew);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objNew);
                    }  
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }                                               
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }            
        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
        #endregion

        #region Method
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();
            if (cboPaymentAccount.TreeView.SelectedNode == null)
            {
                cboPaymentAccount.SetValidation(string.Format(Resources.REQUIRED, lblPaymentAccount.Text));
                val = true;
            }
            if (cboAccountExpense.TreeView.SelectedNode == null)
            {
                cboAccountExpense.SetValidation(string.Format(Resources.REQUIRED, lblAccountExpense.Text));
                val = true;
            }
            if (txtTranBy.Text.Trim() == string.Empty)
            {
                txtTranBy.SetValidation(string.Format(Resources.REQUIRED, lblTranBy.Text));
                val = true;
            } 
            if (txtAmount.Text.Trim() == string.Empty)
            {
                txtAmount.SetValidation(string.Format(Resources.REQUIRED, lblSoldAmount.Text));
                val = true;
            }
            if (DataHelper.ParseToDecimal(txtAmount.Text) < 0)
            {
                txtAmount.SetValidation(string.Format(Resources.REQUIRED_POSITIVE_NUMBER, lblSoldAmount.Text));
                val = true;
            }
            
            return val;
        }

        /// <summary>
        /// Read data from object.
        /// </summary>
        private void read()
        {
            cboPaymentAccount.TreeView.SelectedNode = this.cboPaymentAccount.TreeView.GetNodesIncludeAncestors()
                                                   .FirstOrDefault(x => (int)x.Tag == this._objNew.PAYMENT_ACCOUNT_ID);
            if (cboPaymentAccount.TreeView.SelectedNode != null)
            {
                this.cboPaymentAccount.Text = cboPaymentAccount.TreeView.SelectedNode.Text;
            }

            cboAccountExpense.TreeView.SelectedNode = this.cboAccountExpense.TreeView.GetNodesIncludeAncestors()
                                                  .FirstOrDefault(x => (int)x.Tag == this._objNew.EXPENSE_MAINTENANCE_ACCOUNT_ID);
            if (cboAccountExpense.TreeView.SelectedNode != null)
            {
                this.cboAccountExpense.Text = cboAccountExpense.TreeView.SelectedNode.Text;
            }

            txtFixAssetName.Text = _objFixAsset.FIX_ASSET_NAME; 
            cboCurrency.SelectedValue = _objNew.CURRENCY_ID;
            txtTranBy.Text = _objNew.TRAN_BY;
            txtRefNo.Text = _objNew.REF_NO;
            txtNote.Text = _objNew.NOTE;
            dtpTranDate.Value = _objNew.TRAN_DATE;
            txtAmount.Text =UIHelper.FormatCurrency(_objNew.AMOUNT,_objNew.CURRENCY_ID);
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            _objNew.FIX_ASSET_ITEM_ID = _objFixAsset.FIX_ASSET_ITEM_ID;
            _objNew.CURRENCY_ID = (int)cboCurrency.SelectedValue;
            _objNew.AMOUNT = DataHelper.ParseToDecimal(txtAmount.Text);
            _objNew.PAYMENT_ACCOUNT_ID = (int)cboPaymentAccount.TreeView.SelectedNode.Tag;
            _objNew.EXPENSE_MAINTENANCE_ACCOUNT_ID = (int)cboAccountExpense.TreeView.SelectedNode.Tag;
            _objNew.TRAN_BY = txtTranBy.Text;
            _objNew.TRAN_DATE = dtpTranDate.Value;
            _objNew.REF_NO = txtRefNo.Text;
            _objNew.NOTE = txtNote.Text;
            _objNew.CREATE_BY = Login.CurrentLogin.LOGIN_NAME;
            _objNew.CREATE_ON = DBDataContext.Db.GetSystemDate();
            _objNew.IS_ACTIVE = true;
        }
        #endregion

        private void DialogAccountCategory_Load(object sender, EventArgs e)
        {
            this.txtTranBy.Focus();
        }

        private void txtAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        } 
    }
}
