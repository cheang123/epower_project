﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogExchangeRate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogExchangeRate));
            this.cboCurrencyFrom = new System.Windows.Forms.ComboBox();
            this.lblDATE = new System.Windows.Forms.Label();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.lblCurrency = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.rDivisor1 = new System.Windows.Forms.RadioButton();
            this.rMultiplier1 = new System.Windows.Forms.RadioButton();
            this.lblTO = new System.Windows.Forms.Label();
            this.cboCurrencyTo = new System.Windows.Forms.ComboBox();
            this.txtExchangeRate = new System.Windows.Forms.TextBox();
            this.lblExchangeRate1 = new System.Windows.Forms.Label();
            this.btnSAVE = new SoftTech.Component.ExButton();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.EXCHANGE_RATE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_ON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_BY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EXCHANGE_RATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MULTIPLIER_METHOD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblHISTORY_EXCHANGE_RATE = new System.Windows.Forms.Label();
            this.lblRATE = new System.Windows.Forms.Label();
            this.btnREMOVE = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnEDIT = new SoftTech.Component.ExLinkLabel(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.lblRATE);
            this.content.Controls.Add(this.lblHISTORY_EXCHANGE_RATE);
            this.content.Controls.Add(this.dgv);
            this.content.Controls.Add(this.btnSAVE);
            this.content.Controls.Add(this.lblExchangeRate1);
            this.content.Controls.Add(this.txtExchangeRate);
            this.content.Controls.Add(this.rMultiplier1);
            this.content.Controls.Add(this.rDivisor1);
            this.content.Controls.Add(this.cboCurrencyTo);
            this.content.Controls.Add(this.lblTO);
            this.content.Controls.Add(this.dtpDate);
            this.content.Controls.Add(this.lblDATE);
            this.content.Controls.Add(this.lblCurrency);
            this.content.Controls.Add(this.label12);
            this.content.Controls.Add(this.cboCurrencyFrom);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.cboCurrencyFrom, 0);
            this.content.Controls.SetChildIndex(this.label12, 0);
            this.content.Controls.SetChildIndex(this.lblCurrency, 0);
            this.content.Controls.SetChildIndex(this.lblDATE, 0);
            this.content.Controls.SetChildIndex(this.dtpDate, 0);
            this.content.Controls.SetChildIndex(this.lblTO, 0);
            this.content.Controls.SetChildIndex(this.cboCurrencyTo, 0);
            this.content.Controls.SetChildIndex(this.rDivisor1, 0);
            this.content.Controls.SetChildIndex(this.rMultiplier1, 0);
            this.content.Controls.SetChildIndex(this.txtExchangeRate, 0);
            this.content.Controls.SetChildIndex(this.lblExchangeRate1, 0);
            this.content.Controls.SetChildIndex(this.btnSAVE, 0);
            this.content.Controls.SetChildIndex(this.dgv, 0);
            this.content.Controls.SetChildIndex(this.lblHISTORY_EXCHANGE_RATE, 0);
            this.content.Controls.SetChildIndex(this.lblRATE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            // 
            // cboCurrencyFrom
            // 
            this.cboCurrencyFrom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrencyFrom.FormattingEnabled = true;
            resources.ApplyResources(this.cboCurrencyFrom, "cboCurrencyFrom");
            this.cboCurrencyFrom.Name = "cboCurrencyFrom";
            this.cboCurrencyFrom.SelectedIndexChanged += new System.EventHandler(this.cboCurrencyFrom_SelectedIndexChanged);
            // 
            // lblDATE
            // 
            resources.ApplyResources(this.lblDATE, "lblDATE");
            this.lblDATE.Name = "lblDATE";
            // 
            // dtpDate
            // 
            resources.ApplyResources(this.dtpDate, "dtpDate");
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.ValueChanged += new System.EventHandler(this.dtpDate_ValueChanged);
            // 
            // lblCurrency
            // 
            resources.ApplyResources(this.lblCurrency, "lblCurrency");
            this.lblCurrency.Name = "lblCurrency";
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.Name = "label12";
            // 
            // rDivisor1
            // 
            resources.ApplyResources(this.rDivisor1, "rDivisor1");
            this.rDivisor1.Name = "rDivisor1";
            this.rDivisor1.UseVisualStyleBackColor = true;
            // 
            // rMultiplier1
            // 
            resources.ApplyResources(this.rMultiplier1, "rMultiplier1");
            this.rMultiplier1.Checked = true;
            this.rMultiplier1.Name = "rMultiplier1";
            this.rMultiplier1.TabStop = true;
            this.rMultiplier1.UseVisualStyleBackColor = true;
            // 
            // lblTO
            // 
            resources.ApplyResources(this.lblTO, "lblTO");
            this.lblTO.Name = "lblTO";
            // 
            // cboCurrencyTo
            // 
            this.cboCurrencyTo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrencyTo.FormattingEnabled = true;
            resources.ApplyResources(this.cboCurrencyTo, "cboCurrencyTo");
            this.cboCurrencyTo.Name = "cboCurrencyTo";
            this.cboCurrencyTo.SelectedIndexChanged += new System.EventHandler(this.cboCurrencyTo_SelectedIndexChanged);
            // 
            // txtExchangeRate
            // 
            this.txtExchangeRate.AcceptsTab = true;
            resources.ApplyResources(this.txtExchangeRate, "txtExchangeRate");
            this.txtExchangeRate.Name = "txtExchangeRate";
            this.txtExchangeRate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtExchangeRate_KeyDown);
            // 
            // lblExchangeRate1
            // 
            resources.ApplyResources(this.lblExchangeRate1, "lblExchangeRate1");
            this.lblExchangeRate1.Name = "lblExchangeRate1";
            // 
            // btnSAVE
            // 
            resources.ApplyResources(this.btnSAVE, "btnSAVE");
            this.btnSAVE.Name = "btnSAVE";
            this.btnSAVE.UseVisualStyleBackColor = true;
            this.btnSAVE.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EXCHANGE_RATE_ID,
            this.CREATE_ON,
            this.CREATE_BY,
            this.EXCHANGE_RATE,
            this.MULTIPLIER_METHOD});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgv.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellDoubleClick);
            this.dgv.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgv_KeyDown);
            // 
            // EXCHANGE_RATE_ID
            // 
            this.EXCHANGE_RATE_ID.DataPropertyName = "EXCHANGE_RATE_ID";
            resources.ApplyResources(this.EXCHANGE_RATE_ID, "EXCHANGE_RATE_ID");
            this.EXCHANGE_RATE_ID.Name = "EXCHANGE_RATE_ID";
            this.EXCHANGE_RATE_ID.ReadOnly = true;
            // 
            // CREATE_ON
            // 
            this.CREATE_ON.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CREATE_ON.DataPropertyName = "CREATE_ON";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.Format = "dd-MM-yyyy HH:mm:ss";
            dataGridViewCellStyle2.NullValue = null;
            this.CREATE_ON.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.CREATE_ON, "CREATE_ON");
            this.CREATE_ON.Name = "CREATE_ON";
            this.CREATE_ON.ReadOnly = true;
            // 
            // CREATE_BY
            // 
            this.CREATE_BY.DataPropertyName = "CREATE_BY";
            this.CREATE_BY.FillWeight = 150F;
            resources.ApplyResources(this.CREATE_BY, "CREATE_BY");
            this.CREATE_BY.Name = "CREATE_BY";
            this.CREATE_BY.ReadOnly = true;
            // 
            // EXCHANGE_RATE
            // 
            this.EXCHANGE_RATE.DataPropertyName = "EXCHANGE_RATE";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "#,##0.####";
            dataGridViewCellStyle3.NullValue = null;
            this.EXCHANGE_RATE.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.EXCHANGE_RATE, "EXCHANGE_RATE");
            this.EXCHANGE_RATE.Name = "EXCHANGE_RATE";
            this.EXCHANGE_RATE.ReadOnly = true;
            // 
            // MULTIPLIER_METHOD
            // 
            this.MULTIPLIER_METHOD.DataPropertyName = "MULTIPLIER_METHOD";
            resources.ApplyResources(this.MULTIPLIER_METHOD, "MULTIPLIER_METHOD");
            this.MULTIPLIER_METHOD.Name = "MULTIPLIER_METHOD";
            this.MULTIPLIER_METHOD.ReadOnly = true;
            // 
            // lblHISTORY_EXCHANGE_RATE
            // 
            resources.ApplyResources(this.lblHISTORY_EXCHANGE_RATE, "lblHISTORY_EXCHANGE_RATE");
            this.lblHISTORY_EXCHANGE_RATE.Name = "lblHISTORY_EXCHANGE_RATE";
            // 
            // lblRATE
            // 
            resources.ApplyResources(this.lblRATE, "lblRATE");
            this.lblRATE.Name = "lblRATE";
            // 
            // btnREMOVE
            // 
            resources.ApplyResources(this.btnREMOVE, "btnREMOVE");
            this.btnREMOVE.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnREMOVE.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnREMOVE.Name = "btnREMOVE";
            this.btnREMOVE.TabStop = true;
            this.btnREMOVE.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnREMOVE.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnRemove_LinkClicked);
            // 
            // btnEDIT
            // 
            resources.ApplyResources(this.btnEDIT, "btnEDIT");
            this.btnEDIT.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnEDIT.ForeColor = System.Drawing.Color.Red;
            this.btnEDIT.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnEDIT.Name = "btnEDIT";
            this.btnEDIT.TabStop = true;
            this.btnEDIT.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnEDIT.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnEdit_LinkClicked);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnEDIT);
            this.panel1.Controls.Add(this.btnREMOVE);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // DialogExchangeRate
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogExchangeRate";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private ComboBox cboCurrencyFrom;
        private DateTimePicker dtpDate;
        private Label lblDATE;
        private Label lblCurrency;
        private Label label12;
        private ComboBox cboCurrencyTo;
        private Label lblTO;
        private RadioButton rDivisor1;
        private RadioButton rMultiplier1;
        private Label lblExchangeRate1;
        private TextBox txtExchangeRate;
        private ExButton btnSAVE;
        private Label lblHISTORY_EXCHANGE_RATE;
        private DataGridView dgv;
        private Label lblRATE;
        private ExLinkLabel btnREMOVE;
        private ExLinkLabel btnEDIT;
        private Panel panel1;
        private DataGridViewTextBoxColumn EXCHANGE_RATE_ID;
        private DataGridViewTextBoxColumn CREATE_ON;
        private DataGridViewTextBoxColumn CREATE_BY;
        private DataGridViewTextBoxColumn EXCHANGE_RATE;
        private DataGridViewTextBoxColumn MULTIPLIER_METHOD;
    }
}