﻿using System;
using System.Data;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class DialogDepreciationMonthly : ExDialog
    {
        bool load = false;
        public bool isDepreciation = false;
        #region Constructor
        public DialogDepreciationMonthly()
        {
            InitializeComponent();
            UIHelper.SetDataSourceToComboBox(cboCurrency, Lookup.GetCurrencies(), Resources.ALL_CURRENCY); 
            readDepreciation(); 
            load = true;
        } 

        #endregion

        #region Method
        private void readDepreciation()
        {
            Runner.Run(delegate
            {
                DataTable depreciation = (from d in DBDataContext.Db.TBL_DEPRECIATIONs
                                          join c in DBDataContext.Db.TLKP_CURRENCies on d.CURRENCY_ID equals c.CURRENCY_ID
                                          join i in DBDataContext.Db.TBL_FIX_ASSET_ITEMs on d.FIX_ASSET_ITEM_ID equals i.FIX_ASSET_ITEM_ID
                                          where d.IS_ACTIVE
                                               && d.STATUS_ID == (int)DepreciationStatus.ReadyDepreciation
                                               && (!dtpYear.Checked || d.DEPRECIATION_MONTH.Year == dtpYear.Value.Year)
                                               && i.STATUS_ID == (int)FixAssetItemStatus.USING
                                               && i.IS_ACTIVE
                                               && ((int)cboCurrency.SelectedValue == 0 || d.CURRENCY_ID == (int)cboCurrency.SelectedValue)
                                          orderby d.CURRENCY_ID, d.DEPRECIATION_MONTH
                                          select new
                                          {
                                              d.DEPRECIATION_MONTH,
                                              AMOUNT = d.AMOUNT,
                                              c.CURRENCY_SING,
                                              c.CURRENCY_ID
                                          })._ToDataTable();
                depreciation = depreciation.Rows.Cast<DataRow>().Select(x => new
                {
                    DEPRECIATION_MONTH = (DateTime)x["DEPRECIATION_MONTH"],
                    AMOUNT = DataHelper.ParseToDecimal(x["AMOUNT"].ToString()),
                    CURRENCY_SING = x["CURRENCY_SING"].ToString(),
                    CURRENCY_ID = (int)x["CURRENCY_ID"]
                })
                                    .GroupBy(x => new { x.CURRENCY_ID, x.CURRENCY_SING, x.DEPRECIATION_MONTH })
                                    .Select(x => new
                                    {
                                        IS_DEPRECIATION = false,
                                        x.Key.DEPRECIATION_MONTH,
                                        AMOUNT = x.Sum(r => r.AMOUNT),
                                        TOTAL_FIX_ASSET_ITEM = x.Count(),
                                        x.Key.CURRENCY_ID,
                                        x.Key.CURRENCY_SING,
                                    })._ToDataTable();
                dgv.DataSource = depreciation;
            }, Resources.PROCESSING); 
            getTotalDepreciation();
            chkCheckAll_.Checked = false;
        }

        private void getTotalDepreciation()
        {
            Runner.Run(delegate
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("CURRENCY_SING");
                dt.Columns.Add("AMOUNT", typeof(decimal));

                foreach (DataGridViewRow row in this.dgv.Rows)
                {
                    dt.Rows.Add(row.Cells[CURRENCY_SING_.Name].Value.ToString(), (decimal)row.Cells[AMOUNT_DEPRECIATION.Name].Value);
                }
                var dBalance = dt.Rows.Cast<DataRow>().Select(x => new
                {
                    AMOUNT = DataHelper.ParseToDecimal(x["AMOUNT"].ToString()),
                    CURRENCY_SING = x["CURRENCY_SING"].ToString()
                }).GroupBy(x => new { x.CURRENCY_SING })
                          .Select(x => new
                          {
                              x.Key.CURRENCY_SING,
                              AMOUNT = x.Sum(r => r.AMOUNT)
                          })._ToDataTable();
                dgvTotal.DataSource = dBalance;
            }, Resources.PROCESSING);
        }

        private void loadDetail()
        {
            int currencyId = (int)dgv.SelectedRows[0].Cells[CURRENCY_ID.Name].Value;
            DateTime date = (DateTime)dgv.SelectedRows[0].Cells[DEPRECIATION_DATE.Name].Value;
            new DialogDepreciationByMonthDetail(date, currencyId,(int)DepreciationStatus.ReadyDepreciation).ShowDialog();
        }

        #endregion

        private void chkCheckAll_CheckedChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in this.dgv.Rows)
            {
                row.Cells[IS_DEPRECIATION_.Name].Value = this.chkCheckAll_.Checked;  
            } 
        }  

        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1 && e.ColumnIndex == dgv.Columns[this.IS_DEPRECIATION_.Name].Index)
            {
                dgv.Rows[e.RowIndex].Cells[this.IS_DEPRECIATION_.Name].Value = !(bool)dgv.Rows[e.RowIndex].Cells[IS_DEPRECIATION_.Name].Value; 
            }
        }

        private void dtpYear_ValueChanged(object sender, EventArgs e)
        {
            if (load)
            {
                readDepreciation();
            } 
        }
 
        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgv.Rows.Count > 0 && e.RowIndex > -1)
            {
                loadDetail();
            }
        }

        private void lnkDepreciation_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DialogDepreciationByMonth dig = new DialogDepreciationByMonth();
            if (dig.ShowDialog() == DialogResult.OK)
            {
                isDepreciation = true;
                readDepreciation();
            }
        }

        private void lnkDetail_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (dgv.Rows.Count > 0)
            {
                loadDetail();
            }
        }

        private void lnkDeleteDepreciation_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                bool IsHasDepreciation = false;
                foreach (DataGridViewRow row in this.dgv.Rows)
                {
                    if ((bool)row.Cells[IS_DEPRECIATION_.Name].Value == true)
                    {
                        IsHasDepreciation = true;
                        break;
                    }
                }
                if (IsHasDepreciation)
                {
                    if (MsgBox.ShowQuestion(Resources.MSQ_CONFIRM_TO_DELETE_DEPRICIATION, Resources.DELETE) == DialogResult.Yes)
                    {
                        Runner.Run(delegate
                        {
                            using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                            {
                                DateTime date = DBDataContext.Db.GetSystemDate();
                                string depreciatinoBy = Login.CurrentLogin.LOGIN_NAME;
                                foreach (DataGridViewRow row in this.dgv.Rows)
                                {
                                    if ((bool)row.Cells[IS_DEPRECIATION_.Name].Value == true)
                                    {
                                        DateTime depreciationMonth = (DateTime)row.Cells[DEPRECIATION_DATE.Name].Value;
                                        int currencyId=(int)row.Cells[CURRENCY_ID.Name].Value;
                                        var db = from d in DBDataContext.Db.TBL_DEPRECIATIONs
                                                 join i in DBDataContext.Db.TBL_FIX_ASSET_ITEMs on d.FIX_ASSET_ITEM_ID equals i.FIX_ASSET_ITEM_ID
                                                 where d.DEPRECIATION_MONTH == depreciationMonth
                                                      && d.IS_ACTIVE
                                                      && d.STATUS_ID == (int)DepreciationStatus.ReadyDepreciation
                                                      && i.STATUS_ID == (int)FixAssetItemStatus.USING
                                                      && d.CURRENCY_ID==currencyId
                                                 select new
                                                 {
                                                     d.DEPRECIATION_ID,
                                                     d.FIX_ASSET_ITEM_ID
                                                 };
                                        foreach (var item in db)
                                        {
                                            // UPDATE TBL_DEPRECIATION
                                            TBL_DEPRECIATION objDepreciation = DBDataContext.Db.TBL_DEPRECIATIONs.FirstOrDefault(x => x.DEPRECIATION_ID == item.DEPRECIATION_ID);
                                            objDepreciation.STATUS_ID = (int)DepreciationStatus.NotYet;
                                            objDepreciation.DEPRECIATION_BY = "";
                                            objDepreciation.ROW_DATE = date;

                                            // UPDATE TBL_FIX_ASSET_ITEM
                                            TBL_FIX_ASSET_ITEM objFixAssetItem = DBDataContext.Db.TBL_FIX_ASSET_ITEMs.FirstOrDefault(x => x.FIX_ASSET_ITEM_ID == item.FIX_ASSET_ITEM_ID);
                                            objFixAssetItem.ACCUMULATED_DEPRECIATION -= objDepreciation.AMOUNT;
                                            objFixAssetItem.ROW_DATE = date;

                                            if (DBDataContext.Db.TBL_FIX_ASSET_DISPOSEs.Where(x => x.FIX_ASSET_ITEM_ID == objFixAssetItem.FIX_ASSET_ITEM_ID && x.IS_ACTIVE
                                                                                           && x.TRAN_DATE > objDepreciation.DEPRECIATION_DATE).Count() > 0)
                                            {
                                                MsgBox.ShowInformation(string.Format(Resources.MS_CANNOT_DELETE_DEPRICIATION_MONTHLY,objFixAssetItem.FIX_ASSET_NAME,objDepreciation.DEPRECIATION_MONTH.ToString("MM-yyyy")), Resources.DELETE);
                                                return;
                                            }
                                            
                                            DBDataContext.Db.SubmitChanges();
                                        }
                                    }
                                }
                                tran.Complete();
                            }
                        }, Resources.PROCESSING);
                        readDepreciation();
                        isDepreciation = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            } 
        }

        private void DialogDepreciationMonthly_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (isDepreciation)
            {
                this.DialogResult=DialogResult.OK;
            }
        } 
         
    }
}
