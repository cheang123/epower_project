﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;

namespace EPower.Interface  
{
    public class AccountChartPopulator
    {
        List<TBL_ACCOUNT_CHART> accounts = new List<TBL_ACCOUNT_CHART>();
        TreeView tvw = null;
        public void PopluateTree(TreeView tvw,List<TBL_ACCOUNT_CHART> accounts = null)
        {
            this.tvw = tvw;
            this.tvw.Nodes.Clear(); 
            this.accounts = accounts?? DBDataContext.Db.TBL_ACCOUNT_CHARTs.Where(x => x.IS_ACTIVE).OrderBy(x => x.ACCOUNT_CODE + " " + x.ACCOUNT_NAME).ToList();
           
            if (accounts == null)
            {
                var node = new TreeNode();
                node.Text = string.Format(Resources.ALL_ACCOUNT_ITEM);
                node.Tag = 0; // parent categothisry 
                this.tvw.Nodes.Add(node);
                addChildNode(node);
            }
            else
            {
                foreach (var account in accounts.Where(x => accounts.Select(t => t.ACCOUNT_ID).Contains(x.PARENT_ID)==false))
                {
                    var node = new TreeNode();
                    node.Text = account.ACCOUNT_CODE + " " + account.ACCOUNT_NAME;
                    node.Tag = account.ACCOUNT_ID;
                    this.tvw.Nodes.Add(node);
                    addChildNode(node);
                } 
            }
        }

        private void addChildNode(TreeNode node)
        {
            var id = (int)node.Tag;
            foreach (var account in accounts.Where(x => x.PARENT_ID == id && x.IS_ACTIVE).OrderBy(x=>x.ACCOUNT_CODE+" " +x.ACCOUNT_NAME))
            {
                var childNode = new TreeNode();
                childNode.Text = account.ACCOUNT_CODE + " " + account.ACCOUNT_NAME;
                childNode.Tag = account.ACCOUNT_ID;
                node.Nodes.Add(childNode);
                addChildNode(childNode);
            }
        }
    }
}
