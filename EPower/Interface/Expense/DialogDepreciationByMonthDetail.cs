﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class DialogDepreciationByMonthDetail : ExDialog
    {
        DateTime depreciationMonth = DateTime.Now;
        int currencyId = 0;
        int statusId = 0;
          
        #region Constructor
        public DialogDepreciationByMonthDetail(DateTime date,int _currencyId,int _statusId)
        {
            InitializeComponent(); 
            depreciationMonth = date;
            currencyId = _currencyId;
            statusId = _statusId;
            readFixAsset();
        }        

        #endregion

        #region Method
        private void readFixAsset()
        {
            DataTable depreciation = (from f in DBDataContext.Db.TBL_FIX_ASSET_ITEMs
                                      join c in DBDataContext.Db.TLKP_CURRENCies on f.CURRENCY_ID equals c.CURRENCY_ID
                                      join d in DBDataContext.Db.TBL_DEPRECIATIONs on f.FIX_ASSET_ITEM_ID equals d.FIX_ASSET_ITEM_ID
                                      where d.STATUS_ID==statusId
                                            && d.DEPRECIATION_MONTH==depreciationMonth
                                            && d.CURRENCY_ID==currencyId
                                            && d.IS_ACTIVE
                                      select new
                                      {
                                          f.FIX_ASSET_NAME
                                          ,AMOUNT=UIHelper.FormatCurrency(d.AMOUNT,c.CURRENCY_ID)
                                          ,c.CURRENCY_SING
                                      }
                                    )._ToDataTable(); 
            dgv.DataSource = depreciation; 
        } 

        #endregion

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
        }

         
 

          
         
    }
}
