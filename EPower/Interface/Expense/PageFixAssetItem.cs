﻿using EPower.Base.Helper;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageFixAssetItem : Form
    {
        List<int> categoryIds = new List<int>();

        public TBL_FIX_ASSET_ITEM FixAssetItem
        {
            get
            {
                TBL_FIX_ASSET_ITEM objFixAssetItem = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    int fixAssetItemId = (int)dgv.SelectedRows[0].Cells[FIX_ASSET_ITEM_ID.Name].Value;
                    try
                    {
                        objFixAssetItem = DBDataContext.Db.TBL_FIX_ASSET_ITEMs.FirstOrDefault(x => x.FIX_ASSET_ITEM_ID == fixAssetItemId);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objFixAssetItem;
            }
        }
        bool isLoad = false;

    

        #region Constructor
        public PageFixAssetItem()
        {
            isLoad = false;
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            lookup();
            new FixAssetCategoryPopulator().PopluateTree(tvw);
            this.tvw.ExpandAll(); 
            this.tvw.SelectedNode = this.tvw.Nodes[0];
            isLoad = true;
        }
        #endregion

        #region Method
        private bool IsDeletable()
        {
            bool val = false;
            if (dgv.SelectedRows.Count > 0)
            {
                val = true;
            }
            return val;
        }

        public void lookup()
        {
            UIHelper.SetDataSourceToComboBox(cboCurrency, Lookup.GetCurrencies(), Resources.ALL_CURRENCY);
            UIHelper.SetDataSourceToComboBox(cboFixAssetStatus, Lookup.GetFixAssetStatus(), Resources.ALL_STATUS);
            cboFixAssetStatus.SelectedValue = (int)FixAssetItemStatus.USING; 
        }

        #endregion 
         
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            } 
        }

        private void BindData()
        {
            try
            {
                int currencyId = (int)cboCurrency.SelectedValue;
                int statusId = (int)cboFixAssetStatus.SelectedValue;
                dgv.DataSource = from i in DBDataContext.Db.TBL_FIX_ASSET_ITEMs
                                 join s in DBDataContext.Db.TBL_FIX_ASSET_CATEGORies on i.CATEGORY_ID equals s.CATEGORY_ID
                                 join c in DBDataContext.Db.TLKP_CURRENCies on i.CURRENCY_ID equals c.CURRENCY_ID
                                 where i.IS_ACTIVE
                                 && ((i.FIX_ASSET_NAME + " " + i.UNIT).ToLower().Contains(txtQuickSearch.Text.ToLower().Trim()))
                                 && (this.categoryIds.Contains(i.CATEGORY_ID))
                                 && (currencyId == 0 || i.CURRENCY_ID == currencyId)
                                 && (statusId == 0 || i.STATUS_ID == statusId)
                                 orderby i.CATEGORY_ID
                                 select new
                                 {
                                     i.FIX_ASSET_ITEM_ID,
                                     i.FIX_ASSET_NAME,
                                     i.USE_DATE,
                                     i.USEFUL_LIFE,
                                     i.ANNUAL_DEPRECIATION_RATE,
                                     i.UNIT,
                                     i.PRICE,
                                     i.QUANTITY,
                                     i.TOTAL_COST,
                                     i.ACCUMULATED_DEPRECIATION,
                                     c.CURRENCY_SING,
                                     i.STATUS_ID
                                 };
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }   
        
        private void btnNew_Click(object sender, EventArgs e)
        {
            var objFA = new TBL_FIX_ASSET_ITEM() 
            {
                USE_DATE=DateTime.Now,
                CATEGORY_ID= categoryIds[0],
                CURRENCY_ID= (int)cboCurrency.SelectedValue
            };
            var objDefaultPaymentAcc = AccountChartHelper.GetAccounts(AccountConfig.PAYMENT_DEFAULT_ACCOUNTS).FirstOrDefault();
            if (objDefaultPaymentAcc != null)
            {
                objFA.PAYMENT_ACCOUNT_ID = objDefaultPaymentAcc.ACCOUNT_ID;
            }
            DialogFixAssetItem dig = new DialogFixAssetItem(GeneralProcess.Insert, objFA,0);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, "FIX_ASSET_ITEM_ID", dig.FixAssetItem.FIX_ASSET_ITEM_ID);
            }
        } 
        
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                DialogFixAssetItem dig = new DialogFixAssetItem(GeneralProcess.Update, FixAssetItem,0);
                dig.ShowDialog();
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, "FIX_ASSET_ITEM_ID", dig.FixAssetItem.FIX_ASSET_ITEM_ID);
            }
        } 
        
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if ( IsDeletable())
            {
                DialogFixAssetItem dig = new DialogFixAssetItem(GeneralProcess.Delete, FixAssetItem,0);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, "FIX_ASSET_ITEM_ID", dig.FixAssetItem.FIX_ASSET_ITEM_ID - 1);
                }
            }
        }

        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void cboSubCatetory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (isLoad)
            {
                txt_QuickSearch(null, null);
            }
        }

        private void btnDepreciation_Click(object sender, EventArgs e)
        {
            DialogDepreciationMonthly diag= new DialogDepreciationMonthly();
            if (diag.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null); 
            }
        }

        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1 && e.ColumnIndex == dgv.Columns[this.ACCUMULATE_DEPRECIATION.Name].Index)
            {
                int fixAssetItemId = (int)dgv.SelectedRows[0].Cells[FIX_ASSET_ITEM_ID.Name].Value;
                TBL_FIX_ASSET_ITEM objFixAssetItem = DBDataContext.Db.TBL_FIX_ASSET_ITEMs.FirstOrDefault(x => x.FIX_ASSET_ITEM_ID == fixAssetItemId);
                DialogFixAssetItem dig = new DialogFixAssetItem(GeneralProcess.Update, objFixAssetItem, 1); 
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, objFixAssetItem.FIX_ASSET_ITEM_ID);
                }
            }
        }

        private void dgv_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1 && e.ColumnIndex == dgv.Columns[this.ACCUMULATE_DEPRECIATION.Name].Index)
            {
                Cursor = Cursors.Hand;
            }
        }

        private void dgv_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            Cursor = Cursors.Default;
        }

        private void cboFixAssetStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (isLoad)
            {
                txt_QuickSearch(null, null);
            }
        }

        private void tvw_AfterSelect(object sender, TreeViewEventArgs e)
        { 
            this.categoryIds = GetCategoriesId(e.Node);
            this.BindData();
        }

        private List<int> GetCategoriesId(TreeNode node)
        {
            var id = (int)node.Tag;
            var tmp = new List<int>();
            tmp.Add(id);
            foreach (TreeNode cnode in node.Nodes)
            {
                tmp.AddRange(GetCategoriesId(cnode));
            }
            return tmp;
        } 
        
        private void dgv_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.RowIndex > 0 && e.ColumnIndex > 0)
            {
                var row = this.dgv.Rows[e.RowIndex];
                var statusId = (int)row.Cells[this.STATUS_ID.Name].Value;
                if (statusId == (int)FixAssetItemStatus.STOP_USE)
                {
                    row.Cells[e.ColumnIndex].Style.ForeColor = Color.Red;
                    row.Cells[e.ColumnIndex].Style.SelectionForeColor = Color.Red;
                } 
            }
        }

        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        } 
    }
}
