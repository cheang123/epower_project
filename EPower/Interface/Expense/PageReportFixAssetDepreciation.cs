﻿using EPower.Base.Helper;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageReportFixAssetDepreciation : Form
    {
        CrystalReportHelper ch = null;

        #region Constructor
        public PageReportFixAssetDepreciation()
        {
            InitializeComponent();
            viewer.DefaultView();
            DateTime dt = DBDataContext.Db.GetSystemDate();
            dtpD1.Value = new DateTime(dt.Year, 1, 1); 
        }

        #endregion

        #region Method

        public void lookup()
        {
            new FixAssetCategoryPopulator().PopluateTree(cboCategory.TreeView);
            cboCategory.TreeView.SelectedNode = this.cboCategory.TreeView.GetNodesIncludeAncestors()
                                                   .FirstOrDefault(x => (int)x.Tag == 0);
            if (cboCategory.TreeView.SelectedNode != null)
            {
                this.cboCategory.Text = cboCategory.TreeView.SelectedNode.Text;
            }

            new AccountChartPopulator().PopluateTree(cboPaymentAccount.TreeView, AccountChartHelper.GetAccounts(AccountConfig.PAYMENT_ACCOUNTS, Resources.ALL_PAYMENT_ACCOUNT));
            if (cboPaymentAccount.TreeView.Nodes.Count > 0)
            {
                cboPaymentAccount.TreeView.Nodes[0].Expand();
                cboPaymentAccount.TreeView.SelectedNode = cboPaymentAccount.TreeView.Nodes[0];
                cboPaymentAccount.Text = cboPaymentAccount.SelectedNode.Text;
            }

            UIHelper.SetDataSourceToComboBox(this.cboDisplayCurrency, Lookup.GetCurrencies());
            UIHelper.SetDataSourceToComboBox(this.cboTranCurrency, Lookup.GetCurrencies(), Resources.ALL_CURRENCY);
        }

        public CrystalReportHelper ViewReport(DateTime d1,DateTime d2,int categoryId,int tranCurrencyId,int displayCurrencyId,int paymentAccountId,string displayCurrencySign)
        {
            CrystalReportHelper c = new CrystalReportHelper("ReportFixAssetDepreciation.rpt");
            c.SetParameter("@D1", d1);
            c.SetParameter("@D2", d2);
            c.SetParameter("@CATEGORY_ID", categoryId);
            c.SetParameter("@TRAN_CURRENCY_ID", tranCurrencyId);
            c.SetParameter("@DISPLAY_CURRENCY_ID", displayCurrencyId);
            c.SetParameter("@PAYMENT_ACCOUNT_ID", paymentAccountId);
            c.SetParameter("@DISPLAY_CURRENCY_SING", displayCurrencySign);
            return c;
        }

        private void viewReport()
        {
            int year = dtpD1.Value.Year;
            int categoryId = (int)cboCategory.TreeView.SelectedNode.Tag; 
            int tranCurrencyId = (int)cboTranCurrency.SelectedValue;
            int displayCurrencyId = (int)cboDisplayCurrency.SelectedValue;
            int paymentAccountId = (int)cboPaymentAccount.TreeView.SelectedNode.Tag;
            string displayCurrencySign = cboDisplayCurrency.Text;

            ch = ViewReport(dtpD1.Value,dtpD2.Value,categoryId,tranCurrencyId,displayCurrencyId,paymentAccountId,displayCurrencySign);
            this.viewer.ReportSource = ch.Report;
        }

        private void sendMail()
        {
            try
            {
                this.viewReport();
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + this.ch.ReportName + ".xls").FullName;
                this.ch.ExportToExcel(exportPath);
                DialogSendMail.Instance.Add(this.ch.ReportName, exportPath);
                DialogSendMail.Instance.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.ShowWarning(EPower.Properties.Resources.YOU_CANNOT_SEND_EMAIL, Resources.WARNING);
                MsgBox.LogError(ex);
            }
        }

        #endregion
        private void btnViewReport_Click(object sender, EventArgs e)
        {
            Runner.Run(this.viewReport);
        } 

        private void btnMail_Click(object sender, EventArgs e)
        {
            this.sendMail();
        } 
         
          
    }
}
