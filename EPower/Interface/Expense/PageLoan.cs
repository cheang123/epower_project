﻿using EPower.Base.Helper;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Windows.Forms;
using Login = SoftTech.Security.Logic.Login;

namespace EPower.Interface
{
    public partial class PageLoan : Form
    { 
        public TBL_LOAN Loan
        {
            get
            {
                TBL_LOAN objLoad = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    int loadId = (int)dgv.SelectedRows[0].Cells[LOAN_ID.Name].Value;
                    try
                    {
                        objLoad = DBDataContext.Db.TBL_LOANs.FirstOrDefault(x => x.LOAN_ID == loadId);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objLoad;
            }
        }
        bool isLoad = false;
        #region Constructor
        public PageLoan()
        {
            InitializeComponent();
            lookup();
            txt_QuickSearch(null, null);
            UIHelper.DataGridViewProperties(dgv);
        }
        #endregion

        #region Operation
        /// <summary>
        /// Load data from database.
        /// </summary>
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            try
            {
                if (!isLoad) return;
                int statusId=0;
                int lookup = 0;
                int currencyId = 0;
                if (cboLookup.SelectedIndex != -1)
                {
                    statusId = (int)cboStatus.SelectedValue;
                }
                if (cboLookup.SelectedIndex != -1)
                {
                    lookup = (int)cboLookup.SelectedValue;
                }
                if (cboCurrency.SelectedIndex != -1)
                {
                    currencyId = (int)cboCurrency.SelectedValue;
                }
                var tmp = from l in DBDataContext.Db.TBL_LOANs
                          join a in DBDataContext.Db.TBL_ACCOUNT_CHARTs on l.LIABILITY_ACCOUNT_ID equals a.ACCOUNT_ID
                          join ap in DBDataContext.Db.TBL_ACCOUNT_CHARTs on l.PAYMENT_ACCOUNT_ID equals ap.ACCOUNT_ID
                          join c in DBDataContext.Db.TLKP_CURRENCies on l.CURRENCY_ID equals c.CURRENCY_ID
                          join lu in DBDataContext.Db.TBL_LOOKUP_VALUEs on l.BUSINESS_DIVISION_ID equals lu.VALUE_ID
                          where l.IS_ACTIVE && (l.LOAN_NO + " " + l.BANK_NAME).ToUpper().Contains(txtQuickSearch.Text.ToUpper())
                                && (lookup == 0 || l.BUSINESS_DIVISION_ID == lookup)
                                && (currencyId == 0 || l.CURRENCY_ID == currencyId) 
                          select new
                          {
                              l.LOAN_ID,
                              l.LOAN_NO,
                              l.LOAN_DATE,
                              l.CLOSE_DATE,
                              l.BANK_NAME,
                              l.ANNUAL_INTEREST_RATE,
                              ACCOUNT_NAME = a.ACCOUNT_CODE + " " + a.ACCOUNT_NAME,
                              l.LOAN_AMOUNT,
                              c.CURRENCY_SING,
                              l.PAID_AMOUNT
                          };
                if (statusId == (int)LoanStatus.LOAN_NOT_PAID)
                {
                    tmp=tmp.Where(x => x.LOAN_AMOUNT > x.PAID_AMOUNT);
                }
                else if (statusId == (int)LoanStatus.LOAN_PAID)
                {
                    tmp=tmp.Where(x => x.LOAN_AMOUNT == x.PAID_AMOUNT);
                }
                dgv.DataSource = tmp;
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
            
        }

        

        /// <summary>
        /// Add new area.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNew_Click(object sender, EventArgs e)
        {
            DateTime date=DBDataContext.Db.GetSystemDate();
            TBL_LOAN objLoan = new TBL_LOAN()
            {
                CURRENCY_ID = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.IS_DEFAULT_CURRENCY).CURRENCY_ID,
                PAYMENT_ACCOUNT_ID = AccountChartHelper.GetAccounts(AccountConfig.PAYMENT_DEFAULT_ACCOUNTS).FirstOrDefault().ACCOUNT_ID,
                LOAN_DATE = date,
                CLOSE_DATE=date,
                CREATE_BY=Login.CurrentLogin.LOGIN_NAME,
                CREATE_ON=date
            };
            DialogLoan dig = new DialogLoan(GeneralProcess.Insert, objLoan);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.Loan.LOAN_ID);
            }
        }

        /// <summary>
        /// Edit area.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                DialogLoan dig = new DialogLoan(GeneralProcess.Update, Loan);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.Loan.LOAN_ID);
                }
            }
        }

        /// <summary>
        /// Remove area.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (dgv.Rows.Count > 0)
            {
                DialogLoan dig = new DialogLoan(GeneralProcess.Delete, Loan);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.Loan.LOAN_ID);
                }
            }
            
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }
        #endregion

        #region Method               
         
        public void lookup()
        {
            isLoad = false;
            UIHelper.SetDataSourceToComboBox(cboCurrency, Lookup.GetCurrencies(), Resources.ALL_CURRENCY);
            UIHelper.SetDataSourceToComboBox(cboLookup, Lookup.GetLookUpValue((int)LookUp.BUSINESS_DIVISION), Resources.ALL_TYPE);
            UIHelper.SetDataSourceToComboBox(cboStatus, Lookup.GetLoanStatus());
            cboStatus.SelectedValue = (int)LoanStatus.LOAN_NOT_PAID;
            isLoad = true;
        }
         
        #endregion 

        

      
    }
}
