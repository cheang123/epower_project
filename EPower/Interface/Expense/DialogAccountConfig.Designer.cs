﻿namespace EPower.Interface
{
    partial class DialogAccountConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tvw = new SoftTech.Component.TriStateTreeView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.chkINCLUDEROOT = new System.Windows.Forms.CheckBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.chkINCLUDEROOT);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.tvw);
            this.content.Size = new System.Drawing.Size(796, 504);
            this.content.Text = "CONTENT";
            this.content.Controls.SetChildIndex(this.tvw, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.chkINCLUDEROOT, 0);
            // 
            // tvw
            // 
            this.tvw.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tvw.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tvw.CheckBoxes = true;
            this.tvw.Dock = System.Windows.Forms.DockStyle.Top;
            this.tvw.Location = new System.Drawing.Point(1, 0);
            this.tvw.Name = "tvw";
            this.tvw.PreventCheckEvent = false;
            this.tvw.Size = new System.Drawing.Size(794, 460);
            this.tvw.TabIndex = 23;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Location = new System.Drawing.Point(2, 466);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(797, 1);
            this.panel1.TabIndex = 24;
            // 
            // btnCLOSE
            // 
            this.btnCLOSE.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnCLOSE.Location = new System.Drawing.Point(714, 473);
            this.btnCLOSE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.Size = new System.Drawing.Size(75, 23);
            this.btnCLOSE.TabIndex = 1;
            this.btnCLOSE.Text = "បិទ";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnCLOSE_Click);
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnOK.Location = new System.Drawing.Point(634, 473);
            this.btnOK.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "យល់ព្រម";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // chkINCLUDEROOT
            // 
            this.chkINCLUDEROOT.AutoSize = true;
            this.chkINCLUDEROOT.Location = new System.Drawing.Point(4, 473);
            this.chkINCLUDEROOT.Name = "chkINCLUDEROOT";
            this.chkINCLUDEROOT.Size = new System.Drawing.Size(108, 23);
            this.chkINCLUDEROOT.TabIndex = 25;
            this.chkINCLUDEROOT.Text = "Included Root(s)";
            this.chkINCLUDEROOT.UseVisualStyleBackColor = true;
            // 
            // DialogAccountConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(796, 527);
            this.Name = "DialogAccountConfig";
            this.Text = "ការកំណត់";
            this.Load += new System.EventHandler(this.DialogAccountConfig_Load);
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private SoftTech.Component.TriStateTreeView tvw;
        private System.Windows.Forms.Panel panel1;
        private SoftTech.Component.ExButton btnCLOSE;
        private SoftTech.Component.ExButton btnOK;
        private System.Windows.Forms.CheckBox chkINCLUDEROOT;


    }
}