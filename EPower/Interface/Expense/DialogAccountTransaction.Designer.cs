﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogAccountTransaction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogAccountTransaction));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClose = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.lblNOTE = new System.Windows.Forms.Label();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.lblCREATE_BY = new System.Windows.Forms.Label();
            this.lblDATE = new System.Windows.Forms.Label();
            this.dtpTranDate = new System.Windows.Forms.DateTimePicker();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.txtRefNo = new System.Windows.Forms.TextBox();
            this.lblREF_NO = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtTranBy = new System.Windows.Forms.TextBox();
            this.lblACCOUNT = new System.Windows.Forms.Label();
            this.lblPAYMENT_ACCOUNT = new System.Windows.Forms.Label();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.lblAMOUNT = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cboPaymentAccount = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit();
            this.cboTransAccount = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboPaymentAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTransAccount.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.cboTransAccount);
            this.content.Controls.Add(this.cboPaymentAccount);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.cboCurrency);
            this.content.Controls.Add(this.lblPAYMENT_ACCOUNT);
            this.content.Controls.Add(this.lblACCOUNT);
            this.content.Controls.Add(this.txtTranBy);
            this.content.Controls.Add(this.label14);
            this.content.Controls.Add(this.label13);
            this.content.Controls.Add(this.txtRefNo);
            this.content.Controls.Add(this.lblREF_NO);
            this.content.Controls.Add(this.txtAmount);
            this.content.Controls.Add(this.lblAMOUNT);
            this.content.Controls.Add(this.dtpTranDate);
            this.content.Controls.Add(this.lblDATE);
            this.content.Controls.Add(this.lblCREATE_BY);
            this.content.Controls.Add(this.txtNote);
            this.content.Controls.Add(this.lblNOTE);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnClose);
            this.content.Controls.Add(this.btnOK);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnClose, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.lblNOTE, 0);
            this.content.Controls.SetChildIndex(this.txtNote, 0);
            this.content.Controls.SetChildIndex(this.lblCREATE_BY, 0);
            this.content.Controls.SetChildIndex(this.lblDATE, 0);
            this.content.Controls.SetChildIndex(this.dtpTranDate, 0);
            this.content.Controls.SetChildIndex(this.lblAMOUNT, 0);
            this.content.Controls.SetChildIndex(this.txtAmount, 0);
            this.content.Controls.SetChildIndex(this.lblREF_NO, 0);
            this.content.Controls.SetChildIndex(this.txtRefNo, 0);
            this.content.Controls.SetChildIndex(this.label13, 0);
            this.content.Controls.SetChildIndex(this.label14, 0);
            this.content.Controls.SetChildIndex(this.txtTranBy, 0);
            this.content.Controls.SetChildIndex(this.lblACCOUNT, 0);
            this.content.Controls.SetChildIndex(this.lblPAYMENT_ACCOUNT, 0);
            this.content.Controls.SetChildIndex(this.cboCurrency, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.cboPaymentAccount, 0);
            this.content.Controls.SetChildIndex(this.cboTransAccount, 0);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnClose
            // 
            resources.ApplyResources(this.btnClose, "btnClose");
            this.btnClose.Name = "btnClose";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // lblNOTE
            // 
            resources.ApplyResources(this.lblNOTE, "lblNOTE");
            this.lblNOTE.Name = "lblNOTE";
            // 
            // txtNote
            // 
            resources.ApplyResources(this.txtNote, "txtNote");
            this.txtNote.Name = "txtNote";
            this.txtNote.Enter += new System.EventHandler(this.ChangeKeyboardKhmer);
            // 
            // lblCREATE_BY
            // 
            resources.ApplyResources(this.lblCREATE_BY, "lblCREATE_BY");
            this.lblCREATE_BY.Name = "lblCREATE_BY";
            // 
            // lblDATE
            // 
            resources.ApplyResources(this.lblDATE, "lblDATE");
            this.lblDATE.Name = "lblDATE";
            // 
            // dtpTranDate
            // 
            resources.ApplyResources(this.dtpTranDate, "dtpTranDate");
            this.dtpTranDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTranDate.Name = "dtpTranDate";
            this.dtpTranDate.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            // 
            // txtAmount
            // 
            resources.ApplyResources(this.txtAmount, "txtAmount");
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            // 
            // txtRefNo
            // 
            resources.ApplyResources(this.txtRefNo, "txtRefNo");
            this.txtRefNo.Name = "txtRefNo";
            this.txtRefNo.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            // 
            // lblREF_NO
            // 
            resources.ApplyResources(this.lblREF_NO, "lblREF_NO");
            this.lblREF_NO.Name = "lblREF_NO";
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Name = "label13";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Name = "label14";
            // 
            // txtTranBy
            // 
            this.txtTranBy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtTranBy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            resources.ApplyResources(this.txtTranBy, "txtTranBy");
            this.txtTranBy.Name = "txtTranBy";
            this.txtTranBy.Enter += new System.EventHandler(this.ChangeKeyboardKhmer);
            // 
            // lblACCOUNT
            // 
            resources.ApplyResources(this.lblACCOUNT, "lblACCOUNT");
            this.lblACCOUNT.Name = "lblACCOUNT";
            // 
            // lblPAYMENT_ACCOUNT
            // 
            resources.ApplyResources(this.lblPAYMENT_ACCOUNT, "lblPAYMENT_ACCOUNT");
            this.lblPAYMENT_ACCOUNT.Name = "lblPAYMENT_ACCOUNT";
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            this.cboCurrency.Items.AddRange(new object[] {
            resources.GetString("cboCurrency.Items"),
            resources.GetString("cboCurrency.Items1"),
            resources.GetString("cboCurrency.Items2")});
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            // 
            // lblAMOUNT
            // 
            resources.ApplyResources(this.lblAMOUNT, "lblAMOUNT");
            this.lblAMOUNT.Name = "lblAMOUNT";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // cboPaymentAccount
            // 
            resources.ApplyResources(this.cboPaymentAccount, "cboPaymentAccount");
            this.cboPaymentAccount.Name = "cboPaymentAccount";
            this.cboPaymentAccount.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("cboPaymentAccount.Properties.Appearance.Font")));
            this.cboPaymentAccount.Properties.Appearance.Options.UseFont = true;
            this.cboPaymentAccount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cboPaymentAccount.Properties.Buttons"))))});
            this.cboPaymentAccount.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("cboPaymentAccount.Properties.Columns"), resources.GetString("cboPaymentAccount.Properties.Columns1"), ((int)(resources.GetObject("cboPaymentAccount.Properties.Columns2"))), ((DevExpress.Utils.FormatType)(resources.GetObject("cboPaymentAccount.Properties.Columns3"))), resources.GetString("cboPaymentAccount.Properties.Columns4"), ((bool)(resources.GetObject("cboPaymentAccount.Properties.Columns5"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("cboPaymentAccount.Properties.Columns6"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("cboPaymentAccount.Properties.Columns7"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("cboPaymentAccount.Properties.Columns8")))),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("cboPaymentAccount.Properties.Columns9"), resources.GetString("cboPaymentAccount.Properties.Columns10"), ((int)(resources.GetObject("cboPaymentAccount.Properties.Columns11"))), ((DevExpress.Utils.FormatType)(resources.GetObject("cboPaymentAccount.Properties.Columns12"))), resources.GetString("cboPaymentAccount.Properties.Columns13"), ((bool)(resources.GetObject("cboPaymentAccount.Properties.Columns14"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("cboPaymentAccount.Properties.Columns15"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("cboPaymentAccount.Properties.Columns16"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("cboPaymentAccount.Properties.Columns17")))),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("cboPaymentAccount.Properties.Columns18"), resources.GetString("cboPaymentAccount.Properties.Columns19"), ((int)(resources.GetObject("cboPaymentAccount.Properties.Columns20"))), ((DevExpress.Utils.FormatType)(resources.GetObject("cboPaymentAccount.Properties.Columns21"))), resources.GetString("cboPaymentAccount.Properties.Columns22"), ((bool)(resources.GetObject("cboPaymentAccount.Properties.Columns23"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("cboPaymentAccount.Properties.Columns24"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("cboPaymentAccount.Properties.Columns25"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("cboPaymentAccount.Properties.Columns26"))))});
            this.cboPaymentAccount.Properties.NullText = resources.GetString("cboPaymentAccount.Properties.NullText");
            this.cboPaymentAccount.Properties.PopupWidth = 500;
            this.cboPaymentAccount.Properties.PopupWidthMode = DevExpress.XtraEditors.PopupWidthMode.ContentWidth;
            this.cboPaymentAccount.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.cboPaymentAccount.Required = false;
            // 
            // cboTransAccount
            // 
            resources.ApplyResources(this.cboTransAccount, "cboTransAccount");
            this.cboTransAccount.Name = "cboTransAccount";
            this.cboTransAccount.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("dLookUpEdit1.Properties.Appearance.Font")));
            this.cboTransAccount.Properties.Appearance.Options.UseFont = true;
            this.cboTransAccount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("dLookUpEdit1.Properties.Buttons"))))});
            this.cboTransAccount.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("dLookUpEdit1.Properties.Columns"), resources.GetString("dLookUpEdit1.Properties.Columns1"), ((int)(resources.GetObject("dLookUpEdit1.Properties.Columns2"))), ((DevExpress.Utils.FormatType)(resources.GetObject("dLookUpEdit1.Properties.Columns3"))), resources.GetString("dLookUpEdit1.Properties.Columns4"), ((bool)(resources.GetObject("dLookUpEdit1.Properties.Columns5"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("dLookUpEdit1.Properties.Columns6"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("dLookUpEdit1.Properties.Columns7"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("dLookUpEdit1.Properties.Columns8")))),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("dLookUpEdit1.Properties.Columns9"), resources.GetString("dLookUpEdit1.Properties.Columns10"), ((int)(resources.GetObject("dLookUpEdit1.Properties.Columns11"))), ((DevExpress.Utils.FormatType)(resources.GetObject("dLookUpEdit1.Properties.Columns12"))), resources.GetString("dLookUpEdit1.Properties.Columns13"), ((bool)(resources.GetObject("dLookUpEdit1.Properties.Columns14"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("dLookUpEdit1.Properties.Columns15"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("dLookUpEdit1.Properties.Columns16"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("dLookUpEdit1.Properties.Columns17")))),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("dLookUpEdit1.Properties.Columns18"), resources.GetString("dLookUpEdit1.Properties.Columns19"), ((int)(resources.GetObject("dLookUpEdit1.Properties.Columns20"))), ((DevExpress.Utils.FormatType)(resources.GetObject("dLookUpEdit1.Properties.Columns21"))), resources.GetString("dLookUpEdit1.Properties.Columns22"), ((bool)(resources.GetObject("dLookUpEdit1.Properties.Columns23"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("dLookUpEdit1.Properties.Columns24"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("dLookUpEdit1.Properties.Columns25"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("dLookUpEdit1.Properties.Columns26"))))});
            this.cboTransAccount.Properties.NullText = resources.GetString("dLookUpEdit1.Properties.NullText");
            this.cboTransAccount.Properties.PopupWidth = 500;
            this.cboTransAccount.Properties.PopupWidthMode = DevExpress.XtraEditors.PopupWidthMode.ContentWidth;
            this.cboTransAccount.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.cboTransAccount.Required = false;
            // 
            // DialogAccountTransaction
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogAccountTransaction";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboPaymentAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTransAccount.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnClose;
        private ExButton btnOK;
        private ExButton btnCHANGE_LOG;
        private TextBox txtNote;
        private Label lblNOTE;
        private TextBox txtRefNo;
        private Label lblREF_NO;
        private TextBox txtAmount;
        private DateTimePicker dtpTranDate;
        private Label lblDATE;
        private Label lblCREATE_BY;
        private Label label14;
        private Label label13;
        private Label lblACCOUNT;
        private TextBox txtTranBy;
        private Label lblPAYMENT_ACCOUNT;
        private ComboBox cboCurrency;
        private Label lblAMOUNT;
        private Label label1;
        private HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit cboPaymentAccount;
        private HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit cboTransAccount;
    }
}