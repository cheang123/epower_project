﻿using EPower.Base.Helper;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageReportTranDetail : Form
    {
        CrystalReportHelper ch = null;
        AccountType accountType = AccountType.Income;
        public PageReportTranDetail()
        {
            InitializeComponent();
            viewer.DefaultView();
            UIHelper.SetDataSourceToComboBox(this.cboDisplayCurrency, Lookup.GetCurrencies());
            UIHelper.SetDataSourceToComboBox(this.cboTranCurrency, Lookup.GetCurrencies(),Resources.ALL_CURRENCY);
            var objDefaultCurrency = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x=>x.IS_DEFAULT_CURRENCY);
            if(objDefaultCurrency!=null){
                this.cboDisplayCurrency.SelectedValue = objDefaultCurrency.CURRENCY_ID;
            }
        }
        public void SetAccountType(AccountType accountType)
        {
            this.accountType = accountType; 
             

            var accounts = new List<TBL_ACCOUNT_CHART>();
            if (accountType == AccountType.Income)
            {
                accounts = AccountChartHelper.GetAccounts(AccountConfig.INCOME_TRANS_ACCOUNTS,Resources.ALL_ACCOUNT);
            }
            else
            {
                accounts = AccountChartHelper.GetAccounts(AccountConfig.EXPENSE_TRANS_ACCOUNTS, Resources.ALL_ACCOUNT);
            }
            new AccountChartPopulator().PopluateTree(cboTransAccount.TreeView, accounts);
            if (cboTransAccount.TreeView.Nodes.Count > 0)
            {
                cboTransAccount.TreeView.Nodes[0].Expand();
                cboTransAccount.TreeView.SelectedNode = cboTransAccount.TreeView.Nodes[0];
                cboTransAccount.Text = cboTransAccount.SelectedNode.Text;
            } 

            new AccountChartPopulator().PopluateTree(cboPaymentAccount.TreeView, AccountChartHelper.GetAccounts(AccountConfig.PAYMENT_ACCOUNTS, Resources.ALL_PAYMENT_ACCOUNT));
            if (cboPaymentAccount.TreeView.Nodes.Count > 0)
            {
                cboPaymentAccount.TreeView.Nodes[0].Expand();
                cboPaymentAccount.TreeView.SelectedNode = cboPaymentAccount.TreeView.Nodes[0];
                cboPaymentAccount.Text = cboPaymentAccount.SelectedNode.Text;
            } 
        } 


        private void btnViewReport_Click(object sender, EventArgs e)
        {
            Runner.Run(this.viewReport);
        }

    
        private void viewReport()
        {
            CrystalReportHelper c = new CrystalReportHelper("ReportAccountTranDetail.rpt");
            c.SetParameter("@D1", dtp1.Value);
            c.SetParameter("@D2", dtp2.Value);
            c.SetParameter("@TYPE_ID", (int)accountType);
            c.SetParameter("@TRAN_ACCOUNT_ID", (int)cboTransAccount.SelectedNode.Tag);
            c.SetParameter("@PAYMENT_ACCOUNT_ID", (int)cboPaymentAccount.SelectedNode.Tag);
            c.SetParameter("@TRAN_CURRENCY_ID", (int)cboTranCurrency.SelectedValue);
            c.SetParameter("@DISPLAY_CURRENCY_ID", (int)cboDisplayCurrency.SelectedValue);

            this.viewer.ReportSource = c.Report;
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            this.sendMail();
        }
        private void sendMail()
        {
            try
            {
                this.viewReport();
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + this.ch.ReportName + ".xls").FullName;
                this.ch.ExportToExcel(exportPath);
                DialogSendMail.Instance.Add(this.ch.ReportName, exportPath);
                DialogSendMail.Instance.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.ShowWarning(EPower.Properties.Resources.YOU_CANNOT_SEND_EMAIL, Resources.WARNING);
                MsgBox.LogError(ex);
            }
        }
        private void ChangeKeyboardKhmer(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void ChangeKeyboardEnglish(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }  
         
    }

    public class PageReportTranIncomeDetail : PageReportTranDetail
    {
        public PageReportTranIncomeDetail()
        {
            this.SetAccountType(AccountType.Income);
        }
    }
    public class PageReportTranExpenseDetail : PageReportTranDetail
    {
        public PageReportTranExpenseDetail()
        {
            this.SetAccountType(AccountType.Expense);
        }
    }
}
