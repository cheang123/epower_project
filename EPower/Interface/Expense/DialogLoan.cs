﻿using EPower.Base.Helper;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using Login = SoftTech.Security.Logic.Login;

namespace EPower.Interface
{
    public partial class DialogLoan : ExDialog
    {
        #region Date
        GeneralProcess _flag;

        TBL_LOAN _objNew = new TBL_LOAN();
        TBL_LOAN _objOld = new TBL_LOAN();

        public TBL_LOAN Loan
        {
            get { return _objNew; }
        }
        #endregion

        #region Constructor
        public DialogLoan(GeneralProcess flag, TBL_LOAN objLoan)
        {
            InitializeComponent();
            _flag = flag;
            objLoan._CopyTo(_objNew);
            objLoan._CopyTo(_objOld);

            // Setup Account
            new AccountChartPopulator().PopluateTree(cboTransAccount.TreeView, AccountChartHelper.GetAccounts(AccountConfig.LIABILITY_ACCOUNT));
            if (cboTransAccount.TreeView.Nodes.Count > 0)
            {
                cboTransAccount.TreeView.Nodes[0].Expand();
            }
            new AccountChartPopulator().PopluateTree(cboPaymentAccount.TreeView, AccountChartHelper.GetAccounts(AccountConfig.PAYMENT_ACCOUNTS));
            if (cboTransAccount.TreeView.Nodes.Count > 0)
            {
                cboTransAccount.TreeView.Nodes[0].Expand();
            }
            UIHelper.SetDataSourceToComboBox(cboCurrency, Lookup.GetCurrencies());
            UIHelper.SetDataSourceToComboBox(cboBusinessDivension, Lookup.GetLookUpValue((int)LookUp.BUSINESS_DIVISION));

            // Autocomplete
            var bank = from a in DBDataContext.Db.TBL_LOANs
                       where a.IS_ACTIVE
                       select new { a.BANK_NAME };
            foreach (var item in bank.Distinct())
            {
                txtBankNo.AutoCompleteCustomSource.Add(item.BANK_NAME);
            }

            //Initailize control 
            read();
            this.Text = flag.GetText(this.Text);
            UIHelper.SetEnabled(this, !(flag == GeneralProcess.Delete));
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;

            if (_flag == GeneralProcess.Insert)
            {
                tabControl1.TabPages.Remove(tabPAYMENT);
            }
            else if (_flag == GeneralProcess.Update)
            {
                readLoanPayment();
                disableControl();
            }
            else if (_flag == GeneralProcess.Delete)
            {
                btnADD.Enabled =
                btnEDIT.Enabled =
                btnREMOVE.Enabled = false;
            }
        }
        #endregion

        #region Operation
        /// <summary>
        /// Change Keyboard to Khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKeyboardKhmer(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void ChangeKeyboardEnglish(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        /// <summary>
        /// Data transaction.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
            //If record is duplicate.
            this.ClearAllValidation();
            if (DBDataContext.Db.IsExits(_objNew, "LOAN_NO"))
            {
                txtLoanNo.SetValidation(string.Format(Resources.MS_IS_EXISTS, lblLOAN_NO.Text));
                return;
            }

            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objNew);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        if (_objNew.PAID_AMOUNT > 0)
                        {
                            if (MsgBox.ShowQuestion(string.Format(Resources.MSQ_LOAN_HAVE_PAYMENT, _objNew.LOAN_NO), Resources.DELETE) == DialogResult.Yes)
                            {
                                foreach (var item in DBDataContext.Db.TBL_LOAN_PAYMENTs.Where(x => x.IS_ACTIVE))
                                {
                                    item.IS_ACTIVE = false;
                                }
                                DBDataContext.Db.SubmitChanges();
                                DBDataContext.Db.Delete(_objNew);
                            }
                        }
                        else
                        {
                            DBDataContext.Db.Delete(_objNew);
                        }
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
        #endregion

        #region Method
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();
            if (txtLoanNo.Text.Trim() == string.Empty)
            {
                txtLoanNo.SetValidation(string.Format(Resources.REQUIRED, lblLOAN_NO.Text));
                val = true;
            }
            if (txtBankNo.Text.Trim() == string.Empty)
            {
                txtBankNo.SetValidation(string.Format(Resources.REQUIRED, lblBANK.Text));
                val = true;
            }
            if (txtInterestRate.Text.Trim() == string.Empty)
            {
                txtInterestRate.SetValidation(Resources.REQUIRED_INPUT_NUMBER);
                val = true;
            }
            if (cboTransAccount.TreeView.SelectedNode == null)
            {
                cboTransAccount.SetValidation(string.Format(Resources.REQUIRED, lblLOAN_ACCOUNT.Text));
                val = true;
            }
            if (cboTransAccount.SelectedNode != null &&
                cboTransAccount.SelectedNode.Nodes.Count > 0)
            {
                cboTransAccount.SetValidation(Resources.MS_CHOOSE_CHILD_END_ACCOUNT);
                val = true;
            }

            if (cboPaymentAccount.TreeView.SelectedNode == null)
            {
                cboPaymentAccount.SetValidation(string.Format(Resources.REQUIRED, lblPAYMENT_ACCOUNT.Text));
                val = true;
            }
            if (cboPaymentAccount.SelectedNode != null &&
                cboPaymentAccount.SelectedNode.Nodes.Count > 0)
            {
                cboTransAccount.SetValidation(Resources.MS_CHOOSE_CHILD_END_ACCOUNT);
                val = true;
            }
            if (cboBusinessDivension.SelectedIndex == -1)
            {
                cboBusinessDivension.SetValidation(Resources.REQUIRED);
                val = true;
            }
            if (txtLoanAmount.Text.Trim() == string.Empty)
            {
                txtLoanAmount.SetValidation(Resources.REQUIRED_INPUT_NUMBER);
                val = true;
            }
            if (cboCurrency.SelectedValue == null)
            {
                cboCurrency.SetValidation(string.Format(Resources.REQUIRED, Resources.CURRENCY));
                val = true;
            }
            return val;
        }

        /// <summary>
        /// Read data from object.
        /// </summary>
        private void read()
        {
            txtLoanNo.Text = _objNew.LOAN_NO;
            txtBankNo.Text = _objNew.BANK_NAME;
            txtInterestRate.Text = _objNew.ANNUAL_INTEREST_RATE.ToString();
            dtpLoanDate.Value = _objNew.LOAN_DATE;
            dtpCloseDate.Value = _objNew.CLOSE_DATE;
            cboPaymentAccount.TreeView.SelectedNode = cboPaymentAccount.TreeView.GetNodesIncludeAncestors().FirstOrDefault(x => (int)x.Tag == _objNew.PAYMENT_ACCOUNT_ID);
            cboPaymentAccount.Text = cboPaymentAccount.TreeView.SelectedNode != null ? cboPaymentAccount.TreeView.SelectedNode.Text : "";
            cboTransAccount.TreeView.SelectedNode = cboTransAccount.TreeView.GetNodesIncludeAncestors().FirstOrDefault(x => (int)x.Tag == _objNew.LIABILITY_ACCOUNT_ID);
            cboTransAccount.Text = cboTransAccount.TreeView.SelectedNode != null ? cboTransAccount.TreeView.SelectedNode.Text : "";
            cboBusinessDivension.SelectedValue = _objNew.BUSINESS_DIVISION_ID;
            txtLoanAmount.Text = UIHelper.FormatCurrency(_objNew.LOAN_AMOUNT, _objNew.CURRENCY_ID);
            cboCurrency.SelectedValue = _objNew.CURRENCY_ID;
            txtNote.Text = _objNew.NOTE;
            txtLoanPayment.Text = UIHelper.FormatCurrency(_objNew.PAID_AMOUNT, _objNew.CURRENCY_ID);
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            _objNew.LOAN_NO = txtLoanNo.Text;
            _objNew.BANK_NAME = txtBankNo.Text;
            _objNew.ANNUAL_INTEREST_RATE = DataHelper.ParseToDecimal(txtInterestRate.Text);
            _objNew.LOAN_DATE = dtpLoanDate.Value;
            _objNew.CLOSE_DATE = dtpCloseDate.Value;
            _objNew.LIABILITY_ACCOUNT_ID = (int)cboTransAccount.TreeView.SelectedNode.Tag;
            _objNew.PAYMENT_ACCOUNT_ID = (int)cboPaymentAccount.TreeView.SelectedNode.Tag;
            _objNew.BUSINESS_DIVISION_ID = (int)cboBusinessDivension.SelectedValue;
            _objNew.LOAN_AMOUNT = DataHelper.ParseToDecimal(UIHelper.FormatCurrency(decimal.Parse(txtLoanAmount.Text), (int)cboCurrency.SelectedValue));
            _objNew.CURRENCY_ID = (int)cboCurrency.SelectedValue;
            _objNew.NOTE = txtNote.Text;
            _objNew.PAID_AMOUNT = DataHelper.ParseToDecimal(UIHelper.FormatCurrency(DataHelper.ParseToDecimal(txtLoanPayment.Text), _objNew.CURRENCY_ID));
        }

        private void readLoanPayment()
        {
            var db = from lp in DBDataContext.Db.TBL_LOAN_PAYMENTs
                     join a in DBDataContext.Db.TBL_ACCOUNT_CHARTs on lp.EXPENSE_ACCOUNT_ID equals a.ACCOUNT_ID
                     join ap in DBDataContext.Db.TBL_ACCOUNT_CHARTs on lp.PAYMENT_ACCOUNT_ID equals ap.ACCOUNT_ID
                     join l in DBDataContext.Db.TBL_LOANs on lp.LOAN_ID equals l.LOAN_ID
                     join c in DBDataContext.Db.TLKP_CURRENCies on l.CURRENCY_ID equals c.CURRENCY_ID
                     where lp.IS_ACTIVE && lp.LOAN_ID == _objNew.LOAN_ID
                     select new
                     {
                         lp.PAYMENT_ID,
                         lp.PAY_DATE,
                         a.ACCOUNT_NAME,
                         PAYMENT_ACCOUNT = ap.ACCOUNT_NAME,
                         lp.INTEREST_AMOUNT,
                         lp.CAPITAL_AMOUNT,
                         lp.PAY_AMOUNT,
                         c.CURRENCY_SING
                     };
            dgv.DataSource = db;
            txtPayment.Text = txtLoanPayment.Text;
        }

        private void disableControl()
        {
            TBL_LOAN objLoan = DBDataContext.Db.TBL_LOANs.FirstOrDefault(x => x.LOAN_ID == _objNew.LOAN_ID);
            txtPayment.Text = UIHelper.FormatCurrency(objLoan.PAID_AMOUNT, _objNew.CURRENCY_ID);
            txtLoanPayment.Text = UIHelper.FormatCurrency(objLoan.PAID_AMOUNT, _objNew.CURRENCY_ID);

            if (objLoan.PAID_AMOUNT > 0)
            {
                txtInterestRate.Enabled =
                txtLoanPayment.Enabled =
                txtLoanAmount.Enabled =
                cboCurrency.Enabled = false;


            }
            else
            {
                txtInterestRate.Enabled =
                txtLoanPayment.Enabled =
                txtLoanAmount.Enabled =
                 cboCurrency.Enabled = true;
            }

            decimal totalInterest = 0;
            decimal totalCapital = 0;
            foreach (DataGridViewRow item in dgv.Rows)
            {
                totalInterest += (decimal)item.Cells[INTEREST_AMOUNT.Name].Value;
                totalCapital += (decimal)item.Cells[CAPITAL_AMOUNT.Name].Value;
            }
            txtTotalInterest.Text = UIHelper.FormatCurrency(totalInterest, _objNew.CURRENCY_ID);
            txtTotalCapital.Text = UIHelper.FormatCurrency(totalCapital, _objNew.CURRENCY_ID);
        }

        #endregion

        #region Event

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }

        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtInterestRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        }

        private void lblAdd_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DateTime date = DBDataContext.Db.GetSystemDate();
            TBL_LOAN_PAYMENT objLoanPayment = new TBL_LOAN_PAYMENT()
            {
                PAY_DATE = date,
                CREATE_ON = date,
                CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                PAYMENT_ACCOUNT_ID = AccountChartHelper.GetAccounts(AccountConfig.PAYMENT_DEFAULT_ACCOUNTS).FirstOrDefault().ACCOUNT_ID
            };
            DialogLoanPayment dig = new DialogLoanPayment(GeneralProcess.Insert, objLoanPayment, Loan);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                readLoanPayment();
                UIHelper.SelectRow(dgv, dig.LoanPayment.PAYMENT_ID);
                disableControl();
            }
        }

        private void lblEdit_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (dgv.Rows.Count > 0)
            {
                TBL_LOAN_PAYMENT objLoanPayment = DBDataContext.Db.TBL_LOAN_PAYMENTs.FirstOrDefault(x => x.PAYMENT_ID == (int)dgv.SelectedRows[0].Cells[PAYMENT_ID.Name].Value);
                DialogLoanPayment dig = new DialogLoanPayment(GeneralProcess.Update, objLoanPayment, Loan);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    readLoanPayment();
                    UIHelper.SelectRow(dgv, dig.LoanPayment.PAYMENT_ID);
                    disableControl();
                }
            }
        }

        private void lblDelete_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (dgv.Rows.Count > 0)
            {
                TBL_LOAN_PAYMENT objLoanPayment = DBDataContext.Db.TBL_LOAN_PAYMENTs.FirstOrDefault(x => x.PAYMENT_ID == (int)dgv.SelectedRows[0].Cells[PAYMENT_ID.Name].Value);
                DialogLoanPayment dig = new DialogLoanPayment(GeneralProcess.Delete, objLoanPayment, Loan);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    readLoanPayment();
                    UIHelper.SelectRow(dgv, dig.LoanPayment.PAYMENT_ID - 1);
                    disableControl();
                }
            }
        }

        #endregion 
    }
}