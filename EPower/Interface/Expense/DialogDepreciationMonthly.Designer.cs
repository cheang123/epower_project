﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogDepreciationMonthly
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogDepreciationMonthly));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.chkCheckAll_ = new System.Windows.Forms.CheckBox();
            this.lblUSE_YEAR = new System.Windows.Forms.Label();
            this.lblCURRENCY = new System.Windows.Forms.Label();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.dgvTotal = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblACCUMULATE_DEPRECIATION = new System.Windows.Forms.Label();
            this.btnDEPRECIATION = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnDETAIL = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnREMOVE_DEPRECIATION = new SoftTech.Component.ExLinkLabel(this.components);
            this.dtpYear = new System.Windows.Forms.DateTimePicker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.IS_DEPRECIATION_ = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.DEPRECIATION_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_FIX_ASSET_ITEM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_DEPRECIATION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_SING_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTotal)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.dtpYear);
            this.content.Controls.Add(this.dgvTotal);
            this.content.Controls.Add(this.cboCurrency);
            this.content.Controls.Add(this.chkCheckAll_);
            this.content.Controls.Add(this.dgv);
            this.content.Controls.Add(this.lblCURRENCY);
            this.content.Controls.Add(this.lblACCUMULATE_DEPRECIATION);
            this.content.Controls.Add(this.lblUSE_YEAR);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblUSE_YEAR, 0);
            this.content.Controls.SetChildIndex(this.lblACCUMULATE_DEPRECIATION, 0);
            this.content.Controls.SetChildIndex(this.lblCURRENCY, 0);
            this.content.Controls.SetChildIndex(this.dgv, 0);
            this.content.Controls.SetChildIndex(this.chkCheckAll_, 0);
            this.content.Controls.SetChildIndex(this.cboCurrency, 0);
            this.content.Controls.SetChildIndex(this.dgvTotal, 0);
            this.content.Controls.SetChildIndex(this.dtpYear, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            // 
            // chkCheckAll_
            // 
            resources.ApplyResources(this.chkCheckAll_, "chkCheckAll_");
            this.chkCheckAll_.Name = "chkCheckAll_";
            this.chkCheckAll_.UseVisualStyleBackColor = true;
            this.chkCheckAll_.CheckedChanged += new System.EventHandler(this.chkCheckAll_CheckedChanged);
            // 
            // lblUSE_YEAR
            // 
            resources.ApplyResources(this.lblUSE_YEAR, "lblUSE_YEAR");
            this.lblUSE_YEAR.Name = "lblUSE_YEAR";
            // 
            // lblCURRENCY
            // 
            resources.ApplyResources(this.lblCURRENCY, "lblCURRENCY");
            this.lblCURRENCY.Name = "lblCURRENCY";
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IS_DEPRECIATION_,
            this.DEPRECIATION_DATE,
            this.TOTAL_FIX_ASSET_ITEM,
            this.AMOUNT_DEPRECIATION,
            this.CURRENCY_SING_,
            this.CURRENCY_ID});
            this.dgv.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellContentClick);
            this.dgv.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellDoubleClick);
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.SelectedIndexChanged += new System.EventHandler(this.dtpYear_ValueChanged);
            // 
            // dgvTotal
            // 
            this.dgvTotal.AllowUserToAddRows = false;
            this.dgvTotal.AllowUserToDeleteRows = false;
            this.dgvTotal.AllowUserToResizeColumns = false;
            this.dgvTotal.AllowUserToResizeRows = false;
            this.dgvTotal.BackgroundColor = System.Drawing.Color.White;
            this.dgvTotal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvTotal.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvTotal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTotal.ColumnHeadersVisible = false;
            this.dgvTotal.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvTotal.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvTotal.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgvTotal, "dgvTotal");
            this.dgvTotal.Name = "dgvTotal";
            this.dgvTotal.ReadOnly = true;
            this.dgvTotal.RowHeadersVisible = false;
            this.dgvTotal.RowTemplate.Height = 25;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "CURRENCY_ID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn2, "dataGridViewTextBoxColumn2");
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "AMOUNT";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.Format = "N2";
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle1;
            resources.ApplyResources(this.dataGridViewTextBoxColumn3, "dataGridViewTextBoxColumn3");
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "CURRENCY_SING";
            this.dataGridViewTextBoxColumn4.FillWeight = 65F;
            resources.ApplyResources(this.dataGridViewTextBoxColumn4, "dataGridViewTextBoxColumn4");
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // lblACCUMULATE_DEPRECIATION
            // 
            resources.ApplyResources(this.lblACCUMULATE_DEPRECIATION, "lblACCUMULATE_DEPRECIATION");
            this.lblACCUMULATE_DEPRECIATION.Name = "lblACCUMULATE_DEPRECIATION";
            // 
            // btnDEPRECIATION
            // 
            resources.ApplyResources(this.btnDEPRECIATION, "btnDEPRECIATION");
            this.btnDEPRECIATION.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnDEPRECIATION.Name = "btnDEPRECIATION";
            this.btnDEPRECIATION.TabStop = true;
            this.btnDEPRECIATION.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkDepreciation_LinkClicked);
            // 
            // btnDETAIL
            // 
            resources.ApplyResources(this.btnDETAIL, "btnDETAIL");
            this.btnDETAIL.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnDETAIL.Name = "btnDETAIL";
            this.btnDETAIL.TabStop = true;
            this.btnDETAIL.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkDetail_LinkClicked);
            // 
            // btnREMOVE_DEPRECIATION
            // 
            resources.ApplyResources(this.btnREMOVE_DEPRECIATION, "btnREMOVE_DEPRECIATION");
            this.btnREMOVE_DEPRECIATION.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnREMOVE_DEPRECIATION.Name = "btnREMOVE_DEPRECIATION";
            this.btnREMOVE_DEPRECIATION.TabStop = true;
            this.btnREMOVE_DEPRECIATION.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkDeleteDepreciation_LinkClicked);
            // 
            // dtpYear
            // 
            resources.ApplyResources(this.dtpYear, "dtpYear");
            this.dtpYear.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpYear.Name = "dtpYear";
            this.dtpYear.ShowCheckBox = true;
            this.dtpYear.ValueChanged += new System.EventHandler(this.dtpYear_ValueChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnREMOVE_DEPRECIATION);
            this.panel1.Controls.Add(this.btnDEPRECIATION);
            this.panel1.Controls.Add(this.btnDETAIL);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // IS_DEPRECIATION_
            // 
            this.IS_DEPRECIATION_.DataPropertyName = "IS_DEPRECIATION";
            resources.ApplyResources(this.IS_DEPRECIATION_, "IS_DEPRECIATION_");
            this.IS_DEPRECIATION_.Name = "IS_DEPRECIATION_";
            this.IS_DEPRECIATION_.ReadOnly = true;
            // 
            // DEPRECIATION_DATE
            // 
            this.DEPRECIATION_DATE.DataPropertyName = "DEPRECIATION_MONTH";
            dataGridViewCellStyle4.Format = "MM-yyyy";
            dataGridViewCellStyle4.NullValue = null;
            this.DEPRECIATION_DATE.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.DEPRECIATION_DATE, "DEPRECIATION_DATE");
            this.DEPRECIATION_DATE.Name = "DEPRECIATION_DATE";
            this.DEPRECIATION_DATE.ReadOnly = true;
            // 
            // TOTAL_FIX_ASSET_ITEM
            // 
            this.TOTAL_FIX_ASSET_ITEM.DataPropertyName = "TOTAL_FIX_ASSET_ITEM";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N0";
            dataGridViewCellStyle5.NullValue = null;
            this.TOTAL_FIX_ASSET_ITEM.DefaultCellStyle = dataGridViewCellStyle5;
            resources.ApplyResources(this.TOTAL_FIX_ASSET_ITEM, "TOTAL_FIX_ASSET_ITEM");
            this.TOTAL_FIX_ASSET_ITEM.Name = "TOTAL_FIX_ASSET_ITEM";
            this.TOTAL_FIX_ASSET_ITEM.ReadOnly = true;
            // 
            // AMOUNT_DEPRECIATION
            // 
            this.AMOUNT_DEPRECIATION.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AMOUNT_DEPRECIATION.DataPropertyName = "AMOUNT";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "#,##0.####";
            dataGridViewCellStyle6.NullValue = null;
            this.AMOUNT_DEPRECIATION.DefaultCellStyle = dataGridViewCellStyle6;
            resources.ApplyResources(this.AMOUNT_DEPRECIATION, "AMOUNT_DEPRECIATION");
            this.AMOUNT_DEPRECIATION.Name = "AMOUNT_DEPRECIATION";
            this.AMOUNT_DEPRECIATION.ReadOnly = true;
            // 
            // CURRENCY_SING_
            // 
            this.CURRENCY_SING_.DataPropertyName = "CURRENCY_SING";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.CURRENCY_SING_.DefaultCellStyle = dataGridViewCellStyle7;
            resources.ApplyResources(this.CURRENCY_SING_, "CURRENCY_SING_");
            this.CURRENCY_SING_.Name = "CURRENCY_SING_";
            this.CURRENCY_SING_.ReadOnly = true;
            // 
            // CURRENCY_ID
            // 
            this.CURRENCY_ID.DataPropertyName = "CURRENCY_ID";
            resources.ApplyResources(this.CURRENCY_ID, "CURRENCY_ID");
            this.CURRENCY_ID.Name = "CURRENCY_ID";
            this.CURRENCY_ID.ReadOnly = true;
            // 
            // DialogDepreciationMonthly
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogDepreciationMonthly";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.DialogDepreciationMonthly_FormClosed);
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTotal)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private CheckBox chkCheckAll_;
        private Label lblUSE_YEAR;
        private Label lblCURRENCY;
        private DataGridView dgv;
        private ComboBox cboCurrency;
        private DataGridView dgvTotal;
        private Label lblACCUMULATE_DEPRECIATION;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private ExLinkLabel btnREMOVE_DEPRECIATION;
        private ExLinkLabel btnDETAIL;
        private ExLinkLabel btnDEPRECIATION;
        private DateTimePicker dtpYear;
        private Panel panel1;
        private DataGridViewCheckBoxColumn IS_DEPRECIATION_;
        private DataGridViewTextBoxColumn DEPRECIATION_DATE;
        private DataGridViewTextBoxColumn TOTAL_FIX_ASSET_ITEM;
        private DataGridViewTextBoxColumn AMOUNT_DEPRECIATION;
        private DataGridViewTextBoxColumn CURRENCY_SING_;
        private DataGridViewTextBoxColumn CURRENCY_ID;
    }
}