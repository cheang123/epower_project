﻿using EPower.Base.Logic;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageAccountChart : Form
    {

        public TBL_ACCOUNT_CHART AccountChart
        {
            get
            {
                TBL_ACCOUNT_CHART objFixAssetCategory = null;
                if (tvw.SelectedNode != null)
                {
                    int accountId = (int)tvw.SelectedNode.Tag;
                    try
                    {
                        objFixAssetCategory = DBDataContext.Db.TBL_ACCOUNT_CHARTs.FirstOrDefault(x => x.ACCOUNT_ID == accountId);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objFixAssetCategory;
            }
        }

        #region Constructor
        public PageAccountChart()
        {
            InitializeComponent();
            txt_QuickSearch(null, null);

            this.btnEDIT.Enabled =
                this.btnREMOVE.Enabled =
                this.btnADD.Enabled = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_EDIT_ACCOUNT_CHART]);

        }
        #endregion

        #region Operation
        /// <summary>
        /// Load data from database.
        /// </summary>
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            try
            {
                new AccountChartPopulator().PopluateTree(tvw);
                tvw.Nodes[0].Expand();
                this.tvw.ExpandAll();
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }
        void SelectNode(int id)
        {
            var node = this.tvw.GetNodesIncludeAncestors()
                      .FirstOrDefault(x => (int)x.Tag == id);
            if (node != null)
            {
                this.tvw.SelectedNode = node;
            }
        }

        /// <summary>
        /// Add new ExpenseCategory.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNew_Click(object sender, EventArgs e)
        {
            var parentId = 0;
            if (this.tvw.SelectedNode != null)
            {
                parentId = (int)this.tvw.SelectedNode.Tag;
            }
            var objParent = DBDataContext.Db.TBL_ACCOUNT_CHARTs.FirstOrDefault(x => x.ACCOUNT_ID == parentId);

            var objCategory = new TBL_ACCOUNT_CHART();
            objCategory.PARENT_ID = parentId;

            objCategory.POST_TYPE_ID = objParent == null ? 0 : objParent.POST_TYPE_ID;
            DialogAccountChart dig = new DialogAccountChart(GeneralProcess.Insert, objCategory);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                //this.SelectNode(dig.AccountChart.ACCOUNT_ID);
            }
        }

        /// <summary>
        /// Edit ExpenseCategory.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (this.tvw.SelectedNode != null && this.tvw.SelectedNode.Level > 0)
            {
                DialogAccountChart dig = new DialogAccountChart(GeneralProcess.Update, AccountChart);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    this.SelectNode(dig.AccountChart.ACCOUNT_ID);
                }
            }
        }

        /// <summary>
        /// Remove ExpenseCategory.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (IsDeletable())
            {
                DialogAccountChart dig = new DialogAccountChart(GeneralProcess.Delete, AccountChart);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    this.SelectNode(dig.AccountChart.ACCOUNT_ID - 1);
                }
            }
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        #endregion

        #region Method               
        private bool IsDeletable()
        {
            bool val = false;
            if (this.tvw.SelectedNode != null)
            {
                val = true;
                //if (DBDataContext.Db.TBL_FIX_ASSET_CATEGORies.Where(x => x.PARENT_ID == FixAssetCategory.CATEGORY_ID && x.IS_ACTIVE).Count() > 0)
                //{
                //    MsgBox.ShowInformation(Resources.MsgIsDeletable, Resources.MsgTextInfo);
                //    val = false;
                //}
            }
            return val;
        }
        #endregion

    }
}
