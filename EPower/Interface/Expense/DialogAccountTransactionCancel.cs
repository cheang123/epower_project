﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogAccountTransactionCancel : ExDialog
    {
        GeneralProcess _flag;

        TBL_ACCOUNT_TRAN _objNewTran = new TBL_ACCOUNT_TRAN();
        TBL_ACCOUNT_TRAN _objOldTran = new TBL_ACCOUNT_TRAN();

        TBL_ACCOUNT_TRAN_CANCEL _objTranCancel = new TBL_ACCOUNT_TRAN_CANCEL();
        public TBL_ACCOUNT_TRAN AccountTran
        {
            get { return _objNewTran; }
        }


        #region Constructor
        public DialogAccountTransactionCancel(TBL_ACCOUNT_TRAN objAccountTran, AccountType tranType)
        {
            InitializeComponent();
            txtNoteCancel.Focus();
            objAccountTran._CopyTo(_objNewTran);
            objAccountTran._CopyTo(_objOldTran);

            //Set display text dialog.
            this.Text = string.Concat(this.Text, " ", DBDataContext.Db.TBL_ACCOUNT_TYPEs.FirstOrDefault(x => x.TYPE_ID == (int)tranType).TYPE_NAME);

            //look Account item
            bind(tranType);
            //Initailize control 
            read();
        }
        #endregion

        void bind(AccountType tranType)
        {
            var accItem = from ai in DBDataContext.Db.TBL_ACCOUNT_ITEMs
                          join ag in DBDataContext.Db.TBL_ACCOUNT_CATEGORies on ai.CATEGORY_ID equals ag.CATEGORY_ID
                          where ag.TYPE_ID == (int)tranType && ai.IS_ACTIVE
                          select new
                          {
                              ai.ITEM_ID,
                              ITEM_NAME = ai.ITEM_CODE + " " + ai.ITEM_NAME
                          };
            UIHelper.SetDataSourceToComboBox(cboAccountItem, accItem.OrderBy(x => x.ITEM_NAME));

            var tran_by = from at in DBDataContext.Db.TBL_ACCOUNT_TRANs
                          where at.IS_ACTIVE
                          select new { at.TRAN_BY };
            foreach (var item in tran_by.Distinct())
            {
                txtTranBy.AutoCompleteCustomSource.Add(item.TRAN_BY);
            }
        }

        #region Operation
        /// <summary>
        /// Change Keyboard to Khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKeyboardKhmer(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void ChangeKeyboardEnglish(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        /// <summary>
        /// Data transaction.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();

            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    TBL_CHANGE_LOG objparant = DBDataContext.Db.Insert(_objTranCancel);

                    DBDataContext.Db.UpdateChild(_objOldTran, _objNewTran, _objTranCancel, ref objparant);

                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
        #endregion

        #region Method
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();
            if (txtNoteCancel.Text.Trim() == string.Empty)
            {
                txtNoteCancel.SetValidation(string.Format(Resources.REQUIRED, lblCANCEL_NOTE.Text));
                val = true;
            }

            return val;
        }

        /// <summary>
        /// Read data from object.
        /// </summary>
        private void read()
        {
            cboAccountItem.SelectedValue = _objNewTran.TRAN_ACCOUNT_ID;
            txtRefNo.Text = _objNewTran.REF_NO;
            txtTranBy.Text = _objNewTran.TRAN_BY;
            dtpTranDate.Value = _objNewTran.TRAN_DATE;
            txtAmount.Text = UIHelper.FormatCurrency(_objNewTran.AMOUNT);
            txtNote.Text = _objNewTran.NOTE;
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            _objNewTran.IS_ACTIVE = false;
            _objTranCancel = new TBL_ACCOUNT_TRAN_CANCEL();
            _objTranCancel.TRAN_ID = _objNewTran.TRAN_ID;
            _objTranCancel.CREATE_BY = Login.CurrentLogin.LOGIN_NAME;
            _objTranCancel.CREATE_ON = DBDataContext.Db.GetSystemDate();
            _objTranCancel.NOTE = txtNoteCancel.Text;
        }
        #endregion

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNewTran);
        }
    }
}