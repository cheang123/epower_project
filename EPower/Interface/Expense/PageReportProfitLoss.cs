﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageReportProfitLoss : Form
    {
        CrystalReportHelper ch = null;
        public PageReportProfitLoss()
        {
            InitializeComponent();
            viewer.DefaultView();
            this.cboReport.SelectedIndex = 0;
            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("REPORT_ID", typeof(int));
            dtReport.Columns.Add("REPORT_NAME", typeof(string));

            DataRow drReportMonthly = dtReport.NewRow();
            drReportMonthly["REPORT_ID"] = 1;
            drReportMonthly["REPORT_NAME"] = Resources.MONTHLY;
            dtReport.Rows.Add(drReportMonthly);

            DataRow drReportYearly = dtReport.NewRow();
            drReportYearly["REPORT_ID"] = 2;
            drReportYearly["REPORT_NAME"] = Resources.YEARLY;
            dtReport.Rows.Add(drReportYearly);

            UIHelper.SetDataSourceToComboBox(cboReport, dtReport);

            UIHelper.SetDataSourceToComboBox(this.cboDisplayCurrency, Lookup.GetCurrencies());
            var objDefaultCurrency = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.IS_DEFAULT_CURRENCY);
            if (objDefaultCurrency != null)
            {
                this.cboDisplayCurrency.SelectedValue = objDefaultCurrency.CURRENCY_ID;
            }
        } 
       
        private void btnViewReport_Click(object sender, EventArgs e)
        {
            Runner.Run(this.viewReport);
        }
          
        private void viewReport()
        {
            CrystalReportHelper c = new CrystalReportHelper("ReportAccountProfitLoss.rpt");
            c.SetParameter("@D1", dtp1.Value);
            c.SetParameter("@D2", dtp2.Value);
            c.SetParameter("@DISPLAY_CURRENCY_ID", (int)cboDisplayCurrency.SelectedValue);
            this.viewer.ReportSource = c.Report;
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            this.sendMail();
        }

        private void sendMail()
        {
            try
            {
                this.viewReport();
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + this.ch.ReportName + ".xls").FullName;
                this.ch.ExportToExcel(exportPath);
                DialogSendMail.Instance.Add(this.ch.ReportName, exportPath);
                DialogSendMail.Instance.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.ShowWarning(EPower.Properties.Resources.YOU_CANNOT_SEND_EMAIL, Resources.WARNING);
                MsgBox.LogError(ex);
            }
        }

        private void cboReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            DateTime now=DBDataContext.Db.GetSystemDate();
            if (this.cboReport.SelectedIndex == 0)
            {
                this.dtp1.Value = new DateTime(now.Year, now.Month, 1);
            }
            else
            {
                this.dtp1.Value = new DateTime(now.Year, 1, 1);
            }
        }

        private void dtp1_ValueChanged(object sender, EventArgs e)
        {
            if (this.cboReport.SelectedIndex == 0)
            {
                this.dtp2.Value = dtp1.Value.Date.AddMonths(1);
                if (dtp1.Value.Day == 1)
                {
                    this.dtp2.Value = dtp2.Value.AddDays(-1);
                } 
            }
            else
            {
                this.dtp2.Value = dtp1.Value.Date.AddYears(1);
                if (dtp1.Value.Day == 1)
                {
                    this.dtp2.Value = dtp2.Value.AddDays(-1);
                } 
            }
        } 
    }
}
