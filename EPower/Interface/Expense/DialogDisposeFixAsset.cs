﻿using EPower.Base.Helper;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using Login = SoftTech.Security.Logic.Login;

namespace EPower.Interface
{
    public partial class DialogDisposeFixAsset : ExDialog
    {
        GeneralProcess _flag;
        bool load = false;

        TBL_FIX_ASSET_ITEM _objFixAsset = new TBL_FIX_ASSET_ITEM();
        TBL_FIX_ASSET_ITEM _objOldFixAsset = new TBL_FIX_ASSET_ITEM();
        TBL_FIX_ASSET_DISPOSE _objNew = new TBL_FIX_ASSET_DISPOSE();
        TBL_FIX_ASSET_DISPOSE _objOld = new TBL_FIX_ASSET_DISPOSE();
        TBL_DEPRECIATION _objDepreciation = new TBL_DEPRECIATION();
        decimal totalMonth = 0;
        

        #region Constructor
        public DialogDisposeFixAsset(GeneralProcess flag, TBL_FIX_ASSET_ITEM objFixAsset, TBL_FIX_ASSET_DISPOSE objDisposeFixAsset)
        {
            InitializeComponent();
            UIHelper.SetEnabled(this, !(flag == GeneralProcess.Delete));
            _flag = flag;
            this.Text = flag.GetText(this.Text);
            // Account 
            new AccountChartPopulator().PopluateTree(cboPaymentAccount.TreeView, AccountChartHelper.GetAccounts(AccountConfig.PAYMENT_ACCOUNTS));
            cboPaymentAccount.TreeView.SelectedNode = cboPaymentAccount.TreeView.GetNodesIncludeAncestors().FirstOrDefault(x => (int)x.Tag == AccountChartHelper.GetAccounts(AccountConfig.PAYMENT_DEFAULT_ACCOUNTS).FirstOrDefault().ACCOUNT_ID);
            cboPaymentAccount.Text = cboPaymentAccount.TreeView.SelectedNode != null ? cboPaymentAccount.TreeView.SelectedNode.Text : "";

            new AccountChartPopulator().PopluateTree(cboAccountIncome.TreeView, AccountChartHelper.GetAccounts(AccountConfig.INCOME_DISPOSE_ACCOUNTS));
            
            UIHelper.SetDataSourceToComboBox(cboCurrency, Lookup.GetCurrencies());
            cboCurrency.Enabled = false;
            var tranBy = from a in DBDataContext.Db.TBL_FIX_ASSET_DISPOSEs
                        where a.IS_ACTIVE
                        select new { a.TRAN_BY };
            foreach (var item in tranBy.Distinct())
            {
                txtTranBy.AutoCompleteCustomSource.Add(item.TRAN_BY);
            }

            objFixAsset._CopyTo(_objFixAsset);
            objFixAsset._CopyTo(_objOldFixAsset);
            objDisposeFixAsset._CopyTo(_objNew);
            objDisposeFixAsset._CopyTo(_objOld);
            _objDepreciation = DBDataContext.Db.TBL_DEPRECIATIONs.FirstOrDefault(x => x.IS_ACTIVE && x.FIX_ASSET_ITEM_ID == objFixAsset.FIX_ASSET_ITEM_ID);
            totalMonth = DBDataContext.Db.TBL_DEPRECIATIONs.Where(x => x.IS_ACTIVE && x.FIX_ASSET_ITEM_ID == objFixAsset.FIX_ASSET_ITEM_ID
                                                                && x.STATUS_ID == (int)DepreciationStatus.NotYet).Count();
            read();
            if (_flag == GeneralProcess.Insert)
            {
                if (DataHelper.ParseToDecimal(txtQtyBalance.Text) <= 0)
                {
                    MsgBox.ShowInformation(Resources.MS_CANNOT_DISPOSED_ITEM_CANNOT_CHANGE_QTY);
                    btnOK.Enabled = false;
                } 
            } 
            else
            {
                if (DBDataContext.Db.TBL_DEPRECIATIONs.Where(x => x.FIX_ASSET_ITEM_ID == _objNew.FIX_ASSET_ITEM_ID && x.IS_ACTIVE
                                                            && x.STATUS_ID == (int)DepreciationStatus.ReadyDepreciation
                                                            && x.DEPRECIATION_DATE > objDisposeFixAsset.TRAN_DATE).Count() > 0)
                {
                    MsgBox.ShowInformation(Resources.MS_CANNOT_UPDATE_OR_DELETE_DISPOSE);
                    btnOK.Enabled = false;
                }
            }
            load = true;
        }


        #endregion

        #region Method
        
        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();
            if (cboPaymentAccount.TreeView.SelectedNode == null)
            {
                cboPaymentAccount.SetValidation(string.Format(Resources.REQUIRED, lblPAYMENT_ACCOUNT.Text));
                val = true;
            }
            if (cboAccountIncome.TreeView.SelectedNode == null)
            {
                cboAccountIncome.SetValidation(string.Format(Resources.REQUIRED, lblINCOME_ACCOUNT.Text));
                val = true;
            }
            if (txtPrice.Text.Trim() == string.Empty && _flag!=GeneralProcess.Delete)
            {
                txtPrice.SetValidation(string.Format(Resources.REQUIRED,lblUNIT_PRICE.Text));
                val=true;
            }
            if(txtQty.Text.Trim()==string.Empty){
                txtQty.SetValidation(string.Format(Resources.REQUIRED,lblQUANTITY.Text));
                val=true;
            }
            if (txtTranBy.Text.Trim() == string.Empty)
            {
                txtTranBy.SetValidation(string.Format(Resources.REQUIRED, lblCREATE_BY.Text));
                val = true;
            }
            if (DataHelper.ParseToDecimal(txtSoldAmount.Text) <0 && _flag!=GeneralProcess.Delete)
            {
                txtSoldAmount.SetValidation(string.Format(Resources.REQUIRED_POSITIVE_NUMBER, lblDISPOSE_SOLD_VALUE.Text));
                val = true;
            }
            if (DataHelper.ParseToDecimal(txtQty.Text) > DataHelper.ParseToDecimal(txtQtyBalance.Text) && _flag!=GeneralProcess.Delete)
            {
                txtQty.SetValidation(Resources.MS_QTY_DISPOSE_MORE_THAN_QTY_IN_BALANCE);
                val = true;
            }


            return val;
        }
         
        private void read()
        {
            txtFixAssetName.Text = _objFixAsset.FIX_ASSET_NAME;
            // Account
            cboPaymentAccount.TreeView.SelectedNode = this.cboPaymentAccount.TreeView.GetNodesIncludeAncestors()
                                                   .FirstOrDefault(x => (int)x.Tag == this._objNew.PAYMENT_ACCOUNT_ID);
            if (cboPaymentAccount.TreeView.SelectedNode != null)
            {
                this.cboPaymentAccount.Text = cboPaymentAccount.TreeView.SelectedNode.Text;
            }

            cboAccountIncome.TreeView.SelectedNode = this.cboAccountIncome.TreeView.GetNodesIncludeAncestors()
                                                   .FirstOrDefault(x => (int)x.Tag == this._objNew.INCOME_DISPOSE_ACCOUNT_ID);
            if (cboAccountIncome.TreeView.SelectedNode != null)
            {
                this.cboAccountIncome.Text = cboAccountIncome.TreeView.SelectedNode.Text;
            }
            txtTranBy.Text = _objNew.TRAN_BY;
            txtRefNo.Text = _objNew.REF_NO;
            dtpTranDate.Value = _objNew.TRAN_DATE;
            cboCurrency.SelectedValue = _objFixAsset.CURRENCY_ID;
            txtQty.Text = _objNew.QTY.ToString();
            txtPrice.Text = UIHelper.FormatCurrency(_objNew.PRICE, _objNew.CURRENCY_ID);
            txtBookValue.Text = UIHelper.FormatCurrency(_objNew.BOOK_VALUE, _objNew.CURRENCY_ID);
            txtSoldAmount.Text = UIHelper.FormatCurrency(_objNew.SOLD_VALUE, _objFixAsset.CURRENCY_ID);
            txtGainLoss.Text = UIHelper.FormatCurrency(_objNew.SOLD_VALUE - _objNew.BOOK_VALUE, _objNew.CURRENCY_ID);
            txtNote.Text = _objNew.NOTE;
            decimal d =DBDataContext.Db.TBL_FIX_ASSET_DISPOSEs.Where(x => x.IS_ACTIVE && x.FIX_ASSET_ITEM_ID == _objFixAsset.FIX_ASSET_ITEM_ID).Sum(x => (decimal?)x.QTY) ?? 0;
            txtQtyBalance.Text = (_objFixAsset.QUANTITY -d).ToString();
        }
    
        private void write()
        {
            _objNew.FIX_ASSET_ITEM_ID = _objFixAsset.FIX_ASSET_ITEM_ID;
            _objNew.BOOK_VALUE = DataHelper.ParseToDecimal(txtBookValue.Text);
            _objNew.SOLD_VALUE = DataHelper.ParseToDecimal(txtSoldAmount.Text);
            _objNew.CURRENCY_ID = _objFixAsset.CURRENCY_ID;
            _objNew.PAYMENT_ACCOUNT_ID = (int)cboPaymentAccount.TreeView.SelectedNode.Tag;
            _objNew.INCOME_DISPOSE_ACCOUNT_ID = (int)cboAccountIncome.TreeView.SelectedNode.Tag;
            _objNew.TRAN_BY = txtTranBy.Text;
            _objNew.TRAN_DATE = dtpTranDate.Value;
            _objNew.REF_NO = txtRefNo.Text;
            _objNew.NOTE = txtNote.Text;
            _objNew.CREATE_BY = Login.CurrentLogin.LOGIN_NAME;
            _objNew.CREATE_ON = DBDataContext.Db.GetSystemDate();
            _objNew.IS_ACTIVE = true;
            _objNew.QTY = DataHelper.ParseToDecimal(txtQty.Text);
            _objNew.PRICE = DataHelper.ParseToDecimal(txtPrice.Text);
        }
        
        #endregion

        
        private void ChangeKeyboardKhmer(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        private void ChangeKeyboardEnglish(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        } 
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
             //If record is duplicate.
            this.ClearAllValidation(); 

            try
            {
                Runner.RunNewThread(delegate {
                    using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                    {
                        if (_flag == GeneralProcess.Insert)
                        {
                            DBDataContext.Db.Insert(_objNew);
                        }
                        else if (_flag == GeneralProcess.Update)
                        {
                            DBDataContext.Db.Update(_objOld, _objNew);
                        }
                        else if (_flag == GeneralProcess.Delete)
                        {
                            DBDataContext.Db.Delete(_objNew);
                        }
                        DBDataContext.Db.ExecuteCommand("EXEC FIX_ASSET_DISPOSE {0},{1}", _objFixAsset.FIX_ASSET_ITEM_ID, _objFixAsset.CURRENCY_ID);
                        tran.Complete();
                        this.DialogResult = DialogResult.OK;
                    }
                },Resources.PROCESSING);                               
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }            
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }  
        private void DialogAccountCategory_Load(object sender, EventArgs e)
        {
            this.txtTranBy.Focus();
        }
        private void txtAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        }
        private void txtQty_TextChanged(object sender, EventArgs e)
        {
            if (load)
            {
                decimal qtyBalance=DataHelper.ParseToDecimal(txtQtyBalance.Text);
                decimal qtyDispose=DataHelper.ParseToDecimal(txtQty.Text);
                decimal bookValue = (DBDataContext.Db.TBL_DEPRECIATIONs.Where(x => x.IS_ACTIVE && x.STATUS_ID == (int)DepreciationStatus.NotYet
                                                                          && x.FIX_ASSET_ITEM_ID == _objFixAsset.FIX_ASSET_ITEM_ID).Sum(x => (decimal?)x.PRICE * (qtyDispose)))??0;
                decimal soldValue = DataHelper.ParseToDecimal(txtQty.Text) * DataHelper.ParseToDecimal(txtPrice.Text);
                decimal GainLossValue = soldValue - bookValue;

                txtSoldAmount.Text = UIHelper.FormatCurrency(soldValue, _objFixAsset.CURRENCY_ID);
                txtBookValue.Text = UIHelper.FormatCurrency(bookValue, _objFixAsset.CURRENCY_ID);
                txtGainLoss.Text = UIHelper.FormatCurrency(GainLossValue, _objFixAsset.CURRENCY_ID);
            } 
        }
                 
    }
}
