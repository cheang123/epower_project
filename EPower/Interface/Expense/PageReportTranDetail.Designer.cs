﻿using System.ComponentModel;
using System.Windows.Forms;
using AxCrystalActiveXReportViewerLib105;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageReportTranDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageReportTranDetail));
            this.panel1 = new System.Windows.Forms.Panel();
            this.cboTranCurrency = new System.Windows.Forms.ComboBox();
            this.lblSHOW_AS = new System.Windows.Forms.Label();
            this.cboDisplayCurrency = new System.Windows.Forms.ComboBox();
            this.cboPaymentAccount = new SoftTech.Component.TreeComboBox();
            this.cboTransAccount = new SoftTech.Component.TreeComboBox();
            this.dtp2 = new System.Windows.Forms.DateTimePicker();
            this.btnMAIL = new SoftTech.Component.ExButton();
            this.dtp1 = new System.Windows.Forms.DateTimePicker();
            this.btnVIEW = new SoftTech.Component.ExButton();
            this.viewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.cboTranCurrency);
            this.panel1.Controls.Add(this.lblSHOW_AS);
            this.panel1.Controls.Add(this.cboDisplayCurrency);
            this.panel1.Controls.Add(this.cboPaymentAccount);
            this.panel1.Controls.Add(this.cboTransAccount);
            this.panel1.Controls.Add(this.dtp2);
            this.panel1.Controls.Add(this.btnMAIL);
            this.panel1.Controls.Add(this.dtp1);
            this.panel1.Controls.Add(this.btnVIEW);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // cboTranCurrency
            // 
            this.cboTranCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTranCurrency.FormattingEnabled = true;
            resources.ApplyResources(this.cboTranCurrency, "cboTranCurrency");
            this.cboTranCurrency.Name = "cboTranCurrency";
            // 
            // lblSHOW_AS
            // 
            resources.ApplyResources(this.lblSHOW_AS, "lblSHOW_AS");
            this.lblSHOW_AS.Name = "lblSHOW_AS";
            // 
            // cboDisplayCurrency
            // 
            this.cboDisplayCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDisplayCurrency.FormattingEnabled = true;
            resources.ApplyResources(this.cboDisplayCurrency, "cboDisplayCurrency");
            this.cboDisplayCurrency.Name = "cboDisplayCurrency";
            // 
            // cboPaymentAccount
            // 
            this.cboPaymentAccount.AbsoluteChildrenSelectableOnly = false;
            this.cboPaymentAccount.BranchSeparator = "/";
            this.cboPaymentAccount.Imagelist = null;
            resources.ApplyResources(this.cboPaymentAccount, "cboPaymentAccount");
            this.cboPaymentAccount.Name = "cboPaymentAccount";
            this.cboPaymentAccount.PopupHeight = 250;
            this.cboPaymentAccount.PopupWidth = 350;
            this.cboPaymentAccount.SelectedNode = null;
            // 
            // cboTransAccount
            // 
            this.cboTransAccount.AbsoluteChildrenSelectableOnly = false;
            this.cboTransAccount.BranchSeparator = "/";
            this.cboTransAccount.Imagelist = null;
            resources.ApplyResources(this.cboTransAccount, "cboTransAccount");
            this.cboTransAccount.Name = "cboTransAccount";
            this.cboTransAccount.PopupHeight = 250;
            this.cboTransAccount.PopupWidth = 350;
            this.cboTransAccount.SelectedNode = null;
            // 
            // dtp2
            // 
            resources.ApplyResources(this.dtp2, "dtp2");
            this.dtp2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp2.Name = "dtp2";
            this.dtp2.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            // 
            // btnMAIL
            // 
            resources.ApplyResources(this.btnMAIL, "btnMAIL");
            this.btnMAIL.Name = "btnMAIL";
            this.btnMAIL.UseVisualStyleBackColor = true;
            this.btnMAIL.Click += new System.EventHandler(this.btnMail_Click);
            // 
            // dtp1
            // 
            resources.ApplyResources(this.dtp1, "dtp1");
            this.dtp1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp1.Name = "dtp1";
            this.dtp1.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            // 
            // btnVIEW
            // 
            resources.ApplyResources(this.btnVIEW, "btnVIEW");
            this.btnVIEW.Name = "btnVIEW";
            this.btnVIEW.UseVisualStyleBackColor = true;
            this.btnVIEW.Click += new System.EventHandler(this.btnViewReport_Click);
            // 
            // viewer
            // 
            this.viewer.ActiveViewIndex = -1;
            this.viewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.viewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.viewer.DisplayStatusBar = false;
            resources.ApplyResources(this.viewer, "viewer");
            this.viewer.Name = "viewer";
            // 
            // PageReportTranDetail
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.viewer);
            this.Controls.Add(this.panel1);
            this.Name = "PageReportTranDetail";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnVIEW;
        private DateTimePicker dtp1;
        private ExButton btnMAIL;
        private DateTimePicker dtp2;
        private TreeComboBox cboTransAccount;
        private TreeComboBox cboPaymentAccount;
        private Label lblSHOW_AS;
        public ComboBox cboDisplayCurrency;
        public ComboBox cboTranCurrency;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer viewer;
    }
}
