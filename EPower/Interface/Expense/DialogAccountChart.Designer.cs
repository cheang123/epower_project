﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogAccountChart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogAccountChart));
            this.lblACCOUNT_TYPE = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClose = new SoftTech.Component.ExButton();
            this.btnAdd = new SoftTech.Component.ExButton();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.lblNOTE = new System.Windows.Forms.Label();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cboParent = new SoftTech.Component.TreeComboBox();
            this.lblPARENT_ACCOUNT = new System.Windows.Forms.Label();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.lblACCOUNT_CODE = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblACCOUNT_NAME_EN = new System.Windows.Forms.Label();
            this.txtNameEn = new System.Windows.Forms.TextBox();
            this.lblACCOUNT_POST_TYPE = new System.Windows.Forms.Label();
            this.cboPostType = new System.Windows.Forms.ComboBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.txtCode);
            this.content.Controls.Add(this.cboParent);
            this.content.Controls.Add(this.cboPostType);
            this.content.Controls.Add(this.lblACCOUNT_POST_TYPE);
            this.content.Controls.Add(this.txtNameEn);
            this.content.Controls.Add(this.lblACCOUNT_NAME_EN);
            this.content.Controls.Add(this.label8);
            this.content.Controls.Add(this.lblACCOUNT_CODE);
            this.content.Controls.Add(this.lblPARENT_ACCOUNT);
            this.content.Controls.Add(this.label7);
            this.content.Controls.Add(this.txtNote);
            this.content.Controls.Add(this.lblNOTE);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnClose);
            this.content.Controls.Add(this.btnAdd);
            this.content.Controls.Add(this.txtName);
            this.content.Controls.Add(this.lblACCOUNT_TYPE);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblACCOUNT_TYPE, 0);
            this.content.Controls.SetChildIndex(this.txtName, 0);
            this.content.Controls.SetChildIndex(this.btnAdd, 0);
            this.content.Controls.SetChildIndex(this.btnClose, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.lblNOTE, 0);
            this.content.Controls.SetChildIndex(this.txtNote, 0);
            this.content.Controls.SetChildIndex(this.label7, 0);
            this.content.Controls.SetChildIndex(this.lblPARENT_ACCOUNT, 0);
            this.content.Controls.SetChildIndex(this.lblACCOUNT_CODE, 0);
            this.content.Controls.SetChildIndex(this.label8, 0);
            this.content.Controls.SetChildIndex(this.lblACCOUNT_NAME_EN, 0);
            this.content.Controls.SetChildIndex(this.txtNameEn, 0);
            this.content.Controls.SetChildIndex(this.lblACCOUNT_POST_TYPE, 0);
            this.content.Controls.SetChildIndex(this.cboPostType, 0);
            this.content.Controls.SetChildIndex(this.cboParent, 0);
            this.content.Controls.SetChildIndex(this.txtCode, 0);
            // 
            // lblACCOUNT_TYPE
            // 
            resources.ApplyResources(this.lblACCOUNT_TYPE, "lblACCOUNT_TYPE");
            this.lblACCOUNT_TYPE.Name = "lblACCOUNT_TYPE";
            // 
            // txtName
            // 
            resources.ApplyResources(this.txtName, "txtName");
            this.txtName.Name = "txtName";
            this.txtName.Enter += new System.EventHandler(this.ChangeKeyboardKhmer);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnClose
            // 
            resources.ApplyResources(this.btnClose, "btnClose");
            this.btnClose.Name = "btnClose";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnAdd
            // 
            resources.ApplyResources(this.btnAdd, "btnAdd");
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // lblNOTE
            // 
            resources.ApplyResources(this.lblNOTE, "lblNOTE");
            this.lblNOTE.Name = "lblNOTE";
            // 
            // txtNote
            // 
            resources.ApplyResources(this.txtNote, "txtNote");
            this.txtNote.Name = "txtNote";
            this.txtNote.Enter += new System.EventHandler(this.ChangeKeyboardKhmer);
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Name = "label7";
            // 
            // cboParent
            // 
            this.cboParent.AbsoluteChildrenSelectableOnly = false;
            this.cboParent.BranchSeparator = "/";
            this.cboParent.Imagelist = null;
            resources.ApplyResources(this.cboParent, "cboParent");
            this.cboParent.Name = "cboParent";
            this.cboParent.PopupHeight = 250;
            this.cboParent.PopupWidth = 0;
            this.cboParent.SelectedNode = null;
            this.cboParent.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            // 
            // lblPARENT_ACCOUNT
            // 
            resources.ApplyResources(this.lblPARENT_ACCOUNT, "lblPARENT_ACCOUNT");
            this.lblPARENT_ACCOUNT.Name = "lblPARENT_ACCOUNT";
            // 
            // txtCode
            // 
            resources.ApplyResources(this.txtCode, "txtCode");
            this.txtCode.Name = "txtCode";
            this.txtCode.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            // 
            // lblACCOUNT_CODE
            // 
            resources.ApplyResources(this.lblACCOUNT_CODE, "lblACCOUNT_CODE");
            this.lblACCOUNT_CODE.Name = "lblACCOUNT_CODE";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Name = "label8";
            // 
            // lblACCOUNT_NAME_EN
            // 
            resources.ApplyResources(this.lblACCOUNT_NAME_EN, "lblACCOUNT_NAME_EN");
            this.lblACCOUNT_NAME_EN.Name = "lblACCOUNT_NAME_EN";
            // 
            // txtNameEn
            // 
            resources.ApplyResources(this.txtNameEn, "txtNameEn");
            this.txtNameEn.Name = "txtNameEn";
            this.txtNameEn.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            // 
            // lblACCOUNT_POST_TYPE
            // 
            resources.ApplyResources(this.lblACCOUNT_POST_TYPE, "lblACCOUNT_POST_TYPE");
            this.lblACCOUNT_POST_TYPE.Name = "lblACCOUNT_POST_TYPE";
            // 
            // cboPostType
            // 
            this.cboPostType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPostType.FormattingEnabled = true;
            resources.ApplyResources(this.cboPostType, "cboPostType");
            this.cboPostType.Name = "cboPostType";
            // 
            // DialogAccountChart
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogAccountChart";
            this.Load += new System.EventHandler(this.DialogAccountCategory_Load);
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBox txtName;
        private Label lblACCOUNT_TYPE;
        private Panel panel1;
        private ExButton btnClose;
        private ExButton btnAdd;
        private ExButton btnCHANGE_LOG;
        private TextBox txtNote;
        private Label lblNOTE;
        private Label label7;
        private Label lblPARENT_ACCOUNT;
        private TreeComboBox cboParent;
        private Label lblACCOUNT_CODE;
        private TextBox txtCode;
        private Label label8;
        private TextBox txtNameEn;
        private Label lblACCOUNT_NAME_EN;
        private Label lblACCOUNT_POST_TYPE;
        public ComboBox cboPostType;
    }
}