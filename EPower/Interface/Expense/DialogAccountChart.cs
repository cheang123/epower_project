﻿using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;

namespace EPower.Interface
{
    public partial class DialogAccountChart : ExDialog
    {
        GeneralProcess _flag;


        TBL_ACCOUNT_CHART _objNew = new TBL_ACCOUNT_CHART();
        TBL_ACCOUNT_CHART _objOld = new TBL_ACCOUNT_CHART();
        public TBL_ACCOUNT_CHART AccountChart
        {
            get { return _objNew; }
        }
        

        #region Constructor
        public DialogAccountChart(GeneralProcess flag, TBL_ACCOUNT_CHART objFixAssetCategory)
        {
            InitializeComponent();
            new AccountChartPopulator().PopluateTree(cboParent.TreeView);
            UIHelper.SetDataSourceToComboBox(cboPostType, DBDataContext.Db.TLKP_ACCOUNT_POST_TYPEs);

            _flag = flag;
            objFixAssetCategory._CopyTo(_objNew);
            objFixAssetCategory._CopyTo(_objOld);
            this.Text = flag.GetText(this.Text); 
            read();
            UIHelper.SetEnabled(this, !(flag == GeneralProcess.Delete));
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert; 
        }

       

        #endregion

        #region Operation
        /// <summary>
        /// Change Keyboard to Khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKeyboardKhmer(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        private void ChangeKeyboardEnglish(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }  

        /// <summary>
        /// Data transaction.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
             //If record is duplicate.
            this.ClearAllValidation();
            //if (DBDataContext.Db.IsExits(_objNew, "ACCOUNT_NAME"))
            //{
            //    txtName.SetValidation(string.Format(Resources.MsgIsExits, lblAccountCategory.Text));
            //    return;
            //}

            try
            {
                using (TransactionScope tran=new TransactionScope( TransactionScopeOption.Required,TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objNew);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objNew);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }                                               
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }            
        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
        #endregion

        #region Method
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();
            if (this.cboParent.TreeView.SelectedNode == null)
            {
                cboParent.SetValidation(string.Format(Resources.REQUIRED, lblPARENT_ACCOUNT.Text));
                val = true;
            }
            if (txtCode.Text.Trim() == string.Empty)
            {
                txtCode.SetValidation(string.Format(Resources.REQUIRED, lblACCOUNT_CODE.Text));
                val = true;
            }
            if (txtName.Text.Trim()==string.Empty)
            {
                txtName.SetValidation(string.Format(Resources.REQUIRED, lblACCOUNT_TYPE.Text));
                val = true;
            }
            return val;
        }

        /// <summary>
        /// Read data from object.
        /// </summary>
        private void read()
        {  
            cboParent.TreeView.SelectedNode = this.cboParent.TreeView.GetNodesIncludeAncestors()
                                                   .FirstOrDefault(x => (int)x.Tag == this._objNew.PARENT_ID);
            if (cboParent.TreeView.SelectedNode  != null)
            {
                this.cboParent.Text = cboParent.TreeView.SelectedNode.Text;
            }
            txtNameEn.Text = _objNew.ACCOUNT_NAME_EN ;
            txtName.Text = _objNew.ACCOUNT_NAME;
            txtNote.Text = _objNew.NOTE;
            txtCode.Text = _objNew.ACCOUNT_CODE;
            cboPostType.SelectedValue = _objNew.POST_TYPE_ID;
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            _objNew.ACCOUNT_NAME = txtName.Text; 
            _objNew.NOTE = txtNote.Text;
            _objNew.ACCOUNT_CODE = txtCode.Text;
            _objNew.ACCOUNT_NAME_EN =  txtNameEn.Text;
            _objNew.PARENT_ID = (int)cboParent.TreeView.SelectedNode.Tag;
            _objNew.POST_TYPE_ID = (int)this.cboPostType.SelectedValue;
        }
        #endregion

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }

        private void DialogAccountCategory_Load(object sender, EventArgs e)
        { 
            this.txtCode.Focus();
            this.txtCode.Select();
        }

    }
}