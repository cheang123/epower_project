﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogDepreciationByMonth
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogDepreciationByMonth));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.chkCheckAll_ = new System.Windows.Forms.CheckBox();
            this.txtDepreciationBy = new System.Windows.Forms.TextBox();
            this.lblFIX_ASSET = new System.Windows.Forms.Label();
            this.dtpMonth = new System.Windows.Forms.DateTimePicker();
            this.lblUSE_MONTH = new System.Windows.Forms.Label();
            this.lblCURRENCY = new System.Windows.Forms.Label();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.btnDETAIL = new System.Windows.Forms.LinkLabel();
            this.dgvTotal = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblACCUMULATE_DEPRECIATION = new System.Windows.Forms.Label();
            this.IS_DEPRECIATION_ = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.DEPRECIATION_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_FIX_ASSET_ITEM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_DEPRECIATION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_SING_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTotal)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.dgvTotal);
            this.content.Controls.Add(this.btnDETAIL);
            this.content.Controls.Add(this.cboCurrency);
            this.content.Controls.Add(this.chkCheckAll_);
            this.content.Controls.Add(this.dgv);
            this.content.Controls.Add(this.lblCURRENCY);
            this.content.Controls.Add(this.dtpMonth);
            this.content.Controls.Add(this.lblACCUMULATE_DEPRECIATION);
            this.content.Controls.Add(this.lblUSE_MONTH);
            this.content.Controls.Add(this.txtDepreciationBy);
            this.content.Controls.Add(this.lblFIX_ASSET);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.lblFIX_ASSET, 0);
            this.content.Controls.SetChildIndex(this.txtDepreciationBy, 0);
            this.content.Controls.SetChildIndex(this.lblUSE_MONTH, 0);
            this.content.Controls.SetChildIndex(this.lblACCUMULATE_DEPRECIATION, 0);
            this.content.Controls.SetChildIndex(this.dtpMonth, 0);
            this.content.Controls.SetChildIndex(this.lblCURRENCY, 0);
            this.content.Controls.SetChildIndex(this.dgv, 0);
            this.content.Controls.SetChildIndex(this.chkCheckAll_, 0);
            this.content.Controls.SetChildIndex(this.cboCurrency, 0);
            this.content.Controls.SetChildIndex(this.btnDETAIL, 0);
            this.content.Controls.SetChildIndex(this.dgvTotal, 0);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // chkCheckAll_
            // 
            resources.ApplyResources(this.chkCheckAll_, "chkCheckAll_");
            this.chkCheckAll_.Name = "chkCheckAll_";
            this.chkCheckAll_.UseVisualStyleBackColor = true;
            this.chkCheckAll_.CheckedChanged += new System.EventHandler(this.chkCheckAll_CheckedChanged);
            // 
            // txtDepreciationBy
            // 
            resources.ApplyResources(this.txtDepreciationBy, "txtDepreciationBy");
            this.txtDepreciationBy.Name = "txtDepreciationBy";
            this.txtDepreciationBy.ReadOnly = true;
            // 
            // lblFIX_ASSET
            // 
            resources.ApplyResources(this.lblFIX_ASSET, "lblFIX_ASSET");
            this.lblFIX_ASSET.Name = "lblFIX_ASSET";
            // 
            // dtpMonth
            // 
            resources.ApplyResources(this.dtpMonth, "dtpMonth");
            this.dtpMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMonth.Name = "dtpMonth";
            this.dtpMonth.ValueChanged += new System.EventHandler(this.dtpMonth_ValueChanged);
            // 
            // lblUSE_MONTH
            // 
            resources.ApplyResources(this.lblUSE_MONTH, "lblUSE_MONTH");
            this.lblUSE_MONTH.Name = "lblUSE_MONTH";
            // 
            // lblCURRENCY
            // 
            resources.ApplyResources(this.lblCURRENCY, "lblCURRENCY");
            this.lblCURRENCY.Name = "lblCURRENCY";
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IS_DEPRECIATION_,
            this.DEPRECIATION_DATE,
            this.TOTAL_FIX_ASSET_ITEM,
            this.AMOUNT_DEPRECIATION,
            this.CURRENCY_SING_,
            this.CURRENCY_ID});
            this.dgv.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellContentClick);
            this.dgv.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellDoubleClick);
            this.dgv.Sorted += new System.EventHandler(this.dgv_Sorted);
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.SelectedIndexChanged += new System.EventHandler(this.dtpMonth_ValueChanged);
            // 
            // btnDETAIL
            // 
            resources.ApplyResources(this.btnDETAIL, "btnDETAIL");
            this.btnDETAIL.Name = "btnDETAIL";
            this.btnDETAIL.TabStop = true;
            this.btnDETAIL.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblDetail_LinkClicked);
            // 
            // dgvTotal
            // 
            this.dgvTotal.AllowUserToAddRows = false;
            this.dgvTotal.AllowUserToDeleteRows = false;
            this.dgvTotal.AllowUserToResizeColumns = false;
            this.dgvTotal.AllowUserToResizeRows = false;
            this.dgvTotal.BackgroundColor = System.Drawing.Color.White;
            this.dgvTotal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvTotal.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvTotal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTotal.ColumnHeadersVisible = false;
            this.dgvTotal.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvTotal.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvTotal.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgvTotal, "dgvTotal");
            this.dgvTotal.Name = "dgvTotal";
            this.dgvTotal.ReadOnly = true;
            this.dgvTotal.RowHeadersVisible = false;
            this.dgvTotal.RowTemplate.Height = 25;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "CURRENCY_ID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn2, "dataGridViewTextBoxColumn2");
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "AMOUNT";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.Format = "N2";
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle1;
            resources.ApplyResources(this.dataGridViewTextBoxColumn3, "dataGridViewTextBoxColumn3");
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "CURRENCY_SING";
            this.dataGridViewTextBoxColumn4.FillWeight = 65F;
            resources.ApplyResources(this.dataGridViewTextBoxColumn4, "dataGridViewTextBoxColumn4");
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // lblACCUMULATE_DEPRECIATION
            // 
            resources.ApplyResources(this.lblACCUMULATE_DEPRECIATION, "lblACCUMULATE_DEPRECIATION");
            this.lblACCUMULATE_DEPRECIATION.Name = "lblACCUMULATE_DEPRECIATION";
            // 
            // IS_DEPRECIATION_
            // 
            this.IS_DEPRECIATION_.DataPropertyName = "IS_DEPRECIATION";
            resources.ApplyResources(this.IS_DEPRECIATION_, "IS_DEPRECIATION_");
            this.IS_DEPRECIATION_.Name = "IS_DEPRECIATION_";
            this.IS_DEPRECIATION_.ReadOnly = true;
            // 
            // DEPRECIATION_DATE
            // 
            this.DEPRECIATION_DATE.DataPropertyName = "DEPRECIATION_MONTH";
            dataGridViewCellStyle4.Format = "MM-yyyy";
            dataGridViewCellStyle4.NullValue = null;
            this.DEPRECIATION_DATE.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.DEPRECIATION_DATE, "DEPRECIATION_DATE");
            this.DEPRECIATION_DATE.Name = "DEPRECIATION_DATE";
            this.DEPRECIATION_DATE.ReadOnly = true;
            // 
            // TOTAL_FIX_ASSET_ITEM
            // 
            this.TOTAL_FIX_ASSET_ITEM.DataPropertyName = "TOTAL_FIX_ASSET_ITEM";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N0";
            dataGridViewCellStyle5.NullValue = null;
            this.TOTAL_FIX_ASSET_ITEM.DefaultCellStyle = dataGridViewCellStyle5;
            resources.ApplyResources(this.TOTAL_FIX_ASSET_ITEM, "TOTAL_FIX_ASSET_ITEM");
            this.TOTAL_FIX_ASSET_ITEM.Name = "TOTAL_FIX_ASSET_ITEM";
            this.TOTAL_FIX_ASSET_ITEM.ReadOnly = true;
            // 
            // AMOUNT_DEPRECIATION
            // 
            this.AMOUNT_DEPRECIATION.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AMOUNT_DEPRECIATION.DataPropertyName = "AMOUNT";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "#,##0.####";
            dataGridViewCellStyle6.NullValue = null;
            this.AMOUNT_DEPRECIATION.DefaultCellStyle = dataGridViewCellStyle6;
            resources.ApplyResources(this.AMOUNT_DEPRECIATION, "AMOUNT_DEPRECIATION");
            this.AMOUNT_DEPRECIATION.Name = "AMOUNT_DEPRECIATION";
            this.AMOUNT_DEPRECIATION.ReadOnly = true;
            // 
            // CURRENCY_SING_
            // 
            this.CURRENCY_SING_.DataPropertyName = "CURRENCY_SING";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.CURRENCY_SING_.DefaultCellStyle = dataGridViewCellStyle7;
            resources.ApplyResources(this.CURRENCY_SING_, "CURRENCY_SING_");
            this.CURRENCY_SING_.Name = "CURRENCY_SING_";
            this.CURRENCY_SING_.ReadOnly = true;
            // 
            // CURRENCY_ID
            // 
            this.CURRENCY_ID.DataPropertyName = "CURRENCY_ID";
            resources.ApplyResources(this.CURRENCY_ID, "CURRENCY_ID");
            this.CURRENCY_ID.Name = "CURRENCY_ID";
            this.CURRENCY_ID.ReadOnly = true;
            // 
            // DialogDepreciationByMonth
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogDepreciationByMonth";
            this.Load += new System.EventHandler(this.DialogDepreciationByMonth_Load);
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTotal)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnCLOSE;
        private ExButton btnOK;
        private CheckBox chkCheckAll_;
        private TextBox txtDepreciationBy;
        private Label lblFIX_ASSET;
        private DateTimePicker dtpMonth;
        private Label lblUSE_MONTH;
        private Label lblCURRENCY;
        private DataGridView dgv;
        private ComboBox cboCurrency;
        private LinkLabel btnDETAIL;
        private DataGridView dgvTotal;
        private Label lblACCUMULATE_DEPRECIATION;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewCheckBoxColumn IS_DEPRECIATION_;
        private DataGridViewTextBoxColumn DEPRECIATION_DATE;
        private DataGridViewTextBoxColumn TOTAL_FIX_ASSET_ITEM;
        private DataGridViewTextBoxColumn AMOUNT_DEPRECIATION;
        private DataGridViewTextBoxColumn CURRENCY_SING_;
        private DataGridViewTextBoxColumn CURRENCY_ID;
    }
}