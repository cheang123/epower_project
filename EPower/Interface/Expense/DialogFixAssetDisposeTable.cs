﻿using EPower.Base.Helper;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogFixAssetDisposeTable : ExDialog
    {
        TBL_FIX_ASSET_ITEM objFixAssetItem = new TBL_FIX_ASSET_ITEM();
        #region Constructor
        public DialogFixAssetDisposeTable(TBL_FIX_ASSET_ITEM _obj)
        {
            InitializeComponent();
            objFixAssetItem = _obj;
            readFixAsset();
            readDipose();
        } 

        #endregion

        #region Method

        private void readFixAsset()
        {
            txtFixAssetName.Text = objFixAssetItem.FIX_ASSET_NAME;
            dtpStartUse.Value = objFixAssetItem.USE_DATE;
            txtQty.Text = objFixAssetItem.QUANTITY.ToString();
            txtPrice.Text = UIHelper.FormatCurrency(objFixAssetItem.PRICE, objFixAssetItem.CURRENCY_ID);
            txtTotalAmount.Text = UIHelper.FormatCurrency(objFixAssetItem.TOTAL_COST, objFixAssetItem.CURRENCY_ID)+" "+DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x=>x.CURRENCY_ID==objFixAssetItem.CURRENCY_ID).CURRENCY_SING;
        }

        private void readDipose()
        {
            var data = DBDataContext.Db.TBL_FIX_ASSET_DISPOSEs.Where(x => x.FIX_ASSET_ITEM_ID == objFixAssetItem.FIX_ASSET_ITEM_ID && x.IS_ACTIVE)
                                                              .Select(x => new
                                                              {
                                                                  x.DISPOSE_ID,
                                                                  x.TRAN_DATE,
                                                                  x.CREATE_BY,
                                                                  x.QTY,
                                                                  x.PRICE,
                                                                  AMOUNT = x.QTY * x.PRICE,
                                                                  x.CURRENCY_ID
                                                              });
            dgv.DataSource = data;

            txtTotal.Text = UIHelper.FormatCurrency(data.Sum(x => (decimal?)x.AMOUNT) ?? 0, objFixAssetItem.CURRENCY_ID); 
        }

        #endregion

        private void lblAddDepose_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DateTime date=DBDataContext.Db.GetSystemDate();
            int AccountId = AccountChartHelper.GetAccounts(AccountConfig.PAYMENT_DEFAULT_ACCOUNTS).FirstOrDefault().ACCOUNT_ID;
            DialogDisposeFixAsset diag = new DialogDisposeFixAsset(GeneralProcess.Insert, objFixAssetItem,
                                                                    new TBL_FIX_ASSET_DISPOSE() {TRAN_DATE=date,PAYMENT_ACCOUNT_ID=AccountId });
            if (diag.ShowDialog() == DialogResult.OK)
            {
                readDipose();
            }
        }

        private void lblUpdateDepose_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (dgv.Rows.Count > 0)
            {
                int disposeId = (int)dgv.SelectedRows[0].Cells[DISPOSE_ID.Name].Value;
                TBL_FIX_ASSET_DISPOSE objFixAssetDispose = DBDataContext.Db.TBL_FIX_ASSET_DISPOSEs.FirstOrDefault(x=>x.DISPOSE_ID==disposeId);
                DialogDisposeFixAsset diag = new DialogDisposeFixAsset(GeneralProcess.Update, objFixAssetItem, objFixAssetDispose);
                if (diag.ShowDialog() == DialogResult.OK)
                {
                    readDipose();
                }
            }
        }

        private void lblDeleteDepose_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (dgv.Rows.Count > 0)
            {
                int disposeId = (int)dgv.SelectedRows[0].Cells[DISPOSE_ID.Name].Value;
                TBL_FIX_ASSET_DISPOSE objFixAssetDispose = DBDataContext.Db.TBL_FIX_ASSET_DISPOSEs.FirstOrDefault(x => x.DISPOSE_ID == disposeId);
                DialogDisposeFixAsset diag = new DialogDisposeFixAsset(GeneralProcess.Delete, objFixAssetItem, objFixAssetDispose);
                if (diag.ShowDialog() == DialogResult.OK)
                {
                    readDipose();
                }
            }
        }

        
         
    }
}
