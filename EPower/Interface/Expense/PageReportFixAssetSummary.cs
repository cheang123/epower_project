﻿using EPower.Base.Helper;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageReportFixAsset : Form
    {
        CrystalReportHelper ch = null;

        #region Constructor
        public PageReportFixAsset()
        {
            InitializeComponent();
            viewer.DefaultView();
        }

        #endregion

        #region Method

        public void lookup()
        {
            new FixAssetCategoryPopulator().PopluateTree(cboCategory.TreeView);
            cboCategory.TreeView.SelectedNode = this.cboCategory.TreeView.GetNodesIncludeAncestors()
                                                   .FirstOrDefault(x => (int)x.Tag == 0);
            if (cboCategory.TreeView.SelectedNode != null)
            {
                this.cboCategory.Text = cboCategory.TreeView.SelectedNode.Text;
            }

            new AccountChartPopulator().PopluateTree(cboPaymentAccount.TreeView, AccountChartHelper.GetAccounts(AccountConfig.PAYMENT_ACCOUNTS, Resources.ALL_PAYMENT_ACCOUNT));
            if (cboPaymentAccount.TreeView.Nodes.Count > 0)
            {
                cboPaymentAccount.TreeView.Nodes[0].Expand();
                cboPaymentAccount.TreeView.SelectedNode = cboPaymentAccount.TreeView.Nodes[0];
                cboPaymentAccount.Text = cboPaymentAccount.SelectedNode.Text;
            }

            UIHelper.SetDataSourceToComboBox(this.cboDisplayCurrency, Lookup.GetCurrencies());
            UIHelper.SetDataSourceToComboBox(this.cboTranCurrency, Lookup.GetCurrencies(), Resources.ALL_CURRENCY);
        }

        private void ChangeKeyboardKhmer(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void ChangeKeyboardEnglish(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }  

        public CrystalReportHelper ViewReport(int year,int categoryId,int tranCurrencyId,int displayCurrencyId,int paymentAccountId,string displayCurrencySign)
        {
            CrystalReportHelper c = new CrystalReportHelper("ReportFixAssetSummary.rpt");
            c.SetParameter("@DATE", year.ToString());
            c.SetParameter("@CATEGORY_ID", categoryId);
            c.SetParameter("@TRAN_CURRENCY_ID", tranCurrencyId);
            c.SetParameter("@DISPLAY_CURRENCY_ID", displayCurrencyId);
            c.SetParameter("@PAYMENT_ACCOUNT_ID", paymentAccountId);
            c.SetParameter("@DISPLAY_CURRENCY_SING", displayCurrencySign);
            return c;
        }

        private void viewReport()
        {
            int year = dtp.Value.Year;
            int categoryId = (int)cboCategory.TreeView.SelectedNode.Tag; 
            int tranCurrencyId = (int)cboTranCurrency.SelectedValue;
            int displayCurrencyId = (int)cboDisplayCurrency.SelectedValue;
            int paymentAccountId = (int)cboPaymentAccount.TreeView.SelectedNode.Tag;
            string displayCurrencySign = cboDisplayCurrency.Text;

            ch = ViewReport(year,categoryId,tranCurrencyId,displayCurrencyId,paymentAccountId,displayCurrencySign);
            this.viewer.ReportSource = ch.Report;
        }

        private void sendMail()
        {
            try
            {
                this.viewReport();
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + this.ch.ReportName + ".xls").FullName;
                this.ch.ExportToExcel(exportPath);

                DialogSendMail.Instance.Add(this.ch.ReportName, exportPath);
                DialogSendMail.Instance.txtTo.Text = "eackh.report@gmail.com";
                DialogSendMail.Instance.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.ShowWarning(EPower.Properties.Resources.YOU_CANNOT_SEND_EMAIL, Resources.WARNING);
                MsgBox.LogError(ex);
            }
        }

        #endregion
        private void btnViewReport_Click(object sender, EventArgs e)
        {
            Runner.Run(this.viewReport);
        } 

        private void btnMail_Click(object sender, EventArgs e)
        {
            this.sendMail();
        } 
         
          
    }
}
