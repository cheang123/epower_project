﻿using EPower.Base.Helper;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageAccountTransaction : Form
    {
        AccountType _tranType;
        public TBL_ACCOUNT_TRAN AccountTran
        {
            get
            {
                TBL_ACCOUNT_TRAN objAccountTran = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    int tranID = (int)dgv.SelectedRows[0].Cells[TRAN_ID.Name].Value;
                    try
                    {
                        objAccountTran = DBDataContext.Db.TBL_ACCOUNT_TRANs.FirstOrDefault(x => x.TRAN_ID == tranID);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objAccountTran;
            }
        }

        #region Constructor
        public PageAccountTransaction()
        {
            InitializeComponent();
            this.dtpStart.Value = this.dtpEnd.Value = DateTime.Now;
        }

        public PageAccountTransaction(AccountType TranType) : this()
        {
            this._tranType = TranType;
            dgv.Columns[ACCOUNT.Name].HeaderText = dgv.Columns[ACCOUNT.Name].HeaderText + DBDataContext.Db.TBL_ACCOUNT_TYPEs.FirstOrDefault(x => x.TYPE_ID == (int)_tranType).TYPE_NAME;

            txt_QuickSearch(null, null);
            UIHelper.DataGridViewProperties(dgv);
            PageAccountTransaction_VisibleChanged(this, EventArgs.Empty);

        }
        #endregion

        #region Operation
        /// <summary>
        /// Load data from database.
        /// </summary>
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            try
            {
                var accounts = new List<int>();
                if (this.cboTransAccount.TreeView.SelectedNode != null)
                {
                    accounts = this.cboTransAccount.TreeView.SelectedNode
                                    .GetNodesIncudeAncestors()
                                    .Select(x => (int)x.Tag)
                                    .ToList();
                }

                dgv.DataSource = from at in DBDataContext.Db.TBL_ACCOUNT_TRANs
                                 join ai in DBDataContext.Db.TBL_ACCOUNT_CHARTs on at.TRAN_ACCOUNT_ID equals ai.ACCOUNT_ID
                                 join ap in DBDataContext.Db.TBL_ACCOUNT_CHARTs on at.PAYMENT_ACCOUNT_ID equals ap.ACCOUNT_ID
                                 join c in DBDataContext.Db.TLKP_CURRENCies on at.CURRENCY_ID equals c.CURRENCY_ID
                                 where at.IS_ACTIVE
                                 && (at.TRAN_DATE.Date >= dtpStart.Value.Date && at.TRAN_DATE.Date <= dtpEnd.Value.Date)
                                 && at.ACCOUNT_TYPE_ID == (int)_tranType
                                 && (at.REF_NO + " " + at.TRAN_BY + ai.ACCOUNT_CODE + " " + ai.ACCOUNT_CODE).ToLower().Contains(txtQuickSearch.Text.ToLower())
                                 && (accounts.Contains(at.TRAN_ACCOUNT_ID))
                                 select new
                                 {
                                     at.TRAN_ID,
                                     at.REF_NO,
                                     TRAN_ACCOUNT = ai.ACCOUNT_CODE + "  " + ai.ACCOUNT_NAME,
                                     at.TRAN_BY,
                                     at.TRAN_DATE,
                                     PAYMENT_ACCOUNT = ap.ACCOUNT_CODE + " " + ap.ACCOUNT_NAME,
                                     at.AMOUNT,
                                     CURRENCY = c.CURRENCY_SING,
                                     at.NOTE
                                 };

            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        /// <summary>
        /// Add new ExpenseCategory.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNew_Click(object sender, EventArgs e)
        {


            var objTran = new TBL_ACCOUNT_TRAN()
            {
                TRAN_DATE = DateTime.Now,
                NOTE = string.Empty,
                CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                CREATE_ON = DBDataContext.Db.GetSystemDate(),
                TRAN_ACCOUNT_ID = (int)(cboTransAccount.SelectedNode.Tag ?? 0),
                CURRENCY_ID = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.IS_DEFAULT_CURRENCY).CURRENCY_ID
            };

            var objDefaultPaymentAcc = AccountChartHelper.GetAccounts(AccountConfig.PAYMENT_DEFAULT_ACCOUNTS).FirstOrDefault();
            if (objDefaultPaymentAcc != null)
            {
                objTran.PAYMENT_ACCOUNT_ID = objDefaultPaymentAcc.ACCOUNT_ID;
            }

            DialogAccountTransaction dig = new DialogAccountTransaction(GeneralProcess.Insert, objTran, this._tranType);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.AccountTran.TRAN_ID);
            }
        }

        /// <summary>
        /// Edit ExpenseCategory.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (this.AccountTran == null)
            {
                return;
            }
            DialogAccountTransaction dig = new DialogAccountTransaction(GeneralProcess.Update, AccountTran, this._tranType);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.AccountTran.TRAN_ID);
            }
        }

        /// <summary>
        /// Remove ExpenseCategory.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (this.AccountTran == null)
            {
                return;
            }

            DialogAccountTransaction dig = new DialogAccountTransaction(GeneralProcess.Delete, AccountTran, this._tranType);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.AccountTran.TRAN_ID - 1);
            }
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        #endregion 

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (this.AccountTran == null)
            {
                return;
            }
            DialogAccountTransactionCancel dig = new DialogAccountTransactionCancel(this.AccountTran, this._tranType);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.AccountTran.TRAN_ID - 1);
            }
        }

        private void PageAccountTransaction_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                var accounts = AccountChartHelper.GetAccounts(_tranType == AccountType.Income ? AccountConfig.INCOME_TRANS_ACCOUNTS : AccountConfig.EXPENSE_TRANS_ACCOUNTS, Resources.ALL_ACCOUNT_ITEM);
                new AccountChartPopulator().PopluateTree(cboTransAccount.TreeView, accounts);
                cboTransAccount.TreeView.SelectedNode = cboTransAccount.TreeView.Nodes[0];
                cboTransAccount.Text = cboTransAccount.TreeView.SelectedNode != null ?
                                        cboTransAccount.TreeView.SelectedNode.Text : "";
            }
        }
        private void cboSaveState_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            if (cbo.SelectedIndex == -1)
            {
                return;
            }
            cbo.Tag = cbo.SelectedValue;
            txt_QuickSearch(sender, e);
        }

        private void cboTransAccount_AfterSelectNode(object sender, TreeViewEventArgs e)
        {
            txt_QuickSearch(null, EventArgs.Empty);
        }

        private void cboTransAccount_AfterSelectNode_1(object sender, TreeViewEventArgs e)
        {
            txt_QuickSearch(null, EventArgs.Empty);
        }

        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }

    }

    public class PageAccountTransactionIncome : PageAccountTransaction
    {
        public PageAccountTransactionIncome()
            : base(AccountType.Income)
        {
        }
    }

    public class PageAccountTransactionExpense : PageAccountTransaction
    {
        public PageAccountTransactionExpense()
            : base(AccountType.Expense)
        {
        }
    }

}
