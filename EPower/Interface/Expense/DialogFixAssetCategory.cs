﻿using EPower.Base.Helper;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogFixAssetCategory : ExDialog
    {
        GeneralProcess _flag;


        TBL_FIX_ASSET_CATEGORY _objNew = new TBL_FIX_ASSET_CATEGORY();
        TBL_FIX_ASSET_CATEGORY _objOld = new TBL_FIX_ASSET_CATEGORY();
        public TBL_FIX_ASSET_CATEGORY FixAssetCategory
        {
            get { return _objNew; }
        }
        

        #region Constructor
        public DialogFixAssetCategory(GeneralProcess flag, TBL_FIX_ASSET_CATEGORY objFixAssetCategory)
        {
            InitializeComponent();
            new FixAssetCategoryPopulator().PopluateTree(cboParent.TreeView);

            UIHelper.SetDataSourceToComboBox(cboAssetAccount, AccountChartHelper.GetAccountsTable(AccountConfig.FIX_ASSET_ASSET_ACCOUNTS ,""));
            UIHelper.SetDataSourceToComboBox(cboAccumulatedDepreciationAccount, AccountChartHelper.GetAccountsTable(AccountConfig.FIX_ASSET_ACCUMULATED_DEPRECIATION_ACCOUNTS, ""));
            UIHelper.SetDataSourceToComboBox(cboDepreciationExpenseAccount, AccountChartHelper.GetAccountsTable(AccountConfig.FIX_ASSET_DEPRECIATION_EXPENSE_ACCOUNTS,""));


            _flag = flag;
            objFixAssetCategory._CopyTo(_objNew);
            objFixAssetCategory._CopyTo(_objOld);
            this.Text = flag.GetText(this.Text); 
            read();
            UIHelper.SetEnabled(this, !(flag == GeneralProcess.Delete));
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
        }

       

        #endregion

        #region Operation
        /// <summary>
        /// Change Keyboard to Khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKeyboardKhmer(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        private void ChangeKeyboardEnglish(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }  

        /// <summary>
        /// Data transaction.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
             //If record is duplicate.
            this.ClearAllValidation(); 
            try
            {
                using (TransactionScope tran=new TransactionScope( TransactionScopeOption.Required,TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objNew);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objNew);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }                                               
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }            
        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
        #endregion

        #region Method
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();
            if (this.cboParent.TreeView.SelectedNode == null)
            {
                cboParent.SetValidation(string.Format(Resources.REQUIRED, lblPARENT_FIX_ASSET.Text));
                val = true;
            }
            if (txtCode.Text.Trim() == string.Empty)
            {
                txtCode.SetValidation(string.Format(Resources.REQUIRED, lblFIX_ASSET_TYPE_CODE.Text));
                val = true;
            }
            if (txtName.Text.Trim()==string.Empty)
            {
                txtName.SetValidation(string.Format(Resources.REQUIRED, lblFIX_ASSET_TYPE.Text));
                val = true;
            }
            if ( cboAssetAccount.SelectedIndex == -1)
            {
                cboAssetAccount.SetValidation(string.Format(Resources.REQUIRED, lblFIX_ASSET_TYPE.Text));
                val = true;
            }

            if (cboAccumulatedDepreciationAccount.SelectedIndex == -1)
            {
                cboAssetAccount.SetValidation(string.Format(Resources.REQUIRED, lblFIX_ASSET_TYPE.Text));
                val = true;
            }

            if (cboDepreciationExpenseAccount.SelectedIndex == -1)
            {
                cboAssetAccount.SetValidation(string.Format(Resources.REQUIRED, lblFIX_ASSET_TYPE.Text));
                val = true;
            }
            return val;
        }

        /// <summary>
        /// Read data from object.
        /// </summary>
        private void read()
        {  
            cboParent.TreeView.SelectedNode = this.cboParent.TreeView.GetNodesIncludeAncestors()
                                                   .FirstOrDefault(x => (int)x.Tag == this._objNew.PARENT_ID);
            if (cboParent.TreeView.SelectedNode  != null)
            {
                this.cboParent.Text = cboParent.TreeView.SelectedNode.Text;
            }
            txtUsefulLife.Text = _objNew.DEFAULT_USEFUL_LIFE.ToString("#");
            txtName.Text = _objNew.CATEGORY_NAME;
            txtNote.Text = _objNew.NOTE;
            txtCode.Text = _objNew.CATEGORY_CODE;
            cboAssetAccount.SelectedValue = _objNew.ASSET_ACCOUNT_ID;
            cboAccumulatedDepreciationAccount.SelectedValue = _objNew.ACCUMULATED_DEPRECIATION_ACCOUNT_ID;
            cboDepreciationExpenseAccount.SelectedValue = _objNew.DEPRECIATION_EXPENSE_ID;
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            _objNew.CATEGORY_NAME = txtName.Text; 
            _objNew.NOTE = txtNote.Text;
            _objNew.CATEGORY_CODE = txtCode.Text;
            _objNew.DEFAULT_USEFUL_LIFE = DataHelper.ParseToInt (txtUsefulLife.Text);
            _objNew.PARENT_ID = (int)cboParent.TreeView.SelectedNode.Tag;
            _objNew.ASSET_ACCOUNT_ID = (int)this.cboAssetAccount.SelectedValue;
            _objNew.ACCUMULATED_DEPRECIATION_ACCOUNT_ID = (int)this.cboAccumulatedDepreciationAccount.SelectedValue;
            _objNew.DEPRECIATION_EXPENSE_ID = (int)this.cboDepreciationExpenseAccount.SelectedValue;
        }
        #endregion

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }

        private void DialogAccountCategory_Load(object sender, EventArgs e)
        {
            this.txtName.Focus();
        }

    }
}