﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogDepreciationByItem : ExDialog
    {
        string currencySing = "";
        TBL_FIX_ASSET_ITEM _objNew = new TBL_FIX_ASSET_ITEM();
        public TBL_FIX_ASSET_ITEM FixAssetItem
        {
            get { return _objNew; }
        }


        #region Constructor
        public DialogDepreciationByItem(TBL_FIX_ASSET_ITEM objFixAssetItem)
        {
            InitializeComponent();
            objFixAssetItem._CopyTo(_objNew);
            currencySing = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.CURRENCY_ID == objFixAssetItem.CURRENCY_ID).CURRENCY_SING;
            readFixAssetItem();
            readDepreciation();
        }

        #endregion

        #region Method
        private void readDepreciation()
        {
            Runner.Run(delegate
            {
                DataTable depreciation = (from d in DBDataContext.Db.TBL_DEPRECIATIONs
                                          join c in DBDataContext.Db.TLKP_CURRENCies on d.CURRENCY_ID equals c.CURRENCY_ID
                                          where d.IS_ACTIVE
                                               && d.FIX_ASSET_ITEM_ID == _objNew.FIX_ASSET_ITEM_ID
                                               && d.STATUS_ID == (int)DepreciationStatus.NotYet
                                               && d.DEPRECIATION_MONTH <= dtpSuggestionDate.Value
                                               && d.QTY > 0
                                          select new
                                          {
                                              d.DEPRECIATION_ID,
                                              IS_DEPRECIATION = d.STATUS_ID == (int)DepreciationStatus.NotYet ? false : true,
                                              d.DEPRECIATION_MONTH,
                                              d.DEPRECIATION_BY,
                                              AMOUNT = d.AMOUNT,
                                              c.CURRENCY_SING,
                                              d.STATUS_ID,
                                          })._ToDataTable();
                dgv.DataSource = depreciation;
            }, Resources.PROCESSING);
        }

        private void setDepreciationRowColor()
        {
            dgv.Refresh();
            foreach (DataGridViewRow row in this.dgv.Rows)
            {
                int statusId = (int)row.Cells[STATUS_ID.Name].Value;
                DateTime depreciationMonth = (DateTime)row.Cells[DEPRECIATION_MONTH.Name].Value;

                // SET TRUE TO CHECK 
                if (statusId == (int)DepreciationStatus.ReadyDepreciation)
                {
                    row.Cells[IS_DEPRECIATION_.Name].ReadOnly = false;
                    row.Cells[IS_DEPRECIATION_.Name].Value = true;
                    row.DefaultCellStyle.ForeColor = Color.Green;
                    row.DefaultCellStyle.SelectionForeColor = Color.Green;
                }
                else if (statusId == (int)DepreciationStatus.NotYet && depreciationMonth <= dtpSuggestionDate.Value)
                {
                    // SET COLOR 
                    row.DefaultCellStyle.ForeColor = Color.Blue;
                    row.DefaultCellStyle.SelectionForeColor = Color.Blue;
                }
                else
                {
                    // SET COLOR 
                    row.DefaultCellStyle.ForeColor = Color.Black;
                    row.DefaultCellStyle.SelectionForeColor = Color.Black;
                }
            }
            dgv.Refresh();
        }

        private void readFixAssetItem()
        {
            txtFixAssetName.Text = _objNew.FIX_ASSET_NAME;
            txtAmount.Text = UIHelper.FormatCurrency(_objNew.TOTAL_COST, _objNew.CURRENCY_ID);
            txtAccDepre.Text = UIHelper.FormatCurrency(_objNew.ACCUMULATED_DEPRECIATION, _objNew.CURRENCY_ID);
        }

        private decimal getTotalDepreciation()
        {
            decimal tmp = 0.0m;
            foreach (DataGridViewRow row in this.dgv.Rows)
            {
                //Only sum all invoice due amount
                if (DataHelper.ParseToBoolean(row.Cells[this.IS_DEPRECIATION_.Name].Value.ToString())
                    && (int)row.Cells[STATUS_ID.Name].Value == (int)DepreciationStatus.NotYet)
                {
                    tmp += DataHelper.ParseToDecimal(row.Cells[this.ACCUMULATE_DEPRECIATION.Name].Value.ToString());
                }
            }
            return tmp;
        }

        #endregion

        private void chkCheckAll_CheckedChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in this.dgv.Rows)
            {
                row.Cells[IS_DEPRECIATION_.Name].Value = this.chkCheckAll_.Checked;
                if ((int)row.Cells[STATUS_ID.Name].Value == (int)DepreciationStatus.ReadyDepreciation)
                {
                    row.Cells[IS_DEPRECIATION_.Name].ReadOnly = true;
                    row.Cells[IS_DEPRECIATION_.Name].Value = true;
                }
                this.txtTotalAmount.Text = UIHelper.FormatCurrency(this.getTotalDepreciation(), _objNew.CURRENCY_ID);
            }
        }

        private void cboStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            readDepreciation();
            setDepreciationRowColor();
            this.txtTotalAmount.Text = UIHelper.FormatCurrency(this.getTotalDepreciation(), _objNew.CURRENCY_ID);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (DataHelper.ParseToDecimal(txtTotalAmount.Text) > 0)
            {
                if (_objNew.STATUS_ID == (int)FixAssetItemStatus.STOP_USE)
                {
                    MsgBox.ShowInformation(string.Format(Resources.MS_FIX_ASSET_IS_ALREADY_STOP_USE, _objNew.FIX_ASSET_NAME));
                    return;
                }
                if (MsgBox.ShowQuestion(string.Format(Resources.MSQ_CONFIRM_DEPRICIATION, _objNew.FIX_ASSET_NAME), this.Text) == DialogResult.Yes)
                {
                    Runner.Run(delegate
                    {
                        using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                        {
                            DateTime date = DBDataContext.Db.GetSystemDate();
                            string depreciatinoBy = Login.CurrentLogin.LOGIN_NAME;
                            foreach (DataGridViewRow row in this.dgv.Rows)
                            {
                                if ((bool)row.Cells[IS_DEPRECIATION_.Name].Value && (int)row.Cells[STATUS_ID.Name].Value == (int)DepreciationStatus.NotYet)
                                {
                                    // UPDATE TBL_DEPRECIATION
                                    TBL_DEPRECIATION objDepreciation = DBDataContext.Db.TBL_DEPRECIATIONs.FirstOrDefault(x => x.DEPRECIATION_ID == (int)row.Cells[DEPRECIATION_ID.Name].Value);
                                    objDepreciation.STATUS_ID = (int)DepreciationStatus.ReadyDepreciation;
                                    objDepreciation.DEPRECIATION_DATE = date;
                                    objDepreciation.DEPRECIATION_BY = depreciatinoBy;
                                    objDepreciation.ROW_DATE = date;

                                    // UPDATE TBL_FIX_ASSET_ITEM
                                    TBL_FIX_ASSET_ITEM objFixAssetItem = DBDataContext.Db.TBL_FIX_ASSET_ITEMs.FirstOrDefault(x => x.FIX_ASSET_ITEM_ID == _objNew.FIX_ASSET_ITEM_ID);
                                    objFixAssetItem.ACCUMULATED_DEPRECIATION += objDepreciation.AMOUNT;
                                    objFixAssetItem.ROW_DATE = date;

                                    DBDataContext.Db.SubmitChanges();
                                }
                            }
                            tran.Complete();
                        }
                    }, Resources.PROCESSING);
                    this.DialogResult = DialogResult.OK;
                }
            }
        }

        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1 && e.ColumnIndex == dgv.Columns[this.IS_DEPRECIATION_.Name].Index && (int)dgv.Rows[e.RowIndex].Cells[this.STATUS_ID.Name].Value == (int)DepreciationStatus.NotYet)
            {
                dgv.Rows[e.RowIndex].Cells[this.IS_DEPRECIATION_.Name].Value = !(bool)dgv.Rows[e.RowIndex].Cells[IS_DEPRECIATION_.Name].Value;
                this.txtTotalAmount.Text = UIHelper.FormatCurrency(this.getTotalDepreciation(), _objNew.CURRENCY_ID);
            }
        }

        private void dgv_Sorted(object sender, EventArgs e)
        {
            setDepreciationRowColor();
        }

        private void DialogDepreciationByItem_Load(object sender, EventArgs e)
        {
            setDepreciationRowColor();
        }
    }
}
