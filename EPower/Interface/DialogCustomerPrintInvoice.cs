﻿using EPower.Base.Logic;
using EPower.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace EPower.Interface
{
    public partial class DialogCustomerPrintInvoice : SoftTech.Component.ExDialog
    {
         
        #region Constructor
        
        bool isConcorrentPrinting = false;
        int printInvoiceId = 0;
        bool load = false;
        private bool _isBinded = false;
        private string ConnectionString = Method.GetConnectionString(Settings.Default.CONNECTION);
        public DialogCustomerPrintInvoice(bool _isConcorrentPrinting,int _printInvoiceId)
        {
            InitializeComponent();
            viewer.DefaultView();
            load = false;
            isConcorrentPrinting = _isConcorrentPrinting;
            printInvoiceId = _printInvoiceId;
            BindBillPrinting();
            _isBinded = true;
            ApplyDefault(); 
            load = true;
        }

        public void ApplyDefault()
        {
            // for the first time if user is not set default report for print invoice 
            if (Settings.Default.REPORT_INVOICE == null || Settings.Default.REPORT_INVOICE == "")
            {
                Settings.Default.REPORT_INVOICE = "ReportInvoice.rpt";
                Settings.Default.Save();
            }  
            var selectedReport = Settings.Default.REPORT_INVOICE;
            cboReport.SelectedValue = selectedReport;
            if (string.IsNullOrEmpty(selectedReport))
            {
                cboReport.SelectedIndex = 0;
            }
            view(selectedReport);
        }

        private void BindBillPrinting()
        {
            var uTILITY = DBDataContext.Db.TBL_UTILITies.FirstOrDefault(x => x.UTILITY_ID == 40);
            TBL_UTILITY UTILITY = uTILITY;
            var bill_printingList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BillPrintingListModel>>(UTILITY.UTILITY_VALUE);
            cboReport.DataSource = bill_printingList;
            cboReport.DisplayMember = nameof(BillPrintingListModel.TemplateName);
            cboReport.ValueMember = nameof(BillPrintingListModel.TemplateName);
        }
        #endregion 

        private void view(string reportName)
        {
            try
            {
                var TemplateString = DBDataContext.Db.TBL_UTILITies.FirstOrDefault(x => x.UTILITY_ID == 40).UTILITY_VALUE.ToString();
                var Templates = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BillPrintingListModel>>(TemplateString);
                var template = Templates.FirstOrDefault(x => x.TemplateName == reportName);

                if (isConcorrentPrinting)
                {
                    if (!template.IsOld)
                    {
                        ///New Invoice
                        var reportNew = ReportLogic.Instance.GetViewNewInvoice(reportName, printInvoiceId);
                        viewer.ReportSource = reportNew;
                    }
                    else
                    {
                        CrystalReportHelper ch = new CrystalReportHelper(reportName, "INVOICE_USAGE", "REPORT_INVOICE_HISTORY");
                        ch.SetParameter("@PRINT_INVOICE_ID", printInvoiceId);
                        viewer.ReportSource = ch.Report;
                    }
                }
                else
                {
                    CrystalReportHelper ch = Method.GetInvoiceReport();
                    viewer.ReportSource = ch.Report;
                }
            }catch(Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_VIEW_REPORT, Base.Properties.Resources.WARNING);
            }
            
        }
       
        private void cboReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_isBinded)
            {
                return;
            }

            var selectedValue = this.cboReport.SelectedValue;
            if (selectedValue != null)
            {
                Settings.Default.REPORT_INVOICE = selectedValue?.ToString() ?? "";
                Settings.Default.Save();
            }

            string path = string.Format(Settings.Default.PATH_REPORT, Settings.Default.LANGUAGE == "en-US" ? "" : "" + Settings.Default.LANGUAGE);
            path = Path.Combine(Directory.GetCurrentDirectory(), path);
            string PathCustomized = path + "Customized\\" + Settings.Default.REPORT_INVOICE;
            if (load)
            {
                if (!File.Exists(PathCustomized))
                {
                    if (!File.Exists(path + Settings.Default.REPORT_INVOICE))
                    {
                        MsgBox.ShowInformation(string.Format(Resources.NOT_FOUND_INVOICE, "", Settings.Default.REPORT_INVOICE), Resources.INFORMATION);
                        return;
                    }
                    else
                    {
                        view(selectedValue.ToString());
                    }
                }
                else
                {
                    view(selectedValue.ToString());
                }
            }
        }
    }
}