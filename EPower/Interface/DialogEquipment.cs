﻿using System;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;

namespace EPower.Interface
{
    public partial class DialogEquipment : ExDialog
    {
        GeneralProcess _flag;        
        TBL_EQUIPMENT _objEquipment = new TBL_EQUIPMENT();
        public TBL_EQUIPMENT Equipment
        {
            get { return _objEquipment; }
        }
        TBL_EQUIPMENT _oldobjPhase = new TBL_EQUIPMENT();

        #region Constructor
        public DialogEquipment(GeneralProcess flag, TBL_EQUIPMENT objEquipment)
        {
            InitializeComponent();
            _flag = flag;

            objEquipment._CopyTo(_objEquipment);
            objEquipment._CopyTo(_oldobjPhase);

            if (flag == GeneralProcess.Insert)
            {
                _objEquipment.IS_ACTIVE = true;
                this.Text = string.Concat(Resources.INSERT, this.Text);
            }
            else if (flag == GeneralProcess.Update)
            {
                this.Text = string.Concat(Resources.UPDATE, this.Text);
            }
            else if (flag == GeneralProcess.Delete)
            {
                this.Text = string.Concat(Resources.DELETE, this.Text);
                UIHelper.SetEnabled(this, false);
            }
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;            
            read();
        }        
        #endregion

        #region Opeartion
        /// <summary>
        /// Data transaction.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
            txtEquipment.ClearValidation();
            if (DBDataContext.Db.IsExits(_objEquipment, "EQUIPMENT_NAME"))
            {
                txtEquipment.SetValidation(string.Format( Resources.MS_IS_EXISTS,lblEQUIPMENT.Text));                
                return;
            }
            try
            {
                using (TransactionScope tran=new TransactionScope( TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objEquipment);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_oldobjPhase, _objEquipment);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objEquipment);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }                                                
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }            
        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
           this.DialogResult = DialogResult.Cancel;
        }

       /// <summary>
        /// Change current keyboard layout to khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        #endregion

        #region Method
        /// <summary>
        /// Load data from object.
        /// </summary>
        private void read()
        {           
            txtEquipment.Text = _objEquipment.EQUIPMENT_NAME;
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            _objEquipment.EQUIPMENT_NAME = txtEquipment.Text.Trim();
        }

        private bool inValid()
        {
            bool val = false;
            txtEquipment.ClearValidation();                       

            if (txtEquipment.Text.Trim()==string.Empty)
            {
                txtEquipment.SetValidation(string.Format(Resources.REQUIRED, lblEQUIPMENT.Text));
                val = true;
            }
            return val;
        }
        #endregion

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objEquipment);
        }
    }
}
