﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogDiscount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogDiscount));
            this.lblDISCOUNT_NAME = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblDISCOUNT = new System.Windows.Forms.Label();
            this.txtDiscount = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.lblNOTE = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.cboDiscountUnit = new System.Windows.Forms.ComboBox();
            this.cboDiscountType = new System.Windows.Forms.ComboBox();
            this.lblDISCOUNT_TYPE = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.label6);
            this.content.Controls.Add(this.lblDISCOUNT_TYPE);
            this.content.Controls.Add(this.cboDiscountType);
            this.content.Controls.Add(this.cboDiscountUnit);
            this.content.Controls.Add(this.txtDescription);
            this.content.Controls.Add(this.lblNOTE);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.txtDiscount);
            this.content.Controls.Add(this.lblDISCOUNT);
            this.content.Controls.Add(this.txtName);
            this.content.Controls.Add(this.lblDISCOUNT_NAME);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblDISCOUNT_NAME, 0);
            this.content.Controls.SetChildIndex(this.txtName, 0);
            this.content.Controls.SetChildIndex(this.lblDISCOUNT, 0);
            this.content.Controls.SetChildIndex(this.txtDiscount, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.lblNOTE, 0);
            this.content.Controls.SetChildIndex(this.txtDescription, 0);
            this.content.Controls.SetChildIndex(this.cboDiscountUnit, 0);
            this.content.Controls.SetChildIndex(this.cboDiscountType, 0);
            this.content.Controls.SetChildIndex(this.lblDISCOUNT_TYPE, 0);
            this.content.Controls.SetChildIndex(this.label6, 0);
            // 
            // lblDISCOUNT_NAME
            // 
            resources.ApplyResources(this.lblDISCOUNT_NAME, "lblDISCOUNT_NAME");
            this.lblDISCOUNT_NAME.Name = "lblDISCOUNT_NAME";
            // 
            // txtName
            // 
            resources.ApplyResources(this.txtName, "txtName");
            this.txtName.Name = "txtName";
            this.txtName.Enter += new System.EventHandler(this.ChangeKeyboard);
            // 
            // lblDISCOUNT
            // 
            resources.ApplyResources(this.lblDISCOUNT, "lblDISCOUNT");
            this.lblDISCOUNT.Name = "lblDISCOUNT";
            // 
            // txtDiscount
            // 
            resources.ApplyResources(this.txtDiscount, "txtDiscount");
            this.txtDiscount.Name = "txtDiscount";
            this.txtDiscount.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            this.txtDiscount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDiscount_KeyPress);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // lblNOTE
            // 
            resources.ApplyResources(this.lblNOTE, "lblNOTE");
            this.lblNOTE.Name = "lblNOTE";
            // 
            // txtDescription
            // 
            resources.ApplyResources(this.txtDescription, "txtDescription");
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Enter += new System.EventHandler(this.ChangeKeyboard);
            // 
            // cboDiscountUnit
            // 
            this.cboDiscountUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDiscountUnit.FormattingEnabled = true;
            this.cboDiscountUnit.Items.AddRange(new object[] {
            resources.GetString("cboDiscountUnit.Items"),
            resources.GetString("cboDiscountUnit.Items1")});
            resources.ApplyResources(this.cboDiscountUnit, "cboDiscountUnit");
            this.cboDiscountUnit.Name = "cboDiscountUnit";
            this.cboDiscountUnit.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // cboDiscountType
            // 
            this.cboDiscountType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDiscountType.FormattingEnabled = true;
            this.cboDiscountType.Items.AddRange(new object[] {
            resources.GetString("cboDiscountType.Items"),
            resources.GetString("cboDiscountType.Items1")});
            resources.ApplyResources(this.cboDiscountType, "cboDiscountType");
            this.cboDiscountType.Name = "cboDiscountType";
            this.cboDiscountType.SelectedIndexChanged += new System.EventHandler(this.cboDiscountType_SelectedIndexChanged);
            // 
            // lblDISCOUNT_TYPE
            // 
            resources.ApplyResources(this.lblDISCOUNT_TYPE, "lblDISCOUNT_TYPE");
            this.lblDISCOUNT_TYPE.Name = "lblDISCOUNT_TYPE";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Name = "label6";
            // 
            // DialogDiscount
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogDiscount";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBox txtDiscount;
        private Label lblDISCOUNT;
        private TextBox txtName;
        private Label lblDISCOUNT_NAME;
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label label1;
        private Label label9;
        private ExButton btnCHANGE_LOG;
        private TextBox txtDescription;
        private Label lblNOTE;
        private ComboBox cboDiscountUnit;
        private Label label6;
        private Label lblDISCOUNT_TYPE;
        private ComboBox cboDiscountType;
    }
}