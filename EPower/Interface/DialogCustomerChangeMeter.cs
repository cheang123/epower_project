﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using Login = SoftTech.Security.Logic.Login;

namespace EPower.Interface
{
    public partial class DialogCustomerChangeMeter : ExDialog
    {
        #region Private Data
        TBL_CUSTOMER _objCustomer = new TBL_CUSTOMER();

        TBL_CUSTOMER_METER _objCustomerMeterOld = new TBL_CUSTOMER_METER();
        TBL_CUSTOMER_METER _objCustomerMeterNew = new TBL_CUSTOMER_METER();

        TBL_METER _objMeterOld = null;
        TBL_METER _objMeterNew = null;

        DateTime date = new DateTime();
        #endregion Private Data

        #region Contructor
        public DialogCustomerChangeMeter(TBL_CUSTOMER objCustomer)
        {
            InitializeComponent();
            //permission
            this.dtpCHANGE_DATE.Enabled = SoftTech.Security.Logic.Login.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_REGISTER_UPDATE_CHANGEMETER_DATE);
            date = DBDataContext.Db.GetSystemDate();
            this._objCustomer = objCustomer;
            this.bind();
            this.read();
        }
        #endregion Contructor

        #region Method
        private void InputEnglish(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void InputKhmer(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bind()
        {
            UIHelper.SetDataSourceToComboBox(this.cboOLD_METER_CABLE_SEAL, DBDataContext.Db.TBL_SEALs.Where(row => row.IS_ACTIVE));
            UIHelper.SetDataSourceToComboBox(this.cboOLD_METER_METER_SEAL, DBDataContext.Db.TBL_SEALs.Where(row => row.IS_ACTIVE));
            UIHelper.SetDataSourceToComboBox(this.cboNEW_METER_CABLE_SEAL, DBDataContext.Db.TBL_SEALs.Where(row => row.IS_ACTIVE));
            UIHelper.SetDataSourceToComboBox(this.cboNEW_METER_METER_SEAL, DBDataContext.Db.TBL_SEALs.Where(row => row.IS_ACTIVE));
            UIHelper.SetDataSourceToComboBox(this.cboOLD_METER_STATUS, DBDataContext.Db.TLKP_METER_STATUS.Where(s => s.STATUS_ID != (int)MeterStatus.Used));
            UIHelper.SetDataSourceToComboBox(this.cboReason, DBDataContext.Db.TLKP_REASONs.Where(x => x.IS_ACTIVE && x.TYPE_ID == (int)ReasonType.ChangeMeter || x.TYPE_ID == (int)ReasonType.ChangeAmpere).Select(x => new { x.REASON_ID, x.REASON }), "");
            this.cboOLD_METER_STATUS.SelectedValue = (int)MeterStatus.Unavailable;
        }

        private void read()
        {

            this.txtCUSTOMER_CODE.Text = this._objCustomer.CUSTOMER_CODE;
            this.txtCUSTOMER_NAME.Text = this._objCustomer.LAST_NAME_KH + " " + this._objCustomer.FIRST_NAME_KH;
            this.dtpCHANGE_DATE.Value = date;
            // read customer meter.
            this._objCustomerMeterOld = DBDataContext.Db.TBL_CUSTOMER_METERs.FirstOrDefault(r => r.CUSTOMER_ID == this._objCustomer.CUSTOMER_ID && r.IS_ACTIVE);
            if (this._objCustomerMeterOld != null)
            {
                this.cboOLD_METER_CABLE_SEAL.SelectedValue = this._objCustomerMeterOld.CABLE_SHIELD_ID;
                this.cboOLD_METER_METER_SEAL.SelectedValue = this._objCustomerMeterOld.METER_SHIELD_ID;
                this._objMeterOld = DBDataContext.Db.TBL_METERs.FirstOrDefault(m => m.METER_ID == this._objCustomerMeterOld.METER_ID);
                this.txtOLD_METER.Text = this._objMeterOld.METER_CODE;
                this.txtOLD_METER.AcceptSearch();
            }

            // last month usage.
            var datMonth = Method.GetNextBillingMonth(this._objCustomer.BILLING_CYCLE_ID);
            TBL_USAGE objUsage = (from u in DBDataContext.Db.TBL_USAGEs
                                  where u.CUSTOMER_ID == this._objCustomer.CUSTOMER_ID
                                        && u.METER_ID == this._objMeterOld.METER_ID
                                        && u.USAGE_MONTH <= datMonth
                                  orderby u.USAGE_ID descending
                                  select u).FirstOrDefault();
            if (objUsage != null)
            {
                this.txtOLD_METER_LAST_USAGE.Text = objUsage.END_USAGE.ToString(UIHelper._DefaultUsageFormat);
                this.dtpOLD_METER_LAST_USAGE_DATE.Value = objUsage.END_USE_DATE;
            }
        }

        private bool invalid()
        {
            bool result = false;
            this.ClearAllValidation();

            if (!DataHelper.IsNumber(this.txtOLD_METER_USAGE.Text))
            {
                txtOLD_METER_USAGE.SetValidation(string.Format(Resources.REQUIRED, this.lblEND_USAGE_I.Text));
                result = true;
            }

            if (this.cboOLD_METER_STATUS.SelectedIndex == -1)
            {
                cboOLD_METER_STATUS.SetValidation(string.Format(Resources.REQUIRED, this.lblMETER_STATUS.Text));
                result = true;
            }

            System.Text.RegularExpressions.Regex regex = null;
            regex = new System.Text.RegularExpressions.Regex("^([a-zA-Z0-9])*$");

            if (regex.IsMatch(txtNEW_METER.Text))
            {
                result = false;
            }
            else
            {
                txtNEW_METER.SetValidation(string.Format(Resources.NO_SPECIAL_CHARACTER_ALLOWED, lblMETER_CODE.Text));
                result = true;
            }
            if (this._objMeterNew == null)
            {
                txtNEW_METER.SetValidation(string.Format(Resources.REQUIRED, this.lblMETER_CODE_1.Text));
                result = true;
            }
            if (this.cboNEW_METER_CABLE_SEAL.SelectedIndex == -1)
            {
                cboNEW_METER_CABLE_SEAL.SetValidation(string.Format(Resources.REQUIRED, this.lblCABLE_SHIELD_1.Text));
                result = true;
            }

            if (this.cboNEW_METER_METER_SEAL.SelectedIndex == -1)
            {
                cboNEW_METER_METER_SEAL.SetValidation(string.Format(Resources.REQUIRED, this.lblSHIELD_1.Text));
                result = true;
            }

            if (!DataHelper.IsNumber(this.txtNEW_METER_USAGE.Text))
            {
                txtNEW_METER_USAGE.SetValidation(string.Format(Resources.REQUIRED, this.lblSTART_USAGE_I.Text));
                result = true;
            }
            if (cboReason.SelectedIndex <= 0)
            {
                cboReason.SetValidation(string.Format(Resources.REQUIRED, this.lblREASON.Text));
                result = true;
            }

            return result;
        }
        private void saveData()
        {
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    DateTime now = DBDataContext.Db.GetSystemDate();
                    // TO CHANGE METER OF A CUSTOMER
                    // *********************************************
                    // 1. INSERT NEW CUSTOMER_METER
                    // 2. DELETE OLD CUSTOMER_METER
                    // 3. INSERT OLD METER USAGE
                    // 4. INSERT NEW METER USAGE
                    // 5. UPDATE OLD METER STATUS
                    // 6. UPDATE NEW METER STATUS
                    // 7. DELETE IN TBL_UNKNOWN_METER
                    // ********************************************* 
                    // create a blank change log.
                    TBL_CHANGE_LOG log = null;

                    // 1. DELETE OLD CUSTOMER METER
                    DBDataContext.Db.DeleteChild(this._objCustomerMeterOld, this._objCustomer, ref log);

                    // 2. INSERT NEW CUSTOMER METER
                    this._objCustomerMeterNew = new TBL_CUSTOMER_METER()
                    {
                        BOX_ID = this._objCustomerMeterOld.BOX_ID,
                        BREAKER_ID = this._objCustomerMeterOld.BREAKER_ID,
                        CABLE_SHIELD_ID = (int)this.cboNEW_METER_CABLE_SEAL.SelectedValue,
                        CUS_METER_ID = 0,
                        CUSTOMER_ID = this._objCustomer.CUSTOMER_ID,
                        IS_ACTIVE = true,
                        METER_ID = this._objMeterNew.METER_ID,
                        METER_SHIELD_ID = (int)this.cboNEW_METER_METER_SEAL.SelectedValue,
                        POLE_ID = this._objCustomerMeterOld.POLE_ID,
                        REMAIN_USAGE = DataHelper.ParseToDecimal(this.txtOLD_METER_USAGE.Text),
                        USED_DATE = this.dtpCHANGE_DATE.Value,

                    };
                    DBDataContext.Db.InsertChild(this._objCustomerMeterNew, this._objCustomer, ref log);

                    // 3. INSERT OLD METER USAGE. 
                    DateTime datStart = UIHelper._DefaultDate;
                    DateTime datEnd = UIHelper._DefaultDate;
                    DateTime datMonth = Method.GetNextBillingMonth(this._objCustomer.BILLING_CYCLE_ID, ref datStart, ref datEnd);


                    var objOldMeterUsage = DBDataContext.Db.TBL_USAGEs.FirstOrDefault(row => row.CUSTOMER_ID == _objCustomer.CUSTOMER_ID &&
                                                                                           row.METER_ID == _objMeterOld.METER_ID &&
                                                                                           row.USAGE_MONTH == datMonth);
                    if (objOldMeterUsage == null)
                    {
                        DBDataContext.Db.TBL_USAGEs.InsertOnSubmit(new TBL_USAGE()
                        {
                            COLLECTOR_ID = 1,
                            CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                            CUSTOMER_ID = this._objCustomer.CUSTOMER_ID,
                            DEVICE_ID = 0,
                            END_USAGE = DataHelper.ParseToDecimal(this.txtOLD_METER_USAGE.Text),
                            END_USE_DATE = new DateTime(this.dtpCHANGE_DATE.Value.Year, this.dtpCHANGE_DATE.Value.Month, this.dtpCHANGE_DATE.Value.Day),
                            IS_METER_RENEW_CYCLE = this.chkIS_NEW_CYCLE.Checked,
                            METER_ID = this._objCustomerMeterOld.METER_ID,
                            POSTING_DATE = now,
                            START_USAGE = DataHelper.ParseToDecimal(this.txtOLD_METER_LAST_USAGE.Text),
                            START_USE_DATE = datStart,
                            USAGE_MONTH = datMonth,
                            MULTIPLIER = _objMeterOld.MULTIPLIER
                        });
                    }
                    else
                    {
                        objOldMeterUsage.END_USAGE = DataHelper.ParseToDecimal(this.txtOLD_METER_USAGE.Text);
                        objOldMeterUsage.END_USE_DATE = new DateTime(this.dtpCHANGE_DATE.Value.Year, this.dtpCHANGE_DATE.Value.Month, this.dtpCHANGE_DATE.Value.Day);
                        objOldMeterUsage.COLLECTOR_ID = -1;
                        objOldMeterUsage.IS_METER_RENEW_CYCLE = this.chkIS_NEW_CYCLE.Checked;
                    }
                    DBDataContext.Db.SubmitChanges();


                    // 4. INSERT NEW METER USAGE.
                    TBL_USAGE objNewMeterUsage = new TBL_USAGE()
                    {
                        COLLECTOR_ID = 0,
                        CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                        CUSTOMER_ID = this._objCustomer.CUSTOMER_ID,
                        DEVICE_ID = 0,
                        END_USAGE = DataHelper.ParseToDecimal(this.txtNEW_METER_USAGE.Text),
                        END_USE_DATE = datEnd,
                        IS_METER_RENEW_CYCLE = false,
                        METER_ID = this._objCustomerMeterNew.METER_ID,
                        POSTING_DATE = now,
                        START_USAGE = DataHelper.ParseToDecimal(this.txtNEW_METER_USAGE.Text),
                        START_USE_DATE = datStart,
                        USAGE_MONTH = datMonth,
                        MULTIPLIER = _objMeterNew.MULTIPLIER
                    };
                    DBDataContext.Db.TBL_USAGEs.InsertOnSubmit(objNewMeterUsage);
                    // add previous month 
                    var objNewMeterUsage1 = DBDataContext.Db.TBL_USAGEs.FirstOrDefault(row => row.CUSTOMER_ID == _objCustomer.CUSTOMER_ID &&
                                                                                           row.METER_ID == _objMeterNew.METER_ID &&
                                                                                           row.USAGE_MONTH == datMonth.AddMonths(-1));
                    if (objNewMeterUsage1 == null)
                    {
                        objNewMeterUsage1 = new TBL_USAGE()
                        {
                            COLLECTOR_ID = 0,
                            CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                            CUSTOMER_ID = this._objCustomer.CUSTOMER_ID,
                            DEVICE_ID = 0,
                            END_USAGE = DataHelper.ParseToDecimal(this.txtNEW_METER_USAGE.Text),
                            END_USE_DATE = datEnd,
                            IS_METER_RENEW_CYCLE = false,
                            METER_ID = this._objCustomerMeterNew.METER_ID,
                            POSTING_DATE = now,
                            START_USAGE = DataHelper.ParseToDecimal(this.txtNEW_METER_USAGE.Text),
                            START_USE_DATE = datStart,
                            USAGE_MONTH = datMonth.AddMonths(-1),
                            MULTIPLIER = _objMeterNew.MULTIPLIER
                        };
                        DBDataContext.Db.TBL_USAGEs.InsertOnSubmit(objNewMeterUsage1);
                    }

                    DBDataContext.Db.SubmitChanges();

                    // 5. UPDATE OLD METER STATUS
                    TBL_METER tmpOld = new TBL_METER();
                    _objMeterOld._CopyTo(tmpOld);
                    _objMeterOld.STATUS_ID = (int)this.cboOLD_METER_STATUS.SelectedValue;
                    DBDataContext.Db.Update(tmpOld, _objMeterOld);


                    // 6. UPDATE NEW METER STATUS
                    TBL_METER tmpNew = new TBL_METER();
                    _objMeterNew._CopyTo(tmpNew);
                    _objMeterNew.STATUS_ID = (int)MeterStatus.Used;
                    DBDataContext.Db.Update(tmpNew, _objMeterNew);


                    // log meter status change
                    DBDataContext.Db.TBL_STOCK_TRANs.InsertOnSubmit(new TBL_STOCK_TRAN()
                    {
                        STOCK_TRAN_TYPE_ID = (int)StockTranType.Use,
                        FROM_STOCK_TYPE_ID = (int)StockType.Stock,
                        TO_STOCK_TYPE_ID = (int)StockType.Used,
                        CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                        CREATE_ON = DBDataContext.Db.GetSystemDate(),
                        ITEM_ID = _objMeterNew.METER_TYPE_ID,
                        REMARK = _objMeterNew.METER_CODE,
                        ITEM_TYPE_ID = (int)StockItemType.Meter
                    });
                    DBDataContext.Db.SubmitChanges();
                    DBDataContext.Db.TBL_STOCK_TRANs.InsertOnSubmit(new TBL_STOCK_TRAN()
                    {
                        STOCK_TRAN_TYPE_ID = (int)StockTranType.StockIn,
                        FROM_STOCK_TYPE_ID = (int)StockType.Used,
                        TO_STOCK_TYPE_ID = (int)_objMeterOld.STATUS_ID,
                        CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                        CREATE_ON = DBDataContext.Db.GetSystemDate(),
                        ITEM_ID = _objMeterOld.METER_TYPE_ID,
                        REMARK = _objMeterOld.METER_CODE,
                        ITEM_TYPE_ID = (int)StockItemType.Meter
                    });
                    DBDataContext.Db.SubmitChanges();



                    // 7. DELETE IN TBL_UNKNOWN_METER
                    Method.RemoveUnknownMeter(_objMeterNew.METER_CODE);

                    // Log user change meter
                    DBDataContext.Db.TBL_CUSTOMER_CHANGE_METERs.InsertOnSubmit(new TBL_CUSTOMER_CHANGE_METER()
                    {
                        CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                        CREATE_ON = DBDataContext.Db.GetSystemDate(),
                        CUSTOMER_ID = _objCustomer.CUSTOMER_ID,
                        IS_ACTIVE = true,
                        IS_METER_RENEW_CYCLE = this.chkIS_NEW_CYCLE.Checked,
                        LOGIN_ID = SoftTech.Security.Logic.Login.CurrentLogin.LOGIN_ID,
                        NEW_METER_ID = _objCustomerMeterNew.METER_ID,
                        NEW_METER_START_USAGE = DataHelper.ParseToDecimal(this.txtNEW_METER_USAGE.Text),
                        OLD_METER_END_USAGE = DataHelper.ParseToDecimal(this.txtOLD_METER_USAGE.Text),
                        OLD_METER_ID = _objCustomerMeterOld.METER_ID,
                        OLD_METER_START_USAGE = DataHelper.ParseToDecimal(this.txtOLD_METER_LAST_USAGE.Text),
                        MULTIPLIER = _objMeterOld.MULTIPLIER,
                        TOTAL_USAGE = Method.GetTotalUsage(DataHelper.ParseToDecimal(this.txtOLD_METER_LAST_USAGE.Text), DataHelper.ParseToDecimal(this.txtOLD_METER_USAGE.Text), _objMeterOld.MULTIPLIER, this.chkIS_NEW_CYCLE.Checked),
                        CHANGE_DATE = dtpCHANGE_DATE.Value,
                        NOTE = cboReason.Text,
                        REASON_ID = (int)cboReason.SelectedValue
                    }); ;
                    DBDataContext.Db.SubmitChanges();

                    tran.Complete();
                }

                // if succes than close form.
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }
        #endregion Method

        #region Events
        private void txtMeter_AdvanceSearch(object sender, EventArgs e)
        {
            if (txtOLD_METER.Text.Trim() == "")
            {
                txtOLD_METER.CancelSearch();
                return;
            }

            string strMeterCode = Method.FormatMeterCode(txtOLD_METER.Text);
            _objMeterOld = DBDataContext.Db.TBL_METERs.FirstOrDefault(m => m.METER_CODE == strMeterCode);

            if (_objMeterOld == null)
            {
                this.txtOLD_METER.CancelSearch();
                return;
            }
            TBL_METER_TYPE objMeterType = DBDataContext.Db.TBL_METER_TYPEs.FirstOrDefault(mt => mt.METER_TYPE_ID == _objMeterOld.METER_TYPE_ID);
            txtOLD_METER_METER_TYPE.Text = objMeterType.METER_TYPE_NAME;
            txtOLD_METER_AMPHERE.Text = DBDataContext.Db.TBL_AMPAREs.FirstOrDefault(a => a.AMPARE_ID == objMeterType.METER_AMP_ID).AMPARE_NAME;
            txtOLD_METER_PHASE.Text = DBDataContext.Db.TBL_PHASEs.FirstOrDefault(p => p.PHASE_ID == objMeterType.METER_PHASE_ID).PHASE_NAME;
            txtOLD_METER_VOLTAGE.Text = DBDataContext.Db.TBL_VOLTAGEs.FirstOrDefault(v => v.VOLTAGE_ID == objMeterType.METER_VOL_ID).VOLTAGE_NAME;
            txtOLD_METER_CONSTANT.Text = DBDataContext.Db.TBL_CONSTANTs.FirstOrDefault(c => c.CONSTANT_ID == objMeterType.METER_CONST_ID).CONSTANT_NAME;
        }

        private void txtNEW_METER_AdvanceSearch(object sender, EventArgs e)
        {
            // if not entry.
            if (txtNEW_METER.Text.Trim() == "")
            {
                txtNEW_METER.CancelSearch();
                return;
            }

            string strMeterCode = Method.FormatMeterCode(txtNEW_METER.Text);
            TBL_METER tmp = DBDataContext.Db.TBL_METERs.FirstOrDefault(m => m.METER_CODE == strMeterCode);

            // if not contain in database.
            if (tmp == null)
            {
                //if user have permission to add new meter
                if (Login.IsAuthorized(Permission.ADMIN_METER))
                {
                    //if user agree to add new meter
                    if (MsgBox.ShowQuestion(string.Format(Resources.MSQ_CREATE_METER, strMeterCode), "") == DialogResult.Yes)
                    {
                        DialogMeter diagMeter = new DialogMeter(GeneralProcess.Insert, new TBL_METER() { METER_CODE = strMeterCode });
                        diagMeter.ShowDialog();
                        if (diagMeter.DialogResult == DialogResult.OK)
                        {
                            tmp = diagMeter.Meter;
                        }
                        else
                        {
                            this.txtNEW_METER.CancelSearch();
                            return;
                        }
                    }
                    else
                    {
                        this.txtNEW_METER.CancelSearch();
                        return;
                    }
                }
                else
                {
                    MsgBox.ShowInformation(Resources.MS_METER_NOT_FOUND);
                    this.txtNEW_METER.CancelSearch();
                    return;
                }
            }

            // if meter is inused.
            if (tmp.STATUS_ID == (int)MeterStatus.Used)
            {
                MsgBox.ShowInformation(Resources.MS_METER_STATUS_IN_USE);
                this.txtNEW_METER.CancelSearch();
                return;
            }

            // if meter is unavailable.
            if (tmp.STATUS_ID == (int)MeterStatus.Unavailable)
            {
                MsgBox.ShowInformation(Resources.MS_METER_IS_UNAVAILABLE);
                this.txtNEW_METER.CancelSearch();
                return;
            }

            // meter is selected.
            _objMeterNew = new TBL_METER();
            tmp._CopyTo(_objMeterNew);

            TBL_METER_TYPE objMeterType = DBDataContext.Db.TBL_METER_TYPEs.FirstOrDefault(mt => mt.METER_TYPE_ID == _objMeterNew.METER_TYPE_ID);
            txtNEW_METER.Text = tmp.METER_CODE;
            txtNEW_METER_TYPE.Text = objMeterType.METER_TYPE_NAME;
            txtNEW_METER_AMPARE.Text = DBDataContext.Db.TBL_AMPAREs.FirstOrDefault(a => a.AMPARE_ID == objMeterType.METER_AMP_ID).AMPARE_NAME;
            txtNEW_METER_PHASE.Text = DBDataContext.Db.TBL_PHASEs.FirstOrDefault(p => p.PHASE_ID == objMeterType.METER_PHASE_ID).PHASE_NAME;
            txtNEW_METER_VOLTAGE.Text = DBDataContext.Db.TBL_VOLTAGEs.FirstOrDefault(v => v.VOLTAGE_ID == objMeterType.METER_VOL_ID).VOLTAGE_NAME;
            txtNEW_METER_CONSTANT.Text = DBDataContext.Db.TBL_CONSTANTs.FirstOrDefault(c => c.CONSTANT_ID == objMeterType.METER_CONST_ID).CONSTANT_NAME;

        }

        private void txtNEW_METER_CancelAdvanceSearch(object sender, EventArgs e)
        {
            txtNEW_METER_TYPE.Text = "";
            txtNEW_METER_AMPARE.Text = "";
            txtNEW_METER_PHASE.Text = "";
            txtNEW_METER_VOLTAGE.Text = "";
            txtNEW_METER_CONSTANT.Text = "";
            _objMeterNew = null;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            // validate it first.
            if (invalid())
            {
                return;
            }

            // MORE VALIDATION 
            // if start usage is greater than end usage 
            // and user not check the new cycle then 
            // ask he/she to verify it's not an incorrect input.
            if (DataHelper.ParseToDecimal(this.txtOLD_METER_LAST_USAGE.Text) > DataHelper.ParseToDecimal(this.txtOLD_METER_USAGE.Text) != this.chkIS_NEW_CYCLE.Checked)
            {
                MsgBox.ShowInformation(Resources.MS_METER_IS_NEW_CYCLE, Resources.NewCycle);
                return;
            }
            Runner.Run(saveData);

        }
        #endregion Events

        private void txtOLD_METER_USAGE_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.txtNEW_METER.Focus();
            }
        }
    }
}
