﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogCircuitBreaker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCircuitBreaker));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cboStatus = new System.Windows.Forms.ComboBox();
            this.lblSTATUS = new System.Windows.Forms.Label();
            this.cboBrakerType = new System.Windows.Forms.ComboBox();
            this.lblBREAKER_TYPE = new System.Windows.Forms.Label();
            this.txtBrakerCode = new System.Windows.Forms.TextBox();
            this.lblBREAKER = new System.Windows.Forms.Label();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.btnAddCbType = new SoftTech.Component.ExAddItem();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.btnAddCbType);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.label4);
            this.content.Controls.Add(this.label3);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.cboStatus);
            this.content.Controls.Add(this.lblSTATUS);
            this.content.Controls.Add(this.cboBrakerType);
            this.content.Controls.Add(this.lblBREAKER_TYPE);
            this.content.Controls.Add(this.txtBrakerCode);
            this.content.Controls.Add(this.lblBREAKER);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.lblBREAKER, 0);
            this.content.Controls.SetChildIndex(this.txtBrakerCode, 0);
            this.content.Controls.SetChildIndex(this.lblBREAKER_TYPE, 0);
            this.content.Controls.SetChildIndex(this.cboBrakerType, 0);
            this.content.Controls.SetChildIndex(this.lblSTATUS, 0);
            this.content.Controls.SetChildIndex(this.cboStatus, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.label3, 0);
            this.content.Controls.SetChildIndex(this.label4, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.btnAddCbType, 0);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Name = "label4";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Name = "label3";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // cboStatus
            // 
            this.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboStatus.FormattingEnabled = true;
            resources.ApplyResources(this.cboStatus, "cboStatus");
            this.cboStatus.Name = "cboStatus";
            this.cboStatus.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // lblSTATUS
            // 
            resources.ApplyResources(this.lblSTATUS, "lblSTATUS");
            this.lblSTATUS.Name = "lblSTATUS";
            // 
            // cboBrakerType
            // 
            this.cboBrakerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBrakerType.FormattingEnabled = true;
            resources.ApplyResources(this.cboBrakerType, "cboBrakerType");
            this.cboBrakerType.Name = "cboBrakerType";
            this.cboBrakerType.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // lblBREAKER_TYPE
            // 
            resources.ApplyResources(this.lblBREAKER_TYPE, "lblBREAKER_TYPE");
            this.lblBREAKER_TYPE.Name = "lblBREAKER_TYPE";
            // 
            // txtBrakerCode
            // 
            resources.ApplyResources(this.txtBrakerCode, "txtBrakerCode");
            this.txtBrakerCode.Name = "txtBrakerCode";
            this.txtBrakerCode.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // lblBREAKER
            // 
            resources.ApplyResources(this.lblBREAKER, "lblBREAKER");
            this.lblBREAKER.Name = "lblBREAKER";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // btnAddCbType
            // 
            this.btnAddCbType.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnAddCbType, "btnAddCbType");
            this.btnAddCbType.Name = "btnAddCbType";
            this.btnAddCbType.AddItem += new System.EventHandler(this.btnAddCbType_AddItem);
            // 
            // DialogCircuitBreaker
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogCircuitBreaker";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label label4;
        private Label label3;
        private Label label9;
        private ComboBox cboStatus;
        private Label lblSTATUS;
        private ComboBox cboBrakerType;
        private Label lblBREAKER_TYPE;
        private TextBox txtBrakerCode;
        private Label lblBREAKER;
        private ExButton btnCHANGE_LOG;
        private ExAddItem btnAddCbType;
    }
}