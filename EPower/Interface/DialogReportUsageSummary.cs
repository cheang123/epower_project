﻿using System;
using System.Linq;
using System.Windows.Forms;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class DialogReportUsageSummary: ExDialog
    { 
        public DialogReportUsageSummary( )
        {
            InitializeComponent();

            var p1 = DBDataContext.Db.TBL_PARAM_REPORT_USAGEs.FirstOrDefault(row => row.PARAM_ID == 1);
            if (p1 != null)
            {
                this.txtFrom1.Text = p1.FROM_USAGE.ToString(UIHelper._DefaultUsageFormat);
                this.txtTo1.Text = p1.TO_USAGE.ToString(UIHelper._DefaultUsageFormat);
            }

            var p2 = DBDataContext.Db.TBL_PARAM_REPORT_USAGEs.FirstOrDefault(row => row.PARAM_ID == 2);
            if (p2 != null)
            {
                this.txtFrom2.Text = p2.FROM_USAGE.ToString(UIHelper._DefaultUsageFormat);
                this.txtTo2.Text = p2.TO_USAGE.ToString(UIHelper._DefaultUsageFormat);
            }

            var p3 = DBDataContext.Db.TBL_PARAM_REPORT_USAGEs.FirstOrDefault(row => row.PARAM_ID == 3);
            if (p3 != null)
            {
                this.txtFrom3.Text = p3.FROM_USAGE.ToString(UIHelper._DefaultUsageFormat);
                this.txtTo3.Text = p3.TO_USAGE.ToString(UIHelper._DefaultUsageFormat);
            }

            var p4 = DBDataContext.Db.TBL_PARAM_REPORT_USAGEs.FirstOrDefault(row => row.PARAM_ID == 4);
            if (p4 != null)
            {
                this.txtFrom4.Text = p4.FROM_USAGE.ToString(UIHelper._DefaultUsageFormat);
                this.txtTo4.Text = p4.TO_USAGE.ToString(UIHelper._DefaultUsageFormat);
            }

        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            var p1 = DBDataContext.Db.TBL_PARAM_REPORT_USAGEs.FirstOrDefault(row => row.PARAM_ID == 1);
            if (p1 != null)
            {
                p1.FROM_USAGE = DataHelper.ParseToInt(this.txtFrom1.Text);
                p1.TO_USAGE = DataHelper.ParseToInt(this.txtTo1.Text);
            }
            else
            {
                DBDataContext.Db.TBL_PARAM_REPORT_USAGEs.InsertOnSubmit(
                new TBL_PARAM_REPORT_USAGE()
                {
                    PARAM_ID = 1,
                    FROM_USAGE = DataHelper.ParseToInt(this.txtFrom1.Text),
                    TO_USAGE = DataHelper.ParseToInt(this.txtTo1.Text)
                });
            }


            var p2 = DBDataContext.Db.TBL_PARAM_REPORT_USAGEs.FirstOrDefault(row => row.PARAM_ID == 2);
            if (p2 != null)
            {
                p2.FROM_USAGE = DataHelper.ParseToInt(this.txtFrom2.Text);
                p2.TO_USAGE = DataHelper.ParseToInt(this.txtTo2.Text);
            }
            else
            {
                DBDataContext.Db.TBL_PARAM_REPORT_USAGEs.InsertOnSubmit(
                new TBL_PARAM_REPORT_USAGE()
                {
                    PARAM_ID = 2,
                    FROM_USAGE = DataHelper.ParseToInt(this.txtFrom2.Text),
                    TO_USAGE = DataHelper.ParseToInt(this.txtTo2.Text)
                });
            }


            var p3 = DBDataContext.Db.TBL_PARAM_REPORT_USAGEs.FirstOrDefault(row => row.PARAM_ID == 3);
            if (p3 != null)
            {
                p3.FROM_USAGE = DataHelper.ParseToInt(this.txtFrom3.Text);
                p3.TO_USAGE = DataHelper.ParseToInt(this.txtTo3.Text);
            }
            else
            {
                DBDataContext.Db.TBL_PARAM_REPORT_USAGEs.InsertOnSubmit(
                new TBL_PARAM_REPORT_USAGE()
                {
                    PARAM_ID = 3,
                    FROM_USAGE = DataHelper.ParseToInt(this.txtFrom3.Text),
                    TO_USAGE = DataHelper.ParseToInt(this.txtTo3.Text)
                });
            }


            var p4 = DBDataContext.Db.TBL_PARAM_REPORT_USAGEs.FirstOrDefault(row => row.PARAM_ID == 4);
            if (p4 != null)
            {
                p4.FROM_USAGE = DataHelper.ParseToInt(this.txtFrom4.Text);
                p4.TO_USAGE = DataHelper.ParseToInt(this.txtTo4.Text);
            }
            else
            {
                DBDataContext.Db.TBL_PARAM_REPORT_USAGEs.InsertOnSubmit(
                new TBL_PARAM_REPORT_USAGE()
                {
                    PARAM_ID = 4,
                    FROM_USAGE = DataHelper.ParseToInt(this.txtFrom4.Text),
                    TO_USAGE = DataHelper.ParseToInt(this.txtTo4.Text)
                });
            }
            DBDataContext.Db.SubmitChanges();

            this.DialogResult = DialogResult.OK;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}