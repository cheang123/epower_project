﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech.Component;

namespace EPower.Interface
{
    public partial class DialogChangeLanguage: ExDialog
    {
        DataSet ds = new DataSet();

        public DialogChangeLanguage()
        {
            InitializeComponent();


            string strDefaulLan = Settings.Default.LANGUAGE;
            if (strDefaulLan.Trim()==string.Empty)
            {
                radKh.Checked = true; ;
                radKh.Font = new Font(radKh.Font, FontStyle.Bold);
            }
            else
            {
                radEng.Checked = true;
                radEng.Font = new Font(radEng.Font, FontStyle.Bold);
            }

        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (radEng.Checked && radEng.Font.Style== FontStyle.Bold)
            {
                this.DialogResult = DialogResult.No;
                return;
                
            }
            if (radKh.Checked && radKh.Font.Style== FontStyle.Bold)
            {
                this.DialogResult = DialogResult.No;
                return;
            }
            try
            {
                Settings.Default.LANGUAGE = radKh.Checked ? "" : "en-GB";
                Settings.Default.Save();
                Application.Restart();
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

    }
}
