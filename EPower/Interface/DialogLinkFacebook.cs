﻿using EPower.Base.Logic;
using SoftTech.Component;
using System;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogLinkFacebook : ExDialog
    {
        public string text;
        public DialogLinkFacebook(string _url)
        {
            InitializeComponent();
            //this.webBrowser1.Height = this.content.Height- this.panel1.Height;
            //this.webBrowser1.Width = this.content.Width;
            //this.webBrowser1.Location = this.content.Location;
            //this.WindowState = FormWindowState.Normal;
            //this.StartPosition = FormStartPosition.Manual;
            //this.Top = 0;
            //this.Left = 0;
            //this.Height = Screen.GetWorkingArea(this).Height;
            //this.Width = Screen.GetWorkingArea(this).Width;
            //this.btnOK.Location = new Point(Width - 100, Height - 50);
            //this.panel2.Width = this.panel1.Width = Width-1;
            //this.panel1.Height = 50;
            //this.panel1.Location = this.panel2.Location = new Point(panel1.Location.X, Height-62);
            Method.ChangeIEVersion();
            webBrowser1.Navigate(_url);
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            webBrowser1.Navigate("www.facecbook.com/me");
            while (this.webBrowser1.ReadyState != WebBrowserReadyState.Complete)
            {
                Runner.RunNewThread(delegate ()
                {
                    Application.DoEvents();
                });
            }
            text = webBrowser1.Url.ToString();
            this.DialogResult = DialogResult.OK;
        }
    }
}
