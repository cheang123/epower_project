﻿using EPower.Base.Logic;
using EPower.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogPrintReceipt : ExDialog
    {
        #region Private Data
        TBL_CUSTOMER _objCustomer = null;
        TBL_PAYMENT _objPayment = null;
        TBL_CUS_PREPAYMENT _objPrepayment = null;
        #endregion Private Data

        #region Constructor
        public DialogPrintReceipt()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgvPayment);
            dtpMonth.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        }
        #endregion Constructor

        #region Method

        private void DisplayCustomer(bool blnBindByCustomer)
        {
            txtCustomerName.AcceptSearch(false);
            txtCustomerCode.AcceptSearch(false);
            txtPaymentNumber.AcceptSearch(false);

            // show customer information.
            this.txtCustomerName.Text = this._objCustomer.LAST_NAME_KH + " " + this._objCustomer.FIRST_NAME_KH;
            this.txtCustomerCode.Text = this._objCustomer.CUSTOMER_CODE;
            this.txtAreaName.Text = DBDataContext.Db.TBL_AREAs.FirstOrDefault(row => row.AREA_ID == this._objCustomer.AREA_ID).AREA_NAME;

            this.txtBox.Text = (from b in DBDataContext.Db.TBL_BOXes
                                join cusmeter in DBDataContext.Db.TBL_CUSTOMER_METERs on b.BOX_ID equals cusmeter.BOX_ID
                                where cusmeter.CUSTOMER_ID == _objCustomer.CUSTOMER_ID
                                select b.BOX_CODE).FirstOrDefault();

            DateTime date = new DateTime(dtpMonth.Value.Year, dtpMonth.Value.Month, 1).AddMonths(1).AddSeconds(-1).Date;



            if (blnBindByCustomer)
            {
                //Display payment in data grid
                var payments = (from p in DBDataContext.Db.TBL_PAYMENTs
                                join pd in DBDataContext.Db.TBL_PAYMENT_DETAILs on p.PAYMENT_ID equals pd.PAYMENT_ID
                                join c in DBDataContext.Db.TLKP_CURRENCies on p.CURRENCY_ID equals c.CURRENCY_ID
                                join acc in DBDataContext.Db.TBL_ACCOUNT_CHARTs on p.PAYMENT_ACCOUNT_ID equals acc.ACCOUNT_ID
                                join bpd in DBDataContext.Db.TBL_BANK_PAYMENT_DETAILs on p.BANK_PAYMENT_DETAIL_ID equals bpd.BANK_PAYMENT_DETAIL_ID into l
                                from bpd in l.DefaultIfEmpty()
                                where p.CUSTOMER_ID == _objCustomer.CUSTOMER_ID
                                             && p.IS_ACTIVE
                                             && p.PAY_DATE.Month == date.Month && p.PAY_DATE.Year == date.Year
                                             && p.IS_VOID == false
                                group p by new { p.PAYMENT_ID, p.PAY_DATE, p.PAYMENT_NO, CREATE_BY = bpd.BANK_ALIAS ?? p.CREATE_BY, acc.ACCOUNT_NAME, p.PAY_AMOUNT, c.CURRENCY_SING }
                                        into tmp
                                select new PaymentPrintListModel()
                                {
                                    PAYMENT_ID = tmp.Key.PAYMENT_ID,
                                    CUS_PREPAYMENT_ID = 0,
                                    PAY_DATE = tmp.Key.PAY_DATE,
                                    PAYMENT_NO = tmp.Key.PAYMENT_NO,
                                    CREATE_BY = tmp.Key.CREATE_BY,
                                    ACCOUNT_NAME = tmp.Key.ACCOUNT_NAME,
                                    PAY_AMOUNT = tmp.Key.PAY_AMOUNT,
                                    CURRENCY_SING = tmp.Key.CURRENCY_SING,
                                    NO_INVOICE = tmp.Count()
                                }).ToList();

                payments = payments ?? new List<PaymentPrintListModel>();

                var prepayment = (from p in DBDataContext.Db.TBL_CUS_PREPAYMENTs
                                  join c in DBDataContext.Db.TLKP_CURRENCies on p.CURRENCY_ID equals c.CURRENCY_ID
                                  join acc in DBDataContext.Db.TBL_ACCOUNT_CHARTs on p.PAYMENT_ACCOUNT_ID equals acc.ACCOUNT_ID
                                  where p.CUSTOMER_ID == _objCustomer.CUSTOMER_ID
                                  && p.CREATE_ON.Month == date.Month && p.CREATE_ON.Year == date.Year
                                  group p by new
                                  {
                                      p.PAYMENT_ID,
                                      p.CUS_PREPAYMENT_ID,
                                      p.CREATE_ON,
                                      p.PREPAYMENT_NO,
                                      acc.ACCOUNT_NAME,
                                      p.CREATE_BY,
                                      p.AMOUNT,
                                      c.CURRENCY_SING
                                  } into tmp
                                  select new PaymentPrintListModel()
                                  {
                                      PAYMENT_ID = tmp.Key.PAYMENT_ID,
                                      CUS_PREPAYMENT_ID = tmp.Key.CUS_PREPAYMENT_ID,
                                      PAY_DATE = tmp.Key.CREATE_ON,
                                      PAYMENT_NO = tmp.Key.PREPAYMENT_NO,
                                      CREATE_BY = tmp.Key.CREATE_BY,
                                      ACCOUNT_NAME = tmp.Key.ACCOUNT_NAME,
                                      PAY_AMOUNT = tmp.Key.AMOUNT,
                                      CURRENCY_SING = tmp.Key.CURRENCY_SING,
                                      NO_INVOICE = tmp.Count()
                                  }).ToList();
                prepayment = prepayment ?? new List<PaymentPrintListModel>();
                dgvPayment.DataSource = payments.Union(prepayment).OrderBy(x => x.PAY_DATE).ToList();
            }
            else
            {
                //Display payment in data grid
                var payments = (from p in DBDataContext.Db.TBL_PAYMENTs
                                join pd in DBDataContext.Db.TBL_PAYMENT_DETAILs on p.PAYMENT_ID equals pd.PAYMENT_ID
                                join c in DBDataContext.Db.TLKP_CURRENCies on p.CURRENCY_ID equals c.CURRENCY_ID
                                join acc in DBDataContext.Db.TBL_ACCOUNT_CHARTs on p.PAYMENT_ACCOUNT_ID equals acc.ACCOUNT_ID
                                where p.PAYMENT_ID == _objPayment.PAYMENT_ID
                                && p.IS_ACTIVE
                                && DBDataContext.Db.TBL_PAYMENTs.Count(row => row.PAYMENT_NO == p.PAYMENT_NO) == 1
                                group p by new
                                {
                                    p.PAYMENT_ID,
                                    p.PAY_DATE,
                                    p.PAYMENT_NO,
                                    p.CREATE_BY,
                                    acc.ACCOUNT_NAME,
                                    p.PAY_AMOUNT,
                                    c.CURRENCY_SING
                                } into tmp
                                select new PaymentPrintListModel()
                                {
                                    PAYMENT_ID = tmp.Key.PAYMENT_ID,
                                    PAY_DATE = tmp.Key.PAY_DATE,
                                    PAYMENT_NO = tmp.Key.PAYMENT_NO,
                                    CREATE_BY = tmp.Key.CREATE_BY,
                                    ACCOUNT_NAME = tmp.Key.ACCOUNT_NAME,
                                    PAY_AMOUNT = tmp.Key.PAY_AMOUNT,
                                    CURRENCY_SING = tmp.Key.CURRENCY_SING,
                                    NO_INVOICE = tmp.Count()
                                }).ToList();

                var prepayment = (from p in DBDataContext.Db.TBL_CUS_PREPAYMENTs
                                  join c in DBDataContext.Db.TLKP_CURRENCies on p.CURRENCY_ID equals c.CURRENCY_ID
                                  join acc in DBDataContext.Db.TBL_ACCOUNT_CHARTs on p.PAYMENT_ACCOUNT_ID equals acc.ACCOUNT_ID
                                  where p.CUSTOMER_ID == _objCustomer.CUSTOMER_ID
                                  && p.CREATE_ON.Month == date.Month && p.CREATE_ON.Year == date.Year
                                  group p by new
                                  {
                                      p.PAYMENT_ID,
                                      p.CUS_PREPAYMENT_ID,
                                      p.CREATE_ON,
                                      p.PREPAYMENT_NO,
                                      acc.ACCOUNT_NAME,
                                      p.CREATE_BY,
                                      p.AMOUNT,
                                      c.CURRENCY_SING
                                  } into tmp
                                  select new PaymentPrintListModel()
                                  {
                                      PAYMENT_ID = tmp.Key.PAYMENT_ID,
                                      CUS_PREPAYMENT_ID = tmp.Key.CUS_PREPAYMENT_ID,
                                      PAY_DATE = tmp.Key.CREATE_ON,
                                      PAYMENT_NO = tmp.Key.PREPAYMENT_NO,
                                      CREATE_BY = tmp.Key.CREATE_BY,
                                      ACCOUNT_NAME = tmp.Key.ACCOUNT_NAME,
                                      PAY_AMOUNT = tmp.Key.AMOUNT,
                                      CURRENCY_SING = tmp.Key.CURRENCY_SING,
                                      NO_INVOICE = tmp.Count()
                                  }).ToList();
                prepayment = prepayment ?? new List<PaymentPrintListModel>();
                dgvPayment.DataSource = payments.Union(prepayment).OrderBy(x => x.PAY_DATE).ToList();
            }

            this.btnPRINT.Enabled = true;
        }

        /// <summary>
        /// New Interface after cancel payment
        /// </summary>
        private void newPaymentCancel()
        {
            _objCustomer = null;
            _objPayment = null;

            txtAreaName.Text = "";
            txtBox.Text = "";
            txtCustomerCode.Text = "";
            txtPaymentNumber.Text = "";
            txtCustomerName.Text = "";

            dgvPayment.DataSource = new List<PaymentPrintListModel>();

            this.txtCustomerCode.CancelSearch(false);
            this.txtCustomerName.CancelSearch(false);
            this.txtPaymentNumber.CancelSearch(false);
        }

        private void PrintReceipt()
        {
            if (dgvPayment.SelectedRows.Count == 0)
            {
                MsgBox.ShowInformation(lblSELECT_PAYMENT_TO_PRINT.Text);
                return;
            }

            int intPaymentID = DataHelper.ParseToInt(dgvPayment.SelectedRows[0].Cells["PAYMENT_ID"].Value.ToString());
            int intPrepaymentID = DataHelper.ParseToInt(dgvPayment.SelectedRows[0].Cells["CUS_PREPAYMENT_ID"].Value.ToString());


            _objPayment = DBDataContext.Db.TBL_PAYMENTs.Where(p => p.PAYMENT_ID == intPaymentID).FirstOrDefault();
            _objPrepayment = DBDataContext.Db.TBL_CUS_PREPAYMENTs.Where(p => p.CUS_PREPAYMENT_ID == intPrepaymentID).FirstOrDefault();
            DateTime datDate = DBDataContext.Db.GetSystemDate();

            if (intPaymentID != 0)
            {
                Printing.ReceiptPayment((int)intPaymentID);
            }
            else
            {
                CrystalReportHelper ch = new CrystalReportHelper("ReportReceiptPrepayment.rpt");
                ch.SetParameter("CUS_PREPAYMENT_ID", intPrepaymentID);
                ch.PrintReport(Settings.Default.PRINTER_RECIEPT);
            }
        }
        #endregion Method

        #region Event
        private void txtCustomerCode_AdvanceSearch(object sender, EventArgs e)
        {
            if (this.txtCustomerCode.Text == "")
            {
                this.txtCustomerCode.CancelSearch(false);
                return;
            }
            _objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_CODE == Method.FormatCustomerCode(this.txtCustomerCode.Text));
            if (_objCustomer == null)
            {
                this.txtCustomerCode.CancelSearch(false);
                MsgBox.ShowInformation(Resources.MS_CUSTOMER_NOT_EXISTS);
                return;
            }
            DisplayCustomer(true);
        }

        private void txtCustomerName_AdvanceSearch(object sender, EventArgs e)
        {
            DialogCustomerSearch diag = new DialogCustomerSearch(this.txtCustomerName.Text, DialogCustomerSearch.PowerType.Postpaid);
            if (diag.ShowDialog() != DialogResult.OK)
            {
                this.txtCustomerName.CancelSearch(false);
                return;
            }
            this._objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == diag.CustomerID);
            if (_objCustomer == null)
            {
                this.txtCustomerName.CancelSearch(false);
                return;
            }
            DisplayCustomer(true);
        }

        private void txtPaymentNumber_AdvanceSearch(object sender, EventArgs e)
        {
            if (this.txtPaymentNumber.Text == "")
            {
                this.txtPaymentNumber.CancelSearch(false);
                return;
            }

            _objPayment = DBDataContext.Db.TBL_PAYMENTs.Where(p => p.PAYMENT_NO == txtPaymentNumber.Text.Trim()).FirstOrDefault();

            //if payment not found
            if (_objPayment == null)
            {
                this.txtPaymentNumber.CancelSearch(false);
                MsgBox.ShowInformation(Resources.MS_PAYMENT_NOT_FOUND);
                return;
            }


            //get customer for this payment number
            _objCustomer = (from cus in DBDataContext.Db.TBL_CUSTOMERs
                            join pay in DBDataContext.Db.TBL_PAYMENTs on cus.CUSTOMER_ID equals pay.CUSTOMER_ID
                            where pay.PAYMENT_ID == _objPayment.PAYMENT_ID
                            select cus).FirstOrDefault();


            //display customer information
            DisplayCustomer(false);

            this.btnPRINT.Enabled = true;
        }

        private void txtCustomerCode_CancelAdvanceSearch(object sender, EventArgs e)
        {
            newPaymentCancel();
        }

        private void linkPaymentDetail_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (dgvPayment.SelectedRows.Count > 0)
            {
                int intPaymentId = DataHelper.ParseToInt(dgvPayment.SelectedRows[0].Cells["PAYMENT_ID"].Value.ToString());
                if (intPaymentId == 0)
                {
                    return;
                }
                new DialogPaymentDetailCancel(intPaymentId).ShowDialog();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Runner.Run(PrintReceipt);
        }

        private void dtpMonth_ValueChanged(object sender, EventArgs e)
        {
            if (_objCustomer == null)
            {
                return;
            }


            DisplayCustomer(true);
        }

        private void InputEnglish(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void InputKhmer(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        public class PaymentPrintListModel
        {
            public int PAYMENT_ID { get; set; }
            public int CUS_PREPAYMENT_ID { get; set; }

            public DateTime PAY_DATE { get; set; }
            public string PAYMENT_NO { get; set; }
            public string ACCOUNT_NAME { get; set; }
            public decimal PAY_AMOUNT { get; set; }
            public string CURRENCY_SING { get; set; }
            public int NO_INVOICE { get; set; }
            public string CREATE_BY { get; set; }

        }

        #endregion Event

    }
}
