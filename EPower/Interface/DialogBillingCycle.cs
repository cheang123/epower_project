﻿using System;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;

namespace EPower.Interface
{
    public partial class DialogBillingCycle : ExDialog
    {        
        GeneralProcess _flag;
        TBL_BILLING_CYCLE _objCycle = new TBL_BILLING_CYCLE();
        public TBL_BILLING_CYCLE BillingCycle
        {
            get { return _objCycle; }
        }
        TBL_BILLING_CYCLE _oldObjCycle = new TBL_BILLING_CYCLE();

        #region Constructor
        public DialogBillingCycle(GeneralProcess flag, TBL_BILLING_CYCLE objCycle)
        {
            InitializeComponent();
            _flag = flag;
            objCycle._CopyTo(_objCycle);
            objCycle._CopyTo(_oldObjCycle); 
            if (flag == GeneralProcess.Insert)
            {
                _objCycle.IS_ACTIVE = true;
                
                this.Text = string.Concat(Resources.INSERT, this.Text);
            }
            else if (flag == GeneralProcess.Update)
            {
                this.Text = string.Concat(Resources.UPDATE, this.Text);
            }
            else if (flag == GeneralProcess.Delete)
            {
                this.Text = string.Concat(Resources.DELETE, this.Text);
                UIHelper.SetEnabled(this, false);
            }
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
            read();
        }
        #endregion

        #region Operation
        /// <summary>
        /// Data transaction
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
            //if duplicate record.
            if (DBDataContext.Db.IsExits(_objCycle, "CYCLE_NAME"))
            {
                MsgBox.ShowInformation(string.Format( Resources.MS_IS_EXISTS,lblCYCLE_NAME.Text), Resources.INFORMATION);
                return;
            }
            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objCycle);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_oldObjCycle, _objCycle);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objCycle);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
            
        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
        
        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        /// <summary>
        /// Change current keyboard layout to khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        #endregion

        #region Method
        /// <summary>
        /// Load data from object.
        /// </summary>
        private void read()
        {            
            txtCycleName.Text = _objCycle.CYCLE_NAME;
            numCollectDate.Value = _objCycle.COLLECT_DAY;
            radIsEndOfMoth.Checked = _objCycle.IS_END_MONTH;
            if (!_objCycle.IS_END_MONTH)
            {
                numEndDate.Value = _objCycle.END_DAY;
            }           
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            _objCycle.CYCLE_NAME = txtCycleName.Text.Trim();
            _objCycle.COLLECT_DAY = (int)numCollectDate.Value;
            _objCycle.IS_END_MONTH = radIsEndOfMoth.Checked;
            if (!radIsEndOfMoth.Checked)
            {
                _objCycle.END_DAY = (int)numEndDate.Value;
            }
            else
            {
                _objCycle.END_DAY = 0;
            }
        }

        private bool inValid()
        {
            bool val = false;
            txtCycleName.ClearValidation();
            if (txtCycleName.Text.Trim().Length <= 0)
            {
                txtCycleName.SetValidation(string.Format(Resources.REQUIRED, lblCYCLE_NAME.Text));
                val = true;
            }
            if (radSpecificDay.Checked && numEndDate.Value<=0)
            {
                numEndDate.SetValidation(Resources.MS_BILLING_DAY_MUST_BE_GREATER_THAN_ZERO);
                val = true;
            }
            return val;
        } 
        #endregion         

        private void radSpecificDay_CheckedChanged(object sender, EventArgs e)
        {
            numEndDate.Enabled = radSpecificDay.Checked;
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objCycle);
        }
    }
}
