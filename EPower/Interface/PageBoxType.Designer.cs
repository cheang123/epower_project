﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogPageBoxType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogPageBoxType));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnREMOVE = new SoftTech.Component.ExButton();
            this.btnBox = new SoftTech.Component.ExButton();
            this.btnADD = new SoftTech.Component.ExButton();
            this.btnEDIT = new SoftTech.Component.ExButton();
            this.txtQuickSearch = new SoftTech.Component.ExTextbox();
            this.dgvBoxType = new System.Windows.Forms.DataGridView();
            this.BOX_TYPE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BOX_TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_ACTIVE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBoxType)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.dgvBoxType);
            this.content.Controls.Add(this.panel1);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.dgvBoxType, 0);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.btnREMOVE);
            this.panel1.Controls.Add(this.btnBox);
            this.panel1.Controls.Add(this.btnADD);
            this.panel1.Controls.Add(this.btnEDIT);
            this.panel1.Controls.Add(this.txtQuickSearch);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnREMOVE
            // 
            resources.ApplyResources(this.btnREMOVE, "btnREMOVE");
            this.btnREMOVE.Name = "btnREMOVE";
            this.btnREMOVE.UseVisualStyleBackColor = true;
            this.btnREMOVE.Click += new System.EventHandler(this.btnREMOVE_Click);
            // 
            // btnBox
            // 
            resources.ApplyResources(this.btnBox, "btnBox");
            this.btnBox.Name = "btnBox";
            this.btnBox.UseVisualStyleBackColor = true;
            this.btnBox.Click += new System.EventHandler(this.btnBox_Click);
            // 
            // btnADD
            // 
            resources.ApplyResources(this.btnADD, "btnADD");
            this.btnADD.Name = "btnADD";
            this.btnADD.UseVisualStyleBackColor = true;
            this.btnADD.Click += new System.EventHandler(this.btnADD_Click);
            // 
            // btnEDIT
            // 
            resources.ApplyResources(this.btnEDIT, "btnEDIT");
            this.btnEDIT.Name = "btnEDIT";
            this.btnEDIT.UseVisualStyleBackColor = true;
            this.btnEDIT.Click += new System.EventHandler(this.btnEDIT_Click);
            // 
            // txtQuickSearch
            // 
            this.txtQuickSearch.BackColor = System.Drawing.Color.White;
            this.txtQuickSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtQuickSearch, "txtQuickSearch");
            this.txtQuickSearch.Name = "txtQuickSearch";
            this.txtQuickSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtQuickSearch.QuickSearch += new System.EventHandler(this.txtQuickSearch_QuickSearch);
            this.txtQuickSearch.Enter += new System.EventHandler(this.ChangeKeyboard);
            // 
            // dgvBoxType
            // 
            this.dgvBoxType.AllowUserToAddRows = false;
            this.dgvBoxType.AllowUserToDeleteRows = false;
            this.dgvBoxType.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvBoxType.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvBoxType.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvBoxType.BackgroundColor = System.Drawing.Color.White;
            this.dgvBoxType.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvBoxType.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvBoxType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBoxType.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BOX_TYPE_ID,
            this.BOX_TYPE,
            this.IS_ACTIVE});
            resources.ApplyResources(this.dgvBoxType, "dgvBoxType");
            this.dgvBoxType.EnableHeadersVisualStyles = false;
            this.dgvBoxType.MultiSelect = false;
            this.dgvBoxType.Name = "dgvBoxType";
            this.dgvBoxType.ReadOnly = true;
            this.dgvBoxType.RowHeadersVisible = false;
            this.dgvBoxType.RowTemplate.Height = 25;
            this.dgvBoxType.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // BOX_TYPE_ID
            // 
            this.BOX_TYPE_ID.DataPropertyName = "BOX_TYPE_ID";
            resources.ApplyResources(this.BOX_TYPE_ID, "BOX_TYPE_ID");
            this.BOX_TYPE_ID.Name = "BOX_TYPE_ID";
            this.BOX_TYPE_ID.ReadOnly = true;
            // 
            // BOX_TYPE
            // 
            this.BOX_TYPE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.BOX_TYPE.DataPropertyName = "BOX_TYPE_NAME";
            resources.ApplyResources(this.BOX_TYPE, "BOX_TYPE");
            this.BOX_TYPE.Name = "BOX_TYPE";
            this.BOX_TYPE.ReadOnly = true;
            // 
            // IS_ACTIVE
            // 
            this.IS_ACTIVE.DataPropertyName = "IS_ACTIVE";
            resources.ApplyResources(this.IS_ACTIVE, "IS_ACTIVE");
            this.IS_ACTIVE.Name = "IS_ACTIVE";
            this.IS_ACTIVE.ReadOnly = true;
            // 
            // DialogPageBoxType
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogPageBoxType";
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBoxType)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Panel panel1;
        private ExButton btnREMOVE;
        private ExButton btnADD;
        private ExButton btnEDIT;
        private ExTextbox txtQuickSearch;
        private DataGridView dgvBoxType;
        private DataGridViewTextBoxColumn BOX_TYPE_ID;
        private DataGridViewTextBoxColumn BOX_TYPE;
        private DataGridViewTextBoxColumn IS_ACTIVE;
        private ExButton btnBox;
    }
}