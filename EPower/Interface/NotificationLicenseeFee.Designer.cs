﻿using System.ComponentModel;
using System.Windows.Forms;

namespace EPower.Interface
{
    partial class NotificationLicenseeFee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NotificationLicenseeFee));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnUpdate_ = new System.Windows.Forms.LinkLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.btnUpdate_);
            this.content.Controls.Add(this.panel1);
            resources.ApplyResources(this.content, "content");
            this.content.MouseLeave += new System.EventHandler(this.Notification_MouseLeave);
            this.content.MouseHover += new System.EventHandler(this.Notification_MouseHover);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.btnUpdate_, 0);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnUpdate_
            // 
            resources.ApplyResources(this.btnUpdate_, "btnUpdate_");
            this.btnUpdate_.Name = "btnUpdate_";
            this.btnUpdate_.TabStop = true;
            this.btnUpdate_.Click += new System.EventHandler(this.btnUpdate_Click);
            this.btnUpdate_.MouseHover += new System.EventHandler(this.Notification_MouseHover);
            // 
            // timer1
            // 
            this.timer1.Interval = 250;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // NotificationLicenseeFee
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "NotificationLicenseeFee";
            this.MouseLeave += new System.EventHandler(this.Notification_MouseLeave);
            this.MouseHover += new System.EventHandler(this.Notification_MouseHover);
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private LinkLabel btnUpdate_;
        private Timer timer1;
    }
}