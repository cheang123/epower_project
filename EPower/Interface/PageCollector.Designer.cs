﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageCollector
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageCollector));
            this.dgv = new System.Windows.Forms.DataGridView();
            this.EMPLOYEE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMPLOYEE_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PASSWORD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_ACTIVE = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cboDevice = new System.Windows.Forms.ComboBox();
            this.cboPosition = new System.Windows.Forms.ComboBox();
            this.exButton1 = new SoftTech.Component.ExButton();
            this.btnNew = new SoftTech.Component.ExButton();
            this.btnEdit = new SoftTech.Component.ExButton();
            this.txtQuickSearch = new SoftTech.Component.ExTextbox();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EMPLOYEE_ID,
            this.EMPLOYEE_NAME,
            this.PASSWORD,
            this.IS_ACTIVE});
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellDoubleClick);
            // 
            // EMPLOYEE_ID
            // 
            this.EMPLOYEE_ID.DataPropertyName = "EMPLOYEE_ID";
            resources.ApplyResources(this.EMPLOYEE_ID, "EMPLOYEE_ID");
            this.EMPLOYEE_ID.Name = "EMPLOYEE_ID";
            // 
            // EMPLOYEE_NAME
            // 
            this.EMPLOYEE_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.EMPLOYEE_NAME.DataPropertyName = "EMPLOYEE_NAME";
            resources.ApplyResources(this.EMPLOYEE_NAME, "EMPLOYEE_NAME");
            this.EMPLOYEE_NAME.Name = "EMPLOYEE_NAME";
            // 
            // PASSWORD
            // 
            this.PASSWORD.DataPropertyName = "PASSWORD";
            resources.ApplyResources(this.PASSWORD, "PASSWORD");
            this.PASSWORD.Name = "PASSWORD";
            // 
            // IS_ACTIVE
            // 
            this.IS_ACTIVE.DataPropertyName = "IS_ACTIVE";
            resources.ApplyResources(this.IS_ACTIVE, "IS_ACTIVE");
            this.IS_ACTIVE.Name = "IS_ACTIVE";
            this.IS_ACTIVE.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.IS_ACTIVE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.cboDevice);
            this.panel1.Controls.Add(this.cboPosition);
            this.panel1.Controls.Add(this.exButton1);
            this.panel1.Controls.Add(this.btnNew);
            this.panel1.Controls.Add(this.btnEdit);
            this.panel1.Controls.Add(this.txtQuickSearch);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // cboDevice
            // 
            this.cboDevice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDevice.FormattingEnabled = true;
            this.cboDevice.Items.AddRange(new object[] {
            resources.GetString("cboDevice.Items"),
            resources.GetString("cboDevice.Items1"),
            resources.GetString("cboDevice.Items2"),
            resources.GetString("cboDevice.Items3"),
            resources.GetString("cboDevice.Items4")});
            resources.ApplyResources(this.cboDevice, "cboDevice");
            this.cboDevice.Name = "cboDevice";
            this.cboDevice.SelectedIndexChanged += new System.EventHandler(this.txt_QuickSearch);
            // 
            // cboPosition
            // 
            this.cboPosition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPosition.FormattingEnabled = true;
            this.cboPosition.Items.AddRange(new object[] {
            resources.GetString("cboPosition.Items"),
            resources.GetString("cboPosition.Items1"),
            resources.GetString("cboPosition.Items2"),
            resources.GetString("cboPosition.Items3"),
            resources.GetString("cboPosition.Items4")});
            resources.ApplyResources(this.cboPosition, "cboPosition");
            this.cboPosition.Name = "cboPosition";
            this.cboPosition.SelectedIndexChanged += new System.EventHandler(this.txt_QuickSearch);
            // 
            // exButton1
            // 
            resources.ApplyResources(this.exButton1, "exButton1");
            this.exButton1.Name = "exButton1";
            this.exButton1.UseVisualStyleBackColor = true;
            this.exButton1.Click += new System.EventHandler(this.Remove_Click);
            // 
            // btnNew
            // 
            resources.ApplyResources(this.btnNew, "btnNew");
            this.btnNew.Name = "btnNew";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnEdit
            // 
            resources.ApplyResources(this.btnEdit, "btnEdit");
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // txtQuickSearch
            // 
            this.txtQuickSearch.BackColor = System.Drawing.Color.White;
            this.txtQuickSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtQuickSearch, "txtQuickSearch");
            this.txtQuickSearch.Name = "txtQuickSearch";
            this.txtQuickSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtQuickSearch.QuickSearch += new System.EventHandler(this.txt_QuickSearch);
            this.txtQuickSearch.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // PageCollector
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.panel1);
            this.Name = "PageCollector";
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExTextbox txtQuickSearch;
        private ExButton btnNew;
        private ExButton btnEdit;
        private DataGridView dgv;
        private ExButton exButton1;
        public ComboBox cboDevice;
        public ComboBox cboPosition;
        private DataGridViewTextBoxColumn EMPLOYEE_ID;
        private DataGridViewTextBoxColumn EMPLOYEE_NAME;
        private DataGridViewTextBoxColumn PASSWORD;
        private DataGridViewCheckBoxColumn IS_ACTIVE;
    }
}
