﻿using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;

namespace EPower.Interface
{
    public partial class DialogPriceExtraCharge : ExDialog
    {
        GeneralProcess _flag;
        TBL_PRICE_EXTRA_CHARGE _objPriceExtraCharge = new TBL_PRICE_EXTRA_CHARGE();
        TBL_PRICE _objPrice = new TBL_PRICE();
        public TBL_PRICE_EXTRA_CHARGE PriceExtraCharge
        {
            get { return _objPriceExtraCharge; }
        }
        TBL_PRICE_EXTRA_CHARGE _oldObjPriceExtracharge = new TBL_PRICE_EXTRA_CHARGE();

        #region Constructor
        public DialogPriceExtraCharge(GeneralProcess flag, TBL_PRICE objPrice, TBL_PRICE_EXTRA_CHARGE objPriceExtraCharge)
        {
            InitializeComponent();

            _flag = flag;
            
            if (_flag == GeneralProcess.Insert) 
            {
                objPriceExtraCharge.CHARGE_AFTER = 0;
                objPriceExtraCharge.CHARGE_BEFORE = 1000000000;
            }

            objPriceExtraCharge._CopyTo(_objPriceExtraCharge);
            objPriceExtraCharge._CopyTo(_oldObjPriceExtracharge);
            objPrice._CopyTo(_objPrice);

            var db = from i in DBDataContext.Db.TBL_INVOICE_ITEMs
                     where i.INVOICE_ITEM_TYPE_ID!=0
                     select new
                     {
                         i.INVOICE_ITEM_ID,
                         i.INVOICE_ITEM_NAME
                     };
            UIHelper.SetDataSourceToComboBox(cboInvoiceItem, db);

            cboCharge.Items.Add("%");
            cboCharge.Items.Add(DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x=>x.CURRENCY_ID==_objPrice.CURRENCY_ID).CURRENCY_SING);

            this.Text = flag.GetText(this.Text);
            UIHelper.SetEnabled(this, !(flag == GeneralProcess.Delete));
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;

            read();
        }        
        #endregion

        #region Operation
        /// <summary>
        /// Change Keyboard to Khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        } 

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void KeyPressDecimal(object sender,KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        }

        /// <summary>
        /// Data transaction.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
             //If record is duplicate.
            txtValue.ClearValidation();
            if (DBDataContext.Db.TBL_PRICE_EXTRA_CHARGEs.Where(x => x.INVOICE_ITEM_ID == (int)cboInvoiceItem.SelectedValue && x.IS_ACTIVE && x.PRICE_ID == _objPrice.PRICE_ID && x.EXTRA_CHARGE_ID!=_objPriceExtraCharge.EXTRA_CHARGE_ID).Count()>0)
            {
                txtValue.SetValidation(string.Format( Resources.MS_IS_EXISTS,lblSERVICE.Text));
                return;
            }

            try
            {
                using (TransactionScope tran=new TransactionScope( TransactionScopeOption.Required,TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objPriceExtraCharge);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_oldObjPriceExtracharge, _objPriceExtraCharge);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objPriceExtraCharge);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }                                               
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }            
        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
        #endregion

        #region Method
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();

            if (txtValue.Text.Trim().Length <= 0)
            {
                txtValue.SetValidation(string.Format(Resources.REQUIRED, lblEXTRA_CHARGE.Text));
                val = true;
            }

            if (cboInvoiceItem.SelectedIndex == -1)
            {
                cboInvoiceItem.SetValidation(string.Format(Resources.REQUIRED, lblSERVICE.Text));
                val = true;
            }

            //if (cboCharge.SelectedIndex == 0 && DataHelper.ParseToDecimal(txtValue.Text) > 100)
            //{
            //    txtValue.SetValidation(Resources.MsgPercentageCanNotOver100);
            //    val = true;
            //} 
                      
            return val;
        }

        /// <summary>
        /// Read data from object.
        /// </summary>
        private void read()
        {
            cboInvoiceItem.SelectedValue = _objPriceExtraCharge.INVOICE_ITEM_ID;
            txtValue.Text = _objPriceExtraCharge.CHARGE_BY_PERCENTAGE ? _objPriceExtraCharge.CHARGE_VALUE.ToString("N2") : UIHelper.FormatCurrency(_objPriceExtraCharge.CHARGE_VALUE, _objPrice.CURRENCY_ID);
            txtNote.Text = _objPriceExtraCharge.CHARGE_DESCRIPTION;
            cboCharge.SelectedIndex = _objPriceExtraCharge.CHARGE_BY_PERCENTAGE ? 0 : 1;
            this.chkCHARGE_ON.Checked = !(_objPriceExtraCharge.CHARGE_AFTER == 0 &&
                                       _objPriceExtraCharge.CHARGE_BEFORE == 1000000000);
            this.txtChargeAfter.Text = _objPriceExtraCharge.CHARGE_AFTER.ToString("N2");
            this.txtChargeBefore.Text = _objPriceExtraCharge.CHARGE_BEFORE.ToString("N2");
            this.chkMERGE_INVOICE.Checked = _objPriceExtraCharge.IS_MERGE_INVOICE;

            this.txtChargeAfter.Enabled = this.txtChargeBefore.Enabled = this.chkCHARGE_ON.Checked;
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            _objPriceExtraCharge.PRICE_ID = _objPrice.PRICE_ID;
            _objPriceExtraCharge.INVOICE_ITEM_ID = (int)cboInvoiceItem.SelectedValue;
            _objPriceExtraCharge.CHARGE_BY_PERCENTAGE = cboCharge.SelectedIndex == 0 ? true : false;
            _objPriceExtraCharge.CHARGE_VALUE = DataHelper.ParseToDecimal(txtValue.Text);
            _objPriceExtraCharge.CHARGE_DESCRIPTION = txtNote.Text;
            _objPriceExtraCharge.CHARGE_AFTER = DataHelper.ParseToDecimal(this.txtChargeAfter.Text);
            _objPriceExtraCharge.CHARGE_BEFORE = DataHelper.ParseToDecimal(this.txtChargeBefore.Text);
            _objPriceExtraCharge.IS_MERGE_INVOICE = this.chkMERGE_INVOICE.Checked;
        }
        
        #endregion

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objPriceExtraCharge);
        }

        private void cboInvoiceItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (txtNote.Text !=  cboInvoiceItem.Text)
            {
                txtNote.Text = cboInvoiceItem.Text;
            }
        }

        private void chkChargeOn_CheckedChanged(object sender, EventArgs e)
        {
            this.txtChargeAfter.Enabled = this.txtChargeBefore.Enabled = this.chkCHARGE_ON.Checked;
        }  
    }
}