﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogBankTran
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogBankTran));
            this.lblREF_NO = new System.Windows.Forms.Label();
            this.txtRefNo = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.label2 = new System.Windows.Forms.Label();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.lblAMOUNT = new System.Windows.Forms.Label();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.lblNOTE = new System.Windows.Forms.Label();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lblDATE = new System.Windows.Forms.Label();
            this.txtTransDate = new System.Windows.Forms.DateTimePicker();
            this.lblCURRENCY = new System.Windows.Forms.Label();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.lblEXCHANGE_RATE = new System.Windows.Forms.GroupBox();
            this.rDivisor = new System.Windows.Forms.RadioButton();
            this.rMultiplier = new System.Windows.Forms.RadioButton();
            this.lblEXCHANGE_RATE_1 = new System.Windows.Forms.Label();
            this.txtExchangeRate = new System.Windows.Forms.TextBox();
            this.lblAPPLY_PAYMENT = new System.Windows.Forms.Label();
            this.cboBalanceCurrency = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.lblEXCHANGE_RATE.SuspendLayout();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.label3);
            this.content.Controls.Add(this.cboBalanceCurrency);
            this.content.Controls.Add(this.lblAPPLY_PAYMENT);
            this.content.Controls.Add(this.lblEXCHANGE_RATE);
            this.content.Controls.Add(this.label13);
            this.content.Controls.Add(this.txtTransDate);
            this.content.Controls.Add(this.lblDATE);
            this.content.Controls.Add(this.lblCURRENCY);
            this.content.Controls.Add(this.lblNOTE);
            this.content.Controls.Add(this.label8);
            this.content.Controls.Add(this.txtNote);
            this.content.Controls.Add(this.txtAmount);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.cboCurrency);
            this.content.Controls.Add(this.label2);
            this.content.Controls.Add(this.lblAMOUNT);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.txtRefNo);
            this.content.Controls.Add(this.lblREF_NO);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblREF_NO, 0);
            this.content.Controls.SetChildIndex(this.txtRefNo, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.lblAMOUNT, 0);
            this.content.Controls.SetChildIndex(this.label2, 0);
            this.content.Controls.SetChildIndex(this.cboCurrency, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.txtAmount, 0);
            this.content.Controls.SetChildIndex(this.txtNote, 0);
            this.content.Controls.SetChildIndex(this.label8, 0);
            this.content.Controls.SetChildIndex(this.lblNOTE, 0);
            this.content.Controls.SetChildIndex(this.lblCURRENCY, 0);
            this.content.Controls.SetChildIndex(this.lblDATE, 0);
            this.content.Controls.SetChildIndex(this.txtTransDate, 0);
            this.content.Controls.SetChildIndex(this.label13, 0);
            this.content.Controls.SetChildIndex(this.lblEXCHANGE_RATE, 0);
            this.content.Controls.SetChildIndex(this.lblAPPLY_PAYMENT, 0);
            this.content.Controls.SetChildIndex(this.cboBalanceCurrency, 0);
            this.content.Controls.SetChildIndex(this.label3, 0);
            // 
            // lblREF_NO
            // 
            resources.ApplyResources(this.lblREF_NO, "lblREF_NO");
            this.lblREF_NO.Name = "lblREF_NO";
            // 
            // txtRefNo
            // 
            resources.ApplyResources(this.txtRefNo, "txtRefNo");
            this.txtRefNo.Name = "txtRefNo";
            this.txtRefNo.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Name = "label2";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // lblAMOUNT
            // 
            resources.ApplyResources(this.lblAMOUNT, "lblAMOUNT");
            this.lblAMOUNT.Name = "lblAMOUNT";
            // 
            // txtAmount
            // 
            resources.ApplyResources(this.txtAmount, "txtAmount");
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.TextChanged += new System.EventHandler(this.txtAmount_TextChanged);
            this.txtAmount.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // lblNOTE
            // 
            resources.ApplyResources(this.lblNOTE, "lblNOTE");
            this.lblNOTE.Name = "lblNOTE";
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.SelectedIndexChanged += new System.EventHandler(this.cboCurrency_SelectedIndexChanged);
            this.cboCurrency.Enter += new System.EventHandler(this.InputKhmer);
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Name = "label8";
            // 
            // lblDATE
            // 
            resources.ApplyResources(this.lblDATE, "lblDATE");
            this.lblDATE.Name = "lblDATE";
            // 
            // txtTransDate
            // 
            resources.ApplyResources(this.txtTransDate, "txtTransDate");
            this.txtTransDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtTransDate.Name = "txtTransDate";
            this.txtTransDate.ValueChanged += new System.EventHandler(this.txtTransDate_ValueChanged);
            this.txtTransDate.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // lblCURRENCY
            // 
            resources.ApplyResources(this.lblCURRENCY, "lblCURRENCY");
            this.lblCURRENCY.Name = "lblCURRENCY";
            // 
            // txtNote
            // 
            resources.ApplyResources(this.txtNote, "txtNote");
            this.txtNote.Name = "txtNote";
            this.txtNote.Enter += new System.EventHandler(this.InputKhmer);
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Name = "label13";
            // 
            // lblEXCHANGE_RATE
            // 
            this.lblEXCHANGE_RATE.Controls.Add(this.rDivisor);
            this.lblEXCHANGE_RATE.Controls.Add(this.rMultiplier);
            this.lblEXCHANGE_RATE.Controls.Add(this.lblEXCHANGE_RATE_1);
            this.lblEXCHANGE_RATE.Controls.Add(this.txtExchangeRate);
            resources.ApplyResources(this.lblEXCHANGE_RATE, "lblEXCHANGE_RATE");
            this.lblEXCHANGE_RATE.Name = "lblEXCHANGE_RATE";
            this.lblEXCHANGE_RATE.TabStop = false;
            // 
            // rDivisor
            // 
            resources.ApplyResources(this.rDivisor, "rDivisor");
            this.rDivisor.Name = "rDivisor";
            this.rDivisor.UseVisualStyleBackColor = true;
            // 
            // rMultiplier
            // 
            resources.ApplyResources(this.rMultiplier, "rMultiplier");
            this.rMultiplier.Checked = true;
            this.rMultiplier.Name = "rMultiplier";
            this.rMultiplier.TabStop = true;
            this.rMultiplier.UseVisualStyleBackColor = true;
            // 
            // lblEXCHANGE_RATE_1
            // 
            resources.ApplyResources(this.lblEXCHANGE_RATE_1, "lblEXCHANGE_RATE_1");
            this.lblEXCHANGE_RATE_1.Name = "lblEXCHANGE_RATE_1";
            // 
            // txtExchangeRate
            // 
            this.txtExchangeRate.AcceptsTab = true;
            resources.ApplyResources(this.txtExchangeRate, "txtExchangeRate");
            this.txtExchangeRate.Name = "txtExchangeRate";
            // 
            // lblAPPLY_PAYMENT
            // 
            resources.ApplyResources(this.lblAPPLY_PAYMENT, "lblAPPLY_PAYMENT");
            this.lblAPPLY_PAYMENT.Name = "lblAPPLY_PAYMENT";
            // 
            // cboBalanceCurrency
            // 
            this.cboBalanceCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBalanceCurrency.FormattingEnabled = true;
            resources.ApplyResources(this.cboBalanceCurrency, "cboBalanceCurrency");
            this.cboBalanceCurrency.Name = "cboBalanceCurrency";
            this.cboBalanceCurrency.SelectedIndexChanged += new System.EventHandler(this.cboCurrency_SelectedIndexChanged);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Name = "label3";
            // 
            // DialogBankTran
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogBankTran";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.lblEXCHANGE_RATE.ResumeLayout(false);
            this.lblEXCHANGE_RATE.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBox txtRefNo;
        private Label lblREF_NO;
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label label2;
        private ExButton btnCHANGE_LOG;
        private Label lblNOTE;
        private TextBox txtAmount;
        private Label lblAMOUNT;
        private ComboBox cboCurrency;
        private Label label8;
        private DateTimePicker txtTransDate;
        private Label lblDATE;
        private Label label13;
        private Label lblCURRENCY;
        private TextBox txtNote;
        private Label label3;
        private ComboBox cboBalanceCurrency;
        private Label lblAPPLY_PAYMENT;
        private GroupBox lblEXCHANGE_RATE;
        private RadioButton rDivisor;
        private RadioButton rMultiplier;
        private Label lblEXCHANGE_RATE_1;
        private TextBox txtExchangeRate;
    }
}