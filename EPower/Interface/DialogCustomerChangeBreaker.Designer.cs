﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogCustomerChangeBreaker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCONSTANT = new System.Windows.Forms.Label();
            this.txtOLD_BREAKER_CONSTANT = new System.Windows.Forms.TextBox();
            this.txtOLD_BREAKER_TYPE = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.lblAMPARE = new System.Windows.Forms.Label();
            this.lblVOLTAGE = new System.Windows.Forms.Label();
            this.lblPHASE = new System.Windows.Forms.Label();
            this.txtOLD_BREAKER_AMPHERE = new System.Windows.Forms.TextBox();
            this.txtOLD_BREAKER_VOLTAGE = new System.Windows.Forms.TextBox();
            this.txtOLD_BREAKER_PHASE = new System.Windows.Forms.TextBox();
            this.lblBREAKER_CODE = new System.Windows.Forms.Label();
            this.lblOLD_BREAKER = new System.Windows.Forms.Label();
            this.lblBREAKER_TYPE = new System.Windows.Forms.Label();
            this.txtOLD_BREAKER = new SoftTech.Component.ExTextbox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblNEW_BREAKER = new System.Windows.Forms.Label();
            this.lblCUSTOMER_INFORMATION = new System.Windows.Forms.Label();
            this.txtCUSTOMER_NAME = new System.Windows.Forms.TextBox();
            this.lblCUSTOMER_NAME = new System.Windows.Forms.Label();
            this.txtCUSTOMER_CODE = new System.Windows.Forms.TextBox();
            this.lblCUSTOMER_CODE = new System.Windows.Forms.Label();
            this.lblBREAKER_STATUS = new System.Windows.Forms.Label();
            this.cboOLD_BREAKER_STATUS = new System.Windows.Forms.ComboBox();
            this.btnClose = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblCONSTANT_1 = new System.Windows.Forms.Label();
            this.txtNEW_BREAKER_CONSTANT = new System.Windows.Forms.TextBox();
            this.txtNEW_BREAKER_TYPE = new System.Windows.Forms.TextBox();
            this.lblAMPARE_1 = new System.Windows.Forms.Label();
            this.lblVOLTAGE_1 = new System.Windows.Forms.Label();
            this.lblPHASE_1 = new System.Windows.Forms.Label();
            this.txtNEW_BREAKER_AMPARE = new System.Windows.Forms.TextBox();
            this.txtNEW_BREAKER_VOLTAGE = new System.Windows.Forms.TextBox();
            this.txtNEW_METER_PHASE = new System.Windows.Forms.TextBox();
            this.lblBREAKER_CODE_1 = new System.Windows.Forms.Label();
            this.lblBREAKER_TYPE_1 = new System.Windows.Forms.Label();
            this.txtNEW_BREAKER = new SoftTech.Component.ExTextbox();
            this.label25 = new System.Windows.Forms.Label();
            this.lblCHANGE_DATE = new System.Windows.Forms.Label();
            this.dtpCHANGE_DATE = new System.Windows.Forms.DateTimePicker();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.lblCONSTANT_1);
            this.content.Controls.Add(this.txtNEW_BREAKER_CONSTANT);
            this.content.Controls.Add(this.dtpCHANGE_DATE);
            this.content.Controls.Add(this.txtNEW_BREAKER_TYPE);
            this.content.Controls.Add(this.lblCHANGE_DATE);
            this.content.Controls.Add(this.lblAMPARE_1);
            this.content.Controls.Add(this.lblVOLTAGE_1);
            this.content.Controls.Add(this.lblPHASE_1);
            this.content.Controls.Add(this.txtNEW_BREAKER_AMPARE);
            this.content.Controls.Add(this.txtNEW_BREAKER_VOLTAGE);
            this.content.Controls.Add(this.txtNEW_METER_PHASE);
            this.content.Controls.Add(this.lblBREAKER_CODE_1);
            this.content.Controls.Add(this.lblBREAKER_TYPE_1);
            this.content.Controls.Add(this.txtNEW_BREAKER);
            this.content.Controls.Add(this.btnClose);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.txtCUSTOMER_CODE);
            this.content.Controls.Add(this.lblCUSTOMER_CODE);
            this.content.Controls.Add(this.cboOLD_BREAKER_STATUS);
            this.content.Controls.Add(this.txtCUSTOMER_NAME);
            this.content.Controls.Add(this.lblBREAKER_STATUS);
            this.content.Controls.Add(this.lblCUSTOMER_NAME);
            this.content.Controls.Add(this.lblCUSTOMER_INFORMATION);
            this.content.Controls.Add(this.lblNEW_BREAKER);
            this.content.Controls.Add(this.lblCONSTANT);
            this.content.Controls.Add(this.txtOLD_BREAKER_CONSTANT);
            this.content.Controls.Add(this.txtOLD_BREAKER_TYPE);
            this.content.Controls.Add(this.lblAMPARE);
            this.content.Controls.Add(this.lblVOLTAGE);
            this.content.Controls.Add(this.lblPHASE);
            this.content.Controls.Add(this.txtOLD_BREAKER_AMPHERE);
            this.content.Controls.Add(this.txtOLD_BREAKER_VOLTAGE);
            this.content.Controls.Add(this.txtOLD_BREAKER_PHASE);
            this.content.Controls.Add(this.lblBREAKER_CODE);
            this.content.Controls.Add(this.lblOLD_BREAKER);
            this.content.Controls.Add(this.lblBREAKER_TYPE);
            this.content.Controls.Add(this.txtOLD_BREAKER);
            this.content.Controls.Add(this.label3);
            this.content.Controls.Add(this.label25);
            this.content.Controls.Add(this.label14);
            this.content.Size = new System.Drawing.Size(780, 577);
            this.content.TabIndex = 0;
            this.content.Text = "\'CONTENT\'";
            this.content.Controls.SetChildIndex(this.label14, 0);
            this.content.Controls.SetChildIndex(this.label25, 0);
            this.content.Controls.SetChildIndex(this.label3, 0);
            this.content.Controls.SetChildIndex(this.txtOLD_BREAKER, 0);
            this.content.Controls.SetChildIndex(this.lblBREAKER_TYPE, 0);
            this.content.Controls.SetChildIndex(this.lblOLD_BREAKER, 0);
            this.content.Controls.SetChildIndex(this.lblBREAKER_CODE, 0);
            this.content.Controls.SetChildIndex(this.txtOLD_BREAKER_PHASE, 0);
            this.content.Controls.SetChildIndex(this.txtOLD_BREAKER_VOLTAGE, 0);
            this.content.Controls.SetChildIndex(this.txtOLD_BREAKER_AMPHERE, 0);
            this.content.Controls.SetChildIndex(this.lblPHASE, 0);
            this.content.Controls.SetChildIndex(this.lblVOLTAGE, 0);
            this.content.Controls.SetChildIndex(this.lblAMPARE, 0);
            this.content.Controls.SetChildIndex(this.txtOLD_BREAKER_TYPE, 0);
            this.content.Controls.SetChildIndex(this.txtOLD_BREAKER_CONSTANT, 0);
            this.content.Controls.SetChildIndex(this.lblCONSTANT, 0);
            this.content.Controls.SetChildIndex(this.lblNEW_BREAKER, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_INFORMATION, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblBREAKER_STATUS, 0);
            this.content.Controls.SetChildIndex(this.txtCUSTOMER_NAME, 0);
            this.content.Controls.SetChildIndex(this.cboOLD_BREAKER_STATUS, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_CODE, 0);
            this.content.Controls.SetChildIndex(this.txtCUSTOMER_CODE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnClose, 0);
            this.content.Controls.SetChildIndex(this.txtNEW_BREAKER, 0);
            this.content.Controls.SetChildIndex(this.lblBREAKER_TYPE_1, 0);
            this.content.Controls.SetChildIndex(this.lblBREAKER_CODE_1, 0);
            this.content.Controls.SetChildIndex(this.txtNEW_METER_PHASE, 0);
            this.content.Controls.SetChildIndex(this.txtNEW_BREAKER_VOLTAGE, 0);
            this.content.Controls.SetChildIndex(this.txtNEW_BREAKER_AMPARE, 0);
            this.content.Controls.SetChildIndex(this.lblPHASE_1, 0);
            this.content.Controls.SetChildIndex(this.lblVOLTAGE_1, 0);
            this.content.Controls.SetChildIndex(this.lblAMPARE_1, 0);
            this.content.Controls.SetChildIndex(this.lblCHANGE_DATE, 0);
            this.content.Controls.SetChildIndex(this.txtNEW_BREAKER_TYPE, 0);
            this.content.Controls.SetChildIndex(this.dtpCHANGE_DATE, 0);
            this.content.Controls.SetChildIndex(this.txtNEW_BREAKER_CONSTANT, 0);
            this.content.Controls.SetChildIndex(this.lblCONSTANT_1, 0);
            // 
            // lblCONSTANT
            // 
            this.lblCONSTANT.AutoSize = true;
            this.lblCONSTANT.Location = new System.Drawing.Point(434, 186);
            this.lblCONSTANT.Margin = new System.Windows.Forms.Padding(2);
            this.lblCONSTANT.Name = "lblCONSTANT";
            this.lblCONSTANT.Size = new System.Drawing.Size(45, 19);
            this.lblCONSTANT.TabIndex = 312;
            this.lblCONSTANT.Text = "កុងស្តង់";
            // 
            // txtOLD_BREAKER_CONSTANT
            // 
            this.txtOLD_BREAKER_CONSTANT.Location = new System.Drawing.Point(558, 182);
            this.txtOLD_BREAKER_CONSTANT.Margin = new System.Windows.Forms.Padding(2);
            this.txtOLD_BREAKER_CONSTANT.Name = "txtOLD_BREAKER_CONSTANT";
            this.txtOLD_BREAKER_CONSTANT.ReadOnly = true;
            this.txtOLD_BREAKER_CONSTANT.Size = new System.Drawing.Size(199, 27);
            this.txtOLD_BREAKER_CONSTANT.TabIndex = 8;
            this.txtOLD_BREAKER_CONSTANT.TabStop = false;
            // 
            // txtOLD_BREAKER_TYPE
            // 
            this.txtOLD_BREAKER_TYPE.Location = new System.Drawing.Point(558, 121);
            this.txtOLD_BREAKER_TYPE.Margin = new System.Windows.Forms.Padding(2);
            this.txtOLD_BREAKER_TYPE.Name = "txtOLD_BREAKER_TYPE";
            this.txtOLD_BREAKER_TYPE.ReadOnly = true;
            this.txtOLD_BREAKER_TYPE.Size = new System.Drawing.Size(199, 27);
            this.txtOLD_BREAKER_TYPE.TabIndex = 6;
            this.txtOLD_BREAKER_TYPE.TabStop = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(143, 124);
            this.label14.Margin = new System.Windows.Forms.Padding(2);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(15, 20);
            this.label14.TabIndex = 309;
            this.label14.Text = "*";
            // 
            // lblAMPARE
            // 
            this.lblAMPARE.AutoSize = true;
            this.lblAMPARE.Location = new System.Drawing.Point(17, 186);
            this.lblAMPARE.Margin = new System.Windows.Forms.Padding(2);
            this.lblAMPARE.Name = "lblAMPARE";
            this.lblAMPARE.Size = new System.Drawing.Size(69, 19);
            this.lblAMPARE.TabIndex = 298;
            this.lblAMPARE.Text = "អាំងតង់ស៊ីតេ";
            // 
            // lblVOLTAGE
            // 
            this.lblVOLTAGE.AutoSize = true;
            this.lblVOLTAGE.Location = new System.Drawing.Point(434, 155);
            this.lblVOLTAGE.Margin = new System.Windows.Forms.Padding(2);
            this.lblVOLTAGE.Name = "lblVOLTAGE";
            this.lblVOLTAGE.Size = new System.Drawing.Size(49, 19);
            this.lblVOLTAGE.TabIndex = 297;
            this.lblVOLTAGE.Text = "តុងស្យុង";
            // 
            // lblPHASE
            // 
            this.lblPHASE.AutoSize = true;
            this.lblPHASE.Location = new System.Drawing.Point(17, 155);
            this.lblPHASE.Margin = new System.Windows.Forms.Padding(2);
            this.lblPHASE.Name = "lblPHASE";
            this.lblPHASE.Size = new System.Drawing.Size(50, 19);
            this.lblPHASE.TabIndex = 296;
            this.lblPHASE.Text = "ចំនួនហ្វា";
            // 
            // txtOLD_BREAKER_AMPHERE
            // 
            this.txtOLD_BREAKER_AMPHERE.Location = new System.Drawing.Point(160, 182);
            this.txtOLD_BREAKER_AMPHERE.Margin = new System.Windows.Forms.Padding(2);
            this.txtOLD_BREAKER_AMPHERE.Name = "txtOLD_BREAKER_AMPHERE";
            this.txtOLD_BREAKER_AMPHERE.ReadOnly = true;
            this.txtOLD_BREAKER_AMPHERE.Size = new System.Drawing.Size(199, 27);
            this.txtOLD_BREAKER_AMPHERE.TabIndex = 4;
            this.txtOLD_BREAKER_AMPHERE.TabStop = false;
            // 
            // txtOLD_BREAKER_VOLTAGE
            // 
            this.txtOLD_BREAKER_VOLTAGE.Location = new System.Drawing.Point(558, 151);
            this.txtOLD_BREAKER_VOLTAGE.Margin = new System.Windows.Forms.Padding(2);
            this.txtOLD_BREAKER_VOLTAGE.Name = "txtOLD_BREAKER_VOLTAGE";
            this.txtOLD_BREAKER_VOLTAGE.ReadOnly = true;
            this.txtOLD_BREAKER_VOLTAGE.Size = new System.Drawing.Size(199, 27);
            this.txtOLD_BREAKER_VOLTAGE.TabIndex = 7;
            this.txtOLD_BREAKER_VOLTAGE.TabStop = false;
            // 
            // txtOLD_BREAKER_PHASE
            // 
            this.txtOLD_BREAKER_PHASE.Location = new System.Drawing.Point(160, 151);
            this.txtOLD_BREAKER_PHASE.Margin = new System.Windows.Forms.Padding(2);
            this.txtOLD_BREAKER_PHASE.Name = "txtOLD_BREAKER_PHASE";
            this.txtOLD_BREAKER_PHASE.ReadOnly = true;
            this.txtOLD_BREAKER_PHASE.Size = new System.Drawing.Size(199, 27);
            this.txtOLD_BREAKER_PHASE.TabIndex = 3;
            this.txtOLD_BREAKER_PHASE.TabStop = false;
            // 
            // lblBREAKER_CODE
            // 
            this.lblBREAKER_CODE.AutoSize = true;
            this.lblBREAKER_CODE.Location = new System.Drawing.Point(17, 125);
            this.lblBREAKER_CODE.Margin = new System.Windows.Forms.Padding(2);
            this.lblBREAKER_CODE.Name = "lblBREAKER_CODE";
            this.lblBREAKER_CODE.Size = new System.Drawing.Size(55, 19);
            this.lblBREAKER_CODE.TabIndex = 290;
            this.lblBREAKER_CODE.Text = "លេខកូដ  ";
            // 
            // lblOLD_BREAKER
            // 
            this.lblOLD_BREAKER.AutoSize = true;
            this.lblOLD_BREAKER.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblOLD_BREAKER.Location = new System.Drawing.Point(16, 92);
            this.lblOLD_BREAKER.Margin = new System.Windows.Forms.Padding(2);
            this.lblOLD_BREAKER.Name = "lblOLD_BREAKER";
            this.lblOLD_BREAKER.Size = new System.Drawing.Size(120, 19);
            this.lblOLD_BREAKER.TabIndex = 289;
            this.lblOLD_BREAKER.Text = "ពត៌មានឌីសុងទរ័ចាស់";
            // 
            // lblBREAKER_TYPE
            // 
            this.lblBREAKER_TYPE.AutoSize = true;
            this.lblBREAKER_TYPE.Location = new System.Drawing.Point(434, 125);
            this.lblBREAKER_TYPE.Margin = new System.Windows.Forms.Padding(2);
            this.lblBREAKER_TYPE.Name = "lblBREAKER_TYPE";
            this.lblBREAKER_TYPE.Size = new System.Drawing.Size(69, 19);
            this.lblBREAKER_TYPE.TabIndex = 288;
            this.lblBREAKER_TYPE.Text = "ប្រភេទកុងទ័រ";
            // 
            // txtOLD_BREAKER
            // 
            this.txtOLD_BREAKER.BackColor = System.Drawing.Color.White;
            this.txtOLD_BREAKER.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOLD_BREAKER.Enabled = false;
            this.txtOLD_BREAKER.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.txtOLD_BREAKER.Location = new System.Drawing.Point(160, 121);
            this.txtOLD_BREAKER.Margin = new System.Windows.Forms.Padding(2);
            this.txtOLD_BREAKER.Name = "txtOLD_BREAKER";
            this.txtOLD_BREAKER.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtOLD_BREAKER.Size = new System.Drawing.Size(199, 27);
            this.txtOLD_BREAKER.TabIndex = 2;
            this.txtOLD_BREAKER.AdvanceSearch += new System.EventHandler(this.txtMeter_AdvanceSearch);
            this.txtOLD_BREAKER.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(143, 305);
            this.label3.Margin = new System.Windows.Forms.Padding(2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 20);
            this.label3.TabIndex = 335;
            this.label3.Text = "*";
            // 
            // lblNEW_BREAKER
            // 
            this.lblNEW_BREAKER.AutoSize = true;
            this.lblNEW_BREAKER.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblNEW_BREAKER.Location = new System.Drawing.Point(16, 273);
            this.lblNEW_BREAKER.Margin = new System.Windows.Forms.Padding(2);
            this.lblNEW_BREAKER.Name = "lblNEW_BREAKER";
            this.lblNEW_BREAKER.Size = new System.Drawing.Size(103, 19);
            this.lblNEW_BREAKER.TabIndex = 315;
            this.lblNEW_BREAKER.Text = "ពត៌មានឌីសុងទរ័ថ្មី";
            // 
            // lblCUSTOMER_INFORMATION
            // 
            this.lblCUSTOMER_INFORMATION.AutoSize = true;
            this.lblCUSTOMER_INFORMATION.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblCUSTOMER_INFORMATION.Location = new System.Drawing.Point(16, 18);
            this.lblCUSTOMER_INFORMATION.Margin = new System.Windows.Forms.Padding(2);
            this.lblCUSTOMER_INFORMATION.Name = "lblCUSTOMER_INFORMATION";
            this.lblCUSTOMER_INFORMATION.Size = new System.Drawing.Size(94, 19);
            this.lblCUSTOMER_INFORMATION.TabIndex = 340;
            this.lblCUSTOMER_INFORMATION.Text = "ពត៌មានអតិថិជន";
            // 
            // txtCUSTOMER_NAME
            // 
            this.txtCUSTOMER_NAME.Location = new System.Drawing.Point(558, 40);
            this.txtCUSTOMER_NAME.Margin = new System.Windows.Forms.Padding(2);
            this.txtCUSTOMER_NAME.Name = "txtCUSTOMER_NAME";
            this.txtCUSTOMER_NAME.ReadOnly = true;
            this.txtCUSTOMER_NAME.Size = new System.Drawing.Size(199, 27);
            this.txtCUSTOMER_NAME.TabIndex = 1;
            this.txtCUSTOMER_NAME.TabStop = false;
            // 
            // lblCUSTOMER_NAME
            // 
            this.lblCUSTOMER_NAME.AutoSize = true;
            this.lblCUSTOMER_NAME.Location = new System.Drawing.Point(434, 44);
            this.lblCUSTOMER_NAME.Margin = new System.Windows.Forms.Padding(2);
            this.lblCUSTOMER_NAME.Name = "lblCUSTOMER_NAME";
            this.lblCUSTOMER_NAME.Size = new System.Drawing.Size(80, 19);
            this.lblCUSTOMER_NAME.TabIndex = 341;
            this.lblCUSTOMER_NAME.Text = "ឈ្មោះអតិថិជន";
            // 
            // txtCUSTOMER_CODE
            // 
            this.txtCUSTOMER_CODE.Location = new System.Drawing.Point(160, 40);
            this.txtCUSTOMER_CODE.Margin = new System.Windows.Forms.Padding(2);
            this.txtCUSTOMER_CODE.Name = "txtCUSTOMER_CODE";
            this.txtCUSTOMER_CODE.ReadOnly = true;
            this.txtCUSTOMER_CODE.Size = new System.Drawing.Size(199, 27);
            this.txtCUSTOMER_CODE.TabIndex = 0;
            this.txtCUSTOMER_CODE.TabStop = false;
            // 
            // lblCUSTOMER_CODE
            // 
            this.lblCUSTOMER_CODE.AutoSize = true;
            this.lblCUSTOMER_CODE.Location = new System.Drawing.Point(17, 44);
            this.lblCUSTOMER_CODE.Margin = new System.Windows.Forms.Padding(2);
            this.lblCUSTOMER_CODE.Name = "lblCUSTOMER_CODE";
            this.lblCUSTOMER_CODE.Size = new System.Drawing.Size(49, 19);
            this.lblCUSTOMER_CODE.TabIndex = 344;
            this.lblCUSTOMER_CODE.Text = "លេខកូដ";
            // 
            // lblBREAKER_STATUS
            // 
            this.lblBREAKER_STATUS.AutoSize = true;
            this.lblBREAKER_STATUS.Location = new System.Drawing.Point(17, 217);
            this.lblBREAKER_STATUS.Margin = new System.Windows.Forms.Padding(2);
            this.lblBREAKER_STATUS.Name = "lblBREAKER_STATUS";
            this.lblBREAKER_STATUS.Size = new System.Drawing.Size(54, 19);
            this.lblBREAKER_STATUS.TabIndex = 347;
            this.lblBREAKER_STATUS.Text = "ស្ថានភាព";
            // 
            // cboOLD_BREAKER_STATUS
            // 
            this.cboOLD_BREAKER_STATUS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboOLD_BREAKER_STATUS.FormattingEnabled = true;
            this.cboOLD_BREAKER_STATUS.Location = new System.Drawing.Point(160, 213);
            this.cboOLD_BREAKER_STATUS.Margin = new System.Windows.Forms.Padding(2);
            this.cboOLD_BREAKER_STATUS.Name = "cboOLD_BREAKER_STATUS";
            this.cboOLD_BREAKER_STATUS.Size = new System.Drawing.Size(199, 27);
            this.cboOLD_BREAKER_STATUS.TabIndex = 5;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnClose.Location = new System.Drawing.Point(700, 548);
            this.btnClose.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 17;
            this.btnClose.Text = "បិទ";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnOK.Location = new System.Drawing.Point(619, 548);
            this.btnOK.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 16;
            this.btnOK.Text = "យល់ព្រម";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Location = new System.Drawing.Point(1, 542);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(780, 1);
            this.panel1.TabIndex = 356;
            // 
            // lblCONSTANT_1
            // 
            this.lblCONSTANT_1.AutoSize = true;
            this.lblCONSTANT_1.Location = new System.Drawing.Point(434, 367);
            this.lblCONSTANT_1.Margin = new System.Windows.Forms.Padding(2);
            this.lblCONSTANT_1.Name = "lblCONSTANT_1";
            this.lblCONSTANT_1.Size = new System.Drawing.Size(45, 19);
            this.lblCONSTANT_1.TabIndex = 382;
            this.lblCONSTANT_1.Text = "កុងស្តង់";
            // 
            // txtNEW_BREAKER_CONSTANT
            // 
            this.txtNEW_BREAKER_CONSTANT.Location = new System.Drawing.Point(558, 363);
            this.txtNEW_BREAKER_CONSTANT.Margin = new System.Windows.Forms.Padding(2);
            this.txtNEW_BREAKER_CONSTANT.Name = "txtNEW_BREAKER_CONSTANT";
            this.txtNEW_BREAKER_CONSTANT.ReadOnly = true;
            this.txtNEW_BREAKER_CONSTANT.Size = new System.Drawing.Size(199, 27);
            this.txtNEW_BREAKER_CONSTANT.TabIndex = 15;
            this.txtNEW_BREAKER_CONSTANT.TabStop = false;
            // 
            // txtNEW_BREAKER_TYPE
            // 
            this.txtNEW_BREAKER_TYPE.Location = new System.Drawing.Point(558, 302);
            this.txtNEW_BREAKER_TYPE.Margin = new System.Windows.Forms.Padding(2);
            this.txtNEW_BREAKER_TYPE.Name = "txtNEW_BREAKER_TYPE";
            this.txtNEW_BREAKER_TYPE.ReadOnly = true;
            this.txtNEW_BREAKER_TYPE.Size = new System.Drawing.Size(199, 27);
            this.txtNEW_BREAKER_TYPE.TabIndex = 13;
            this.txtNEW_BREAKER_TYPE.TabStop = false;
            // 
            // lblAMPARE_1
            // 
            this.lblAMPARE_1.AutoSize = true;
            this.lblAMPARE_1.Location = new System.Drawing.Point(17, 367);
            this.lblAMPARE_1.Margin = new System.Windows.Forms.Padding(2);
            this.lblAMPARE_1.Name = "lblAMPARE_1";
            this.lblAMPARE_1.Size = new System.Drawing.Size(69, 19);
            this.lblAMPARE_1.TabIndex = 368;
            this.lblAMPARE_1.Text = "អាំងតង់ស៊ីតេ";
            // 
            // lblVOLTAGE_1
            // 
            this.lblVOLTAGE_1.AutoSize = true;
            this.lblVOLTAGE_1.Location = new System.Drawing.Point(434, 336);
            this.lblVOLTAGE_1.Margin = new System.Windows.Forms.Padding(2);
            this.lblVOLTAGE_1.Name = "lblVOLTAGE_1";
            this.lblVOLTAGE_1.Size = new System.Drawing.Size(49, 19);
            this.lblVOLTAGE_1.TabIndex = 367;
            this.lblVOLTAGE_1.Text = "តុងស្យុង";
            // 
            // lblPHASE_1
            // 
            this.lblPHASE_1.AutoSize = true;
            this.lblPHASE_1.Location = new System.Drawing.Point(17, 336);
            this.lblPHASE_1.Margin = new System.Windows.Forms.Padding(2);
            this.lblPHASE_1.Name = "lblPHASE_1";
            this.lblPHASE_1.Size = new System.Drawing.Size(50, 19);
            this.lblPHASE_1.TabIndex = 366;
            this.lblPHASE_1.Text = "ចំនួនហ្វា";
            // 
            // txtNEW_BREAKER_AMPARE
            // 
            this.txtNEW_BREAKER_AMPARE.Location = new System.Drawing.Point(160, 363);
            this.txtNEW_BREAKER_AMPARE.Margin = new System.Windows.Forms.Padding(2);
            this.txtNEW_BREAKER_AMPARE.Name = "txtNEW_BREAKER_AMPARE";
            this.txtNEW_BREAKER_AMPARE.ReadOnly = true;
            this.txtNEW_BREAKER_AMPARE.Size = new System.Drawing.Size(199, 27);
            this.txtNEW_BREAKER_AMPARE.TabIndex = 12;
            this.txtNEW_BREAKER_AMPARE.TabStop = false;
            // 
            // txtNEW_BREAKER_VOLTAGE
            // 
            this.txtNEW_BREAKER_VOLTAGE.Location = new System.Drawing.Point(558, 332);
            this.txtNEW_BREAKER_VOLTAGE.Margin = new System.Windows.Forms.Padding(2);
            this.txtNEW_BREAKER_VOLTAGE.Name = "txtNEW_BREAKER_VOLTAGE";
            this.txtNEW_BREAKER_VOLTAGE.ReadOnly = true;
            this.txtNEW_BREAKER_VOLTAGE.Size = new System.Drawing.Size(199, 27);
            this.txtNEW_BREAKER_VOLTAGE.TabIndex = 14;
            this.txtNEW_BREAKER_VOLTAGE.TabStop = false;
            // 
            // txtNEW_METER_PHASE
            // 
            this.txtNEW_METER_PHASE.Location = new System.Drawing.Point(160, 332);
            this.txtNEW_METER_PHASE.Margin = new System.Windows.Forms.Padding(2);
            this.txtNEW_METER_PHASE.Name = "txtNEW_METER_PHASE";
            this.txtNEW_METER_PHASE.ReadOnly = true;
            this.txtNEW_METER_PHASE.Size = new System.Drawing.Size(199, 27);
            this.txtNEW_METER_PHASE.TabIndex = 11;
            this.txtNEW_METER_PHASE.TabStop = false;
            // 
            // lblBREAKER_CODE_1
            // 
            this.lblBREAKER_CODE_1.AutoSize = true;
            this.lblBREAKER_CODE_1.Location = new System.Drawing.Point(17, 306);
            this.lblBREAKER_CODE_1.Margin = new System.Windows.Forms.Padding(2);
            this.lblBREAKER_CODE_1.Name = "lblBREAKER_CODE_1";
            this.lblBREAKER_CODE_1.Size = new System.Drawing.Size(49, 19);
            this.lblBREAKER_CODE_1.TabIndex = 360;
            this.lblBREAKER_CODE_1.Text = "លេខកូដ";
            // 
            // lblBREAKER_TYPE_1
            // 
            this.lblBREAKER_TYPE_1.AutoSize = true;
            this.lblBREAKER_TYPE_1.Location = new System.Drawing.Point(434, 306);
            this.lblBREAKER_TYPE_1.Margin = new System.Windows.Forms.Padding(2);
            this.lblBREAKER_TYPE_1.Name = "lblBREAKER_TYPE_1";
            this.lblBREAKER_TYPE_1.Size = new System.Drawing.Size(69, 19);
            this.lblBREAKER_TYPE_1.TabIndex = 359;
            this.lblBREAKER_TYPE_1.Text = "ប្រភេទកុងទ័រ";
            // 
            // txtNEW_BREAKER
            // 
            this.txtNEW_BREAKER.BackColor = System.Drawing.Color.White;
            this.txtNEW_BREAKER.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNEW_BREAKER.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.txtNEW_BREAKER.Location = new System.Drawing.Point(160, 302);
            this.txtNEW_BREAKER.Margin = new System.Windows.Forms.Padding(2);
            this.txtNEW_BREAKER.Name = "txtNEW_BREAKER";
            this.txtNEW_BREAKER.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtNEW_BREAKER.Size = new System.Drawing.Size(199, 27);
            this.txtNEW_BREAKER.TabIndex = 10;
            this.txtNEW_BREAKER.AdvanceSearch += new System.EventHandler(this.txtNEW_METER_AdvanceSearch);
            this.txtNEW_BREAKER.CancelAdvanceSearch += new System.EventHandler(this.txtNEW_METER_CancelAdvanceSearch);
            this.txtNEW_BREAKER.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label25.ForeColor = System.Drawing.Color.Red;
            this.label25.Location = new System.Drawing.Point(143, 216);
            this.label25.Margin = new System.Windows.Forms.Padding(2);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(15, 20);
            this.label25.TabIndex = 396;
            this.label25.Text = "*";
            // 
            // lblCHANGE_DATE
            // 
            this.lblCHANGE_DATE.AutoSize = true;
            this.lblCHANGE_DATE.Location = new System.Drawing.Point(434, 217);
            this.lblCHANGE_DATE.Margin = new System.Windows.Forms.Padding(2);
            this.lblCHANGE_DATE.Name = "lblCHANGE_DATE";
            this.lblCHANGE_DATE.Size = new System.Drawing.Size(45, 19);
            this.lblCHANGE_DATE.TabIndex = 385;
            this.lblCHANGE_DATE.Text = "ថ្ងៃខែប្តូរ";
            // 
            // dtpCHANGE_DATE
            // 
            this.dtpCHANGE_DATE.CustomFormat = "ថ្ងៃdd - ខែMM - ឆ្នាំyyyy";
            this.dtpCHANGE_DATE.Enabled = false;
            this.dtpCHANGE_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpCHANGE_DATE.Location = new System.Drawing.Point(558, 213);
            this.dtpCHANGE_DATE.Margin = new System.Windows.Forms.Padding(2);
            this.dtpCHANGE_DATE.Name = "dtpCHANGE_DATE";
            this.dtpCHANGE_DATE.Size = new System.Drawing.Size(199, 27);
            this.dtpCHANGE_DATE.TabIndex = 9;
            this.dtpCHANGE_DATE.ValueChanged += new System.EventHandler(this.dtpActivateDate_ValueChanged);
            this.dtpCHANGE_DATE.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // DialogCustomerChangeBreaker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(780, 600);
            this.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Name = "DialogCustomerChangeBreaker";
            this.Text = "ប្តូរឌីសុងទ័រ";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBox txtCUSTOMER_CODE;
        private Label lblCUSTOMER_CODE;
        private TextBox txtCUSTOMER_NAME;
        private Label lblCUSTOMER_NAME;
        private Label lblCUSTOMER_INFORMATION;
        private Label label3;
        private Label lblNEW_BREAKER;
        private Label lblCONSTANT;
        private TextBox txtOLD_BREAKER_CONSTANT;
        private TextBox txtOLD_BREAKER_TYPE;
        private Label label14;
        private Label lblAMPARE;
        private Label lblVOLTAGE;
        private Label lblPHASE;
        private TextBox txtOLD_BREAKER_AMPHERE;
        private TextBox txtOLD_BREAKER_VOLTAGE;
        private TextBox txtOLD_BREAKER_PHASE;
        private Label lblBREAKER_CODE;
        private Label lblOLD_BREAKER;
        private Label lblBREAKER_TYPE;
        private ExTextbox txtOLD_BREAKER;
        private Label lblBREAKER_STATUS;
        private ComboBox cboOLD_BREAKER_STATUS;
        private ExButton btnClose;
        private ExButton btnOK;
        private Panel panel1;
        private Label lblCONSTANT_1;
        private TextBox txtNEW_BREAKER_CONSTANT;
        private TextBox txtNEW_BREAKER_TYPE;
        private Label lblAMPARE_1;
        private Label lblVOLTAGE_1;
        private Label lblPHASE_1;
        private TextBox txtNEW_BREAKER_AMPARE;
        private TextBox txtNEW_BREAKER_VOLTAGE;
        private TextBox txtNEW_METER_PHASE;
        private Label lblBREAKER_CODE_1;
        private Label lblBREAKER_TYPE_1;
        private ExTextbox txtNEW_BREAKER;
        private Label label25;
        private DateTimePicker dtpCHANGE_DATE;
        private Label lblCHANGE_DATE;
    }
}