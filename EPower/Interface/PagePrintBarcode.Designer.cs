﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PagePrintBarcode
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PagePrintBarcode));
            this.dgv = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkSHOW_CUSTOMER_TO_PRINT_BARCODE = new System.Windows.Forms.CheckBox();
            this.txtQuickSearch = new SoftTech.Component.ExTextbox();
            this.cboTransformer = new System.Windows.Forms.ComboBox();
            this.cboArea = new System.Windows.Forms.ComboBox();
            this.btnPRINT = new SoftTech.Component.ExButton();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chkAll_ = new System.Windows.Forms.CheckBox();
            this.CUSTOMER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_PRINT_ = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.CUSTOMER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AREA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.POLE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BOX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.METER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PHONE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_STATUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CUSTOMER_ID,
            this.IS_PRINT_,
            this.CUSTOMER_CODE,
            this.CUSTOMER_NAME,
            this.CUSTOMER_TYPE,
            this.AREA,
            this.POLE,
            this.BOX,
            this.METER,
            this.PHONE,
            this.CUSTOMER_STATUS});
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellClick);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.chkSHOW_CUSTOMER_TO_PRINT_BARCODE);
            this.panel1.Controls.Add(this.txtQuickSearch);
            this.panel1.Controls.Add(this.cboTransformer);
            this.panel1.Controls.Add(this.cboArea);
            this.panel1.Controls.Add(this.btnPRINT);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // chkSHOW_CUSTOMER_TO_PRINT_BARCODE
            // 
            resources.ApplyResources(this.chkSHOW_CUSTOMER_TO_PRINT_BARCODE, "chkSHOW_CUSTOMER_TO_PRINT_BARCODE");
            this.chkSHOW_CUSTOMER_TO_PRINT_BARCODE.Name = "chkSHOW_CUSTOMER_TO_PRINT_BARCODE";
            this.chkSHOW_CUSTOMER_TO_PRINT_BARCODE.UseVisualStyleBackColor = true;
            this.chkSHOW_CUSTOMER_TO_PRINT_BARCODE.CheckedChanged += new System.EventHandler(this.chkViewPrintCustomer_CheckedChanged);
            // 
            // txtQuickSearch
            // 
            this.txtQuickSearch.BackColor = System.Drawing.Color.White;
            this.txtQuickSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtQuickSearch, "txtQuickSearch");
            this.txtQuickSearch.Name = "txtQuickSearch";
            this.txtQuickSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtQuickSearch.QuickSearch += new System.EventHandler(this.txtQuickSearch_QuickSearch);
            // 
            // cboTransformer
            // 
            this.cboTransformer.DropDownHeight = 150;
            this.cboTransformer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTransformer.FormattingEnabled = true;
            resources.ApplyResources(this.cboTransformer, "cboTransformer");
            this.cboTransformer.Items.AddRange(new object[] {
            resources.GetString("cboTransformer.Items"),
            resources.GetString("cboTransformer.Items1"),
            resources.GetString("cboTransformer.Items2"),
            resources.GetString("cboTransformer.Items3"),
            resources.GetString("cboTransformer.Items4")});
            this.cboTransformer.Name = "cboTransformer";
            this.cboTransformer.SelectedIndexChanged += new System.EventHandler(this.cboTransformer_SelectedIndexChanged);
            // 
            // cboArea
            // 
            this.cboArea.DropDownHeight = 150;
            this.cboArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboArea.FormattingEnabled = true;
            resources.ApplyResources(this.cboArea, "cboArea");
            this.cboArea.Items.AddRange(new object[] {
            resources.GetString("cboArea.Items"),
            resources.GetString("cboArea.Items1"),
            resources.GetString("cboArea.Items2"),
            resources.GetString("cboArea.Items3"),
            resources.GetString("cboArea.Items4")});
            this.cboArea.Name = "cboArea";
            this.cboArea.SelectedIndexChanged += new System.EventHandler(this.cboArea_SelectedIndexChanged);
            // 
            // btnPRINT
            // 
            resources.ApplyResources(this.btnPRINT, "btnPRINT");
            this.btnPRINT.Name = "btnPRINT";
            this.btnPRINT.UseVisualStyleBackColor = true;
            this.btnPRINT.Click += new System.EventHandler(this.btnPrintBarcode_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "CUSTOMER_ID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn1, "dataGridViewTextBoxColumn1");
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "CUSTOMER_CODE";
            resources.ApplyResources(this.dataGridViewTextBoxColumn2, "dataGridViewTextBoxColumn2");
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "CUSTOMER_NAME";
            resources.ApplyResources(this.dataGridViewTextBoxColumn3, "dataGridViewTextBoxColumn3");
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "CUSTOMER_TYPE";
            resources.ApplyResources(this.dataGridViewTextBoxColumn4, "dataGridViewTextBoxColumn4");
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "AREA";
            resources.ApplyResources(this.dataGridViewTextBoxColumn5, "dataGridViewTextBoxColumn5");
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn6.DataPropertyName = "PHONE";
            resources.ApplyResources(this.dataGridViewTextBoxColumn6, "dataGridViewTextBoxColumn6");
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "BOX";
            resources.ApplyResources(this.dataGridViewTextBoxColumn7, "dataGridViewTextBoxColumn7");
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "METER";
            resources.ApplyResources(this.dataGridViewTextBoxColumn8, "dataGridViewTextBoxColumn8");
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn9.DataPropertyName = "PHONE";
            resources.ApplyResources(this.dataGridViewTextBoxColumn9, "dataGridViewTextBoxColumn9");
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn10.DataPropertyName = "CUSTOMER_STATUS";
            resources.ApplyResources(this.dataGridViewTextBoxColumn10, "dataGridViewTextBoxColumn10");
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "CUSTOMER_STATUS";
            resources.ApplyResources(this.dataGridViewTextBoxColumn11, "dataGridViewTextBoxColumn11");
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            // 
            // chkAll_
            // 
            resources.ApplyResources(this.chkAll_, "chkAll_");
            this.chkAll_.Name = "chkAll_";
            this.chkAll_.UseVisualStyleBackColor = true;
            this.chkAll_.CheckedChanged += new System.EventHandler(this.chkAll_CheckedChanged);
            // 
            // CUSTOMER_ID
            // 
            this.CUSTOMER_ID.DataPropertyName = "CUSTOMER_ID";
            resources.ApplyResources(this.CUSTOMER_ID, "CUSTOMER_ID");
            this.CUSTOMER_ID.Name = "CUSTOMER_ID";
            this.CUSTOMER_ID.ReadOnly = true;
            // 
            // IS_PRINT_
            // 
            this.IS_PRINT_.DataPropertyName = "IS_PRINT_";
            resources.ApplyResources(this.IS_PRINT_, "IS_PRINT_");
            this.IS_PRINT_.Name = "IS_PRINT_";
            this.IS_PRINT_.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.IS_PRINT_.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // CUSTOMER_CODE
            // 
            this.CUSTOMER_CODE.DataPropertyName = "CUSTOMER_CODE";
            resources.ApplyResources(this.CUSTOMER_CODE, "CUSTOMER_CODE");
            this.CUSTOMER_CODE.Name = "CUSTOMER_CODE";
            // 
            // CUSTOMER_NAME
            // 
            this.CUSTOMER_NAME.DataPropertyName = "CUSTOMER_NAME";
            resources.ApplyResources(this.CUSTOMER_NAME, "CUSTOMER_NAME");
            this.CUSTOMER_NAME.Name = "CUSTOMER_NAME";
            // 
            // CUSTOMER_TYPE
            // 
            this.CUSTOMER_TYPE.DataPropertyName = "CUSTOMER_TYPE";
            resources.ApplyResources(this.CUSTOMER_TYPE, "CUSTOMER_TYPE");
            this.CUSTOMER_TYPE.Name = "CUSTOMER_TYPE";
            this.CUSTOMER_TYPE.ReadOnly = true;
            // 
            // AREA
            // 
            this.AREA.DataPropertyName = "AREA";
            resources.ApplyResources(this.AREA, "AREA");
            this.AREA.Name = "AREA";
            this.AREA.ReadOnly = true;
            // 
            // POLE
            // 
            this.POLE.DataPropertyName = "POLE";
            resources.ApplyResources(this.POLE, "POLE");
            this.POLE.Name = "POLE";
            this.POLE.ReadOnly = true;
            // 
            // BOX
            // 
            this.BOX.DataPropertyName = "BOX";
            resources.ApplyResources(this.BOX, "BOX");
            this.BOX.Name = "BOX";
            this.BOX.ReadOnly = true;
            // 
            // METER
            // 
            this.METER.DataPropertyName = "METER";
            resources.ApplyResources(this.METER, "METER");
            this.METER.Name = "METER";
            this.METER.ReadOnly = true;
            // 
            // PHONE
            // 
            this.PHONE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PHONE.DataPropertyName = "PHONE";
            resources.ApplyResources(this.PHONE, "PHONE");
            this.PHONE.Name = "PHONE";
            this.PHONE.ReadOnly = true;
            // 
            // CUSTOMER_STATUS
            // 
            this.CUSTOMER_STATUS.DataPropertyName = "CUSTOMER_STATUS";
            resources.ApplyResources(this.CUSTOMER_STATUS, "CUSTOMER_STATUS");
            this.CUSTOMER_STATUS.Name = "CUSTOMER_STATUS";
            this.CUSTOMER_STATUS.ReadOnly = true;
            // 
            // PagePrintBarcode
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.chkAll_);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.panel1);
            this.Name = "PagePrintBarcode";
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Panel panel1;
        private DataGridView dgv;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private ExButton btnPRINT;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        public ComboBox cboArea;
        public ComboBox cboTransformer;
        private ExTextbox txtQuickSearch;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private CheckBox chkAll_;
        private CheckBox chkSHOW_CUSTOMER_TO_PRINT_BARCODE;
        private DataGridViewTextBoxColumn CUSTOMER_ID;
        private DataGridViewCheckBoxColumn IS_PRINT_;
        private DataGridViewTextBoxColumn CUSTOMER_CODE;
        private DataGridViewTextBoxColumn CUSTOMER_NAME;
        private DataGridViewTextBoxColumn CUSTOMER_TYPE;
        private DataGridViewTextBoxColumn AREA;
        private DataGridViewTextBoxColumn POLE;
        private DataGridViewTextBoxColumn BOX;
        private DataGridViewTextBoxColumn METER;
        private DataGridViewTextBoxColumn PHONE;
        private DataGridViewTextBoxColumn CUSTOMER_STATUS;
    }
}
