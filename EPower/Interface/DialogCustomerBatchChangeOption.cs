﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogCustomerBatchChangeOption : ExDialog
    {
        public DialogCustomerBatchChangeOption()
        {
            InitializeComponent();
            bind();
        }
        private void bind()
        {
            var cusArea = DBDataContext.Db.TBL_CUSTOMERs.Select(x => x.AREA_ID).Distinct().ToArray();
            var area = DBDataContext.Db.TBL_AREAs.Where(x => cusArea.Contains(x.AREA_ID) && x.IS_ACTIVE).Select(x => x.AREA_NAME).ToArray();

            UIHelper.SetDataSourceToComboBox(cboArea, DBDataContext.Db.TBL_AREAs.Where(x => area.Contains(x.AREA_NAME) && x.IS_ACTIVE).OrderBy(x => x.AREA_CODE), Resources.ALL_AREA);
            UIHelper.SetDataSourceToComboBox(cboCustomerStatus, Lookup.GetCustomerStatuses(), Properties.Resources.ALL_STATUS);
            var villages = DBDataContext.Db.TBL_CUSTOMERs.Select(x => x.VILLAGE_CODE).Distinct().ToArray();
            //.Union(DBDataContext.Db.TBL_LICENSE_VILLAGEs.Where(x => x.IS_ACTIVE).Select(x => x.VILLAGE_CODE).Distinct()).ToArray();
            var communes = DBDataContext.Db.TLKP_VILLAGEs.Where(x => villages.Contains(x.VILLAGE_CODE)).Select(x => x.COMMUNE_CODE).Distinct().ToList();
            UIHelper.SetDataSourceToComboBox(cboCommune, DBDataContext.Db.TLKP_COMMUNEs.Where(x => communes.Contains(x.COMMUNE_CODE)), Resources.ALL_COMMUNE);
            UIHelper.SetDataSourceToComboBox(cboCustomerGroup, Lookup.GetCustomerGroup(), Resources.All_CUSTOMER_GROUP);
        }

        private void cboCustomerGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            int groupId = DataHelper.ParseToInt(cboCustomerGroup.SelectedValue.ToString());
            var dt = DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs
                    .OrderBy(x => x.DESCRIPTION)
                    .Where(x => x.NONLICENSE_CUSTOMER_GROUP_ID == groupId);
            if (cboCustomerGroup.SelectedIndex != -1)
            {
                UIHelper.SetDataSourceToComboBox(cboCustomerConnectionType, dt, Resources.All_CUSTOMER_CUSTOMER_TYPE);
                if (cboCustomerConnectionType.DataSource != null)
                {
                    cboCustomerConnectionType.SelectedIndex = 0;
                }
            }
        }
        private void cboSaveState_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            if (cbo.SelectedIndex == -1)
            {
                return;
            }
            cbo.Tag = cbo.SelectedValue;
        }
        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void cboArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboPole.SelectedValue = 0;
            UIHelper.SetDataSourceToComboBox(cboPole, Lookup.GetPoles((int)cboArea.SelectedValue), Resources.ALL_POLE);
            //var cusMeter = DBDataContext.Db.TBL_CUSTOMER_METERs.Select(x => x.POLE_ID).Distinct().ToArray();
            //var pole = DBDataContext.Db.TBL_POLEs.Where(x => cusMeter.Contains(x.POLE_ID) && x.IS_ACTIVE).Select(x=>x.POLE_ID).ToArray();
            //var area = DBDataContext.Db.TBL_AREAs.Where(x => x.AREA_NAME == cboArea.SelectedValue.ToString()).Select(x => x.AREA_ID).ToArray();
            //int areaId = DataHelper.ParseToInt(area.ToString());
            //UIHelper.SetDataSourceToComboBox(cboPole,DBDataContext.Db.TBL_POLEs.Where(x=>(x.AREA_ID==areaId||areaId==0) &&x.IS_ACTIVE), Resources.ALL_POLE);

        }

        private void cboCommune_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboVillage.SelectedValue = 0;
            var villages = DBDataContext.Db.TBL_CUSTOMERs.Select(x => x.VILLAGE_CODE).Distinct().ToArray();
            //.Union(DBDataContext.Db.TBL_LICENSE_VILLAGEs.Where(x => x.IS_ACTIVE).Select(x => x.VILLAGE_CODE).Distinct()).ToArray();
            UIHelper.SetDataSourceToComboBox(cboVillage, DBDataContext.Db.TLKP_VILLAGEs.Where(x => x.COMMUNE_CODE == cboCommune.SelectedValue.ToString() && villages.Contains(x.VILLAGE_CODE)).OrderBy(x => x.VILLAGE_CODE), Resources.ALL_VILLAGE);
        }
    }
}
