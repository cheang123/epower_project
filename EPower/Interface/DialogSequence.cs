﻿using EPower.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogSequence : SoftTech.Component.ExDialog
    {
        GeneralProcess _flag;
        TBL_SEQUENCE _objSequence = new TBL_SEQUENCE();
        bool isValidFormat = true;
        public TBL_SEQUENCE Sequence
        {
            get { return _objSequence; }
        }
        TBL_SEQUENCE _oldobjSequence = new TBL_SEQUENCE();

        #region Constructor
        public DialogSequence(GeneralProcess flag, TBL_SEQUENCE objSequence)
        {
            InitializeComponent();
            _flag = flag;

            objSequence._CopyTo(_objSequence);
            objSequence._CopyTo(_oldobjSequence);

            if (flag == GeneralProcess.Insert)
            {
                //_objEquipment.IS_ACTIVE = true;
                //this.Text = string.Concat(Resources.Insert, this.Text);
            }
            else if (flag == GeneralProcess.Update)
            {
                this.Text = string.Concat(Resources.UPDATE, this.Text);
            }
            else if (flag == GeneralProcess.Delete)
            {
                //this.Text = string.Concat(Resources.Delete, this.Text);
                //UIHelper.SetEnabled(this, false);
            }
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
            read();
        }
        #endregion

        #region Opeartion

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
            txtName.ClearValidation();

            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objSequence);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_oldobjSequence, _objSequence);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objSequence);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        #endregion

        #region Method

        private void read()
        {
            txtName.Text = _objSequence.SEQUENCE_NAME_KH;
            txtValue.Text = _objSequence.VALUE.ToString();
            txtFormat.Text = _objSequence.FORMAT;
            txtResult.Text = SequenceLogic.StringFormat(_objSequence.FORMAT, _objSequence.VALUE);

            this.txtValue.Enabled = _objSequence.VALUE == 0;
            //txtResult.Text = Logic.Method.GetNextSequence((EPower.Sequence)_objSequence.SEQUENCE_ID, false);

            foreach (var parameter in SequenceLogic.ParameterAndDscriptionList)
            {
                var row = dgvParameters.Rows[dgvParameters.Rows.Add()];
                row.Cells[Value.Name].Value = parameter.Key;
                row.Cells[RESULT.Name].Value = parameter.Value;
            }

            // if service or bill sequence => not allow edit 
            var sequenceIds = new List<int>() { 1, 4 };
            if (sequenceIds.Contains(_objSequence.SEQUENCE_ID))
            {
                txtName.ReadOnly = true;
                txtFormat.ReadOnly = true;
                txtValue.ReadOnly = true;
                txtResult.ReadOnly = true;
            }
            else
            {
                txtName.ReadOnly = false;
                txtFormat.ReadOnly = false;
                txtValue.ReadOnly = true;
                txtResult.ReadOnly = true;
            }
        }

        private void write()
        {
            _objSequence.SEQUENCE_NAME_KH = txtName.Text.Trim();
            _objSequence.VALUE = int.Parse(txtValue.Text.Trim());
            _objSequence.FORMAT = txtFormat.Text.Trim();
        }

        private bool inValid()
        {
            bool val = false;

            this.HideValidation();

            if (!txtName.IsValidRequired(lblName.Text) |
                !txtFormat.IsValidRequired(lblFormat.Text))
            {
                val = true;
            }

            if (!isValidFormat)
            {
                txtFormat.SetValidation(Resources.CANNOT_USE_THIS_SEQUENCE_FORMAT);
                val = true;
            }

            return val;
        }
        #endregion

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            SoftTech.Security.Interface.DialogChangeLog.ShowChangeLog(this._objSequence);
        }

        private void txtFormat_TextChanged(object sender, EventArgs e)
        {
            this.txtResult.ClearValidation();
            try
            {
                if (!txtFormat.Text.EndsWith("}"))
                {
                }
                txtResult.Text = Logic.SequenceLogic.StringFormat(txtFormat.Text, int.Parse(txtValue.Text));
                isValidFormat = true;
            }
            catch
            {
                isValidFormat = false;
                txtResult.Text = Resources.CANNOT_USE_THIS_SEQUENCE_FORMAT;
            }

        }
    }
}
