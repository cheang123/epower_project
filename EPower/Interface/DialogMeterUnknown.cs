﻿using System;
using System.Linq;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class DialogMeterUnknown : ExDialog
    {
        #region Constructor
        public DialogMeterUnknown()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            Bind();
        }
        #endregion Constructor

        #region Method

        void Bind()
        {
            this.dgv.DataSource = from m in DBDataContext.Db.TBL_METER_UNKNOWNs
                             join dt in DBDataContext.Db.TLKP_DEVICE_TYPEs on m.DEVICE_TYPE_ID equals dt.DEVICE_TYPE_ID
                             join emp in DBDataContext.Db.TBL_EMPLOYEEs on m.COLLECTOR_ID equals emp.EMPLOYEE_ID into tmp
                             from emptmp in tmp.DefaultIfEmpty()
                             select new
                             {
                                 m.AREA_CODE,
                                 m.BOX_CODE,
                                 m.END_USAGE,
                                 m.DEVICE_CODE,
                                 m.METER_CODE,
                                 m.POLE_CODE,
                                 dt.DEVICE_TYPE_NAME,
                                 emptmp.EMPLOYEE_NAME
                             };
            this.lblCount_.Text = this.dgv.Rows.Count.ToString();
        }

        #endregion Method
 
        #region Event
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public CrystalReportHelper ViewReportMeterUnknown()
        {
            return new CrystalReportHelper("ReportMeterUnknown.rpt");
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            CrystalReportHelper ch = ViewReportMeterUnknown();
            ch.ViewReport(""); 
        }

        #endregion Event


    }


}
