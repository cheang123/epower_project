﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogMeterUnknown
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogMeterUnknown));
            this.dgv = new System.Windows.Forms.DataGridView();
            this.METER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AREA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.POLE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BOX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.END_USAGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DEVICE_TYPE_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DEVICE_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COLLECTOR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnPRINT = new SoftTech.Component.ExButton();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.TOTAL = new System.Windows.Forms.Label();
            this.lblCount_ = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.TOTAL);
            this.content.Controls.Add(this.lblCount_);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnPRINT);
            this.content.Controls.Add(this.dgv);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.dgv, 0);
            this.content.Controls.SetChildIndex(this.btnPRINT, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.lblCount_, 0);
            this.content.Controls.SetChildIndex(this.TOTAL, 0);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.METER_CODE,
            this.AREA,
            this.POLE,
            this.BOX,
            this.END_USAGE,
            this.DEVICE_TYPE_NAME,
            this.DEVICE_CODE,
            this.COLLECTOR});
            this.dgv.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            // 
            // METER_CODE
            // 
            this.METER_CODE.DataPropertyName = "METER_CODE";
            resources.ApplyResources(this.METER_CODE, "METER_CODE");
            this.METER_CODE.Name = "METER_CODE";
            this.METER_CODE.ReadOnly = true;
            // 
            // AREA
            // 
            this.AREA.DataPropertyName = "AREA_CODE";
            resources.ApplyResources(this.AREA, "AREA");
            this.AREA.Name = "AREA";
            this.AREA.ReadOnly = true;
            // 
            // POLE
            // 
            this.POLE.DataPropertyName = "POLE_CODE";
            resources.ApplyResources(this.POLE, "POLE");
            this.POLE.Name = "POLE";
            this.POLE.ReadOnly = true;
            // 
            // BOX
            // 
            this.BOX.DataPropertyName = "BOX_CODE";
            resources.ApplyResources(this.BOX, "BOX");
            this.BOX.Name = "BOX";
            this.BOX.ReadOnly = true;
            // 
            // END_USAGE
            // 
            this.END_USAGE.DataPropertyName = "END_USAGE";
            dataGridViewCellStyle2.Format = "N0";
            this.END_USAGE.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.END_USAGE, "END_USAGE");
            this.END_USAGE.Name = "END_USAGE";
            this.END_USAGE.ReadOnly = true;
            // 
            // DEVICE_TYPE_NAME
            // 
            this.DEVICE_TYPE_NAME.DataPropertyName = "DEVICE_TYPE_NAME";
            resources.ApplyResources(this.DEVICE_TYPE_NAME, "DEVICE_TYPE_NAME");
            this.DEVICE_TYPE_NAME.Name = "DEVICE_TYPE_NAME";
            this.DEVICE_TYPE_NAME.ReadOnly = true;
            // 
            // DEVICE_CODE
            // 
            this.DEVICE_CODE.DataPropertyName = "DEVICE_CODE";
            resources.ApplyResources(this.DEVICE_CODE, "DEVICE_CODE");
            this.DEVICE_CODE.Name = "DEVICE_CODE";
            this.DEVICE_CODE.ReadOnly = true;
            // 
            // COLLECTOR
            // 
            this.COLLECTOR.DataPropertyName = "EMPLOYEE_NAME";
            resources.ApplyResources(this.COLLECTOR, "COLLECTOR");
            this.COLLECTOR.Name = "COLLECTOR";
            this.COLLECTOR.ReadOnly = true;
            // 
            // btnPRINT
            // 
            resources.ApplyResources(this.btnPRINT, "btnPRINT");
            this.btnPRINT.Name = "btnPRINT";
            this.btnPRINT.UseVisualStyleBackColor = true;
            this.btnPRINT.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // TOTAL
            // 
            resources.ApplyResources(this.TOTAL, "TOTAL");
            this.TOTAL.Name = "TOTAL";
            // 
            // lblCount_
            // 
            resources.ApplyResources(this.lblCount_, "lblCount_");
            this.lblCount_.Name = "lblCount_";
            // 
            // DialogMeterUnknown
            // 
            resources.ApplyResources(this, "$this");
            this.Name = "DialogMeterUnknown";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataGridView dgv;
        private ExButton btnCLOSE;
        private ExButton btnPRINT;
        private Label TOTAL;
        private Label lblCount_;
        private DataGridViewTextBoxColumn METER_CODE;
        private DataGridViewTextBoxColumn AREA;
        private DataGridViewTextBoxColumn POLE;
        private DataGridViewTextBoxColumn BOX;
        private DataGridViewTextBoxColumn END_USAGE;
        private DataGridViewTextBoxColumn DEVICE_TYPE_NAME;
        private DataGridViewTextBoxColumn DEVICE_CODE;
        private DataGridViewTextBoxColumn COLLECTOR;
    }
}
