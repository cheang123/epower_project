﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.Stock
{
    partial class DialogStockTran
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogStockTran));
            this.lblPRODUCT_NAME = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.label9 = new System.Windows.Forms.Label();
            this.cboTrans = new System.Windows.Forms.ComboBox();
            this.groupRoot = new System.Windows.Forms.FlowLayoutPanel();
            this.groupItemName = new System.Windows.Forms.Panel();
            this.cboItem = new System.Windows.Forms.ComboBox();
            this.groupTransactionType = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.lblTRANS = new System.Windows.Forms.Label();
            this.groupFromTypeAndToType = new System.Windows.Forms.FlowLayoutPanel();
            this.groupFromType = new System.Windows.Forms.Panel();
            this.cboFromType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblFROM_STATUS = new System.Windows.Forms.Label();
            this.groupToType = new System.Windows.Forms.Panel();
            this.cboToType = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.lblTO_STATUS = new System.Windows.Forms.Label();
            this.groupQTYAndPrice = new System.Windows.Forms.Panel();
            this.groupPrice = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.lblUNIT = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.txtQty = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.lblQTY = new System.Windows.Forms.Label();
            this.groupRemark = new System.Windows.Forms.Panel();
            this.txtRemark = new System.Windows.Forms.TextBox();
            this.lblNOTE = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupRoot.SuspendLayout();
            this.groupItemName.SuspendLayout();
            this.groupTransactionType.SuspendLayout();
            this.groupFromTypeAndToType.SuspendLayout();
            this.groupFromType.SuspendLayout();
            this.groupToType.SuspendLayout();
            this.groupQTYAndPrice.SuspendLayout();
            this.groupPrice.SuspendLayout();
            this.groupRemark.SuspendLayout();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.groupRoot);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.groupRoot, 0);
            // 
            // lblPRODUCT_NAME
            // 
            resources.ApplyResources(this.lblPRODUCT_NAME, "lblPRODUCT_NAME");
            this.lblPRODUCT_NAME.Name = "lblPRODUCT_NAME";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // cboTrans
            // 
            this.cboTrans.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTrans.FormattingEnabled = true;
            resources.ApplyResources(this.cboTrans, "cboTrans");
            this.cboTrans.Name = "cboTrans";
            this.cboTrans.SelectedIndexChanged += new System.EventHandler(this.cboTrans_SelectedIndexChanged);
            this.cboTrans.Enter += new System.EventHandler(this.InputKH);
            // 
            // groupRoot
            // 
            this.groupRoot.Controls.Add(this.groupItemName);
            this.groupRoot.Controls.Add(this.groupTransactionType);
            this.groupRoot.Controls.Add(this.groupFromTypeAndToType);
            this.groupRoot.Controls.Add(this.groupQTYAndPrice);
            this.groupRoot.Controls.Add(this.groupRemark);
            resources.ApplyResources(this.groupRoot, "groupRoot");
            this.groupRoot.Name = "groupRoot";
            // 
            // groupItemName
            // 
            this.groupItemName.Controls.Add(this.cboItem);
            this.groupItemName.Controls.Add(this.label9);
            this.groupItemName.Controls.Add(this.lblPRODUCT_NAME);
            resources.ApplyResources(this.groupItemName, "groupItemName");
            this.groupItemName.Name = "groupItemName";
            // 
            // cboItem
            // 
            this.cboItem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboItem.FormattingEnabled = true;
            resources.ApplyResources(this.cboItem, "cboItem");
            this.cboItem.Name = "cboItem";
            this.cboItem.Enter += new System.EventHandler(this.InputKH);
            // 
            // groupTransactionType
            // 
            this.groupTransactionType.Controls.Add(this.cboTrans);
            this.groupTransactionType.Controls.Add(this.label5);
            this.groupTransactionType.Controls.Add(this.lblTRANS);
            resources.ApplyResources(this.groupTransactionType, "groupTransactionType");
            this.groupTransactionType.Name = "groupTransactionType";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Name = "label5";
            // 
            // lblTRANS
            // 
            resources.ApplyResources(this.lblTRANS, "lblTRANS");
            this.lblTRANS.Name = "lblTRANS";
            // 
            // groupFromTypeAndToType
            // 
            this.groupFromTypeAndToType.Controls.Add(this.groupFromType);
            this.groupFromTypeAndToType.Controls.Add(this.groupToType);
            resources.ApplyResources(this.groupFromTypeAndToType, "groupFromTypeAndToType");
            this.groupFromTypeAndToType.Name = "groupFromTypeAndToType";
            // 
            // groupFromType
            // 
            this.groupFromType.Controls.Add(this.cboFromType);
            this.groupFromType.Controls.Add(this.label2);
            this.groupFromType.Controls.Add(this.lblFROM_STATUS);
            resources.ApplyResources(this.groupFromType, "groupFromType");
            this.groupFromType.Name = "groupFromType";
            // 
            // cboFromType
            // 
            this.cboFromType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFromType.FormattingEnabled = true;
            resources.ApplyResources(this.cboFromType, "cboFromType");
            this.cboFromType.Name = "cboFromType";
            this.cboFromType.Enter += new System.EventHandler(this.InputKH);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Name = "label2";
            // 
            // lblFROM_STATUS
            // 
            resources.ApplyResources(this.lblFROM_STATUS, "lblFROM_STATUS");
            this.lblFROM_STATUS.Name = "lblFROM_STATUS";
            // 
            // groupToType
            // 
            this.groupToType.Controls.Add(this.cboToType);
            this.groupToType.Controls.Add(this.label11);
            this.groupToType.Controls.Add(this.lblTO_STATUS);
            resources.ApplyResources(this.groupToType, "groupToType");
            this.groupToType.Name = "groupToType";
            // 
            // cboToType
            // 
            this.cboToType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboToType.FormattingEnabled = true;
            resources.ApplyResources(this.cboToType, "cboToType");
            this.cboToType.Name = "cboToType";
            this.cboToType.Enter += new System.EventHandler(this.InputKH);
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Name = "label11";
            // 
            // lblTO_STATUS
            // 
            resources.ApplyResources(this.lblTO_STATUS, "lblTO_STATUS");
            this.lblTO_STATUS.Name = "lblTO_STATUS";
            // 
            // groupQTYAndPrice
            // 
            this.groupQTYAndPrice.Controls.Add(this.groupPrice);
            this.groupQTYAndPrice.Controls.Add(this.txtQty);
            this.groupQTYAndPrice.Controls.Add(this.label13);
            this.groupQTYAndPrice.Controls.Add(this.lblQTY);
            resources.ApplyResources(this.groupQTYAndPrice, "groupQTYAndPrice");
            this.groupQTYAndPrice.Name = "groupQTYAndPrice";
            // 
            // groupPrice
            // 
            this.groupPrice.Controls.Add(this.label6);
            this.groupPrice.Controls.Add(this.lblUNIT);
            this.groupPrice.Controls.Add(this.txtPrice);
            resources.ApplyResources(this.groupPrice, "groupPrice");
            this.groupPrice.Name = "groupPrice";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Name = "label6";
            // 
            // lblUNIT
            // 
            resources.ApplyResources(this.lblUNIT, "lblUNIT");
            this.lblUNIT.Name = "lblUNIT";
            // 
            // txtPrice
            // 
            resources.ApplyResources(this.txtPrice, "txtPrice");
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Enter += new System.EventHandler(this.InputEN);
            // 
            // txtQty
            // 
            resources.ApplyResources(this.txtQty, "txtQty");
            this.txtQty.Name = "txtQty";
            this.txtQty.TextChanged += new System.EventHandler(this.txtQty_TextChanged);
            this.txtQty.Enter += new System.EventHandler(this.InputEN);
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Name = "label13";
            // 
            // lblQTY
            // 
            resources.ApplyResources(this.lblQTY, "lblQTY");
            this.lblQTY.Name = "lblQTY";
            // 
            // groupRemark
            // 
            this.groupRemark.Controls.Add(this.txtRemark);
            this.groupRemark.Controls.Add(this.lblNOTE);
            resources.ApplyResources(this.groupRemark, "groupRemark");
            this.groupRemark.Name = "groupRemark";
            // 
            // txtRemark
            // 
            this.txtRemark.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.txtRemark, "txtRemark");
            this.txtRemark.Name = "txtRemark";
            // 
            // lblNOTE
            // 
            resources.ApplyResources(this.lblNOTE, "lblNOTE");
            this.lblNOTE.Name = "lblNOTE";
            // 
            // DialogStockTran
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogStockTran";
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupRoot.ResumeLayout(false);
            this.groupItemName.ResumeLayout(false);
            this.groupItemName.PerformLayout();
            this.groupTransactionType.ResumeLayout(false);
            this.groupTransactionType.PerformLayout();
            this.groupFromTypeAndToType.ResumeLayout(false);
            this.groupFromType.ResumeLayout(false);
            this.groupFromType.PerformLayout();
            this.groupToType.ResumeLayout(false);
            this.groupToType.PerformLayout();
            this.groupQTYAndPrice.ResumeLayout(false);
            this.groupQTYAndPrice.PerformLayout();
            this.groupPrice.ResumeLayout(false);
            this.groupPrice.PerformLayout();
            this.groupRemark.ResumeLayout(false);
            this.groupRemark.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Label lblPRODUCT_NAME;
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label label9;
        private ComboBox cboTrans;
        private FlowLayoutPanel groupRoot;
        private Panel groupItemName;
        private Panel groupTransactionType;
        private Label label5;
        private Label lblTRANS;
        private Panel groupRemark;
        private Panel groupQTYAndPrice;
        private Label lblUNIT;
        private TextBox txtPrice;
        private TextBox txtQty;
        private Label label13;
        private Label lblQTY;
        private Label lblNOTE;
        private FlowLayoutPanel groupFromTypeAndToType;
        private Panel groupFromType;
        private ComboBox cboFromType;
        private Label label2;
        private Label lblFROM_STATUS;
        private Panel groupToType;
        private ComboBox cboToType;
        private Label label11;
        private Label lblTO_STATUS;
        private TextBox txtRemark;
        private ComboBox cboItem;
        private Panel groupPrice;
        private Label label6;
    }
}