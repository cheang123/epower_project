﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.Stock
{
    partial class PageStockManagement
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageStockManagement));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.ITEM_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRODUCT_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UNIT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QTY_STOCK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QTY_USED = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QTY_DAMAGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cboReport = new System.Windows.Forms.ComboBox();
            this.btnREPORT = new SoftTech.Component.ExButton();
            this.btnTRANS = new SoftTech.Component.ExButton();
            this.btnHISTORY = new SoftTech.Component.ExButton();
            this.txtQuickSearch = new SoftTech.Component.ExTextbox();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnViewReport = new SoftTech.Component.ExButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ITEM_ID,
            this.TYPE,
            this.PRODUCT_NAME,
            this.UNIT,
            this.QTY_STOCK,
            this.QTY_USED,
            this.QTY_DAMAGE});
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // ITEM_ID
            // 
            this.ITEM_ID.DataPropertyName = "ITEM_ID";
            resources.ApplyResources(this.ITEM_ID, "ITEM_ID");
            this.ITEM_ID.Name = "ITEM_ID";
            // 
            // TYPE
            // 
            this.TYPE.DataPropertyName = "TYPE";
            resources.ApplyResources(this.TYPE, "TYPE");
            this.TYPE.Name = "TYPE";
            // 
            // PRODUCT_NAME
            // 
            this.PRODUCT_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PRODUCT_NAME.DataPropertyName = "ITEM_NAME";
            resources.ApplyResources(this.PRODUCT_NAME, "PRODUCT_NAME");
            this.PRODUCT_NAME.Name = "PRODUCT_NAME";
            // 
            // UNIT
            // 
            this.UNIT.DataPropertyName = "UNIT";
            resources.ApplyResources(this.UNIT, "UNIT");
            this.UNIT.Name = "UNIT";
            // 
            // QTY_STOCK
            // 
            this.QTY_STOCK.DataPropertyName = "QTY_STOCK";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "#";
            this.QTY_STOCK.DefaultCellStyle = dataGridViewCellStyle6;
            resources.ApplyResources(this.QTY_STOCK, "QTY_STOCK");
            this.QTY_STOCK.Name = "QTY_STOCK";
            // 
            // QTY_USED
            // 
            this.QTY_USED.DataPropertyName = "QTY_USED";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "#";
            this.QTY_USED.DefaultCellStyle = dataGridViewCellStyle7;
            resources.ApplyResources(this.QTY_USED, "QTY_USED");
            this.QTY_USED.Name = "QTY_USED";
            // 
            // QTY_DAMAGE
            // 
            this.QTY_DAMAGE.DataPropertyName = "QTY_DAMAGE";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "#";
            this.QTY_DAMAGE.DefaultCellStyle = dataGridViewCellStyle8;
            resources.ApplyResources(this.QTY_DAMAGE, "QTY_DAMAGE");
            this.QTY_DAMAGE.Name = "QTY_DAMAGE";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.btnViewReport);
            this.panel1.Controls.Add(this.cboReport);
            this.panel1.Controls.Add(this.btnREPORT);
            this.panel1.Controls.Add(this.btnTRANS);
            this.panel1.Controls.Add(this.btnHISTORY);
            this.panel1.Controls.Add(this.txtQuickSearch);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // cboReport
            // 
            this.cboReport.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboReport.FormattingEnabled = true;
            resources.ApplyResources(this.cboReport, "cboReport");
            this.cboReport.Name = "cboReport";
            this.cboReport.SelectedIndexChanged += new System.EventHandler(this.cboReport_SelectedIndexChanged);
            // 
            // btnREPORT
            // 
            resources.ApplyResources(this.btnREPORT, "btnREPORT");
            this.btnREPORT.Name = "btnREPORT";
            this.btnREPORT.UseVisualStyleBackColor = true;
            this.btnREPORT.Click += new System.EventHandler(this.btnReport_Click);
            // 
            // btnTRANS
            // 
            resources.ApplyResources(this.btnTRANS, "btnTRANS");
            this.btnTRANS.Name = "btnTRANS";
            this.btnTRANS.UseVisualStyleBackColor = true;
            this.btnTRANS.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnHISTORY
            // 
            resources.ApplyResources(this.btnHISTORY, "btnHISTORY");
            this.btnHISTORY.Name = "btnHISTORY";
            this.btnHISTORY.UseVisualStyleBackColor = true;
            this.btnHISTORY.Click += new System.EventHandler(this.btnHistory_Click);
            // 
            // txtQuickSearch
            // 
            this.txtQuickSearch.BackColor = System.Drawing.Color.White;
            this.txtQuickSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtQuickSearch, "txtQuickSearch");
            this.txtQuickSearch.Name = "txtQuickSearch";
            this.txtQuickSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtQuickSearch.QuickSearch += new System.EventHandler(this.txt_QuickSearch);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "ITEM_ID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn1, "dataGridViewTextBoxColumn1");
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "ITEM_NAME";
            resources.ApplyResources(this.dataGridViewTextBoxColumn2, "dataGridViewTextBoxColumn2");
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "UNIT";
            resources.ApplyResources(this.dataGridViewTextBoxColumn3, "dataGridViewTextBoxColumn3");
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // btnViewReport
            // 
            resources.ApplyResources(this.btnViewReport, "btnViewReport");
            this.btnViewReport.Name = "btnViewReport";
            this.btnViewReport.UseVisualStyleBackColor = true;
            this.btnViewReport.Click += new System.EventHandler(this.btnViewReport_Click);
            // 
            // PageStockManagement
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.panel1);
            this.Name = "PageStockManagement";
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExTextbox txtQuickSearch;
        private ExButton btnTRANS;
        private ExButton btnHISTORY;
        private DataGridView dgv;
        private ExButton btnREPORT;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn ITEM_ID;
        private DataGridViewTextBoxColumn TYPE;
        private DataGridViewTextBoxColumn PRODUCT_NAME;
        private DataGridViewTextBoxColumn UNIT;
        private DataGridViewTextBoxColumn QTY_STOCK;
        private DataGridViewTextBoxColumn QTY_USED;
        private DataGridViewTextBoxColumn QTY_DAMAGE;
        private ComboBox cboReport;
        private ExButton btnViewReport;
    }
}
