﻿using System;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;

namespace EPower.Interface.Stock
{
    public partial class DialogStockItem: ExDialog
    {
        GeneralProcess _flag;
        TBL_STOCK_ITEM _objNew = new TBL_STOCK_ITEM();
        public TBL_STOCK_ITEM Ampare
        {
            get { return _objNew; }
        }
        TBL_STOCK_ITEM _objOld = new TBL_STOCK_ITEM();

        #region Constructor
        public DialogStockItem(GeneralProcess flag, TBL_STOCK_ITEM obj)
        {
            InitializeComponent();
            _flag = flag;
            obj._CopyTo(_objNew);
            obj._CopyTo(_objOld);

            if (flag == GeneralProcess.Insert)
            {
                _objNew.IS_ACTIVE = true;
                this.Text = string.Concat(Resources.INSERT, this.Text);
            }
            else if (flag == GeneralProcess.Update)
            {
                this.Text = string.Concat(Resources.UPDATE, this.Text);
            }
            else if (flag == GeneralProcess.Delete)
            {
                this.Text = string.Concat(Resources.DELETE, this.Text);
                UIHelper.SetEnabled(this, false);
            }

            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
            
            read();
        }
        #endregion

        #region Operation
        /// <summary>
        /// Data transaction
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
            //if duplicate record.
            txtItemName.ClearAllValidation();
            if (DBDataContext.Db.IsExits(_objNew, "ITEM_NAME"))
            {
                txtItemName.SetValidation(string.Format(Resources.MS_IS_EXISTS, lblPRODUCT_NAME.Text));
                return;
            }
            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objNew); 
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objNew);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
            
        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
        
        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InputKhmer(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        #endregion

        #region Method
        /// <summary>
        /// Load data from object.
        /// </summary>
        private void read()
        {
            txtItemName.Text = _objNew.ITEM_NAME;
            txtUnit.Text = _objNew.UNIT;
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            _objNew.ITEM_NAME = txtItemName.Text.Trim();
            _objNew.UNIT = txtUnit.Text.Trim();
        }

        private bool inValid()
        {
            bool val = false;
            txtItemName.ClearValidation();
            if( txtItemName.Text.Trim()=="")
            {
                txtItemName.SetValidation(string.Format(Resources.REQUIRED, lblPRODUCT_NAME.Text));
                val = true;
            }
            return val;
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }

        private void txtAmpareName_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputNumberOnly(sender, e);
        }
        #endregion        

       
    }
}