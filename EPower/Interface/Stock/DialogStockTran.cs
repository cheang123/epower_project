﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface.Stock
{
    public partial class DialogStockTran : ExDialog
    {
        GeneralProcess _flag;
        StockItemType _itemType;
        TBL_STOCK_TRAN _objNew = new TBL_STOCK_TRAN();
        bool _loading = false;

        public TBL_STOCK_TRAN Ampare
        {
            get { return _objNew; }
        }
        TBL_STOCK_TRAN _objOld = new TBL_STOCK_TRAN();

        #region Constructor
        public DialogStockTran(GeneralProcess flag, StockItemType itemType, TBL_STOCK_TRAN obj)
        {
            InitializeComponent();

            _loading = true;

            _flag = flag;
            _itemType = itemType;

            obj._CopyTo(_objNew);
            obj._CopyTo(_objOld);

            // init UI
            if (itemType == StockItemType.Breaker)
            {
                UIHelper.SetDataSourceToComboBox(this.cboItem, DBDataContext.Db.TBL_CIRCUIT_BREAKER_TYPEs);
            }
            else
            {
                UIHelper.SetDataSourceToComboBox(this.cboItem, DBDataContext.Db.TBL_STOCK_ITEMs);
            }
            UIHelper.SetDataSourceToComboBox(this.cboTrans, DBDataContext.Db.TLKP_STOCK_TRAN_TYPEs);


            // update interface by stock transaction type.
            this.stockTranTypeChanged(this._objNew.STOCK_TRAN_TYPE_ID);

            // adjust user interface base on Process Flag
            if (flag == GeneralProcess.Insert)
            {
                // this.Text = string.Concat(Resources.Insert, this.Text);
            }
            else if (flag == GeneralProcess.Update)
            {
                //  this.Text = string.Concat(Resources.Update, this.Text);
            }
            else if (flag == GeneralProcess.Delete)
            {
                //  this.Text = string.Concat(Resources.Delete, this.Text);
                UIHelper.SetEnabled(this, false);
            }

            if (this._flag == GeneralProcess.View)
            {
                this.cboFromType.Enabled = false;
                this.cboItem.Enabled = false;
                this.cboToType.Enabled = false;
                this.cboTrans.Enabled = false;
                this.txtPrice.Enabled = false;
                this.txtQty.Enabled = false;
                this.txtRemark.Enabled = false;
            }

            this.cboItem.Enabled = false;

            // read data
            read();

            this._loading = false;
        }
        #endregion


        #region Operation
        /// <summary>
        /// Data transaction
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objNew);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objNew);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }

        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InputKhmer(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        #endregion

        #region Method

        private void stockTranTypeChanged(int stockTranType)
        {
            this.groupFromTypeAndToType.Show();
            this.groupFromType.Show();
            this.groupToType.Show();
            this.groupPrice.Show();

            // adjust user interface base on stock type.
            if (stockTranType == (int)StockTranType.StockIn)
            {
                this._objNew.FROM_STOCK_TYPE_ID = (int)StockType.None;
                this._objNew.TO_STOCK_TYPE_ID = (int)StockType.Stock;

                UIHelper.SetDataSourceToComboBox(this.cboFromType, DBDataContext.Db.TLKP_STOCK_TYPEs.Where(row => row.STOCK_TYPE_ID == (int)StockType.None));
                UIHelper.SetDataSourceToComboBox(this.cboToType, DBDataContext.Db.TLKP_STOCK_TYPEs.Where(row => row.STOCK_TYPE_ID == (int)StockType.Stock));

                this.groupFromTypeAndToType.Hide();
            }
            else if (stockTranType == (int)StockTranType.StockOut)
            {
                this._objNew.FROM_STOCK_TYPE_ID = (int)StockType.Stock;
                this._objNew.TO_STOCK_TYPE_ID = (int)StockType.None;

                UIHelper.SetDataSourceToComboBox(this.cboFromType, DBDataContext.Db.TLKP_STOCK_TYPEs.Where(row => row.STOCK_TYPE_ID == (int)StockType.Stock || row.STOCK_TYPE_ID == (int)StockType.Unavailable));
                UIHelper.SetDataSourceToComboBox(this.cboToType, DBDataContext.Db.TLKP_STOCK_TYPEs.Where(row => row.STOCK_TYPE_ID == (int)StockType.None));

                this.groupToType.Hide();
                this.groupPrice.Hide();
            }
            else if (stockTranType == (int)StockTranType.Damage)
            {

                this._objNew.TO_STOCK_TYPE_ID = (int)StockType.Unavailable;
                this._objNew.FROM_STOCK_TYPE_ID = (int)StockType.Stock;

                UIHelper.SetDataSourceToComboBox(this.cboFromType, DBDataContext.Db.TLKP_STOCK_TYPEs.Where(row => row.STOCK_TYPE_ID == (int)StockType.Used || row.STOCK_TYPE_ID == (int)StockType.Stock));
                UIHelper.SetDataSourceToComboBox(this.cboToType, DBDataContext.Db.TLKP_STOCK_TYPEs.Where(row => row.STOCK_TYPE_ID == (int)StockType.Unavailable));
                this.groupToType.Hide();
                this.groupPrice.Hide();
            }
            else if (stockTranType == (int)StockTranType.Use)
            {
                this._objNew.FROM_STOCK_TYPE_ID = (int)StockType.Stock;
                this._objNew.TO_STOCK_TYPE_ID = (int)StockType.Used;

                UIHelper.SetDataSourceToComboBox(this.cboFromType, DBDataContext.Db.TLKP_STOCK_TYPEs.Where(row => row.STOCK_TYPE_ID == (int)StockType.Stock));
                UIHelper.SetDataSourceToComboBox(this.cboToType, DBDataContext.Db.TLKP_STOCK_TYPEs.Where(row => row.STOCK_TYPE_ID == (int)StockType.Used));

                this.groupFromTypeAndToType.Hide();
                this.groupPrice.Hide();
            }
            else if (stockTranType == (int)StockTranType.Adjust)
            {
                this._objNew.FROM_STOCK_TYPE_ID = (int)StockType.None;
                this._objNew.TO_STOCK_TYPE_ID = (int)StockType.Stock; // by default adjust good!


                int[] typesToAdjust = { (int)StockType.Stock, (int)StockType.Used, (int)StockType.Unavailable };
                UIHelper.SetDataSourceToComboBox(this.cboFromType, DBDataContext.Db.TLKP_STOCK_TYPEs.Where(row => row.STOCK_TYPE_ID == (int)StockType.None));
                UIHelper.SetDataSourceToComboBox(this.cboToType, DBDataContext.Db.TLKP_STOCK_TYPEs.Where(row => typesToAdjust.Contains(row.STOCK_TYPE_ID)));

                this.groupPrice.Hide();
                this.groupFromType.Hide();
            }
        }
        /// <summary>
        /// Load data from object.
        /// </summary>
        private void read()
        {
            this.cboTrans.SelectedValue = this._objNew.STOCK_TRAN_TYPE_ID;
            this.cboFromType.SelectedValue = this._objNew.FROM_STOCK_TYPE_ID;
            this.cboToType.SelectedValue = this._objNew.TO_STOCK_TYPE_ID;
            this.cboItem.SelectedValue = this._objNew.ITEM_ID;

            this.txtPrice.Text = this._objNew.UNIT_PRICE.ToString("N2");
            this.txtQty.Text = this._objNew.QTY.ToString();
            this.txtRemark.Text = this._objNew.REMARK;

            if (this._objNew.QTY == 0) this.txtQty.Text = "";
            if (this._objNew.UNIT_PRICE == 0) this.txtPrice.Text = "";

        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            this._objNew.ITEM_ID = (int)this.cboItem.SelectedValue;
            this._objNew.QTY = DataHelper.ParseToDecimal(this.txtQty.Text);
            this._objNew.UNIT_PRICE = DataHelper.ParseToDecimal(this.txtPrice.Text);
            this._objNew.STOCK_TRAN_TYPE_ID = (int)this.cboTrans.SelectedValue;
            this._objNew.TO_STOCK_TYPE_ID = (int)this.cboToType.SelectedValue;
            this._objNew.FROM_STOCK_TYPE_ID = (int)this.cboFromType.SelectedValue;
            this._objNew.REMARK = this.txtRemark.Text;
            this._objNew.ITEM_ID = (int)this.cboItem.SelectedValue;
            this._objNew.ITEM_TYPE_ID = (int)_itemType;
            if (this._flag == GeneralProcess.Insert)
            {
                this._objNew.CREATE_BY = Login.CurrentLogin.LOGIN_NAME;
                this._objNew.CREATE_ON = DBDataContext.Db.GetSystemDate();
            }
        }

        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();
            if (this.cboItem.SelectedIndex == -1)
            {
                this.cboItem.SetValidation(string.Format(Resources.REQUIRED, this.lblPRODUCT_NAME.Text));
                val = true;
            }
            if (this.cboTrans.SelectedIndex == -1)
            {
                this.cboTrans.SetValidation(string.Format(Resources.REQUIRED, this.lblTRANS.Text));
                val = true;
            }
            if (this.groupFromTypeAndToType.Visible && this.groupFromType.Visible && this.cboFromType.SelectedIndex == -1)
            {
                this.cboFromType.SetValidation(string.Format(Resources.REQUIRED, this.lblFROM_STATUS.Text));
                val = true;
            }
            if (this.groupFromTypeAndToType.Visible && this.groupToType.Visible && this.cboToType.SelectedIndex == -1)
            {
                this.cboToType.SetValidation(string.Format(Resources.REQUIRED, this.lblTO_STATUS.Text));
                val = true;
            }
            if (!DataHelper.IsNumber(this.txtQty.Text))
            {
                this.txtQty.SetValidation(string.Format(Resources.REQUIRED_INPUT_NUMBER, this.lblQTY.Text));
                val = true;
            }
            if (this.groupPrice.Visible && !DataHelper.IsNumber(this.txtPrice.Text))
            {
                this.txtPrice.SetValidation(string.Format(Resources.REQUIRED_INPUT_NUMBER, this.lblUNIT.Text));
                val = true;
            }
            return val;
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }

        private void txtAmpareName_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputNumberOnly(sender, e);
        }
        #endregion         

        private void cboTrans_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_loading)
            {
                this.stockTranTypeChanged((int)this.cboTrans.SelectedValue);
            }
        }

        private void InputKH(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void InputEN(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;

        }

        private void txtQty_TextChanged(object sender, EventArgs e)
        {

        }
    }
}