﻿using System.ComponentModel;
using System.Windows.Forms;

namespace EPower.Interface.BankPayment
{
    partial class DialogRemindPrePayment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnPRE_PAYMENT = new SoftTech.Component.ExButton();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.btnPRE_PAYMENT);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Size = new System.Drawing.Size(434, 144);
            this.content.Text = "\'CONTENT\'";
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.btnPRE_PAYMENT, 0);
            // 
            // btnCLOSE
            // 
            this.btnCLOSE.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnCLOSE.Location = new System.Drawing.Point(353, 114);
            this.btnCLOSE.Margin = new System.Windows.Forms.Padding(2);
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.Size = new System.Drawing.Size(75, 23);
            this.btnCLOSE.TabIndex = 302;
            this.btnCLOSE.Text = "បិទ";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnCLOSE_Click);
            // 
            // btnPRE_PAYMENT
            // 
            this.btnPRE_PAYMENT.Font = new System.Drawing.Font("Khmer OS System", 7.1F, System.Drawing.FontStyle.Bold);
            this.btnPRE_PAYMENT.Location = new System.Drawing.Point(274, 114);
            this.btnPRE_PAYMENT.Margin = new System.Windows.Forms.Padding(2);
            this.btnPRE_PAYMENT.Name = "btnPRE_PAYMENT";
            this.btnPRE_PAYMENT.Size = new System.Drawing.Size(75, 23);
            this.btnPRE_PAYMENT.TabIndex = 303;
            this.btnPRE_PAYMENT.Text = "ទូរទាត់";
            this.btnPRE_PAYMENT.UseVisualStyleBackColor = true;
            this.btnPRE_PAYMENT.Click += new System.EventHandler(this.btnPRE_PAYMENT_Click);
            // 
            // DialogRemindPrePayment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 167);
            this.Name = "DialogRemindPrePayment";
            this.Text = "ការទូរទាត់បង់ប្រាក់មុន";
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private SoftTech.Component.ExButton btnPRE_PAYMENT;
        private SoftTech.Component.ExButton btnCLOSE;
    }
}