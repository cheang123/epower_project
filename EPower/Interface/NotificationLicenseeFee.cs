﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech.Component;
using System;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class NotificationLicenseeFee : ExDialog
    {
        bool mVersion;
        int timeOut = 0;
        double opacity = 200;

        #region Constructor
        public NotificationLicenseeFee(bool IsNewVersion)
        {
            InitializeComponent();
            this.mVersion = IsNewVersion;
            if (mVersion == false)
            {
                this.Top = Screen.PrimaryScreen.WorkingArea.Height - this.Height;
                this.Left = Screen.PrimaryScreen.WorkingArea.Width - this.Width;
            }
            else
            {
                this.Top = Screen.PrimaryScreen.WorkingArea.Height - (this.Height + 140);
                this.Left = Screen.PrimaryScreen.WorkingArea.Width - this.Width;
            }

        }

        protected override void OnLoad(EventArgs e)
        {
            btnUpdate_.Text = Resources.MS_LICENSEE_FEE_PENDING_PAYMENT;
            timer1.Start();
        }

        #endregion 

        private void timer1_Tick(object sender, EventArgs e)
        {
            timeOut += timer1.Interval;
            if (timeOut == TimeSpan.FromMinutes(5).TotalMilliseconds)
            {
                this.Close();
            }
            opacity -= 5;
            this.Opacity = opacity / 100;
        }

        private void Notification_MouseHover(object sender, EventArgs e)
        {
            timer1.Stop();
            timeOut = 0;
            opacity = 100;
            this.Opacity = 1.0;
        }

        private void Notification_MouseLeave(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            string url = Method.Utilities[Utility.B24_EAC_SUPPLIER_API_URL] + Method.Utilities[Utility.B24_EAC_SUPPLIER_API_KEY] + ":" + Method.Utilities[Utility.B24_EAC_CUSTOMER_CODE].Replace("-", "");
            //new DialogWebView(url).ShowDialog();
            System.Diagnostics.Process.Start(url);
        }
    }
}