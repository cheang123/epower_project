﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogPrepaymentAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogPrepaymentAdd));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkPRINT_RECEIPT = new System.Windows.Forms.CheckBox();
            this.lblCUSTOMER_TYPE = new System.Windows.Forms.Label();
            this.lblAREA = new System.Windows.Forms.Label();
            this.lblCUSTOMER_NAME = new System.Windows.Forms.Label();
            this.txtCustomerName = new System.Windows.Forms.TextBox();
            this.txtCustomerType = new System.Windows.Forms.TextBox();
            this.txtAREA = new System.Windows.Forms.TextBox();
            this.txtLastBalance = new System.Windows.Forms.TextBox();
            this.txtNode = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtAdjustAmount = new System.Windows.Forms.TextBox();
            this.lblBALANCE = new System.Windows.Forms.Label();
            this.lblAMOUNT = new System.Windows.Forms.Label();
            this.lblPREPAYMENT_AMOUNT = new System.Windows.Forms.Label();
            this.txtCurrentBalance = new System.Windows.Forms.TextBox();
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.lblCURRENCY = new System.Windows.Forms.Label();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.lblNOTE = new System.Windows.Forms.Label();
            this.cboPaymentAccount = new SoftTech.Component.TreeComboBox();
            this.lblPAYMENT_ACCOUNT = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.cboPaymentAccount);
            this.content.Controls.Add(this.lblPAYMENT_ACCOUNT);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.cboCurrency);
            this.content.Controls.Add(this.lblCURRENCY);
            this.content.Controls.Add(this.txtNode);
            this.content.Controls.Add(this.txtLastBalance);
            this.content.Controls.Add(this.groupBox2);
            this.content.Controls.Add(this.label12);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.chkPRINT_RECEIPT);
            this.content.Controls.Add(this.txtAdjustAmount);
            this.content.Controls.Add(this.txtCurrentBalance);
            this.content.Controls.Add(this.lblBALANCE);
            this.content.Controls.Add(this.lblNOTE);
            this.content.Controls.Add(this.lblAMOUNT);
            this.content.Controls.Add(this.lblPREPAYMENT_AMOUNT);
            this.content.Controls.Add(this.txtAREA);
            this.content.Controls.Add(this.txtCustomerType);
            this.content.Controls.Add(this.txtCustomerName);
            this.content.Controls.Add(this.lblCUSTOMER_NAME);
            this.content.Controls.Add(this.lblCUSTOMER_TYPE);
            this.content.Controls.Add(this.lblAREA);
            this.content.Controls.Add(this.btnOK);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.lblAREA, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_TYPE, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_NAME, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerName, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerType, 0);
            this.content.Controls.SetChildIndex(this.txtAREA, 0);
            this.content.Controls.SetChildIndex(this.lblPREPAYMENT_AMOUNT, 0);
            this.content.Controls.SetChildIndex(this.lblAMOUNT, 0);
            this.content.Controls.SetChildIndex(this.lblNOTE, 0);
            this.content.Controls.SetChildIndex(this.lblBALANCE, 0);
            this.content.Controls.SetChildIndex(this.txtCurrentBalance, 0);
            this.content.Controls.SetChildIndex(this.txtAdjustAmount, 0);
            this.content.Controls.SetChildIndex(this.chkPRINT_RECEIPT, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.label12, 0);
            this.content.Controls.SetChildIndex(this.groupBox2, 0);
            this.content.Controls.SetChildIndex(this.txtLastBalance, 0);
            this.content.Controls.SetChildIndex(this.txtNode, 0);
            this.content.Controls.SetChildIndex(this.lblCURRENCY, 0);
            this.content.Controls.SetChildIndex(this.cboCurrency, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.lblPAYMENT_ACCOUNT, 0);
            this.content.Controls.SetChildIndex(this.cboPaymentAccount, 0);
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            // 
            // groupBox2
            // 
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // chkPRINT_RECEIPT
            // 
            resources.ApplyResources(this.chkPRINT_RECEIPT, "chkPRINT_RECEIPT");
            this.chkPRINT_RECEIPT.Name = "chkPRINT_RECEIPT";
            this.chkPRINT_RECEIPT.UseVisualStyleBackColor = true;
            this.chkPRINT_RECEIPT.CheckedChanged += new System.EventHandler(this.chkPrintInvoice_CheckedChanged);
            // 
            // lblCUSTOMER_TYPE
            // 
            resources.ApplyResources(this.lblCUSTOMER_TYPE, "lblCUSTOMER_TYPE");
            this.lblCUSTOMER_TYPE.Name = "lblCUSTOMER_TYPE";
            // 
            // lblAREA
            // 
            resources.ApplyResources(this.lblAREA, "lblAREA");
            this.lblAREA.Name = "lblAREA";
            // 
            // lblCUSTOMER_NAME
            // 
            resources.ApplyResources(this.lblCUSTOMER_NAME, "lblCUSTOMER_NAME");
            this.lblCUSTOMER_NAME.Name = "lblCUSTOMER_NAME";
            // 
            // txtCustomerName
            // 
            resources.ApplyResources(this.txtCustomerName, "txtCustomerName");
            this.txtCustomerName.Name = "txtCustomerName";
            // 
            // txtCustomerType
            // 
            resources.ApplyResources(this.txtCustomerType, "txtCustomerType");
            this.txtCustomerType.Name = "txtCustomerType";
            // 
            // txtAREA
            // 
            resources.ApplyResources(this.txtAREA, "txtAREA");
            this.txtAREA.Name = "txtAREA";
            // 
            // txtLastBalance
            // 
            resources.ApplyResources(this.txtLastBalance, "txtLastBalance");
            this.txtLastBalance.Name = "txtLastBalance";
            // 
            // txtNode
            // 
            resources.ApplyResources(this.txtNode, "txtNode");
            this.txtNode.Name = "txtNode";
            this.txtNode.Enter += new System.EventHandler(this.InputKhmer);
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Name = "label12";
            // 
            // txtAdjustAmount
            // 
            resources.ApplyResources(this.txtAdjustAmount, "txtAdjustAmount");
            this.txtAdjustAmount.Name = "txtAdjustAmount";
            this.txtAdjustAmount.TextChanged += new System.EventHandler(this.txtAdjustAmount_TextChanged);
            this.txtAdjustAmount.Enter += new System.EventHandler(this.InputEnglish);
            this.txtAdjustAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDecimalOnly);
            // 
            // lblBALANCE
            // 
            resources.ApplyResources(this.lblBALANCE, "lblBALANCE");
            this.lblBALANCE.Name = "lblBALANCE";
            // 
            // lblAMOUNT
            // 
            resources.ApplyResources(this.lblAMOUNT, "lblAMOUNT");
            this.lblAMOUNT.Name = "lblAMOUNT";
            // 
            // lblPREPAYMENT_AMOUNT
            // 
            resources.ApplyResources(this.lblPREPAYMENT_AMOUNT, "lblPREPAYMENT_AMOUNT");
            this.lblPREPAYMENT_AMOUNT.Name = "lblPREPAYMENT_AMOUNT";
            // 
            // txtCurrentBalance
            // 
            this.txtCurrentBalance.AcceptsReturn = true;
            resources.ApplyResources(this.txtCurrentBalance, "txtCurrentBalance");
            this.txtCurrentBalance.Name = "txtCurrentBalance";
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblCURRENCY
            // 
            resources.ApplyResources(this.lblCURRENCY, "lblCURRENCY");
            this.lblCURRENCY.Name = "lblCURRENCY";
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.SelectedIndexChanged += new System.EventHandler(this.cboCurrency_SelectedIndexChanged);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // lblNOTE
            // 
            resources.ApplyResources(this.lblNOTE, "lblNOTE");
            this.lblNOTE.Name = "lblNOTE";
            // 
            // cboPaymentAccount
            // 
            this.cboPaymentAccount.AbsoluteChildrenSelectableOnly = true;
            this.cboPaymentAccount.BranchSeparator = "/";
            this.cboPaymentAccount.Imagelist = null;
            resources.ApplyResources(this.cboPaymentAccount, "cboPaymentAccount");
            this.cboPaymentAccount.Name = "cboPaymentAccount";
            this.cboPaymentAccount.PopupHeight = 250;
            this.cboPaymentAccount.PopupWidth = 200;
            this.cboPaymentAccount.SelectedNode = null;
            // 
            // lblPAYMENT_ACCOUNT
            // 
            resources.ApplyResources(this.lblPAYMENT_ACCOUNT, "lblPAYMENT_ACCOUNT");
            this.lblPAYMENT_ACCOUNT.Name = "lblPAYMENT_ACCOUNT";
            // 
            // DialogPrepaymentAdd
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogPrepaymentAdd";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private GroupBox groupBox2;
        private CheckBox chkPRINT_RECEIPT;
        private Label lblCUSTOMER_TYPE;
        private Label lblAREA;
        private TextBox txtCustomerName;
        private Label lblCUSTOMER_NAME;
        private TextBox txtAREA;
        private TextBox txtCustomerType;
        private TextBox txtLastBalance;
        private TextBox txtNode;
        private Label label12;
        private TextBox txtAdjustAmount;
        private Label lblBALANCE;
        private Label lblAMOUNT;
        private Label lblPREPAYMENT_AMOUNT;
        private TextBox txtCurrentBalance;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private ComboBox cboCurrency;
        private Label lblCURRENCY;
        private Label label9;
        private Label lblNOTE;
        private TreeComboBox cboPaymentAccount;
        private Label lblPAYMENT_ACCOUNT;
    }
}