﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PagePrice
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PagePrice));
            this.dgvPrice = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnEXTRA_CHARGE = new SoftTech.Component.ExButton();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.btnREMOVE = new SoftTech.Component.ExButton();
            this.btnADD = new SoftTech.Component.ExButton();
            this.btnEDIT = new SoftTech.Component.ExButton();
            this.txtSearchPrice = new SoftTech.Component.ExTextbox();
            this.PRICE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRICE_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrice)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvPrice
            // 
            this.dgvPrice.AllowUserToAddRows = false;
            this.dgvPrice.AllowUserToDeleteRows = false;
            this.dgvPrice.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvPrice.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPrice.BackgroundColor = System.Drawing.Color.White;
            this.dgvPrice.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvPrice.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvPrice.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPrice.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PRICE_ID,
            this.PRICE_NAME,
            this.CURRENCY});
            resources.ApplyResources(this.dgvPrice, "dgvPrice");
            this.dgvPrice.EnableHeadersVisualStyles = false;
            this.dgvPrice.Name = "dgvPrice";
            this.dgvPrice.ReadOnly = true;
            this.dgvPrice.RowHeadersVisible = false;
            this.dgvPrice.RowTemplate.Height = 25;
            this.dgvPrice.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPrice_CellDoubleClick);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.btnEXTRA_CHARGE);
            this.panel1.Controls.Add(this.cboCurrency);
            this.panel1.Controls.Add(this.btnREMOVE);
            this.panel1.Controls.Add(this.btnADD);
            this.panel1.Controls.Add(this.btnEDIT);
            this.panel1.Controls.Add(this.txtSearchPrice);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnEXTRA_CHARGE
            // 
            resources.ApplyResources(this.btnEXTRA_CHARGE, "btnEXTRA_CHARGE");
            this.btnEXTRA_CHARGE.Name = "btnEXTRA_CHARGE";
            this.btnEXTRA_CHARGE.UseVisualStyleBackColor = true;
            this.btnEXTRA_CHARGE.Click += new System.EventHandler(this.btnExtraCharge_Click);
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.SelectedIndexChanged += new System.EventHandler(this.txtSearchPrice_QuickSearch);
            // 
            // btnREMOVE
            // 
            resources.ApplyResources(this.btnREMOVE, "btnREMOVE");
            this.btnREMOVE.Name = "btnREMOVE";
            this.btnREMOVE.UseVisualStyleBackColor = true;
            this.btnREMOVE.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnADD
            // 
            resources.ApplyResources(this.btnADD, "btnADD");
            this.btnADD.Name = "btnADD";
            this.btnADD.UseVisualStyleBackColor = true;
            this.btnADD.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnEDIT
            // 
            resources.ApplyResources(this.btnEDIT, "btnEDIT");
            this.btnEDIT.Name = "btnEDIT";
            this.btnEDIT.UseVisualStyleBackColor = true;
            this.btnEDIT.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // txtSearchPrice
            // 
            this.txtSearchPrice.BackColor = System.Drawing.Color.White;
            this.txtSearchPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtSearchPrice, "txtSearchPrice");
            this.txtSearchPrice.Name = "txtSearchPrice";
            this.txtSearchPrice.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtSearchPrice.QuickSearch += new System.EventHandler(this.txtSearchPrice_QuickSearch);
            // 
            // PRICE_ID
            // 
            this.PRICE_ID.DataPropertyName = "PRICE_ID";
            resources.ApplyResources(this.PRICE_ID, "PRICE_ID");
            this.PRICE_ID.Name = "PRICE_ID";
            this.PRICE_ID.ReadOnly = true;
            // 
            // PRICE_NAME
            // 
            this.PRICE_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PRICE_NAME.DataPropertyName = "PRICE_NAME";
            resources.ApplyResources(this.PRICE_NAME, "PRICE_NAME");
            this.PRICE_NAME.Name = "PRICE_NAME";
            this.PRICE_NAME.ReadOnly = true;
            // 
            // CURRENCY
            // 
            this.CURRENCY.DataPropertyName = "CURRENCY_NAME";
            resources.ApplyResources(this.CURRENCY, "CURRENCY");
            this.CURRENCY.Name = "CURRENCY";
            this.CURRENCY.ReadOnly = true;
            // 
            // PagePrice
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dgvPrice);
            this.Controls.Add(this.panel1);
            this.Name = "PagePrice";
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrice)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExTextbox txtSearchPrice;
        private ExButton btnADD;
        private ExButton btnEDIT;
        private DataGridView dgvPrice;
        private ExButton btnREMOVE;
        private ComboBox cboCurrency;
        private ExButton btnEXTRA_CHARGE;
        private DataGridViewTextBoxColumn PRICE_ID;
        private DataGridViewTextBoxColumn PRICE_NAME;
        private DataGridViewTextBoxColumn CURRENCY;
    }
}
