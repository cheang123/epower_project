﻿using System;
using System.Data.Linq;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Windows.Forms;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
namespace EPower.Interface
{
    public partial class DialogReportPowerPurchaseLicenseFee : ExDialog
    {
        CrystalReportHelper ch = null;

        TBL_POWER_PURCHASE _objPurchase = new TBL_POWER_PURCHASE();
        int sourceId = 0;
        string sourceName = "";


        public DialogReportPowerPurchaseLicenseFee(TBL_POWER_PURCHASE obj, int _sourceId, string _sourceName)
        {
            InitializeComponent();
            viewer.DefaultView();
            sourceId = _sourceId;
            sourceName = _sourceName;
            _objPurchase = obj;
        }

        public CrystalReportHelper ViewReportPowerPurchaseLicenseFee()
        {
            var doc = new CrystalReportHelper("ReportPowerPurchaseLicenseFee.rpt", "RPS", "RPSC");
            TBL_COMPANY objCompany = DBDataContext.Db.TBL_COMPANies.FirstOrDefault();
            var location = (from p in DBDataContext.Db.TLKP_PROVINCEs
                            join d in DBDataContext.Db.TLKP_DISTRICTs on p.PROVINCE_CODE equals d.PROVINCE_CODE
                            join com in DBDataContext.Db.TLKP_COMMUNEs on d.DISTRICT_CODE equals com.DISTRICT_CODE
                            join v in DBDataContext.Db.TLKP_VILLAGEs on com.COMMUNE_CODE equals v.COMMUNE_CODE
                            where v.VILLAGE_CODE == objCompany.VILLAGE_CODE
                            select new { p, d, com, v }).FirstOrDefault();

            DateTime month = _objPurchase.MONTH;
            DateTime start = new DateTime(month.Year, month.Month, 1);
            DateTime end = start.AddMonths(1).AddDays(-1);

            doc.SetParameter("@MONTH", month);
            doc.SetParameter("@START", start);
            doc.SetParameter("@END", end);
            doc.SetParameter("@MONTH", month);
            doc.SetParameter("@SOURCE_ID", sourceId);
            doc.SetParameter("@START", start);
            doc.SetParameter("@END", end);
            doc.SetParameter("@PROVINCE_NAME", location.p.PROVINCE_NAME);
            doc.SetParameter("@LICENSE_NO", objCompany.LICENSE_NUMBER);
            doc.SetParameter("@LICENSEE", objCompany.LICENSE_NAME_KH);
            doc.SetParameter("@SOURCE_NAME", sourceName);

            return doc;
        }

        private void viewReport()
        {
            try
            {
                this.ch = ViewReportPowerPurchaseLicenseFee();
                this.viewer.ReportSource = ch.Report;
            }
            catch(Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_VIEW_REPORT, Base.Properties.Resources.WARNING);
            }
        }
        public static void GetFile(string _fileName, Binary attachment)
        {
            try
            {
                string fileName = System.IO.Path.Combine(Application.StartupPath + "\\TEMP", _fileName);
                using (FileStream stream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.Read))
                {
                    stream.Write(attachment.ToArray(), 0, attachment.Length);
                }
            }
            catch { }
        }
        private void clearFolder(string FolderName)
        {
            DirectoryInfo dir = new DirectoryInfo(FolderName);

            foreach (FileInfo fi in dir.GetFiles())
            {
                fi.Delete();
            }

            foreach (DirectoryInfo di in dir.GetDirectories())
            {
                clearFolder(di.FullName);
                di.Delete();
            }
        }
        public static long DirSize(DirectoryInfo d)
        {
            long size = 0;
            // Add file sizes.
            FileInfo[] fis = d.GetFiles();
            foreach (FileInfo fi in fis)
            {
                size += fi.Length;
            }
            // Add subdirectory sizes.
            DirectoryInfo[] dis = d.GetDirectories();
            foreach (DirectoryInfo di in dis)
            {
                size += DirSize(di);
            }
            return size;
        }
        private void sendMail()
        {
            try
            {
                clearFolder(Settings.Default.PATH_TEMP);
                this.viewReport();
                DateTime month = _objPurchase.MONTH;
                var purchases = DBDataContext.Db.TBL_POWER_PURCHASEs.Where(x => x.MONTH == month && x.IS_ACTIVE && x.ATTACHMENT_ID != 0).Select(x => x.ATTACHMENT_ID);
                var attachments = DBDataContext.Db.TBL_ATTACHMENTs.Where(x => purchases.Contains(x.ATTACHMENT_ID));
                TBL_COMPANY com = null;
                com = DBDataContext.Db.TBL_COMPANies.FirstOrDefault();
                string licensename = com.LICENSE_NUMBER + " - ";

                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + this.ch.ReportName + ".pdf").FullName;
                this.ch.ExportToPDF(exportPath);
                DialogSendMail diag = new DialogSendMail();
                diag.chkMERG_FILE.Checked = false;
                diag.chkMERG_FILE.Visible = false;
                //diag.txtTo.Text = "eac.billings@gmail.com";
                diag.txtTo.Text = Method.utilities[Utility.EAC_BILLING_EMAIL];
                diag.txtSubject.Text = licensename + "Power Electricity " + month.Year + "-" + month.ToString("MM");
                //diag.Add(this.ch.ReportName, exportPath);
                DirectoryInfo dir = new DirectoryInfo(Settings.Default.PATH_TEMP);

                if (attachments.Count() == 0)
                {
                    diag.Add(this.ch.ReportName, exportPath);
                }
                else
                {
                    Runner.Run(() =>
                    {
                        foreach (var attachment in attachments)
                        {
                            GetFile(attachment.ATTACHMENT_NAME, attachment.ATTACHMENT);
                            //diag.Add(attachment.ATTACHMENT_NAME, Settings.Default.PATH_TEMP + attachment.ATTACHMENT_NAME);
                        }
                        ZipFile.CreateFromDirectory(Settings.Default.PATH_TEMP, "Attachments.zip");
                        //string zippath = Settings.Default + @"\Attachments";
                        //diag.Add("Attachments", zippath);
                        clearFolder(Settings.Default.PATH_TEMP);
                        string fileName = "Attachments.zip";
                        string source = Application.StartupPath + @"\";
                        string dest = Settings.Default.PATH_TEMP;
                        string sourceFile = Path.Combine(source, fileName);
                        string destFile = Path.Combine(dest, fileName);
                        File.Move(sourceFile, destFile);
                        long FilesSizes = DirSize(dir);
                        diag.Add(fileName + " (" + (FilesSizes / 1000000).ToString("0.00") + "MBs)", destFile);
                    });

                }
                long FilesSize = DirSize(dir);
                if (FilesSize >= 25000000)
                {
                    MsgBox.ShowInformation(Base.Properties.Resources.SIZE_OVER_25_MB_CANNOT_SEND_EMAIL);
                    return;
                }
                else
                {
                    var date = DBDataContext.Db.GetSystemDate();
                    var lastSend = DBDataContext.Db.TBL_POWER_PURCHASE_SENDs.OrderByDescending(x => x.POWER_PURCHASE_SEND_ID).FirstOrDefault(x => x.DATA_MONTH == month && x.IS_LAST == true);
                    var invoice = (from p in DBDataContext.Db.TBL_POWER_PURCHASEs where p.MONTH == month && p.IS_ACTIVE == true select p.MONTH).Count();

                    if (lastSend == null)
                    {
                        diag.ShowDialog();
                        if (diag.success == true)
                        {

                            TBL_POWER_PURCHASE_SEND pp = new TBL_POWER_PURCHASE_SEND
                            {
                                DATA_MONTH = month,
                                CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                                CREATED_ON = date,
                                IS_LAST = true,
                                ROW_DATE = date,
                                NOTE = " ",
                                TOTAL_INVOICE = invoice
                            };
                            DBDataContext.Db.TBL_POWER_PURCHASE_SENDs.InsertOnSubmit(pp);
                            DBDataContext.Db.SubmitChanges();
                        }
                    }
                    else
                    {
                        //if (MsgBox.ShowQuestion(Resources.REPORT_HAS_SEND_DO_YOU_REALLY_WANT_TO_SEND_AGAIN, Resources.NOTE) == DialogResult.Yes)
                        //{

                        //}
                        DialogNoteReason di = new DialogNoteReason(Resources.REPORT_POWER_PURCHASE_LICENSE_FEE);
                        di.ShowDialog();
                        if (di.DialogResult == DialogResult.OK)
                        {
                            diag.ShowDialog();
                            if (diag.success == true)
                            {
                                //update old record
                                TBL_POWER_PURCHASE_SEND _obj = lastSend;
                                _obj.IS_LAST = false;
                                //_obj.NOTE = "Canceled";
                                DBDataContext.Db.SubmitChanges();
                                //Insert new row
                                TBL_POWER_PURCHASE_SEND pp = new TBL_POWER_PURCHASE_SEND
                                {
                                    DATA_MONTH = month,
                                    CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                                    CREATED_ON = date,
                                    IS_LAST = true,
                                    ROW_DATE = date,
                                    NOTE = di.txtREASON.Text,
                                    TOTAL_INVOICE = invoice
                                };
                                DBDataContext.Db.TBL_POWER_PURCHASE_SENDs.InsertOnSubmit(pp);
                                DBDataContext.Db.SubmitChanges();
                            }
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_SEND_EMAIL, Base.Properties.Resources.WARNING);
            }
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            this.sendMail();
        }

        private void DialogReportTaxDetail_Load(object sender, EventArgs e)
        {
            this.viewReport();
        }

    }
}