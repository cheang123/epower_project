﻿using System.ComponentModel;
using System.Windows.Forms;
using AxCrystalActiveXReportViewerLib105;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageReportSubsidy
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageReportSubsidy));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnPRINT = new SoftTech.Component.ExButton();
            this.chkIS_HAVE_ZERO_USAGE = new System.Windows.Forms.CheckBox();
            this.btnSETUP = new SoftTech.Component.ExButton();
            this.cboArea = new System.Windows.Forms.ComboBox();
            this.cboBillingCycle = new System.Windows.Forms.ComboBox();
            this.cboReport = new System.Windows.Forms.ComboBox();
            this.btnMAIL = new SoftTech.Component.ExButton();
            this.dtpYear = new System.Windows.Forms.DateTimePicker();
            this.btnVIEW = new SoftTech.Component.ExButton();
            this.viewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.btnPRINT);
            this.panel1.Controls.Add(this.chkIS_HAVE_ZERO_USAGE);
            this.panel1.Controls.Add(this.btnSETUP);
            this.panel1.Controls.Add(this.cboArea);
            this.panel1.Controls.Add(this.cboBillingCycle);
            this.panel1.Controls.Add(this.cboReport);
            this.panel1.Controls.Add(this.btnMAIL);
            this.panel1.Controls.Add(this.dtpYear);
            this.panel1.Controls.Add(this.btnVIEW);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnPRINT
            // 
            resources.ApplyResources(this.btnPRINT, "btnPRINT");
            this.btnPRINT.Name = "btnPRINT";
            this.btnPRINT.UseVisualStyleBackColor = true;
            this.btnPRINT.Click += new System.EventHandler(this.btnPRINT_Click);
            // 
            // chkIS_HAVE_ZERO_USAGE
            // 
            resources.ApplyResources(this.chkIS_HAVE_ZERO_USAGE, "chkIS_HAVE_ZERO_USAGE");
            this.chkIS_HAVE_ZERO_USAGE.Name = "chkIS_HAVE_ZERO_USAGE";
            this.chkIS_HAVE_ZERO_USAGE.UseVisualStyleBackColor = true;
            // 
            // btnSETUP
            // 
            resources.ApplyResources(this.btnSETUP, "btnSETUP");
            this.btnSETUP.Name = "btnSETUP";
            this.btnSETUP.UseVisualStyleBackColor = true;
            this.btnSETUP.Click += new System.EventHandler(this.btnSETUP_Click);
            // 
            // cboArea
            // 
            this.cboArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboArea.DropDownWidth = 150;
            this.cboArea.FormattingEnabled = true;
            resources.ApplyResources(this.cboArea, "cboArea");
            this.cboArea.Name = "cboArea";
            // 
            // cboBillingCycle
            // 
            this.cboBillingCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBillingCycle.DropDownWidth = 150;
            this.cboBillingCycle.FormattingEnabled = true;
            resources.ApplyResources(this.cboBillingCycle, "cboBillingCycle");
            this.cboBillingCycle.Name = "cboBillingCycle";
            // 
            // cboReport
            // 
            this.cboReport.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboReport.FormattingEnabled = true;
            resources.ApplyResources(this.cboReport, "cboReport");
            this.cboReport.Name = "cboReport";
            this.cboReport.SelectedIndexChanged += new System.EventHandler(this.cboReport_SelectedIndexChanged);
            // 
            // btnMAIL
            // 
            resources.ApplyResources(this.btnMAIL, "btnMAIL");
            this.btnMAIL.Name = "btnMAIL";
            this.btnMAIL.UseVisualStyleBackColor = true;
            this.btnMAIL.Click += new System.EventHandler(this.btnMail_Click);
            // 
            // dtpYear
            // 
            resources.ApplyResources(this.dtpYear, "dtpYear");
            this.dtpYear.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpYear.Name = "dtpYear";
            // 
            // btnVIEW
            // 
            resources.ApplyResources(this.btnVIEW, "btnVIEW");
            this.btnVIEW.Name = "btnVIEW";
            this.btnVIEW.UseVisualStyleBackColor = true;
            this.btnVIEW.Click += new System.EventHandler(this.btnViewReport_Click);
            // 
            // viewer
            // 
            this.viewer.ActiveViewIndex = -1;
            this.viewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.viewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.viewer.DisplayStatusBar = false;
            resources.ApplyResources(this.viewer, "viewer");
            this.viewer.Name = "viewer";
            // 
            // PageReportSubsidy
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.viewer);
            this.Controls.Add(this.panel1);
            this.Name = "PageReportSubsidy";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnVIEW;
        private DateTimePicker dtpYear;
        private ExButton btnMAIL;
        private ComboBox cboReport;
        private ComboBox cboArea;
        private ComboBox cboBillingCycle;
        private ExButton btnSETUP;
        private CheckBox chkIS_HAVE_ZERO_USAGE;
        private ExButton btnPRINT;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer viewer;
    }
}
