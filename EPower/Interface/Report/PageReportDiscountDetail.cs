﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageReportDiscountDetail : Form
    {
        CrystalReportHelper ch = null;
        public PageReportDiscountDetail()
        {
            InitializeComponent();
            viewer.DefaultView();
            dataLookup();
        }
        

        private void dataLookup()
        {
            try
            {
                var a = from ar in DBDataContext.Db.TBL_AREAs
                        where ar.IS_ACTIVE
                        orderby ar.AREA_NAME
                        select new { ar.AREA_ID, ar.AREA_NAME };
                UIHelper.SetDataSourceToComboBox(cboBillingCycle, a, Resources.ALL_AREA);

                TBL_BILLING_CYCLE objBillingCycle = DBDataContext.Db.TBL_BILLING_CYCLEs.FirstOrDefault(row => row.IS_ACTIVE);
                if (objBillingCycle != null)
                {
                    DateTime datCurrentMonth=Method.GetNextBillingMonth(objBillingCycle.CYCLE_ID).AddMonths(-1);
                    this.dtpDate.Value = new DateTime(datCurrentMonth.Year,datCurrentMonth.Month,1) ;
                }
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp);
            }            
        }
       
        private void btnViewReport_Click(object sender, EventArgs e)
        {
            Runner.Run(this.viewReport);
        }

        public CrystalReportHelper ViewReportDiscountDetail(int intArea,DateTime dtMonth)
        {
            CrystalReportHelper c = new CrystalReportHelper("ReportDiscountDetail.rpt");
            c.SetParameter("@AREA_ID", intArea);
            c.SetParameter("@MONTH", dtMonth);
            return c;
        }

        private void viewReport()
        {
            try
            {
                this.ch = ViewReportDiscountDetail((int)cboBillingCycle.SelectedValue, new DateTime(dtpDate.Value.Year, dtpDate.Value.Month, 1));
                this.viewer.ReportSource = ch.Report;
            }
            catch(Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_VIEW_REPORT, Base.Properties.Resources.WARNING);
            }
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            this.sendMail();
        }

        private void sendMail()
        {
            try
            {
                this.viewReport();
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + this.ch.ReportName + ".xls").FullName;
                this.ch.ExportToExcel(exportPath);
                DialogSendMail.Instance.Add(this.ch.ReportName, exportPath);
                DialogSendMail.Instance.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_SEND_EMAIL, Base.Properties.Resources.WARNING);
            }
        }

        private void PageReportDiscountDetail_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                dataLookup();
            }
        }
        private void cboSaveState_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            if (cbo.SelectedIndex==-1)
            {
                return;
            }
            cbo.Tag = cbo.SelectedValue;
        }
    }
}
