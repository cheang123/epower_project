﻿using EPower.Base.Logic;
using EPower.Interface.Report;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageReportCustomerUsageSummary : Form
    {
        private CrystalReportHelper ch = null;

        DialogReportCustomerUsageSummaryOption dialog = new DialogReportCustomerUsageSummaryOption();
        public PageReportCustomerUsageSummary()
        {
            InitializeComponent();
            viewer.DefaultView();
            bind();
            TBL_BILLING_CYCLE objBillingCycle = DBDataContext.Db.TBL_BILLING_CYCLEs.FirstOrDefault(row => row.IS_ACTIVE);
            if (objBillingCycle != null)
            {
                DateTime datCurrentMonth = Method.GetNextBillingMonth(objBillingCycle.CYCLE_ID).AddMonths(-1);
                this.dtpDate.Value = new DateTime(datCurrentMonth.Year, datCurrentMonth.Month, 1);
            }
        }

        private void bind()
        {
            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("REPORT_ID", typeof(int));
            dtReport.Columns.Add("REPORT_NAME", typeof(string));
            dtReport.Rows.Add(1, Resources.REPORT_CUSTOMER_USAGE_SUMMARY_BY_AREA);
            dtReport.Rows.Add(2, Resources.REPORT_CUSTOMER_USAGE_SUMMARY_BY_POWER);
            UIHelper.SetDataSourceToComboBox(cboReport, dtReport, "REPORT_ID", "REPORT_NAME");
        }

        //private void dataLookup()
        //{ 
        //    UIHelper.SetDataSourceToComboBox(this.cboCustomerType, DBDataContext.Db.TBL_CUSTOMER_TYPEs.Where(row => row.IS_ACTIVE || row.CUSTOMER_TYPE_ID == -1)._ToDataTable(),Resources.ALL_TYPE);
        //    this.cboCustomerType.SelectedValue = cboCustomerType.Tag; 
        //    UIHelper.SetDataSourceToComboBox(this.cboPrice, DBDataContext.Db.TBL_PRICEs._ToDataTable(),Resources.ALL_PRICE);
        //    this.cboPrice.SelectedValue = cboPrice.Tag; 
        //    UIHelper.SetDataSourceToComboBox(this.cboArea, DBDataContext.Db.TBL_AREAs.Where(row => row.IS_ACTIVE)._ToDataTable(),Resources.ALL_AREA);
        //    this.cboArea.SelectedValue = cboArea.Tag;
        //    UIHelper.SetDataSourceToComboBox(this.cboCycle, DBDataContext.Db.TBL_BILLING_CYCLEs.Where(row => row.IS_ACTIVE)._ToDataTable(),Resources.ALL_CYCLE);
        //    this.cboCycle.SelectedValue = cboCycle.Tag;
        //}

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            Runner.Run(this.viewReport);
        }


        public CrystalReportHelper ViewReportCustomerUsageSummary(int intPrice, int intCycle, int intCusGroupType, int intCusType, int intArea
            , DateTime dtMonth, string strPrice, string strCycle, string strGroup, string strCustType, string strArea)
        {
            if (cboReport.SelectedIndex == -1)
            {
                cboReport.SelectedIndex = 0;
            }
            CrystalReportHelper c = new CrystalReportHelper(cboReport.SelectedIndex == 1 ? "ReportCustomerUsageSummary.rpt" : "ReportCustomerUsed.rpt");
            c.SetParameter("@PRICE_ID", intPrice);
            c.SetParameter("@CYCLE_ID", intCycle);
            c.SetParameter("@CUSTOMER_GROUP_TYPE_ID", intCusGroupType);
            c.SetParameter("@CUSTOMER_CONNECTION_TYPE_ID", intCusType);
            c.SetParameter("@AREA_ID", intArea);
            c.SetParameter("@DATE", dtMonth);

            c.SetParameter("@PRICE", strPrice);
            c.SetParameter("@CYCLE", strCycle);
            c.SetParameter("@CUSTOMER_GROUP", strGroup);
            c.SetParameter("@CUSTOMER_CONNECTION_TYPE", strCustType);
            c.SetParameter("@AREA", strArea);
            return c;
        }

        private void viewReport()
        {
            try
            {
                DateTime dt = new DateTime(dtpDate.Value.Year, dtpDate.Value.Month, 1);
                this.ch = ViewReportCustomerUsageSummary((int)dialog.cboPrice.SelectedValue,
                        (int)dialog.cboCycle.SelectedValue,
                        (int)dialog.cboCustomerGroup.SelectedValue,
                        (int)dialog.cboCustomerConnection.SelectedValue,
                        (int)dialog.cboArea.SelectedValue, dt,
                        dialog.cboPrice.Text,
                        dialog.cboCycle.Text,
                        dialog.cboCustomerGroup.Text,
                        dialog.cboCustomerConnection.Text,
                        dialog.cboArea.Text);
                this.viewer.ReportSource = ch.Report;
            }
            catch(Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_VIEW_REPORT, Base.Properties.Resources.WARNING);
            }
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            sendMail();
        }

        private void sendMail()
        {
            try
            {
                this.viewReport();
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + this.ch.ReportName + ".xls").FullName;
                this.ch.ExportToExcel(exportPath);
                DialogSendMail.Instance.Add(this.ch.ReportName, exportPath);
                DialogSendMail.Instance.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_SEND_EMAIL, Base.Properties.Resources.WARNING);
            }
        }

        //private void PageReportCustomerUsageSummary_VisibleChanged(object sender, EventArgs e)
        //{
        //    if (this.Visible)
        //    {
        //        dataLookup();
        //    }
        //}

        private void cboSaveState_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            if (cbo.SelectedIndex==-1)
            {
                return;
            }
            cbo.Tag = cbo.SelectedValue;
        }

        private void btnSETUP_Click(object sender, EventArgs e)
        {
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                Runner.Run(this.viewReport);
            }
        }
    }
}
