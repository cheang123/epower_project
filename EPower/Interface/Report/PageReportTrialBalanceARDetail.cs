﻿using System;
using System.Data;
using System.IO;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System.Drawing;
using System.Collections.Generic;
using EPower.Base.Logic;

namespace EPower.Interface
{
    public partial class PageReportTrialBalanceARDetail : Form
    {
        CrystalReportHelper ch = null;

        public PageReportTrialBalanceARDetail()
        {
            try
            {
                InitializeComponent();
                viewer.DefaultView();
                dtp1.Value = DBDataContext.Db.GetSystemDate().Date;
                dtp2.Value = dtp1.Value.AddDays(1).AddSeconds(-1);
                diag.cboOrderBy.Visible = false;
                bind();
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }

        }

        private void bind()
        {
            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("REPORT_ID", typeof(int));
            dtReport.Columns.Add("REPORT_NAME", typeof(string));
            dtReport.Rows.Add(1, Resources.PAYMENT);
            dtReport.Rows.Add(2, Resources.PAYMENT_BY_RECIEPT);
            dtReport.Rows.Add(3, Resources.INVOICE);
            dtReport.Rows.Add(4, Resources.ADJUST);
            dtReport.Rows.Add(5, Resources.CANCEL_PAYMENT);
            dtReport.Rows.Add(6, Resources.SUMMARY);
            dtReport.Rows.Add(7, Resources.DEPOSIT_CONNECTION_FEE);
            dtReport.Rows.Add(8, Resources.PAYMENT_TOTAL);
            dtReport.Rows.Add(9, Resources.PAYMENT_SUMMARY);
            dtReport.Rows.Add(10, Resources.TOTAL_SUMMARY_PAYMENT_MONTHLY_AND_AMPERE);
            dtReport.Rows.Add(11, Resources.PAYMENT_SUMMARY_BY_AREA);
            dtReport.Rows.Add(12, Resources.INVOICE_REVERSED);
            UIHelper.SetDataSourceToComboBox(cboReport, dtReport, "REPORT_ID", "REPORT_NAME");
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            Runner.Run(this.viewReport);
        }

        private void viewReport()
        {
            try
            {
                if (cboReport.SelectedIndex == -1)
                {
                    MsgBox.ShowInformation(string.Format(Resources.REQUIRED, Resources.REPORT));
                    return;
                }

                int intReportId = DataHelper.ParseToInt(cboReport.SelectedValue.ToString());

                var type = "ReportInvoiceDetail.rpt";
                if (intReportId == 1)
                {
                    type = "ReportPaymentDetail.rpt";
                }
                else if (intReportId == 2)
                {
                    type = "ReportPaymentDetailByReciept.rpt";
                }
                else if (intReportId == 3)
                {
                    type = "ReportInvoiceDetail.rpt";
                }
                else if (intReportId == 4)
                {
                    type = "ReportInvoiceAdjustmentDetail.rpt";
                }
                else if (intReportId == 5)
                {
                    type = "ReportPaymentCancelDetail.rpt";
                }
                else if (intReportId == 6)
                {
                    type = "ReportPaymentSummary.rpt";
                }
                else if (intReportId == 7)
                {
                    type = "ReportCashReceiveDepositConnectionFee.rpt";
                }
                else if (intReportId == 8)
                {
                    type = "ReportPaymentTotalDetail.rpt";
                }
                else if (intReportId == 9)
                {
                    type = "ReportPaymentTotal.rpt";
                }
                else if (intReportId == 10)
                {
                    type = "ReportPaymentTotalSummaryByMonthAndAmpare.rpt";
                }
                else if (intReportId == 11)
                {
                    type = "ReportPaymentSummaryByArea.rpt";
                }
                else if (intReportId == 12)
                {
                    type = "ReportInvoiceReversed.rpt";
                }
                ch = ViewReportTrailBalanceARDetail(type,
                        dtp1.Value,
                        dtp2.Value,
                        (int)diag.cboItemType.SelectedValue,
                        diag.cboItem.SelectedValue == null ? 0 : (int)diag.cboItem.SelectedValue,
                        (int)diag.cboCurrency.SelectedValue,
                        diag.cboCurrency.Text,
                        (int)diag.cboArea.SelectedValue,
                        diag.cboArea.Text,
                        (int)diag.cboBillingCycle.SelectedValue,
                        diag.cboBillingCycle.Text
                );
                if (intReportId == 1)
                {
                    ch.SetParameter("@CASH_DRAWER_ID", (int)diag.cboCashDrawer.SelectedValue);
                    ch.SetParameter("@CASH_DRAWER_NAME", diag.cboCashDrawer.Text);
                }
                if (intReportId == 1 || intReportId == 2 || intReportId == 8 || intReportId == 9 || intReportId == 10)
                {
                    ch.SetParameter("@INCLUDE_DELETE", this.chkINCLUDE_DELETE.Checked);
                }
                if (intReportId == 1 || intReportId == 2 || intReportId == 8 || intReportId == 9 || intReportId == 10 || intReportId == 11)
                {
                    ch.SetParameter("@PAYMENT_ACCOUNT_ID", (int?)diag.cboPaymentAccount.EditValue ?? 0);
                    ch.SetParameter("@PAYMENT_ACCOUNT_NAME", string.IsNullOrEmpty(diag.cboPaymentAccount.Text) ? Resources.ALL_ACCOUNT : diag.cboPaymentAccount.Text);
                }
                if (intReportId == 1 || intReportId == 2 || intReportId == 3 || intReportId == 4 || intReportId == 5 || intReportId == 12)
                {
                    DateTime dt = new DateTime(diag.dtpMonth.Value.Year, diag.dtpMonth.Value.Month, 1);
                    ch.SetParameter("@DATA_MONTH", diag.dtpMonth.Checked ? dt : UIHelper._DefaultDate);
                    ch.SetParameter("@DATA_MONTH_NAME", diag.dtpMonth.Checked ? diag.dtpMonth.Value.ToString("MM-yyyy") : Resources.ALL_DATA_MONTH);
                }
                if (intReportId == 3)
                {
                    ch.SetParameter("@ORDER_BY", diag.cboOrderBy.SelectedIndex);
                }
                if (intReportId != 12)
                {
                    ch.SetParameter("@ITEM_TYPE_ID", diag.cboItemType.SelectedValue);
                    ch.SetParameter("@ITEM_ID", diag.cboItem.SelectedValue);
                }
                this.viewer.ReportSource = ch.Report;
            }
            catch(Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_VIEW_REPORT, Base.Properties.Resources.WARNING);
            }
        } 

        public CrystalReportHelper ViewReportTrailBalanceARDetail(string type, DateTime dtStart, DateTime dtEnd, int itemTypeId, int itemId, int currencyId,
                                                                string currencyName, int areaId, string areaName, int billingCycleId, string billingCycleName)
        {
            CrystalReportHelper c = new CrystalReportHelper(type);
            c.SetParameter("@D1", dtStart);
            c.SetParameter("@D2", dtEnd);
            c.SetParameter("@CURRENCY_ID", currencyId);
            c.SetParameter("@CURRENCY_NAME", currencyName);
            c.SetParameter("@AREA_ID", areaId);
            c.SetParameter("@AREA_NAME", areaName);
            c.SetParameter("@BILLING_CYCLE_ID", billingCycleId);
            c.SetParameter("@BILLING_CYCLE_NAME", billingCycleName);
            return c;
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            this.sendMail();
        }

        private void sendMail()
        {
            try
            {
                this.viewReport();
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + this.ch.ReportName + ".xls").FullName;
                this.ch.ExportToExcel(exportPath);
                DialogSendMail.Instance.Add(this.ch.ReportName, exportPath);
                DialogSendMail.Instance.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_SEND_EMAIL, Base.Properties.Resources.WARNING);
            }
        }

        private void cboReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            chkINCLUDE_DELETE.Visible = cboReport.SelectedIndex == 0 || cboReport.SelectedIndex == 1;
            var k = new List<int>() { 1, 2, 8, 9, 10, 11 };
            if (k.Contains((int)cboReport.SelectedValue))
            {
                this.dtp1.CustomFormat = this.dtp2.CustomFormat = "dd-MM-yyyy HH:mm:ss";
            }
            else
            {
                this.dtp1.CustomFormat = this.dtp2.CustomFormat = "dd-MM-yyyy";
            }
            //chkINCLUDE_DELETE.Visible = cboReport.SelectedIndex == 1;
        }

        DialogReportTrialBalanceARDetailOption diag = new DialogReportTrialBalanceARDetailOption();
        private void btnSetup_Click(object sender, EventArgs e)
        {
            diag.Height = 340;
            int intReportId = DataHelper.ParseToInt(cboReport.SelectedValue.ToString());
            diag.cboPaymentAccount.Visible =
            diag.lblPAYMENT_ACCOUNT.Visible = !(intReportId == 3 || intReportId == 4 || intReportId == 5 || intReportId == 6 || intReportId == 7 || intReportId == 12);

            diag.dtpMonth.Visible =
            diag.lblMONTH.Visible = (intReportId == 1 || intReportId == 2 || intReportId == 3 || intReportId == 4 || intReportId == 5 || intReportId == 12);

            diag.lblMONTH.Location = intReportId == 1 || intReportId == 2 ? new Point(10, 203) : new Point(10, 170);
            diag.dtpMonth.Location = intReportId == 1 || intReportId == 2 ? new Point(121, 198) : new Point(121, 167);
            diag.lblCASH_DRAWER.Visible = intReportId == 1 ? true : false;
            diag.cboCashDrawer.Visible = intReportId == 1 ? true : false;
            diag.lblCASH_DRAWER.Location = intReportId == 1 ? new Point(10, 234) : new Point(10, 255);
            diag.cboCashDrawer.Location = intReportId == 1 ? new Point(121, 231) : new Point(121, 262);


            diag.cboOrderBy.Visible =
            diag.lblORDER.Visible = (intReportId == 3 || intReportId == 12);
            diag.lblORDER.Location = (intReportId == 3 || intReportId == 12) ? new Point(10, 203) : new Point(10, 136);
            diag.cboOrderBy.Location = (intReportId == 3 || intReportId == 12) ? new Point(121, 198) : new Point(121, 144);

            if (intReportId > 3 && intReportId < 8)
            {
                diag.Height = 280;
            }

            if (intReportId == 12 || intReportId == 7)
            {

                diag.cboItem.Visible =
                diag.lblSERVICE.Visible = false;

                diag.cboItemType.Visible =
                diag.lblSERVICE_TYPE.Visible = false;

                diag.cboCurrency.Location = diag.cboItemType.Location;
                diag.lblCURRENCY.Location = diag.lblSERVICE_TYPE.Location;

                diag.lblMONTH.Location = diag.lblSERVICE.Location;
                diag.dtpMonth.Location = diag.cboItem.Location;

                diag.Height = 200;
            }
            else
            {
                diag.cboItem.Visible =
                diag.lblSERVICE.Visible = true;

                diag.cboItemType.Visible =
                diag.lblSERVICE_TYPE.Visible = true;

                diag.cboCurrency.Location = new Point(121, 136);
                diag.lblCURRENCY.Location = new Point(12, 137);
            }

            //Ignore filter 
            //-6,-7 =>Customer deposit and customer prepayment
            var ignoreFilterReports = new List<int>() { 12, 3, 4 };
            var selectedReport = (int)cboReport.SelectedValue;
            if (ignoreFilterReports.Contains(selectedReport))
            {
                diag._ignoreIds = new List<int>()
                {
                    -6,-7
                };
            }
            else
            {
                diag._ignoreIds = new List<int>() { };
            }
            if (diag.ShowDialog() == DialogResult.OK)
            {
                Runner.Run(viewReport);
            }
        }
    }
}
