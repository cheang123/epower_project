﻿
namespace EPower.Interface.Report
{
    partial class PageReportPrepaidPaymentHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblYEAR = new System.Windows.Forms.Label();
            this.dtpRunYear = new System.Windows.Forms.DateTimePicker();
            this.cboBillingCycle = new System.Windows.Forms.ComboBox();
            this.lblCYCLE_NAME = new System.Windows.Forms.Label();
            this.btnMail = new SoftTech.Component.ExButton();
            this.btnViewReport = new SoftTech.Component.ExButton();
            this.viewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.lblYEAR);
            this.panel1.Controls.Add(this.dtpRunYear);
            this.panel1.Controls.Add(this.cboBillingCycle);
            this.panel1.Controls.Add(this.lblCYCLE_NAME);
            this.panel1.Controls.Add(this.btnMail);
            this.panel1.Controls.Add(this.btnViewReport);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(798, 34);
            this.panel1.TabIndex = 2;
            // 
            // lblYEAR
            // 
            this.lblYEAR.AutoSize = true;
            this.lblYEAR.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblYEAR.Location = new System.Drawing.Point(236, 8);
            this.lblYEAR.Name = "lblYEAR";
            this.lblYEAR.Size = new System.Drawing.Size(45, 19);
            this.lblYEAR.TabIndex = 11;
            this.lblYEAR.Text = "ប្រចាំឆ្នាំ";
            // 
            // dtpRunYear
            // 
            this.dtpRunYear.CustomFormat = "yyyy";
            this.dtpRunYear.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpRunYear.Location = new System.Drawing.Point(287, 4);
            this.dtpRunYear.Name = "dtpRunYear";
            this.dtpRunYear.ShowUpDown = true;
            this.dtpRunYear.Size = new System.Drawing.Size(76, 27);
            this.dtpRunYear.TabIndex = 10;
            // 
            // cboBillingCycle
            // 
            this.cboBillingCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBillingCycle.FormattingEnabled = true;
            this.cboBillingCycle.Location = new System.Drawing.Point(80, 4);
            this.cboBillingCycle.Name = "cboBillingCycle";
            this.cboBillingCycle.Size = new System.Drawing.Size(150, 27);
            this.cboBillingCycle.TabIndex = 9;
            // 
            // lblCYCLE_NAME
            // 
            this.lblCYCLE_NAME.AutoSize = true;
            this.lblCYCLE_NAME.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblCYCLE_NAME.Location = new System.Drawing.Point(4, 8);
            this.lblCYCLE_NAME.Name = "lblCYCLE_NAME";
            this.lblCYCLE_NAME.Size = new System.Drawing.Size(45, 19);
            this.lblCYCLE_NAME.TabIndex = 8;
            this.lblCYCLE_NAME.Text = "ជុំទូទាត់";
            // 
            // btnMail
            // 
            this.btnMail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMail.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnMail.Location = new System.Drawing.Point(721, 6);
            this.btnMail.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnMail.Name = "btnMail";
            this.btnMail.Size = new System.Drawing.Size(71, 23);
            this.btnMail.TabIndex = 7;
            this.btnMail.Text = "អ៊ីមែល";
            this.btnMail.UseVisualStyleBackColor = true;
            this.btnMail.Click += new System.EventHandler(this.btnMail_Click);
            // 
            // btnViewReport
            // 
            this.btnViewReport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewReport.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnViewReport.Location = new System.Drawing.Point(645, 6);
            this.btnViewReport.Margin = new System.Windows.Forms.Padding(2);
            this.btnViewReport.Name = "btnViewReport";
            this.btnViewReport.Size = new System.Drawing.Size(71, 23);
            this.btnViewReport.TabIndex = 5;
            this.btnViewReport.Text = "មើល";
            this.btnViewReport.UseVisualStyleBackColor = true;
            this.btnViewReport.Click += new System.EventHandler(this.btnViewReport_Click);
            // 
            // viewer
            // 
            this.viewer.ActiveViewIndex = -1;
            this.viewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.viewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.viewer.DisplayStatusBar = false;
            this.viewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.viewer.Location = new System.Drawing.Point(0, 34);
            this.viewer.Name = "viewer";
            this.viewer.Size = new System.Drawing.Size(798, 356);
            this.viewer.TabIndex = 3;
            this.viewer.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // PageReportPrepaidPaymentHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.viewer);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "PageReportPrepaidPaymentHistory";
            this.Size = new System.Drawing.Size(798, 390);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private SoftTech.Component.ExButton btnMail;
        private SoftTech.Component.ExButton btnViewReport;
        private System.Windows.Forms.Label lblYEAR;
        private System.Windows.Forms.DateTimePicker dtpRunYear;
        private System.Windows.Forms.ComboBox cboBillingCycle;
        private System.Windows.Forms.Label lblCYCLE_NAME;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer viewer;
    }
}