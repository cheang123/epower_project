﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface.Report
{
    public partial class DialogReportCustomerOption : ExDialog
    {


        public DialogReportCustomerOption()
        {
            InitializeComponent();
            bind();
        }
        #region Method
        public void bind()
        {
            UIHelper.SetDataSourceToComboBox(cboArea, Lookup.GetAreas(), Resources.ALL_AREA);
            UIHelper.SetDataSourceToComboBox(cboCustomerGroup, Lookup.GetCustomerGroup(), Resources.All_CUSTOMER_GROUP);
            UIHelper.SetDataSourceToComboBox(cboBillingCycle, Lookup.GetBillingCycles(), Resources.ALL_CYCLE);
            UIHelper.SetDataSourceToComboBox(cboPrice, Lookup.GetPrices(), Resources.ALL_PRICE);
            UIHelper.SetDataSourceToComboBox(cboCustomerStatus, Lookup.GetCustomCustomerStatused(), Resources.ALL_STATUS);
            UIHelper.SetDataSourceToComboBox(cboAmpare, Lookup.GetPowerAmpare(), Resources.ALL_AMPARE);
            cboCustomerStatus.SelectedIndex = 1;
        }
        #endregion

        private void cboCUSTOMER_GROUP_SelectedIndexChanged(object sender, EventArgs e)
        {
            int groupId = DataHelper.ParseToInt(cboCustomerGroup.SelectedValue.ToString());
            var getConnectionTye = DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs
                                  .Where(x => x.NONLICENSE_CUSTOMER_GROUP_ID == groupId)
                                  .OrderBy(x => x.DESCRIPTION);
            if (cboCustomerGroup.SelectedIndex != -1)
            {
                UIHelper.SetDataSourceToComboBox(cboCustomerConnectionType, getConnectionTye, Resources.ALL_CONNECTION_TYPE);
                if (cboCustomerConnectionType.DataSource != null)
                {
                    cboCustomerConnectionType.SelectedIndex = 0;
                }
            }

        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
