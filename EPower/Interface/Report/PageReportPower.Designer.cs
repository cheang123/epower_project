﻿using System.ComponentModel;
using System.Windows.Forms;
using AxCrystalActiveXReportViewerLib105;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageReportPower
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageReportPower));
            this.panel1 = new System.Windows.Forms.Panel();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.cboReport = new System.Windows.Forms.ComboBox();
            this.chkSHOW_POWER_ADJUST = new System.Windows.Forms.CheckBox();
            this.cboTransformer = new System.Windows.Forms.ComboBox();
            this.btnMAIL = new SoftTech.Component.ExButton();
            this.btnVIEW = new SoftTech.Component.ExButton();
            this.lblTransformer = new System.Windows.Forms.Label();
            this.viewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.dtpDate);
            this.panel1.Controls.Add(this.cboReport);
            this.panel1.Controls.Add(this.chkSHOW_POWER_ADJUST);
            this.panel1.Controls.Add(this.cboTransformer);
            this.panel1.Controls.Add(this.btnMAIL);
            this.panel1.Controls.Add(this.btnVIEW);
            this.panel1.Controls.Add(this.lblTransformer);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // dtpDate
            // 
            resources.ApplyResources(this.dtpDate, "dtpDate");
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Name = "dtpDate";
            // 
            // cboReport
            // 
            this.cboReport.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboReport.FormattingEnabled = true;
            this.cboReport.Items.AddRange(new object[] {
            resources.GetString("cboReport.Items"),
            resources.GetString("cboReport.Items1")});
            resources.ApplyResources(this.cboReport, "cboReport");
            this.cboReport.Name = "cboReport";
            this.cboReport.SelectedIndexChanged += new System.EventHandler(this.cboReport_SelectedIndexChanged);
            // 
            // chkSHOW_POWER_ADJUST
            // 
            resources.ApplyResources(this.chkSHOW_POWER_ADJUST, "chkSHOW_POWER_ADJUST");
            this.chkSHOW_POWER_ADJUST.Checked = true;
            this.chkSHOW_POWER_ADJUST.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSHOW_POWER_ADJUST.Name = "chkSHOW_POWER_ADJUST";
            this.chkSHOW_POWER_ADJUST.UseVisualStyleBackColor = true;
            // 
            // cboTransformer
            // 
            this.cboTransformer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTransformer.FormattingEnabled = true;
            resources.ApplyResources(this.cboTransformer, "cboTransformer");
            this.cboTransformer.Name = "cboTransformer";
            this.cboTransformer.SelectedIndexChanged += new System.EventHandler(this.cboTransformer_SelectedIndexChanged);
            // 
            // btnMAIL
            // 
            resources.ApplyResources(this.btnMAIL, "btnMAIL");
            this.btnMAIL.Name = "btnMAIL";
            this.btnMAIL.UseVisualStyleBackColor = true;
            this.btnMAIL.Click += new System.EventHandler(this.btnMail_Click);
            // 
            // btnVIEW
            // 
            resources.ApplyResources(this.btnVIEW, "btnVIEW");
            this.btnVIEW.Name = "btnVIEW";
            this.btnVIEW.UseVisualStyleBackColor = true;
            this.btnVIEW.Click += new System.EventHandler(this.btnViewReport_Click);
            // 
            // lblTransformer
            // 
            resources.ApplyResources(this.lblTransformer, "lblTransformer");
            this.lblTransformer.Name = "lblTransformer";
            // 
            // viewer
            // 
            this.viewer.ActiveViewIndex = -1;
            this.viewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.viewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.viewer.DisplayStatusBar = false;
            resources.ApplyResources(this.viewer, "viewer");
            this.viewer.Name = "viewer";
            // 
            // PageReportPower
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.viewer);
            this.Controls.Add(this.panel1);
            this.Name = "PageReportPower";
            this.VisibleChanged += new System.EventHandler(this.PageReportPower_VisibleChanged);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnVIEW;
        private ExButton btnMAIL;
        private Label lblTransformer;
        private ComboBox cboTransformer;
        private CheckBox chkSHOW_POWER_ADJUST;
        private ComboBox cboReport;
        private DateTimePicker dtpDate;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer viewer;
    }
}
