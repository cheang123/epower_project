﻿namespace EPower.Interface.Report
{
    partial class DialogReportCustomerUsageSummaryOption
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cboArea = new System.Windows.Forms.ComboBox();
            this.cboCustomerGroup = new System.Windows.Forms.ComboBox();
            this.cboCustomerConnection = new System.Windows.Forms.ComboBox();
            this.cboCycle = new System.Windows.Forms.ComboBox();
            this.cboPrice = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClose = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.lblAREA = new System.Windows.Forms.Label();
            this.lblCUSTOMER_GROUP = new System.Windows.Forms.Label();
            this.lblCUSTOMER_CONNECTION_TYPE = new System.Windows.Forms.Label();
            this.lblBILLING_CYCLE = new System.Windows.Forms.Label();
            this.lblPRICE = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.lblPRICE);
            this.content.Controls.Add(this.lblBILLING_CYCLE);
            this.content.Controls.Add(this.lblCUSTOMER_CONNECTION_TYPE);
            this.content.Controls.Add(this.lblCUSTOMER_GROUP);
            this.content.Controls.Add(this.lblAREA);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnClose);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.cboPrice);
            this.content.Controls.Add(this.cboCycle);
            this.content.Controls.Add(this.cboCustomerConnection);
            this.content.Controls.Add(this.cboCustomerGroup);
            this.content.Controls.Add(this.cboArea);
            this.content.Size = new System.Drawing.Size(303, 260);
            this.content.Text = "CONTENT";
            this.content.Controls.SetChildIndex(this.cboArea, 0);
            this.content.Controls.SetChildIndex(this.cboCustomerGroup, 0);
            this.content.Controls.SetChildIndex(this.cboCustomerConnection, 0);
            this.content.Controls.SetChildIndex(this.cboCycle, 0);
            this.content.Controls.SetChildIndex(this.cboPrice, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnClose, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.lblAREA, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_GROUP, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_CONNECTION_TYPE, 0);
            this.content.Controls.SetChildIndex(this.lblBILLING_CYCLE, 0);
            this.content.Controls.SetChildIndex(this.lblPRICE, 0);
            // 
            // cboArea
            // 
            this.cboArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboArea.FormattingEnabled = true;
            this.cboArea.Items.AddRange(new object[] {
            "គ្រប់តំបន់ទាំងអស់",
            "ក១",
            "ក២",
            "ក៣",
            "ក៤"});
            this.cboArea.Location = new System.Drawing.Point(103, 58);
            this.cboArea.Margin = new System.Windows.Forms.Padding(1);
            this.cboArea.Name = "cboArea";
            this.cboArea.Size = new System.Drawing.Size(186, 27);
            this.cboArea.TabIndex = 18;
            // 
            // cboCustomerGroup
            // 
            this.cboCustomerGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomerGroup.FormattingEnabled = true;
            this.cboCustomerGroup.Items.AddRange(new object[] {
            "គ្រប់តំបន់ទាំងអស់",
            "ក១",
            "ក២",
            "ក៣",
            "ក៤"});
            this.cboCustomerGroup.Location = new System.Drawing.Point(103, 90);
            this.cboCustomerGroup.Margin = new System.Windows.Forms.Padding(1);
            this.cboCustomerGroup.Name = "cboCustomerGroup";
            this.cboCustomerGroup.Size = new System.Drawing.Size(186, 27);
            this.cboCustomerGroup.TabIndex = 19;
            this.cboCustomerGroup.SelectedIndexChanged += new System.EventHandler(this.cboCustomerGroup_SelectedIndexChanged);
            // 
            // cboCustomerConnection
            // 
            this.cboCustomerConnection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomerConnection.FormattingEnabled = true;
            this.cboCustomerConnection.Items.AddRange(new object[] {
            "គ្រប់តំបន់ទាំងអស់",
            "ក១",
            "ក២",
            "ក៣",
            "ក៤"});
            this.cboCustomerConnection.Location = new System.Drawing.Point(104, 124);
            this.cboCustomerConnection.Margin = new System.Windows.Forms.Padding(1);
            this.cboCustomerConnection.Name = "cboCustomerConnection";
            this.cboCustomerConnection.Size = new System.Drawing.Size(186, 27);
            this.cboCustomerConnection.TabIndex = 20;
            // 
            // cboCycle
            // 
            this.cboCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCycle.FormattingEnabled = true;
            this.cboCycle.Items.AddRange(new object[] {
            "គ្រប់តំបន់ទាំងអស់",
            "ក១",
            "ក២",
            "ក៣",
            "ក៤"});
            this.cboCycle.Location = new System.Drawing.Point(103, 24);
            this.cboCycle.Margin = new System.Windows.Forms.Padding(1);
            this.cboCycle.Name = "cboCycle";
            this.cboCycle.Size = new System.Drawing.Size(186, 27);
            this.cboCycle.TabIndex = 21;
            // 
            // cboPrice
            // 
            this.cboPrice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPrice.FormattingEnabled = true;
            this.cboPrice.Items.AddRange(new object[] {
            "គ្រប់តំបន់ទាំងអស់",
            "ក១",
            "ក២",
            "ក៣",
            "ក៤"});
            this.cboPrice.Location = new System.Drawing.Point(103, 157);
            this.cboPrice.Margin = new System.Windows.Forms.Padding(1);
            this.cboPrice.Name = "cboPrice";
            this.cboPrice.Size = new System.Drawing.Size(186, 27);
            this.cboPrice.TabIndex = 22;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Location = new System.Drawing.Point(2, 226);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(299, 1);
            this.panel1.TabIndex = 25;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnClose.Location = new System.Drawing.Point(214, 230);
            this.btnClose.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 24;
            this.btnClose.Text = "បិទ";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnOK.Location = new System.Drawing.Point(134, 230);
            this.btnOK.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 23;
            this.btnOK.Text = "យល់ព្រម";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblAREA
            // 
            this.lblAREA.AutoSize = true;
            this.lblAREA.Location = new System.Drawing.Point(12, 61);
            this.lblAREA.Name = "lblAREA";
            this.lblAREA.Size = new System.Drawing.Size(33, 19);
            this.lblAREA.TabIndex = 26;
            this.lblAREA.Text = "តំបន់";
            // 
            // lblCUSTOMER_GROUP
            // 
            this.lblCUSTOMER_GROUP.AutoSize = true;
            this.lblCUSTOMER_GROUP.Location = new System.Drawing.Point(12, 93);
            this.lblCUSTOMER_GROUP.Name = "lblCUSTOMER_GROUP";
            this.lblCUSTOMER_GROUP.Size = new System.Drawing.Size(41, 19);
            this.lblCUSTOMER_GROUP.TabIndex = 27;
            this.lblCUSTOMER_GROUP.Text = "ប្រភេទ";
            // 
            // lblCUSTOMER_CONNECTION_TYPE
            // 
            this.lblCUSTOMER_CONNECTION_TYPE.AutoSize = true;
            this.lblCUSTOMER_CONNECTION_TYPE.Location = new System.Drawing.Point(12, 127);
            this.lblCUSTOMER_CONNECTION_TYPE.Name = "lblCUSTOMER_CONNECTION_TYPE";
            this.lblCUSTOMER_CONNECTION_TYPE.Size = new System.Drawing.Size(65, 19);
            this.lblCUSTOMER_CONNECTION_TYPE.TabIndex = 28;
            this.lblCUSTOMER_CONNECTION_TYPE.Text = "ការភ្ជាប់ចរន្ត";
            // 
            // lblBILLING_CYCLE
            // 
            this.lblBILLING_CYCLE.AutoSize = true;
            this.lblBILLING_CYCLE.Location = new System.Drawing.Point(12, 27);
            this.lblBILLING_CYCLE.Name = "lblBILLING_CYCLE";
            this.lblBILLING_CYCLE.Size = new System.Drawing.Size(73, 19);
            this.lblBILLING_CYCLE.TabIndex = 29;
            this.lblBILLING_CYCLE.Text = "ជុំនៃការទូទាត់";
            // 
            // lblPRICE
            // 
            this.lblPRICE.AutoSize = true;
            this.lblPRICE.Location = new System.Drawing.Point(12, 160);
            this.lblPRICE.Name = "lblPRICE";
            this.lblPRICE.Size = new System.Drawing.Size(65, 19);
            this.lblPRICE.TabIndex = 30;
            this.lblPRICE.Text = "តម្លៃអគ្គិសនី";
            // 
            // DialogReportCustomerUsageSummaryOption
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(303, 283);
            this.Name = "DialogReportCustomerUsageSummaryOption";
            this.Text = "DialogReportCustomerUsageSummaryOption";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.ComboBox cboPrice;
        public System.Windows.Forms.ComboBox cboCycle;
        public System.Windows.Forms.ComboBox cboCustomerConnection;
        public System.Windows.Forms.ComboBox cboCustomerGroup;
        public System.Windows.Forms.ComboBox cboArea;
        private System.Windows.Forms.Panel panel1;
        private SoftTech.Component.ExButton btnClose;
        private SoftTech.Component.ExButton btnOK;
        private System.Windows.Forms.Label lblPRICE;
        private System.Windows.Forms.Label lblBILLING_CYCLE;
        private System.Windows.Forms.Label lblCUSTOMER_CONNECTION_TYPE;
        private System.Windows.Forms.Label lblCUSTOMER_GROUP;
        private System.Windows.Forms.Label lblAREA;
    }
}