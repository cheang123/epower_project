﻿using System.ComponentModel;
using System.Windows.Forms;
using AxCrystalActiveXReportViewerLib105;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageReportDiscountDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageReportDiscountDetail));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnMAIL = new SoftTech.Component.ExButton();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.btnVIEW = new SoftTech.Component.ExButton();
            this.cboBillingCycle = new System.Windows.Forms.ComboBox();
            this.viewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.btnMAIL);
            this.panel1.Controls.Add(this.dtpDate);
            this.panel1.Controls.Add(this.btnVIEW);
            this.panel1.Controls.Add(this.cboBillingCycle);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnMAIL
            // 
            resources.ApplyResources(this.btnMAIL, "btnMAIL");
            this.btnMAIL.Name = "btnMAIL";
            this.btnMAIL.UseVisualStyleBackColor = true;
            this.btnMAIL.Click += new System.EventHandler(this.btnMail_Click);
            // 
            // dtpDate
            // 
            resources.ApplyResources(this.dtpDate, "dtpDate");
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Name = "dtpDate";
            // 
            // btnVIEW
            // 
            resources.ApplyResources(this.btnVIEW, "btnVIEW");
            this.btnVIEW.Name = "btnVIEW";
            this.btnVIEW.UseVisualStyleBackColor = true;
            this.btnVIEW.Click += new System.EventHandler(this.btnViewReport_Click);
            // 
            // cboBillingCycle
            // 
            this.cboBillingCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBillingCycle.FormattingEnabled = true;
            this.cboBillingCycle.Items.AddRange(new object[] {
            resources.GetString("cboBillingCycle.Items"),
            resources.GetString("cboBillingCycle.Items1"),
            resources.GetString("cboBillingCycle.Items2"),
            resources.GetString("cboBillingCycle.Items3"),
            resources.GetString("cboBillingCycle.Items4")});
            resources.ApplyResources(this.cboBillingCycle, "cboBillingCycle");
            this.cboBillingCycle.Name = "cboBillingCycle";
            this.cboBillingCycle.SelectedIndexChanged += new System.EventHandler(this.cboSaveState_SelectedIndexChanged);
            // 
            // viewer
            // 
            this.viewer.ActiveViewIndex = -1;
            this.viewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.viewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.viewer.DisplayStatusBar = false;
            resources.ApplyResources(this.viewer, "viewer");
            this.viewer.Name = "viewer";
            // 
            // PageReportDiscountDetail
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.viewer);
            this.Controls.Add(this.panel1);
            this.Name = "PageReportDiscountDetail";
            this.VisibleChanged += new System.EventHandler(this.PageReportDiscountDetail_VisibleChanged);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        public ComboBox cboBillingCycle;
        private ExButton btnVIEW;
        private DateTimePicker dtpDate;
        private ExButton btnMAIL;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer viewer;
    }
}
