﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;

namespace EPower.Interface
{
    [DefaultEvent("PeriodChanged")]
    public partial class PanelReportPeriod : UserControl
    {
        public enum ReportType
        {
            Daily,
            Monthly,
            Yearly,
            Detail,
            ForMonth,
            ForYear
        }
        public class ReportTypeItem
        {
            public ReportType Type;
            public string Text;
            public override string ToString()
            {
                return Text;
            }
        }
        public event EventHandler PeriodChanged;
        List<ReportTypeItem> reports = new List<ReportTypeItem>(); 
        public PanelReportPeriod()
        {
            InitializeComponent();

            reports.Add(new ReportTypeItem()
            {
                Type = ReportType.Daily,
                Text = Resources.REPORT_DIALY
            });
            reports.Add(new ReportTypeItem()
            {
                Type = ReportType.Monthly,
                Text = Resources.MONTH
            });
            reports.Add(new ReportTypeItem()
            {
                Type = ReportType.Yearly,
                Text = Resources.YEAR
            });
            reports.Add(new ReportTypeItem()
            {
                Type = ReportType.Detail,
                Text = Resources.REPORT_DETAIL
            });
            reports.Add(new ReportTypeItem()
            {
                Type = ReportType.ForMonth,
                Text = Resources.FOR_MONTH
            });
            reports.Add(new ReportTypeItem()
            {
                Type = ReportType.ForYear,
                Text = Resources.FOR_YEAR
            });

            this.cboPeriod.Items.Clear();
        }
 
        bool _loading = false;
        public void AddPeriod(ReportType type)
        {
            _loading = true;
            var tmp = reports.FirstOrDefault(row => row.Type == type);
            this.cboPeriod.Items.Add(tmp);
            _loading = false;
        }


        public ReportType Type
        {
            get
            {
                if (this.cboPeriod.SelectedIndex == -1) return ReportType.Daily;
                var tmp = ((ReportTypeItem)this.cboPeriod.Items[this.cboPeriod.SelectedIndex]);
                if (tmp == null) return ReportType.Detail;
                return tmp.Type;
            }
            set
            {
                int index = 0;
                foreach (var itm in this.cboPeriod.Items)
                {
                    if (((ReportTypeItem)itm).Type == value)
                    {
                        this.cboPeriod.SelectedIndex = index;
                        break;
                    }
                    index++;
                }
            }
        }

        private void cboPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this._loading) return;
            if (this.cboPeriod.SelectedIndex == -1) this.cboPeriod.SelectedIndex = 0;
            ReportType ReportPeriod = this.Type;
            dtDate1.Value = new DateTime(dtDate2.Value.Year, dtDate2.Value.Month, 1);
            dtDate2.Value = dtDate1.Value.Date.AddMonths(1).AddDays(-1);
            if (ReportPeriod == ReportType.Detail)
            {
                this.dtDate1.Visible = this.dtDate2.Visible = true;
                this.dtDate1.CustomFormat = this.dtDate2.CustomFormat = "yyyy - MM - dd";
            }
            else if (ReportPeriod == ReportType.Daily || ReportPeriod == ReportType.ForMonth)
            {
                this.dtDate1.Show();
                this.dtDate2.Hide();
                this.dtDate1.CustomFormat = "yyyy - MM";
            }
            else if (ReportPeriod == ReportType.Monthly || ReportPeriod == ReportType.ForYear)
            {
                this.dtDate1.Show();
                this.dtDate2.Hide();
                this.dtDate1.CustomFormat = "yyyy";
            }
            else if (ReportPeriod == ReportType.Yearly)
            {
                this.dtDate1.Show();
                this.dtDate2.Show();
                this.dtDate1.CustomFormat = "yyyy";
                this.dtDate2.CustomFormat = "yyyy";
            }
            if (PeriodChanged != null) PeriodChanged(this, EventArgs.Empty);
        } 
    }
}
