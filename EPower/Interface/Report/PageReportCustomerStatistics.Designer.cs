﻿using System.ComponentModel;
using System.Windows.Forms;
using AxCrystalActiveXReportViewerLib105;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageReportCustomerStatistics
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageReportCustomerStatistics));
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkSHOW_DETAIL = new System.Windows.Forms.CheckBox();
            this.cboStatisticLevel = new System.Windows.Forms.ComboBox();
            this.cboCommune = new System.Windows.Forms.ComboBox();
            this.cboDistrict = new System.Windows.Forms.ComboBox();
            this.cboProvince = new System.Windows.Forms.ComboBox();
            this.btnMAIL = new SoftTech.Component.ExButton();
            this.btnVIEW = new SoftTech.Component.ExButton();
            this.viewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.chkSHOW_DETAIL);
            this.panel1.Controls.Add(this.cboStatisticLevel);
            this.panel1.Controls.Add(this.cboCommune);
            this.panel1.Controls.Add(this.cboDistrict);
            this.panel1.Controls.Add(this.cboProvince);
            this.panel1.Controls.Add(this.btnMAIL);
            this.panel1.Controls.Add(this.btnVIEW);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // chkSHOW_DETAIL
            // 
            resources.ApplyResources(this.chkSHOW_DETAIL, "chkSHOW_DETAIL");
            this.chkSHOW_DETAIL.Checked = true;
            this.chkSHOW_DETAIL.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSHOW_DETAIL.Name = "chkSHOW_DETAIL";
            this.chkSHOW_DETAIL.UseVisualStyleBackColor = true;
            // 
            // cboStatisticLevel
            // 
            this.cboStatisticLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboStatisticLevel.FormattingEnabled = true;
            this.cboStatisticLevel.Items.AddRange(new object[] {
            resources.GetString("cboStatisticLevel.Items"),
            resources.GetString("cboStatisticLevel.Items1"),
            resources.GetString("cboStatisticLevel.Items2"),
            resources.GetString("cboStatisticLevel.Items3")});
            resources.ApplyResources(this.cboStatisticLevel, "cboStatisticLevel");
            this.cboStatisticLevel.Name = "cboStatisticLevel";
            this.cboStatisticLevel.SelectedIndexChanged += new System.EventHandler(this.cboStatisticLevel_SelectedIndexChanged);
            // 
            // cboCommune
            // 
            this.cboCommune.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCommune.FormattingEnabled = true;
            resources.ApplyResources(this.cboCommune, "cboCommune");
            this.cboCommune.Name = "cboCommune";
            this.cboCommune.SelectedIndexChanged += new System.EventHandler(this.cboCommune_SelectedIndexChanged);
            // 
            // cboDistrict
            // 
            this.cboDistrict.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDistrict.FormattingEnabled = true;
            resources.ApplyResources(this.cboDistrict, "cboDistrict");
            this.cboDistrict.Name = "cboDistrict";
            this.cboDistrict.SelectedIndexChanged += new System.EventHandler(this.cboDistrict_SelectedIndexChanged);
            // 
            // cboProvince
            // 
            this.cboProvince.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboProvince.FormattingEnabled = true;
            resources.ApplyResources(this.cboProvince, "cboProvince");
            this.cboProvince.Name = "cboProvince";
            this.cboProvince.SelectedIndexChanged += new System.EventHandler(this.cboProvince_SelectedIndexChanged);
            // 
            // btnMAIL
            // 
            resources.ApplyResources(this.btnMAIL, "btnMAIL");
            this.btnMAIL.Name = "btnMAIL";
            this.btnMAIL.UseVisualStyleBackColor = true;
            this.btnMAIL.Click += new System.EventHandler(this.btnMail_Click);
            // 
            // btnVIEW
            // 
            resources.ApplyResources(this.btnVIEW, "btnVIEW");
            this.btnVIEW.Name = "btnVIEW";
            this.btnVIEW.UseVisualStyleBackColor = true;
            this.btnVIEW.Click += new System.EventHandler(this.btnViewReport_Click);
            // 
            // viewer
            // 
            this.viewer.ActiveViewIndex = -1;
            this.viewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.viewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.viewer.DisplayStatusBar = false;
            resources.ApplyResources(this.viewer, "viewer");
            this.viewer.Name = "viewer";
            // 
            // PageReportCustomerStatistics
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.viewer);
            this.Controls.Add(this.panel1);
            this.Name = "PageReportCustomerStatistics";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnVIEW;
        private ExButton btnMAIL;
        private ComboBox cboCommune;
        private ComboBox cboDistrict;
        private ComboBox cboProvince;
        private ComboBox cboStatisticLevel;
        private CheckBox chkSHOW_DETAIL;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer viewer;
    }
}
