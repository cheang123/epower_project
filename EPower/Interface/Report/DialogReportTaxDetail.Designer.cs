﻿using System.ComponentModel;
using System.Windows.Forms;
using AxCrystalActiveXReportViewerLib105;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogReportTaxDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogReportTaxDetail));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnEXPORT_EXCEL = new SoftTech.Component.ExButton();
            this.btnMAIL = new SoftTech.Component.ExButton();
            this.tab_ = new System.Windows.Forms.TabControl();
            this.tabNEW_CUSTOMER = new System.Windows.Forms.TabPage();
            this.viewerNewCus = new AxCrystalActiveXReportViewerLib105.AxCrystalActiveXReportViewer();
            this.tabPOWER_INCOME = new System.Windows.Forms.TabPage();
            this.viewerCusUsage = new AxCrystalActiveXReportViewerLib105.AxCrystalActiveXReportViewer();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.tab_.SuspendLayout();
            this.tabNEW_CUSTOMER.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.viewerNewCus)).BeginInit();
            this.tabPOWER_INCOME.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.viewerCusUsage)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.tab_);
            this.content.Controls.Add(this.panel1);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.tab_, 0);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.btnEXPORT_EXCEL);
            this.panel1.Controls.Add(this.btnMAIL);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnEXPORT_EXCEL
            // 
            resources.ApplyResources(this.btnEXPORT_EXCEL, "btnEXPORT_EXCEL");
            this.btnEXPORT_EXCEL.Name = "btnEXPORT_EXCEL";
            this.btnEXPORT_EXCEL.UseVisualStyleBackColor = true;
            this.btnEXPORT_EXCEL.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // btnMAIL
            // 
            resources.ApplyResources(this.btnMAIL, "btnMAIL");
            this.btnMAIL.Name = "btnMAIL";
            this.btnMAIL.UseVisualStyleBackColor = true;
            this.btnMAIL.Click += new System.EventHandler(this.btnMail_Click);
            // 
            // tab_
            // 
            this.tab_.Controls.Add(this.tabNEW_CUSTOMER);
            this.tab_.Controls.Add(this.tabPOWER_INCOME);
            resources.ApplyResources(this.tab_, "tab_");
            this.tab_.Name = "tab_";
            this.tab_.SelectedIndex = 0;
            // 
            // tabNEW_CUSTOMER
            // 
            this.tabNEW_CUSTOMER.Controls.Add(this.viewerNewCus);
            resources.ApplyResources(this.tabNEW_CUSTOMER, "tabNEW_CUSTOMER");
            this.tabNEW_CUSTOMER.Name = "tabNEW_CUSTOMER";
            this.tabNEW_CUSTOMER.UseVisualStyleBackColor = true;
            // 
            // viewerNewCus
            // 
            resources.ApplyResources(this.viewerNewCus, "viewerNewCus");
            this.viewerNewCus.Name = "viewerNewCus";
            this.viewerNewCus.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("viewerNewCus.OcxState")));
            // 
            // tabPOWER_INCOME
            // 
            this.tabPOWER_INCOME.BackColor = System.Drawing.Color.Transparent;
            this.tabPOWER_INCOME.Controls.Add(this.viewerCusUsage);
            resources.ApplyResources(this.tabPOWER_INCOME, "tabPOWER_INCOME");
            this.tabPOWER_INCOME.Name = "tabPOWER_INCOME";
            // 
            // viewerCusUsage
            // 
            resources.ApplyResources(this.viewerCusUsage, "viewerCusUsage");
            this.viewerCusUsage.Name = "viewerCusUsage";
            this.viewerCusUsage.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("viewerCusUsage.OcxState")));
            // 
            // DialogReportTaxDetail
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogReportTaxDetail";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.DialogReportTaxDetail_Load);
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.tab_.ResumeLayout(false);
            this.tabNEW_CUSTOMER.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.viewerNewCus)).EndInit();
            this.tabPOWER_INCOME.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.viewerCusUsage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnMAIL;
        private TabControl tab_;
        private TabPage tabNEW_CUSTOMER;
        private AxCrystalActiveXReportViewer viewerNewCus;
        private TabPage tabPOWER_INCOME;
        private AxCrystalActiveXReportViewer viewerCusUsage;
        private ExButton btnEXPORT_EXCEL;


    }
}