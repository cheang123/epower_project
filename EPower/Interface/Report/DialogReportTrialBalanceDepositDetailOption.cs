﻿using EPower.Base.Helper;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogReportTrialBalanceDepositDetailOption : ExDialog
    {
        private int _service_typeId = 0;
        public DialogReportTrialBalanceDepositDetailOption()
        {
            InitializeComponent();
            Bind();
        }

        List<KeyValuePair<int, string>> lst = new List<KeyValuePair<int, string>>()
            {
                new KeyValuePair<int, string>(1, "បង់កន្លែងផ្ទាល់"),
                new KeyValuePair<int, string>(2, "បង់តាមធនាគារ")
            };

        #region Method

        public void Bind()
        {
            UIHelper.SetDataSourceToComboBox(this.cboBillingCycle, Lookup.GetBillingCycles(), Properties.Resources.ALL_CYCLE);
            UIHelper.SetDataSourceToComboBox(this.cboCurrency, Lookup.GetCurrencies(), Properties.Resources.ALL_CURRENCY);
            UIHelper.SetDataSourceToComboBox(this.cboArea, Lookup.GetAreas(), Properties.Resources.ALL_AREA);
            UIHelper.SetDataSourceToComboBox(this.cboAction, DBDataContext.Db.TLKP_PREPAYMENT_ACTIONs.Select(x => new { x.ACTION_ID, x.ACTION_NAME_KH }), Properties.Resources.ALL_STATUS);
            // Account 
            new AccountChartPopulator().PopluateTree(cboPaymentAccount.TreeView, AccountChartHelper.GetAccounts(AccountConfig.PAYMENT_ACCOUNTS, 0, Resources.ALL_ACCOUNT, 71));
            cboPaymentAccount.TreeView.SelectedNode = cboPaymentAccount.TreeView.GetNodesIncludeAncestors().FirstOrDefault(x => (int)x.Tag == 0);
            cboPaymentAccount.Text = Resources.ALL_ACCOUNT;

            //Item type
            UIHelper.SetDataSourceToComboBox(this.cboItemType, DBDataContext.Db.TBL_INVOICE_ITEM_TYPEs.Where(x => x.IS_ACTIVE).OrderBy(x => x.INVOICE_ITEM_TYPE_ID).ToList(), Resources.ALL_SERVICE_TYPE);
            var sources = DBDataContext.Db.TBL_INVOICE_ITEMs.OrderBy(x => x.INVOICE_ITEM_TYPE_ID).ThenBy(x => x.INVOICE_ITEM_ID).Where(x => x.IS_ACTIVE && x.INVOICE_ITEM_ID >= -5).Select(x => new { x.INVOICE_ITEM_ID, x.INVOICE_ITEM_NAME });
            UIHelper.SetDataSourceToComboBox(cboItem, sources, Resources.ALL_SERVICE);
        }

        #endregion

        private void cboItemType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cboItemType_SelectedValueChanged(object sender, EventArgs e)
        {
            int typeId = cboItemType.SelectedIndex == -1 ? 0 : (int)cboItemType.SelectedValue;
            if (typeId == _service_typeId)
            {
                return;
            }
            UIHelper.SetDataSourceToComboBox(cboItem, DBDataContext.Db.TBL_INVOICE_ITEMs.OrderBy(x => x.INVOICE_ITEM_TYPE_ID).ThenBy(x => x.INVOICE_ITEM_ID).Where(x => x.INVOICE_ITEM_ID >= -5 && (x.INVOICE_ITEM_TYPE_ID == typeId || typeId == 0) && x.IS_ACTIVE).Select(x => new { x.INVOICE_ITEM_ID, x.INVOICE_ITEM_NAME }), Resources.ALL_SERVICE);
            cboItem.SelectedValue = 0;
            _service_typeId = typeId;
        }

        private void btnOK_Click_1(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}