﻿using AxCrystalActiveXReportViewerLib105;

namespace EPower.Interface 
{
    public static class ViewerExt
    {
        public static void ApplyDefaultFormat(this AxCrystalActiveXReportViewer viewer)
        {
            viewer.EnableExportButton = true;
            viewer.DisplayBorder = false;
            viewer.DisplayGroupTree = false;
            viewer.DisplayTabs = false;
            viewer.EnableRefreshButton = false;
            viewer.EnableStopButton = false;

        }
    }
}
