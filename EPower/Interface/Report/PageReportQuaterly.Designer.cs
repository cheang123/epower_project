﻿using System.ComponentModel;
using System.Windows.Forms;
using AxCrystalActiveXReportViewerLib105;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageReportQuaterly
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageReportQuaterly));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSetup = new SoftTech.Component.ExButton();
            this.cboYear = new System.Windows.Forms.ComboBox();
            this.cboQuarter = new System.Windows.Forms.ComboBox();
            this.lblYEAR = new System.Windows.Forms.Label();
            this.btnMail = new SoftTech.Component.ExButton();
            this.btnViewReport = new SoftTech.Component.ExButton();
            this.lblQUARTER = new System.Windows.Forms.Label();
            this.viewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.btnSetup);
            this.panel1.Controls.Add(this.cboYear);
            this.panel1.Controls.Add(this.cboQuarter);
            this.panel1.Controls.Add(this.lblYEAR);
            this.panel1.Controls.Add(this.btnMail);
            this.panel1.Controls.Add(this.btnViewReport);
            this.panel1.Controls.Add(this.lblQUARTER);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnSetup
            // 
            resources.ApplyResources(this.btnSetup, "btnSetup");
            this.btnSetup.Name = "btnSetup";
            this.btnSetup.UseVisualStyleBackColor = true;
            this.btnSetup.Click += new System.EventHandler(this.btnSetting_Click);
            // 
            // cboYear
            // 
            this.cboYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboYear.FormattingEnabled = true;
            this.cboYear.Items.AddRange(new object[] {
            resources.GetString("cboYear.Items"),
            resources.GetString("cboYear.Items1")});
            resources.ApplyResources(this.cboYear, "cboYear");
            this.cboYear.Name = "cboYear";
            this.cboYear.SelectedIndexChanged += new System.EventHandler(this.cboYear_SelectedIndexChanged);
            // 
            // cboQuarter
            // 
            this.cboQuarter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboQuarter.FormattingEnabled = true;
            this.cboQuarter.Items.AddRange(new object[] {
            resources.GetString("cboQuarter.Items"),
            resources.GetString("cboQuarter.Items1"),
            resources.GetString("cboQuarter.Items2"),
            resources.GetString("cboQuarter.Items3")});
            resources.ApplyResources(this.cboQuarter, "cboQuarter");
            this.cboQuarter.Name = "cboQuarter";
            this.cboQuarter.SelectedIndexChanged += new System.EventHandler(this.cboQuarter_SelectedIndexChanged);
            // 
            // lblYEAR
            // 
            resources.ApplyResources(this.lblYEAR, "lblYEAR");
            this.lblYEAR.Name = "lblYEAR";
            // 
            // btnMail
            // 
            resources.ApplyResources(this.btnMail, "btnMail");
            this.btnMail.Name = "btnMail";
            this.btnMail.UseVisualStyleBackColor = true;
            this.btnMail.Click += new System.EventHandler(this.btnMail_Click);
            // 
            // btnViewReport
            // 
            resources.ApplyResources(this.btnViewReport, "btnViewReport");
            this.btnViewReport.Name = "btnViewReport";
            this.btnViewReport.UseVisualStyleBackColor = true;
            this.btnViewReport.Click += new System.EventHandler(this.btnViewReport_Click);
            // 
            // lblQUARTER
            // 
            resources.ApplyResources(this.lblQUARTER, "lblQUARTER");
            this.lblQUARTER.Name = "lblQUARTER";
            // 
            // viewer
            // 
            this.viewer.ActiveViewIndex = -1;
            this.viewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.viewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.viewer.DisplayStatusBar = false;
            resources.ApplyResources(this.viewer, "viewer");
            this.viewer.Name = "viewer";
            // 
            // PageReportQuaterly
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.viewer);
            this.Controls.Add(this.panel1);
            this.Name = "PageReportQuaterly";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        public ComboBox cboYear;
        private ExButton btnViewReport;
        private ExButton btnMail;
        public ComboBox cboQuarter;
        private Label lblYEAR;
        private Label lblQUARTER;
        private ExButton btnSetup;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer viewer;
    }
}
