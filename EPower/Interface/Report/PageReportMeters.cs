﻿using EPower.Base.Logic;
using EPower.Interface.Report;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageReportMeters : Form
    {
        CrystalReportHelper ch = null;
        DialogReportMeterOption diag;
        public PageReportMeters()
        {
            InitializeComponent();
            viewer.DefaultView();

            dtpDate.Value = DBDataContext.Db.GetSystemDate();
            diag = new DialogReportMeterOption();
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            Runner.Run(this.viewReport);
        }

        public CrystalReportHelper OpenReport()
        {
            var ch = new CrystalReportHelper("ReportMeterHistory.rpt");
            ch.SetParameter("@DATE", dtpDate.Value.Date);
            ch.SetParameter("@CYCLE_ID", (int)diag.cboBillingCycle.SelectedValue);
            ch.SetParameter("@AREA_ID", (int)diag.cboArea.SelectedValue);
            ch.SetParameter("@TYPE_ID", (int)diag.cboType.SelectedValue);
            ch.SetParameter("@STATUS_ID", (int)diag.cboStatus.SelectedValue);
            ch.SetParameter("@CYCLE_NAME", diag.cboBillingCycle.Text);
            ch.SetParameter("@AREA_NAME", diag.cboArea.Text);
            ch.SetParameter("@TYPE_NAME", diag.cboType.Text);
            ch.SetParameter("@STATUS_NAME", diag.cboStatus.Text);
            ch.SetParameter("@START", DataHelper.ParseToInt(diag.txtStart.Text));
            ch.SetParameter("@END", diag.txtEnd.Text == Resources.UNLIMITED ? 0 : DataHelper.ParseToInt(diag.txtEnd.Text));
            return ch;
        }

        private void viewReport()
        {
            try
            {
                this.ch = OpenReport();
                this.viewer.ReportSource = ch.Report;
            }
            catch(Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_VIEW_REPORT, Base.Properties.Resources.WARNING);
            }
        }

        private void dtpDate_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void btnSetup_Click(object sender, EventArgs e)
        {
            if (diag.ShowDialog() == DialogResult.OK)
            {
                Runner.Run(this.viewReport);
            }
        }
    }
}
