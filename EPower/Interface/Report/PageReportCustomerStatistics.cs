﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PageReportCustomerStatistics : Form
    {
        CrystalReportHelper ch = null;
        IEnumerable<TLKP_VILLAGE> VILLAGES = null;
        IEnumerable<TLKP_COMMUNE> COMMUNES = null;
        IEnumerable<TLKP_DISTRICT> DISTRICTS = null;
        IEnumerable<TLKP_PROVINCE> PROVINCES = null;
        public PageReportCustomerStatistics()
        {
            InitializeComponent();
            viewer.DefaultView();

            VILLAGES = DBDataContext.Db.TLKP_VILLAGEs.Where(row=> DBDataContext.Db.TBL_CUSTOMERs.Select(r=>r.VILLAGE_CODE).Distinct().Contains(row.VILLAGE_CODE));
            COMMUNES = DBDataContext.Db.TLKP_COMMUNEs.Where(row => VILLAGES.Select(x => x.COMMUNE_CODE).Distinct().Contains(row.COMMUNE_CODE));
            DISTRICTS = DBDataContext.Db.TLKP_DISTRICTs.Where(row => COMMUNES.Select(x => x.DISTRICT_CODE).Distinct().Contains(row.DISTRICT_CODE));
            PROVINCES = DBDataContext.Db.TLKP_PROVINCEs.Where(row => DISTRICTS.Select(x => x.PROVINCE_CODE).Distinct().Contains(row.PROVINCE_CODE));

            UIHelper.SetDataSourceToComboBox(cboProvince, PROVINCES,"");
            this.cboProvince.SelectedIndex = 0;

            //this.cboStatisticLevel.SelectedIndex = 0;
            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("REPORT_ID", typeof(int));
            dtReport.Columns.Add("REPORT_NAME", typeof(string));

            DataRow drProvince = dtReport.NewRow();
            drProvince["REPORT_ID"] = 1;
            drProvince["REPORT_NAME"] = Resources.BY_PROVINCE;
            dtReport.Rows.Add(drProvince);

            DataRow drDistrict = dtReport.NewRow();
            drDistrict["REPORT_ID"] = 2;
            drDistrict["REPORT_NAME"] = Resources.BY_DISTRICT;
            dtReport.Rows.Add(drDistrict);

            DataRow drCommune = dtReport.NewRow();
            drCommune["REPORT_ID"] = 3;
            drCommune["REPORT_NAME"] = Resources.BY_COMMUNE;
            dtReport.Rows.Add(drCommune);

            DataRow drVillage = dtReport.NewRow();
            drVillage["REPORT_ID"] = 4;
            drVillage["REPORT_NAME"] = Resources.BY_VILLAGE;
            dtReport.Rows.Add(drVillage);

            UIHelper.SetDataSourceToComboBox(cboStatisticLevel, dtReport);
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        { 
            Runner.Run(this.viewReport);
        }

        public CrystalReportHelper ViewReportPowerYearly()
        {
            this.ch = new CrystalReportHelper("ReportCustomerStatistics.rpt");
            this.ch.SetParameter("@PROVINCE_CODE", this.cboProvince.SelectedValue);
            this.ch.SetParameter("@DISTRICT_CODE", this.cboDistrict.SelectedValue);
            this.ch.SetParameter("@COMMUNE_CODE", this.cboCommune.SelectedValue);
            this.ch.SetParameter("@VILLAGE_CODE", "0");
            this.ch.SetParameter("@STATS_LEVEL", this.cboStatisticLevel.SelectedIndex);
            this.ch.SetParameter("@VIEW_DETAIL", this.chkSHOW_DETAIL.Checked);
            this.ch.SetParameter("@STATS_LEVEL_TEXT", this.cboStatisticLevel.Text);
            this.ch.SetParameter("@PROVINCE_NAME", this.cboProvince.SelectedIndex == 0 ? "" : Resources.PROVINCE + this.cboProvince.Text);
            this.ch.SetParameter("@DISTRICT_NAME", this.cboDistrict.SelectedIndex == 0 ? "" : Resources.DISTRICT + this.cboDistrict.Text);
            this.ch.SetParameter("@COMMUNE_NAME", this.cboCommune.SelectedIndex == 0 ? "" : Resources.COMMUNE + this.cboProvince.Text);
            this.ch.SetParameter("@VILLAGE_NAME", "");
            return this.ch;
        }

        private void viewReport()
        {
            this.viewer.ReportSource = ViewReportPowerYearly().Report;
        }

        private void sendMail()
        {
            try
            {
                this.viewReport();
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + this.ch.ReportName + ".xls").FullName;
                this.ch.ExportToExcel(exportPath);
                DialogSendMail.Instance.Add(this.ch.ReportName, exportPath);
                DialogSendMail.Instance.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.ShowWarning(EPower.Properties.Resources.YOU_CANNOT_SEND_EMAIL, Resources.WARNING);
                MsgBox.LogError(ex);
            }
        }
        private void btnMail_Click(object sender, EventArgs e)
        {
            this.sendMail();
        }

        private void cboProvince_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboProvince.SelectedIndex < 0) return;
            UIHelper.SetDataSourceToComboBox(cboDistrict, DISTRICTS.Where(row => row.PROVINCE_CODE == cboProvince.SelectedValue.ToString()),"");
        }


        private void cboDistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboDistrict.SelectedIndex < 0)return;
            UIHelper.SetDataSourceToComboBox(cboCommune, COMMUNES.Where(row => row.DISTRICT_CODE == cboDistrict.SelectedValue.ToString()),"");
        }

        private void cboCommune_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cboStatisticLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.chkSHOW_DETAIL.Enabled = this.cboStatisticLevel.SelectedIndex != 3;
            this.chkSHOW_DETAIL.Checked = this.cboStatisticLevel.SelectedIndex != 3;
        }
    }
}
