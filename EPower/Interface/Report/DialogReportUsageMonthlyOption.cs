﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogReportUsageMonthlyOption : ExDialog
    {
        public enum FilterType
        {
            FilterByPower = 1,
            FilterByPercentage = 2
        }
        public DialogReportUsageMonthlyOption()
        {
            InitializeComponent();
            Bind();
        }


        #region Method

        public void Bind()
        {
            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("REPORT_ID", typeof(int));
            dtReport.Columns.Add("REPORT_NAME", typeof(string));

            DataRow drPayment = dtReport.NewRow();
            drPayment["REPORT_ID"] = 1;
            drPayment["REPORT_NAME"] = Resources.POWER;
            dtReport.Rows.Add(drPayment);

            DataRow drInvoice = dtReport.NewRow();
            drInvoice["REPORT_ID"] = 2;
            drInvoice["REPORT_NAME"] = Resources.PERCENTAGE;
            dtReport.Rows.Add(drInvoice);
            UIHelper.SetDataSourceToComboBox(cboFilterType, dtReport, "REPORT_ID", "REPORT_NAME");

            DataTable dtOrder = new DataTable();
            dtOrder.Columns.Add("ORDER_ID", typeof(int));
            dtOrder.Columns.Add("ORDER_NAME", typeof(string));

            DataRow drOrder1 = dtOrder.NewRow();
            drOrder1["ORDER_ID"] = 1;
            drOrder1["ORDER_NAME"] = Resources.ORDER_BY_AREA;
            dtOrder.Rows.Add(drOrder1);

            DataRow drOrder2 = dtOrder.NewRow();
            drOrder2["ORDER_ID"] = 2;
            drOrder2["ORDER_NAME"] = Resources.ORDER_BY_USAGE_DESENDING;
            dtOrder.Rows.Add(drOrder2);

            DataRow drOrder3 = dtOrder.NewRow();
            drOrder3["ORDER_ID"] = 3;
            drOrder3["ORDER_NAME"] = Resources.ORDER_BY_USAGE_ASCENDING;
            dtOrder.Rows.Add(drOrder3);
            UIHelper.SetDataSourceToComboBox(this.cboOrderType, dtOrder, "ORDER_ID", "ORDER_NAME");
            UIHelper.SetDataSourceToComboBox(this.cboCustomerGroup, Lookup.GetCustomerGroup(), Resources.All_CUSTOMER_GROUP);
            UIHelper.SetDataSourceToComboBox(this.cboCustomerStatus, Lookup.GetCustomerStatuses(), Resources.ALL_STATUS);
            UIHelper.SetDataSourceToComboBox(this.cboPRICE, Lookup.GetPrices(), Resources.ALL_PRICE);
        }

        #endregion

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void cboOperatorM1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.txtVal1M1.Visible = this.cboOperatorM1.Text != "";
            this.txtVal2M1.Visible = this.cboOperatorM1.Text == "between";
            this.lblM1FilterType_.Visible = this.cboOperatorM1.Text != "";
        }

        private void cboOperatorM2_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.txtVal1M2.Visible = this.cboOperatorM2.Text != "";
            this.txtVal2M2.Visible = this.cboOperatorM2.Text == "between";
            this.lblM2FilterType_.Visible = this.cboOperatorM2.Text != "";
        }

        private void cboOperatorM3_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.txtVal1M3.Visible = this.cboOperatorM3.Text != "";
            this.txtVal2M3.Visible = this.cboOperatorM3.Text == "between";
            this.lblM3FilterType_.Visible = this.cboOperatorM3.Text != "";
        }

        private void cboFilterType_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblM1FilterType_.Text = lblM2FilterType_.Text = lblM3FilterType_.Text = (int)cboFilterType.SelectedValue == (int)FilterType.FilterByPower ? "kwh" : "%";
        }

        private void cboCustomerGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            int groupId = DataHelper.ParseToInt(cboCustomerGroup.SelectedValue.ToString());
            var connectionType = DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs
                                .Where(x => x.NONLICENSE_CUSTOMER_GROUP_ID == groupId)
                                .OrderBy(x => x.DESCRIPTION);
            UIHelper.SetDataSourceToComboBox(cboConnectionType, connectionType, Resources.ALL_CONNECTION_TYPE);
            if (cboConnectionType.DataSource != null)
            {
                cboConnectionType.SelectedIndex = 0;
            }
        }
    }
}