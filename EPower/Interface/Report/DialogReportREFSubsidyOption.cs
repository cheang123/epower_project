﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogREFSubsidyOption: ExDialog
    {
        public DialogREFSubsidyOption()
        {
            InitializeComponent(); 
            Bind();
        }


        #region Method

        public void Bind()
        {
            DataTable dtFilterPower = new DataTable();
            dtFilterPower.Columns.Add("FILTER_POWER_ID", typeof(int));
            dtFilterPower.Columns.Add("FILTER_POWER_NAME", typeof(string));
            dtFilterPower.Columns.Add("START_USAGE", typeof(decimal));
            dtFilterPower.Columns.Add("END_USAGE", typeof(decimal));

            dtFilterPower.Rows.Add(1, Resources.SHOW_ALL, 0, 99999999999999.9999);
            dtFilterPower.Rows.Add(2, Resources.REPORT_REF_FILTER_POWER_0KWH, 0, 0);
            dtFilterPower.Rows.Add(3, Resources.REPORT_REF_FILTER_POWER_1_TO_10KWH, 1, 10);
            dtFilterPower.Rows.Add(4, Resources.REPORT_REF_FILTER_POWER_OVER_10KWH, 11, 99999999999999.9999);
            dtFilterPower.Rows.Add(5, Resources.REPORT_REF_FILTER_POWER_11_TO_50KWH, 11, 50);
            dtFilterPower.Rows.Add(6, Resources.REPORT_REF_FILTER_POWER_OVER_50KWH, 51, 99999999999999.9999);

            UIHelper.SetDataSourceToComboBox(this.cboCustomerGroup, Lookup.GetCustomerGroup(), Resources.DisplayAllCustomer);
            //UIHelper.SetDataSourceToComboBox(cboCustomerConnectionType, DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs, Resources.ALL_CONNECTION_TYPE);
            UIHelper.SetDataSourceToComboBox(this.cboCurrency, Lookup.GetCurrencies(), Resources.ALL_CURRENCY);
            UIHelper.SetDataSourceToComboBox(cboFilterPower, dtFilterPower);
        }

        #endregion

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

		private void cboCustomerGroup_SelectedIndexChanged(object sender, EventArgs e)
		{
			int groupId = DataHelper.ParseToInt(cboCustomerGroup.SelectedValue.ToString());
			var _ds = DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs
					.Where(x => x.NONLICENSE_CUSTOMER_GROUP_ID == groupId)
					.OrderBy(x => x.DESCRIPTION);

			UIHelper.SetDataSourceToComboBox(cboCustomerConnectionType, _ds, Resources.ALL_CONNECTION_TYPE);
			if (cboCustomerConnectionType.DataSource != null)
			{
				this.cboCustomerConnectionType.SelectedIndex = 0;
			}
		}
	}
}