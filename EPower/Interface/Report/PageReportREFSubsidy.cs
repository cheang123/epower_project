﻿using EPower.Base.Logic;
using EPower.Base.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageReportSubsidy : Form
    {
        public enum Report
        {
            ReportREFSubsidy = 1,
            ReportCustomerConnectionType = 2,
            ReportREFInvoice = 3,
            ReportREFInvalidCustomer = 4,
            ReportCustomerCovid19Subsidy = 5,
            reportPrintCustomerCovid19Subsidy = 6
        }

        CrystalReportHelper ch = null;
        TBL_COMPANY objCompany = null;
        string licenseNo = "";
        public PageReportSubsidy()
        {
            try
            {
                InitializeComponent();
                viewer.DefaultView();
                objCompany = DBDataContext.Db.TBL_COMPANies.FirstOrDefault();
                bind();
                lookUp();
                TBL_BILLING_CYCLE objBillingCycle = DBDataContext.Db.TBL_BILLING_CYCLEs.FirstOrDefault(row => row.IS_ACTIVE);
                if (objBillingCycle != null)
                {
                    DateTime datCurrentMonth = Method.GetNextBillingMonth(objBillingCycle.CYCLE_ID).AddMonths(-1);
                    this.dtpYear.Value = new DateTime(datCurrentMonth.Year, datCurrentMonth.Month, 1);
                }
            }
            catch (Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Resources.YOU_CANNOT_VIEW_REPORT,Resources.WARNING);
            }
        }

        private void bind()
        {
            // Bind Billing Cycle
            UIHelper.SetDataSourceToComboBox(cboBillingCycle, Lookup.GetBillingCycles(), Resources.ALL_CYCLE);

            // Bind Area
            UIHelper.SetDataSourceToComboBox(cboArea, Lookup.GetAreas(), Resources.ALL_AREA);
        }

        private void lookUp()
        {
            DateTime dt = new DateTime(dtpYear.Value.Year, dtpYear.Value.Month, 1);
            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("REPORT_ID", typeof(int));
            dtReport.Columns.Add("REPORT_NAME", typeof(string));
            dtReport.Rows.Add(1, Resources.REPORT_REF_SUBSIDY);
            dtReport.Rows.Add(2, Resources.REPORT_CUSTOMER_CONNCTION_TYPE);
            dtReport.Rows.Add(3, Resources.INVOICE_DETAIL);
            dtReport.Rows.Add(4, Resources.REPORT_REF_INVALID_CUSTOMER);
            dtReport.Rows.Add(5, Resources.REPORT_CUSTOMER_COVID_19_SUBSIDY);
            dtReport.Rows.Add(6, Resources.PRINT_INVOICE_COVID_SUBSIDY);
            UIHelper.SetDataSourceToComboBox(cboReport, dtReport);
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            DateTime dt = new DateTime(dtpYear.Value.Year, dtpYear.Value.Month, 1);

            var invDetail = DBDataContext.Db.TBL_INVOICE_DETAILs.Select(x => x.INVOICE_ID);
            var inv = DBDataContext.Db.TBL_INVOICEs.Where(x => x.IS_SERVICE_BILL == false && x.INVOICE_MONTH == dt && !invDetail.Contains(x.INVOICE_ID)).ToList();
            if (inv.Count() > 0)
            {
                foreach (var i in inv)
                {
                    var exchangeRate = new ExchangeRateLogic().findLastExchangeRate(i.INVOICE_DATE, i.CURRENCY_ID);
                    TBL_INVOICE_DETAIL insert = new TBL_INVOICE_DETAIL
                    {
                        INVOICE_ID = i.INVOICE_ID,
                        START_USAGE = 0,
                        END_USAGE = i.END_USAGE - i.START_USAGE,
                        USAGE = i.TOTAL_USAGE,
                        PRICE = i.PRICE,
                        INVOICE_ITEM_ID = 1,//វិក្កយបត្រអគ្គិសនី
                        AMOUNT = i.TOTAL_AMOUNT,
                        EXCHANGE_RATE = exchangeRate.EXCHANGE_RATE,
                        EXCHANGE_RATE_DATE = exchangeRate.CREATE_ON,
                        REF_NO = i.INVOICE_NO,
                        TAX_AMOUNT = 0,
                        TRAN_DATE = i.INVOICE_DATE,
                        CHARGE_DESCRIPTION = "Auto insert after missed"
                    };
                    DBDataContext.Db.TBL_INVOICE_DETAILs.InsertOnSubmit(insert);
                }
                DBDataContext.Db.SubmitChanges();

                //MsgBox.ShowError(Resources.MS_MISSING_INVOICE_DETAIL_TO_RUN_SUBSIDY,Resources.WARNING);
                //return;
            }
            if (dtpYear.Value < new DateTime(2016, 03, 1) && (int)cboReport.SelectedValue == (int)Report.ReportREFSubsidy)
            {
                MsgBox.ShowInformation(Resources.MS_NO_DATA_TO_RUN_SUBSIDY, Resources.WARNING);
                return;
            }
            if ((int)cboReport.SelectedValue == (int)Report.reportPrintCustomerCovid19Subsidy)
            {
                var invoiceids = loadPrintInv();
                if (invoiceids.Count > 0)
                {
                    bool isConcurrentPrinting = false;
                    isConcurrentPrinting = DataHelper.ParseToBoolean(Method.Utilities[Utility.INVOICE_CONCURRENT_PRINTING]);
                    if (isConcurrentPrinting)
                    {
                        TBL_PRINT_INVOICE objPrintInvoice = new TBL_PRINT_INVOICE()
                        {
                            PRINT_DATE = DBDataContext.Db.GetSystemDate(),
                            LOGIN_ID = Login.CurrentLogin.LOGIN_ID
                        };
                        using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                        {
                            DBDataContext.Db.TBL_PRINT_INVOICEs.InsertOnSubmit(objPrintInvoice);
                            DBDataContext.Db.SubmitChanges();
                            invoiceids.ForEach(x => x.PRINT_INVOICE_ID = objPrintInvoice.PRINT_INVOICE_ID);
                            DBDataContext.Db.BulkCopy(invoiceids);
                            DBDataContext.Db.SubmitChanges();
                            tran.Complete();
                        }
                        new DialogCustomerPrintInvoice(true, objPrintInvoice.PRINT_INVOICE_ID).ShowDialog();
                    }
                    else
                    {
                        using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                        {
                            DBDataContext.Db.TBL_INVOICE_TO_PRINTs.DeleteAllOnSubmit(DBDataContext.Db.TBL_INVOICE_TO_PRINTs);
                            DBDataContext.Db.BulkCopy(invoiceids);
                            DBDataContext.Db.SubmitChanges();
                            tran.Complete();
                        }
                        new DialogCustomerPrintInvoice(false, 0).ShowDialog();
                    }
                }
                else
                {
                    MsgBox.ShowInformation(Resources.MSG_NO_INVOICE_COVID_19_SUBSIDY, Resources.WARNING);
                }
            }
            else
            {
                if ((int)cboReport.SelectedValue == (int)Report.ReportREFSubsidy)
                {

                    //var now = DBDataContext.Db.GetSystemDate();
                    var log = DBDataContext.Db.TBL_LOG_SEND_MAILs
                                        .Where(x => x.REPORT_NAME == Resources.REPORT_POWER_SOLD && x.REPORT_DATE.Year == DateTime.Now.Year)
                                        .OrderByDescending(x => x.SEND_DATE).FirstOrDefault();

                    if (log != null)
                    {
                        var maxRunBill = DBDataContext.Db.TBL_RUN_BILLs
                                        .Where(x => x.BILLING_MONTH >= log.SEND_DATE && x.IS_ACTIVE && x.BILLING_MONTH.Month % 3 == 0)
                                        .OrderByDescending(x => x.BILLING_MONTH).FirstOrDefault();

                        if (maxRunBill != null)
                        {
                            if (TCPHelper.IsInternetConnected())
                            {
                                PageReportCustomerUsageStatistic p = new PageReportCustomerUsageStatistic();
                                p.sendMail(true);
                            }
                            else
                            {
                                MsgBox.ShowInformation(string.Format(Resources.MSG_PLEASE_SEND_REPORT_STATISTIC_TO_EAC));
                                return;
                            }
                        }
                    }
                    else
                    {
                        if (TCPHelper.IsInternetConnected())
                        {
                            PageReportCustomerUsageStatistic p = new PageReportCustomerUsageStatistic();
                            p.sendMail(true);
                        }
                        else
                        {
                            MsgBox.ShowInformation(string.Format(Resources.MSG_PLEASE_SEND_REPORT_STATISTIC_TO_EAC));
                            return;
                        }
                    }

                }
                Runner.Run(this.viewReport);
            }
        }

        private void viewReport()
        {
            try
            {
                DateTime dt = new DateTime(dtpYear.Value.Year, dtpYear.Value.Month, 1);
                DateTime getDate = DBDataContext.Db.GetSystemDate();

                if ((int)cboReport.SelectedValue == (int)Report.ReportCustomerConnectionType)
                {
                    this.ch = new CrystalReportHelper("ReportREFVerifyCustomer.rpt");
                    ch.SetParameter("@AREA_ID", (int)cboArea.SelectedValue);
                    ch.SetParameter("@AREA_NAME", cboArea.Text);
                    ch.SetParameter("@DATA_MONTH", dt);
                    ch.SetParameter("@BILLING_CYCLE_ID", (int)cboBillingCycle.SelectedValue);
                    ch.SetParameter("@BILLING_CYCLE_NAME", cboBillingCycle.Text);
                }
                else if ((int)cboReport.SelectedValue == (int)Report.ReportCustomerCovid19Subsidy)
                {
                    this.ch = new CrystalReportHelper("ReportCovid19Adjustment.rpt");
                    ch.SetParameter("@AREA_ID", (int)cboArea.SelectedValue);
                    ch.SetParameter("@AREA_NAME", cboArea.Text);
                    ch.SetParameter("@DATA_MONTH", dt);
                    ch.SetParameter("@BILLING_CYCLE_ID", (int)cboBillingCycle.SelectedValue);
                    ch.SetParameter("@BILLING_CYCLE_NAME", cboBillingCycle.Text);
                    ch.SetParameter("@IS_HAVE_ZERO_USAGE", chkIS_HAVE_ZERO_USAGE.Checked);
                }
                else
                {
                    int billingCycleId = (int)cboBillingCycle.SelectedValue;
                    int areaId = (int)cboArea.SelectedValue;

                    using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                    {
                        DBDataContext.Db.CommandTimeout = 20 * 60;
                        DBDataContext.Db.ExecuteCommand("EXEC RUN_REF @DATA_MONTH=@p0", dt);
                        DBDataContext.Db.ExecuteCommand("EXEC RUN_REF_02 @DATA_MONTH=@p0,@BILLING_CYCLE_ID=@p1,@AREA_ID=@p2", dt, billingCycleId, areaId);
                        DBDataContext.Db.ExecuteCommand("EXEC RUN_REF_03A @DATA_MONTH=@p0,@BILLING_CYCLE_ID=@p1,@AREA_ID=@p2", dt, billingCycleId, areaId);
                        DBDataContext.Db.ExecuteCommand("EXEC RUN_REF_03B @DATA_MONTH=@p0,@BILLING_CYCLE_ID=@p1,@AREA_ID=@p2", dt, billingCycleId, areaId);
                        DBDataContext.Db.CommandTimeout = 60;
                        tran.Complete();
                    }
                    if (DBDataContext.Db.TBL_REF_INVALID_CUSTOMERs.Count(x => x.DATA_MONTH == dt
                        && ((int)cboBillingCycle.SelectedValue == 0 || x.BILLING_CYCLE_ID == (int)cboBillingCycle.SelectedValue)) > 0
                        && (int)cboReport.SelectedValue != (int)Report.ReportREFInvalidCustomer)
                    {
                        if (MsgBox.ShowQuestion(Resources.MSG_REF_INVALID_CUSTOMER, Resources.WARNING) == DialogResult.Yes)
                        {
                            cboReport.SelectedValue = (int)Report.ReportREFInvalidCustomer;
                        }
                    }

                    if ((int)cboReport.SelectedValue != (int)Report.ReportREFSubsidy)
                    {
                        this.ch = new CrystalReportHelper((int)cboReport.SelectedValue == (int)Report.ReportREFInvoice ? "ReportREFInvoice.rpt" : "ReportREFInvalidCustomer.rpt");
                        ch.SetParameter("@CURRENCY_NAME", dig.cboCurrency.Text);
                        ch.SetParameter("@CURRENCY_ID", (int)dig.cboCurrency.SelectedValue);
                        ch.SetParameter("@CUSTOMER_GROUP_NAME", dig.cboCustomerGroup.Text);
                        ch.SetParameter("@CUSTOMER_GROUP_ID", (int)dig.cboCustomerGroup.SelectedValue);
                        ch.SetParameter("@CUSTOMER_CONNECTION_TYPE_NAME", dig.cboCustomerConnectionType.Text);
                        ch.SetParameter("@CUSTOMER_CONNECTION_TYPE_ID", (int)dig.cboCustomerConnectionType.SelectedValue);
                        ch.SetParameter("@FILTER_POWER", dig.cboFilterPower.Text);
                        ch.SetParameter("@BILLING_CYCLE_ID", billingCycleId);
                        ch.SetParameter("@BILLING_CYCLE_NAME", cboBillingCycle.Text);
                        ch.SetParameter("@AREA_ID", areaId);
                        ch.SetParameter("@AREA_NAME", cboArea.Text);
                        if (dig.cboFilterPower.SelectedIndex == 0)
                        {
                            ch.SetParameter("@V1", 0);
                            ch.SetParameter("@V2", 10000000);
                        }
                        else if (dig.cboFilterPower.SelectedIndex == 1)
                        {
                            ch.SetParameter("@V1", 0);
                            ch.SetParameter("@V2", 0);
                        }
                        else if (dig.cboFilterPower.SelectedIndex == 2)
                        {
                            ch.SetParameter("@V1", 1);
                            ch.SetParameter("@V2", 10);
                        }
                        else if (dig.cboFilterPower.SelectedIndex == 3)
                        {
                            ch.SetParameter("@V1", 11);
                            ch.SetParameter("@V2", 10000000);
                        }
                        else if (dig.cboFilterPower.SelectedIndex == 4)
                        {
                            ch.SetParameter("@V1", 11);
                            ch.SetParameter("@V2", 50);
                        }
                        else if (dig.cboFilterPower.SelectedIndex == 5)
                        {
                            ch.SetParameter("@V1", 51);
                            ch.SetParameter("@V2", 10000000);
                        }
                    }
                    else
                    {
                        this.ch = new CrystalReportHelper("ReportREFSubsidy.rpt", "REPORT_REF_01", "REPORT_REF_02", "REPORT_REF_03A", "REPORT_REF_03B");
                    }
                    ch.SetParameter("@DATA_MONTH", dt);
                }
                this.viewer.ReportSource = ch.Report;
            }
            catch(Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Resources.YOU_CANNOT_VIEW_REPORT, Resources.WARNING);
            }
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            this.sendMail();
        }

        private void sendMail()
        {
            try
            {
                // this.viewReport();
                if (ch?.Report == null)
                {
                    MsgBox.ShowInformation(Resources.PLEASE_VIEW_REPORT_FIRST);
                    return;
                }

                string exportPath = new FileInfo(Properties.Settings.Default.PATH_TEMP + this.ch.ReportName + " " + licenseNo + ".xls").FullName;
                this.ch.ExportToExcel(exportPath);
                var diag = new DialogSendMail();
                diag.Add(this.ch.ReportName, exportPath);
                diag.txtTo.Text = "eac.tariff@gmail.com";
                diag.txtSubject.Text = Resources.REPORT + cboReport.Text + " " + Resources.MONTH + $" { string.Format("{0:00}", dtpYear.Value.Month) }" + " - " + licenseNo + " " + objCompany.COMPANY_NAME;
                diag.chkMERG_FILE.Checked = false;
                diag.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Resources.YOU_CANNOT_SEND_EMAIL, Resources.WARNING);
            }

        }
        private void cboReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool val = ((int)cboReport.SelectedValue == (int)Report.ReportREFInvoice || (int)cboReport.SelectedValue == (int)Report.ReportREFInvalidCustomer);
            btnSETUP.Visible = val;
            chkIS_HAVE_ZERO_USAGE.Visible = (int)cboReport.SelectedValue == (int)Report.ReportCustomerCovid19Subsidy;
            chkIS_HAVE_ZERO_USAGE.Location = btnSETUP.Location;
            if ((int)cboReport.SelectedValue == (int)Report.reportPrintCustomerCovid19Subsidy)
            {
                btnPRINT.Location = btnMAIL.Location;
            }
            btnPRINT.Visible = (int)cboReport.SelectedValue == (int)Report.reportPrintCustomerCovid19Subsidy;
        }
        DialogREFSubsidyOption dig = new DialogREFSubsidyOption();
        private void btnSETUP_Click(object sender, EventArgs e)
        {
            if (dig.ShowDialog() == DialogResult.OK)
            {
                Runner.Run(this.viewReport);
            }
        }

        private void btnPRINT_Click(object sender, EventArgs e)
        {
            bool isComplete = false;
            DialogPrintSetup diag = new DialogPrintSetup();
            if (diag.ShowDialog() == DialogResult.OK)
            {
                //Runner.RunNewThread(delegate ()
                //{
                var invoiceids = loadPrintInv();
                if (invoiceids.Count > 0)
                {
                    TBL_PRINT_INVOICE objPrintInvoice = new TBL_PRINT_INVOICE()
                    {
                        PRINT_DATE = DBDataContext.Db.GetSystemDate(),
                        LOGIN_ID = Login.CurrentLogin.LOGIN_ID
                    };
                    using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                    {
                        DBDataContext.Db.TBL_PRINT_INVOICEs.InsertOnSubmit(objPrintInvoice);
                        DBDataContext.Db.SubmitChanges();
                        invoiceids.ForEach(x => x.PRINT_INVOICE_ID = objPrintInvoice.PRINT_INVOICE_ID);
                        DBDataContext.Db.BulkCopy(invoiceids);
                        DBDataContext.Db.SubmitChanges();
                        tran.Complete();
                    }
                    CrystalReportHelper cr = Method.GetInvoiceReport();
                    cr.SetParameter("@PRINT_INVOICE_ID", objPrintInvoice.PRINT_INVOICE_ID);
                    cr.PrintReport(Properties.Settings.Default.PRINTER_INVOICE);
                    cr.Dispose();
                    isComplete = true;
                }
                //});
            }
            if (!isComplete)
            {
                MsgBox.ShowInformation(Resources.MSG_NO_INVOICE_COVID_19_SUBSIDY, Resources.WARNING);
            }
        }

        private List<TBL_PRINT_INVOICE_DETAIL> loadPrintInv()
        {
            int i = 1;
            var parameters = new Dictionary<string, object>();
            parameters["@AREA_ID"] = (int)cboArea.SelectedValue;
            parameters["@BILLING_CYCLE_ID"] = (int)cboBillingCycle.SelectedValue;
            parameters["@DATA_MONTH"] = new DateTime(dtpYear.Value.Year, dtpYear.Value.Month, 1);
            parameters["@IS_HAVE_ZERO_USAGE"] = chkIS_HAVE_ZERO_USAGE.Checked;
            var qry = DBDataContext.Db.ExecuteResult("REPORT_CUSTOMER_COVID19_SUBSIDIZED_MONTHLY", parameters);
            var objInvPrints = (qry.Tables[0].AsEnumerable().Select(x => new TBL_PRINT_INVOICE_DETAIL { INVOICE_ID = DataHelper.ParseToInt(x.Field<long>("INVOICE_ID").ToString()), PRINT_ORDER = i++ })).ToList();
            return objInvPrints;
        }
    }
}
