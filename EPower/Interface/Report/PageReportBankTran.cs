﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageReportBankTran : Form
    {
        CrystalReportHelper ch = null;
        public PageReportBankTran()
        {
            InitializeComponent();
            viewer.DefaultView();
            bind();
            var now = DBDataContext.Db.GetSystemDate();
            this.dtpDate1.Value = new DateTime(now.Year, now.Month, 1);
            this.dtpDate2.Value = dtpDate1.Value.AddMonths(1).AddSeconds(-1);
            UIHelper.SetDataSourceToComboBox(cboCurrency, Lookup.GetCurrencies(), Resources.ALL_CURRENCY);
        }

        private void bind()
        {
            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("REPORT_ID", typeof(int));
            dtReport.Columns.Add("REPORT_NAME", typeof(string));
            dtReport.Rows.Add(1, Resources.REPORT_BANK_TRAN_DAILY);
            dtReport.Rows.Add(2, Resources.REPORT_BANK_TRAN_DETAIL);
            UIHelper.SetDataSourceToComboBox(cboReport, dtReport, "REPORT_ID", "REPORT_NAME");
        }
        private void btnViewReport_Click(object sender, EventArgs e)
        {
            Runner.Run(this.viewReport);
        }

        private void viewReport()
        {
            try
            {
                if (this.cboReport.SelectedIndex == 0)
                {
                    this.ch = new CrystalReportHelper("ReportBankTran.rpt");
                }
                else if (this.cboReport.SelectedIndex == 1)
                {
                    this.ch = new CrystalReportHelper("ReportBankTranDetail.rpt");
                }
                ch.SetParameter("@D1", this.dtpDate1.Value);
                ch.SetParameter("@D2", this.dtpDate2.Value);
                ch.SetParameter("@CURRENCY_ID", (int)this.cboCurrency.SelectedValue);
                ch.SetParameter("@CURRENCY_NAME", this.cboCurrency.Text);
                this.viewer.ReportSource = ch.Report;
            }
            catch(Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_VIEW_REPORT, Base.Properties.Resources.WARNING);
            }
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            this.sendMail();
        }

        private void sendMail()
        {
            try
            {
                this.viewReport();
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + this.ch.ReportName + ".xls").FullName;
                this.ch.ExportToExcel(exportPath);
                DialogSendMail.Instance.Add(this.ch.ReportName, exportPath);
                DialogSendMail.Instance.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_SEND_EMAIL, Base.Properties.Resources.WARNING);
            }
        }

        private void cboReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            dtpDate1_ValueChanged(sender, e);
        }

        private void dtpDate1_ValueChanged(object sender, EventArgs e)
        {
            if (this.cboReport.SelectedIndex == 0)
            {
                if (this.dtpDate1.Value.Day == 1)
                {
                    this.dtpDate2.Value = dtpDate1.Value.Date.AddMonths(1).AddSeconds(-1);
                }
                //else
                //{
                //    this.dtpDate2.Value = this.dtpDate1.Value.AddMonths(1).AddSeconds(-1);
                //}
            }
        }

    }
}
