﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.IO;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageReportTrialBalanceDaily : Form
    {
        private CrystalReportHelper ch = null;
        public PageReportTrialBalanceDaily()
        {
            InitializeComponent();
            viewer.DefaultView();
            dtpEndDate.Value = this.dtpDate.Value = DBDataContext.Db.GetSystemDate();
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            Runner.Run(this.viewReport);
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            sendMail();
        }

        public CrystalReportHelper ViewReportTrialBalanceDaily(DateTime dtStart, DateTime dtEnd)
        {
            CrystalReportHelper c = new CrystalReportHelper("ReportTrialBalanceDaily.rpt");
            DateTime datDate = dtEnd.Date;
            DateTime datStartDate = dtStart.Date;
            DateTime datLastDate = datStartDate.Date.AddDays(-1);
            c.SetParameter("@DATE", datDate);
            c.SetParameter("@START_DATE", datStartDate);
            c.SetParameter("@LAST_DATE", datLastDate);
            c.SetParameter("@IS_USE_PREPAID", Settings.Default.IS_USE_PREPAID);
            return c;                  
        }


        private void viewReport()
        {
            try
            {
                this.ch = ViewReportTrialBalanceDaily(dtpDate.Value.Date, dtpEndDate.Value.Date);
                this.viewer.ReportSource = ch.Report;
            }
            catch(Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_VIEW_REPORT, Base.Properties.Resources.WARNING);
            } 
        }

        private void sendMail()
        {
            try
            {
                this.viewReport();
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + this.ch.ReportName + ".xls").FullName;
                this.ch.ExportToExcel(exportPath);
                DialogSendMail.Instance.Add(this.ch.ReportName, exportPath);
                DialogSendMail.Instance.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_SEND_EMAIL, Base.Properties.Resources.WARNING);
            }
        }
    }
}
