﻿using System;
using System.Linq;
using System.Windows.Forms;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PageReportSchedule : Form
    {
        
        public TBL_REPORT_SCHEDULE ReportSend
        {
            get
            {
                TBL_REPORT_SCHEDULE objReportSend = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    try
                    {
                        int intReportSendId = (int)dgv.SelectedRows[0].Cells["SCHEDULE_ID"].Value;
                        objReportSend = DBDataContext.Db.TBL_REPORT_SCHEDULEs.FirstOrDefault(x => x.SCHEDULE_ID == intReportSendId);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objReportSend;
            }
        }

        #region Constructor
        public PageReportSchedule()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            txt_QuickSearch(null, null);
        }
        #endregion

        #region Operation
        /// <summary>
        /// Add new phase.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNew_Click(object sender, EventArgs e)
        {
            
            DialogReportSechdule dig = new DialogReportSechdule(GeneralProcess.Insert, new TBL_REPORT_SCHEDULE()  );
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.ReportSend.SCHEDULE_ID);
            }
        }

        /// <summary>
        /// Edit phase.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                DialogReportSechdule dig = new DialogReportSechdule(GeneralProcess.Update, ReportSend);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.ReportSend.SCHEDULE_ID);
                }
            }
        }

        /// <summary>
        /// Remove phase.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemove_Click(object sender, EventArgs e)
        {
            
                DialogReportSechdule dig = new DialogReportSechdule(GeneralProcess.Delete, ReportSend);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.ReportSend.SCHEDULE_ID - 1);
                }
            
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        #endregion

        #region Method
        /// <summary>
        /// Load data form database and display.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            try
            {
                dgv.DataSource = from rs in DBDataContext.Db.TBL_REPORT_SCHEDULEs
                                 join sp in DBDataContext.Db.TLKP_SEND_PERIODs on rs.SEND_PERIOD_ID equals sp.SEND_PERIOD_ID
                                 where rs.IS_ACTIVE

                                 select new
                                 {
                                     rs.SCHEDULE_ID,
                                     rs.SCHEDULE_NAME,
                                     sp.SEND_PERIOD_NAME,
                                     rs.START_DATE,
                                     rs.START_TIME
                                 };
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
           
        }
        #endregion

      
    }
}