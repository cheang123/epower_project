﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace EPower.Interface.Report
{
    public partial class DialogReportMeterOption : ExDialog
    {
        public DialogReportMeterOption()
        {
            InitializeComponent();
            bind();
        }


        #region Method
        public void bind()
        {
            UIHelper.SetDataSourceToComboBox(this.cboBillingCycle, Lookup.GetBillingCycles(), Resources.ALL_CYCLE);
            UIHelper.SetDataSourceToComboBox(this.cboArea, Lookup.GetAreas(), Resources.ALL_AREA);
            UIHelper.SetDataSourceToComboBox(this.cboType, Lookup.GetMeterType(), Resources.ALL_TYPE);
            UIHelper.SetDataSourceToComboBox(this.cboStatus, Lookup.GetMeterStatus(), Resources.ALL_STATUS);
            txtEnd.Items.Add(Resources.UNLIMITED);
            txtEnd.SelectedIndex = 0;
            cboStatus.SelectedValue = 2;
            txtStart.Text = "10";
        }
        #endregion


        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void picHelp_MouseHover(object sender, EventArgs e)
        {
            ToolTip tp = new ToolTip
            {
                AutoPopDelay = 15000,
                ShowAlways = true,
                OwnerDraw = true,
            };
            tp.SetToolTip(picHelp, Resources.MS_CALCULATE_METER_OLD);
            tp.Draw += new DrawToolTipEventHandler(toolTip1_Draw);
            tp.Popup += new PopupEventHandler(toolTip1_Popup);
        }
        void toolTip1_Popup(object sender, PopupEventArgs e)
        {
            e.ToolTipSize = TextRenderer.MeasureText(Resources.MS_CALCULATE_METER_OLD, new Font("Kh Siemreap", 8.5f));
        }

        void toolTip1_Draw(object sender, DrawToolTipEventArgs e)
        {
            using (e.Graphics)
            {
                Font f = new Font("Kh Siemreap", 8.5f);
                e.DrawBackground();
                e.DrawBorder();
                e.Graphics.DrawString(e.ToolTipText, f, Brushes.Black, new PointF(2, 2));
            }
        }
    }
}
