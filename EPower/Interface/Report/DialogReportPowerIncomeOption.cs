﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogReportPowerIncomeOption : ExDialog
    {
        public DialogReportPowerIncomeOption()
        {
            InitializeComponent();
        }


        #region Method

        public void Bind()
        {
            UIHelper.SetDataSourceToComboBox(this.cboBillingCycle, Lookup.GetBillingCycles(), Resources.ALL_CYCLE);
            UIHelper.SetDataSourceToComboBox(cboArea, Lookup.GetAreas(), Resources.ALL_AREA);
            //UIHelper.SetDataSourceToComboBox(this.cboCustomerType, DBDataContext.Db.TBL_CUSTOMER_TYPEs.Where(x => x.IS_ACTIVE), Resources.DisplayAllCustomer);
            UIHelper.SetDataSourceToComboBox(cboPrice, Lookup.GetPrices(), Resources.ALL_PRICE);
            UIHelper.SetDataSourceToComboBox(cboCurrency, Lookup.GetCurrencies(), Resources.ALL_CURRENCY);
            UIHelper.SetDataSourceToComboBox(cboSTATUS, Lookup.GetCustomerStatuses(), Resources.ALL_STATUS);
            UIHelper.SetDataSourceToComboBox(cboCustomerGroup, Lookup.GetCustomerGroup(), Resources.All_CUSTOMER_GROUP);
        }

        #endregion

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void cboCustomerGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            UIHelper.SetDataSourceToComboBox(cboCustomerConnectionType, DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs.Where(x => x.NONLICENSE_CUSTOMER_GROUP_ID == (int)cboCustomerGroup.SelectedValue).OrderBy(x => x.DESCRIPTION), Resources.All_CUSTOMER_CUSTOMER_TYPE);
            if (cboCustomerConnectionType.DataSource != null)
            {
                cboCustomerConnectionType.SelectedIndex = 0;
            }
        }
    }
}