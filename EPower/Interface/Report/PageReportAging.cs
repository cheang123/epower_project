﻿using System;
using System.IO;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech.Component;
using SoftTech.Helper;
using System.Data;
using EPower.Base.Logic;

namespace EPower.Interface
{
    public partial class PageReportAging : Form
    {
        #region Data
        private CrystalReportHelper ch = null;
        #endregion Data

        #region Constructor
        public PageReportAging()
        {
            InitializeComponent();
            viewer.DefaultView();
            bind();
        }
        #endregion Constructor


        #region Method

        private void bind()
        { 
            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("REPORT_ID", typeof(int));
            dtReport.Columns.Add("REPORT_NAME", typeof(string));
            dtReport.Rows.Add(1, Resources.REPORT_AGING_SUMMARY);
            dtReport.Rows.Add(2, Resources.REPORT_AGING_DETAIL);
            UIHelper.SetDataSourceToComboBox(cboReport, dtReport, "REPORT_ID", "REPORT_NAME");
        }


        public CrystalReportHelper ViewReportAging(DialogReportAgingOptions diag)
        {
            CrystalReportHelper c = new CrystalReportHelper("ReportAgingSummary.rpt");
            c.SetParameter("@HEAD1", string.Format("{0}-{1}", 0, (int)nudDays.Value));
            c.SetParameter("@HEAD2", string.Format("{0}-{1}", (int)nudDays.Value, 2 * (int)nudDays.Value));
            c.SetParameter("@HEAD3", string.Format(" > {0}", 2 * (int)nudDays.Value));
            c.SetParameter("@INTERVAL", (int)nudDays.Value); 
            return c;
        }
        public CrystalReportHelper ViewReportAgingDetail(DialogReportAgingOptions diag)
        {
            CrystalReportHelper c = new CrystalReportHelper("ReportAgingDetail.rpt");
            return c;
        } 
        private void viewReport()
        {
            this.ch = cboReport.SelectedIndex == 0 ? ViewReportAging(diagOption) : ViewReportAgingDetail(diagOption);
            ch.SetParameter("@DATE", dtpDate.Value.Date);
            ch.SetParameter("@MONTH", diagOption.dtpMonth.Checked ? new DateTime(diagOption.dtpMonth.Value.Year, diagOption.dtpMonth.Value.Month, 1) : (DateTime?)null);
            ch.SetParameter("@ITEM_TYPE_ID", (int)diagOption.cboItemType.SelectedValue);
            ch.SetParameter("@ITEM_TYPE_NAME", diagOption.cboItemType.Text);
            ch.SetParameter("@ITEM_ID", (int)diagOption.cboItem.SelectedValue);
            ch.SetParameter("@ITEM_NAME", diagOption.cboItem.Text);
            ch.SetParameter("@CUSTOMER_CONNECTION_TYPE_ID", (int)diagOption.cboCustomerConnectionType.SelectedValue);
            ch.SetParameter("@CUSTOMER_CONNECTION_TYPE_NAME", diagOption.cboCustomerConnectionType.Text);
            ch.SetParameter("@CUSTOMER_GROUP_NAME", diagOption.cboCustomerGroup.Text);
            ch.SetParameter("@CURRENCY_ID", (int)diagOption.cboCurrency.SelectedValue);
            ch.SetParameter("@CURRENCY_NAME", diagOption.cboCurrency.Text);
            ch.SetParameter("@AREA_ID", (int)diagOption.cboArea.SelectedValue);
            ch.SetParameter("@AREA_NAME", diagOption.cboArea.Text);
            ch.SetParameter("@STATUS_ID", (int)diagOption.cboCustomerStatus.SelectedValue);
            ch.SetParameter("@CUS_STATUS_NAME", diagOption.cboCustomerStatus.Text);
            ch.SetParameter("@CYCLE_ID", (int)diagOption.cboCycle.SelectedValue);
            ch.SetParameter("@CYCLE_NAME", diagOption.cboCycle.Text);
            ch.SetParameter("@CUSTOMER_GROUP_TYPE_ID",(int)diagOption.cboCustomerGroup.SelectedValue);
            this.viewer.ReportSource = this.ch.Report;
        }

        private void sendMail()
        {
            try
            { 
                this.viewReport(); 
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + this.ch.ReportName + ".xls").FullName;
                this.ch.ExportToExcel(exportPath);
                DialogSendMail.Instance.Add(this.ch.ReportName,exportPath);
                DialogSendMail.Instance.ShowDialog(); 
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        #endregion Method  
        private void btnViewReport_Click(object sender, EventArgs e)
        {
            Runner.Run(this.viewReport);
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            sendMail();
        }  

        private void cboReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            Application.DoEvents(); 
            lblDAY.Visible  =  this.nudDays.Visible = this.cboReport.SelectedIndex == 0;
        }
        DialogReportAgingOptions diagOption = new DialogReportAgingOptions();
        private void btnSetup_Click(object sender, EventArgs e)
        {
            if (diagOption.ShowDialog() == DialogResult.OK)
            {
                Runner.Run(this.viewReport);
            }
        } 
    }
}
