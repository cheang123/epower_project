﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageReportTrialBalanceDepositDetail : Form
    {
        CrystalReportHelper ch=null;
        DialogReportTrialBalanceDepositDetailOption diag = new DialogReportTrialBalanceDepositDetailOption();
        public PageReportTrialBalanceDepositDetail()
        {
            InitializeComponent();
            viewer.DefaultView();
            bind();
        }
        private void bind()
        {
            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("REPORT_ID", typeof(int));
            dtReport.Columns.Add("REPORT_NAME", typeof(string));
            dtReport.Rows.Add(1, Resources.REPORT_DEPOSIT_DETAIL);
            dtReport.Rows.Add(2, Resources.REPORT_PREPAYMENT);
            dtReport.Rows.Add(3, Resources.REPORT_PREPAYMENT_DETAIL);
            dtReport.Rows.Add(4, Resources.REPORT_PREPAYMENT_REMAIN_DETAIL);
            UIHelper.SetDataSourceToComboBox(cboReport, dtReport, "REPORT_ID", "REPORT_NAME");
            //UIHelper.SetDataSourceToComboBox(cboCurrency, Logic.Lookup.GetCurrencies(), Properties.Resources.DisplayAllCurrency);
            dtpEnd.Value = dtpStart.Value = DBDataContext.Db.GetSystemDate();
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            Runner.Run(this.viewReport);
        }
        public CrystalReportHelper ViewReport(string type)
        {
            CrystalReportHelper c = new CrystalReportHelper(type);
            c.SetParameter("@END_DATE", dtpEnd.Value.Date);
            c.SetParameter("@START_DATE", dtpStart.Value.Date);
            c.SetParameter("@CURRENCY_ID", (int)diag.cboCurrency.SelectedValue);
            c.SetParameter("@CYCLE_ID", (int)diag.cboBillingCycle.SelectedValue);
            c.SetParameter("@AREA", (int)diag.cboArea.SelectedValue);
            c.SetParameter("@CURRENCY_NAME", diag.cboCurrency.Text);
            return c;
        }

        private void viewReport()
        {
            try
            {
                if (cboReport.SelectedIndex == -1)
                {
                    MsgBox.ShowInformation(string.Format(Properties.Resources.REQUIRED_SELECT_VALUE, Resources.REPORT));
                    return;
                }
                var type = "ReportTrialBalanceDepositDetail.rpt";
                int intReportId = DataHelper.ParseToInt(cboReport.SelectedValue.ToString());

                if (intReportId == 1)
                {
                    type = "ReportTrialBalanceDepositDetail.rpt";
                    this.ch = ViewReport(type);
                }
                else if (intReportId == 2)
                {
                    type = "ReportPrepaymentDetail.rpt";
                    this.ch = ViewReport(type);
                    this.ch.SetParameter("@ACTION_ID", diag.cboAction.SelectedValue);
                    this.ch.SetParameter("@PAYMENT_ACCOUNT_ID", (int)diag.cboPaymentAccount.TreeView.SelectedNode.Tag);
                    this.ch.SetParameter("@PAYMENT_ACCOUNT_NAME", diag.cboPaymentAccount.TreeView.SelectedNode.Text);
                }
                else if (intReportId == 3)
                {
                    type = "ReportPrepaymentDetailInvoice.rpt";
                    this.ch = ViewReport(type);
                    DateTime dt = new DateTime(diag.dtpMonth.Value.Year, diag.dtpMonth.Value.Month, 1);
                    this.ch.SetParameter("@DATA_MONTH", diag.dtpMonth.Checked ? dt : UIHelper._DefaultDate);
                    this.ch.SetParameter("@DATA_MONTH_NAME", diag.dtpMonth.Checked ? diag.dtpMonth.Value.ToString("MM-yyyy") : Resources.ALL_DATA_MONTH);
                    this.ch.SetParameter("@ITEM_TYPE_ID", diag.cboItemType.SelectedValue);
                    this.ch.SetParameter("@ITEM_ID", diag.cboItem.SelectedValue);
                    this.ch.SetParameter("@AREA_NAME", diag.cboArea.Text);
                    this.ch.SetParameter("@CYCLE_NAME", diag.cboBillingCycle.Text);
                }
                else if (intReportId == 4)
                {
                    type = "ReportPrepaymentRemainDetail.rpt";
                    this.ch = ViewReport(type);
                    this.ch.SetParameter("@AREA_NAME", diag.cboArea.Text);
                    this.ch.SetParameter("@CYCLE_NAME", diag.cboBillingCycle.Text);
                }

                this.viewer.ReportSource = ch.Report;
            }
            catch(Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_VIEW_REPORT, Base.Properties.Resources.WARNING);
            }
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            this.sendMail();
        }

        private void sendMail()
        {
            try
            {
                this.viewReport();
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + this.ch.ReportName + ".xls").FullName;
                this.ch.ExportToExcel(exportPath);
                DialogSendMail.Instance.Add(this.ch.ReportName, exportPath);
                DialogSendMail.Instance.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_SEND_EMAIL, Base.Properties.Resources.WARNING);
            }
        }

        private void btnSETUP_Click(object sender, EventArgs e)
        {
            int intReportId = DataHelper.ParseToInt(cboReport.SelectedValue.ToString());
            if (intReportId == 1)
            {
                diag.cboAction.Visible = false;
                diag.lblACTION.Visible = false;

                diag.lblPAYMENT_ACCOUNT.Visible = false;
                diag.lblMONTH.Visible = false;
                diag.lblSERVICE.Visible = false;
                diag.lblSERVICE_TYPE.Visible = false;
             
                diag.cboAction.Visible = false;
                diag.cboItemType.Visible = false;
                diag.cboItem.Visible = false;
                diag.dtpMonth.Visible = false;
                diag.cboPaymentAccount.Visible = false;

                diag.cboCurrency.Location = new System.Drawing.Point(121, 73);
                diag.lblCURRENCY.Location= new System.Drawing.Point(10, 77);

                diag.Size = new System.Drawing.Size(356, 210);

            }
            else if (intReportId==2)
            {
                diag.cboAction.Visible = true;
                diag.lblACTION.Visible = true;
                diag.cboItemType.Visible = false;
                diag.cboItem.Visible = false;
                diag.dtpMonth.Visible = false;
                diag.lblMONTH.Visible = false;
                diag.lblSERVICE.Visible = false;
                diag.lblSERVICE_TYPE.Visible = false;
                diag.cboPaymentAccount.Visible = true;
                diag.lblPAYMENT_ACCOUNT.Visible= true;

                diag.cboCurrency.Location = new System.Drawing.Point(121, 73);
                diag.cboAction.Location = new System.Drawing.Point(121, 104);
                diag.lblACTION.Location = new System.Drawing.Point(10, 109);
                diag.lblACTION.Visible = true;
                diag.lblCURRENCY.Location = new System.Drawing.Point(10, 77);
                diag.cboPaymentAccount.Location = new System.Drawing.Point(121, 136);
                diag.lblPAYMENT_ACCOUNT.Location = new System.Drawing.Point(10, 140);

                diag.Size = new System.Drawing.Size(356, 270);
            }
            else if (intReportId==3)
            {
                diag.cboAction.Visible = false;
                diag.lblACTION.Visible = false;

                diag.lblMONTH.Visible = true;
                diag.lblPAYMENT_ACCOUNT.Visible = false;
                diag.lblSERVICE.Visible = true;
                diag.lblSERVICE_TYPE.Visible = true;

                diag.cboAction.Visible = false;
                diag.cboItemType.Visible = true;
                diag.cboItem.Visible = true;
                diag.dtpMonth.Visible = true;
                diag.cboPaymentAccount.Visible = false;

                diag.cboItemType.Location = new System.Drawing.Point(121, 73);
                diag.cboItem.Location = new System.Drawing.Point(121, 105);
                diag.cboCurrency.Location = new System.Drawing.Point(121, 136);
               // diag.cboPaymentAccount.Location = new System.Drawing.Point(121, 167);
                diag.dtpMonth.Location = new System.Drawing.Point(121, 167);

                diag.lblSERVICE_TYPE.Location = new System.Drawing.Point(10, 77);
                diag.lblSERVICE.Location = new System.Drawing.Point(10, 109);
                diag.lblCURRENCY.Location = new System.Drawing.Point(10, 140);
                //diag.lblPAYMENT_ACCOUNT.Location = new System.Drawing.Point(11, 171);
                diag.lblMONTH.Location = new System.Drawing.Point(11, 171);

                diag.Size = new System.Drawing.Size(356, 300);
                
            }
            else if(intReportId==4)
            {
                diag.cboAction.Visible = false;
                diag.lblACTION.Visible = false;

                diag.cboAction.Visible = false;
                diag.cboItemType.Visible = false;
                diag.cboItem.Visible = false;
                diag.dtpMonth.Visible = false;
                diag.cboPaymentAccount.Visible = false;
                diag.lblPAYMENT_ACCOUNT.Visible = false;
                diag.lblMONTH.Visible = false;
                diag.lblPAYMENT_ACCOUNT.Visible = false;
                diag.lblSERVICE.Visible = false;
                diag.lblSERVICE_TYPE.Visible = false;
                diag.cboCurrency.Location = new System.Drawing.Point(121, 73);
                diag.lblCURRENCY.Location= new System.Drawing.Point(10, 77);

                diag.Size = new System.Drawing.Size(356, 210);
            }
            if (diag.ShowDialog() == DialogResult.OK)
            {
                Runner.Run(viewReport);
            }
        }

        private void cboReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void cboReport_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (cboReport.SelectedIndex == -1)
            {
                return;
            }

            else if (cboReport.SelectedIndex == 0)
            {
                dtpEnd.Visible = true;
                btnSETUP.Location = new System.Drawing.Point(360, 6);
            }

            else if (cboReport.SelectedIndex == 1)
            {
                dtpEnd.Visible = true;
                btnSETUP.Location = new System.Drawing.Point(360, 6);
            }

            else if (cboReport.SelectedIndex == 2)
            {
                dtpEnd.Visible = true;
                btnSETUP.Location = new System.Drawing.Point(360, 6);
            }

            else if (cboReport.SelectedIndex == 3)
            {
                dtpEnd.Visible = false;
                btnSETUP.Location = new System.Drawing.Point(260, 6);
            }
        }
    }
}
