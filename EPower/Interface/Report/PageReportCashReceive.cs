﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageReportCashReceive : Form
    {
        CrystalReportHelper ch = null; 
        public PageReportCashReceive()
        {
            InitializeComponent();
            bind();
            viewer.DefaultView();
            this.cboReport.SelectedIndex = 0;
            var now = DBDataContext.Db.GetSystemDate();
            this.dtpDate1.Value = new DateTime(now.Year, now.Month,1);
            this.dtpDate2.Value = this.dtpDate1.Value.AddMonths(1).AddSeconds(-1);
            UIHelper.SetDataSourceToComboBox(cboCurrency, Lookup.GetCurrencies(), Resources.ALL_CURRENCY);

        }

        private void bind()
        {
            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("REPORT_ID", typeof(int));
            dtReport.Columns.Add("REPORT_NAME", typeof(string));
            dtReport.Rows.Add(1, Resources.REPORT_CASH_RECEIVE_DAILY);
            dtReport.Rows.Add(2, Resources.REPORT_CASH_RECEIVE_MONTHLY);
            dtReport.Rows.Add(3, Resources.REPORT_CASH_RECEIVE_YEARLY);
            UIHelper.SetDataSourceToComboBox(cboReport, dtReport, "REPORT_ID", "REPORT_NAME");
        }
 
        private void btnViewReport_Click(object sender, EventArgs e)
        {
            Runner.Run(this.viewReport);
        }

        public CrystalReportHelper ViewReportCashReceiveDaily(DateTime d1, DateTime d2,int currencyId,string currencyName)
        {
            CrystalReportHelper c = new CrystalReportHelper("ReportCashReceivedDaily.rpt");
            c.SetParameter("@D1", d1);
            c.SetParameter("@D2", d2);
            c.SetParameter("@CURRENCY_ID", currencyId);
            c.SetParameter("@CURRENCY_NAME", currencyName);
            return c;
        }
        public CrystalReportHelper ViewReportCashReceiveMontly(int intYear, int currencyId, string currencyName)
        {
            CrystalReportHelper c = new CrystalReportHelper("ReportCashReceivedMonthly.rpt");
            c.SetParameter("@YEAR", intYear);
            c.SetParameter("@CURRENCY_ID", currencyId);
            c.SetParameter("@CURRENCY_NAME", currencyName);
            return c;
        }
        public CrystalReportHelper ViewReportCashReceiveYearly(int y1, int y2, int currencyId, string currencyName)
        {
            CrystalReportHelper c = new CrystalReportHelper("ReportCashReceivedYearly.rpt");
            c.SetParameter("@Y1", y1);
            c.SetParameter("@Y2", y2);
            c.SetParameter("@CURRENCY_ID", currencyId);
            c.SetParameter("@CURRENCY_NAME", currencyName);
            return c;
        }

        private void viewReport()
        {
            try
            {
                if (this.cboReport.SelectedIndex == 0)
                {
                    this.ch = ViewReportCashReceiveDaily(this.dtpDate1.Value, this.dtpDate2.Value, (int)cboCurrency.SelectedValue, cboCurrency.Text);
                }
                else if (this.cboReport.SelectedIndex == 1)
                {
                    this.ch = ViewReportCashReceiveMontly(this.dtpDate1.Value.Year, (int)cboCurrency.SelectedValue, cboCurrency.Text);
                }
                else if (cboReport.SelectedIndex == 2)
                {
                    this.ch = ViewReportCashReceiveYearly(this.dtpDate1.Value.Year, this.dtpDate2.Value.Year, (int)cboCurrency.SelectedValue, cboCurrency.Text);
                }
                this.viewer.ReportSource = ch.Report;
            }
            catch(Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_VIEW_REPORT, Base.Properties.Resources.WARNING);
            }
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            this.sendMail();
        }
        private void sendMail()
        {
            try
            {
                this.viewReport();
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + this.ch.ReportName + ".xls").FullName;
                this.ch.ExportToExcel(exportPath);
                DialogSendMail.Instance.Add(this.ch.ReportName, exportPath);
                DialogSendMail.Instance.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_SEND_EMAIL, Base.Properties.Resources.WARNING);
            }
        }
        private void cboReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboReport.SelectedIndex == 0)
            {
                this.dtpDate1.CustomFormat = this.dtpDate2.CustomFormat = "dd-MM-yyyy HH:mm:ss";
                this.dtpDate2.Visible = true;
            }
            else if (cboReport.SelectedIndex == 1)
            {
                this.dtpDate1.CustomFormat = "yyyy";
                this.dtpDate2.Visible = false;
            }
            else if (cboReport.SelectedIndex == 2) 
            {
                this.dtpDate1.CustomFormat = this.dtpDate2.CustomFormat =  "yyyy";
                this.dtpDate2.Visible = true;

            }
        }

        private void dtpDate1_ValueChanged(object sender, EventArgs e)
        {
            if (dtpDate1.Value.Day == 1)
            {
                dtpDate2.Value = dtpDate1.Value.Date.AddMonths(1).AddSeconds(-1);
            }
            //else
            //{
            //    dtpDate2.Value = this.dtpDate1.Value.AddMonths(1).AddSeconds(-1);
            //}
        }
         
    }
}
