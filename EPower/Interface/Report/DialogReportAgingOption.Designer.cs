﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogReportAgingOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogReportAgingOptions));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClose = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.cboCustomerGroup = new System.Windows.Forms.ComboBox();
            this.cboItemType = new System.Windows.Forms.ComboBox();
            this.cboItem = new System.Windows.Forms.ComboBox();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.dtpMonth = new System.Windows.Forms.DateTimePicker();
            this.lblCUSTOMER_GROUP = new System.Windows.Forms.Label();
            this.lblSERVICE_TYPE = new System.Windows.Forms.Label();
            this.lblSERVICE = new System.Windows.Forms.Label();
            this.lblCURRENCY = new System.Windows.Forms.Label();
            this.lblMONTH = new System.Windows.Forms.Label();
            this.lblAREA = new System.Windows.Forms.Label();
            this.cboArea = new System.Windows.Forms.ComboBox();
            this.lblCUSTOMER_STATUS = new System.Windows.Forms.Label();
            this.cboCustomerStatus = new System.Windows.Forms.ComboBox();
            this.cboCycle = new System.Windows.Forms.ComboBox();
            this.lblBILLING_CYCLE = new System.Windows.Forms.Label();
            this.cboCustomerConnectionType = new System.Windows.Forms.ComboBox();
            this.lblCUSTOMER_CONNECTION_TYPE = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.lblCUSTOMER_CONNECTION_TYPE);
            this.content.Controls.Add(this.cboCustomerConnectionType);
            this.content.Controls.Add(this.cboCycle);
            this.content.Controls.Add(this.lblBILLING_CYCLE);
            this.content.Controls.Add(this.cboArea);
            this.content.Controls.Add(this.cboCustomerGroup);
            this.content.Controls.Add(this.cboItemType);
            this.content.Controls.Add(this.cboCustomerStatus);
            this.content.Controls.Add(this.cboItem);
            this.content.Controls.Add(this.cboCurrency);
            this.content.Controls.Add(this.dtpMonth);
            this.content.Controls.Add(this.lblCUSTOMER_STATUS);
            this.content.Controls.Add(this.lblMONTH);
            this.content.Controls.Add(this.lblSERVICE);
            this.content.Controls.Add(this.lblCURRENCY);
            this.content.Controls.Add(this.lblAREA);
            this.content.Controls.Add(this.lblSERVICE_TYPE);
            this.content.Controls.Add(this.lblCUSTOMER_GROUP);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnClose);
            this.content.Controls.Add(this.btnOK);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnClose, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_GROUP, 0);
            this.content.Controls.SetChildIndex(this.lblSERVICE_TYPE, 0);
            this.content.Controls.SetChildIndex(this.lblAREA, 0);
            this.content.Controls.SetChildIndex(this.lblCURRENCY, 0);
            this.content.Controls.SetChildIndex(this.lblSERVICE, 0);
            this.content.Controls.SetChildIndex(this.lblMONTH, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_STATUS, 0);
            this.content.Controls.SetChildIndex(this.dtpMonth, 0);
            this.content.Controls.SetChildIndex(this.cboCurrency, 0);
            this.content.Controls.SetChildIndex(this.cboItem, 0);
            this.content.Controls.SetChildIndex(this.cboCustomerStatus, 0);
            this.content.Controls.SetChildIndex(this.cboItemType, 0);
            this.content.Controls.SetChildIndex(this.cboCustomerGroup, 0);
            this.content.Controls.SetChildIndex(this.cboArea, 0);
            this.content.Controls.SetChildIndex(this.lblBILLING_CYCLE, 0);
            this.content.Controls.SetChildIndex(this.cboCycle, 0);
            this.content.Controls.SetChildIndex(this.cboCustomerConnectionType, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_CONNECTION_TYPE, 0);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnClose
            // 
            resources.ApplyResources(this.btnClose, "btnClose");
            this.btnClose.Name = "btnClose";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // cboCustomerGroup
            // 
            this.cboCustomerGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomerGroup.DropDownWidth = 200;
            this.cboCustomerGroup.FormattingEnabled = true;
            resources.ApplyResources(this.cboCustomerGroup, "cboCustomerGroup");
            this.cboCustomerGroup.Name = "cboCustomerGroup";
            this.cboCustomerGroup.SelectedIndexChanged += new System.EventHandler(this.cboCustomerGroup_SelectedIndexChanged);
            // 
            // cboItemType
            // 
            this.cboItemType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboItemType.DropDownWidth = 200;
            this.cboItemType.FormattingEnabled = true;
            resources.ApplyResources(this.cboItemType, "cboItemType");
            this.cboItemType.Name = "cboItemType";
            this.cboItemType.SelectedIndexChanged += new System.EventHandler(this.cboItemType_SelectedIndexChanged);
            // 
            // cboItem
            // 
            this.cboItem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboItem.DropDownWidth = 200;
            this.cboItem.FormattingEnabled = true;
            resources.ApplyResources(this.cboItem, "cboItem");
            this.cboItem.Name = "cboItem";
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.DropDownWidth = 100;
            this.cboCurrency.FormattingEnabled = true;
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            // 
            // dtpMonth
            // 
            this.dtpMonth.Checked = false;
            resources.ApplyResources(this.dtpMonth, "dtpMonth");
            this.dtpMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMonth.Name = "dtpMonth";
            this.dtpMonth.ShowCheckBox = true;
            // 
            // lblCUSTOMER_GROUP
            // 
            resources.ApplyResources(this.lblCUSTOMER_GROUP, "lblCUSTOMER_GROUP");
            this.lblCUSTOMER_GROUP.Name = "lblCUSTOMER_GROUP";
            // 
            // lblSERVICE_TYPE
            // 
            resources.ApplyResources(this.lblSERVICE_TYPE, "lblSERVICE_TYPE");
            this.lblSERVICE_TYPE.Name = "lblSERVICE_TYPE";
            // 
            // lblSERVICE
            // 
            resources.ApplyResources(this.lblSERVICE, "lblSERVICE");
            this.lblSERVICE.Name = "lblSERVICE";
            // 
            // lblCURRENCY
            // 
            resources.ApplyResources(this.lblCURRENCY, "lblCURRENCY");
            this.lblCURRENCY.Name = "lblCURRENCY";
            // 
            // lblMONTH
            // 
            resources.ApplyResources(this.lblMONTH, "lblMONTH");
            this.lblMONTH.Name = "lblMONTH";
            // 
            // lblAREA
            // 
            resources.ApplyResources(this.lblAREA, "lblAREA");
            this.lblAREA.Name = "lblAREA";
            // 
            // cboArea
            // 
            this.cboArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboArea.DropDownWidth = 200;
            this.cboArea.FormattingEnabled = true;
            resources.ApplyResources(this.cboArea, "cboArea");
            this.cboArea.Name = "cboArea";
            // 
            // lblCUSTOMER_STATUS
            // 
            resources.ApplyResources(this.lblCUSTOMER_STATUS, "lblCUSTOMER_STATUS");
            this.lblCUSTOMER_STATUS.Name = "lblCUSTOMER_STATUS";
            // 
            // cboCustomerStatus
            // 
            this.cboCustomerStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomerStatus.DropDownWidth = 200;
            this.cboCustomerStatus.FormattingEnabled = true;
            resources.ApplyResources(this.cboCustomerStatus, "cboCustomerStatus");
            this.cboCustomerStatus.Name = "cboCustomerStatus";
            // 
            // cboCycle
            // 
            this.cboCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCycle.DropDownWidth = 200;
            this.cboCycle.FormattingEnabled = true;
            resources.ApplyResources(this.cboCycle, "cboCycle");
            this.cboCycle.Name = "cboCycle";
            // 
            // lblBILLING_CYCLE
            // 
            resources.ApplyResources(this.lblBILLING_CYCLE, "lblBILLING_CYCLE");
            this.lblBILLING_CYCLE.Name = "lblBILLING_CYCLE";
            // 
            // cboCustomerConnectionType
            // 
            this.cboCustomerConnectionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomerConnectionType.DropDownWidth = 200;
            this.cboCustomerConnectionType.FormattingEnabled = true;
            resources.ApplyResources(this.cboCustomerConnectionType, "cboCustomerConnectionType");
            this.cboCustomerConnectionType.Name = "cboCustomerConnectionType";
            // 
            // lblCUSTOMER_CONNECTION_TYPE
            // 
            resources.ApplyResources(this.lblCUSTOMER_CONNECTION_TYPE, "lblCUSTOMER_CONNECTION_TYPE");
            this.lblCUSTOMER_CONNECTION_TYPE.Name = "lblCUSTOMER_CONNECTION_TYPE";
            // 
            // DialogReportAgingOptions
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogReportAgingOptions";
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnClose;
        private ExButton btnOK;
        private Label lblSERVICE;
        private Label lblCURRENCY;
        private Label lblSERVICE_TYPE;
        private Label lblCUSTOMER_GROUP;
        private Label lblMONTH;
        private Label lblAREA;
        public ComboBox cboCustomerGroup;
        public ComboBox cboItemType;
        public ComboBox cboItem;
        public ComboBox cboCurrency;
        public DateTimePicker dtpMonth;
        public ComboBox cboArea;
        public ComboBox cboCustomerStatus;
        private Label lblCUSTOMER_STATUS;
        public ComboBox cboCycle;
        private Label lblBILLING_CYCLE;
        private Label lblCUSTOMER_CONNECTION_TYPE;
        public ComboBox cboCustomerConnectionType;
    }
}