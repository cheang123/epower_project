﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageReportCustomerUsageStatistic : Form
    {
        enum ReportName
        {
            ReportPowerSold = 1,
            ReportPowerSoldByCustomerType = 2,
            PowertPowerSoldByConnectionType = 3,
            ReportCustomerUsageStatistic = 4,
            ReportPowerSold2018 = 5,
            ReportPowerSold2021 = 6
        }

        CrystalReportHelper ch = null;
        TBL_COMPANY objCompany = null;
        TBL_CONFIG objConfig = null;
        string licenseNo = "";
        public PageReportCustomerUsageStatistic()
        {
            InitializeComponent();
            viewer.DefaultView();

            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("REPORT_ID", typeof(int));
            dtReport.Columns.Add("REPORT_NAME", typeof(string));
            dtReport.Rows.Add(1, Resources.REPORT_POWER_SOLD);
            //dtReport.Rows.Add(4, Resources.REPORT_CUSTOMER_USAGE_STATISTIC);
            UIHelper.SetDataSourceToComboBox(cboReport, dtReport, "REPORT_ID", "REPORT_NAME");

            dataLookup();
        }

        private void dataLookup()
        {
            try
            {
                UIHelper.SetDataSourceToComboBox(cboArea, DBDataContext.Db.TBL_AREAs.Where(x => x.IS_ACTIVE).Select(x => new { x.AREA_ID, x.AREA_NAME }).OrderBy(x => x.AREA_NAME), Resources.ALL_AREA);
                UIHelper.SetDataSourceToComboBox(cboBillingCycle, Lookup.GetBillingCycles(), Resources.ALL_CYCLE);
                objCompany = DBDataContext.Db.TBL_COMPANies.FirstOrDefault();
                objConfig = DBDataContext.Db.TBL_CONFIGs.FirstOrDefault();
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp);
            }
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            Runner.Run(this.viewReport);
        }

        private CrystalReportHelper ViewReport(int yearId, int areaId, int bilingCycleId, string lincenseNo)
        {
            string reportName = "ReportPowerSold.rpt";

            //if ((int)cboReport.SelectedValue == (int)ReportName.ReportPowerSold)
            //{
            //    return;
            //}

            if (dtpYear.Value.Year <= 2020)
            {
                reportName = "ReportPowerSoldNoCapacity.rpt";
            }
            if (dtpYear.Value.Year <= 2017)
            {
                reportName = "ReportPowerSoldOld.rpt";
            }

            CrystalReportHelper c = new CrystalReportHelper(reportName);
            c.SetParameter("@YEAR_ID", yearId);
            c.SetParameter("@AREA_ID", areaId);
            c.SetParameter("@BILLING_CYCLE_ID", bilingCycleId);
            c.SetParameter("@LICENSE_NO", lincenseNo);
            return c;
        }

        private void viewReport()
        {
            try
            {
                licenseNo = objCompany.LICENSE_NUMBER.Replace(" ", "") == ""
                       || objCompany.LICENSE_NUMBER.Replace(" ", "").Length < 3
                       ? objConfig.AREA_CODE : objCompany.LICENSE_NUMBER.Replace(" ", "").Substring(0, 3);
                if (DBDataContext.Db.Connection.Database.ToUpper().Contains("DEMO"))
                {
                    licenseNo = "000-DEMO";
                }
                this.ch = ViewReport(
                    dtpYear.Value.Year,
                    (int)cboArea.SelectedValue,
                    (int)cboBillingCycle.SelectedValue,
                    licenseNo
                );
                this.viewer.ReportSource = ch.Report;

            }
            catch(Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_VIEW_REPORT, Base.Properties.Resources.WARNING);
            }
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            this.sendMail();
        }

        public void sendMail(bool instanceSend = false)
        {
            try
            {
                this.viewReport();
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + "EAC Customer Statistics " + licenseNo + ".xls").FullName;
                this.ch.ExportToExcel(exportPath);
                var diag = new DialogSendMail();
                diag.Add("ReportPowerSold" + dtpYear.Value.Year.ToString(), exportPath);
                diag.txtTo.Text = Method.Utilities[Utility.EAC_MAIL_FOR_REPORT_STATISTIC];
                diag.txtSubject.Text = "EAC Customer Statistics - " + licenseNo + " " + objCompany.COMPANY_NAME;
                diag.chkMERG_FILE.Checked = false;
                if (instanceSend)
                {
                    //diag.btnSend_Click(null, null);
                }
                else
                {
                    diag.ShowDialog();
                }
                if (diag.success)
                {
                    TBL_LOG_SEND_MAIL log = new TBL_LOG_SEND_MAIL()
                    {
                        LOG_ID = Guid.NewGuid(),
                        REPORT_DATE = dtpYear.Value.Date,
                        REPORT_NAME = Resources.REPORT_POWER_SOLD,
                        SEND_DATE = DBDataContext.Db.GetSystemDate(),
                        SEND_TO = diag.txtTo.Text
                    };
                    DBDataContext.Db.TBL_LOG_SEND_MAILs.InsertOnSubmit(log);
                    DBDataContext.Db.SubmitChanges();
                }
            }
            catch (Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_SEND_EMAIL, Base.Properties.Resources.WARNING);
            }
        }

        private void PageReportOtherRevenueByMonth_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                dataLookup();
            }
        }
        private void cboSaveState_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            if (cbo.SelectedIndex == -1)
            {
                return;
            }
            cbo.Tag = cbo.SelectedValue;
        }
    }
}
