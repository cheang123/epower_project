﻿namespace EPower.Interface.Report
{
    partial class DialogReportMeterOption
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogReportMeterOption));
            this.cboArea = new System.Windows.Forms.ComboBox();
            this.cboType = new System.Windows.Forms.ComboBox();
            this.cboBillingCycle = new System.Windows.Forms.ComboBox();
            this.cboStatus = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClose = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.lblAREA = new System.Windows.Forms.Label();
            this.lblCUSTOMER_GROUP = new System.Windows.Forms.Label();
            this.lblBILLING_CYCLE = new System.Windows.Forms.Label();
            this.lblSTATUS = new System.Windows.Forms.Label();
            this.lblDURATION = new System.Windows.Forms.Label();
            this.txtEnd = new System.Windows.Forms.ComboBox();
            this.txtStart = new System.Windows.Forms.TextBox();
            this.lblTO = new System.Windows.Forms.Label();
            this.picHelp = new System.Windows.Forms.PictureBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picHelp)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.lblTO);
            this.content.Controls.Add(this.txtStart);
            this.content.Controls.Add(this.lblDURATION);
            this.content.Controls.Add(this.txtEnd);
            this.content.Controls.Add(this.lblSTATUS);
            this.content.Controls.Add(this.lblBILLING_CYCLE);
            this.content.Controls.Add(this.lblCUSTOMER_GROUP);
            this.content.Controls.Add(this.lblAREA);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnClose);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.cboStatus);
            this.content.Controls.Add(this.cboBillingCycle);
            this.content.Controls.Add(this.cboType);
            this.content.Controls.Add(this.cboArea);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.cboArea, 0);
            this.content.Controls.SetChildIndex(this.cboType, 0);
            this.content.Controls.SetChildIndex(this.cboBillingCycle, 0);
            this.content.Controls.SetChildIndex(this.cboStatus, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnClose, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.lblAREA, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_GROUP, 0);
            this.content.Controls.SetChildIndex(this.lblBILLING_CYCLE, 0);
            this.content.Controls.SetChildIndex(this.lblSTATUS, 0);
            this.content.Controls.SetChildIndex(this.txtEnd, 0);
            this.content.Controls.SetChildIndex(this.lblDURATION, 0);
            this.content.Controls.SetChildIndex(this.txtStart, 0);
            this.content.Controls.SetChildIndex(this.lblTO, 0);
            // 
            // cboArea
            // 
            this.cboArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboArea.FormattingEnabled = true;
            resources.ApplyResources(this.cboArea, "cboArea");
            this.cboArea.Name = "cboArea";
            // 
            // cboType
            // 
            this.cboType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboType.FormattingEnabled = true;
            resources.ApplyResources(this.cboType, "cboType");
            this.cboType.Name = "cboType";
            // 
            // cboBillingCycle
            // 
            this.cboBillingCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBillingCycle.FormattingEnabled = true;
            resources.ApplyResources(this.cboBillingCycle, "cboBillingCycle");
            this.cboBillingCycle.Name = "cboBillingCycle";
            // 
            // cboStatus
            // 
            this.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboStatus.FormattingEnabled = true;
            resources.ApplyResources(this.cboStatus, "cboStatus");
            this.cboStatus.Name = "cboStatus";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnClose
            // 
            resources.ApplyResources(this.btnClose, "btnClose");
            this.btnClose.Name = "btnClose";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblAREA
            // 
            resources.ApplyResources(this.lblAREA, "lblAREA");
            this.lblAREA.Name = "lblAREA";
            // 
            // lblCUSTOMER_GROUP
            // 
            resources.ApplyResources(this.lblCUSTOMER_GROUP, "lblCUSTOMER_GROUP");
            this.lblCUSTOMER_GROUP.Name = "lblCUSTOMER_GROUP";
            // 
            // lblBILLING_CYCLE
            // 
            resources.ApplyResources(this.lblBILLING_CYCLE, "lblBILLING_CYCLE");
            this.lblBILLING_CYCLE.Name = "lblBILLING_CYCLE";
            // 
            // lblSTATUS
            // 
            resources.ApplyResources(this.lblSTATUS, "lblSTATUS");
            this.lblSTATUS.Name = "lblSTATUS";
            // 
            // lblDURATION
            // 
            resources.ApplyResources(this.lblDURATION, "lblDURATION");
            this.lblDURATION.Name = "lblDURATION";
            // 
            // txtEnd
            // 
            this.txtEnd.FormattingEnabled = true;
            resources.ApplyResources(this.txtEnd, "txtEnd");
            this.txtEnd.Name = "txtEnd";
            // 
            // txtStart
            // 
            resources.ApplyResources(this.txtStart, "txtStart");
            this.txtStart.Name = "txtStart";
            // 
            // lblTO
            // 
            resources.ApplyResources(this.lblTO, "lblTO");
            this.lblTO.Name = "lblTO";
            // 
            // picHelp
            // 
            this.picHelp.Cursor = System.Windows.Forms.Cursors.Default;
            this.picHelp.Image = global::EPower.Properties.Resources.question_mark;
            resources.ApplyResources(this.picHelp, "picHelp");
            this.picHelp.Name = "picHelp";
            this.picHelp.TabStop = false;
            this.picHelp.MouseHover += new System.EventHandler(this.picHelp_MouseHover);
            // 
            // DialogReportMeterOption
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.picHelp);
            this.Name = "DialogReportMeterOption";
            this.Controls.SetChildIndex(this.picHelp, 0);
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picHelp)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblBILLING_CYCLE;
        private System.Windows.Forms.Label lblCUSTOMER_GROUP;
        private System.Windows.Forms.Label lblAREA;
        public System.Windows.Forms.ComboBox cboType;
        public System.Windows.Forms.ComboBox cboArea;
        public System.Windows.Forms.ComboBox cboStatus;
        public System.Windows.Forms.ComboBox cboBillingCycle;
        public System.Windows.Forms.Label lblSTATUS;
        public System.Windows.Forms.ComboBox txtEnd;
        public System.Windows.Forms.Label lblDURATION;
        public System.Windows.Forms.Panel panel1;
        public SoftTech.Component.ExButton btnClose;
        public SoftTech.Component.ExButton btnOK;
        public System.Windows.Forms.Label lblTO;
        public System.Windows.Forms.TextBox txtStart;
        private System.Windows.Forms.PictureBox picHelp;
    }
}