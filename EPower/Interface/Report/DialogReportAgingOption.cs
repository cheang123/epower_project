﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogReportAgingOptions : ExDialog
    {
        private int _service_typeId = 0;
        private int _last_service_type_id;
        private int _last_service_id;
        private int _last_account_id;
        public DialogReportAgingOptions()
        {
            InitializeComponent();
            Bind();
            //this.Load += DialogReportAgingOptions_Load;
        }

        private void DialogReportAgingOptions_Load(object sender, EventArgs e)
        {
            Bind();
            cboItem.SelectedValue = _last_service_id;
            cboItemType.SelectedValue = _last_service_type_id;
        }

        protected override void OnClosed(EventArgs e)
        {
            _last_service_id = (int)cboItem.SelectedValue;
            _last_service_type_id = (int)cboItemType.SelectedValue;

            base.OnClosed(e);

        }

        #region Method

        public void Bind()
        {
            var ignoreIds = new List<int>()
            {
                -6,-7
            };
            UIHelper.SetDataSourceToComboBox(this.cboItemType, DBDataContext.Db.TBL_INVOICE_ITEM_TYPEs.Where(x => x.IS_ACTIVE).OrderBy(x => x.INVOICE_ITEM_TYPE_ID), Resources.ALL_SERVICE_TYPE);
            var sources = DBDataContext.Db.TBL_INVOICE_ITEMs.OrderBy(x => x.INVOICE_ITEM_TYPE_ID).ThenBy(x => x.INVOICE_ITEM_ID).Where(x => x.IS_ACTIVE && !ignoreIds.Contains(x.INVOICE_ITEM_ID) && (x.INVOICE_ITEM_TYPE_ID == _last_service_type_id || _service_typeId == 0)).Select(x => new { x.INVOICE_ITEM_ID, x.INVOICE_ITEM_NAME });
            UIHelper.SetDataSourceToComboBox(this.cboItem, sources, Resources.ALL_SERVICE);
            UIHelper.SetDataSourceToComboBox(this.cboCustomerGroup, Lookup.GetCustomerGroup(), Resources.All_CUSTOMER_GROUP);
            UIHelper.SetDataSourceToComboBox(cboCurrency, Lookup.GetCurrencies(), Resources.ALL_CURRENCY);
            UIHelper.SetDataSourceToComboBox(cboArea, Lookup.GetAreas(), Resources.ALL_AREA);
            UIHelper.SetDataSourceToComboBox(cboCustomerStatus, Lookup.GetCustomerStatuses(), Resources.ALL_STATUS);
            UIHelper.SetDataSourceToComboBox(cboCycle, Lookup.GetBillingCycles(), Resources.ALL_CYCLE);
        }
        #endregion

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void cboItemType_SelectedIndexChanged(object sender, EventArgs e)
        {
            var ignoreIds = new List<int>()
            {
                -6,-7
            };

            int typeId = cboItemType.SelectedIndex == -1 ? 0 : (int)cboItemType.SelectedValue;
            if (typeId == _service_typeId)
            {
                return;
            }
            UIHelper.SetDataSourceToComboBox(cboItem, DBDataContext.Db.TBL_INVOICE_ITEMs.OrderBy(x => x.INVOICE_ITEM_TYPE_ID).ThenBy(x => x.INVOICE_ITEM_ID).Where(x => !ignoreIds.Contains(x.INVOICE_ITEM_ID) && (x.INVOICE_ITEM_TYPE_ID == typeId || typeId == 0) && x.IS_ACTIVE).Select(x => new { x.INVOICE_ITEM_ID, x.INVOICE_ITEM_NAME }), Resources.ALL_SERVICE);
            cboItem.SelectedValue = 0;
            _service_typeId = typeId;
        }

        private void cboCustomerGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            UIHelper.SetDataSourceToComboBox(cboCustomerConnectionType, DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs.Where(x => x.NONLICENSE_CUSTOMER_GROUP_ID == (int)cboCustomerGroup.SelectedValue).OrderBy(x => x.DESCRIPTION), Resources.All_CUSTOMER_CUSTOMER_TYPE);
            if (cboCustomerConnectionType.DataSource != null)
            {
                cboCustomerConnectionType.SelectedIndex = 0;
            }
        }
    }
}