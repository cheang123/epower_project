﻿using System.ComponentModel;
using System.Windows.Forms;
using AxCrystalActiveXReportViewerLib105;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageReportTransformerDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageReportTransformerDetail));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnMail = new SoftTech.Component.ExButton();
            this.label1 = new System.Windows.Forms.Label();
            this.btnViewReport = new SoftTech.Component.ExButton();
            this.cboCapacity = new System.Windows.Forms.ComboBox();
            this.viewer = new AxCrystalActiveXReportViewerLib105.AxCrystalActiveXReportViewer();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.viewer)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AccessibleDescription = null;
            this.panel1.AccessibleName = null;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.btnMail);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnViewReport);
            this.panel1.Controls.Add(this.cboCapacity);
            this.panel1.Font = null;
            this.panel1.Name = "panel1";
            // 
            // btnMail
            // 
            this.btnMail.AccessibleDescription = null;
            this.btnMail.AccessibleName = null;
            resources.ApplyResources(this.btnMail, "btnMail");
            this.btnMail.BackgroundImage = null;
            this.btnMail.Name = "btnMail";
            this.btnMail.UseVisualStyleBackColor = true;
            this.btnMail.Click += new System.EventHandler(this.btnMail_Click);
            // 
            // label1
            // 
            this.label1.AccessibleDescription = null;
            this.label1.AccessibleName = null;
            resources.ApplyResources(this.label1, "label1");
            this.label1.Font = null;
            this.label1.Name = "label1";
            // 
            // btnViewReport
            // 
            this.btnViewReport.AccessibleDescription = null;
            this.btnViewReport.AccessibleName = null;
            resources.ApplyResources(this.btnViewReport, "btnViewReport");
            this.btnViewReport.BackgroundImage = null;
            this.btnViewReport.Name = "btnViewReport";
            this.btnViewReport.UseVisualStyleBackColor = true;
            this.btnViewReport.Click += new System.EventHandler(this.btnViewReport_Click);
            // 
            // cboCapacity
            // 
            this.cboCapacity.AccessibleDescription = null;
            this.cboCapacity.AccessibleName = null;
            resources.ApplyResources(this.cboCapacity, "cboCapacity");
            this.cboCapacity.BackgroundImage = null;
            this.cboCapacity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCapacity.Font = null;
            this.cboCapacity.FormattingEnabled = true;
            this.cboCapacity.Name = "cboCapacity";
            // 
            // viewer
            // 
            this.viewer.AccessibleDescription = null;
            this.viewer.AccessibleName = null;
            resources.ApplyResources(this.viewer, "viewer");
            this.viewer.Font = null;
            this.viewer.Name = "viewer";
            this.viewer.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("viewer.OcxState")));
            // 
            // PageReportTransformerDetail
            // 
            this.AccessibleDescription = null;
            this.AccessibleName = null;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = null;
            this.Controls.Add(this.viewer);
            this.Controls.Add(this.panel1);
            this.Icon = null;
            this.Name = "PageReportTransformerDetail";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.viewer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        public ComboBox cboCapacity;
        private ExButton btnViewReport;
        private AxCrystalActiveXReportViewer viewer;
        private Label label1;
        private ExButton btnMail;
    }
}
