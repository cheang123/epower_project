﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogReportTrialBalanceDepositDetailOption
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogReportTrialBalanceDepositDetailOption));
            this.cboBillingCycle = new System.Windows.Forms.ComboBox();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.lblCYCLE_NAME = new System.Windows.Forms.Label();
            this.lblAREA = new System.Windows.Forms.Label();
            this.cboArea = new System.Windows.Forms.ComboBox();
            this.cboAction = new System.Windows.Forms.ComboBox();
            this.lblACTION = new System.Windows.Forms.Label();
            this.lblPAYMENT_ACCOUNT = new System.Windows.Forms.Label();
            this.cboPaymentAccount = new SoftTech.Component.TreeComboBox();
            this.cboItemType = new System.Windows.Forms.ComboBox();
            this.cboItem = new System.Windows.Forms.ComboBox();
            this.lblSERVICE = new System.Windows.Forms.Label();
            this.lblSERVICE_TYPE = new System.Windows.Forms.Label();
            this.dtpMonth = new System.Windows.Forms.DateTimePicker();
            this.lblMONTH = new System.Windows.Forms.Label();
            this.lblCURRENCY = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClose = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.panel2);
            this.content.Controls.Add(this.lblCURRENCY);
            this.content.Controls.Add(this.dtpMonth);
            this.content.Controls.Add(this.lblMONTH);
            this.content.Controls.Add(this.cboItemType);
            this.content.Controls.Add(this.cboItem);
            this.content.Controls.Add(this.lblSERVICE);
            this.content.Controls.Add(this.lblSERVICE_TYPE);
            this.content.Controls.Add(this.lblPAYMENT_ACCOUNT);
            this.content.Controls.Add(this.cboPaymentAccount);
            this.content.Controls.Add(this.cboAction);
            this.content.Controls.Add(this.lblACTION);
            this.content.Controls.Add(this.cboArea);
            this.content.Controls.Add(this.cboBillingCycle);
            this.content.Controls.Add(this.cboCurrency);
            this.content.Controls.Add(this.lblAREA);
            this.content.Controls.Add(this.lblCYCLE_NAME);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblCYCLE_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblAREA, 0);
            this.content.Controls.SetChildIndex(this.cboCurrency, 0);
            this.content.Controls.SetChildIndex(this.cboBillingCycle, 0);
            this.content.Controls.SetChildIndex(this.cboArea, 0);
            this.content.Controls.SetChildIndex(this.lblACTION, 0);
            this.content.Controls.SetChildIndex(this.cboAction, 0);
            this.content.Controls.SetChildIndex(this.cboPaymentAccount, 0);
            this.content.Controls.SetChildIndex(this.lblPAYMENT_ACCOUNT, 0);
            this.content.Controls.SetChildIndex(this.lblSERVICE_TYPE, 0);
            this.content.Controls.SetChildIndex(this.lblSERVICE, 0);
            this.content.Controls.SetChildIndex(this.cboItem, 0);
            this.content.Controls.SetChildIndex(this.cboItemType, 0);
            this.content.Controls.SetChildIndex(this.lblMONTH, 0);
            this.content.Controls.SetChildIndex(this.dtpMonth, 0);
            this.content.Controls.SetChildIndex(this.lblCURRENCY, 0);
            this.content.Controls.SetChildIndex(this.panel2, 0);
            // 
            // cboBillingCycle
            // 
            this.cboBillingCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBillingCycle.DropDownWidth = 200;
            this.cboBillingCycle.FormattingEnabled = true;
            resources.ApplyResources(this.cboBillingCycle, "cboBillingCycle");
            this.cboBillingCycle.Name = "cboBillingCycle";
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.DropDownWidth = 100;
            this.cboCurrency.FormattingEnabled = true;
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            // 
            // lblCYCLE_NAME
            // 
            resources.ApplyResources(this.lblCYCLE_NAME, "lblCYCLE_NAME");
            this.lblCYCLE_NAME.Name = "lblCYCLE_NAME";
            // 
            // lblAREA
            // 
            resources.ApplyResources(this.lblAREA, "lblAREA");
            this.lblAREA.Name = "lblAREA";
            // 
            // cboArea
            // 
            this.cboArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboArea.DropDownWidth = 200;
            this.cboArea.FormattingEnabled = true;
            resources.ApplyResources(this.cboArea, "cboArea");
            this.cboArea.Name = "cboArea";
            // 
            // cboAction
            // 
            this.cboAction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAction.DropDownWidth = 100;
            this.cboAction.FormattingEnabled = true;
            resources.ApplyResources(this.cboAction, "cboAction");
            this.cboAction.Name = "cboAction";
            // 
            // lblACTION
            // 
            resources.ApplyResources(this.lblACTION, "lblACTION");
            this.lblACTION.Name = "lblACTION";
            // 
            // lblPAYMENT_ACCOUNT
            // 
            resources.ApplyResources(this.lblPAYMENT_ACCOUNT, "lblPAYMENT_ACCOUNT");
            this.lblPAYMENT_ACCOUNT.Name = "lblPAYMENT_ACCOUNT";
            // 
            // cboPaymentAccount
            // 
            this.cboPaymentAccount.AbsoluteChildrenSelectableOnly = true;
            this.cboPaymentAccount.BranchSeparator = "/";
            this.cboPaymentAccount.Imagelist = null;
            resources.ApplyResources(this.cboPaymentAccount, "cboPaymentAccount");
            this.cboPaymentAccount.Name = "cboPaymentAccount";
            this.cboPaymentAccount.PopupHeight = 250;
            this.cboPaymentAccount.PopupWidth = 200;
            this.cboPaymentAccount.SelectedNode = null;
            // 
            // cboItemType
            // 
            this.cboItemType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboItemType.DropDownWidth = 200;
            this.cboItemType.FormattingEnabled = true;
            resources.ApplyResources(this.cboItemType, "cboItemType");
            this.cboItemType.Name = "cboItemType";
            this.cboItemType.SelectedIndexChanged += new System.EventHandler(this.cboItemType_SelectedIndexChanged);
            this.cboItemType.SelectedValueChanged += new System.EventHandler(this.cboItemType_SelectedValueChanged);
            // 
            // cboItem
            // 
            this.cboItem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboItem.DropDownWidth = 200;
            this.cboItem.FormattingEnabled = true;
            resources.ApplyResources(this.cboItem, "cboItem");
            this.cboItem.Name = "cboItem";
            // 
            // lblSERVICE
            // 
            resources.ApplyResources(this.lblSERVICE, "lblSERVICE");
            this.lblSERVICE.Name = "lblSERVICE";
            // 
            // lblSERVICE_TYPE
            // 
            resources.ApplyResources(this.lblSERVICE_TYPE, "lblSERVICE_TYPE");
            this.lblSERVICE_TYPE.Name = "lblSERVICE_TYPE";
            // 
            // dtpMonth
            // 
            this.dtpMonth.Checked = false;
            resources.ApplyResources(this.dtpMonth, "dtpMonth");
            this.dtpMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMonth.Name = "dtpMonth";
            this.dtpMonth.ShowCheckBox = true;
            // 
            // lblMONTH
            // 
            resources.ApplyResources(this.lblMONTH, "lblMONTH");
            this.lblMONTH.Name = "lblMONTH";
            // 
            // lblCURRENCY
            // 
            resources.ApplyResources(this.lblCURRENCY, "lblCURRENCY");
            this.lblCURRENCY.Name = "lblCURRENCY";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.btnClose);
            this.panel2.Controls.Add(this.btnOK);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnClose
            // 
            resources.ApplyResources(this.btnClose, "btnClose");
            this.btnClose.Name = "btnClose";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click_1);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click_1);
            // 
            // DialogReportTrialBalanceDepositDetailOption
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogReportTrialBalanceDepositDetailOption";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private Label lblCYCLE_NAME;
        private Label lblAREA;
        public ComboBox cboBillingCycle;
        public ComboBox cboCurrency;
        public ComboBox cboArea;
        public ComboBox cboAction;
        public Label lblACTION;
        public Label lblPAYMENT_ACCOUNT;
        public TreeComboBox cboPaymentAccount;
        public ComboBox cboItemType;
        public ComboBox cboItem;
        public Label lblSERVICE;
        public Label lblSERVICE_TYPE;
        public DateTimePicker dtpMonth;
        public Label lblMONTH;
        public Label lblCURRENCY;
        private Panel panel2;
        private Panel panel1;
        private ExButton btnClose;
        private ExButton btnOK;
    }
}