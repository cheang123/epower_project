﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogReportQuarterOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogReportQuarterOptions));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClose = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.cboYear = new System.Windows.Forms.ComboBox();
            this.cboQuarter = new System.Windows.Forms.ComboBox();
            this.lblYEAR = new System.Windows.Forms.Label();
            this.lblQUARTER = new System.Windows.Forms.Label();
            this.lblSHOW = new System.Windows.Forms.Label();
            this.chkREPORT_COVER = new System.Windows.Forms.CheckBox();
            this.chkTABLE_GENERATION_FACILITY = new System.Windows.Forms.CheckBox();
            this.chkTABLE_POWER_GENERATION = new System.Windows.Forms.CheckBox();
            this.chkTABLE_DISTRIBUTION_FACILTITY = new System.Windows.Forms.CheckBox();
            this.chkTABLE_SOLD_LICENSEE = new System.Windows.Forms.CheckBox();
            this.chkTABLE_SOLD_CONSUMER = new System.Windows.Forms.CheckBox();
            this.chkTABLE_TRANSFORMER = new System.Windows.Forms.CheckBox();
            this.chkTABLE_CUSTOMER_LICENSEE = new System.Windows.Forms.CheckBox();
            this.chkTABLE_CUSTOMER_CONSUMER = new System.Windows.Forms.CheckBox();
            this.chkTABLE_POWER_SOURCE = new System.Windows.Forms.CheckBox();
            this.chkTABLE_POWER_PURCHASE = new System.Windows.Forms.CheckBox();
            this.chkTABLE_POWER_COVERAGE = new System.Windows.Forms.CheckBox();
            this.chkSHOW_HIDE_ALL = new System.Windows.Forms.CheckBox();
            this.dtpCreateDate = new System.Windows.Forms.DateTimePicker();
            this.lblDATE = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.dtpCreateDate);
            this.content.Controls.Add(this.chkSHOW_HIDE_ALL);
            this.content.Controls.Add(this.chkTABLE_POWER_COVERAGE);
            this.content.Controls.Add(this.chkTABLE_POWER_PURCHASE);
            this.content.Controls.Add(this.chkTABLE_POWER_SOURCE);
            this.content.Controls.Add(this.chkTABLE_CUSTOMER_LICENSEE);
            this.content.Controls.Add(this.chkTABLE_CUSTOMER_CONSUMER);
            this.content.Controls.Add(this.chkTABLE_SOLD_LICENSEE);
            this.content.Controls.Add(this.chkTABLE_SOLD_CONSUMER);
            this.content.Controls.Add(this.chkTABLE_TRANSFORMER);
            this.content.Controls.Add(this.chkTABLE_DISTRIBUTION_FACILTITY);
            this.content.Controls.Add(this.chkTABLE_POWER_GENERATION);
            this.content.Controls.Add(this.chkTABLE_GENERATION_FACILITY);
            this.content.Controls.Add(this.chkREPORT_COVER);
            this.content.Controls.Add(this.lblSHOW);
            this.content.Controls.Add(this.cboYear);
            this.content.Controls.Add(this.cboQuarter);
            this.content.Controls.Add(this.lblYEAR);
            this.content.Controls.Add(this.lblDATE);
            this.content.Controls.Add(this.lblQUARTER);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnClose);
            this.content.Controls.Add(this.btnOK);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnClose, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.lblQUARTER, 0);
            this.content.Controls.SetChildIndex(this.lblDATE, 0);
            this.content.Controls.SetChildIndex(this.lblYEAR, 0);
            this.content.Controls.SetChildIndex(this.cboQuarter, 0);
            this.content.Controls.SetChildIndex(this.cboYear, 0);
            this.content.Controls.SetChildIndex(this.lblSHOW, 0);
            this.content.Controls.SetChildIndex(this.chkREPORT_COVER, 0);
            this.content.Controls.SetChildIndex(this.chkTABLE_GENERATION_FACILITY, 0);
            this.content.Controls.SetChildIndex(this.chkTABLE_POWER_GENERATION, 0);
            this.content.Controls.SetChildIndex(this.chkTABLE_DISTRIBUTION_FACILTITY, 0);
            this.content.Controls.SetChildIndex(this.chkTABLE_TRANSFORMER, 0);
            this.content.Controls.SetChildIndex(this.chkTABLE_SOLD_CONSUMER, 0);
            this.content.Controls.SetChildIndex(this.chkTABLE_SOLD_LICENSEE, 0);
            this.content.Controls.SetChildIndex(this.chkTABLE_CUSTOMER_CONSUMER, 0);
            this.content.Controls.SetChildIndex(this.chkTABLE_CUSTOMER_LICENSEE, 0);
            this.content.Controls.SetChildIndex(this.chkTABLE_POWER_SOURCE, 0);
            this.content.Controls.SetChildIndex(this.chkTABLE_POWER_PURCHASE, 0);
            this.content.Controls.SetChildIndex(this.chkTABLE_POWER_COVERAGE, 0);
            this.content.Controls.SetChildIndex(this.chkSHOW_HIDE_ALL, 0);
            this.content.Controls.SetChildIndex(this.dtpCreateDate, 0);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnClose
            // 
            resources.ApplyResources(this.btnClose, "btnClose");
            this.btnClose.Name = "btnClose";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // cboYear
            // 
            this.cboYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboYear.FormattingEnabled = true;
            this.cboYear.Items.AddRange(new object[] {
            resources.GetString("cboYear.Items"),
            resources.GetString("cboYear.Items1")});
            resources.ApplyResources(this.cboYear, "cboYear");
            this.cboYear.Name = "cboYear";
            // 
            // cboQuarter
            // 
            this.cboQuarter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboQuarter.FormattingEnabled = true;
            this.cboQuarter.Items.AddRange(new object[] {
            resources.GetString("cboQuarter.Items"),
            resources.GetString("cboQuarter.Items1"),
            resources.GetString("cboQuarter.Items2"),
            resources.GetString("cboQuarter.Items3")});
            resources.ApplyResources(this.cboQuarter, "cboQuarter");
            this.cboQuarter.Name = "cboQuarter";
            // 
            // lblYEAR
            // 
            resources.ApplyResources(this.lblYEAR, "lblYEAR");
            this.lblYEAR.Name = "lblYEAR";
            // 
            // lblQUARTER
            // 
            resources.ApplyResources(this.lblQUARTER, "lblQUARTER");
            this.lblQUARTER.Name = "lblQUARTER";
            // 
            // lblSHOW
            // 
            resources.ApplyResources(this.lblSHOW, "lblSHOW");
            this.lblSHOW.Name = "lblSHOW";
            // 
            // chkREPORT_COVER
            // 
            resources.ApplyResources(this.chkREPORT_COVER, "chkREPORT_COVER");
            this.chkREPORT_COVER.Name = "chkREPORT_COVER";
            this.chkREPORT_COVER.UseVisualStyleBackColor = true;
            // 
            // chkTABLE_GENERATION_FACILITY
            // 
            resources.ApplyResources(this.chkTABLE_GENERATION_FACILITY, "chkTABLE_GENERATION_FACILITY");
            this.chkTABLE_GENERATION_FACILITY.Name = "chkTABLE_GENERATION_FACILITY";
            this.chkTABLE_GENERATION_FACILITY.UseVisualStyleBackColor = true;
            // 
            // chkTABLE_POWER_GENERATION
            // 
            resources.ApplyResources(this.chkTABLE_POWER_GENERATION, "chkTABLE_POWER_GENERATION");
            this.chkTABLE_POWER_GENERATION.Name = "chkTABLE_POWER_GENERATION";
            this.chkTABLE_POWER_GENERATION.UseVisualStyleBackColor = true;
            // 
            // chkTABLE_DISTRIBUTION_FACILTITY
            // 
            resources.ApplyResources(this.chkTABLE_DISTRIBUTION_FACILTITY, "chkTABLE_DISTRIBUTION_FACILTITY");
            this.chkTABLE_DISTRIBUTION_FACILTITY.Name = "chkTABLE_DISTRIBUTION_FACILTITY";
            this.chkTABLE_DISTRIBUTION_FACILTITY.UseVisualStyleBackColor = true;
            // 
            // chkTABLE_SOLD_LICENSEE
            // 
            resources.ApplyResources(this.chkTABLE_SOLD_LICENSEE, "chkTABLE_SOLD_LICENSEE");
            this.chkTABLE_SOLD_LICENSEE.Name = "chkTABLE_SOLD_LICENSEE";
            this.chkTABLE_SOLD_LICENSEE.UseVisualStyleBackColor = true;
            // 
            // chkTABLE_SOLD_CONSUMER
            // 
            resources.ApplyResources(this.chkTABLE_SOLD_CONSUMER, "chkTABLE_SOLD_CONSUMER");
            this.chkTABLE_SOLD_CONSUMER.Name = "chkTABLE_SOLD_CONSUMER";
            this.chkTABLE_SOLD_CONSUMER.UseVisualStyleBackColor = true;
            // 
            // chkTABLE_TRANSFORMER
            // 
            resources.ApplyResources(this.chkTABLE_TRANSFORMER, "chkTABLE_TRANSFORMER");
            this.chkTABLE_TRANSFORMER.Name = "chkTABLE_TRANSFORMER";
            this.chkTABLE_TRANSFORMER.UseVisualStyleBackColor = true;
            // 
            // chkTABLE_CUSTOMER_LICENSEE
            // 
            resources.ApplyResources(this.chkTABLE_CUSTOMER_LICENSEE, "chkTABLE_CUSTOMER_LICENSEE");
            this.chkTABLE_CUSTOMER_LICENSEE.Name = "chkTABLE_CUSTOMER_LICENSEE";
            this.chkTABLE_CUSTOMER_LICENSEE.UseVisualStyleBackColor = true;
            // 
            // chkTABLE_CUSTOMER_CONSUMER
            // 
            resources.ApplyResources(this.chkTABLE_CUSTOMER_CONSUMER, "chkTABLE_CUSTOMER_CONSUMER");
            this.chkTABLE_CUSTOMER_CONSUMER.Name = "chkTABLE_CUSTOMER_CONSUMER";
            this.chkTABLE_CUSTOMER_CONSUMER.UseVisualStyleBackColor = true;
            // 
            // chkTABLE_POWER_SOURCE
            // 
            resources.ApplyResources(this.chkTABLE_POWER_SOURCE, "chkTABLE_POWER_SOURCE");
            this.chkTABLE_POWER_SOURCE.Name = "chkTABLE_POWER_SOURCE";
            this.chkTABLE_POWER_SOURCE.UseVisualStyleBackColor = true;
            // 
            // chkTABLE_POWER_PURCHASE
            // 
            resources.ApplyResources(this.chkTABLE_POWER_PURCHASE, "chkTABLE_POWER_PURCHASE");
            this.chkTABLE_POWER_PURCHASE.Name = "chkTABLE_POWER_PURCHASE";
            this.chkTABLE_POWER_PURCHASE.UseVisualStyleBackColor = true;
            // 
            // chkTABLE_POWER_COVERAGE
            // 
            resources.ApplyResources(this.chkTABLE_POWER_COVERAGE, "chkTABLE_POWER_COVERAGE");
            this.chkTABLE_POWER_COVERAGE.Name = "chkTABLE_POWER_COVERAGE";
            this.chkTABLE_POWER_COVERAGE.UseVisualStyleBackColor = true;
            // 
            // chkSHOW_HIDE_ALL
            // 
            resources.ApplyResources(this.chkSHOW_HIDE_ALL, "chkSHOW_HIDE_ALL");
            this.chkSHOW_HIDE_ALL.Name = "chkSHOW_HIDE_ALL";
            this.chkSHOW_HIDE_ALL.UseVisualStyleBackColor = true;
            this.chkSHOW_HIDE_ALL.CheckedChanged += new System.EventHandler(this.CHK_SHOW_HIDE_ALL_CheckedChanged);
            // 
            // dtpCreateDate
            // 
            resources.ApplyResources(this.dtpCreateDate, "dtpCreateDate");
            this.dtpCreateDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpCreateDate.Name = "dtpCreateDate";
            // 
            // lblDATE
            // 
            resources.ApplyResources(this.lblDATE, "lblDATE");
            this.lblDATE.Name = "lblDATE";
            // 
            // DialogReportQuarterOptions
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogReportQuarterOptions";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnClose;
        private ExButton btnOK;
        private Label lblSHOW;
        public ComboBox cboYear;
        public ComboBox cboQuarter;
        private Label lblYEAR;
        private Label lblQUARTER;
        public CheckBox chkTABLE_DISTRIBUTION_FACILTITY;
        public CheckBox chkTABLE_POWER_GENERATION;
        public CheckBox chkTABLE_GENERATION_FACILITY;
        public CheckBox chkREPORT_COVER;
        public CheckBox chkTABLE_POWER_SOURCE;
        public CheckBox chkTABLE_CUSTOMER_LICENSEE;
        public CheckBox chkTABLE_CUSTOMER_CONSUMER;
        public CheckBox chkTABLE_SOLD_LICENSEE;
        public CheckBox chkTABLE_SOLD_CONSUMER;
        public CheckBox chkTABLE_TRANSFORMER;
        public CheckBox chkTABLE_POWER_COVERAGE;
        public CheckBox chkTABLE_POWER_PURCHASE;
        public CheckBox chkSHOW_HIDE_ALL;
        private Label lblDATE;
        public DateTimePicker dtpCreateDate;
    }
}