﻿using System.ComponentModel;
using System.Windows.Forms;
using AxCrystalActiveXReportViewerLib105;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageReportUsageMonthly
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageReportUsageMonthly));
            this.panel1 = new System.Windows.Forms.Panel();
            this.cboReport = new System.Windows.Forms.ComboBox();
            this.lblUSAGE_IN_MONTH = new System.Windows.Forms.Label();
            this.dtpByMonth = new System.Windows.Forms.DateTimePicker();
            this.cboCycle = new System.Windows.Forms.ComboBox();
            this.cboArea = new System.Windows.Forms.ComboBox();
            this.cboOperator = new System.Windows.Forms.ComboBox();
            this.txtValue1 = new System.Windows.Forms.TextBox();
            this.txtValue2 = new System.Windows.Forms.TextBox();
            this.btnSETUP = new SoftTech.Component.ExButton();
            this.btnMAIL = new SoftTech.Component.ExButton();
            this.btnVIEW = new SoftTech.Component.ExButton();
            this.viewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.cboReport);
            this.panel1.Controls.Add(this.lblUSAGE_IN_MONTH);
            this.panel1.Controls.Add(this.dtpByMonth);
            this.panel1.Controls.Add(this.cboCycle);
            this.panel1.Controls.Add(this.cboArea);
            this.panel1.Controls.Add(this.cboOperator);
            this.panel1.Controls.Add(this.txtValue1);
            this.panel1.Controls.Add(this.txtValue2);
            this.panel1.Controls.Add(this.btnSETUP);
            this.panel1.Controls.Add(this.btnMAIL);
            this.panel1.Controls.Add(this.btnVIEW);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // cboReport
            // 
            this.cboReport.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboReport.DropDownWidth = 200;
            this.cboReport.FormattingEnabled = true;
            resources.ApplyResources(this.cboReport, "cboReport");
            this.cboReport.Name = "cboReport";
            // 
            // lblUSAGE_IN_MONTH
            // 
            resources.ApplyResources(this.lblUSAGE_IN_MONTH, "lblUSAGE_IN_MONTH");
            this.lblUSAGE_IN_MONTH.Name = "lblUSAGE_IN_MONTH";
            // 
            // dtpByMonth
            // 
            resources.ApplyResources(this.dtpByMonth, "dtpByMonth");
            this.dtpByMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpByMonth.Name = "dtpByMonth";
            this.dtpByMonth.ValueChanged += new System.EventHandler(this.dtpByMonth_ValueChanged);
            // 
            // cboCycle
            // 
            this.cboCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCycle.DropDownWidth = 250;
            this.cboCycle.FormattingEnabled = true;
            this.cboCycle.Items.AddRange(new object[] {
            resources.GetString("cboCycle.Items"),
            resources.GetString("cboCycle.Items1"),
            resources.GetString("cboCycle.Items2"),
            resources.GetString("cboCycle.Items3"),
            resources.GetString("cboCycle.Items4"),
            resources.GetString("cboCycle.Items5"),
            resources.GetString("cboCycle.Items6")});
            resources.ApplyResources(this.cboCycle, "cboCycle");
            this.cboCycle.Name = "cboCycle";
            // 
            // cboArea
            // 
            this.cboArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboArea.DropDownWidth = 250;
            this.cboArea.FormattingEnabled = true;
            this.cboArea.Items.AddRange(new object[] {
            resources.GetString("cboArea.Items"),
            resources.GetString("cboArea.Items1"),
            resources.GetString("cboArea.Items2"),
            resources.GetString("cboArea.Items3"),
            resources.GetString("cboArea.Items4"),
            resources.GetString("cboArea.Items5"),
            resources.GetString("cboArea.Items6")});
            resources.ApplyResources(this.cboArea, "cboArea");
            this.cboArea.Name = "cboArea";
            // 
            // cboOperator
            // 
            this.cboOperator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboOperator.FormattingEnabled = true;
            this.cboOperator.Items.AddRange(new object[] {
            resources.GetString("cboOperator.Items"),
            resources.GetString("cboOperator.Items1"),
            resources.GetString("cboOperator.Items2"),
            resources.GetString("cboOperator.Items3"),
            resources.GetString("cboOperator.Items4"),
            resources.GetString("cboOperator.Items5"),
            resources.GetString("cboOperator.Items6")});
            resources.ApplyResources(this.cboOperator, "cboOperator");
            this.cboOperator.Name = "cboOperator";
            this.cboOperator.SelectedIndexChanged += new System.EventHandler(this.cboOperator_SelectedIndexChanged);
            // 
            // txtValue1
            // 
            resources.ApplyResources(this.txtValue1, "txtValue1");
            this.txtValue1.Name = "txtValue1";
            this.txtValue1.Enter += new System.EventHandler(this.txtValue1_Enter);
            // 
            // txtValue2
            // 
            resources.ApplyResources(this.txtValue2, "txtValue2");
            this.txtValue2.Name = "txtValue2";
            this.txtValue2.Enter += new System.EventHandler(this.txtValue1_Enter);
            // 
            // btnSETUP
            // 
            resources.ApplyResources(this.btnSETUP, "btnSETUP");
            this.btnSETUP.Name = "btnSETUP";
            this.btnSETUP.UseVisualStyleBackColor = true;
            this.btnSETUP.Click += new System.EventHandler(this.btnOption_Click);
            // 
            // btnMAIL
            // 
            resources.ApplyResources(this.btnMAIL, "btnMAIL");
            this.btnMAIL.Name = "btnMAIL";
            this.btnMAIL.UseVisualStyleBackColor = true;
            this.btnMAIL.Click += new System.EventHandler(this.btnMail_Click);
            // 
            // btnVIEW
            // 
            resources.ApplyResources(this.btnVIEW, "btnVIEW");
            this.btnVIEW.Name = "btnVIEW";
            this.btnVIEW.UseVisualStyleBackColor = true;
            this.btnVIEW.Click += new System.EventHandler(this.btnViewReport_Click);
            // 
            // viewer
            // 
            this.viewer.ActiveViewIndex = -1;
            this.viewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.viewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.viewer.DisplayStatusBar = false;
            resources.ApplyResources(this.viewer, "viewer");
            this.viewer.Name = "viewer";
            // 
            // PageReportUsageMonthly
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.viewer);
            this.Controls.Add(this.panel1);
            this.Name = "PageReportUsageMonthly";
            this.VisibleChanged += new System.EventHandler(this.PageReportUsageMonthly_VisibleChanged);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnVIEW;
        private ExButton btnMAIL;
        private DateTimePicker dtpByMonth;
        public ComboBox cboCycle;
        public ComboBox cboArea;
        public ComboBox cboOperator;
        private TextBox txtValue1;
        private TextBox txtValue2;
        private ExButton btnSETUP;
        private Label lblUSAGE_IN_MONTH;
        public ComboBox cboReport;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer viewer;
    }
}
