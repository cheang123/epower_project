﻿namespace EPower.Interface.Report
{
    partial class DialogReportCustomerOption
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogReportCustomerOption));
            this.cboArea = new System.Windows.Forms.ComboBox();
            this.cboCustomerGroup = new System.Windows.Forms.ComboBox();
            this.cboCustomerConnectionType = new System.Windows.Forms.ComboBox();
            this.cboBillingCycle = new System.Windows.Forms.ComboBox();
            this.cboPrice = new System.Windows.Forms.ComboBox();
            this.cboCustomerStatus = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClose = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.lblAREA = new System.Windows.Forms.Label();
            this.lblCUSTOMER_GROUP = new System.Windows.Forms.Label();
            this.lblCUSTOMER_CONNECTION_TYPE = new System.Windows.Forms.Label();
            this.lblBILLING_CYCLE = new System.Windows.Forms.Label();
            this.lblPRICE = new System.Windows.Forms.Label();
            this.lblCUSTOMER_STATUS = new System.Windows.Forms.Label();
            this.chkIsmarketVendor = new System.Windows.Forms.CheckBox();
            this.lblAmpare = new System.Windows.Forms.Label();
            this.cboAmpare = new System.Windows.Forms.ComboBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.lblAmpare);
            this.content.Controls.Add(this.cboAmpare);
            this.content.Controls.Add(this.chkIsmarketVendor);
            this.content.Controls.Add(this.lblCUSTOMER_STATUS);
            this.content.Controls.Add(this.lblPRICE);
            this.content.Controls.Add(this.lblBILLING_CYCLE);
            this.content.Controls.Add(this.lblCUSTOMER_CONNECTION_TYPE);
            this.content.Controls.Add(this.lblCUSTOMER_GROUP);
            this.content.Controls.Add(this.lblAREA);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnClose);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.cboCustomerStatus);
            this.content.Controls.Add(this.cboPrice);
            this.content.Controls.Add(this.cboBillingCycle);
            this.content.Controls.Add(this.cboCustomerConnectionType);
            this.content.Controls.Add(this.cboCustomerGroup);
            this.content.Controls.Add(this.cboArea);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.cboArea, 0);
            this.content.Controls.SetChildIndex(this.cboCustomerGroup, 0);
            this.content.Controls.SetChildIndex(this.cboCustomerConnectionType, 0);
            this.content.Controls.SetChildIndex(this.cboBillingCycle, 0);
            this.content.Controls.SetChildIndex(this.cboPrice, 0);
            this.content.Controls.SetChildIndex(this.cboCustomerStatus, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnClose, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.lblAREA, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_GROUP, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_CONNECTION_TYPE, 0);
            this.content.Controls.SetChildIndex(this.lblBILLING_CYCLE, 0);
            this.content.Controls.SetChildIndex(this.lblPRICE, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_STATUS, 0);
            this.content.Controls.SetChildIndex(this.chkIsmarketVendor, 0);
            this.content.Controls.SetChildIndex(this.cboAmpare, 0);
            this.content.Controls.SetChildIndex(this.lblAmpare, 0);
            // 
            // cboArea
            // 
            this.cboArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboArea.FormattingEnabled = true;
            resources.ApplyResources(this.cboArea, "cboArea");
            this.cboArea.Name = "cboArea";
            // 
            // cboCustomerGroup
            // 
            this.cboCustomerGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomerGroup.FormattingEnabled = true;
            resources.ApplyResources(this.cboCustomerGroup, "cboCustomerGroup");
            this.cboCustomerGroup.Name = "cboCustomerGroup";
            this.cboCustomerGroup.SelectedIndexChanged += new System.EventHandler(this.cboCUSTOMER_GROUP_SelectedIndexChanged);
            // 
            // cboCustomerConnectionType
            // 
            this.cboCustomerConnectionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomerConnectionType.FormattingEnabled = true;
            resources.ApplyResources(this.cboCustomerConnectionType, "cboCustomerConnectionType");
            this.cboCustomerConnectionType.Name = "cboCustomerConnectionType";
            // 
            // cboBillingCycle
            // 
            this.cboBillingCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBillingCycle.FormattingEnabled = true;
            resources.ApplyResources(this.cboBillingCycle, "cboBillingCycle");
            this.cboBillingCycle.Name = "cboBillingCycle";
            // 
            // cboPrice
            // 
            this.cboPrice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPrice.FormattingEnabled = true;
            resources.ApplyResources(this.cboPrice, "cboPrice");
            this.cboPrice.Name = "cboPrice";
            // 
            // cboCustomerStatus
            // 
            this.cboCustomerStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomerStatus.FormattingEnabled = true;
            resources.ApplyResources(this.cboCustomerStatus, "cboCustomerStatus");
            this.cboCustomerStatus.Name = "cboCustomerStatus";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnClose
            // 
            resources.ApplyResources(this.btnClose, "btnClose");
            this.btnClose.Name = "btnClose";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblAREA
            // 
            resources.ApplyResources(this.lblAREA, "lblAREA");
            this.lblAREA.Name = "lblAREA";
            // 
            // lblCUSTOMER_GROUP
            // 
            resources.ApplyResources(this.lblCUSTOMER_GROUP, "lblCUSTOMER_GROUP");
            this.lblCUSTOMER_GROUP.Name = "lblCUSTOMER_GROUP";
            // 
            // lblCUSTOMER_CONNECTION_TYPE
            // 
            resources.ApplyResources(this.lblCUSTOMER_CONNECTION_TYPE, "lblCUSTOMER_CONNECTION_TYPE");
            this.lblCUSTOMER_CONNECTION_TYPE.Name = "lblCUSTOMER_CONNECTION_TYPE";
            // 
            // lblBILLING_CYCLE
            // 
            resources.ApplyResources(this.lblBILLING_CYCLE, "lblBILLING_CYCLE");
            this.lblBILLING_CYCLE.Name = "lblBILLING_CYCLE";
            // 
            // lblPRICE
            // 
            resources.ApplyResources(this.lblPRICE, "lblPRICE");
            this.lblPRICE.Name = "lblPRICE";
            // 
            // lblCUSTOMER_STATUS
            // 
            resources.ApplyResources(this.lblCUSTOMER_STATUS, "lblCUSTOMER_STATUS");
            this.lblCUSTOMER_STATUS.Name = "lblCUSTOMER_STATUS";
            // 
            // chkIsmarketVendor
            // 
            resources.ApplyResources(this.chkIsmarketVendor, "chkIsmarketVendor");
            this.chkIsmarketVendor.Name = "chkIsmarketVendor";
            this.chkIsmarketVendor.UseVisualStyleBackColor = true;
            // 
            // lblAmpare
            // 
            resources.ApplyResources(this.lblAmpare, "lblAmpare");
            this.lblAmpare.Name = "lblAmpare";
            // 
            // cboAmpare
            // 
            this.cboAmpare.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAmpare.FormattingEnabled = true;
            resources.ApplyResources(this.cboAmpare, "cboAmpare");
            this.cboAmpare.Name = "cboAmpare";
            // 
            // DialogReportCustomerOption
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogReportCustomerOption";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblPRICE;
        private System.Windows.Forms.Label lblBILLING_CYCLE;
        private System.Windows.Forms.Label lblCUSTOMER_CONNECTION_TYPE;
        private System.Windows.Forms.Label lblCUSTOMER_GROUP;
        private System.Windows.Forms.Label lblAREA;
        public System.Windows.Forms.ComboBox cboCustomerConnectionType;
        public System.Windows.Forms.ComboBox cboCustomerGroup;
        public System.Windows.Forms.ComboBox cboArea;
        public System.Windows.Forms.ComboBox cboCustomerStatus;
        public System.Windows.Forms.ComboBox cboPrice;
        public System.Windows.Forms.ComboBox cboBillingCycle;
        public System.Windows.Forms.Label lblCUSTOMER_STATUS;
        public System.Windows.Forms.CheckBox chkIsmarketVendor;
        public System.Windows.Forms.ComboBox cboAmpare;
        public System.Windows.Forms.Label lblAmpare;
        public System.Windows.Forms.Panel panel1;
        public SoftTech.Component.ExButton btnClose;
        public SoftTech.Component.ExButton btnOK;
    }
}