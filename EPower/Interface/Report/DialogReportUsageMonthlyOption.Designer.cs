﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogReportUsageMonthlyOption
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogReportUsageMonthlyOption));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClose = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.lblMONTH_2 = new System.Windows.Forms.Label();
            this.lblMONTH_1 = new System.Windows.Forms.Label();
            this.lblSEARCH_2 = new System.Windows.Forms.Label();
            this.lblSEARCH_1 = new System.Windows.Forms.Label();
            this.lblSEARCH = new System.Windows.Forms.Label();
            this.lblMONTH = new System.Windows.Forms.Label();
            this.lblCOMPARE = new System.Windows.Forms.Label();
            this.cboFilterType = new System.Windows.Forms.ComboBox();
            this.cboOperatorM3 = new System.Windows.Forms.ComboBox();
            this.cboOperatorM2 = new System.Windows.Forms.ComboBox();
            this.cboOperatorM1 = new System.Windows.Forms.ComboBox();
            this.txtVal2M1 = new System.Windows.Forms.TextBox();
            this.txtVal1M1 = new System.Windows.Forms.TextBox();
            this.txtVal2M2 = new System.Windows.Forms.TextBox();
            this.txtVal1M2 = new System.Windows.Forms.TextBox();
            this.txtVal2M3 = new System.Windows.Forms.TextBox();
            this.txtVal1M3 = new System.Windows.Forms.TextBox();
            this.dtpM1 = new System.Windows.Forms.DateTimePicker();
            this.dtpM2 = new System.Windows.Forms.DateTimePicker();
            this.dtpM3 = new System.Windows.Forms.DateTimePicker();
            this.lblM1FilterType_ = new System.Windows.Forms.Label();
            this.lblM2FilterType_ = new System.Windows.Forms.Label();
            this.lblM3FilterType_ = new System.Windows.Forms.Label();
            this.lblORDER = new System.Windows.Forms.Label();
            this.cboOrderType = new System.Windows.Forms.ComboBox();
            this.lblCUSTOMER_STATUS = new System.Windows.Forms.Label();
            this.cboCustomerStatus = new System.Windows.Forms.ComboBox();
            this.cboCustomerGroup = new System.Windows.Forms.ComboBox();
            this.lblCUSTOMER_GROUP = new System.Windows.Forms.Label();
            this.cboPRICE = new System.Windows.Forms.ComboBox();
            this.lblPRICE = new System.Windows.Forms.Label();
            this.cboConnectionType = new System.Windows.Forms.ComboBox();
            this.lblCUSTOMER_CONNECTION_TYPE = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.cboConnectionType);
            this.content.Controls.Add(this.lblCUSTOMER_CONNECTION_TYPE);
            this.content.Controls.Add(this.cboPRICE);
            this.content.Controls.Add(this.lblPRICE);
            this.content.Controls.Add(this.cboCustomerGroup);
            this.content.Controls.Add(this.lblCUSTOMER_GROUP);
            this.content.Controls.Add(this.cboCustomerStatus);
            this.content.Controls.Add(this.lblCUSTOMER_STATUS);
            this.content.Controls.Add(this.dtpM3);
            this.content.Controls.Add(this.dtpM2);
            this.content.Controls.Add(this.dtpM1);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnClose);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.lblM3FilterType_);
            this.content.Controls.Add(this.lblM2FilterType_);
            this.content.Controls.Add(this.lblM1FilterType_);
            this.content.Controls.Add(this.cboOrderType);
            this.content.Controls.Add(this.cboFilterType);
            this.content.Controls.Add(this.lblMONTH_2);
            this.content.Controls.Add(this.txtVal1M3);
            this.content.Controls.Add(this.txtVal1M2);
            this.content.Controls.Add(this.txtVal1M1);
            this.content.Controls.Add(this.lblMONTH_1);
            this.content.Controls.Add(this.lblSEARCH_2);
            this.content.Controls.Add(this.lblSEARCH_1);
            this.content.Controls.Add(this.txtVal2M3);
            this.content.Controls.Add(this.txtVal2M2);
            this.content.Controls.Add(this.txtVal2M1);
            this.content.Controls.Add(this.lblSEARCH);
            this.content.Controls.Add(this.lblORDER);
            this.content.Controls.Add(this.lblMONTH);
            this.content.Controls.Add(this.lblCOMPARE);
            this.content.Controls.Add(this.cboOperatorM1);
            this.content.Controls.Add(this.cboOperatorM2);
            this.content.Controls.Add(this.cboOperatorM3);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.cboOperatorM3, 0);
            this.content.Controls.SetChildIndex(this.cboOperatorM2, 0);
            this.content.Controls.SetChildIndex(this.cboOperatorM1, 0);
            this.content.Controls.SetChildIndex(this.lblCOMPARE, 0);
            this.content.Controls.SetChildIndex(this.lblMONTH, 0);
            this.content.Controls.SetChildIndex(this.lblORDER, 0);
            this.content.Controls.SetChildIndex(this.lblSEARCH, 0);
            this.content.Controls.SetChildIndex(this.txtVal2M1, 0);
            this.content.Controls.SetChildIndex(this.txtVal2M2, 0);
            this.content.Controls.SetChildIndex(this.txtVal2M3, 0);
            this.content.Controls.SetChildIndex(this.lblSEARCH_1, 0);
            this.content.Controls.SetChildIndex(this.lblSEARCH_2, 0);
            this.content.Controls.SetChildIndex(this.lblMONTH_1, 0);
            this.content.Controls.SetChildIndex(this.txtVal1M1, 0);
            this.content.Controls.SetChildIndex(this.txtVal1M2, 0);
            this.content.Controls.SetChildIndex(this.txtVal1M3, 0);
            this.content.Controls.SetChildIndex(this.lblMONTH_2, 0);
            this.content.Controls.SetChildIndex(this.cboFilterType, 0);
            this.content.Controls.SetChildIndex(this.cboOrderType, 0);
            this.content.Controls.SetChildIndex(this.lblM1FilterType_, 0);
            this.content.Controls.SetChildIndex(this.lblM2FilterType_, 0);
            this.content.Controls.SetChildIndex(this.lblM3FilterType_, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnClose, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.dtpM1, 0);
            this.content.Controls.SetChildIndex(this.dtpM2, 0);
            this.content.Controls.SetChildIndex(this.dtpM3, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_STATUS, 0);
            this.content.Controls.SetChildIndex(this.cboCustomerStatus, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_GROUP, 0);
            this.content.Controls.SetChildIndex(this.cboCustomerGroup, 0);
            this.content.Controls.SetChildIndex(this.lblPRICE, 0);
            this.content.Controls.SetChildIndex(this.cboPRICE, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_CONNECTION_TYPE, 0);
            this.content.Controls.SetChildIndex(this.cboConnectionType, 0);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnClose
            // 
            resources.ApplyResources(this.btnClose, "btnClose");
            this.btnClose.Name = "btnClose";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblMONTH_2
            // 
            resources.ApplyResources(this.lblMONTH_2, "lblMONTH_2");
            this.lblMONTH_2.Name = "lblMONTH_2";
            // 
            // lblMONTH_1
            // 
            resources.ApplyResources(this.lblMONTH_1, "lblMONTH_1");
            this.lblMONTH_1.Name = "lblMONTH_1";
            // 
            // lblSEARCH_2
            // 
            resources.ApplyResources(this.lblSEARCH_2, "lblSEARCH_2");
            this.lblSEARCH_2.Name = "lblSEARCH_2";
            // 
            // lblSEARCH_1
            // 
            resources.ApplyResources(this.lblSEARCH_1, "lblSEARCH_1");
            this.lblSEARCH_1.Name = "lblSEARCH_1";
            // 
            // lblSEARCH
            // 
            resources.ApplyResources(this.lblSEARCH, "lblSEARCH");
            this.lblSEARCH.Name = "lblSEARCH";
            // 
            // lblMONTH
            // 
            resources.ApplyResources(this.lblMONTH, "lblMONTH");
            this.lblMONTH.Name = "lblMONTH";
            // 
            // lblCOMPARE
            // 
            resources.ApplyResources(this.lblCOMPARE, "lblCOMPARE");
            this.lblCOMPARE.Name = "lblCOMPARE";
            // 
            // cboFilterType
            // 
            this.cboFilterType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFilterType.FormattingEnabled = true;
            resources.ApplyResources(this.cboFilterType, "cboFilterType");
            this.cboFilterType.Name = "cboFilterType";
            this.cboFilterType.SelectedIndexChanged += new System.EventHandler(this.cboFilterType_SelectedIndexChanged);
            // 
            // cboOperatorM3
            // 
            this.cboOperatorM3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboOperatorM3.FormattingEnabled = true;
            this.cboOperatorM3.Items.AddRange(new object[] {
            resources.GetString("cboOperatorM3.Items"),
            resources.GetString("cboOperatorM3.Items1"),
            resources.GetString("cboOperatorM3.Items2"),
            resources.GetString("cboOperatorM3.Items3"),
            resources.GetString("cboOperatorM3.Items4"),
            resources.GetString("cboOperatorM3.Items5"),
            resources.GetString("cboOperatorM3.Items6"),
            resources.GetString("cboOperatorM3.Items7"),
            resources.GetString("cboOperatorM3.Items8")});
            resources.ApplyResources(this.cboOperatorM3, "cboOperatorM3");
            this.cboOperatorM3.Name = "cboOperatorM3";
            this.cboOperatorM3.SelectedIndexChanged += new System.EventHandler(this.cboOperatorM3_SelectedIndexChanged);
            // 
            // cboOperatorM2
            // 
            this.cboOperatorM2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboOperatorM2.FormattingEnabled = true;
            this.cboOperatorM2.Items.AddRange(new object[] {
            resources.GetString("cboOperatorM2.Items"),
            resources.GetString("cboOperatorM2.Items1"),
            resources.GetString("cboOperatorM2.Items2"),
            resources.GetString("cboOperatorM2.Items3"),
            resources.GetString("cboOperatorM2.Items4"),
            resources.GetString("cboOperatorM2.Items5"),
            resources.GetString("cboOperatorM2.Items6"),
            resources.GetString("cboOperatorM2.Items7"),
            resources.GetString("cboOperatorM2.Items8")});
            resources.ApplyResources(this.cboOperatorM2, "cboOperatorM2");
            this.cboOperatorM2.Name = "cboOperatorM2";
            this.cboOperatorM2.SelectedIndexChanged += new System.EventHandler(this.cboOperatorM2_SelectedIndexChanged);
            // 
            // cboOperatorM1
            // 
            this.cboOperatorM1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboOperatorM1.FormattingEnabled = true;
            this.cboOperatorM1.Items.AddRange(new object[] {
            resources.GetString("cboOperatorM1.Items"),
            resources.GetString("cboOperatorM1.Items1"),
            resources.GetString("cboOperatorM1.Items2"),
            resources.GetString("cboOperatorM1.Items3"),
            resources.GetString("cboOperatorM1.Items4"),
            resources.GetString("cboOperatorM1.Items5"),
            resources.GetString("cboOperatorM1.Items6"),
            resources.GetString("cboOperatorM1.Items7"),
            resources.GetString("cboOperatorM1.Items8")});
            resources.ApplyResources(this.cboOperatorM1, "cboOperatorM1");
            this.cboOperatorM1.Name = "cboOperatorM1";
            this.cboOperatorM1.SelectedIndexChanged += new System.EventHandler(this.cboOperatorM1_SelectedIndexChanged);
            // 
            // txtVal2M1
            // 
            resources.ApplyResources(this.txtVal2M1, "txtVal2M1");
            this.txtVal2M1.Name = "txtVal2M1";
            // 
            // txtVal1M1
            // 
            resources.ApplyResources(this.txtVal1M1, "txtVal1M1");
            this.txtVal1M1.Name = "txtVal1M1";
            // 
            // txtVal2M2
            // 
            resources.ApplyResources(this.txtVal2M2, "txtVal2M2");
            this.txtVal2M2.Name = "txtVal2M2";
            // 
            // txtVal1M2
            // 
            resources.ApplyResources(this.txtVal1M2, "txtVal1M2");
            this.txtVal1M2.Name = "txtVal1M2";
            // 
            // txtVal2M3
            // 
            resources.ApplyResources(this.txtVal2M3, "txtVal2M3");
            this.txtVal2M3.Name = "txtVal2M3";
            // 
            // txtVal1M3
            // 
            resources.ApplyResources(this.txtVal1M3, "txtVal1M3");
            this.txtVal1M3.Name = "txtVal1M3";
            // 
            // dtpM1
            // 
            resources.ApplyResources(this.dtpM1, "dtpM1");
            this.dtpM1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpM1.Name = "dtpM1";
            // 
            // dtpM2
            // 
            resources.ApplyResources(this.dtpM2, "dtpM2");
            this.dtpM2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpM2.Name = "dtpM2";
            // 
            // dtpM3
            // 
            resources.ApplyResources(this.dtpM3, "dtpM3");
            this.dtpM3.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpM3.Name = "dtpM3";
            // 
            // lblM1FilterType_
            // 
            resources.ApplyResources(this.lblM1FilterType_, "lblM1FilterType_");
            this.lblM1FilterType_.Name = "lblM1FilterType_";
            // 
            // lblM2FilterType_
            // 
            resources.ApplyResources(this.lblM2FilterType_, "lblM2FilterType_");
            this.lblM2FilterType_.Name = "lblM2FilterType_";
            // 
            // lblM3FilterType_
            // 
            resources.ApplyResources(this.lblM3FilterType_, "lblM3FilterType_");
            this.lblM3FilterType_.Name = "lblM3FilterType_";
            // 
            // lblORDER
            // 
            resources.ApplyResources(this.lblORDER, "lblORDER");
            this.lblORDER.Name = "lblORDER";
            // 
            // cboOrderType
            // 
            this.cboOrderType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboOrderType.FormattingEnabled = true;
            resources.ApplyResources(this.cboOrderType, "cboOrderType");
            this.cboOrderType.Name = "cboOrderType";
            this.cboOrderType.SelectedIndexChanged += new System.EventHandler(this.cboFilterType_SelectedIndexChanged);
            // 
            // lblCUSTOMER_STATUS
            // 
            resources.ApplyResources(this.lblCUSTOMER_STATUS, "lblCUSTOMER_STATUS");
            this.lblCUSTOMER_STATUS.Name = "lblCUSTOMER_STATUS";
            // 
            // cboCustomerStatus
            // 
            this.cboCustomerStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomerStatus.FormattingEnabled = true;
            resources.ApplyResources(this.cboCustomerStatus, "cboCustomerStatus");
            this.cboCustomerStatus.Name = "cboCustomerStatus";
            // 
            // cboCustomerGroup
            // 
            this.cboCustomerGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomerGroup.DropDownWidth = 200;
            this.cboCustomerGroup.FormattingEnabled = true;
            resources.ApplyResources(this.cboCustomerGroup, "cboCustomerGroup");
            this.cboCustomerGroup.Name = "cboCustomerGroup";
            this.cboCustomerGroup.SelectedIndexChanged += new System.EventHandler(this.cboCustomerGroup_SelectedIndexChanged);
            // 
            // lblCUSTOMER_GROUP
            // 
            resources.ApplyResources(this.lblCUSTOMER_GROUP, "lblCUSTOMER_GROUP");
            this.lblCUSTOMER_GROUP.Name = "lblCUSTOMER_GROUP";
            // 
            // cboPRICE
            // 
            this.cboPRICE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPRICE.DropDownWidth = 200;
            this.cboPRICE.FormattingEnabled = true;
            resources.ApplyResources(this.cboPRICE, "cboPRICE");
            this.cboPRICE.Name = "cboPRICE";
            // 
            // lblPRICE
            // 
            resources.ApplyResources(this.lblPRICE, "lblPRICE");
            this.lblPRICE.Name = "lblPRICE";
            // 
            // cboConnectionType
            // 
            this.cboConnectionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboConnectionType.DropDownWidth = 200;
            this.cboConnectionType.FormattingEnabled = true;
            resources.ApplyResources(this.cboConnectionType, "cboConnectionType");
            this.cboConnectionType.Name = "cboConnectionType";
            // 
            // lblCUSTOMER_CONNECTION_TYPE
            // 
            resources.ApplyResources(this.lblCUSTOMER_CONNECTION_TYPE, "lblCUSTOMER_CONNECTION_TYPE");
            this.lblCUSTOMER_CONNECTION_TYPE.Name = "lblCUSTOMER_CONNECTION_TYPE";
            // 
            // DialogReportUsageMonthlyOption
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogReportUsageMonthlyOption";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnClose;
        private ExButton btnOK;
        public ComboBox cboFilterType;
        private Label lblMONTH_2;
        private Label lblMONTH_1;
        private Label lblSEARCH_2;
        private Label lblSEARCH_1;
        private Label lblSEARCH;
        private Label lblMONTH;
        private Label lblCOMPARE;
        public ComboBox cboOperatorM1;
        public ComboBox cboOperatorM2;
        public ComboBox cboOperatorM3;
        public TextBox txtVal1M3;
        public TextBox txtVal1M2;
        public TextBox txtVal1M1;
        public TextBox txtVal2M3;
        public TextBox txtVal2M2;
        public TextBox txtVal2M1;
        public DateTimePicker dtpM3;
        public DateTimePicker dtpM2;
        public DateTimePicker dtpM1;
        private Label lblM3FilterType_;
        private Label lblM2FilterType_;
        private Label lblM1FilterType_;
        public ComboBox cboOrderType;
        private Label lblORDER;
        public ComboBox cboCustomerStatus;
        public ComboBox cboCustomerGroup;
        public Label lblCUSTOMER_STATUS;
        public Label lblCUSTOMER_GROUP;
        public ComboBox cboPRICE;
        public Label lblPRICE;
        public ComboBox cboConnectionType;
        public Label lblCUSTOMER_CONNECTION_TYPE;
    }
}