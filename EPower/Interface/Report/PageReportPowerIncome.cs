﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageReportPowerIncome : Form
    {
        CrystalReportHelper ch = null;

        enum ReportName
        {
            PowerIncomeMonthly,
            PowerIncomeMonthlyByInvoice,
            PowerIncomeByArea,
            PowerIncomeByAmpare,
            PowerIncomeSummary,
            PowerIncomeYearly,
            CustomersNoBill,
        }

        DialogReportPowerIncomeOption diagOption;

        public PageReportPowerIncome()
        {
            InitializeComponent();
            viewer.DefaultView();
            bind();
             
            this.cboReport.SelectedIndex = 0;
            TBL_BILLING_CYCLE objBillingCycle = DBDataContext.Db.TBL_BILLING_CYCLEs.FirstOrDefault(row => row.IS_ACTIVE);
            if (objBillingCycle != null)
            {
                DateTime datCurrentMonth = Method.GetNextBillingMonth(objBillingCycle.CYCLE_ID).AddMonths(-1);
                this.dtDate.Value = datCurrentMonth;
            }

        }

        private void bind()
        {
            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("REPORT_ID", typeof(int));
            dtReport.Columns.Add("REPORT_NAME", typeof(string));
            dtReport.Rows.Add(1, Resources.REPORT_POWER_INCOME_MONTHLY_DETAIL);
            dtReport.Rows.Add(2, Resources.REPORT_POWER_INVOICE_DETAIL);
            dtReport.Rows.Add(3, Resources.REPORT_POWER_INCOME_MONTHLY_BY_AREA);
            dtReport.Rows.Add(4, Resources.REPORT_POWER_INCOME_MONTHLY_BY_AMPARE);
            dtReport.Rows.Add(5, Resources.REPORT_POWER_INCOME_MONTHLY_SUMMARY);
            dtReport.Rows.Add(6, Resources.REPORT_POWER_INCOME_YEARLY);
            dtReport.Rows.Add(7, Resources.REPORT_CUSTOMERS_NO_BILL);
            UIHelper.SetDataSourceToComboBox(cboReport, dtReport, "REPORT_ID", "REPORT_NAME");
        }
        private void btnViewReport_Click(object sender, EventArgs e)
        {
            Runner.Run(this.viewReport);
        } 
        private void viewReport()
        {
            try
            {
                if (this.cboReport.SelectedIndex == (int)ReportName.PowerIncomeMonthly)
                {
                    this.ch = new CrystalReportHelper("ReportPowerIncomeMonthly.rpt");
                }
                if (this.cboReport.SelectedIndex == (int)ReportName.PowerIncomeMonthlyByInvoice)
                {
                    this.ch = new CrystalReportHelper("ReportPowerInvoiceDetail.rpt");
                }
                else if (this.cboReport.SelectedIndex == (int)ReportName.PowerIncomeByArea)
                {
                    this.ch = new CrystalReportHelper("ReportPowerIncomeByArea.rpt");
                }
                else if (this.cboReport.SelectedIndex == (int)ReportName.PowerIncomeSummary)
                {
                    this.ch = new CrystalReportHelper("ReportPowerIncomeSummary.rpt");
                }
                else if (this.cboReport.SelectedIndex == (int)ReportName.PowerIncomeYearly)
                {
                    this.ch = new CrystalReportHelper("ReportPowerIncomeYearly.rpt");
                }
                else if (this.cboReport.SelectedIndex == (int)ReportName.PowerIncomeByAmpare)
                {
                    this.ch = new CrystalReportHelper("ReportPowerIncomeByAmpare.rpt");
                }
                else if (this.cboReport.SelectedIndex == (int)ReportName.CustomersNoBill)
                {
                    this.ch = new CrystalReportHelper("ReportCustomersNoBill.rpt");
                }

                if (cboReport.SelectedIndex == (int)ReportName.PowerIncomeYearly)
                {
                    this.ch.SetParameter("@YEAR", this.dtDate.Value.Year);
                }
                else
                {
                    this.ch.SetParameter("@MONTH", this.dtDate.Value);
                }
                this.ch.SetParameter("@AREA_ID", (int)diagOption.cboArea.SelectedValue);
                //this.ch.SetParameter("@CUSTOMER_TYPE_ID", (int)diagOption.cboCustomerType.SelectedValue);
                this.ch.SetParameter("@CYCLE_ID", (int)diagOption.cboBillingCycle.SelectedValue);
                this.ch.SetParameter("@PRICE_ID", (int)diagOption.cboPrice.SelectedValue);
                this.ch.SetParameter("@CURRENCY_ID", (int)diagOption.cboCurrency.SelectedValue);
                this.ch.SetParameter("@AREA_NAME", diagOption.cboArea.Text);
                //this.ch.SetParameter("@CUSTOMER_TYPE_NAME", diagOption.cboCustomerType.Text);
                this.ch.SetParameter("@CYCLE_NAME", diagOption.cboBillingCycle.Text);
                this.ch.SetParameter("@PRICE_NAME", diagOption.cboPrice.Text);
                this.ch.SetParameter("@CURRENCY_NAME", diagOption.cboCurrency.Text);
                this.ch.SetParameter("@STATUS_ID", diagOption.cboSTATUS.SelectedValue);
                this.ch.SetParameter("@STATUS_NAME", diagOption.cboSTATUS.Text);
                //this.ch.SetParameter("@CUSTOMER_GROUP_ID", (int)diagOption.cboCustomerGroup.SelectedValue);
                this.ch.SetParameter("@CUSTOMER_GROUP_NAME", diagOption.cboCustomerGroup.Text);
                this.ch.SetParameter("@CUSTOMER_CONNECTION_TYPE_ID", (int)diagOption.cboCustomerConnectionType.SelectedValue);
                this.ch.SetParameter("@CUSTOMER_CONNECTION_TYPE_NAME", diagOption.cboCustomerConnectionType.Text);

                //Thunny
                this.ch.SetParameter("@CUSTOMER_GROUP_TYPE_ID", diagOption.cboCustomerGroup.SelectedValue);
                this.viewer.ReportSource = ch.Report;

            }
            catch(Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_VIEW_REPORT, Base.Properties.Resources.WARNING);
            }
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            this.sendMail();
        }

        private void sendMail()
        {
            try
            {
                this.viewReport();
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + this.ch.ReportName + ".xls").FullName;
                this.ch.ExportToExcel(exportPath);
                DialogSendMail.Instance.Add(this.ch.ReportName, exportPath);
                DialogSendMail.Instance.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_SEND_EMAIL, Base.Properties.Resources.WARNING);
            }
        }

        private void reportPeriod_PeriodChanged(object sender, EventArgs e)
        {
            //this.cboArea.Visible =
            //    this.cboCustomerType.Visible = this.reportPeriod.Type == PanelReportPeriod.ReportType.ForMonth;
        }

        private void cboReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.dtDate.CustomFormat = cboReport.SelectedIndex == (int)ReportName.PowerIncomeYearly ? "yyyy" : "MM-yyyy"; 
        }

        private void PageReportPowerIncome_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                diagOption = new DialogReportPowerIncomeOption();
                diagOption.Bind();
            }
        }  
        private void cboSaveState_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            if (cbo.SelectedIndex==-1)
            {
                return;
            }
            cbo.Tag = cbo.SelectedValue;
        }
        
        private void btnSetup_Click(object sender, EventArgs e)
        {
            if (diagOption.ShowDialog() == DialogResult.OK)
            {
                Runner.Run(this.viewReport);
            }
        }

    }
}
