﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageReportTrialBalanceCustomerDetail : Form
    {
        CrystalReportHelper ch = null;


        public PageReportTrialBalanceCustomerDetail()
        {
            InitializeComponent();
            viewer.DefaultView();

            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("REPORT_ID", typeof(int));
            dtReport.Columns.Add("REPORT_NAME", typeof(string));
            dtReport.Rows.Add(0, Resources.REPORT_TRIAL_BALANCE_CUSTOMER_DETAIL);
            dtReport.Rows.Add(1, Resources.REPORT_DEPOSIT_AND_CONNECTIN_FEE);
            dtReport.Rows.Add(2, Resources.REPORT_CUSTOMER_CHANGE_AMPARE);
            dtReport.Rows.Add(3, Resources.REPORT_CUSTOMER_CHANGE_BOX);
            dtReport.Rows.Add(4, Resources.REPORT_CUSTOMER_CHANGE_METER);
            dtReport.Rows.Add(5, Resources.REPORT_CHANGE_SERIAL_METER);
            //dtReport.Rows.Add(6, Resources.REPORT_CUSTOMER_CHANGE_INVOICE);
            UIHelper.SetDataSourceToComboBox(cboReport, dtReport, "REPORT_ID", "REPORT_NAME");

            dtpEnd.Value = dtpStart.Value = DBDataContext.Db.GetSystemDate();
            cboReport.SelectedIndex = 0;
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            Runner.Run(this.viewReport);
        }
        public CrystalReportHelper ViewReportTrialBalanceCustomerDetail(DateTime dtStart, DateTime dtEnd, int currencyId, string currencyName,
                                string reportName, int areaId, string areaName, int cycleId, string cycleName, int meterTypeID, string meterTypeName, int cusStatusId, string cusStatusName)
        {
            CrystalReportHelper c = new CrystalReportHelper(reportName);


            c.SetParameter("@D1", dtStart);
            c.SetParameter("@D2", dtEnd);
            c.SetParameter("@CURRENCY_ID", currencyId);
            c.SetParameter("@CURRENCY_NAME", currencyName);
            c.SetParameter("@AREA_ID", areaId);
            c.SetParameter("@AREA_NAME", areaName);
            c.SetParameter("@CYCLE_ID", cycleId);
            c.SetParameter("@CYCLE_NAME", cycleName);
            //c.SetParameter("@CUS_STATUS_ID", cusStatusId);
            //c.SetParameter("@CUS_STATUS_NAME", cusStatusName);

            if (cboReport.SelectedIndex == 4)
            {
                c.SetParameter("@METER_TYPE_ID", meterTypeID);
                c.SetParameter("@METER_TYPE_NAME", meterTypeName);
            }

            return c;
        }
        private void viewReport()
        {
            try
            {
                string report = "";
                if ((int)cboReport.SelectedValue == 0 && chkSHOW_ADDRESS.Checked)
                {
                    report = "ReportTrialBalanceCustomerDetailShowAddress.rpt";
                }
                else if ((int)cboReport.SelectedValue == 0 && !chkSHOW_ADDRESS.Checked)
                {
                    report = "ReportTrialBalanceCustomerDetail.rpt";
                }
                else if ((int)cboReport.SelectedValue == 1 && chkSHOW_ADDRESS.Checked)
                {
                    report = "ReportCustomersDepositConnectionFeeShowAddress.rpt";
                }
                else if ((int)cboReport.SelectedValue == 1 && !chkSHOW_ADDRESS.Checked)
                {
                    report = "ReportCustomersDepositConnectionFee.rpt";
                }
                else if ((int)cboReport.SelectedValue == 2 && chkSHOW_ADDRESS.Checked)
                {
                    report = "ReportCustomersChangeAmpareShowAddress.rpt";
                }
                else if ((int)cboReport.SelectedValue == 2 && !chkSHOW_ADDRESS.Checked)
                {
                    report = "ReportCustomersChangeAmpare.rpt";
                }
                else if ((int)cboReport.SelectedValue == 3 && chkSHOW_ADDRESS.Checked)
                {
                    report = "ReportCustomersChangeBoxShowAddress.rpt";
                }
                else if ((int)cboReport.SelectedValue == 3 && !chkSHOW_ADDRESS.Checked)
                {
                    report = "ReportCustomersChangeBox.rpt";
                }
                else if ((int)cboReport.SelectedValue == 4 && chkSHOW_ADDRESS.Checked)
                {
                    report = "ReportCustomersChangeMeterShowAddress.rpt";
                }
                else if ((int)cboReport.SelectedValue == 4 && !chkSHOW_ADDRESS.Checked)
                {
                    report = "ReportCustomersChangeMeter.rpt";
                }
                else if ((int)cboReport.SelectedValue == 5 && !chkSHOW_ADDRESS.Checked)
                {
                    report = "ReportChangeSerialMeter.rpt";
                }
                else if ((int)cboReport.SelectedValue == 5 && chkSHOW_ADDRESS.Checked)
                {
                    report = "ReportChangeSerialMeterShowAddress.rpt";
                }
                this.ch = ViewReportTrialBalanceCustomerDetail(
                    dtpStart.Value.Date,
                    dtpEnd.Value.Date,
                    (int)diag.cboCurrency.SelectedValue,
                    diag.cboCurrency.Text,
                    report,
                    (int)diag.cboArea.SelectedValue,
                    diag.cboArea.Text,
                    (int)diag.cboCycle.SelectedValue,
                    diag.cboCycle.Text,
                    (int)diag.cboMeterType.SelectedValue,
                    diag.cboMeterType.Text,
                    (int)diag.cboCustomerStatus.SelectedValue,
                    diag.cboCustomerStatus.Text);
                this.viewer.ReportSource = ch.Report;
            }
            catch(Exception ex){
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_VIEW_REPORT, Base.Properties.Resources.WARNING);
            }
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            this.sendMail();
        }

        private void sendMail()
        {
            try
            {
                this.viewReport();
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + this.ch.ReportName + ".xls").FullName;
                this.ch.ExportToExcel(exportPath);
                DialogSendMail.Instance.Add(this.ch.ReportName, exportPath);
                DialogSendMail.Instance.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_SEND_EMAIL, Base.Properties.Resources.WARNING);
            }
        }

        private void cboReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboReport.SelectedIndex != 4)
            {
                diag.cboMeterType.Visible = false;
                diag.lblMETER_TYPE.Visible = false;
            }
            else
            {
                diag.cboMeterType.Visible = true;
                diag.lblMETER_TYPE.Visible = true;
            }
        }
        DialogReportTrialBalanceCustomerDetailOption diag = new DialogReportTrialBalanceCustomerDetailOption();
        private void btnSETUP_Click(object sender, EventArgs e)
        {
            if (diag.ShowDialog() == DialogResult.OK)
            {
                viewReport();
            }
        }
    }
}
