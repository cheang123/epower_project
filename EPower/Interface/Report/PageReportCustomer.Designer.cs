﻿using System.ComponentModel;
using System.Windows.Forms;
using AxCrystalActiveXReportViewerLib105;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageReportCustomer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageReportCustomer));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSetup = new SoftTech.Component.ExButton();
            this.cboReport = new System.Windows.Forms.ComboBox();
            this.chkPOSITION_IN_BOX = new System.Windows.Forms.CheckBox();
            this.dtpByMonth = new System.Windows.Forms.DateTimePicker();
            this.btnMAIL = new SoftTech.Component.ExButton();
            this.btnVIEW = new SoftTech.Component.ExButton();
            this.viewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.btnCSV = new SoftTech.Component.ExButton();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.btnCSV);
            this.panel1.Controls.Add(this.btnSetup);
            this.panel1.Controls.Add(this.cboReport);
            this.panel1.Controls.Add(this.chkPOSITION_IN_BOX);
            this.panel1.Controls.Add(this.dtpByMonth);
            this.panel1.Controls.Add(this.btnMAIL);
            this.panel1.Controls.Add(this.btnVIEW);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnSetup
            // 
            resources.ApplyResources(this.btnSetup, "btnSetup");
            this.btnSetup.Name = "btnSetup";
            this.btnSetup.UseVisualStyleBackColor = true;
            this.btnSetup.Click += new System.EventHandler(this.btnSetup_Click);
            // 
            // cboReport
            // 
            this.cboReport.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboReport.DropDownWidth = 200;
            this.cboReport.FormattingEnabled = true;
            resources.ApplyResources(this.cboReport, "cboReport");
            this.cboReport.Name = "cboReport";
            this.cboReport.SelectedIndexChanged += new System.EventHandler(this.cboReport_SelectedIndexChanged);
            // 
            // chkPOSITION_IN_BOX
            // 
            resources.ApplyResources(this.chkPOSITION_IN_BOX, "chkPOSITION_IN_BOX");
            this.chkPOSITION_IN_BOX.Name = "chkPOSITION_IN_BOX";
            this.chkPOSITION_IN_BOX.UseVisualStyleBackColor = true;
            this.chkPOSITION_IN_BOX.CheckedChanged += new System.EventHandler(this.chkWithPositionInBox_CheckedChanged);
            // 
            // dtpByMonth
            // 
            resources.ApplyResources(this.dtpByMonth, "dtpByMonth");
            this.dtpByMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpByMonth.Name = "dtpByMonth";
            this.dtpByMonth.Enter += new System.EventHandler(this.dtpByMonth_Enter);
            // 
            // btnMAIL
            // 
            resources.ApplyResources(this.btnMAIL, "btnMAIL");
            this.btnMAIL.Name = "btnMAIL";
            this.btnMAIL.UseVisualStyleBackColor = true;
            this.btnMAIL.Click += new System.EventHandler(this.btnMail_Click);
            // 
            // btnVIEW
            // 
            resources.ApplyResources(this.btnVIEW, "btnVIEW");
            this.btnVIEW.Name = "btnVIEW";
            this.btnVIEW.UseVisualStyleBackColor = true;
            this.btnVIEW.Click += new System.EventHandler(this.btnViewReport_Click);
            // 
            // viewer
            // 
            this.viewer.ActiveViewIndex = -1;
            this.viewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.viewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.viewer.DisplayStatusBar = false;
            resources.ApplyResources(this.viewer, "viewer");
            this.viewer.Name = "viewer";
            // 
            // btnCSV
            // 
            resources.ApplyResources(this.btnCSV, "btnCSV");
            this.btnCSV.Name = "btnCSV";
            this.btnCSV.UseVisualStyleBackColor = true;
            this.btnCSV.Click += new System.EventHandler(this.btnCSV_Click);
            // 
            // PageReportCustomer
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.viewer);
            this.Controls.Add(this.panel1);
            this.Name = "PageReportCustomer";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ComboBox reportcboReport;
        private ExButton btnMAIL;
        private ExButton btnVIEW;
        private ExButton btnSetup;
        public ComboBox cboReport;
        private CheckBox chkPOSITION_IN_BOX;
        private DateTimePicker dtpByMonth;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer viewer;
        private ExButton btnCSV;
    }
}
