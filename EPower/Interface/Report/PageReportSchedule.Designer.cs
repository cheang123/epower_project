﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageReportSchedule
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageReportSchedule));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.SCHEDULE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SCHEDULE_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SEND_PERIOD_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.START_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.START_TIME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnRemove = new SoftTech.Component.ExButton();
            this.btnNew = new SoftTech.Component.ExButton();
            this.btnEdit = new SoftTech.Component.ExButton();
            this.txtQuickSearch = new SoftTech.Component.ExTextbox();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SCHEDULE_ID,
            this.SCHEDULE_NAME,
            this.SEND_PERIOD_NAME,
            this.START_DATE,
            this.START_TIME});
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // SCHEDULE_ID
            // 
            this.SCHEDULE_ID.DataPropertyName = "SCHEDULE_ID";
            resources.ApplyResources(this.SCHEDULE_ID, "SCHEDULE_ID");
            this.SCHEDULE_ID.Name = "SCHEDULE_ID";
            this.SCHEDULE_ID.ReadOnly = true;
            // 
            // SCHEDULE_NAME
            // 
            this.SCHEDULE_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.SCHEDULE_NAME.DataPropertyName = "SCHEDULE_NAME";
            resources.ApplyResources(this.SCHEDULE_NAME, "SCHEDULE_NAME");
            this.SCHEDULE_NAME.Name = "SCHEDULE_NAME";
            this.SCHEDULE_NAME.ReadOnly = true;
            // 
            // SEND_PERIOD_NAME
            // 
            this.SEND_PERIOD_NAME.DataPropertyName = "SEND_PERIOD_NAME";
            resources.ApplyResources(this.SEND_PERIOD_NAME, "SEND_PERIOD_NAME");
            this.SEND_PERIOD_NAME.Name = "SEND_PERIOD_NAME";
            this.SEND_PERIOD_NAME.ReadOnly = true;
            // 
            // START_DATE
            // 
            this.START_DATE.DataPropertyName = "START_DATE";
            dataGridViewCellStyle2.Format = "dd-MM-yyyy";
            this.START_DATE.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.START_DATE, "START_DATE");
            this.START_DATE.Name = "START_DATE";
            this.START_DATE.ReadOnly = true;
            // 
            // START_TIME
            // 
            this.START_TIME.DataPropertyName = "START_TIME";
            dataGridViewCellStyle3.Format = "hh : mm : ss tt";
            this.START_TIME.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.START_TIME, "START_TIME");
            this.START_TIME.Name = "START_TIME";
            this.START_TIME.ReadOnly = true;
            // 
            // panel1
            // 
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.btnRemove);
            this.panel1.Controls.Add(this.btnNew);
            this.panel1.Controls.Add(this.btnEdit);
            this.panel1.Controls.Add(this.txtQuickSearch);
            this.panel1.Name = "panel1";
            // 
            // btnRemove
            // 
            resources.ApplyResources(this.btnRemove, "btnRemove");
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnNew
            // 
            resources.ApplyResources(this.btnNew, "btnNew");
            this.btnNew.Name = "btnNew";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnEdit
            // 
            resources.ApplyResources(this.btnEdit, "btnEdit");
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // txtQuickSearch
            // 
            resources.ApplyResources(this.txtQuickSearch, "txtQuickSearch");
            this.txtQuickSearch.BackColor = System.Drawing.Color.White;
            this.txtQuickSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtQuickSearch.Name = "txtQuickSearch";
            this.txtQuickSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtQuickSearch.QuickSearch += new System.EventHandler(this.txt_QuickSearch);
            this.txtQuickSearch.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // PageReportSchedule
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.panel1);
            this.Name = "PageReportSchedule";
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExTextbox txtQuickSearch;
        private ExButton btnNew;
        private ExButton btnEdit;
        private DataGridView dgv;
        private ExButton btnRemove;
        private DataGridViewTextBoxColumn SCHEDULE_ID;
        private DataGridViewTextBoxColumn SCHEDULE_NAME;
        private DataGridViewTextBoxColumn SEND_PERIOD_NAME;
        private DataGridViewTextBoxColumn START_DATE;
        private DataGridViewTextBoxColumn START_TIME;
    }
}
