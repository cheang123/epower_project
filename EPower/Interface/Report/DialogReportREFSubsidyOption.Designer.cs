﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogREFSubsidyOption
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogREFSubsidyOption));
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnClose = new SoftTech.Component.ExButton();
			this.btnOK = new SoftTech.Component.ExButton();
			this.cboCurrency = new System.Windows.Forms.ComboBox();
			this.cboCustomerGroup = new System.Windows.Forms.ComboBox();
			this.lblCURRENCY = new System.Windows.Forms.Label();
			this.lblCUSTOMER_GROUP = new System.Windows.Forms.Label();
			this.cboCustomerConnectionType = new System.Windows.Forms.ComboBox();
			this.lblCONNECTION = new System.Windows.Forms.Label();
			this.cboFilterPower = new System.Windows.Forms.ComboBox();
			this.lblFILTER_POWER = new System.Windows.Forms.Label();
			this.content.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// content
			// 
			this.content.Controls.Add(this.lblFILTER_POWER);
			this.content.Controls.Add(this.cboFilterPower);
			this.content.Controls.Add(this.lblCONNECTION);
			this.content.Controls.Add(this.cboCurrency);
			this.content.Controls.Add(this.cboCustomerConnectionType);
			this.content.Controls.Add(this.cboCustomerGroup);
			this.content.Controls.Add(this.lblCUSTOMER_GROUP);
			this.content.Controls.Add(this.lblCURRENCY);
			this.content.Controls.Add(this.panel1);
			this.content.Controls.Add(this.btnClose);
			this.content.Controls.Add(this.btnOK);
			resources.ApplyResources(this.content, "content");
			this.content.Controls.SetChildIndex(this.btnOK, 0);
			this.content.Controls.SetChildIndex(this.btnClose, 0);
			this.content.Controls.SetChildIndex(this.panel1, 0);
			this.content.Controls.SetChildIndex(this.lblCURRENCY, 0);
			this.content.Controls.SetChildIndex(this.lblCUSTOMER_GROUP, 0);
			this.content.Controls.SetChildIndex(this.cboCustomerGroup, 0);
			this.content.Controls.SetChildIndex(this.cboCustomerConnectionType, 0);
			this.content.Controls.SetChildIndex(this.cboCurrency, 0);
			this.content.Controls.SetChildIndex(this.lblCONNECTION, 0);
			this.content.Controls.SetChildIndex(this.cboFilterPower, 0);
			this.content.Controls.SetChildIndex(this.lblFILTER_POWER, 0);
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.Silver;
			resources.ApplyResources(this.panel1, "panel1");
			this.panel1.Name = "panel1";
			// 
			// btnClose
			// 
			resources.ApplyResources(this.btnClose, "btnClose");
			this.btnClose.Name = "btnClose";
			this.btnClose.UseVisualStyleBackColor = true;
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// btnOK
			// 
			resources.ApplyResources(this.btnOK, "btnOK");
			this.btnOK.Name = "btnOK";
			this.btnOK.UseVisualStyleBackColor = true;
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// cboCurrency
			// 
			this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboCurrency.DropDownWidth = 200;
			this.cboCurrency.FormattingEnabled = true;
			resources.ApplyResources(this.cboCurrency, "cboCurrency");
			this.cboCurrency.Name = "cboCurrency";
			// 
			// cboCustomerGroup
			// 
			this.cboCustomerGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboCustomerGroup.DropDownWidth = 100;
			this.cboCustomerGroup.FormattingEnabled = true;
			resources.ApplyResources(this.cboCustomerGroup, "cboCustomerGroup");
			this.cboCustomerGroup.Name = "cboCustomerGroup";
			this.cboCustomerGroup.SelectedIndexChanged += new System.EventHandler(this.cboCustomerGroup_SelectedIndexChanged);
			// 
			// lblCURRENCY
			// 
			resources.ApplyResources(this.lblCURRENCY, "lblCURRENCY");
			this.lblCURRENCY.Name = "lblCURRENCY";
			// 
			// lblCUSTOMER_GROUP
			// 
			resources.ApplyResources(this.lblCUSTOMER_GROUP, "lblCUSTOMER_GROUP");
			this.lblCUSTOMER_GROUP.Name = "lblCUSTOMER_GROUP";
			// 
			// cboCustomerConnectionType
			// 
			this.cboCustomerConnectionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboCustomerConnectionType.DropDownWidth = 200;
			this.cboCustomerConnectionType.FormattingEnabled = true;
			resources.ApplyResources(this.cboCustomerConnectionType, "cboCustomerConnectionType");
			this.cboCustomerConnectionType.Name = "cboCustomerConnectionType";
			// 
			// lblCONNECTION
			// 
			resources.ApplyResources(this.lblCONNECTION, "lblCONNECTION");
			this.lblCONNECTION.Name = "lblCONNECTION";
			// 
			// cboFilterPower
			// 
			this.cboFilterPower.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboFilterPower.DropDownWidth = 220;
			this.cboFilterPower.FormattingEnabled = true;
			this.cboFilterPower.Items.AddRange(new object[] {
            resources.GetString("cboFilterPower.Items"),
            resources.GetString("cboFilterPower.Items1"),
            resources.GetString("cboFilterPower.Items2"),
            resources.GetString("cboFilterPower.Items3")});
			resources.ApplyResources(this.cboFilterPower, "cboFilterPower");
			this.cboFilterPower.Name = "cboFilterPower";
			// 
			// lblFILTER_POWER
			// 
			resources.ApplyResources(this.lblFILTER_POWER, "lblFILTER_POWER");
			this.lblFILTER_POWER.Name = "lblFILTER_POWER";
			// 
			// DialogREFSubsidyOption
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Name = "DialogREFSubsidyOption";
			this.content.ResumeLayout(false);
			this.content.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnClose;
        private ExButton btnOK;
        private Label lblCUSTOMER_GROUP;
        private Label lblCURRENCY;
        public ComboBox cboCurrency;
        public ComboBox cboCustomerGroup;
        public ComboBox cboCustomerConnectionType;
        private Label lblCONNECTION;
        private Label lblFILTER_POWER;
        public ComboBox cboFilterPower;
    }
}