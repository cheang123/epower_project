﻿using System;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PageReportTransformerDetail : Form
    {
        CrystalReportHelper ch = null;

        public PageReportTransformerDetail()
        {
            InitializeComponent();
            this.viewer.ApplyDefaultFormat();

            UIHelper.SetDataSourceToComboBox(this.cboCapacity, DBDataContext.Db.TBL_CAPACITies.Where(x => x.IS_ACTIVE), Resources.ALL_CAPACITY);
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            if (this.cboCapacity.SelectedIndex == -1)
            {
                MsgBox.ShowInformation(Resources.MS_SELECT_YEAR);
                this.cboCapacity.Focus();
                return;
            }
            Runner.Run(this.viewReport);
        }

        public CrystalReportHelper ViewReportTransformerDetail(int intCapaId,  string strCapaName )
        {
            
            CrystalReportHelper c = new CrystalReportHelper("ReportTransformerDetail.rpt");
            c.SetParameter("@CAPACITY_ID", intCapaId);
            c.SetParameter("@CAPACITY_NAME", strCapaName);            
            return c; 
        }

        private void viewReport()
        { 
            using(TransactionScope tran=new TransactionScope( TransactionScopeOption.Required,TimeSpan.MaxValue))
	        {
                this.ch = ViewReportTransformerDetail((int)cboCapacity.SelectedValue, cboCapacity.Text);                 
                this.viewer.ReportSource = ch.Report;
                this.viewer.ViewReport();
                tran.Complete();
            }
        }


        private void sendMail()
        {
            try
            {
                this.viewReport();
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + this.ch.ReportName + ".xls").FullName;
                this.ch.ExportToExcel(exportPath);
                DialogSendMail.Instance.Add(this.ch.ReportName, exportPath);
                DialogSendMail.Instance.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            this.sendMail();
        }
    } 
}
