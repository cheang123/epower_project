﻿using DevExpress.XtraBars;
using EPower.Base.Helper.Component;
using EPower.Base.Helper;
using EPower.Base.Helper.DevExpressCustomize;
using EPower.Interface.Report.EFilling.Model;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class EFillingSale : Form
    {
        private EFilingReport _eFilingReport = EFilingReport.Summary;
        private SaleEFiling _efilingReport = new SaleEFiling();
        private Dictionary<string, string> _gridViewReportHeader = new Dictionary<string, string>();
        private Dictionary<string, object> _gridSources = new Dictionary<string, object>();

        public EFillingSale()
        {
            InitializeComponent();
            ApplySetting();
            //RestoreUserConfigure();            
        }
        private void ApplySetting()
        {
            //Dynamic.Helpers.ResourceHelper.ApplyResource(this);
            //dpExport.ToolTip = Resources.ShowAsExportFuction;
            rdpDate.Value = ReportDatePicker.DateRanges.LastMonth;
            searchControl.Properties.NullValuePrompt = Resources.SEARCH;
            // Report header
            var company = DBDataContext.Db.TBL_COMPANies.FirstOrDefault();
            _picLogo.Image = UIHelper.ConvertBinaryToImage(company.COMPANY_LOGO);
            _lblCompany.Text = company.COMPANY_NAME;
            _lblAddress.Text = company.ADDRESS;
            _lblTitle.Text = Resources.SALE_EFILLING;
            _lblSubTitle1.Text = string.Format(Resources.FROM_TO, rdpDate.FromDate.ToShortDate(), rdpDate.ToDate.ToShortDate());
            _lblCompany.AutoSize = false;
            _lblAddress.AutoSize = false;
            _lblCompany.Size = new System.Drawing.Size(300, 30);
            _lblAddress.Size = new System.Drawing.Size(300, 50);
            //efiling purchase gridview   
            dgvEFilingSale.SetDefaultBandGridview(false);
            dgvInvidualCustomer.SetDefaultGridview();
            dgvOverseaCustomer.SetDefaultGridview();
            dgvEFilingSale.OptionsPrint.PrintSelectedRowsOnly = false;
            dgvEFilingSale.OptionsPrint.PrintDetails = true;
            dgvEFilingSale.SetBestFitColumns();

            dgvInvidualCustomer.SetDefaultGridview();
            dgvInvidualCustomer.OptionsSelection.MultiSelect = false;
            dgvInvidualCustomer.OptionsPrint.PrintSelectedRowsOnly = false;
            colTaxAmount.FormatCountryCurrency();

            colNote.OptionsColumn.AllowShowHide = true;
            //gbNote.OptionsBand.allo

            dgvOverseaCustomer.OptionsSelection.MultiSelect = false;
            //dgvOverseaCustomer.option

            ////row no
            dgvEFilingSale.CustomUnboundColumnData += DgvEVATPurchase_CustomUnboundColumnData;
            dgvEFilingSale.CustomDrawGroupRow += DgvEVATPurchase_CustomDrawGroupRow;
             
            dgvOverseaCustomer.CustomUnboundColumnData += DgvEVATPurchase_CustomUnboundColumnData;
            dgvOverseaCustomer.CustomDrawGroupRow += DgvEVATPurchase_CustomDrawGroupRow;
             
            dgvInvidualCustomer.CustomUnboundColumnData += DgvEVATPurchase_CustomUnboundColumnData;
            dgvInvidualCustomer.CustomDrawGroupRow += DgvEVATPurchase_CustomDrawGroupRow;

            //init for export all 
            _gridViewReportHeader.Add(dgvEFilingSale.Name, "SALE");
            _gridViewReportHeader.Add(dgvInvidualCustomer.Name, "NONTAXABLE");
            _gridViewReportHeader.Add(dgvOverseaCustomer.Name, "OVERSEACOMPANY");
            _gridViewReportHeader.Add(dgvEFilingSale.Name + "Minus", "SALE(MINUS)");

            //var timer = new Timer();
            //timer.Interval = 500;//(int)TimeSpan.FromSeconds(1).TotalMilliseconds;
            //timer.Start();
            //timer.Tick += (object sender1, EventArgs e1) =>
            //{
            //    timer.Stop();
            rdpDate.ValueChanged += RdpDate_ValueChanged;
            //};
            Reload();

        }

        private void DgvEVATPurchase_CustomDrawGroupRow(object sender, DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridGroupRowInfo info = e.Info as DevExpress.XtraGrid.Views.Grid.ViewInfo.GridGroupRowInfo;
            DevExpress.XtraGrid.Views.Grid.GridView view = sender as DevExpress.XtraGrid.Views.Grid.GridView;
            string s = "";
            foreach (DevExpress.XtraGrid.GridGroupSummaryItem g in view.GroupSummary)
            {
                if (g.ShowInGroupColumnFooter == null)
                {
                    if (s != string.Empty)
                        s += "";
                    s += view.GetGroupSummaryDisplayText(e.RowHandle, g);
                }
            }
            info.GroupText = string.Format(view.GroupFormat, info.Column.Caption, info.EditValue, s);
        }
        private void DgvEVATPurchase_CustomUnboundColumnData(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs e)
        {
            if (e.IsGetData)
            {
                e.Value = e.ListSourceRowIndex + 1;
            }
        }
        private void RdpDate_ValueChanged(object sender, EventArgs e)
        {
            Reload();
        }


        public void Reload()
        {
            //if (this.IsLoading) return;
            // Report header
            var company  = DBDataContext.Db.TBL_COMPANies.FirstOrDefault();
            _picLogo.Image = UIHelper.ConvertBinaryToImage(company.COMPANY_LOGO);
            _lblCompany.Text = company.COMPANY_NAME;
            _lblAddress.Text = company.ADDRESS;
            _lblTitle.Text = Resources.SALE_EFILLING;
            _lblSubTitle1.Text = string.Format(Resources.FROM_TO, rdpDate.FromDate.ToShortDate(), rdpDate.ToDate.ToShortDate());
            //$"{Resources.FROM_TO} : {rdpDate.FromDate.ToShortDate()} " +
            //$"{Resources.ToDate} : {rdpDate.ToDate.ToShortDate()}";
            Search();
            //reportHeader.SetFormat();
        }

        private SaleEFiling getData(DateTime fromDate, DateTime toDate)
        {
            fromDate = fromDate.Date;
            toDate = toDate.Date.AddDays(1).AddSeconds(-1);
            var efilling = new SaleEFiling();
            //List<int> InvoiceItemExclude = new List<int>() { (int)InvoiceItem.TaxService, -7 };
            var saleData = (from i in DBDataContext.Db.TBL_INVOICEs
                            join c in DBDataContext.Db.TBL_CUSTOMERs on i.CUSTOMER_ID equals c.CUSTOMER_ID
                            join d in DBDataContext.Db.TBL_INVOICE_DETAILs on i.INVOICE_ID equals d.INVOICE_ID
                            where d.TRAN_DATE >= fromDate && d.TRAN_DATE <= toDate
                            group new { c, d } by new
                            {
                                INV_NO = i.INVOICE_NO,
                                TAX_CUSTOMER_TYPE_ID = c.TAX_CUSTOMER_TYPE_ID,
                                IS_SERVICE_BILL = i.IS_SERVICE_BILL,
                                d.REF_NO,
                                CustomerNameKh = c.LAST_NAME_KH + " " + c.FIRST_NAME_KH,
                                CustomerName = c.LAST_NAME + " " + c.FIRST_NAME,
                                rate = d.EXCHANGE_RATE,
                                CURRENCY_ID = i.CURRENCY_ID
                            } into g
                            select new SaleEFilingListModel
                            {
                                NameKH = g.Key.CustomerNameKh,
                                NameLatin = g.Key.CustomerName,
                                PartnerType = g.Key.TAX_CUSTOMER_TYPE_ID,
                                VendorInvoiceNo = g.Key.REF_NO,
                                TranDate = g.Max(x => x.d.TRAN_DATE),
                                VatIn = (!chkSHOW_ID_IF_HAVE.Checked && g.Key.TAX_CUSTOMER_TYPE_ID == (int)TaxCustomerType.TaxFree) ? "GC000001" : (!chkSHOW_ID_IF_HAVE.Checked) ? g.Max(x => x.c.ID_CARD_NO) : (g.Max(x => x.c.ID_CARD_NO) != "-") ? g.Max(x=>x.c.ID_CARD_NO) : "GC000001",
                                TaxAmount = g.Sum(x => x.d.AMOUNT) < 0 ? Math.Ceiling(g.Sum(x => x.d.AMOUNT) * g.Key.rate) : Math.Floor(g.Sum(x => x.d.AMOUNT) * g.Key.rate), //g.Sum(x => InvoiceItemExclude.Contains(x.d.INVOICE_ITEM_ID) == true ? x.d.AMOUNT : 0) * g.Key.rate,
                                NoneTaxAmount = 0,
                                Quantity = 0,
                                Type = TransactionType.Invoice,
                                TaxCode = g.Key.IS_SERVICE_BILL ? "1" : "3",
                                FormatDigit = "#,##0",
                                Note = g.Key.INV_NO,
                                CurrencyId = g.Key.CURRENCY_ID,
                                IncomeTaxRate = 1,
                                Sector = 0,
                                Description = g.Key.IS_SERVICE_BILL ? " កម្រៃសេវាកម្ម" : "លក់ថាមពលអគ្គិសនី"
                            }).ToList();

            //var prepaidSale = (from b in DBDataContext.Db.TBL_CUSTOMER_BUYs
            //                   join p in DBDataContext.Db.TBL_PREPAID_CUSTOMERs on b.PREPAID_CUS_ID equals p.PREPAID_CUS_ID
            //                   join c in DBDataContext.Db.TBL_CUSTOMERs on p.CUSTOMER_ID equals c.CUSTOMER_ID
            //                   where b.CREATE_ON.Date >= rdpDate.fromDate && b.CREATE_ON.Date <= rdpDate.toDate
            //                   select new SaleEFilingListModel
            //                   {
            //                       NameKH = c.LAST_NAME_KH + " " + c.FIRST_NAME_KH,
            //                       NameLatin = c.LAST_NAME + " " + c.FIRST_NAME,
            //                       PartnerType = c.TAX_CUSTOMER_TYPE_ID,
            //                       VendorInvoiceNo = b.BUY_NO,
            //                       TranDate = b.CREATE_ON,
            //                       VatIn = c.TAX_CUSTOMER_TYPE_ID == (int)TaxCustomerType.TaxFree ? "GC000001" : c.ID_CARD_NO,
            //                       TaxAmount = b.BUY_AMOUNT,
            //                       NoneTaxAmount = 0,
            //                       Quantity = 0,
            //                       Type = TransactionType.Invoice,
            //                       TaxCode = "3",
            //                       FormatDigit = "#,##0",
            //                       Note = "",
            //                       CurrencyId = b.CURRENCY_ID,
            //                       IncomeTaxRate = 1
            //                   }).ToList();

            var prepaidMinus = (from b in DBDataContext.Db.TBL_CUSTOMER_BUYs
                                join p in DBDataContext.Db.TBL_PREPAID_CUSTOMERs on b.PREPAID_CUS_ID equals p.PREPAID_CUS_ID
                                join c in DBDataContext.Db.TBL_CUSTOMERs on p.CUSTOMER_ID equals c.CUSTOMER_ID
                                join m in DBDataContext.Db.TBL_CUSTOMER_BUYs on b.PARENT_ID equals m.CUSTOMER_BUY_ID into g
                                from l in g.DefaultIfEmpty()
                                where b.CREATE_ON >= fromDate && b.CREATE_ON <= toDate
                                select new SaleEFilingListModel
                                {
                                    NameKH = c.LAST_NAME_KH + " " + c.FIRST_NAME_KH,
                                    NameLatin = c.LAST_NAME + " " + c.FIRST_NAME,
                                    PartnerType = c.TAX_CUSTOMER_TYPE_ID,
                                    VendorInvoiceNo = b.BUY_NO,
                                    TranDate = b.CREATE_ON,
                                    VatIn = (!chkSHOW_ID_IF_HAVE.Checked && c.TAX_CUSTOMER_TYPE_ID == (int)TaxCustomerType.TaxFree) ? "GC000001" : (!chkSHOW_ID_IF_HAVE.Checked) ? c.ID_CARD_NO : c.ID_CARD_NO != "-" ? c.ID_CARD_NO : "GC000001",
                                    TaxAmount = b.BUY_AMOUNT,
                                    NoneTaxAmount = 0,
                                    Quantity = 0,
                                    Type = TransactionType.Invoice,
                                    TaxCode = "3",
                                    FormatDigit = "#,##0",
                                    Note = l.BUY_NO??"",
                                    CurrencyId = b.CURRENCY_ID,
                                    IncomeTaxRate = 1
                                }).ToList();

            var cusData = DBDataContext.Db.TBL_CUSTOMERs.Where(x => x.ROW_DATE >= fromDate && x.ROW_DATE <= toDate && x.TAX_CUSTOMER_TYPE_ID == (int)TaxCustomerType.TaxFree).Select(x => new IndividualCustomerEFilingListModel()
            {
                Id = x.CUSTOMER_ID,
                NameKH = x.LAST_NAME_KH + " " + x.FIRST_NAME_KH,
                NameLatin = x.LAST_NAME + " " + x.FIRST_NAME,
                VatIn = (!chkSHOW_ID_IF_HAVE.Checked && x.TAX_CUSTOMER_TYPE_ID == (int)TaxCustomerType.TaxFree) ? "GC000001" : (!chkSHOW_ID_IF_HAVE.Checked) ? x.ID_CARD_NO : x.ID_CARD_NO != "-" ? x.ID_CARD_NO : "GC000001",
            }).ToList();

            saleData = saleData.Union(prepaidMinus).ToList();
            var EFsale = saleData.Where(x => x.TaxAmount > 0).OrderBy(x => x.VendorInvoiceNo).ToList();
            //EFsale.ForEach(x => x.TaxAmount = UIHelper.Round(x.TaxAmount, x.CurrencyId));
            efilling.SaleEFilingListModels = EFsale;
            efilling.SaleEFilingDetailListModels = saleData.Where(x => x.TaxAmount > 0).OrderBy(x => x.VendorInvoiceNo).ToList();

            var saleMinus = saleData.Where(x => x.TaxAmount < 0).OrderBy(x => x.VendorInvoiceNo).ToList();
            saleMinus.ForEach(x => x.TaxAmount = Math.Abs(x.TaxAmount));

            efilling.CreditNoteListModels = saleMinus;
            efilling.IndividualCustomerEFilingListModels = cusData;
            efilling.OverseaCustomerEFilingListModels = new List<OverseaCustomerEFilingListModel>();
            return efilling;
        }

        private void Search()
        { 
            Runner.RunNewThread(() =>
            {
                try
                {
                    _efilingReport = getData(rdpDate.FromDate, rdpDate.ToDate);
                }
                catch (Exception ex)
                {
                    MsgBox.LogError(ex);
                    throw new Exception(Base.Properties.Resources.YOU_CANNOT_VIEW_REPORT);
                }
            }, Resources.DOWNLOADING);

            switch (_eFilingReport)
            {
                case EFilingReport.Summary:
                    // Report header
                    _lblTitle.Text = Resources.SALE_EFILLING;
                    _lblSubTitle1.Text = string.Format(Resources.FROM_TO, rdpDate.FromDate.ToShortDate(), rdpDate.ToDate.ToShortDate());
                    //$"{Resources.FromDate} : {rdpDate.FromDate.ToShortDate()} " +
                    //$"{Resources.ToDate} : {rdpDate.ToDate.ToShortDate()}";
                    try
                    {
                        dgv.DataSource = _efilingReport.SaleEFilingListModels;
                        dgv.MainView = dgvEFilingSale;
                        gbNote.Visible = false;
                        dgvEFilingSale.RefreshData();
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                    break;
                case EFilingReport.Detail:
                    _lblTitle.Text = $"{Resources.SALE_EFILLING}-{Resources.DETAIL}";
                    _lblSubTitle1.Text = string.Format(Resources.FROM_TO, rdpDate.FromDate.ToShortDate(), rdpDate.ToDate.ToShortDate());
                    //$"{Resources.FromDate} : {rdpDate.FromDate.ToShortDate()} " +
                    //$"{Resources.ToDate} : {rdpDate.ToDate.ToShortDate()}";
                    try
                    {
                        dgv.DataSource = _efilingReport.SaleEFilingDetailListModels;
                        dgv.MainView = dgvEFilingSale;
                        dgvEFilingSale.RefreshData();
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                    break;
                case EFilingReport.IndividualVendor:
                    _lblTitle.Text = Resources.LIST_OF_INDIVIDUAL_VENDOR;
                    // Report header             
                    _lblSubTitle1.Text = string.Format(Resources.FROM_TO, rdpDate.FromDate.ToShortDate(), rdpDate.ToDate.ToShortDate());
                    //$"{Resources.FromDate} : {rdpDate.FromDate.ToShortDate()} " +
                    //$"{Resources.ToDate} : {rdpDate.ToDate.ToShortDate()}";
                    try
                    {
                        dgv.DataSource = _efilingReport.IndividualCustomerEFilingListModels;
                        dgv.MainView = dgvInvidualCustomer;
                        dgvInvidualCustomer.RefreshData();
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                    break;
                case EFilingReport.OverSeaVedor:
                    _lblTitle.Text = Resources.LIST_OF_OVERSEAS_VENDOR;
                    // Report header             
                    _lblSubTitle1.Text = string.Format(Resources.FROM_TO, rdpDate.FromDate.ToShortDate(), rdpDate.ToDate.ToShortDate());
                    //$"{Resources.FromDate} : {rdpDate.FromDate.ToShortDate()} " +
                    //$"{Resources.ToDate} : {rdpDate.ToDate.ToShortDate()}";
                    try
                    {
                        dgv.DataSource = _efilingReport.OverseaCustomerEFilingListModels;
                        dgv.MainView = dgvOverseaCustomer;
                        dgv.RefreshDataSource();
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                    break;
                case EFilingReport.CreditNote:
                    _lblTitle.Text = Resources.CREDIT_NOTE;
                    // Report header             
                    _lblSubTitle1.Text = $"{Resources.START_DATE} : {rdpDate.FromDate.ToShortDate()} {Resources.TO} : {rdpDate.ToDate.ToShortDate()} ";
                    //$"{Resources.FromDate} : {rdpDate.FromDate.ToShortDate()} " +
                    //$"{Resources.ToDate} : {rdpDate.ToDate.ToShortDate()}";
                    try
                    {
                        dgv.DataSource = _efilingReport.CreditNoteListModels;
                        dgv.MainView = dgvEFilingSale;
                        dgvEFilingSale.RefreshData();
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                    break;
                default:
                    break;
            }
        }


        public void RegisterExportEvent()
        {
            btnExcel.ItemClick += (object sender, ItemClickEventArgs e) => { ExportToExcel(); };
            btnPrint.ItemClick += (object sender, ItemClickEventArgs e) => { Print(); };
            btnCsv.ItemClick += (object sender, ItemClickEventArgs e) => { ExportToCsv(); };
            btnPdf.ItemClick += (object sender, ItemClickEventArgs e) => { ExportToPdf(); };
        }
        private string GetReportName()
        {
            var dateExport = DateTime.Now.ToString("_yyyyMMdd");
            var defaultFileName = $"{_lblTitle.Text}{dateExport}";
            return defaultFileName;
        }
        private string GetReportDescription()
        {
            var description =  string.Format(Resources.FROM_TO, rdpDate.FromDate.ToShortDate(), rdpDate.ToDate.ToShortDate());
            //var description = $"{Resources.FromDate} {rdpDate.FromDate.ToShortDate()} {Resources.ToDate} {rdpDate.ToDate.ToShortDate()} ";
            return description;
        }
        public void ExportToCsv()
        {
            DevExtension.ToCsv(dgv, _lblTitle.Text, GetReportDescription(), GetReportName(), true);
        }
        public void Print()
        {
            DevExtension.ShowGridPreview(dgv, _lblTitle.Text, GetReportDescription(), true);
        }
        public void ExportToExcel()
        {
            DevExtension.ToExcel(dgv, _lblTitle.Text, GetReportDescription(), GetReportName(), true, true);
        }
        public void ExportToExcelAll(bool isDetail = false)
        {
            _gridSources = new Dictionary<string, object>();

            //hide NOTE column
            //gbNote.Visible = false;
            _gridSources.Add(dgvEFilingSale.Name, _efilingReport.SaleEFilingListModels);
            _gridSources.Add(dgvInvidualCustomer.Name, _efilingReport.IndividualCustomerEFilingListModels);
            _gridSources.Add(dgvOverseaCustomer.Name, _efilingReport.OverseaCustomerEFilingListModels);

            //show NOTE column
            var state = gbNote.Visible;
            gbNote.Visible = true;
            _gridSources.Add(dgvEFilingSale.Name + "Minus", _efilingReport.CreditNoteListModels);
            string forMonth = rdpDate.ToDate.ToString("MM/yyyy");
            var dateDisplay = rdpDate.ToDate.ToString("yyyy-MM");
            var defaultFileName = $"E-Filling-Sale {dateDisplay}";

            if (dgvEFilingSale.RowCount > 10000)
            {
                 if( MsgBox.ShowInformation(Resources.EXPORT_WILL_TAKE_MORETHAN_5MIN) != DialogResult.OK)
                {
                    return;
                }
            }
            Runner.RunNewThread(delegate ()
            {
                try
                {
                    DevExtension.ExportEFiling(this, dgv, forMonth, _gridSources, _gridViewReportHeader, "", GetReportDescription(), defaultFileName, true, true, rdpDate.ToDate);
                }
                catch (Exception ex)
                {
                    MsgBox.LogError(ex);
                    throw new Exception(Base.Properties.Resources.MSG_EXPORT_ERROR);
                }
            }, Resources.EXPORTING);
            gbNote.Visible = state;
            btnEFillingSale_ItemClick(null, null);
        }
        public void ExportToPdf()
        {
            DevExtension.ToPdf(dgv, _lblTitle.Text, GetReportDescription(), GetReportName(), true);
        }
        //public void SaveUserConfigure()
        //{
        //    dgvEFilingSale.SaveUserConfigureToXml();
        //    dgvInvidualCustomer.SaveUserConfigureToXml();
        //    dgvOverseaCustomer.SaveUserConfigureToXml();
        //}
        private void btnEFillingSale_ItemClick(object sender, ItemClickEventArgs e)
        {
            _lblTitle.Text = Resources.SALE_EFILLING;
            _lblSubTitle1.Text = string.Format(Resources.FROM_TO, rdpDate.FromDate.ToShortDate(), rdpDate.ToDate.ToShortDate());
            //$"{Resources.FromDate} : {rdpDate.FromDate.ToShortDate()} " +
            //$"{Resources.ToDate} : {rdpDate.ToDate.ToShortDate()}";

            _eFilingReport = EFilingReport.Summary;
            dgv.DataSource = _efilingReport.SaleEFilingListModels;
            dgv.MainView = dgvEFilingSale;
            gbNote.Visible = false;
            dgv.RefreshDataSource();
        }

        private void btnCreditNote_ItemClick(object sender, ItemClickEventArgs e)
        {
            _lblTitle.Text = Resources.CREDIT_NOTE;
            _lblSubTitle1.Text = string.Format(Resources.FROM_TO, rdpDate.FromDate.ToShortDate(), rdpDate.ToDate.ToShortDate());
            //$"{Resources.FromDate} : {rdpDate.FromDate.ToShortDate()} " +
            //$"{Resources.ToDate} : {rdpDate.ToDate.ToShortDate()}";

            _eFilingReport = EFilingReport.CreditNote;
            dgv.DataSource = _efilingReport.CreditNoteListModels;
            dgv.MainView = dgvEFilingSale;
            gbNote.Visible = true;
            dgv.RefreshDataSource();
        }

        private void btnEFilingDetail_ItemClick(object sender, ItemClickEventArgs e)
        {
            _lblTitle.Text = $"{Resources.SALE_EFILLING}-{Resources.DETAIL}";
            _lblSubTitle1.Text = string.Format(Resources.FROM_TO, rdpDate.FromDate.ToShortDate(), rdpDate.ToDate.ToShortDate());
            //$"{Resources.FromDate} : {rdpDate.FromDate.ToShortDate()} " +
            //$"{Resources.ToDate} : {rdpDate.ToDate.ToShortDate()}";

            _eFilingReport = EFilingReport.Detail;
            dgv.DataSource = _efilingReport.SaleEFilingDetailListModels;
            dgv.MainView = dgvEFilingSale;
            gbNote.Visible = false;
            dgv.RefreshDataSource();
        }

        private void btnIndividualVendor_ItemClick(object sender, ItemClickEventArgs e)
        {
            _lblTitle.Text = $"{Resources.LIST_OF_INDIVIDUAL_VENDOR}";
            _lblSubTitle1.Text = string.Format(Resources.FROM_TO, rdpDate.FromDate.ToShortDate(), rdpDate.ToDate.ToShortDate());
            //$"{Resources.FromDate} : {rdpDate.FromDate.ToShortDate()} " +
            //$"{Resources.ToDate} : {rdpDate.ToDate.ToShortDate()}";

            _eFilingReport = EFilingReport.IndividualVendor;
            dgv.DataSource = _efilingReport.IndividualCustomerEFilingListModels;
            dgv.MainView = dgvInvidualCustomer;
            dgv.RefreshDataSource();
        }

        private void btnPurchaseOverSea_ItemClick(object sender, ItemClickEventArgs e)
        {
            _lblTitle.Text = $"{Resources.LIST_OF_OVERSEAS_VENDOR}";
            _lblSubTitle1.Text = string.Format(Resources.FROM_TO, rdpDate.FromDate.ToShortDate(), rdpDate.ToDate.ToShortDate());
            //$"{Resources.FromDate} : {rdpDate.FromDate.ToShortDate()} " +
            //$"{Resources.ToDate} : {rdpDate.ToDate.ToShortDate()}";

            _eFilingReport = EFilingReport.OverSeaVedor;
            dgv.DataSource = _efilingReport.OverseaCustomerEFilingListModels;
            dgv.MainView = dgvOverseaCustomer;
            dgv.RefreshDataSource();
        }

        private void btnExcel_ItemClick(object sender, ItemClickEventArgs e)
        {
            ExportToExcel();
        }

        private void btnCsv_ItemClick(object sender, ItemClickEventArgs e)
        {
            ExportToCsv();
        }

        private void btnPdf_ItemClick(object sender, ItemClickEventArgs e)
        {
            ExportToPdf();
        }

        private void btnPrint_ItemClick(object sender, ItemClickEventArgs e)
        {
            Print();
        }

        private void btnExcelAll_ItemClick(object sender, ItemClickEventArgs e)
        {
            ExportToExcelAll();
        }

        public SaleEFilingListModel GetTransacion()
        {
            var selectedTransaction = (dgvEFilingSale).GetFocusedRow() as SaleEFilingListModel;
            if (selectedTransaction == null)
            {
                return null;
            }
            return selectedTransaction;
        }

        private void _chkShowID_CheckedChanged(object sender, EventArgs e)
        {
            Reload();
        }
    }
}