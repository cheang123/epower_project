﻿using EPower.Base.Helper.Component;

namespace EPower.Interface
{
    partial class EFillingSale
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EFillingSale));
            this.dgvOverseaCustomer = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId_1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTinOverSea = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyNameKh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyNameLatin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCountryCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyPhone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyMail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRowNoOc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFormat_2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dgv = new DevExpress.XtraGrid.GridControl();
            this.dgvInvidualCustomer = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRowNoIc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTins = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNameKH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNameLatins = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFormat_1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dgvEFilingSale = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.colRowNo = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTransDate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colInvoiceVendor = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPartnerType = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTIN = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colKhName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colNameLatin = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTaxCode = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTaxAmount = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colNoneTaxAmount = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSpecialTax = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSpecialTaxonService = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPublicTax = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colOfficeTax = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colIncomeTaxRate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSector = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTresuryCreditNumber = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDescription = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colNote = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colFormat = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.picconvertToBill = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.picDelivery = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.picPrint = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkSHOW_ID_IF_HAVE = new System.Windows.Forms.CheckBox();
            this.searchControl = new DevExpress.XtraEditors.SearchControl();
            this.rdpDate = new EPower.Base.Helper.Component.ReportDatePicker();
            this.popActions = new DevExpress.XtraBars.PopupMenu(this.components);
            this.btnEFilingSale = new DevExpress.XtraBars.BarButtonItem();
            this.btnCreditNote = new DevExpress.XtraBars.BarButtonItem();
            this.btnPurchaseOverSea = new DevExpress.XtraBars.BarButtonItem();
            this.btnIndividualVendor = new DevExpress.XtraBars.BarButtonItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.btnExcel = new DevExpress.XtraBars.BarButtonItem();
            this.btnCsv = new DevExpress.XtraBars.BarButtonItem();
            this.btnPdf = new DevExpress.XtraBars.BarButtonItem();
            this.btnPrint = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportEfilingSale = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.popupExport = new DevExpress.XtraBars.PopupMenu(this.components);
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this._lblSubTitle1 = new System.Windows.Forms.Label();
            this._lblTitle = new System.Windows.Forms.Label();
            this._lblAddress = new System.Windows.Forms.Label();
            this._lblCompany = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this._picLogo = new System.Windows.Forms.PictureBox();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.dpExport = new DevExpress.XtraEditors.DropDownButton();
            this.dpAction = new DevExpress.XtraEditors.DropDownButton();
            this.gbRowNo = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gbBillDate = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gbInvoiceNo = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gbVendor = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gbType = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand12 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand13 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand11 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gvPartnerType = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gbTotalTax = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gbTotalNonTax = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gbSpcecialTax = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gbPublicTax = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gbOfficeTax = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.IncomeTaxRate = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gbSector = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gbDescription = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gbNote = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOverseaCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInvidualCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEFilingSale)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picconvertToBill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDelivery)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPrint)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchControl.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popActions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupExport)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._picLogo)).BeginInit();
            this.flowLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvOverseaCustomer
            // 
            this.dgvOverseaCustomer.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.dgvOverseaCustomer.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.dgvOverseaCustomer.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.dgvOverseaCustomer.Appearance.DetailTip.Options.UseFont = true;
            this.dgvOverseaCustomer.Appearance.Empty.Options.UseFont = true;
            this.dgvOverseaCustomer.Appearance.EvenRow.Options.UseFont = true;
            this.dgvOverseaCustomer.Appearance.FilterCloseButton.Options.UseFont = true;
            this.dgvOverseaCustomer.Appearance.FilterPanel.Options.UseFont = true;
            this.dgvOverseaCustomer.Appearance.FixedLine.Options.UseFont = true;
            this.dgvOverseaCustomer.Appearance.FocusedCell.Options.UseFont = true;
            this.dgvOverseaCustomer.Appearance.FocusedRow.Options.UseFont = true;
            this.dgvOverseaCustomer.Appearance.FooterPanel.Options.UseFont = true;
            this.dgvOverseaCustomer.Appearance.GroupButton.Options.UseFont = true;
            this.dgvOverseaCustomer.Appearance.GroupFooter.Options.UseFont = true;
            this.dgvOverseaCustomer.Appearance.GroupPanel.Options.UseFont = true;
            this.dgvOverseaCustomer.Appearance.GroupRow.Options.UseFont = true;
            this.dgvOverseaCustomer.Appearance.HeaderPanel.Options.UseFont = true;
            this.dgvOverseaCustomer.Appearance.HideSelectionRow.Options.UseFont = true;
            this.dgvOverseaCustomer.Appearance.HorzLine.Options.UseFont = true;
            this.dgvOverseaCustomer.Appearance.HotTrackedRow.Options.UseFont = true;
            this.dgvOverseaCustomer.Appearance.OddRow.Options.UseFont = true;
            this.dgvOverseaCustomer.Appearance.Preview.Options.UseFont = true;
            this.dgvOverseaCustomer.Appearance.Row.Options.UseFont = true;
            this.dgvOverseaCustomer.Appearance.RowSeparator.Options.UseFont = true;
            this.dgvOverseaCustomer.Appearance.SelectedRow.Options.UseFont = true;
            this.dgvOverseaCustomer.Appearance.TopNewRow.Options.UseFont = true;
            this.dgvOverseaCustomer.Appearance.VertLine.Options.UseFont = true;
            this.dgvOverseaCustomer.Appearance.ViewCaption.Options.UseFont = true;
            this.dgvOverseaCustomer.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId_1,
            this.colTinOverSea,
            this.colCompanyNameKh,
            this.colCompanyNameLatin,
            this.colCountryCode,
            this.colCompanyPhone,
            this.colCompanyMail,
            this.colCompanyAddress,
            this.colRowNoOc,
            this.colFormat_2});
            this.dgvOverseaCustomer.GridControl = this.dgv;
            this.dgvOverseaCustomer.Name = "dgvOverseaCustomer";
            this.dgvOverseaCustomer.OptionsBehavior.Editable = false;
            this.dgvOverseaCustomer.OptionsCustomization.AllowQuickHideColumns = false;
            this.dgvOverseaCustomer.OptionsView.ShowIndicator = false;
            // 
            // colId_1
            // 
            this.colId_1.Caption = "Id";
            this.colId_1.FieldName = "Id";
            this.colId_1.Name = "colId_1";
            // 
            // colTinOverSea
            // 
            this.colTinOverSea.Caption = "លេខអត្ដសញ្ញាណកម្មបរទេស*";
            this.colTinOverSea.FieldName = "Vattin";
            this.colTinOverSea.Name = "colTinOverSea";
            this.colTinOverSea.Visible = true;
            this.colTinOverSea.VisibleIndex = 1;
            // 
            // colCompanyNameKh
            // 
            this.colCompanyNameKh.Caption = "ឈ្មោះក្រុមហ៊ុន (ខ្មែរ)*";
            this.colCompanyNameKh.FieldName = "NameKH";
            this.colCompanyNameKh.Name = "colCompanyNameKh";
            this.colCompanyNameKh.Visible = true;
            this.colCompanyNameKh.VisibleIndex = 2;
            // 
            // colCompanyNameLatin
            // 
            this.colCompanyNameLatin.Caption = "ឈ្មោះក្រុមហ៊ុន (ឡាតាំង)*";
            this.colCompanyNameLatin.FieldName = "NameLatin";
            this.colCompanyNameLatin.Name = "colCompanyNameLatin";
            this.colCompanyNameLatin.Visible = true;
            this.colCompanyNameLatin.VisibleIndex = 3;
            // 
            // colCountryCode
            // 
            this.colCountryCode.Caption = "លេខកូដប្រទេស*";
            this.colCountryCode.FieldName = "CountryCode";
            this.colCountryCode.Name = "colCountryCode";
            this.colCountryCode.Visible = true;
            this.colCountryCode.VisibleIndex = 4;
            // 
            // colCompanyPhone
            // 
            this.colCompanyPhone.Caption = "លេខទូរស័ព្ទក្រុមហ៊ុន*";
            this.colCompanyPhone.FieldName = "Phone";
            this.colCompanyPhone.Name = "colCompanyPhone";
            this.colCompanyPhone.Visible = true;
            this.colCompanyPhone.VisibleIndex = 5;
            // 
            // colCompanyMail
            // 
            this.colCompanyMail.Caption = "សារអេឡិកត្រូនិក*";
            this.colCompanyMail.FieldName = "Email";
            this.colCompanyMail.Name = "colCompanyMail";
            this.colCompanyMail.Visible = true;
            this.colCompanyMail.VisibleIndex = 6;
            // 
            // colCompanyAddress
            // 
            this.colCompanyAddress.Caption = "អាសយដ្ឋាន*";
            this.colCompanyAddress.FieldName = "Address";
            this.colCompanyAddress.Name = "colCompanyAddress";
            this.colCompanyAddress.Visible = true;
            this.colCompanyAddress.VisibleIndex = 7;
            // 
            // colRowNoOc
            // 
            this.colRowNoOc.Caption = "ល.រ*";
            this.colRowNoOc.FieldName = "RowNo";
            this.colRowNoOc.Name = "colRowNoOc";
            this.colRowNoOc.Visible = true;
            this.colRowNoOc.VisibleIndex = 0;
            // 
            // colFormat_2
            // 
            this.colFormat_2.Caption = "Format";
            this.colFormat_2.FieldName = "Format";
            this.colFormat_2.Name = "colFormat_2";
            // 
            // dgv
            // 
            this.dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv.Font = new System.Drawing.Font("Khmer Kep", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            gridLevelNode1.LevelTemplate = this.dgvOverseaCustomer;
            gridLevelNode1.RelationName = "Level1";
            gridLevelNode2.LevelTemplate = this.dgvInvidualCustomer;
            gridLevelNode2.RelationName = "Level2";
            this.dgv.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1,
            gridLevelNode2});
            this.dgv.Location = new System.Drawing.Point(0, 178);
            this.dgv.MainView = this.dgvEFilingSale;
            this.dgv.Name = "dgv";
            this.dgv.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.picconvertToBill,
            this.picDelivery,
            this.picPrint});
            this.dgv.Size = new System.Drawing.Size(1197, 403);
            this.dgv.TabIndex = 224;
            this.dgv.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.dgvInvidualCustomer,
            this.dgvEFilingSale,
            this.dgvOverseaCustomer});
            // 
            // dgvInvidualCustomer
            // 
            this.dgvInvidualCustomer.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.dgvInvidualCustomer.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.dgvInvidualCustomer.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.dgvInvidualCustomer.Appearance.DetailTip.Options.UseFont = true;
            this.dgvInvidualCustomer.Appearance.Empty.Options.UseFont = true;
            this.dgvInvidualCustomer.Appearance.EvenRow.Options.UseFont = true;
            this.dgvInvidualCustomer.Appearance.FilterCloseButton.Options.UseFont = true;
            this.dgvInvidualCustomer.Appearance.FilterPanel.Options.UseFont = true;
            this.dgvInvidualCustomer.Appearance.FixedLine.Options.UseFont = true;
            this.dgvInvidualCustomer.Appearance.FocusedCell.Options.UseFont = true;
            this.dgvInvidualCustomer.Appearance.FocusedRow.Options.UseFont = true;
            this.dgvInvidualCustomer.Appearance.FooterPanel.Options.UseFont = true;
            this.dgvInvidualCustomer.Appearance.GroupButton.Options.UseFont = true;
            this.dgvInvidualCustomer.Appearance.GroupFooter.Options.UseFont = true;
            this.dgvInvidualCustomer.Appearance.GroupPanel.Options.UseFont = true;
            this.dgvInvidualCustomer.Appearance.GroupRow.Options.UseFont = true;
            this.dgvInvidualCustomer.Appearance.HeaderPanel.Options.UseFont = true;
            this.dgvInvidualCustomer.Appearance.HideSelectionRow.Options.UseFont = true;
            this.dgvInvidualCustomer.Appearance.HorzLine.Options.UseFont = true;
            this.dgvInvidualCustomer.Appearance.HotTrackedRow.Options.UseFont = true;
            this.dgvInvidualCustomer.Appearance.OddRow.Options.UseFont = true;
            this.dgvInvidualCustomer.Appearance.Preview.Options.UseFont = true;
            this.dgvInvidualCustomer.Appearance.Row.Options.UseFont = true;
            this.dgvInvidualCustomer.Appearance.RowSeparator.Options.UseFont = true;
            this.dgvInvidualCustomer.Appearance.SelectedRow.Options.UseFont = true;
            this.dgvInvidualCustomer.Appearance.TopNewRow.Options.UseFont = true;
            this.dgvInvidualCustomer.Appearance.VertLine.Options.UseFont = true;
            this.dgvInvidualCustomer.Appearance.ViewCaption.Options.UseFont = true;
            this.dgvInvidualCustomer.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colRowNoIc,
            this.colTins,
            this.colNameKH,
            this.colNameLatins,
            this.colFormat_1});
            this.dgvInvidualCustomer.GridControl = this.dgv;
            this.dgvInvidualCustomer.Name = "dgvInvidualCustomer";
            this.dgvInvidualCustomer.OptionsCustomization.AllowQuickHideColumns = false;
            // 
            // colId
            // 
            this.colId.Caption = "Id";
            this.colId.Name = "colId";
            // 
            // colRowNoIc
            // 
            this.colRowNoIc.Caption = "ល.រ*";
            this.colRowNoIc.FieldName = "RowNo";
            this.colRowNoIc.Name = "colRowNoIc";
            this.colRowNoIc.Visible = true;
            this.colRowNoIc.VisibleIndex = 0;
            this.colRowNoIc.Width = 159;
            // 
            // colTins
            // 
            this.colTins.Caption = "លេខសម្គាល់អត្តសញ្ញាណ*";
            this.colTins.FieldName = "VatIn";
            this.colTins.Name = "colTins";
            this.colTins.Visible = true;
            this.colTins.VisibleIndex = 1;
            this.colTins.Width = 484;
            // 
            // colNameKH
            // 
            this.colNameKH.Caption = "ឈ្មោះ (ខ្មែរ)*";
            this.colNameKH.FieldName = "NameKH";
            this.colNameKH.Name = "colNameKH";
            this.colNameKH.Visible = true;
            this.colNameKH.VisibleIndex = 2;
            this.colNameKH.Width = 484;
            // 
            // colNameLatins
            // 
            this.colNameLatins.Caption = "ឈ្មោះ (ឡាតាំង)*";
            this.colNameLatins.FieldName = "NameLatin";
            this.colNameLatins.Name = "colNameLatins";
            this.colNameLatins.Visible = true;
            this.colNameLatins.VisibleIndex = 3;
            this.colNameLatins.Width = 488;
            // 
            // colFormat_1
            // 
            this.colFormat_1.Caption = "Format";
            this.colFormat_1.FieldName = "Format";
            this.colFormat_1.Name = "colFormat_1";
            // 
            // dgvEFilingSale
            // 
            this.dgvEFilingSale.Appearance.BandPanel.Options.UseBorderColor = true;
            this.dgvEFilingSale.Appearance.BandPanel.Options.UseFont = true;
            this.dgvEFilingSale.Appearance.BandPanelBackground.Options.UseFont = true;
            this.dgvEFilingSale.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.dgvEFilingSale.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.dgvEFilingSale.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.dgvEFilingSale.Appearance.DetailTip.Options.UseFont = true;
            this.dgvEFilingSale.Appearance.Empty.Options.UseFont = true;
            this.dgvEFilingSale.Appearance.EvenRow.Options.UseFont = true;
            this.dgvEFilingSale.Appearance.FilterCloseButton.Options.UseFont = true;
            this.dgvEFilingSale.Appearance.FilterPanel.Options.UseFont = true;
            this.dgvEFilingSale.Appearance.FixedLine.Options.UseFont = true;
            this.dgvEFilingSale.Appearance.FocusedCell.Options.UseFont = true;
            this.dgvEFilingSale.Appearance.FocusedRow.Options.UseFont = true;
            this.dgvEFilingSale.Appearance.FooterPanel.Options.UseFont = true;
            this.dgvEFilingSale.Appearance.GroupButton.Options.UseFont = true;
            this.dgvEFilingSale.Appearance.GroupFooter.Options.UseFont = true;
            this.dgvEFilingSale.Appearance.GroupPanel.Options.UseFont = true;
            this.dgvEFilingSale.Appearance.GroupRow.Options.UseFont = true;
            this.dgvEFilingSale.Appearance.HeaderPanel.Options.UseFont = true;
            this.dgvEFilingSale.Appearance.HeaderPanelBackground.Options.UseFont = true;
            this.dgvEFilingSale.Appearance.HideSelectionRow.Options.UseFont = true;
            this.dgvEFilingSale.Appearance.HorzLine.Options.UseFont = true;
            this.dgvEFilingSale.Appearance.HotTrackedRow.Options.UseFont = true;
            this.dgvEFilingSale.Appearance.OddRow.Options.UseFont = true;
            this.dgvEFilingSale.Appearance.Preview.Options.UseFont = true;
            this.dgvEFilingSale.Appearance.Row.Options.UseFont = true;
            this.dgvEFilingSale.Appearance.RowSeparator.Options.UseFont = true;
            this.dgvEFilingSale.Appearance.SelectedRow.Options.UseFont = true;
            this.dgvEFilingSale.Appearance.TopNewRow.Options.UseFont = true;
            this.dgvEFilingSale.Appearance.VertLine.Options.UseFont = true;
            this.dgvEFilingSale.Appearance.ViewCaption.Options.UseFont = true;
            this.dgvEFilingSale.AppearancePrint.BandPanel.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.dgvEFilingSale.AppearancePrint.BandPanel.Options.UseFont = true;
            this.dgvEFilingSale.AppearancePrint.EvenRow.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.dgvEFilingSale.AppearancePrint.EvenRow.Options.UseFont = true;
            this.dgvEFilingSale.AppearancePrint.FilterPanel.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.dgvEFilingSale.AppearancePrint.FilterPanel.Options.UseFont = true;
            this.dgvEFilingSale.AppearancePrint.FooterPanel.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.dgvEFilingSale.AppearancePrint.FooterPanel.Options.UseFont = true;
            this.dgvEFilingSale.AppearancePrint.GroupFooter.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.dgvEFilingSale.AppearancePrint.GroupFooter.Options.UseFont = true;
            this.dgvEFilingSale.AppearancePrint.GroupRow.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.dgvEFilingSale.AppearancePrint.GroupRow.Options.UseFont = true;
            this.dgvEFilingSale.AppearancePrint.HeaderPanel.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.dgvEFilingSale.AppearancePrint.HeaderPanel.Options.UseFont = true;
            this.dgvEFilingSale.AppearancePrint.Lines.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.dgvEFilingSale.AppearancePrint.Lines.Options.UseFont = true;
            this.dgvEFilingSale.AppearancePrint.OddRow.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.dgvEFilingSale.AppearancePrint.OddRow.Options.UseFont = true;
            this.dgvEFilingSale.AppearancePrint.Preview.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.dgvEFilingSale.AppearancePrint.Preview.Options.UseFont = true;
            this.dgvEFilingSale.AppearancePrint.Row.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.dgvEFilingSale.AppearancePrint.Row.Options.UseFont = true;
            this.dgvEFilingSale.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gbRowNo,
            this.gbBillDate,
            this.gbInvoiceNo,
            this.gbVendor,
            this.gvPartnerType,
            this.gbTotalTax,
            this.gbTotalNonTax,
            this.gbSpcecialTax,
            this.gridBand6,
            this.gbPublicTax,
            this.gbOfficeTax,
            this.IncomeTaxRate,
            this.gbSector,
            this.gridBand3,
            this.gbDescription,
            this.gbNote});
            this.dgvEFilingSale.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colTransDate,
            this.colTaxAmount,
            this.colNoneTaxAmount,
            this.colInvoiceVendor,
            this.colPartnerType,
            this.colTIN,
            this.colKhName,
            this.colNameLatin,
            this.colTaxCode,
            this.colFormat,
            this.colSpecialTax,
            this.colPublicTax,
            this.colRowNo,
            this.colOfficeTax,
            this.colIncomeTaxRate,
            this.colSector,
            this.colDescription,
            this.colNote,
            this.colTresuryCreditNumber,
            this.colSpecialTaxonService});
            this.dgvEFilingSale.GridControl = this.dgv;
            this.dgvEFilingSale.Name = "dgvEFilingSale";
            this.dgvEFilingSale.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.dgvEFilingSale.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.dgvEFilingSale.OptionsBehavior.Editable = false;
            this.dgvEFilingSale.OptionsCustomization.AllowQuickHideColumns = false;
            this.dgvEFilingSale.OptionsView.ShowColumnHeaders = false;
            this.dgvEFilingSale.OptionsView.ShowIndicator = false;
            // 
            // colRowNo
            // 
            this.colRowNo.AppearanceCell.Options.UseTextOptions = true;
            this.colRowNo.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRowNo.Caption = "ល.រ*";
            this.colRowNo.FieldName = "RowNo";
            this.colRowNo.Name = "colRowNo";
            this.colRowNo.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.colRowNo.Visible = true;
            this.colRowNo.Width = 61;
            // 
            // colTransDate
            // 
            this.colTransDate.AppearanceCell.Options.UseTextOptions = true;
            this.colTransDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTransDate.Caption = "កាលបរិច្ឆេទ*";
            this.colTransDate.DisplayFormat.FormatString = "dd-MM-yyyy";
            this.colTransDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTransDate.FieldName = "TranDate";
            this.colTransDate.Name = "colTransDate";
            this.colTransDate.Visible = true;
            this.colTransDate.Width = 92;
            // 
            // colInvoiceVendor
            // 
            this.colInvoiceVendor.AppearanceCell.Options.UseTextOptions = true;
            this.colInvoiceVendor.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colInvoiceVendor.Caption = "លេខវិក្កយបត្រ ឬ ប្រតិវេទន៍គយ*";
            this.colInvoiceVendor.FieldName = "VendorInvoiceNo";
            this.colInvoiceVendor.Name = "colInvoiceVendor";
            this.colInvoiceVendor.Visible = true;
            this.colInvoiceVendor.Width = 111;
            // 
            // colPartnerType
            // 
            this.colPartnerType.AppearanceCell.Options.UseTextOptions = true;
            this.colPartnerType.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colPartnerType.Caption = "ប្រភេទ*";
            this.colPartnerType.FieldName = "PartnerType";
            this.colPartnerType.Name = "colPartnerType";
            this.colPartnerType.Visible = true;
            this.colPartnerType.Width = 55;
            // 
            // colTIN
            // 
            this.colTIN.AppearanceCell.Options.UseTextOptions = true;
            this.colTIN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colTIN.Caption = "លេខសម្គាល់*";
            this.colTIN.FieldName = "VatIn";
            this.colTIN.Name = "colTIN";
            this.colTIN.Visible = true;
            this.colTIN.Width = 80;
            // 
            // colKhName
            // 
            this.colKhName.AppearanceCell.Options.UseTextOptions = true;
            this.colKhName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colKhName.Caption = "ឈ្មោះ(ខ្មែរ)";
            this.colKhName.FieldName = "NameKH";
            this.colKhName.Name = "colKhName";
            this.colKhName.Visible = true;
            // 
            // colNameLatin
            // 
            this.colNameLatin.AppearanceCell.Options.UseTextOptions = true;
            this.colNameLatin.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colNameLatin.Caption = "ឈ្មោះ(ឡាតាំង)";
            this.colNameLatin.FieldName = "NameLatin";
            this.colNameLatin.Name = "colNameLatin";
            this.colNameLatin.Visible = true;
            this.colNameLatin.Width = 126;
            // 
            // colTaxCode
            // 
            this.colTaxCode.AppearanceCell.Options.UseTextOptions = true;
            this.colTaxCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colTaxCode.Caption = "ប្រភេទផ្គត់ផ្គង់ទំនិញ*";
            this.colTaxCode.FieldName = "TaxCode";
            this.colTaxCode.Name = "colTaxCode";
            this.colTaxCode.Visible = true;
            this.colTaxCode.Width = 137;
            // 
            // colTaxAmount
            // 
            this.colTaxAmount.AppearanceCell.Options.UseTextOptions = true;
            this.colTaxAmount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colTaxAmount.Caption = "តម្លៃសរុបជាប់ អតប*";
            this.colTaxAmount.DisplayFormat.FormatString = "{0:#,#;#,#}";
            this.colTaxAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTaxAmount.FieldName = "TaxAmount";
            this.colTaxAmount.Name = "colTaxAmount";
            this.colTaxAmount.Visible = true;
            this.colTaxAmount.Width = 127;
            // 
            // colNoneTaxAmount
            // 
            this.colNoneTaxAmount.AppearanceCell.Options.UseTextOptions = true;
            this.colNoneTaxAmount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colNoneTaxAmount.Caption = "តម្លៃសរុបមិនជាប់ អតប ឬ អតប អត្រា ០%";
            this.colNoneTaxAmount.Name = "colNoneTaxAmount";
            this.colNoneTaxAmount.Visible = true;
            this.colNoneTaxAmount.Width = 121;
            // 
            // colSpecialTax
            // 
            this.colSpecialTax.Caption = "អាករពិសេសលើទំនិញមួយចំនួន";
            this.colSpecialTax.FieldName = "SpecialTax";
            this.colSpecialTax.Name = "colSpecialTax";
            this.colSpecialTax.Visible = true;
            this.colSpecialTax.Width = 139;
            // 
            // colSpecialTaxonService
            // 
            this.colSpecialTaxonService.Caption = "អាករពិសេសលើសេវាមួយចំនួន";
            this.colSpecialTaxonService.Name = "colSpecialTaxonService";
            this.colSpecialTaxonService.Visible = true;
            this.colSpecialTaxonService.Width = 139;
            // 
            // colPublicTax
            // 
            this.colPublicTax.Caption = "អាករបំភ្លឺសាធារណៈ";
            this.colPublicTax.FieldName = "PublicTax";
            this.colPublicTax.Name = "colPublicTax";
            this.colPublicTax.Visible = true;
            this.colPublicTax.Width = 149;
            // 
            // colOfficeTax
            // 
            this.colOfficeTax.Caption = "អាករលើការស្នាក់នៅ";
            this.colOfficeTax.FieldName = "TaxOnOffice";
            this.colOfficeTax.Name = "colOfficeTax";
            this.colOfficeTax.Visible = true;
            this.colOfficeTax.Width = 132;
            // 
            // colIncomeTaxRate
            // 
            this.colIncomeTaxRate.Caption = "អត្រាប្រាក់រំដោះពន្ធលើប្រាក់ចំណូល";
            this.colIncomeTaxRate.FieldName = "IncomeTaxRate";
            this.colIncomeTaxRate.Name = "colIncomeTaxRate";
            this.colIncomeTaxRate.Visible = true;
            this.colIncomeTaxRate.Width = 231;
            // 
            // colSector
            // 
            this.colSector.Caption = "វិស័យ";
            this.colSector.FieldName = "Sector";
            this.colSector.Name = "colSector";
            this.colSector.Visible = true;
            this.colSector.Width = 66;
            // 
            // colTresuryCreditNumber
            // 
            this.colTresuryCreditNumber.Caption = "លេខឥណទានរតនាគារជាតិ";
            this.colTresuryCreditNumber.Name = "colTresuryCreditNumber";
            this.colTresuryCreditNumber.Visible = true;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "បរិយាយ*";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.Visible = true;
            this.colDescription.Width = 66;
            // 
            // colNote
            // 
            this.colNote.Caption = "សម្គាល់";
            this.colNote.FieldName = "Note";
            this.colNote.Name = "colNote";
            this.colNote.Visible = true;
            this.colNote.Width = 62;
            // 
            // colFormat
            // 
            this.colFormat.Caption = "Format";
            this.colFormat.FieldName = "Format";
            this.colFormat.Name = "colFormat";
            this.colFormat.Visible = true;
            // 
            // picconvertToBill
            // 
            this.picconvertToBill.Name = "picconvertToBill";
            // 
            // picDelivery
            // 
            this.picDelivery.Name = "picDelivery";
            // 
            // picPrint
            // 
            this.picPrint.Name = "picPrint";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.Controls.Add(this.chkSHOW_ID_IF_HAVE);
            this.panel1.Controls.Add(this.searchControl);
            this.panel1.Controls.Add(this.rdpDate);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1197, 37);
            this.panel1.TabIndex = 18;
            // 
            // chkSHOW_ID_IF_HAVE
            // 
            this.chkSHOW_ID_IF_HAVE.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.chkSHOW_ID_IF_HAVE.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.chkSHOW_ID_IF_HAVE.Location = new System.Drawing.Point(360, 7);
            this.chkSHOW_ID_IF_HAVE.Name = "chkSHOW_ID_IF_HAVE";
            this.chkSHOW_ID_IF_HAVE.Size = new System.Drawing.Size(191, 24);
            this.chkSHOW_ID_IF_HAVE.TabIndex = 399;
            this.chkSHOW_ID_IF_HAVE.Text = "បង្ហាញលេខអត្តសញ្ញាណ (បើមាន)";
            this.chkSHOW_ID_IF_HAVE.UseVisualStyleBackColor = true;
            this.chkSHOW_ID_IF_HAVE.CheckedChanged += new System.EventHandler(this._chkShowID_CheckedChanged);
            // 
            // searchControl
            // 
            this.searchControl.EditValue = "";
            this.searchControl.Location = new System.Drawing.Point(3, 4);
            this.searchControl.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.searchControl.Name = "searchControl";
            this.searchControl.Properties.Appearance.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.searchControl.Properties.Appearance.Options.UseFont = true;
            this.searchControl.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Repository.ClearButton(),
            new DevExpress.XtraEditors.Repository.SearchButton()});
            this.searchControl.Properties.DropDownRows = 10;
            this.searchControl.Properties.Padding = new System.Windows.Forms.Padding(1);
            this.searchControl.Size = new System.Drawing.Size(173, 28);
            this.searchControl.TabIndex = 35;
            // 
            // rdpDate
            // 
            this.rdpDate.AutoSave = true;
            this.rdpDate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.rdpDate.DateFilterTypes = EPower.Base.Helper.Component.ReportDatePicker.DateFilterType.Period;
            this.rdpDate.DefaultValue = EPower.Base.Helper.Component.ReportDatePicker.DateRanges.LastMonth;
            this.rdpDate.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.rdpDate.FromDate = new System.DateTime(2022, 6, 1, 0, 0, 0, 0);
            this.rdpDate.LastCustomDays = 30;
            this.rdpDate.LastFinancialYear = true;
            this.rdpDate.LastMonth = true;
            this.rdpDate.LastQuarter = true;
            this.rdpDate.LastWeek = true;
            this.rdpDate.Location = new System.Drawing.Point(181, 5);
            this.rdpDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rdpDate.Name = "rdpDate";
            this.rdpDate.Size = new System.Drawing.Size(173, 27);
            this.rdpDate.TabIndex = 19;
            this.rdpDate.ThisFinancialYear = true;
            this.rdpDate.ThisMonth = true;
            this.rdpDate.ThisQuarter = true;
            this.rdpDate.ThisWeek = true;
            this.rdpDate.ToDate = new System.DateTime(2022, 6, 30, 23, 59, 59, 0);
            this.rdpDate.Today = true;
            this.rdpDate.Value = EPower.Base.Helper.Component.ReportDatePicker.DateRanges.LastMonth;
            this.rdpDate.Yesterday = true;
            // 
            // popActions
            // 
            this.popActions.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnEFilingSale),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnCreditNote),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnPurchaseOverSea),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnIndividualVendor)});
            this.popActions.Manager = this.barManager1;
            this.popActions.Name = "popActions";
            // 
            // btnEFilingSale
            // 
            this.btnEFilingSale.Caption = "e-Filing (ការលក់)";
            this.btnEFilingSale.Id = 0;
            this.btnEFilingSale.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnEFilingSale.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnEFilingSale.ItemAppearance.Normal.Options.UseFont = true;
            this.btnEFilingSale.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnEFilingSale.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.btnEFilingSale.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.btnEFilingSale.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnEFilingSale.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.btnEFilingSale.Name = "btnEFilingSale";
            this.btnEFilingSale.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnEFillingSale_ItemClick);
            // 
            // btnCreditNote
            // 
            this.btnCreditNote.Caption = "វិក្កយបត្រឥណទាន";
            this.btnCreditNote.Id = 7;
            this.btnCreditNote.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnCreditNote.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnCreditNote.ItemAppearance.Normal.Options.UseFont = true;
            this.btnCreditNote.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnCreditNote.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.btnCreditNote.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.btnCreditNote.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnCreditNote.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.btnCreditNote.Name = "btnCreditNote";
            this.btnCreditNote.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCreditNote_ItemClick);
            // 
            // btnPurchaseOverSea
            // 
            this.btnPurchaseOverSea.Caption = "បញ្ជីក្រុមហ៊ុនក្រៅប្រទេស";
            this.btnPurchaseOverSea.Id = 1;
            this.btnPurchaseOverSea.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnPurchaseOverSea.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnPurchaseOverSea.ItemAppearance.Normal.Options.UseFont = true;
            this.btnPurchaseOverSea.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnPurchaseOverSea.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.btnPurchaseOverSea.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.btnPurchaseOverSea.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnPurchaseOverSea.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.btnPurchaseOverSea.Name = "btnPurchaseOverSea";
            this.btnPurchaseOverSea.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnPurchaseOverSea_ItemClick);
            // 
            // btnIndividualVendor
            // 
            this.btnIndividualVendor.Caption = "បញ្ជីបុគ្គលមិនជាប់អាករ";
            this.btnIndividualVendor.Id = 2;
            this.btnIndividualVendor.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnIndividualVendor.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnIndividualVendor.ItemAppearance.Normal.Options.UseFont = true;
            this.btnIndividualVendor.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnIndividualVendor.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.btnIndividualVendor.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.btnIndividualVendor.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnIndividualVendor.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.btnIndividualVendor.Name = "btnIndividualVendor";
            this.btnIndividualVendor.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnIndividualVendor_ItemClick);
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControl1);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnExcel,
            this.btnCsv,
            this.btnPdf,
            this.btnPrint,
            this.btnEFilingSale,
            this.btnPurchaseOverSea,
            this.btnIndividualVendor,
            this.btnExportEfilingSale,
            this.barButtonItem1,
            this.btnCreditNote});
            this.barManager1.MaxItemId = 8;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1197, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 581);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1197, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 581);
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl1.Location = new System.Drawing.Point(1197, 0);
            this.barDockControl1.Manager = this.barManager1;
            this.barDockControl1.Size = new System.Drawing.Size(0, 581);
            // 
            // btnExcel
            // 
            this.btnExcel.Caption = "Excel";
            this.btnExcel.Id = 3;
            this.btnExcel.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnExcel.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnExcel.ItemAppearance.Normal.Options.UseFont = true;
            this.btnExcel.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnExcel.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.btnExcel.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.btnExcel.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnExcel.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.btnExcel.Name = "btnExcel";
            this.btnExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExcel_ItemClick);
            // 
            // btnCsv
            // 
            this.btnCsv.Caption = "Csv";
            this.btnCsv.Id = 5;
            this.btnCsv.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnCsv.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnCsv.ItemAppearance.Normal.Options.UseFont = true;
            this.btnCsv.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnCsv.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.btnCsv.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.btnCsv.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnCsv.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.btnCsv.Name = "btnCsv";
            this.btnCsv.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCsv_ItemClick);
            // 
            // btnPdf
            // 
            this.btnPdf.Caption = "Pdf";
            this.btnPdf.Id = 2;
            this.btnPdf.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnPdf.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnPdf.ItemAppearance.Normal.Options.UseFont = true;
            this.btnPdf.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnPdf.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.btnPdf.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.btnPdf.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnPdf.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.btnPdf.Name = "btnPdf";
            this.btnPdf.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnPdf_ItemClick);
            // 
            // btnPrint
            // 
            this.btnPrint.Caption = "បោះពុម្ភ";
            this.btnPrint.Id = 4;
            this.btnPrint.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnPrint.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnPrint.ItemAppearance.Normal.Options.UseFont = true;
            this.btnPrint.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnPrint.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.btnPrint.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.btnPrint.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnPrint.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnPrint_ItemClick);
            // 
            // btnExportEfilingSale
            // 
            this.btnExportEfilingSale.Caption = "ទាញយក e-Filing(ការលក់)";
            this.btnExportEfilingSale.Id = 4;
            this.btnExportEfilingSale.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnExportEfilingSale.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnExportEfilingSale.ItemAppearance.Normal.Options.UseFont = true;
            this.btnExportEfilingSale.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnExportEfilingSale.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.btnExportEfilingSale.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.btnExportEfilingSale.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnExportEfilingSale.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.btnExportEfilingSale.Name = "btnExportEfilingSale";
            this.btnExportEfilingSale.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExcelAll_ItemClick);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Id = 6;
            this.barButtonItem1.ItemAppearance.Disabled.Options.UseFont = true;
            this.barButtonItem1.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem1.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem1.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem1.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.barButtonItem1.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem1.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.barButtonItem1.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // popupExport
            // 
            this.popupExport.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportEfilingSale, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExcel),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnCsv),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnPdf),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnPrint, true)});
            this.popupExport.Manager = this.barManager1;
            this.popupExport.Name = "popupExport";
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Font = new System.Drawing.Font("Khmer Kep", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand1.AppearanceHeader.Options.UseFont = true;
            this.gridBand1.Caption = "ល.រ*";
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = -1;
            this.gridBand1.Width = 45;
            // 
            // gridBand4
            // 
            this.gridBand4.AppearanceHeader.Font = new System.Drawing.Font("Khmer Kep", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand4.AppearanceHeader.Options.UseFont = true;
            this.gridBand4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand4.Caption = "កាលបរិច្ឆេទ*";
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = -1;
            this.gridBand4.Width = 82;
            // 
            // gridBand5
            // 
            this.gridBand5.AppearanceHeader.Font = new System.Drawing.Font("Khmer Kep", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand5.AppearanceHeader.Options.UseFont = true;
            this.gridBand5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand5.Caption = "លេខវិក្កយបត្រ​ ឬ ប្រតិវេទន៍គយ*";
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.VisibleIndex = -1;
            this.gridBand5.Width = 148;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Font = new System.Drawing.Font("Khmer Kep", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand2.AppearanceHeader.Options.UseFont = true;
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "អ្នកផ្គត់ផ្គង់\t\t\t\t";
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = -1;
            this.gridBand2.Width = 419;
            // 
            // gridBand8
            // 
            this.gridBand8.AppearanceHeader.Font = new System.Drawing.Font("Khmer Kep", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand8.AppearanceHeader.Options.UseFont = true;
            this.gridBand8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand8.Caption = "ប្រភេទ*";
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.VisibleIndex = -1;
            this.gridBand8.Width = 96;
            // 
            // gridBand9
            // 
            this.gridBand9.AppearanceHeader.Font = new System.Drawing.Font("Khmer Kep", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand9.AppearanceHeader.Options.UseFont = true;
            this.gridBand9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand9.Caption = "លេខសម្គាល់*";
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.VisibleIndex = -1;
            this.gridBand9.Width = 111;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this._lblSubTitle1);
            this.panel2.Controls.Add(this._lblTitle);
            this.panel2.Controls.Add(this._lblAddress);
            this.panel2.Controls.Add(this._lblCompany);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 37);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.panel2.Size = new System.Drawing.Size(1197, 141);
            this.panel2.TabIndex = 219;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Kh Siemreap", 9F);
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(117, 114);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.label1.Size = new System.Drawing.Size(1080, 27);
            this.label1.TabIndex = 230;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _lblSubTitle1
            // 
            this._lblSubTitle1.BackColor = System.Drawing.Color.White;
            this._lblSubTitle1.Cursor = System.Windows.Forms.Cursors.Hand;
            this._lblSubTitle1.Dock = System.Windows.Forms.DockStyle.Top;
            this._lblSubTitle1.Font = new System.Drawing.Font("Kh Siemreap", 9F);
            this._lblSubTitle1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this._lblSubTitle1.Location = new System.Drawing.Point(117, 87);
            this._lblSubTitle1.Name = "_lblSubTitle1";
            this._lblSubTitle1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this._lblSubTitle1.Size = new System.Drawing.Size(1080, 27);
            this._lblSubTitle1.TabIndex = 224;
            this._lblSubTitle1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _lblTitle
            // 
            this._lblTitle.BackColor = System.Drawing.Color.White;
            this._lblTitle.Cursor = System.Windows.Forms.Cursors.Hand;
            this._lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this._lblTitle.Font = new System.Drawing.Font("Kh Muol", 9F);
            this._lblTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this._lblTitle.Location = new System.Drawing.Point(117, 60);
            this._lblTitle.Name = "_lblTitle";
            this._lblTitle.Size = new System.Drawing.Size(1080, 27);
            this._lblTitle.TabIndex = 223;
            this._lblTitle.Text = "e-Filing(ការលក់)";
            this._lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _lblAddress
            // 
            this._lblAddress.BackColor = System.Drawing.Color.White;
            this._lblAddress.Cursor = System.Windows.Forms.Cursors.Hand;
            this._lblAddress.Dock = System.Windows.Forms.DockStyle.Top;
            this._lblAddress.Font = new System.Drawing.Font("Kh Siemreap", 9.75F);
            this._lblAddress.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this._lblAddress.Location = new System.Drawing.Point(117, 30);
            this._lblAddress.Name = "_lblAddress";
            this._lblAddress.Size = new System.Drawing.Size(1080, 30);
            this._lblAddress.TabIndex = 222;
            this._lblAddress.Text = "អាស័យដ្ឋានក្រុមហ៊ុន";
            this._lblAddress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _lblCompany
            // 
            this._lblCompany.BackColor = System.Drawing.Color.White;
            this._lblCompany.Cursor = System.Windows.Forms.Cursors.Hand;
            this._lblCompany.Dock = System.Windows.Forms.DockStyle.Top;
            this._lblCompany.Font = new System.Drawing.Font("Kh Muol", 11.25F);
            this._lblCompany.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this._lblCompany.Location = new System.Drawing.Point(117, 0);
            this._lblCompany.Name = "_lblCompany";
            this._lblCompany.Size = new System.Drawing.Size(1080, 30);
            this._lblCompany.TabIndex = 221;
            this._lblCompany.Text = "ឈ្មោះក្រុមហ៊ុន";
            this._lblCompany.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel4
            // 
            this.panel4.AutoSize = true;
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(1197, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(0, 138);
            this.panel4.TabIndex = 220;
            // 
            // panel3
            // 
            this.panel3.AutoSize = true;
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this._picLogo);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(117, 138);
            this.panel3.TabIndex = 219;
            // 
            // _picLogo
            // 
            this._picLogo.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this._picLogo.Location = new System.Drawing.Point(3, 13);
            this._picLogo.Name = "_picLogo";
            this._picLogo.Size = new System.Drawing.Size(111, 111);
            this._picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this._picLogo.TabIndex = 0;
            this._picLogo.TabStop = false;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel3.BackColor = System.Drawing.Color.Transparent;
            this.flowLayoutPanel3.Controls.Add(this.dpExport);
            this.flowLayoutPanel3.Controls.Add(this.dpAction);
            this.flowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(1093, 187);
            this.flowLayoutPanel3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(101, 32);
            this.flowLayoutPanel3.TabIndex = 234;
            // 
            // dpExport
            // 
            this.dpExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dpExport.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.dpExport.Appearance.BorderColor = System.Drawing.Color.Transparent;
            this.dpExport.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dpExport.Appearance.Options.UseBackColor = true;
            this.dpExport.Appearance.Options.UseBorderColor = true;
            this.dpExport.Appearance.Options.UseFont = true;
            this.dpExport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dpExport.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            this.dpExport.DropDownControl = this.popupExport;
            this.dpExport.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("dpExport.ImageOptions.Image")));
            this.dpExport.ImageOptions.ImageUri.Uri = "SendCSV;Size32x32;GrayScaled";
            this.dpExport.ImageOptions.SvgImageSize = new System.Drawing.Size(10, 10);
            this.dpExport.Location = new System.Drawing.Point(57, 1);
            this.dpExport.LookAndFeel.SkinMaskColor = System.Drawing.Color.Transparent;
            this.dpExport.LookAndFeel.SkinMaskColor2 = System.Drawing.Color.Transparent;
            this.dpExport.LookAndFeel.UseDefaultLookAndFeel = false;
            this.dpExport.Margin = new System.Windows.Forms.Padding(1);
            this.dpExport.Name = "dpExport";
            this.dpExport.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.dpExport.Size = new System.Drawing.Size(43, 26);
            this.dpExport.TabIndex = 19;
            // 
            // dpAction
            // 
            this.dpAction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dpAction.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.dpAction.Appearance.BorderColor = System.Drawing.Color.Transparent;
            this.dpAction.Appearance.Font = new System.Drawing.Font("Khmer Kep", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dpAction.Appearance.Options.UseBackColor = true;
            this.dpAction.Appearance.Options.UseBorderColor = true;
            this.dpAction.Appearance.Options.UseFont = true;
            this.dpAction.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dpAction.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            this.dpAction.DropDownControl = this.popActions;
            this.dpAction.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("dpAction.ImageOptions.Image")));
            this.dpAction.ImageOptions.ImageUri.Uri = "WrapText;Size32x32;GrayScaled";
            this.dpAction.ImageOptions.SvgImageColorizationMode = DevExpress.Utils.SvgImageColorizationMode.Full;
            this.dpAction.Location = new System.Drawing.Point(19, 1);
            this.dpAction.LookAndFeel.SkinMaskColor = System.Drawing.Color.Transparent;
            this.dpAction.LookAndFeel.SkinMaskColor2 = System.Drawing.Color.Transparent;
            this.dpAction.LookAndFeel.UseDefaultLookAndFeel = false;
            this.dpAction.Margin = new System.Windows.Forms.Padding(1);
            this.dpAction.Name = "dpAction";
            this.dpAction.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.dpAction.Size = new System.Drawing.Size(36, 26);
            this.dpAction.TabIndex = 20;
            this.dpAction.Text = "x";
            // 
            // gbRowNo
            // 
            this.gbRowNo.Caption = "ល.រ*";
            this.gbRowNo.Columns.Add(this.colRowNo);
            this.gbRowNo.Name = "gbRowNo";
            this.gbRowNo.OptionsBand.AllowMove = false;
            this.gbRowNo.OptionsBand.FixedWidth = true;
            this.gbRowNo.VisibleIndex = 0;
            this.gbRowNo.Width = 61;
            // 
            // gbBillDate
            // 
            this.gbBillDate.Caption = "កាលបរិច្ឆេទ*";
            this.gbBillDate.Columns.Add(this.colTransDate);
            this.gbBillDate.Name = "gbBillDate";
            this.gbBillDate.VisibleIndex = 1;
            this.gbBillDate.Width = 92;
            // 
            // gbInvoiceNo
            // 
            this.gbInvoiceNo.Caption = "លេខវិក្កយបត្រ ឬ ប្រតិវេទន៍គយ*";
            this.gbInvoiceNo.Columns.Add(this.colInvoiceVendor);
            this.gbInvoiceNo.MinWidth = 70;
            this.gbInvoiceNo.Name = "gbInvoiceNo";
            this.gbInvoiceNo.VisibleIndex = 2;
            this.gbInvoiceNo.Width = 111;
            // 
            // gbVendor
            // 
            this.gbVendor.AppearanceHeader.Options.UseTextOptions = true;
            this.gbVendor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gbVendor.Caption = "អ្នកទិញ";
            this.gbVendor.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gbType,
            this.gridBand12,
            this.gridBand13,
            this.gridBand11});
            this.gbVendor.Name = "gbVendor";
            this.gbVendor.VisibleIndex = 3;
            this.gbVendor.Width = 336;
            // 
            // gbType
            // 
            this.gbType.Caption = "ប្រភេទ*";
            this.gbType.Columns.Add(this.colPartnerType);
            this.gbType.Name = "gbType";
            this.gbType.VisibleIndex = 0;
            this.gbType.Width = 55;
            // 
            // gridBand12
            // 
            this.gridBand12.Caption = "លេខសម្គាល់*";
            this.gridBand12.Columns.Add(this.colTIN);
            this.gridBand12.Name = "gridBand12";
            this.gridBand12.VisibleIndex = 1;
            this.gridBand12.Width = 80;
            // 
            // gridBand13
            // 
            this.gridBand13.Caption = "ឈ្មោះ(ខ្មែរ)";
            this.gridBand13.Columns.Add(this.colKhName);
            this.gridBand13.Name = "gridBand13";
            this.gridBand13.VisibleIndex = 2;
            this.gridBand13.Width = 75;
            // 
            // gridBand11
            // 
            this.gridBand11.Caption = "ឈ្មោះ(ឡាតាំង)";
            this.gridBand11.Columns.Add(this.colNameLatin);
            this.gridBand11.Name = "gridBand11";
            this.gridBand11.VisibleIndex = 3;
            this.gridBand11.Width = 126;
            // 
            // gvPartnerType
            // 
            this.gvPartnerType.Caption = "ប្រភេទផ្គត់ផ្គង់ទំនិញ*";
            this.gvPartnerType.Columns.Add(this.colTaxCode);
            this.gvPartnerType.Name = "gvPartnerType";
            this.gvPartnerType.VisibleIndex = 4;
            this.gvPartnerType.Width = 137;
            // 
            // gbTotalTax
            // 
            this.gbTotalTax.Caption = "តម្លៃសរុបជាប់ អតប*";
            this.gbTotalTax.Columns.Add(this.colTaxAmount);
            this.gbTotalTax.Name = "gbTotalTax";
            this.gbTotalTax.VisibleIndex = 5;
            this.gbTotalTax.Width = 127;
            // 
            // gbTotalNonTax
            // 
            this.gbTotalNonTax.Caption = "តម្លៃសរុបមិនជាប់ អតប ឬ អតប អត្រា ០%";
            this.gbTotalNonTax.Columns.Add(this.colNoneTaxAmount);
            this.gbTotalNonTax.Name = "gbTotalNonTax";
            this.gbTotalNonTax.VisibleIndex = 6;
            this.gbTotalNonTax.Width = 121;
            // 
            // gbSpcecialTax
            // 
            this.gbSpcecialTax.Caption = "អាករពិសេសលើទំនិញមួយចំនួន";
            this.gbSpcecialTax.Columns.Add(this.colSpecialTax);
            this.gbSpcecialTax.Name = "gbSpcecialTax";
            this.gbSpcecialTax.VisibleIndex = 7;
            this.gbSpcecialTax.Width = 139;
            // 
            // gridBand6
            // 
            this.gridBand6.Caption = "អាករពិសេសលើសេវាមួយចំនួន";
            this.gridBand6.Columns.Add(this.colSpecialTaxonService);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = 8;
            this.gridBand6.Width = 139;
            // 
            // gbPublicTax
            // 
            this.gbPublicTax.Caption = "អាករបំភ្លឺសាធារណៈ";
            this.gbPublicTax.Columns.Add(this.colPublicTax);
            this.gbPublicTax.Name = "gbPublicTax";
            this.gbPublicTax.VisibleIndex = 9;
            this.gbPublicTax.Width = 149;
            // 
            // gbOfficeTax
            // 
            this.gbOfficeTax.Caption = "អាករលើការស្នាក់នៅ";
            this.gbOfficeTax.Columns.Add(this.colOfficeTax);
            this.gbOfficeTax.Name = "gbOfficeTax";
            this.gbOfficeTax.VisibleIndex = 10;
            this.gbOfficeTax.Width = 132;
            // 
            // IncomeTaxRate
            // 
            this.IncomeTaxRate.Caption = "អត្រាប្រាក់រំដោះពន្ធលើប្រាក់ចំណូល";
            this.IncomeTaxRate.Columns.Add(this.colIncomeTaxRate);
            this.IncomeTaxRate.Name = "IncomeTaxRate";
            this.IncomeTaxRate.VisibleIndex = 11;
            this.IncomeTaxRate.Width = 231;
            // 
            // gbSector
            // 
            this.gbSector.Caption = "វិស័យ";
            this.gbSector.Columns.Add(this.colSector);
            this.gbSector.Name = "gbSector";
            this.gbSector.VisibleIndex = 12;
            this.gbSector.Width = 66;
            // 
            // gridBand3
            // 
            this.gridBand3.Caption = "លេខឥណទានរតនាគារជាតិ";
            this.gridBand3.Columns.Add(this.colTresuryCreditNumber);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 13;
            this.gridBand3.Width = 75;
            // 
            // gbDescription
            // 
            this.gbDescription.Caption = "បរិយាយ*";
            this.gbDescription.Columns.Add(this.colDescription);
            this.gbDescription.Name = "gbDescription";
            this.gbDescription.VisibleIndex = 14;
            this.gbDescription.Width = 66;
            // 
            // gbNote
            // 
            this.gbNote.Caption = "សម្គាល់";
            this.gbNote.Columns.Add(this.colNote);
            this.gbNote.Name = "gbNote";
            this.gbNote.VisibleIndex = 15;
            this.gbNote.Width = 62;
            // 
            // EFillingSale
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1197, 581);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControl1);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Name = "EFillingSale";
            this.Text = "PurchaseJournalsPage";
            ((System.ComponentModel.ISupportInitialize)(this.dgvOverseaCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInvidualCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEFilingSale)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picconvertToBill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDelivery)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPrint)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.searchControl.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popActions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupExport)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._picLogo)).EndInit();
            this.flowLayoutPanel3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.SearchControl searchControl;
        private ReportDatePicker rdpDate;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarButtonItem btnExcel;
        private DevExpress.XtraBars.BarButtonItem btnCsv;
        private DevExpress.XtraBars.BarButtonItem btnPdf;
        private DevExpress.XtraBars.BarButtonItem btnPrint;
        private DevExpress.XtraBars.PopupMenu popupExport;
        private DevExpress.XtraBars.PopupMenu popActions;
        private DevExpress.XtraBars.BarButtonItem btnEFilingSale;
        private DevExpress.XtraBars.BarButtonItem btnPurchaseOverSea;
        private DevExpress.XtraBars.BarButtonItem btnIndividualVendor;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
        private DevExpress.XtraBars.BarButtonItem btnExportEfilingSale;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem btnCreditNote;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label _lblSubTitle1;
        private System.Windows.Forms.Label _lblTitle;
        private System.Windows.Forms.Label _lblAddress;
        private System.Windows.Forms.Label _lblCompany;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private DevExpress.XtraGrid.GridControl dgv;
        private DevExpress.XtraGrid.Views.Grid.GridView dgvOverseaCustomer;
        private DevExpress.XtraGrid.Columns.GridColumn colId_1;
        private DevExpress.XtraGrid.Columns.GridColumn colTinOverSea;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyNameKh;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyNameLatin;
        private DevExpress.XtraGrid.Columns.GridColumn colCountryCode;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyPhone;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyMail;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colRowNoOc;
        private DevExpress.XtraGrid.Columns.GridColumn colFormat_2;
        private DevExpress.XtraGrid.Views.Grid.GridView dgvInvidualCustomer;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colRowNoIc;
        private DevExpress.XtraGrid.Columns.GridColumn colTins;
        private DevExpress.XtraGrid.Columns.GridColumn colNameKH;
        private DevExpress.XtraGrid.Columns.GridColumn colNameLatins;
        private DevExpress.XtraGrid.Columns.GridColumn colFormat_1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView dgvEFilingSale;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colRowNo;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTransDate;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colInvoiceVendor;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPartnerType;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTIN;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colKhName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colNameLatin;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTaxCode;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTaxAmount;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colNoneTaxAmount;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSpecialTax;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPublicTax;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colOfficeTax;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colIncomeTaxRate;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSector;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDescription;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colNote;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colFormat;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit picconvertToBill;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit picDelivery;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit picPrint;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private DevExpress.XtraEditors.DropDownButton dpExport;
        private DevExpress.XtraEditors.DropDownButton dpAction;
        private System.Windows.Forms.CheckBox chkSHOW_ID_IF_HAVE;
        private System.Windows.Forms.PictureBox _picLogo;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTresuryCreditNumber;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSpecialTaxonService;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gbRowNo;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gbBillDate;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gbInvoiceNo;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gbVendor;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gbType;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand12;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand13;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand11;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gvPartnerType;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gbTotalTax;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gbTotalNonTax;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gbSpcecialTax;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gbPublicTax;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gbOfficeTax;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand IncomeTaxRate;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gbSector;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gbDescription;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gbNote;
    }
}