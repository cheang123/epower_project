﻿using EPower.Base.Helper;
using EPower.Base.Logic;
using EPower.Properties;
using HB01.Domain.Enums;
using HB01.Domain.ListModels;
using HB01.Helpers.DevExpressCustomize;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogReportTrialBalanceARDetailOption : ExDialog
    {
        private int _service_typeId = 0;
        public List<int> _ignoreIds = new List<int>();
        private int _last_service_type_id;
        private int _last_service_id;
        private int _last_account_id;

        public DialogReportTrialBalanceARDetailOption()
        {
            InitializeComponent();
            Bind();
        }

        protected override void OnClosed(EventArgs e)
        {
            _last_service_id = (int)cboItem.SelectedValue;
            _last_service_type_id = (int)cboItemType.SelectedValue;
            _last_account_id = (int)cboPaymentAccount.EditValue;

            base.OnClosed(e);

        }

        #region Method 
        public void Bind()
        {
            UIHelper.SetDataSourceToComboBox(this.cboItemType, DBDataContext.Db.TBL_INVOICE_ITEM_TYPEs.Where(x => x.IS_ACTIVE).OrderBy(x => x.INVOICE_ITEM_TYPE_ID).ToList(), Resources.ALL_SERVICE_TYPE);
            var sources = DBDataContext.Db.TBL_INVOICE_ITEMs.OrderBy(x => x.INVOICE_ITEM_TYPE_ID).ThenBy(x => x.INVOICE_ITEM_ID).Where(x => x.IS_ACTIVE && !_ignoreIds.Contains(x.INVOICE_ITEM_ID) && (x.INVOICE_ITEM_TYPE_ID == _last_service_type_id || _last_service_type_id == 0)).Select(x => new { x.INVOICE_ITEM_ID, x.INVOICE_ITEM_NAME });
            UIHelper.SetDataSourceToComboBox(cboItem, sources, Resources.ALL_SERVICE);
            UIHelper.SetDataSourceToComboBox(this.cboBillingCycle, Lookup.GetBillingCycles(), Resources.ALL_CYCLE);
            UIHelper.SetDataSourceToComboBox(cboCurrency, Lookup.GetCurrencies(), Resources.ALL_CURRENCY);
            UIHelper.SetDataSourceToComboBox(cboArea, Lookup.GetAreas(), Resources.ALL_AREA);
            UIHelper.SetDataSourceToComboBox(cboCashDrawer, Lookup.GetCashDrawer(), Resources.ALL_CASH_DRAWER);
            cboOrderBy.SelectedIndex = 0;
            // Account 
            List<AccountListModel> pAcc = new List<AccountListModel>();
            pAcc.Add(new AccountListModel { Id = 0, AccountName = Resources.ALL_ACCOUNT, DisplayAccountName= Resources.ALL_ACCOUNT });
            pAcc.AddRange(AccountChartHelper.GetAccounts(new AccountSearchParam { Natures = new List<AccountNatures> { AccountNatures.Asset } }));
            this.cboPaymentAccount.SetDatasourceList(pAcc, nameof(AccountListModel.DisplayAccountName));
        }
        #endregion

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }


        private void cboItemType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dtpMonth_MouseDown(object sender, MouseEventArgs e)
        {
            if (dtpMonth.Checked)
            {
                dtpMonth.SetValue(new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
                dtpMonth.CustomFormat = "MM-yyyy";
            }
        }

        private void InputEnglish(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void cboItemType_SelectedValueChanged(object sender, EventArgs e)
        {
            int typeId = cboItemType.SelectedIndex == -1 ? 0 : (int)cboItemType.SelectedValue;
            if (typeId == _service_typeId)
            {
                return;
            }
            UIHelper.SetDataSourceToComboBox(cboItem, DBDataContext.Db.TBL_INVOICE_ITEMs.OrderBy(x => x.INVOICE_ITEM_TYPE_ID).ThenBy(x => x.INVOICE_ITEM_ID).Where(x => !_ignoreIds.Contains(x.INVOICE_ITEM_ID) && (x.INVOICE_ITEM_TYPE_ID == typeId || typeId == 0) && x.IS_ACTIVE).Select(x => new { x.INVOICE_ITEM_ID, x.INVOICE_ITEM_NAME }), Resources.ALL_SERVICE);
            cboItem.SelectedValue = 0;
            _service_typeId = typeId;
        }
        private void DialogReportTrialBalanceARDetailOption1_Load(object sender, EventArgs e)
        {
            Bind();
            cboItem.SelectedValue = _last_service_id;
            cboItemType.SelectedValue = _last_service_type_id;
            cboPaymentAccount.EditValue = _last_account_id;
        }
    }
}