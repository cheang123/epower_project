﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.IO;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageReportCashDailySummary : Form
    {
        private CrystalReportHelper ch = null;

        public PageReportCashDailySummary()
        {
            InitializeComponent();
            viewer.DefaultView();
            UIHelper.SetDataSourceToComboBox(cboCurrency, Lookup.GetCurrencies(), Resources.ALL_CURRENCY);

        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            Runner.Run(this.viewReport);
        }

        public CrystalReportHelper ViewReportCashDailySummary(DateTime dt,int currencyId,string currencyName)
        {
            CrystalReportHelper c = new CrystalReportHelper("ReportCashDailySummary.rpt");
            c.SetParameter("@DATE", dt.Date);
            c.SetParameter("@CURRENCY_ID", currencyId);
            c.SetParameter("@CURRENCY_NAME", currencyName);
            return c;
        }

        private void viewReport()
        {
            try
            {
                this.ch = ViewReportCashDailySummary(dtpDate.Value.Date, (int)cboCurrency.SelectedValue, cboCurrency.Text);
                this.viewer.ReportSource = ch.Report;
            }
            catch(Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_VIEW_REPORT, Base.Properties.Resources.WARNING);
            }
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            sendMail();
        }

        private void sendMail()
        {
            try
            {
                this.viewReport();
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + this.ch.ReportName + ".xls").FullName;
                this.ch.ExportToExcel(exportPath);
                DialogSendMail.Instance.Add(this.ch.ReportName, exportPath);
                DialogSendMail.Instance.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_SEND_EMAIL, Base.Properties.Resources.WARNING);
            }
        }
    }
}
