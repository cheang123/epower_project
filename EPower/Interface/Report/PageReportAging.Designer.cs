﻿using System.ComponentModel;
using System.Windows.Forms;
using AxCrystalActiveXReportViewerLib105;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageReportAging
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageReportAging));
            this.panel1 = new System.Windows.Forms.Panel();
            this.cboReport = new System.Windows.Forms.ComboBox();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.btnMail = new SoftTech.Component.ExButton();
            this.lblDAY = new System.Windows.Forms.Label();
            this.btnViewReport = new SoftTech.Component.ExButton();
            this.nudDays = new System.Windows.Forms.NumericUpDown();
            this.btnSetup = new SoftTech.Component.ExButton();
            this.viewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDays)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.cboReport);
            this.panel1.Controls.Add(this.dtpDate);
            this.panel1.Controls.Add(this.btnMail);
            this.panel1.Controls.Add(this.lblDAY);
            this.panel1.Controls.Add(this.btnViewReport);
            this.panel1.Controls.Add(this.nudDays);
            this.panel1.Controls.Add(this.btnSetup);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // cboReport
            // 
            this.cboReport.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboReport.FormattingEnabled = true;
            resources.ApplyResources(this.cboReport, "cboReport");
            this.cboReport.Name = "cboReport";
            this.cboReport.SelectedIndexChanged += new System.EventHandler(this.cboReport_SelectedIndexChanged);
            // 
            // dtpDate
            // 
            resources.ApplyResources(this.dtpDate, "dtpDate");
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Name = "dtpDate";
            // 
            // btnMail
            // 
            resources.ApplyResources(this.btnMail, "btnMail");
            this.btnMail.Name = "btnMail";
            this.btnMail.UseVisualStyleBackColor = true;
            this.btnMail.Click += new System.EventHandler(this.btnMail_Click);
            // 
            // lblDAY
            // 
            resources.ApplyResources(this.lblDAY, "lblDAY");
            this.lblDAY.Name = "lblDAY";
            // 
            // btnViewReport
            // 
            resources.ApplyResources(this.btnViewReport, "btnViewReport");
            this.btnViewReport.Name = "btnViewReport";
            this.btnViewReport.UseVisualStyleBackColor = true;
            this.btnViewReport.Click += new System.EventHandler(this.btnViewReport_Click);
            // 
            // nudDays
            // 
            resources.ApplyResources(this.nudDays, "nudDays");
            this.nudDays.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudDays.Name = "nudDays";
            this.nudDays.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // btnSetup
            // 
            resources.ApplyResources(this.btnSetup, "btnSetup");
            this.btnSetup.Name = "btnSetup";
            this.btnSetup.UseVisualStyleBackColor = true;
            this.btnSetup.Click += new System.EventHandler(this.btnSetup_Click);
            // 
            // viewer
            // 
            this.viewer.ActiveViewIndex = -1;
            this.viewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.viewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.viewer.DisplayStatusBar = false;
            resources.ApplyResources(this.viewer, "viewer");
            this.viewer.Name = "viewer";
            // 
            // PageReportAging
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.viewer);
            this.Controls.Add(this.panel1);
            this.Name = "PageReportAging";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nudDays)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnViewReport;
        private Label lblDAY;
        private NumericUpDown nudDays;
        private ExButton btnMail;
        private DateTimePicker dtpDate;
        private ComboBox cboReport;
        private ExButton btnSetup;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer viewer;
    }
}
