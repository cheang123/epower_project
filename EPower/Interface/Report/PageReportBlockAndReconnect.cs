﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.IO;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageReportBlockAndReconnect : Form
    {
        CrystalReportHelper ch = null;
        public PageReportBlockAndReconnect()
        {
            InitializeComponent();
            viewer.DefaultView();
            dtStart.Value = new DateTime(dtEnd.Value.Year,dtEnd.Value.Month,1);
            UIHelper.SetDataSourceToComboBox(cboCurrency, Lookup.GetCurrencies(), Resources.ALL_CURRENCY);

        } 

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            Runner.Run(this.viewReport);
        }

        public CrystalReportHelper ViewReportBlockAndReconnect(DateTime dtStart,DateTime dtEnd,int currencyId,string currencyName)
        {
            CrystalReportHelper c = new CrystalReportHelper("ReportBlockAndReconnect.rpt");
            c.SetParameter("@D1", dtStart.Date);
            c.SetParameter("@D2", dtEnd.Date);
            c.SetParameter("@CURRENCY_ID", currencyId);
            c.SetParameter("@CURRENCY_NAME", currencyName);
            return c;
        }

        private void viewReport()
        {
            try
            {
                this.ch = ViewReportBlockAndReconnect(dtStart.Value, dtEnd.Value, (int)cboCurrency.SelectedValue, cboCurrency.Text);
                this.viewer.ReportSource = ch.Report;
            }
            catch(Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_VIEW_REPORT, Base.Properties.Resources.WARNING);
            }
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            this.sendMail();
        }

        private void sendMail()
        {
            try
            {
                this.viewReport();
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + this.ch.ReportName + ".xls").FullName;
                this.ch.ExportToExcel(exportPath);
                DialogSendMail.Instance.Add(this.ch.ReportName, exportPath);
                DialogSendMail.Instance.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_SEND_EMAIL, Base.Properties.Resources.WARNING);
            }
        }
    }
}
