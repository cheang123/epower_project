﻿using System;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;

namespace EPower.Interface
{
    public partial class DialogBoxType : ExDialog
    {
        GeneralProcess _flag;
        TLKP_BOX_TYPE _objBoxType = new TLKP_BOX_TYPE();
        public TLKP_BOX_TYPE BoxType
        {
            get { return _objBoxType; }
        }
        TLKP_BOX_TYPE _oldObjBoxType = new TLKP_BOX_TYPE();

        #region Constructor
        public DialogBoxType(GeneralProcess flag, TLKP_BOX_TYPE objBoxType)
        {
            InitializeComponent();

            _flag = flag;
            objBoxType._CopyTo(_objBoxType);
            objBoxType._CopyTo(_oldObjBoxType);
            read();

            this.Text = flag.GetText(this.Text);
            UIHelper.SetEnabled(this, this._flag != GeneralProcess.Delete);
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
        }

        #endregion

        #region Method

        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();

            if (txtBoxType.Text.Trim().Length <= 0)
            {
                txtBoxType.SetValidation(string.Format(Resources.REQUIRED, lblBOX_TYPE.Text));
                val = true;
            }
            return val;
        }

        private void read()
        {
            txtBoxType.Text = _objBoxType.BOX_TYPE_NAME;
        }

        private void write()
        {
            _objBoxType.BOX_TYPE_NAME = txtBoxType.Text.Trim();            
        }
        #endregion

        #region Operation
        
        private void ChangeKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
             //If record is duplicate.
            txtBoxType.ClearValidation();
            if (DBDataContext.Db.IsExits(_objBoxType, "BOX_TYPE_NAME"))
            {
                txtBoxType.SetValidation(string.Format( Resources.MS_IS_EXISTS,lblBOX_TYPE.Text));
                return;
            }

            try
            {
                using (TransactionScope tran=new TransactionScope( TransactionScopeOption.Required,TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objBoxType);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_oldObjBoxType, _objBoxType);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objBoxType);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }                                               
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }            
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objBoxType);
        }
 
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
        
        #endregion
    }
}