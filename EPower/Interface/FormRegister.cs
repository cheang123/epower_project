﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using SoftTech;

namespace EPower.Interface
{
    public partial class FormRegister : Form
    {
        DBDataContext db = null;

        public FormRegister()
        {
            InitializeComponent();

            this.groupIRReader.Enabled = false;
            this.groupLicense.Enabled = false;
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection con = new SqlConnection(this.txtConnection.Text);
                con.Open();
                this.txtEncryptedConnection.Text = Cryption.Cryption.EncryptString(this.txtConnection.Text);
                this.db = new DBDataContext(this.txtConnection.Text);

                this.groupLicense.Enabled = true;
                this.groupIRReader.Enabled = true;
                MessageBox.Show("Connect success!");
            }
            catch (Exception ex)
            {
                this.groupLicense.Enabled = false;
                this.groupIRReader.Enabled = false;
                MessageBox.Show(ex.Message);
            }
        }

        private void btnApply_Click(object sender, EventArgs e)
        { 
            TBL_CONFIG obj = this.db.TBL_CONFIGs.FirstOrDefault();

            obj.ACTIVATE_DATE = this.txtActivate.Value.Date;
            obj.TOTAL_CUTOMER=int.Parse(this.txtCustomers.Text);
            obj.TOTAL_MONTH= int.Parse(this.txtMonths.Text);
            obj.AREA_CODE = this.txtPrepaidCode.Text;

            string tmp=obj.ACTIVATE_DATE.ToString("yyyyMMdd") +
                    obj.AREA_CODE +
                    obj.TOTAL_CUTOMER +
                    obj.TOTAL_MONTH;

            obj.PROCESSOR_ID  =  GetRecordChecksum(tmp, GetProcessorId());

            this.db.ExecuteCommand(string.Format(@"
UPDATE TBL_CONFIG
SET TOTAL_CUTOMER={0},
    TOTAL_MONTH={1},
    ACTIVATE_DATE='{2}',
    PROCESSOR_ID='{3}',
AREA_CODE='{4}';
", obj.TOTAL_CUTOMER, obj.TOTAL_MONTH, obj.ACTIVATE_DATE.ToString("yyyy-MM-dd"), obj.PROCESSOR_ID, obj.AREA_CODE));



            this.txtProcessorCode.Text = obj.PROCESSOR_ID;
            MessageBox.Show("Register Success!");
        }

        /// <summary>
        /// Get Record Check Sum
        /// </summary>
        /// <param name="strFullFilename"></param>
        /// <returns></returns>
        internal   string GetRecordChecksum(string strStringValue, string strCheck)
        {
            //Add more salt to encrypt
            strStringValue = strStringValue + "2&mp@$3Gh&a5&^$" + strCheck;
            MD5 md5 = new MD5CryptoServiceProvider(); // MD5 algorithm
            byte[] checksum = md5.ComputeHash(new UnicodeEncoding().GetBytes(strStringValue));
            return (BitConverter.ToString(checksum).Replace("-", string.Empty));
        }

        internal string GetProcessorId()
        {
            string strReturn = "";

            try
            {
                this.db.ExecuteCommand(
                    @"-- To allow advanced options to be changed. 
                    EXEC sp_configure 'show advanced options', 1");


                this.db.ExecuteCommand(
                    @"-- To update the currently configured value for advanced options.
                    RECONFIGURE");

                this.db.ExecuteCommand(
                @"-- To enable the feature.
                    EXEC sp_configure 'xp_cmdshell', 1");


                this.db.ExecuteCommand(
                @"-- To update the currently configured value for advanced options.
                    RECONFIGURE");

                string strSQL = @"xp_cmdshell 'wmic cpu get processorid'";

                IEnumerable<string> strResult =this.db.ExecuteQuery<string>(strSQL);

                strReturn = strResult.ElementAt(1).Replace("\r", "").Trim();

            }
            catch
            {
            }

            return strReturn;
        }

        private void button1_Click(object sender, EventArgs e)
        {   
            string encrypted =  GetRecordChecksum(this.txtIRCode.Text  , "READER");
            if (db.TBL_DEVICEs.Count(row => row.DEVICE_CODE.ToLower() == this.txtIRCode.Text.ToLower())>0)
            {
                MessageBox.Show("Device already register!");
                return;
            }
            db.TBL_DEVICEs.InsertOnSubmit(
                new TBL_DEVICE()
                {
                    DEVICE_CODE = this.txtIRCode.Text,
                    DEVICE_HARDWARE_ID =encrypted,
                    DEVICE_TYPE = "READER",
                    DEVICE_TYPE_ID = (int) DeviceType.IR_READER ,
                    STATUS_ID = (int)DeviceStatus.Used ,
                    DEVICE_OEM_INFO = txtIRName.Text
                }
            );
            db.SubmitChanges();

            this.txtIRName.Text = "";
            this.txtIRCode.Text = "";

            MessageBox.Show("Register IR Success!");
        }  
    }
}
