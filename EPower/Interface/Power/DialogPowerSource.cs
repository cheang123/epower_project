﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogPowerSource : ExDialog
    {

        GeneralProcess _flag;

        TBL_POWER_SOURCE _objNew = new TBL_POWER_SOURCE();
        public TBL_POWER_SOURCE PowerSource
        {
            get { return _objNew; }
        }
        TBL_POWER_SOURCE _objOld = new TBL_POWER_SOURCE();

        #region Constructor
        public DialogPowerSource(GeneralProcess flag, TBL_POWER_SOURCE objDisType)
        {
            InitializeComponent();
            dtpStartDate.CustomFormat = UIHelper._DefaultDateFormat;
            bind();
            _flag = flag;
            objDisType._CopyTo(_objNew);
            objDisType._CopyTo(_objOld);
            this.Text = flag.GetText(this.Text);
            UIHelper.SetEnabled(this, flag != GeneralProcess.Delete);
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
            read();
        }

        private void bind()
        {
            try
            {
                //UIHelper.SetDataSourceToComboBox(cboVoltage, DBDataContext.Db.TBL_VOLTAGEs.Where(x => x.IS_ACTIVE && x.VOLTAGE_ID >0));
                UIHelper.SetDataSourceToComboBox(cboVoltage, Lookup.GetPowerVoltage());
                UIHelper.SetDataSourceToComboBox(cboCurrency, DBDataContext.Db.TLKP_CURRENCies.Select(x => new { x.CURRENCY_ID, x.CURRENCY_NAME }));
                UIHelper.SetDataSourceToComboBox(cboPowerSourceLicensee, DBDataContext.Db.TBL_POWER_PURCHASE_LICENSEEs.Where(x => x.IS_ACTIVE));
                UIHelper.SetDataSourceToComboBox(cboCustomerConnectionType, DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs.Where(x => x.NONLICENSE_CUSTOMER_GROUP_ID == 3));
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp);
            }
        }
        #endregion

        #region Method
        /// <summary>
        /// Load data from object.
        /// </summary>
        private void read()
        {
            cboPowerSourceLicensee.SelectedValue = _objNew.POWER_PURCHASE_LICENSEE_ID;
            cboCustomerConnectionType.SelectedValue = _objNew.CUSTOMER_CONNECTION_TYPE_ID;
            txtSell.Text = _objNew.SELLER_NAME;
            cboVoltage.SelectedValue = _objNew.VOLTAGE_ID;
            txtPrice.Text = _objNew.PRICE.ToString();
            cboCurrency.SelectedValue = _objNew.CURRENCY_ID;
            txtNoConnection.Text = _objNew.NO_OF_CONNECTION.ToString("N0");
            dtpStartDate.Value = _objNew.START_DATE;
            dtpEndDate.Value = _objNew.END_DATE;
            dtpEndDate.Checked = !_objNew.IS_INUSED;
            txtNote.Text = _objNew.NOTE;
            if (_objNew.IS_INUSED)
            {
                dtpEndDate.ClearValue();
            }
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            _objNew.POWER_PURCHASE_LICENSEE_ID = (int)cboPowerSourceLicensee.SelectedValue;
            _objNew.CUSTOMER_CONNECTION_TYPE_ID = (int)cboCustomerConnectionType.SelectedValue;
            _objNew.SELLER_NAME = txtSell.Text;
            _objNew.VOLTAGE_ID = (int)cboVoltage.SelectedValue;
            _objNew.PRICE = DataHelper.ParseToDecimal(txtPrice.Text);
            _objNew.CURRENCY_ID = (int)cboCurrency.SelectedValue;
            _objNew.START_DATE = dtpStartDate.Value.Date;
            _objNew.NO_OF_CONNECTION = DataHelper.ParseToInt(txtNoConnection.Text);
            _objNew.IS_INUSED = !dtpEndDate.Checked;
            if (_objNew.IS_INUSED)
            {
                dtpEndDate.Value = UIHelper._DefaultDate;
            }
            _objNew.END_DATE = dtpEndDate.Value;
            _objNew.NOTE = this.txtNote.Text;
        }

        private bool inValid()
        {
            bool val = false;
            this.ClearValidation();

            if (cboPowerSourceLicensee.SelectedIndex == -1)
            {
                cboPowerSourceLicensee.SetValidation(string.Format(Resources.REQUIRED, lblPOWER_SOURCE.Text));
                val = true;
            }

            if (cboCustomerConnectionType.SelectedIndex == -1)
            {
                cboCustomerConnectionType.SetValidation(string.Format(Resources.REQUIRED, lblTYPE.Text));
                val = true;
            }

            if (txtSell.Text.Trim() == string.Empty)
            {
                txtSell.SetValidation(string.Format(Resources.REQUIRED, lblSELLER.Text));
                val = true;
            }
            if (cboVoltage.SelectedIndex == -1)
            {
                cboVoltage.SetValidation(string.Format(Resources.REQUIRED, lblVOLTAGE.Text));
                val = true;
            }
            System.Text.RegularExpressions.Regex regex = null;
            System.Text.RegularExpressions.Regex regex1 = null;
            regex = new System.Text.RegularExpressions.Regex("^([0-9][.]?)*$");
            regex1 = new System.Text.RegularExpressions.Regex("^([0-9]?)*$");
            if (!DataHelper.IsNumber(txtPrice.Text) || !regex.IsMatch(txtPrice.Text))
            {
                txtPrice.SetValidation(Resources.REQUIRE_INPUT_POSITIVE_NUMBER);
                val = true;
            }
            if (cboCurrency.SelectedIndex == -1)
            {
                cboCurrency.SetValidation(string.Format(Resources.REQUIRED, lblCURRENCY.Text));
                val = true;
            }
            if (!DataHelper.IsNumber(txtNoConnection.Text) || !regex1.IsMatch(txtNoConnection.Text))
            {
                txtNoConnection.SetValidation(Resources.REQUIRE_INPUT_POSITIVE_NUMBER);
                val = true;
            }

            return val;
        }
        #endregion

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
            //if duplicate record.
            txtPrice.ClearValidation();
            if (DBDataContext.Db.IsExits(_objNew, "SELLER_NAME"))
            {
                txtSell.SetValidation(string.Format(Resources.MS_IS_EXISTS, lblSELLER.Text));
                return;
            }

            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objNew);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objNew);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void ChangeEnglishKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }

        private void dtpEndDate_MouseDown(object sender, MouseEventArgs e)
        {
            if (dtpEndDate.Checked)
            {
                if (dtpEndDate.Value.Date.Equals(UIHelper._DefaultDate))
                {
                    dtpEndDate.SetValue(DBDataContext.Db.GetSystemDate());
                }
                else
                {
                    dtpEndDate.CustomFormat = UIHelper._DefaultDateFormat;
                }
            }
            else
            {
                dtpEndDate.ClearValue();
            }
        }

        private void cboPowerSourceLicensee_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtSell.Text = cboPowerSourceLicensee.Text + (cboCustomerConnectionType.Text == "" ? "" : " (" + cboCustomerConnectionType.Text + ")");
        }

        private void InputNumber(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputNumberOnly(sender, e);
        }

        private void InputDecimal(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        }
    }
}
