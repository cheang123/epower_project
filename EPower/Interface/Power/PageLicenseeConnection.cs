﻿using System;
using System.Linq;
using System.Windows.Forms;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PageLicenseeConnection : Form
    {        
        
        public  TBL_LICENSEE_CONNECTION LicenseeConnection
        {
            get
            {
                TBL_LICENSEE_CONNECTION obj = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    try
                    {
                        int intid = (int)dgv.SelectedRows[0].Cells["CONNECTION_ID"].Value;
                        obj = DBDataContext.Db.TBL_LICENSEE_CONNECTIONs.FirstOrDefault(x => x.CONNECTION_ID == intid);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return obj;
            }
        }

        #region Constructor
        public PageLicenseeConnection()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            txt_QuickSearch(null, null);
        }
        #endregion

        #region Operation 
        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }

        private void btnNew_Click(object sender, EventArgs e)
        { 
            
            DialogLicenseeConnection dig = new DialogLicenseeConnection(GeneralProcess.Insert, new TBL_LICENSEE_CONNECTION() 
            {
                LOCATION=string.Empty,
                START_DATE=DBDataContext.Db.GetSystemDate(), 
                END_DATE=UIHelper._DefaultDate,
                IS_INUSED=true 
            } );
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.LicenseeConnection.CONNECTION_ID);
            }
        }
         
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (LicenseeConnection==null)
            {
                return;
            }
            DialogLicenseeConnection dig = new DialogLicenseeConnection(GeneralProcess.Update, LicenseeConnection);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.LicenseeConnection.CONNECTION_ID);
            }
        }
         
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (LicenseeConnection==null)
            {
                return;
            }
            DialogLicenseeConnection dig = new DialogLicenseeConnection(GeneralProcess.Delete, LicenseeConnection);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.LicenseeConnection.CONNECTION_ID - 1);
            } 
        }
         
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            dgv.DataSource = from lc in DBDataContext.Db.TBL_LICENSEE_CONNECTIONs
                             join l in DBDataContext.Db.TBL_LICENSEEs on lc.LICENSEE_ID equals l.LICENSEE_ID
                             join c in DBDataContext.Db.TBL_CUSTOMERs on lc.CUSTOMER_ID equals c.CUSTOMER_ID
                             join v in DBDataContext.Db.TBL_VOLTAGEs on c.VOL_ID equals v.VOLTAGE_ID
                             where lc.IS_ACTIVE
                             && string.Concat(l.LICENSEE_NO, " ", c.LAST_NAME_KH, " ", c.FIRST_NAME_KH, " ", v.VOLTAGE_NAME).ToLower().Contains(txtQuickSearch.Text.ToLower())
                             select new
                             {
                                 lc.CONNECTION_ID,
                                 LICENSE_NO = l.LICENSEE_NO,
                                 CUS_NAME = string.Concat(c.LAST_NAME_KH, " ", c.FIRST_NAME_KH),
                                 v.VOLTAGE_NAME,
                                 PRICE = DBDataContext.Db.TBL_PRICE_DETAILs.FirstOrDefault(x => x.PRICE_ID == c.PRICE_ID).PRICE,
                                 lc.LOCATION,
                                 lc.START_DATE,
                                 END_DATE = lc.IS_INUSED ? string.Empty : string.Concat(lc.END_DATE.Day, "-", lc.END_DATE.Month, "-", lc.END_DATE.Year),
                             };  
        }

        /// <summary>
        /// Change current keyboard layout to khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        #endregion

        
        
    }
}
