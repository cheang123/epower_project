﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageLicensee
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageLicensee));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnReport = new SoftTech.Component.ExButton();
            this.btnRemove = new SoftTech.Component.ExButton();
            this.btnEDIT = new SoftTech.Component.ExButton();
            this.btnADD = new SoftTech.Component.ExButton();
            this.btnLICENSEE_CONNECTION = new SoftTech.Component.ExButton();
            this.txtQuickSearch = new SoftTech.Component.ExTextbox();
            this.LICENSEE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LICENSE_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LICENSE_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VOLTAGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TARIFF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LOCATION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_CONNECTION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LICENSEE_ID,
            this.LICENSE_NO,
            this.LICENSE_NAME,
            this.VOLTAGE,
            this.TARIFF,
            this.CURRENCY_,
            this.LOCATION,
            this.TOTAL_CONNECTION});
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellDoubleClick);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.flowLayoutPanel1);
            this.panel1.Controls.Add(this.txtQuickSearch);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnReport);
            this.flowLayoutPanel1.Controls.Add(this.btnRemove);
            this.flowLayoutPanel1.Controls.Add(this.btnEDIT);
            this.flowLayoutPanel1.Controls.Add(this.btnADD);
            this.flowLayoutPanel1.Controls.Add(this.btnLICENSEE_CONNECTION);
            resources.ApplyResources(this.flowLayoutPanel1, "flowLayoutPanel1");
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            // 
            // btnReport
            // 
            resources.ApplyResources(this.btnReport, "btnReport");
            this.btnReport.Name = "btnReport";
            this.btnReport.UseVisualStyleBackColor = true;
            this.btnReport.Click += new System.EventHandler(this.btnReport_Click);
            // 
            // btnRemove
            // 
            resources.ApplyResources(this.btnRemove, "btnRemove");
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnEDIT
            // 
            resources.ApplyResources(this.btnEDIT, "btnEDIT");
            this.btnEDIT.Name = "btnEDIT";
            this.btnEDIT.UseVisualStyleBackColor = true;
            this.btnEDIT.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnADD
            // 
            resources.ApplyResources(this.btnADD, "btnADD");
            this.btnADD.Name = "btnADD";
            this.btnADD.UseVisualStyleBackColor = true;
            this.btnADD.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnLICENSEE_CONNECTION
            // 
            resources.ApplyResources(this.btnLICENSEE_CONNECTION, "btnLICENSEE_CONNECTION");
            this.btnLICENSEE_CONNECTION.Name = "btnLICENSEE_CONNECTION";
            this.btnLICENSEE_CONNECTION.UseVisualStyleBackColor = true;
            this.btnLICENSEE_CONNECTION.Click += new System.EventHandler(this.btnLicenseeConnection_Click);
            // 
            // txtQuickSearch
            // 
            this.txtQuickSearch.BackColor = System.Drawing.Color.White;
            this.txtQuickSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtQuickSearch, "txtQuickSearch");
            this.txtQuickSearch.Name = "txtQuickSearch";
            this.txtQuickSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtQuickSearch.QuickSearch += new System.EventHandler(this.txt_QuickSearch);
            this.txtQuickSearch.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // LICENSEE_ID
            // 
            this.LICENSEE_ID.DataPropertyName = "LICENSEE_ID";
            resources.ApplyResources(this.LICENSEE_ID, "LICENSEE_ID");
            this.LICENSEE_ID.Name = "LICENSEE_ID";
            // 
            // LICENSE_NO
            // 
            this.LICENSE_NO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.LICENSE_NO.DataPropertyName = "LICENSEE_NO";
            resources.ApplyResources(this.LICENSE_NO, "LICENSE_NO");
            this.LICENSE_NO.Name = "LICENSE_NO";
            // 
            // LICENSE_NAME
            // 
            this.LICENSE_NAME.DataPropertyName = "LICENSEE_NAME";
            resources.ApplyResources(this.LICENSE_NAME, "LICENSE_NAME");
            this.LICENSE_NAME.Name = "LICENSE_NAME";
            // 
            // VOLTAGE
            // 
            this.VOLTAGE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.VOLTAGE.DataPropertyName = "VOLTAGE_NAME";
            resources.ApplyResources(this.VOLTAGE, "VOLTAGE");
            this.VOLTAGE.Name = "VOLTAGE";
            // 
            // TARIFF
            // 
            this.TARIFF.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.TARIFF.DataPropertyName = "TARIFF";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "#,##0.####0";
            dataGridViewCellStyle2.NullValue = null;
            this.TARIFF.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.TARIFF, "TARIFF");
            this.TARIFF.Name = "TARIFF";
            // 
            // CURRENCY_
            // 
            this.CURRENCY_.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CURRENCY_.DataPropertyName = "CURRENCY_SING";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.CURRENCY_.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.CURRENCY_, "CURRENCY_");
            this.CURRENCY_.Name = "CURRENCY_";
            // 
            // LOCATION
            // 
            this.LOCATION.DataPropertyName = "LOCATION";
            resources.ApplyResources(this.LOCATION, "LOCATION");
            this.LOCATION.Name = "LOCATION";
            // 
            // TOTAL_CONNECTION
            // 
            this.TOTAL_CONNECTION.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.TOTAL_CONNECTION.DataPropertyName = "COUNT_CONNECTION";
            dataGridViewCellStyle4.Format = "N0";
            this.TOTAL_CONNECTION.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.TOTAL_CONNECTION, "TOTAL_CONNECTION");
            this.TOTAL_CONNECTION.Name = "TOTAL_CONNECTION";
            // 
            // PageLicensee
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.panel1);
            this.Name = "PageLicensee";
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panel1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExTextbox txtQuickSearch;
        private ExButton btnADD;
        private ExButton btnEDIT;
        private DataGridView dgv;
        private ExButton btnRemove;
        private ExButton btnLICENSEE_CONNECTION;
        private ExButton btnReport;
        private FlowLayoutPanel flowLayoutPanel1;
        private DataGridViewTextBoxColumn LICENSEE_ID;
        private DataGridViewTextBoxColumn LICENSE_NO;
        private DataGridViewTextBoxColumn LICENSE_NAME;
        private DataGridViewTextBoxColumn VOLTAGE;
        private DataGridViewTextBoxColumn TARIFF;
        private DataGridViewTextBoxColumn CURRENCY_;
        private DataGridViewTextBoxColumn LOCATION;
        private DataGridViewTextBoxColumn TOTAL_CONNECTION;
    }
}
