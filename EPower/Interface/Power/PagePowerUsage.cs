﻿using System;
using System.Linq;
using System.Windows.Forms;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PagePowerUsage : Form
    {        
        public TBL_POWER Power
        {
            get
            {
                TBL_POWER objPower = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    try
                    {
                        int boxID = (int)dgv.SelectedRows[0].Cells["POWER_ID"].Value;
                        objPower = DBDataContext.Db.TBL_POWERs.FirstOrDefault(x => x.POWER_ID == boxID);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objPower;
            }
        }

        #region Constructor
        public PagePowerUsage()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            loadData();
        }
        #endregion

        #region Operation
        /// <summary>
        /// Add new seal.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNew_Click(object sender, EventArgs e)
        {
            DialogPowerUsage dig = new DialogPowerUsage(GeneralProcess.Insert, new TBL_POWER());
            if (dig.ShowDialog() == DialogResult.OK)
            {
                loadData();
                UIHelper.SelectRow(dgv, dig.Power.POWER_ID);
            }
        }

        /// <summary>
        /// Edit seal.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                DialogPowerUsage dig = new DialogPowerUsage(GeneralProcess.Update, Power);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    loadData();
                    UIHelper.SelectRow(dgv, dig.Power.POWER_ID);
                }
            }
        }

        /// <summary>
        /// Remove seal.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (IsDeletable())
            {
                DialogPowerUsage dig = new DialogPowerUsage(GeneralProcess.Delete, Power);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    loadData();
                    UIHelper.SelectRow(dgv, dig.Power.POWER_ID - 1);
                }
            }
        }

        /// <summary>
        /// Load data from database and display
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loadData()
        {        
            try
            {

                dgv.DataSource = from p in DBDataContext.Db.TBL_POWERs
                                 where p.IS_ACTIVE
                                 && ((p.POWER_MONTH.Date.Month == dtpMonth.Value.Date.Month
                                 && p.POWER_MONTH.Year == dtpMonth.Value.Year) || dtpMonth.Checked == false)
                                 && p.TRANSFORMER_ID == 0 // total power
                                 select new
                                 {
                                     p.POWER_ID,
                                     p.POWER_MONTH,
                                     p.POWER_INPUT 
                                 };
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }            
        }

        private void dtpMonth_ValueChanged(object sender, EventArgs e)
        {
            loadData();
        }

        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }
        #endregion

        #region Method
        private bool IsDeletable()
        {
            //TODO: can delte.
            bool val = false;
            if (dgv.SelectedRows.Count > 0)
            {
                val = true;
            }
            return val;
        }

        #endregion

       
    }
}
