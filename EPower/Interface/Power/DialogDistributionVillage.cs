﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;

namespace EPower.Interface
{
    public partial class DialogDistributionVillage : ExDialog
    {
        bool _isSuccess = false;
        TBL_DISTRIBUTION_FACILITY _objDistributoinFacility = new TBL_DISTRIBUTION_FACILITY();
        public DialogDistributionVillage(TBL_DISTRIBUTION_FACILITY obj)
        {
            InitializeComponent();
            obj._CopyTo(_objDistributoinFacility);
        }

        private void read()
        {
            var villages = (from s in DBDataContext.Db.TLKP_VILLAGEs
                            join l in DBDataContext.Db.TBL_LICENSE_VILLAGEs on s.VILLAGE_CODE equals l.VILLAGE_CODE
                            where l.IS_ACTIVE
                            select new Villages
                            {
                                Code = s.VILLAGE_CODE,
                                Name = s.VILLAGE_NAME,
                                ParentCode = s.COMMUNE_CODE,
                                IsVillage = true
                            }).Distinct();

            var communes = (from s in DBDataContext.Db.TLKP_COMMUNEs
                            join v in villages on s.COMMUNE_CODE equals v.ParentCode
                            select new Villages
                            {
                                Code = s.COMMUNE_CODE,
                                Name = s.COMMUNE_NAME,
                                ParentCode = s.DISTRICT_CODE
                            }).Distinct();

            var districts = (from s in DBDataContext.Db.TLKP_DISTRICTs
                             join c in communes on s.DISTRICT_CODE equals c.ParentCode
                             select new Villages
                             {
                                 Code = s.DISTRICT_CODE,
                                 Name = s.DISTRICT_NAME,
                                 ParentCode = s.PROVINCE_CODE
                             }).Distinct();

            var provinces = (from s in DBDataContext.Db.TLKP_PROVINCEs
                             join d in districts on s.PROVINCE_CODE equals d.ParentCode
                             select new Villages
                             {
                                 Code = s.PROVINCE_CODE,
                                 Name = s.PROVINCE_NAME,
                                 ParentCode = ""
                             }).Distinct().ToList();

            var vil = provinces.Concat(districts.ToList()).Concat(communes.ToList()).Concat(villages.ToList());

            this.treeList.DataSource = vil.ToList();

            var activeVillages = DBDataContext.Db.TBL_DISTRIBUTION_VILLAGEs.Where(x => x.IS_ACTIVE).ToList();
            foreach (var village in activeVillages)
            {
                var node = treeList.FindNodeByKeyID(village.VILLAGE_CODE);
                if (node == null)
                {
                    continue;
                }
                treeList.SetNodeCheckState(node, CheckState.Checked, true);
            }

            this.treeList.ExpandAll();
        }

        private void parentExpend(TreeNode node)
        {
            node.Expand();
            if (node.Parent != null)
            {
                parentExpend(node.Parent);
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            _isSuccess = false;
            Runner.RunNewThread(Save, Resources.SAVE);
            if (_isSuccess)
            {
                this.DialogResult = DialogResult.OK;
            }
        }

        private void Save()
        {
            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    List<Villages> lstCheckedVillages = new List<Villages>();
                    var checkedVillage = treeList.GetAllCheckedNodes();
                    foreach (var node in checkedVillage)
                    {
                        lstCheckedVillages.Add((Villages)treeList.GetDataRecordByNode(node));
                    }

                    //Update checked village
                    var newCheckedVilalges = DBDataContext.Db.TBL_DISTRIBUTION_VILLAGEs.Where(x => lstCheckedVillages.Select(y => y.Code).Contains(x.VILLAGE_CODE)).ToList();
                    newCheckedVilalges.ForEach(x => x.IS_ACTIVE = true);

                    //Update unchecked villages
                    var uncheckedVillage = DBDataContext.Db.TBL_DISTRIBUTION_VILLAGEs.Where(x => !lstCheckedVillages.Select(y => y.Code).Contains(x.VILLAGE_CODE)).ToList();
                    uncheckedVillage.ForEach(x => x.IS_ACTIVE = false);

                    //Insert
                    var newVillages = lstCheckedVillages.Where(x => x.IsVillage && !DBDataContext.Db.TBL_DISTRIBUTION_VILLAGEs.Select(y => y.VILLAGE_CODE).Contains(x.Code)).ToList();
                    foreach (var item in newVillages)
                    {
                        TBL_DISTRIBUTION_VILLAGE obj = new TBL_DISTRIBUTION_VILLAGE()
                        {
                            DISTRIBUTION_FACILITY_ID = _objDistributoinFacility.DISTRIBUTION_FACILITY_ID,
                            IS_ACTIVE = true,
                            VILLAGE_CODE = item.Code,
                            ROW_DATE = DateTime.Now
                        };
                        DBDataContext.Db.TBL_DISTRIBUTION_VILLAGEs.InsertOnSubmit(obj);
                    }
                    DBDataContext.Db.SubmitChanges();
                    tran.Complete();
                    _isSuccess = true;
                }
            }
            catch (Exception exp)
            {
                throw new Exception(Resources.MAXIMUM_DATA_2100);
                MsgBox.LogError(exp);
            }
        }

        private void DialogLicenseVillage_Load(object sender, EventArgs e)
        {
            Runner.Run(read, Resources.SHOW_INFORMATION);
        }

        private void btnCHANGE_LOG_Click(object sender, EventArgs e)
        {
            SoftTech.Security.Interface.DialogChangeLog.ShowChangeLog(this._objDistributoinFacility);
        }

    }
}