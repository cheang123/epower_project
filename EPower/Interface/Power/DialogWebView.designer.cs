﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogWebView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogWebView));
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.pnlNavigation = new System.Windows.Forms.Panel();
            this.picRefresh = new System.Windows.Forms.PictureBox();
            this.picGoNext = new System.Windows.Forms.PictureBox();
            this.picCloseWeb = new System.Windows.Forms.PictureBox();
            this.picGoBack = new System.Windows.Forms.PictureBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlNavigation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picRefresh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picGoNext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCloseWeb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picGoBack)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.webBrowser1);
            this.content.Controls.Add(this.pnlNavigation);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.content.Controls.SetChildIndex(this.webBrowser1, 0);
            // 
            // webBrowser1
            // 
            resources.ApplyResources(this.webBrowser1, "webBrowser1");
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowser1_DocumentCompleted);
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Controls.Add(this.picRefresh);
            this.pnlNavigation.Controls.Add(this.picGoNext);
            this.pnlNavigation.Controls.Add(this.picCloseWeb);
            this.pnlNavigation.Controls.Add(this.picGoBack);
            resources.ApplyResources(this.pnlNavigation, "pnlNavigation");
            this.pnlNavigation.Name = "pnlNavigation";
            // 
            // picRefresh
            // 
            this.picRefresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picRefresh.Image = global::EPower.Properties.Resources.Refresh;
            resources.ApplyResources(this.picRefresh, "picRefresh");
            this.picRefresh.Name = "picRefresh";
            this.picRefresh.TabStop = false;
            this.picRefresh.Click += new System.EventHandler(this.picRefresh_Click);
            // 
            // picGoNext
            // 
            this.picGoNext.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picGoNext.Image = global::EPower.Properties.Resources.Next;
            resources.ApplyResources(this.picGoNext, "picGoNext");
            this.picGoNext.Name = "picGoNext";
            this.picGoNext.TabStop = false;
            this.picGoNext.Click += new System.EventHandler(this.picGoNext_Click);
            // 
            // picCloseWeb
            // 
            this.picCloseWeb.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picCloseWeb.Image = global::EPower.Properties.Resources.icon_close;
            resources.ApplyResources(this.picCloseWeb, "picCloseWeb");
            this.picCloseWeb.Name = "picCloseWeb";
            this.picCloseWeb.TabStop = false;
            this.picCloseWeb.Click += new System.EventHandler(this.picCloseWeb_Click);
            // 
            // picGoBack
            // 
            this.picGoBack.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picGoBack.Image = global::EPower.Properties.Resources.Back;
            resources.ApplyResources(this.picGoBack, "picGoBack");
            this.picGoBack.Name = "picGoBack";
            this.picGoBack.TabStop = false;
            this.picGoBack.Click += new System.EventHandler(this.picGoBack_Click);
            // 
            // DialogWebView
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogWebView";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.DialogWebView_Load);
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlNavigation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picRefresh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picGoNext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCloseWeb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picGoBack)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private WebBrowser webBrowser1;
        private Panel pnlNavigation;
        private PictureBox picRefresh;
        private PictureBox picGoNext;
        private PictureBox picCloseWeb;
        private PictureBox picGoBack;
    }
}