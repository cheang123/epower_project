﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageDistributionFacility : Form
    {        
        
        public  TBL_DISTRIBUTION_FACILITY DistributionFacility
        {
            get
            {
                TBL_DISTRIBUTION_FACILITY objDisFaci = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    try
                    {
                        int intid = (int)dgv.SelectedRows[0].Cells[DISTRIBUTION_FACILITY_ID.Name].Value;
                        objDisFaci = DBDataContext.Db.TBL_DISTRIBUTION_FACILITies.FirstOrDefault(x => x.DISTRIBUTION_FACILITY_ID == intid);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objDisFaci;
            }

        }

        #region Constructor
        public PageDistributionFacility()
        {
            InitializeComponent();
            this.btnREPORT.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_QUARTERLY_REPORT]);
            UIHelper.DataGridViewProperties(dgv);
            txt_QuickSearch(null, null);
            BindDistribution();
            
        }
        #endregion
        int distribution_id = 0;
        #region Operation 
        private void btnNew_Click(object sender, EventArgs e)
        {  
            DialogDistributionFacility dig = new DialogDistributionFacility(GeneralProcess.Insert, new TBL_DISTRIBUTION_FACILITY() 
            {  
                START_DATE=DBDataContext.Db.GetSystemDate(),
                END_DATE=UIHelper._DefaultDate,
                IS_INUSED=true, 
                IS_SHIELDED=true
            } );
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.DistributionFacility.DISTRIBUTION_FACILITY_ID);
            }
        }
        public void BindDistribution()
        {
            try
            {
                int dis = (int)(this.cboDistribution.SelectedValue ?? 0);
                UIHelper.SetDataSourceToComboBox(this.cboDistribution, DBDataContext.Db.TLKP_DISTRIBUTIONs, Resources.ALL_DISTRIBUTION_FACIBILITY_TYPE);
                this.cboDistribution.SelectedValue = dis;
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (DistributionFacility==null)
            {
                return;
            }
            DialogDistributionFacility dig = new DialogDistributionFacility(GeneralProcess.Update, DistributionFacility);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.DistributionFacility.DISTRIBUTION_FACILITY_ID);
            }
        }
         
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (DistributionFacility==null)
            {
                return;
            }
            DialogDistributionFacility dig = new DialogDistributionFacility(GeneralProcess.Delete, DistributionFacility);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.DistributionFacility.DISTRIBUTION_FACILITY_ID - 1);
            } 
        }
         
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            //distribution_id = (int)cboDistribution.SelectedValue;
            dgv.DataSource = from df in DBDataContext.Db.TBL_DISTRIBUTION_FACILITies
                             join d in DBDataContext.Db.TLKP_DISTRIBUTIONs on df.DISTRIBUTION_ID equals d.DISTRIBUTION_ID
                             join v in DBDataContext.Db.TBL_VOLTAGEs on df.VOLTAGE_ID equals v.VOLTAGE_ID
                             join p in DBDataContext.Db.TBL_PHASEs on df.PHASE_ID equals p.PHASE_ID 
                             where df.IS_ACTIVE
                             && string.Concat(d.DISTRIBUTION_NAME).ToLower().Contains(txtQuickSearch.Text.ToLower())
                             && (distribution_id == 0 || d.DISTRIBUTION_ID == distribution_id)
                             orderby df.DISTRIBUTION_ID,df.START_DATE
                             select new
                             {
                                 df.DISTRIBUTION_FACILITY_ID,
                                 d.DISTRIBUTION_NAME,
                                 v.VOLTAGE_NAME,
                                 p.PHASE_NAME,
                                 df.LENGTH_OF_LINE,
                                 df.START_DATE,
                                 END_DATE=df.IS_INUSED?string.Empty:string.Concat( df.END_DATE.Day,"-",df.END_DATE.Month,"-",df.END_DATE.Year) ,
                                 df.TOTAL_POLE,
                                 df.ENDING_VOLTAGE,
                                 TYPE=df.IS_SHIELDED?Resources.CABLE_JACKET:Resources.STRIPPED_WIRE
                             }; 
        }

        /// <summary>
        /// Change current keyboard layout to khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        #endregion

        private void btnDistributoinVillage_Click(object sender, EventArgs e)
        {
            if (this.DistributionFacility==null)
            {
                return;
            }
            DialogDistributionVillage dig = new DialogDistributionVillage(this.DistributionFacility);
            dig.ShowDialog();
        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            var diag = new DialogReportQuarterOptions(false);
            diag.chkTABLE_DISTRIBUTION_FACILTITY.Checked = true;
            if (diag.ShowDialog() == DialogResult.OK)
            {
                var doc = PageReportQuaterly.OpenReport(diag);
                doc.ViewReport("");
            }
        }

        private void cboDistribution_SelectedIndexChanged(object sender, EventArgs e)
        {
            distribution_id = (int)cboDistribution.SelectedValue;
            txt_QuickSearch(null, null);
        }
    }
}
