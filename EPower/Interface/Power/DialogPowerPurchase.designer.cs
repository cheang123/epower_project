﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogPowerPurchase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogPowerPurchase));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.cboSource = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblSELLER = new System.Windows.Forms.Label();
            this.dtpMonth = new System.Windows.Forms.DateTimePicker();
            this.lblMONTH = new System.Windows.Forms.Label();
            this.cboTransformer = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblTRANSFORMER = new System.Windows.Forms.Label();
            this.lblFILE_PATH = new System.Windows.Forms.Label();
            this.txtRemark = new System.Windows.Forms.TextBox();
            this.lblNOTE = new System.Windows.Forms.Label();
            this.txtAttachmentName = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.lblPRICE = new System.Windows.Forms.Label();
            this.txtUnitPrice = new System.Windows.Forms.TextBox();
            this.lblATTACHMENT_NAME = new System.Windows.Forms.Label();
            this.lblCURRENCY = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtInvoice = new System.Windows.Forms.TextBox();
            this.lblINVOICE_NO = new System.Windows.Forms.Label();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.lblEXPENSE_ACCOUNT = new System.Windows.Forms.Label();
            this.lblPAYMENT_ACCOUNT = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.cboTransAccount = new SoftTech.Component.TreeComboBox();
            this.cboPaymentAccount = new SoftTech.Component.TreeComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.POWER_PURCHASE_DETAIL_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.METER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.START_USAGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.END_USAGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MULTIPLIER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SURPLUS_POWER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_REACTIVE = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.USAGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CONTROL0 = new System.Windows.Forms.DataGridViewImageColumn();
            this.IS_ACTIVE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cboPointOfConnection = new System.Windows.Forms.ComboBox();
            this.lblPOINT_OF_CONNECTION = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblORIGINAL_FILE_NAME = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnOPEN_FILE0 = new System.Windows.Forms.Button();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.groupBox3);
            this.content.Controls.Add(this.groupBox1);
            this.content.Controls.Add(this.groupBox2);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.groupBox2, 0);
            this.content.Controls.SetChildIndex(this.groupBox1, 0);
            this.content.Controls.SetChildIndex(this.groupBox3, 0);
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // cboSource
            // 
            this.cboSource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSource.DropDownWidth = 300;
            this.cboSource.FormattingEnabled = true;
            resources.ApplyResources(this.cboSource, "cboSource");
            this.cboSource.Name = "cboSource";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // lblSELLER
            // 
            resources.ApplyResources(this.lblSELLER, "lblSELLER");
            this.lblSELLER.Name = "lblSELLER";
            // 
            // dtpMonth
            // 
            resources.ApplyResources(this.dtpMonth, "dtpMonth");
            this.dtpMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMonth.Name = "dtpMonth";
            this.dtpMonth.Enter += new System.EventHandler(this.ChangeEnglishKeyboard);
            // 
            // lblMONTH
            // 
            resources.ApplyResources(this.lblMONTH, "lblMONTH");
            this.lblMONTH.Name = "lblMONTH";
            // 
            // cboTransformer
            // 
            this.cboTransformer.DropDownHeight = 450;
            this.cboTransformer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTransformer.FormattingEnabled = true;
            resources.ApplyResources(this.cboTransformer, "cboTransformer");
            this.cboTransformer.Name = "cboTransformer";
            this.cboTransformer.SelectedIndexChanged += new System.EventHandler(this.cboTransformer_SelectedIndexChanged);
            // 
            // label17
            // 
            resources.ApplyResources(this.label17, "label17");
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Name = "label17";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Name = "label14";
            // 
            // lblTRANSFORMER
            // 
            resources.ApplyResources(this.lblTRANSFORMER, "lblTRANSFORMER");
            this.lblTRANSFORMER.Name = "lblTRANSFORMER";
            // 
            // lblFILE_PATH
            // 
            resources.ApplyResources(this.lblFILE_PATH, "lblFILE_PATH");
            this.lblFILE_PATH.Name = "lblFILE_PATH";
            // 
            // txtRemark
            // 
            this.txtRemark.AcceptsTab = true;
            resources.ApplyResources(this.txtRemark, "txtRemark");
            this.txtRemark.Name = "txtRemark";
            this.txtRemark.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // lblNOTE
            // 
            resources.ApplyResources(this.lblNOTE, "lblNOTE");
            this.lblNOTE.Name = "lblNOTE";
            // 
            // txtAttachmentName
            // 
            this.txtAttachmentName.AcceptsTab = true;
            resources.ApplyResources(this.txtAttachmentName, "txtAttachmentName");
            this.txtAttachmentName.Name = "txtAttachmentName";
            this.txtAttachmentName.ReadOnly = true;
            this.txtAttachmentName.Enter += new System.EventHandler(this.ChangeEnglishKeyboard);
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Name = "label12";
            // 
            // lblPRICE
            // 
            resources.ApplyResources(this.lblPRICE, "lblPRICE");
            this.lblPRICE.Name = "lblPRICE";
            // 
            // txtUnitPrice
            // 
            this.txtUnitPrice.AcceptsTab = true;
            resources.ApplyResources(this.txtUnitPrice, "txtUnitPrice");
            this.txtUnitPrice.Name = "txtUnitPrice";
            // 
            // lblATTACHMENT_NAME
            // 
            resources.ApplyResources(this.lblATTACHMENT_NAME, "lblATTACHMENT_NAME");
            this.lblATTACHMENT_NAME.Name = "lblATTACHMENT_NAME";
            // 
            // lblCURRENCY
            // 
            resources.ApplyResources(this.lblCURRENCY, "lblCURRENCY");
            this.lblCURRENCY.Name = "lblCURRENCY";
            // 
            // txtPrice
            // 
            this.txtPrice.AcceptsTab = true;
            resources.ApplyResources(this.txtPrice, "txtPrice");
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Enter += new System.EventHandler(this.ChangeEnglishKeyboard);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // txtInvoice
            // 
            this.txtInvoice.AcceptsTab = true;
            resources.ApplyResources(this.txtInvoice, "txtInvoice");
            this.txtInvoice.Name = "txtInvoice";
            this.txtInvoice.Enter += new System.EventHandler(this.ChangeEnglishKeyboard);
            // 
            // lblINVOICE_NO
            // 
            resources.ApplyResources(this.lblINVOICE_NO, "lblINVOICE_NO");
            this.lblINVOICE_NO.Name = "lblINVOICE_NO";
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            // 
            // label22
            // 
            resources.ApplyResources(this.label22, "label22");
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Name = "label22";
            // 
            // lblEXPENSE_ACCOUNT
            // 
            resources.ApplyResources(this.lblEXPENSE_ACCOUNT, "lblEXPENSE_ACCOUNT");
            this.lblEXPENSE_ACCOUNT.Name = "lblEXPENSE_ACCOUNT";
            // 
            // lblPAYMENT_ACCOUNT
            // 
            resources.ApplyResources(this.lblPAYMENT_ACCOUNT, "lblPAYMENT_ACCOUNT");
            this.lblPAYMENT_ACCOUNT.Name = "lblPAYMENT_ACCOUNT";
            // 
            // label29
            // 
            resources.ApplyResources(this.label29, "label29");
            this.label29.Name = "label29";
            // 
            // cboTransAccount
            // 
            this.cboTransAccount.AbsoluteChildrenSelectableOnly = true;
            this.cboTransAccount.BranchSeparator = "/";
            this.cboTransAccount.Imagelist = null;
            resources.ApplyResources(this.cboTransAccount, "cboTransAccount");
            this.cboTransAccount.Name = "cboTransAccount";
            this.cboTransAccount.PopupHeight = 250;
            this.cboTransAccount.PopupWidth = 200;
            this.cboTransAccount.SelectedNode = null;
            // 
            // cboPaymentAccount
            // 
            this.cboPaymentAccount.AbsoluteChildrenSelectableOnly = true;
            this.cboPaymentAccount.BranchSeparator = "/";
            this.cboPaymentAccount.Imagelist = null;
            resources.ApplyResources(this.cboPaymentAccount, "cboPaymentAccount");
            this.cboPaymentAccount.Name = "cboPaymentAccount";
            this.cboPaymentAccount.PopupHeight = 250;
            this.cboPaymentAccount.PopupWidth = 200;
            this.cboPaymentAccount.SelectedNode = null;
            // 
            // label30
            // 
            resources.ApplyResources(this.label30, "label30");
            this.label30.ForeColor = System.Drawing.Color.Red;
            this.label30.Name = "label30";
            // 
            // label31
            // 
            resources.ApplyResources(this.label31, "label31");
            this.label31.ForeColor = System.Drawing.Color.Red;
            this.label31.Name = "label31";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgv);
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeColumns = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.POWER_PURCHASE_DETAIL_ID,
            this.METER,
            this.START_USAGE,
            this.END_USAGE,
            this.MULTIPLIER,
            this.SURPLUS_POWER,
            this.IS_REACTIVE,
            this.USAGE,
            this.CONTROL0,
            this.IS_ACTIVE});
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellClick);
            this.dgv.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellEndEdit);
            this.dgv.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgv_ColumnHeaderMouseClick);
            this.dgv.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgv_DataError);
            this.dgv.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvShow_EditingControlShowing);
            this.dgv.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgv_KeyPress);
            // 
            // POWER_PURCHASE_DETAIL_ID
            // 
            this.POWER_PURCHASE_DETAIL_ID.DataPropertyName = "POWER_PURCHASE_DETAIL_ID";
            resources.ApplyResources(this.POWER_PURCHASE_DETAIL_ID, "POWER_PURCHASE_DETAIL_ID");
            this.POWER_PURCHASE_DETAIL_ID.Name = "POWER_PURCHASE_DETAIL_ID";
            this.POWER_PURCHASE_DETAIL_ID.ReadOnly = true;
            // 
            // METER
            // 
            this.METER.DataPropertyName = "METER";
            this.METER.FillWeight = 148.002F;
            resources.ApplyResources(this.METER, "METER");
            this.METER.Name = "METER";
            this.METER.ReadOnly = true;
            this.METER.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // START_USAGE
            // 
            this.START_USAGE.DataPropertyName = "START_USAGE";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N0";
            dataGridViewCellStyle2.NullValue = null;
            this.START_USAGE.DefaultCellStyle = dataGridViewCellStyle2;
            this.START_USAGE.FillWeight = 107.868F;
            resources.ApplyResources(this.START_USAGE, "START_USAGE");
            this.START_USAGE.Name = "START_USAGE";
            this.START_USAGE.ReadOnly = true;
            this.START_USAGE.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // END_USAGE
            // 
            this.END_USAGE.DataPropertyName = "END_USAGE";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N0";
            dataGridViewCellStyle3.NullValue = null;
            this.END_USAGE.DefaultCellStyle = dataGridViewCellStyle3;
            this.END_USAGE.FillWeight = 142.1726F;
            resources.ApplyResources(this.END_USAGE, "END_USAGE");
            this.END_USAGE.Name = "END_USAGE";
            this.END_USAGE.ReadOnly = true;
            this.END_USAGE.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // MULTIPLIER
            // 
            this.MULTIPLIER.DataPropertyName = "MULTIPLIER";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.MULTIPLIER.DefaultCellStyle = dataGridViewCellStyle4;
            this.MULTIPLIER.FillWeight = 68.02114F;
            resources.ApplyResources(this.MULTIPLIER, "MULTIPLIER");
            this.MULTIPLIER.Name = "MULTIPLIER";
            this.MULTIPLIER.ReadOnly = true;
            this.MULTIPLIER.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // SURPLUS_POWER
            // 
            this.SURPLUS_POWER.DataPropertyName = "ADJUST_USAGE";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N0";
            dataGridViewCellStyle5.NullValue = null;
            this.SURPLUS_POWER.DefaultCellStyle = dataGridViewCellStyle5;
            this.SURPLUS_POWER.FillWeight = 142.1726F;
            resources.ApplyResources(this.SURPLUS_POWER, "SURPLUS_POWER");
            this.SURPLUS_POWER.Name = "SURPLUS_POWER";
            this.SURPLUS_POWER.ReadOnly = true;
            this.SURPLUS_POWER.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // IS_REACTIVE
            // 
            this.IS_REACTIVE.DataPropertyName = "IS_REACTIVE";
            this.IS_REACTIVE.FillWeight = 94.44269F;
            resources.ApplyResources(this.IS_REACTIVE, "IS_REACTIVE");
            this.IS_REACTIVE.Name = "IS_REACTIVE";
            this.IS_REACTIVE.ReadOnly = true;
            this.IS_REACTIVE.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.IS_REACTIVE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // USAGE
            // 
            this.USAGE.DataPropertyName = "USAGE";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N0";
            dataGridViewCellStyle6.NullValue = null;
            this.USAGE.DefaultCellStyle = dataGridViewCellStyle6;
            this.USAGE.FillWeight = 119.8262F;
            resources.ApplyResources(this.USAGE, "USAGE");
            this.USAGE.Name = "USAGE";
            this.USAGE.ReadOnly = true;
            this.USAGE.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // CONTROL0
            // 
            this.CONTROL0.FillWeight = 27.4946F;
            resources.ApplyResources(this.CONTROL0, "CONTROL0");
            this.CONTROL0.Image = global::EPower.Properties.Resources.icon_close;
            this.CONTROL0.Name = "CONTROL0";
            this.CONTROL0.ReadOnly = true;
            this.CONTROL0.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // IS_ACTIVE
            // 
            this.IS_ACTIVE.DataPropertyName = "IS_ACTIVE";
            resources.ApplyResources(this.IS_ACTIVE, "IS_ACTIVE");
            this.IS_ACTIVE.Name = "IS_ACTIVE";
            this.IS_ACTIVE.ReadOnly = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cboPointOfConnection);
            this.groupBox1.Controls.Add(this.lblPOINT_OF_CONNECTION);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtInvoice);
            this.groupBox1.Controls.Add(this.lblMONTH);
            this.groupBox1.Controls.Add(this.dtpMonth);
            this.groupBox1.Controls.Add(this.cboPaymentAccount);
            this.groupBox1.Controls.Add(this.lblSELLER);
            this.groupBox1.Controls.Add(this.cboTransAccount);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label31);
            this.groupBox1.Controls.Add(this.cboSource);
            this.groupBox1.Controls.Add(this.label30);
            this.groupBox1.Controls.Add(this.lblTRANSFORMER);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.cboCurrency);
            this.groupBox1.Controls.Add(this.cboTransformer);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.lblINVOICE_NO);
            this.groupBox1.Controls.Add(this.txtPrice);
            this.groupBox1.Controls.Add(this.lblCURRENCY);
            this.groupBox1.Controls.Add(this.lblEXPENSE_ACCOUNT);
            this.groupBox1.Controls.Add(this.lblPAYMENT_ACCOUNT);
            this.groupBox1.Controls.Add(this.txtUnitPrice);
            this.groupBox1.Controls.Add(this.lblPRICE);
            this.groupBox1.Controls.Add(this.label29);
            this.groupBox1.Controls.Add(this.label12);
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // cboPointOfConnection
            // 
            this.cboPointOfConnection.DropDownHeight = 450;
            this.cboPointOfConnection.FormattingEnabled = true;
            resources.ApplyResources(this.cboPointOfConnection, "cboPointOfConnection");
            this.cboPointOfConnection.Name = "cboPointOfConnection";
            this.cboPointOfConnection.SelectedIndexChanged += new System.EventHandler(this.cboPointOfConnection_SelectedIndexChanged);
            this.cboPointOfConnection.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbotxtPointOfConnection_KeyDown);
            this.cboPointOfConnection.MouseLeave += new System.EventHandler(this.cbotxtPointOfConnection_MouseLeave);
            // 
            // lblPOINT_OF_CONNECTION
            // 
            resources.ApplyResources(this.lblPOINT_OF_CONNECTION, "lblPOINT_OF_CONNECTION");
            this.lblPOINT_OF_CONNECTION.Name = "lblPOINT_OF_CONNECTION";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Name = "label3";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lblORIGINAL_FILE_NAME);
            this.groupBox3.Controls.Add(this.btnOPEN_FILE0);
            this.groupBox3.Controls.Add(this.lblFILE_PATH);
            this.groupBox3.Controls.Add(this.lblATTACHMENT_NAME);
            this.groupBox3.Controls.Add(this.txtAttachmentName);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.lblNOTE);
            this.groupBox3.Controls.Add(this.txtRemark);
            resources.ApplyResources(this.groupBox3, "groupBox3");
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.TabStop = false;
            // 
            // lblORIGINAL_FILE_NAME
            // 
            this.lblORIGINAL_FILE_NAME.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblORIGINAL_FILE_NAME.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.lblORIGINAL_FILE_NAME, "lblORIGINAL_FILE_NAME");
            this.lblORIGINAL_FILE_NAME.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblORIGINAL_FILE_NAME.Name = "lblORIGINAL_FILE_NAME";
            this.lblORIGINAL_FILE_NAME.TabStop = true;
            this.lblORIGINAL_FILE_NAME.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblORIGINAL_FILE_NAME_LinkClicked);
            // 
            // btnOPEN_FILE0
            // 
            resources.ApplyResources(this.btnOPEN_FILE0, "btnOPEN_FILE0");
            this.btnOPEN_FILE0.Name = "btnOPEN_FILE0";
            this.btnOPEN_FILE0.UseVisualStyleBackColor = true;
            this.btnOPEN_FILE0.Click += new System.EventHandler(this.btnOPEN_FILE_Click);
            // 
            // DialogPowerPurchase
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogPowerPurchase";
            this.Load += new System.EventHandler(this.DialogPowerPurchase_Load);
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private ExButton btnCHANGE_LOG;
        private ComboBox cboTransformer;
        private Label label17;
        private Label label14;
        private Label lblTRANSFORMER;
        private Label lblFILE_PATH;
        private TextBox txtRemark;
        private Label lblNOTE;
        private TextBox txtAttachmentName;
        private Label label12;
        private Label lblPRICE;
        private TextBox txtUnitPrice;
        private Label lblATTACHMENT_NAME;
        private Label lblCURRENCY;
        private TextBox txtPrice;
        private ComboBox cboSource;
        private Label label1;
        private Label lblSELLER;
        private DateTimePicker dtpMonth;
        private Label lblMONTH;
        private Label label9;
        private TextBox txtInvoice;
        private Label lblINVOICE_NO;
        private ComboBox cboCurrency;
        private Label label22;
        private Label lblEXPENSE_ACCOUNT;
        private Label lblPAYMENT_ACCOUNT;
        private Label label29;
        private TreeComboBox cboPaymentAccount;
        private TreeComboBox cboTransAccount;
        private Label label31;
        private Label label30;
        private GroupBox groupBox2;
        private DataGridView dgv;
        private GroupBox groupBox1;
        private GroupBox groupBox3;
        private Label lblPOINT_OF_CONNECTION;
        private Label label3;
        private Button btnOPEN_FILE0;
        private DataGridViewTextBoxColumn POWER_PURCHASE_DETAIL_ID;
        private DataGridViewTextBoxColumn METER;
        private DataGridViewTextBoxColumn START_USAGE;
        private DataGridViewTextBoxColumn END_USAGE;
        private DataGridViewTextBoxColumn MULTIPLIER;
        private DataGridViewTextBoxColumn SURPLUS_POWER;
        private DataGridViewCheckBoxColumn IS_REACTIVE;
        private DataGridViewTextBoxColumn USAGE;
        private DataGridViewImageColumn CONTROL0;
        private DataGridViewTextBoxColumn IS_ACTIVE;
        private ExLinkLabel lblORIGINAL_FILE_NAME;
        private ComboBox cboPointOfConnection;
    }
}