﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogPowerUsageByTransformer : ExDialog
    {
        #region Data
        GeneralProcess _flag;
        TBL_POWER _objNew = new TBL_POWER();
        public TBL_POWER Power
        {
            get { return _objNew; }
        }
        TBL_POWER _objOld = new TBL_POWER();

        //bool _loading = false;
        private bool load = true;
        #endregion

        #region Constructor
        public DialogPowerUsageByTransformer(GeneralProcess flag, TBL_POWER objPower)
        {
            InitializeComponent();
            UIHelper.SetDataSourceToComboBox(this.cboTransformer, DBDataContext.Db.TBL_TRANSFORMERs.Where(row => row.IS_ACTIVE).OrderBy(x => x.TRANSFORMER_CODE));
            DateTime dt = new DateTime();
            dt = DBDataContext.Db.GetSystemDate();
            dtp.Value = new DateTime(dt.Year, dt.Month, 1);
            _flag = flag;
            objPower._CopyTo(_objNew);
            objPower._CopyTo(_objOld);
            read();
        }
        #endregion

        #region Operation 
        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        private void DialogPowerUsageByTransformer_Load(object sender, EventArgs e)
        {
            this.Text = _flag.GetText(this.Text);
            UIHelper.SetEnabled(this, !(_flag == GeneralProcess.Delete));
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
        }

        private void dtp_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void cboTransformer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_flag == GeneralProcess.Delete || _flag == GeneralProcess.Update)
            {
                return;
            }
            if (this.load || cboTransformer.SelectedIndex == -1)
            {
                return;
            }
            var objLast = (from x in DBDataContext.Db.TBL_POWERs where x.TRANSFORMER_ID == (int)cboTransformer.SelectedValue && x.IS_ACTIVE select x.METER_CODE).Distinct();
            if (objLast == null)
            {
                return;
            }
            cboMeter.DataSource = objLast;
        }
        private void cboMeter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_flag == GeneralProcess.Update || _flag == GeneralProcess.Delete)
            {
                return;
            }
            if (cboTransformer.SelectedValue == null)
            {
                return;
            }
            DateTime date = dtp.Value.Month == 1 ? new DateTime(dtp.Value.Year - 1, 12, 1) : new DateTime(dtp.Value.Year, dtp.Value.Month - 1, 1);
            TBL_POWER objPower = DBDataContext.Db.TBL_POWERs.OrderByDescending(x => x.POWER_MONTH).FirstOrDefault(x => x.TRANSFORMER_ID == (int)cboTransformer.SelectedValue && x.METER_CODE == cboMeter.Text && x.POWER_MONTH == date && x.IS_ACTIVE);
            if (objPower == null)
            {
                txtMulti.Text = "1";
                txtStart.Text = "0";
                txtEnd.Text = "0";
                return;
            }
            txtStart.Text = objPower.END_USAGE.ToString("N0");
            txtMulti.Text = objPower.MULTIPLIER.ToString();
        }
        private void txtCalcUsage(object sender, EventArgs e)
        {
            decimal usage, start, end, adjust;
            int multi;
            start = DataHelper.ParseToDecimal(txtStart.Text);
            end = DataHelper.ParseToDecimal(txtEnd.Text);
            adjust = DataHelper.ParseToDecimal(txtAdjustPower.Text);
            multi = DataHelper.ParseToInt(txtMulti.Text);
            if (multi < 1)
            {
                multi = 1;
            }
            usage = Method.GetTotalUsage(start, end, multi, end < start);
            txtInputUsagePostpaid.Text = (usage + adjust).ToString("N0");
        }
        private void dtp_ValueChanged(object sender, EventArgs e)
        {
            if (_flag == GeneralProcess.Update || _flag == GeneralProcess.Delete)
            {
                return;
            }
            cboMeter_SelectedIndexChanged(null, null);
        }
        /// <summary>
        /// Data transaction.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (inValid())
                {
                    return;
                }

                if (checkExist())
                {
                    cboMeter.SetValidation(string.Format(Resources.MS_IS_EXISTS, lblMETER_CODE.Text));
                    return;
                }

                write();

                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objNew);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objNew);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }
        #endregion

        #region Method
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool inValid()
        {
            this.ClearAllValidation();
            bool blnVal = false;
            if (cboMeter.Text == string.Empty)
            {
                cboMeter.SetValidation(string.Format(Resources.REQUIRED, lblMETER_CODE.Text));
                blnVal = true;
            }
            if (cboTransformer.SelectedIndex == -1)
            {
                cboTransformer.SetValidation(string.Format(Resources.REQUIRED, lblTRANSFORMER.Text));
                blnVal = true;
            }

            if (!DataHelper.IsNumber(txtStart.Text))
            {
                txtStart.SetValidation(Resources.REQUIRED_INPUT_NUMBER);
                blnVal = true;
            }

            if (DataHelper.ParseToInt(txtStart.Text) < 0)
            {
                txtStart.SetValidation(Resources.REQUIRED_POSITIVE_NUMBER);
                blnVal = true;
            }

            if (!DataHelper.IsNumber(txtEnd.Text))
            {
                txtEnd.SetValidation(Resources.REQUIRED_INPUT_NUMBER);
                blnVal = true;
            }

            if (DataHelper.ParseToInt(txtEnd.Text) < 0)
            {
                txtEnd.SetValidation(Resources.REQUIRED_POSITIVE_NUMBER);
                blnVal = true;
            }

            if (DataHelper.ParseToInt(txtMulti.Text) < 1)
            {
                txtMulti.SetValidation(Resources.REQUEST_GRATER_THAN_ZERO);
                blnVal = true;
            }

            if (!DataHelper.IsNumber(txtAdjustPower.Text))
            {
                txtAdjustPower.SetValidation(Resources.REQUIRED_INPUT_NUMBER);
                blnVal = true;
            }

            return blnVal;
        }

        private bool checkExist()
        {
            if (_flag == GeneralProcess.Delete) return false;
            DateTime date = new DateTime(dtp.Value.Year, dtp.Value.Month, 1);
            bool exit = false;
            var obj = from t in DBDataContext.Db.TBL_POWERs
                      where t.IS_ACTIVE
                            && t.TRANSFORMER_ID == (int)cboTransformer.SelectedValue
                            && t.METER_CODE.Trim().Replace(" ", "") == this.cboMeter.Text.Trim().Replace(" ", "")
                            && t.POWER_MONTH == date
                            && t.POWER_ID != _objOld.POWER_ID
                      select new { t.POWER_ID };
            if (obj.Count() > 0)
            {
                exit = true;
            }
            return exit;
        }

        /// <summary>
        /// Read data from object.
        /// </summary>
        private void read()
        {
            dtp.Value = _objNew.POWER_MONTH;
            this.cboTransformer.SelectedValue = this._objNew.TRANSFORMER_ID;
            txtStart.Text = _objNew.START_USAGE.ToString("N0");
            txtEnd.Text = _objNew.END_USAGE.ToString("N0");
            txtMulti.Text = _objNew.MULTIPLIER.ToString();
            txtAdjustPower.Text = _objNew.ADJUST_USAGE.ToString("N0");
            txtInputUsagePostpaid.Text = _objNew.POWER_INPUT.ToString("N0");
            this.cboMeter.Text = this._objNew.METER_CODE;

            //Handler event after load completed
            this.txtStart.TextChanged += new EventHandler(this.txtCalcUsage);
            this.txtEnd.TextChanged += new EventHandler(this.txtCalcUsage);
            this.txtMulti.TextChanged += new EventHandler(this.txtCalcUsage);
            this.txtAdjustPower.TextChanged += new EventHandler(this.txtCalcUsage);
            this.cboTransformer.SelectedIndexChanged += new EventHandler(this.cboTransformer_SelectedIndexChanged);
            load = false;
        }

        private void write()
        {
            _objNew.POWER_MONTH = new DateTime(dtp.Value.Year, dtp.Value.Month, 1);
            _objNew.TRANSFORMER_ID = (int)this.cboTransformer.SelectedValue;
            _objNew.METER_CODE = cboMeter.Text;
            _objNew.START_USAGE = DataHelper.ParseToDecimal(txtStart.Text);
            _objNew.END_USAGE = DataHelper.ParseToDecimal(txtEnd.Text);
            _objNew.MULTIPLIER = DataHelper.ParseToInt(txtMulti.Text);
            _objNew.ADJUST_USAGE = DataHelper.ParseToDecimal(txtAdjustPower.Text);
            _objNew.POWER_INPUT = DataHelper.ParseToDecimal(txtInputUsagePostpaid.Text);
        }

        #endregion


    }
}