﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PagePowerGeneration
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PagePowerGeneration));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.GENERATION_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GENERATION_MONTH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GENERATOR_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.POWER_GENERATION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AUXILIARY_USE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FUEL_CONSUMPTION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LUB_COMSUMPTION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OPERATION_HOUR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.cboGenerator = new System.Windows.Forms.ComboBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnREPORT = new SoftTech.Component.ExButton();
            this.btnREMOVE = new SoftTech.Component.ExButton();
            this.btnEDIT = new SoftTech.Component.ExButton();
            this.btnADD = new SoftTech.Component.ExButton();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.GENERATION_ID,
            this.GENERATION_MONTH,
            this.GENERATOR_NO,
            this.POWER_GENERATION,
            this.AUXILIARY_USE,
            this.FUEL_CONSUMPTION,
            this.LUB_COMSUMPTION,
            this.OPERATION_HOUR});
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // GENERATION_ID
            // 
            this.GENERATION_ID.DataPropertyName = "GENERATION_ID";
            resources.ApplyResources(this.GENERATION_ID, "GENERATION_ID");
            this.GENERATION_ID.Name = "GENERATION_ID";
            // 
            // GENERATION_MONTH
            // 
            this.GENERATION_MONTH.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.GENERATION_MONTH.DataPropertyName = "GENERATION_MONTH";
            dataGridViewCellStyle2.Format = "MM-yyyy";
            this.GENERATION_MONTH.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.GENERATION_MONTH, "GENERATION_MONTH");
            this.GENERATION_MONTH.Name = "GENERATION_MONTH";
            // 
            // GENERATOR_NO
            // 
            this.GENERATOR_NO.DataPropertyName = "GENERATOR_NO";
            resources.ApplyResources(this.GENERATOR_NO, "GENERATOR_NO");
            this.GENERATOR_NO.Name = "GENERATOR_NO";
            // 
            // POWER_GENERATION
            // 
            this.POWER_GENERATION.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.POWER_GENERATION.DataPropertyName = "POWER_GENERATION";
            dataGridViewCellStyle3.Format = "N0";
            this.POWER_GENERATION.DefaultCellStyle = dataGridViewCellStyle3;
            this.POWER_GENERATION.FillWeight = 150F;
            resources.ApplyResources(this.POWER_GENERATION, "POWER_GENERATION");
            this.POWER_GENERATION.Name = "POWER_GENERATION";
            // 
            // AUXILIARY_USE
            // 
            this.AUXILIARY_USE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.AUXILIARY_USE.DataPropertyName = "AUXILIARY_USE";
            dataGridViewCellStyle4.Format = "N0";
            this.AUXILIARY_USE.DefaultCellStyle = dataGridViewCellStyle4;
            this.AUXILIARY_USE.FillWeight = 150F;
            resources.ApplyResources(this.AUXILIARY_USE, "AUXILIARY_USE");
            this.AUXILIARY_USE.Name = "AUXILIARY_USE";
            // 
            // FUEL_CONSUMPTION
            // 
            this.FUEL_CONSUMPTION.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.FUEL_CONSUMPTION.DataPropertyName = "FUEL_CONSUMPTION";
            dataGridViewCellStyle5.Format = "N0";
            this.FUEL_CONSUMPTION.DefaultCellStyle = dataGridViewCellStyle5;
            resources.ApplyResources(this.FUEL_CONSUMPTION, "FUEL_CONSUMPTION");
            this.FUEL_CONSUMPTION.Name = "FUEL_CONSUMPTION";
            // 
            // LUB_COMSUMPTION
            // 
            this.LUB_COMSUMPTION.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.LUB_COMSUMPTION.DataPropertyName = "LUB_COMSUMPTION";
            dataGridViewCellStyle6.Format = "N0";
            this.LUB_COMSUMPTION.DefaultCellStyle = dataGridViewCellStyle6;
            resources.ApplyResources(this.LUB_COMSUMPTION, "LUB_COMSUMPTION");
            this.LUB_COMSUMPTION.Name = "LUB_COMSUMPTION";
            // 
            // OPERATION_HOUR
            // 
            this.OPERATION_HOUR.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.OPERATION_HOUR.DataPropertyName = "OPERATION_HOUR";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N0";
            dataGridViewCellStyle7.NullValue = null;
            this.OPERATION_HOUR.DefaultCellStyle = dataGridViewCellStyle7;
            resources.ApplyResources(this.OPERATION_HOUR, "OPERATION_HOUR");
            this.OPERATION_HOUR.Name = "OPERATION_HOUR";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.dtpDate);
            this.panel1.Controls.Add(this.cboGenerator);
            this.panel1.Controls.Add(this.flowLayoutPanel1);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // dtpDate
            // 
            resources.ApplyResources(this.dtpDate, "dtpDate");
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.ShowCheckBox = true;
            this.dtpDate.ValueChanged += new System.EventHandler(this.dtpDate_ValueChanged);
            // 
            // cboGenerator
            // 
            this.cboGenerator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboGenerator.FormattingEnabled = true;
            resources.ApplyResources(this.cboGenerator, "cboGenerator");
            this.cboGenerator.Name = "cboGenerator";
            this.cboGenerator.SelectedIndexChanged += new System.EventHandler(this.cboSearchType_SelectedIndexChanged);
            // 
            // flowLayoutPanel1
            // 
            resources.ApplyResources(this.flowLayoutPanel1, "flowLayoutPanel1");
            this.flowLayoutPanel1.Controls.Add(this.btnREPORT);
            this.flowLayoutPanel1.Controls.Add(this.btnREMOVE);
            this.flowLayoutPanel1.Controls.Add(this.btnEDIT);
            this.flowLayoutPanel1.Controls.Add(this.btnADD);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            // 
            // btnREPORT
            // 
            resources.ApplyResources(this.btnREPORT, "btnREPORT");
            this.btnREPORT.Name = "btnREPORT";
            this.btnREPORT.UseVisualStyleBackColor = true;
            this.btnREPORT.Click += new System.EventHandler(this.btnReport_Click);
            // 
            // btnREMOVE
            // 
            resources.ApplyResources(this.btnREMOVE, "btnREMOVE");
            this.btnREMOVE.Name = "btnREMOVE";
            this.btnREMOVE.UseVisualStyleBackColor = true;
            this.btnREMOVE.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnEDIT
            // 
            resources.ApplyResources(this.btnEDIT, "btnEDIT");
            this.btnEDIT.Name = "btnEDIT";
            this.btnEDIT.UseVisualStyleBackColor = true;
            this.btnEDIT.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnADD
            // 
            resources.ApplyResources(this.btnADD, "btnADD");
            this.btnADD.Name = "btnADD";
            this.btnADD.UseVisualStyleBackColor = true;
            this.btnADD.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "ENERGY_ID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn1, "dataGridViewTextBoxColumn1");
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "GENERATOR_NO";
            resources.ApplyResources(this.dataGridViewTextBoxColumn2, "dataGridViewTextBoxColumn2");
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "GENERATION_MONTH";
            dataGridViewCellStyle8.Format = "MM - yyyy";
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle8;
            resources.ApplyResources(this.dataGridViewTextBoxColumn3, "dataGridViewTextBoxColumn3");
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "POWER_GENERATION";
            dataGridViewCellStyle9.Format = "N0";
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewTextBoxColumn4.FillWeight = 150F;
            resources.ApplyResources(this.dataGridViewTextBoxColumn4, "dataGridViewTextBoxColumn4");
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "AUXILIARY_USE";
            dataGridViewCellStyle10.Format = "N0";
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewTextBoxColumn5.FillWeight = 150F;
            resources.ApplyResources(this.dataGridViewTextBoxColumn5, "dataGridViewTextBoxColumn5");
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "FUEL_CONSUMPTION";
            dataGridViewCellStyle11.Format = "N0";
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle11;
            resources.ApplyResources(this.dataGridViewTextBoxColumn6, "dataGridViewTextBoxColumn6");
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn7.DataPropertyName = "LUB_COMSUMPTION";
            dataGridViewCellStyle12.Format = "N0";
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle12;
            resources.ApplyResources(this.dataGridViewTextBoxColumn7, "dataGridViewTextBoxColumn7");
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // PagePowerGeneration
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.panel1);
            this.Name = "PagePowerGeneration";
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panel1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnADD;
        private ExButton btnEDIT;
        private DataGridView dgv;
        private ExButton btnREMOVE;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private ExButton btnREPORT;
        private FlowLayoutPanel flowLayoutPanel1;
        private ComboBox cboGenerator;
        private DateTimePicker dtpDate;
        private DataGridViewTextBoxColumn GENERATION_ID;
        private DataGridViewTextBoxColumn GENERATION_MONTH;
        private DataGridViewTextBoxColumn GENERATOR_NO;
        private DataGridViewTextBoxColumn POWER_GENERATION;
        private DataGridViewTextBoxColumn AUXILIARY_USE;
        private DataGridViewTextBoxColumn FUEL_CONSUMPTION;
        private DataGridViewTextBoxColumn LUB_COMSUMPTION;
        private DataGridViewTextBoxColumn OPERATION_HOUR;
    }
}
