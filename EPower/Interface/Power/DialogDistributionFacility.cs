﻿using System;
using System.Data;
using System.Transactions;
using System.Windows.Forms;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;

namespace EPower.Interface
{
    public partial class DialogDistributionFacility: ExDialog
    {
        GeneralProcess _flag;
        
        TBL_DISTRIBUTION_FACILITY _objNew = new TBL_DISTRIBUTION_FACILITY();

        public TBL_DISTRIBUTION_FACILITY DistributionFacility
        {
            get { return _objNew; } 
        } 
        TBL_DISTRIBUTION_FACILITY _objOld = new TBL_DISTRIBUTION_FACILITY();

        #region Constructor
        public DialogDistributionFacility(GeneralProcess flag, TBL_DISTRIBUTION_FACILITY obj)
        {
            InitializeComponent();
            dtpStartDate.CustomFormat = UIHelper._DefaultDateFormat;
            _flag = flag; 
            obj._CopyTo(_objNew);
            obj._CopyTo(_objOld);
             
            UIHelper.SetDataSourceToComboBox(cboDistribution, Lookup.GetDistribution());
            UIHelper.SetDataSourceToComboBox(cboVoltage, Lookup.GetPowerVoltage());
            UIHelper.SetDataSourceToComboBox(cboPhase, Lookup.GetPowerPhases());


            this.cboType.SelectedIndex = 0;
            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("REPORT_ID", typeof(int));
            dtReport.Columns.Add("REPORT_NAME", typeof(string));

            DataRow drJacked = dtReport.NewRow();
            drJacked["REPORT_ID"] = 1;
            drJacked["REPORT_NAME"] = Resources.JACKED_CABLE;
            dtReport.Rows.Add(drJacked);

            DataRow drNonIsolator = dtReport.NewRow();
            drNonIsolator["REPORT_ID"] = 2;
            drNonIsolator["REPORT_NAME"] = Resources.NON_ISOLATOR_WIRE;
            dtReport.Rows.Add(drNonIsolator);
            UIHelper.SetDataSourceToComboBox(cboType, dtReport);

            this.Text = _flag.GetText(this.Text);
            UIHelper.SetEnabled(this, !(flag == GeneralProcess.Delete)); 
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert; 
            read();
            cboChange();
        }
        #endregion

        #region Operation
        /// <summary>
        /// Data transaction
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
            //if duplicate record.
            txtLine.ClearAllValidation(); 
            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objNew); 
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objNew);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
            
        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
        
        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        #endregion

        #region Method
        /// <summary>
        /// Load data from object.
        /// </summary>
        private void read()
        {
            cboDistribution.SelectedValue = (int)_objNew.DISTRIBUTION_ID;
            cboVoltage.SelectedValue = (int)_objNew.VOLTAGE_ID;
            cboPhase.SelectedValue = (int)_objNew.PHASE_ID;
            txtLine.Text = _objNew.LENGTH_OF_LINE.ToString("N0"); 
            dtpStartDate.Value = _objNew.START_DATE;
            dtpEndDate.Value = _objNew.END_DATE;
            dtpEndDate.Checked= !_objNew.IS_INUSED;
            txtNote.Text = _objNew.NOTE;
            if (_objNew.IS_INUSED)
            {
                dtpEndDate.ClearValue();
            }
            cboType.SelectedIndex = _objNew.IS_SHIELDED ? 0 : 1;
            txtEndingVoltage.Text = _objNew.ENDING_VOLTAGE;
            txtTotalPole.Text = _objNew.TOTAL_POLE.ToString("N0");
            
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        { 
            _objNew.DISTRIBUTION_ID = (int)cboDistribution.SelectedValue;
            _objNew.VOLTAGE_ID=(int)cboVoltage.SelectedValue;
            _objNew.PHASE_ID = (int)cboPhase.SelectedValue;
            _objNew.LENGTH_OF_LINE = DataHelper.ParseToDecimal(txtLine.Text);
            _objNew.START_DATE = dtpStartDate.Value;
            _objNew.IS_INUSED = !dtpEndDate.Checked;
            _objNew.NOTE = this.txtNote.Text;
            if (_objNew.IS_INUSED)
            {
                dtpEndDate.Value = UIHelper._DefaultDate;
            }
            _objNew.END_DATE = dtpEndDate.Value;
            _objNew.IS_SHIELDED = cboType.SelectedIndex == 0 ? true : false;
            _objNew.ENDING_VOLTAGE = txtEndingVoltage.Text;
            _objNew.TOTAL_POLE = DataHelper.ParseToDecimal(txtTotalPole.Text);
        }

        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation(); 
            if (cboDistribution.SelectedIndex==-1)
            {
                cboDistribution.SetValidation(string.Format(Resources.REQUIRED, lblDISTRIBUTION.Text));
                val = true;
            }
            if (cboVoltage.SelectedIndex==-1)
            {
                cboVoltage.SetValidation(string.Format(Resources.REQUIRED, lblVOLTAGE.Text));
                val = true;
            }
            if (cboPhase.SelectedIndex==-1)
            {
                cboPhase.SetValidation(string.Format(Resources.REQUIRED, lblPHASE.Text));
                val = true;
            }
            if (!DataHelper.IsNumber(txtLine.Text))
            {
                txtLine.SetValidation(Resources.REQUIRED_INPUT_NUMBER);
                val = true;
            }
            if (cboType.SelectedIndex == -1)
            {
                cboType.SetValidation(string.Format(Resources.REQUIRED, lblLENGTH_OF_LINE_AND_CABLE_TYPE.Text));
                val = true;
            }
            if (txtEndingVoltage.Text.Trim() == string.Empty)
            {
                txtEndingVoltage.SetValidation(string.Format(Resources.REQUIRED, lblTOTAL_POLE_AND_ENDING_VOLTAGE.Text));
                val = true;
            }
            if (txtTotalPole.Text.Trim() == string.Empty)
            {
                txtTotalPole.SetValidation(string.Format(Resources.REQUIRED, lblTOTAL_POLE_AND_ENDING_VOLTAGE.Text));
                val = true;
            }
            if (DataHelper.ParseToDecimal(txtLine.Text) < 0)
            {
                txtLine.SetValidation(string.Format(Resources.REQUEST_GRATER_THAN_ZERO, lblLENGTH_OF_LINE_AND_CABLE_TYPE));
                val = true;
            }
            if (DataHelper.ParseToDecimal(txtTotalPole.Text) < 0)
            {
                txtTotalPole.SetValidation(string.Format(Resources.REQUEST_GRATER_THAN_ZERO, lblTOTAL_POLE_AND_ENDING_VOLTAGE));
                val = true;
            }
            return val;
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        } 
        #endregion  

   
        private void dtpEndDate_MouseDown(object sender, MouseEventArgs e)
        {
            if (dtpEndDate.Checked)
            {
                if (dtpEndDate.Value.Date.Equals(UIHelper._DefaultDate))
                {
                    dtpEndDate.SetValue(DBDataContext.Db.GetSystemDate());
                }
                else
                {
                    dtpEndDate.CustomFormat = UIHelper._DefaultDateFormat;
                }
            }
            else
            {
                dtpEndDate.ClearValue();
            }
        }

        private void txtTotalPole_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputNumberOnly(sender, e);
        }

        private void txtLine_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        }

        private void cboDistribution_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboChange();
        }

        private void cboChange()
        {
            if (cboDistribution.SelectedIndex == 0)
            {
                UIHelper.SetDataSourceToComboBox(cboVoltage, Lookup.GetVoltageMid());
                UIHelper.SetDataSourceToComboBox(cboPhase, Lookup.GetPhase1_2_3());
            }

            if (cboDistribution.SelectedIndex == 1)
            {
                UIHelper.SetDataSourceToComboBox(cboVoltage, Lookup.GetVoltageLow());
                UIHelper.SetDataSourceToComboBox(cboPhase, Lookup.GetPhase1_3());
            }

            if (cboDistribution.SelectedIndex == 2)
            {
                UIHelper.SetDataSourceToComboBox(cboVoltage, Lookup.GetVoltageHigh());
                UIHelper.SetDataSourceToComboBox(cboPhase, Lookup.GetPhase3());
            }
        }
    }
}