﻿using System;
using System.Linq;
using System.Windows.Forms;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class DialogLicenseeConnectionDetail: ExDialog
    {
        TBL_LICENSEE _objLicensee = new TBL_LICENSEE();

        public TBL_LICENSEE Licensee
        {
            get { return _objLicensee; }
            set { _objLicensee = value; }
        } 

        public TBL_LICENSEE_CONNECTION LicenseeConnection
        {
            get 
            {
                TBL_LICENSEE_CONNECTION obj = null;
                if (dgv.SelectedRows.Count>0)
                {
                    obj=DBDataContext.Db.TBL_LICENSEE_CONNECTIONs.FirstOrDefault(x=>x.CONNECTION_ID==(int)dgv.SelectedRows[0].Cells["CONNECTION_ID"].Value); 
                }
                
                return obj;
            } 
        } 
         

        #region Constructor
        public DialogLicenseeConnectionDetail(TBL_LICENSEE obj)
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            _objLicensee = obj; 
            read();
        }
        #endregion 
        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
        
        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        } 

        #region Method
        /// <summary>
        /// Load data from object.
        /// </summary>
        private void read()
        {
            txtLicenseeNo.Text = _objLicensee.LICENSEE_NO;
            txtLicenseeName.Text = _objLicensee.LICENSEE_NAME;
            txtTariff.Text = _objLicensee.TARIFF.ToString();
            txtVoltage.Text = DBDataContext.Db.TBL_VOLTAGEs.FirstOrDefault(x => x.VOLTAGE_ID == _objLicensee.VOLTAGE_ID).VOLTAGE_NAME;
            bind(); 
        }

        private void bind()
        {
            dgv.DataSource = from lc in DBDataContext.Db.TBL_LICENSEE_CONNECTIONs
                             join l in DBDataContext.Db.TBL_LICENSEEs on lc.LICENSEE_ID equals l.LICENSEE_ID
                             join c in DBDataContext.Db.TBL_CUSTOMERs on lc.CUSTOMER_ID equals c.CUSTOMER_ID 
                             where lc.IS_ACTIVE && l.LICENSEE_ID ==_objLicensee.LICENSEE_ID 
                             select new
                             {
                                 lc.CONNECTION_ID, 
                                 c.CUSTOMER_CODE,
                                 CUS_NAME = string.Concat(c.LAST_NAME_KH, " ", c.FIRST_NAME_KH), 
                                 lc.LOCATION,
                                 lc.START_DATE,
                                 END_DATE = lc.IS_INUSED ? string.Empty : string.Concat(lc.END_DATE.Day, "-", lc.END_DATE.Month, "-", lc.END_DATE.Year),
                             };
        }

        private void btnNew_Click(object sender, EventArgs e)
        {

            DialogLicenseeConnection dig = new DialogLicenseeConnection(GeneralProcess.Insert, new TBL_LICENSEE_CONNECTION()
            {
                LICENSEE_ID=_objLicensee.LICENSEE_ID,
                LOCATION = string.Empty,
                START_DATE = DBDataContext.Db.GetSystemDate(),
                END_DATE = UIHelper._DefaultDate,
                IS_INUSED = true
            });
            if (dig.ShowDialog() == DialogResult.OK)
            {
                bind();
                UIHelper.SelectRow(dgv, dig.LicenseeConnection.CONNECTION_ID);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (LicenseeConnection == null)
            {
                return;
            }
            DialogLicenseeConnection dig = new DialogLicenseeConnection(GeneralProcess.Update, LicenseeConnection);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                bind();
                UIHelper.SelectRow(dgv, dig.LicenseeConnection.CONNECTION_ID);
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (LicenseeConnection == null)
            {
                return;
            }
            DialogLicenseeConnection dig = new DialogLicenseeConnection(GeneralProcess.Delete, LicenseeConnection);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                bind();
                UIHelper.SelectRow(dgv, dig.LicenseeConnection.CONNECTION_ID - 1);
            }
        }
        #endregion Method​
    }
}