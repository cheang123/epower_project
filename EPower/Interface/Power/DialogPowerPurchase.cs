﻿using EPower.Base.Helper;
using EPower.Base.Logic;
using EPower.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogPowerPurchase : ExDialog
    {
        //int nMeter= 0;

        GeneralProcess _flag;
        bool load = true;
        TBL_POWER_PURCHASE _objNew = new TBL_POWER_PURCHASE();
        public TBL_POWER_PURCHASE PowerPurchase => _objNew;
        TBL_POWER_PURCHASE _objOld = new TBL_POWER_PURCHASE();

        TBL_ATTACHMENT _attNew = new TBL_ATTACHMENT();
        TBL_ATTACHMENT _attOld = new TBL_ATTACHMENT();

        IEnumerable<TBL_POWER_PURCHASE_DETAIL> _oldObjDetail = null;
        DataTable dt = new DataTable();
        DataTable dtFtp = new DataTable();

        FTP ftp = new FTP();
        string path = "",
            attachmentName = "",
            _ftpPath = "",
            branch_code = "";

        #region Constructor
        public DialogPowerPurchase(GeneralProcess flag, TBL_POWER_PURCHASE obj, TBL_ATTACHMENT objAtt)
        {
            InitializeComponent();

            var today = DBDataContext.Db.GetSystemDate().Date;
            this.Text = flag.GetText(this.Text);


            UIHelper.SetEnabled(this, !(flag == GeneralProcess.Delete));
            this.btnOPEN_FILE0.Enabled =
            this.cboTransAccount.Enabled =
            this.cboPaymentAccount.Enabled = !(flag == GeneralProcess.Delete);
            _flag = flag;

            // Power Purchase
            obj._CopyTo(_objNew);
            obj._CopyTo(_objOld);

            // Attachment
            objAtt._CopyTo(_attNew);
            objAtt._CopyTo(_attOld);
            btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;

            UIHelper.SetDataSourceToComboBox(cboSource, DBDataContext.Db.TBL_POWER_SOURCEs.Where(x => x.IS_ACTIVE && (x.IS_INUSED || x.END_DATE.Date >= today || x.SOURCE_ID == _objNew.SOURCE_ID)));
            UIHelper.SetDataSourceToComboBox(cboTransformer, DBDataContext.Db.TBL_TRANSFORMERs.Where(x => x.IS_ACTIVE).OrderBy(x => x.TRANSFORMER_CODE), "");
            UIHelper.SetDataSourceToComboBox(cboCurrency, DBDataContext.Db.TLKP_CURRENCies.Select(x => new { x.CURRENCY_ID, x.CURRENCY_NAME }));
            UIHelper.SetDataSourceToComboBox(cboPointOfConnection, DBDataContext.Db.TBL_POWER_PURCHASEs.Where(x => x.IS_ACTIVE).Select(x => new { ID = x.POINT_OF_CONNECTION, x.POINT_OF_CONNECTION }).Distinct());
            cboPointOfConnection.SelectedIndex = -1;

            // Setup Lookup Account
            new AccountChartPopulator().PopluateTree(cboTransAccount.TreeView, AccountChartHelper.GetAccounts(AccountConfig.POWER_PURCHASE_ACCOUNT));
            new AccountChartPopulator().PopluateTree(cboPaymentAccount.TreeView, AccountChartHelper.GetAccounts(AccountConfig.PAYMENT_ACCOUNTS));

            initEvent();
            InitailDataProp();
            InitailPurchaseDetailProp();
            read();
        }

        private void initEvent()
        {
            cboPointOfConnection.KeyPress += InputOnlyNumber;
            txtPrice.KeyPress += InputOnlyNumber;
        }

        #endregion

        #region Method
        private void read()
        {
            // Power Purchase 
            txtInvoice.Text = _objNew.INVOICE_NO;
            dtpMonth.Value = new DateTime(_objNew.MONTH.Year, _objNew.MONTH.Month, 1);
            cboSource.SelectedValue = _objNew.SOURCE_ID;
            cboTransformer.SelectedValue = _objNew.TRANSFORMER_ID;
            //txtPointOfConnection.Text = _objNew.POINT_OF_CONNECTION.ToString() == "0" ? "" : _objNew.POINT_OF_CONNECTION.ToString();
            cboPointOfConnection.Text = _objNew.POINT_OF_CONNECTION.ToString() == "0" ? "" : _objNew.POINT_OF_CONNECTION.ToString();
            txtPrice.Text = _objNew.PRICE.ToString("N5");
            cboCurrency.SelectedValue = _objNew.CURRENCY_ID;
            txtUnitPrice.Text = _objNew.UNIT_OF_PRICE;
            txtRemark.Text = _objNew.REMARK;

            this.cboTransformer.SelectedIndexChanged += new EventHandler(this.cboTransformer_SelectedIndexChanged);
            this.cboSource.SelectedIndexChanged += new EventHandler(this.cboSource_SelectedIndexChanged);
            //this.cboPointOfConnection.SelectedIndexChanged += new EventHandler(this.cboPointOfConnection_SelectedIndexChanged);
            this.dtpMonth.ValueChanged += new EventHandler(this.dtpMonth_ValueChanged);

            // Account 
            cboPaymentAccount.TreeView.SelectedNode = cboPaymentAccount.TreeView.GetNodesIncludeAncestors().FirstOrDefault(x => (int)x.Tag == _objNew.PAYMENT_ACCOUNT_ID);
            cboPaymentAccount.Text = cboPaymentAccount.TreeView.SelectedNode != null ? cboPaymentAccount.TreeView.SelectedNode.Text : "";
            cboTransAccount.TreeView.SelectedNode = cboTransAccount.TreeView.GetNodesIncludeAncestors().FirstOrDefault(x => (int)x.Tag == _objNew.ACCOUNT_ID);
            cboTransAccount.Text = cboTransAccount.TreeView.SelectedNode != null ? cboTransAccount.TreeView.SelectedNode.Text : "";

            // Attachment
            txtAttachmentName.Text = _attNew.ATTACHMENT_NAME;
            lblORIGINAL_FILE_NAME.Text = Path.GetFileName(_attNew.FILE_NAME);

            // Power Purchase Detail
            var PurchaseDetail = DBDataContext.Db.TBL_POWER_PURCHASE_DETAILs
                .Where(x => x.POWER_PURCHASE_ID == _objNew.POWER_PURCHASE_ID && x.IS_ACTIVE)
                .Select(x => x);

            dt.Clear();
            DataRow dr;
            foreach (var row in PurchaseDetail)
            {
                dr = dt.NewRow();
                dr[POWER_PURCHASE_DETAIL_ID.DataPropertyName] = row.POWER_PURCHASE_DETAIL_ID;
                dr[METER.DataPropertyName.ToString()] = row.METER_CODE.ToString();
                dr[START_USAGE.DataPropertyName.ToString()] = row.START_USAGE.ToString(UIHelper._DefaultUsageFormat) == "" ? "0" : (string)row.START_USAGE.ToString(UIHelper._DefaultUsageFormat);
                dr[END_USAGE.DataPropertyName.ToString()] = row.END_USAGE.ToString(UIHelper._DefaultUsageFormat);
                dr[MULTIPLIER.DataPropertyName.ToString()] = row.MULTIPLIER;
                dr[SURPLUS_POWER.DataPropertyName.ToString()] = DataHelper.ParseToDecimal(row.ADJUST_USAGE.ToString());
                dr[IS_REACTIVE.DataPropertyName] = row.IS_REACTIVE;
                dr[USAGE.DataPropertyName.ToString()] = row.POWER_PURCHASE.ToString(UIHelper._DefaultUsageFormat);
                dr[IS_ACTIVE.DataPropertyName] = row.IS_ACTIVE;
                dt.Rows.Add(dr);
                //nMeter++;
            }

            if (_flag != GeneralProcess.Insert)
            {
                dgv.DataSource = dt;
            }

            load = false;

            if (_flag == GeneralProcess.Insert)
            {
                cboTransformer_SelectedIndexChanged(cboTransformer, EventArgs.Empty);
                txtUnitPrice.Text = Resources.KWH;
            }
        }

        private void write()
        {
            btnOK.Focus();

            // Attachment
            _attNew.ATTACHMENT_NAME = txtAttachmentName.Text.ToString();
            if (path == string.Empty)
            {
                _attNew.ATTACHMENT = _attOld.ATTACHMENT;
                _attNew.FILE_NAME = _attOld.FILE_NAME;
            }
            else
            {
                _attNew.ATTACHMENT = File.ReadAllBytes(path);
                _attNew.FILE_NAME = path.ToString();
            }
            _attNew.IS_ACTIVE = true;
            _attNew.ROW_DATE = DBDataContext.Db.GetSystemDate();

            if (_flag == GeneralProcess.Insert)
            {
                if (path.ToString() != string.Empty)
                {
                    DBDataContext.Db.Insert(_attNew);
                }
            }

            else if (_flag == GeneralProcess.Update)
            {
                if (_objNew.ATTACHMENT_ID == 0 && path != string.Empty)
                {
                    DBDataContext.Db.Insert(_attNew);
                }
                else
                {
                    DBDataContext.Db.Update(_attOld, _attNew);
                }
            }

            else if (_flag == GeneralProcess.Delete)
            {
                DBDataContext.Db.Delete(_attNew);
            }

            _objNew.INVOICE_NO = txtInvoice.Text;
            _objNew.MONTH = new DateTime(dtpMonth.Value.Year, dtpMonth.Value.Month, 1);
            _objNew.SOURCE_ID = (int)cboSource.SelectedValue;
            _objNew.TRANSFORMER_ID = (int)cboTransformer.SelectedValue;
            _objNew.POINT_OF_CONNECTION = DataHelper.ParseToLong(cboPointOfConnection.Text);
            _objNew.PRICE = DataHelper.ParseToDecimal(txtPrice.Text);
            _objNew.CURRENCY_ID = (int)cboCurrency.SelectedValue;
            _objNew.UNIT_OF_PRICE = txtUnitPrice.Text;
            _objNew.REMARK = txtRemark.Text;
            _objNew.ATTACHMENT_ID = lblORIGINAL_FILE_NAME.Text == string.Empty ? 0 : _attNew.ATTACHMENT_ID;
            _objNew.ATTACHMENT_URL = _attNew.ATTACHMENT_NAME;
            _objNew.ROW_DATE = DBDataContext.Db.GetSystemDate();
            _objNew.IS_ACTIVE = true;
            // Account
            _objNew.ACCOUNT_ID = (int)cboTransAccount.TreeView.SelectedNode.Tag;
            _objNew.PAYMENT_ACCOUNT_ID = (int)cboPaymentAccount.TreeView.SelectedNode.Tag;
        }

        private void InitailPurchaseDetailProp()
        {
            _oldObjDetail = DBDataContext.Db.TBL_POWER_PURCHASE_DETAILs
                .Where(x => x.POWER_PURCHASE_ID == _objOld.POWER_PURCHASE_ID)
                .Select(x => x);
        }


        private void InitailDataProp()
        {
            UIHelper.DataGridViewProperties(dgv);
            dgv.ReadOnly = false;
            dgv.Columns[USAGE.Name].ReadOnly = true;

            DataGridViewCellStyle style = new DataGridViewCellStyle();
            style.ForeColor = Color.Black;
            style.Font = new Font("Microsoft Sans Serif", 11.0F, FontStyle.Regular);
            dgv.Columns[CONTROL0.Name].HeaderCell.Style = style;
            dt.Columns.Add("POWER_PURCHASE_DETAIL_ID", typeof(int));
            dt.Columns.Add("METER");
            dt.Columns.Add("START_USAGE");
            dt.Columns.Add("END_USAGE");
            dt.Columns.Add("MULTIPLIER", typeof(int));
            dt.Columns.Add("ADJUST_USAGE", typeof(int));
            dt.Columns.Add("IS_REACTIVE", typeof(bool));
            dt.Columns.Add("USAGE");
            dt.Columns.Add("IS_ACTIVE", typeof(bool));

            // add columns to datatable for FTP file
            dtFtp.Columns.Add("ATT_NAME", typeof(string));
            dtFtp.Columns.Add("FILE_NAME", typeof(string));

            // ftp credentail
            ftp.SetCredential(Method.Utilities[Utility.FTP_URL], Method.Utilities[Utility.FTP_USER], Method.Utilities[Utility.FTP_PWD]);
        }

        private bool ValidateDataGridViewProp()
        {
            bool isValid = false;
            dgv.Refresh();
            dgv.EndEdit();
            bool active = false;
            int active_meter = 0;
            int reactive_meter = 0;
            if (dgv.Rows.Count < 1)
            {
                isValid = false;
            }
            if (_flag != GeneralProcess.Delete)
            {
                DataTable dtTemp = new DataTable();
                dtTemp.Columns.Add("METER", typeof(string));

                foreach (DataGridViewRow row in dgv.Rows)
                {
                    var meterCode = row.Cells[METER.Name].Value.ToString();
                    decimal startUsage = DataHelper.ParseToDecimal(row.Cells[START_USAGE.Name].Value.ToString());
                    decimal endUsage = DataHelper.ParseToDecimal(row.Cells[END_USAGE.Name].Value.ToString());
                    //int multiplier = (int)row.Cells[MULTIPLIER.Name].Value;
                    if (row.Cells[METER.Name].Value.ToString().Trim() == string.Empty)
                    {
                        MsgBox.ShowInformation(string.Format(Resources.REQUIRED, Resources.METER_CODE), this.Text);
                        isValid = true;
                        return isValid;
                    }

                    if (endUsage < startUsage || endUsage == 0)
                    {
                        dtTemp.Rows.Add(meterCode);
                    }

                    //if (multiplier < 1)
                    //{
                    //    row.Cells[MULTIPLIER.Name].Value = 1;
                    //    isValid = true;
                    //}

                    if ((bool)row.Cells[IS_REACTIVE.Name].Value == false)
                    {
                        active_meter++;
                        active = true;
                    }
                    else reactive_meter++;

                    //if(DataHelper.ParseToDecimal(row.Cells[START_USAGE.Name].ToString()) < 0 || DataHelper.ParseToDecimal(row.Cells[END_USAGE.Name].ToString())<0 || DataHelper.ParseToDecimal(row.Cells[SURPLUS_POWER.Name].ToString())<0)
                    //{
                    //    MsgBox.ShowInformation(string.Format(Resources.REQUIRED_POSITIVE_NUMBER));
                    //    isValid = true;
                    //}
                }
                if (dtTemp.Rows.Count > 1)
                {
                    var meter = "";
                    foreach (DataRow item in dtTemp.Rows)
                    {
                        meter += "," + item[METER.Name];
                    }

                    if (MsgBox.ShowQuestion(string.Format(Resources.MS_CONFIRM_TO_INSERT_USAGE_POWER_PURCHASE, meter), this.Text) == DialogResult.Yes)
                    {
                        isValid = true;
                        return isValid;
                    }
                }
                if (active == false)
                {
                    MsgBox.ShowInformation(string.Format(Resources.MSG_PLEASE_INPUT_POWER_ACTIVE_FOR_POWER_PURCHASE), Resources.WARNING);
                    isValid = true;
                    return isValid;
                }
                if (active_meter > 3 || reactive_meter > 1)
                {
                    if (reactive_meter > 1)
                        MsgBox.ShowInformation(string.Format(Resources.MSG_CANNOT_ADD_METER_MORE_THAN, "រ៉េអាក់ទីវ", 1), Resources.WARNING);
                    else
                        MsgBox.ShowInformation(string.Format(Resources.MSG_CANNOT_ADD_METER_MORE_THAN, "អាក់ទីវ", 3), Resources.WARNING);
                    isValid = true;
                    return isValid;
                }
            }

            return isValid;
        }

        private void insertObjDetail(TBL_CHANGE_LOG objChangeLog)
        {
            List<int> _newObjDetailId = new List<int>();
            foreach (var item in DBDataContext.Db.TBL_POWER_PURCHASE_DETAILs.Where(x => x.POWER_PURCHASE_ID == _objNew.POWER_PURCHASE_ID && x.IS_ACTIVE))
            {
                TBL_POWER_PURCHASE_DETAIL _newObjDetail = DBDataContext.Db.TBL_POWER_PURCHASE_DETAILs.FirstOrDefault(x => x.POWER_PURCHASE_DETAIL_ID == item.POWER_PURCHASE_DETAIL_ID);
                _newObjDetail.IS_ACTIVE = false;
                DBDataContext.Db.UpdateChild(item, _newObjDetail, _objOld, ref objChangeLog);
            }
            if (_flag == GeneralProcess.Delete) return;
            foreach (DataRow row in dt.Rows)
            {
                TBL_POWER_PURCHASE_DETAIL _newObjDetail = new TBL_POWER_PURCHASE_DETAIL();
                _newObjDetail.POWER_PURCHASE_DETAIL_ID = int.Parse(row[POWER_PURCHASE_DETAIL_ID.DataPropertyName].ToString());
                _newObjDetail.POWER_PURCHASE_ID = _objNew.POWER_PURCHASE_ID;
                _newObjDetail.METER_CODE = row[METER.DataPropertyName].ToString();
                _newObjDetail.START_USAGE = DataHelper.ParseToDecimal(row[START_USAGE.DataPropertyName].ToString());
                _newObjDetail.END_USAGE = DataHelper.ParseToDecimal(row[END_USAGE.DataPropertyName].ToString());
                _newObjDetail.MULTIPLIER = DataHelper.ParseToInt(row[MULTIPLIER.DataPropertyName].ToString());
                _newObjDetail.ADJUST_USAGE = DataHelper.ParseToDecimal(row[SURPLUS_POWER.DataPropertyName].ToString());
                _newObjDetail.POWER_PURCHASE = DataHelper.ParseToDecimal(row[USAGE.DataPropertyName].ToString());
                _newObjDetail.ROW_DATE = DBDataContext.Db.GetSystemDate();
                _newObjDetail.IS_REACTIVE = (bool)(row[IS_REACTIVE.DataPropertyName] ?? false);
                _newObjDetail.IS_ACTIVE = true;
                DBDataContext.Db.InsertChild(_newObjDetail, _objNew, ref objChangeLog);
                _newObjDetailId.Add(_newObjDetail.POWER_PURCHASE_DETAIL_ID);
            }
        }

        private bool UploadToFtp()
        {

            bool completed = false;
            dtFtp.Rows.Add(this.attachmentName, this.path);
            string _strFtpPath = this.getFtpDir();

            try
            {
                Runner.RunNewThread(() =>
                {
                    if (path != string.Empty && ftp.checkFtpConnection())
                    {
                        ftp.createFtpDirectory(_strFtpPath);
                        if (ftp.FtpDirectoryExists(_strFtpPath))
                        {
                            ftp.UploadFilesToFtp(_strFtpPath, dtFtp);
                            completed = true;
                        }
                    }
                }, Resources.PROCESSING);
            }
            catch (Exception err)
            {
                MsgBox.ShowError(err);
            }
            return completed;
        }

        private void removeFtpFile()
        {
            string _strFtpPath = getFtpDir();
            string _strFtpFile = Path.Combine(_strFtpPath, txtAttachmentName.Text);
            try
            {
                Runner.RunNewThread(() =>
                {
                    if (ftp.checkFtpConnection())
                    {
                        ftp.DeleteFtpFile(_strFtpFile);
                    }
                }, Resources.PROCESSING);
            }
            catch (Exception err)
            {
                MsgBox.ShowError(err);
            }

        }

        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();
            if (cboTransAccount.TreeView.SelectedNode == null)
            {
                cboTransAccount.SetValidation(string.Format(Resources.REQUIRED, lblEXPENSE_ACCOUNT.Text));
                val = true;
            }
            if (cboPaymentAccount.TreeView.SelectedNode == null)
            {
                cboPaymentAccount.SetValidation(string.Format(Resources.REQUIRED, lblPAYMENT_ACCOUNT.Text));
                val = true;
            }
            if (cboSource.SelectedIndex == -1)
            {
                cboSource.SetValidation(string.Format(Resources.REQUIRED, lblSELLER.Text));
                val = true;
            }
            if (cboTransformer.SelectedIndex == -1)
            {
                cboTransformer.SetValidation(string.Format(Resources.REQUIRED, lblTRANSFORMER.Text));
                val = true;
            }
            if (!DataHelper.IsNumber(txtPrice.Text))
            {
                txtPrice.SetValidation(Resources.REQUIRED_INPUT_NUMBER);
                val = true;
            }
            if (cboCurrency.SelectedIndex == -1)
            {
                cboCurrency.SetValidation(string.Format(Resources.REQUIRED, lblCURRENCY.Text));
                val = true;
            }
            if (DataHelper.ParseToInt(txtPrice.Text) < 0)
            {
                txtPrice.SetValidation(Resources.REQUIRED_POSITIVE_NUMBER);
                val = true;
            }
            if (cboPointOfConnection.Text == "" || DataHelper.ParseToLong(cboPointOfConnection.Text) == 0)
            {
                cboPointOfConnection.SetValidation(string.Format(Resources.REQUIRED, lblPOINT_OF_CONNECTION.Text));
                val = true;
            }
            return val;
        }

        private bool checkExist()
        {
            if (_flag == GeneralProcess.Delete)
            {
                return false;
            }
            DateTime date = new DateTime(dtpMonth.Value.Year, dtpMonth.Value.Month, 1);
            foreach (DataGridViewRow row in dgv.Rows)
            {
                var lst = from pp in DBDataContext.Db.TBL_POWER_PURCHASEs
                          join ppd in DBDataContext.Db.TBL_POWER_PURCHASE_DETAILs on pp.POWER_PURCHASE_ID equals ppd.POWER_PURCHASE_ID
                          where pp.IS_ACTIVE
                                && pp.TRANSFORMER_ID == (int)cboTransformer.SelectedValue
                                && pp.MONTH.Date == date.Date
                                && ppd.METER_CODE.Trim().Replace(" ", "") == row.Cells[METER.Name].Value.ToString().Trim().Replace(" ", "")
                                && pp.POINT_OF_CONNECTION == DataHelper.ParseToLong(cboPointOfConnection.Text)
                                && pp.POWER_PURCHASE_ID != _objOld.POWER_PURCHASE_ID
                          select new { pp.POWER_PURCHASE_ID };

                if (lst.Count() > 0)
                {
                    return true;
                }
            }
            return false;
        }

        private string getFtpDir()
        {
            // ftp directory name
            var monthFormat = dtpMonth.Value.Month < 10 ? "0" + dtpMonth.Value.Month : dtpMonth.Value.Month.ToString();
            var powerPurchaseLicenseeId = DBDataContext.Db.TBL_POWER_SOURCEs.FirstOrDefault(x => x.SOURCE_ID == (int)cboSource.SelectedValue).POWER_PURCHASE_LICENSEE_ID;
            var powerPurchaseLicensee = DBDataContext.Db.TBL_POWER_PURCHASE_LICENSEEs.FirstOrDefault(x => x.POWER_PURCHASE_LICENSEE_ID == powerPurchaseLicenseeId);
            branch_code = powerPurchaseLicensee.BRANCH_CODE;
            return _ftpPath = string.Format("{0}/{1}/{2}/{3}",
                Method.Utilities[Utility.B24_EAC_CUSTOMER_CODE],
                dtpMonth.Value.Year, monthFormat, powerPurchaseLicensee.BRANCH_CODE);
        }

        private void addNewRow()
        {
            dt.Clear();
            DateTime date = dtpMonth.Value.Month == 1 ? new DateTime(dtpMonth.Value.Year - 1, 12, 1) :
                new DateTime(dtpMonth.Value.Year, dtpMonth.Value.Month - 1, 1);

            DataRow dr;
            var dtMeter = (from p in DBDataContext.Db.TBL_POWER_PURCHASEs
                           join pd in DBDataContext.Db.TBL_POWER_PURCHASE_DETAILs
                           on p.POWER_PURCHASE_ID equals pd.POWER_PURCHASE_ID
                           where p.IS_ACTIVE && pd.IS_ACTIVE
                             && p.POINT_OF_CONNECTION == DataHelper.ParseToLong(cboPointOfConnection.Text)
                             && p.MONTH == date
                           select new
                           {
                               p.POINT_OF_CONNECTION,
                               pd.METER_CODE,
                               pd.START_USAGE,
                               pd.END_USAGE,
                               pd.MULTIPLIER,
                               pd.ADJUST_USAGE,
                               pd.POWER_PURCHASE
                           });
            if (dtMeter.Count() > 0)
            {
                foreach (var meter in dtMeter)
                {
                    dr = dt.NewRow();
                    dr[POWER_PURCHASE_DETAIL_ID.DataPropertyName] = 0;
                    dr[METER.DataPropertyName.ToString()] = meter.METER_CODE.ToString();
                    dr[START_USAGE.DataPropertyName.ToString()] = DataHelper.ParseToDecimal(meter.END_USAGE.ToString());
                    dr[END_USAGE.DataPropertyName.ToString()] = 0;
                    dr[MULTIPLIER.DataPropertyName.ToString()] = meter.MULTIPLIER;
                    dr[SURPLUS_POWER.DataPropertyName.ToString()] = 0;
                    dr[IS_REACTIVE.DataPropertyName] = false;
                    dr[USAGE.DataPropertyName.ToString()] = 0;
                    dt.Rows.Add(dr);
                }
                dgv.DataSource = dt;

                var connectionPoint = dtMeter.FirstOrDefault().POINT_OF_CONNECTION;
            }
            else
            {
                dt.Rows.Add(0, null, 0, 0, 1, 0, false, 0);
                dgv.DataSource = dt;
            }
        }

        private void addNewRows()
        {
            dt.Clear();
            DateTime date = dtpMonth.Value.Month == 1 ? new DateTime(dtpMonth.Value.Year - 1, 12, 1) :
                new DateTime(dtpMonth.Value.Year, dtpMonth.Value.Month - 1, 1);

            DataRow dr;
            var dtMeter = (from p in DBDataContext.Db.TBL_POWER_PURCHASEs
                           join pd in DBDataContext.Db.TBL_POWER_PURCHASE_DETAILs
                           on p.POWER_PURCHASE_ID equals pd.POWER_PURCHASE_ID
                           where p.IS_ACTIVE && pd.IS_ACTIVE
                             && p.POINT_OF_CONNECTION == DataHelper.ParseToLong(cboPointOfConnection.Text)
                             && p.POINT_OF_CONNECTION != 1
                             && p.MONTH == date
                           select new
                           {
                               p.POINT_OF_CONNECTION,
                               pd.METER_CODE,
                               pd.START_USAGE,
                               pd.END_USAGE,
                               pd.MULTIPLIER,
                               pd.ADJUST_USAGE,
                               pd.POWER_PURCHASE
                           });
            if (dtMeter.Count() > 0)
            {
                foreach (var meter in dtMeter)
                {
                    dr = dt.NewRow();
                    dr[POWER_PURCHASE_DETAIL_ID.DataPropertyName] = 0;
                    dr[METER.DataPropertyName.ToString()] = meter.METER_CODE.ToString();
                    dr[START_USAGE.DataPropertyName.ToString()] = DataHelper.ParseToDecimal(meter.END_USAGE.ToString());
                    dr[END_USAGE.DataPropertyName.ToString()] = 0;
                    dr[MULTIPLIER.DataPropertyName.ToString()] = meter.MULTIPLIER;
                    dr[SURPLUS_POWER.DataPropertyName.ToString()] = 0;
                    dr[IS_REACTIVE.DataPropertyName] = false;
                    dr[USAGE.DataPropertyName.ToString()] = 0;
                    dt.Rows.Add(dr);
                    //nMeter++;
                }
                dgv.DataSource = dt;

                var connectionPoint = dtMeter.FirstOrDefault().POINT_OF_CONNECTION;
            }
            else
            {
                dt.Rows.Add(0, null, 0, 0, 1, 0, false, 0.00);
                dgv.DataSource = dt;
                //nMeter++;
            }
        }

        #endregion

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            if (dgv.Rows.Count <= 0)
            {
                MsgBox.ShowInformation(string.Format(Resources.REQUIRED, Resources.POWER_PURCHASE));
                return;
            }

            if (checkExist())
            {
                MsgBox.ShowInformation(string.Format(Resources.MS_IS_EXISTS, Resources.METER_CODE));
                return;
            }

            if (DBDataContext.Db.TBL_POWER_SOURCEs.FirstOrDefault(x => x.SOURCE_ID == (int)cboSource.SelectedValue).POWER_PURCHASE_LICENSEE_ID == 0)
            {
                MsgBox.ShowInformation(Resources.MSG_YOU_ARE_NOT_YET_SETUP_POWER_PURCHASE_LICENSEE);
                return;
            }

            if (ValidateDataGridViewProp())
            {
                return;
            }

            if (txtAttachmentName.Text == "" && _flag != GeneralProcess.Delete)
            {
                if (MsgBox.ShowQuestion(Resources.MS_ATTACHMENT_FILE_IS_NOT_KNOWN, string.Empty) == DialogResult.No)
                    return;

            }

            write();

            try
            {

                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    TBL_CHANGE_LOG objChangeLog = null;
                    if (_flag == GeneralProcess.Insert)
                    {
                        if (UploadToFtp())
                        {
                            DBDataContext.Db.Insert(_objNew);
                        }
                        else
                        {
                            DBDataContext.Db.Insert(_objNew);
                        }
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        if (UploadToFtp())
                        {
                            DBDataContext.Db.Update(_objOld, _objNew);
                        }
                        else
                        {
                            DBDataContext.Db.Update(_objOld, _objNew);
                        }
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        if (_objNew.ATTACHMENT_ID == 0)
                        {
                            DBDataContext.Db.Delete(_objNew);
                        }
                        else
                        {
                            DBDataContext.Db.Delete(_objNew);
                            removeFtpFile();
                        }
                    }
                    insertObjDetail(objChangeLog);
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void ChangeEnglishKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }

        private void cboTransformer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.load || cboTransformer.SelectedIndex == -1)
            {
                return;
            }
        }

        private void cboSource_SelectedIndexChanged(object sender, EventArgs e)
        {
            DateTime date = dtpMonth.Value.Month == 1 ? new DateTime(dtpMonth.Value.Year - 1, 12, 1) : new DateTime(dtpMonth.Value.Year, dtpMonth.Value.Month - 1, 1);
            if (this.load || cboSource.SelectedIndex == -1)
            {
                return;
            }
            TBL_POWER_SOURCE objSrc = DBDataContext.Db.TBL_POWER_SOURCEs.FirstOrDefault(x => x.SOURCE_ID == (int)cboSource.SelectedValue && x.IS_ACTIVE);
            if (objSrc == null)
            {
                return;
            }
            cboCurrency.SelectedValue = objSrc.CURRENCY_ID;
            txtPrice.Text = (int)cboCurrency.SelectedValue == 1 ? objSrc.PRICE.ToString("N5") : objSrc.PRICE.ToString("N5");
        }

        private void dtpMonth_ValueChanged(object sender, EventArgs e)
        {
            cboSource_SelectedIndexChanged(cboSource, EventArgs.Empty);
        }

        private void dgv_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            decimal start, end, adjust, multi;
            if (!load)
            {
                if (e.ColumnIndex == dgv.CurrentRow.Cells[METER.Name].ColumnIndex
                    || e.ColumnIndex == dgv.CurrentRow.Cells[START_USAGE.Name].ColumnIndex
                    || e.ColumnIndex == dgv.CurrentRow.Cells[END_USAGE.Name].ColumnIndex
                    || e.ColumnIndex == dgv.CurrentRow.Cells[MULTIPLIER.Name].ColumnIndex
                    || e.ColumnIndex == dgv.CurrentRow.Cells[SURPLUS_POWER.Name].ColumnIndex)
                {
                    // if enable this code user will not able to edit start usage or multiplier
                    //if (dgv.CurrentRow.Cells[METER.Name].Value != null)
                    //{
                    //    DateTime date = dtpMonth.Value.Month == 1 ? new DateTime(dtpMonth.Value.Year - 1, 12, 1)
                    //        : new DateTime(dtpMonth.Value.Year, dtpMonth.Value.Month - 1, 1);
                    //    var objPur = (from p in DBDataContext.Db.TBL_POWER_PURCHASEs
                    //                  join pd in DBDataContext.Db.TBL_POWER_PURCHASE_DETAILs on p.POWER_PURCHASE_ID equals pd.POWER_PURCHASE_ID
                    //                  where pd.METER_CODE == dgv.CurrentRow.Cells[METER.Name].Value.ToString()
                    //                      && p.MONTH == date
                    //                      && p.IS_ACTIVE
                    //                  select new { pd.END_USAGE, pd.MULTIPLIER }).FirstOrDefault();

                    //if (objPur != null)
                    //{
                    //    dgv.CurrentRow.Cells[START_USAGE.Name].Value = objPur.END_USAGE.ToString("N0");
                    //    dgv.CurrentRow.Cells[MULTIPLIER.Name].Value = objPur.MULTIPLIER.ToString();
                    //}
                    //}

                    start = DataHelper.ParseToDecimal(dgv.CurrentRow.Cells[START_USAGE.Name].Value.ToString());
                    end = DataHelper.ParseToDecimal(dgv.CurrentRow.Cells[END_USAGE.Name].Value.ToString());
                    adjust = DataHelper.ParseToDecimal(dgv.CurrentRow.Cells[SURPLUS_POWER.Name].Value.ToString());
                    multi = DataHelper.ParseToDecimal((int)dgv.CurrentRow.Cells[MULTIPLIER.Name].Value < 1 ? "1" : dgv.CurrentRow.Cells[MULTIPLIER.Name].Value.ToString());
                    dgv.CurrentRow.Cells[USAGE.Name].Value = calculateUsage(start, end, adjust, multi);
                }
            }
        }

        private decimal calculateUsage(decimal startUsage, decimal endUsage, decimal adjustUsage, decimal multiplier)
        {
            decimal usage, result;
            if (multiplier < 1)
            {
                multiplier = 1;
            }
            usage = Method.GetTotalUsage(startUsage, endUsage, multiplier, endUsage < startUsage);
            return result = DataHelper.ParseToDecimal((usage + adjustUsage).ToString("N2"));
        }

        private void dgvShow_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (!load)
            {
                //DataGridViewTextBoxEditingControl tb = (DataGridViewTextBoxEditingControl)e.Control;
                e.Control.KeyPress += new KeyPressEventHandler(dgv_KeyPress);
            }
        }

        private void dgv_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (dgv.CurrentCell.ColumnIndex != dgv.CurrentRow.Cells[METER.Name].ColumnIndex)
            {
                UIHelper.InputDecimalOnly(sender, e);
            }

        }

        private void btnOPEN_FILE_Click(object sender, EventArgs e)
        {

            if (!inValid())
            {
                if (DBDataContext.Db.TBL_POWER_SOURCEs.FirstOrDefault(x => x.SOURCE_ID == (int)cboSource.SelectedValue).POWER_PURCHASE_LICENSEE_ID == 0)
                {
                    MsgBox.ShowInformation(Resources.MSG_YOU_ARE_NOT_YET_SETUP_POWER_PURCHASE_LICENSEE);
                    return;
                }
                using (OpenFileDialog dialog = new OpenFileDialog()
                {
                    Filter = "Image file |*.png; *.jpg; *.jpeg; *.jpe; |PDF file |*.pdf |All file | *.*",
                    FileName = "",
                    Multiselect = false
                })
                {
                    if (dialog.ShowDialog() == DialogResult.OK)
                    {
                        var fileName = Path.GetFileName(dialog.FileName);
                        path = dialog.FileName;
                        var fileExt = Path.GetExtension(dialog.FileName);
                        branch_code = "";
                        attachmentName = string.Format("{0}-{1}{2}{3}",
                            branch_code = cboSource.SelectedValue == null ? "" : (from ppl in DBDataContext.Db.TBL_POWER_PURCHASE_LICENSEEs
                                                                                  join ps in DBDataContext.Db.TBL_POWER_SOURCEs on ppl.POWER_PURCHASE_LICENSEE_ID equals ps.POWER_PURCHASE_LICENSEE_ID
                                                                                  where ps.SOURCE_ID == (int)cboSource.SelectedValue
                                                                                  select ppl.BRANCH_CODE).FirstOrDefault().ToString(),
                            txtInvoice.Text,
                            DBDataContext.Db.GetSystemDate().ToString("yyyyMMddHHmmss"),
                            fileExt);
                        //if (_flag == GeneralProcess.Update && _objNew.ATTACHMENT_ID != 0)
                        //{
                        //    attachmentName = this.txtAttachmentName.Text;
                        //}
                        _attNew.ATTACHMENT = File.ReadAllBytes(path);
                        this.lblORIGINAL_FILE_NAME.Text = fileName;
                        this.lblORIGINAL_FILE_NAME.Tag = fileName;
                        var AttachmentName = attachmentName.Replace(@"\", "-");
                        this.txtAttachmentName.Text = AttachmentName.Replace(@"/", "-");
                    }
                }
            }
            else
                this.ClearValidation();
        }

        private void dgv_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }


        private void dgv_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex == dgv.Columns[CONTROL0.Name].Index)
            {
                if (!inValid())
                {
                    if (dgv.Rows.Count >= 4)
                    {
                        MsgBox.ShowInformation(string.Format(Resources.MSG_CANNOT_ADD_METER_MORE_THAN, "", 4), Resources.WARNING);
                    }
                    else
                    {
                        dt.Rows.Add(0, null, 0, 0, 1, 0, false, 0.00, true);
                        dgv.DataSource = dt;
                        //nMeter++;
                    }
                }
                else
                {
                    this.ClearValidation();
                }
            }
        }

        private void lblORIGINAL_FILE_NAME_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (_attNew.ATTACHMENT != null)
            {
                Method.OpenFile(this.lblORIGINAL_FILE_NAME.Text, _attNew.ATTACHMENT);
            }
        }

        private void cbotxtPointOfConnection_MouseLeave(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            if (cb.Text == null)
            {
                MsgBox.ShowError(Resources.MS_PLEASE_SELECT_POINT_OF_CONNECTION, Resources.INFORMATION);
            }
        }

        private void cbotxtPointOfConnection_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                var transfo = (from p in DBDataContext.Db.TBL_POWER_PURCHASEs
                               join t in DBDataContext.Db.TBL_TRANSFORMERs on p.TRANSFORMER_ID equals t.TRANSFORMER_ID
                               where p.POINT_OF_CONNECTION == DataHelper.ParseToLong(cboPointOfConnection.Text) && t.IS_ACTIVE == true
                               orderby p.MONTH descending
                               select new { t.TRANSFORMER_CODE, t.TRANSFORMER_ID }).ToList().FirstOrDefault();

                if (_flag == GeneralProcess.Insert)
                {
                    if (transfo != null)
                    {
                        //UIHelper.SetDataSourceToComboBox(cboTransformer, DBDataContext.Db.TBL_TRANSFORMERs.Where(x => x.IS_ACTIVE).OrderBy(x => x.TRANSFORMER_CODE));
                        cboTransformer.SelectedValue = transfo.TRANSFORMER_ID;
                    }
                    else if (transfo == null)
                    {
                        UIHelper.SetDataSourceToComboBox(cboTransformer, DBDataContext.Db.TBL_TRANSFORMERs.Where(x => x.IS_ACTIVE).OrderBy(x => x.TRANSFORMER_CODE), "");
                    }

                    this.addNewRows();
                }
            }
        }

        private void cboPointOfConnection_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.load || cboPointOfConnection.SelectedIndex == -1)
            {
                return;
            }
            else
            {
                var transfo = (from p in DBDataContext.Db.TBL_POWER_PURCHASEs
                               join t in DBDataContext.Db.TBL_TRANSFORMERs on p.TRANSFORMER_ID equals t.TRANSFORMER_ID
                               where p.POINT_OF_CONNECTION == DataHelper.ParseToLong(cboPointOfConnection.Text)
                               select new { t.TRANSFORMER_CODE, t.TRANSFORMER_ID }).ToList().FirstOrDefault();

                if (_flag == GeneralProcess.Insert)
                {
                    if (transfo != null)
                    {
                        //UIHelper.SetDataSourceToComboBox(cboTransformer, DBDataContext.Db.TBL_TRANSFORMERs.Where(x => x.IS_ACTIVE).OrderBy(x => x.TRANSFORMER_CODE));
                        cboTransformer.SelectedValue = transfo.TRANSFORMER_ID;
                    }
                    else if (transfo == null)
                    {
                        UIHelper.SetDataSourceToComboBox(cboTransformer, DBDataContext.Db.TBL_TRANSFORMERs.Where(x => x.IS_ACTIVE).OrderBy(x => x.TRANSFORMER_CODE), "");
                    }

                    this.addNewRows();
                }
            }

        }

        private void DialogPowerPurchase_Load(object sender, EventArgs e)
        {
            this.lblORIGINAL_FILE_NAME.Text = Path.GetFileName(_attNew.FILE_NAME);
            this.lblORIGINAL_FILE_NAME.Tag = Path.GetFileName(_attNew.FILE_NAME);
        }

        private void dgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgv.Rows.Count > 0 && e.RowIndex != -1)
            {
                var ind = e.RowIndex;
                if (e.ColumnIndex == dgv.CurrentRow.Cells[CONTROL0.Name].ColumnIndex)
                {
                    dt.Rows.RemoveAt(dgv.CurrentRow.Index);
                    dgv.DataSource = dt;
                    //nMeter--;
                }
            }
        }

        private void InputOnlyNumber(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        }
    }
}