﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogLicensee : ExDialog
    {
        GeneralProcess _flag;

        TBL_LICENSEE _objNew = new TBL_LICENSEE();

        public TBL_LICENSEE Licensee
        {
            get { return _objNew; }
        }
        TBL_LICENSEE _objOld = new TBL_LICENSEE();

        #region Constructor
        public DialogLicensee(GeneralProcess flag, TBL_LICENSEE obj)
        {
            InitializeComponent();
            _flag = flag;
            obj._CopyTo(_objNew);
            obj._CopyTo(_objOld);

            this.Text = _flag.GetText(this.Text);
            UIHelper.SetDataSourceToComboBox(cboVoltage, Lookup.GetPowerVoltage());
            UIHelper.SetDataSourceToComboBox(cboCurrency, Lookup.GetCurrencies());
            UIHelper.SetEnabled(this, !(flag == GeneralProcess.Delete));
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
            read();
        }
        #endregion

        #region Operation
        /// <summary>
        /// Data transaction
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
            //if duplicate record.
            txtNo.ClearAllValidation();
            if (DBDataContext.Db.IsExits(_objNew, "LICENSEE_NO"))
            {
                txtNo.SetValidation(string.Format(Resources.MS_IS_EXISTS, lblLICENSE_NO.Text));
                return;
            }
            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objNew);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objNew);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }

        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        #endregion

        #region Method
        /// <summary>
        /// Load data from object.
        /// </summary>
        private void read()
        {
            txtNo.Text = _objNew.LICENSEE_NO;
            txtName.Text = _objNew.LICENSEE_NAME;
            cboVoltage.SelectedValue = _objNew.VOLTAGE_ID == 0 ? 1 : _objNew.VOLTAGE_ID;
            txtTariff.Text = _objNew.CURRENCY_ID == 1 ? _objNew.TARIFF.ToString("0") : _objNew.TARIFF.ToString();
            txtLocation.Text = _objNew.LOCATION;
            cboCurrency.SelectedValue = _objNew.CURRENCY_ID == 0 ? 1 : _objNew.CURRENCY_ID;
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            _objNew.LICENSEE_NO = txtNo.Text.Trim();
            _objNew.LICENSEE_NAME = txtName.Text;
            _objNew.VOLTAGE_ID = (int)cboVoltage.SelectedValue;
            _objNew.LOCATION = txtLocation.Text;
            _objNew.CURRENCY_ID = (int)cboCurrency.SelectedValue;
            _objNew.TARIFF = DataHelper.ParseToDecimal(txtTariff.Text);
        }

        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();
            if (txtNo.Text.Trim() == string.Empty)
            {
                txtNo.SetValidation(string.Format(Resources.REQUIRED, lblLICENSE_NO.Text));
                val = true;
            }
            if (txtName.Text.Trim() == string.Empty)
            {
                txtName.SetValidation(string.Format(Resources.REQUIRED, lblLICENSE_NAME.Text));
                val = true;
            }
            if (cboVoltage.SelectedIndex == -1)
            {
                cboVoltage.SetValidation(string.Format(Resources.REQUIRED, lblVOLTAGE.Text));
                val = true;
            }
            if (!DataHelper.IsNumber(txtTariff.Text))
            {
                txtTariff.SetValidation(Resources.REQUIRED_INPUT_NUMBER);
                val = true;
            }
            if (DataHelper.ParseToDecimal(txtTariff.Text) < 0)
            {
                txtTariff.SetValidation(Resources.REQUIRED_POSITIVE_NUMBER);
                val = true;
            }
            if (txtLocation.Text.Trim() == string.Empty)
            {
                txtLocation.SetValidation(string.Format(Resources.REQUIRED, lblLOCATION.Text));
                val = true;
            }
            if (cboCurrency.SelectedIndex == -1)
            {
                cboCurrency.SetValidation(string.Format(Resources.REQUIRED_SELECT_VALUE, lblCURRENCY.Text));
                val = true;
            }
            return val;
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }
        #endregion

        private void txt_EnterKh(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
    }
}