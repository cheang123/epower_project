﻿using EPower.Base.Helper;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PagePowerPurchase : Form
    {
        DataTable dt = new DataTable();
        public TBL_POWER_PURCHASE PowerPurchase
        {
            get
            {
                TBL_POWER_PURCHASE objPurchasePower = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    //var k =  (int)dgv.SelectedRows[0].Cells[POWER_PURCHASE_ID.Name].Value;
                    //var kk = (from s in DBDataContext.Db.TBL_POWER_PURCHASEs where s.POWER_PURCHASE_ID == k select s).FirstOrDefault();
                    objPurchasePower = DBDataContext.Db.TBL_POWER_PURCHASEs.FirstOrDefault(x => x.POWER_PURCHASE_ID == (int)dgv.SelectedRows[0].Cells[POWER_PURCHASE_ID.Name].Value);
                }
                return objPurchasePower;
            }
        }

        public TBL_ATTACHMENT Attachment
        {
            get
            {
                TBL_ATTACHMENT objAttachment = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    objAttachment = DBDataContext.Db.TBL_ATTACHMENTs.FirstOrDefault(x => x.ATTACHMENT_ID == PowerPurchase.ATTACHMENT_ID);
                }
                return objAttachment;
            }
        }

        #region Constructor
        public PagePowerPurchase()
        {
            InitializeComponent();

            this.btnREPORT.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_QUARTERLY_REPORT]);

            UIHelper.DataGridViewProperties(dgv);
            BindPowerSource();
            DateTime dt = new DateTime();
            dt = DBDataContext.Db.GetSystemDate();
            dtpDate.Value = new DateTime(dt.Year, dt.Month, 1);
            load();
            BindReport();
        }

        public void BindPowerSource()
        {
            try
            {
                int backup = (int)(this.cboPowerSource.SelectedValue ?? 0);
                UIHelper.SetDataSourceToComboBox(this.cboPowerSource, DBDataContext.Db.TBL_POWER_SOURCEs.Where(row => row.IS_ACTIVE), Resources.ALL_POWER_SOURCE);
                this.cboPowerSource.SelectedValue = backup;
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }
        #endregion

        #region Method

        private void load()
        {
            int source_id = (int)cboPowerSource.SelectedValue;
            DateTime date = new DateTime(dtpDate.Value.Year, dtpDate.Value.Month, 1);

            var PowerPurchase = from pp in DBDataContext.Db.TBL_POWER_PURCHASEs
                                join ppd in DBDataContext.Db.TBL_POWER_PURCHASE_DETAILs on pp.POWER_PURCHASE_ID equals ppd.POWER_PURCHASE_ID
                                join ps in DBDataContext.Db.TBL_POWER_SOURCEs on pp.SOURCE_ID equals ps.SOURCE_ID
                                join t in DBDataContext.Db.TBL_TRANSFORMERs on pp.TRANSFORMER_ID equals t.TRANSFORMER_ID into tj
                                from tr in tj.DefaultIfEmpty()
                                where pp.IS_ACTIVE && ppd.IS_ACTIVE && !ppd.IS_REACTIVE
                                && (pp.MONTH == date)
                                && (source_id == 0 || ps.SOURCE_ID == source_id)
                                && (pp.INVOICE_NO + ps.SELLER_NAME + (tr == null ? "" : tr.TRANSFORMER_CODE)/* + METER_CODE*/).ToLower().Contains(txtQuickSearch.Text.ToLower())
                                select new
                                {
                                    pp.POWER_PURCHASE_ID,
                                    pp.SOURCE_ID,
                                    pp.MONTH,
                                    ps.SELLER_NAME,
                                    pp.PRICE,
                                    pp.UNIT_OF_PRICE,
                                    TRANSFORMER_CODE = tr == null ? "" : tr.TRANSFORMER_CODE,
                                    pp.INVOICE_NO,
                                    ppd.METER_CODE,
                                    ppd.START_USAGE,
                                    ppd.END_USAGE,
                                    ppd.MULTIPLIER,
                                    ppd.ADJUST_USAGE,
                                    POWER_PURCHASE = ppd.POWER_PURCHASE
                                };

            var rs = PowerPurchase.Select(x => x).GroupBy(x => new { x.POWER_PURCHASE_ID, })
                .Select(x => new
                {
                    POWER_PURCHASE_ID = x.FirstOrDefault().POWER_PURCHASE_ID,
                    SOURCE_ID = x.FirstOrDefault().SOURCE_ID,
                    MONTH = x.FirstOrDefault().MONTH,
                    SELLER_NAME = x.FirstOrDefault().SELLER_NAME,
                    TRANSFORMER_CODE = x.FirstOrDefault().TRANSFORMER_CODE,
                    INVOICE_NO = x.FirstOrDefault().INVOICE_NO,
                    PRICE = x.FirstOrDefault().PRICE,
                    UNIT_OF_PRICE = x.FirstOrDefault().UNIT_OF_PRICE,
                    POWER_PURCHASE = x.Sum(s => s.POWER_PURCHASE)
                });

            dt.Clear();
            DataRow dr;
            dgv.Rows.Clear();
            foreach (var item in rs)
            {
                dgv.Rows.Add(item.POWER_PURCHASE_ID, item.SOURCE_ID, item.MONTH, item.SELLER_NAME.ToString(), item.TRANSFORMER_CODE.ToString(),
                    item.INVOICE_NO.ToString(), item.PRICE.ToString(), item.UNIT_OF_PRICE.ToString(), item.POWER_PURCHASE.ToString(UIHelper._DefaultUsageFormat));
            }
        }
        #endregion

        private void btnNew_Click(object sender, EventArgs e)
        {
            var objDefaultPaymentAcc = AccountChartHelper.GetAccounts(AccountConfig.PAYMENT_DEFAULT_ACCOUNTS).FirstOrDefault();
            var objDefaultAccount = AccountChartHelper.GetAccounts(AccountConfig.POWER_PURCHASE_ACCOUNT).FirstOrDefault();
            DialogPowerPurchase dig = new DialogPowerPurchase(GeneralProcess.Insert, new TBL_POWER_PURCHASE()
            {
                MONTH = DateTime.Now,
                INVOICE_NO = string.Empty,
                REMARK = string.Empty,
                PAYMENT_ACCOUNT_ID = objDefaultPaymentAcc.ACCOUNT_ID,
                ACCOUNT_ID = objDefaultAccount.ACCOUNT_ID,
                CURRENCY_ID = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.IS_DEFAULT_CURRENCY).CURRENCY_ID
            },
            new TBL_ATTACHMENT());
            if (dig.ShowDialog() == DialogResult.OK)
            {
                load();
                UIHelper.SelectRow(dgv, POWER_PURCHASE_ID.Name, dig.PowerPurchase.POWER_PURCHASE_ID);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (PowerPurchase == null)
            {
                return;
            }
            DialogPowerPurchase dig = new DialogPowerPurchase(GeneralProcess.Update, PowerPurchase, Attachment);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                load();
                UIHelper.SelectRow(dgv, POWER_PURCHASE_ID.Name, dig.PowerPurchase.POWER_PURCHASE_ID);
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (PowerPurchase == null)
            {
                return;
            }
            DialogPowerPurchase dig = new DialogPowerPurchase(GeneralProcess.Delete, PowerPurchase, Attachment);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                load();
                UIHelper.SelectRow(dgv, dig.PowerPurchase.POWER_PURCHASE_ID - 1);
            }
        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            var diag = new DialogReportQuarterOptions(false);
            diag.chkTABLE_POWER_PURCHASE.Checked = true;
            if (diag.ShowDialog() == DialogResult.OK)
            {
                var doc = PageReportQuaterly.OpenReport(diag);
                doc.ViewReport("");
            }

        }

        private void dtpDate_ValueChanged(object sender, EventArgs e)
        {
            load();
        }

        private void cboPowerSource_SelectedIndexChanged(object sender, EventArgs e)
        {
            load();
        }

        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void btnReportPowerPurchase_Click(object sender, EventArgs e)
        {
            if (this.PowerPurchase == null)
            {
                return;
            }
            DialogReportPowerPurchaseLicenseFee diag = new DialogReportPowerPurchaseLicenseFee(this.PowerPurchase, (int)cboPowerSource.SelectedValue, cboPowerSource.Text);
            diag.ShowDialog();
        }

        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }

        private void btnPAYMENT_LICENSEE_FEE_Click(object sender, EventArgs e)
        {
            string url = Method.Utilities[Utility.B24_EAC_SUPPLIER_API_URL] + Method.Utilities[Utility.B24_EAC_SUPPLIER_API_KEY] + ":" + Method.Utilities[Utility.B24_EAC_CUSTOMER_CODE].Replace("-", "");
            //new DialogWebView(url).ShowDialog();
            System.Diagnostics.Process.Start(url);
        }

        private void btnHISTORY_SEND_REPORT_Click(object sender, EventArgs e)
        {
            DialogHistorySendReport digHistory = new DialogHistorySendReport(dtpDate.Value);
            if (dgv.SelectedRows.Count==0)
            {
                digHistory.ShowDialog();
            }
            else
            {
                var month = (DateTime)dgv.SelectedRows[0].Cells[MONTH.Name].Value;
                DialogHistorySendReport dig = new DialogHistorySendReport(month);
                dig.ShowDialog();
            }
        }

        private void dtpDate_ValueChanged_1(object sender, EventArgs e)
        {
            load();
        }
        private void BindReport()
        {
            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("REPORT_ID", typeof(int));
            dtReport.Columns.Add("REPORT_NAME", typeof(string));
            dtReport.Rows.Add(0, Resources.REPORT_POWER_PURCHASE);
            UIHelper.SetDataSourceToComboBox(cboReport, dtReport, "REPORT_ID", "REPORT_NAME");
            cboReport.SelectedIndex = 0;
        }

        private void cboReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (dgv.SelectedRows.Count == 0)
            //{
            //    return;
            //}

            //if (cboReport.SelectedIndex == 0)
            //{
            //    if (this.PowerPurchase == null)
            //    {
            //        return;
            //    }
            //    DialogReportPowerPurchaseLicenseFee diag = new DialogReportPowerPurchaseLicenseFee(this.PowerPurchase, (int)cboPowerSource.SelectedValue, cboPowerSource.Text);
            //    diag.ShowDialog();
            //}
        }
    }
}
