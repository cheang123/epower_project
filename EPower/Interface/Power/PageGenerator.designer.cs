﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageGenerator
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageGenerator));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cboFuleType = new System.Windows.Forms.ComboBox();
            this.cboCapacity = new System.Windows.Forms.ComboBox();
            this.txtQuickSearch = new SoftTech.Component.ExTextbox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnREPORT = new SoftTech.Component.ExButton();
            this.btnREMOVE = new SoftTech.Component.ExButton();
            this.btnEDIT = new SoftTech.Component.ExButton();
            this.btnADD = new SoftTech.Component.ExButton();
            this.btnGENERATION_OPERATION_TIME = new SoftTech.Component.ExButton();
            this.GENERATOR_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GENERATOR_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CAPACITY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HORSE_POWER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OPERATION_HOUR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FUEL_TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.START_USE_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.END_USE_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_IN_USED = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.GENERATOR_ID,
            this.GENERATOR_NO,
            this.CAPACITY,
            this.HORSE_POWER,
            this.OPERATION_HOUR,
            this.FUEL_TYPE,
            this.START_USE_DATE,
            this.END_USE_DATE,
            this.IS_IN_USED});
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.cboFuleType);
            this.panel1.Controls.Add(this.cboCapacity);
            this.panel1.Controls.Add(this.txtQuickSearch);
            this.panel1.Controls.Add(this.flowLayoutPanel1);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // cboFuleType
            // 
            this.cboFuleType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFuleType.FormattingEnabled = true;
            resources.ApplyResources(this.cboFuleType, "cboFuleType");
            this.cboFuleType.Name = "cboFuleType";
            this.cboFuleType.SelectedIndexChanged += new System.EventHandler(this.txt_QuickSearch);
            // 
            // cboCapacity
            // 
            this.cboCapacity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCapacity.FormattingEnabled = true;
            resources.ApplyResources(this.cboCapacity, "cboCapacity");
            this.cboCapacity.Name = "cboCapacity";
            this.cboCapacity.SelectedIndexChanged += new System.EventHandler(this.txt_QuickSearch);
            // 
            // txtQuickSearch
            // 
            this.txtQuickSearch.BackColor = System.Drawing.Color.White;
            this.txtQuickSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtQuickSearch, "txtQuickSearch");
            this.txtQuickSearch.Name = "txtQuickSearch";
            this.txtQuickSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtQuickSearch.QuickSearch += new System.EventHandler(this.txt_QuickSearch);
            this.txtQuickSearch.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // flowLayoutPanel1
            // 
            resources.ApplyResources(this.flowLayoutPanel1, "flowLayoutPanel1");
            this.flowLayoutPanel1.Controls.Add(this.btnREPORT);
            this.flowLayoutPanel1.Controls.Add(this.btnREMOVE);
            this.flowLayoutPanel1.Controls.Add(this.btnEDIT);
            this.flowLayoutPanel1.Controls.Add(this.btnADD);
            this.flowLayoutPanel1.Controls.Add(this.btnGENERATION_OPERATION_TIME);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            // 
            // btnREPORT
            // 
            resources.ApplyResources(this.btnREPORT, "btnREPORT");
            this.btnREPORT.Name = "btnREPORT";
            this.btnREPORT.UseVisualStyleBackColor = true;
            this.btnREPORT.Click += new System.EventHandler(this.btnReport_Click);
            // 
            // btnREMOVE
            // 
            resources.ApplyResources(this.btnREMOVE, "btnREMOVE");
            this.btnREMOVE.Name = "btnREMOVE";
            this.btnREMOVE.UseVisualStyleBackColor = true;
            this.btnREMOVE.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnEDIT
            // 
            resources.ApplyResources(this.btnEDIT, "btnEDIT");
            this.btnEDIT.Name = "btnEDIT";
            this.btnEDIT.UseVisualStyleBackColor = true;
            this.btnEDIT.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnADD
            // 
            resources.ApplyResources(this.btnADD, "btnADD");
            this.btnADD.Name = "btnADD";
            this.btnADD.UseVisualStyleBackColor = true;
            this.btnADD.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnGENERATION_OPERATION_TIME
            // 
            resources.ApplyResources(this.btnGENERATION_OPERATION_TIME, "btnGENERATION_OPERATION_TIME");
            this.btnGENERATION_OPERATION_TIME.Name = "btnGENERATION_OPERATION_TIME";
            this.btnGENERATION_OPERATION_TIME.UseVisualStyleBackColor = true;
            this.btnGENERATION_OPERATION_TIME.Click += new System.EventHandler(this.btnGenerationOperation_Click);
            // 
            // GENERATOR_ID
            // 
            this.GENERATOR_ID.DataPropertyName = "GENERATOR_ID";
            resources.ApplyResources(this.GENERATOR_ID, "GENERATOR_ID");
            this.GENERATOR_ID.Name = "GENERATOR_ID";
            // 
            // GENERATOR_NO
            // 
            this.GENERATOR_NO.DataPropertyName = "GENERATOR_NO";
            resources.ApplyResources(this.GENERATOR_NO, "GENERATOR_NO");
            this.GENERATOR_NO.Name = "GENERATOR_NO";
            // 
            // CAPACITY
            // 
            this.CAPACITY.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CAPACITY.DataPropertyName = "CAPACITY";
            dataGridViewCellStyle2.Format = "N0";
            this.CAPACITY.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.CAPACITY, "CAPACITY");
            this.CAPACITY.Name = "CAPACITY";
            // 
            // HORSE_POWER
            // 
            this.HORSE_POWER.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.HORSE_POWER.DataPropertyName = "HORSE_POWER";
            dataGridViewCellStyle3.Format = "N0";
            this.HORSE_POWER.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.HORSE_POWER, "HORSE_POWER");
            this.HORSE_POWER.Name = "HORSE_POWER";
            // 
            // OPERATION_HOUR
            // 
            this.OPERATION_HOUR.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.OPERATION_HOUR.DataPropertyName = "OPERATION_HOUR";
            dataGridViewCellStyle4.Format = "N0";
            this.OPERATION_HOUR.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.OPERATION_HOUR, "OPERATION_HOUR");
            this.OPERATION_HOUR.Name = "OPERATION_HOUR";
            // 
            // FUEL_TYPE
            // 
            this.FUEL_TYPE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.FUEL_TYPE.DataPropertyName = "FUEL_TYPE_NAME";
            dataGridViewCellStyle5.Format = "N2";
            this.FUEL_TYPE.DefaultCellStyle = dataGridViewCellStyle5;
            this.FUEL_TYPE.FillWeight = 150F;
            resources.ApplyResources(this.FUEL_TYPE, "FUEL_TYPE");
            this.FUEL_TYPE.Name = "FUEL_TYPE";
            // 
            // START_USE_DATE
            // 
            this.START_USE_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.START_USE_DATE.DataPropertyName = "START_DATE";
            dataGridViewCellStyle6.Format = "dd-MM-yyyy";
            this.START_USE_DATE.DefaultCellStyle = dataGridViewCellStyle6;
            this.START_USE_DATE.FillWeight = 150F;
            resources.ApplyResources(this.START_USE_DATE, "START_USE_DATE");
            this.START_USE_DATE.Name = "START_USE_DATE";
            // 
            // END_USE_DATE
            // 
            this.END_USE_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.END_USE_DATE.DataPropertyName = "END_DATE";
            dataGridViewCellStyle7.Format = "dd - MM - yyyy";
            dataGridViewCellStyle7.NullValue = "-";
            this.END_USE_DATE.DefaultCellStyle = dataGridViewCellStyle7;
            resources.ApplyResources(this.END_USE_DATE, "END_USE_DATE");
            this.END_USE_DATE.Name = "END_USE_DATE";
            // 
            // IS_IN_USED
            // 
            this.IS_IN_USED.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.IS_IN_USED.DataPropertyName = "IS_INUSED";
            resources.ApplyResources(this.IS_IN_USED, "IS_IN_USED");
            this.IS_IN_USED.Name = "IS_IN_USED";
            // 
            // PageGenerator
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.panel1);
            this.Name = "PageGenerator";
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panel1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExTextbox txtQuickSearch;
        private ExButton btnADD;
        private ExButton btnEDIT;
        private DataGridView dgv;
        private ExButton btnREMOVE;
        private ExButton btnGENERATION_OPERATION_TIME;
        private ExButton btnREPORT;
        private FlowLayoutPanel flowLayoutPanel1;
        private ComboBox cboFuleType;
        private ComboBox cboCapacity;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private DataGridViewTextBoxColumn GENERATOR_ID;
        private DataGridViewTextBoxColumn GENERATOR_NO;
        private DataGridViewTextBoxColumn CAPACITY;
        private DataGridViewTextBoxColumn HORSE_POWER;
        private DataGridViewTextBoxColumn OPERATION_HOUR;
        private DataGridViewTextBoxColumn FUEL_TYPE;
        private DataGridViewTextBoxColumn START_USE_DATE;
        private DataGridViewTextBoxColumn END_USE_DATE;
        private DataGridViewCheckBoxColumn IS_IN_USED;
    }
}
