﻿using System;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;

namespace EPower.Interface
{
    public partial class DialogTransformerBrand : ExDialog
    {
        GeneralProcess _flag;
        TBL_TRANSFORMER_BRAND _objBrand = new TBL_TRANSFORMER_BRAND();
        public TBL_TRANSFORMER_BRAND TranBrand
        {
            get { return _objBrand; }
        }
        TBL_TRANSFORMER_BRAND _oldObjBrand = new TBL_TRANSFORMER_BRAND();

        #region Constructor
        public DialogTransformerBrand(GeneralProcess flag, TBL_TRANSFORMER_BRAND objSeal)
        {
            InitializeComponent();

            _flag = flag;
            objSeal._CopyTo(_objBrand);
            objSeal._CopyTo(_objBrand);
            
            if (flag == GeneralProcess.Insert)
            {
                _objBrand.IS_ACTIVE = true;
                _objBrand.NOTE = "";
                this.Text = string.Concat(Resources.INSERT, this.Text);
            }
            else if (flag == GeneralProcess.Update)
            {
                this.Text = string.Concat(Resources.UPDATE, this.Text);
            }
            else if (flag == GeneralProcess.Delete)
            {
                this.Text = string.Concat(Resources.DELETE, this.Text);
                UIHelper.SetEnabled(this, false);
            }
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
            read();
        }
        #endregion

        #region Operation
        /// <summary>
        /// Data transaction.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (invalid())
            {
                return;
            }

            write();
            //If record is duplicate.
            txtTransformerBrand.ClearValidation();
            if (DBDataContext.Db.IsExits(_objBrand, "TRANSFORMER_BRAND_NAME"))
            {
                txtTransformerBrand.SetValidation(string.Format( Resources.MS_IS_EXISTS,lblTRANSFORMER_BRAND.Text));
                return;
            }
         
            try
            {
                using (TransactionScope tran=new TransactionScope( TransactionScopeOption.Required,TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objBrand);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objBrand, _objBrand);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objBrand);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }                                                
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            } 
        }

        /// <summary>
        /// User read only
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// Change current keyboard layout to khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        #endregion

        #region Method
        /// <summary>
        /// Read data from object.
        /// </summary>
        private void read()
        {
            txtTransformerBrand.Text = _objBrand.TRANSFORMER_BRAND_NAME;
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            _objBrand.TRANSFORMER_BRAND_NAME = txtTransformerBrand.Text.Trim();
        }

        private bool invalid()
        {
            bool val = false;
            if (txtTransformerBrand.Text.Trim()==string.Empty)
            {
                txtTransformerBrand.SetValidation(string.Format(Resources.REQUIRED, lblTRANSFORMER_BRAND.Text));
                val = true;
            }
            return val;
        } 
        #endregion

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objBrand);
        }
    }
}