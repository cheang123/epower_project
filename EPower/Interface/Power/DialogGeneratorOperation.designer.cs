﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogGeneratorOperation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogGeneratorOperation));
            this.lblDIALY_OPERATION_TIME = new System.Windows.Forms.Label();
            this.txtOperationTime = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.label9 = new System.Windows.Forms.Label();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.chkYEAR_OF_MADE_METER = new System.Windows.Forms.CheckBox();
            this.dtpYearMeter = new System.Windows.Forms.DateTimePicker();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.dtpYearMeter);
            this.content.Controls.Add(this.chkYEAR_OF_MADE_METER);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.txtOperationTime);
            this.content.Controls.Add(this.lblDIALY_OPERATION_TIME);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblDIALY_OPERATION_TIME, 0);
            this.content.Controls.SetChildIndex(this.txtOperationTime, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.chkYEAR_OF_MADE_METER, 0);
            this.content.Controls.SetChildIndex(this.dtpYearMeter, 0);
            // 
            // lblDIALY_OPERATION_TIME
            // 
            resources.ApplyResources(this.lblDIALY_OPERATION_TIME, "lblDIALY_OPERATION_TIME");
            this.lblDIALY_OPERATION_TIME.Name = "lblDIALY_OPERATION_TIME";
            // 
            // txtOperationTime
            // 
            resources.ApplyResources(this.txtOperationTime, "txtOperationTime");
            this.txtOperationTime.Name = "txtOperationTime";
            this.txtOperationTime.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // chkYEAR_OF_MADE_METER
            // 
            resources.ApplyResources(this.chkYEAR_OF_MADE_METER, "chkYEAR_OF_MADE_METER");
            this.chkYEAR_OF_MADE_METER.Name = "chkYEAR_OF_MADE_METER";
            this.chkYEAR_OF_MADE_METER.UseVisualStyleBackColor = true;
            this.chkYEAR_OF_MADE_METER.CheckedChanged += new System.EventHandler(this.chkHaveMeter_CheckedChanged);
            // 
            // dtpYearMeter
            // 
            resources.ApplyResources(this.dtpYearMeter, "dtpYearMeter");
            this.dtpYearMeter.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpYearMeter.Name = "dtpYearMeter";
            // 
            // DialogGeneratorOperation
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogGeneratorOperation";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBox txtOperationTime;
        private Label lblDIALY_OPERATION_TIME;
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label label9;
        private ExButton btnCHANGE_LOG;
        private CheckBox chkYEAR_OF_MADE_METER;
        private DateTimePicker dtpYearMeter;
    }
}