﻿using System;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;

namespace EPower.Interface
{
    public partial class DialogGeneratorOperation: ExDialog
    {
        TBL_GENERATOR_OPERATION _objNew = new TBL_GENERATOR_OPERATION();
        TBL_GENERATOR_OPERATION _objOld = new TBL_GENERATOR_OPERATION();
        GeneralProcess _flag;
        #region Constructor
        public DialogGeneratorOperation(GeneralProcess flag, TBL_GENERATOR_OPERATION obj)
        {
            InitializeComponent();
            _flag = flag;
            obj._CopyTo(_objNew);
            obj._CopyTo(_objOld);
            read();
        }
        #endregion

        #region Operation
        /// <summary>
        /// Data transaction
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write(); 
            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objNew); 
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objNew);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            } 
        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
        
        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        #endregion

        #region Method
        /// <summary>
        /// Load data from object.
        /// </summary>
        private void read()
        {
            txtOperationTime.Text = _objNew.OPERATION_TIME;
            dtpYearMeter.Value = _objNew.YEAR_METER;
            chkYEAR_OF_MADE_METER.Checked = _objNew.HAVE_METER; 
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            _objNew.OPERATION_TIME = txtOperationTime.Text;
            _objNew.HAVE_METER = chkYEAR_OF_MADE_METER.Checked;
            _objNew.YEAR_METER = new DateTime(dtpYearMeter.Value.Year, 1, 1); 
        }

        private bool inValid()
        {
            txtOperationTime.ClearAllValidation();
            if (txtOperationTime.Text.Trim()==string.Empty)
            {
                txtOperationTime.SetValidation(string.Format(Resources.REQUIRED, lblDIALY_OPERATION_TIME.Text));
                return true;
            }
            return false; 
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        } 
        #endregion  

        private void chkHaveMeter_CheckedChanged(object sender, EventArgs e)
        {
            dtpYearMeter.Enabled = chkYEAR_OF_MADE_METER.Checked;
        }

         
         


        

       
    }
}