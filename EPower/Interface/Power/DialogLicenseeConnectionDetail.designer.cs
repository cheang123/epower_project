﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogLicenseeConnectionDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogLicenseeConnectionDetail));
            this.lblLICENSE_NO = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.txtLicenseeNo = new System.Windows.Forms.TextBox();
            this.lblLICENSE_NAME = new System.Windows.Forms.Label();
            this.txtLicenseeName = new System.Windows.Forms.TextBox();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.CONNECTION_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LOCATION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.START_USE_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.END_USE_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnREMOVE = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnADD = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnEDIT = new SoftTech.Component.ExLinkLabel(this.components);
            this.lblLICENSEE_CONNECTION = new System.Windows.Forms.Label();
            this.lblVOLTAGE = new System.Windows.Forms.Label();
            this.txtVoltage = new System.Windows.Forms.TextBox();
            this.txtTariff = new System.Windows.Forms.TextBox();
            this.lblTARIFF = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.panel2);
            this.content.Controls.Add(this.lblVOLTAGE);
            this.content.Controls.Add(this.txtVoltage);
            this.content.Controls.Add(this.txtTariff);
            this.content.Controls.Add(this.lblTARIFF);
            this.content.Controls.Add(this.lblLICENSEE_CONNECTION);
            this.content.Controls.Add(this.dgv);
            this.content.Controls.Add(this.lblLICENSE_NAME);
            this.content.Controls.Add(this.txtLicenseeName);
            this.content.Controls.Add(this.txtLicenseeNo);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.lblLICENSE_NO);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblLICENSE_NO, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.txtLicenseeNo, 0);
            this.content.Controls.SetChildIndex(this.txtLicenseeName, 0);
            this.content.Controls.SetChildIndex(this.lblLICENSE_NAME, 0);
            this.content.Controls.SetChildIndex(this.dgv, 0);
            this.content.Controls.SetChildIndex(this.lblLICENSEE_CONNECTION, 0);
            this.content.Controls.SetChildIndex(this.lblTARIFF, 0);
            this.content.Controls.SetChildIndex(this.txtTariff, 0);
            this.content.Controls.SetChildIndex(this.txtVoltage, 0);
            this.content.Controls.SetChildIndex(this.lblVOLTAGE, 0);
            this.content.Controls.SetChildIndex(this.panel2, 0);
            // 
            // lblLICENSE_NO
            // 
            resources.ApplyResources(this.lblLICENSE_NO, "lblLICENSE_NO");
            this.lblLICENSE_NO.Name = "lblLICENSE_NO";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // txtLicenseeNo
            // 
            resources.ApplyResources(this.txtLicenseeNo, "txtLicenseeNo");
            this.txtLicenseeNo.Name = "txtLicenseeNo";
            this.txtLicenseeNo.ReadOnly = true;
            // 
            // lblLICENSE_NAME
            // 
            resources.ApplyResources(this.lblLICENSE_NAME, "lblLICENSE_NAME");
            this.lblLICENSE_NAME.Name = "lblLICENSE_NAME";
            // 
            // txtLicenseeName
            // 
            resources.ApplyResources(this.txtLicenseeName, "txtLicenseeName");
            this.txtLicenseeName.Name = "txtLicenseeName";
            this.txtLicenseeName.ReadOnly = true;
            // 
            // dgv
            // 
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CONNECTION_ID,
            this.CUSTOMER_CODE,
            this.CUSTOMER_NAME,
            this.LOCATION,
            this.START_USE_DATE,
            this.END_USE_DATE});
            this.dgv.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // CONNECTION_ID
            // 
            this.CONNECTION_ID.DataPropertyName = "CONNECTION_ID";
            resources.ApplyResources(this.CONNECTION_ID, "CONNECTION_ID");
            this.CONNECTION_ID.Name = "CONNECTION_ID";
            // 
            // CUSTOMER_CODE
            // 
            this.CUSTOMER_CODE.DataPropertyName = "CUSTOMER_CODE";
            resources.ApplyResources(this.CUSTOMER_CODE, "CUSTOMER_CODE");
            this.CUSTOMER_CODE.Name = "CUSTOMER_CODE";
            // 
            // CUSTOMER_NAME
            // 
            this.CUSTOMER_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.CUSTOMER_NAME.DataPropertyName = "CUS_NAME";
            resources.ApplyResources(this.CUSTOMER_NAME, "CUSTOMER_NAME");
            this.CUSTOMER_NAME.Name = "CUSTOMER_NAME";
            // 
            // LOCATION
            // 
            this.LOCATION.DataPropertyName = "LOCATION";
            resources.ApplyResources(this.LOCATION, "LOCATION");
            this.LOCATION.Name = "LOCATION";
            // 
            // START_USE_DATE
            // 
            this.START_USE_DATE.DataPropertyName = "START_DATE";
            dataGridViewCellStyle2.Format = "dd-MM-yyyy";
            this.START_USE_DATE.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.START_USE_DATE, "START_USE_DATE");
            this.START_USE_DATE.Name = "START_USE_DATE";
            // 
            // END_USE_DATE
            // 
            this.END_USE_DATE.DataPropertyName = "END_DATE";
            resources.ApplyResources(this.END_USE_DATE, "END_USE_DATE");
            this.END_USE_DATE.Name = "END_USE_DATE";
            // 
            // btnREMOVE
            // 
            resources.ApplyResources(this.btnREMOVE, "btnREMOVE");
            this.btnREMOVE.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnREMOVE.Name = "btnREMOVE";
            this.btnREMOVE.TabStop = true;
            this.btnREMOVE.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnADD
            // 
            resources.ApplyResources(this.btnADD, "btnADD");
            this.btnADD.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnADD.Name = "btnADD";
            this.btnADD.TabStop = true;
            this.btnADD.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnEDIT
            // 
            resources.ApplyResources(this.btnEDIT, "btnEDIT");
            this.btnEDIT.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnEDIT.Name = "btnEDIT";
            this.btnEDIT.TabStop = true;
            this.btnEDIT.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // lblLICENSEE_CONNECTION
            // 
            resources.ApplyResources(this.lblLICENSEE_CONNECTION, "lblLICENSEE_CONNECTION");
            this.lblLICENSEE_CONNECTION.Name = "lblLICENSEE_CONNECTION";
            // 
            // lblVOLTAGE
            // 
            resources.ApplyResources(this.lblVOLTAGE, "lblVOLTAGE");
            this.lblVOLTAGE.Name = "lblVOLTAGE";
            // 
            // txtVoltage
            // 
            resources.ApplyResources(this.txtVoltage, "txtVoltage");
            this.txtVoltage.Name = "txtVoltage";
            this.txtVoltage.ReadOnly = true;
            // 
            // txtTariff
            // 
            resources.ApplyResources(this.txtTariff, "txtTariff");
            this.txtTariff.Name = "txtTariff";
            this.txtTariff.ReadOnly = true;
            // 
            // lblTARIFF
            // 
            resources.ApplyResources(this.lblTARIFF, "lblTARIFF");
            this.lblTARIFF.Name = "lblTARIFF";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnADD);
            this.panel2.Controls.Add(this.btnEDIT);
            this.panel2.Controls.Add(this.btnREMOVE);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // DialogLicenseeConnectionDetail
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogLicenseeConnectionDetail";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Label lblLICENSE_NO;
        private Panel panel1;
        private ExButton btnCLOSE;
        private TextBox txtLicenseeNo;
        private Label lblLICENSE_NAME;
        private TextBox txtLicenseeName;
        private DataGridView dgv;
        private ExLinkLabel btnEDIT;
        private ExLinkLabel btnREMOVE;
        private ExLinkLabel btnADD;
        private Label lblLICENSEE_CONNECTION;
        private Label lblVOLTAGE;
        private TextBox txtVoltage;
        private TextBox txtTariff;
        private Label lblTARIFF;
        private DataGridViewTextBoxColumn CONNECTION_ID;
        private DataGridViewTextBoxColumn CUSTOMER_CODE;
        private DataGridViewTextBoxColumn CUSTOMER_NAME;
        private DataGridViewTextBoxColumn LOCATION;
        private DataGridViewTextBoxColumn START_USE_DATE;
        private DataGridViewTextBoxColumn END_USE_DATE;
        private Panel panel2;
    }
}