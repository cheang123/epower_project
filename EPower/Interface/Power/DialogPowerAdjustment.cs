﻿using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;

namespace EPower.Interface.Power
{
    public partial class DialogPowerAdjustment : ExDialog
    {
        GeneralProcess _flag;
        TBL_POWER_ADJUST _objNew = new TBL_POWER_ADJUST();
        TBL_POWER_ADJUST _objOld = new TBL_POWER_ADJUST();
        public TBL_POWER_ADJUST Object
        {
            get { return _objNew; }
        }

        public DialogPowerAdjustment(GeneralProcess flag,TBL_POWER_ADJUST objPowerAdjust)
        {
            InitializeComponent();
            _flag = flag;
            objPowerAdjust._CopyTo(_objNew);
            objPowerAdjust._CopyTo(_objOld);
            this.Text = flag.GetText(this.Text);
            
            this.read();

            if (flag == GeneralProcess.Delete)
            {
                UIHelper.SetEnabled(this, false);
            }
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
        }


        #region Method

        private void read()
        {
            dtpMonth.Value = _objNew.ADJUST_MONTH; 
            txtPowerPostpaid.Text = _objNew.POWER_POSTPAID.ToString("N0");
            txtPowerPrepaid.Text = _objNew.POWER_PREPAID.ToString("N0");
            txtPowerProduction.Text = _objNew.POWER_PRODUCTION.ToString("N0");
            txtPowerDiscount.Text = _objNew.POWER_DISCOUNT.ToString("N0");
        }

        private void write()
        {
            _objNew.ADJUST_MONTH=new DateTime (dtpMonth.Value.Year,dtpMonth.Value.Month,1) ;
            _objNew.POWER_POSTPAID = DataHelper.ParseToDecimal(txtPowerPostpaid.Text);
            _objNew.POWER_PREPAID = DataHelper.ParseToDecimal(txtPowerPrepaid.Text);
            _objNew.POWER_PRODUCTION = DataHelper.ParseToDecimal(txtPowerProduction.Text);
            _objNew.POWER_DISCOUNT = DataHelper.ParseToDecimal(txtPowerDiscount.Text);
            
        }

        private bool invalid()
        {
            bool result = false;
            this.ClearAllValidation(); 

            if (txtPowerProduction.Text == "")
            {
                txtPowerProduction.SetValidation(string.Format(Resources.REQUIRED, lblPOWER_PRODUCTION.Text));
                return true;
            }

            if (txtPowerDiscount.Text == "")
            {
                txtPowerDiscount.SetValidation(string.Format(Resources.REQUIRED, lblPOWER_DISCOUNT.Text));
                return true;
            }

            if (txtPowerPostpaid.Text == "")
            {
                txtPowerPostpaid.SetValidation(string.Format(Resources.REQUIRED, lblPOSTPAID.Text));
                return true;
            }

            if (txtPowerPrepaid.Text == "")
            {
                txtPowerPrepaid.SetValidation(string.Format(Resources.REQUIRED, lblPREPAID.Text));
                return true;
            }

            return result;
        }

        private bool checkExit()
        {
            if (_flag == GeneralProcess.Delete) return false;
            DateTime date = new DateTime(dtpMonth.Value.Year, dtpMonth.Value.Month, 1);
            return DBDataContext.Db.TBL_POWER_ADJUSTs.Where(row =>row.IS_ACTIVE &&  row.ADJUST_MONTH ==date && row.ADJUST_ID!=_objOld.ADJUST_ID).Count()>0;
            
        }

        #endregion

        

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }

        private void txtPowerTotal_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputNumberOnly(sender, e);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (invalid()) return;

            //if (DBDataContext.Db.IsExits(_objNew, "ADJUST_MONTH"))
            this.ClearAllValidation();
            if(checkExit())
            {
                dtpMonth.SetValidation(string.Format(Resources.MS_IS_EXISTS, lblMONTH.Text));
                return;
            } 
            write();
            this.ClearAllValidation();
            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objNew);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objNew);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
         

    }
}
