﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageDistributionFacility
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageDistributionFacility));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.DISTRIBUTION_FACILITY_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DISTRIBUTION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VOLTAGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PHASE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LENGTH_OF_LINE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.START_USE_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.END_USE_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_POLE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENDING_VOLTAGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CABLE_TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cboDistribution = new System.Windows.Forms.ComboBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnREPORT = new SoftTech.Component.ExButton();
            this.btnREMOVE = new SoftTech.Component.ExButton();
            this.btnEDIT = new SoftTech.Component.ExButton();
            this.btnADD = new SoftTech.Component.ExButton();
            this.btnDISTRIBUTION_VILLAGE = new SoftTech.Component.ExButton();
            this.txtQuickSearch = new SoftTech.Component.ExTextbox();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle28.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle28;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DISTRIBUTION_FACILITY_ID,
            this.DISTRIBUTION,
            this.VOLTAGE,
            this.PHASE,
            this.LENGTH_OF_LINE,
            this.START_USE_DATE,
            this.END_USE_DATE,
            this.TOTAL_POLE,
            this.ENDING_VOLTAGE,
            this.CABLE_TYPE});
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // DISTRIBUTION_FACILITY_ID
            // 
            this.DISTRIBUTION_FACILITY_ID.DataPropertyName = "DISTRIBUTION_FACILITY_ID";
            resources.ApplyResources(this.DISTRIBUTION_FACILITY_ID, "DISTRIBUTION_FACILITY_ID");
            this.DISTRIBUTION_FACILITY_ID.Name = "DISTRIBUTION_FACILITY_ID";
            // 
            // DISTRIBUTION
            // 
            this.DISTRIBUTION.DataPropertyName = "DISTRIBUTION_NAME";
            resources.ApplyResources(this.DISTRIBUTION, "DISTRIBUTION");
            this.DISTRIBUTION.Name = "DISTRIBUTION";
            // 
            // VOLTAGE
            // 
            this.VOLTAGE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.VOLTAGE.DataPropertyName = "VOLTAGE_NAME";
            resources.ApplyResources(this.VOLTAGE, "VOLTAGE");
            this.VOLTAGE.Name = "VOLTAGE";
            // 
            // PHASE
            // 
            this.PHASE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.PHASE.DataPropertyName = "PHASE_NAME";
            dataGridViewCellStyle29.Format = "N0";
            this.PHASE.DefaultCellStyle = dataGridViewCellStyle29;
            this.PHASE.FillWeight = 150F;
            resources.ApplyResources(this.PHASE, "PHASE");
            this.PHASE.Name = "PHASE";
            // 
            // LENGTH_OF_LINE
            // 
            this.LENGTH_OF_LINE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.LENGTH_OF_LINE.DataPropertyName = "LENGTH_OF_LINE";
            dataGridViewCellStyle30.Format = "N0";
            this.LENGTH_OF_LINE.DefaultCellStyle = dataGridViewCellStyle30;
            this.LENGTH_OF_LINE.FillWeight = 150F;
            resources.ApplyResources(this.LENGTH_OF_LINE, "LENGTH_OF_LINE");
            this.LENGTH_OF_LINE.Name = "LENGTH_OF_LINE";
            // 
            // START_USE_DATE
            // 
            this.START_USE_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.START_USE_DATE.DataPropertyName = "START_DATE";
            dataGridViewCellStyle31.Format = "dd-MM-yyyy";
            this.START_USE_DATE.DefaultCellStyle = dataGridViewCellStyle31;
            this.START_USE_DATE.FillWeight = 150F;
            resources.ApplyResources(this.START_USE_DATE, "START_USE_DATE");
            this.START_USE_DATE.Name = "START_USE_DATE";
            // 
            // END_USE_DATE
            // 
            this.END_USE_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.END_USE_DATE.DataPropertyName = "END_DATE";
            dataGridViewCellStyle32.Format = "dd - MM - yyyy";
            dataGridViewCellStyle32.NullValue = "-";
            this.END_USE_DATE.DefaultCellStyle = dataGridViewCellStyle32;
            resources.ApplyResources(this.END_USE_DATE, "END_USE_DATE");
            this.END_USE_DATE.Name = "END_USE_DATE";
            // 
            // TOTAL_POLE
            // 
            this.TOTAL_POLE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.TOTAL_POLE.DataPropertyName = "TOTAL_POLE";
            dataGridViewCellStyle33.Format = "N0";
            dataGridViewCellStyle33.NullValue = null;
            this.TOTAL_POLE.DefaultCellStyle = dataGridViewCellStyle33;
            resources.ApplyResources(this.TOTAL_POLE, "TOTAL_POLE");
            this.TOTAL_POLE.Name = "TOTAL_POLE";
            // 
            // ENDING_VOLTAGE
            // 
            this.ENDING_VOLTAGE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.ENDING_VOLTAGE.DataPropertyName = "ENDING_VOLTAGE";
            resources.ApplyResources(this.ENDING_VOLTAGE, "ENDING_VOLTAGE");
            this.ENDING_VOLTAGE.Name = "ENDING_VOLTAGE";
            // 
            // CABLE_TYPE
            // 
            this.CABLE_TYPE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CABLE_TYPE.DataPropertyName = "TYPE";
            resources.ApplyResources(this.CABLE_TYPE, "CABLE_TYPE");
            this.CABLE_TYPE.Name = "CABLE_TYPE";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.cboDistribution);
            this.panel1.Controls.Add(this.flowLayoutPanel1);
            this.panel1.Controls.Add(this.txtQuickSearch);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // cboDistribution
            // 
            this.cboDistribution.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDistribution.FormattingEnabled = true;
            resources.ApplyResources(this.cboDistribution, "cboDistribution");
            this.cboDistribution.Name = "cboDistribution";
            this.cboDistribution.SelectedIndexChanged += new System.EventHandler(this.cboDistribution_SelectedIndexChanged);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnREPORT);
            this.flowLayoutPanel1.Controls.Add(this.btnREMOVE);
            this.flowLayoutPanel1.Controls.Add(this.btnEDIT);
            this.flowLayoutPanel1.Controls.Add(this.btnADD);
            this.flowLayoutPanel1.Controls.Add(this.btnDISTRIBUTION_VILLAGE);
            resources.ApplyResources(this.flowLayoutPanel1, "flowLayoutPanel1");
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            // 
            // btnREPORT
            // 
            resources.ApplyResources(this.btnREPORT, "btnREPORT");
            this.btnREPORT.Name = "btnREPORT";
            this.btnREPORT.UseVisualStyleBackColor = true;
            this.btnREPORT.Click += new System.EventHandler(this.btnReport_Click);
            // 
            // btnREMOVE
            // 
            resources.ApplyResources(this.btnREMOVE, "btnREMOVE");
            this.btnREMOVE.Name = "btnREMOVE";
            this.btnREMOVE.UseVisualStyleBackColor = true;
            this.btnREMOVE.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnEDIT
            // 
            resources.ApplyResources(this.btnEDIT, "btnEDIT");
            this.btnEDIT.Name = "btnEDIT";
            this.btnEDIT.UseVisualStyleBackColor = true;
            this.btnEDIT.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnADD
            // 
            resources.ApplyResources(this.btnADD, "btnADD");
            this.btnADD.Name = "btnADD";
            this.btnADD.UseVisualStyleBackColor = true;
            this.btnADD.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnDISTRIBUTION_VILLAGE
            // 
            resources.ApplyResources(this.btnDISTRIBUTION_VILLAGE, "btnDISTRIBUTION_VILLAGE");
            this.btnDISTRIBUTION_VILLAGE.Name = "btnDISTRIBUTION_VILLAGE";
            this.btnDISTRIBUTION_VILLAGE.UseVisualStyleBackColor = true;
            this.btnDISTRIBUTION_VILLAGE.Click += new System.EventHandler(this.btnDistributoinVillage_Click);
            // 
            // txtQuickSearch
            // 
            this.txtQuickSearch.BackColor = System.Drawing.Color.White;
            this.txtQuickSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtQuickSearch, "txtQuickSearch");
            this.txtQuickSearch.Name = "txtQuickSearch";
            this.txtQuickSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtQuickSearch.QuickSearch += new System.EventHandler(this.txt_QuickSearch);
            this.txtQuickSearch.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "DISTRIBUTION_FACILITY_ID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn1, "dataGridViewTextBoxColumn1");
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "DISTRIBUTION_NAME";
            resources.ApplyResources(this.dataGridViewTextBoxColumn2, "dataGridViewTextBoxColumn2");
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "VOLTAGE_NAME";
            resources.ApplyResources(this.dataGridViewTextBoxColumn3, "dataGridViewTextBoxColumn3");
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "PHASE_NAME";
            dataGridViewCellStyle34.Format = "N2";
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle34;
            this.dataGridViewTextBoxColumn4.FillWeight = 150F;
            resources.ApplyResources(this.dataGridViewTextBoxColumn4, "dataGridViewTextBoxColumn4");
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "LENGTH_OF_LINE";
            resources.ApplyResources(this.dataGridViewTextBoxColumn5, "dataGridViewTextBoxColumn5");
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn6.DataPropertyName = "START_DATE";
            dataGridViewCellStyle35.Format = "dd - MM - yyyy";
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle35;
            this.dataGridViewTextBoxColumn6.FillWeight = 150F;
            resources.ApplyResources(this.dataGridViewTextBoxColumn6, "dataGridViewTextBoxColumn6");
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn7.DataPropertyName = "END_DATE";
            dataGridViewCellStyle36.Format = "dd - MM - yyyy";
            dataGridViewCellStyle36.NullValue = "-";
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle36;
            resources.ApplyResources(this.dataGridViewTextBoxColumn7, "dataGridViewTextBoxColumn7");
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // PageDistributionFacility
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.panel1);
            this.Name = "PageDistributionFacility";
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panel1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExTextbox txtQuickSearch;
        private ExButton btnADD;
        private ExButton btnEDIT;
        private DataGridView dgv;
        private ExButton btnREMOVE;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private ExButton btnDISTRIBUTION_VILLAGE;
        private ExButton btnREPORT;
        private FlowLayoutPanel flowLayoutPanel1;
        private DataGridViewTextBoxColumn DISTRIBUTION_FACILITY_ID;
        private DataGridViewTextBoxColumn DISTRIBUTION;
        private DataGridViewTextBoxColumn VOLTAGE;
        private DataGridViewTextBoxColumn PHASE;
        private DataGridViewTextBoxColumn LENGTH_OF_LINE;
        private DataGridViewTextBoxColumn START_USE_DATE;
        private DataGridViewTextBoxColumn END_USE_DATE;
        private DataGridViewTextBoxColumn TOTAL_POLE;
        private DataGridViewTextBoxColumn ENDING_VOLTAGE;
        private DataGridViewTextBoxColumn CABLE_TYPE;
        public ComboBox cboDistribution;
    }
}
