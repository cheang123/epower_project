﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogPowerGeneration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogPowerGeneration));
            this.lblPOWER_GENERATION = new System.Windows.Forms.Label();
            this.txtPower = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.label9 = new System.Windows.Forms.Label();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.label4 = new System.Windows.Forms.Label();
            this.cboGenerator = new System.Windows.Forms.ComboBox();
            this.lblGENERATOR_NO = new System.Windows.Forms.Label();
            this.dtpMonthGeneration = new System.Windows.Forms.DateTimePicker();
            this.lblGENERATION_MONTH = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtAuxiliaryUse = new System.Windows.Forms.TextBox();
            this.lblAUXILIARY_USE = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtFule = new System.Windows.Forms.TextBox();
            this.lblFUEL_CONSUMPTION = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtLub = new System.Windows.Forms.TextBox();
            this.lblLUB_COMSUMPTION = new System.Windows.Forms.Label();
            this.lblOPERATION_HOUR = new System.Windows.Forms.Label();
            this.txtOperationHour = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.lblBASE_RATE = new System.Windows.Forms.Label();
            this.txtBasedRate = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.lblCURRENCY = new System.Windows.Forms.Label();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.label17);
            this.content.Controls.Add(this.label14);
            this.content.Controls.Add(this.label13);
            this.content.Controls.Add(this.txtBasedRate);
            this.content.Controls.Add(this.lblBASE_RATE);
            this.content.Controls.Add(this.txtOperationHour);
            this.content.Controls.Add(this.lblOPERATION_HOUR);
            this.content.Controls.Add(this.txtLub);
            this.content.Controls.Add(this.lblLUB_COMSUMPTION);
            this.content.Controls.Add(this.label6);
            this.content.Controls.Add(this.txtFule);
            this.content.Controls.Add(this.lblFUEL_CONSUMPTION);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.txtAuxiliaryUse);
            this.content.Controls.Add(this.lblAUXILIARY_USE);
            this.content.Controls.Add(this.label11);
            this.content.Controls.Add(this.lblGENERATION_MONTH);
            this.content.Controls.Add(this.dtpMonthGeneration);
            this.content.Controls.Add(this.label20);
            this.content.Controls.Add(this.label4);
            this.content.Controls.Add(this.cboCurrency);
            this.content.Controls.Add(this.cboGenerator);
            this.content.Controls.Add(this.lblCURRENCY);
            this.content.Controls.Add(this.lblGENERATOR_NO);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.txtPower);
            this.content.Controls.Add(this.lblPOWER_GENERATION);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblPOWER_GENERATION, 0);
            this.content.Controls.SetChildIndex(this.txtPower, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.lblGENERATOR_NO, 0);
            this.content.Controls.SetChildIndex(this.lblCURRENCY, 0);
            this.content.Controls.SetChildIndex(this.cboGenerator, 0);
            this.content.Controls.SetChildIndex(this.cboCurrency, 0);
            this.content.Controls.SetChildIndex(this.label4, 0);
            this.content.Controls.SetChildIndex(this.label20, 0);
            this.content.Controls.SetChildIndex(this.dtpMonthGeneration, 0);
            this.content.Controls.SetChildIndex(this.lblGENERATION_MONTH, 0);
            this.content.Controls.SetChildIndex(this.label11, 0);
            this.content.Controls.SetChildIndex(this.lblAUXILIARY_USE, 0);
            this.content.Controls.SetChildIndex(this.txtAuxiliaryUse, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.lblFUEL_CONSUMPTION, 0);
            this.content.Controls.SetChildIndex(this.txtFule, 0);
            this.content.Controls.SetChildIndex(this.label6, 0);
            this.content.Controls.SetChildIndex(this.lblLUB_COMSUMPTION, 0);
            this.content.Controls.SetChildIndex(this.txtLub, 0);
            this.content.Controls.SetChildIndex(this.lblOPERATION_HOUR, 0);
            this.content.Controls.SetChildIndex(this.txtOperationHour, 0);
            this.content.Controls.SetChildIndex(this.lblBASE_RATE, 0);
            this.content.Controls.SetChildIndex(this.txtBasedRate, 0);
            this.content.Controls.SetChildIndex(this.label13, 0);
            this.content.Controls.SetChildIndex(this.label14, 0);
            this.content.Controls.SetChildIndex(this.label17, 0);
            // 
            // lblPOWER_GENERATION
            // 
            resources.ApplyResources(this.lblPOWER_GENERATION, "lblPOWER_GENERATION");
            this.lblPOWER_GENERATION.Name = "lblPOWER_GENERATION";
            // 
            // txtPower
            // 
            resources.ApplyResources(this.txtPower, "txtPower");
            this.txtPower.Name = "txtPower";
            this.txtPower.Enter += new System.EventHandler(this.txtKeyEn);
            this.txtPower.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtOperationHour_KeyPress);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Name = "label4";
            // 
            // cboGenerator
            // 
            this.cboGenerator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboGenerator.FormattingEnabled = true;
            resources.ApplyResources(this.cboGenerator, "cboGenerator");
            this.cboGenerator.Name = "cboGenerator";
            // 
            // lblGENERATOR_NO
            // 
            resources.ApplyResources(this.lblGENERATOR_NO, "lblGENERATOR_NO");
            this.lblGENERATOR_NO.Name = "lblGENERATOR_NO";
            // 
            // dtpMonthGeneration
            // 
            resources.ApplyResources(this.dtpMonthGeneration, "dtpMonthGeneration");
            this.dtpMonthGeneration.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMonthGeneration.Name = "dtpMonthGeneration";
            this.dtpMonthGeneration.ValueChanged += new System.EventHandler(this.dtpMonthGeneration_ValueChanged);
            // 
            // lblGENERATION_MONTH
            // 
            resources.ApplyResources(this.lblGENERATION_MONTH, "lblGENERATION_MONTH");
            this.lblGENERATION_MONTH.Name = "lblGENERATION_MONTH";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Name = "label11";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // txtAuxiliaryUse
            // 
            resources.ApplyResources(this.txtAuxiliaryUse, "txtAuxiliaryUse");
            this.txtAuxiliaryUse.Name = "txtAuxiliaryUse";
            this.txtAuxiliaryUse.Enter += new System.EventHandler(this.txtKeyEn);
            this.txtAuxiliaryUse.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.InputDecimalOnly);
            // 
            // lblAUXILIARY_USE
            // 
            resources.ApplyResources(this.lblAUXILIARY_USE, "lblAUXILIARY_USE");
            this.lblAUXILIARY_USE.Name = "lblAUXILIARY_USE";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Name = "label6";
            // 
            // txtFule
            // 
            resources.ApplyResources(this.txtFule, "txtFule");
            this.txtFule.Name = "txtFule";
            this.txtFule.Enter += new System.EventHandler(this.txtKeyEn);
            this.txtFule.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtOperationHour_KeyPress);
            // 
            // lblFUEL_CONSUMPTION
            // 
            resources.ApplyResources(this.lblFUEL_CONSUMPTION, "lblFUEL_CONSUMPTION");
            this.lblFUEL_CONSUMPTION.Name = "lblFUEL_CONSUMPTION";
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Name = "label13";
            // 
            // txtLub
            // 
            resources.ApplyResources(this.txtLub, "txtLub");
            this.txtLub.Name = "txtLub";
            this.txtLub.Enter += new System.EventHandler(this.txtKeyEn);
            this.txtLub.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtOperationHour_KeyPress);
            // 
            // lblLUB_COMSUMPTION
            // 
            resources.ApplyResources(this.lblLUB_COMSUMPTION, "lblLUB_COMSUMPTION");
            this.lblLUB_COMSUMPTION.Name = "lblLUB_COMSUMPTION";
            // 
            // lblOPERATION_HOUR
            // 
            resources.ApplyResources(this.lblOPERATION_HOUR, "lblOPERATION_HOUR");
            this.lblOPERATION_HOUR.Name = "lblOPERATION_HOUR";
            // 
            // txtOperationHour
            // 
            resources.ApplyResources(this.txtOperationHour, "txtOperationHour");
            this.txtOperationHour.Name = "txtOperationHour";
            this.txtOperationHour.Enter += new System.EventHandler(this.txtKeyEn);
            this.txtOperationHour.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtOperationHour_KeyPress);
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Name = "label14";
            // 
            // lblBASE_RATE
            // 
            resources.ApplyResources(this.lblBASE_RATE, "lblBASE_RATE");
            this.lblBASE_RATE.Name = "lblBASE_RATE";
            // 
            // txtBasedRate
            // 
            resources.ApplyResources(this.txtBasedRate, "txtBasedRate");
            this.txtBasedRate.Name = "txtBasedRate";
            this.txtBasedRate.Enter += new System.EventHandler(this.txtKeyEn);
            this.txtBasedRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.InputDecimalOnly);
            // 
            // label17
            // 
            resources.ApplyResources(this.label17, "label17");
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Name = "label17";
            // 
            // lblCURRENCY
            // 
            resources.ApplyResources(this.lblCURRENCY, "lblCURRENCY");
            this.lblCURRENCY.Name = "lblCURRENCY";
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            // 
            // label20
            // 
            resources.ApplyResources(this.label20, "label20");
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Name = "label20";
            // 
            // DialogPowerGeneration
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogPowerGeneration";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBox txtPower;
        private Label lblPOWER_GENERATION;
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label label9;
        private ExButton btnCHANGE_LOG;
        private Label label4;
        private ComboBox cboGenerator;
        private Label lblGENERATOR_NO;
        private Label lblGENERATION_MONTH;
        private DateTimePicker dtpMonthGeneration;
        private Label label11;
        private Label label1;
        private TextBox txtAuxiliaryUse;
        private Label lblAUXILIARY_USE;
        private Label label13;
        private TextBox txtLub;
        private Label lblLUB_COMSUMPTION;
        private Label label6;
        private TextBox txtFule;
        private Label lblFUEL_CONSUMPTION;
        private Label label14;
        private TextBox txtOperationHour;
        private Label lblOPERATION_HOUR;
        private Label label17;
        private TextBox txtBasedRate;
        private Label lblBASE_RATE;
        private Label label20;
        private ComboBox cboCurrency;
        private Label lblCURRENCY;
    }
}