﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PagePowerGeneration : Form
    {    
        public  TBL_POWER_GENERATION Energy
        {
            get
            {
                TBL_POWER_GENERATION obj = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    try
                    {
                        int intid = (int)dgv.SelectedRows[0].Cells[GENERATION_ID.Name].Value;
                        obj = DBDataContext.Db.TBL_POWER_GENERATIONs.FirstOrDefault(x => x.GENERATION_ID == intid);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return obj;
            }
        }

        #region Constructor
        public PagePowerGeneration()
        {
            InitializeComponent();
            this.btnREPORT.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_QUARTERLY_REPORT]);
            //var intYear = from eg in DBDataContext.Db.TBL_POWER_GENERATIONs
            //              where eg.IS_ACTIVE
            //              select new {YEAR= eg.GENERATION_MONTH.Year, DISPLAY= eg.GENERATION_MONTH.Year.ToString() }; 
            //UIHelper.SetDataSourceToComboBox(cboYear, intYear.Distinct(), "គ្រប់ឆ្នាំ");  

            UIHelper.DataGridViewProperties(dgv);

            BindGenerator();

            load();
        }
        #endregion

        #region Method

        private void load()
        {
            int gernerator_id = (int)cboGenerator.SelectedValue;

            DateTime date = new DateTime(dtpDate.Value.Year, dtpDate.Value.Month, 1);
            dgv.DataSource = from eg in DBDataContext.Db.TBL_POWER_GENERATIONs
                             join g in DBDataContext.Db.TBL_GENERATORs on eg.GENERATOR_ID equals g.GENERATOR_ID
                             where eg.IS_ACTIVE
                             && (eg.GENERATION_MONTH==date || dtpDate.Checked==false)
                             && (gernerator_id==0 || g.GENERATOR_ID==gernerator_id)
                             orderby eg.GENERATION_MONTH,g.GENERATOR_NO ascending
                             select new
                             {
                                 eg.GENERATION_ID,
                                 g.GENERATOR_NO,
                                 eg.GENERATION_MONTH,
                                 eg.POWER_GENERATION,
                                 eg.AUXILIARY_USE,
                                 eg.FUEL_CONSUMPTION,
                                 eg.LUB_COMSUMPTION,
                                 eg.OPERATION_HOUR
                             }; 
        }

        public void BindGenerator()
        {
            DataTable dt = (from g in DBDataContext.Db.TBL_GENERATORs
                           where g.IS_ACTIVE
                           select new
                           {
                               g.GENERATOR_ID
                               ,g.GENERATOR_NO
                           })._ToDataTable();
            int backup = (int)(cboGenerator.SelectedValue ?? 0);
            UIHelper.SetDataSourceToComboBox(cboGenerator, dt,Resources.ALL_GENERATOR);
            this.cboGenerator.SelectedValue = backup;
        }

        #endregion

        #region Operation

        private void btnNew_Click(object sender, EventArgs e)
        {
            DialogPowerGeneration dig = new DialogPowerGeneration(GeneralProcess.Insert, new TBL_POWER_GENERATION() { GENERATION_MONTH=new DateTime(DBDataContext.Db.GetSystemDate().Year,DBDataContext.Db.GetSystemDate().Month,1)});
            if (dig.ShowDialog() == DialogResult.OK) 
            {
                load();
                UIHelper.SelectRow(dgv, dig.PowerGenerator.GENERATION_ID);
            }
        }
         
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (Energy==null)
            {
                return;
            }
            DialogPowerGeneration dig = new DialogPowerGeneration(GeneralProcess.Update, Energy);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                load();
                UIHelper.SelectRow(dgv, dig.PowerGenerator.GENERATION_ID);
            }
        }
         
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (Energy==null)
            {
                return;
            }
            DialogPowerGeneration dig = new DialogPowerGeneration(GeneralProcess.Delete, Energy);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                load();
                UIHelper.SelectRow(dgv, dig.PowerGenerator.GENERATION_ID - 1);
            } 
        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            var diag = new DialogReportQuarterOptions(false);
            diag.chkTABLE_POWER_GENERATION.Checked = true;
            if (diag.ShowDialog() == DialogResult.OK)
            {
                var doc = PageReportQuaterly.OpenReport(diag);
                doc.ViewReport("");
            }

        }

        private void cboSearchType_SelectedIndexChanged(object sender, EventArgs e)
        {
            load();
        }
         
        /// <summary>
        /// Change current keyboard layout to khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void dtpDate_ValueChanged(object sender, EventArgs e)
        {
            load();
        }

        #endregion

       

        



        
    }
}
