﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogPowerUsage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogPowerUsage));
            this.lblInputPower = new System.Windows.Forms.Label();
            this.txtInputUsagePostpaid = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btnChangelog = new SoftTech.Component.ExButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dtp = new System.Windows.Forms.DateTimePicker();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.dtp);
            this.content.Controls.Add(this.label3);
            this.content.Controls.Add(this.label6);
            this.content.Controls.Add(this.btnChangelog);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.label4);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.txtInputUsagePostpaid);
            this.content.Controls.Add(this.lblInputPower);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblInputPower, 0);
            this.content.Controls.SetChildIndex(this.txtInputUsagePostpaid, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.label4, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.btnChangelog, 0);
            this.content.Controls.SetChildIndex(this.label6, 0);
            this.content.Controls.SetChildIndex(this.label3, 0);
            this.content.Controls.SetChildIndex(this.dtp, 0);
            // 
            // lblInputPower
            // 
            resources.ApplyResources(this.lblInputPower, "lblInputPower");
            this.lblInputPower.Name = "lblInputPower";
            // 
            // txtInputUsagePostpaid
            // 
            resources.ApplyResources(this.txtInputUsagePostpaid, "txtInputUsagePostpaid");
            this.txtInputUsagePostpaid.Name = "txtInputUsagePostpaid";
            this.txtInputUsagePostpaid.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            this.txtInputUsagePostpaid.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInputUsage_KeyPress);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // btnChangelog
            // 
            resources.ApplyResources(this.btnChangelog, "btnChangelog");
            this.btnChangelog.Name = "btnChangelog";
            this.btnChangelog.UseVisualStyleBackColor = true;
            this.btnChangelog.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // dtp
            // 
            resources.ApplyResources(this.dtp, "dtp");
            this.dtp.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp.Name = "dtp";
            this.dtp.ValueChanged += new System.EventHandler(this.dtp_ValueChanged);
            // 
            // DialogPowerUsage
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogPowerUsage";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBox txtInputUsagePostpaid;
        private Label lblInputPower;
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label label4;
        private Label label9;
        private ExButton btnChangelog;
        private DateTimePicker dtp;
        private Label label3;
        private Label label6;
    }
}