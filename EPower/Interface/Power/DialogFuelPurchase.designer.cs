﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogFuelPurchase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogFuelPurchase));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.cboFuelType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblFUEL_TYPE = new System.Windows.Forms.Label();
            this.dtpPurchaseDate = new System.Windows.Forms.DateTimePicker();
            this.lblDATE = new System.Windows.Forms.Label();
            this.cboFuelSource = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.lblFUEL_SOURCE = new System.Windows.Forms.Label();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.lblNOTE = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblFUEL_PRICE = new System.Windows.Forms.Label();
            this.lblCURRENCY = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtRefNo = new System.Windows.Forms.TextBox();
            this.lblFUEL_PURCHASE_REF = new System.Windows.Forms.Label();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.lblPAYMENT_ACCOUNT = new System.Windows.Forms.Label();
            this.cboAccountPayment = new SoftTech.Component.TreeComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtVolume = new System.Windows.Forms.TextBox();
            this.lblVOLUME = new System.Windows.Forms.Label();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.lblAMOUNT = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtTranBy = new System.Windows.Forms.TextBox();
            this.lblCREATE_BY = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.cboAccountPayment);
            this.content.Controls.Add(this.label30);
            this.content.Controls.Add(this.label22);
            this.content.Controls.Add(this.cboCurrency);
            this.content.Controls.Add(this.label20);
            this.content.Controls.Add(this.label14);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.txtRefNo);
            this.content.Controls.Add(this.lblFUEL_PURCHASE_REF);
            this.content.Controls.Add(this.cboFuelSource);
            this.content.Controls.Add(this.label17);
            this.content.Controls.Add(this.lblFUEL_SOURCE);
            this.content.Controls.Add(this.txtNote);
            this.content.Controls.Add(this.lblNOTE);
            this.content.Controls.Add(this.label12);
            this.content.Controls.Add(this.lblAMOUNT);
            this.content.Controls.Add(this.lblCREATE_BY);
            this.content.Controls.Add(this.lblVOLUME);
            this.content.Controls.Add(this.lblFUEL_PRICE);
            this.content.Controls.Add(this.lblPAYMENT_ACCOUNT);
            this.content.Controls.Add(this.lblCURRENCY);
            this.content.Controls.Add(this.txtTotal);
            this.content.Controls.Add(this.txtTranBy);
            this.content.Controls.Add(this.txtVolume);
            this.content.Controls.Add(this.txtPrice);
            this.content.Controls.Add(this.cboFuelType);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.lblFUEL_TYPE);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.dtpPurchaseDate);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.lblDATE);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblDATE, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.dtpPurchaseDate, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.lblFUEL_TYPE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.cboFuelType, 0);
            this.content.Controls.SetChildIndex(this.txtPrice, 0);
            this.content.Controls.SetChildIndex(this.txtVolume, 0);
            this.content.Controls.SetChildIndex(this.txtTranBy, 0);
            this.content.Controls.SetChildIndex(this.txtTotal, 0);
            this.content.Controls.SetChildIndex(this.lblCURRENCY, 0);
            this.content.Controls.SetChildIndex(this.lblPAYMENT_ACCOUNT, 0);
            this.content.Controls.SetChildIndex(this.lblFUEL_PRICE, 0);
            this.content.Controls.SetChildIndex(this.lblVOLUME, 0);
            this.content.Controls.SetChildIndex(this.lblCREATE_BY, 0);
            this.content.Controls.SetChildIndex(this.lblAMOUNT, 0);
            this.content.Controls.SetChildIndex(this.label12, 0);
            this.content.Controls.SetChildIndex(this.lblNOTE, 0);
            this.content.Controls.SetChildIndex(this.txtNote, 0);
            this.content.Controls.SetChildIndex(this.lblFUEL_SOURCE, 0);
            this.content.Controls.SetChildIndex(this.label17, 0);
            this.content.Controls.SetChildIndex(this.cboFuelSource, 0);
            this.content.Controls.SetChildIndex(this.lblFUEL_PURCHASE_REF, 0);
            this.content.Controls.SetChildIndex(this.txtRefNo, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.label14, 0);
            this.content.Controls.SetChildIndex(this.label20, 0);
            this.content.Controls.SetChildIndex(this.cboCurrency, 0);
            this.content.Controls.SetChildIndex(this.label22, 0);
            this.content.Controls.SetChildIndex(this.label30, 0);
            this.content.Controls.SetChildIndex(this.cboAccountPayment, 0);
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // cboFuelType
            // 
            this.cboFuelType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFuelType.FormattingEnabled = true;
            resources.ApplyResources(this.cboFuelType, "cboFuelType");
            this.cboFuelType.Name = "cboFuelType";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // lblFUEL_TYPE
            // 
            resources.ApplyResources(this.lblFUEL_TYPE, "lblFUEL_TYPE");
            this.lblFUEL_TYPE.Name = "lblFUEL_TYPE";
            // 
            // dtpPurchaseDate
            // 
            resources.ApplyResources(this.dtpPurchaseDate, "dtpPurchaseDate");
            this.dtpPurchaseDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPurchaseDate.Name = "dtpPurchaseDate";
            this.dtpPurchaseDate.Enter += new System.EventHandler(this.ChangeEnglishKeyboard);
            // 
            // lblDATE
            // 
            resources.ApplyResources(this.lblDATE, "lblDATE");
            this.lblDATE.Name = "lblDATE";
            // 
            // cboFuelSource
            // 
            this.cboFuelSource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFuelSource.FormattingEnabled = true;
            resources.ApplyResources(this.cboFuelSource, "cboFuelSource");
            this.cboFuelSource.Name = "cboFuelSource";
            // 
            // label17
            // 
            resources.ApplyResources(this.label17, "label17");
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Name = "label17";
            // 
            // lblFUEL_SOURCE
            // 
            resources.ApplyResources(this.lblFUEL_SOURCE, "lblFUEL_SOURCE");
            this.lblFUEL_SOURCE.Name = "lblFUEL_SOURCE";
            // 
            // txtNote
            // 
            this.txtNote.AcceptsTab = true;
            resources.ApplyResources(this.txtNote, "txtNote");
            this.txtNote.Name = "txtNote";
            this.txtNote.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // lblNOTE
            // 
            resources.ApplyResources(this.lblNOTE, "lblNOTE");
            this.lblNOTE.Name = "lblNOTE";
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Name = "label12";
            // 
            // lblFUEL_PRICE
            // 
            resources.ApplyResources(this.lblFUEL_PRICE, "lblFUEL_PRICE");
            this.lblFUEL_PRICE.Name = "lblFUEL_PRICE";
            // 
            // lblCURRENCY
            // 
            resources.ApplyResources(this.lblCURRENCY, "lblCURRENCY");
            this.lblCURRENCY.Name = "lblCURRENCY";
            // 
            // txtPrice
            // 
            this.txtPrice.AcceptsTab = true;
            resources.ApplyResources(this.txtPrice, "txtPrice");
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.TextChanged += new System.EventHandler(this.txtCalcAmount);
            this.txtPrice.Enter += new System.EventHandler(this.ChangeEnglishKeyboard);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // txtRefNo
            // 
            this.txtRefNo.AcceptsTab = true;
            resources.ApplyResources(this.txtRefNo, "txtRefNo");
            this.txtRefNo.Name = "txtRefNo";
            this.txtRefNo.Enter += new System.EventHandler(this.ChangeEnglishKeyboard);
            // 
            // lblFUEL_PURCHASE_REF
            // 
            resources.ApplyResources(this.lblFUEL_PURCHASE_REF, "lblFUEL_PURCHASE_REF");
            this.lblFUEL_PURCHASE_REF.Name = "lblFUEL_PURCHASE_REF";
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.SelectedIndexChanged += new System.EventHandler(this.cboCurrency_SelectedIndexChanged);
            // 
            // label22
            // 
            resources.ApplyResources(this.label22, "label22");
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Name = "label22";
            // 
            // lblPAYMENT_ACCOUNT
            // 
            resources.ApplyResources(this.lblPAYMENT_ACCOUNT, "lblPAYMENT_ACCOUNT");
            this.lblPAYMENT_ACCOUNT.Name = "lblPAYMENT_ACCOUNT";
            // 
            // cboAccountPayment
            // 
            this.cboAccountPayment.AbsoluteChildrenSelectableOnly = true;
            this.cboAccountPayment.BranchSeparator = "/";
            this.cboAccountPayment.Imagelist = null;
            resources.ApplyResources(this.cboAccountPayment, "cboAccountPayment");
            this.cboAccountPayment.Name = "cboAccountPayment";
            this.cboAccountPayment.PopupHeight = 250;
            this.cboAccountPayment.PopupWidth = 200;
            this.cboAccountPayment.SelectedNode = null;
            // 
            // label30
            // 
            resources.ApplyResources(this.label30, "label30");
            this.label30.ForeColor = System.Drawing.Color.Red;
            this.label30.Name = "label30";
            // 
            // txtVolume
            // 
            this.txtVolume.AcceptsTab = true;
            resources.ApplyResources(this.txtVolume, "txtVolume");
            this.txtVolume.Name = "txtVolume";
            this.txtVolume.TextChanged += new System.EventHandler(this.txtCalcAmount);
            this.txtVolume.Enter += new System.EventHandler(this.ChangeEnglishKeyboard);
            // 
            // lblVOLUME
            // 
            resources.ApplyResources(this.lblVOLUME, "lblVOLUME");
            this.lblVOLUME.Name = "lblVOLUME";
            // 
            // txtTotal
            // 
            this.txtTotal.AcceptsTab = true;
            resources.ApplyResources(this.txtTotal, "txtTotal");
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.ReadOnly = true;
            this.txtTotal.Enter += new System.EventHandler(this.ChangeEnglishKeyboard);
            // 
            // lblAMOUNT
            // 
            resources.ApplyResources(this.lblAMOUNT, "lblAMOUNT");
            this.lblAMOUNT.Name = "lblAMOUNT";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Name = "label14";
            // 
            // txtTranBy
            // 
            this.txtTranBy.AcceptsTab = true;
            resources.ApplyResources(this.txtTranBy, "txtTranBy");
            this.txtTranBy.Name = "txtTranBy";
            this.txtTranBy.Enter += new System.EventHandler(this.ChangeEnglishKeyboard);
            // 
            // lblCREATE_BY
            // 
            resources.ApplyResources(this.lblCREATE_BY, "lblCREATE_BY");
            this.lblCREATE_BY.Name = "lblCREATE_BY";
            // 
            // label20
            // 
            resources.ApplyResources(this.label20, "label20");
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Name = "label20";
            // 
            // DialogFuelPurchase
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogFuelPurchase";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private ExButton btnCHANGE_LOG;
        private ComboBox cboFuelSource;
        private Label label17;
        private Label lblFUEL_SOURCE;
        private TextBox txtNote;
        private Label lblNOTE;
        private Label label12;
        private Label lblFUEL_PRICE;
        private Label lblCURRENCY;
        private TextBox txtPrice;
        private ComboBox cboFuelType;
        private Label label1;
        private Label lblFUEL_TYPE;
        private DateTimePicker dtpPurchaseDate;
        private Label lblDATE;
        private Label label9;
        private TextBox txtRefNo;
        private Label lblFUEL_PURCHASE_REF;
        private ComboBox cboCurrency;
        private Label label22;
        private Label lblPAYMENT_ACCOUNT;
        private TreeComboBox cboAccountPayment;
        private Label label30;
        private Label lblAMOUNT;
        private Label lblVOLUME;
        private TextBox txtTotal;
        private TextBox txtVolume;
        private Label label20;
        private Label label14;
        private Label lblCREATE_BY;
        private TextBox txtTranBy;
    }
}