﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageLicenseeConnection
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageLicenseeConnection));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.CONNECTION_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LICENSE_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUS_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VOLTAGE_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LOCATION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.START_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.END_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnRemove = new SoftTech.Component.ExButton();
            this.btnNew = new SoftTech.Component.ExButton();
            this.btnEdit = new SoftTech.Component.ExButton();
            this.txtQuickSearch = new SoftTech.Component.ExTextbox();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CONNECTION_ID,
            this.LICENSE_NO,
            this.CUS_NAME,
            this.VOLTAGE_NAME,
            this.PRICE,
            this.LOCATION,
            this.START_DATE,
            this.END_DATE});
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellDoubleClick);
            // 
            // CONNECTION_ID
            // 
            this.CONNECTION_ID.DataPropertyName = "CONNECTION_ID";
            resources.ApplyResources(this.CONNECTION_ID, "CONNECTION_ID");
            this.CONNECTION_ID.Name = "CONNECTION_ID";
            // 
            // LICENSE_NO
            // 
            this.LICENSE_NO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.LICENSE_NO.DataPropertyName = "LICENSE_NO";
            resources.ApplyResources(this.LICENSE_NO, "LICENSE_NO");
            this.LICENSE_NO.Name = "LICENSE_NO";
            // 
            // CUS_NAME
            // 
            this.CUS_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.CUS_NAME.DataPropertyName = "CUS_NAME";
            resources.ApplyResources(this.CUS_NAME, "CUS_NAME");
            this.CUS_NAME.Name = "CUS_NAME";
            // 
            // VOLTAGE_NAME
            // 
            this.VOLTAGE_NAME.DataPropertyName = "VOLTAGE_NAME";
            resources.ApplyResources(this.VOLTAGE_NAME, "VOLTAGE_NAME");
            this.VOLTAGE_NAME.Name = "VOLTAGE_NAME";
            // 
            // PRICE
            // 
            this.PRICE.DataPropertyName = "PRICE";
            dataGridViewCellStyle2.Format = "#,##0.##";
            this.PRICE.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.PRICE, "PRICE");
            this.PRICE.Name = "PRICE";
            // 
            // LOCATION
            // 
            this.LOCATION.DataPropertyName = "LOCATION";
            resources.ApplyResources(this.LOCATION, "LOCATION");
            this.LOCATION.Name = "LOCATION";
            // 
            // START_DATE
            // 
            this.START_DATE.DataPropertyName = "START_DATE";
            dataGridViewCellStyle3.Format = "dd-MM-yyyy";
            this.START_DATE.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.START_DATE, "START_DATE");
            this.START_DATE.Name = "START_DATE";
            // 
            // END_DATE
            // 
            this.END_DATE.DataPropertyName = "END_DATE";
            resources.ApplyResources(this.END_DATE, "END_DATE");
            this.END_DATE.Name = "END_DATE";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.btnRemove);
            this.panel1.Controls.Add(this.btnNew);
            this.panel1.Controls.Add(this.btnEdit);
            this.panel1.Controls.Add(this.txtQuickSearch);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnRemove
            // 
            resources.ApplyResources(this.btnRemove, "btnRemove");
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnNew
            // 
            resources.ApplyResources(this.btnNew, "btnNew");
            this.btnNew.Name = "btnNew";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnEdit
            // 
            resources.ApplyResources(this.btnEdit, "btnEdit");
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // txtQuickSearch
            // 
            this.txtQuickSearch.BackColor = System.Drawing.Color.White;
            this.txtQuickSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtQuickSearch, "txtQuickSearch");
            this.txtQuickSearch.Name = "txtQuickSearch";
            this.txtQuickSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtQuickSearch.QuickSearch += new System.EventHandler(this.txt_QuickSearch);
            this.txtQuickSearch.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // PageLicenseeConnection
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.panel1);
            this.Name = "PageLicenseeConnection";
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExTextbox txtQuickSearch;
        private ExButton btnNew;
        private ExButton btnEdit;
        private DataGridView dgv;
        private ExButton btnRemove;
        private DataGridViewTextBoxColumn CONNECTION_ID;
        private DataGridViewTextBoxColumn LICENSE_NO;
        private DataGridViewTextBoxColumn CUS_NAME;
        private DataGridViewTextBoxColumn VOLTAGE_NAME;
        private DataGridViewTextBoxColumn PRICE;
        private DataGridViewTextBoxColumn LOCATION;
        private DataGridViewTextBoxColumn START_DATE;
        private DataGridViewTextBoxColumn END_DATE;
    }
}
