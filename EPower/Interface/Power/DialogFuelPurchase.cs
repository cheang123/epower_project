﻿using EPower.Base.Helper;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using Login = SoftTech.Security.Logic.Login;

namespace EPower.Interface
{
    public partial class DialogFuelPurchase : ExDialog
    {

        GeneralProcess _flag;
        bool load = true;
        TBL_FUEL_PURCHASE _objNew = new TBL_FUEL_PURCHASE();
        public TBL_FUEL_PURCHASE FuelPurchase
        {
            get { return _objNew; }
        }
        TBL_FUEL_PURCHASE _objOld = new TBL_FUEL_PURCHASE();
        DateTime today = DBDataContext.Db.GetSystemDate().Date;

        #region Constructor
        public DialogFuelPurchase(GeneralProcess flag, TBL_FUEL_PURCHASE obj)
        {
            InitializeComponent();

            this.Text = flag.GetText(this.Text);
            UIHelper.SetEnabled(this, !(flag == GeneralProcess.Delete));
            _flag = flag;
            obj._CopyTo(_objNew);
            obj._CopyTo(_objOld);
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;

            UIHelper.SetDataSourceToComboBox(cboFuelType, Lookup.GetFuelType());
            UIHelper.SetDataSourceToComboBox(cboFuelSource, Lookup.GetFuelSource());
            UIHelper.SetDataSourceToComboBox(cboCurrency, Lookup.GetCurrencies());
            new AccountChartPopulator().PopluateTree(cboAccountPayment.TreeView, AccountChartHelper.GetAccounts(AccountConfig.PAYMENT_ACCOUNTS));

            read();
        }

        #endregion

        #region Operation 
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }

            write();

            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        _objNew.CREATE_BY = Login.CurrentLogin.LOGIN_NAME;
                        _objNew.CREATE_ON = today;
                        DBDataContext.Db.Insert(_objNew);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objNew);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }

        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// Change current keyboard layout to khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        /// <summary>
        /// Change current keyboard layout to English.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEnglishKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        #endregion

        #region Method
        /// <summary>
        /// Load data from object.
        /// </summary>
        private void read()
        {
            txtRefNo.Text = _objNew.REF_NO;
            dtpPurchaseDate.Value = _objNew.PURCHASE_DATE;
            cboFuelType.SelectedValue = _objNew.FUEL_TYPE_ID;
            cboFuelSource.SelectedValue = _objNew.FUEL_SOURCE_ID;
            cboCurrency.SelectedValue = _objNew.CURRENCY_ID;
            txtPrice.Text = UIHelper.FormatCurrency(_objNew.PRICE, _objNew.CURRENCY_ID);
            txtVolume.Text = _objNew.VOLUME.ToString("N2");
            txtTotal.Text = UIHelper.FormatCurrency(_objNew.TOTAL_AMOUNT, _objNew.CURRENCY_ID);
            txtNote.Text = _objNew.NOTE;
            cboAccountPayment.TreeView.SelectedNode = cboAccountPayment.TreeView.GetNodesIncludeAncestors().FirstOrDefault(x => (int)x.Tag == _objNew.PAYMENT_ACCOUNT_ID);
            cboAccountPayment.Text = cboAccountPayment.TreeView.SelectedNode != null ? cboAccountPayment.TreeView.SelectedNode.Text : "";
            txtTranBy.Text = _objNew.TRAN_BY;
            load = false;
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            btnOK.Focus();

            _objNew.REF_NO = txtRefNo.Text;
            _objNew.PURCHASE_DATE = dtpPurchaseDate.Value;
            _objNew.FUEL_TYPE_ID = (int)cboFuelType.SelectedValue;
            _objNew.FUEL_SOURCE_ID = (int)cboFuelSource.SelectedValue;
            _objNew.CURRENCY_ID = (int)cboCurrency.SelectedValue;
            _objNew.PRICE = DataHelper.ParseToDecimal(txtPrice.Text);
            _objNew.VOLUME = DataHelper.ParseToDecimal(txtVolume.Text);
            _objNew.TOTAL_AMOUNT = DataHelper.ParseToDecimal(txtTotal.Text);
            _objNew.TRAN_BY = txtTranBy.Text;
            _objNew.NOTE = txtNote.Text;
            // Account
            _objNew.PAYMENT_ACCOUNT_ID = (int)cboAccountPayment.TreeView.SelectedNode.Tag;

        }

        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();
            if (cboFuelType.SelectedIndex == -1)
            {
                cboFuelType.SetValidation(string.Format(Resources.REQUIRED, lblFUEL_TYPE.Text));
                val = true;
            }
            if (cboFuelSource.SelectedIndex == -1)
            {
                cboFuelSource.SetValidation(string.Format(Resources.REQUIRED, lblFUEL_SOURCE.Text));
                val = true;
            }
            if (cboAccountPayment.TreeView.SelectedNode == null)
            {
                cboAccountPayment.SetValidation(string.Format(Resources.REQUIRED, lblPAYMENT_ACCOUNT.Text));
                val = true;
            }
            if (txtTranBy.Text == "")
            {
                txtTranBy.SetValidation(string.Format(Resources.REQUIRED, lblCREATE_BY.Text));
                val = true;
            }
            if (txtPrice.Text.Trim() == string.Empty)
            {
                txtPrice.SetValidation(string.Format(Resources.REQUIRED, lblFUEL_PRICE.Text));
                val = true;
            }
            if (DataHelper.ParseToInt(txtPrice.Text) < 0)
            {
                txtPrice.SetValidation(Resources.REQUIRED_POSITIVE_NUMBER);
                val = true;
            }
            if (DataHelper.ParseToInt(txtVolume.Text) < 0)
            {
                txtVolume.SetValidation(Resources.REQUIRED_POSITIVE_NUMBER);
                val = true;
            }
            if (txtVolume.Text.Trim() == string.Empty)
            {
                txtVolume.SetValidation(string.Format(Resources.REQUIRED, lblVOLUME));
                val = true;
            }
            if (cboCurrency.SelectedIndex == -1)
            {
                cboCurrency.SetValidation(string.Format(Resources.REQUIRED, lblCURRENCY.Text));
                val = true;
            }

            return val;
        }

        private void txtCalcAmount(object sender, EventArgs e)
        {
            txtTotal.Text = UIHelper.FormatCurrency((DataHelper.ParseToDecimal(txtPrice.Text) * DataHelper.ParseToDecimal(txtVolume.Text)), (int)cboCurrency.SelectedValue);
        }
        #endregion

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }

        private void cboCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (load)
            {
                txtCalcAmount(null, null);
            }
        }


    }
}
