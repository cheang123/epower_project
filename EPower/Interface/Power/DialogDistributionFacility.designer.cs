﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogDistributionFacility
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogDistributionFacility));
            this.lblLENGTH_OF_LINE_AND_CABLE_TYPE = new System.Windows.Forms.Label();
            this.txtLine = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.label9 = new System.Windows.Forms.Label();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.label4 = new System.Windows.Forms.Label();
            this.cboDistribution = new System.Windows.Forms.ComboBox();
            this.lblDISTRIBUTION = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cboVoltage = new System.Windows.Forms.ComboBox();
            this.lblVOLTAGE = new System.Windows.Forms.Label();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.lblSTART_USE_DATE = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblEND_USE_DATE = new System.Windows.Forms.Label();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.cboPhase = new System.Windows.Forms.ComboBox();
            this.lblPHASE = new System.Windows.Forms.Label();
            this.lblNOTE = new System.Windows.Forms.Label();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.lblTOTAL_POLE_AND_ENDING_VOLTAGE = new System.Windows.Forms.Label();
            this.txtTotalPole = new System.Windows.Forms.TextBox();
            this.txtEndingVoltage = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.cboType = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.txtNote);
            this.content.Controls.Add(this.lblNOTE);
            this.content.Controls.Add(this.label3);
            this.content.Controls.Add(this.cboType);
            this.content.Controls.Add(this.cboPhase);
            this.content.Controls.Add(this.lblPHASE);
            this.content.Controls.Add(this.lblEND_USE_DATE);
            this.content.Controls.Add(this.dtpEndDate);
            this.content.Controls.Add(this.label11);
            this.content.Controls.Add(this.lblSTART_USE_DATE);
            this.content.Controls.Add(this.dtpStartDate);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.cboVoltage);
            this.content.Controls.Add(this.lblVOLTAGE);
            this.content.Controls.Add(this.label4);
            this.content.Controls.Add(this.cboDistribution);
            this.content.Controls.Add(this.lblDISTRIBUTION);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.txtEndingVoltage);
            this.content.Controls.Add(this.txtTotalPole);
            this.content.Controls.Add(this.lblTOTAL_POLE_AND_ENDING_VOLTAGE);
            this.content.Controls.Add(this.txtLine);
            this.content.Controls.Add(this.label15);
            this.content.Controls.Add(this.label17);
            this.content.Controls.Add(this.lblLENGTH_OF_LINE_AND_CABLE_TYPE);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblLENGTH_OF_LINE_AND_CABLE_TYPE, 0);
            this.content.Controls.SetChildIndex(this.label17, 0);
            this.content.Controls.SetChildIndex(this.label15, 0);
            this.content.Controls.SetChildIndex(this.txtLine, 0);
            this.content.Controls.SetChildIndex(this.lblTOTAL_POLE_AND_ENDING_VOLTAGE, 0);
            this.content.Controls.SetChildIndex(this.txtTotalPole, 0);
            this.content.Controls.SetChildIndex(this.txtEndingVoltage, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.lblDISTRIBUTION, 0);
            this.content.Controls.SetChildIndex(this.cboDistribution, 0);
            this.content.Controls.SetChildIndex(this.label4, 0);
            this.content.Controls.SetChildIndex(this.lblVOLTAGE, 0);
            this.content.Controls.SetChildIndex(this.cboVoltage, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.dtpStartDate, 0);
            this.content.Controls.SetChildIndex(this.lblSTART_USE_DATE, 0);
            this.content.Controls.SetChildIndex(this.label11, 0);
            this.content.Controls.SetChildIndex(this.dtpEndDate, 0);
            this.content.Controls.SetChildIndex(this.lblEND_USE_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblPHASE, 0);
            this.content.Controls.SetChildIndex(this.cboPhase, 0);
            this.content.Controls.SetChildIndex(this.cboType, 0);
            this.content.Controls.SetChildIndex(this.label3, 0);
            this.content.Controls.SetChildIndex(this.lblNOTE, 0);
            this.content.Controls.SetChildIndex(this.txtNote, 0);
            // 
            // lblLENGTH_OF_LINE_AND_CABLE_TYPE
            // 
            resources.ApplyResources(this.lblLENGTH_OF_LINE_AND_CABLE_TYPE, "lblLENGTH_OF_LINE_AND_CABLE_TYPE");
            this.lblLENGTH_OF_LINE_AND_CABLE_TYPE.Name = "lblLENGTH_OF_LINE_AND_CABLE_TYPE";
            // 
            // txtLine
            // 
            resources.ApplyResources(this.txtLine, "txtLine");
            this.txtLine.Name = "txtLine";
            this.txtLine.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            this.txtLine.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLine_KeyPress);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Name = "label4";
            // 
            // cboDistribution
            // 
            this.cboDistribution.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDistribution.FormattingEnabled = true;
            resources.ApplyResources(this.cboDistribution, "cboDistribution");
            this.cboDistribution.Name = "cboDistribution";
            this.cboDistribution.SelectedIndexChanged += new System.EventHandler(this.cboDistribution_SelectedIndexChanged);
            // 
            // lblDISTRIBUTION
            // 
            resources.ApplyResources(this.lblDISTRIBUTION, "lblDISTRIBUTION");
            this.lblDISTRIBUTION.Name = "lblDISTRIBUTION";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // cboVoltage
            // 
            this.cboVoltage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboVoltage.FormattingEnabled = true;
            resources.ApplyResources(this.cboVoltage, "cboVoltage");
            this.cboVoltage.Name = "cboVoltage";
            // 
            // lblVOLTAGE
            // 
            resources.ApplyResources(this.lblVOLTAGE, "lblVOLTAGE");
            this.lblVOLTAGE.Name = "lblVOLTAGE";
            // 
            // dtpStartDate
            // 
            resources.ApplyResources(this.dtpStartDate, "dtpStartDate");
            this.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartDate.Name = "dtpStartDate";
            // 
            // lblSTART_USE_DATE
            // 
            resources.ApplyResources(this.lblSTART_USE_DATE, "lblSTART_USE_DATE");
            this.lblSTART_USE_DATE.Name = "lblSTART_USE_DATE";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Name = "label11";
            // 
            // lblEND_USE_DATE
            // 
            resources.ApplyResources(this.lblEND_USE_DATE, "lblEND_USE_DATE");
            this.lblEND_USE_DATE.Name = "lblEND_USE_DATE";
            // 
            // dtpEndDate
            // 
            this.dtpEndDate.Checked = false;
            resources.ApplyResources(this.dtpEndDate, "dtpEndDate");
            this.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.ShowCheckBox = true;
            this.dtpEndDate.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dtpEndDate_MouseDown);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Name = "label3";
            // 
            // cboPhase
            // 
            this.cboPhase.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPhase.FormattingEnabled = true;
            resources.ApplyResources(this.cboPhase, "cboPhase");
            this.cboPhase.Name = "cboPhase";
            // 
            // lblPHASE
            // 
            resources.ApplyResources(this.lblPHASE, "lblPHASE");
            this.lblPHASE.Name = "lblPHASE";
            // 
            // lblNOTE
            // 
            resources.ApplyResources(this.lblNOTE, "lblNOTE");
            this.lblNOTE.Name = "lblNOTE";
            // 
            // txtNote
            // 
            resources.ApplyResources(this.txtNote, "txtNote");
            this.txtNote.Name = "txtNote";
            this.txtNote.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // lblTOTAL_POLE_AND_ENDING_VOLTAGE
            // 
            resources.ApplyResources(this.lblTOTAL_POLE_AND_ENDING_VOLTAGE, "lblTOTAL_POLE_AND_ENDING_VOLTAGE");
            this.lblTOTAL_POLE_AND_ENDING_VOLTAGE.Name = "lblTOTAL_POLE_AND_ENDING_VOLTAGE";
            // 
            // txtTotalPole
            // 
            resources.ApplyResources(this.txtTotalPole, "txtTotalPole");
            this.txtTotalPole.Name = "txtTotalPole";
            this.txtTotalPole.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            this.txtTotalPole.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTotalPole_KeyPress);
            // 
            // txtEndingVoltage
            // 
            resources.ApplyResources(this.txtEndingVoltage, "txtEndingVoltage");
            this.txtEndingVoltage.Name = "txtEndingVoltage";
            this.txtEndingVoltage.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // label17
            // 
            resources.ApplyResources(this.label17, "label17");
            this.label17.Name = "label17";
            // 
            // cboType
            // 
            this.cboType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboType.FormattingEnabled = true;
            this.cboType.Items.AddRange(new object[] {
            resources.GetString("cboType.Items"),
            resources.GetString("cboType.Items1")});
            resources.ApplyResources(this.cboType, "cboType");
            this.cboType.Name = "cboType";
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.Name = "label15";
            // 
            // DialogDistributionFacility
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogDistributionFacility";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBox txtLine;
        private Label lblLENGTH_OF_LINE_AND_CABLE_TYPE;
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label label9;
        private ExButton btnCHANGE_LOG;
        private Label label1;
        private ComboBox cboVoltage;
        private Label lblVOLTAGE;
        private Label label4;
        private ComboBox cboDistribution;
        private Label lblDISTRIBUTION;
        private Label lblSTART_USE_DATE;
        private DateTimePicker dtpStartDate;
        private Label label11;
        private Label lblEND_USE_DATE;
        private DateTimePicker dtpEndDate;
        private Label label3;
        private ComboBox cboPhase;
        private Label lblPHASE;
        private TextBox txtNote;
        private Label lblNOTE;
        private TextBox txtTotalPole;
        private Label lblTOTAL_POLE_AND_ENDING_VOLTAGE;
        private TextBox txtEndingVoltage;
        private Label label17;
        private ComboBox cboType;
        private Label label15;
    }
}