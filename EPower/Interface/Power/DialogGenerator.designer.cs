﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogGenerator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogGenerator));
            this.lblGENERATOR_NO = new System.Windows.Forms.Label();
            this.txtGeneratorNo = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.label9 = new System.Windows.Forms.Label();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.cboCapacity = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cboFuleType = new System.Windows.Forms.ComboBox();
            this.lblFUEL_TYPE_AND_CAPACITY = new System.Windows.Forms.Label();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.lblSTART_USE_DATE = new System.Windows.Forms.Label();
            this.lblEND_USE_DATE = new System.Windows.Forms.Label();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.lblNOTE = new System.Windows.Forms.Label();
            this.lblHORSE_POWER = new System.Windows.Forms.Label();
            this.txtHorsePower = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.lblOPERATION_TIMEI = new System.Windows.Forms.Label();
            this.txtShift1 = new System.Windows.Forms.TextBox();
            this.lblOPERATION_TIMEII = new System.Windows.Forms.Label();
            this.txtShift2 = new System.Windows.Forms.TextBox();
            this.lblOPERATION_HOUR = new System.Windows.Forms.Label();
            this.txtOperationHour = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.txtNote);
            this.content.Controls.Add(this.lblNOTE);
            this.content.Controls.Add(this.lblEND_USE_DATE);
            this.content.Controls.Add(this.dtpEndDate);
            this.content.Controls.Add(this.lblSTART_USE_DATE);
            this.content.Controls.Add(this.dtpStartDate);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.cboFuleType);
            this.content.Controls.Add(this.lblFUEL_TYPE_AND_CAPACITY);
            this.content.Controls.Add(this.cboCapacity);
            this.content.Controls.Add(this.label6);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.label11);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.txtHorsePower);
            this.content.Controls.Add(this.lblHORSE_POWER);
            this.content.Controls.Add(this.txtShift2);
            this.content.Controls.Add(this.txtOperationHour);
            this.content.Controls.Add(this.txtShift1);
            this.content.Controls.Add(this.txtGeneratorNo);
            this.content.Controls.Add(this.lblOPERATION_HOUR);
            this.content.Controls.Add(this.lblOPERATION_TIMEII);
            this.content.Controls.Add(this.lblOPERATION_TIMEI);
            this.content.Controls.Add(this.lblGENERATOR_NO);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblGENERATOR_NO, 0);
            this.content.Controls.SetChildIndex(this.lblOPERATION_TIMEI, 0);
            this.content.Controls.SetChildIndex(this.lblOPERATION_TIMEII, 0);
            this.content.Controls.SetChildIndex(this.lblOPERATION_HOUR, 0);
            this.content.Controls.SetChildIndex(this.txtGeneratorNo, 0);
            this.content.Controls.SetChildIndex(this.txtShift1, 0);
            this.content.Controls.SetChildIndex(this.txtOperationHour, 0);
            this.content.Controls.SetChildIndex(this.txtShift2, 0);
            this.content.Controls.SetChildIndex(this.lblHORSE_POWER, 0);
            this.content.Controls.SetChildIndex(this.txtHorsePower, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.label11, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.label6, 0);
            this.content.Controls.SetChildIndex(this.cboCapacity, 0);
            this.content.Controls.SetChildIndex(this.lblFUEL_TYPE_AND_CAPACITY, 0);
            this.content.Controls.SetChildIndex(this.cboFuleType, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.dtpStartDate, 0);
            this.content.Controls.SetChildIndex(this.lblSTART_USE_DATE, 0);
            this.content.Controls.SetChildIndex(this.dtpEndDate, 0);
            this.content.Controls.SetChildIndex(this.lblEND_USE_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblNOTE, 0);
            this.content.Controls.SetChildIndex(this.txtNote, 0);
            // 
            // lblGENERATOR_NO
            // 
            resources.ApplyResources(this.lblGENERATOR_NO, "lblGENERATOR_NO");
            this.lblGENERATOR_NO.Name = "lblGENERATOR_NO";
            // 
            // txtGeneratorNo
            // 
            resources.ApplyResources(this.txtGeneratorNo, "txtGeneratorNo");
            this.txtGeneratorNo.Name = "txtGeneratorNo";
            this.txtGeneratorNo.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // cboCapacity
            // 
            this.cboCapacity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboCapacity, "cboCapacity");
            this.cboCapacity.Name = "cboCapacity";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // cboFuleType
            // 
            this.cboFuleType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFuleType.FormattingEnabled = true;
            resources.ApplyResources(this.cboFuleType, "cboFuleType");
            this.cboFuleType.Name = "cboFuleType";
            // 
            // lblFUEL_TYPE_AND_CAPACITY
            // 
            resources.ApplyResources(this.lblFUEL_TYPE_AND_CAPACITY, "lblFUEL_TYPE_AND_CAPACITY");
            this.lblFUEL_TYPE_AND_CAPACITY.Name = "lblFUEL_TYPE_AND_CAPACITY";
            // 
            // dtpStartDate
            // 
            resources.ApplyResources(this.dtpStartDate, "dtpStartDate");
            this.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartDate.Name = "dtpStartDate";
            // 
            // lblSTART_USE_DATE
            // 
            resources.ApplyResources(this.lblSTART_USE_DATE, "lblSTART_USE_DATE");
            this.lblSTART_USE_DATE.Name = "lblSTART_USE_DATE";
            // 
            // lblEND_USE_DATE
            // 
            resources.ApplyResources(this.lblEND_USE_DATE, "lblEND_USE_DATE");
            this.lblEND_USE_DATE.Name = "lblEND_USE_DATE";
            // 
            // dtpEndDate
            // 
            this.dtpEndDate.Checked = false;
            resources.ApplyResources(this.dtpEndDate, "dtpEndDate");
            this.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.ShowCheckBox = true;
            this.dtpEndDate.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dtpEndDate_MouseDown);
            // 
            // txtNote
            // 
            resources.ApplyResources(this.txtNote, "txtNote");
            this.txtNote.Name = "txtNote";
            this.txtNote.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // lblNOTE
            // 
            resources.ApplyResources(this.lblNOTE, "lblNOTE");
            this.lblNOTE.Name = "lblNOTE";
            // 
            // lblHORSE_POWER
            // 
            resources.ApplyResources(this.lblHORSE_POWER, "lblHORSE_POWER");
            this.lblHORSE_POWER.Name = "lblHORSE_POWER";
            // 
            // txtHorsePower
            // 
            resources.ApplyResources(this.txtHorsePower, "txtHorsePower");
            this.txtHorsePower.Name = "txtHorsePower";
            this.txtHorsePower.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            this.txtHorsePower.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtOperationHour_KeyPress);
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Name = "label11";
            // 
            // lblOPERATION_TIMEI
            // 
            resources.ApplyResources(this.lblOPERATION_TIMEI, "lblOPERATION_TIMEI");
            this.lblOPERATION_TIMEI.Name = "lblOPERATION_TIMEI";
            // 
            // txtShift1
            // 
            resources.ApplyResources(this.txtShift1, "txtShift1");
            this.txtShift1.Name = "txtShift1";
            this.txtShift1.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // lblOPERATION_TIMEII
            // 
            resources.ApplyResources(this.lblOPERATION_TIMEII, "lblOPERATION_TIMEII");
            this.lblOPERATION_TIMEII.Name = "lblOPERATION_TIMEII";
            // 
            // txtShift2
            // 
            resources.ApplyResources(this.txtShift2, "txtShift2");
            this.txtShift2.Name = "txtShift2";
            this.txtShift2.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // lblOPERATION_HOUR
            // 
            resources.ApplyResources(this.lblOPERATION_HOUR, "lblOPERATION_HOUR");
            this.lblOPERATION_HOUR.Name = "lblOPERATION_HOUR";
            // 
            // txtOperationHour
            // 
            resources.ApplyResources(this.txtOperationHour, "txtOperationHour");
            this.txtOperationHour.Name = "txtOperationHour";
            this.txtOperationHour.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            this.txtOperationHour.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtOperationHour_KeyPress);
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // DialogGenerator
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogGenerator";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBox txtGeneratorNo;
        private Label lblGENERATOR_NO;
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label label9;
        private ExButton btnCHANGE_LOG;
        private Label label1;
        private ComboBox cboFuleType;
        private Label lblFUEL_TYPE_AND_CAPACITY;
        private ComboBox cboCapacity;
        private Label lblSTART_USE_DATE;
        private DateTimePicker dtpStartDate;
        private Label lblEND_USE_DATE;
        private DateTimePicker dtpEndDate;
        private TextBox txtNote;
        private Label lblNOTE;
        private Label label11;
        private TextBox txtHorsePower;
        private Label lblHORSE_POWER;
        private TextBox txtShift1;
        private Label lblOPERATION_TIMEI;
        private TextBox txtShift2;
        private TextBox txtOperationHour;
        private Label lblOPERATION_HOUR;
        private Label lblOPERATION_TIMEII;
        private Label label6;
    }
}