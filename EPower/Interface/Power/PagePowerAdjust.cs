﻿using System;
using System.Linq;
using System.Windows.Forms;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface.Power
{
    public partial class PagePowerAdjust : Form
    {
        public TBL_POWER_ADJUST PoweerAdjust
        {
            get
            {
                TBL_POWER_ADJUST objPowerAdjust = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    try
                    {
                        int id = (int)dgv.SelectedRows[0].Cells[COL_ADJUST_ID.Name].Value;
                        objPowerAdjust = DBDataContext.Db.TBL_POWER_ADJUSTs.FirstOrDefault(row => row.ADJUST_ID==id);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objPowerAdjust;
            }
            
        }

        #region Constructor
        public PagePowerAdjust()
        {
            try
            {
                InitializeComponent();
                UIHelper.DataGridViewProperties(dgv);
                load();
            }
            catch (Exception ex) {
                MsgBox.ShowError(ex);
            }
        }
        #endregion

        #region Method

        private void load()
        {
            dgv.DataSource =from pa in DBDataContext.Db.TBL_POWER_ADJUSTs
                            where pa.IS_ACTIVE
                                  && (dtpDate.Checked == false || pa.ADJUST_MONTH.Year == dtpDate.Value.Year)
                            orderby pa.ADJUST_MONTH
                            select new  
                            {
                                pa.ADJUST_ID
                                ,pa.ADJUST_MONTH 
                                ,pa.POWER_POSTPAID
                                ,pa.POWER_PREPAID
                                ,pa.POWER_PRODUCTION
                                ,pa.POWER_DISCOUNT 
                                ,pa.IS_ACTIVE
                            };
        }

        #endregion

        private void btnNew_Click(object sender, EventArgs e)
        {
            DialogPowerAdjustment dig = new DialogPowerAdjustment(GeneralProcess.Insert, new TBL_POWER_ADJUST() { ADJUST_MONTH=DateTime.Now});
            if (dig.ShowDialog() == DialogResult.OK)
            {
                load();
                UIHelper.SelectRow(dgv, dig.Object.ADJUST_ID);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgv.Rows.Count == 0) return;
            int id=(int)dgv.SelectedRows[0].Cells[COL_ADJUST_ID.Name].Value;
            TBL_POWER_ADJUST obj = DBDataContext.Db.TBL_POWER_ADJUSTs.FirstOrDefault(row => row.ADJUST_ID == id);
            DialogPowerAdjustment dig = new DialogPowerAdjustment(GeneralProcess.Update, obj);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                load();
                UIHelper.SelectRow(dgv, dig.Object.ADJUST_ID);
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (dgv.Rows.Count == 0) return;
            int id = (int)dgv.SelectedRows[0].Cells[COL_ADJUST_ID.Name].Value;
            TBL_POWER_ADJUST obj = DBDataContext.Db.TBL_POWER_ADJUSTs.FirstOrDefault(row => row.ADJUST_ID == id);
            DialogPowerAdjustment dig = new DialogPowerAdjustment(GeneralProcess.Delete, obj);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                load();
                UIHelper.SelectRow(dgv, dig.Object.ADJUST_ID-1);
            }
        }

        private void dtpDate_ValueChanged(object sender, EventArgs e)
        {
            load();
        }

    }
}
