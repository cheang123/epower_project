﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PagePowerSource : Form
    {        
        
        public  TBL_POWER_SOURCE PowerSource
        {
            get
            {
                TBL_POWER_SOURCE objPurchaseAgreement = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    try
                    {
                        int intAgreeID = (int)dgv.SelectedRows[0].Cells[SOURCE_ID.Name].Value;
                        objPurchaseAgreement = DBDataContext.Db.TBL_POWER_SOURCEs.FirstOrDefault(x => x.SOURCE_ID == intAgreeID);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objPurchaseAgreement;
            }
        }

        #region Constructor
        public PagePowerSource()
        {
            InitializeComponent();
            this.btnREPORT.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_QUARTERLY_REPORT]);
            UIHelper.DataGridViewProperties(dgv);
            txt_QuickSearch(null, null);
        }
        #endregion

        #region Operation
        /// <summary>
        /// Add new ampare.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNew_Click(object sender, EventArgs e)
        { 
            DialogPowerSource dig = new DialogPowerSource(GeneralProcess.Insert, new TBL_POWER_SOURCE() { START_DATE = DateTime.Now,END_DATE=UIHelper._DefaultDate,IS_INUSED=true,IS_ACTIVE=true,POWER_PURCHASE_LICENSEE_ID=0,CUSTOMER_CONNECTION_TYPE_ID=0 });
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.PowerSource.SOURCE_ID);
            }
        }

        /// <summary>
        /// Edit ampare
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                DialogPowerSource dig = new DialogPowerSource(GeneralProcess.Update, PowerSource);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.PowerSource.SOURCE_ID);
                }
            }
        }

        /// <summary>
        /// Remove ampare
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (PowerSource==null)
            {
                return;
            }

            if (!IsDeletetable())
            {
                MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE);
                return;
            }

            DialogPowerSource dig = new DialogPowerSource(GeneralProcess.Delete, PowerSource);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.PowerSource.SOURCE_ID - 1);
            }

        }

        private bool IsDeletetable()
        {
            return DBDataContext.Db.TBL_POWER_PURCHASEs.Where(x => x.SOURCE_ID == PowerSource.SOURCE_ID && x.IS_ACTIVE).Count() == 0;
        }

        /// <summary>
        /// Load data from database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            try
            {
                dgv.DataSource = from pa in DBDataContext.Db.TBL_POWER_SOURCEs
                                 join v in DBDataContext.Db.TBL_VOLTAGEs on pa.VOLTAGE_ID equals v.VOLTAGE_ID
                                 join c in DBDataContext.Db.TLKP_CURRENCies on pa.CURRENCY_ID equals c.CURRENCY_ID
                                 where pa.IS_ACTIVE
                                        && pa.SELLER_NAME.ToLower().Contains(txtQuickSearch.Text.ToLower())
                                 select new
                                 {
                                     pa.SOURCE_ID,
                                     pa.SELLER_NAME,
                                     v.VOLTAGE_NAME,
                                     pa.PRICE,
                                     c.CURRENCY_SING,
                                     pa.START_DATE,
                                     END_DATE = pa.IS_INUSED ? string.Empty : string.Concat(pa.END_DATE.Day, "-", pa.END_DATE.Month, "-", pa.END_DATE.Year),
                                     pa.NO_OF_CONNECTION,
                                     pa.NOTE
                                 };
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
            
                
        }

        /// <summary>
        /// Change current keyboard layout to khmer.
        /// </summary>.
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        #endregion

        private void btnReport_Click(object sender, EventArgs e)
        {
            var diag = new DialogReportQuarterOptions(false);
            diag.chkTABLE_POWER_SOURCE.Checked = true;
            if (diag.ShowDialog() == DialogResult.OK)
            {
                var doc = PageReportQuaterly.OpenReport(diag);
                doc.ViewReport("");
            }
        }

        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }

        
    }
}
