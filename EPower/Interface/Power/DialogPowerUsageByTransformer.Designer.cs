﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogPowerUsageByTransformer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogPowerUsageByTransformer));
            this.lblUSAGE = new System.Windows.Forms.Label();
            this.txtInputUsagePostpaid = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.lblMONTH = new System.Windows.Forms.Label();
            this.dtp = new System.Windows.Forms.DateTimePicker();
            this.lblTRANSFORMER = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.cboTransformer = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtAdjustPower = new System.Windows.Forms.TextBox();
            this.lblADJUST_USAGE = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtMulti = new System.Windows.Forms.TextBox();
            this.lblMULTIPIER = new System.Windows.Forms.Label();
            this.lblMETER_CODE = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtEnd = new System.Windows.Forms.TextBox();
            this.lblEND_USAGE = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtStart = new System.Windows.Forms.TextBox();
            this.lblSTART_USAGE = new System.Windows.Forms.Label();
            this.cboMeter = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.label2);
            this.content.Controls.Add(this.cboMeter);
            this.content.Controls.Add(this.txtAdjustPower);
            this.content.Controls.Add(this.lblADJUST_USAGE);
            this.content.Controls.Add(this.label23);
            this.content.Controls.Add(this.txtMulti);
            this.content.Controls.Add(this.lblMULTIPIER);
            this.content.Controls.Add(this.lblMETER_CODE);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.txtEnd);
            this.content.Controls.Add(this.lblEND_USAGE);
            this.content.Controls.Add(this.label5);
            this.content.Controls.Add(this.txtStart);
            this.content.Controls.Add(this.lblSTART_USAGE);
            this.content.Controls.Add(this.label15);
            this.content.Controls.Add(this.cboTransformer);
            this.content.Controls.Add(this.label14);
            this.content.Controls.Add(this.lblTRANSFORMER);
            this.content.Controls.Add(this.dtp);
            this.content.Controls.Add(this.lblMONTH);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.txtInputUsagePostpaid);
            this.content.Controls.Add(this.lblUSAGE);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblUSAGE, 0);
            this.content.Controls.SetChildIndex(this.txtInputUsagePostpaid, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.lblMONTH, 0);
            this.content.Controls.SetChildIndex(this.dtp, 0);
            this.content.Controls.SetChildIndex(this.lblTRANSFORMER, 0);
            this.content.Controls.SetChildIndex(this.label14, 0);
            this.content.Controls.SetChildIndex(this.cboTransformer, 0);
            this.content.Controls.SetChildIndex(this.label15, 0);
            this.content.Controls.SetChildIndex(this.lblSTART_USAGE, 0);
            this.content.Controls.SetChildIndex(this.txtStart, 0);
            this.content.Controls.SetChildIndex(this.label5, 0);
            this.content.Controls.SetChildIndex(this.lblEND_USAGE, 0);
            this.content.Controls.SetChildIndex(this.txtEnd, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_CODE, 0);
            this.content.Controls.SetChildIndex(this.lblMULTIPIER, 0);
            this.content.Controls.SetChildIndex(this.txtMulti, 0);
            this.content.Controls.SetChildIndex(this.label23, 0);
            this.content.Controls.SetChildIndex(this.lblADJUST_USAGE, 0);
            this.content.Controls.SetChildIndex(this.txtAdjustPower, 0);
            this.content.Controls.SetChildIndex(this.cboMeter, 0);
            this.content.Controls.SetChildIndex(this.label2, 0);
            // 
            // lblUSAGE
            // 
            resources.ApplyResources(this.lblUSAGE, "lblUSAGE");
            this.lblUSAGE.Name = "lblUSAGE";
            // 
            // txtInputUsagePostpaid
            // 
            resources.ApplyResources(this.txtInputUsagePostpaid, "txtInputUsagePostpaid");
            this.txtInputUsagePostpaid.Name = "txtInputUsagePostpaid";
            this.txtInputUsagePostpaid.ReadOnly = true;
            this.txtInputUsagePostpaid.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // lblMONTH
            // 
            resources.ApplyResources(this.lblMONTH, "lblMONTH");
            this.lblMONTH.Name = "lblMONTH";
            // 
            // dtp
            // 
            resources.ApplyResources(this.dtp, "dtp");
            this.dtp.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp.Name = "dtp";
            this.dtp.ValueChanged += new System.EventHandler(this.dtp_ValueChanged);
            this.dtp.Enter += new System.EventHandler(this.dtp_Enter);
            // 
            // lblTRANSFORMER
            // 
            resources.ApplyResources(this.lblTRANSFORMER, "lblTRANSFORMER");
            this.lblTRANSFORMER.Name = "lblTRANSFORMER";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Name = "label14";
            // 
            // cboTransformer
            // 
            this.cboTransformer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTransformer.FormattingEnabled = true;
            resources.ApplyResources(this.cboTransformer, "cboTransformer");
            this.cboTransformer.Name = "cboTransformer";
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Name = "label15";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // txtAdjustPower
            // 
            this.txtAdjustPower.AcceptsTab = true;
            resources.ApplyResources(this.txtAdjustPower, "txtAdjustPower");
            this.txtAdjustPower.Name = "txtAdjustPower";
            this.txtAdjustPower.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // lblADJUST_USAGE
            // 
            resources.ApplyResources(this.lblADJUST_USAGE, "lblADJUST_USAGE");
            this.lblADJUST_USAGE.Name = "lblADJUST_USAGE";
            // 
            // label23
            // 
            resources.ApplyResources(this.label23, "label23");
            this.label23.ForeColor = System.Drawing.Color.Red;
            this.label23.Name = "label23";
            // 
            // txtMulti
            // 
            this.txtMulti.AcceptsTab = true;
            resources.ApplyResources(this.txtMulti, "txtMulti");
            this.txtMulti.Name = "txtMulti";
            this.txtMulti.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // lblMULTIPIER
            // 
            resources.ApplyResources(this.lblMULTIPIER, "lblMULTIPIER");
            this.lblMULTIPIER.Name = "lblMULTIPIER";
            // 
            // lblMETER_CODE
            // 
            resources.ApplyResources(this.lblMETER_CODE, "lblMETER_CODE");
            this.lblMETER_CODE.Name = "lblMETER_CODE";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // txtEnd
            // 
            this.txtEnd.AcceptsTab = true;
            resources.ApplyResources(this.txtEnd, "txtEnd");
            this.txtEnd.Name = "txtEnd";
            this.txtEnd.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // lblEND_USAGE
            // 
            resources.ApplyResources(this.lblEND_USAGE, "lblEND_USAGE");
            this.lblEND_USAGE.Name = "lblEND_USAGE";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Name = "label5";
            // 
            // txtStart
            // 
            this.txtStart.AcceptsTab = true;
            resources.ApplyResources(this.txtStart, "txtStart");
            this.txtStart.Name = "txtStart";
            this.txtStart.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // lblSTART_USAGE
            // 
            resources.ApplyResources(this.lblSTART_USAGE, "lblSTART_USAGE");
            this.lblSTART_USAGE.Name = "lblSTART_USAGE";
            // 
            // cboMeter
            // 
            this.cboMeter.FormattingEnabled = true;
            resources.ApplyResources(this.cboMeter, "cboMeter");
            this.cboMeter.Name = "cboMeter";
            this.cboMeter.SelectedIndexChanged += new System.EventHandler(this.cboMeter_SelectedIndexChanged);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Name = "label2";
            // 
            // DialogPowerUsageByTransformer
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogPowerUsageByTransformer";
            this.Load += new System.EventHandler(this.DialogPowerUsageByTransformer_Load);
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBox txtInputUsagePostpaid;
        private Label lblUSAGE;
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private ExButton btnCHANGE_LOG;
        private DateTimePicker dtp;
        private Label lblMONTH;
        private ComboBox cboTransformer;
        private Label label14;
        private Label lblTRANSFORMER;
        private Label label15;
        private Label label9;
        private TextBox txtAdjustPower;
        private Label lblADJUST_USAGE;
        private Label label23;
        private TextBox txtMulti;
        private Label lblMULTIPIER;
        private Label lblMETER_CODE;
        private Label label1;
        private TextBox txtEnd;
        private Label lblEND_USAGE;
        private Label label5;
        private TextBox txtStart;
        private Label lblSTART_USAGE;
        private ComboBox cboMeter;
        private Label label2;
    }
}