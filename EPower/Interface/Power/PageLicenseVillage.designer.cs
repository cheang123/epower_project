﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageLicenseVillage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageLicenseVillage));
            this.dgv = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnREPORT = new SoftTech.Component.ExButton();
            this.btnEDIT_VILLAGE = new SoftTech.Component.ExButton();
            this.txtQuickSearch = new SoftTech.Component.ExTextbox();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VILLAGE_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROVINCE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DISTRICT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COMMUNE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VILLAGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_FAMILY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.VILLAGE_CODE,
            this.PROVINCE,
            this.DISTRICT,
            this.COMMUNE,
            this.VILLAGE,
            this.TOTAL_FAMILY});
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellEndEdit);
            this.dgv.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgv_DataError);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.flowLayoutPanel1);
            this.panel1.Controls.Add(this.txtQuickSearch);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnREPORT);
            this.flowLayoutPanel1.Controls.Add(this.btnEDIT_VILLAGE);
            resources.ApplyResources(this.flowLayoutPanel1, "flowLayoutPanel1");
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            // 
            // btnREPORT
            // 
            resources.ApplyResources(this.btnREPORT, "btnREPORT");
            this.btnREPORT.Name = "btnREPORT";
            this.btnREPORT.UseVisualStyleBackColor = true;
            this.btnREPORT.Click += new System.EventHandler(this.btnReport_Click);
            // 
            // btnEDIT_VILLAGE
            // 
            resources.ApplyResources(this.btnEDIT_VILLAGE, "btnEDIT_VILLAGE");
            this.btnEDIT_VILLAGE.Name = "btnEDIT_VILLAGE";
            this.btnEDIT_VILLAGE.UseVisualStyleBackColor = true;
            this.btnEDIT_VILLAGE.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // txtQuickSearch
            // 
            this.txtQuickSearch.BackColor = System.Drawing.Color.White;
            this.txtQuickSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtQuickSearch, "txtQuickSearch");
            this.txtQuickSearch.Name = "txtQuickSearch";
            this.txtQuickSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtQuickSearch.QuickSearch += new System.EventHandler(this.txtQuickSearch_QuickSearch);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "PROVINCE_NAME";
            resources.ApplyResources(this.dataGridViewTextBoxColumn1, "dataGridViewTextBoxColumn1");
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "DISTRICT_NAME";
            resources.ApplyResources(this.dataGridViewTextBoxColumn2, "dataGridViewTextBoxColumn2");
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "COMMUNE_NAME";
            resources.ApplyResources(this.dataGridViewTextBoxColumn3, "dataGridViewTextBoxColumn3");
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "VILLAGE_NAME";
            resources.ApplyResources(this.dataGridViewTextBoxColumn4, "dataGridViewTextBoxColumn4");
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "TOTAL_FAMILIES";
            resources.ApplyResources(this.dataGridViewTextBoxColumn5, "dataGridViewTextBoxColumn5");
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // VILLAGE_CODE
            // 
            this.VILLAGE_CODE.DataPropertyName = "VILLAGE_CODE";
            resources.ApplyResources(this.VILLAGE_CODE, "VILLAGE_CODE");
            this.VILLAGE_CODE.Name = "VILLAGE_CODE";
            // 
            // PROVINCE
            // 
            this.PROVINCE.DataPropertyName = "PROVINCE_NAME";
            resources.ApplyResources(this.PROVINCE, "PROVINCE");
            this.PROVINCE.Name = "PROVINCE";
            this.PROVINCE.ReadOnly = true;
            // 
            // DISTRICT
            // 
            this.DISTRICT.DataPropertyName = "DISTRICT_NAME";
            resources.ApplyResources(this.DISTRICT, "DISTRICT");
            this.DISTRICT.Name = "DISTRICT";
            this.DISTRICT.ReadOnly = true;
            // 
            // COMMUNE
            // 
            this.COMMUNE.DataPropertyName = "COMMUNE_NAME";
            resources.ApplyResources(this.COMMUNE, "COMMUNE");
            this.COMMUNE.Name = "COMMUNE";
            this.COMMUNE.ReadOnly = true;
            // 
            // VILLAGE
            // 
            this.VILLAGE.DataPropertyName = "VILLAGE_NAME";
            resources.ApplyResources(this.VILLAGE, "VILLAGE");
            this.VILLAGE.Name = "VILLAGE";
            this.VILLAGE.ReadOnly = true;
            // 
            // TOTAL_FAMILY
            // 
            this.TOTAL_FAMILY.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.TOTAL_FAMILY.DataPropertyName = "TOTAL_FAMILIES";
            this.TOTAL_FAMILY.FillWeight = 150F;
            resources.ApplyResources(this.TOTAL_FAMILY, "TOTAL_FAMILY");
            this.TOTAL_FAMILY.Name = "TOTAL_FAMILY";
            // 
            // PageLicenseVillage
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.panel1);
            this.Name = "PageLicenseVillage";
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panel1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExTextbox txtQuickSearch;
        private ExButton btnEDIT_VILLAGE;
        private DataGridView dgv;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private ExButton btnREPORT;
        private FlowLayoutPanel flowLayoutPanel1;
        private DataGridViewTextBoxColumn VILLAGE_CODE;
        private DataGridViewTextBoxColumn PROVINCE;
        private DataGridViewTextBoxColumn DISTRICT;
        private DataGridViewTextBoxColumn COMMUNE;
        private DataGridViewTextBoxColumn VILLAGE;
        private DataGridViewTextBoxColumn TOTAL_FAMILY;
    }
}
