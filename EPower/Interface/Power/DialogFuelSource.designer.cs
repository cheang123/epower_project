﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogFuelSource
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogFuelSource));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.label1 = new System.Windows.Forms.Label();
            this.txtFuelSourceName = new System.Windows.Forms.TextBox();
            this.lblFUEL_SOURCE = new System.Windows.Forms.Label();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.lblNOTE = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.txtNote);
            this.content.Controls.Add(this.lblNOTE);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.txtFuelSourceName);
            this.content.Controls.Add(this.lblFUEL_SOURCE);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.lblFUEL_SOURCE, 0);
            this.content.Controls.SetChildIndex(this.txtFuelSourceName, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.lblNOTE, 0);
            this.content.Controls.SetChildIndex(this.txtNote, 0);
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // txtFuelSourceName
            // 
            resources.ApplyResources(this.txtFuelSourceName, "txtFuelSourceName");
            this.txtFuelSourceName.Name = "txtFuelSourceName";
            this.txtFuelSourceName.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // lblFUEL_SOURCE
            // 
            resources.ApplyResources(this.lblFUEL_SOURCE, "lblFUEL_SOURCE");
            this.lblFUEL_SOURCE.Name = "lblFUEL_SOURCE";
            // 
            // txtNote
            // 
            resources.ApplyResources(this.txtNote, "txtNote");
            this.txtNote.Name = "txtNote";
            this.txtNote.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // lblNOTE
            // 
            resources.ApplyResources(this.lblNOTE, "lblNOTE");
            this.lblNOTE.Name = "lblNOTE";
            // 
            // DialogFuelSource
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogFuelSource";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private ExButton btnCHANGE_LOG;
        private Label label1;
        private TextBox txtFuelSourceName;
        private Label lblFUEL_SOURCE;
        private TextBox txtNote;
        private Label lblNOTE;
    }
}