﻿using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;

namespace EPower.Interface
{
    public partial class DialogPowerUsage : ExDialog
    {
        GeneralProcess _flag;        
        TBL_POWER _objPower = new TBL_POWER();
        public TBL_POWER Power
        {
            get { return _objPower; }
        }
        TBL_POWER _oldObjPower = new TBL_POWER();

        #region Constructor
        public DialogPowerUsage(GeneralProcess flag, TBL_POWER objPower)
        {
            InitializeComponent();

            _flag = flag;
            objPower._CopyTo(_objPower);
            objPower._CopyTo(_oldObjPower); 
            if (flag == GeneralProcess.Insert)
            {
                _objPower.IS_ACTIVE = true;
                this.Text = string.Concat(Resources.INSERT, this.Text);
                _objPower.POWER_MONTH= getLastMonth();
            }
            else if (flag == GeneralProcess.Update)
            {
                this.Text = string.Concat(Resources.UPDATE, this.Text);
            }
            else if (flag == GeneralProcess.Delete)
            {
                this.Text = string.Concat(Resources.DELETE, this.Text);
                UIHelper.SetEnabled(this, false);
            }
            this.btnChangelog.Visible = this._flag != GeneralProcess.Insert;
            read();
        }

        private DateTime getLastMonth()
        {
            if (DBDataContext.Db.TBL_POWERs.Where(x=>x.IS_ACTIVE).Count()==0)
            {
                return DBDataContext.Db.GetSystemDate();
            }
            return DBDataContext.Db.TBL_POWERs.Where(x=>x.IS_ACTIVE).Max(x => x.POWER_MONTH).AddMonths(1);

        }        
        #endregion

        #region Operation
        /// <summary>
        /// Change Keyboard to Khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        } 

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        /// <summary>
        /// Data transaction.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();            
            try
            {
                using (TransactionScope tran=new TransactionScope( TransactionScopeOption.Required,TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objPower);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_oldObjPower, _objPower);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objPower);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }                                               
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }            
        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
        #endregion

        #region Method
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool inValid()
        {
            this.ClearAllValidation();
            bool blnVal = false;
            if (txtInputUsagePostpaid.Text.Trim()==string.Empty)
            {
                txtInputUsagePostpaid.SetValidation(string.Format(Resources.REQUIRED, lblInputPower.Text));
                blnVal = true;
            }
            return blnVal;
        }

        /// <summary>
        /// Read data from object.
        /// </summary>
        private void read()
        {
            dtp.Value = _objPower.POWER_MONTH;
            txtInputUsagePostpaid.Text =_objPower.POWER_INPUT!=0m ?  _objPower.POWER_INPUT.ToString(UIHelper._DefaultUsageFormat):"";
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            _objPower.POWER_MONTH = new DateTime(dtp.Value.Year, dtp.Value.Month, 1);
            _objPower.POWER_INPUT = DataHelper.ParseToDecimal(txtInputUsagePostpaid.Text);        
        }
        #endregion



        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objPower);
        }

        private void dtp_ValueChanged(object sender, EventArgs e)
        {
            loadData();
        }

        private void loadData()
        {
            if (_flag != GeneralProcess.Delete)
            {
                try
                {
                    TBL_POWER objPower = DBDataContext.Db.TBL_POWERs.FirstOrDefault(x => x.POWER_MONTH.Month == dtp.Value.Month
                                                                                        && x.POWER_MONTH.Year == dtp.Value.Year
                                                                                        && x.TRANSFORMER_ID == 0 // total power.
                                                                                        && x.IS_ACTIVE);

                    if (objPower != null)
                    {
                        _flag = GeneralProcess.Update;                        
                    }
                    else
                    {
                        _flag = GeneralProcess.Insert;
                        objPower = new TBL_POWER() { POWER_MONTH = new DateTime(dtp.Value.Year, dtp.Value.Month, 1) };
                    }
                    objPower._CopyTo(_objPower);
                    objPower._CopyTo(_oldObjPower);

                    read();
                    this.btnChangelog.Visible = this._flag != GeneralProcess.Insert;
                    this.Text = _flag.GetText( Resources.PRODUCTION);
                }
                catch (Exception ex)
                {
                    MsgBox.ShowError(ex);
                }
            }
        }

        private void txtInputUsage_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputNumberOnly(sender, e);
        }

      
    }
}