﻿using EPower.Base.Helper;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageFuelPurchase : Form
    {
        public TBL_FUEL_PURCHASE FuelPurchase
        {
            get
            {
                TBL_FUEL_PURCHASE objPurchaseFuel = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    objPurchaseFuel = DBDataContext.Db.TBL_FUEL_PURCHASEs.FirstOrDefault(x => x.FUEL_PURCHASE_ID == (int)dgv.SelectedRows[0].Cells[FUEL_PURCHASE_ID.Name].Value);
                }
                return objPurchaseFuel;
            }
        }
        bool isLoad = false;
        #region Constructor
        public PageFuelPurchase()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            DateTime date = DBDataContext.Db.GetSystemDate();
            dtpD1.Value = new DateTime(date.Year, date.Month, 1);
            dtpD2.Value = date;
            lookup();
            load();
            isLoad = true;
        }

        #endregion

        #region Operation
        /// <summary>
        /// Add new ampare.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNew_Click(object sender, EventArgs e)
        {
            TBL_FUEL_PURCHASE objFuelPurchase = new TBL_FUEL_PURCHASE()
            {
                PURCHASE_DATE = DBDataContext.Db.GetSystemDate(),
                PAYMENT_ACCOUNT_ID = AccountChartHelper.GetAccounts(AccountConfig.PAYMENT_DEFAULT_ACCOUNTS).FirstOrDefault().ACCOUNT_ID,
                CURRENCY_ID = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.IS_DEFAULT_CURRENCY).CURRENCY_ID
            };
            DialogFuelPurchase dig = new DialogFuelPurchase(GeneralProcess.Insert, objFuelPurchase);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                load();
                UIHelper.SelectRow(dgv, dig.FuelPurchase.FUEL_PURCHASE_ID);
            }
        }

        /// <summary>
        /// Edit ampare
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgv.Rows.Count > 0)
            {
                DialogFuelPurchase dig = new DialogFuelPurchase(GeneralProcess.Update, FuelPurchase);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    load();
                    UIHelper.SelectRow(dgv, dig.FuelPurchase.FUEL_PURCHASE_ID);
                }
            }
        }

        /// <summary>
        /// Remove ampare
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (dgv.Rows.Count > 0)
            {
                DialogFuelPurchase dig = new DialogFuelPurchase(GeneralProcess.Delete, FuelPurchase);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    load();
                    UIHelper.SelectRow(dgv, dig.FuelPurchase.FUEL_PURCHASE_ID);
                }
            }
        }

        private void dtpDate_ValueChanged(object sender, EventArgs e)
        {
            if (isLoad)
            {
                load();
            }
        }

        /// <summary>
        /// Change current keyboard layout to khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        #endregion        

        #region Method

        public void lookup()
        {
            try
            {
                isLoad = false;
                UIHelper.SetDataSourceToComboBox(this.cboFuelSource, DBDataContext.Db.TBL_FUEL_SOURCEs.Where(row => row.IS_ACTIVE), Resources.ALL_FUEL_SOURCE);
                UIHelper.SetDataSourceToComboBox(this.cboFuelType, Lookup.GetFuelType(), Resources.ALL_FUEL_TYPE);
                UIHelper.SetDataSourceToComboBox(this.cboCurrency, Lookup.GetCurrencies(), Resources.ALL_CURRENCY);
                isLoad = true;
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void load()
        {
            int sourceId = (int)cboFuelSource.SelectedValue;
            int fuelTypeId = (int)cboFuelType.SelectedValue;
            int currencyId = (int)cboCurrency.SelectedValue;

            dgv.DataSource = from fp in DBDataContext.Db.TBL_FUEL_PURCHASEs
                             join t in DBDataContext.Db.TLKP_FUEL_TYPEs on fp.FUEL_TYPE_ID equals t.FUEL_TYPE_ID
                             join ap in DBDataContext.Db.TBL_ACCOUNT_CHARTs on fp.PAYMENT_ACCOUNT_ID equals ap.ACCOUNT_ID
                             join c in DBDataContext.Db.TLKP_CURRENCies on fp.CURRENCY_ID equals c.CURRENCY_ID
                             join fs in DBDataContext.Db.TBL_FUEL_SOURCEs on fp.FUEL_SOURCE_ID equals fs.FUEL_SOURCE_ID into tj
                             from tr in tj.DefaultIfEmpty()
                             where fp.IS_ACTIVE
                             && (fp.PURCHASE_DATE.Date >= dtpD1.Value.Date && fp.PURCHASE_DATE.Date <= dtpD2.Value.Date)
                             && (sourceId == 0 || fp.FUEL_SOURCE_ID == sourceId)
                             && (fuelTypeId == 0 || fp.FUEL_TYPE_ID == fuelTypeId)
                             && (currencyId == 0 || fp.CURRENCY_ID == currencyId)
                             && (fp.REF_NO.ToLower().Contains(txtQuickSearch.Text.ToLower()))
                             select new
                             {
                                 fp.FUEL_PURCHASE_ID,
                                 fp.REF_NO,
                                 fp.PURCHASE_DATE,
                                 tr.FUEL_SOURCE_NAME,
                                 ap.ACCOUNT_NAME,
                                 fp.PRICE,
                                 fp.VOLUME,
                                 fp.TOTAL_AMOUNT,
                                 c.CURRENCY_SING
                             };
        }

        #endregion

        private void cboFuelSource_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (isLoad)
            {
                load();
            }
        }

    }
}
