﻿using System;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;

namespace EPower.Interface
{
    public partial class DialogFuelSource : ExDialog
    {        
        
        GeneralProcess _flag;

        TBL_FUEL_SOURCE _objNew = new TBL_FUEL_SOURCE();
        public TBL_FUEL_SOURCE FuelSource
        {
            get { return _objNew; }
        }
        TBL_FUEL_SOURCE _objOld = new TBL_FUEL_SOURCE();

        #region Constructor
        public DialogFuelSource(GeneralProcess flag, TBL_FUEL_SOURCE objDisType)
        {
            InitializeComponent();  
            _flag = flag;
            objDisType._CopyTo(_objNew);
            objDisType._CopyTo(_objOld);
            this.Text = flag.GetText(this.Text); 
            UIHelper.SetEnabled(this, flag != GeneralProcess.Delete); 
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;            
            read();
        }

        
        #endregion

        #region Operation
        /// <summary>
        /// Data transaction
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
            //if duplicate record.
            this.ClearValidation();
            if (DBDataContext.Db.IsExits(_objNew, "FUEL_SOURCE_NAME"))
            {
                txtFuelSourceName.SetValidation(string.Format(Resources.MS_IS_EXISTS, lblFUEL_SOURCE.Text));
                return;
            }

            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objNew);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objNew);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
            
        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
        
        /// <summary>
        /// Change current keyboard layout to khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        /// <summary>
        /// Change current keyboard layout to English.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEnglishKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        #endregion

        #region Method
        /// <summary>
        /// Load data from object.
        /// </summary>
        private void read()
        {
            txtFuelSourceName.Text = _objNew.FUEL_SOURCE_NAME;
            txtNote.Text = _objNew.NOTE; 
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            _objNew.FUEL_SOURCE_NAME = txtFuelSourceName.Text; 
            _objNew.NOTE = this.txtNote.Text; 
        }

        private bool inValid()
        {
            bool val = false;
            this.ClearValidation(); 
            if (txtFuelSourceName.Text.Trim()==string.Empty)
            {
                txtFuelSourceName.SetValidation(string.Format(Resources.REQUIRED, lblFUEL_SOURCE.Text));
                val = true;
            }             
            return val;
        } 
        #endregion   

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }

        
    }
}
