﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogDistributionVillage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogDistributionVillage));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.treeList = new DevExpress.XtraTreeList.TreeList();
            this.colName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.treeList);
            this.content.Controls.Add(this.panel2);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.panel2, 0);
            this.content.Controls.SetChildIndex(this.treeList, 0);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnCHANGE_LOG_Click);
            // 
            // treeList
            // 
            this.treeList.Appearance.BandPanel.BackColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.BandPanel.BorderColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.BandPanel.Options.UseBackColor = true;
            this.treeList.Appearance.BandPanel.Options.UseBorderColor = true;
            this.treeList.Appearance.Caption.BackColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.Caption.BorderColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.Caption.Options.UseBackColor = true;
            this.treeList.Appearance.Caption.Options.UseBorderColor = true;
            this.treeList.Appearance.CustomizationFormHint.BackColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.CustomizationFormHint.BorderColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.CustomizationFormHint.Options.UseBackColor = true;
            this.treeList.Appearance.CustomizationFormHint.Options.UseBorderColor = true;
            this.treeList.Appearance.Empty.BackColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.Empty.BorderColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.Empty.Options.UseBackColor = true;
            this.treeList.Appearance.Empty.Options.UseBorderColor = true;
            this.treeList.Appearance.EvenRow.BackColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.EvenRow.BorderColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.EvenRow.Options.UseBackColor = true;
            this.treeList.Appearance.EvenRow.Options.UseBorderColor = true;
            this.treeList.Appearance.FilterPanel.BackColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.FilterPanel.BorderColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.FilterPanel.Options.UseBackColor = true;
            this.treeList.Appearance.FilterPanel.Options.UseBorderColor = true;
            this.treeList.Appearance.FixedLine.BackColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.FixedLine.BorderColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.FixedLine.Options.UseBackColor = true;
            this.treeList.Appearance.FixedLine.Options.UseBorderColor = true;
            this.treeList.Appearance.FocusedCell.BackColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.FocusedCell.BorderColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.FocusedCell.Options.UseBackColor = true;
            this.treeList.Appearance.FocusedCell.Options.UseBorderColor = true;
            this.treeList.Appearance.FocusedRow.BackColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.FocusedRow.BorderColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.FocusedRow.Options.UseBackColor = true;
            this.treeList.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.treeList.Appearance.FooterPanel.BackColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.FooterPanel.BorderColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.FooterPanel.Options.UseBackColor = true;
            this.treeList.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.treeList.Appearance.GroupButton.BackColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.GroupButton.BorderColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.GroupButton.Options.UseBackColor = true;
            this.treeList.Appearance.GroupButton.Options.UseBorderColor = true;
            this.treeList.Appearance.GroupFooter.BackColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.GroupFooter.Options.UseBackColor = true;
            this.treeList.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.treeList.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.treeList.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.treeList.Appearance.HeaderPanelBackground.BackColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.HeaderPanelBackground.BorderColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.HeaderPanelBackground.Options.UseBackColor = true;
            this.treeList.Appearance.HeaderPanelBackground.Options.UseBorderColor = true;
            this.treeList.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.treeList.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.treeList.Appearance.HorzLine.BackColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.HorzLine.BorderColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.HorzLine.Options.UseBackColor = true;
            this.treeList.Appearance.HorzLine.Options.UseBorderColor = true;
            this.treeList.Appearance.HotTrackedRow.BackColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.HotTrackedRow.BorderColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.HotTrackedRow.Options.UseBackColor = true;
            this.treeList.Appearance.HotTrackedRow.Options.UseBorderColor = true;
            this.treeList.Appearance.OddRow.BackColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.OddRow.BorderColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.OddRow.Options.UseBackColor = true;
            this.treeList.Appearance.OddRow.Options.UseBorderColor = true;
            this.treeList.Appearance.Preview.BackColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.Preview.BorderColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.Preview.Options.UseBackColor = true;
            this.treeList.Appearance.Preview.Options.UseBorderColor = true;
            this.treeList.Appearance.Row.BackColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.Row.BorderColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.Row.Options.UseBackColor = true;
            this.treeList.Appearance.Row.Options.UseBorderColor = true;
            this.treeList.Appearance.Row.Options.UseTextOptions = true;
            this.treeList.Appearance.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.treeList.Appearance.SelectedRow.BackColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.SelectedRow.BorderColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.SelectedRow.Options.UseBackColor = true;
            this.treeList.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.treeList.Appearance.Separator.BackColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.Separator.BorderColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.Separator.Options.UseBackColor = true;
            this.treeList.Appearance.Separator.Options.UseBorderColor = true;
            this.treeList.Appearance.TopNewRow.BackColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.TopNewRow.BorderColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.TopNewRow.Options.UseBackColor = true;
            this.treeList.Appearance.TopNewRow.Options.UseBorderColor = true;
            this.treeList.Appearance.TreeLine.BackColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.TreeLine.BorderColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.TreeLine.Options.UseBackColor = true;
            this.treeList.Appearance.TreeLine.Options.UseBorderColor = true;
            this.treeList.Appearance.VertLine.BackColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.VertLine.BorderColor = System.Drawing.Color.Transparent;
            this.treeList.Appearance.VertLine.Options.UseBackColor = true;
            this.treeList.Appearance.VertLine.Options.UseBorderColor = true;
            this.treeList.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.treeList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colName});
            resources.ApplyResources(this.treeList, "treeList");
            this.treeList.KeyFieldName = "Code";
            this.treeList.Name = "treeList";
            this.treeList.OptionsBehavior.AllowRecursiveNodeChecking = true;
            this.treeList.OptionsBehavior.Editable = false;
            this.treeList.OptionsCustomization.AllowSort = false;
            this.treeList.OptionsMenu.EnableColumnMenu = false;
            this.treeList.OptionsMenu.EnableFooterMenu = false;
            this.treeList.OptionsView.CheckBoxStyle = DevExpress.XtraTreeList.DefaultNodeCheckBoxStyle.Check;
            this.treeList.OptionsView.RootCheckBoxStyle = DevExpress.XtraTreeList.NodeCheckBoxStyle.Check;
            this.treeList.OptionsView.ShowIndicator = false;
            this.treeList.ParentFieldName = "ParentCode";
            // 
            // colName
            // 
            this.colName.AppearanceCell.BorderColor = System.Drawing.Color.Transparent;
            this.colName.AppearanceCell.Options.UseBorderColor = true;
            resources.ApplyResources(this.colName, "colName");
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.OptionsColumn.ReadOnly = true;
            this.colName.OptionsFilter.AllowFilter = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnOK);
            this.panel2.Controls.Add(this.btnCLOSE);
            this.panel2.Controls.Add(this.btnCHANGE_LOG);
            this.panel2.Controls.Add(this.panel1);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // DialogDistributionVillage
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogDistributionVillage";
            this.Load += new System.EventHandler(this.DialogLicenseVillage_Load);
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private ExButton btnCHANGE_LOG;
        private Panel panel2;
        private DevExpress.XtraTreeList.TreeList treeList;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colName;
    }
}