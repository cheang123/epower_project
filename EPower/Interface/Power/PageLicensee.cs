﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageLicensee : Form
    {        
        
        public  TBL_LICENSEE Licensee
        {
            get
            {
                TBL_LICENSEE obj = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    try
                    {
                        int intid = (int)dgv.SelectedRows[0].Cells[LICENSEE_ID.Name].Value;
                        obj = DBDataContext.Db.TBL_LICENSEEs.FirstOrDefault(x => x.LICENSEE_ID == intid);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return obj;
            }
        }

        #region Constructor
        public PageLicensee()
        {
            InitializeComponent();
            this.btnReport.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_QUARTERLY_REPORT]);
            UIHelper.DataGridViewProperties(dgv);
            txt_QuickSearch(null, null);
        }
        #endregion

        #region Operation 
        private void btnNew_Click(object sender, EventArgs e)
        { 
            
            DialogLicensee dig = new DialogLicensee(GeneralProcess.Insert, new TBL_LICENSEE() 
            {
                LICENSEE_NO=string.Empty,
                LICENSEE_NAME=string.Empty,
                LOCATION=string.Empty 
            } );
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.Licensee.LICENSEE_ID);
            }
        }
         
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (Licensee==null)
            {
                return;
            }
            DialogLicensee dig = new DialogLicensee(GeneralProcess.Update, Licensee);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.Licensee.LICENSEE_ID);
            }
        }
         
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (Licensee==null)
            {
                return;
            }

            if (!IsDeletable())
            {
                MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE);
                return;
            }
            DialogLicensee dig = new DialogLicensee(GeneralProcess.Delete, Licensee);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.Licensee.LICENSEE_ID - 1);
            } 
        }

        private bool IsDeletable()
        {
            return DBDataContext.Db.TBL_LICENSEE_CONNECTIONs.Where(x => x.LICENSEE_ID == Licensee.LICENSEE_ID && x.IS_ACTIVE).Count() == 0;
        }
         
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            dgv.DataSource = from l in DBDataContext.Db.TBL_LICENSEEs
                             join v in DBDataContext.Db.TBL_VOLTAGEs on l.VOLTAGE_ID equals v.VOLTAGE_ID
                             join cu in DBDataContext.Db.TLKP_CURRENCies on l.CURRENCY_ID equals cu.CURRENCY_ID
                             where l.IS_ACTIVE
                             && string.Concat(l.LICENSEE_NO, " ", l.LICENSEE_NAME, " ", l.LOCATION).ToLower().Contains(txtQuickSearch.Text.ToLower())
                             select new
                             {
                                 l.LICENSEE_ID,
                                 l.LICENSEE_NO,
                                 l.LICENSEE_NAME,
                                 v.VOLTAGE_NAME, 
                                 l.TARIFF,
                                 cu.CURRENCY_SING,
                                 l.LOCATION,
                                 COUNT_CONNECTION=DBDataContext.Db.TBL_LICENSEE_CONNECTIONs.Where(x=>x.LICENSEE_ID==l.LICENSEE_ID&&x.IS_ACTIVE).Count()
                             };
                
        }

        /// <summary>
        /// Change current keyboard layout to khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        #endregion

        private void btnLicenseeConnection_Click(object sender, EventArgs e)
        {
            if (Licensee==null)
            {
                return; 
            }
            DialogLicenseeConnectionDetail dig = new DialogLicenseeConnectionDetail(Licensee);
            dig.ShowDialog();
            txt_QuickSearch(null, null);
            UIHelper.SelectRow(dgv, dig.Licensee.LICENSEE_ID);
        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            var diag = new DialogReportQuarterOptions(false);
            diag.chkTABLE_CUSTOMER_LICENSEE.Checked = true;
            if (diag.ShowDialog() == DialogResult.OK)
            {
                var doc = PageReportQuaterly.OpenReport(diag);
                doc.ViewReport("");
            }

        }

        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }

        
    }
}
