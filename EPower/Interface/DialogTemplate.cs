﻿using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Logic;
using System;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogTemplate : ExDialog
    {
        string filterOption = "";
        string gridName = "";
        public TLKP_FILTER_OPTION _objFilter = new TLKP_FILTER_OPTION();
        public TLKP_FILTER_OPTION filter
        {
            get { return _objFilter; }
        }

        #region Constructor
        public DialogTemplate(string option, string pageName)
        {
            InitializeComponent();
            this.filterOption = option;
            this.gridName = pageName;
        }
        #endregion

        #region Operation
        private void ChangeKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            this.write();
            if (inValid())
            {
                return;
            }
            DBDataContext.Db.TLKP_FILTER_OPTIONs.InsertOnSubmit(filter);
            DBDataContext.Db.SubmitChanges();
            this.DialogResult = DialogResult.OK;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
        #endregion

        #region Method
        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();

            if (txtTemplateName.Text.Trim().Length <= 0)
            {
                txtTemplateName.SetValidation(string.Format(Resources.REQUIRED, lblTEMPLATE_NAME.Text));
                val = true;
            }
            if (DBDataContext.Db.IsExits(_objFilter, nameof(TLKP_FILTER_OPTION.FILTER_NAME)))
            {
                txtTemplateName.SetValidation(string.Format(Resources.MS_IS_EXISTS, lblTEMPLATE_NAME.Text));
                val = true;
            }
            return val;
        }

        private void write()
        {
            _objFilter = new TLKP_FILTER_OPTION()
            {
                FILTER_NAME = txtTemplateName.Text,
                GRID_NAME = gridName,
                FILTER_OPTION = filterOption,
                CREATE_ON = DBDataContext.Db.GetSystemDate(),
                CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                IS_ACTIVE = true
            };
        }
        #endregion
    }
}