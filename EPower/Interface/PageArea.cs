﻿using System;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PageArea : Form
    { 
        public TBL_AREA Area
        {
            get
            {
                TBL_AREA objArea = null;
                if (dgvArea.SelectedRows.Count > 0)
                {
                    int areaID = (int)dgvArea.SelectedRows[0].Cells["AREA_ID"].Value;
                    try
                    {
                        objArea = DBDataContext.Db.TBL_AREAs.FirstOrDefault(x => x.AREA_ID == areaID);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objArea;
            }
        }

        #region Constructor
        public PageArea()
        {
            InitializeComponent();
            txt_QuickSearch(null, null);
            UIHelper.DataGridViewProperties(dgvArea);
        }
        #endregion

        #region Operation
        /// <summary>
        /// Load data from database.
        /// </summary>
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            try
            {
                dgvArea.DataSource = from area in DBDataContext.Db.TBL_AREAs
                          where area.IS_ACTIVE && (area.AREA_CODE.ToLower().Contains(txtQuickSearch.Text.ToLower().Trim())
                          || area.AREA_NAME.ToLower().Contains(txtQuickSearch.Text.ToLower().Trim()))
                          orderby area.AREA_CODE
                          select area;               
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
            
        }

        

        /// <summary>
        /// Add new area.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNew_Click(object sender, EventArgs e)
        {
            DialogArea dig = new DialogArea(GeneralProcess.Insert, new TBL_AREA() { AREA_CODE = "" });
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgvArea, dig.Area.AREA_ID);
            }
        }

        /// <summary>
        /// Edit area.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgvArea.SelectedRows.Count > 0)
            {
                DialogArea dig = new DialogArea(GeneralProcess.Update, Area);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgvArea, dig.Area.AREA_ID);
                }
            }
        }

        /// <summary>
        /// Remove area.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if ( IsDeletable())
            {
                DialogArea dig = new DialogArea(GeneralProcess.Delete, Area);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgvArea, dig.Area.AREA_ID - 1);
                }
            }
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        private void dgvArea_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }
        #endregion

        #region Method               
        private bool IsDeletable()
        {
            bool val = false;
            if (dgvArea.SelectedRows.Count>0)
            {
                val = true;
                if (DBDataContext.Db.TBL_POLEs.Where(x => x.AREA_ID == Area.AREA_ID && x.IS_ACTIVE).Count() > 0)
                {
                    MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE,Resources.INFORMATION);
                    val = false;
                }
            }            
            return val;
        }
        #endregion 

       
    }
}
