﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogCustomerCharge
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCustomerCharge));
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this._lblReadKhmerNumber = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblExchangeCurrency = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLabel();
            this.dtpDueDate = new DevExpress.XtraEditors.DateEdit();
            this.dtpInvoiceDate = new DevExpress.XtraEditors.DateEdit();
            this.lblEXCHANGE_RATE = new System.Windows.Forms.Label();
            this.txtExchangeRate = new System.Windows.Forms.TextBox();
            this.cboCurrency = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit();
            this.lblDUE_DATE = new System.Windows.Forms.Label();
            this.lblCUSTOMER = new System.Windows.Forms.Label();
            this.txtCustomer = new System.Windows.Forms.TextBox();
            this.cboPaymentMethod = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit();
            this.lblPAYMENT_METHOD = new System.Windows.Forms.Label();
            this.lblCURRENCY = new System.Windows.Forms.Label();
            this.lblINVOICE_DATE = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dgv = new DevExpress.XtraGrid.GridControl();
            this.dgvServiceList = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colService = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repInvoiceItem = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colPRICE_I = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repPrice = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.colTax = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repTax = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colTAX_AMOUNT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this._colRemove = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repRemove = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colCHARGE_DESCRIPTION = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtTaxAmount = new DevExpress.XtraEditors.TextEdit();
            this.txtTotalAmount = new DevExpress.XtraEditors.TextEdit();
            this.separatorControl1 = new DevExpress.XtraEditors.SeparatorControl();
            this.lblUntaxCurrency = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLabel();
            this.lblTaxCurrency = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLabel();
            this.lblTotalAmountCurrency = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLabel();
            this.txtUnTaxAmount = new DevExpress.XtraEditors.TextEdit();
            this.dLabel2 = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLabel();
            this.dLabel7 = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLabel();
            this.dLabel8 = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLabel();
            this.pTotal = new DevExpress.XtraEditors.PanelControl();
            this.chkIS_PAID = new System.Windows.Forms.CheckBox();
            this.chkPRINT = new System.Windows.Forms.CheckBox();
            this._lblNote = new System.Windows.Forms.Label();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDueDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDueDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpInvoiceDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpInvoiceDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCurrency.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboPaymentMethod.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvServiceList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repInvoiceItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repRemove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnTaxAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pTotal)).BeginInit();
            this.pTotal.SuspendLayout();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.txtNote);
            this.content.Controls.Add(this._lblNote);
            this.content.Controls.Add(this.chkIS_PAID);
            this.content.Controls.Add(this.chkPRINT);
            this.content.Controls.Add(this.panel3);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.pTotal);
            this.content.Controls.Add(this._lblReadKhmerNumber);
            this.content.Controls.Add(this.panel2);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.panel2, 0);
            this.content.Controls.SetChildIndex(this._lblReadKhmerNumber, 0);
            this.content.Controls.SetChildIndex(this.pTotal, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.panel3, 0);
            this.content.Controls.SetChildIndex(this.chkPRINT, 0);
            this.content.Controls.SetChildIndex(this.chkIS_PAID, 0);
            this.content.Controls.SetChildIndex(this._lblNote, 0);
            this.content.Controls.SetChildIndex(this.txtNote, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Controls.Add(this.btnCLOSE);
            this.panel2.Controls.Add(this.btnOK);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // groupBox2
            // 
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // _lblReadKhmerNumber
            // 
            this._lblReadKhmerNumber.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("_lblReadKhmerNumber.Appearance.Font")));
            this._lblReadKhmerNumber.Appearance.Options.UseFont = true;
            this._lblReadKhmerNumber.Appearance.Options.UseTextOptions = true;
            this._lblReadKhmerNumber.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            resources.ApplyResources(this._lblReadKhmerNumber, "_lblReadKhmerNumber");
            this._lblReadKhmerNumber.Name = "_lblReadKhmerNumber";
            this._lblReadKhmerNumber.Required = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblExchangeCurrency);
            this.panel1.Controls.Add(this.dtpDueDate);
            this.panel1.Controls.Add(this.dtpInvoiceDate);
            this.panel1.Controls.Add(this.lblEXCHANGE_RATE);
            this.panel1.Controls.Add(this.txtExchangeRate);
            this.panel1.Controls.Add(this.cboCurrency);
            this.panel1.Controls.Add(this.lblDUE_DATE);
            this.panel1.Controls.Add(this.lblCUSTOMER);
            this.panel1.Controls.Add(this.txtCustomer);
            this.panel1.Controls.Add(this.cboPaymentMethod);
            this.panel1.Controls.Add(this.lblPAYMENT_METHOD);
            this.panel1.Controls.Add(this.lblCURRENCY);
            this.panel1.Controls.Add(this.lblINVOICE_DATE);
            this.panel1.Controls.Add(this.label9);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // lblExchangeCurrency
            // 
            this.lblExchangeCurrency.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblExchangeCurrency.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblExchangeCurrency.Appearance.Font")));
            this.lblExchangeCurrency.Appearance.Options.UseBackColor = true;
            this.lblExchangeCurrency.Appearance.Options.UseFont = true;
            this.lblExchangeCurrency.Appearance.Options.UseTextOptions = true;
            this.lblExchangeCurrency.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            resources.ApplyResources(this.lblExchangeCurrency, "lblExchangeCurrency");
            this.lblExchangeCurrency.Name = "lblExchangeCurrency";
            this.lblExchangeCurrency.Required = false;
            // 
            // dtpDueDate
            // 
            this.dtpDueDate.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.dtpDueDate, "dtpDueDate");
            this.dtpDueDate.Name = "dtpDueDate";
            this.dtpDueDate.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("dtpDueDate.Properties.Appearance.Font")));
            this.dtpDueDate.Properties.Appearance.Options.UseFont = true;
            this.dtpDueDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("dtpDueDate.Properties.Buttons"))))});
            this.dtpDueDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("dtpDueDate.Properties.CalendarTimeProperties.Buttons"))))});
            this.dtpDueDate.Properties.Mask.EditMask = resources.GetString("dtpDueDate.Properties.Mask.EditMask");
            // 
            // dtpInvoiceDate
            // 
            this.dtpInvoiceDate.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.dtpInvoiceDate, "dtpInvoiceDate");
            this.dtpInvoiceDate.Name = "dtpInvoiceDate";
            this.dtpInvoiceDate.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("dtpInvoiceDate.Properties.Appearance.Font")));
            this.dtpInvoiceDate.Properties.Appearance.Options.UseFont = true;
            this.dtpInvoiceDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("dtpInvoiceDate.Properties.Buttons"))))});
            this.dtpInvoiceDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("dtpInvoiceDate.Properties.CalendarTimeProperties.Buttons"))))});
            this.dtpInvoiceDate.Properties.CalendarTimeProperties.DisplayFormat.FormatString = "dd-MM-yyyy";
            this.dtpInvoiceDate.Properties.CalendarTimeProperties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpInvoiceDate.Properties.CalendarTimeProperties.EditFormat.FormatString = "dd-MM-yyyy";
            this.dtpInvoiceDate.Properties.CalendarTimeProperties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpInvoiceDate.Properties.DisplayFormat.FormatString = "dd-MM-yyyy";
            this.dtpInvoiceDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpInvoiceDate.Properties.EditFormat.FormatString = "dd-MM-yyyy";
            this.dtpInvoiceDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpInvoiceDate.Properties.Mask.EditMask = resources.GetString("dtpInvoiceDate.Properties.Mask.EditMask");
            // 
            // lblEXCHANGE_RATE
            // 
            resources.ApplyResources(this.lblEXCHANGE_RATE, "lblEXCHANGE_RATE");
            this.lblEXCHANGE_RATE.Name = "lblEXCHANGE_RATE";
            // 
            // txtExchangeRate
            // 
            resources.ApplyResources(this.txtExchangeRate, "txtExchangeRate");
            this.txtExchangeRate.Name = "txtExchangeRate";
            // 
            // cboCurrency
            // 
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("cboCurrency.Properties.Appearance.Font")));
            this.cboCurrency.Properties.Appearance.Options.UseFont = true;
            this.cboCurrency.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cboCurrency.Properties.Buttons"))))});
            this.cboCurrency.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("cboCurrency.Properties.Columns"), resources.GetString("cboCurrency.Properties.Columns1")),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("cboCurrency.Properties.Columns2"), resources.GetString("cboCurrency.Properties.Columns3"))});
            this.cboCurrency.Properties.DisplayMember = "CURRENCY_NAME";
            this.cboCurrency.Properties.DropDownRows = 5;
            this.cboCurrency.Properties.NullText = resources.GetString("cboCurrency.Properties.NullText");
            this.cboCurrency.Properties.PopupWidth = 300;
            this.cboCurrency.Properties.PopupWidthMode = DevExpress.XtraEditors.PopupWidthMode.ContentWidth;
            this.cboCurrency.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.cboCurrency.Properties.ValueMember = "CURRENCY_ID";
            this.cboCurrency.Required = false;
            // 
            // lblDUE_DATE
            // 
            resources.ApplyResources(this.lblDUE_DATE, "lblDUE_DATE");
            this.lblDUE_DATE.Name = "lblDUE_DATE";
            // 
            // lblCUSTOMER
            // 
            resources.ApplyResources(this.lblCUSTOMER, "lblCUSTOMER");
            this.lblCUSTOMER.Name = "lblCUSTOMER";
            // 
            // txtCustomer
            // 
            resources.ApplyResources(this.txtCustomer, "txtCustomer");
            this.txtCustomer.Name = "txtCustomer";
            this.txtCustomer.ReadOnly = true;
            // 
            // cboPaymentMethod
            // 
            resources.ApplyResources(this.cboPaymentMethod, "cboPaymentMethod");
            this.cboPaymentMethod.Name = "cboPaymentMethod";
            this.cboPaymentMethod.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("cboPaymentMethod.Properties.Appearance.Font")));
            this.cboPaymentMethod.Properties.Appearance.Options.UseFont = true;
            this.cboPaymentMethod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cboPaymentMethod.Properties.Buttons"))))});
            this.cboPaymentMethod.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("cboPaymentMethod.Properties.Columns"), resources.GetString("cboPaymentMethod.Properties.Columns1"), ((int)(resources.GetObject("cboPaymentMethod.Properties.Columns2"))), ((DevExpress.Utils.FormatType)(resources.GetObject("cboPaymentMethod.Properties.Columns3"))), resources.GetString("cboPaymentMethod.Properties.Columns4"), ((bool)(resources.GetObject("cboPaymentMethod.Properties.Columns5"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("cboPaymentMethod.Properties.Columns6"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("cboPaymentMethod.Properties.Columns7"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("cboPaymentMethod.Properties.Columns8"))))});
            this.cboPaymentMethod.Properties.DisplayMember = "PAYMENT_METHOD_NAME";
            this.cboPaymentMethod.Properties.DropDownRows = 5;
            this.cboPaymentMethod.Properties.NullText = resources.GetString("cboPaymentMethod.Properties.NullText");
            this.cboPaymentMethod.Properties.PopupWidth = 300;
            this.cboPaymentMethod.Properties.PopupWidthMode = DevExpress.XtraEditors.PopupWidthMode.ContentWidth;
            this.cboPaymentMethod.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.cboPaymentMethod.Properties.ValueMember = "PAYMENT_METHOD_ID";
            this.cboPaymentMethod.Required = false;
            // 
            // lblPAYMENT_METHOD
            // 
            resources.ApplyResources(this.lblPAYMENT_METHOD, "lblPAYMENT_METHOD");
            this.lblPAYMENT_METHOD.Name = "lblPAYMENT_METHOD";
            // 
            // lblCURRENCY
            // 
            resources.ApplyResources(this.lblCURRENCY, "lblCURRENCY");
            this.lblCURRENCY.Name = "lblCURRENCY";
            // 
            // lblINVOICE_DATE
            // 
            resources.ApplyResources(this.lblINVOICE_DATE, "lblINVOICE_DATE");
            this.lblINVOICE_DATE.Name = "lblINVOICE_DATE";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dgv);
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            // 
            // dgv
            // 
            this.dgv.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.MainView = this.dgvServiceList;
            this.dgv.Name = "dgv";
            this.dgv.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repPrice,
            this.repInvoiceItem,
            this.repRemove,
            this.repTax});
            this.dgv.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.dgvServiceList});
            // 
            // dgvServiceList
            // 
            this.dgvServiceList.Appearance.ColumnFilterButton.Font = ((System.Drawing.Font)(resources.GetObject("dgvServiceList.Appearance.ColumnFilterButton.Font")));
            this.dgvServiceList.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.dgvServiceList.Appearance.ColumnFilterButtonActive.Font = ((System.Drawing.Font)(resources.GetObject("dgvServiceList.Appearance.ColumnFilterButtonActive.Font")));
            this.dgvServiceList.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.dgvServiceList.Appearance.CustomizationFormHint.Font = ((System.Drawing.Font)(resources.GetObject("dgvServiceList.Appearance.CustomizationFormHint.Font")));
            this.dgvServiceList.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.dgvServiceList.Appearance.DetailTip.Font = ((System.Drawing.Font)(resources.GetObject("dgvServiceList.Appearance.DetailTip.Font")));
            this.dgvServiceList.Appearance.DetailTip.Options.UseFont = true;
            this.dgvServiceList.Appearance.Empty.Font = ((System.Drawing.Font)(resources.GetObject("dgvServiceList.Appearance.Empty.Font")));
            this.dgvServiceList.Appearance.Empty.Options.UseFont = true;
            this.dgvServiceList.Appearance.EvenRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvServiceList.Appearance.EvenRow.Font")));
            this.dgvServiceList.Appearance.EvenRow.Options.UseFont = true;
            this.dgvServiceList.Appearance.FilterCloseButton.Font = ((System.Drawing.Font)(resources.GetObject("dgvServiceList.Appearance.FilterCloseButton.Font")));
            this.dgvServiceList.Appearance.FilterCloseButton.Options.UseFont = true;
            this.dgvServiceList.Appearance.FilterPanel.Font = ((System.Drawing.Font)(resources.GetObject("dgvServiceList.Appearance.FilterPanel.Font")));
            this.dgvServiceList.Appearance.FilterPanel.Options.UseFont = true;
            this.dgvServiceList.Appearance.FixedLine.Font = ((System.Drawing.Font)(resources.GetObject("dgvServiceList.Appearance.FixedLine.Font")));
            this.dgvServiceList.Appearance.FixedLine.Options.UseFont = true;
            this.dgvServiceList.Appearance.FocusedCell.Font = ((System.Drawing.Font)(resources.GetObject("dgvServiceList.Appearance.FocusedCell.Font")));
            this.dgvServiceList.Appearance.FocusedCell.Options.UseFont = true;
            this.dgvServiceList.Appearance.FocusedRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvServiceList.Appearance.FocusedRow.Font")));
            this.dgvServiceList.Appearance.FocusedRow.Options.UseFont = true;
            this.dgvServiceList.Appearance.FooterPanel.Font = ((System.Drawing.Font)(resources.GetObject("dgvServiceList.Appearance.FooterPanel.Font")));
            this.dgvServiceList.Appearance.FooterPanel.Options.UseFont = true;
            this.dgvServiceList.Appearance.GroupButton.Font = ((System.Drawing.Font)(resources.GetObject("dgvServiceList.Appearance.GroupButton.Font")));
            this.dgvServiceList.Appearance.GroupButton.Options.UseFont = true;
            this.dgvServiceList.Appearance.GroupFooter.Font = ((System.Drawing.Font)(resources.GetObject("dgvServiceList.Appearance.GroupFooter.Font")));
            this.dgvServiceList.Appearance.GroupFooter.Options.UseFont = true;
            this.dgvServiceList.Appearance.GroupPanel.Font = ((System.Drawing.Font)(resources.GetObject("dgvServiceList.Appearance.GroupPanel.Font")));
            this.dgvServiceList.Appearance.GroupPanel.Options.UseFont = true;
            this.dgvServiceList.Appearance.GroupRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvServiceList.Appearance.GroupRow.Font")));
            this.dgvServiceList.Appearance.GroupRow.Options.UseFont = true;
            this.dgvServiceList.Appearance.HeaderPanel.Font = ((System.Drawing.Font)(resources.GetObject("dgvServiceList.Appearance.HeaderPanel.Font")));
            this.dgvServiceList.Appearance.HeaderPanel.Options.UseFont = true;
            this.dgvServiceList.Appearance.HideSelectionRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvServiceList.Appearance.HideSelectionRow.Font")));
            this.dgvServiceList.Appearance.HideSelectionRow.Options.UseFont = true;
            this.dgvServiceList.Appearance.HorzLine.Font = ((System.Drawing.Font)(resources.GetObject("dgvServiceList.Appearance.HorzLine.Font")));
            this.dgvServiceList.Appearance.HorzLine.Options.UseFont = true;
            this.dgvServiceList.Appearance.HotTrackedRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvServiceList.Appearance.HotTrackedRow.Font")));
            this.dgvServiceList.Appearance.HotTrackedRow.Options.UseFont = true;
            this.dgvServiceList.Appearance.OddRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvServiceList.Appearance.OddRow.Font")));
            this.dgvServiceList.Appearance.OddRow.Options.UseFont = true;
            this.dgvServiceList.Appearance.Preview.Font = ((System.Drawing.Font)(resources.GetObject("dgvServiceList.Appearance.Preview.Font")));
            this.dgvServiceList.Appearance.Preview.Options.UseFont = true;
            this.dgvServiceList.Appearance.Row.Font = ((System.Drawing.Font)(resources.GetObject("dgvServiceList.Appearance.Row.Font")));
            this.dgvServiceList.Appearance.Row.Options.UseFont = true;
            this.dgvServiceList.Appearance.RowSeparator.Font = ((System.Drawing.Font)(resources.GetObject("dgvServiceList.Appearance.RowSeparator.Font")));
            this.dgvServiceList.Appearance.RowSeparator.Options.UseFont = true;
            this.dgvServiceList.Appearance.SelectedRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvServiceList.Appearance.SelectedRow.Font")));
            this.dgvServiceList.Appearance.SelectedRow.Options.UseFont = true;
            this.dgvServiceList.Appearance.TopNewRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvServiceList.Appearance.TopNewRow.Font")));
            this.dgvServiceList.Appearance.TopNewRow.Options.UseFont = true;
            this.dgvServiceList.Appearance.VertLine.Font = ((System.Drawing.Font)(resources.GetObject("dgvServiceList.Appearance.VertLine.Font")));
            this.dgvServiceList.Appearance.VertLine.Options.UseFont = true;
            this.dgvServiceList.Appearance.ViewCaption.Font = ((System.Drawing.Font)(resources.GetObject("dgvServiceList.Appearance.ViewCaption.Font")));
            this.dgvServiceList.Appearance.ViewCaption.Options.UseFont = true;
            this.dgvServiceList.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colService,
            this.colPRICE_I,
            this.colTax,
            this.colTAX_AMOUNT,
            this.colTotal,
            this._colRemove,
            this.colCHARGE_DESCRIPTION});
            this.dgvServiceList.GridControl = this.dgv;
            this.dgvServiceList.Name = "dgvServiceList";
            this.dgvServiceList.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.True;
            this.dgvServiceList.OptionsDetail.EnableMasterViewMode = false;
            this.dgvServiceList.OptionsView.AnimationType = DevExpress.XtraGrid.Views.Base.GridAnimationType.AnimateAllContent;
            this.dgvServiceList.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.dgvServiceList.OptionsView.ShowDetailButtons = false;
            this.dgvServiceList.OptionsView.ShowGroupPanel = false;
            this.dgvServiceList.OptionsView.ShowIndicator = false;
            this.dgvServiceList.OptionsView.WaitAnimationOptions = DevExpress.XtraEditors.WaitAnimationOptions.Panel;
            // 
            // colService
            // 
            resources.ApplyResources(this.colService, "colService");
            this.colService.ColumnEdit = this.repInvoiceItem;
            this.colService.FieldName = "INVOICE_ITEM_ID";
            this.colService.ImageOptions.Alignment = ((System.Drawing.StringAlignment)(resources.GetObject("colService.ImageOptions.Alignment")));
            this.colService.Name = "colService";
            // 
            // repInvoiceItem
            // 
            resources.ApplyResources(this.repInvoiceItem, "repInvoiceItem");
            this.repInvoiceItem.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("repInvoiceItem.Buttons"))))});
            this.repInvoiceItem.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("repInvoiceItem.Columns"), resources.GetString("repInvoiceItem.Columns1"), ((int)(resources.GetObject("repInvoiceItem.Columns2"))), ((DevExpress.Utils.FormatType)(resources.GetObject("repInvoiceItem.Columns3"))), resources.GetString("repInvoiceItem.Columns4"), ((bool)(resources.GetObject("repInvoiceItem.Columns5"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("repInvoiceItem.Columns6"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("repInvoiceItem.Columns7"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("repInvoiceItem.Columns8"))))});
            this.repInvoiceItem.DisplayMember = "INVOICE_ITEM_NAME";
            this.repInvoiceItem.Name = "repInvoiceItem";
            this.repInvoiceItem.PopupWidth = 540;
            this.repInvoiceItem.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.repInvoiceItem.ValueMember = "INVOICE_ITEM_ID";
            // 
            // colPRICE_I
            // 
            resources.ApplyResources(this.colPRICE_I, "colPRICE_I");
            this.colPRICE_I.ColumnEdit = this.repPrice;
            this.colPRICE_I.FieldName = "PRICE";
            this.colPRICE_I.Name = "colPRICE_I";
            // 
            // repPrice
            // 
            resources.ApplyResources(this.repPrice, "repPrice");
            this.repPrice.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("repPrice.Buttons"))))});
            this.repPrice.Name = "repPrice";
            // 
            // colTax
            // 
            this.colTax.AppearanceCell.Options.UseTextOptions = true;
            resources.ApplyResources(this.colTax, "colTax");
            this.colTax.ColumnEdit = this.repTax;
            this.colTax.FieldName = "TAX_ID";
            this.colTax.Name = "colTax";
            // 
            // repTax
            // 
            resources.ApplyResources(this.repTax, "repTax");
            this.repTax.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("repTax.Buttons"))))});
            this.repTax.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("repTax.Columns"), resources.GetString("repTax.Columns1"))});
            this.repTax.DisplayMember = "TAX_NAME";
            this.repTax.Name = "repTax";
            this.repTax.ValueMember = "TAX_ID";
            // 
            // colTAX_AMOUNT
            // 
            resources.ApplyResources(this.colTAX_AMOUNT, "colTAX_AMOUNT");
            this.colTAX_AMOUNT.FieldName = "TAX_AMOUNT";
            this.colTAX_AMOUNT.ImageOptions.Alignment = ((System.Drawing.StringAlignment)(resources.GetObject("colTAX_AMOUNT.ImageOptions.Alignment")));
            this.colTAX_AMOUNT.Name = "colTAX_AMOUNT";
            this.colTAX_AMOUNT.OptionsColumn.AllowEdit = false;
            // 
            // colTotal
            // 
            resources.ApplyResources(this.colTotal, "colTotal");
            this.colTotal.FieldName = "AMOUNT";
            this.colTotal.Name = "colTotal";
            this.colTotal.OptionsColumn.AllowEdit = false;
            // 
            // _colRemove
            // 
            this._colRemove.ColumnEdit = this.repRemove;
            this._colRemove.ImageOptions.Alignment = ((System.Drawing.StringAlignment)(resources.GetObject("_colRemove.ImageOptions.Alignment")));
            this._colRemove.MinWidth = 10;
            this._colRemove.Name = "_colRemove";
            this._colRemove.OptionsColumn.AllowEdit = false;
            this._colRemove.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            resources.ApplyResources(this._colRemove, "_colRemove");
            // 
            // repRemove
            // 
            resources.ApplyResources(this.repRemove, "repRemove");
            this.repRemove.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom;
            this.repRemove.ImageOptions.ImageChecked = ((System.Drawing.Image)(resources.GetObject("repRemove.ImageOptions.ImageChecked")));
            this.repRemove.ImageOptions.ImageGrayed = ((System.Drawing.Image)(resources.GetObject("repRemove.ImageOptions.ImageGrayed")));
            this.repRemove.ImageOptions.ImageUnchecked = ((System.Drawing.Image)(resources.GetObject("repRemove.ImageOptions.ImageUnchecked")));
            this.repRemove.Name = "repRemove";
            // 
            // colCHARGE_DESCRIPTION
            // 
            resources.ApplyResources(this.colCHARGE_DESCRIPTION, "colCHARGE_DESCRIPTION");
            this.colCHARGE_DESCRIPTION.FieldName = "CHARGE_DESCRIPTION";
            this.colCHARGE_DESCRIPTION.Name = "colCHARGE_DESCRIPTION";
            // 
            // txtTaxAmount
            // 
            resources.ApplyResources(this.txtTaxAmount, "txtTaxAmount");
            this.txtTaxAmount.Name = "txtTaxAmount";
            this.txtTaxAmount.Properties.Appearance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtTaxAmount.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("txtTaxAmount.Properties.Appearance.Font")));
            this.txtTaxAmount.Properties.Appearance.Options.UseBackColor = true;
            this.txtTaxAmount.Properties.Appearance.Options.UseFont = true;
            this.txtTaxAmount.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTaxAmount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTaxAmount.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.txtTaxAmount.Properties.ReadOnly = true;
            this.txtTaxAmount.TabStop = false;
            // 
            // txtTotalAmount
            // 
            resources.ApplyResources(this.txtTotalAmount, "txtTotalAmount");
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.Properties.Appearance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtTotalAmount.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("txtTotalAmount.Properties.Appearance.Font")));
            this.txtTotalAmount.Properties.Appearance.Options.UseBackColor = true;
            this.txtTotalAmount.Properties.Appearance.Options.UseFont = true;
            this.txtTotalAmount.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTotalAmount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTotalAmount.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.txtTotalAmount.Properties.ReadOnly = true;
            this.txtTotalAmount.TabStop = false;
            // 
            // separatorControl1
            // 
            this.separatorControl1.AutoSizeMode = true;
            this.separatorControl1.LineAlignment = DevExpress.XtraEditors.Alignment.Far;
            this.separatorControl1.LineColor = System.Drawing.Color.LightGray;
            this.separatorControl1.LineThickness = 1;
            resources.ApplyResources(this.separatorControl1, "separatorControl1");
            this.separatorControl1.Name = "separatorControl1";
            // 
            // lblUntaxCurrency
            // 
            resources.ApplyResources(this.lblUntaxCurrency, "lblUntaxCurrency");
            this.lblUntaxCurrency.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblUntaxCurrency.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblUntaxCurrency.Appearance.Font")));
            this.lblUntaxCurrency.Appearance.Options.UseBackColor = true;
            this.lblUntaxCurrency.Appearance.Options.UseFont = true;
            this.lblUntaxCurrency.Appearance.Options.UseTextOptions = true;
            this.lblUntaxCurrency.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblUntaxCurrency.Name = "lblUntaxCurrency";
            this.lblUntaxCurrency.Required = false;
            // 
            // lblTaxCurrency
            // 
            resources.ApplyResources(this.lblTaxCurrency, "lblTaxCurrency");
            this.lblTaxCurrency.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblTaxCurrency.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblTaxCurrency.Appearance.Font")));
            this.lblTaxCurrency.Appearance.Options.UseBackColor = true;
            this.lblTaxCurrency.Appearance.Options.UseFont = true;
            this.lblTaxCurrency.Appearance.Options.UseTextOptions = true;
            this.lblTaxCurrency.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblTaxCurrency.Name = "lblTaxCurrency";
            this.lblTaxCurrency.Required = false;
            // 
            // lblTotalAmountCurrency
            // 
            resources.ApplyResources(this.lblTotalAmountCurrency, "lblTotalAmountCurrency");
            this.lblTotalAmountCurrency.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblTotalAmountCurrency.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblTotalAmountCurrency.Appearance.Font")));
            this.lblTotalAmountCurrency.Appearance.Options.UseBackColor = true;
            this.lblTotalAmountCurrency.Appearance.Options.UseFont = true;
            this.lblTotalAmountCurrency.Appearance.Options.UseTextOptions = true;
            this.lblTotalAmountCurrency.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblTotalAmountCurrency.Name = "lblTotalAmountCurrency";
            this.lblTotalAmountCurrency.Required = false;
            // 
            // txtUnTaxAmount
            // 
            resources.ApplyResources(this.txtUnTaxAmount, "txtUnTaxAmount");
            this.txtUnTaxAmount.Name = "txtUnTaxAmount";
            this.txtUnTaxAmount.Properties.Appearance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtUnTaxAmount.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("txtUnTaxAmount.Properties.Appearance.Font")));
            this.txtUnTaxAmount.Properties.Appearance.Options.UseBackColor = true;
            this.txtUnTaxAmount.Properties.Appearance.Options.UseFont = true;
            this.txtUnTaxAmount.Properties.Appearance.Options.UseTextOptions = true;
            this.txtUnTaxAmount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtUnTaxAmount.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.txtUnTaxAmount.Properties.ReadOnly = true;
            this.txtUnTaxAmount.TabStop = false;
            // 
            // dLabel2
            // 
            this.dLabel2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.dLabel2.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("dLabel2.Appearance.Font")));
            this.dLabel2.Appearance.Options.UseBackColor = true;
            this.dLabel2.Appearance.Options.UseFont = true;
            resources.ApplyResources(this.dLabel2, "dLabel2");
            this.dLabel2.Name = "dLabel2";
            this.dLabel2.Required = false;
            // 
            // dLabel7
            // 
            this.dLabel7.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.dLabel7.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("dLabel7.Appearance.Font")));
            this.dLabel7.Appearance.Options.UseBackColor = true;
            this.dLabel7.Appearance.Options.UseFont = true;
            resources.ApplyResources(this.dLabel7, "dLabel7");
            this.dLabel7.Name = "dLabel7";
            this.dLabel7.Required = false;
            // 
            // dLabel8
            // 
            this.dLabel8.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.dLabel8.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("dLabel8.Appearance.Font")));
            this.dLabel8.Appearance.Options.UseBackColor = true;
            this.dLabel8.Appearance.Options.UseFont = true;
            resources.ApplyResources(this.dLabel8, "dLabel8");
            this.dLabel8.Name = "dLabel8";
            this.dLabel8.Required = false;
            // 
            // pTotal
            // 
            this.pTotal.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pTotal.Appearance.BackColor2 = ((System.Drawing.Color)(resources.GetObject("pTotal.Appearance.BackColor2")));
            this.pTotal.Appearance.BorderColor = System.Drawing.Color.White;
            this.pTotal.Appearance.Options.UseBackColor = true;
            this.pTotal.Appearance.Options.UseBorderColor = true;
            this.pTotal.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pTotal.Controls.Add(this.dLabel8);
            this.pTotal.Controls.Add(this.dLabel7);
            this.pTotal.Controls.Add(this.dLabel2);
            this.pTotal.Controls.Add(this.txtUnTaxAmount);
            this.pTotal.Controls.Add(this.lblTotalAmountCurrency);
            this.pTotal.Controls.Add(this.lblTaxCurrency);
            this.pTotal.Controls.Add(this.lblUntaxCurrency);
            this.pTotal.Controls.Add(this.separatorControl1);
            this.pTotal.Controls.Add(this.txtTotalAmount);
            this.pTotal.Controls.Add(this.txtTaxAmount);
            resources.ApplyResources(this.pTotal, "pTotal");
            this.pTotal.LookAndFeel.UseDefaultLookAndFeel = false;
            this.pTotal.Name = "pTotal";
            // 
            // chkIS_PAID
            // 
            resources.ApplyResources(this.chkIS_PAID, "chkIS_PAID");
            this.chkIS_PAID.Name = "chkIS_PAID";
            this.chkIS_PAID.UseVisualStyleBackColor = true;
            // 
            // chkPRINT
            // 
            resources.ApplyResources(this.chkPRINT, "chkPRINT");
            this.chkPRINT.Name = "chkPRINT";
            this.chkPRINT.UseVisualStyleBackColor = true;
            // 
            // _lblNote
            // 
            resources.ApplyResources(this._lblNote, "_lblNote");
            this._lblNote.Name = "_lblNote";
            // 
            // txtNote
            // 
            resources.ApplyResources(this.txtNote, "txtNote");
            this.txtNote.Name = "txtNote";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // DialogCustomerCharge
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.KeyPreview = true;
            this.Name = "DialogCustomerCharge";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDueDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDueDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpInvoiceDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpInvoiceDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCurrency.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboPaymentMethod.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvServiceList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repInvoiceItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repRemove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnTaxAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pTotal)).EndInit();
            this.pTotal.ResumeLayout(false);
            this.pTotal.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private Panel panel2;
        private GroupBox groupBox2;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLabel _lblReadKhmerNumber;
        private Panel panel3;
        private DevExpress.XtraGrid.GridControl dgv;
        private DevExpress.XtraGrid.Views.Grid.GridView dgvServiceList;
        private DevExpress.XtraGrid.Columns.GridColumn colService;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repInvoiceItem;
        private DevExpress.XtraGrid.Columns.GridColumn colPRICE_I;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colTax;
        private DevExpress.XtraGrid.Columns.GridColumn colTAX_AMOUNT;
        private DevExpress.XtraGrid.Columns.GridColumn colTotal;
        private DevExpress.XtraGrid.Columns.GridColumn _colRemove;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repRemove;
        private Panel panel1;
        private DevExpress.XtraEditors.DateEdit dtpDueDate;
        private DevExpress.XtraEditors.DateEdit dtpInvoiceDate;
        private Label lblEXCHANGE_RATE;
        private TextBox txtExchangeRate;
        public HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit cboCurrency;
        private Label lblDUE_DATE;
        private Label lblCUSTOMER;
        private TextBox txtCustomer;
        public HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit cboPaymentMethod;
        private Label lblPAYMENT_METHOD;
        private Label lblCURRENCY;
        private Label lblINVOICE_DATE;
        private Label label9;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repTax;
        private DevExpress.XtraEditors.PanelControl pTotal;
        private HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLabel dLabel8;
        private HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLabel dLabel7;
        private HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLabel dLabel2;
        private DevExpress.XtraEditors.TextEdit txtUnTaxAmount;
        private HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLabel lblTotalAmountCurrency;
        private HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLabel lblTaxCurrency;
        private HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLabel lblUntaxCurrency;
        private DevExpress.XtraEditors.SeparatorControl separatorControl1;
        private DevExpress.XtraEditors.TextEdit txtTotalAmount;
        private DevExpress.XtraEditors.TextEdit txtTaxAmount;
        private CheckBox chkIS_PAID;
        private CheckBox chkPRINT;
        private DevExpress.XtraGrid.Columns.GridColumn colCHARGE_DESCRIPTION;
        private HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLabel lblExchangeCurrency;
        private TextBox txtNote;
        private Label _lblNote;
        private Label label1;
    }
}