﻿
namespace EPower.Interface.Setting
{
    partial class DialogHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogHistory));
            this.panel1 = new System.Windows.Forms.Panel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.dpExport = new DevExpress.XtraEditors.DropDownButton();
            this.popupExport = new DevExpress.XtraBars.PopupMenu(this.components);
            this.btnExcel = new DevExpress.XtraBars.BarButtonItem();
            this.btnCsv = new DevExpress.XtraBars.BarButtonItem();
            this.btnPdf = new DevExpress.XtraBars.BarButtonItem();
            this.btnPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.rdpDate = new EPower.Base.Helper.Component.ReportDatePicker();
            this.tgv = new DevExpress.XtraTreeList.TreeList();
            this.colUSER_NAME = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colAUDIT_DATE = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colDEVICE = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colDISPLAY_NAME = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colOLD_VALUE = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colNEW_VALUE = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colViewOldHistory = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repTranNo = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.popActions = new DevExpress.XtraBars.PopupMenu(this.components);
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupExport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTranNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popActions)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.tgv);
            this.content.Controls.Add(this.panel1);
            this.content.Size = new System.Drawing.Size(1294, 705);
            this.content.Text = "CONTENT";
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.tgv, 0);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.Controls.Add(this.flowLayoutPanel3);
            this.panel1.Controls.Add(this.flowLayoutPanel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(1, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1292, 40);
            this.panel1.TabIndex = 76;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.flowLayoutPanel3.Controls.Add(this.dpExport);
            this.flowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(1143, 4);
            this.flowLayoutPanel3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(144, 32);
            this.flowLayoutPanel3.TabIndex = 152;
            // 
            // dpExport
            // 
            this.dpExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dpExport.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.dpExport.Appearance.BorderColor = System.Drawing.Color.Transparent;
            this.dpExport.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dpExport.Appearance.Options.UseBackColor = true;
            this.dpExport.Appearance.Options.UseBorderColor = true;
            this.dpExport.Appearance.Options.UseFont = true;
            this.dpExport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dpExport.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            this.dpExport.DropDownControl = this.popupExport;
            this.dpExport.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("dpExport.ImageOptions.Image")));
            this.dpExport.ImageOptions.ImageUri.Uri = "SendCSV;Size32x32;GrayScaled";
            this.dpExport.ImageOptions.SvgImageSize = new System.Drawing.Size(10, 10);
            this.dpExport.Location = new System.Drawing.Point(100, 1);
            this.dpExport.LookAndFeel.SkinMaskColor = System.Drawing.Color.Transparent;
            this.dpExport.LookAndFeel.SkinMaskColor2 = System.Drawing.Color.Transparent;
            this.dpExport.LookAndFeel.UseDefaultLookAndFeel = false;
            this.dpExport.Margin = new System.Windows.Forms.Padding(1);
            this.dpExport.Name = "dpExport";
            this.dpExport.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.dpExport.Size = new System.Drawing.Size(43, 26);
            this.dpExport.TabIndex = 19;
            // 
            // popupExport
            // 
            this.popupExport.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExcel),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnCsv),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnPdf),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnPrint, true)});
            this.popupExport.Manager = this.barManager1;
            this.popupExport.Name = "popupExport";
            // 
            // btnExcel
            // 
            this.btnExcel.Caption = "Excel";
            this.btnExcel.Id = 3;
            this.btnExcel.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnExcel.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnExcel.ItemAppearance.Normal.Options.UseFont = true;
            this.btnExcel.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnExcel.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.btnExcel.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.btnExcel.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnExcel.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.btnExcel.Name = "btnExcel";
            // 
            // btnCsv
            // 
            this.btnCsv.Caption = "Csv";
            this.btnCsv.Id = 5;
            this.btnCsv.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnCsv.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnCsv.ItemAppearance.Normal.Options.UseFont = true;
            this.btnCsv.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnCsv.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.btnCsv.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.btnCsv.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnCsv.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.btnCsv.Name = "btnCsv";
            // 
            // btnPdf
            // 
            this.btnPdf.Caption = "Pdf";
            this.btnPdf.Id = 2;
            this.btnPdf.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnPdf.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnPdf.ItemAppearance.Normal.Options.UseFont = true;
            this.btnPdf.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnPdf.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.btnPdf.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.btnPdf.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnPdf.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.btnPdf.Name = "btnPdf";
            // 
            // btnPrint
            // 
            this.btnPrint.Caption = "​បោះពុម្ព";
            this.btnPrint.Id = 4;
            this.btnPrint.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnPrint.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnPrint.ItemAppearance.Normal.Options.UseFont = true;
            this.btnPrint.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnPrint.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.btnPrint.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.btnPrint.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnPrint.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.btnPrint.Name = "btnPrint";
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControl2);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnExcel,
            this.btnCsv,
            this.btnPdf,
            this.btnPrint,
            this.barButtonItem1});
            this.barManager1.MaxItemId = 8;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1294, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 728);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1294, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 728);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl2.Location = new System.Drawing.Point(1294, 0);
            this.barDockControl2.Manager = this.barManager1;
            this.barDockControl2.Size = new System.Drawing.Size(0, 728);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Id = 6;
            this.barButtonItem1.ItemAppearance.Disabled.Options.UseFont = true;
            this.barButtonItem1.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem1.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem1.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem1.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.barButtonItem1.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem1.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.barButtonItem1.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.rdpDate);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Padding = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel2.Size = new System.Drawing.Size(401, 40);
            this.flowLayoutPanel2.TabIndex = 3;
            // 
            // rdpDate
            // 
            this.rdpDate.AutoSave = false;
            this.rdpDate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.rdpDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rdpDate.DateFilterTypes = EPower.Base.Helper.Component.ReportDatePicker.DateFilterType.Period;
            this.rdpDate.DefaultValue = EPower.Base.Helper.Component.ReportDatePicker.DateRanges.ThisMonth;
            this.rdpDate.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.rdpDate.FromDate = new System.DateTime(2022, 6, 1, 0, 0, 0, 0);
            this.rdpDate.LastCustomDays = 30;
            this.rdpDate.LastFinancialYear = true;
            this.rdpDate.LastMonth = true;
            this.rdpDate.LastQuarter = true;
            this.rdpDate.LastWeek = true;
            this.rdpDate.Location = new System.Drawing.Point(5, 4);
            this.rdpDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rdpDate.Name = "rdpDate";
            this.rdpDate.Size = new System.Drawing.Size(173, 27);
            this.rdpDate.TabIndex = 21;
            this.rdpDate.ThisFinancialYear = true;
            this.rdpDate.ThisMonth = true;
            this.rdpDate.ThisQuarter = true;
            this.rdpDate.ThisWeek = true;
            this.rdpDate.ToDate = new System.DateTime(2022, 6, 30, 23, 59, 59, 0);
            this.rdpDate.Today = true;
            this.rdpDate.Value = EPower.Base.Helper.Component.ReportDatePicker.DateRanges.ThisMonth;
            this.rdpDate.Yesterday = true;
            this.rdpDate.ValueChanged += new System.EventHandler(this.rdpDate_ValueChanged);
            // 
            // tgv
            // 
            this.tgv.Appearance.Caption.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.tgv.Appearance.Caption.Options.UseFont = true;
            this.tgv.Appearance.EvenRow.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.tgv.Appearance.EvenRow.Options.UseFont = true;
            this.tgv.Appearance.FilterPanel.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.tgv.Appearance.FilterPanel.Options.UseFont = true;
            this.tgv.Appearance.FocusedCell.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.tgv.Appearance.FocusedCell.Options.UseFont = true;
            this.tgv.Appearance.FocusedRow.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.tgv.Appearance.FocusedRow.Options.UseFont = true;
            this.tgv.Appearance.FooterPanel.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.tgv.Appearance.FooterPanel.Options.UseFont = true;
            this.tgv.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tgv.Appearance.GroupButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tgv.Appearance.GroupButton.Options.UseBackColor = true;
            this.tgv.Appearance.GroupFooter.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.tgv.Appearance.GroupFooter.Options.UseFont = true;
            this.tgv.Appearance.HeaderPanel.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.tgv.Appearance.HeaderPanel.Options.UseFont = true;
            this.tgv.Appearance.HeaderPanelBackground.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.tgv.Appearance.HeaderPanelBackground.Options.UseFont = true;
            this.tgv.Appearance.HideSelectionRow.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.tgv.Appearance.HideSelectionRow.Options.UseFont = true;
            this.tgv.Appearance.HotTrackedRow.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.tgv.Appearance.HotTrackedRow.Options.UseFont = true;
            this.tgv.Appearance.OddRow.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.tgv.Appearance.OddRow.Options.UseFont = true;
            this.tgv.Appearance.Preview.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.tgv.Appearance.Preview.Options.UseFont = true;
            this.tgv.Appearance.Row.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.tgv.Appearance.Row.Options.UseFont = true;
            this.tgv.Appearance.SelectedRow.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.tgv.Appearance.SelectedRow.Options.UseFont = true;
            this.tgv.Appearance.TopNewRow.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.tgv.Appearance.TopNewRow.Options.UseFont = true;
            this.tgv.AppearancePrint.BandPanel.Font = new System.Drawing.Font("Khmer Kep", 8.249999F);
            this.tgv.AppearancePrint.BandPanel.Options.UseFont = true;
            this.tgv.AppearancePrint.Caption.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.tgv.AppearancePrint.Caption.Options.UseFont = true;
            this.tgv.AppearancePrint.EvenRow.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.tgv.AppearancePrint.EvenRow.Options.UseFont = true;
            this.tgv.AppearancePrint.FooterPanel.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.tgv.AppearancePrint.FooterPanel.Options.UseFont = true;
            this.tgv.AppearancePrint.GroupFooter.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.tgv.AppearancePrint.GroupFooter.Options.UseFont = true;
            this.tgv.AppearancePrint.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tgv.AppearancePrint.HeaderPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tgv.AppearancePrint.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tgv.AppearancePrint.HeaderPanel.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.tgv.AppearancePrint.HeaderPanel.Options.UseBackColor = true;
            this.tgv.AppearancePrint.HeaderPanel.Options.UseBorderColor = true;
            this.tgv.AppearancePrint.HeaderPanel.Options.UseFont = true;
            this.tgv.AppearancePrint.Lines.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.tgv.AppearancePrint.Lines.Options.UseFont = true;
            this.tgv.AppearancePrint.OddRow.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.tgv.AppearancePrint.OddRow.Options.UseFont = true;
            this.tgv.AppearancePrint.Preview.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.tgv.AppearancePrint.Preview.Options.UseFont = true;
            this.tgv.AppearancePrint.Row.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.tgv.AppearancePrint.Row.Options.UseFont = true;
            this.tgv.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colUSER_NAME,
            this.colAUDIT_DATE,
            this.colDEVICE,
            this.colDISPLAY_NAME,
            this.colOLD_VALUE,
            this.colNEW_VALUE,
            this.colViewOldHistory});
            this.tgv.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tgv.CustomizationFormBounds = new System.Drawing.Rectangle(1100, 92, 266, 373);
            this.tgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tgv.HorzScrollStep = 7;
            this.tgv.KeyFieldName = "RenderId";
            this.tgv.Location = new System.Drawing.Point(1, 40);
            this.tgv.Margin = new System.Windows.Forms.Padding(8);
            this.tgv.MinWidth = 49;
            this.tgv.Name = "tgv";
            this.tgv.OptionsBehavior.AutoPopulateColumns = false;
            this.tgv.OptionsBehavior.Editable = false;
            this.tgv.OptionsBehavior.PopulateServiceColumns = true;
            this.tgv.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.EntireBranch;
            this.tgv.OptionsPrint.PrintReportFooter = false;
            this.tgv.OptionsView.ShowIndicator = false;
            this.tgv.OptionsView.ShowTreeLines = DevExpress.Utils.DefaultBoolean.False;
            this.tgv.ParentFieldName = "ParentRenderId";
            this.tgv.PreviewFieldName = "Vendor";
            this.tgv.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repTranNo});
            this.tgv.Size = new System.Drawing.Size(1292, 664);
            this.tgv.TabIndex = 77;
            this.tgv.TreeLevelWidth = 42;
            // 
            // colUSER_NAME
            // 
            this.colUSER_NAME.Caption = "អ្នកកែប្រែ";
            this.colUSER_NAME.FieldName = "UserName";
            this.colUSER_NAME.MinWidth = 49;
            this.colUSER_NAME.Name = "colUSER_NAME";
            this.colUSER_NAME.Visible = true;
            this.colUSER_NAME.VisibleIndex = 0;
            this.colUSER_NAME.Width = 126;
            // 
            // colAUDIT_DATE
            // 
            this.colAUDIT_DATE.Caption = "កាលបរិច្ឆេទ";
            this.colAUDIT_DATE.FieldName = "AuditDate";
            this.colAUDIT_DATE.Format.FormatString = "dd-MM-yyyy hh:mm tt";
            this.colAUDIT_DATE.Format.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colAUDIT_DATE.MinWidth = 49;
            this.colAUDIT_DATE.Name = "colAUDIT_DATE";
            this.colAUDIT_DATE.SortOrder = System.Windows.Forms.SortOrder.Descending;
            this.colAUDIT_DATE.Visible = true;
            this.colAUDIT_DATE.VisibleIndex = 1;
            this.colAUDIT_DATE.Width = 132;
            // 
            // colDEVICE
            // 
            this.colDEVICE.Caption = "ឈ្មោះឧបករណ៍";
            this.colDEVICE.FieldName = "Device";
            this.colDEVICE.MinWidth = 49;
            this.colDEVICE.Name = "colDEVICE";
            this.colDEVICE.Visible = true;
            this.colDEVICE.VisibleIndex = 2;
            this.colDEVICE.Width = 182;
            // 
            // colDISPLAY_NAME
            // 
            this.colDISPLAY_NAME.Caption = "ប្រតិបត្តិការ";
            this.colDISPLAY_NAME.FieldName = "DisplayName";
            this.colDISPLAY_NAME.MinWidth = 49;
            this.colDISPLAY_NAME.Name = "colDISPLAY_NAME";
            this.colDISPLAY_NAME.Visible = true;
            this.colDISPLAY_NAME.VisibleIndex = 3;
            this.colDISPLAY_NAME.Width = 280;
            // 
            // colOLD_VALUE
            // 
            this.colOLD_VALUE.Caption = "តម្លៃចាស់";
            this.colOLD_VALUE.FieldName = "OldValue";
            this.colOLD_VALUE.MinWidth = 49;
            this.colOLD_VALUE.Name = "colOLD_VALUE";
            this.colOLD_VALUE.Visible = true;
            this.colOLD_VALUE.VisibleIndex = 4;
            this.colOLD_VALUE.Width = 124;
            // 
            // colNEW_VALUE
            // 
            this.colNEW_VALUE.Caption = "តម្លៃថ្មី";
            this.colNEW_VALUE.FieldName = "NewValue";
            this.colNEW_VALUE.MinWidth = 49;
            this.colNEW_VALUE.Name = "colNEW_VALUE";
            this.colNEW_VALUE.Visible = true;
            this.colNEW_VALUE.VisibleIndex = 5;
            this.colNEW_VALUE.Width = 121;
            // 
            // colViewOldHistory
            // 
            this.colViewOldHistory.Caption = "treeListColumn1";
            this.colViewOldHistory.FieldName = "IsViewOldHistory";
            this.colViewOldHistory.MinWidth = 25;
            this.colViewOldHistory.Name = "colViewOldHistory";
            this.colViewOldHistory.OptionsColumn.AllowEdit = false;
            this.colViewOldHistory.OptionsColumn.AllowMove = false;
            this.colViewOldHistory.OptionsColumn.AllowSize = false;
            this.colViewOldHistory.OptionsColumn.AllowSort = false;
            this.colViewOldHistory.OptionsColumn.ShowInCustomizationForm = false;
            this.colViewOldHistory.OptionsColumn.ShowInExpressionEditor = false;
            this.colViewOldHistory.Width = 94;
            // 
            // repTranNo
            // 
            this.repTranNo.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.repTranNo.Appearance.Options.UseForeColor = true;
            this.repTranNo.AppearanceDisabled.ForeColor = System.Drawing.Color.Blue;
            this.repTranNo.AppearanceDisabled.Options.UseForeColor = true;
            this.repTranNo.AppearanceFocused.BackColor = System.Drawing.Color.Blue;
            this.repTranNo.AppearanceFocused.FontStyleDelta = System.Drawing.FontStyle.Underline;
            this.repTranNo.AppearanceFocused.Options.UseBackColor = true;
            this.repTranNo.AppearanceFocused.Options.UseFont = true;
            this.repTranNo.AutoHeight = false;
            this.repTranNo.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repTranNo.LinkColor = System.Drawing.Color.Blue;
            this.repTranNo.Name = "repTranNo";
            // 
            // popActions
            // 
            this.popActions.Manager = this.barManager1;
            this.popActions.Name = "popActions";
            // 
            // DialogHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1294, 728);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "DialogHistory";
            this.Text = "ប្រវត្តិកែប្រែ";
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.flowLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupExport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTranNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popActions)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private DevExpress.XtraTreeList.TreeList tgv;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colUSER_NAME;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colAUDIT_DATE;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colDEVICE;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colDISPLAY_NAME;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colOLD_VALUE;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colNEW_VALUE;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colViewOldHistory;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repTranNo;
        private Base.Helper.Component.ReportDatePicker rdpDate;
        private DevExpress.XtraBars.PopupMenu popupExport;
        private DevExpress.XtraBars.BarButtonItem btnExcel;
        private DevExpress.XtraBars.BarButtonItem btnCsv;
        private DevExpress.XtraBars.BarButtonItem btnPdf;
        private DevExpress.XtraBars.BarButtonItem btnPrint;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.PopupMenu popActions;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private DevExpress.XtraEditors.DropDownButton dpExport;
    }
}