﻿using System;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PageManageStock : Form
    {
        public PageManageStock()
        {
            InitializeComponent();
            bind(); 
        }

        private void bind()
        { 
            try
            {
              
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }     
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (this.dgv.SelectedRows.Count > 0)
            {
                
                int id = (int) this.dgv.SelectedRows[0].Cells[0].Value;
                TBL_STOCK_ITEM obj = DBDataContext.Db.TBL_STOCK_ITEMs.Single(row => row.ITEM_ID== id);
                
                DialogStockItem diag = new DialogStockItem( GeneralProcess.Update,obj);
                if (diag.ShowDialog() == DialogResult.OK)
                {
                    this.bind();
                    UIHelper.SelectRow(this.dgv,diag.StockItem.ITEM_ID);
                }
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            DialogStockItem diag = new DialogStockItem(GeneralProcess.Insert, new TBL_STOCK_ITEM());

            if (diag.ShowDialog() == DialogResult.OK)
            {
                bind();
                UIHelper.SelectRow(this.dgv,diag.StockItem.ITEM_ID);
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (this.dgv.SelectedRows.Count > 0)
            {
                int id = (int)this.dgv.SelectedRows[0].Cells[0].Value;
                if (isDeletable(id))
                {
                    TBL_STOCK_ITEM obj = DBDataContext.Db.TBL_STOCK_ITEMs.Single(row => row.ITEM_ID== id);
                    DialogStockItem diag = new DialogStockItem( GeneralProcess.Delete,obj);
                    if (diag.ShowDialog() == DialogResult.OK)
                    {
                        this.bind();
                        UIHelper.SelectRow(this.dgv, diag.StockItem.ITEM_ID- 1);
                    }
                }
                else
                {
                    MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE); 
                }
            }
        }

        private void txtSearch_QuickSearch(object sender, EventArgs e)
        {
            bind();
        }

        private bool isDeletable(int id)
        {
            return true;
        }

        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }
    }
}
