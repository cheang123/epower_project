﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface.OPSReport
{
    public partial class DialogOPSSelectAffectedAreas : ExDialog
    {
        #region private Data
        private GeneralProcess _flag;
        private List<TBL_LICENSE_VILLAGE> mSelectedAreas = new List<TBL_LICENSE_VILLAGE>();
        public List<TBL_LICENSE_VILLAGE> SelectedAreas
        {
            get
            {
                return mSelectedAreas;
            }
            private set
            {
                mSelectedAreas = value;
            }
        }
        public int TotalCustomers
        {
            get;
            private set;
        }
        #endregion

        #region Constructor
        public DialogOPSSelectAffectedAreas(List<TBL_LICENSE_VILLAGE> village, GeneralProcess flag)
        {
            InitializeComponent();
            _flag = flag;
            loadProvince();
            mSelectedAreas = village;
        }


        #endregion Constructor

        #region Method
        private void loadProvince()
        {
            this.trv.Nodes.Clear();
            TreeNode nodeRoot = new TreeNode();

            nodeRoot.Text = "គ្រប់ខេត្ត";
            this.trv.Nodes.Add(nodeRoot);

            foreach (TLKP_PROVINCE pro in from lv in DBDataContext.Db.TBL_LICENSE_VILLAGEs
                                          join v in DBDataContext.Db.TLKP_VILLAGEs on lv.VILLAGE_CODE equals v.VILLAGE_CODE
                                          join c in DBDataContext.Db.TLKP_COMMUNEs on v.COMMUNE_CODE equals c.COMMUNE_CODE
                                          join d in DBDataContext.Db.TLKP_DISTRICTs on c.DISTRICT_CODE equals d.DISTRICT_CODE
                                          join p in DBDataContext.Db.TLKP_PROVINCEs on d.PROVINCE_CODE equals p.PROVINCE_CODE
                                          select p)
            {
                var nodeProvince = new TreeNode(pro.PROVINCE_NAME);
                nodeProvince.Name = pro.PROVINCE_CODE;
                nodeProvince.Tag = pro;
                if (nodeRoot.Nodes.ContainsKey(nodeProvince.Name))
                {
                    continue;
                }
                nodeRoot.Nodes.Add(nodeProvince);
            }
            trv.Nodes[0].Expand();
        }
        private void loadDistrict(TreeNode node)
        {
            if (node.Nodes.Count > 0)
            {
                return;
            }
            TLKP_PROVINCE objPro = (TLKP_PROVINCE)node.Tag;
            foreach (TLKP_DISTRICT d in from lv in DBDataContext.Db.TBL_LICENSE_VILLAGEs
                                        join v in DBDataContext.Db.TLKP_VILLAGEs on lv.VILLAGE_CODE equals v.VILLAGE_CODE
                                        join c in DBDataContext.Db.TLKP_COMMUNEs on v.COMMUNE_CODE equals c.COMMUNE_CODE
                                        join d in DBDataContext.Db.TLKP_DISTRICTs on c.DISTRICT_CODE equals d.DISTRICT_CODE
                                        join p in DBDataContext.Db.TLKP_PROVINCEs on d.PROVINCE_CODE equals p.PROVINCE_CODE
                                        where p.PROVINCE_CODE == objPro.PROVINCE_CODE
                                        select d)
            {
                var nodeDisctrict = new TreeNode(d.DISTRICT_NAME);
                nodeDisctrict.Name = d.DISTRICT_CODE;
                nodeDisctrict.Tag = d;
                if (node.Nodes.ContainsKey(nodeDisctrict.Name))
                {
                    continue;
                }
                node.Nodes.Add(nodeDisctrict);
            }
        }
        private void loadCommune(TreeNode node)
        {
            if (node.Nodes.Count > 0)
            {
                return;
            }
            TLKP_DISTRICT objDis = (TLKP_DISTRICT)node.Tag;
            foreach (TLKP_COMMUNE c in from lv in DBDataContext.Db.TBL_LICENSE_VILLAGEs
                                       join v in DBDataContext.Db.TLKP_VILLAGEs on lv.VILLAGE_CODE equals v.VILLAGE_CODE
                                       join c in DBDataContext.Db.TLKP_COMMUNEs on v.COMMUNE_CODE equals c.COMMUNE_CODE
                                       join d in DBDataContext.Db.TLKP_DISTRICTs on c.DISTRICT_CODE equals d.DISTRICT_CODE
                                       where d.DISTRICT_CODE == objDis.DISTRICT_CODE
                                       select c)
            {
                var nodeCommune = new TreeNode(c.COMMUNE_NAME);
                nodeCommune.Name = c.COMMUNE_CODE;
                nodeCommune.Tag = c;
                if (node.Nodes.ContainsKey(nodeCommune.Name))
                {
                    continue;
                }
                node.Nodes.Add(nodeCommune);
            }
        }
        private void loadVillage(TreeNode node)
        {
            if (node.Nodes.Count > 0)
            {
                return;
            }
            TLKP_COMMUNE objCom = (TLKP_COMMUNE)node.Tag;
            foreach (TLKP_VILLAGE v in from lv in DBDataContext.Db.TBL_LICENSE_VILLAGEs
                                       join v in DBDataContext.Db.TLKP_VILLAGEs on lv.VILLAGE_CODE equals v.VILLAGE_CODE
                                       join c in DBDataContext.Db.TLKP_COMMUNEs on v.COMMUNE_CODE equals c.COMMUNE_CODE
                                       where c.COMMUNE_CODE == objCom.COMMUNE_CODE
                                       select v)
            {

                var nodeVillage = new TreeNode();
                int n = DBDataContext.Db.TBL_CUSTOMERs.Count(x => x.VILLAGE_CODE == v.VILLAGE_CODE && (x.STATUS_ID == (int)CustomerStatus.Active || x.STATUS_ID == (int)CustomerStatus.Blocked));
                nodeVillage.Text = n == 0 ? v.VILLAGE_NAME : string.Format(v.VILLAGE_NAME + " (" + n + "គ្រួសារ)");
                nodeVillage.Name = v.VILLAGE_CODE;
                nodeVillage.Tag = v;
                nodeVillage.ToolTipText = DBDataContext.Db.TBL_CUSTOMERs.Count(x => x.VILLAGE_CODE == v.VILLAGE_CODE && (x.STATUS_ID == (int)CustomerStatus.Active || x.STATUS_ID == (int)CustomerStatus.Blocked)).ToString();

                if (node.Nodes.ContainsKey(nodeVillage.Name))
                {
                    continue;
                }
                node.Nodes.Add(nodeVillage);
            }
        }
        private void read()
        {
            var vil = from lv in DBDataContext.Db.TBL_LICENSE_VILLAGEs
                      join v in DBDataContext.Db.TLKP_VILLAGEs on lv.VILLAGE_CODE equals v.VILLAGE_CODE
                      join c in DBDataContext.Db.TLKP_COMMUNEs on v.COMMUNE_CODE equals c.COMMUNE_CODE
                      join d in DBDataContext.Db.TLKP_DISTRICTs on c.DISTRICT_CODE equals d.DISTRICT_CODE
                      join p in DBDataContext.Db.TLKP_PROVINCEs on d.PROVINCE_CODE equals p.PROVINCE_CODE

                      select new
                      {
                          TLKP_VILLAGE = v,
                          TLKP_COMMUNE = c,
                          TLKP_DISTRICT = d,
                          TLKP_PROVINCE = p
                      };

            foreach (var pro in vil.Select(x => x.TLKP_PROVINCE).Distinct())
            {
                Application.DoEvents();
                TreeNode nodePro = trv.Nodes[0].Nodes.Find(pro.PROVINCE_CODE, false)[0];
                if (nodePro == null)
                {
                    continue;
                }
                trv_AfterSelect(trv, new TreeViewEventArgs(nodePro));

                foreach (var dis in vil.Select(x => x.TLKP_DISTRICT).Distinct().Where(x => x.PROVINCE_CODE == pro.PROVINCE_CODE))
                {
                    Application.DoEvents();
                    TreeNode nodeDis = nodePro.Nodes.Find(dis.DISTRICT_CODE, false)[0];
                    if (nodeDis == null)
                    {
                        continue;
                    }
                    trv_AfterSelect(trv, new TreeViewEventArgs(nodeDis));

                    foreach (var com in vil.Select(x => x.TLKP_COMMUNE).Distinct().Where(x => x.DISTRICT_CODE == dis.DISTRICT_CODE))
                    {
                        Application.DoEvents();
                        TreeNode nodeCom = nodeDis.Nodes.Find(com.COMMUNE_CODE, false)[0];
                        if (nodeCom == null)
                        {
                            continue;
                        }
                        trv_AfterSelect(trv, new TreeViewEventArgs(nodeCom));

                        foreach (var v in vil.Select(x => x.TLKP_VILLAGE).Distinct().Where(x => x.COMMUNE_CODE == com.COMMUNE_CODE))
                        {
                            Application.DoEvents();
                            TreeNode nodeVil = nodeCom.Nodes.Find(v.VILLAGE_CODE, false)[0];
                            if (nodeVil == null)
                            {
                                continue;
                            }
                            foreach (var item in mSelectedAreas)
                            {
                                if (nodeVil.Name == item.VILLAGE_CODE)
                                {
                                    nodeVil.Checked = true;
                                    string path = nodeVil.FullPath.ToString();
                                    var path_list = path.Split('\\').ToList();
                                    foreach (TreeNode node in trv.Nodes)
                                        if (node.Text == path_list[0])
                                            ExpandNodes(node, path_list);
                                }
                                else
                                    continue;
                            }
                        }
                    }
                }
            }
        }
        private void ExpandNodes(TreeNode node, List<string> path)
        {
            path.RemoveAt(0);
            node.Expand();

            if (path.Count == 0)
                return;

            foreach (TreeNode mynode in node.Nodes)
                if (mynode.Text == path[0])
                {
                    ExpandNodes(mynode, path); //recursive call
                    break;  //this was missing in earlier answer
                }

        }
        private void lookupCheckedNotIncludeRoot(TreeNodeCollection nodes, List<TreeNode> list)
        {
            foreach (TreeNode node in nodes)
            {
                if (node.Checked)
                {
                    foreach (var lv in DBDataContext.Db.TBL_LICENSE_VILLAGEs)
                    {
                        if (lv.VILLAGE_CODE.ToString() == node.Name.ToString())
                        {
                            list.Add(node);
                        }
                    }
                }
                lookupCheckedNotIncludeRoot(node.Nodes, list);
            }
        }
        #endregion Method 


        private void DialogOPSSelectAffectedAreas_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (mSelectedAreas.Count() > 0)
            {
                TotalCustomers = DataHelper.ParseToInt(lblCount.Text);
            }
        }

        private void DialogOPSSelectAffectedAreas_Load(object sender, EventArgs e)
        {
            if (mSelectedAreas.Count() > 0)
            {
                Runner.Run(read, Resources.SHOW_INFORMATION);
            }
            trv_Click(null, null);
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            var listNodes = new List<TreeNode>();
            lookupCheckedNotIncludeRoot(trv.Nodes, listNodes);
            mSelectedAreas = new List<TBL_LICENSE_VILLAGE>();
            foreach (var item in listNodes)
            {
                mSelectedAreas.Add(DBDataContext.Db.TBL_LICENSE_VILLAGEs.FirstOrDefault(x => x.VILLAGE_CODE == item.Name));
            }

            if (mSelectedAreas.Count == 0)
            {
                MsgBox.ShowWarning(Resources.MS_PLEASE_SELECT_ONE_VILLAGE_FOR_AFFECTED_AREAS, this.Text);
                return;
            }
            if (Method.ConcatVillageCode(mSelectedAreas).Length > 600)
            {
                MsgBox.ShowInformation(string.Format(Resources.MSG_VILLAGE_CHOOSED_IS_OVER_LOAD, Method.ConcatVillageCode(SelectedAreas).Split(',').Count().ToString()));
                return;
            }
            TotalCustomers = DataHelper.ParseToInt(lblCount.Text);
            this.DialogResult = DialogResult.OK;
        }

        private void trv_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Level == 1)
            {
                loadDistrict(e.Node);
            }
            else if (e.Node.Level == 2)
            {
                loadCommune(e.Node);
            }
            else if (e.Node.Level == 3)
            {
                loadVillage(e.Node);
            }
        }

        private void trv_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Nodes.Count > 0)
            {
                return;
            }
            if (e.Node.Level == 1)
            {
                if (!e.Node.Checked)
                {
                    return;
                }
                loadDistrict(e.Node);
            }
            else if (e.Node.Level == 2)
            {
                if (!e.Node.Checked)
                {
                    return;
                }
                loadCommune(e.Node);
            }
            else if (e.Node.Level == 3)
            {
                if (!e.Node.Checked)
                {
                    return;
                }
                loadVillage(e.Node);
            }
        }
        private void trv_Click(object sender, EventArgs e)
        {
            int nCus = 0;
            foreach (TreeNode pro in trv.Nodes[0].Nodes)
            {
                if (!pro.Checked)
                {
                    continue;
                }
                foreach (TreeNode dis in pro.Nodes)
                {
                    if (!dis.Checked)
                    {
                        continue;
                    }
                    foreach (TreeNode comm in dis.Nodes)
                    {
                        if (!comm.Checked)
                        {
                            continue;
                        }
                        foreach (TreeNode vil in comm.Nodes)
                        {
                            if (!vil.Checked)
                            {
                                continue;
                            }
                            nCus += DataHelper.ParseToInt(vil.ToolTipText.ToString());
                        }
                    }
                }
            }
            lblCount.Text = nCus.ToString();
        }

    }
}