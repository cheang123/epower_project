﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using EPower.Properties;

namespace EPower.Interface.OPSReport
{
    public partial class PageOPSR07 : Form
    {
        bool _load = true;

        #region Constructor
        public PageOPSR07()
        {
            InitializeComponent();
            _load = false;
            UIHelper.DataGridViewProperties(dgv);
            d1.Value = new DateTime(d1.Value.Year, d1.Value.Month,1 ); 
            loadData();
            _load = true;
        }
        #endregion

        #region Method

        private void loadData()
        {
            var DB = from r in DBDataContext.Db.TBL_OPS_R07s
                     join c in DBDataContext.Db.TBL_CUSTOMERs on r.CUSTOMER_ID equals c.CUSTOMER_ID
                     where r.IS_ACTIVE 
                            && (r.COMPLAINT_DATE.Date >= d1.Value.Date && r.COMPLAINT_DATE.Date <= d2.Value.Date)
                            && (r.COMPLAINT_REASON+" "+c.CUSTOMER_CODE+" "+c.LAST_NAME_KH+" "+c.FIRST_NAME_KH+" "+r.RESULT_DECISION).ToUpper().Contains(txtQuickSearch.Text.ToUpper()) 
                     orderby r.COMPLAINT_DATE
                     select new
                     {
                         r.COMPLAINT_INVOICE_ID
                         ,r.COMPLAINT_DATE
                         ,c.CUSTOMER_CODE
                         ,CUSTOMER_NAME=c.LAST_NAME_KH+" "+c.FIRST_NAME_KH
                         ,r.DECISION_DATE
                         ,r.COMPLAINT_REASON
                         ,r.RESULT_DECISION
                     };
            dgv.DataSource = DB;
                        
        }

        #endregion

        private void txt_QuickSearch(object sender, EventArgs e)
        {
            if (_load)
            {
                loadData();
            }
        } 
         
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void ChangeKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        } 

        private void btnADD_Click(object sender, EventArgs e)
        {
            DateTime dt=DBDataContext.Db.GetSystemDate();
            DialogOPSR07 dia = new DialogOPSR07(GeneralProcess.Insert, new TBL_OPS_R07() { COMPLAINT_DATE = dt, DECISION_DATE = dt });
            if (dia.ShowDialog() == DialogResult.OK)
            {
                loadData();
                UIHelper.SelectRow(dgv, dia.OPSR07.COMPLAINT_INVOICE_ID);
            }
        }

        private void btnEDIT_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                TBL_OPS_R07 ObjOPSR07 = DBDataContext.Db.TBL_OPS_R07s.FirstOrDefault(x => x.COMPLAINT_INVOICE_ID == (int)dgv.SelectedRows[0].Cells[COMPLAINT_INVOICE_ID.Name].Value);
                DialogOPSR07 dia = new DialogOPSR07(GeneralProcess.Update, ObjOPSR07);
                if (dia.ShowDialog() == DialogResult.OK)
                {
                    loadData();
                    UIHelper.SelectRow(dgv, dia.OPSR07.COMPLAINT_INVOICE_ID);
                }
            }
        }

        private void btnREMOVE_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                TBL_OPS_R07 ObjOPSR07 = DBDataContext.Db.TBL_OPS_R07s.FirstOrDefault(x => x.COMPLAINT_INVOICE_ID == (int)dgv.SelectedRows[0].Cells[COMPLAINT_INVOICE_ID.Name].Value);
                DialogOPSR07 dia = new DialogOPSR07(GeneralProcess.Delete, ObjOPSR07);
                if (dia.ShowDialog() == DialogResult.OK)
                {
                    loadData();
                    UIHelper.SelectRow(dgv, dia.OPSR07.COMPLAINT_INVOICE_ID-1);
                }
            }
        }



        
         
    }
}
