﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.OPSReport
{
    partial class DialogOPSR10
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogOPSR10));
            this.lblCUSTOMER_CODE = new System.Windows.Forms.Label();
            this.lblCUSTOMER_NAME = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.lblDISCONNECT_DATE = new System.Windows.Forms.Label();
            this.lblRECONNECT_DATE = new System.Windows.Forms.Label();
            this.lblDISCONNECT_REASON = new System.Windows.Forms.Label();
            this.dtpDisconnectDate = new System.Windows.Forms.DateTimePicker();
            this.dtpReconnectDate = new System.Windows.Forms.DateTimePicker();
            this.txtDisconnectReason = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCustomerCodes = new SoftTech.Component.ExTextbox();
            this.txtCustomerNames = new SoftTech.Component.ExTextbox();
            this.pbsComplaintReason = new System.Windows.Forms.PictureBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsComplaintReason)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.pbsComplaintReason);
            this.content.Controls.Add(this.txtCustomerNames);
            this.content.Controls.Add(this.txtCustomerCodes);
            this.content.Controls.Add(this.dtpReconnectDate);
            this.content.Controls.Add(this.dtpDisconnectDate);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.label6);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.lblDISCONNECT_REASON);
            this.content.Controls.Add(this.lblRECONNECT_DATE);
            this.content.Controls.Add(this.lblDISCONNECT_DATE);
            this.content.Controls.Add(this.lblCUSTOMER_NAME);
            this.content.Controls.Add(this.txtDisconnectReason);
            this.content.Controls.Add(this.lblCUSTOMER_CODE);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_CODE, 0);
            this.content.Controls.SetChildIndex(this.txtDisconnectReason, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblDISCONNECT_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblRECONNECT_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblDISCONNECT_REASON, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.label6, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.dtpDisconnectDate, 0);
            this.content.Controls.SetChildIndex(this.dtpReconnectDate, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerCodes, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerNames, 0);
            this.content.Controls.SetChildIndex(this.pbsComplaintReason, 0);
            // 
            // lblCUSTOMER_CODE
            // 
            resources.ApplyResources(this.lblCUSTOMER_CODE, "lblCUSTOMER_CODE");
            this.lblCUSTOMER_CODE.Name = "lblCUSTOMER_CODE";
            // 
            // lblCUSTOMER_NAME
            // 
            resources.ApplyResources(this.lblCUSTOMER_NAME, "lblCUSTOMER_NAME");
            this.lblCUSTOMER_NAME.Name = "lblCUSTOMER_NAME";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // lblDISCONNECT_DATE
            // 
            resources.ApplyResources(this.lblDISCONNECT_DATE, "lblDISCONNECT_DATE");
            this.lblDISCONNECT_DATE.Name = "lblDISCONNECT_DATE";
            // 
            // lblRECONNECT_DATE
            // 
            resources.ApplyResources(this.lblRECONNECT_DATE, "lblRECONNECT_DATE");
            this.lblRECONNECT_DATE.Name = "lblRECONNECT_DATE";
            // 
            // lblDISCONNECT_REASON
            // 
            resources.ApplyResources(this.lblDISCONNECT_REASON, "lblDISCONNECT_REASON");
            this.lblDISCONNECT_REASON.Name = "lblDISCONNECT_REASON";
            // 
            // dtpDisconnectDate
            // 
            resources.ApplyResources(this.dtpDisconnectDate, "dtpDisconnectDate");
            this.dtpDisconnectDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDisconnectDate.Name = "dtpDisconnectDate";
            this.dtpDisconnectDate.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // dtpReconnectDate
            // 
            resources.ApplyResources(this.dtpReconnectDate, "dtpReconnectDate");
            this.dtpReconnectDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpReconnectDate.Name = "dtpReconnectDate";
            this.dtpReconnectDate.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // txtDisconnectReason
            // 
            resources.ApplyResources(this.txtDisconnectReason, "txtDisconnectReason");
            this.txtDisconnectReason.Name = "txtDisconnectReason";
            this.txtDisconnectReason.Enter += new System.EventHandler(this.ChangeKeyboard);
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Name = "label6";
            // 
            // txtCustomerCodes
            // 
            this.txtCustomerCodes.BackColor = System.Drawing.Color.White;
            this.txtCustomerCodes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtCustomerCodes, "txtCustomerCodes");
            this.txtCustomerCodes.Name = "txtCustomerCodes";
            this.txtCustomerCodes.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtCustomerCodes.AdvanceSearch += new System.EventHandler(this.txtCustomerCodes_AdvanceSearch);
            this.txtCustomerCodes.CancelAdvanceSearch += new System.EventHandler(this.txtCustomerCodes_CancelAdvanceSearch);
            this.txtCustomerCodes.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // txtCustomerNames
            // 
            this.txtCustomerNames.BackColor = System.Drawing.Color.White;
            this.txtCustomerNames.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtCustomerNames, "txtCustomerNames");
            this.txtCustomerNames.Name = "txtCustomerNames";
            this.txtCustomerNames.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtCustomerNames.AdvanceSearch += new System.EventHandler(this.txtCustomerNames_AdvanceSearch);
            this.txtCustomerNames.CancelAdvanceSearch += new System.EventHandler(this.txtCustomerCodes_CancelAdvanceSearch);
            this.txtCustomerNames.Enter += new System.EventHandler(this.ChangeKeyboard);
            // 
            // pbsComplaintReason
            // 
            this.pbsComplaintReason.Image = global::EPower.Properties.Resources.question_mark;
            resources.ApplyResources(this.pbsComplaintReason, "pbsComplaintReason");
            this.pbsComplaintReason.Name = "pbsComplaintReason";
            this.pbsComplaintReason.TabStop = false;
            this.pbsComplaintReason.Click += new System.EventHandler(this.pbsDisconnectReason_Click);
            // 
            // DialogOPSR10
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogOPSR10";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsComplaintReason)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Label lblCUSTOMER_NAME;
        private Label lblCUSTOMER_CODE;
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label label1;
        private Label label9;
        private ExButton btnCHANGE_LOG;
        private DateTimePicker dtpReconnectDate;
        private DateTimePicker dtpDisconnectDate;
        private Label label6;
        private Label lblDISCONNECT_REASON;
        private Label lblRECONNECT_DATE;
        private Label lblDISCONNECT_DATE;
        private TextBox txtDisconnectReason;
        private ExTextbox txtCustomerNames;
        private ExTextbox txtCustomerCodes;
        private PictureBox pbsComplaintReason;
    }
}