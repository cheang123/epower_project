﻿using System.ComponentModel;
using System.Windows.Forms;

namespace EPower.Interface.OPSReport
{
    partial class PageOPSReportSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblOPS_REPORT = new System.Windows.Forms.Label();
            this.lblYEAR = new System.Windows.Forms.Label();
            this.cboYear = new System.Windows.Forms.ComboBox();
            this.cboQuarter = new System.Windows.Forms.ComboBox();
            this.lblQUARTER = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblOPS_REPORT
            // 
            this.lblOPS_REPORT.AutoSize = true;
            this.lblOPS_REPORT.Font = new System.Drawing.Font("Khmer OS Muol Light", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOPS_REPORT.Location = new System.Drawing.Point(32, 110);
            this.lblOPS_REPORT.Name = "lblOPS_REPORT";
            this.lblOPS_REPORT.Size = new System.Drawing.Size(620, 49);
            this.lblOPS_REPORT.TabIndex = 0;
            this.lblOPS_REPORT.Text = "របាយការណ៍ការផ្គត់ផ្គង់ និងសេវាកម្មអគ្គិសនី";
            // 
            // lblYEAR
            // 
            this.lblYEAR.AutoSize = true;
            this.lblYEAR.Location = new System.Drawing.Point(131, 172);
            this.lblYEAR.Margin = new System.Windows.Forms.Padding(2);
            this.lblYEAR.Name = "lblYEAR";
            this.lblYEAR.Size = new System.Drawing.Size(45, 19);
            this.lblYEAR.TabIndex = 1;
            this.lblYEAR.Text = "ប្រចាំឆ្នាំ";
            // 
            // cboYear
            // 
            this.cboYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboYear.FormattingEnabled = true;
            this.cboYear.Items.AddRange(new object[] {
            "2011",
            "2012"});
            this.cboYear.Location = new System.Drawing.Point(233, 168);
            this.cboYear.Margin = new System.Windows.Forms.Padding(1);
            this.cboYear.Name = "cboYear";
            this.cboYear.Size = new System.Drawing.Size(167, 27);
            this.cboYear.TabIndex = 5;
            this.cboYear.SelectedIndexChanged += new System.EventHandler(this.cboYear_SelectedIndexChanged);
            // 
            // cboQuarter
            // 
            this.cboQuarter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboQuarter.FormattingEnabled = true;
            this.cboQuarter.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this.cboQuarter.Location = new System.Drawing.Point(233, 201);
            this.cboQuarter.Name = "cboQuarter";
            this.cboQuarter.Size = new System.Drawing.Size(167, 27);
            this.cboQuarter.TabIndex = 14;
            this.cboQuarter.SelectedIndexChanged += new System.EventHandler(this.cboQuarter_SelectedIndexChanged);
            // 
            // lblQUARTER
            // 
            this.lblQUARTER.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblQUARTER.Location = new System.Drawing.Point(131, 203);
            this.lblQUARTER.Name = "lblQUARTER";
            this.lblQUARTER.Size = new System.Drawing.Size(86, 23);
            this.lblQUARTER.TabIndex = 15;
            this.lblQUARTER.Text = "ប្រចាំត្រីមាស";
            // 
            // PageOPSReportSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(677, 423);
            this.Controls.Add(this.cboQuarter);
            this.Controls.Add(this.lblQUARTER);
            this.Controls.Add(this.cboYear);
            this.Controls.Add(this.lblYEAR);
            this.Controls.Add(this.lblOPS_REPORT);
            this.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "PageOPSReportSetup";
            this.Text = "PageReportSetup";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label lblOPS_REPORT;
        private Label lblYEAR;
        public ComboBox cboYear;
        public ComboBox cboQuarter;
        private Label lblQUARTER;
    }
}