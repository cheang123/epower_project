﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface.OPSReport
{
    public partial class DialogOPSR03 : ExDialog
    {
        GeneralProcess _flag;
        TBL_OPS_R03 _objOPSR03 = new TBL_OPS_R03();
        TBL_CUSTOMER objCustomer = new TBL_CUSTOMER();
        public TBL_OPS_R03 OPSR03
        {
            get { return _objOPSR03; }
        }
        TBL_OPS_R03 _oldObjOPSR03 = new TBL_OPS_R03();

        #region Constructor
        public DialogOPSR03(GeneralProcess flag, TBL_OPS_R03 objOPSR03)
        {
            InitializeComponent();

            _flag = flag;
            objOPSR03._CopyTo(_objOPSR03);
            objOPSR03._CopyTo(_oldObjOPSR03);
            objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == _objOPSR03.CUSTOMER_ID);
            UIHelper.SetDataSourceToComboBox(cboDistrubtion, Lookup.GetDistribution());
            read();
            this.Text = flag.GetText(this.Text);
            UIHelper.SetEnabled(this, !(flag == GeneralProcess.Delete));
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
        }

        #endregion

        #region Method

        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();

            if (txtCustomerCodes.Text.Trim().Length <= 0)
            {
                txtCustomerCodes.SetValidation(string.Format(Resources.REQUIRED, lblCUSTOMER_CODE.Text));
                val = true;
            }

            if (txtCustomerNames.Text.Trim().Length <= 0)
            {
                txtCustomerNames.SetValidation(string.Format(Resources.REQUIRED, lblCUSTOMER_NAME.Text));
                val = true;
            }

            if (txtComplaintReason.Text.Trim().Length <= 0)
            {
                txtComplaintReason.SetValidation(string.Format(Resources.REQUIRED, lblCOMPLAINT_REASON.Text));
                val = true;
            }

            if (txtSolution.Text.Trim().Length <= 0)
            {
                txtSolution.SetValidation(string.Format(Resources.REQUIRED, lblSOLUTION.Text));
                val = true;
            }

            if (txtWorkDoneAfterComplaint.Text.Trim().Length <= 0)
            {
                txtWorkDoneAfterComplaint.SetValidation(string.Format(Resources.REQUIRED, lblWORK_DONE_AFTER_COMPLAINT.Text));
                val = true;
            }

            if (objCustomer == null)
            {
                txtCustomerCodes.SetValidation(string.Format(Resources.REQUIRED, lblCUSTOMER_CODE.Text));
                val = true;
            }

            if (cboDistrubtion.SelectedIndex == -1)
            {
                cboDistrubtion.SetValidation(string.Format(Resources.REQUIRED_SELECT_VALUE, lblDISTRIBUTION.Text));
                val = true;
            }
            return val;
        }

        private void read()
        {
            txtCustomerCodes.Text = _flag == GeneralProcess.Insert ? "" : objCustomer.CUSTOMER_CODE;
            txtCustomerNames.Text = _flag == GeneralProcess.Insert ? "" : objCustomer.LAST_NAME_KH + " " + objCustomer.FIRST_NAME_KH;
            if (_flag != GeneralProcess.Insert)
            {
                txtCustomerCodes.AcceptSearch(_flag == GeneralProcess.Insert);
                txtCustomerNames.AcceptSearch(_flag == GeneralProcess.Insert);
            }
            cboDistrubtion.SelectedValue = _objOPSR03.DISTRIBUTION_ID;
            dtpComplaintDate.Value = _objOPSR03.COMPLAINT_DATE;
            dtpMonitoringDate.Value = _objOPSR03.MONITORING_DATE;
            txtComplaintReason.Text = _objOPSR03.COMPLAINT_REASON;
            txtSolution.Text = _objOPSR03.SOLUTION;
            txtWorkDoneAfterComplaint.Text = _objOPSR03.WORK_DONE_AFTER_COMPLAINT;
        }

        private void write()
        {
            _objOPSR03.CUSTOMER_ID = objCustomer.CUSTOMER_ID;
            _objOPSR03.DISTRIBUTION_ID = (int)cboDistrubtion.SelectedValue;
            _objOPSR03.COMPLAINT_DATE = dtpComplaintDate.Value;
            _objOPSR03.MONITORING_DATE = dtpMonitoringDate.Value;
            _objOPSR03.COMPLAINT_REASON = txtComplaintReason.Text;
            _objOPSR03.SOLUTION = txtSolution.Text;
            _objOPSR03.WORK_DONE_AFTER_COMPLAINT = txtWorkDoneAfterComplaint.Text;
        }

        #endregion

        private void ChangeKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();

            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objOPSR03);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_oldObjOPSR03, _objOPSR03);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objOPSR03);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objOPSR03);
        }

        private void txtCustomerCodes_AdvanceSearch(object sender, EventArgs e)
        {
            // if not entry.
            if (txtCustomerCodes.Text.Trim() == "")
            {
                txtCustomerCodes.CancelSearch();
                return;
            }

            string strCustomerCode = Method.FormatCustomerCode(this.txtCustomerCodes.Text);
            objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(c => c.CUSTOMER_CODE == strCustomerCode && c.STATUS_ID == (int)CustomerStatus.Active);

            if (objCustomer == null)
            {
                MsgBox.ShowInformation(Resources.MS_CUSTOMER_NOT_FOUND);
                this.txtCustomerCodes.CancelSearch();
                return;
            }
            else
            {
                txtCustomerCodes.Text = objCustomer.CUSTOMER_CODE;
                txtCustomerNames.Text = objCustomer.LAST_NAME_KH + " " + objCustomer.FIRST_NAME_KH;
                txtCustomerNames.AcceptSearch(false);
            }
        }

        private void txtCustomerCodes_CancelAdvanceSearch(object sender, EventArgs e)
        {
            txtCustomerCodes.CancelSearch(false);
            txtCustomerNames.CancelSearch(false);
            txtCustomerCodes.Text = "";
            txtCustomerNames.Text = "";
            objCustomer = null;
            txtCustomerCodes.Focus();
        }

        private void txtCustomerNames_AdvanceSearch(object sender, EventArgs e)
        {
            DialogCustomerSearch diag = new DialogCustomerSearch(this.txtCustomerNames.Text, DialogCustomerSearch.PowerType.AllType);
            if (diag.ShowDialog() != DialogResult.OK)
            {
                this.txtCustomerNames.CancelSearch(false);
                return;
            }
            this.objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == diag.CustomerID);
            if (objCustomer == null)
            {
                this.txtCustomerNames.CancelSearch(false);
                return;
            }

            txtCustomerCodes.Text = objCustomer.CUSTOMER_CODE;
            txtCustomerNames.Text = objCustomer.LAST_NAME_KH + " " + objCustomer.FIRST_NAME_KH;
            txtCustomerCodes.AcceptSearch(false);
        }

        private void pbsComplaintReason_Click(object sender, EventArgs e)
        {
            DialogOPSSuggestion diag = new DialogOPSSuggestion("TBL_OPS_R03", "COMPLAINT_REASON");
            if (diag.ShowDialog() == DialogResult.OK)
            {
                txtComplaintReason.Text = txtComplaintReason.Text + diag.suggestionName;
                txtComplaintReason.Focus();
            }
        }

        private void pbsSolution_Click(object sender, EventArgs e)
        {
            DialogOPSSuggestion diag = new DialogOPSSuggestion("TBL_OPS_R03", "SOLUTION");
            if (diag.ShowDialog() == DialogResult.OK)
            {
                txtSolution.Text = txtSolution.Text + diag.suggestionName;
                txtSolution.Focus();
            }
        }

        private void pbsWorkDoneAfterComplete_Click(object sender, EventArgs e)
        {
            DialogOPSSuggestion diag = new DialogOPSSuggestion("TBL_OPS_R03", "WORK_DONE_AFTER_COMPLAINT");
            if (diag.ShowDialog() == DialogResult.OK)
            {
                txtWorkDoneAfterComplaint.Text = txtWorkDoneAfterComplaint.Text + diag.suggestionName;
                txtWorkDoneAfterComplaint.Focus();
            }
        }


    }
}