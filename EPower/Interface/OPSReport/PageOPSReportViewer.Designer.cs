﻿using System.ComponentModel;
using System.Windows.Forms;
using AxCrystalActiveXReportViewerLib105;
using SoftTech.Component;

namespace EPower.Interface.OPSReport
{
    partial class PageOPSReportViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnVIEW = new SoftTech.Component.ExButton();
            this.lblBUY_POWER_DETAIL_ = new System.Windows.Forms.Label();
            this.viewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnVIEW);
            this.panel1.Controls.Add(this.lblBUY_POWER_DETAIL_);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(849, 33);
            this.panel1.TabIndex = 7;
            // 
            // btnVIEW
            // 
            this.btnVIEW.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVIEW.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVIEW.Location = new System.Drawing.Point(766, 4);
            this.btnVIEW.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnVIEW.Name = "btnVIEW";
            this.btnVIEW.Size = new System.Drawing.Size(78, 23);
            this.btnVIEW.TabIndex = 6;
            this.btnVIEW.Text = "មើល";
            this.btnVIEW.UseVisualStyleBackColor = true;
            this.btnVIEW.Click += new System.EventHandler(this.btnView_Click);
            // 
            // lblBUY_POWER_DETAIL_
            // 
            this.lblBUY_POWER_DETAIL_.AutoSize = true;
            this.lblBUY_POWER_DETAIL_.Font = new System.Drawing.Font("Khmer OS Muol Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBUY_POWER_DETAIL_.Location = new System.Drawing.Point(3, 3);
            this.lblBUY_POWER_DETAIL_.Name = "lblBUY_POWER_DETAIL_";
            this.lblBUY_POWER_DETAIL_.Size = new System.Drawing.Size(279, 29);
            this.lblBUY_POWER_DETAIL_.TabIndex = 5;
            this.lblBUY_POWER_DETAIL_.Text = "ព័ត៌មានលំអិតអំពីការទិញថាមពល";
            // 
            // viewer
            // 
            this.viewer.ActiveViewIndex = -1;
            this.viewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.viewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.viewer.DisplayStatusBar = false;
            this.viewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.viewer.Location = new System.Drawing.Point(0, 33);
            this.viewer.Name = "viewer";
            this.viewer.Size = new System.Drawing.Size(849, 390);
            this.viewer.TabIndex = 8;
            this.viewer.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // PageOPSReportViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(849, 423);
            this.Controls.Add(this.viewer);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "PageOPSReportViewer";
            this.Text = "PageRunReport";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private Label lblBUY_POWER_DETAIL_;
        private ExButton btnVIEW;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer viewer;
    }
}