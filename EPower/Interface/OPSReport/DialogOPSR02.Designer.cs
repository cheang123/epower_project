﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.OPSReport
{
    partial class DialogOPSR02
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogOPSR02));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.lblDATE = new System.Windows.Forms.Label();
            this.lblAFFECTED_AREAS = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblBLACKOUT_END = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cboDISTRIBUTION = new System.Windows.Forms.ComboBox();
            this.lblDISTRIBUTION = new System.Windows.Forms.Label();
            this.lblBLACKOUT_BEGIN = new System.Windows.Forms.Label();
            this.dtpBLACKOUT_DATE = new System.Windows.Forms.DateTimePicker();
            this.dtpBLACKOUT_BEGIN = new System.Windows.Forms.DateTimePicker();
            this.dtpBLACKOUT_END = new System.Windows.Forms.DateTimePicker();
            this.txtBLACKOUT_DURATION = new System.Windows.Forms.TextBox();
            this.txtBLACKOUT_REASON = new System.Windows.Forms.TextBox();
            this.txtAFFECTED_AREAS = new System.Windows.Forms.TextBox();
            this.txtAFFECTED_CONSUMERS = new System.Windows.Forms.TextBox();
            this.lblAFFECTED_CONSUMERS = new System.Windows.Forms.Label();
            this.lblOUTAGES_DURATION = new System.Windows.Forms.Label();
            this.lblBLACKOUT_REASON = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btnSELECT_AREA1 = new SoftTech.Component.ExButton();
            this.pbsBlackOutReason = new System.Windows.Forms.PictureBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsBlackOutReason)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.pbsBlackOutReason);
            this.content.Controls.Add(this.btnSELECT_AREA1);
            this.content.Controls.Add(this.label11);
            this.content.Controls.Add(this.label10);
            this.content.Controls.Add(this.label7);
            this.content.Controls.Add(this.lblBLACKOUT_REASON);
            this.content.Controls.Add(this.lblOUTAGES_DURATION);
            this.content.Controls.Add(this.lblAFFECTED_CONSUMERS);
            this.content.Controls.Add(this.txtAFFECTED_CONSUMERS);
            this.content.Controls.Add(this.txtAFFECTED_AREAS);
            this.content.Controls.Add(this.txtBLACKOUT_REASON);
            this.content.Controls.Add(this.txtBLACKOUT_DURATION);
            this.content.Controls.Add(this.dtpBLACKOUT_END);
            this.content.Controls.Add(this.dtpBLACKOUT_BEGIN);
            this.content.Controls.Add(this.dtpBLACKOUT_DATE);
            this.content.Controls.Add(this.lblBLACKOUT_BEGIN);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.cboDISTRIBUTION);
            this.content.Controls.Add(this.lblDISTRIBUTION);
            this.content.Controls.Add(this.lblBLACKOUT_END);
            this.content.Controls.Add(this.label5);
            this.content.Controls.Add(this.lblAFFECTED_AREAS);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.lblDATE);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.lblDATE, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.lblAFFECTED_AREAS, 0);
            this.content.Controls.SetChildIndex(this.label5, 0);
            this.content.Controls.SetChildIndex(this.lblBLACKOUT_END, 0);
            this.content.Controls.SetChildIndex(this.lblDISTRIBUTION, 0);
            this.content.Controls.SetChildIndex(this.cboDISTRIBUTION, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.lblBLACKOUT_BEGIN, 0);
            this.content.Controls.SetChildIndex(this.dtpBLACKOUT_DATE, 0);
            this.content.Controls.SetChildIndex(this.dtpBLACKOUT_BEGIN, 0);
            this.content.Controls.SetChildIndex(this.dtpBLACKOUT_END, 0);
            this.content.Controls.SetChildIndex(this.txtBLACKOUT_DURATION, 0);
            this.content.Controls.SetChildIndex(this.txtBLACKOUT_REASON, 0);
            this.content.Controls.SetChildIndex(this.txtAFFECTED_AREAS, 0);
            this.content.Controls.SetChildIndex(this.txtAFFECTED_CONSUMERS, 0);
            this.content.Controls.SetChildIndex(this.lblAFFECTED_CONSUMERS, 0);
            this.content.Controls.SetChildIndex(this.lblOUTAGES_DURATION, 0);
            this.content.Controls.SetChildIndex(this.lblBLACKOUT_REASON, 0);
            this.content.Controls.SetChildIndex(this.label7, 0);
            this.content.Controls.SetChildIndex(this.label10, 0);
            this.content.Controls.SetChildIndex(this.label11, 0);
            this.content.Controls.SetChildIndex(this.btnSELECT_AREA1, 0);
            this.content.Controls.SetChildIndex(this.pbsBlackOutReason, 0);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnCLOSE_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnCHANGE_LOG_Click);
            // 
            // lblDATE
            // 
            resources.ApplyResources(this.lblDATE, "lblDATE");
            this.lblDATE.Name = "lblDATE";
            // 
            // lblAFFECTED_AREAS
            // 
            resources.ApplyResources(this.lblAFFECTED_AREAS, "lblAFFECTED_AREAS");
            this.lblAFFECTED_AREAS.Name = "lblAFFECTED_AREAS";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Name = "label5";
            // 
            // lblBLACKOUT_END
            // 
            resources.ApplyResources(this.lblBLACKOUT_END, "lblBLACKOUT_END");
            this.lblBLACKOUT_END.Name = "lblBLACKOUT_END";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // cboDISTRIBUTION
            // 
            this.cboDISTRIBUTION.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDISTRIBUTION.FormattingEnabled = true;
            resources.ApplyResources(this.cboDISTRIBUTION, "cboDISTRIBUTION");
            this.cboDISTRIBUTION.Name = "cboDISTRIBUTION";
            // 
            // lblDISTRIBUTION
            // 
            resources.ApplyResources(this.lblDISTRIBUTION, "lblDISTRIBUTION");
            this.lblDISTRIBUTION.Name = "lblDISTRIBUTION";
            // 
            // lblBLACKOUT_BEGIN
            // 
            resources.ApplyResources(this.lblBLACKOUT_BEGIN, "lblBLACKOUT_BEGIN");
            this.lblBLACKOUT_BEGIN.Name = "lblBLACKOUT_BEGIN";
            // 
            // dtpBLACKOUT_DATE
            // 
            resources.ApplyResources(this.dtpBLACKOUT_DATE, "dtpBLACKOUT_DATE");
            this.dtpBLACKOUT_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpBLACKOUT_DATE.Name = "dtpBLACKOUT_DATE";
            this.dtpBLACKOUT_DATE.ValueChanged += new System.EventHandler(this.dtpBLACKOUT_DATE_ValueChanged);
            this.dtpBLACKOUT_DATE.Enter += new System.EventHandler(this.ChangeImeEN);
            // 
            // dtpBLACKOUT_BEGIN
            // 
            resources.ApplyResources(this.dtpBLACKOUT_BEGIN, "dtpBLACKOUT_BEGIN");
            this.dtpBLACKOUT_BEGIN.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpBLACKOUT_BEGIN.Name = "dtpBLACKOUT_BEGIN";
            this.dtpBLACKOUT_BEGIN.Enter += new System.EventHandler(this.ChangeImeEN);
            // 
            // dtpBLACKOUT_END
            // 
            resources.ApplyResources(this.dtpBLACKOUT_END, "dtpBLACKOUT_END");
            this.dtpBLACKOUT_END.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpBLACKOUT_END.Name = "dtpBLACKOUT_END";
            this.dtpBLACKOUT_END.Enter += new System.EventHandler(this.ChangeImeEN);
            // 
            // txtBLACKOUT_DURATION
            // 
            this.txtBLACKOUT_DURATION.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.txtBLACKOUT_DURATION, "txtBLACKOUT_DURATION");
            this.txtBLACKOUT_DURATION.Name = "txtBLACKOUT_DURATION";
            this.txtBLACKOUT_DURATION.ReadOnly = true;
            this.txtBLACKOUT_DURATION.Enter += new System.EventHandler(this.ChangeImeEN);
            // 
            // txtBLACKOUT_REASON
            // 
            resources.ApplyResources(this.txtBLACKOUT_REASON, "txtBLACKOUT_REASON");
            this.txtBLACKOUT_REASON.Name = "txtBLACKOUT_REASON";
            this.txtBLACKOUT_REASON.Enter += new System.EventHandler(this.ChangeImeKH);
            // 
            // txtAFFECTED_AREAS
            // 
            this.txtAFFECTED_AREAS.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.txtAFFECTED_AREAS, "txtAFFECTED_AREAS");
            this.txtAFFECTED_AREAS.Name = "txtAFFECTED_AREAS";
            this.txtAFFECTED_AREAS.ReadOnly = true;
            // 
            // txtAFFECTED_CONSUMERS
            // 
            resources.ApplyResources(this.txtAFFECTED_CONSUMERS, "txtAFFECTED_CONSUMERS");
            this.txtAFFECTED_CONSUMERS.Name = "txtAFFECTED_CONSUMERS";
            this.txtAFFECTED_CONSUMERS.Enter += new System.EventHandler(this.ChangeImeEN);
            // 
            // lblAFFECTED_CONSUMERS
            // 
            resources.ApplyResources(this.lblAFFECTED_CONSUMERS, "lblAFFECTED_CONSUMERS");
            this.lblAFFECTED_CONSUMERS.Name = "lblAFFECTED_CONSUMERS";
            // 
            // lblOUTAGES_DURATION
            // 
            resources.ApplyResources(this.lblOUTAGES_DURATION, "lblOUTAGES_DURATION");
            this.lblOUTAGES_DURATION.Name = "lblOUTAGES_DURATION";
            // 
            // lblBLACKOUT_REASON
            // 
            resources.ApplyResources(this.lblBLACKOUT_REASON, "lblBLACKOUT_REASON");
            this.lblBLACKOUT_REASON.Name = "lblBLACKOUT_REASON";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Name = "label7";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Name = "label10";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Name = "label11";
            // 
            // btnSELECT_AREA1
            // 
            this.btnSELECT_AREA1.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.btnSELECT_AREA1, "btnSELECT_AREA1");
            this.btnSELECT_AREA1.Name = "btnSELECT_AREA1";
            this.btnSELECT_AREA1.UseVisualStyleBackColor = true;
            this.btnSELECT_AREA1.Click += new System.EventHandler(this.btnSELECT_AREA1_Click);
            // 
            // pbsBlackOutReason
            // 
            this.pbsBlackOutReason.Image = global::EPower.Properties.Resources.question_mark;
            resources.ApplyResources(this.pbsBlackOutReason, "pbsBlackOutReason");
            this.pbsBlackOutReason.Name = "pbsBlackOutReason";
            this.pbsBlackOutReason.TabStop = false;
            this.pbsBlackOutReason.Click += new System.EventHandler(this.pbsBlackOutReason_Click);
            // 
            // DialogOPSR02
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogOPSR02";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsBlackOutReason)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private ExButton btnCHANGE_LOG;
        private Label lblDATE;
        private Label label5;
        private Label lblAFFECTED_AREAS;
        private Label lblBLACKOUT_END;
        private Label label1;
        private ComboBox cboDISTRIBUTION;
        private Label lblDISTRIBUTION;
        private Label lblBLACKOUT_BEGIN;
        private DateTimePicker dtpBLACKOUT_DATE;
        private Label lblBLACKOUT_REASON;
        private Label lblOUTAGES_DURATION;
        private Label lblAFFECTED_CONSUMERS;
        private TextBox txtAFFECTED_CONSUMERS;
        private TextBox txtAFFECTED_AREAS;
        private TextBox txtBLACKOUT_REASON;
        private TextBox txtBLACKOUT_DURATION;
        private DateTimePicker dtpBLACKOUT_END;
        private DateTimePicker dtpBLACKOUT_BEGIN;
        private Label label11;
        private Label label10;
        private Label label7;
        private ExButton btnSELECT_AREA1;
        private PictureBox pbsBlackOutReason;
    }
}