﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface.OPSReport
{
    public partial class PageOPSR01 : Form
    {

        public PageOPSR01()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            /*
             * Default value and format
             */ 
            var date = DBDataContext.Db.GetSystemDate();
            dtpD1.Value = new DateTime(date.Year,date.Month,1);
            dtpD2.Value = dtpD1.Value.AddMonths(1).AddSeconds(-1);
            dtpD1.CustomFormat = dtpD2.CustomFormat = UIHelper._DefaultDateFormat;
            dtpD1.Format = dtpD2.Format = DateTimePickerFormat.Custom;
            /*
             * Cell DataGridView
             */ 
            OUTAGES_DATE.DefaultCellStyle.Format = UIHelper._DefaultDateFormat;
            OUTAGES_BEGIN.DefaultCellStyle.Format = UIHelper._DefaultShortDateTimeNoSecond;
            OUTAGES_END.DefaultCellStyle.Format = UIHelper._DefaultShortDateTimeNoSecond;
            OUTAGES_DURATION.DefaultCellStyle.Format = UIHelper._DefaultCustomNumber;
            /*
             * binding data
             */
            UIHelper.SetDataSourceToComboBox(cboDISTRIBUTION,Lookup.GetDistribution(),Resources.ALL_DISTRIBUTION);

            /*
             * Set event UI
             */
            cboDISTRIBUTION.SelectedIndexChanged += cboDISTRIBUTION_SelectedIndexChanged;
            dtpD1.ValueChanged += dtp_ValueChanged;
            dtpD2.ValueChanged += dtp_ValueChanged;

            /*
             * Load data
             */
            bind();
        }

        private void txt_QuickSearch(object sender, EventArgs e)
        {
            bind();
        }

        void dtp_ValueChanged(object sender, EventArgs e)
        {
            bind();
        }

        void cboDISTRIBUTION_SelectedIndexChanged(object sender, EventArgs e)
        {
            bind();
        }

        void bind()
        {
            if (cboDISTRIBUTION.SelectedIndex == -1)
            {
                return;
            }

            var disId = (int)cboDISTRIBUTION.SelectedValue;

            var q = from r1 in DBDataContext.Db.TBL_OPS_R01s
                    join d in DBDataContext.Db.TLKP_DISTRIBUTIONs on r1.DISTRIBUTION_ID equals d.DISTRIBUTION_ID
                    where r1.IS_ACTIVE
                          && r1.OUTAGES_DATE.Date >= dtpD1.Value.Date && r1.OUTAGES_DATE.Date <= dtpD2.Value.Date
                          && (disId == 0 || r1.DISTRIBUTION_ID == disId)
                          && (d.DISTRIBUTION_NAME + " " + r1.JOB_DESCRIPTION + " " + r1.AFFECTED_AREAS + " " + r1.NOTIFICATION_WAYS).ToLower().Contains(txtQuickSearch.Text.ToLower())
                    orderby r1.OUTAGES_DATE
                    select new
                    {
                        r1.SCHEDULE_OUTTAGES_ID,
                        r1.OUTAGES_DATE,
                        r1.JOB_DESCRIPTION,
                        d.DISTRIBUTION_NAME,
                        r1.OUTAGES_BEGIN,
                        r1.OUTAGES_END,
                        OUTAGES_DURATION = r1.OUTAGES_END.Subtract(r1.OUTAGES_BEGIN).TotalHours,
                        AFFECTED_AREAS = Method.ConcatVillageName(Method.villageByCode(r1.AFFECTED_AREAS)),
                        r1.AFFECTED_CONSUMERS,
                        r1.NOTIFICATION_WAYS
                    };

            dgv.DataSource = q.ToList();
        }

        public TBL_OPS_R01 OPSR01
        {
            get
            {
                if (dgv.SelectedRows.Count==0)
                {
                    return null;
                }
                var objR01 =
                    DBDataContext.Db.TBL_OPS_R01s.FirstOrDefault(
                        x => x.SCHEDULE_OUTTAGES_ID == (int) dgv.SelectedRows[0].Cells[SCHEDULE_OUTTAGES_ID.Name].Value);
                return objR01;
            }
        }
        private void ChangeKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void btnADD_Click(object sender, EventArgs e)
        {
            var now = DBDataContext.Db.GetSystemDate();
            var dig = new DialogOPSR01(GeneralProcess.Insert, new TBL_OPS_R01() { OUTAGES_DATE = now, OUTAGES_BEGIN = now, OUTAGES_END = now,AFFECTED_AREAS="" });
            if (dig.ShowDialog()== DialogResult.OK)
            {
                bind();
                UIHelper.SelectRow(dgv, dig.OPSR01.SCHEDULE_OUTTAGES_ID);
            }
        }

        private void btnEDIT_Click(object sender, EventArgs e)
        {
            if (OPSR01==null)
            {
                return;
            }
            var dig = new DialogOPSR01(GeneralProcess.Update, OPSR01);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                bind();
                UIHelper.SelectRow(dgv, dig.OPSR01.SCHEDULE_OUTTAGES_ID);
            }
        }

        private void btnREMOVE_Click(object sender, EventArgs e)
        {
            if (OPSR01 == null)
            {
                return;
            }
            var dig = new DialogOPSR01(GeneralProcess.Delete, OPSR01);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                bind();
                UIHelper.SelectRow(dgv, dig.OPSR01.SCHEDULE_OUTTAGES_ID-1);
            }
        }
    }
}