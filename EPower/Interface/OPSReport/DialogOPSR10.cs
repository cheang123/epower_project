﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface.OPSReport
{
    public partial class DialogOPSR10 : ExDialog
    {
        GeneralProcess _flag;
        TBL_OPS_R10 _objOPSR10 = new TBL_OPS_R10();
        TBL_CUSTOMER objCustomer = new TBL_CUSTOMER();
        public TBL_OPS_R10 OPSR10
        {
            get { return _objOPSR10; }
        }
        TBL_OPS_R10 _oldObjOPSR10 = new TBL_OPS_R10();

        #region Constructor
        public DialogOPSR10(GeneralProcess flag, TBL_OPS_R10 objOPSR10)
        {
            InitializeComponent();

            _flag = flag;
            objOPSR10._CopyTo(_objOPSR10);
            objOPSR10._CopyTo(_oldObjOPSR10);
            objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == _objOPSR10.CUSTOMER_ID);
            read();
            this.Text = flag.GetText(this.Text);
            UIHelper.SetEnabled(this, !(flag == GeneralProcess.Delete));
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
        }

        #endregion

        #region Method

        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();

            if (txtCustomerCodes.Text.Trim().Length <= 0)
            {
                txtCustomerCodes.SetValidation(string.Format(Resources.REQUIRED, lblCUSTOMER_CODE.Text));
                val = true;
            }

            if (txtCustomerNames.Text.Trim().Length <= 0)
            {
                txtCustomerNames.SetValidation(string.Format(Resources.REQUIRED, lblCUSTOMER_NAME.Text));
                val = true;
            }

            if (txtDisconnectReason.Text.Trim().Length <= 0)
            {
                txtDisconnectReason.SetValidation(string.Format(Resources.REQUIRED, lblDISCONNECT_REASON.Text));
                val = true;
            }

            if (objCustomer == null)
            {
                txtCustomerCodes.SetValidation(string.Format(Resources.REQUIRED, lblCUSTOMER_CODE.Text));
                val = true;
            }
            return val;
        }

        private void read()
        {
            txtCustomerCodes.Text = _flag == GeneralProcess.Insert ? "" : objCustomer.CUSTOMER_CODE;
            txtCustomerNames.Text = _flag == GeneralProcess.Insert ? "" : objCustomer.LAST_NAME_KH + " " + objCustomer.FIRST_NAME_KH;
            if (_flag != GeneralProcess.Insert)
            {
                txtCustomerCodes.Focus();
                txtCustomerNames.Focus();
            }
            dtpDisconnectDate.Value = _objOPSR10.DISCONNECT_DATE;
            dtpReconnectDate.Value = _objOPSR10.RECONNECT_DATE;
            txtDisconnectReason.Text = _objOPSR10.DISCONNECT_REASON;
        }

        private void write()
        {
            _objOPSR10.DISCONNECT_DATE = dtpDisconnectDate.Value;
            _objOPSR10.DISCONNECT_REASON = txtDisconnectReason.Text;
            _objOPSR10.CUSTOMER_ID = objCustomer.CUSTOMER_ID;
            _objOPSR10.RECONNECT_DATE = dtpReconnectDate.Value;
        }

        #endregion

        private void ChangeKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();

            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objOPSR10);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_oldObjOPSR10, _objOPSR10);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objOPSR10);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objOPSR10);
        }

        private void txtCustomerCodes_AdvanceSearch(object sender, EventArgs e)
        {
            // if not entry.
            if (txtCustomerCodes.Text.Trim() == "")
            {
                txtCustomerCodes.CancelSearch();
                return;
            }

            string strCustomerCode = Method.FormatCustomerCode(this.txtCustomerCodes.Text);
            objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(c => c.CUSTOMER_CODE == strCustomerCode && c.STATUS_ID == (int)CustomerStatus.Active);

            if (objCustomer == null)
            {
                MsgBox.ShowInformation(Resources.MS_CUSTOMER_NOT_FOUND);
                this.txtCustomerCodes.CancelSearch();
                return;
            }
            else
            {
                txtCustomerCodes.Text = objCustomer.CUSTOMER_CODE;
                txtCustomerNames.Text = objCustomer.LAST_NAME_KH + " " + objCustomer.FIRST_NAME_KH;
                txtCustomerNames.AcceptSearch(false);
            }
        }

        private void txtCustomerCodes_CancelAdvanceSearch(object sender, EventArgs e)
        {
            txtCustomerCodes.CancelSearch(false);
            txtCustomerNames.CancelSearch(false);
            txtCustomerCodes.Text = "";
            txtCustomerNames.Text = "";
            objCustomer = null;
            txtCustomerCodes.Focus();
        }

        private void txtCustomerNames_AdvanceSearch(object sender, EventArgs e)
        {
            DialogCustomerSearch diag = new DialogCustomerSearch(this.txtCustomerNames.Text, DialogCustomerSearch.PowerType.AllType);
            if (diag.ShowDialog() != DialogResult.OK)
            {
                this.txtCustomerNames.CancelSearch(false);
                return;
            }
            this.objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == diag.CustomerID);
            if (objCustomer == null)
            {
                this.txtCustomerNames.CancelSearch(false);
                return;
            }

            txtCustomerCodes.Text = objCustomer.CUSTOMER_CODE;
            txtCustomerNames.Text = objCustomer.LAST_NAME_KH + " " + objCustomer.FIRST_NAME_KH;
            txtCustomerCodes.AcceptSearch(false);
        }

        private void pbsDisconnectReason_Click(object sender, EventArgs e)
        {
            DialogOPSSuggestion diag = new DialogOPSSuggestion("TBL_OPS_R10", "DISCONNECT_REASON");
            if (diag.ShowDialog() == DialogResult.OK)
            {
                txtDisconnectReason.Text = txtDisconnectReason.Text + diag.suggestionName;
                txtDisconnectReason.Focus();
            }
        }
    }
}