﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface.OPSReport
{
    public partial class DialogOPSR06 : ExDialog
    {
        GeneralProcess _flag;
        TBL_OPS_R06 _objOPSR06 = new TBL_OPS_R06();
        TBL_CUSTOMER objCustomer = new TBL_CUSTOMER();
        public TBL_OPS_R06 OPSR06
        {
            get { return _objOPSR06; }
        }
        TBL_OPS_R06 _oldObjOPSR06 = new TBL_OPS_R06();

        #region Constructor
        public DialogOPSR06(GeneralProcess flag, TBL_OPS_R06 objOPSR06)
        {
            InitializeComponent();

            _flag = flag;
            objOPSR06._CopyTo(_objOPSR06);
            objOPSR06._CopyTo(_oldObjOPSR06);
            objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == _objOPSR06.CUSTOMER_ID);
            read();
            this.Text = flag.GetText(this.Text);
            UIHelper.SetEnabled(this, !(flag == GeneralProcess.Delete));
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
        }

        #endregion

        #region Method

        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();

            if (txtCustomerCodes.Text.Trim().Length <= 0)
            {
                txtCustomerCodes.SetValidation(string.Format(Resources.REQUIRED, lblCUSTOMER_CODE.Text));
                val = true;
            }

            if (txtCustomerNames.Text.Trim().Length <= 0)
            {
                txtCustomerNames.SetValidation(string.Format(Resources.REQUIRED, lblCUSTOMER_NAME.Text));
                val = true;
            }

            if (txtRequestReason.Text.Trim().Length <= 0)
            {
                txtRequestReason.SetValidation(string.Format(Resources.REQUIRED, lblREQUEST_REASON.Text));
                val = true;
            }

            if (txtTestResult.Text.Trim().Length <= 0)
            {
                txtTestResult.SetValidation(string.Format(Resources.REQUIRED, lblTEST_RESULT.Text));
                val = true;
            }

            if (objCustomer == null)
            {
                txtCustomerCodes.SetValidation(string.Format(Resources.REQUIRED, lblCUSTOMER_CODE.Text));
                val = true;
            }
            return val;
        }

        private void read()
        {
            txtCustomerCodes.Text = _flag == GeneralProcess.Insert ? "" : objCustomer.CUSTOMER_CODE;
            txtCustomerNames.Text = _flag == GeneralProcess.Insert ? "" : objCustomer.LAST_NAME_KH + " " + objCustomer.FIRST_NAME_KH;
            if (_flag != GeneralProcess.Insert)
            {
                txtCustomerCodes.AcceptSearch(false);
                txtCustomerNames.AcceptSearch(false);
            }
            dtpRequestDate.Value = _objOPSR06.REQUEST_DATE;
            dtpTestDate.Value = _objOPSR06.TEST_DATE;
            dtpNotificationDate.Value = _objOPSR06.NOTIFICATION_DATE;
            txtRequestReason.Text = _objOPSR06.REQUEST_REASON;
            txtTestResult.Text = _objOPSR06.TEST_RESULT;
        }

        private void write()
        {
            _objOPSR06.REQUEST_DATE = dtpRequestDate.Value;
            _objOPSR06.REQUEST_REASON = txtRequestReason.Text;
            _objOPSR06.CUSTOMER_ID = objCustomer.CUSTOMER_ID;
            _objOPSR06.TEST_DATE = dtpTestDate.Value;
            _objOPSR06.TEST_RESULT = txtTestResult.Text;
            _objOPSR06.NOTIFICATION_DATE = dtpNotificationDate.Value;
        }

        #endregion

        private void ChangeKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();

            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objOPSR06);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_oldObjOPSR06, _objOPSR06);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objOPSR06);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objOPSR06);
        }

        private void txtCustomerCodes_AdvanceSearch(object sender, EventArgs e)
        {
            // if not entry.
            if (txtCustomerCodes.Text.Trim() == "")
            {
                txtCustomerCodes.CancelSearch();
                return;
            }

            string strCustomerCode = Method.FormatCustomerCode(this.txtCustomerCodes.Text);
            objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(c => c.CUSTOMER_CODE == strCustomerCode && c.STATUS_ID == (int)CustomerStatus.Active);

            if (objCustomer == null)
            {
                MsgBox.ShowInformation(Resources.MS_CUSTOMER_NOT_FOUND);
                this.txtCustomerCodes.CancelSearch();
                return;
            }
            else
            {
                txtCustomerCodes.Text = objCustomer.CUSTOMER_CODE;
                txtCustomerNames.Text = objCustomer.LAST_NAME_KH + " " + objCustomer.FIRST_NAME_KH;
                txtCustomerNames.AcceptSearch(false);
            }
        }

        private void txtCustomerCodes_CancelAdvanceSearch(object sender, EventArgs e)
        {
            txtCustomerCodes.CancelSearch(false);
            txtCustomerNames.CancelSearch(false);
            txtCustomerCodes.Text = "";
            txtCustomerNames.Text = "";
            objCustomer = null;
            txtCustomerCodes.Focus();
        }

        private void txtCustomerNames_AdvanceSearch(object sender, EventArgs e)
        {
            DialogCustomerSearch diag = new DialogCustomerSearch(this.txtCustomerNames.Text, DialogCustomerSearch.PowerType.AllType);
            if (diag.ShowDialog() != DialogResult.OK)
            {
                this.txtCustomerNames.CancelSearch(false);
                return;
            }
            this.objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == diag.CustomerID);
            if (objCustomer == null)
            {
                this.txtCustomerNames.CancelSearch(false);
                return;
            }

            txtCustomerCodes.Text = objCustomer.CUSTOMER_CODE;
            txtCustomerNames.Text = objCustomer.LAST_NAME_KH + " " + objCustomer.FIRST_NAME_KH;
            txtCustomerCodes.AcceptSearch(false);
        }

        private void pbsRequestReason_Click(object sender, EventArgs e)
        {
            DialogOPSSuggestion diag = new DialogOPSSuggestion("TBL_OPS_R06", "REQUEST_REASON");
            if (diag.ShowDialog() == DialogResult.OK)
            {
                txtRequestReason.Text = txtRequestReason.Text + diag.suggestionName;
                txtRequestReason.Focus();
            }
        }

        private void pbsTestResult_Click(object sender, EventArgs e)
        {
            DialogOPSSuggestion diag = new DialogOPSSuggestion("TBL_OPS_R06", "TEST_RESULT");
            if (diag.ShowDialog() == DialogResult.OK)
            {
                txtTestResult.Text = txtTestResult.Text + diag.suggestionName;
                txtTestResult.Focus();
            }
        }
    }
}