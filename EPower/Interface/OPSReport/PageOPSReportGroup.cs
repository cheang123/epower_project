﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using EPower.Properties;
using iTextSharp.text;
using iTextSharp.text.pdf;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface.OPSReport

{
    public partial class PageOPSReportGroup : Form
    {

        public static int YEAR_ID = 0;
        public static int QUATER = 0;

        bool loading = true;
        string tableCode = "R99";

        public static void InializeReportData()
        {
            if (QUATER == 0)
            {
                QUATER = ((int)((DBDataContext.Db.GetSystemDate().Month - 1) / 3) + 1);
            }
            if (YEAR_ID == 0)
            {
                YEAR_ID = DBDataContext.Db.GetSystemDate().Year;
            }
        }
        public PageOPSReportGroup()
        {
            InitializeComponent();


            try
            {
                loading = true;

                UIHelper.SetDataSourceToComboBox(cboYear, PageOPSReportSetup.DataYear());
                UIHelper.SetDataSourceToComboBox(cboQuarter, PageOPSReportSetup.DataQuarter());

                InializeReportData();
                cboYear.SelectedValue = YEAR_ID;
                cboQuarter.SelectedValue = QUATER;

                loading = false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ViewReport(string tableCode)
        {
            this.cboYear.SelectedValue = YEAR_ID;
            this.cboQuarter.SelectedValue = QUATER;

            var tables = DBDataContext.Db.TBL_OPS_TABLEs
                                         .Where(x => x.IS_ACTIVE && (tableCode == "R99" || x.TABLE_CODE == tableCode))
                                         .OrderBy(x => x.TABLE_CODE);

            this.tabControl1.TabPages.Clear();
            foreach (var table in tables)
            {
                var tabPage = new TabPage(table.TABLE_CODE);
                tabControl1.TabPages.Add(tabPage);

                var frm = new PageOPSReportViewer(table.TABLE_CODE);
                frm.TopLevel = false;
                frm.Show();

                tabPage.Controls.Add(frm);
            }

            this.btnPRINT_ALL.Visible =
                btnEXPORT_TO_EXCEL.Visible = tableCode == "R99";
        }

        private void btnPrintAll_Click(object sender, EventArgs e)
        {
            var diag = new PrintDialog();
            if (diag.ShowDialog() != DialogResult.OK)
            {
                return;
            }
            var printerName = diag.PrinterSettings.PrinterName;


            // print both Landscape & Portrait require default Printer
            // so 1. find original default printer
            //    2. set default printer as new selected pritner
            //    3. printer report to default printer ("")
            //    4. set default printer to the original

            var defaultPrinter = GetDefaultPrinter();
            SetDefaultPrinter(printerName);
            foreach (var table in DBDataContext.Db.TBL_OPS_TABLEs.Where(x => x.IS_ACTIVE).OrderBy(x => x.TABLE_CODE))
            {
                // printer to default printer
                printReport(table.TABLE_CODE, "");
            }
            SetDefaultPrinter(defaultPrinter);
        }

        void printReport(string tableCode, string printerName)
        {
            var cr = new CrystalReportHelper();

            cr.OpenReport(string.Format("{0}.rpt", tableCode));
            cr.SetParameter("@QUARTER", QUATER);
            cr.SetParameter("@YEAR_ID", YEAR_ID);
            cr.PrintReport(printerName);

        }

        private void cboYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!loading)
            {
                YEAR_ID = (int)cboYear.SelectedValue;
            }
        }

        private void cboQuarter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!loading)
            {
                QUATER = (int)cboQuarter.SelectedValue;
            }
        }

        [DllImport("Winspool.drv")]
        private static extern bool SetDefaultPrinter(string printerName);

        string GetDefaultPrinter()
        {
            PrinterSettings settings = new PrinterSettings();
            foreach (string printer in PrinterSettings.InstalledPrinters)
            {
                settings.PrinterName = printer;
                if (settings.IsDefaultPrinter)
                    return printer;
            }
            return string.Empty;
        }
        private static void MergePDF(string R00, string R01, string R02, string R03, string R04, string R05, string R06, string R07, string R08, string R09, string R10)
        {
            string[] fileArray = new string[12];
            fileArray[0] = R00;
            fileArray[1] = R01;
            fileArray[2] = R02;
            fileArray[3] = R03;
            fileArray[4] = R04;
            fileArray[5] = R05;
            fileArray[6] = R06;
            fileArray[7] = R07;
            fileArray[8] = R08;
            fileArray[9] = R09;
            fileArray[10] = R10;
            PdfReader reader = null;
            Document sourceDocument = null;
            PdfCopy pdfCopyProvider = null;
            PdfImportedPage importedPage;
            string outputPdfPath = string.Concat(Application.StartupPath, "\\", Settings.Default.PATH_TEMP, string.Format("OPS Report {0} QUARTER {1}.pdf", YEAR_ID, QUATER));

            sourceDocument = new Document();
            pdfCopyProvider = new PdfCopy(sourceDocument, new System.IO.FileStream(outputPdfPath, System.IO.FileMode.Create));

            //output file Open  
            sourceDocument.Open();


            //files list wise Loop  
            for (int f = 0; f < fileArray.Length - 1; f++)
            {
                int pages = TotalPageCount(fileArray[f]);

                reader = new PdfReader(fileArray[f]);
                //Add pages in new file  
                for (int i = 1; i <= pages; i++)
                {
                    importedPage = pdfCopyProvider.GetImportedPage(reader, i);
                    pdfCopyProvider.AddPage(importedPage);
                }

                reader.Close();
            }
            //save the output file  
            sourceDocument.Close();
        }

        private static int TotalPageCount(string file)
        {
            using (StreamReader sr = new StreamReader(System.IO.File.OpenRead(file)))
            {
                Regex regex = new Regex(@"/Type\s*/Page[^s]");
                MatchCollection matches = regex.Matches(sr.ReadToEnd());

                return matches.Count;
            }
        }
        private void btnExportToExcel_Click(object sender, EventArgs e)
        {
            List<string> fileReport = new List<string>();
            CrystalReportHelper report = null;
            int i = 0;
            foreach (var itm in DBDataContext.Db.TBL_OPS_TABLEs.Where(x => x.IS_ACTIVE).OrderBy(x => x.TABLE_CODE))
            {
                // printer to default printer
                report = new CrystalReportHelper(string.Format(@"{0}.rpt", itm.TABLE_CODE));
                report.SetParameter("@QUARTER", QUATER);
                report.SetParameter("@YEAR_ID", YEAR_ID);
                string part = string.Concat(Application.StartupPath, "\\", Settings.Default.PATH_TEMP, string.Format(@"\{0}", itm.TABLE_CODE), ".xls");
                fileReport.Add(part);
                report.ExportToExcel(fileReport[i]);
                i++;
            }
            bool result = false;
            SaveFileDialog sdig = new SaveFileDialog();
            sdig.Filter = "Excel 2007|*.xlsx|Excel 2003-97|*.xls";
            sdig.FileName = string.Format("OPS Report {0} QUARTER {1}.xlsx", YEAR_ID, QUATER);
            if (sdig.ShowDialog() == DialogResult.OK)
            {
                result = SoftTech.Helper.ExcelEngine.CombineWorkBooks(sdig.FileName, fileReport.ToArray(), false);
            }
            if (result)
            {
                System.Diagnostics.Process.Start("explorer.exe", "/select," + sdig.FileName);
            }
        }

        private void btnMAIL_Click(object sender, EventArgs e)
        {
            List<string> fileReport = new List<string>();
            CrystalReportHelper report = null;
            int i = 0;
            foreach (var itm in DBDataContext.Db.TBL_OPS_TABLEs.Where(x => x.IS_ACTIVE).OrderBy(x => x.TABLE_CODE))
            {
                // printer to default printer
                report = new CrystalReportHelper(string.Format(@"OPS\{0}.rpt", itm.TABLE_CODE));
                report.SetParameter("@QUARTER", QUATER);
                report.SetParameter("@YEAR_ID", YEAR_ID);
                string part = string.Concat(Application.StartupPath, "\\", Settings.Default.PATH_TEMP, string.Format(@"\{0}", itm.TABLE_CODE), ".pdf");
                fileReport.Add(part);
                report.ExportToPDF(fileReport[i]);
                i++;
            }
            string R00= string.Concat(Application.StartupPath, "\\", Settings.Default.PATH_TEMP, "R00.pdf");
            string R01 = string.Concat(Application.StartupPath, "\\", Settings.Default.PATH_TEMP, "R01.pdf");
            string R02 = string.Concat(Application.StartupPath, "\\", Settings.Default.PATH_TEMP, "R02.pdf");
            string R03 = string.Concat(Application.StartupPath, "\\", Settings.Default.PATH_TEMP, "R03.pdf");
            string R04 = string.Concat(Application.StartupPath, "\\", Settings.Default.PATH_TEMP, "R04.pdf");
            string R05 = string.Concat(Application.StartupPath, "\\", Settings.Default.PATH_TEMP, "R05.pdf");
            string R06 = string.Concat(Application.StartupPath, "\\", Settings.Default.PATH_TEMP, "R06.pdf");
            string R07 = string.Concat(Application.StartupPath, "\\", Settings.Default.PATH_TEMP, "R07.pdf");
            string R08 = string.Concat(Application.StartupPath, "\\", Settings.Default.PATH_TEMP, "R08.pdf");
            string R09 = string.Concat(Application.StartupPath, "\\", Settings.Default.PATH_TEMP, "R09.pdf");
            string R10 = string.Concat(Application.StartupPath, "\\", Settings.Default.PATH_TEMP, "R10.pdf");
            MergePDF(R00,R01,R02,R03,R04,R05,R06,R07,R08,R09,R10);
            DialogSendMail diag = new DialogSendMail();
            diag.chkMERG_FILE.Checked = false;
            diag.chkMERG_FILE.Visible = false;
            TBL_COMPANY com = null;
            com = DBDataContext.Db.TBL_COMPANies.FirstOrDefault();
            string licensename = com.LICENSE_NUMBER + " - " + com.COMPANY_NAME;
            string exportPath= string.Concat(Application.StartupPath, "\\", Settings.Default.PATH_TEMP, string.Format("OPS Report {0} QUARTER {1}.pdf", YEAR_ID, QUATER));
            diag.txtTo.Text = "eackh.report@gmail.com";
            diag.txtSubject.Text = licensename + " - " + Resources.O_P_S_REPORT + Resources.QUARTER_NO + cboQuarter.SelectedValue.ToString() + " " + Resources.YEAR + cboYear.SelectedValue;
            diag.Add(string.Format("OPS Report {0} QUARTER {1}.pdf", YEAR_ID, QUATER), exportPath);
            diag.ShowDialog();
        }
    }
}
