﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using EPower.Properties;

namespace EPower.Interface.OPSReport
{
    public partial class PageOPSR06 : Form
    {
        bool _load = true;

        #region Constructor
        public PageOPSR06()
        {
            InitializeComponent();
            _load = false;
            UIHelper.DataGridViewProperties(dgv);
            d1.Value = new DateTime(d1.Value.Year, d1.Value.Month,1 ); 
            loadData();
            _load = true;
        }
        #endregion

        #region Method

        private void loadData()
        {
            var DB = from r in DBDataContext.Db.TBL_OPS_R06s
                     join c in DBDataContext.Db.TBL_CUSTOMERs on r.CUSTOMER_ID equals c.CUSTOMER_ID
                     where r.IS_ACTIVE
                            && (r.REQUEST_DATE.Date >= d1.Value.Date && r.REQUEST_DATE.Date <= d2.Value.Date)
                            && (r.TEST_RESULT +" "+r.REQUEST_REASON+ " " + c.CUSTOMER_CODE + " " + c.LAST_NAME_KH + " " + c.FIRST_NAME_KH).ToUpper().Contains(txtQuickSearch.Text.ToUpper()) 
                     orderby r.REQUEST_DATE
                     select new
                     {
                         r.METER_TESTING_ID
                         ,r.REQUEST_DATE
                         ,c.CUSTOMER_CODE
                         ,CUSTOMER_NAME=c.LAST_NAME_KH+" "+c.FIRST_NAME_KH
                         ,r.REQUEST_REASON
                         ,r.TEST_DATE
                         ,r.NOTIFICATION_DATE
                         ,r.TEST_RESULT
                     };
            dgv.DataSource = DB;
                        
        }

        #endregion

        private void txt_QuickSearch(object sender, EventArgs e)
        {
            if (_load)
            {
                loadData();
            }
        } 
         
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void ChangeKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        } 

        private void btnADD_Click(object sender, EventArgs e)
        {
            DateTime dt=DBDataContext.Db.GetSystemDate();
            DialogOPSR06 dia = new DialogOPSR06(GeneralProcess.Insert, new TBL_OPS_R06() { REQUEST_DATE = dt, TEST_DATE = dt,NOTIFICATION_DATE=dt });
            if (dia.ShowDialog() == DialogResult.OK)
            {
                loadData();
                UIHelper.SelectRow(dgv, dia.OPSR06.METER_TESTING_ID);
            }
        }

        private void btnEDIT_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                TBL_OPS_R06 ObjOPSR06 = DBDataContext.Db.TBL_OPS_R06s.FirstOrDefault(x => x.METER_TESTING_ID == (int)dgv.SelectedRows[0].Cells[METER_TESTING_ID.Name].Value);
                DialogOPSR06 dia = new DialogOPSR06(GeneralProcess.Update, ObjOPSR06);
                if (dia.ShowDialog() == DialogResult.OK)
                {
                    loadData();
                    UIHelper.SelectRow(dgv, dia.OPSR06.METER_TESTING_ID);
                }
            }
        }

        private void btnREMOVE_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                TBL_OPS_R06 ObjOPSR06 = DBDataContext.Db.TBL_OPS_R06s.FirstOrDefault(x => x.METER_TESTING_ID == (int)dgv.SelectedRows[0].Cells[METER_TESTING_ID.Name].Value);
                DialogOPSR06 dia = new DialogOPSR06(GeneralProcess.Delete, ObjOPSR06);
                if (dia.ShowDialog() == DialogResult.OK)
                {
                    loadData();
                    UIHelper.SelectRow(dgv, dia.OPSR06.METER_TESTING_ID);
                }
            }
        }



        
         
    }
}
