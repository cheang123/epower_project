﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.OPSReport
{
    partial class DialogOPSR03
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogOPSR03));
            this.lblCUSTOMER_CODE = new System.Windows.Forms.Label();
            this.lblCUSTOMER_NAME = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.lblCOMPLAINT_DATE = new System.Windows.Forms.Label();
            this.lblMONITORING_DATE = new System.Windows.Forms.Label();
            this.lblCOMPLAINT_REASON = new System.Windows.Forms.Label();
            this.lblSOLUTION = new System.Windows.Forms.Label();
            this.dtpComplaintDate = new System.Windows.Forms.DateTimePicker();
            this.dtpMonitoringDate = new System.Windows.Forms.DateTimePicker();
            this.txtComplaintReason = new System.Windows.Forms.TextBox();
            this.txtSolution = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtCustomerCodes = new SoftTech.Component.ExTextbox();
            this.txtCustomerNames = new SoftTech.Component.ExTextbox();
            this.pbsComplaintReason = new System.Windows.Forms.PictureBox();
            this.pbsSolution = new System.Windows.Forms.PictureBox();
            this.cboDistrubtion = new System.Windows.Forms.ComboBox();
            this.lblDISTRIBUTION = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtWorkDoneAfterComplaint = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblWORK_DONE_AFTER_COMPLAINT = new System.Windows.Forms.Label();
            this.pbsWorkDoneAfterComplete = new System.Windows.Forms.PictureBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsComplaintReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsSolution)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsWorkDoneAfterComplete)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.cboDistrubtion);
            this.content.Controls.Add(this.pbsWorkDoneAfterComplete);
            this.content.Controls.Add(this.pbsSolution);
            this.content.Controls.Add(this.pbsComplaintReason);
            this.content.Controls.Add(this.txtCustomerNames);
            this.content.Controls.Add(this.txtCustomerCodes);
            this.content.Controls.Add(this.lblWORK_DONE_AFTER_COMPLAINT);
            this.content.Controls.Add(this.lblSOLUTION);
            this.content.Controls.Add(this.dtpMonitoringDate);
            this.content.Controls.Add(this.dtpComplaintDate);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.label4);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.label2);
            this.content.Controls.Add(this.label7);
            this.content.Controls.Add(this.label6);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.lblCOMPLAINT_REASON);
            this.content.Controls.Add(this.lblMONITORING_DATE);
            this.content.Controls.Add(this.lblCOMPLAINT_DATE);
            this.content.Controls.Add(this.lblDISTRIBUTION);
            this.content.Controls.Add(this.lblCUSTOMER_NAME);
            this.content.Controls.Add(this.txtWorkDoneAfterComplaint);
            this.content.Controls.Add(this.txtSolution);
            this.content.Controls.Add(this.txtComplaintReason);
            this.content.Controls.Add(this.lblCUSTOMER_CODE);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_CODE, 0);
            this.content.Controls.SetChildIndex(this.txtComplaintReason, 0);
            this.content.Controls.SetChildIndex(this.txtSolution, 0);
            this.content.Controls.SetChildIndex(this.txtWorkDoneAfterComplaint, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblDISTRIBUTION, 0);
            this.content.Controls.SetChildIndex(this.lblCOMPLAINT_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblMONITORING_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblCOMPLAINT_REASON, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.label6, 0);
            this.content.Controls.SetChildIndex(this.label7, 0);
            this.content.Controls.SetChildIndex(this.label2, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.label4, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.dtpComplaintDate, 0);
            this.content.Controls.SetChildIndex(this.dtpMonitoringDate, 0);
            this.content.Controls.SetChildIndex(this.lblSOLUTION, 0);
            this.content.Controls.SetChildIndex(this.lblWORK_DONE_AFTER_COMPLAINT, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerCodes, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerNames, 0);
            this.content.Controls.SetChildIndex(this.pbsComplaintReason, 0);
            this.content.Controls.SetChildIndex(this.pbsSolution, 0);
            this.content.Controls.SetChildIndex(this.pbsWorkDoneAfterComplete, 0);
            this.content.Controls.SetChildIndex(this.cboDistrubtion, 0);
            // 
            // lblCUSTOMER_CODE
            // 
            resources.ApplyResources(this.lblCUSTOMER_CODE, "lblCUSTOMER_CODE");
            this.lblCUSTOMER_CODE.Name = "lblCUSTOMER_CODE";
            // 
            // lblCUSTOMER_NAME
            // 
            resources.ApplyResources(this.lblCUSTOMER_NAME, "lblCUSTOMER_NAME");
            this.lblCUSTOMER_NAME.Name = "lblCUSTOMER_NAME";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // lblCOMPLAINT_DATE
            // 
            resources.ApplyResources(this.lblCOMPLAINT_DATE, "lblCOMPLAINT_DATE");
            this.lblCOMPLAINT_DATE.Name = "lblCOMPLAINT_DATE";
            // 
            // lblMONITORING_DATE
            // 
            resources.ApplyResources(this.lblMONITORING_DATE, "lblMONITORING_DATE");
            this.lblMONITORING_DATE.Name = "lblMONITORING_DATE";
            // 
            // lblCOMPLAINT_REASON
            // 
            resources.ApplyResources(this.lblCOMPLAINT_REASON, "lblCOMPLAINT_REASON");
            this.lblCOMPLAINT_REASON.Name = "lblCOMPLAINT_REASON";
            // 
            // lblSOLUTION
            // 
            resources.ApplyResources(this.lblSOLUTION, "lblSOLUTION");
            this.lblSOLUTION.Name = "lblSOLUTION";
            // 
            // dtpComplaintDate
            // 
            resources.ApplyResources(this.dtpComplaintDate, "dtpComplaintDate");
            this.dtpComplaintDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpComplaintDate.Name = "dtpComplaintDate";
            this.dtpComplaintDate.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // dtpMonitoringDate
            // 
            resources.ApplyResources(this.dtpMonitoringDate, "dtpMonitoringDate");
            this.dtpMonitoringDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMonitoringDate.Name = "dtpMonitoringDate";
            this.dtpMonitoringDate.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // txtComplaintReason
            // 
            resources.ApplyResources(this.txtComplaintReason, "txtComplaintReason");
            this.txtComplaintReason.Name = "txtComplaintReason";
            this.txtComplaintReason.Enter += new System.EventHandler(this.ChangeKeyboard);
            // 
            // txtSolution
            // 
            resources.ApplyResources(this.txtSolution, "txtSolution");
            this.txtSolution.Name = "txtSolution";
            this.txtSolution.Enter += new System.EventHandler(this.ChangeKeyboard);
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Name = "label6";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Name = "label7";
            // 
            // txtCustomerCodes
            // 
            this.txtCustomerCodes.BackColor = System.Drawing.Color.White;
            this.txtCustomerCodes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtCustomerCodes, "txtCustomerCodes");
            this.txtCustomerCodes.Name = "txtCustomerCodes";
            this.txtCustomerCodes.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtCustomerCodes.AdvanceSearch += new System.EventHandler(this.txtCustomerCodes_AdvanceSearch);
            this.txtCustomerCodes.CancelAdvanceSearch += new System.EventHandler(this.txtCustomerCodes_CancelAdvanceSearch);
            this.txtCustomerCodes.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // txtCustomerNames
            // 
            this.txtCustomerNames.BackColor = System.Drawing.Color.White;
            this.txtCustomerNames.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtCustomerNames, "txtCustomerNames");
            this.txtCustomerNames.Name = "txtCustomerNames";
            this.txtCustomerNames.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtCustomerNames.AdvanceSearch += new System.EventHandler(this.txtCustomerNames_AdvanceSearch);
            this.txtCustomerNames.CancelAdvanceSearch += new System.EventHandler(this.txtCustomerCodes_CancelAdvanceSearch);
            this.txtCustomerNames.Enter += new System.EventHandler(this.ChangeKeyboard);
            // 
            // pbsComplaintReason
            // 
            this.pbsComplaintReason.Image = global::EPower.Properties.Resources.question_mark;
            resources.ApplyResources(this.pbsComplaintReason, "pbsComplaintReason");
            this.pbsComplaintReason.Name = "pbsComplaintReason";
            this.pbsComplaintReason.TabStop = false;
            this.pbsComplaintReason.Click += new System.EventHandler(this.pbsComplaintReason_Click);
            // 
            // pbsSolution
            // 
            this.pbsSolution.Image = global::EPower.Properties.Resources.question_mark;
            resources.ApplyResources(this.pbsSolution, "pbsSolution");
            this.pbsSolution.Name = "pbsSolution";
            this.pbsSolution.TabStop = false;
            this.pbsSolution.Click += new System.EventHandler(this.pbsSolution_Click);
            // 
            // cboDistrubtion
            // 
            this.cboDistrubtion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDistrubtion.DropDownWidth = 200;
            this.cboDistrubtion.FormattingEnabled = true;
            resources.ApplyResources(this.cboDistrubtion, "cboDistrubtion");
            this.cboDistrubtion.Name = "cboDistrubtion";
            // 
            // lblDISTRIBUTION
            // 
            resources.ApplyResources(this.lblDISTRIBUTION, "lblDISTRIBUTION");
            this.lblDISTRIBUTION.Name = "lblDISTRIBUTION";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Name = "label4";
            // 
            // txtWorkDoneAfterComplaint
            // 
            resources.ApplyResources(this.txtWorkDoneAfterComplaint, "txtWorkDoneAfterComplaint");
            this.txtWorkDoneAfterComplaint.Name = "txtWorkDoneAfterComplaint";
            this.txtWorkDoneAfterComplaint.Enter += new System.EventHandler(this.ChangeKeyboard);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Name = "label2";
            // 
            // lblWORK_DONE_AFTER_COMPLAINT
            // 
            resources.ApplyResources(this.lblWORK_DONE_AFTER_COMPLAINT, "lblWORK_DONE_AFTER_COMPLAINT");
            this.lblWORK_DONE_AFTER_COMPLAINT.Name = "lblWORK_DONE_AFTER_COMPLAINT";
            // 
            // pbsWorkDoneAfterComplete
            // 
            this.pbsWorkDoneAfterComplete.Image = global::EPower.Properties.Resources.question_mark;
            resources.ApplyResources(this.pbsWorkDoneAfterComplete, "pbsWorkDoneAfterComplete");
            this.pbsWorkDoneAfterComplete.Name = "pbsWorkDoneAfterComplete";
            this.pbsWorkDoneAfterComplete.TabStop = false;
            this.pbsWorkDoneAfterComplete.Click += new System.EventHandler(this.pbsWorkDoneAfterComplete_Click);
            // 
            // DialogOPSR03
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogOPSR03";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsComplaintReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsSolution)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsWorkDoneAfterComplete)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Label lblCUSTOMER_NAME;
        private Label lblCUSTOMER_CODE;
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label label1;
        private Label label9;
        private ExButton btnCHANGE_LOG;
        private Label lblSOLUTION;
        private DateTimePicker dtpMonitoringDate;
        private DateTimePicker dtpComplaintDate;
        private Label label7;
        private Label label6;
        private Label lblCOMPLAINT_REASON;
        private Label lblMONITORING_DATE;
        private Label lblCOMPLAINT_DATE;
        private TextBox txtSolution;
        private TextBox txtComplaintReason;
        private ExTextbox txtCustomerNames;
        private ExTextbox txtCustomerCodes;
        private PictureBox pbsComplaintReason;
        private PictureBox pbsSolution;
        private ComboBox cboDistrubtion;
        private Label label4;
        private Label lblDISTRIBUTION;
        private PictureBox pbsWorkDoneAfterComplete;
        private Label lblWORK_DONE_AFTER_COMPLAINT;
        private Label label2;
        private TextBox txtWorkDoneAfterComplaint;
    }
}