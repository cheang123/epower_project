﻿using System;
using System.Data;
using System.Windows.Forms;
using SoftTech;
using SoftTech.Helper;

namespace EPower.Interface.OPSReport
{
    public partial class PageOPSReportSetup : Form
    {
        bool loading = true;
        public PageOPSReportSetup()
        {
            InitializeComponent();

            try
            {
                this.loading = true;
                
                UIHelper.SetDataSourceToComboBox(cboYear, DataYear());
                UIHelper.SetDataSourceToComboBox(cboQuarter, DataQuarter());

                var now = DBDataContext.Db.GetSystemDate();
                cboYear.SelectedValue = now.Year;
                cboQuarter.SelectedValue = ((int)((now.Month - 1) / 3) + 1);

                PageOPSReportGroup.YEAR_ID = (int)cboYear.SelectedValue;
                PageOPSReportGroup.QUATER = (int)cboQuarter.SelectedValue;

                this.loading = false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateUI()
        { 
            this.cboYear.SelectedValue = PageOPSReportGroup.YEAR_ID;
            this.cboQuarter.SelectedValue = PageOPSReportGroup.QUATER;
        }

        private void cboYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!loading)
            {
                PageOPSReportGroup.YEAR_ID = (int)cboYear.SelectedValue;
            }
        }
         
        private void cboQuarter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!loading)
            {
                PageOPSReportGroup.QUATER =(int)cboQuarter.SelectedValue;
            }
        }

        public static DataTable DataYear()
        {
            var dtYears = new DataTable();
            dtYears.Columns.Add("Year", typeof(int));
            dtYears.Columns.Add("Name", typeof(string));
            var y = DateTime.Now.Year;
            while (y > 2008)
            {
                var row = dtYears.NewRow();
                row["Year"] = y;
                row["Name"] = y.ToString();
                dtYears.Rows.Add(row);
                y--;
            }

            return dtYears;

        }

        public static DataTable DataQuarter()
        {
            var dtQuarter = new DataTable();
            dtQuarter.Columns.Add("QUARTER_ID", typeof(int));
            dtQuarter.Columns.Add("QUARTER_NAME", typeof(string));
            dtQuarter.Rows.Add(1, "1");
            dtQuarter.Rows.Add(2, "2");
            dtQuarter.Rows.Add(3, "3");
            dtQuarter.Rows.Add(4, "4");
            return dtQuarter;
        }
    }
}
