﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Helper;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface.OPSReport
{
    public partial class PageOPSR03 : Form
    {
        bool _load = true;

        #region Constructor
        public PageOPSR03()
        {
            InitializeComponent();
            _load = false;
            UIHelper.DataGridViewProperties(dgv);
            d1.Value = new DateTime(d1.Value.Year, d1.Value.Month, 1);
            UIHelper.SetDataSourceToComboBox(cboDistrubtion, Lookup.GetDistribution(), Resources.ALL_DISTRIBUTION);
            loadData();
            _load = true;
        }
        #endregion

        #region Method

        private void loadData()
        {
            var DB = from r in DBDataContext.Db.TBL_OPS_R03s
                     join d in DBDataContext.Db.TLKP_DISTRIBUTIONs on r.DISTRIBUTION_ID equals d.DISTRIBUTION_ID
                     join c in DBDataContext.Db.TBL_CUSTOMERs on r.CUSTOMER_ID equals c.CUSTOMER_ID
                     where r.IS_ACTIVE && r.COMPLAINT_DATE.Date >= d1.Value.Date && r.COMPLAINT_DATE.Date <= d2.Value.Date
                            && (c.CUSTOMER_CODE + " " + c.LAST_NAME_KH + " " + c.FIRST_NAME_KH + " " + r.COMPLAINT_REASON + " " + r.SOLUTION).ToUpper().Contains(txtQuickSearch.Text.ToUpper())
                            && ((int)cboDistrubtion.SelectedValue == 0 || r.DISTRIBUTION_ID == (int)cboDistrubtion.SelectedValue)
                     orderby r.COMPLAINT_DATE, r.MONITORING_DATE
                     select new
                     {
                         r.COMPLAINT_VOLTAGE_ID
                         ,
                         r.COMPLAINT_DATE
                         ,
                         c.CUSTOMER_CODE
                         ,
                         CUSTOMER_NAME = c.LAST_NAME_KH + " " + c.FIRST_NAME_KH
                         ,
                         d.DISTRIBUTION_NAME
                         ,
                         r.COMPLAINT_REASON
                         ,
                         r.MONITORING_DATE
                         ,
                         r.SOLUTION
                         ,
                         r.WORK_DONE_AFTER_COMPLAINT
                     };
            dgv.DataSource = DB;

        }

        #endregion

        private void txt_QuickSearch(object sender, EventArgs e)
        {
            if (_load)
            {
                loadData();
            }
        }

        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void ChangeKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void btnADD_Click(object sender, EventArgs e)
        {
            DateTime dt = DBDataContext.Db.GetSystemDate();
            DialogOPSR03 dia = new DialogOPSR03(GeneralProcess.Insert, new TBL_OPS_R03() { COMPLAINT_DATE = dt, MONITORING_DATE = dt });
            if (dia.ShowDialog() == DialogResult.OK)
            {
                loadData();
                UIHelper.SelectRow(dgv, dia.OPSR03.COMPLAINT_VOLTAGE_ID);
            }
        }

        private void btnEDIT_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                TBL_OPS_R03 ObjOPSR03 = DBDataContext.Db.TBL_OPS_R03s.FirstOrDefault(x => x.COMPLAINT_VOLTAGE_ID == (int)dgv.SelectedRows[0].Cells[COMPLAINT_VOLTAGE_ID.Name].Value);
                DialogOPSR03 dia = new DialogOPSR03(GeneralProcess.Update, ObjOPSR03);
                if (dia.ShowDialog() == DialogResult.OK)
                {
                    loadData();
                    UIHelper.SelectRow(dgv, dia.OPSR03.COMPLAINT_VOLTAGE_ID);
                }
            }
        }

        private void btnREMOVE_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                TBL_OPS_R03 ObjOPSR03 = DBDataContext.Db.TBL_OPS_R03s.FirstOrDefault(x => x.COMPLAINT_VOLTAGE_ID == (int)dgv.SelectedRows[0].Cells[COMPLAINT_VOLTAGE_ID.Name].Value);
                DialogOPSR03 dia = new DialogOPSR03(GeneralProcess.Delete, ObjOPSR03);
                if (dia.ShowDialog() == DialogResult.OK)
                {
                    loadData();
                    UIHelper.SelectRow(dgv, dia.OPSR03.COMPLAINT_VOLTAGE_ID);
                }
            }
        }




    }
}
