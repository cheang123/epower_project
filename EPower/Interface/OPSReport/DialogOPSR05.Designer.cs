﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.OPSReport
{
    partial class DialogOPSR05
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogOPSR05));
            this.lblCUSTOMER_CODE = new System.Windows.Forms.Label();
            this.lblCUSTOMER_NAME = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.lblDEFFECTIVE_DATE = new System.Windows.Forms.Label();
            this.lblREPLACING_DATE = new System.Windows.Forms.Label();
            this.lblACCURACY = new System.Windows.Forms.Label();
            this.dtpDeffectiveDate = new System.Windows.Forms.DateTimePicker();
            this.dtpReplacingDate = new System.Windows.Forms.DateTimePicker();
            this.txtAccuracy = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCustomerCodes = new SoftTech.Component.ExTextbox();
            this.txtCustomerNames = new SoftTech.Component.ExTextbox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.txtCustomerNames);
            this.content.Controls.Add(this.txtCustomerCodes);
            this.content.Controls.Add(this.dtpReplacingDate);
            this.content.Controls.Add(this.dtpDeffectiveDate);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.label6);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.lblACCURACY);
            this.content.Controls.Add(this.lblREPLACING_DATE);
            this.content.Controls.Add(this.lblDEFFECTIVE_DATE);
            this.content.Controls.Add(this.lblCUSTOMER_NAME);
            this.content.Controls.Add(this.txtAccuracy);
            this.content.Controls.Add(this.lblCUSTOMER_CODE);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_CODE, 0);
            this.content.Controls.SetChildIndex(this.txtAccuracy, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblDEFFECTIVE_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblREPLACING_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblACCURACY, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.label6, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.dtpDeffectiveDate, 0);
            this.content.Controls.SetChildIndex(this.dtpReplacingDate, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerCodes, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerNames, 0);
            // 
            // lblCUSTOMER_CODE
            // 
            resources.ApplyResources(this.lblCUSTOMER_CODE, "lblCUSTOMER_CODE");
            this.lblCUSTOMER_CODE.Name = "lblCUSTOMER_CODE";
            // 
            // lblCUSTOMER_NAME
            // 
            resources.ApplyResources(this.lblCUSTOMER_NAME, "lblCUSTOMER_NAME");
            this.lblCUSTOMER_NAME.Name = "lblCUSTOMER_NAME";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // lblDEFFECTIVE_DATE
            // 
            resources.ApplyResources(this.lblDEFFECTIVE_DATE, "lblDEFFECTIVE_DATE");
            this.lblDEFFECTIVE_DATE.Name = "lblDEFFECTIVE_DATE";
            // 
            // lblREPLACING_DATE
            // 
            resources.ApplyResources(this.lblREPLACING_DATE, "lblREPLACING_DATE");
            this.lblREPLACING_DATE.Name = "lblREPLACING_DATE";
            // 
            // lblACCURACY
            // 
            resources.ApplyResources(this.lblACCURACY, "lblACCURACY");
            this.lblACCURACY.Name = "lblACCURACY";
            // 
            // dtpDeffectiveDate
            // 
            resources.ApplyResources(this.dtpDeffectiveDate, "dtpDeffectiveDate");
            this.dtpDeffectiveDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDeffectiveDate.Name = "dtpDeffectiveDate";
            this.dtpDeffectiveDate.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // dtpReplacingDate
            // 
            resources.ApplyResources(this.dtpReplacingDate, "dtpReplacingDate");
            this.dtpReplacingDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpReplacingDate.Name = "dtpReplacingDate";
            this.dtpReplacingDate.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // txtAccuracy
            // 
            resources.ApplyResources(this.txtAccuracy, "txtAccuracy");
            this.txtAccuracy.Name = "txtAccuracy";
            this.txtAccuracy.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Name = "label6";
            // 
            // txtCustomerCodes
            // 
            this.txtCustomerCodes.BackColor = System.Drawing.Color.White;
            this.txtCustomerCodes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtCustomerCodes, "txtCustomerCodes");
            this.txtCustomerCodes.Name = "txtCustomerCodes";
            this.txtCustomerCodes.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtCustomerCodes.AdvanceSearch += new System.EventHandler(this.txtCustomerCodes_AdvanceSearch);
            this.txtCustomerCodes.CancelAdvanceSearch += new System.EventHandler(this.txtCustomerCodes_CancelAdvanceSearch);
            this.txtCustomerCodes.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // txtCustomerNames
            // 
            this.txtCustomerNames.BackColor = System.Drawing.Color.White;
            this.txtCustomerNames.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtCustomerNames, "txtCustomerNames");
            this.txtCustomerNames.Name = "txtCustomerNames";
            this.txtCustomerNames.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtCustomerNames.AdvanceSearch += new System.EventHandler(this.txtCustomerNames_AdvanceSearch);
            this.txtCustomerNames.CancelAdvanceSearch += new System.EventHandler(this.txtCustomerCodes_CancelAdvanceSearch);
            this.txtCustomerNames.Enter += new System.EventHandler(this.ChangeKeyboard);
            // 
            // DialogOPSR05
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogOPSR05";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Label lblCUSTOMER_NAME;
        private Label lblCUSTOMER_CODE;
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label label1;
        private Label label9;
        private ExButton btnCHANGE_LOG;
        private DateTimePicker dtpReplacingDate;
        private DateTimePicker dtpDeffectiveDate;
        private Label label6;
        private Label lblACCURACY;
        private Label lblREPLACING_DATE;
        private Label lblDEFFECTIVE_DATE;
        private TextBox txtAccuracy;
        private ExTextbox txtCustomerNames;
        private ExTextbox txtCustomerCodes;
    }
}