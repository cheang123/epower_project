﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.OPSReport
{
    partial class DialogOPSR01
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogOPSR01));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.lblOUTAGES_DATE = new System.Windows.Forms.Label();
            this.lblAFFECTED_AREAS = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblOUTAGES_END = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cboDISTRIBUTION = new System.Windows.Forms.ComboBox();
            this.lblDISTRIBUTION = new System.Windows.Forms.Label();
            this.lblOUTAGES_BEGIN = new System.Windows.Forms.Label();
            this.dtpOUTAGES_DATE = new System.Windows.Forms.DateTimePicker();
            this.dtpOUTAGES_BEGIN = new System.Windows.Forms.DateTimePicker();
            this.dtpOUTAGES_END = new System.Windows.Forms.DateTimePicker();
            this.txtOUTAGES_DURATION = new System.Windows.Forms.TextBox();
            this.txtJOB_DESCRIPTION = new System.Windows.Forms.TextBox();
            this.txtAFFECTED_AREAS = new System.Windows.Forms.TextBox();
            this.txtAFFECTED_CONSUMERS = new System.Windows.Forms.TextBox();
            this.lblAFFECTED_CONSUMERS = new System.Windows.Forms.Label();
            this.lblOUTAGES_DURATION = new System.Windows.Forms.Label();
            this.lblJOB_DESCRIPTION = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblNOTIFICATION_WAYS = new System.Windows.Forms.Label();
            this.txtNOTIFICATION_WAYS = new System.Windows.Forms.TextBox();
            this.btnSELECT_AREA1 = new SoftTech.Component.ExButton();
            this.pbsJobDescription = new System.Windows.Forms.PictureBox();
            this.pbsNotificationWay = new System.Windows.Forms.PictureBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsJobDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsNotificationWay)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.pbsNotificationWay);
            this.content.Controls.Add(this.pbsJobDescription);
            this.content.Controls.Add(this.btnSELECT_AREA1);
            this.content.Controls.Add(this.label2);
            this.content.Controls.Add(this.lblNOTIFICATION_WAYS);
            this.content.Controls.Add(this.txtNOTIFICATION_WAYS);
            this.content.Controls.Add(this.label11);
            this.content.Controls.Add(this.label10);
            this.content.Controls.Add(this.label7);
            this.content.Controls.Add(this.lblJOB_DESCRIPTION);
            this.content.Controls.Add(this.lblOUTAGES_DURATION);
            this.content.Controls.Add(this.lblAFFECTED_CONSUMERS);
            this.content.Controls.Add(this.txtAFFECTED_CONSUMERS);
            this.content.Controls.Add(this.txtAFFECTED_AREAS);
            this.content.Controls.Add(this.txtJOB_DESCRIPTION);
            this.content.Controls.Add(this.txtOUTAGES_DURATION);
            this.content.Controls.Add(this.dtpOUTAGES_END);
            this.content.Controls.Add(this.dtpOUTAGES_BEGIN);
            this.content.Controls.Add(this.dtpOUTAGES_DATE);
            this.content.Controls.Add(this.lblOUTAGES_BEGIN);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.cboDISTRIBUTION);
            this.content.Controls.Add(this.lblDISTRIBUTION);
            this.content.Controls.Add(this.lblOUTAGES_END);
            this.content.Controls.Add(this.label5);
            this.content.Controls.Add(this.lblAFFECTED_AREAS);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.lblOUTAGES_DATE);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.lblOUTAGES_DATE, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.lblAFFECTED_AREAS, 0);
            this.content.Controls.SetChildIndex(this.label5, 0);
            this.content.Controls.SetChildIndex(this.lblOUTAGES_END, 0);
            this.content.Controls.SetChildIndex(this.lblDISTRIBUTION, 0);
            this.content.Controls.SetChildIndex(this.cboDISTRIBUTION, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.lblOUTAGES_BEGIN, 0);
            this.content.Controls.SetChildIndex(this.dtpOUTAGES_DATE, 0);
            this.content.Controls.SetChildIndex(this.dtpOUTAGES_BEGIN, 0);
            this.content.Controls.SetChildIndex(this.dtpOUTAGES_END, 0);
            this.content.Controls.SetChildIndex(this.txtOUTAGES_DURATION, 0);
            this.content.Controls.SetChildIndex(this.txtJOB_DESCRIPTION, 0);
            this.content.Controls.SetChildIndex(this.txtAFFECTED_AREAS, 0);
            this.content.Controls.SetChildIndex(this.txtAFFECTED_CONSUMERS, 0);
            this.content.Controls.SetChildIndex(this.lblAFFECTED_CONSUMERS, 0);
            this.content.Controls.SetChildIndex(this.lblOUTAGES_DURATION, 0);
            this.content.Controls.SetChildIndex(this.lblJOB_DESCRIPTION, 0);
            this.content.Controls.SetChildIndex(this.label7, 0);
            this.content.Controls.SetChildIndex(this.label10, 0);
            this.content.Controls.SetChildIndex(this.label11, 0);
            this.content.Controls.SetChildIndex(this.txtNOTIFICATION_WAYS, 0);
            this.content.Controls.SetChildIndex(this.lblNOTIFICATION_WAYS, 0);
            this.content.Controls.SetChildIndex(this.label2, 0);
            this.content.Controls.SetChildIndex(this.btnSELECT_AREA1, 0);
            this.content.Controls.SetChildIndex(this.pbsJobDescription, 0);
            this.content.Controls.SetChildIndex(this.pbsNotificationWay, 0);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnCLOSE_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnCHANGE_LOG_Click);
            // 
            // lblOUTAGES_DATE
            // 
            resources.ApplyResources(this.lblOUTAGES_DATE, "lblOUTAGES_DATE");
            this.lblOUTAGES_DATE.Name = "lblOUTAGES_DATE";
            // 
            // lblAFFECTED_AREAS
            // 
            resources.ApplyResources(this.lblAFFECTED_AREAS, "lblAFFECTED_AREAS");
            this.lblAFFECTED_AREAS.Name = "lblAFFECTED_AREAS";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Name = "label5";
            // 
            // lblOUTAGES_END
            // 
            resources.ApplyResources(this.lblOUTAGES_END, "lblOUTAGES_END");
            this.lblOUTAGES_END.Name = "lblOUTAGES_END";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // cboDISTRIBUTION
            // 
            this.cboDISTRIBUTION.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDISTRIBUTION.FormattingEnabled = true;
            resources.ApplyResources(this.cboDISTRIBUTION, "cboDISTRIBUTION");
            this.cboDISTRIBUTION.Name = "cboDISTRIBUTION";
            // 
            // lblDISTRIBUTION
            // 
            resources.ApplyResources(this.lblDISTRIBUTION, "lblDISTRIBUTION");
            this.lblDISTRIBUTION.Name = "lblDISTRIBUTION";
            // 
            // lblOUTAGES_BEGIN
            // 
            resources.ApplyResources(this.lblOUTAGES_BEGIN, "lblOUTAGES_BEGIN");
            this.lblOUTAGES_BEGIN.Name = "lblOUTAGES_BEGIN";
            // 
            // dtpOUTAGES_DATE
            // 
            resources.ApplyResources(this.dtpOUTAGES_DATE, "dtpOUTAGES_DATE");
            this.dtpOUTAGES_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpOUTAGES_DATE.Name = "dtpOUTAGES_DATE";
            this.dtpOUTAGES_DATE.ValueChanged += new System.EventHandler(this.dtpOUTAGES_DATE_ValueChanged);
            this.dtpOUTAGES_DATE.Enter += new System.EventHandler(this.ChangeImeEN);
            // 
            // dtpOUTAGES_BEGIN
            // 
            resources.ApplyResources(this.dtpOUTAGES_BEGIN, "dtpOUTAGES_BEGIN");
            this.dtpOUTAGES_BEGIN.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpOUTAGES_BEGIN.Name = "dtpOUTAGES_BEGIN";
            this.dtpOUTAGES_BEGIN.Enter += new System.EventHandler(this.ChangeImeEN);
            // 
            // dtpOUTAGES_END
            // 
            resources.ApplyResources(this.dtpOUTAGES_END, "dtpOUTAGES_END");
            this.dtpOUTAGES_END.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpOUTAGES_END.Name = "dtpOUTAGES_END";
            this.dtpOUTAGES_END.Enter += new System.EventHandler(this.ChangeImeEN);
            // 
            // txtOUTAGES_DURATION
            // 
            this.txtOUTAGES_DURATION.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.txtOUTAGES_DURATION, "txtOUTAGES_DURATION");
            this.txtOUTAGES_DURATION.Name = "txtOUTAGES_DURATION";
            this.txtOUTAGES_DURATION.ReadOnly = true;
            this.txtOUTAGES_DURATION.Enter += new System.EventHandler(this.ChangeImeEN);
            // 
            // txtJOB_DESCRIPTION
            // 
            resources.ApplyResources(this.txtJOB_DESCRIPTION, "txtJOB_DESCRIPTION");
            this.txtJOB_DESCRIPTION.Name = "txtJOB_DESCRIPTION";
            this.txtJOB_DESCRIPTION.Enter += new System.EventHandler(this.ChangeImeKH);
            // 
            // txtAFFECTED_AREAS
            // 
            this.txtAFFECTED_AREAS.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.txtAFFECTED_AREAS, "txtAFFECTED_AREAS");
            this.txtAFFECTED_AREAS.Name = "txtAFFECTED_AREAS";
            this.txtAFFECTED_AREAS.ReadOnly = true;
            // 
            // txtAFFECTED_CONSUMERS
            // 
            resources.ApplyResources(this.txtAFFECTED_CONSUMERS, "txtAFFECTED_CONSUMERS");
            this.txtAFFECTED_CONSUMERS.Name = "txtAFFECTED_CONSUMERS";
            this.txtAFFECTED_CONSUMERS.Enter += new System.EventHandler(this.ChangeImeEN);
            // 
            // lblAFFECTED_CONSUMERS
            // 
            resources.ApplyResources(this.lblAFFECTED_CONSUMERS, "lblAFFECTED_CONSUMERS");
            this.lblAFFECTED_CONSUMERS.Name = "lblAFFECTED_CONSUMERS";
            // 
            // lblOUTAGES_DURATION
            // 
            resources.ApplyResources(this.lblOUTAGES_DURATION, "lblOUTAGES_DURATION");
            this.lblOUTAGES_DURATION.Name = "lblOUTAGES_DURATION";
            // 
            // lblJOB_DESCRIPTION
            // 
            resources.ApplyResources(this.lblJOB_DESCRIPTION, "lblJOB_DESCRIPTION");
            this.lblJOB_DESCRIPTION.Name = "lblJOB_DESCRIPTION";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Name = "label7";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Name = "label10";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Name = "label11";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Name = "label2";
            // 
            // lblNOTIFICATION_WAYS
            // 
            resources.ApplyResources(this.lblNOTIFICATION_WAYS, "lblNOTIFICATION_WAYS");
            this.lblNOTIFICATION_WAYS.Name = "lblNOTIFICATION_WAYS";
            // 
            // txtNOTIFICATION_WAYS
            // 
            resources.ApplyResources(this.txtNOTIFICATION_WAYS, "txtNOTIFICATION_WAYS");
            this.txtNOTIFICATION_WAYS.Name = "txtNOTIFICATION_WAYS";
            this.txtNOTIFICATION_WAYS.Enter += new System.EventHandler(this.ChangeImeKH);
            // 
            // btnSELECT_AREA1
            // 
            this.btnSELECT_AREA1.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.btnSELECT_AREA1, "btnSELECT_AREA1");
            this.btnSELECT_AREA1.Name = "btnSELECT_AREA1";
            this.btnSELECT_AREA1.UseVisualStyleBackColor = true;
            this.btnSELECT_AREA1.Click += new System.EventHandler(this.btnSELECT_AREA1_Click);
            // 
            // pbsJobDescription
            // 
            this.pbsJobDescription.Image = global::EPower.Properties.Resources.question_mark;
            resources.ApplyResources(this.pbsJobDescription, "pbsJobDescription");
            this.pbsJobDescription.Name = "pbsJobDescription";
            this.pbsJobDescription.TabStop = false;
            this.pbsJobDescription.Click += new System.EventHandler(this.pbsJobDescription_Click);
            // 
            // pbsNotificationWay
            // 
            this.pbsNotificationWay.Image = global::EPower.Properties.Resources.question_mark;
            resources.ApplyResources(this.pbsNotificationWay, "pbsNotificationWay");
            this.pbsNotificationWay.Name = "pbsNotificationWay";
            this.pbsNotificationWay.TabStop = false;
            this.pbsNotificationWay.Click += new System.EventHandler(this.pbsNotificationWay_Click);
            // 
            // DialogOPSR01
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogOPSR01";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsJobDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsNotificationWay)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private ExButton btnCHANGE_LOG;
        private Label lblOUTAGES_DATE;
        private Label label5;
        private Label lblAFFECTED_AREAS;
        private Label lblOUTAGES_END;
        private Label label1;
        private ComboBox cboDISTRIBUTION;
        private Label lblDISTRIBUTION;
        private Label lblOUTAGES_BEGIN;
        private DateTimePicker dtpOUTAGES_DATE;
        private Label lblJOB_DESCRIPTION;
        private Label lblOUTAGES_DURATION;
        private Label lblAFFECTED_CONSUMERS;
        private TextBox txtAFFECTED_CONSUMERS;
        private TextBox txtAFFECTED_AREAS;
        private TextBox txtJOB_DESCRIPTION;
        private TextBox txtOUTAGES_DURATION;
        private DateTimePicker dtpOUTAGES_END;
        private DateTimePicker dtpOUTAGES_BEGIN;
        private Label label11;
        private Label label10;
        private Label label7;
        private Label label2;
        private Label lblNOTIFICATION_WAYS;
        private TextBox txtNOTIFICATION_WAYS;
        private ExButton btnSELECT_AREA1;
        private PictureBox pbsNotificationWay;
        private PictureBox pbsJobDescription;
    }
}