﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using EPower.Properties;
using SoftTech.Security.Interface;
using System.Transactions;

namespace EPower.Interface.OPSReport
{
    public partial class DialogOPSSuggestion : ExDialog
    {
        public string suggestionName = ""; 

        #region Constructor
        public DialogOPSSuggestion(string tableName,string columnName)
        {
            InitializeComponent();
            dgv.DataSource = DBDataContext.Db.TBL_OPS_SUGGESTIONs.Where(x => x.TABLE_NAME == tableName && x.COLUMN_NAME == columnName && x.IS_ACTIVE);
        }        
        
        #endregion

        private void btnOK_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow item in dgv.SelectedRows)
            {
                suggestionName = suggestionName + item.Cells[SUGGESTION.Name].Value.ToString()+" ";
            }
            this.DialogResult = DialogResult.OK;
        }
         
        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            btnOK_Click(null, null);
        }

           
    }
}