﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface.OPSReport
{
    public partial class PageOPSR02 : Form
    {
        

        #region Constructor
        public PageOPSR02()
        {
            InitializeComponent();
            
            UIHelper.DataGridViewProperties(dgv);
            /*
             * Default value and format
             */
            var date = DBDataContext.Db.GetSystemDate();
            dtpD1.Value = new DateTime(date.Year, date.Month, 1);
            dtpD2.Value = dtpD1.Value.AddMonths(1).AddSeconds(-1);
            dtpD1.CustomFormat = dtpD2.CustomFormat = UIHelper._DefaultDateFormat;
            dtpD1.Format = dtpD2.Format = DateTimePickerFormat.Custom;
            /*
             * Cell DataGridView
             */
            DATE.DefaultCellStyle.Format = UIHelper._DefaultDateFormat;
            BLACKOUT_BEGIN.DefaultCellStyle.Format = UIHelper._DefaultShortDateTimeNoSecond;
            BLACKOUT_END.DefaultCellStyle.Format = UIHelper._DefaultShortDateTimeNoSecond;
            BLACKOUT_DURATION.DefaultCellStyle.Format = UIHelper._DefaultCustomNumber;

            
            /*
             * Binding data
             */
            dtpD1.Value = new DateTime(dtpD1.Value.Year, dtpD1.Value.Month,1 );
            UIHelper.SetDataSourceToComboBox(cboDistribution, Lookup.GetDistribution(),Resources.ALL_DISTRIBUTION);


            /*
             * Set event UI
             */
            cboDistribution.SelectedIndexChanged += cboDistribution_SelectedIndexChanged;
            loadData();
            
        }

        private void cboDistribution_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadData();
        }
        #endregion

        #region Method

        private void loadData()
        {
            if (cboDistribution.SelectedIndex == -1)
            {
                return;
            }
            int distributionId = (int)cboDistribution.SelectedValue;
            var DB = from r in DBDataContext.Db.TBL_OPS_R02s
                     join d in DBDataContext.Db.TLKP_DISTRIBUTIONs on r.DISTRIBUTION_ID equals d.DISTRIBUTION_ID
                     where r.IS_ACTIVE && r.BLACKOUT_DATE >= dtpD1.Value.Date && r.BLACKOUT_DATE <= dtpD2.Value.Date
                            && r.BLACKOUT_REASON.ToUpper().Contains(txtQuickSearch.Text)
                            && (distributionId == 0 || r.DISTRIBUTION_ID == distributionId)
                     orderby r.BLACKOUT_DATE
                     select new
                     {
                         r.BLACKOUT_ID,
                         r.BLACKOUT_DATE,
                         r.BLACKOUT_REASON,
                         d.DISTRIBUTION_NAME,
                         r.BLACKOUT_BEGIN,
                         r.BLACKOUT_END,
                         BLACKOUT_DURATION = r.BLACKOUT_END.Subtract(r.BLACKOUT_BEGIN).TotalHours,
                         AFFECTED_AREAS = Method.ConcatVillageName(Method.villageByCode(r.AFFECTED_AREAS)),
                         r.AFFECTED_CONSUMERS
                     };
            dgv.DataSource = DB;
        }

        #endregion

        private void txt_QuickSearch(object sender, EventArgs e)
        {
            loadData();
        } 
         
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void btnADD_Click(object sender, EventArgs e)
        {
            DateTime dt = DBDataContext.Db.GetSystemDate();
            DialogOPSR02 diag = new DialogOPSR02(GeneralProcess.Insert, new TBL_OPS_R02() { BLACKOUT_BEGIN = dt, BLACKOUT_DATE = dt, BLACKOUT_END = dt,AFFECTED_AREAS="" });
            if (diag.ShowDialog() == DialogResult.OK)
            {
                loadData();
                UIHelper.SelectRow(dgv, diag.OPSR02.BLACKOUT_ID);
            }
        }

        private void btnEDIT_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                TBL_OPS_R02 objOPSR02 = DBDataContext.Db.TBL_OPS_R02s.FirstOrDefault(x => x.BLACKOUT_ID == (int)dgv.SelectedRows[0].Cells[BLACKOUT_ID.Name].Value);
                DialogOPSR02 diag = new DialogOPSR02(GeneralProcess.Update, objOPSR02);
                if(diag.ShowDialog()==DialogResult.OK)
                {
                    loadData();
                    UIHelper.SelectRow(dgv, diag.OPSR02.BLACKOUT_ID);
                }
            }
        }

        private void btnREMOVE_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                TBL_OPS_R02 objOPSR02 = DBDataContext.Db.TBL_OPS_R02s.FirstOrDefault(x => x.BLACKOUT_ID == (int)dgv.SelectedRows[0].Cells[BLACKOUT_ID.Name].Value);
                DialogOPSR02 diag = new DialogOPSR02(GeneralProcess.Delete, objOPSR02);
                if (diag.ShowDialog() == DialogResult.OK)
                {
                    loadData();
                    UIHelper.SelectRow(dgv, diag.OPSR02.BLACKOUT_ID);
                }
            }
        }

        private void dtpD1_ValueChanged(object sender, EventArgs e)
        {
            loadData();
        }


        
         
    }
}
