﻿using EPower.Base.Logic;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface.OPSReport
{
    public partial class PageOPSReportViewer : Form
    {
        private string tableCode = "R01";
        public PageOPSReportViewer(string tableCode)
        {
            InitializeComponent();
            viewer.DefaultView();

            this.FormBorderStyle = FormBorderStyle.None;
            this.Dock = DockStyle.Fill;
            

            this.tableCode = tableCode;
            var table = DBDataContext.Db.TBL_OPS_TABLEs.FirstOrDefault(x => x.TABLE_CODE ==  tableCode);
            this.lblBUY_POWER_DETAIL_.Text = table.TABLE_CODE + " " + table.TABLE_NAME;


            ResourceHelper.ApplyResource(this);
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                var cr = new CrystalReportHelper();
                var reportName = string.Format("{0}.rpt", tableCode);
                cr.OpenReport(reportName);
                cr.SetParameter("@YEAR_ID", PageOPSReportGroup.YEAR_ID);
                cr.SetParameter("@QUARTER", PageOPSReportGroup.QUATER); 

                this.viewer.ReportSource = cr.Report;
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

    }
}
