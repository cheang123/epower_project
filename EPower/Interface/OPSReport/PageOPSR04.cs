﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using EPower.Properties;

namespace EPower.Interface.OPSReport
{
    public partial class PageOPSR04 : Form
    {
        bool _load = true;

        #region Constructor
        public PageOPSR04()
        {
            InitializeComponent();
            _load = false;
            UIHelper.DataGridViewProperties(dgv);
            d1.Value = new DateTime(d1.Value.Year, d1.Value.Month,1 ); 
            loadData();
            _load = true;
        }
        #endregion

        #region Method

        private void loadData()
        {
            var DB = from r in DBDataContext.Db.TBL_OPS_R04s
                     join c in DBDataContext.Db.TBL_CUSTOMERs on r.CUSTOMER_ID equals c.CUSTOMER_ID
                     where r.IS_ACTIVE
                            && (r.COMPLAINT_DATE.Date >= d1.Value.Date && r.COMPLAINT_DATE.Date <= d2.Value.Date)
                            && (r.COMPLAINT_REASON + " " + r.SOLUTION + " " + c.CUSTOMER_CODE + " " + c.LAST_NAME_KH + " " + c.FIRST_NAME_KH).ToUpper().Contains(txtQuickSearch.Text.ToUpper()) 
                     orderby r.COMPLAINT_DATE
                     select new
                     {
                         r.COMPLAINT_METER_READING_ID
                         ,r.COMPLAINT_DATE
                         ,c.CUSTOMER_CODE
                         ,CUSTOMER_NAME=c.LAST_NAME_KH+" "+c.FIRST_NAME_KH
                         ,r.COMPLAINT_REASON
                         ,r.NOTIFICATION_DATE
                         ,r.SOLUTION
                     };
            dgv.DataSource = DB;
                        
        }

        #endregion

        private void txt_QuickSearch(object sender, EventArgs e)
        {
            if (_load)
            {
                loadData();
            }
        } 
         
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void ChangeKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        } 

        private void btnADD_Click(object sender, EventArgs e)
        {
            DateTime dt=DBDataContext.Db.GetSystemDate();
            DialogOPSR04 dia = new DialogOPSR04(GeneralProcess.Insert, new TBL_OPS_R04() { COMPLAINT_DATE = dt, NOTIFICATION_DATE=dt });
            if (dia.ShowDialog() == DialogResult.OK)
            {
                loadData();
                UIHelper.SelectRow(dgv, dia.OPSR04.COMPLAINT_METER_READING_ID);
            }
        }

        private void btnEDIT_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                TBL_OPS_R04 ObjOPSR04 = DBDataContext.Db.TBL_OPS_R04s.FirstOrDefault(x => x.COMPLAINT_METER_READING_ID == (int)dgv.SelectedRows[0].Cells[COMPLAINT_METER_READING_ID.Name].Value);
                DialogOPSR04 dia = new DialogOPSR04(GeneralProcess.Update, ObjOPSR04);
                if (dia.ShowDialog() == DialogResult.OK)
                {
                    loadData();
                    UIHelper.SelectRow(dgv, dia.OPSR04.COMPLAINT_METER_READING_ID);
                }
            }
        }

        private void btnREMOVE_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                TBL_OPS_R04 ObjOPSR04 = DBDataContext.Db.TBL_OPS_R04s.FirstOrDefault(x => x.COMPLAINT_METER_READING_ID == (int)dgv.SelectedRows[0].Cells[COMPLAINT_METER_READING_ID.Name].Value);
                DialogOPSR04 dia = new DialogOPSR04(GeneralProcess.Delete, ObjOPSR04);
                if (dia.ShowDialog() == DialogResult.OK)
                {
                    loadData();
                    UIHelper.SelectRow(dgv, dia.OPSR04.COMPLAINT_METER_READING_ID);
                }
            }
        }



        
         
    }
}
