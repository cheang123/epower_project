﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface.OPSReport
{
    public partial class DialogOPSR01 : ExDialog
    {
        private GeneralProcess _flag;
        private TBL_OPS_R01 _objR01 = new TBL_OPS_R01();
        private TBL_OPS_R01 _objOldR01 = new TBL_OPS_R01();
        public TBL_OPS_R01 OPSR01
        {
            get { return _objR01; }
        }

        public DialogOPSR01(GeneralProcess flag, TBL_OPS_R01 objR01)
        {
            InitializeComponent();
            this.SetTextDialog(flag);

            _flag = flag;

            objR01._CopyTo(_objR01);
            objR01._CopyTo(_objOldR01);
            var enable = flag != GeneralProcess.Delete;
            UIHelper.SetEnabled(this, enable);
            btnSELECT_AREA1.Enabled = enable;

            /*
             * Default value and format.
             */
            dtpOUTAGES_DATE.CustomFormat = UIHelper._DefaultDateFormat;
            dtpOUTAGES_BEGIN.CustomFormat = dtpOUTAGES_END.CustomFormat = UIHelper._DefaultShortDateTimeNoSecond;
            /*
             * binding data
             */
            UIHelper.SetDataSourceToComboBox(cboDISTRIBUTION, Lookup.GetDistribution());
            /*
             * load data
             */
            read();

            /*
             * Set event UI
             */
            dtpOUTAGES_BEGIN.ValueChanged += dtpOUTAGES_DURATION_ValueChanged;
            dtpOUTAGES_END.ValueChanged += dtpOUTAGES_DURATION_ValueChanged;
        }

        void dtpOUTAGES_DURATION_ValueChanged(object sender, System.EventArgs e)
        {
            var duration = dtpOUTAGES_END.Value.Subtract(dtpOUTAGES_BEGIN.Value).TotalHours;
            txtOUTAGES_DURATION.Text = duration.ToString(UIHelper._DefaultUsageFormat);
        }

        private void read()
        {
            cboDISTRIBUTION.SelectedValue = _objR01.DISTRIBUTION_ID;
            dtpOUTAGES_DATE.Value = _objR01.OUTAGES_DATE;
            dtpOUTAGES_BEGIN.Value = _objR01.OUTAGES_BEGIN;
            dtpOUTAGES_END.Value = _objR01.OUTAGES_END;
            txtOUTAGES_DURATION.Text =
                _objR01.OUTAGES_END.Subtract(_objR01.OUTAGES_BEGIN).TotalHours.ToString(UIHelper._DefaultUsageFormat);
            txtJOB_DESCRIPTION.Text = _objR01.JOB_DESCRIPTION;
            var area = Method.villageByCode(_objR01.AFFECTED_AREAS);
            txtAFFECTED_AREAS.Text = Method.ConcatVillageName(area);
            txtAFFECTED_CONSUMERS.Text = _objR01.AFFECTED_CONSUMERS.ToString();
            txtNOTIFICATION_WAYS.Text = _objR01.NOTIFICATION_WAYS;
        }

        private bool inValid()
        {
            var r = false;
            this.ClearAllValidation();
            if (cboDISTRIBUTION.SelectedIndex == -1)
            {
                cboDISTRIBUTION.SetValidation(string.Format(Resources.REQUIRED_SELECT_VALUE, lblDISTRIBUTION.Text));
                r = true;
            }
            if (!DataHelper.IsDecimal(txtOUTAGES_DURATION.Text))
            {
                txtOUTAGES_DURATION.SetValidation(string.Format(Resources.REQUIRED, lblOUTAGES_DURATION.Text));
                r = true;
            }
            if (string.IsNullOrEmpty(txtJOB_DESCRIPTION.Text.Trim()))
            {
                txtJOB_DESCRIPTION.SetValidation(string.Format(Resources.REQUIRED, lblJOB_DESCRIPTION.Text));
                r = true;
            }
            if (string.IsNullOrEmpty(txtNOTIFICATION_WAYS.Text.Trim()))
            {
                txtNOTIFICATION_WAYS.SetValidation(string.Format(Resources.REQUIRED, lblNOTIFICATION_WAYS.Text));
                r = true;
            }
            if (string.IsNullOrEmpty(_objR01.AFFECTED_AREAS))
            {
                txtAFFECTED_AREAS.SetValidation(string.Format(Resources.REQUIRED_SELECT_VALUE, lblAFFECTED_AREAS.Text));
                r = true;
            }
            if (!DataHelper.IsNumber(txtAFFECTED_CONSUMERS.Text))
            {
                txtAFFECTED_CONSUMERS.SetValidation(string.Format(Resources.REQUIRED, lblAFFECTED_CONSUMERS.Text));
                r = true;
            }
            if (dtpOUTAGES_END.Value < dtpOUTAGES_BEGIN.Value)
            {
                dtpOUTAGES_END.SetValidation(string.Format(Resources.REQUIRED_LOOKUP, lblOUTAGES_END.Text));
                r = true;
            }

            return r;
        }

        private void write()
        {
            _objR01.DISTRIBUTION_ID = (int)cboDISTRIBUTION.SelectedValue;
            _objR01.OUTAGES_DATE = dtpOUTAGES_DATE.Value;
            _objR01.OUTAGES_BEGIN = dtpOUTAGES_BEGIN.Value;
            _objR01.OUTAGES_END = dtpOUTAGES_END.Value;
            _objR01.OUTAGES_DURATION = (decimal)_objR01.OUTAGES_END.Subtract(_objR01.OUTAGES_BEGIN).TotalMinutes;
            _objR01.JOB_DESCRIPTION = txtJOB_DESCRIPTION.Text;
            _objR01.AFFECTED_CONSUMERS = DataHelper.ParseToInt(txtAFFECTED_CONSUMERS.Text);
            _objR01.NOTIFICATION_WAYS = txtNOTIFICATION_WAYS.Text;
        }

        private void btnOK_Click(object sender, System.EventArgs e)
        {
            try
            {
                if (inValid())
                {
                    return;
                }
                write();
                using (var tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objR01);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOldR01, _objR01);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objR01);
                    }
                    tran.Complete();
                }
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp);
            }
            DialogResult = DialogResult.OK;
        }

        private void btnCLOSE_Click(object sender, System.EventArgs e)
        {
            DialogResult = DialogResult.No;
        }

        private void btnCHANGE_LOG_Click(object sender, System.EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(_objR01);
        }

        private void btnSELECT_AREA1_Click(object sender, System.EventArgs e)
        {
            var areas = Method.villageByCode(_objR01.AFFECTED_AREAS);
            var dig = new DialogOPSSelectAffectedAreas(areas.ToList(), _flag);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                _objR01.AFFECTED_AREAS = Method.ConcatVillageCode(dig.SelectedAreas);
                txtAFFECTED_AREAS.Text = Method.ConcatVillageName(dig.SelectedAreas);
                txtAFFECTED_CONSUMERS.Text = dig.TotalCustomers.ToString();
            }
        }

        private void ChangeImeKH(object sender, System.EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        private void ChangeImeEN(object sender, System.EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void pbsJobDescription_Click(object sender, EventArgs e)
        {
            DialogOPSSuggestion diag = new DialogOPSSuggestion("TBL_OPS_R01", "JOB_DESCRIPTION");
            if (diag.ShowDialog() == DialogResult.OK)
            {
                txtJOB_DESCRIPTION.Text = txtJOB_DESCRIPTION.Text + diag.suggestionName;
                txtJOB_DESCRIPTION.Focus();
            }
        }

        private void pbsNotificationWay_Click(object sender, EventArgs e)
        {
            DialogOPSSuggestion diag = new DialogOPSSuggestion("TBL_OPS_R01", "NOTIFICATION_WAYS");
            if (diag.ShowDialog() == DialogResult.OK)
            {
                txtNOTIFICATION_WAYS.Text = txtNOTIFICATION_WAYS.Text + diag.suggestionName;
                txtNOTIFICATION_WAYS.Focus();
            }
        }

        private void dtpOUTAGES_DATE_ValueChanged(object sender, EventArgs e)
        {
            dtpOUTAGES_BEGIN.Value =
            dtpOUTAGES_END.Value = dtpOUTAGES_DATE.Value;
        }
    }
}