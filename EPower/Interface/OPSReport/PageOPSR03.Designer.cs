﻿namespace EPower.Interface.OPSReport
{
    partial class PageOPSR03
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageOPSR03));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.COMPLAINT_VOLTAGE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COMPLAINT_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DISTRIBUTION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COMPLAINT_REASON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MONITORING_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SOLUTION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WORK_DONE_AFTER_COMPLAINT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cboDistrubtion = new System.Windows.Forms.ComboBox();
            this.d2 = new System.Windows.Forms.DateTimePicker();
            this.d1 = new System.Windows.Forms.DateTimePicker();
            this.btnREMOVE = new SoftTech.Component.ExButton();
            this.btnADD = new SoftTech.Component.ExButton();
            this.btnEDIT = new SoftTech.Component.ExButton();
            this.txtQuickSearch = new SoftTech.Component.ExTextbox();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.COMPLAINT_VOLTAGE_ID,
            this.COMPLAINT_DATE,
            this.CUSTOMER_CODE,
            this.CUSTOMER_NAME,
            this.DISTRIBUTION,
            this.COMPLAINT_REASON,
            this.MONITORING_DATE,
            this.SOLUTION,
            this.WORK_DONE_AFTER_COMPLAINT});
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // COMPLAINT_VOLTAGE_ID
            // 
            this.COMPLAINT_VOLTAGE_ID.DataPropertyName = "COMPLAINT_VOLTAGE_ID";
            resources.ApplyResources(this.COMPLAINT_VOLTAGE_ID, "COMPLAINT_VOLTAGE_ID");
            this.COMPLAINT_VOLTAGE_ID.Name = "COMPLAINT_VOLTAGE_ID";
            this.COMPLAINT_VOLTAGE_ID.ReadOnly = true;
            // 
            // COMPLAINT_DATE
            // 
            this.COMPLAINT_DATE.DataPropertyName = "COMPLAINT_DATE";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Format = "dd-MM-yyyy";
            dataGridViewCellStyle2.NullValue = null;
            this.COMPLAINT_DATE.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.COMPLAINT_DATE, "COMPLAINT_DATE");
            this.COMPLAINT_DATE.Name = "COMPLAINT_DATE";
            this.COMPLAINT_DATE.ReadOnly = true;
            // 
            // CUSTOMER_CODE
            // 
            this.CUSTOMER_CODE.DataPropertyName = "CUSTOMER_CODE";
            resources.ApplyResources(this.CUSTOMER_CODE, "CUSTOMER_CODE");
            this.CUSTOMER_CODE.Name = "CUSTOMER_CODE";
            this.CUSTOMER_CODE.ReadOnly = true;
            // 
            // CUSTOMER_NAME
            // 
            this.CUSTOMER_NAME.DataPropertyName = "CUSTOMER_NAME";
            resources.ApplyResources(this.CUSTOMER_NAME, "CUSTOMER_NAME");
            this.CUSTOMER_NAME.Name = "CUSTOMER_NAME";
            this.CUSTOMER_NAME.ReadOnly = true;
            // 
            // DISTRIBUTION
            // 
            this.DISTRIBUTION.DataPropertyName = "DISTRIBUTION_NAME";
            resources.ApplyResources(this.DISTRIBUTION, "DISTRIBUTION");
            this.DISTRIBUTION.Name = "DISTRIBUTION";
            this.DISTRIBUTION.ReadOnly = true;
            // 
            // COMPLAINT_REASON
            // 
            this.COMPLAINT_REASON.DataPropertyName = "COMPLAINT_REASON";
            resources.ApplyResources(this.COMPLAINT_REASON, "COMPLAINT_REASON");
            this.COMPLAINT_REASON.Name = "COMPLAINT_REASON";
            this.COMPLAINT_REASON.ReadOnly = true;
            // 
            // MONITORING_DATE
            // 
            this.MONITORING_DATE.DataPropertyName = "MONITORING_DATE";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.Format = "dd-MM-yyyy";
            this.MONITORING_DATE.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.MONITORING_DATE, "MONITORING_DATE");
            this.MONITORING_DATE.Name = "MONITORING_DATE";
            this.MONITORING_DATE.ReadOnly = true;
            // 
            // SOLUTION
            // 
            this.SOLUTION.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.SOLUTION.DataPropertyName = "SOLUTION";
            resources.ApplyResources(this.SOLUTION, "SOLUTION");
            this.SOLUTION.Name = "SOLUTION";
            this.SOLUTION.ReadOnly = true;
            // 
            // WORK_DONE_AFTER_COMPLAINT
            // 
            this.WORK_DONE_AFTER_COMPLAINT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.WORK_DONE_AFTER_COMPLAINT.DataPropertyName = "WORK_DONE_AFTER_COMPLAINT";
            resources.ApplyResources(this.WORK_DONE_AFTER_COMPLAINT, "WORK_DONE_AFTER_COMPLAINT");
            this.WORK_DONE_AFTER_COMPLAINT.Name = "WORK_DONE_AFTER_COMPLAINT";
            this.WORK_DONE_AFTER_COMPLAINT.ReadOnly = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.cboDistrubtion);
            this.panel1.Controls.Add(this.d2);
            this.panel1.Controls.Add(this.d1);
            this.panel1.Controls.Add(this.btnREMOVE);
            this.panel1.Controls.Add(this.btnADD);
            this.panel1.Controls.Add(this.btnEDIT);
            this.panel1.Controls.Add(this.txtQuickSearch);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // cboDistrubtion
            // 
            this.cboDistrubtion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDistrubtion.DropDownWidth = 200;
            this.cboDistrubtion.FormattingEnabled = true;
            resources.ApplyResources(this.cboDistrubtion, "cboDistrubtion");
            this.cboDistrubtion.Name = "cboDistrubtion";
            this.cboDistrubtion.SelectedIndexChanged += new System.EventHandler(this.txt_QuickSearch);
            // 
            // d2
            // 
            resources.ApplyResources(this.d2, "d2");
            this.d2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.d2.Name = "d2";
            this.d2.ValueChanged += new System.EventHandler(this.txt_QuickSearch);
            this.d2.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // d1
            // 
            resources.ApplyResources(this.d1, "d1");
            this.d1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.d1.Name = "d1";
            this.d1.ValueChanged += new System.EventHandler(this.txt_QuickSearch);
            this.d1.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // btnREMOVE
            // 
            resources.ApplyResources(this.btnREMOVE, "btnREMOVE");
            this.btnREMOVE.Name = "btnREMOVE";
            this.btnREMOVE.UseVisualStyleBackColor = true;
            this.btnREMOVE.Click += new System.EventHandler(this.btnREMOVE_Click);
            // 
            // btnADD
            // 
            resources.ApplyResources(this.btnADD, "btnADD");
            this.btnADD.Name = "btnADD";
            this.btnADD.UseVisualStyleBackColor = true;
            this.btnADD.Click += new System.EventHandler(this.btnADD_Click);
            // 
            // btnEDIT
            // 
            resources.ApplyResources(this.btnEDIT, "btnEDIT");
            this.btnEDIT.Name = "btnEDIT";
            this.btnEDIT.UseVisualStyleBackColor = true;
            this.btnEDIT.Click += new System.EventHandler(this.btnEDIT_Click);
            // 
            // txtQuickSearch
            // 
            this.txtQuickSearch.BackColor = System.Drawing.Color.White;
            this.txtQuickSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtQuickSearch, "txtQuickSearch");
            this.txtQuickSearch.Name = "txtQuickSearch";
            this.txtQuickSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtQuickSearch.QuickSearch += new System.EventHandler(this.txt_QuickSearch);
            this.txtQuickSearch.Enter += new System.EventHandler(this.ChangeKeyboard);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "BLACKOUT_ID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn1, "dataGridViewTextBoxColumn1");
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "BLACK_OUT_DATE";
            resources.ApplyResources(this.dataGridViewTextBoxColumn2, "dataGridViewTextBoxColumn2");
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "BLACKOUT_REASON";
            resources.ApplyResources(this.dataGridViewTextBoxColumn3, "dataGridViewTextBoxColumn3");
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "DISTRIBUTION_NAME";
            resources.ApplyResources(this.dataGridViewTextBoxColumn4, "dataGridViewTextBoxColumn4");
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "BLACKOUT_DURATION";
            resources.ApplyResources(this.dataGridViewTextBoxColumn5, "dataGridViewTextBoxColumn5");
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "BLACKOUT_BEGIN";
            resources.ApplyResources(this.dataGridViewTextBoxColumn6, "dataGridViewTextBoxColumn6");
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "BLACKOUT_END";
            resources.ApplyResources(this.dataGridViewTextBoxColumn7, "dataGridViewTextBoxColumn7");
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "AFFECTED_AREAS";
            resources.ApplyResources(this.dataGridViewTextBoxColumn8, "dataGridViewTextBoxColumn8");
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "AFFECTED_CONSUMERS";
            resources.ApplyResources(this.dataGridViewTextBoxColumn9, "dataGridViewTextBoxColumn9");
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // PageOPSR03
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.panel1);
            this.Name = "PageOPSR03";
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private SoftTech.Component.ExTextbox txtQuickSearch;
        private SoftTech.Component.ExButton btnADD;
        private SoftTech.Component.ExButton btnEDIT;
        private System.Windows.Forms.DataGridView dgv;
        private SoftTech.Component.ExButton btnREMOVE;
        private System.Windows.Forms.DateTimePicker d2;
        private System.Windows.Forms.DateTimePicker d1;
        private System.Windows.Forms.ComboBox cboDistrubtion;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn COMPLAINT_VOLTAGE_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn COMPLAINT_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUSTOMER_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUSTOMER_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn DISTRIBUTION;
        private System.Windows.Forms.DataGridViewTextBoxColumn COMPLAINT_REASON;
        private System.Windows.Forms.DataGridViewTextBoxColumn MONITORING_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn SOLUTION;
        private System.Windows.Forms.DataGridViewTextBoxColumn WORK_DONE_AFTER_COMPLAINT;
    }
}
