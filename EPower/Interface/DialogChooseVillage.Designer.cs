﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogChooseVillage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cboProvince = new System.Windows.Forms.ComboBox();
            this.cboDistrict = new System.Windows.Forms.ComboBox();
            this.cboCommune = new System.Windows.Forms.ComboBox();
            this.cboVillage = new System.Windows.Forms.ComboBox();
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.lblVillage = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.lblProvince = new System.Windows.Forms.Label();
            this.lblDistrict = new System.Windows.Forms.Label();
            this.lblCommune = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.label38);
            this.content.Controls.Add(this.label39);
            this.content.Controls.Add(this.label37);
            this.content.Controls.Add(this.label36);
            this.content.Controls.Add(this.lblProvince);
            this.content.Controls.Add(this.lblDistrict);
            this.content.Controls.Add(this.lblCommune);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.label18);
            this.content.Controls.Add(this.label19);
            this.content.Controls.Add(this.label13);
            this.content.Controls.Add(this.label14);
            this.content.Controls.Add(this.lblVillage);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.cboVillage);
            this.content.Controls.Add(this.cboCommune);
            this.content.Controls.Add(this.cboDistrict);
            this.content.Controls.Add(this.cboProvince);
            this.content.Size = new System.Drawing.Size(386, 256);
            this.content.Text = "CONTENT";
            this.content.Controls.SetChildIndex(this.cboProvince, 0);
            this.content.Controls.SetChildIndex(this.cboDistrict, 0);
            this.content.Controls.SetChildIndex(this.cboCommune, 0);
            this.content.Controls.SetChildIndex(this.cboVillage, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.lblVillage, 0);
            this.content.Controls.SetChildIndex(this.label14, 0);
            this.content.Controls.SetChildIndex(this.label13, 0);
            this.content.Controls.SetChildIndex(this.label19, 0);
            this.content.Controls.SetChildIndex(this.label18, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.lblCommune, 0);
            this.content.Controls.SetChildIndex(this.lblDistrict, 0);
            this.content.Controls.SetChildIndex(this.lblProvince, 0);
            this.content.Controls.SetChildIndex(this.label36, 0);
            this.content.Controls.SetChildIndex(this.label37, 0);
            this.content.Controls.SetChildIndex(this.label39, 0);
            this.content.Controls.SetChildIndex(this.label38, 0);
            // 
            // cboProvince
            // 
            this.cboProvince.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboProvince.FormattingEnabled = true;
            this.cboProvince.Location = new System.Drawing.Point(143, 20);
            this.cboProvince.Name = "cboProvince";
            this.cboProvince.Size = new System.Drawing.Size(221, 27);
            this.cboProvince.TabIndex = 0;
            this.cboProvince.SelectedIndexChanged += new System.EventHandler(this.cboProvince_SelectedIndexChanged);
            // 
            // cboDistrict
            // 
            this.cboDistrict.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDistrict.FormattingEnabled = true;
            this.cboDistrict.Location = new System.Drawing.Point(143, 53);
            this.cboDistrict.Name = "cboDistrict";
            this.cboDistrict.Size = new System.Drawing.Size(221, 27);
            this.cboDistrict.TabIndex = 1;
            this.cboDistrict.SelectedIndexChanged += new System.EventHandler(this.cboDistrict_SelectedIndexChanged);
            // 
            // cboCommune
            // 
            this.cboCommune.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCommune.FormattingEnabled = true;
            this.cboCommune.Location = new System.Drawing.Point(143, 86);
            this.cboCommune.Name = "cboCommune";
            this.cboCommune.Size = new System.Drawing.Size(221, 27);
            this.cboCommune.TabIndex = 2;
            this.cboCommune.SelectedIndexChanged += new System.EventHandler(this.cboCommune_SelectedIndexChanged);
            // 
            // cboVillage
            // 
            this.cboVillage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboVillage.FormattingEnabled = true;
            this.cboVillage.Location = new System.Drawing.Point(143, 119);
            this.cboVillage.Name = "cboVillage";
            this.cboVillage.Size = new System.Drawing.Size(221, 27);
            this.cboVillage.TabIndex = 3;
            this.cboVillage.SelectedIndexChanged += new System.EventHandler(this.cboVillage_SelectedIndexChanged);
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOK.Location = new System.Drawing.Point(234, 224);
            this.btnOK.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(66, 23);
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "យល់ព្រម";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCLOSE
            // 
            this.btnCLOSE.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCLOSE.Location = new System.Drawing.Point(306, 224);
            this.btnCLOSE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.Size = new System.Drawing.Size(71, 23);
            this.btnCLOSE.TabIndex = 7;
            this.btnCLOSE.Text = "បិទ";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblVillage
            // 
            this.lblVillage.AutoSize = true;
            this.lblVillage.Location = new System.Drawing.Point(139, 162);
            this.lblVillage.Name = "lblVillage";
            this.lblVillage.Size = new System.Drawing.Size(95, 19);
            this.lblVillage.TabIndex = 12;
            this.lblVillage.Text = "ភូមិស្រែ  ឃុំជីក្រែង";
            // 
            // label38
            // 
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label38.ForeColor = System.Drawing.Color.Red;
            this.label38.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label38.Location = new System.Drawing.Point(68, 84);
            this.label38.Margin = new System.Windows.Forms.Padding(1);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(15, 10);
            this.label38.TabIndex = 293;
            this.label38.Text = "*";
            // 
            // label39
            // 
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label39.ForeColor = System.Drawing.Color.Red;
            this.label39.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label39.Location = new System.Drawing.Point(61, 116);
            this.label39.Margin = new System.Windows.Forms.Padding(1);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(15, 10);
            this.label39.TabIndex = 294;
            this.label39.Text = "*";
            // 
            // label37
            // 
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label37.ForeColor = System.Drawing.Color.Red;
            this.label37.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label37.Location = new System.Drawing.Point(67, 53);
            this.label37.Margin = new System.Windows.Forms.Padding(1);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(15, 10);
            this.label37.TabIndex = 292;
            this.label37.Text = "*";
            // 
            // label36
            // 
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label36.ForeColor = System.Drawing.Color.Red;
            this.label36.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label36.Location = new System.Drawing.Point(81, 16);
            this.label36.Margin = new System.Windows.Forms.Padding(1);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(15, 10);
            this.label36.TabIndex = 291;
            this.label36.Text = "*";
            // 
            // lblProvince
            // 
            this.lblProvince.AutoSize = true;
            this.lblProvince.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.lblProvince.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblProvince.Location = new System.Drawing.Point(21, 27);
            this.lblProvince.Name = "lblProvince";
            this.lblProvince.Size = new System.Drawing.Size(71, 19);
            this.lblProvince.TabIndex = 283;
            this.lblProvince.Text = "ខេត្ត /រាជធានី";
            // 
            // lblDistrict
            // 
            this.lblDistrict.AutoSize = true;
            this.lblDistrict.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.lblDistrict.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblDistrict.Location = new System.Drawing.Point(21, 58);
            this.lblDistrict.Name = "lblDistrict";
            this.lblDistrict.Size = new System.Drawing.Size(59, 19);
            this.lblDistrict.TabIndex = 284;
            this.lblDistrict.Text = "ស្រុក /ក្រុង";
            // 
            // lblCommune
            // 
            this.lblCommune.AutoSize = true;
            this.lblCommune.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.lblCommune.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblCommune.Location = new System.Drawing.Point(21, 89);
            this.lblCommune.Name = "lblCommune";
            this.lblCommune.Size = new System.Drawing.Size(56, 19);
            this.lblCommune.TabIndex = 285;
            this.lblCommune.Text = "ឃុំ/សង្កាត់";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(21, 121);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 19);
            this.label1.TabIndex = 286;
            this.label1.Text = "ភូមិ/ក្រុម";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label18.Location = new System.Drawing.Point(117, 54);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(15, 19);
            this.label18.TabIndex = 290;
            this.label18.Text = "៖";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label19.Location = new System.Drawing.Point(117, 25);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(15, 19);
            this.label19.TabIndex = 289;
            this.label19.Text = "៖";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label13.Location = new System.Drawing.Point(116, 120);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(15, 19);
            this.label13.TabIndex = 288;
            this.label13.Text = "៖";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label14.Location = new System.Drawing.Point(116, 89);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(15, 19);
            this.label14.TabIndex = 287;
            this.label14.Text = "៖";
            // 
            // DialogChooseVillage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(386, 279);
            this.Name = "DialogChooseVillage";
            this.Text = "ជ្រើសរើសភូមិ";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnOK;
        private ComboBox cboCommune;
        private ComboBox cboDistrict;
        private ComboBox cboProvince;
        private ExButton btnCLOSE;
        private Label label38;
        private Label label39;
        private Label label37;
        private Label label36;
        private Label lblProvince;
        private Label lblDistrict;
        private Label lblCommune;
        private Label label1;
        private Label label18;
        private Label label19;
        private Label label13;
        private Label label14;
        public ComboBox cboVillage;
        public Label lblVillage;
    }
}