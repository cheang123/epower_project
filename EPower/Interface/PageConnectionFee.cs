﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageConnectionFee : Form
    {
        public TBL_CONNECTION_FEE DEPOSIT
        {
            get
            {
                TBL_CONNECTION_FEE objDeposit = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    try
                    {
                        int intDeposit = (int)dgv.SelectedRows[0].Cells[DEPOSIT_ID.Name].Value;
                        objDeposit = DBDataContext.Db.TBL_CONNECTION_FEEs.FirstOrDefault(x => x.DEPOSIT_ID == intDeposit);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objDeposit;
            }
        }

        #region Constructor
        public PageConnectionFee()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            this.Reload();
            UIHelper.SetDataSourceToComboBox(cboCurrency, DBDataContext.Db.TLKP_CURRENCies.Select(x => new { x.CURRENCY_ID, x.CURRENCY_NAME })._ToDataTable(), Resources.ALL_CURRENCY);
        }
        #endregion

        #region Operation
        /// <summary>
        /// Add new ampare.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNew_Click(object sender, EventArgs e)
        {
            DialogConnectionFee objDialog = new DialogConnectionFee(GeneralProcess.Insert, new TBL_CONNECTION_FEE());
            if (objDialog.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, objDialog.Deposit.DEPOSIT_ID);
            }
        }

        /// <summary>
        /// Edit ampare
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                DialogConnectionFee dig = new DialogConnectionFee(GeneralProcess.Update, DEPOSIT);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.Deposit.DEPOSIT_ID);
                }
            }
        }

        /// <summary>
        /// Remove ampare
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exButton1_Click(object sender, EventArgs e)
        {
            if (IsDeletable())
            {
                DialogConnectionFee dig = new DialogConnectionFee(GeneralProcess.Delete, DEPOSIT);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.Deposit.DEPOSIT_ID - 1);
                }
            }
        }

        /// <summary>
        /// Load data from database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            try
            {
                Reload();

            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            } 
        }

        public void Reload()
        {
            int nCurrency = 0;
            if (cboCurrency.SelectedIndex!=-1)
            {
                nCurrency = (int)cboCurrency.SelectedValue;
            }

            dgv.DataSource = (from dep in DBDataContext.Db.TBL_CONNECTION_FEEs
                              join c in DBDataContext.Db.TLKP_CURRENCies on dep.CURRENCY_ID equals c.CURRENCY_ID
                              join imp in DBDataContext.Db.TBL_AMPAREs on dep.AMPARE_ID equals imp.AMPARE_ID
                              join phase in DBDataContext.Db.TBL_PHASEs on dep.PHASE_ID equals phase.PHASE_ID
                              where dep.IS_ACTIVE && (imp.AMPARE_NAME + " " + phase.PHASE_NAME).ToLower().Contains(txtQuickSearch.Text.ToLower().Trim())
                              &&(nCurrency==0||dep.CURRENCY_ID ==nCurrency)
                              select new
                              {
                                  dep.DEPOSIT_ID,
                                  imp.AMPARE_NAME,
                                  phase.PHASE_NAME,
                                  dep.NEW_CONECTION,
                                  c.CURRENCY_SING
                              }).ToList()
                             .OrderBy(x => x.PHASE_NAME)
                             .ThenBy(x => Lookup.ParseAmpare(x.AMPARE_NAME))
                             .ToList();
        }
        private void cboCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            Reload();
        }

        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }
        #endregion

        #region Method
        private bool IsDeletable()
        {
            bool val = true;
            return val;
        }
        #endregion

        
    }
}