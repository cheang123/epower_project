﻿namespace EPower.Interface
{
    partial class DialogCustomerBlockOption
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cboCustomerGroup = new System.Windows.Forms.ComboBox();
            this.cboCustomerConnectionType = new System.Windows.Forms.ComboBox();
            this.cboArea = new System.Windows.Forms.ComboBox();
            this.cboBilling = new System.Windows.Forms.ComboBox();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClose = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.lblCUSTOMER_GROUP = new System.Windows.Forms.Label();
            this.lblCUSTOMER_CONNECTION_TYPE = new System.Windows.Forms.Label();
            this.lblAREA = new System.Windows.Forms.Label();
            this.lblBILLING_CYCLE = new System.Windows.Forms.Label();
            this.lblCURRENCY = new System.Windows.Forms.Label();
            this.lblDAY_CUT = new System.Windows.Forms.Label();
            this.txtDayStart = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDayEnd = new System.Windows.Forms.ComboBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.txtDayEnd);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.txtDayStart);
            this.content.Controls.Add(this.lblDAY_CUT);
            this.content.Controls.Add(this.lblCURRENCY);
            this.content.Controls.Add(this.lblBILLING_CYCLE);
            this.content.Controls.Add(this.lblAREA);
            this.content.Controls.Add(this.lblCUSTOMER_CONNECTION_TYPE);
            this.content.Controls.Add(this.lblCUSTOMER_GROUP);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnClose);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.cboCurrency);
            this.content.Controls.Add(this.cboBilling);
            this.content.Controls.Add(this.cboArea);
            this.content.Controls.Add(this.cboCustomerConnectionType);
            this.content.Controls.Add(this.cboCustomerGroup);
            this.content.Size = new System.Drawing.Size(311, 251);
            this.content.Text = "CONTENT";
            this.content.Controls.SetChildIndex(this.cboCustomerGroup, 0);
            this.content.Controls.SetChildIndex(this.cboCustomerConnectionType, 0);
            this.content.Controls.SetChildIndex(this.cboArea, 0);
            this.content.Controls.SetChildIndex(this.cboBilling, 0);
            this.content.Controls.SetChildIndex(this.cboCurrency, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnClose, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_GROUP, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_CONNECTION_TYPE, 0);
            this.content.Controls.SetChildIndex(this.lblAREA, 0);
            this.content.Controls.SetChildIndex(this.lblBILLING_CYCLE, 0);
            this.content.Controls.SetChildIndex(this.lblCURRENCY, 0);
            this.content.Controls.SetChildIndex(this.lblDAY_CUT, 0);
            this.content.Controls.SetChildIndex(this.txtDayStart, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.txtDayEnd, 0);
            // 
            // cboCustomerGroup
            // 
            this.cboCustomerGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomerGroup.FormattingEnabled = true;
            this.cboCustomerGroup.Location = new System.Drawing.Point(100, 6);
            this.cboCustomerGroup.Name = "cboCustomerGroup";
            this.cboCustomerGroup.Size = new System.Drawing.Size(200, 27);
            this.cboCustomerGroup.TabIndex = 6;
            this.cboCustomerGroup.SelectedIndexChanged += new System.EventHandler(this.cboCustomerGroup_SelectedIndexChanged);
            // 
            // cboCustomerConnectionType
            // 
            this.cboCustomerConnectionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomerConnectionType.FormattingEnabled = true;
            this.cboCustomerConnectionType.Location = new System.Drawing.Point(100, 39);
            this.cboCustomerConnectionType.Name = "cboCustomerConnectionType";
            this.cboCustomerConnectionType.Size = new System.Drawing.Size(200, 27);
            this.cboCustomerConnectionType.TabIndex = 7;
            // 
            // cboArea
            // 
            this.cboArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboArea.FormattingEnabled = true;
            this.cboArea.Location = new System.Drawing.Point(100, 72);
            this.cboArea.Name = "cboArea";
            this.cboArea.Size = new System.Drawing.Size(200, 27);
            this.cboArea.TabIndex = 8;
            // 
            // cboBilling
            // 
            this.cboBilling.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBilling.FormattingEnabled = true;
            this.cboBilling.Location = new System.Drawing.Point(100, 105);
            this.cboBilling.Name = "cboBilling";
            this.cboBilling.Size = new System.Drawing.Size(200, 27);
            this.cboBilling.TabIndex = 9;
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            this.cboCurrency.Location = new System.Drawing.Point(100, 138);
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.Size = new System.Drawing.Size(200, 27);
            this.cboCurrency.TabIndex = 10;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Location = new System.Drawing.Point(2, 209);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(307, 1);
            this.panel1.TabIndex = 27;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnClose.Location = new System.Drawing.Point(225, 215);
            this.btnClose.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 26;
            this.btnClose.Text = "បិទ";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnOK.Location = new System.Drawing.Point(145, 215);
            this.btnOK.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 25;
            this.btnOK.Text = "យល់ព្រម";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblCUSTOMER_GROUP
            // 
            this.lblCUSTOMER_GROUP.AutoSize = true;
            this.lblCUSTOMER_GROUP.Location = new System.Drawing.Point(12, 9);
            this.lblCUSTOMER_GROUP.Name = "lblCUSTOMER_GROUP";
            this.lblCUSTOMER_GROUP.Size = new System.Drawing.Size(41, 19);
            this.lblCUSTOMER_GROUP.TabIndex = 28;
            this.lblCUSTOMER_GROUP.Text = "ប្រភេទ";
            // 
            // lblCUSTOMER_CONNECTION_TYPE
            // 
            this.lblCUSTOMER_CONNECTION_TYPE.AutoSize = true;
            this.lblCUSTOMER_CONNECTION_TYPE.Location = new System.Drawing.Point(12, 42);
            this.lblCUSTOMER_CONNECTION_TYPE.Name = "lblCUSTOMER_CONNECTION_TYPE";
            this.lblCUSTOMER_CONNECTION_TYPE.Size = new System.Drawing.Size(65, 19);
            this.lblCUSTOMER_CONNECTION_TYPE.TabIndex = 29;
            this.lblCUSTOMER_CONNECTION_TYPE.Text = "ការភ្ជាប់ចរន្ត";
            // 
            // lblAREA
            // 
            this.lblAREA.AutoSize = true;
            this.lblAREA.Location = new System.Drawing.Point(13, 75);
            this.lblAREA.Name = "lblAREA";
            this.lblAREA.Size = new System.Drawing.Size(33, 19);
            this.lblAREA.TabIndex = 30;
            this.lblAREA.Text = "តំបន់";
            // 
            // lblBILLING_CYCLE
            // 
            this.lblBILLING_CYCLE.AutoSize = true;
            this.lblBILLING_CYCLE.Location = new System.Drawing.Point(12, 108);
            this.lblBILLING_CYCLE.Name = "lblBILLING_CYCLE";
            this.lblBILLING_CYCLE.Size = new System.Drawing.Size(73, 19);
            this.lblBILLING_CYCLE.TabIndex = 31;
            this.lblBILLING_CYCLE.Text = "ជុំនៃការទូទាត់";
            // 
            // lblCURRENCY
            // 
            this.lblCURRENCY.AutoSize = true;
            this.lblCURRENCY.Location = new System.Drawing.Point(12, 141);
            this.lblCURRENCY.Name = "lblCURRENCY";
            this.lblCURRENCY.Size = new System.Drawing.Size(58, 19);
            this.lblCURRENCY.TabIndex = 32;
            this.lblCURRENCY.Text = "រូបិយបណ្ណ";
            // 
            // lblDAY_CUT
            // 
            this.lblDAY_CUT.AutoSize = true;
            this.lblDAY_CUT.Location = new System.Drawing.Point(12, 176);
            this.lblDAY_CUT.Name = "lblDAY_CUT";
            this.lblDAY_CUT.Size = new System.Drawing.Size(53, 19);
            this.lblDAY_CUT.TabIndex = 33;
            this.lblDAY_CUT.Text = "ចំនួន(ថ្ងៃ)";
            // 
            // txtDayStart
            // 
            this.txtDayStart.DropDownWidth = 100;
            this.txtDayStart.FormattingEnabled = true;
            this.txtDayStart.Location = new System.Drawing.Point(100, 170);
            this.txtDayStart.Name = "txtDayStart";
            this.txtDayStart.Size = new System.Drawing.Size(82, 27);
            this.txtDayStart.TabIndex = 34;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(182, 176);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 19);
            this.label1.TabIndex = 36;
            this.label1.Text = "ដល់";
            // 
            // txtDayEnd
            // 
            this.txtDayEnd.DropDownWidth = 100;
            this.txtDayEnd.FormattingEnabled = true;
            this.txtDayEnd.Location = new System.Drawing.Point(218, 170);
            this.txtDayEnd.Name = "txtDayEnd";
            this.txtDayEnd.Size = new System.Drawing.Size(82, 27);
            this.txtDayEnd.TabIndex = 37;
            // 
            // DialogCustomerBlockOption
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(311, 274);
            this.Name = "DialogCustomerBlockOption";
            this.Text = "អតិថិជនត្រូវផ្តាច់";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblBILLING_CYCLE;
        private System.Windows.Forms.Label lblAREA;
        private System.Windows.Forms.Label lblCUSTOMER_CONNECTION_TYPE;
        private System.Windows.Forms.Label lblCUSTOMER_GROUP;
        private System.Windows.Forms.Panel panel1;
        private SoftTech.Component.ExButton btnClose;
        private SoftTech.Component.ExButton btnOK;
        private System.Windows.Forms.Label lblCURRENCY;
        public System.Windows.Forms.ComboBox cboCurrency;
        public System.Windows.Forms.ComboBox cboBilling;
        public System.Windows.Forms.ComboBox cboArea;
        public System.Windows.Forms.ComboBox cboCustomerConnectionType;
        public System.Windows.Forms.ComboBox cboCustomerGroup;
        private System.Windows.Forms.Label lblDAY_CUT;
        public System.Windows.Forms.ComboBox txtDayStart;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.ComboBox txtDayEnd;
    }
}