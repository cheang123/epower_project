﻿using System;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;

namespace EPower.Interface
{
    public partial class DialogPhase: ExDialog
    {
        GeneralProcess _flag;
        TBL_PHASE _objPhase = new TBL_PHASE();
        public TBL_PHASE Phase
        {
            get { return _objPhase; }
        }
        TBL_PHASE _oldobjPhase = new TBL_PHASE();

        #region Constructor
        public DialogPhase(GeneralProcess flag, TBL_PHASE objPhase)
        {
            InitializeComponent();
            _flag = flag;

            objPhase._CopyTo(_objPhase);
            objPhase._CopyTo(_oldobjPhase);

            if (flag == GeneralProcess.Insert)
            {
                _objPhase.IS_ACTIVE = true;
                this.Text = string.Concat(Resources.INSERT, this.Text);
            }
            else if (flag == GeneralProcess.Update)
            {
                this.Text = string.Concat(Resources.UPDATE, this.Text);
            }
            else if (flag == GeneralProcess.Delete)
            {
                this.Text = string.Concat(Resources.DELETE, this.Text);
                UIHelper.SetEnabled(this, false);
            }
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;       
            read();
        }        
        #endregion

        #region Opeartion
        /// <summary>
        /// Data transaction.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
            txtPhaseName.ClearValidation();
            if (DBDataContext.Db.IsExits(_objPhase, "PHASE_NAME"))
            {
                txtPhaseName.SetValidation(string.Format( Resources.MS_IS_EXISTS,lblPHASE.Text));
                return;
            }
            try
            {
                using (TransactionScope tran=new TransactionScope( TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objPhase);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_oldobjPhase, _objPhase);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objPhase);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }                                                
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }            
        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
           this.DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        #endregion

        #region Method
        /// <summary>
        /// Load data from object.
        /// </summary>
        private void read()
        {
            txtPhaseName.Text = _objPhase.PHASE_NAME;
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            _objPhase.PHASE_NAME = txtPhaseName.Text.Trim();
        }

        private bool inValid()
        {
            bool val = false;
            txtPhaseName.ClearValidation();
            if (!DataHelper.IsNumber(txtPhaseName.Text))
            {
                txtPhaseName.SetValidation(string.Format(Resources.REQUIRED_INPUT_NUMBER, lblPHASE.Text));
                val = true;
            }
            return val;
        }
        #endregion

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objPhase);
        }

        private void txtPhaseName_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputNumberOnly(sender, e);
        }
    }
}