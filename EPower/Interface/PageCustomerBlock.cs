﻿using DevExpress.Data.Filtering;
using DevExpress.XtraBars;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using EPower.Base.Helper.DevExpressCustomize;
using EPower.Base.Logic;
using EPower.Base.Properties;
using EPower.Interface.BankPayment;
using EPower.Logic;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageCustomerBlock : Form
    {
        bool _loading = false;
        DialogCustomerBlockOption diag = new DialogCustomerBlockOption();
        List<Base.Logic.Currency> currency = new List<Base.Logic.Currency>();
        List<CustomerBlockList> lst = new List<CustomerBlockList>();
        List<TLKP_FILTER_OPTION> options = new List<TLKP_FILTER_OPTION>();

        #region Constructor
        public PageCustomerBlock()
        {
            InitializeComponent();
            searchControl.Properties.NullValuePrompt = Resources.SEARCH;
            currency = SettingLogic.Currencies;
            //BindReport();
            //cboShowStatus.SelectedIndex = 1;
            //BindData();
            //colSTATUS.SortOrder = DevExpress.Data.ColumnSortOrder.Descending;
            //colPENALTY.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
            //colCURRENCY.SortOrder = DevExpress.Data.ColumnSortOrder.Descending;
            //colCUSTOMER_CODE.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
            //Events
            this.Load += PageCustomerBlock_Load;
            this.btnCsv.ItemClick += btnCsv_ItemClick;
            this.btnExcelLandscape.ItemClick += btnExcel_ItemClick;
            this.btnPdfLandscape.ItemClick += btnPdf_ItemClick;
            this.btnExcelPortrait.ItemClick += btnExcel_ItemClick;
            this.btnPdfPortrait.ItemClick += btnPdf_ItemClick;
            this.btnPrint.ItemClick += btnPRINT_ItemClick;
            this.btnBLOCK.ItemClick += btnBlock_Click;
            this.btnCUSTOMER_PENALTY.ItemClick += btnCustomerPenalty_Click;
            this.btnSAVE_TEMPLATE.ItemClick += btnSAVE_TEMPLATE_ItemClick;
            this.btnSETTING.Click += btnSETTING_Click;
            dgvCustomerBlock.CustomColumnDisplayText += DgvCustomerBlock_CustomColumnDisplayText;
            dgvCustomerBlock.CustomDrawRowIndicator += DgvCustomerBlock_CustomDrawRowIndicator;
            dgvCustomerBlock.CustomDrawFooterCell += DgvCustomerBlock_CustomDrawFooterCell;

            colROW_NO.OptionsColumn.FixedWidth = true;
            colROW_NO.Width = 45;
            colROW_NO.MinWidth = 45;
            colROW_NO.MaxWidth = 45;
            dgvCustomerBlock.SetDefaultGridview(); 
            RestoreUserConfigure();
            dgvCustomerBlock.OptionsPrint.PrintSelectedRowsOnly = false;
        }

        public void RestoreUserConfigure()
        {
            dgvCustomerBlock.RestoreUserConfigureFromXml();
        }
        

        private void DgvCustomerBlock_CustomDrawFooterCell(object sender, FooterCellCustomDrawEventArgs e)
        {
            ((GridView)sender).FooterPanelHeight = 14;
        }

        private void DgvCustomerBlock_CustomDrawRowFooterCell(object sender, FooterCellCustomDrawEventArgs e)
        {
            Rectangle rect = e.Info.Bounds;
            string text = e.Info.DisplayText;
            e.Info.DisplayText = "";
            e.Painter.DrawObject(e.Info);
            e.Appearance.DrawString(e.Cache, text, rect);
            e.Info.DisplayText = text;
            e.Handled = true;
        }

        private void DgvCustomerBlock_CustomDrawRowIndicator(object sender, RowIndicatorCustomDrawEventArgs e)
        {
            GridView view = sender as GridView;
            if (view.GroupedColumns.Count != 0 && !view.IsGroupRow(e.RowHandle))
            {
                e.Info.DisplayText = (view.GetRowGroupIndexByRowHandle(e.RowHandle) + 1).ToString();
            }
        }
        private void DgvCustomerBlock_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            GridView view = sender as GridView;
            if (e.Column != colROW_NO)
                return;
            if (view.GroupedColumns.Count != 0 && !e.IsForGroupRow)
            {
                int rowHandle = view.GetRowHandle(e.ListSourceRowIndex);
                e.DisplayText = (view.GetRowGroupIndexByRowHandle(rowHandle) + 1).ToString();
            }
        }

        #endregion Constructor

        #region Method
        public void BindData()
        {
            if (_loading) return;
            _loading = true;

            //bool allCus = (int)cboShowStatus.SelectedValue == 0;
            resCurrency.DataSource = currency;
            resCurrencySign.DataSource = currency;

            //get cut of amount.
            decimal decCutAmount = Convert.ToDecimal(DBDataContext.Db.TBL_UTILITies
                                .FirstOrDefault(u => u.UTILITY_ID == (int)Utility.CUT_OFF_AMOUNT)
                                .UTILITY_VALUE);

            bool onlyPower = DataHelper.ParseToBoolean(Method.Utilities[Utility.USE_ONLY_POWER_INVOICE_TO_BLOCK_CUSTOMER]);

            DateTime datCurrentDate = DBDataContext.Db.GetSystemDate();
            Cursor.Current = Cursors.WaitCursor;

            Runner.RunNewThread(delegate ()
            {
                var allInv = DBDataContext.Db.TBL_INVOICEs.Where(x => x.INVOICE_STATUS == (int)InvoiceStatus.Open);

                var chargeInv = (from i in allInv
                                 join d in DBDataContext.Db.TBL_INVOICE_DETAILs on i.INVOICE_ID equals d.INVOICE_ID
                                 where i.IS_SERVICE_BILL && d.INVOICE_ITEM_ID == -4
                                 select i).ToList();

                var invoice = (from inv in allInv
                               where inv.INVOICE_STATUS == (int)InvoiceStatus.Open
                                  && (!onlyPower || inv.IS_SERVICE_BILL == false)
                               group inv by new
                               {
                                   inv.CUSTOMER_ID,
                                   inv.CURRENCY_ID
                               } into g
                               select new
                               {
                                   g.Key.CUSTOMER_ID,
                                   g.Key.CURRENCY_ID,
                                   LAST_DUE_DATE = g.Max(o => o.DUE_DATE),
                                   DUE_DATE = g.Min(p => p.DUE_DATE),
                                   BALANCE_DUE = g.Sum(p => p.SETTLE_AMOUNT - p.PAID_AMOUNT)
                               }).ToList();

                var customer = (from c in DBDataContext.Db.TBL_CUSTOMERs
                                join ct in DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs on c.CUSTOMER_CONNECTION_TYPE_ID equals ct.CUSTOMER_CONNECTION_TYPE_ID
                                join cg in DBDataContext.Db.TLKP_CUSTOMER_GROUPs on ct.NONLICENSE_CUSTOMER_GROUP_ID equals cg.CUSTOMER_GROUP_ID
                                join cl in DBDataContext.Db.TBL_BILLING_CYCLEs on c.BILLING_CYCLE_ID equals cl.CYCLE_ID
                                where c.STATUS_ID == (int)CustomerStatus.Active
                                 && c.IS_POST_PAID
                                select new
                                {
                                    c.CUT_OFF_DAY,
                                    CYCLE_ID = cl.CYCLE_ID,
                                    CYCLE_NAME = cl.CYCLE_NAME,
                                    c.CUSTOMER_CODE,
                                    c.CUSTOMER_ID,
                                    CUSTOMER_GROUP_NAME = cg.CUSTOMER_GROUP_NAME,
                                    CUSTOMER_CONNECTION_TYPE_NAME = cg.CUSTOMER_GROUP_ID == 1 || cg.CUSTOMER_GROUP_ID == 2 || cg.CUSTOMER_GROUP_ID == 3 ? cg.CUSTOMER_GROUP_NAME + " (" + ct.CUSTOMER_CONNECTION_TYPE_NAME + ")" : ct.CUSTOMER_CONNECTION_TYPE_NAME,
                                    CUSTOMER_NAME = c.LAST_NAME_KH + " " + c.FIRST_NAME_KH,
                                    PHONE = c.PHONE_1
                                }).ToList();

                var cusMeter = (from cm in DBDataContext.Db.TBL_CUSTOMER_METERs
                                join m in DBDataContext.Db.TBL_METERs on cm.METER_ID equals m.METER_ID
                                join b in DBDataContext.Db.TBL_BOXes on cm.BOX_ID equals b.BOX_ID
                                join p in DBDataContext.Db.TBL_POLEs on b.POLE_ID equals p.POLE_ID
                                join a in DBDataContext.Db.TBL_AREAs on p.AREA_ID equals a.AREA_ID
                                where cm.IS_ACTIVE
                                select new
                                {
                                    cm.CUSTOMER_ID,
                                    m.METER_CODE,
                                    AREA_CODE = a.AREA_NAME,
                                    p.POLE_CODE,
                                    b.BOX_CODE
                                }).ToList();

                lst = (from c in customer
                       join cm in cusMeter on c.CUSTOMER_ID equals cm.CUSTOMER_ID
                       join i in invoice on c.CUSTOMER_ID equals i.CUSTOMER_ID
                       //where (allCus || (i?.BALANCE_DUE >= decCutAmount
                       // && i?.DUE_DATE.AddDays(c.CUT_OFF_DAY).Date <= datCurrentDate.Date))
                       //orderby c.CUSTOMER_CODE
                       select new CustomerBlockList
                       {
                           CUSTOMER_ID = c.CUSTOMER_ID,
                           CUSTOMER_CODE = c.CUSTOMER_CODE,
                           CUSTOMER_NAME = c.CUSTOMER_NAME,
                           CUSTOMER_GROUP = c.CUSTOMER_GROUP_NAME,
                           CUSTOMER_CONNECTION_TYPE = c.CUSTOMER_CONNECTION_TYPE_NAME,
                           CYCLE_ID = c.CYCLE_ID,
                           CYCLE_NAME = c.CYCLE_NAME,
                           AREA = cm.AREA_CODE,
                           POLE = cm.POLE_CODE,
                           BOX = cm.BOX_CODE,
                           METER_CODE = cm.METER_CODE,
                           PHONE = c.PHONE,
                           //DUE_DATE = allCus ? i?.LAST_DUE_DATE.AddDays(c.CUT_OFF_DAY) : i?.DUE_DATE.AddDays(c.CUT_OFF_DAY),
                           //DAY = (int?)(datCurrentDate - (allCus ? i?.DUE_DATE : i?.DUE_DATE)?.AddDays(c.CUT_OFF_DAY))?.TotalDays,
                           DUE_DATE = i?.LAST_DUE_DATE.AddDays(c.CUT_OFF_DAY),
                           DAY = (int?)(datCurrentDate - (i?.DUE_DATE)?.AddDays(c.CUT_OFF_DAY))?.TotalDays,
                           DUE_AMOUNT = i?.BALANCE_DUE,
                           CURRENCY_ID = i?.CURRENCY_ID ?? 0,
                           CURRENCY = i?.CURRENCY_ID ?? 0,
                           PENALTY = chargeInv.Any(x => x.IS_SERVICE_BILL && x.DUE_DATE.AddDays(c.CUT_OFF_DAY).Date > datCurrentDate.Date && x.CUSTOMER_ID == c.CUSTOMER_ID) ? Resources.FINED : Resources.NOT_YET_FINED
                       }).ToList();
            });

            lst.ForEach(x => x.STATUS = x.DAY >= 0 ? Resources.OVERDUE : Resources.NOT_OVERDUE);
            dgv.DataSource = lst.OrderBy(x => x.CURRENCY).ThenBy(x => x.AREA).ThenBy(x => x.POLE).ThenBy(x => x.BOX).ThenBy(x => x.CUSTOMER_CODE).ToList();
            dgvCustomerBlock.ExpandAllGroups();
            _loading = false;
            Cursor.Current = Cursors.Default;
        }

        public void BindAction()
        {
            options = DBDataContext.Db.TLKP_FILTER_OPTIONs.Where(x => x.IS_ACTIVE && x.GRID_NAME == this.Name.ToUpper()).ToList();
            var font = new Font("Khmer OS System", 8.24f);
            foreach (var item in options)
            {
                if (!popActions.ItemLinks.Where(x => x.Caption == $"បង្ហាញទម្រង់៖ {item.FILTER_NAME}").Any())
                {
                    BarButtonItem btnRemove = new BarButtonItem();
                    btnRemove.Caption = Resources.REMOVE;
                    btnRemove.Tag = item.FILTER_ID;
                    btnRemove.ItemClick += btnRemove_ItemClick;
                    

                    var btnFilter = new BarSubItem();
                    btnFilter.Caption = $"បង្ហាញទម្រង់៖ {item.FILTER_NAME}";
                    btnFilter.Tag = item.FILTER_ID;
                    if(!item.IS_SYSTEM)
                    {
                        barManager1.Items.Add(btnRemove);
                        btnFilter.AddItem(btnRemove);
                    }
                    popActions.AddItem(btnFilter);
                    btnFilter.ItemClick += btnFilter_ItemClick;
                    btnFilter.MenuAppearance.AppearanceMenu.SetFont(font);
                }
            }
            barManager1.DockWindowTabFont = font;
            popActions.MenuAppearance.AppearanceMenu.SetFont(font);
        }

        private string GetReportDescription()
        {
            var a = dgvCustomerBlock.ActiveFilterString;
            var description = "";// $"{Resources.FROMDATE} {rdpDate.FromDate.ToDevShortDate()} {Resources.TODATE} {rdpDate.ToDate.ToDevShortDate()} ";
            return description;
        }

        #endregion Method

        #region Event

        private void btnBlock_Click(object sender, EventArgs e)
        {
            var selectedCus = dgvCustomerBlock.GetSelectedRows().Where(x => x > -1).Count();
            if (selectedCus < 1)
            {
                MsgBox.ShowInformation(string.Format(Resources.REQUIRE_CHECK, Resources.BLOCK));
                return;
            }
            dgvCustomerBlock.GetSelectedRows().Where(x => x > -1);
            if (selectedCus > 2000)
            {
                MsgBox.ShowInformation(string.Format(Resources.NOT_ALLOW_DATA_MORE_THAN_2000, Resources.CUSTOMER_BLOCK));
                return;
            }
            if (MsgBox.ShowQuestion(string.Format(Resources.MSG_CONFIRM_BLOCK_CUSTOMER, selectedCus), Resources.WARNING) != DialogResult.Yes)
            {
                return;
            }

            List<CustomerBlockList> lst = new List<CustomerBlockList>();
            foreach (var item in dgvCustomerBlock.GetSelectedRows().Where(x => x > -1))
            {
                CustomerBlockList customer = dgvCustomerBlock.GetRow(item) as CustomerBlockList;
                if (customer != null)
                {
                    lst.Add(customer);
                }
            }
            var now = DBDataContext.Db.GetSystemDate();
            var cus = DBDataContext.Db.TBL_CUSTOMERs.Where(x => lst.Select(c => c.CUSTOMER_ID).Contains(x.CUSTOMER_ID)).ToList();
            Runner.RunNewThread(() =>
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    foreach (var c in cus)
                    {
                        TBL_CUSTOMER oldCus = new TBL_CUSTOMER();
                        c._CopyTo(oldCus);
                        var objCusStatus = new TBL_CUS_STATUS_CHANGE()
                        {
                            CUSTOMER_ID = c.CUSTOMER_ID,
                            CHNAGE_DATE = now,
                            CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                            OLD_STATUS_ID = c.STATUS_ID,
                            NEW_STATUS_ID = (int)CustomerStatus.Blocked,
                            INVOICE_ID = 0
                        };
                        DBDataContext.Db.TBL_CUS_STATUS_CHANGEs.InsertOnSubmit(objCusStatus);
                        c.STATUS_ID = (int)CustomerStatus.Blocked;
                        DBDataContext.Db.Update(oldCus, c);
                        DBDataContext.Db.SubmitChanges();
                    }
                    tran.Complete();
                }
            });
            BindData();
        }

        private void btnCustomerPenalty_Click(object sender, EventArgs e)
        {
            var selectedCus = dgvCustomerBlock.GetSelectedRows().Where(x => x > -1).Count();
            var now = DBDataContext.Db.GetSystemDate();
            if (selectedCus < 1)
            {
                MsgBox.ShowInformation(string.Format(Resources.REQUIRE_CHECK, Resources.PENALTY));
                return;
            }
            List<TBL_CUSTOMER> cusIds = new List<TBL_CUSTOMER>();
            int currId = 0;
            foreach (var cus in dgvCustomerBlock.GetSelectedRows().Where(x => x > -1))
            {
                if (cus < 0) continue;
                CustomerBlockList customer = dgvCustomerBlock.GetRow(cus) as CustomerBlockList;
                cusIds.Add(new TBL_CUSTOMER { CUSTOMER_ID = customer.CUSTOMER_ID, BILLING_CYCLE_ID = customer.CYCLE_ID });
                currId = customer.CURRENCY_ID ?? 0;
            }

            DialogCustomersCharge diag = new DialogCustomersCharge(cusIds, currId);

            if (diag.ShowDialog() == DialogResult.OK)
            {
                BindData();
            }
        } 
        private void btnSAVE_TEMPLATE_ItemClick(object sender, ItemClickEventArgs e)
        {
            DialogTemplate dialog = new DialogTemplate(dgvCustomerBlock.ActiveFilterString, this.Name.ToUpper());
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                var file = dgvCustomerBlock.SaveUserConfigureToXml(dialog.txtTemplateName.Text);
                TLKP_FILTER_OPTION obj = dialog._objFilter;
                obj.XML_FILE = File.ReadAllBytes(file);
                DBDataContext.Db.SubmitChanges();
                BindAction();
            }
        } 

        private void btnPRINT_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DevExtension.ShowGridPreview(dgv, Resources.REPORT_CUSTOMER_BLOCK, GetReportDescription(), false);
        }

        private void btnExcel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            bool ispLanscape = e.Item.Name == btnExcelLandscape.Name;
            DevExtension.ToExcel(dgv, Resources.REPORT_CUSTOMER_BLOCK, GetReportDescription(), Resources.REPORT_CUSTOMER_BLOCK, ispLanscape);
        }

        private void btnCsv_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DevExtension.ToCsv(dgv, Resources.REPORT_CUSTOMER_BLOCK, GetReportDescription(), Resources.REPORT_CUSTOMER_BLOCK, true);
        }

        private void btnPdf_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            bool ispLanscape = e.Item.Name == btnPdfLandscape.Name;
            //var g = this.dgvCustomerBlock.GroupedColumns.ToList();
            //this.dgvCustomerBlock.GroupedColumns.ToList().ForEach(x => { x.UnGroup(); x.Visible = false; });
            DevExtension.ToPdf(dgv, Resources.REPORT_CUSTOMER_BLOCK, GetReportDescription(), Resources.REPORT_CUSTOMER_BLOCK, ispLanscape);
            //foreach (var item in g)
            //{
            //    this.dgvCustomerBlock.Columns.FirstOrDefault(x => x == item).Visible = true;
            //    this.dgvCustomerBlock.Columns.FirstOrDefault(x => x == item).Group();
            //    this.dgvCustomerBlock.Columns.FirstOrDefault(x => x == item).GroupIndex = item.GroupIndex;
            //}
            //colSTATUS.SortOrder = DevExpress.Data.ColumnSortOrder.Descending;
            //colPENALTY.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
            //colCURRENCY.SortOrder = DevExpress.Data.ColumnSortOrder.Descending;
            //colCUSTOMER_CODE.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
            //this.dgvCustomerBlock.ExpandAllGroups();
        }

        private void btnRemove_ItemClick(object sender, ItemClickEventArgs e)
        {
            var filter = options.FirstOrDefault(x => x.FILTER_ID == (int)e.Item.Tag);
            if (filter == null)
            {
                return;
            }
            filter.IS_ACTIVE = false;
            DBDataContext.Db.SubmitChanges();
            popActions.RemoveLink(popActions.ItemLinks.ToList().FirstOrDefault(x => x.Caption == $"បង្ហាញទម្រង់៖ {filter.FILTER_NAME}"));
        }

        private void PageCustomerBlock_Load(object sender, EventArgs e)
        {
            //Add menu 
            BindAction();
        }

        private void btnFilter_ItemClick(object sender, ItemClickEventArgs e)
        { 
            var filter = options.FirstOrDefault(x => x.FILTER_ID == (int)e.Item.Tag);
            if (filter == null)
            {
                return;
            }
            this.dgvCustomerBlock.ActiveFilterString = filter.FILTER_OPTION;
            this.popActions.HidePopup();
            var file = dgvCustomerBlock.RestoreUserConfigureFromXml(filter.FILTER_NAME);
            if (string.IsNullOrEmpty(file) && filter.XML_FILE != null)
            {
                var baseDirectory = Path.Combine(Application.StartupPath, "UserConfigures");
                file = Path.Combine(baseDirectory, $"{dgvCustomerBlock.GridControl.Parent.Name}.{filter.FILTER_NAME}.xml");
                File.WriteAllBytes(file, filter.XML_FILE.ToArray());
                dgvCustomerBlock.RestoreUserConfigureFromXml(filter.FILTER_NAME);
            }
            dgvCustomerBlock.ExpandAllGroups();
        }

        private void btnSETTING_Click(object sender, EventArgs e)
        {
            string filter = "";
            if (string.IsNullOrEmpty(this.dgvCustomerBlock.ActiveFilterString))
            {
                diag = new DialogCustomerBlockOption();
            }
            if (diag.ShowDialog() == DialogResult.OK)
            {
                if ((int)diag.cboCustomerGroup.SelectedValue > 0)
                {
                    filter = $"[{colCUSTOMER_GROUP.FieldName}] = '{diag.cboCustomerGroup.Text}' And ";
                }
                if ((int)diag.cboCustomerConnectionType.SelectedValue > 0)
                {
                    filter += $"[{colCUSTOMER_CONNECTION_TYPE.FieldName}] = '{diag.cboCustomerConnectionType.Text}' And ";
                }
                if ((int)diag.cboArea.SelectedValue > 0)
                {
                    filter += $"[{colAREA.FieldName}] = '{diag.cboArea.Text}' And ";
                }
                if ((int)diag.cboBilling.SelectedValue > 0)
                {
                    filter += $"[{colCYCLE_NAME.FieldName}] = '{diag.cboBilling.Text}' And ";
                }
                if ((int)diag.cboCurrency.SelectedValue > 0)
                {
                    filter += $"[{colCURRENCY.FieldName}] = '{(int)diag.cboCurrency.SelectedValue}' And ";
                }
                if (diag.txtDayStart.Text != Resources.UNLIMITED)
                {
                    filter += $"[{colDAY.FieldName}] >= '{DataHelper.ParseToInt(diag.txtDayStart.Text)}' And ";
                }
                if (diag.txtDayEnd.Text != Resources.UNLIMITED)
                {
                    filter += $"[{colDAY.FieldName}] <= '{DataHelper.ParseToInt(diag.txtDayEnd.Text)}' And ";
                }
                if (!string.IsNullOrEmpty(filter))
                {
                    filter = filter.Remove(filter.Length - 5, 5);
                }
                this.dgvCustomerBlock.ActiveFilterString = filter;
            }
        }

        #endregion Event

        private void dgvCustomerBlock_CustomUnboundColumnData(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs e)
        {

            //e.Value = e.ListSourceRowIndex + 1;
        }

        private void dgvCustomerBlock_GroupLevelStyle(object sender, GroupLevelStyleEventArgs e)
        {

        }

        public void SaveUserConfigure()
        {
            dgvCustomerBlock.SaveUserConfigureToXml();
        }

        protected override void OnVisibleChanged(EventArgs e)
        {
            SaveUserConfigure();
            base.OnVisibleChanged(e);
        }
        protected override void OnFormClosed(FormClosedEventArgs e)
        {
            SaveUserConfigure();
            base.OnFormClosed(e);
        }
    }

    public static class GridExtensions
    {
        public static int GetRowGroupIndexByRowHandle(this GridView view, int rowHandle)
        {
            int parentRowHandle = view.GetParentRowHandle(rowHandle);
            int childCount = view.GetChildRowCount(parentRowHandle);
            int lastRowHandle = view.GetChildRowHandle(parentRowHandle, childCount - 1);
            return (childCount - 1) - Math.Abs(lastRowHandle - rowHandle);
        }
    }

    class CustomerBlockList
    {
        public int CUSTOMER_ID { get; set; }
        public string CUSTOMER_CODE { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string METER_CODE { get; set; }
        public string CUSTOMER_GROUP { get; set; }
        public string CUSTOMER_CONNECTION_TYPE { get; set; }
        public int CYCLE_ID { get; set; }
        public string CYCLE_NAME { get; set; }
        public string AREA { get; set; }
        public string POLE { get; set; }
        public string BOX { get; set; }
        public string PHONE { get; set; }
        public DateTime? DUE_DATE { get; set; }
        public int? DAY { get; set; }
        public string STATUS { get; set; }
        public decimal? DUE_AMOUNT { get; set; }
        public int? CURRENCY_ID { get; set; }
        public int? CURRENCY { get; set; }
        public string PENALTY { get; set; }
    }
}
