﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogPaymentMethod
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogPaymentMethod));
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPaymentMethod = new System.Windows.Forms.TextBox();
            this.lblNAME = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.dgv = new DevExpress.XtraGrid.GridControl();
            this.dgvPaymentAccount = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colACCOUNT_TYPE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colACCOUNT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.resAccount = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colCURRENCY_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPaymentAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resAccount)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.panel5);
            this.content.Controls.Add(this.panel3);
            this.content.Controls.Add(this.panel2);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.panel2, 0);
            this.content.Controls.SetChildIndex(this.panel3, 0);
            this.content.Controls.SetChildIndex(this.panel5, 0);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnCHANGE_LOG);
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.btnCLOSE);
            this.panel2.Controls.Add(this.btnOK);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.txtPaymentMethod);
            this.panel3.Controls.Add(this.lblNAME);
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Name = "label2";
            // 
            // txtPaymentMethod
            // 
            resources.ApplyResources(this.txtPaymentMethod, "txtPaymentMethod");
            this.txtPaymentMethod.Name = "txtPaymentMethod";
            // 
            // lblNAME
            // 
            resources.ApplyResources(this.lblNAME, "lblNAME");
            this.lblNAME.Name = "lblNAME";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.dgv);
            resources.ApplyResources(this.panel5, "panel5");
            this.panel5.Name = "panel5";
            // 
            // dgv
            // 
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.MainView = this.dgvPaymentAccount;
            this.dgv.Name = "dgv";
            this.dgv.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.resAccount});
            this.dgv.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.dgvPaymentAccount});
            // 
            // dgvPaymentAccount
            // 
            this.dgvPaymentAccount.Appearance.ColumnFilterButton.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentAccount.Appearance.ColumnFilterButton.Font")));
            this.dgvPaymentAccount.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.dgvPaymentAccount.Appearance.ColumnFilterButtonActive.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentAccount.Appearance.ColumnFilterButtonActive.Font")));
            this.dgvPaymentAccount.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.dgvPaymentAccount.Appearance.CustomizationFormHint.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentAccount.Appearance.CustomizationFormHint.Font")));
            this.dgvPaymentAccount.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.dgvPaymentAccount.Appearance.DetailTip.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentAccount.Appearance.DetailTip.Font")));
            this.dgvPaymentAccount.Appearance.DetailTip.Options.UseFont = true;
            this.dgvPaymentAccount.Appearance.Empty.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentAccount.Appearance.Empty.Font")));
            this.dgvPaymentAccount.Appearance.Empty.Options.UseFont = true;
            this.dgvPaymentAccount.Appearance.EvenRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentAccount.Appearance.EvenRow.Font")));
            this.dgvPaymentAccount.Appearance.EvenRow.Options.UseFont = true;
            this.dgvPaymentAccount.Appearance.FilterCloseButton.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentAccount.Appearance.FilterCloseButton.Font")));
            this.dgvPaymentAccount.Appearance.FilterCloseButton.Options.UseFont = true;
            this.dgvPaymentAccount.Appearance.FilterPanel.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentAccount.Appearance.FilterPanel.Font")));
            this.dgvPaymentAccount.Appearance.FilterPanel.Options.UseFont = true;
            this.dgvPaymentAccount.Appearance.FixedLine.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentAccount.Appearance.FixedLine.Font")));
            this.dgvPaymentAccount.Appearance.FixedLine.Options.UseFont = true;
            this.dgvPaymentAccount.Appearance.FocusedCell.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentAccount.Appearance.FocusedCell.Font")));
            this.dgvPaymentAccount.Appearance.FocusedCell.Options.UseFont = true;
            this.dgvPaymentAccount.Appearance.FocusedRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentAccount.Appearance.FocusedRow.Font")));
            this.dgvPaymentAccount.Appearance.FocusedRow.Options.UseFont = true;
            this.dgvPaymentAccount.Appearance.FooterPanel.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentAccount.Appearance.FooterPanel.Font")));
            this.dgvPaymentAccount.Appearance.FooterPanel.Options.UseFont = true;
            this.dgvPaymentAccount.Appearance.GroupButton.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentAccount.Appearance.GroupButton.Font")));
            this.dgvPaymentAccount.Appearance.GroupButton.Options.UseFont = true;
            this.dgvPaymentAccount.Appearance.GroupFooter.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentAccount.Appearance.GroupFooter.Font")));
            this.dgvPaymentAccount.Appearance.GroupFooter.Options.UseFont = true;
            this.dgvPaymentAccount.Appearance.GroupPanel.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentAccount.Appearance.GroupPanel.Font")));
            this.dgvPaymentAccount.Appearance.GroupPanel.Options.UseFont = true;
            this.dgvPaymentAccount.Appearance.GroupRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentAccount.Appearance.GroupRow.Font")));
            this.dgvPaymentAccount.Appearance.GroupRow.Options.UseFont = true;
            this.dgvPaymentAccount.Appearance.HeaderPanel.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentAccount.Appearance.HeaderPanel.Font")));
            this.dgvPaymentAccount.Appearance.HeaderPanel.Options.UseFont = true;
            this.dgvPaymentAccount.Appearance.HideSelectionRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentAccount.Appearance.HideSelectionRow.Font")));
            this.dgvPaymentAccount.Appearance.HideSelectionRow.Options.UseFont = true;
            this.dgvPaymentAccount.Appearance.HorzLine.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentAccount.Appearance.HorzLine.Font")));
            this.dgvPaymentAccount.Appearance.HorzLine.Options.UseFont = true;
            this.dgvPaymentAccount.Appearance.HotTrackedRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentAccount.Appearance.HotTrackedRow.Font")));
            this.dgvPaymentAccount.Appearance.HotTrackedRow.Options.UseFont = true;
            this.dgvPaymentAccount.Appearance.OddRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentAccount.Appearance.OddRow.Font")));
            this.dgvPaymentAccount.Appearance.OddRow.Options.UseFont = true;
            this.dgvPaymentAccount.Appearance.Preview.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentAccount.Appearance.Preview.Font")));
            this.dgvPaymentAccount.Appearance.Preview.Options.UseFont = true;
            this.dgvPaymentAccount.Appearance.Row.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentAccount.Appearance.Row.Font")));
            this.dgvPaymentAccount.Appearance.Row.Options.UseFont = true;
            this.dgvPaymentAccount.Appearance.RowSeparator.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentAccount.Appearance.RowSeparator.Font")));
            this.dgvPaymentAccount.Appearance.RowSeparator.Options.UseFont = true;
            this.dgvPaymentAccount.Appearance.SelectedRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentAccount.Appearance.SelectedRow.Font")));
            this.dgvPaymentAccount.Appearance.SelectedRow.Options.UseFont = true;
            this.dgvPaymentAccount.Appearance.TopNewRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentAccount.Appearance.TopNewRow.Font")));
            this.dgvPaymentAccount.Appearance.TopNewRow.Options.UseFont = true;
            this.dgvPaymentAccount.Appearance.VertLine.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentAccount.Appearance.VertLine.Font")));
            this.dgvPaymentAccount.Appearance.VertLine.Options.UseFont = true;
            this.dgvPaymentAccount.Appearance.ViewCaption.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentAccount.Appearance.ViewCaption.Font")));
            this.dgvPaymentAccount.Appearance.ViewCaption.Options.UseFont = true;
            this.dgvPaymentAccount.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colACCOUNT_TYPE,
            this.colACCOUNT,
            this.colCURRENCY_ID});
            this.dgvPaymentAccount.GridControl = this.dgv;
            this.dgvPaymentAccount.Name = "dgvPaymentAccount";
            this.dgvPaymentAccount.OptionsView.ShowGroupPanel = false;
            this.dgvPaymentAccount.OptionsView.ShowIndicator = false;
            // 
            // colACCOUNT_TYPE
            // 
            resources.ApplyResources(this.colACCOUNT_TYPE, "colACCOUNT_TYPE");
            this.colACCOUNT_TYPE.FieldName = "Currency";
            this.colACCOUNT_TYPE.Name = "colACCOUNT_TYPE";
            this.colACCOUNT_TYPE.OptionsColumn.AllowEdit = false;
            // 
            // colACCOUNT
            // 
            resources.ApplyResources(this.colACCOUNT, "colACCOUNT");
            this.colACCOUNT.ColumnEdit = this.resAccount;
            this.colACCOUNT.FieldName = "AccountId";
            this.colACCOUNT.MinWidth = 200;
            this.colACCOUNT.Name = "colACCOUNT";
            // 
            // resAccount
            // 
            this.resAccount.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("resAccount.Appearance.Font")));
            this.resAccount.Appearance.Options.UseFont = true;
            this.resAccount.AppearanceDisabled.Font = ((System.Drawing.Font)(resources.GetObject("resAccount.AppearanceDisabled.Font")));
            this.resAccount.AppearanceDisabled.Options.UseFont = true;
            this.resAccount.AppearanceDropDown.Font = ((System.Drawing.Font)(resources.GetObject("resAccount.AppearanceDropDown.Font")));
            this.resAccount.AppearanceDropDown.Options.UseFont = true;
            this.resAccount.AppearanceDropDownHeader.Font = ((System.Drawing.Font)(resources.GetObject("resAccount.AppearanceDropDownHeader.Font")));
            this.resAccount.AppearanceDropDownHeader.Options.UseFont = true;
            this.resAccount.AppearanceFocused.Font = ((System.Drawing.Font)(resources.GetObject("resAccount.AppearanceFocused.Font")));
            this.resAccount.AppearanceFocused.Options.UseFont = true;
            this.resAccount.AppearanceReadOnly.Font = ((System.Drawing.Font)(resources.GetObject("resAccount.AppearanceReadOnly.Font")));
            this.resAccount.AppearanceReadOnly.Options.UseFont = true;
            resources.ApplyResources(this.resAccount, "resAccount");
            this.resAccount.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("resAccount.Buttons"))))});
            this.resAccount.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("resAccount.Columns"), resources.GetString("resAccount.Columns1"), ((int)(resources.GetObject("resAccount.Columns2"))), ((DevExpress.Utils.FormatType)(resources.GetObject("resAccount.Columns3"))), resources.GetString("resAccount.Columns4"), ((bool)(resources.GetObject("resAccount.Columns5"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("resAccount.Columns6"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("resAccount.Columns7"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("resAccount.Columns8")))),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("resAccount.Columns9"), resources.GetString("resAccount.Columns10"), ((int)(resources.GetObject("resAccount.Columns11"))), ((DevExpress.Utils.FormatType)(resources.GetObject("resAccount.Columns12"))), resources.GetString("resAccount.Columns13"), ((bool)(resources.GetObject("resAccount.Columns14"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("resAccount.Columns15"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("resAccount.Columns16"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("resAccount.Columns17"))))});
            this.resAccount.DisplayMember = "AccountName";
            this.resAccount.Name = "resAccount";
            this.resAccount.PopupWidth = 400;
            this.resAccount.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.resAccount.ValueMember = "Id";
            // 
            // colCURRENCY_ID
            // 
            resources.ApplyResources(this.colCURRENCY_ID, "colCURRENCY_ID");
            this.colCURRENCY_ID.FieldName = "CurrencyId";
            this.colCURRENCY_ID.Name = "colCURRENCY_ID";
            // 
            // DialogPaymentMethod
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogPaymentMethod";
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPaymentAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resAccount)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Panel panel2;
        private ExButton btnCHANGE_LOG;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Panel panel5;
        private DevExpress.XtraGrid.GridControl dgv;
        private DevExpress.XtraGrid.Views.Grid.GridView dgvPaymentAccount;
        private DevExpress.XtraGrid.Columns.GridColumn colACCOUNT_TYPE;
        private DevExpress.XtraGrid.Columns.GridColumn colACCOUNT;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit resAccount;
        private Panel panel3;
        private Label label2;
        private TextBox txtPaymentMethod;
        private Label lblNAME;
        private DevExpress.XtraGrid.Columns.GridColumn colCURRENCY_ID;
        private Panel panel1;
    }
}