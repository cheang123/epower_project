﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogCustomerBatchChange_2 : ExDialog
    {
        public static string LastVillageCode = "";

        public List<TBL_CUSTOMER> _customers = new List<TBL_CUSTOMER>();
        List<TBL_CUSTOMER> allCus = DBDataContext.Db.TBL_CUSTOMERs.ToList();
        List<TLKP_CUSTOMER_CONNECTION_TYPE> lstConnectionType = new List<TLKP_CUSTOMER_CONNECTION_TYPE>();
        bool _ismarketvendor = false;
        public DialogCustomerBatchChange_2(List<TBL_CUSTOMER> customers)
        {
            InitializeComponent();
            _customers = customers;
            read();
        }

        void read()
        {
            int i = 0;
            DateTime date = DateTime.Now;
            foreach (var item in DBDataContext.Db.TBL_BILLING_CYCLEs.Where(x => x.IS_ACTIVE))
            {
                DateTime datStart = UIHelper._DefaultDate;
                DateTime datEnd = UIHelper._DefaultDate;
                DateTime datRuningMonth = Method.GetNextBillingMonth(item.CYCLE_ID, ref datStart, ref datEnd);
                if (i == 0)
                {
                    date = datRuningMonth;
                }
                if (DBDataContext.Db.TBL_CUSTOMERs.Where(x => x.BILLING_CYCLE_ID == item.CYCLE_ID && x.STATUS_ID == (int)CustomerStatus.Active).Count() > 0)
                {
                    if (datRuningMonth != date)
                    {
                        cboCycle.Visible =
                        lblCYCLE_NAME.Visible = false;
                        break;
                    }
                }
                i++;
            }

            //DateTime date = UIHelper._DefaultDate;
            //var cycle = _customers.GroupBy(x => x.BILLING_CYCLE_ID).Select(x => x.Key).ToList();
            //DateTime datRuningMonth = Method.GetNextBillingMonth(cycle[0], ref date, ref date);
            //foreach (var id in cycle)
            //{
            //    if (datRuningMonth != Method.GetNextBillingMonth(id, ref date, ref date))
            //    {
            //        cboCycle.Visible =
            //            lblCYCLE_NAME.Visible = false;
            //        break;
            //    }
            //}

            UIHelper.SetDataSourceToComboBox(this.cboPrice, Lookup.GetPrices(), "");
            UIHelper.SetDataSourceToComboBox(this.cboCycle, Lookup.GetBillingCycles(), "");
            // show only district that set in License Village
            var d = (from l in DBDataContext.Db.TBL_LICENSE_VILLAGEs
                     join v in DBDataContext.Db.TLKP_VILLAGEs on l.VILLAGE_CODE equals v.VILLAGE_CODE
                     join c in DBDataContext.Db.TLKP_COMMUNEs on v.COMMUNE_CODE equals c.COMMUNE_CODE
                     where l.IS_ACTIVE
                     select c.DISTRICT_CODE).Distinct();
            UIHelper.SetDataSourceToComboBox(this.cboDistrict, DBDataContext.Db.TLKP_DISTRICTs.Where(x => d.Contains(x.DISTRICT_CODE)), "");

            this.cboPrice.SelectedIndex = 0;
            this.cboCycle.SelectedIndex = 0;
            this.lblCustomer_.Text = _customers.Count.ToString("0");

            var village = (from v in DBDataContext.Db.TLKP_VILLAGEs
                           join c in DBDataContext.Db.TLKP_COMMUNEs on v.COMMUNE_CODE equals c.COMMUNE_CODE
                           where v.VILLAGE_CODE == LastVillageCode
                           select new { v.VILLAGE_CODE, c.COMMUNE_CODE, c.DISTRICT_CODE }).FirstOrDefault();
            if (village != null)
            {
                this.cboDistrict.SelectedValue = village.DISTRICT_CODE;
                this.cboCommune.SelectedValue = village.COMMUNE_CODE;
                this.cboVillage.SelectedValue = village.VILLAGE_CODE;
            }

            var cusType = _customers.GroupBy(x => x.CUSTOMER_TYPE_ID).Select(x => x.Key).ToList();
            if (cusType.Count > 1)
            {
                chkIsmarketVendor.Visible = false;
            }
            _ismarketvendor = chkIsmarketVendor.Checked = cusType[0] == -2;

            if (cboConnectionType == null)
            {
                this.cboConnectionType.SelectedValue = 0;
            }
            var lstConnectionType = DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs.ToList();
            UIHelper.SetDataSourceToComboBox(cboCustomerGroup, DBDataContext.Db.TLKP_CUSTOMER_GROUPs.Where(x => x.IS_ACTIVE).OrderBy(x => x.DESCRIPTION), "");
            this.cboCustomerGroup.SelectedIndex = 0;

            UIHelper.SetDataSourceToComboBox(cboVoltage, Lookup.GetPowerVoltage(), "");
            this.cboVoltage.SelectedIndex = 0;
        }

        //public void SetCustomer(List<TBL_CUSTOMER> customers)
        //{
        //    this.lblCustomer_.Text = customers.Count.ToString();
        //    this._customers = customers;

        //    int i = 0;
        //    var pre_custypeId = 0;
        //    foreach (var id in this._customers)
        //    {
        //        var cusTypeId = allCus.FirstOrDefault(x => x.CUSTOMER_ID == id.CUSTOMER_ID).CUSTOMER_TYPE_ID;
        //        if (i == 0)
        //        {
        //            pre_custypeId = cusTypeId;
        //        }
        //        if (cusTypeId != pre_custypeId)
        //        {
        //            chkIsmarketVendor.Visible = false;
        //            break;
        //        }
        //        i++;
        //    }
        //    foreach (var id in this._customers)
        //    {
        //        var cusTypeId = allCus.FirstOrDefault(x => x.CUSTOMER_ID == id.CUSTOMER_ID).CUSTOMER_TYPE_ID;
        //        if (cusTypeId == -2)
        //        {
        //            chkIsmarketVendor.Checked = true;
        //        }
        //        else
        //        {
        //            chkIsmarketVendor.Checked = false;
        //        }
        //        i++;
        //        _ismarketvendor = chkIsmarketVendor.Checked;
        //    }
        //}

        private void btnAdd_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            // backup last village for next change
            LastVillageCode = this.cboVillage.SelectedValue.ToString();
            int priceId = (int)this.cboPrice.SelectedValue;
            int cycleId = (int)this.cboCycle.SelectedValue;
            int groupId = (int)this.cboCustomerGroup.SelectedValue;
            string villageCode = this.cboVillage.SelectedValue.ToString();
            int voltageID = (int)this.cboVoltage.SelectedValue;
            //valid customer merges- if user want to change cycle and selected customer contain customer merges which not come with all parent and child, not allow
            if (cycleId != 0)
            {
                var selectedcusMerges = (from c in _customers
                                         where (c.INVOICE_CUSTOMER_ID != 0 || c.USAGE_CUSTOMER_ID != 0)
                                         orderby c.INVOICE_CUSTOMER_ID, c.USAGE_CUSTOMER_ID
                                         select c).ToList();

                if (selectedcusMerges.Count() > 0)
                {
                    //group by all customer merges and count numbers
                    var allMerges = allCus.Where(x => x.INVOICE_CUSTOMER_ID != 0).GroupBy(x => x.INVOICE_CUSTOMER_ID).Select(x => new { Id = x.Key, Count = x.Count() })
                                        .Union(allCus.Where(x => x.USAGE_CUSTOMER_ID != 0).GroupBy(x => x.USAGE_CUSTOMER_ID).Select(x => new { Id = x.Key, Count = x.Count() })).ToList();

                    //group by all selected customer merges and count numbers
                    var a = (from cus in selectedcusMerges
                             where cus.INVOICE_CUSTOMER_ID != 0
                             group cus by cus.INVOICE_CUSTOMER_ID into g
                             select new
                             {
                                 Id = g.Key,
                                 Count = g.Count()
                             }).ToList();
                    var b = (from cus in selectedcusMerges
                             where cus.USAGE_CUSTOMER_ID != 0
                             group cus by cus.USAGE_CUSTOMER_ID into g
                             select new
                             {
                                 Id = g.Key,
                                 Count = g.Count()
                             }).ToList();
                    var selectedMereges = a.Union(b).ToList();

                    foreach (var i in selectedMereges)
                    {
                        var valid = allMerges.Where(x => x.Id == i.Id && x.Count != i.Count);
                        if (valid.Count() > 0)
                        {
                            MsgBox.ShowInformation(Resources.MSG_INVALID_CUSTOMERGE_SELECTED, Resources.WARNING);
                            return;
                        }
                    }
                }
            }

            if (priceId == 0 && cycleId == 0 && villageCode == "0" && chkIsmarketVendor.Checked == _ismarketvendor && groupId == 0 && voltageID == 0)
            {
                return;
            }

            success = false;
            Runner.Run(updateCustomerInfo);
            if (success)
            {
                this.DialogResult = DialogResult.OK;
            }

        }
        bool success = false;

        private void updateCustomerInfo()
        {
            int priceId = (int)this.cboPrice.SelectedValue;
            int cycleId = (int)this.cboCycle.SelectedValue;
            string villageCode = this.cboVillage.SelectedValue.ToString();
            int IsMarketVendor = chkIsmarketVendor.Checked == true ? 1 : 0;
            int connectionTypeId = this.cboConnectionType.SelectedValue == null ? 0 : (int)this.cboConnectionType.SelectedValue;
            int voltageId = (int)this.cboVoltage.SelectedValue;
            int i = 0;

            foreach (var obj in this._customers)
            {
                //var c = allCus.FirstOrDefault(x => x.CUSTOMER_ID == id.CUSTOMER_ID);
                if (priceId != 0) obj.PRICE_ID = priceId;
                if (cycleId != 0) obj.BILLING_CYCLE_ID = cycleId;
                if (connectionTypeId != 0) obj.CUSTOMER_CONNECTION_TYPE_ID = connectionTypeId;
                if (voltageId != 0) obj.VOL_ID = voltageId;
                if (villageCode != "0")
                {
                    obj.VILLAGE_CODE = villageCode;
                    obj.ADDRESS = this.txtVillageText.Text;
                }
                if (IsMarketVendor == 1)
                {
                    obj.CUSTOMER_TYPE_ID = -2;
                }
                else if (IsMarketVendor == 0)
                {
                    obj.CUSTOMER_TYPE_ID = obj.CUSTOMER_TYPE_ID != -2 ? obj.CUSTOMER_TYPE_ID : 1;
                }

                //MV Customer
                if (cboConnectionType.SelectedIndex != -1)
                {
                    if (DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.BusinessMVCustomer
                    || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.IndustryMVCustomer
                    || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.BusinessTOU_MVCustomer
                    || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.IndustryTOU_MVCustomer
                    || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.LicenseeMVCustomer
                    || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.MINING_INDUSTRY_MV
                    || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.MANUFACTURING_INDUSTRY_MV
                    || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.TEXTILE_INDUSTRY_MV
                    || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.AGRICULTURE_MV
                    || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.ACTIVITIES_MV
                    || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.AGRICULTURAL_MV
                    || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.BUSINESS_MV
                    || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.PUBLIC_ADMINISTRATION_MV
                    || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.OTHER_SERVICES_MV)
                    {
                        obj.IS_MV = true;
                    }
                    else
                    {
                        obj.IS_MV = false;
                    }
                }

                DBDataContext.Db.SubmitChanges();
                Application.DoEvents();
                Runner.Instance.Text = string.Format(Properties.Resources.CHANGING_CUSTOMER_INFO, (100 * i++ / _customers.Count));
            }
            success = true;
        }

        private void cboDistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboDistrict.SelectedIndex == -1)
            {
                cboCommune.SelectedIndex = -1;
                cboVillage.SelectedIndex = -1;
            }
            string districCode = this.cboDistrict.SelectedValue.ToString();
            var districts = (
                     from l in DBDataContext.Db.TBL_LICENSE_VILLAGEs
                     join v in DBDataContext.Db.TLKP_VILLAGEs on l.VILLAGE_CODE equals v.VILLAGE_CODE
                     where l.IS_ACTIVE
                     select v.COMMUNE_CODE).Distinct();
            UIHelper.SetDataSourceToComboBox(cboCommune, DBDataContext.Db.TLKP_COMMUNEs.Where(x => x.DISTRICT_CODE == districCode && districts.Contains(x.COMMUNE_CODE)), "");
        }

        private void cboCommune_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboCommune.SelectedIndex == -1)
            {
                cboVillage.SelectedIndex = -1;
            }
            else
            {
                string communeCode = this.cboCommune.SelectedValue.ToString();
                var villages = from l in DBDataContext.Db.TBL_LICENSE_VILLAGEs
                               join v in DBDataContext.Db.TLKP_VILLAGEs on l.VILLAGE_CODE equals v.VILLAGE_CODE
                               where l.IS_ACTIVE && v.COMMUNE_CODE == communeCode
                               select v;
                UIHelper.SetDataSourceToComboBox(cboVillage, villages, "");
            }
        }

        private void cboVillage_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cboVillage.SelectedIndex == 0)
            {
                this.txtVillageText.Text = "";
                this.txtVillageText.ReadOnly = true;
            }
            else
            {
                this.txtVillageText.Text = string.Format("ភូមិ{0} ឃុំ{1}", this.cboVillage.Text, this.cboCommune.Text);
                this.txtVillageText.ReadOnly = false;
            }
        }

        private void cboCustomerGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboCustomerGroup.SelectedIndex <= 0)
            {
                cboConnectionType.SelectedIndex = -1;
                return;
            }
            UIHelper.SetDataSourceToComboBox(cboConnectionType, DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs.OrderBy(x => x.DESCRIPTION).Where(x => x.NONLICENSE_CUSTOMER_GROUP_ID == (int)cboCustomerGroup.SelectedValue));
        }

        private void cboConnectionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboConnectionType.SelectedIndex == -1)
            {
                cboPrice.SelectedIndex = -1;
                return;
            }
            var connType = DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs.FirstOrDefault(x => x.CUSTOMER_CONNECTION_TYPE_ID == (int)this.cboConnectionType.SelectedValue);
            this.cboPrice.SelectedValue = connType.PRICE_ID;
        }
    }
}