﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;

namespace EPower.Interface
{
    public partial class DialogReportSechdule : ExDialog
    {
        GeneralProcess _flag;

        TBL_REPORT_SCHEDULE _objNew = new TBL_REPORT_SCHEDULE();
        DataTable _dtRoleSchedule = new DataTable();
        DataTable _dtReportSchedule = new DataTable(); 

        public TBL_REPORT_SCHEDULE ReportSend
        {
            get { return _objNew; }
        }
        TBL_REPORT_SCHEDULE _objOld = new TBL_REPORT_SCHEDULE();


        public DialogReportSechdule(GeneralProcess flag, TBL_REPORT_SCHEDULE obj)
        {
            InitializeComponent();
            dtpEndDate.ClearValue();

            _flag = flag;
            obj._CopyTo(_objNew);
            obj._CopyTo(_objOld);

            bindSendPeriod();

            if (_flag != GeneralProcess.Insert)
            {
                read();
                if (_flag == GeneralProcess.Delete)
                {
                    this.tabSchedule.Enabled = false;
                }
            }
            else
            {
                btnChangelog.Visible = false;
            }
        }

        private void read()
        {
            cboPeriod.SelectedValue = _objNew.SEND_PERIOD_ID;

            txtSechuleName.Text = _objNew.SCHEDULE_NAME;
            nudDay.Value = _objNew.DAY_OF_MONTH == 0 ? 1 : _objNew.DAY_OF_MONTH;
            chkEndofMonth.Checked = _objNew.END_OF_MONTH;
            nudRecurs.Value = _objNew.RECURS == 0 ? 1 : _objNew.RECURS;

            //Day of Week
            chkMonday.Checked = _objNew.MONTDAY;
            chkTuesday.Checked = _objNew.TUESDAY;
            chkWednesday.Checked = _objNew.WEDNESDAY;
            chkThursday.Checked = _objNew.THURSDAY;
            chkFriday.Checked = _objNew.FRIDAY;
            chkSatursday.Checked = _objNew.SATURDAY;
            chkSunday.Checked = _objNew.SUNDAY;

            radDailyRecurs.Checked = _objNew.DAILY_RECURS;
            radEveryHours.Checked = !radDailyRecurs.Checked;
            nudEveryHours.Value = _objNew.NUMBER_OF_HOUR == 0 ? 1 : _objNew.NUMBER_OF_HOUR;
            dtpStartTime.Value = _objNew.START_TIME < UIHelper._DefaultDate ? DateTime.Now : _objNew.START_TIME;

            radIsUnlimited.Checked = _objNew.IS_UNLIMITED;
            radEndDate.Checked = !radIsUnlimited.Checked;
            dtpStartDate.Value = _objNew.START_DATE < UIHelper._DefaultDate ? DateTime.Now : _objNew.START_DATE;
            dtpEndDate.Value = _objNew.END_DATE < UIHelper._DefaultDate ? DateTime.Now : _objNew.END_DATE;
            chkResend.Checked = _objNew.IS_RESEND;
        }

        private void bindSendPeriod()
        {
            UIHelper.SetDataSourceToComboBox(cboPeriod, DBDataContext.Db.TLKP_SEND_PERIODs);

            //load Role in schedule
            _dtRoleSchedule = (from r in DBDataContext.Db.TBL_ROLEs
                               join ros in DBDataContext.Db.TBL_ROLE_SCHEDULEs.Where(x => x.SCHEDULE_ID == _objNew.SCHEDULE_ID)
                               on r.ROLE_ID equals ros.ROLE_ID into roleSche
                               from rss in roleSche.DefaultIfEmpty()
                               select new
                               {
                                   SELECTED = rss == null ? false : true,
                                   ROLE_SCHEDULE_ID = rss == null ? 0 : rss.ROLE_SCHEDULE_ID,
                                   r.ROLE_ID,
                                   r.ROLE_NAME,
                                   r.DESCRIPTION,
                                   r.IS_ACTIVE
                               })._ToDataTable();
            dgvRole.DataSource = _dtRoleSchedule;

            //load report in schedule
            _dtReportSchedule = (from re in DBDataContext.Db.TLKP_REPORTs
                                 join reS in DBDataContext.Db.TBL_REPORT_SENDs.Where(x => x.SCHEDULE_ID == _objNew.SCHEDULE_ID)
                                 on re.REPORT_ID equals reS.REPORT_ID into repSche
                                 from rep in repSche.DefaultIfEmpty()
                                 select new
                                 {
                                     SELECTED_REPORT = rep == null ? false : true,
                                     REPORT_SEND_ID = rep == null ? 0 : rep.REPORT_SEND_ID,
                                     SEND_YESTERDAY = rep == null ? false : rep.SEND_YESTERDAY,
                                     SEND_IS_RUNBILL = rep == null ? false : rep.SEND_IS_RUNBILL,
                                     IS_ENGLISH= rep.IS_ENGLISH==null?false:rep.IS_ENGLISH,
                                     re.REPORT_ID,
                                     re.REPORT_LOCAL_NAME                                     
                                 })._ToDataTable();
            DataRow dr = _dtReportSchedule.NewRow();
            dr["SELECTED_REPORT"] = false;
            dr["REPORT_SEND_ID"] = -1;
            dr["SEND_YESTERDAY"] = false;
            dr["SEND_IS_RUNBILL"] = false;
            dr["IS_ENGLISH"] = false;
            dr["REPORT_ID"] = -1;
            dr["REPORT_LOCAL_NAME"] =  Resources.ALL_REPORT;
            _dtReportSchedule.Rows.InsertAt(dr, 0); 

            dgvReport.DataSource = _dtReportSchedule;

        }

        private void cboPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboPeriod.SelectedIndex == -1)
            {
                return;
            }
            if ((int)cboPeriod.SelectedValue == (int)SendPeriod.DIALY)
            {
                sendPeriodDialy();
            }
            else if ((int)cboPeriod.SelectedValue == (int)SendPeriod.WEEKLY)
            {
                sendPeriodWeekly();
            }
            else if ((int)cboPeriod.SelectedValue == (int)SendPeriod.MONTHLY)
            {
                sendPeriodMonthly();
            }
        }

        private void sendPeriodMonthly()
        {
            lblRecurs.Text = Resources.PER_MONTH;
            grbMonthly.Enabled = true;
            grbWeekly.Enabled = false;
            foreach (CheckBox item in grbWeekly.Controls)
            {
                item.Checked = false;
            }
        }

        private void sendPeriodWeekly()
        {
            lblRecurs.Text = Resources.PER_WEEK;
            nudDay.Value = 1;
            grbMonthly.Enabled = false;
            chkEndofMonth.Checked = false;
            grbWeekly.Enabled = true;
        }

        private void sendPeriodDialy()
        {
            lblRecurs.Text = Resources.PER_DAY;
            nudDay.Value = 1;
            grbMonthly.Enabled = false;
            chkEndofMonth.Checked = false;
            grbWeekly.Enabled = false;
            foreach (CheckBox item in grbWeekly.Controls)
            {
                item.Checked = false;
            }
        }

        private void chkEndofMonth_CheckedChanged(object sender, EventArgs e)
        {
            nudDay.Enabled = !chkEndofMonth.Checked;
        }

        private void radEndDate_CheckedChanged(object sender, EventArgs e)
        {
            dtpEndDate.Enabled = radEndDate.Checked;
            if (dtpEndDate.Enabled == false)
            {
                dtpEndDate.ClearValue();
            }
            else
            {
                dtpEndDate.SetValue(DateTime.Now);
            }
        }

        private void radEveryHours_CheckedChanged(object sender, EventArgs e)
        {
            nudEveryHours.Enabled = radEveryHours.Checked;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (invalid())
            {
                return;
            }
            write();

            //if duplicate record.
            txtSechuleName.ClearAllValidation();
            if (DBDataContext.Db.IsExits(_objNew, "SCHEDULE_NAME"))
            {
                txtSechuleName.SetValidation(string.Format(Resources.MS_IS_EXISTS, lblSechuleName.Text));
                return;
            }
            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objNew);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objNew);
                    }
                    updateRoleSchedule();
                    updateReportSchedule();
                    updateReportSendHistory();
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }

        }

       

        private void updateReportSchedule()
        {
            foreach (DataRow dr in _dtReportSchedule.Rows)
            {
                //Delete report not in Schedule
                if ((bool)dr["SELECTED_REPORT"] == false && (int)dr["REPORT_SEND_ID"] > 0)
                {
                    DBDataContext.Db.TBL_REPORT_SENDs.DeleteOnSubmit(DBDataContext.Db.TBL_REPORT_SENDs
                        .FirstOrDefault(x => x.REPORT_SEND_ID == (int)dr["REPORT_SEND_ID"]));
                }
                //Update on exitst Report in Schedule
                else if ((bool)dr["SELECTED_REPORT"] == true && (int)dr["REPORT_SEND_ID"] > 0)
                {
                    TBL_REPORT_SEND objNew = DBDataContext.Db.TBL_REPORT_SENDs
                        .FirstOrDefault(x => x.REPORT_SEND_ID == (int)dr["REPORT_SEND_ID"]);
                    objNew.SEND_IS_RUNBILL = (bool)dr["SEND_IS_RUNBILL"];
                    objNew.SEND_YESTERDAY = (bool)dr["SEND_YESTERDAY"];
                    objNew.IS_ENGLISH = (bool)dr["IS_ENGLISH"];
                }
                //Insert new report in Schedule
                else if ((bool)dr["SELECTED_REPORT"] == true && (int)dr["REPORT_SEND_ID"] == 0)
                {
                    DBDataContext.Db.TBL_REPORT_SENDs.InsertOnSubmit(
                    new TBL_REPORT_SEND()
                    {
                        REPORT_ID = (int)dr["REPORT_ID"],
                        SCHEDULE_ID = _objNew.SCHEDULE_ID,
                        SEND_IS_RUNBILL = (bool)dr["SEND_IS_RUNBILL"],
                        SEND_YESTERDAY = (bool)dr["SEND_IS_RUNBILL"],
                        IS_ENGLISH=(bool)dr["IS_ENGLISH"],
                    });
                }
            }
            DBDataContext.Db.SubmitChanges();
        }

        private void updateReportSendHistory()
        {
            DBDataContext.Db.TBL_SEND_REPORT_HISTORies.DeleteAllOnSubmit(
                    DBDataContext.Db.TBL_SEND_REPORT_HISTORies.Where(
                    x => x.SCHEDULE_ID == _objNew.SCHEDULE_ID
                    && x.SEND_STATUS == false &&
                    x.IS_AFTER_RUN_BILL == false));
            DBDataContext.Db.SubmitChanges();
        }

        private void updateRoleSchedule()
        {
            foreach (DataRow dr in _dtRoleSchedule.Rows)
            {
                if ((bool)dr["SELECTED"] == false && (int)dr["ROLE_SCHEDULE_ID"] > 0)           //Delete role not in Schedule
                {
                    DBDataContext.Db.TBL_ROLE_SCHEDULEs.DeleteOnSubmit(DBDataContext.Db.TBL_ROLE_SCHEDULEs.FirstOrDefault(x => x.ROLE_SCHEDULE_ID == (int)dr["ROLE_SCHEDULE_ID"]));
                }
                else if ((bool)dr["SELECTED"] == true && (int)dr["ROLE_SCHEDULE_ID"] == 0)  //Insert new role in schedule
                {
                    DBDataContext.Db.TBL_ROLE_SCHEDULEs.InsertOnSubmit(
                    new TBL_ROLE_SCHEDULE()
                    {
                        ROLE_ID = (int)dr["ROLE_ID"],
                        SCHEDULE_ID = _objNew.SCHEDULE_ID
                    });
                }
            }
            DBDataContext.Db.SubmitChanges();
        }



        private void write()
        {
            _objNew.SCHEDULE_NAME = txtSechuleName.Text;
            _objNew.SEND_PERIOD_ID = (int)cboPeriod.SelectedValue;

            _objNew.DAY_OF_MONTH = DataHelper.ParseToInt(nudDay.Value.ToString());
            _objNew.END_OF_MONTH = chkEndofMonth.Checked;


            _objNew.MONTDAY = chkMonday.Checked;
            _objNew.TUESDAY = chkTuesday.Checked;
            _objNew.WEDNESDAY = chkWednesday.Checked;
            _objNew.THURSDAY = chkThursday.Checked;
            _objNew.FRIDAY = chkFriday.Checked;
            _objNew.SATURDAY = chkSatursday.Checked;
            _objNew.SUNDAY = chkSunday.Checked;

            _objNew.RECURS = DataHelper.ParseToInt(nudRecurs.Value.ToString());
            _objNew.DAILY_RECURS = radDailyRecurs.Checked;
            _objNew.START_TIME = dtpStartTime.Value;
            _objNew.START_DATE = dtpStartDate.Value;
            _objNew.IS_UNLIMITED = radIsUnlimited.Checked;
            _objNew.IS_RESEND = chkResend.Checked;

            _objNew.END_DATE = UIHelper._DefaultDate;
            if (radEndDate.Checked)
            {
                _objNew.END_DATE = dtpEndDate.Value;
            }
            else
            {
                _objNew.END_DATE = UIHelper._DefaultDate;
            }

            if (radEveryHours.Checked)
            {
                _objNew.NUMBER_OF_HOUR = DataHelper.ParseToInt(nudEveryHours.Value.ToString());
            }
            else
            {
                _objNew.NUMBER_OF_HOUR = 0;
            }
        }

        private bool invalid()
        {
            bool val = false;
            this.ClearAllValidation();
            if (txtSechuleName.Text.Trim() == string.Empty)
            {
                txtSechuleName.SetValidation(string.Format(Resources.REQUIRED, lblSechuleName.Text));
                val = true;
            }
            if (cboPeriod.SelectedIndex == -1)
            {
                cboPeriod.SetValidation(string.Format(Resources.REQUIRED, lblPeriod.Text));
                return true;
            }
            if ((int)cboPeriod.SelectedValue == (int)SendPeriod.WEEKLY)
            {
                val = inValidWeekly();
            }

            if (nudDay.Value == 0 && nudDay.Enabled)
            {
                //{0} ត្រូវធំជាងសូន្យ(០)
                
                nudDay.SetValidation(Resources.REQUEST_GRATER_THAN_ZERO);
                val = true;
            }
            if (nudEveryHours.Value == 0 && nudEveryHours.Enabled)
            {
                nudEveryHours.SetValidation(Resources.REQUEST_GRATER_THAN_ZERO);
                val = true;
            }
            if (nudRecurs.Value == 0)
            {
                nudRecurs.SetValidation(Resources.REQUEST_GRATER_THAN_ZERO);
                val = true;
            }


            if (radEndDate.Checked && dtpEndDate.Value.Date < dtpStartDate.Value.Date)
            {
                //ថ្ងៃបញ្ចប់ មិនអាចតូចជាង ថ្ងៃចាប់ផ្តើម!
                dtpEndDate.SetValidation(string.Format(Resources.MS_VALIDATE_START_END_DATE, lblStartDate.Text, radEndDate.Text));
                val = true;
                
            }

            return val;

        }

        private bool inValidMonthly()
        {

            return false;
        }

        private bool inValidWeekly()
        {
            foreach (CheckBox item in grbWeekly.Controls)
            {
                if (item.Checked)
                {
                    return false;
                }
            }
            //សូមជ្រើសថ្ងៃមួយ រឺច្រើនក្នុងចំនោមថ្ងៃដែលមានក្នុងសប្តាហ៍!
            grbWeekly.SetValidation(Resources.REQUIRED_SELECT_DAY_OF_WEEK);
            return true;
        }

        private void dgvRole_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            dgvRole.Rows[e.RowIndex].Cells["SELECTED"].Value = !(bool)dgvRole.Rows[e.RowIndex].Cells["SELECTED"].Value;
        }

        private void DialogReportSechdule_Load(object sender, EventArgs e)
        {
             //InsertAllReportToDatabase();             
        }

        private static void InsertAllReportToDatabase()
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            //create a list for the namespaces
            List<string> namespaceList = new List<string>();
            //create a list that will hold all the classes
            //the suplied namespace is executing
            List<string> returnList = new List<string>();

            foreach (Type type in asm.GetTypes())
            {
                if (type.Namespace == "EPower.Report.CrystalReport")
                    namespaceList.Add(type.Name);
            }
            //now loop through all the classes returned above and add
            //them to our classesName list
            foreach (String className in namespaceList.Where(x => !x.ToLower().StartsWith("cached")))
                returnList.Add(className);



            foreach (string item in returnList)
            {
                DBDataContext.Db.TLKP_REPORTs.InsertOnSubmit(
                    new TLKP_REPORT()
                    {
                        REPORT_NAME = item,
                        REPORT_LOCAL_NAME = "",
                    });
            }
            DBDataContext.Db.SubmitChanges();
        }

        private void dgvReport_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            if (e.ColumnIndex == dgvReport.Columns["SELECTED_REPORT"].Index)
            {
                dgvReport.Rows[e.RowIndex].Cells["SELECTED_REPORT"].Value = !(bool)dgvReport.Rows[e.RowIndex].Cells["SELECTED_REPORT"].Value;
                if ((int)dgvReport["REPORT_SEND_ID", e.RowIndex].Value == -1)
                {
                    foreach (DataGridViewRow item in dgvReport.Rows)
                    {
                        item.Cells[e.ColumnIndex].Value = dgvReport.Rows[e.RowIndex].Cells["SELECTED_REPORT"].Value;
                    }
                }
            }
            if (e.ColumnIndex == dgvReport.Columns["SEND_YESTERDAY"].Index)
            {
                dgvReport.Rows[e.RowIndex].Cells["SEND_YESTERDAY"].Value = !(bool)dgvReport.Rows[e.RowIndex].Cells["SEND_YESTERDAY"].Value;
                if ((int)dgvReport["REPORT_SEND_ID", e.RowIndex].Value == -1)
                {
                    foreach (DataGridViewRow item in dgvReport.Rows)
                    {
                        item.Cells[e.ColumnIndex].Value = dgvReport.Rows[e.RowIndex].Cells["SEND_YESTERDAY"].Value;
                    }
                }
            }
            if (e.ColumnIndex == dgvReport.Columns["SEND_IS_RUNBILL"].Index)
            {
                dgvReport.Rows[e.RowIndex].Cells["SEND_IS_RUNBILL"].Value = !(bool)dgvReport.Rows[e.RowIndex].Cells["SEND_IS_RUNBILL"].Value;
                if ((int)dgvReport["REPORT_SEND_ID", e.RowIndex].Value == -1)
                {
                    foreach (DataGridViewRow item in dgvReport.Rows)
                    {
                        item.Cells[e.ColumnIndex].Value = dgvReport.Rows[e.RowIndex].Cells["SEND_IS_RUNBILL"].Value;
                    }
                }
            }
            if (e.ColumnIndex == dgvReport.Columns["IS_ENGLISH"].Index)
            {
                dgvReport.Rows[e.RowIndex].Cells["IS_ENGLISH"].Value = !(bool)dgvReport.Rows[e.RowIndex].Cells["IS_ENGLISH"].Value;
                if ((int)dgvReport["REPORT_SEND_ID", e.RowIndex].Value == -1)
                {
                    foreach (DataGridViewRow item in dgvReport.Rows)
                    {
                        item.Cells[e.ColumnIndex].Value = dgvReport.Rows[e.RowIndex].Cells["IS_ENGLISH"].Value;
                    }
                }
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }

        private void Khmer_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void English_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

     
    }
}

   
    

    

    
