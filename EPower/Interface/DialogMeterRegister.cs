﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogMeterRegister : ExDialog
    {
        bool success = false;

        int numNewMeter = 0;
        int numOldMeter = 0;
        List<string> LisError = new List<string>();
        string[] lines = null;

        public DialogMeterRegister()
        {
            InitializeComponent();
            bind();
            this.btnOK.Enabled = false;
        }
        private void bind()
        {
            UIHelper.SetDataSourceToComboBox(this.cboMeterType, DBDataContext.Db.TBL_METER_TYPEs.Where(row => row.IS_ACTIVE));
        }
        private void btnExport_Click(object sender, EventArgs e)
        {
            this.success = false;

            using (TransactionScope tran = new TransactionScope())
            {
                DateTime now = DBDataContext.Db.GetSystemDate();

                numNewMeter = 0;
                numOldMeter = 0;
                LisError = new List<string>();
                // check 
                for (int i = 2; i < lines.Length; i++)
                {
                    string line = lines[i];
                    string[] col = line.Split('-');
                    var m = new
                    {
                        METER_CODE = Method.FormatMeterCode(col[0]),
                        IS_DIGITAL = int.Parse(col[1]) == 1,
                        REGISTER_CODE = col[2]
                    };

                    if (Method.GetMETER_REGCODE(m.METER_CODE, m.IS_DIGITAL) != m.REGISTER_CODE)
                    {
                        LisError.Add(col[0]);
                        continue;
                    }

                    TBL_METER objMeter = DBDataContext.Db.TBL_METERs.FirstOrDefault(t => t.METER_CODE == m.METER_CODE);
                    if (objMeter == null)
                    {
                        objMeter = new TBL_METER()
                        {
                            CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                            CREATE_ON = DBDataContext.Db.GetSystemDate(),
                            METER_CODE = m.METER_CODE,
                            METER_TYPE_ID = this.cboMeterType.SelectedIndex == -1 ? 1 : (int)this.cboMeterType.SelectedValue,
                            REGISTER_CODE = m.REGISTER_CODE,
                            MULTIPLIER = 1,
                            STATUS_ID = (int)MeterStatus.Stock,
                            IS_DIGITAL = m.IS_DIGITAL
                        };
                        DBDataContext.Db.TBL_METERs.InsertOnSubmit(objMeter);
                        DBDataContext.Db.SubmitChanges();
                        numNewMeter++;
                    }
                    else
                    {
                        objMeter.REGISTER_CODE = m.REGISTER_CODE;
                        objMeter.METER_TYPE_ID = this.cboMeterType.SelectedIndex == -1 ? 1 : (int)this.cboMeterType.SelectedValue;
                        objMeter.IS_DIGITAL = m.IS_DIGITAL;
                        numOldMeter++;
                    }
                }
                if (numNewMeter > 0)
                {
                    DBDataContext.Db.TBL_STOCK_TRANs.InsertOnSubmit(new TBL_STOCK_TRAN()
                    {
                        CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                        CREATE_ON = DBDataContext.Db.GetSystemDate(),
                        FROM_STOCK_TYPE_ID = (int)StockType.None,
                        TO_STOCK_TYPE_ID = (int)StockType.Stock,
                        ITEM_ID = (int)this.cboMeterType.SelectedValue,
                        ITEM_TYPE_ID = (int)StockItemType.Meter,
                        QTY = numNewMeter,
                        REMARK = this.Text,
                        UNIT_PRICE = 0,
                        STOCK_TRAN_TYPE_ID = (int)StockTranType.StockIn
                    });
                }

                DBDataContext.Db.SubmitChanges();
                tran.Complete();
                success = true;
            }

            if (success)
            {
                this.txtError.Text = this.LisError.Count.ToString();
                this.txtNew.Text = this.numNewMeter.ToString();
                this.txtOld.Text = this.numOldMeter.ToString();
                MsgBox.ShowInformation(this.LisError.Count == 0 ? Resources.MS_METER_REGISTER_SUCCESS : Resources.MS_REGISTER_METER_IS_COMPLETE_WITH_ERROR);
            }
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            this.txtNew.Text = "";
            this.txtOld.Text = "";
            this.txtError.Text = "";
            this.success = false;
            this.btnOK.Enabled = false;


            OpenFileDialog diag = new OpenFileDialog();
            diag.Filter = "EPower File|*.epf";
            if (diag.ShowDialog() == DialogResult.OK)
            {
                this.txtFileName.Text = diag.FileName;
            }
            else
            {
                return;
            }



            lines = File.ReadAllLines(this.txtFileName.Text);

            // check correct header file
            if (lines[0] != Method.GetFILE_HEADER(ItemType.POSTPAID_METER)
                && lines[0] != Method.GetFILE_HEADER(ItemType.PREPAID_METER))
            {
                MsgBox.ShowInformation(Resources.MS_FILE_IS_NOT_CORRECT_FORMAT);
                return;
            }
            //TBL_METER_TYPE mt = DBDataContext.Db.TBL_METER_TYPEs.FirstOrDefault(row => row.METER_TYPE_CODE.ToUpper() == lines[1].ToUpper());
            //if (mt == null)
            //{
            //    mt = new TBL_METER_TYPE()
            //    {
            //        IS_ACTIVE = true,
            //        IS_POST_PAID = lines[0] == Logic.Method.GetFILE_HEADER(ItemType.POSTPAID_METER),
            //        MAX_USAGE = 999999,
            //        METER_AMP_ID = 1,
            //        METER_CONST_ID = 1,
            //        METER_IMAX = 4,
            //        METER_PHASE_ID=1,
            //        METER_TYPE_CODE = lines[1],
            //        METER_TYPE_ID=1,
            //        METER_TYPE_NAME = lines[1],
            //        METER_VERSION =1,
            //        METER_VOL_ID=1
            //    };
            //    DBDataContext.Db.Insert(mt);
            //    bind();
            //}
            //this.cboMeterType.SelectedValue = mt.METER_TYPE_ID;
            this.cboMeterType.SelectedIndex = 0;
            this.btnOK.Enabled = true;
        }
    }
}
