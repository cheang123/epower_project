﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;


namespace EPower.Interface
{
    public partial class PageCustomerActivateList : Form
    {
        #region Constructor
        public PageCustomerActivateList()
        {
            InitializeComponent();
            this.btnADD_SERVICE.Enabled = SoftTech.Security.Logic.Login.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_REGISTER_FIXCHARGE);
            this.btnADD.Enabled = SoftTech.Security.Logic.Login.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_REGISTER_INSERT);
            this.btnDEPOSIT.Visible = !DataHelper.ParseToBoolean(Method.Utilities[Utility.DEPOSIT_IS_IGNORED]);
            UIHelper.DataGridViewProperties(dgv);

            BindData();
        }
        #endregion Constructor

        #region Method
        public void BindData()
        {
            int customerID = 0;
            if (this.dgv.SelectedRows.Count > 0)
            {
                customerID = (int)this.dgv.SelectedRows[0].Cells["CUSTOMER_ID"].Value;
            }

            string strSearch = this.txtSearchCus.Text.ToLower();
            dgv.DataSource = from cus in DBDataContext.Db.TBL_CUSTOMERs.Where(x => x.STATUS_ID == (int)CustomerStatus.Pending && x.IS_POST_PAID)
                                 //join ct in DBDataContext.Db.TBL_CUSTOMER_TYPEs on cus.CUSTOMER_TYPE_ID equals ct.CUSTOMER_TYPE_ID
                             join cct in DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs on cus.CUSTOMER_CONNECTION_TYPE_ID equals cct.CUSTOMER_CONNECTION_TYPE_ID
                             join cg in DBDataContext.Db.TLKP_CUSTOMER_GROUPs on cct.NONLICENSE_CUSTOMER_GROUP_ID equals cg.CUSTOMER_GROUP_ID
                             join a in DBDataContext.Db.TBL_AREAs on cus.AREA_ID equals a.AREA_ID
                             join am in DBDataContext.Db.TBL_AMPAREs on cus.AMP_ID equals am.AMPARE_ID
                             join p in DBDataContext.Db.TBL_PHASEs on cus.PHASE_ID equals p.PHASE_ID
                             join xc in //Find currency type of customer use for New Connection & Deposit
                                 (from cu in DBDataContext.Db.TBL_CUSTOMERs.Where(x => x.IS_POST_PAID && x.STATUS_ID == (int)CustomerStatus.Pending)
                                  from cr in DBDataContext.Db.TLKP_CURRENCies
                                  where DBDataContext.Db.TBL_CUS_DEPOSITs.Where(x => x.CURRENCY_ID == cr.CURRENCY_ID && x.CUSTOMER_ID == cu.CUSTOMER_ID).Count() > 0
                                  || DBDataContext.Db.TBL_INVOICEs.Where(x => x.CURRENCY_ID == cr.CURRENCY_ID && x.CUSTOMER_ID == cu.CUSTOMER_ID &&
                                      DBDataContext.Db.TBL_INVOICE_DETAILs.Where(y => y.INVOICE_ID == x.INVOICE_ID && y.INVOICE_ITEM_ID == (int)InvoiceItem.NewConnection).Count() > 0).Count() > 0
                                  select new
                                  {
                                      cu.CUSTOMER_ID,
                                      cr.CURRENCY_ID
                                  }).Distinct() on cus.CUSTOMER_ID equals xc.CUSTOMER_ID into tCus
                             from xCus in tCus.DefaultIfEmpty()
                             join x in
                                 (from cd in DBDataContext.Db.TBL_CUS_DEPOSITs.Where(x => x.IS_PAID)
                                  join c in DBDataContext.Db.TLKP_CURRENCies on cd.CURRENCY_ID equals c.CURRENCY_ID
                                  group cd by new { cd.CUSTOMER_ID, c.CURRENCY_ID, c.CURRENCY_SING } into xD
                                  select new
                                  {
                                      xD.Key.CUSTOMER_ID,
                                      xD.Key.CURRENCY_ID,
                                      xD.Key.CURRENCY_SING,
                                      AMOUNT = xD.Sum(x => x.AMOUNT)
                                  }) on new { cus.CUSTOMER_ID, xCus.CURRENCY_ID } equals new { x.CUSTOMER_ID, x.CURRENCY_ID } into tDep
                             from xDep in tDep.DefaultIfEmpty()
                             join z in
                                 (from inv in DBDataContext.Db.TBL_INVOICEs.Where(x => DBDataContext.Db.TBL_INVOICE_DETAILs.Count(id => id.INVOICE_ID == x.INVOICE_ID && id.INVOICE_ITEM_ID == (int)InvoiceItem.NewConnection) > 0)
                                  join ic in DBDataContext.Db.TLKP_CURRENCies on inv.CURRENCY_ID equals ic.CURRENCY_ID
                                  group inv by new { inv.CUSTOMER_ID, ic.CURRENCY_ID, ic.CURRENCY_SING } into xI
                                  select new
                                  {
                                      xI.Key.CUSTOMER_ID,
                                      xI.Key.CURRENCY_ID,
                                      xI.Key.CURRENCY_SING,
                                      SETTLE_AMOUNT = xI.Sum(x => x.SETTLE_AMOUNT)
                                  }) on new { cus.CUSTOMER_ID, xCus.CURRENCY_ID } equals new { z.CUSTOMER_ID, z.CURRENCY_ID } into tInv
                             from xInv in tInv.DefaultIfEmpty()
                             where cus.STATUS_ID == (int)CustomerStatus.Pending
                                    && cus.IS_POST_PAID
                                    && ((cus.CUSTOMER_CODE + " " + cus.LAST_NAME_KH + " " + cus.LAST_NAME_KH + " " + cus.LAST_NAME + " " + cus.FIRST_NAME_KH + " " + cus.PHONE_1 + cus.PHONE_2).ToLower().Contains(strSearch))
                             orderby cus.CUSTOMER_ID
                             select new
                             {
                                 cus.CUSTOMER_ID,
                                 cus.CUSTOMER_CODE,
                                 CUSTOMER_NAME = cus.LAST_NAME_KH + " " + cus.FIRST_NAME_KH,
                                 CUSTOMER_CONNECTION_TYPE_NAME = cg.CUSTOMER_GROUP_ID == 1 || cg.CUSTOMER_GROUP_ID == 2 || cg.CUSTOMER_GROUP_ID == 3 ? cg.CUSTOMER_GROUP_NAME + " (" + cct.CUSTOMER_CONNECTION_TYPE_NAME + ")" : cct.CUSTOMER_CONNECTION_TYPE_NAME,
                                 a.AREA_NAME,
                                 am.AMPARE_NAME,
                                 p.PHASE_NAME,
                                 CURRENCY_SING = xDep != null ? xDep.CURRENCY_SING : xInv != null ? xInv.CURRENCY_SING : "",
                                 DEPOSIT = xDep != null ? xDep.AMOUNT : 0,
                                 CONNECTION_FEE = xInv != null ? xInv.SETTLE_AMOUNT : 0
                             };




            foreach (DataGridViewRow row in this.dgv.Rows)
            {
                if (customerID == (int)row.Cells[this.CUSTOMER_ID.Name].Value)
                {
                    this.dgv.CurrentCell = row.Cells[this.CUSTOMER_CODE.Name];
                    row.Selected = true;
                    break;
                }
            }
        }
        #endregion

        #region Event

        private void btnActive_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                int intCusId = int.Parse(dgv.SelectedRows[0].Cells[CUSTOMER_ID.Name].Value.ToString());

                int numDeposit = DBDataContext.Db.TBL_CUS_DEPOSITs.Count(row => row.CUSTOMER_ID == intCusId && row.DEPOSIT_ACTION_ID == (int)DepositAction.AddDeposit);

                if (!DataHelper.ParseToBoolean(Method.Utilities[Utility.DEPOSIT_IS_IGNORED]) && numDeposit <= 0)
                {
                    MsgBox.ShowInformation(Resources.MS_CUSTOMER_NO_DEPOSIT);
                    return;
                }

                TBL_CUSTOMER objCus = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(c => c.CUSTOMER_ID == intCusId);
                DialogCustomerActivate objDiag = new DialogCustomerActivate(objCus);
                objDiag.ShowDialog();
                if (objDiag.DialogResult == DialogResult.OK)
                {
                    BindData();
                }
            }
        }

        private void btnBlock_Click(object sender, EventArgs e)
        {
            if (this.dgv.SelectedRows.Count > 0)
            {
                int intCusId = int.Parse(dgv.SelectedRows[0].Cells[CUSTOMER_ID.Name].Value.ToString());

                // check merge customer
                var obj = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == intCusId);
                if (obj.USAGE_CUSTOMER_ID == obj.CUSTOMER_ID || obj.INVOICE_CUSTOMER_ID == obj.CUSTOMER_ID)
                {
                    MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE_PARENT_OF_MERGE_CUSTOMER);
                    return;
                }

                var qryDeposit = DBDataContext.Db.TBL_CUS_DEPOSITs.Where(c => c.CUSTOMER_ID == intCusId && c.IS_PAID);
                if (qryDeposit.Count() > 0)
                {
                    if (qryDeposit.Sum(d => d.AMOUNT) > 0)
                    {
                        MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE_CUSTOMER_NEED_TO_APPLY_DEPOSIT_FIRST);
                        return;
                    }
                }

                if (MsgBox.ShowQuestion(Resources.MSQ_REMOVE_CUSTOMER, Resources.CLOSE_CUSTOMER) == DialogResult.Yes)
                {
                    try
                    {
                        using (TransactionScope tran = new TransactionScope())
                        {
                            TBL_CUSTOMER tmp = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(c => c.CUSTOMER_ID == intCusId);
                            TBL_CUSTOMER objNew = new TBL_CUSTOMER();
                            TBL_CUSTOMER objOld = new TBL_CUSTOMER();

                            tmp._CopyTo(objOld);
                            tmp._CopyTo(objNew);

                            // update customer.
                            objNew.FIRST_NAME_KH = objNew.FIRST_NAME_KH + " ";
                            objNew.LAST_NAME_KH = objNew.LAST_NAME_KH + " ";
                            objNew.STATUS_ID = (int)CustomerStatus.Cancelled;
                            DBDataContext.Db.Update(objOld, objNew);

                            // add a keep track update change status one record.
                            DBDataContext.Db.TBL_CUS_STATUS_CHANGEs.InsertOnSubmit(new TBL_CUS_STATUS_CHANGE()
                            {
                                CHNAGE_DATE = DBDataContext.Db.GetSystemDate(),
                                CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                                CUSTOMER_ID = tmp.CUSTOMER_ID,
                                NEW_STATUS_ID = objNew.STATUS_ID,
                                OLD_STATUS_ID = tmp.STATUS_ID,
                                INVOICE_ID = 0
                            });
                            DBDataContext.Db.SubmitChanges();

                            tran.Complete();
                        }
                        BindData();
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
            }
        }

        private void txtSearchCus_QuickSearch(object sender, EventArgs e)
        {
            BindData();
        }

        private void btnNewDeposit_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {

                int intCustomerID = DataHelper.ParseToInt(dgv.SelectedRows[0].Cells[CUSTOMER_ID.Name].Value.ToString());

                TBL_CUSTOMER objCus = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == intCustomerID);

                DialogDepositAdd diag = null;
                TBL_CUS_DEPOSIT objDeposit = DBDataContext.Db.TBL_CUS_DEPOSITs.FirstOrDefault(row => !row.IS_PAID && row.CUSTOMER_ID == intCustomerID);
                if (objDeposit != null)
                {
                    diag = new DialogDepositAdd(objCus, DepositAction.AddDeposit, GeneralProcess.Update, objDeposit);
                }
                else
                {
                    diag = new DialogDepositAdd(objCus, DepositAction.AddDeposit, GeneralProcess.Insert, objDeposit);
                }
                diag.ShowDialog();
                this.BindData();
                PostingLogic.Instance.PostToPointerBg(DateTime.Now.Date, DateTime.Now.Date.AddDays(1).AddSeconds(-1));
            }
        }

        private void btnAddCharge_Click(object sender, EventArgs e)
        {
            if (this.dgv.SelectedRows.Count == 0)
            {
                return;
            }

            if (Login.CurrentCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_ADD_SERVICE, string.Empty) == DialogResult.Yes)
                    new DialogOpenCashDrawer().ShowDialog();
                if (Login.CurrentCashDrawer == null)
                    return;
            }

            int id = (int)this.dgv.SelectedRows[0].Cells[this.CUSTOMER_ID.Name].Value;
            TBL_CUSTOMER obj = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == id);

            if (obj != null)
            {
                (new DialogCustomerCharge(obj, GeneralProcess.Insert)).ShowDialog();
                this.BindData();
                PostingLogic.Instance.PostToPointerBg(DateTime.Now.Date, DateTime.Now.Date.AddDays(1).AddSeconds(-1));
            }
        }


        private void btnReport_Click(object sender, EventArgs e)
        {
            if (dgv.Rows.Count <= 0) return;
            int id = (int)this.dgv.SelectedRows[0].Cells[this.CUSTOMER_ID.Name].Value;
            CrystalReportHelper ch = new CrystalReportHelper("ReportCustomerAgreement.rpt");
            ch.SetParameter("@CUSTOMER_ID", id);
            ch.ViewReport("");
        }

        private void btnConnection_Click(object sender, EventArgs e)
        {
            if (dgv.Rows.Count <= 0) return;
            int id = (int)this.dgv.SelectedRows[0].Cells[this.CUSTOMER_ID.Name].Value;
            CrystalReportHelper ch = new CrystalReportHelper("ReportCustomerConnection.rpt");
            ch.SetParameter("@CUSTOMER_ID", id);
            ch.ViewReport("");
        }

        private void btnADD_Click(object sender, EventArgs e)
        {
            DialogCustomer diag = new DialogCustomer(GeneralProcess.Insert, new TBL_CUSTOMER());
            if (diag.ShowDialog(this) == DialogResult.OK)
            {
                BindData();
                UIHelper.SelectRow(dgv, diag.Object.CUSTOMER_ID);
            }
        }

        private void exButton1_Click(object sender, EventArgs e)
        {
            if (this.dgv.SelectedRows.Count == 0)
            {
                return;
            }

            if (Login.CurrentCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_ADD_SERVICE, string.Empty) == DialogResult.Yes)
                    new DialogOpenCashDrawer().ShowDialog();
                if (Login.CurrentCashDrawer == null)
                    return;
            }

            int id = (int)this.dgv.SelectedRows[0].Cells[this.CUSTOMER_ID.Name].Value;
            TBL_CUSTOMER obj = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == id);

            if (obj != null)
            {
                DialogCustomerCharge diaCus = new DialogCustomerCharge(obj, GeneralProcess.Insert);
                diaCus.InvoiceItemChanged((int)InvoiceItem.NewConnection);
                diaCus.ShowDialog();
                this.BindData();
                PostingLogic.Instance.PostToPointerBg(DateTime.Now.Date, DateTime.Now.Date.AddDays(1).AddSeconds(-1));
            }
        }

        #endregion Event
    }
}
