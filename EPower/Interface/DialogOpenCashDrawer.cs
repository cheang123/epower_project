﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogOpenCashDrawer : ExDialog
    {
        public bool isBank = false;
        TBL_USER_CASH_DRAWER _obj = null;
        public DialogOpenCashDrawer()
        {
            InitializeComponent();

            this.bindCashDrawer();
            this.dtpDate.Value = DBDataContext.Db.GetSystemDate();
            this._obj = Login.CurrentCashDrawer;

            this.dgv.ReadOnly = false;
            this.CASH_FLOAT.ReadOnly = false;
            this.CURRENCY_SING_.ReadOnly = true;

            this.read();
        }

        private void bindCashDrawer()
        {
            if (Login.CurrentCashDrawer == null)
            {
                DataTable dt = Login.CashDrawersToOpen._ToDataTable();
                DataRow dr = dt.NewRow();
                dr[0] = 0;
                dr[1] = dt.Rows.Count > 0 ? Resources.SELECT_CASH_DRAWER : Resources.NO_CASH_DRAWER;
                dt.Rows.InsertAt(dr, 0);
                UIHelper.SetDataSourceToComboBox(this.cboCashDrawer, dt);
            }
            else
            {
                // only one cash drawer in combobox.
                UIHelper.SetDataSourceToComboBox(this.cboCashDrawer, DBDataContext.Db.TBL_CASH_DRAWERs.Where(row => row.CASH_DRAWER_ID == Login.CurrentCashDrawer.CASH_DRAWER_ID));
            }
        }

        private void write()
        {
            this._obj = new TBL_USER_CASH_DRAWER()
            {
                CASH_DRAWER_ID = (int)this.cboCashDrawer.SelectedValue,
                CLOSED_DATE = UIHelper._DefaultDate,
                CREATE_BY = SoftTech.Security.Logic.Login.CurrentLogin.LOGIN_NAME,
                IS_ACTIVE = true,
                OPEN_DATE = this.dtpDate.Value,
                USER_CASH_DRAWER_ID = 0,
                USER_ID = SoftTech.Security.Logic.Login.CurrentLogin.LOGIN_ID,
                NOTE = txtNote.Text.Trim(),
                IS_CLOSED = false,
                IS_TAKE_OVER = false
            };
        }

        private void read()
        {
            if (this._obj != null)
            {
                // begin write data
                this.cboCashDrawer.SelectedValue = this._obj.CASH_DRAWER_ID;
                this.dtpDate.Value = this._obj.OPEN_DATE;

                UIHelper.SetEnabled(this, false);
                this.btnOK.Enabled = false;


            }
            var userCashDrawerId = this._obj == null ? 0 : this._obj.USER_CASH_DRAWER_ID;
            var q = from c in DBDataContext.Db.TLKP_CURRENCies
                    join d in DBDataContext.Db.TBL_USER_CASH_DRAWER_DETAILs.Where(x => x.USER_CASH_DRAWER_ID == userCashDrawerId) on c.CURRENCY_ID equals d.CURRENCY_ID into dj
                    from j in dj.DefaultIfEmpty()
                    select new
                    {
                        c.CURRENCY_ID,
                        c.CURRENCY_SING,
                        CASH_FLOAT = j == null ? 0.0m : j.CASH_FLOAT
                    };
            this.dgv.DataSource = q._ToDataTable();
        }

        private bool invalid()
        {
            bool invalid = false;
            this.ClearAllValidation();
            if (this.cboCashDrawer.SelectedIndex < 1)
            {
                this.cboCashDrawer.SetValidation(String.Format(Resources.REQUIRED, this.lblCASH_DRAWER.Text));
                invalid = true;
            }
            return invalid;
        }

        private bool checkBank()
        {
            bool invalid = true;
            if (DBDataContext.Db.TBL_CASH_DRAWERs.Where(x => x.CASH_DRAWER_ID == (int)this.cboCashDrawer.SelectedValue && x.IS_BANK != isBank).Any())
            {
                if (MsgBox.ShowQuestion(Properties.Resources.MS_THIS_CASH_DRAWER_IS_NOT_FOR_BANK, Resources.INFORMATION) == DialogResult.Yes)
                    invalid = false;
            }
            return invalid;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.invalid()) return;
                if (isBank) { if (this.checkBank()) return; }
                txtNote.Focus(); // For lose focus of datagridview cell editing
                using (var tran = new TransactionScope())
                {
                    this.write();
                    DBDataContext.Db.Insert(this._obj);
                    foreach (DataGridViewRow row in this.dgv.Rows)
                    {
                        var currencyId = (int)row.Cells[this.CURRENCY_ID.Name].Value;
                        var cashFloat = (decimal)row.Cells[this.CASH_FLOAT.Name].Value;
                        var objDetail = DBDataContext.Db.TBL_USER_CASH_DRAWER_DETAILs.FirstOrDefault(x => x.USER_CASH_DRAWER_ID == this._obj.USER_CASH_DRAWER_ID && x.CURRENCY_ID == currencyId);
                        if (objDetail == null)
                        {
                            objDetail = new TBL_USER_CASH_DRAWER_DETAIL();
                            objDetail.USER_CASH_DRAWER_ID = this._obj.USER_CASH_DRAWER_ID;
                            objDetail.CURRENCY_ID = currencyId;
                            objDetail.CASH_FLOAT = cashFloat;
                            DBDataContext.Db.Insert(objDetail);
                            //DBDataContext.Db.TBL_USER_CASH_DRAWER_DETAILs.InsertOnSubmit(objDetail);
                            //DBDataContext.Db.SubmitChanges();
                        }
                        else
                        {
                            objDetail.CASH_FLOAT = cashFloat;
                        }
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void cboCashDrawer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cboCashDrawer.SelectedIndex == -1) return;
            TBL_CASH_DRAWER obj = DBDataContext.Db.TBL_CASH_DRAWERs.FirstOrDefault(row => row.CASH_DRAWER_ID == (int)this.cboCashDrawer.SelectedValue);
            if (obj == null) return;


            TLKP_CURRENCY objCurrency = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(row => row.CURRENCY_ID == obj.CURRENCY_ID);
            if (objCurrency == null) return;

            //this.txtStartCash.Text = UIHelper.FormatCurrency(obj.DEFAULT_FLOATING_CASH, objCurrency.CURRENCY_ID);
            //this.lblCurrency.Text = objCurrency.CURRENCY_NAME; 
        }

        private void txtStartCash_TextChanged(object sender, EventArgs e)
        {
            //this.lblText.Text = DataHelper.NumberToWord(DataHelper.ParseToDecimal(this.txtStartCash.Text));
        }

        private void InputEnglish(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void InputKhmer(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void dgv_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            DataGridViewTextBoxEditingControl tb = (DataGridViewTextBoxEditingControl)e.Control;
            tb.KeyPress += new KeyPressEventHandler(dataGridViewTextBox_KeyPress);
            e.Control.KeyPress += new KeyPressEventHandler(dataGridViewTextBox_KeyPress);
        }

        private void dataGridViewTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        }

        private void dgv_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }

        private void dgv_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1) return;
            var row = this.dgv.Rows[e.RowIndex];
            if (row.Cells[e.ColumnIndex].Value.ToString() == "" || row.Cells[e.ColumnIndex].Value.ToString() == ".")
            {
                row.Cells[e.ColumnIndex].Value = 0;
            }
        }
    }
}
