﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogCloseCashDrawer : ExDialog
    {
        TBL_USER_CASH_DRAWER _objUserCashDrawer = null;
        TBL_CASH_DRAWER _objCashDrawer = null;
        DataTable dt = new DataTable();
        DataTable olddt = new DataTable();
        DataTable dtMoney = new DataTable();
        bool isCheck = false;

        public DialogCloseCashDrawer()
        {
            InitializeComponent();

            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("REPORT_ID", typeof(int));
            dtReport.Columns.Add("REPORT_NAME", typeof(string));

            DataRow drReportTotal = dtReport.NewRow();
            drReportTotal["REPORT_ID"] = 1;
            drReportTotal["REPORT_NAME"] = Resources.REPORT_CASH_DIALY_TOTAL;
            dtReport.Rows.Add(drReportTotal);

            DataRow drReportDetail = dtReport.NewRow();
            drReportDetail["REPORT_ID"] = 2;
            drReportDetail["REPORT_NAME"] = Resources.REPORT_CASH_DIALY_DETAIL;
            dtReport.Rows.Add(drReportDetail);

            DataRow drReportSummary = dtReport.NewRow();
            drReportSummary["REPORT_ID"] = 3;
            drReportSummary["REPORT_NAME"] = Resources.REPORT_CASH_DIALY_SUMMARY;
            dtReport.Rows.Add(drReportSummary);

            DataRow drReportCountCash = dtReport.NewRow();
            drReportCountCash["REPORT_ID"] = 4;
            drReportCountCash["REPORT_NAME"] = Resources.REPORT_COUNT_CASH;
            dtReport.Rows.Add(drReportCountCash);

            DataRow drReportUserCashDrawerDaily = dtReport.NewRow();
            drReportUserCashDrawerDaily["REPORT_ID"] = 5;
            drReportUserCashDrawerDaily["REPORT_NAME"] = Resources.REPORT_USER_CASH_DRAWER_DAILY;
            dtReport.Rows.Add(drReportUserCashDrawerDaily);

            UIHelper.SetDataSourceToComboBox(cboReport, dtReport);

            this.dgv.ReadOnly = false;
            this.CURRENCY_SING_.ReadOnly = true;
            this.CASH_FLOAT.ReadOnly = true;
            this.TOTAL_CASH.ReadOnly = true;
            this.ACTUAL_CASH.ReadOnly = false;
            this.SURPLUS_CASH.ReadOnly = true;


            bindPrinter();

            // find for cash drawer.
            DateTime now = DBDataContext.Db.GetSystemDate();
            this._objUserCashDrawer = Login.CurrentCashDrawer;
            if (this._objUserCashDrawer != null)
            {
                this._objCashDrawer = DBDataContext.Db.TBL_CASH_DRAWERs.FirstOrDefault(row => row.CASH_DRAWER_ID == this._objUserCashDrawer.CASH_DRAWER_ID);
            }
            this.txtCloseDate.Value = now;
            // if no cashdrawer is open then lock user edit.
            if (this._objCashDrawer != null)
            {
                foreach (var d in DBDataContext.Db.TBL_USER_CASH_DRAWER_DETAILs.Where(x => x.USER_CASH_DRAWER_ID == this._objUserCashDrawer.USER_CASH_DRAWER_ID))
                {
                    decimal totalAmount = 0;

                    var qryPayment = DBDataContext.Db.TBL_PAYMENTs.Where(row => row.IS_ACTIVE && row.USER_CASH_DRAWER_ID == this._objUserCashDrawer.USER_CASH_DRAWER_ID && row.PAY_AMOUNT > 0 && row.CURRENCY_ID == d.CURRENCY_ID);
                    var qryAdjust = from a in DBDataContext.Db.TBL_INVOICE_ADJUSTMENTs
                                    join i in DBDataContext.Db.TBL_INVOICEs on a.INVOICE_ID equals i.INVOICE_ID
                                    where a.USER_CASH_DRAWER_ID == this._objUserCashDrawer.USER_CASH_DRAWER_ID && i.CURRENCY_ID == d.CURRENCY_ID
                                    select a.ADJUST_AMOUNT;
                    var qryDeposit = DBDataContext.Db.TBL_CUS_DEPOSITs.Where(row => row.IS_PAID && row.USER_CASH_DRAWER_ID == this._objUserCashDrawer.USER_CASH_DRAWER_ID && row.CURRENCY_ID == d.CURRENCY_ID);
                    var qryCancelPayment = DBDataContext.Db.TBL_PAYMENTs.Where(row => row.IS_ACTIVE && row.USER_CASH_DRAWER_ID == this._objUserCashDrawer.USER_CASH_DRAWER_ID && row.PAY_AMOUNT < 0 && row.CURRENCY_ID == d.CURRENCY_ID);
                    var qryCustomerBuy = DBDataContext.Db.TBL_CUSTOMER_BUYs.Where(x => x.PARENT_ID == 0 && x.USER_CASH_DRAWER_ID == _objUserCashDrawer.USER_CASH_DRAWER_ID && x.CURRENCY_ID == d.CURRENCY_ID);
                    var qryCustomerVoid = DBDataContext.Db.TBL_CUSTOMER_BUYs.Where(x => x.IS_VOID == true && x.PARENT_ID != 0 && x.USER_CASH_DRAWER_ID == _objUserCashDrawer.USER_CASH_DRAWER_ID && x.CURRENCY_ID == d.CURRENCY_ID);
                    var qryPrepayment = DBDataContext.Db.TBL_CUS_PREPAYMENTs.Where(x => x.USER_CASH_DRAWER_ID == this._objUserCashDrawer.USER_CASH_DRAWER_ID && x.CURRENCY_ID == d.CURRENCY_ID);
                    if (qryPayment.Count() > 0)
                    {
                        totalAmount += qryPayment.Sum(row => row.PAY_AMOUNT);
                    }
                    if (qryDeposit.Count() > 0)
                    {
                        totalAmount += qryDeposit.Sum(row => row.AMOUNT);
                    }
                    if (qryPrepayment.Count() > 0)
                    {
                        totalAmount += qryPrepayment.Sum(row => row.AMOUNT);
                    }
                    if (qryCancelPayment.Count() > 0)
                    {
                        totalAmount += qryCancelPayment.Sum(x => x.PAY_AMOUNT);
                    }
                    if (qryCustomerBuy.Count() > 0)
                    {
                        totalAmount += qryCustomerBuy.Sum(x => x.BUY_AMOUNT);
                    }
                    if (qryCustomerVoid.Count() > 0)
                    {
                        totalAmount -= qryCustomerVoid.Sum(x => x.BUY_AMOUNT);
                    }

                    d.TOTAL_CASH = totalAmount;
                    d.ACTUAL_CASH = 0.0m;
                    d.SURPLUS = totalAmount + d.CASH_FLOAT;
                    DBDataContext.Db.SubmitChanges();

                }

                var q = from c in DBDataContext.Db.TLKP_CURRENCies
                        join d in DBDataContext.Db.TBL_USER_CASH_DRAWER_DETAILs.Where(x => x.USER_CASH_DRAWER_ID == this._objUserCashDrawer.USER_CASH_DRAWER_ID) on c.CURRENCY_ID equals d.CURRENCY_ID
                        select new
                        {
                            c.CURRENCY_ID,
                            c.CURRENCY_SING,
                            d.CASH_FLOAT,
                            d.TOTAL_CASH,
                            d.ACTUAL_CASH,
                            d.SURPLUS
                        };
                dtMoney = q._ToDataTable();
                this.dgv.DataSource = dtMoney;
                txtCashDrawer.Text = this._objCashDrawer.CASH_DRAWER_NAME;
                txtOpenDate.Text = this._objUserCashDrawer.OPEN_DATE.ToString();
                bindCounCash();
            }
            else
            {
                UIHelper.SetEnabled(this, false);
                btnVIEW.Enabled = false;
                this.btnOK.Enabled = false;
            }
        }

        private void bindPrinter()
        {
            this.cboPrinterCashDrawer.Items.Add("");
            foreach (string printer in PrinterSettings.InstalledPrinters)
            {
                this.cboPrinterCashDrawer.Items.Add(printer);
            }
            this.cboPrinterCashDrawer.Text = Settings.Default.PRINTER_CASHDRAWER;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtRealAmount_TextChanged(object sender, EventArgs e)
        {
            //this.txtDifferentAmount.Text = UIHelper.FormatCurrency((DataHelper.ParseToDecimal(this.txtActualAmount.Text) - DataHelper.ParseToDecimal(this.txtTotalAmount.Text)), _objCashDrawer.CURRENCY_ID);
            //this.lblText.Text = DataHelper.NumberToWord(DataHelper.ParseToDecimal(this.txtActualAmount.Text));
        }

        private bool invalid()
        {
            bool result = false;
            this.ClearAllValidation();
            return result;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (invalid()) return;
            txtNote.Focus(); // For lose focus of datagridview cell editing
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    /*
                     * PROCESS TO CLOSE CASH DRAWER
                     * 1.UPDATE CASH DRAWER
                     * 2.UPDATE CASH DRAWER DETAIL
                     */
                    TBL_USER_CASH_DRAWER old = new TBL_USER_CASH_DRAWER();
                    this._objUserCashDrawer._CopyTo(old);
                    this._objUserCashDrawer.CLOSED_DATE = this.txtCloseDate.Value;
                    this._objUserCashDrawer.IS_CLOSED = true;
                    this._objUserCashDrawer.NOTE = this.txtNote.Text;

                    // create blank log 
                    TBL_CHANGE_LOG log = null;
                    DBDataContext.Db.Update(old, this._objUserCashDrawer);
                    TBL_USER_CASH_DRAWER_DETAIL objUserCashDrawerDetail = new TBL_USER_CASH_DRAWER_DETAIL();
                    foreach (DataGridViewRow row in this.dgv.Rows)
                    {
                        var currencyId = DataHelper.ParseToInt(row.Cells[this.CURRENCY_ID.Name].Value.ToString());
                        var objDetail = DBDataContext.Db.TBL_USER_CASH_DRAWER_DETAILs.FirstOrDefault(x => x.USER_CASH_DRAWER_ID == this._objUserCashDrawer.USER_CASH_DRAWER_ID && x.CURRENCY_ID == currencyId);

                        // pass all data to old object TBL_USER_CASH_DRAWER_DETAIL
                        objDetail._CopyTo(objUserCashDrawerDetail);
                        if (objDetail != null)
                        {
                            objDetail.CASH_FLOAT = (decimal)row.Cells[this.CASH_FLOAT.Name].Value;
                            objDetail.TOTAL_CASH = (decimal)row.Cells[this.TOTAL_CASH.Name].Value;
                            objDetail.ACTUAL_CASH = (decimal)row.Cells[this.ACTUAL_CASH.Name].Value;
                            objDetail.SURPLUS = (decimal)row.Cells[this.SURPLUS_CASH.Name].Value;

                            // Update TBL_USER_CASH_DRAWER_DETAIL with ref LOG table
                            DBDataContext.Db.UpdateChild(objUserCashDrawerDetail, objDetail, old, ref log);
                        }
                    }
                    if (chkCOUNT_MONEY.Checked)
                    {
                        var re = (from o in olddt.Select().Where(x => DataHelper.ParseToInt(x[COUNT_CASH_ID.DataPropertyName.ToString()].ToString()) != -1)
                                  where !(dt.Select().Select(x => DataHelper.ParseToInt(x[COUNT_CASH_ID.DataPropertyName.ToString()].ToString())))
                                        .Contains(DataHelper.ParseToInt(o[COUNT_CASH_ID.DataPropertyName.ToString()].ToString()))
                                  select new
                                  {
                                      COUNT_CASH_ID = DataHelper.ParseToInt(o[COUNT_CASH_ID.DataPropertyName.ToString()].ToString())
                                  })._ToDataTable();
                        foreach (DataRow item in re.Rows)
                        {
                            TBL_COUNT_CASH _Old = new TBL_COUNT_CASH();
                            TBL_COUNT_CASH _New = new TBL_COUNT_CASH();
                            _New = DBDataContext.Db.TBL_COUNT_CASHes.FirstOrDefault(x => x.COUNT_CASH_ID == DataHelper.ParseToInt(item[COUNT_CASH_ID.DataPropertyName.ToString()].ToString()));
                            _New._CopyTo(_Old);
                            _New.IS_ACTIVE = false;
                            DBDataContext.Db.Update(_Old, _New);
                        }
                        foreach (DataGridViewRow item in dgvCOUNT_MONEY.Rows)
                        {
                            if (DataHelper.ParseToInt(item.Cells[COUNT_CASH_ID.Name].Value.ToString()) != -1 && item.Cells[CASH_VALUE.Name].Value.ToString() != ""
                                && DataHelper.ParseToInt(item.Cells[QUANTITY.Name].Value.ToString()) != 0)
                            {
                                TBL_COUNT_CASH _Old = new TBL_COUNT_CASH();
                                TBL_COUNT_CASH _New = new TBL_COUNT_CASH();
                                if (DataHelper.ParseToInt(item.Cells[COUNT_CASH_ID.Name].Value.ToString()) == 0)
                                {
                                    _New.CASH_VALUE = DataHelper.ParseToInt(item.Cells[CASH_VALUE.Name].Value.ToString());
                                    _New.USER_CASH_DRAWER_ID = _objUserCashDrawer.USER_CASH_DRAWER_ID;
                                    _New.CURRENCY_ID = DataHelper.ParseToInt(item.Cells[CURRENCY_ID_.Name].Value.ToString());
                                    _New.QUANTITY = DataHelper.ParseToInt(item.Cells[QUANTITY.Name].Value.ToString());
                                    _New.IS_ACTIVE = true;
                                    DBDataContext.Db.Insert(_New);
                                }
                                else
                                {
                                    _New = DBDataContext.Db.TBL_COUNT_CASHes.FirstOrDefault(x => x.COUNT_CASH_ID == DataHelper.ParseToInt(item.Cells[COUNT_CASH_ID.Name].Value.ToString()));
                                    _New._CopyTo(_Old);
                                    _New.CASH_VALUE = DataHelper.ParseToInt(item.Cells[CASH_VALUE.Name].Value.ToString());
                                    _New.QUANTITY = DataHelper.ParseToInt(item.Cells[QUANTITY.Name].Value.ToString());
                                    DBDataContext.Db.Update(_Old, _New);
                                }
                            }
                        }
                    }
                    tran.Complete();
                }
                //printReport(false);
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            printReport(true);
        }

        CrystalReportHelper ch;
        CrystalReportHelper chCountCash;
        private void printReport(bool preview)
        {
            if (this._objUserCashDrawer == null) return;
            TBL_LOGIN objLogin = DBDataContext.Db.TBL_LOGINs.FirstOrDefault(x => x.LOGIN_ID == _objUserCashDrawer.USER_ID);
            if (cboReport.SelectedIndex == 0)
            {
                ch = new CrystalReportHelper("ReportCashDaily.rpt", "Adjustment", "VoidPayment", "VoidCustomerBuy");
            }
            if (chkCOUNT_MONEY.Checked)
            {
                chCountCash = new CrystalReportHelper("ReportCountCash.rpt");
                chCountCash.SetParameter("@USER_CASH_DRAWER_ID", _objUserCashDrawer.USER_CASH_DRAWER_ID);
                chCountCash.SetParameter("@CASH_NAME", _objCashDrawer.CASH_DRAWER_NAME);
            }
            else if (cboReport.SelectedIndex == 1)
            {
                ch = new CrystalReportHelper("ReportCashDailyDetail.rpt");
                ch.SetParameter("@USER", objLogin.LOGIN_NAME);
                ch.SetParameter("@CASHDRAWER", _objCashDrawer.CASH_DRAWER_NAME);
                ch.SetParameter("@OPEN_DATE", txtOpenDate.Value);
                ch.SetParameter("@CLOSE_DATE", txtCloseDate.Value);
            }
            else if (cboReport.SelectedIndex == 2)
            {
                ch = new CrystalReportHelper("ReportCashSummary.rpt");
                ch.SetParameter("@USER", objLogin.LOGIN_NAME);
                ch.SetParameter("@CASHDRAWER", _objCashDrawer.CASH_DRAWER_NAME);
                ch.SetParameter("@OPEN_DATE", txtOpenDate.Value);
                ch.SetParameter("@CLOSE_DATE", txtCloseDate.Value);
            }
            else if (cboReport.SelectedIndex == 3)
            {
                ch = new CrystalReportHelper("ReportCountCash.rpt");
                ch.SetParameter("@USER_CASH_DRAWER_ID", _objUserCashDrawer.USER_CASH_DRAWER_ID);
                ch.SetParameter("@CASH_NAME", _objCashDrawer.CASH_DRAWER_NAME);
            }
            else if (cboReport.SelectedIndex == 4)
            {
                ch = new CrystalReportHelper("ReportUserCashDrawerDaily.rpt");
                ch.SetParameter("@USER_CASH_DRAWER_ID", _objUserCashDrawer.USER_CASH_DRAWER_ID);
                ch.SetParameter("@USER", objLogin.LOGIN_NAME);
                ch.SetParameter("@CASHDRAWER", _objCashDrawer.CASH_DRAWER_NAME);
                ch.SetParameter("@OPEN_DATE", txtOpenDate.Value);
                ch.SetParameter("@CLOSE_DATE", txtCloseDate.Value);
            }

            ch.SetParameter("@USER_CASH_DRAWER_ID", _objUserCashDrawer.USER_CASH_DRAWER_ID);
            if (preview)
            {
                DialogReportViewer diag = new DialogReportViewer(ch.Report, "");
                //diag.viewer.EnableExportButton = false;
                diag.ShowDialog();
            }
            else
            {
                ch.PrintReport(Settings.Default.PRINTER_CASHDRAWER);
                if (chkCOUNT_MONEY.Checked)
                {
                    chCountCash.PrintReport(Settings.Default.PRINTER_CASHDRAWER);
                }
            }
        }

        private void InputEnglish(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void InputKhmer(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void cboPrinterCashDrawer_SelectedIndexChanged(object sender, EventArgs e)
        {
            Settings.Default.PRINTER_CASHDRAWER = this.cboPrinterCashDrawer.Text;
        }

        private void dgv_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1) return;
            var row = this.dgv.Rows[e.RowIndex];
            if (row.Cells[e.ColumnIndex].Value.ToString() == "")
            {
                row.Cells[e.ColumnIndex].Value = 0;
            }
            var cashFloat = (decimal)row.Cells[this.CASH_FLOAT.Name].Value;
            var totalCash = (decimal)row.Cells[this.TOTAL_CASH.Name].Value;
            var actualCash = (decimal)row.Cells[this.ACTUAL_CASH.Name].Value;
            var surplus = cashFloat + totalCash - actualCash;
            row.Cells[this.SURPLUS_CASH.Name].Value = surplus;
        }

        private void dgv_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            DataGridViewTextBoxEditingControl tb = (DataGridViewTextBoxEditingControl)e.Control;
            tb.KeyPress += new KeyPressEventHandler(dataGridViewTextBox_KeyPress);
            e.Control.KeyPress += new KeyPressEventHandler(dataGridViewTextBox_KeyPress);
        }

        private void dataGridViewTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        }

        private void dgv_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }

        private void chkCOUNT_MONEY_CheckedChanged(object sender, EventArgs e)
        {
            if (chkCOUNT_MONEY.Checked)
            {
                pnlCountMoney.Height = pnlCountMoney.Height + dgvCOUNT_MONEY.Height;
                this.Height = this.Height + dgvCOUNT_MONEY.Height;
            }
            else
            {
                pnlCountMoney.Height = pnlCountMoney.Height - dgvCOUNT_MONEY.Height;
                this.Height = this.Height - dgvCOUNT_MONEY.Height;
            }
            dgvCOUNT_MONEY.Visible = chkCOUNT_MONEY.Checked;
        }

        private void dgvCOUNT_MONEY_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            DataGridViewRow row = dgvCOUNT_MONEY.Rows[e.RowIndex];
            if (row.Cells[CONTROL_.Name].ColumnIndex == e.ColumnIndex)
            {
                if (DataHelper.ParseToInt(row.Cells[COUNT_CASH_ID.Name].Value.ToString()) == -1 && dt.Select()
                    .Where(x => DataHelper.ParseToInt(x[CURRENCY_ID_.DataPropertyName.ToString()].ToString())
                    == DataHelper.ParseToInt(row.Cells[CURRENCY_ID_.Name].Value.ToString())).Count() == 1)
                {
                    addRow(row);
                }
                else if (DataHelper.ParseToInt(row.Cells[COUNT_CASH_ID.Name].Value.ToString()) != -1)
                {
                    dt.Rows.RemoveAt(e.RowIndex);
                }
                sum();
            }
        }

        private void dgvCOUNT_MONEY_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            DataGridViewRow row = dgvCOUNT_MONEY.Rows[e.RowIndex];
            if (row.Cells[CASH_VALUE.Name].ColumnIndex == e.ColumnIndex && row.Cells[CASH_VALUE.Name].Value.ToString() != "")
            {
                if (e.RowIndex == dgvCOUNT_MONEY.Rows.Count - 1)
                {
                    addRow(row);
                }
                else if (row.Cells[CURRENCY_ID_.Name].Value.ToString() != dgvCOUNT_MONEY.Rows[e.RowIndex + 1].Cells[CURRENCY_ID_.Name].Value.ToString())
                {
                    addRow(row);
                }
            }
            row.Cells[AMOUNT.Name].Value = DataHelper.ParseToDecimal(row.Cells[CASH_VALUE.Name].Value.ToString())
                * DataHelper.ParseToDecimal(row.Cells[QUANTITY.Name].Value.ToString());
            sum();
        }

        private void addRow(DataGridViewRow row)
        {
            DataRow dr = dt.NewRow();
            dr[COUNT_CASH_ID.DataPropertyName.ToString()] = 0;
            dr[CURRENCY_ID_.DataPropertyName.ToString()] = row.Cells[CURRENCY_ID_.Name].Value;
            dr[CURRENCY_SING0.DataPropertyName.ToString()] = row.Cells[CURRENCY_SING0.Name].Value;
            dr[CONTROL_.DataPropertyName.ToString()] = Resources.icon_close;
            dt.Rows.InsertAt(dr, row.Index + 1);
            DataGridViewComboBoxCell cmb = new DataGridViewComboBoxCell();
            cmb.DataSource = from ct in DBDataContext.Db.TLKP_CASH_TYPEs
                             orderby ct.CASH_VALUE descending
                             where ct.CURRENCY_ID == DataHelper.ParseToInt(row.Cells[CURRENCY_ID_.Name].Value.ToString())
                             select new
                             {
                                 CASH_VALUE = string.Format("{0:n0}", ct.CASH_VALUE)
                             };
            cmb.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            cmb.ValueMember = CASH_VALUE.DataPropertyName.ToString();
            cmb.DisplayMember = CASH_VALUE.DataPropertyName.ToString();
            dgvCOUNT_MONEY[CASH_VALUE.Name, row.Index + 1] = cmb;
        }

        private void dgvCOUNT_MONEY_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }

        private void dgvCOUNT_MONEY_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (dgvCOUNT_MONEY.CurrentCell == dgvCOUNT_MONEY.CurrentRow.Cells[CASH_VALUE.Name])
            {
                return;
            }
            DataGridViewTextBoxEditingControl tb = (DataGridViewTextBoxEditingControl)e.Control;
            tb.KeyPress += new KeyPressEventHandler(dgvCOUNT_MONEY_KeyPress);
            e.Control.KeyPress += new KeyPressEventHandler(dgvCOUNT_MONEY_KeyPress);
        }

        private void dgvCOUNT_MONEY_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        }

        private void sum()
        {
            foreach (DataGridViewRow row in dgvCOUNT_MONEY.Rows)
            {
                if (DataHelper.ParseToInt(row.Cells[COUNT_CASH_ID.Name].Value.ToString()) == -1)
                {
                    decimal amount;
                    int i;
                    var q = dt.Select().Where(x => DataHelper.ParseToInt(x[COUNT_CASH_ID.DataPropertyName.ToString()].ToString()) != -1
                       && DataHelper.ParseToInt(x[CURRENCY_ID_.DataPropertyName.ToString()].ToString()) == DataHelper.ParseToInt(row.Cells[CURRENCY_ID_.Name].Value.ToString()));
                    amount = q.Sum(x => DataHelper.ParseToDecimal(x[AMOUNT.DataPropertyName.ToString()].ToString()));
                    i = q.Sum(x => DataHelper.ParseToInt(x[QUANTITY.DataPropertyName.ToString()].ToString()));
                    row.Cells[AMOUNT.Name].Value = amount;
                    row.Cells[QUANTITY.Name].Value = i;
                    var dr = dtMoney.Select().FirstOrDefault(x => DataHelper.ParseToInt(x[CURRENCY_ID.DataPropertyName.ToString()].ToString())
                        == DataHelper.ParseToInt(row.Cells[CURRENCY_ID_.Name].Value.ToString()));
                    if (dr != null)
                    {
                        dr[ACTUAL_CASH.DataPropertyName.ToString()] = amount;
                        dr[SURPLUS_CASH.DataPropertyName.ToString()] = DataHelper.ParseToDecimal(dr[CASH_FLOAT.DataPropertyName.ToString()].ToString()) + DataHelper.ParseToDecimal(dr[TOTAL_CASH.Name].ToString()) - amount;
                        dtMoney.AcceptChanges();
                    }
                }
            }
        }

        private void bindCounCash()
        {
            try
            {
                var q = (from c in DBDataContext.Db.TLKP_CURRENCies
                         select new
                         {
                             COUNT_CASH_ID = -1,
                             CURRENCY_ID = c.CURRENCY_ID,
                             CASH_VALUE = c.CURRENCY_NAME + " (" + c.CURRENCY_SING + ")",
                             QUANTITY = "",
                             AMOUNT = 0,
                             c.CURRENCY_SING,
                             CONTROL = Resources.down
                         });
                var cc = (from co in DBDataContext.Db.TBL_COUNT_CASHes.Where(x => x.USER_CASH_DRAWER_ID == _objUserCashDrawer.USER_CASH_DRAWER_ID && x.IS_ACTIVE)
                          join c in DBDataContext.Db.TLKP_CURRENCies on co.CURRENCY_ID equals c.CURRENCY_ID
                          select new
                          {
                              COUNT_CASH_ID = co.COUNT_CASH_ID,
                              CURRENCY_ID = co.CURRENCY_ID,
                              CASH_VALUE = string.Format("{0:n0}", co.CASH_VALUE),
                              QUANTITY = co.QUANTITY,
                              AMOUNT = co.CASH_VALUE * co.QUANTITY,
                              c.CURRENCY_SING,
                              CONTROL = Resources.icon_close
                          })._ToDataTable();
                DataTable dtTemp = new DataTable();
                dtTemp = q._ToDataTable();
                foreach (DataRow row in cc.Rows)
                {
                    dtTemp.Rows.Add(row.ItemArray);
                }
                dt = dtTemp.Select().OrderBy(x => x[CURRENCY_ID_.DataPropertyName.ToString()]).CopyToDataTable();
                olddt = dt.Copy();
                dgvCOUNT_MONEY.DataSource = dt;
                if (cc.Rows.Count > 0)
                {
                    isCheck = true;
                }
                else
                {
                    isCheck = false;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void DialogCloseCashDrawer_Load(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvCOUNT_MONEY.Rows)
            {
                DataRowView drv = row.DataBoundItem as DataRowView;
                DataRow dr = drv.Row;
                if (DataHelper.ParseToInt(dr[COUNT_CASH_ID.DataPropertyName.ToString()].ToString()) == -1)
                {
                    row.DefaultCellStyle.BackColor = Color.LightBlue;
                    row.DefaultCellStyle.Font = new Font("Khmer OS System", 8, FontStyle.Bold);
                    row.DefaultCellStyle.SelectionBackColor = Color.LightBlue;
                    row.ReadOnly = true;
                }
                else
                {
                    DataGridViewComboBoxCell cmb = new DataGridViewComboBoxCell();
                    cmb.DataSource = from ct in DBDataContext.Db.TLKP_CASH_TYPEs
                                     orderby ct.CASH_VALUE descending
                                     where ct.CURRENCY_ID == DataHelper.ParseToInt(row.Cells[CURRENCY_ID_.Name].Value.ToString())
                                     select new
                                     {
                                         CASH_VALUE = string.Format("{0:n0}", ct.CASH_VALUE)
                                     };
                    cmb.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
                    cmb.ValueMember = CASH_VALUE.DataPropertyName.ToString();
                    cmb.DisplayMember = CASH_VALUE.DataPropertyName.ToString();
                    dgvCOUNT_MONEY[CASH_VALUE.Name, row.Index] = cmb;
                }
            }
            chkCOUNT_MONEY.Checked = isCheck;
            sum();
        }
    }
}
