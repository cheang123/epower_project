﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PageDevice : Form
    {
        int intStatus = 0;
        public int StatusID
        {
            get { return intStatus; }
            private set { intStatus = value; }
        }

        public TBL_DEVICE Device
        {
            get
            {
                TBL_DEVICE objDevice = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    try
                    {
                        int intDeviceID = (int)dgv.SelectedRows[0].Cells["DEVICE_ID"].Value;
                        objDevice = DBDataContext.Db.TBL_DEVICEs.FirstOrDefault(x => x.DEVICE_ID == intDeviceID);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objDevice;
            }
        }
        #region Constructor
        public PageDevice()
        {
            InitializeComponent();

            UIHelper.DataGridViewProperties(dgv);
            txt_QuickSearch(null, null);
           
        }
        #endregion

        #region Operation
        private void btnNew_Click(object sender, EventArgs e)
        {
            TBL_DEVICE objDevice = new TBL_DEVICE() { DEVICE_CODE = "" };

            if (cboStatus.SelectedIndex != -1)
            {
                objDevice.STATUS_ID = (int)cboStatus.SelectedValue;
            }

            DialogDevice dig = new DialogDevice(GeneralProcess.Insert, objDevice);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.Device.DEVICE_ID);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count <= 0)
            {
                return;
            }
            if (Device.DEVICE_TYPE_ID==(int)DeviceType.IR_READER)
            {
                MsgBox.ShowInformation(Resources.MS_CANNOT_UPDATE_DEVICE_INFORMATION);
                return;
            }                

            DialogDevice dig = new DialogDevice(GeneralProcess.Update, Device);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.Device.DEVICE_ID);
            }
        }

        private void txt_QuickSearch(object sender, EventArgs e)
        {
            if (cboStatus.SelectedIndex!=-1)
            {
                intStatus = (int)cboStatus.SelectedValue;
            }

            try
            {

                dgv.DataSource = from d in DBDataContext.Db.TBL_DEVICEs
                                 join s in DBDataContext.Db.TLKP_DEVICE_STATUS on d.STATUS_ID equals s.STATUS_ID
                                 join dt in DBDataContext.Db.TLKP_DEVICE_TYPEs on d.DEVICE_TYPE_ID equals dt.DEVICE_TYPE_ID
                                 where (intStatus == 0 || d.STATUS_ID == intStatus) &&
                                 (d.DEVICE_CODE.ToLower().Contains(txtQuickSearch.Text.ToLower().Trim()) ||
                                 d.DEVICE_OEM_INFO.ToLower().Contains(txtQuickSearch.Text.ToLower().Trim()))
                                 select new
                                 {
                                     d.DEVICE_ID,
                                     d.DEVICE_CODE,
                                     d.DEVICE_OEM_INFO,
                                     d.DEVICE_TYPE_ID,
                                     dt.DEVICE_TYPE_NAME,
                                     s.STATUS_NAME
                                 };
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }        
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        #endregion

        public void loadDeviceStatus()
        {
            int tempStatus = StatusID;
            DataTable dt = (from a in DBDataContext.Db.TLKP_DEVICE_STATUS
                            select a)._ToDataTable();
            //Create Row Templet
            DataRow dr = dt.NewRow();
            dr["STATUS_ID"] = 0;
            dr["STATUS_NAME"] =Resources.ALL_STATUS;
            dt.Rows.InsertAt(dr, 0);
            UIHelper.SetDataSourceToComboBox(cboStatus, dt, "STATUS_ID", "STATUS_NAME");
            cboStatus.SelectedValue = tempStatus;
        }

        private void btnDeviceRegister_Click(object sender, EventArgs e)
        {
            DialogIRRegistration dig = new DialogIRRegistration();
            if (dig.ShowDialog()== DialogResult.OK)
            {
                txt_QuickSearch(null, null);
              //  UIHelper.SelectRow(dgv, dig.Device.DEVICE_ID);
            }
        }

        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }
    }
}
