﻿using System;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PageCustomerMergeAccount : Form
    { 
        #region Constructor
        public PageCustomerMergeAccount()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
        }

        DialogCustomerMergeAccounts.CustomerMergeType _flagMerge;
        #endregion

        #region Operation
        public void BindData()
        {
            try
            {
                _flagMerge=rdoMERG_USAGE.Checked? _flagMerge = DialogCustomerMergeAccounts.CustomerMergeType.Usage:_flagMerge = DialogCustomerMergeAccounts.CustomerMergeType.Invoice;
                var qry =   from c in DBDataContext.Db.TBL_CUSTOMERs
                            join a in DBDataContext.Db.TBL_AREAs on c.AREA_ID equals a.AREA_ID
                            where (c.CUSTOMER_CODE+" "+c.FIRST_NAME_KH+" "+c.LAST_NAME_KH+""+a.AREA_CODE+" "+a.AREA_NAME).ToLower().Contains(txtQuickSearch.Text.ToLower())
                                    &&((c.USAGE_CUSTOMER_ID==c.CUSTOMER_ID && _flagMerge== DialogCustomerMergeAccounts.CustomerMergeType.Usage)
                                        ||(c.INVOICE_CUSTOMER_ID ==c.CUSTOMER_ID && _flagMerge== DialogCustomerMergeAccounts.CustomerMergeType.Invoice))
                                    && c.STATUS_ID != (int)CustomerStatus.Closed
                                    && c.STATUS_ID != (int)CustomerStatus.Cancelled 
                            select new
                            {
                                c.CUSTOMER_ID,
                                c.CUSTOMER_CODE,
                                CUSTOMER_NAME = c.LAST_NAME_KH + " " + c.FIRST_NAME_KH,
                                a.AREA_NAME,
                                TOTAL = _flagMerge == DialogCustomerMergeAccounts.CustomerMergeType.Usage
                                        ? DBDataContext.Db.TBL_CUSTOMERs.Count(row=>row.USAGE_CUSTOMER_ID==c.CUSTOMER_ID)
                                        : DBDataContext.Db.TBL_CUSTOMERs.Count(row=>row.INVOICE_CUSTOMER_ID==c.CUSTOMER_ID)
                            };
                this.dgv.DataSource = qry.OrderBy(x => x.CUSTOMER_CODE);
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            } 
        }

        /// <summary>
        /// Add new area.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNew_Click(object sender, EventArgs e)
        {
            DialogCustomerMergeAccounts diag = new DialogCustomerMergeAccounts(GeneralProcess.Insert,0, _flagMerge);
            if (diag.ShowDialog() == DialogResult.OK)
            {
                this.BindData();
                UIHelper.SelectRow(this.dgv, diag.Customer.CUSTOMER_ID);
            }
        }

        /// <summary>
        /// Edit area.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (this.dgv.SelectedRows.Count == 0) return;
            int id = (int)this.dgv.SelectedRows[0].Cells[this.CUSTOMER_ID.Name].Value;
            DialogCustomerMergeAccounts diag = new DialogCustomerMergeAccounts(GeneralProcess.Update, id, _flagMerge);
            diag.ShowDialog();
            this.BindData();
            UIHelper.SelectRow(this.dgv, id);
        }

        /// <summary>
        /// Remove area.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (this.dgv.SelectedRows.Count == 0) return;
            if (MsgBox.ShowQuestion(Resources.MSQ_REMOVE_CUSTOMER,Properties.Resources.MERG_CUSTOMER) != DialogResult.Yes) return;
            var objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == (int)this.dgv.SelectedRows[0].Cells[this.CUSTOMER_ID.Name].Value);
            if (objCustomer == null) return;
            foreach (var c in DBDataContext.Db.TBL_CUSTOMERs.Where(row => row.USAGE_CUSTOMER_ID ==objCustomer.CUSTOMER_ID||row.INVOICE_CUSTOMER_ID==objCustomer.CUSTOMER_ID))
            {
                if(rdoMERG_USAGE.Checked){
                    c.USAGE_CUSTOMER_ID = 0;
                };  
                if (rdoMERG_INVOICE.Checked)
                {
                    c.INVOICE_CUSTOMER_ID =0;
                    c.IS_REACTIVE = false;
                    c.REACTIVE_RULE_ID = 0;
                }
            }
            DBDataContext.Db.SubmitChanges();
            this.BindData();
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        #endregion

        #region Events

        private void rdoMergeUsage_CheckedChanged(object sender, EventArgs e)
        {
            BindData();
        }
        
        private void txtQuickSearch_QuickSearch(object sender, EventArgs e)
        {
            BindData();
        }

        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }
        #endregion
    }
}
