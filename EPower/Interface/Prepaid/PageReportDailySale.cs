﻿using System;
using System.IO;
using System.Windows.Forms;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface.PrePaid
{
    public partial class PageReportDailySale : UserControl
    {
        private CrystalReportHelper ch;
        public PageReportDailySale()
        {
            InitializeComponent();
            viewer.DefaultView();
            dtpDate.Value = DBDataContext.Db.GetSystemDate().Date;
            dtpEndDate.Value = dtpDate.Value.AddDays(1).AddSeconds(-1);
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            Runner.Run(viewReport);
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            sendMail();
        }

        private void viewReport()
        {
            ch = ViewReportPrepaidDailySale(dtpDate.Value, dtpEndDate.Value);
            viewer.ReportSource = ch.Report;
        }

        public CrystalReportHelper ViewReportPrepaidDailySale(DateTime dtStart,DateTime dtEnd)
        {
            CrystalReportHelper c = new CrystalReportHelper("ReportPrepaidDailySale.rpt");
            c.SetParameter("@START_DATE", dtStart);
            c.SetParameter("@END_DATE", dtEnd);
            return c; 
        }
        private void sendMail()
        {
            try
            {
                viewReport();
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + ch.ReportName + ".xls").FullName;
                ch.ExportToExcel(exportPath);
                DialogSendMail.Instance.Add(ch.ReportName, exportPath);
                DialogSendMail.Instance.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }
    }
}
