﻿using dotnetCHARTING.WinForms;
using EPower.Base.Logic;
using EPower.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface.PrePaid
{
    public partial class DialogCustomer : ExDialog
    {
        #region Private Data 

        GeneralProcess _flag = GeneralProcess.Insert;
        TBL_CUSTOMER _objNew = new TBL_CUSTOMER();
        TBL_CUSTOMER _objOld = new TBL_CUSTOMER();

        TBL_METER _objMeter = null;
        TBL_CIRCUIT_BREAKER _objBreaker = null;
        TBL_BOX _objBox = null;
        TBL_CUSTOMER_METER _objCustomerMeter = null;
        TBL_PREPAID_CUSTOMER _objPreCus = null;
        TBL_POLE _objPole = null;
        private TLKP_CURRENCY _objKhCurrency = null;

        bool _loading = false;
        bool printingInvoice = false;
        public bool CustomerIsActivated = false;

        #endregion Private Data

        #region Constructor

        public DialogCustomer(GeneralProcess flag, TBL_CUSTOMER obj)
        {
            _loading = true;

            InitializeComponent();
            printingInvoice = DataHelper.ParseToBoolean(Method.Utilities[Utility.INVOICE_CONCURRENT_PRINTING]);
            btnActivate.Enabled = SoftTech.Security.Logic.Login.IsAuthorized(Permission.PREPAID_REGISTER_ACTIVATE);
            //linkEditDeposit.Enabled     = SoftTech.Security.Logic.Login.IsAuthorized(Permission.PREPAID_REGISTER_UPDATE_ADJUSTDESPOSIT);
            btnCHANGE_METER.Enabled = SoftTech.Security.Logic.Login.IsAuthorized(Permission.PREPAID_REGISTER_UPDATE_CHANGEMETER);
            btnCHANGE_BREAKER.Enabled = SoftTech.Security.Logic.Login.IsAuthorized(Permission.PREPAID_REGISTER_UPDATE_CHANGEBREAKER);
            btnCHANGE_BOX.Enabled = SoftTech.Security.Logic.Login.IsAuthorized(Permission.PREPAID_REGISTER_UPDATE_CHANGEBOX);
            btnADJUST_USAGE.Enabled = false; //not use for prepaid
            btnADJUST_AMOUNT.Enabled = SoftTech.Security.Logic.Login.IsAuthorized(Permission.PREPAID_REGISTER_UPDATE_ADJUSTAMOUNT);
            btnPRINT_1.Enabled = SoftTech.Security.Logic.Login.IsAuthorized(Permission.PREPAID_REGISTER_UPDATE_PRINT);

            UIHelper.DataGridViewProperties(dgvEquitment, dgvInvoiceList, dgvCusPurchase, dgvDeposit, dgvPrepaidUsage);
            _objKhCurrency = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.CURRENCY_ID == 1);

            SETTLE_AMOUNT.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            DUE_AMOUNT.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            DateTime sysDate = DBDataContext.Db.GetSystemDate();
            dtpStartDate.Value = new DateTime(sysDate.Year, sysDate.Month, 1);
            dtpEndDate.Value = dtpStartDate.Value.AddMonths(1).AddDays(-1);
            dtPurchaseMonth.Value = new DateTime(sysDate.Year, sysDate.Month, 1);
            dtpYear.Value = new DateTime(sysDate.Year, 1, 1);
            //dtpUsageD2.Value =  dtpUsageD1.Value.AddYears(1).AddSeconds(-1);
            _flag = flag;

            Text = _flag.GetText(Text);

            bind();

            _objNew = obj;

            _objNew._CopyTo(_objOld);

            if (_flag == GeneralProcess.Insert)
            {

                tabControl1.TabPages.Remove(tabDEPOSIT);
                tabControl1.TabPages.Remove(tabBILLING_AND_METER);
                tabControl1.TabPages.Remove(tabINVOICE);
                tabControl1.TabPages.Remove(tabBUY_POWER_MONTHLY);
                tabControl1.TabPages.Remove(tabBUY_POWER_YEARLY);
                tabControl1.TabPages.Remove(tabPREPAID_USAGE);
                newCus();
            }
            else if (_flag == GeneralProcess.Update)
            {
                btnActivate.Visible = false;
                //btnAddDeposit.Visible = false;
                read();
                bindDeposit();
                // when pending.
                if (_objOld.STATUS_ID == (int)CustomerStatus.Pending)
                {
                    tabControl1.TabPages.Remove(tabBILLING_AND_METER);
                    tabControl1.TabPages.Remove(tabINVOICE);
                }

                cboArea.Enabled = !(_objNew.STATUS_ID == (int)CustomerStatus.Active);

                //bind invoice status
                List<int> StatusInv = new List<int>()
                {
                    (int)InvoiceStatus.Open,
                    (int)InvoiceStatus.Close,
                    (int)InvoiceStatus.Cancel
                };

                //bind invoice status
                UIHelper.SetDataSourceToComboBox(cboInvoiceStatus, DBDataContext.Db.TLKP_INVOICE_STATUS.Where(x => StatusInv.Contains(x.INVOICE_STATUS_ID)), Resources.ALL_STATUS);
                cboInvoiceStatus.SelectedValue = (int)InvoiceStatus.Open;

                loadTotalDueAmount();
                loadPurchasePowerMonthly();
                loadPurchaseYearly();
                loadPrepaidUsage();
            }
            else if (_flag == GeneralProcess.Delete)
            {
                btnActivate.Visible = false;
                btnSAVE.Text = Resources.CLOSE;
                tabControl1.TabPages.Remove(tabDEPOSIT);
                tabControl1.TabPages.Remove(tabBILLING_AND_METER);
                tabControl1.TabPages.Remove(tabINVOICE);
                tabControl1.TabPages.Remove(tabBUY_POWER_MONTHLY);
                tabControl1.TabPages.Remove(tabBUY_POWER_YEARLY);
                read();
            }


            btnCHANGE_LOG.Visible = _flag != GeneralProcess.Insert;
            btnREACTIVATE.Visible = _objNew.STATUS_ID == (int)CustomerStatus.Closed;
            _loading = false;
        }

        #endregion Constructor

        #region Public Data
        public TBL_CUSTOMER Object
        {
            get
            {
                return _objNew;
            }
        }
        #endregion Public Data

        #region Method

        private bool invalid()
        {
            bool blnReturn = false;

            this.ClearAllValidation();

            if (txtLastNameKH.Text.Trim() == string.Empty)
            {
                tabControl1.SelectedTab = tabGENERAL_INFORMATION;
                txtLastNameKH.SetValidation(string.Format(Resources.REQUIRED, lblLAST_NAME_KH.Text));
                blnReturn = true;
            }

            if (txtCustomerCode.Text.Trim() == string.Empty)
            {
                tabControl1.SelectedTab = tabGENERAL_INFORMATION;
                txtCustomerCode.SetValidation(string.Format(Resources.REQUIRED, lblCUSTOMER_CODE.Text));
                blnReturn = true;
            }

            if (cboSex.SelectedIndex == -1)
            {
                tabControl1.SelectedTab = tabGENERAL_INFORMATION;
                cboSex.SetValidation(string.Format(Resources.REQUIRED, lblSEX.Text));
                blnReturn = true;
            }

            if (!DataHelper.IsInteger(txtTotalFamily.Text))
            {
                cboSex.SetValidation(string.Format(Resources.REQUIRED_INPUT_NUMBER, lblTOTAL_FAMILY.Text));
                blnReturn = true;
            }
            if (cboProvince.SelectedIndex == -1)
            {
                tabControl1.SelectedTab = tabGENERAL_INFORMATION;
                cboProvince.SetValidation(string.Format(Resources.REQUIRED, lblPROVINCE.Text));
                blnReturn = true;
            }

            if (cboDistrict.SelectedIndex == -1)
            {

                tabControl1.SelectedTab = tabGENERAL_INFORMATION;
                cboDistrict.SetValidation(string.Format(Resources.REQUIRED, lblDISTRICT.Text));
                blnReturn = true;
            }

            if (cboCommune.SelectedIndex == -1)
            {

                tabControl1.SelectedTab = tabGENERAL_INFORMATION;
                cboCommune.SetValidation(string.Format(Resources.REQUIRED, lblCOMMUNE.Text));
                blnReturn = true;
            }

            if (cboVillage.SelectedIndex == -1)
            {

                tabControl1.SelectedTab = tabGENERAL_INFORMATION;
                cboVillage.SetValidation(string.Format(Resources.REQUIRED, lblVILLAGE.Text));
                blnReturn = true;
            }

            if (cboConnectionType.SelectedIndex == -1)
            {
                tabControl1.SelectedTab = tabUSAGE_INFORMATION;
                cboConnectionType.SetValidation(string.Format(Resources.REQUIRED, lblCONNECTION.Text));
                blnReturn = true;
            }

            if (cboPhase.SelectedIndex == -1)
            {
                tabControl1.SelectedTab = tabUSAGE_INFORMATION;
                cboPhase.SetValidation(string.Format(Resources.REQUIRED, lblPHASE.Text));
                blnReturn = true;
            }

            if (cboAMP.SelectedIndex == -1)
            {
                tabControl1.SelectedTab = tabUSAGE_INFORMATION;
                cboAMP.SetValidation(string.Format(Resources.REQUIRED, Resources.AMPARE));
                blnReturn = true;
            }

            if (cboVol.SelectedIndex == -1)
            {
                tabControl1.SelectedTab = tabUSAGE_INFORMATION;
                cboVol.SetValidation(string.Format(Resources.REQUIRED, lblVOLTAGE.Text));
                blnReturn = true;
            }

            if (cboPrice.SelectedIndex == -1)
            {
                tabControl1.SelectedTab = tabUSAGE_INFORMATION;
                cboPrice.SetValidation(string.Format(Resources.REQUIRED, lblPRICE_AND_BILLING_CYCLE.Text));
                blnReturn = true;
            }
            if (cboBillingCycle.SelectedIndex == -1)
            {
                tabControl1.SelectedTab = tabUSAGE_INFORMATION;
                cboBillingCycle.SetValidation(Resources.SELECT_BILLING_CYCLE);
                blnReturn = true;
            }
            if (cboArea.SelectedIndex == -1)
            {
                cboArea.SetValidation(string.Format(Resources.REQUIRED, lblAREA.Text));
                blnReturn = true;
            }
            // merge customer , non-base customer 
            // change billing cycle is not allows.
            if (_flag == GeneralProcess.Update)
            {
                // billing cycle was change.
                if (_objOld.BILLING_CYCLE_ID != (int)cboBillingCycle.SelectedValue)
                {
                    var basedCustomer = new TBL_CUSTOMER();
                    // for merge usage.
                    if (_objOld.USAGE_CUSTOMER_ID != 0 && _objOld.USAGE_CUSTOMER_ID != _objOld.CUSTOMER_ID)
                    {
                        basedCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == _objOld.USAGE_CUSTOMER_ID);
                        MsgBox.ShowInformation(string.Format(Resources.MS_CANNOT_CHANGE_BILLING_CYCLE_MERGED_CUSTOMER, basedCustomer.CUSTOMER_CODE + " " + basedCustomer.LAST_NAME_KH + " " + basedCustomer.FIRST_NAME_KH), "");
                        return true;
                    }
                    // for merge invoice
                    if (_objOld.INVOICE_CUSTOMER_ID != 0 && _objOld.INVOICE_CUSTOMER_ID != _objOld.CUSTOMER_ID)
                    {
                        basedCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == _objOld.INVOICE_CUSTOMER_ID);
                        MsgBox.ShowInformation(string.Format(Resources.MS_CANNOT_CHANGE_BILLING_CYCLE_MERGED_CUSTOMER, basedCustomer.CUSTOMER_CODE + " " + basedCustomer.LAST_NAME_KH + " " + basedCustomer.FIRST_NAME_KH), "");
                        return true;
                    }
                }
            }
            return blnReturn;
        }

        private void newCus()
        {
            bind();

            dtpDOB.ClearValue();
            txtCustomerCode.Text = getNextCustomerID();
            UIHelper.SetDataSourceToComboBox(this.cboConnectionType, DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs.Where(x => x.NONLICENSE_CUSTOMER_GROUP_ID == 4));
            this.cboConnectionType.SelectedValue = DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs.FirstOrDefault(x => x.NONLICENSE_CUSTOMER_GROUP_ID == 4).CUSTOMER_CONNECTION_TYPE_ID;

            TBL_COMPANY objCompany = DBDataContext.Db.TBL_COMPANies.FirstOrDefault(c => c.COMPANY_ID == 1);
            selectVillage(objCompany.VILLAGE_CODE);
            txtLastNameKH.Focus();
            cboArea.SelectedIndex = -1;

            cboAMP.SelectedIndex = -1;

            if (cboPrice.Items.Count > 1)
            {
                cboPrice.SelectedIndex = -1;
            }
            if (cboBillingCycle.Items.Count > 1)
            {
                cboBillingCycle.SelectedIndex = -1;
            }
        }

        private void read()
        {
            // set title.
            Text += " : " + _objNew.LAST_NAME_KH + " " + _objNew.FIRST_NAME_KH;

            dtpActivateDate.SetValue(_objNew.ACTIVATE_DATE);
            txtAddress.Text = _objNew.ADDRESS;
            cboAMP.SelectedValue = _objNew.AMP_ID;
            cboArea.SelectedValue = _objNew.AREA_ID;
            cboBillingCycle.SelectedValue = _objNew.BILLING_CYCLE_ID;
            txtCheckDigit.Text = _objNew.CHECK_DIGIT;
            // _objNew.CLOSED_DATE 

            //Customer ConnectionType
            int groupId = (from g in DBDataContext.Db.TLKP_CUSTOMER_GROUPs
                           join t in DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs.Where(x => x.CUSTOMER_CONNECTION_TYPE_ID == _objNew.CUSTOMER_CONNECTION_TYPE_ID)
                           on g.CUSTOMER_GROUP_ID equals t.NONLICENSE_CUSTOMER_GROUP_ID
                           select g.CUSTOMER_GROUP_ID).FirstOrDefault();
            this.cboCustomerGroup.SelectedValue = groupId;
            UIHelper.SetDataSourceToComboBox(cboConnectionType, DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs.Where(x => x.NONLICENSE_CUSTOMER_GROUP_ID == groupId).OrderBy(x => x.DESCRIPTION));
            this.cboConnectionType.SelectedValue = this._objNew.CUSTOMER_CONNECTION_TYPE_ID;

            txtCompanyName.Text = _objNew.COMPANY_NAME;
            txtCustomerCode.Text = _objNew.CUSTOMER_CODE;
            cboConnectionType.SelectedValue = _objNew.CUSTOMER_CONNECTION_TYPE_ID;
            nudCutoffDays.Value = _objNew.CUT_OFF_DAY;
            txtFirstName.Text = _objNew.FIRST_NAME;

            txtFirstNameKH.Text = _objNew.FIRST_NAME_KH;
            cboSex.SelectedValue = _objNew.GENDER_ID;
            txtHouseNo.Text = _objNew.HOUSE_NO;
            txtIdNumber.Text = _objNew.ID_CARD_NO;
            txtJob.Text = _objNew.JOB;
            txtLastName.Text = _objNew.LAST_NAME;
            txtLastNameKH.Text = _objNew.LAST_NAME_KH;

            cboPhase.SelectedValue = _objNew.PHASE_ID;
            txtPhone1.Text = _objNew.PHONE_1;
            txtPhone2.Text = _objNew.PHONE_2;
            picPhoto.Image = UIHelper.ConvertBinaryToImage(_objNew.PHOTO);
            cboPrice.SelectedValue = _objNew.PRICE_ID;


            //_objNew.STATUS_ID;

            txtStreetNo.Text = _objNew.STREET_NO;
            selectVillage(_objNew.VILLAGE_CODE);
            cboVol.SelectedValue = _objNew.VOL_ID;
            dtpDOB.SetValue(_objNew.BIRTH_DATE);
            txtPlaceofBirth.Text = _objNew.BIRTH_PLACE;

            txtTotalFamily.Text = _objNew.TOTAL_FAMILY.ToString();
            dtpREQUEST_CONNECTION_DATE.Value = _objNew.REQUEST_CONNECTION_DATE;
            //Set Deposit when update 
            int intCurrencyId = 0;
            decimal decDepositBalance = Method.GetCustomerDeposit(_objOld.CUSTOMER_ID, out intCurrencyId);

            _objPreCus = DBDataContext.Db.TBL_PREPAID_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == _objNew.CUSTOMER_ID);
            //this.cboDeposit.SelectedValue = 
            // read customer equipment.
            readCustomerEquipment();


            // read customer meter.
            readCustomerMeter();



        }

        private void selectVillage(string villageCode)
        {
            string communeCode = DBDataContext.Db.TLKP_VILLAGEs.FirstOrDefault(row => row.VILLAGE_CODE == villageCode).COMMUNE_CODE;
            string districCode = DBDataContext.Db.TLKP_COMMUNEs.FirstOrDefault(row => row.COMMUNE_CODE == communeCode).DISTRICT_CODE;
            string province = DBDataContext.Db.TLKP_DISTRICTs.FirstOrDefault(row => row.DISTRICT_CODE == districCode).PROVINCE_CODE;

            UIHelper.SetDataSourceToComboBox(cboDistrict, DBDataContext.Db.TLKP_DISTRICTs.Where(row => row.PROVINCE_CODE == province));
            UIHelper.SetDataSourceToComboBox(cboCommune, DBDataContext.Db.TLKP_COMMUNEs.Where(row => row.DISTRICT_CODE == districCode));
            UIHelper.SetDataSourceToComboBox(cboVillage, DBDataContext.Db.TLKP_VILLAGEs.Where(row => row.COMMUNE_CODE == communeCode));

            cboProvince.SelectedValue = province;
            cboDistrict.SelectedValue = districCode;
            cboCommune.SelectedValue = communeCode;
            cboVillage.SelectedValue = villageCode;
        }


        private void readCustomerMeter()
        {
            _objCustomerMeter = DBDataContext.Db.TBL_CUSTOMER_METERs.FirstOrDefault(r => r.CUSTOMER_ID == _objNew.CUSTOMER_ID && r.IS_ACTIVE);
            if (_objCustomerMeter != null)
            {
                cboCableShield.SelectedValue = _objCustomerMeter.CABLE_SHIELD_ID;
                cboMeterShield.SelectedValue = _objCustomerMeter.METER_SHIELD_ID;
                _objMeter = DBDataContext.Db.TBL_METERs.FirstOrDefault(m => m.METER_ID == _objCustomerMeter.METER_ID);
                txtMeter.Text = _objMeter.METER_CODE;
                txtMeter.AcceptSearch();

                _objBreaker = DBDataContext.Db.TBL_CIRCUIT_BREAKERs.FirstOrDefault(b => b.BREAKER_ID == _objCustomerMeter.BREAKER_ID);
                txtBreaker.Text = _objBreaker.BREAKER_CODE;
                txtBreaker.AcceptSearch();

                _objBox = DBDataContext.Db.TBL_BOXes.FirstOrDefault(b => b.BOX_ID == _objCustomerMeter.BOX_ID);
                txtBox.Text = _objBox.BOX_CODE;
                txtBox.AcceptSearch();
            }
        }

        private void readCustomerEquipment()
        {
            var qry = from ce in DBDataContext.Db.TBL_CUSTOMER_EQUIPMENTs
                      join e in DBDataContext.Db.TBL_EQUIPMENTs on ce.EQUIPMENT_ID equals e.EQUIPMENT_ID
                      where ce.CUSTOMER_ID == _objNew.CUSTOMER_ID
                      select new
                      {
                          CUS_EQUIPMENT_ID = ce.CUS_EQUIPMENT_ID,
                          EQUIPMENT_ID = e.EQUIPMENT_ID,
                          EQUIPMENT_NAME = e.EQUIPMENT_NAME,
                          QUANTITY = ce.QUANTITY,
                          POWER = ce.POWER,
                          TOTAL_POWER = ce.TOTAL_POWER,
                          COMMENT = ce.COMMENT,
                          _DB = true,   // dummy field.
                          _EDITED = false  // dummy field.
                      };
            foreach (DataRow row in qry._ToDataTable().Rows)
            {
                dgvEquitment.Rows.Add(row.ItemArray);
            }
            calculateTotalPower();
        }

        private void bind()
        {
            //Sex
            UIHelper.SetDataSourceToComboBox(cboSex, DBDataContext.Db.TLKP_SEXes);

            //Customertype
            //UIHelper.SetDataSourceToComboBox(cboCustomerType, Lookup.GetCustomerTypes());

            //Phase
            UIHelper.SetDataSourceToComboBox(cboPhase, Lookup.GetPowerPhases());

            //Amp
            UIHelper.SetDataSourceToComboBox(cboAMP, Lookup.GetPowerAmpare());

            //Vol
            UIHelper.SetDataSourceToComboBox(cboVol, Lookup.GetPowerVoltage());

            //province
            UIHelper.SetDataSourceToComboBox(cboProvince, DBDataContext.Db.TLKP_PROVINCEs);

            //Area
            UIHelper.SetDataSourceToComboBox(cboArea, Lookup.GetAreas());

            //Price
            var qPrice = from p in DBDataContext.Db.TBL_PRICEs
                         join c in DBDataContext.Db.TLKP_CURRENCies on p.CURRENCY_ID equals c.CURRENCY_ID
                         where p.IS_ACTIVE
                         let pValue = DBDataContext.Db.TBL_PRICE_DETAILs.FirstOrDefault(x => x.PRICE_ID == p.PRICE_ID)
                         select new
                         {
                             p.PRICE_ID,
                             PRICE_NAME = p.PRICE_NAME + " (" + c.CURRENCY_SING + ")"
                         };

            UIHelper.SetDataSourceToComboBox(cboPrice, qPrice._ToDataTable());

            //Billing cycle
            UIHelper.SetDataSourceToComboBox(cboBillingCycle, Lookup.GetBillingCycles());

            // Seal
            UIHelper.SetDataSourceToComboBox(cboMeterShield, DBDataContext.Db.TBL_SEALs.Where(row => row.IS_ACTIVE));
            UIHelper.SetDataSourceToComboBox(cboCableShield, DBDataContext.Db.TBL_SEALs.Where(row => row.IS_ACTIVE));



            DataTable dtVoid = new DataTable();
            dtVoid.Columns.Add("IsVoid", typeof(bool));
            dtVoid.Columns.Add("Name", typeof(string));

            DataRow dr = dtVoid.NewRow();
            dr["Name"] = Resources.BUY_POWER;
            dr["IsVoid"] = false;
            dtVoid.Rows.Add(dr);

            DataRow dr1 = dtVoid.NewRow();
            dr1["Name"] = Resources.VOID_BUY_POWER;
            dr1["IsVoid"] = true;
            dtVoid.Rows.Add(dr1);
            UIHelper.SetDataSourceToComboBox(cboIsVoidPurchase, dtVoid);

            //Customer Group
            UIHelper.SetDataSourceToComboBox(cboCustomerGroup, DBDataContext.Db.TLKP_CUSTOMER_GROUPs.OrderBy(x => x.DESCRIPTION));

            //Customer Connection Type 
            UIHelper.SetDataSourceToComboBox(cboConnectionType, DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs.ToList());
        }

        private void write()
        {
            _objNew.ADDRESS = txtAddress.Text;
            _objNew.AMP_ID = int.Parse(cboAMP.SelectedValue.ToString());
            _objNew.AREA_ID = int.Parse(cboArea.SelectedValue.ToString());

            _objNew.CHECK_DIGIT = txtCheckDigit.Text;
            _objNew.CLOSED_DATE = UIHelper._DefaultDate;
            _objNew.COMPANY_NAME = txtCompanyName.Text;
            _objNew.CREATED_BY = Login.CurrentLogin.LOGIN_NAME;
            _objNew.CUSTOMER_CODE = txtCustomerCode.Text;
            // insert 1 to customer type, because It's not allow to insert null
            _objNew.CUSTOMER_TYPE_ID = _objOld.CUSTOMER_TYPE_ID == 0 ? 1 : _objOld.CUSTOMER_TYPE_ID;

            _objNew.FIRST_NAME = txtFirstName.Text;
            _objNew.FIRST_NAME_KH = txtFirstNameKH.Text;
            _objNew.GENDER_ID = int.Parse(cboSex.SelectedValue.ToString());
            _objNew.HOUSE_NO = txtHouseNo.Text;
            _objNew.ID_CARD_NO = txtIdNumber.Text;
            _objNew.JOB = txtJob.Text;
            _objNew.LAST_NAME = txtLastName.Text;
            _objNew.LAST_NAME_KH = txtLastNameKH.Text;
            _objNew.PHASE_ID = int.Parse(cboPhase.SelectedValue.ToString());
            _objNew.PHONE_1 = txtPhone1.Text;
            _objNew.PHONE_2 = txtPhone2.Text;
            _objNew.PHOTO = UIHelper.ConvertImageToBinary(picPhoto.Image);

            _objNew.STREET_NO = txtStreetNo.Text;
            _objNew.VILLAGE_CODE = cboVillage.SelectedValue.ToString();
            _objNew.VOL_ID = int.Parse(cboVol.SelectedValue.ToString());
            _objNew.PRICE_ID = (int)cboPrice.SelectedValue;

            _objNew.BIRTH_DATE = dtpDOB.Value;
            _objNew.BIRTH_PLACE = txtPlaceofBirth.Text;
            _objNew.TOTAL_FAMILY = DataHelper.ParseToInt(txtTotalFamily.Text);

            _objNew.BILLING_CYCLE_ID = (int)cboBillingCycle.SelectedValue;

            //MV Customer
            if (DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.BusinessMVCustomer
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.IndustryMVCustomer
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.BusinessTOU_MVCustomer
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.IndustryTOU_MVCustomer
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.LicenseeMVCustomer
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.MINING_INDUSTRY_MV
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.MANUFACTURING_INDUSTRY_MV
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.TEXTILE_INDUSTRY_MV
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.AGRICULTURE_MV
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.ACTIVITIES_MV
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.AGRICULTURAL_MV
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.BUSINESS_MV
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.PUBLIC_ADMINISTRATION_MV
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.OTHER_SERVICES_MV
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.AGRICULTURAL_WATER_9_TO_7
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.MINING_INDUSTRY_BUY_MV_7_TO_9
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.MINING_INDUSTRY_BUY_MV_9_TO_7
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.MINING_INDUSTRY_BUY_MV_SUN
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.MANUFACTURIING_INDUSTRY_BUY_MV_7_TO_9
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.MANUFACTURIING_INDUSTRY_BUY_MV_9_TO_7
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.MANUFACTURIING_INDUSTRY_BUY_MV_SUN
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.TXTTILE_INDUSTRY_BUY_MV_7_TO_9
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.TXTTILE_INDUSTRY_BUY_MV_9_TO_7
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.TXTTILE_INDUSTRY_BUY_MV_SUN
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.AGRICULTURE_BUY_MV_7_TO_9
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.AGRICULTURE_BUY_MV_9_TO_7
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.AGRICULTURE_BUY_MV_SUN
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.ACTIVITIES_BUY_MV_7_TO_9
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.ACTIVITIES_BUY_MV_9_TO_7
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.ACTIVITIES_BUY_MV_SUN
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.AGRICULTURAL_BUY_MV_7_TO_9
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.AGRICULTURAL_BUY_MV_9_TO_7
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.AGRICULTURAL_BUY_MV_SUN
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.BUSINESS_BUY_MV_7_TO_9
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.BUSINESS_BUY_MV_9_TO_7
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.BUSINESS_BUY_MV_SUN
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.PUBLIC_ADMINISTRATION_BUY_MV_7_TO_9
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.PUBLIC_ADMINISTRATION_BUY_MV_9_TO_7
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.PUBLIC_ADMINISTRATION_BUY_MV_SUN
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.OTHER_SERVICES_BUY_MV_7_TO_9
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.OTHER_SERVICES_BUY_MV_9_TO_7
                || DataHelper.ParseToInt(cboConnectionType.SelectedValue.ToString()) == (int)MVCustomer.OTHER_SERVICES_BUY_MV_SUN)
            {
                _objNew.IS_MV = true;
            }
            else
            {
                _objNew.IS_MV = false;
            }
            _objNew.CUSTOMER_CONNECTION_TYPE_ID = (int)cboConnectionType.SelectedValue;
            _objNew.REQUEST_CONNECTION_DATE = dtpREQUEST_CONNECTION_DATE.Value;

            if (_flag == GeneralProcess.Insert)
            {
                _objNew.CREATED_ON = DBDataContext.Db.GetSystemDate();
                _objNew.STATUS_ID = (int)CustomerStatus.Pending;
                _objNew.ACTIVATE_DATE = UIHelper._DefaultDate;
                _objNew.BILLING_CYCLE_ID = 0; // not yet have
                _objNew.CUT_OFF_DAY = 0;
                _objNew.IS_POST_PAID = false;

            }
            else if (_flag == GeneralProcess.Update)
            {
                _objNew.ACTIVATE_DATE = dtpActivateDate.Value;
                _objNew.CUT_OFF_DAY = (int)nudCutoffDays.Value;
                _objNew.PRICE_ID = cboPrice.SelectedIndex == -1 ? 0 : (int)cboPrice.SelectedValue;
            }
            else if (_flag == GeneralProcess.Delete)
            {
                _objNew.STATUS_ID = (int)CustomerStatus.Cancelled;
                // do not update activate,billing cycle,price, cutoffday
            }
        }

        private string getNextCustomerID()
        {
            string strReturn = string.Empty;
            if (DBDataContext.Db.TBL_CUSTOMERs.Count() > 0)
            {
                // get new id.
                int lastId = DBDataContext.Db.TBL_CUSTOMERs.Max(c => c.CUSTOMER_ID);
                int newID = int.Parse(DBDataContext.Db.TBL_CUSTOMERs.First(c => c.CUSTOMER_ID == lastId).CUSTOMER_CODE) + 1;
                strReturn = newID.ToString("000000");
            }
            else
            {
                strReturn = "000001";
            }
            return strReturn;
        }

        private void calculateTotalPower()
        {
            decimal total = 0.0m;
            foreach (DataGridViewRow row in dgvEquitment.Rows)
            {
                if (row.Visible)
                {
                    total += (decimal)row.Cells[TOTAL_WATT.Name].Value;
                }
            }
            txtTotalPower.Text = total.ToString("N2");
        }

        /// <summary>
        /// display total due amount.
        /// </summary>
        private void loadTotalDueAmount()
        {
            var totalAmount = from i in DBDataContext.Db.TBL_INVOICEs
                              join c in DBDataContext.Db.TLKP_CURRENCies on i.CURRENCY_ID equals c.CURRENCY_ID
                              where i.CUSTOMER_ID == _objNew.CUSTOMER_ID && i.INVOICE_STATUS != (int)InvoiceStatus.Cancel
                              group i by new { c.CURRENCY_ID, c.CURRENCY_SING } into tmpBalance

                              select new
                              {
                                  tmpBalance.Key.CURRENCY_ID,
                                  tmpBalance.Key.CURRENCY_SING,
                                  TOTAL_BALANCE = tmpBalance.Sum(x => x.SETTLE_AMOUNT - x.PAID_AMOUNT)
                              };
            dgvTotalBalance.DataSource = totalAmount;
        }

        private bool saveCustomer()
        {

            if (invalid())
            {
                return false;
            }

            write();
            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {

                    TBL_CHANGE_LOG objChangeLog = new TBL_CHANGE_LOG();
                    //renew customer code & update sequance for each save
                    if (_flag == GeneralProcess.Insert)
                    {
                        _objNew.CUSTOMER_CODE = Method.GetNextSequence(Sequence.Customer, true);
                    }
                    // save general infomation.
                    if (_flag == GeneralProcess.Insert)
                    {
                        objChangeLog = DBDataContext.Db.Insert(_objNew);

                    }
                    else // both delete and edit are just update
                    {
                        objChangeLog = DBDataContext.Db.Update(_objOld, _objNew);
                    }

                    // save customer equipment. 
                    foreach (DataGridViewRow row in dgvEquitment.Rows)
                    {
                        TBL_CUSTOMER_EQUIPMENT objENew = new TBL_CUSTOMER_EQUIPMENT()
                        {
                            CUS_EQUIPMENT_ID = (int)row.Cells["CUS_EQUIPMENT_ID"].Value,
                            CUSTOMER_ID = _objNew.CUSTOMER_ID,
                            EQUIPMENT_ID = (int)row.Cells["EQUIPMENT_ID"].Value,
                            POWER = (decimal)row.Cells["POWER"].Value,
                            QUANTITY = (decimal)row.Cells["QUANTITY"].Value,
                            TOTAL_POWER = (decimal)row.Cells["TOTAL_POWER"].Value,
                            COMMENT = row.Cells["COMMENT"].Value.ToString()
                        };
                        if (row.Visible)
                        {
                            if ((bool)row.Cells["_DB"].Value)
                            {
                                // all data from db
                                // update only field that face update.
                                if ((bool)row.Cells["_EDITED"].Value)
                                {
                                    TBL_CUSTOMER_EQUIPMENT tmp = DBDataContext.Db.TBL_CUSTOMER_EQUIPMENTs.FirstOrDefault(r => r.CUS_EQUIPMENT_ID == objENew.CUS_EQUIPMENT_ID);
                                    if (tmp != null)
                                    {
                                        TBL_CUSTOMER_EQUIPMENT objEOld = new TBL_CUSTOMER_EQUIPMENT();
                                        tmp._CopyTo(objEOld);
                                        DBDataContext.Db.UpdateChild(objEOld, objENew, _objNew, ref objChangeLog);
                                    }
                                }
                            }
                            else
                            {
                                DBDataContext.Db.InsertChild(objENew, _objNew, ref objChangeLog);
                            }
                        }
                        else
                        {
                            if ((bool)row.Cells["_DB"].Value)
                            {
                                DBDataContext.Db.DeleteChild(objENew, _objNew, ref objChangeLog);
                            }
                        }
                    }
                    tran.Complete();
                    DialogResult = DialogResult.OK;
                    return true;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
                return false;
            }
        }

        #endregion Method

        #region Event

        private void btnSave_Click(object sender, EventArgs e)
        {
            saveCustomer();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnActivate_Click(object sender, EventArgs e)
        {
            //if (saveCustomer())
            //{
            //    DialogCustomerActivate diag = new DialogCustomerActivate(this._objNew);
            //    if (diag.ShowDialog() == DialogResult.OK)
            //    {
            //        this.CustomerIsActivated = true;
            //    }
            //    this.DialogResult = DialogResult.OK;
            //} 

            DialogCustomerActivatePrepaid diag = new DialogCustomerActivatePrepaid(DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault());
            if (diag.ShowDialog() == DialogResult.OK)
            {
                CustomerIsActivated = true;
            }
            DialogResult = DialogResult.OK;
        }

        private void cboProvince_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_loading && cboProvince.SelectedIndex != -1)
            {
                string strProvinceCode = cboProvince.SelectedValue.ToString();
                //distict
                UIHelper.SetDataSourceToComboBox(cboDistrict, DBDataContext.Db.TLKP_DISTRICTs.Where(d => d.PROVINCE_CODE == strProvinceCode));
            }
        }

        private void cboDistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_loading && cboProvince.SelectedIndex != -1)
            {
                string strDisCode = cboDistrict.SelectedValue.ToString();
                //communte
                UIHelper.SetDataSourceToComboBox(cboCommune, DBDataContext.Db.TLKP_COMMUNEs.Where(c => c.DISTRICT_CODE == strDisCode));
            }
        }

        private void cboCommune_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_loading && cboCommune.SelectedIndex != -1)
            {
                string strComCode = cboCommune.SelectedValue.ToString();
                //village
                UIHelper.SetDataSourceToComboBox(cboVillage, DBDataContext.Db.TLKP_VILLAGEs.Where(v => v.COMMUNE_CODE == strComCode));
            }
        }

        private void LinkPhoto_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            OpenFileDialog objDialog = new OpenFileDialog();
            objDialog.Filter = "(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|All files (*.*)|*.*";
            objDialog.ShowDialog();
            if (objDialog.FileName != string.Empty)
            {
                picPhoto.ImageLocation = objDialog.FileName;
            }
        }

        private void dtpDOB_MouseDown(object sender, MouseEventArgs e)
        {
            if (dtpDOB.Checked)
            {
                if (dtpDOB.Value.Date.Equals(UIHelper._DefaultDate))
                {
                    dtpDOB.SetValue(DateTime.Now.Date);
                }
                else
                {
                    dtpDOB.CustomFormat = UIHelper._DefaultDateFormat;
                }
            }
            else
            {
                dtpDOB.ClearValue();
            }
        }

        private void txtLastNameKH_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void txtLastName_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void btnAdd_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DialogCustomerEquipment diag = new DialogCustomerEquipment(GeneralProcess.Insert, new TBL_CUSTOMER_EQUIPMENT() { CUSTOMER_ID = _objNew.CUSTOMER_ID });
            if (diag.ShowDialog() == DialogResult.OK)
            {
                int newIndex = dgvEquitment.Rows.Add();
                DataGridViewRow row = dgvEquitment.Rows[newIndex];
                TBL_CUSTOMER_EQUIPMENT obj = diag.Object;
                row.Cells[CUS_EQUIPMENT_ID.Name].Value = obj.CUS_EQUIPMENT_ID;
                row.Cells[EQUIPMENT_ID.Name].Value = obj.EQUIPMENT_ID;
                row.Cells[EQUIPMENT_NAME.Name].Value = DBDataContext.Db.TBL_EQUIPMENTs.FirstOrDefault(r => r.EQUIPMENT_ID == obj.EQUIPMENT_ID).EQUIPMENT_NAME;
                row.Cells[EQUIPMENT_QUANTITY.Name].Value = obj.QUANTITY;
                row.Cells[WATT.Name].Value = obj.POWER;
                row.Cells[TOTAL_WATT.Name].Value = obj.TOTAL_POWER;
                row.Cells[COMMENT.Name].Value = obj.COMMENT;

                // make sure need to insert to db.
                row.Cells[_DB.Name].Value = false;
                // makse sure it's not update.
                // useful for update operation
                row.Cells[_EDITED.Name].Value = false;

                calculateTotalPower();
            }
        }
        private int getVisibleRows(DataGridView dgv)
        {
            int tmp = 0;
            foreach (DataGridViewRow row in dgv.Rows)
            {
                if (row.Visible)
                {
                    tmp++;
                }
            }
            return tmp;
        }
        private void btnEdit_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // since some row are hidden 
            // we need to check only number of visible 
            // because when all row in data grid view are 
            // hidden it select the last selected row 
            if (getVisibleRows(dgvEquitment) == 0)
            {
                return;
            }

            if (dgvEquitment.SelectedRows.Count == 0)
            {
                return;
            }

            DataGridViewRow row = dgvEquitment.SelectedRows[0];

            DialogCustomerEquipment diag = new DialogCustomerEquipment(
                GeneralProcess.Update,
                new TBL_CUSTOMER_EQUIPMENT()
                {
                    CUS_EQUIPMENT_ID = (int)row.Cells[CUS_EQUIPMENT_ID.Name].Value,
                    EQUIPMENT_ID = (int)row.Cells[EQUIPMENT_ID.Name].Value,
                    QUANTITY = (decimal)row.Cells[EQUIPMENT_QUANTITY.Name].Value,
                    POWER = (decimal)row.Cells[WATT.Name].Value,
                    TOTAL_POWER = (decimal)row.Cells[TOTAL_WATT.Name].Value,
                    COMMENT = row.Cells[COMMENT.Name].Value.ToString()
                }
            );

            if (diag.ShowDialog() == DialogResult.OK)
            {
                TBL_CUSTOMER_EQUIPMENT obj = diag.Object;
                row.Cells[CUS_EQUIPMENT_ID.Name].Value = obj.CUS_EQUIPMENT_ID;
                row.Cells[EQUIPMENT_ID.Name].Value = obj.EQUIPMENT_ID;
                row.Cells[EQUIPMENT_NAME.Name].Value = DBDataContext.Db.TBL_EQUIPMENTs.FirstOrDefault(r => r.EQUIPMENT_ID == obj.EQUIPMENT_ID).EQUIPMENT_NAME;
                row.Cells[EQUIPMENT_QUANTITY.Name].Value = obj.QUANTITY;
                row.Cells[WATT.Name].Value = obj.POWER;
                row.Cells[TOTAL_WATT.Name].Value = obj.TOTAL_POWER;
                row.Cells[COMMENT.Name].Value = obj.COMMENT;

                // flag that this row is update 
                // if it's from DB so update database.
                row.Cells[_EDITED.Name].Value = true;

                // update total power to display
                calculateTotalPower();
            }

        }

        private void btnRemove_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // since some row are hidden 
            // we need to check only number of visible 
            // because when all row in data grid view are 
            // hidden it select the last selected row 
            if (getVisibleRows(dgvEquitment) == 0)
            {
                return;
            }

            if (dgvEquitment.SelectedRows.Count == 0)
            {
                return;
            }
            DataGridViewRow row = dgvEquitment.SelectedRows[0];
            if (MsgBox.ShowQuestion(Resources.MSQ_REMOVE_DEVICE, Resources.REMOVE_DEVICE) == DialogResult.Yes)
            {
                row.Visible = false;
                // update total power.
                calculateTotalPower();
            };
        }

        private void InputEnglish(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void InputKhmer(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void txtMeter_AdvanceSearch(object sender, EventArgs e)
        {
            if (txtMeter.Text.Trim() != string.Empty)
            {
                string strMeterCode = txtMeter.Text;
                _objMeter = DBDataContext.Db.TBL_METERs.FirstOrDefault(m => m.METER_CODE == strMeterCode);
                if (_objMeter != null)
                {
                    TBL_METER_TYPE objMeterType = DBDataContext.Db.TBL_METER_TYPEs.FirstOrDefault(mt => mt.METER_TYPE_ID == _objMeter.METER_TYPE_ID);
                    txtMeterType.Text = objMeterType.METER_TYPE_NAME;
                    txtMeterAmp.Text = DBDataContext.Db.TBL_AMPAREs.FirstOrDefault(a => a.AMPARE_ID == objMeterType.METER_AMP_ID).AMPARE_NAME;
                    txtMeterPhase.Text = DBDataContext.Db.TBL_PHASEs.FirstOrDefault(p => p.PHASE_ID == objMeterType.METER_PHASE_ID).PHASE_NAME;
                    txtMeterVol.Text = DBDataContext.Db.TBL_VOLTAGEs.FirstOrDefault(v => v.VOLTAGE_ID == objMeterType.METER_VOL_ID).VOLTAGE_NAME;
                    txtMeterConstant.Text = DBDataContext.Db.TBL_CONSTANTs.FirstOrDefault(c => c.CONSTANT_ID == objMeterType.METER_CONST_ID).CONSTANT_NAME;
                    txtBreaker.Focus();
                }
                else
                {
                    txtMeter.CancelSearch();
                    MsgBox.ShowInformation(Resources.MS_METER_NOT_FOUND);
                }
            }
            else
            {
                txtMeter.CancelSearch();
            }
        }

        private void txtMeter_CancelAdvanceSearch(object sender, EventArgs e)
        {
            _objMeter = null;
            txtMeterType.Text =
            txtMeterAmp.Text =
            txtMeterPhase.Text =
            txtMeterVol.Text =
            txtMeterConstant.Text = string.Empty;
        }

        private void txtBreaker_AdvanceSearch(object sender, EventArgs e)
        {
            if (txtBreaker.Text.Trim() != string.Empty)
            {
                string strBreakerCode = txtBreaker.Text;
                _objBreaker = DBDataContext.Db.TBL_CIRCUIT_BREAKERs.FirstOrDefault(b => b.BREAKER_CODE == strBreakerCode);
                if (_objBreaker != null)
                {
                    TBL_CIRCUIT_BREAKER_TYPE objBreakerType = DBDataContext.Db.TBL_CIRCUIT_BREAKER_TYPEs.FirstOrDefault(bt => bt.BREAKER_TYPE_ID == _objBreaker.BREAKER_TYPE_ID);
                    txtBreakerType.Text = objBreakerType.BREAKER_TYPE_NAME;
                    txtBreakerAmp.Text = DBDataContext.Db.TBL_AMPAREs.FirstOrDefault(a => a.AMPARE_ID == objBreakerType.BREAKER_AMP_ID).AMPARE_NAME;
                    txtBreakerPhase.Text = DBDataContext.Db.TBL_PHASEs.FirstOrDefault(p => p.PHASE_ID == objBreakerType.BREAKER_PHASE_ID).PHASE_NAME;
                    txtBreakerVol.Text = DBDataContext.Db.TBL_VOLTAGEs.FirstOrDefault(v => v.VOLTAGE_ID == objBreakerType.BREAKER_VOL_ID).VOLTAGE_NAME;
                    txtBreakerConstant.Text = DBDataContext.Db.TBL_CONSTANTs.FirstOrDefault(c => c.CONSTANT_ID == objBreakerType.BREAKER_CONST_ID).CONSTANT_NAME;
                    txtBox.Focus();
                }
                else
                {
                    txtBreaker.CancelSearch();
                    MsgBox.ShowInformation(Resources.MS_BREAKER_NOT_FOUND);
                }
            }
            else
            {
                txtBreaker.CancelSearch();
            }
        }

        private void txtBreaker_CancelAdvanceSearch(object sender, EventArgs e)
        {
            _objBreaker = null;
            txtBreakerType.Text =
            txtBreakerAmp.Text =
            txtBreakerPhase.Text =
            txtBreakerVol.Text =
            txtBreakerConstant.Text = string.Empty;
        }

        private void txtBox_AdvanceSearch(object sender, EventArgs e)
        {
            if (txtBox.Text.Trim() != string.Empty)
            {
                string strBox = txtBox.Text;
                _objBox = DBDataContext.Db.TBL_BOXes.FirstOrDefault(b => b.BOX_CODE == strBox);
                if (_objBox != null)
                {
                    _objPole = DBDataContext.Db.TBL_POLEs.FirstOrDefault(p => p.POLE_ID == _objBox.POLE_ID);
                    txtPoleCode.Text = _objPole.POLE_CODE;
                    txtCollector.Text = DBDataContext.Db.TBL_EMPLOYEEs.FirstOrDefault(emp => emp.EMPLOYEE_ID == _objPole.COLLECTOR_ID).EMPLOYEE_NAME;
                    txtBiller.Text = DBDataContext.Db.TBL_EMPLOYEEs.FirstOrDefault(emp => emp.EMPLOYEE_ID == _objPole.BILLER_ID).EMPLOYEE_NAME;
                    txtCutter.Text = DBDataContext.Db.TBL_EMPLOYEEs.FirstOrDefault(emp => emp.EMPLOYEE_ID == _objPole.CUTTER_ID).EMPLOYEE_NAME;
                }
                else
                {
                    txtBox.CancelSearch();
                    MsgBox.ShowInformation(Resources.MS_BOX_NOT_FOUND);
                }
            }
            else
            {
                txtBox.CancelSearch();
            }
        }

        private void txtBox_CancelAdvanceSearch(object sender, EventArgs e)
        {
            _objPole = null;
            _objBox = null;
            txtPoleCode.Text =
            txtCollector.Text =
            txtBiller.Text =
            txtCutter.Text = string.Empty;
        }

        private void btnChangeMeter_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DialogCustomerChangeMeterPrepaid diag = new DialogCustomerChangeMeterPrepaid(_objNew);
            if (diag.ShowDialog() == DialogResult.OK)
            {
                // read customer meter.
                readCustomerMeter();
            }
        }

        private void btnChangeBreaker_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DialogCustomerChangeBreaker diag = new DialogCustomerChangeBreaker(_objNew);
            if (diag.ShowDialog() == DialogResult.OK)
            {
                // read customer meter.
                readCustomerMeter();
            }
        }

        private void btnChangeBox_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DialogCustomerChangeBox diag = new DialogCustomerChangeBox(_objNew);
            if (diag.ShowDialog() == DialogResult.OK)
            {
                // read customer meter.
                readCustomerMeter();
                cboArea.SelectedValue = diag.Customer.AREA_ID;
            }
        }

        private void cboInvoiceStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadInvoice();
        }

        private void loadInvoice()
        {
            if (cboInvoiceStatus.SelectedIndex != -1 && _flag == GeneralProcess.Update)
            {
                int intStatus = (int)cboInvoiceStatus.SelectedValue;
                bool blDate = dtpStartDate.Checked;
                dtpEndDate.Enabled = blDate;
                try
                {
                    var invioceInfo = from i in DBDataContext.Db.TBL_INVOICEs
                                      join c in DBDataContext.Db.TLKP_CURRENCies on i.CURRENCY_ID equals c.CURRENCY_ID
                                      where
                                      i.CUSTOMER_ID == _objNew.CUSTOMER_ID
                                      &&
                                      (i.INVOICE_STATUS == intStatus || intStatus == 0)
                                      &&
                                      ((i.START_DATE.Date >= dtpStartDate.Value.Date
                                          && i.START_DATE <= dtpEndDate.Value.Date) || blDate == false)
                                      select new
                                      {
                                          i.INVOICE_ID,
                                          i.INVOICE_DATE,
                                          i.INVOICE_NO,
                                          i.INVOICE_TITLE,
                                          i.SETTLE_AMOUNT,
                                          i.PAID_AMOUNT,
                                          DUE_AMOUNT = i.SETTLE_AMOUNT - i.PAID_AMOUNT,
                                          c.CURRENCY_ID,
                                          c.CURRENCY_SING,
                                          i.DUE_DATE,
                                          DUE_DAY = (DateTime.Now.Date - i.DUE_DATE.Date).TotalDays > 0 && i.INVOICE_STATUS == (int)InvoiceStatus.Open ? (DateTime.Now.Date - i.DUE_DATE.Date).TotalDays : 0
                                      };
                    dgvInvoiceList.DataSource = invioceInfo;
                    loadTotalDueAmount();

                }
                catch (Exception exp)
                {
                    MsgBox.ShowError(exp);
                }
            }
        }

        private void loadPurchasePowerMonthly()
        {
            if (_objPreCus == null)
            {
                return;
            }

            bool blnIsVoid = (bool)cboIsVoidPurchase.SelectedValue;
            DateTime dt = dtPurchaseMonth.Value;
            var CusBuyPow = from cb in DBDataContext.Db.TBL_CUSTOMER_BUYs
                            join c in DBDataContext.Db.TLKP_CURRENCies on cb.CURRENCY_ID equals c.CURRENCY_ID
                            where cb.PREPAID_CUS_ID == _objPreCus.PREPAID_CUS_ID
                            && cb.IS_VOID == blnIsVoid
                            && cb.CREATE_ON.Month == dt.Month
                            && cb.CREATE_ON.Year == dt.Year
                            && cb.PARENT_ID == 0
                            select new
                            {
                                cb.CUSTOMER_BUY_ID,
                                cb.PREPAID_CUS_ID,
                                cb.BUY_TIMES,
                                cb.CREATE_ON,
                                cb.BUY_QTY,
                                cb.COMPENSATED,
                                cb.DISCOUNT_QTY,
                                cb.PRICE,
                                cb.BUY_AMOUNT,
                                cb.PAID_CREDIT_AMOUNT,
                                c.CURRENCY_SING,
                                cb.IS_VOID
                            };
            dgvCusPurchase.DataSource = CusBuyPow;

            txtTotalPurchasePower.Text = CusBuyPow.Count() == 0 ? string.Empty : CusBuyPow.Sum(x => x.BUY_QTY).ToString("N0");
        }

        private void loadPurchaseYearly()
        {
            //if  new customer not activate
            if (_objNew.STATUS_ID == (int)CustomerStatus.Pending || _objNew.STATUS_ID == (int)CustomerStatus.Cancelled)
            {
                tabControl1.TabPages.Remove(tabBUY_POWER_YEARLY);
                return;
            }

            var varCus = from cb in DBDataContext.Db.TBL_CUSTOMER_BUYs
                         where cb.PREPAID_CUS_ID == _objPreCus.PREPAID_CUS_ID
                         && cb.IS_VOID == false
                         group cb by new DateTime(cb.CREATE_ON.Year, cb.CREATE_ON.Month, 1) into cusBuy
                         orderby cusBuy.Key
                         select new
                         {
                             MONTH = cusBuy.Key != null ? cusBuy.Key : UIHelper._DefaultDate,
                             PURCHASE_POWER = cusBuy.Key != null ? cusBuy.Sum(x => x.BUY_QTY) : 0
                         };


            DataTable dt = new DataTable();
            dt.Columns.Add("MONTH");
            dt.Columns.Add("PURCHASE_POWER", typeof(decimal));



            foreach (var obj in varCus)
            {
                DataRow row = dt.NewRow();
                row[0] = obj.MONTH.ToString("MMM yy");
                row[1] = obj.PURCHASE_POWER;
                dt.Rows.Add(row);
            }
            // fill up chart for 2 year (24 months)
            // to make sure chart size remain the same
            while (dt.Rows.Count < 24)
            {
                DataRow row = dt.NewRow();
                row[0] = "";
                row[1] = 0;
                dt.Rows.Add(row);
            }

            //styling chart
            chart1.TempDirectory = "temp";
            chart1.Type = ChartType.Combo;
            chart1.Palette = new Color[] { Color.FromArgb(49, 255, 49), Color.FromArgb(0, 156, 255), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) };
            chart1.ChartArea.Background.Color = Color.FromArgb(255, 250, 250, 250);
            chart1.YAxis.AlternateGridBackground.Color = Color.FromArgb(100, 235, 235, 235);
            chart1.ShadingEffectMode = ShadingEffectMode.Two;

            //chart1.DefaultElement.ShowValue = true; 

            //set global properties
            chart1.Title = "Customer Purchase Power";
            // Set the x axis label
            chart1.ChartArea.XAxis.Label.Text = "Month";
            // Set the y axis label
            chart1.ChartArea.YAxis.Label.Text = "Power(kWh)";

            //Adding series programatically
            chart1.Series.Name = "Power (kWh)";
            chart1.LegendBox.Visible = false;
            chart1.Series.Data = dt;
            chart1.SeriesCollection.Add();

        }

        private void loadPrepaidUsage()
        {
            if (_objPreCus == null)
            {
                return;
            }

            var d1 = new DateTime(dtpYear.Value.Year, 1, 1);
            var d2 = new DateTime(dtpYear.Value.Year, 12, 31);
            var all = !dtpYear.Checked;

            var q = from c in DBDataContext.Db.TBL_PREPAID_CREDITs
                    join cr in DBDataContext.Db.TLKP_CURRENCies on c.CURRENCY_ID equals cr.CURRENCY_ID
                    where c.CUSTOMER_ID == _objPreCus.CUSTOMER_ID
                          && (c.CREDIT_MONTH.Date >= d1.Date && c.CREDIT_MONTH.Date <= d2.Date || all)
                    orderby c.CREDIT_MONTH ascending
                    select new
                    {
                        c.PREPAID_CREDIT_ID,
                        c.START_DATE,
                        c.END_DATE,
                        c.CREDIT_MONTH,
                        c.USAGE,
                        PRICE_CREDIT = c.PRICE,
                        c.CREDIT_AMOUNT,
                        CREDIT_BALANCE = DBDataContext.Db.TBL_PREPAID_CREDITs.Where(x => x.CUSTOMER_ID == c.CUSTOMER_ID
                            && x.PREPAID_CREDIT_ID <= c.PREPAID_CREDIT_ID).Sum(x => x.CREDIT_AMOUNT - x.PAID_AMOUNT),
                        CREDIT_PAID_AMOUNT = c.PAID_AMOUNT,
                        CREDIT_CURRENCY_SING = cr.CURRENCY_SING
                    };
            dgvPrepaidUsage.DataSource = q.OrderByDescending(x => x.CREDIT_MONTH).ToList();

            if (!DBDataContext.Db.TBL_PREPAID_CREDITs.Any(x => x.CUSTOMER_ID == _objPreCus.CUSTOMER_ID))
                return;

            var remainCredit =
                DBDataContext.Db.TBL_PREPAID_CREDITs.Where(x => x.CUSTOMER_ID == _objPreCus.CUSTOMER_ID)
                    .Sum(x => x.CREDIT_AMOUNT - x.PAID_AMOUNT);
            txtTotalRemainCredit.Text = UIHelper.FormatCurrency(remainCredit, _objKhCurrency.CURRENCY_ID);
            txtTotalRemainCreditCurrencySign.Text = _objKhCurrency.CURRENCY_SING;
        }


        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(_objNew);
        }

        private void cboDeposit_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        }

        #endregion Event

        #region Adjust Invoice Event

        private void linkAdjustUsage_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //if user not yet open cash drawer
            //not allow to adjust invoice
            if (Login.CurrentCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_ADJUST_INVOICE, string.Empty) == DialogResult.Yes)
                    new DialogOpenCashDrawer().ShowDialog();
                if (Login.CurrentCashDrawer == null)
                    return;
            }

            if (dgvInvoiceList.SelectedRows.Count == 0)
            {
                return;
            }

            TBL_INVOICE objInvoice = DBDataContext.Db.TBL_INVOICEs.FirstOrDefault(x => x.INVOICE_ID == Int32.Parse(dgvInvoiceList.SelectedRows[0].Cells["INVOICE_ID"].Value.ToString()));
            if (objInvoice.INVOICE_STATUS != (int)InvoiceStatus.Open)
            {
                MsgBox.ShowInformation(Resources.MS_CANNOT_ADJUST_PAID_INVOICE);
                return;
            }
            if (objInvoice.IS_SERVICE_BILL == true)
            {
                MsgBox.ShowInformation(Resources.MS_CANNOT_ADJUST_SERVICE_INVOICE);
                return;
            }

            DialogAdjustment dig = new DialogAdjustment(_objNew, objInvoice, _objOld.PRICE_ID);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                loadInvoice();
            }
        }

        bool isNotValidToAdjust()
        {
            bool blnReturn = false;
            //if user not yet open cash drawer
            //not allow to adjust invoice
            if (Login.CurrentCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_ADJUST_INVOICE, string.Empty) == DialogResult.Yes)
                    new DialogOpenCashDrawer().ShowDialog();
                if (Login.CurrentCashDrawer == null)
                    blnReturn = true;
            }

            if (dgvInvoiceList.SelectedRows.Count == 0)
            {
                blnReturn = true;
            }
            else
            {

                TBL_INVOICE objInvoice = DBDataContext.Db.TBL_INVOICEs.FirstOrDefault(x => x.INVOICE_ID == Int32.Parse(dgvInvoiceList.SelectedRows[0].Cells[INVOICE_ID.Name].Value.ToString()));

                //can adust when the invoice is in last cycle
                if (objInvoice.IS_SERVICE_BILL == false)
                {
                    DateTime datNextBillingMonth = Method.GetNextBillingMonth(objInvoice.CYCLE_ID);
                    if (datNextBillingMonth.AddMonths(-1).Date != objInvoice.INVOICE_MONTH.Date)
                    {
                        MsgBox.ShowInformation(Resources.MS_INVALID_ADJUST_INVOICE);
                        blnReturn = true;
                    }
                }

                //can adjust when
                //Invoice status == Open
                //OR Closeed Invoice that have settle amount == 0 
                if (objInvoice.PAID_AMOUNT > 0)
                {
                    MsgBox.ShowInformation(Resources.MS_CANNOT_ADJUST_PAID_INVOICE);
                    blnReturn = true;
                }
            }
            return blnReturn;
        }

        private void linkAdjustAmount_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (isNotValidToAdjust())
            {
                return;
            }
            TBL_INVOICE objInvoice = DBDataContext.Db.TBL_INVOICEs.FirstOrDefault(x => x.INVOICE_ID == Int32.Parse(dgvInvoiceList.SelectedRows[0].Cells["INVOICE_ID"].Value.ToString()));
            DialogAdjustmentAmount dig = new DialogAdjustmentAmount(objInvoice, _objOld.PRICE_ID);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                loadInvoice();
            }
        }

        private void linkPrintInvoice_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (dgvInvoiceList.SelectedRows.Count > 0)
            {
                SupportTaker objSt = new SupportTaker(SupportTaker.Process.ViewInvoice);
                if (objSt.Check())
                {
                    return;
                }

                if (printingInvoice)
                {
                    TBL_PRINT_INVOICE objPrintInvoice = new TBL_PRINT_INVOICE()
                    {
                        PRINT_DATE = DBDataContext.Db.GetSystemDate(),
                        LOGIN_ID = Login.CurrentLogin.LOGIN_ID
                    };
                    using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                    {

                        DBDataContext.Db.TBL_PRINT_INVOICEs.InsertOnSubmit(objPrintInvoice);
                        DBDataContext.Db.SubmitChanges();
                        int invoiceID = int.Parse(dgvInvoiceList.SelectedRows[0].Cells[INVOICE_ID.Name].Value.ToString());
                        DBDataContext.Db.TBL_PRINT_INVOICE_DETAILs.InsertOnSubmit(new TBL_PRINT_INVOICE_DETAIL()
                        {
                            PRINT_INVOICE_ID = objPrintInvoice.PRINT_INVOICE_ID,
                            INVOICE_ID = invoiceID,
                            PRINT_ORDER = 0
                        });
                        DBDataContext.Db.SubmitChanges();
                        tran.Complete();
                    }
                    new DialogCustomerPrintInvoice(true, objPrintInvoice.PRINT_INVOICE_ID).ShowDialog();
                }
                else
                {
                    using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                    {
                        long invoiceID = (long)dgvInvoiceList.SelectedRows[0].Cells["INVOICE_ID"].Value;
                        DBDataContext.Db.TBL_INVOICE_TO_PRINTs.DeleteAllOnSubmit(DBDataContext.Db.TBL_INVOICE_TO_PRINTs);
                        DBDataContext.Db.TBL_INVOICE_TO_PRINTs.InsertOnSubmit(new TBL_INVOICE_TO_PRINT() { INVOICE_ID = invoiceID });
                        DBDataContext.Db.SubmitChanges();
                        tran.Complete();
                    }
                    new DialogCustomerPrintInvoice(false, 0).ShowDialog();
                }
            }
        }

        #endregion Adjust Invoice Event

        private void btnAddArea_AddItem(object sender, EventArgs e)
        {
            DialogArea dig = new DialogArea(GeneralProcess.Insert, new TBL_AREA());
            if (dig.ShowDialog() == DialogResult.OK)
            {
                UIHelper.SetDataSourceToComboBox(cboArea, DBDataContext.Db.TBL_AREAs.Where(t => t.IS_ACTIVE == true).OrderBy(row => row.AREA_NAME));
                cboArea.SelectedValue = dig.Area.AREA_ID;
            }
        }

        private void btnAddPrice_AddItem(object sender, EventArgs e)
        {
            DialogPrice dig = new DialogPrice(new TBL_PRICE(), GeneralProcess.Insert);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                UIHelper.SetDataSourceToComboBox(cboPrice, DBDataContext.Db.TBL_PRICEs.Where(row => row.IS_ACTIVE));
                cboPrice.SelectedValue = dig.Price.PRICE_ID;
            }
        }

        private void btnAddAmpare_AddItem(object sender, EventArgs e)
        {
            DialogAmpare dig = new DialogAmpare(GeneralProcess.Insert, new TBL_AMPARE());
            if (dig.ShowDialog() == DialogResult.OK)
            {
                UIHelper.SetDataSourceToComboBox(cboAMP, DBDataContext.Db.TBL_AMPAREs.Where(t => t.IS_ACTIVE == true));
                cboAMP.SelectedValue = dig.Ampare.AMPARE_ID;
            }
        }

        private void btnAddPhase_AddItem(object sender, EventArgs e)
        {
            DialogPhase dig = new DialogPhase(GeneralProcess.Insert, new TBL_PHASE());
            if (dig.ShowDialog() == DialogResult.OK)
            {
                UIHelper.SetDataSourceToComboBox(cboPhase, DBDataContext.Db.TBL_PHASEs.Where(t => t.IS_ACTIVE == true));
                cboPhase.SelectedValue = dig.Phase.PHASE_ID;
            }
        }

        private void btnAddVoltage_AddItem(object sender, EventArgs e)
        {
            DialogVoltage dig = new DialogVoltage(new TBL_VOLTAGE(), GeneralProcess.Insert);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                UIHelper.SetDataSourceToComboBox(cboVol, DBDataContext.Db.TBL_VOLTAGEs.Where(t => t.IS_ACTIVE == true));
                cboVol.SelectedValue = dig.Voltage.VOLTAGE_ID;
            }
        }

        private void dtPurchaseMonth_ValueChanged(object sender, EventArgs e)
        {
            loadPurchasePowerMonthly();
        }

        private void cboIsVoidPurchase_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadPurchasePowerMonthly();
        }

        private void btnPay_Click(object sender, EventArgs e)
        {
            int invoiceOpen = DBDataContext.Db.TBL_INVOICEs.Where(x => x.CUSTOMER_ID == _objNew.CUSTOMER_ID && x.INVOICE_STATUS == (int)InvoiceStatus.Open).Count();
            if (invoiceOpen == 0)
            {
                return;
            }

            DialogReceivePayment dig = new DialogReceivePayment(_objNew);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                loadInvoice();
                //loadTotalDueAmount();
            }
        }

        private void btnCharge_Click(object sender, EventArgs e)
        {
            if (_objNew.STATUS_ID == (int)CustomerStatus.Closed)
            {
                MsgBox.ShowInformation(Resources.MS_CANNOT_ADD_CHARGE_TO_CLOSE_CUSTOMER);
                return;
            }
            if (_objNew.STATUS_ID == (int)CustomerStatus.Cancelled)
            {
                MsgBox.ShowInformation(Resources.MS_CANNOT_ADD_CHARGE_TO_CANCEL_CUSTOMER);
                return;
            }
            DialogCustomerCharge dig = new DialogCustomerCharge(_objNew, GeneralProcess.Insert);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                loadInvoice();
                //loadTotalDueAmount();
            }
        }

        private void lblPrintReciptPurchasePower_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (dgvCusPurchase.SelectedRows.Count == 0)
            {
                return;
            }

            var objCustomerBuyPow = DBDataContext.Db.TBL_CUSTOMER_BUYs.FirstOrDefault(x => x.CUSTOMER_BUY_ID == (long)dgvCusPurchase.SelectedRows[0].Cells["CUSTOMER_BUY_ID"].Value);

            if (objCustomerBuyPow.IS_VOID)
            {
                MsgBox.ShowInformation(Resources.MS_CANNOT_PRINT_RECEIPT);
                return;
            }


            var ch = new CrystalReportHelper("ReportReceiptPruchasePower.rpt");

            ch.SetParameter("@CUSTOMER_BUY_ID", (int)objCustomerBuyPow.CUSTOMER_BUY_ID);
            ch.PrintReport(Settings.Default.PRINTER_RECIEPT);

        }

        private void linkAddDeposit_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //GET DEPOSIT THAT NOT YET PAID
            TBL_CUS_DEPOSIT objDeposit = DBDataContext.Db.TBL_CUS_DEPOSITs.FirstOrDefault(row => !row.IS_PAID && row.CUSTOMER_ID == _objNew.CUSTOMER_ID);
            DialogDepositAdd dialog = null;
            if (objDeposit != null)
            {
                dialog = new DialogDepositAdd(_objNew, DepositAction.AddDeposit, GeneralProcess.Update, objDeposit);
            }
            else
            {
                dialog = new DialogDepositAdd(_objNew, DepositAction.AddDeposit, GeneralProcess.Insert, objDeposit);
            }

            dialog.ShowDialog();
            if (dialog.DialogResult == DialogResult.OK)
            {
                bindDeposit();
            }
        }

        private void linkAdjustDeposit_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (Login.CurrentCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_RECEIVE_PAYMENT, string.Empty) == DialogResult.Yes)
                    new DialogOpenCashDrawer().ShowDialog();
                if (Login.CurrentCashDrawer == null)
                    return;
            }
            TBL_CUS_DEPOSIT objDeposit = DBDataContext.Db.TBL_CUS_DEPOSITs.FirstOrDefault(row => !row.IS_PAID && row.CUSTOMER_ID == _objNew.CUSTOMER_ID);
            if (objDeposit != null)
            {
                MsgBox.ShowInformation(Resources.MS_CUSTOMER_NO_DEPOSIT);
                return;
            }
            DialogDepositAdd dialog = new DialogDepositAdd(_objNew, DepositAction.AdjustDeposit, GeneralProcess.Insert, null);
            dialog.ShowDialog();
            if (dialog.DialogResult == DialogResult.OK)
            {
                bindDeposit();
            }
        }

        private void linkApplyPayment_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (Login.CurrentCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_RECEIVE_PAYMENT, string.Empty) == DialogResult.Yes)
                    new DialogOpenCashDrawer().ShowDialog();
                if (Login.CurrentCashDrawer == null)
                    return;
            }
            TBL_CUS_DEPOSIT objDeposit = DBDataContext.Db.TBL_CUS_DEPOSITs.FirstOrDefault(row => !row.IS_PAID && row.CUSTOMER_ID == _objNew.CUSTOMER_ID);
            if (objDeposit != null)
            {
                MsgBox.ShowInformation(Resources.MS_CUSTOMER_NO_DEPOSIT);
                return;
            }
            DialogDepositApplyPayment dialog = new DialogDepositApplyPayment(_objNew);
            dialog.ShowDialog();
            if (dialog.DialogResult == DialogResult.OK)
            {
                bindDeposit();
            }
        }

        private void linkRefundDeposit_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (Login.CurrentCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_RECEIVE_PAYMENT, string.Empty) == DialogResult.Yes)
                    new DialogOpenCashDrawer().ShowDialog();
                if (Login.CurrentCashDrawer == null)
                    return;
            }
            TBL_CUS_DEPOSIT objDeposit = DBDataContext.Db.TBL_CUS_DEPOSITs.FirstOrDefault(row => !row.IS_PAID && row.CUSTOMER_ID == _objNew.CUSTOMER_ID);
            if (objDeposit != null)
            {
                MsgBox.ShowInformation(Resources.MS_CUSTOMER_NO_DEPOSIT);
                return;
            }
            DialogDepositAdd dialog = new DialogDepositAdd(_objNew, DepositAction.RefundDeposit, GeneralProcess.Insert, null);
            dialog.ShowDialog();
            if (dialog.DialogResult == DialogResult.OK)
            {
                bindDeposit();
            }
        }

        private void btnPrintReceipt_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (dgvDeposit.SelectedRows.Count > 0)
            {
                int intCusDeposit = (int)dgvDeposit.SelectedRows[0].Cells[CUS_DEPOSIT_ID.Name].Value;
                TBL_CUS_DEPOSIT objDeposit = DBDataContext.Db.TBL_CUS_DEPOSITs.FirstOrDefault(row => row.CUS_DEPOSIT_ID == intCusDeposit);
                CrystalReportHelper ch;
                if (objDeposit.IS_PAID)
                {
                    ch = new CrystalReportHelper("ReportDepositReceipt.rpt");
                }
                else
                {
                    ch = new CrystalReportHelper("ReportDepositInvoice.rpt");
                }
                ch.SetParameter("CUS_DEPOSIT_ID", intCusDeposit);
                ch.ViewReport("");
            }
        }

        private void bindDeposit()
        {
            var lstDeposit = from de in DBDataContext.Db.TBL_CUS_DEPOSITs
                             join ac in DBDataContext.Db.TLKP_DEPOSIT_ACTIONs on de.DEPOSIT_ACTION_ID equals ac.DEPOSIT_ACTION_ID
                             join c in DBDataContext.Db.TLKP_CURRENCies on de.CURRENCY_ID equals c.CURRENCY_ID
                             where de.CUSTOMER_ID == _objNew.CUSTOMER_ID
                             orderby de.CURRENCY_ID, de.CUS_DEPOSIT_ID
                             select new
                             {
                                 de.CUS_DEPOSIT_ID,
                                 de.DEPOSIT_NO,
                                 de.CREATE_ON,
                                 de.CREATE_BY,
                                 ac.DEPOSIT_ACTION_NAME_KH,
                                 de.AMOUNT,
                                 de.BALANCE,
                                 c.CURRENCY_SING,
                                 de.IS_PAID
                             };
            dgvDeposit.DataSource = lstDeposit;

            var dBalance = from de in DBDataContext.Db.TBL_CUS_DEPOSITs
                           join c in DBDataContext.Db.TLKP_CURRENCies on de.CURRENCY_ID equals c.CURRENCY_ID
                           where de.CUSTOMER_ID == _objNew.CUSTOMER_ID && de.IS_PAID
                           group de by new { c.CURRENCY_ID, c.CURRENCY_SING } into tmpBalance
                           select new
                           {
                               tmpBalance.Key.CURRENCY_ID,
                               tmpBalance.Key.CURRENCY_SING,
                               tmpBalance.OrderByDescending(x => x.CUS_DEPOSIT_ID).FirstOrDefault().BALANCE
                           };
            dgvBalanceDeposit.DataSource = dBalance;

            if (dBalance.Count() > 0)
            {
                btnAPPLY_PAYMENT.Enabled = true;
                btnREFUND_DEPOSIT.Enabled = true;
                btnADJUST_DEPOSIT.Enabled = true;
            }
            else
            {
                btnAPPLY_PAYMENT.Enabled = false;
                btnREFUND_DEPOSIT.Enabled = false;
                btnADJUST_DEPOSIT.Enabled = false;
            }
        }

        private void btnConnection_Click(object sender, EventArgs e)
        {
            CrystalReportHelper ch = new CrystalReportHelper("ReportCustomerConnection.rpt", "EQUIPMENT");
            ch.SetParameter("@CUSTOMER_ID", _objNew.CUSTOMER_ID);
            ch.ViewReport("");
        }

        private void btnAgreement_Click(object sender, EventArgs e)
        {
            CrystalReportHelper ch = new CrystalReportHelper("ReportCustomerAgreement.rpt");
            ch.SetParameter("@CUSTOMER_ID", _objNew.CUSTOMER_ID);
            ch.ViewReport("");
        }


        private void dtpUsage_ValueChanged(object sender, EventArgs e)
        {
            loadPrepaidUsage();
        }

        private void btnReactivate_Click(object sender, EventArgs e)
        {
            var diag = new DialogCustomerActivatePrepaid(_objNew);
            if (diag.ShowDialog() == DialogResult.OK)
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void btnPrepaidAdjustment_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //if user not yet open cash drawer
            //not allow to adjust invoice
            if (Login.CurrentCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_ADJUST_INVOICE, string.Empty) == DialogResult.Yes)
                    new DialogOpenCashDrawer().ShowDialog();
                if (Login.CurrentCashDrawer == null)
                    return;
            }

            if (dgvPrepaidUsage.SelectedRows.Count == 0)
            {
                return;
            }

            var objPrepaidCredit =
                DBDataContext.Db.TBL_PREPAID_CREDITs.FirstOrDefault(
                    x =>
                        x.PREPAID_CREDIT_ID == (int)dgvPrepaidUsage.SelectedRows[0].Cells[PREPAID_CREDIT_ID.Name].Value);
            if (objPrepaidCredit.PAID_AMOUNT > 0)
            {
                MsgBox.ShowInformation(Resources.MS_CANNOT_ADJUST_PAID_INVOICE);
                return;
            }

            var enableAdjust = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_ADJUST_OLD_INVOICE]);
            if (!enableAdjust)
            {
                var run = (from r in DBDataContext.Db.TBL_RUN_BILLs
                           join pr in DBDataContext.Db.TBL_RUN_PREPAID_CREDITs on r.RUN_ID equals pr.RUN_ID
                           where pr.RUN_PREPAID_CREDIT_ID == objPrepaidCredit.RUN_PREPAID_CREDIT_ID
                           select new { TBL_RUN_BILL = r, TBL_RUN_PREPAID_CREDIT = pr }).FirstOrDefault();
                var datNextBillingMonth = Method.GetNextBillingMonth(run.TBL_RUN_BILL.CYCLE_ID);

                if (datNextBillingMonth.AddMonths(-1).Date != objPrepaidCredit.CREDIT_MONTH.Date)
                {
                    MsgBox.ShowInformation(Resources.MS_INVALID_ADJUST_INVOICE);
                    return;
                }
            }

            var dig = new DialogPrepaidCreditAdjustment(_objNew, objPrepaidCredit, _objOld.PRICE_ID);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                loadPrepaidUsage();
                UIHelper.SelectRow(dgvPrepaidUsage, "PREPAID_CREDIT_ID", objPrepaidCredit.PREPAID_CREDIT_ID);
            }
        }

        private void cboCustomerGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboCustomerGroup.SelectedIndex != -1 && !_loading)
            {
                int groupId = (int)cboCustomerGroup.SelectedValue;
                UIHelper.SetDataSourceToComboBox(cboConnectionType,
                    DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs.OrderBy(x => x.DESCRIPTION).Where(x => x.NONLICENSE_CUSTOMER_GROUP_ID == groupId));

                if (groupId == 5)
                {
                    this.cboConnectionType.DropDownWidth = 300;
                }
                else
                {
                    this.cboConnectionType.DropDownWidth = 180;
                }
            }
        }

        private void ChangeConnectionType_Click(object sender, EventArgs e)
        {
            try
            {
                var invId = (int)dgvPrepaidUsage.CurrentRow.Cells[PREPAID_CREDIT_ID.Name].Value;
                new DialogChangeConnectionType(invId).ShowDialog();
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void dgvPrepaidUsage_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }

            if (e.Button == MouseButtons.Right)
            {
                //dgvInvoiceList.CurrentCell = dgvInvoiceList.Rows[e.RowIndex].Cells[1];
                cmsStrip.Show();
                cmsStrip.Left = Cursor.Position.X;
                cmsStrip.Top = Cursor.Position.Y;
            }
        }

        private void dtpYear_ValueChanged(object sender, EventArgs e)
        {
            loadPrepaidUsage();
        }
    }
}

