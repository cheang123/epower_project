﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface.PrePaid
{
    public partial class PageReportMonthToDateSaleSummary : UserControl
    {
        private CrystalReportHelper ch;
        public PageReportMonthToDateSaleSummary()
        {
            InitializeComponent();
            viewer.DefaultView();
            dtpEnd.Value = DBDataContext.Db.GetSystemDate();
            dtpDate.Value = new DateTime(dtpEnd.Value.Year, dtpEnd.Value.Month, 1);
            dataLookup();
        }

        private void dataLookup()
        {
            DataTable type = DBDataContext.Db.TBL_CUSTOMER_TYPEs.Where(row => row.IS_ACTIVE || row.CUSTOMER_TYPE_ID == -1)._ToDataTable();
            DataRow newRow = type.NewRow();
            newRow[0] = 0;
            newRow[1] = Resources.ALL_TYPE;
            type.Rows.InsertAt(newRow, 0);
            UIHelper.SetDataSourceToComboBox(cboCustomerType, type);
             

            DataTable area = DBDataContext.Db.TBL_AREAs.Where(row => row.IS_ACTIVE)._ToDataTable();
            DataRow newArea = area.NewRow();
            newArea[0] = 0;
            newArea[1] = Resources.ALL_AREA;
            area.Rows.InsertAt(newArea, 0);
            UIHelper.SetDataSourceToComboBox(cboArea, area); 
        }
        private void btnViewReport_Click(object sender, EventArgs e)
        {
            Runner.Run(viewReport);
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            sendMail();
        }

        private void viewReport()
        {
            if (invalid())
            {
                return;
            }
            var dtStart = new DateTime();
            
            dtStart = dtpDate.Checked ? dtpDate.Value.Date : UIHelper._DefaultDate.Date;
            ch = ViewReportPrepaidMonthToDateSaleSummary(dtStart, dtpEnd.Value.Date, (int)cboCustomerType.SelectedValue
                    , (int)cboArea.SelectedValue, cboCustomerType.Text, cboArea.Text);             
            viewer.ReportSource = ch.Report;
        }

        public CrystalReportHelper ViewReportPrepaidMonthToDateSaleSummary(DateTime dtStart,DateTime dtEnd,int intCusType,int intArea,string strCusType,string strArea)
        {
            string title = string.Empty;
            CrystalReportHelper c = new CrystalReportHelper("ReportPrepaidMonthToDateSaleSummary.rpt");

            if (dtStart.Date==UIHelper._DefaultDate)
            {
                title = string.Format(Resources.TO, dtpEnd.Value.Date.ToString("dd-MM-yyyy"));
                c.SetParameter("@START_DATE", UIHelper._DefaultDate);
            }
            else
            {
                title = string.Format(Resources.FROM_TO, dtpDate.Value.Date.ToString("dd-MM-yyyy"), dtpEnd.Value.Date.ToString("dd-MM-yyyy"));
                c.SetParameter("@START_DATE", dtpDate.Value.Date);
            }            
            c.SetParameter("@END_DATE", dtEnd);
            c.SetParameter("@CUSTOMER_TYPE_ID",intCusType);
            c.SetParameter("@AREA_ID", intArea);
            c.SetParameter("@CUSTOMER_TYPE", strCusType);
            c.SetParameter("@AREA", strArea);
            c.SetParameter("@TITLE", title);

            return c;
        }

        private bool invalid()
        {
            bool blnval = false;
            if (cboArea.SelectedIndex == -1)
            {
                cboArea.SetValidation("");
                blnval = true;
            }
            if (cboCustomerType.SelectedIndex == -1)
            {
                cboCustomerType.SetValidation("");
                blnval = true;
            }
            return blnval;
        }
        private void sendMail()
        {
            try
            {
                viewReport();
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + ch.ReportName + ".xls").FullName;
                ch.ExportToExcel(exportPath);
                DialogSendMail.Instance.Add(ch.ReportName, exportPath);
                DialogSendMail.Instance.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }
    }
}
