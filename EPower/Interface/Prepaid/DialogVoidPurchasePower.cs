﻿using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using EPower.Logic;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;


namespace EPower.Interface.PrePaid
{
    public partial class DialogVoidBuyPower : ExDialog
    {
        #region Private Data
        TBL_CUSTOMER _objCusReadCard;
        TBL_CUSTOMER_METER _objCustomerMeter;
        TBL_METER _objMeter;
        TBL_PREPAID_CUSTOMER _objPrePaidCustomer;
        TBL_PREPAID_CUSTOMER _objOldPrePaidCustomer = new TBL_PREPAID_CUSTOMER();
        TBL_CUSTOMER_BUY _objLastBuy;
        TBL_CUSTOMER_BUY _objOldLastBuy = new TBL_CUSTOMER_BUY();

        TBL_AREA _objArea;
        TBL_POLE _objPole;
        TBL_BOX _objBox;
        TBL_PRICE _objPrice;
        TBL_PRICE_DETAIL _objPriceDetail;

        PowerCard _objReadCard;
        private TLKP_CURRENCY _objKhCurrency;
        #endregion


        public DialogVoidBuyPower()
        {
            InitializeComponent();
            _objKhCurrency = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.CURRENCY_ID == 1);
            ResetObject();
            read();                   
        }

        

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
 

        private void txtEnterEnglish(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void txtKeyNumber(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputNumberOnly(sender, e);
        }

        private void btnReadCard_Click(object sender, EventArgs e)
        {
            try
            {
                ReadCardInfo();
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp);
            }
        }



        #region Method
        private void read()
        {
            txtCashDrawerName.Text = DBDataContext.Db.TBL_CASH_DRAWERs.FirstOrDefault(x => x.CASH_DRAWER_ID == Login.CurrentCashDrawer.CASH_DRAWER_ID).CASH_DRAWER_NAME;
            txtUserCashDrawerName.Text = Login.CurrentLogin.LOGIN_NAME;
        }         

      

        private ICCard ReadCardInfo()
        {
            ResetObject();

            ICCard objReadCard = ICCard.Read();            
            switch ((CardType)objReadCard.ICCardType)
            {
                case CardType.NoCard:
                    lblCARD_TYPE_.Text=Resources.MS_PLEASE_INSERT_NO_POWER_CARD_TO_DEVICE;
                    break;
                case CardType.BlankCard:
                case CardType.FormattedCard:
                    lblCARD_TYPE_.Text = string.Format("{0}, {1}", Resources.NEW_CARD,Resources.MS_PLEASE_INSERT_NO_POWER_CARD_TO_DEVICE);
                    break;
                case CardType.HavePowerCard:
                    lblCARD_TYPE_.Text = Resources.IC_CARD_HAVE_POWER;
                    _objReadCard = (PowerCard)objReadCard;
                    bind();
                    break;
                case CardType.NoPowerCard:
                    lblCARD_TYPE_.Text = Resources.MS_CUSTOMER_ALREADY_INSERT_POWER_CARD_TO_METER;
                    _objReadCard = (PowerCard)objReadCard;                    
                    bind();
                    break;
                case CardType.ResetCard:
                case CardType.PresetCard:
                case CardType.TestCard:
                case CardType.CutCard:
                    lblCARD_TYPE_.Text = Resources.MS_CARD_CANNOT_USE;
                    break;
                default:
                    break;
            }

            if ((CardType)objReadCard.ICCardType!= CardType.HavePowerCard)
            {
                txtVoidCompensated.Text =
                txtVoidDiscount.Text =
                txtVoidPurchasePower.Text =
                txtVoidTotalAmount.Text = string.Empty;
            }

            return objReadCard;
        }

        private void bind()
        {

            var objReadInfo = (from c in DBDataContext.Db.TBL_CUSTOMERs
                               join pc in DBDataContext.Db.TBL_PREPAID_CUSTOMERs on c.CUSTOMER_ID equals pc.CUSTOMER_ID
                               join cb in DBDataContext.Db.TBL_CUSTOMER_BUYs on pc.PREPAID_CUS_ID equals cb.PREPAID_CUS_ID
                               where Prepaid.GetAreaCode() == _objReadCard.AreaCode && cb.IS_VOID == false
                               && pc.CARD_CODE.ToLower()== _objReadCard.CustomerCode.ToLower()
                               orderby cb.CREATE_ON descending
                               select new
                               {
                                   CUSTOMER = c,
                                   PREPAID_CUSTOMER = pc,
                                   CUSTOMER_LAST_BUY = cb??new TBL_CUSTOMER_BUY() { CREATE_ON= UIHelper._DefaultDate}
                               }).FirstOrDefault();
            if (objReadInfo == null)
            {
                if (Prepaid.GetAreaCode()!=_objReadCard.AreaCode)
                {
                    lblCARD_TYPE_.Text = "អតិថិជនម្នាក់នេះមិនអាចប្រើប្រាស់ក្នុងតំបន់ចែកចាយអគ្គិសនីនេះទេ!";
                }

                return;
            }
            _objCusReadCard = objReadInfo.CUSTOMER;
            _objPrePaidCustomer = objReadInfo.PREPAID_CUSTOMER;
            _objLastBuy = objReadInfo.CUSTOMER_LAST_BUY;

            txtCustomerCode.Text = _objCusReadCard.CUSTOMER_CODE;
            txtFullName.Text = string.Concat(_objCusReadCard.LAST_NAME_KH, " ", _objCusReadCard.FIRST_NAME_KH);

            //used info
            txtTotalBuyPower.Text = _objPrePaidCustomer.TOTAL_BUY_POWER.ToString("N0");            
            txtLastPurchasePower.Text = _objLastBuy!=null?_objLastBuy.BUY_QTY.ToString("N0"):string.Empty;
            dtpLastPurchaseDate.Value = _objLastBuy!=null&&_objLastBuy.CREATE_ON>dtpLastPurchaseDate.MinDate?_objLastBuy.CREATE_ON:_objCusReadCard.ACTIVATE_DATE;
            txtPurchaseTimes.Text = _objLastBuy!=null?_objLastBuy.BUY_TIMES.ToString():"0";

            //void purchase
            txtVoidPurchasePower.Text = _objLastBuy!=null?_objLastBuy.BUY_QTY.ToString("N0"):string.Empty;
            txtVoidDiscount.Text = _objLastBuy != null ? _objLastBuy.DISCOUNT_QTY.ToString("N0") : string.Empty;
            txtVoidCompensated.Text = _objLastBuy != null ? _objLastBuy.COMPENSATED.ToString("N0") : string.Empty;
            txtVoidTotalAmount.Text = UIHelper.FormatCurrency(_objLastBuy!=null?_objLastBuy.BUY_AMOUNT:0);
            txtCredit.Text = UIHelper.FormatCurrency(_objLastBuy != null ? _objLastBuy.PAID_CREDIT_AMOUNT : 0,_objKhCurrency.CURRENCY_ID);
            readCustomerMeter();            
        }

        private void readCustomerMeter()
        {
            _objCustomerMeter = DBDataContext.Db.TBL_CUSTOMER_METERs.FirstOrDefault(r => r.CUSTOMER_ID == _objCusReadCard.CUSTOMER_ID && r.IS_ACTIVE);
            if (_objCustomerMeter != null)
            {
                _objMeter = DBDataContext.Db.TBL_METERs.FirstOrDefault(m => m.METER_ID == _objCustomerMeter.METER_ID);
                txtMeterCode.Text = _objMeter.METER_CODE;

                _objArea = DBDataContext.Db.TBL_AREAs.FirstOrDefault(a => a.AREA_ID == _objCusReadCard.AREA_ID);
                txtAreaCode.Text = _objArea.AREA_CODE;

                _objPole = DBDataContext.Db.TBL_POLEs.FirstOrDefault(p => p.POLE_ID == _objCustomerMeter.POLE_ID);
                txtPoleCode.Text = _objPole.POLE_CODE;

                _objBox = DBDataContext.Db.TBL_BOXes.FirstOrDefault(b => b.BOX_ID == _objCustomerMeter.BOX_ID);
                txtBoxCode.Text = _objBox.BOX_CODE;

                _objPrice = DBDataContext.Db.TBL_PRICEs.FirstOrDefault(x => x.PRICE_ID == _objCusReadCard.PRICE_ID);
                txtPriceType.Text = _objPrice.PRICE_NAME;
                txtSignUnitPrice.Text = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.CURRENCY_ID == _objPrice.CURRENCY_ID).CURRENCY_SING;

                _objPriceDetail = DBDataContext.Db.TBL_PRICE_DETAILs.FirstOrDefault(x => x.PRICE_ID == _objPrice.PRICE_ID);
                txtPrice.Text =  _objLastBuy!=null?_objLastBuy.PRICE.ToString("N0"):string.Empty;
            }
        }

        private void ResetObject()
        {
            //Release object
            _objCusReadCard = null;
            _objCustomerMeter = null;
            _objPrePaidCustomer = null;
            _objLastBuy = null;
            _objMeter = null;
            _objArea = null;
            _objPole = null;
            _objBox = null;
            _objPrice = null;
            _objPriceDetail = null;
            _objReadCard = null;
            //Reset general info
            txtCustomerCode.Text =
            txtFullName.Text =
            txtMeterCode.Text =
            
            txtAreaCode.Text =
            txtPoleCode.Text =
            txtBoxCode.Text =
            //Reset last purchase
            txtPriceType.Text =
            txtTotalBuyPower.Text =
            txtVoidCompensated.Text =

            txtLastPurchasePower.Text =string.Empty;
            dtpLastPurchaseDate.Value = UIHelper._DefaultDate;
            txtPurchaseTimes.Text =
                //Reset Purchase power
            txtVoidPurchasePower.Text =
            txtPrice.Text =
            txtVoidDiscount.Text =
            txtVoidTotalAmount.Text =
            txtCredit.Text =
            lblCARD_TYPE_.Text= string.Empty;

            
        }

        private bool Invalid()
        {             
            ICCard objReadCard = ReadCardInfo();

            if ((CardType)objReadCard.ICCardType!= CardType.HavePowerCard)
            {
                MsgBox.ShowInformation(Resources.MS_PLEASE_INSERT_NO_POWER_CARD_TO_DEVICE);
                return true;
            }            
            return false;            
        }
        #endregion

        

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (Invalid())
            {
                return;                
            }
            bool blnSaleSuccess = false;
            Runner.RunNewThread(() =>
            {
                try
                {
                    DateTime dtNow = DBDataContext.Db.GetSystemDate();
                    var exchangeRate = new ExchangeRateLogic().findLastExchangeRate(DBDataContext.Db.GetSystemDate(), _objPrice.CURRENCY_ID);
                    using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                    {
                        _objPrePaidCustomer._CopyTo(_objOldPrePaidCustomer);

                        TBL_CUSTOMER_BUY objVoidCusBuy = new TBL_CUSTOMER_BUY();

                        _objLastBuy._CopyTo(objVoidCusBuy);
                        _objLastBuy._CopyTo(_objOldLastBuy);
                        objVoidCusBuy.BUY_NO = Method.GetNextSequence(Sequence.Reverse, true);
                        objVoidCusBuy.BUY_QTY = -objVoidCusBuy.BUY_QTY;
                        objVoidCusBuy.COMPENSATED = -objVoidCusBuy.COMPENSATED;
                        objVoidCusBuy.DISCOUNT_QTY = -objVoidCusBuy.DISCOUNT_QTY;
                        objVoidCusBuy.BUY_AMOUNT = -objVoidCusBuy.BUY_AMOUNT;
                        objVoidCusBuy.PAID_CREDIT_AMOUNT = -objVoidCusBuy.PAID_CREDIT_AMOUNT;
                        objVoidCusBuy.IS_VOID = true;
                        objVoidCusBuy.CUSTOMER_BUY_ID = 0;
                        objVoidCusBuy.CREATE_ON = dtNow;
                        objVoidCusBuy.CREATE_BY = Login.CurrentLogin.LOGIN_NAME;
                        objVoidCusBuy.USER_CASH_DRAWER_ID = Login.CurrentCashDrawer.USER_CASH_DRAWER_ID;
                        objVoidCusBuy.PARENT_ID = _objLastBuy.CUSTOMER_BUY_ID;

                        _objLastBuy.IS_VOID = true;

                        _objPrePaidCustomer.BUY_TIMES = _objPrePaidCustomer.BUY_TIMES - 1;
                        _objPrePaidCustomer.TOTAL_BUY_POWER -= _objLastBuy.BUY_QTY;
                        _objPrePaidCustomer.COMPENSATED += objVoidCusBuy.COMPENSATED;
                        objVoidCusBuy.EXCHANGE_RATE = exchangeRate.EXCHANGE_RATE;
                        objVoidCusBuy.EXCHANGE_RATE_DATE = exchangeRate.CREATE_ON;

                        TBL_CHANGE_LOG objChangelog = DBDataContext.Db.Update(_objOldPrePaidCustomer, _objPrePaidCustomer);
                        DBDataContext.Db.InsertChild(objVoidCusBuy, _objPrePaidCustomer, ref objChangelog);
                        DBDataContext.Db.UpdateChild(_objOldLastBuy, _objLastBuy, _objPrePaidCustomer, ref objChangelog);

                        foreach (var oldItem in DBDataContext.Db.TBL_PREPAID_CREDIT_PAID_AMOUNTs
                            .Where(x => x.CUSTOMER_BUY_ID == _objLastBuy.CUSTOMER_BUY_ID && x.IS_ACTIVE))
                        {
                            var newItem = new TBL_PREPAID_CREDIT_PAID_AMOUNT();
                            newItem.CUSTOMER_BUY_ID = (int)objVoidCusBuy.CUSTOMER_BUY_ID;
                            newItem.PREPAID_CREDIT_ID = oldItem.PREPAID_CREDIT_ID;
                            newItem.PAID_AMOUNT = -oldItem.PAID_AMOUNT;
                            newItem.IS_ACTIVE = true;

                            var objoldCredit =
                                DBDataContext.Db.TBL_PREPAID_CREDITs.FirstOrDefault(
                                    x => x.PREPAID_CREDIT_ID == oldItem.PREPAID_CREDIT_ID);
                            var objNewCredit = new TBL_PREPAID_CREDIT();
                            objoldCredit._CopyTo(objNewCredit);

                            objNewCredit.PAID_AMOUNT += newItem.PAID_AMOUNT;
                            DBDataContext.Db.InsertChild(newItem, _objPrePaidCustomer, ref objChangelog);
                            DBDataContext.Db.UpdateChild(objoldCredit, objNewCredit, _objPrePaidCustomer, ref objChangelog);
                        }

                        //void card 
                        string strReturn = string.Empty;
                        int intReturn = 1;
                        intReturn = _objReadCard.VoidLastPurchasing(out strReturn);

                        if (intReturn != 0)
                        {
                            return;
                        }
                        tran.Complete();
                        blnSaleSuccess = true;
                    }
                }
                catch (Exception exp)
                {
                    throw exp;
                }
            });
            if (blnSaleSuccess)
            {
                ResetObject();
                btnREAD_CARD.Focus();
            }

        }               
    }   
}