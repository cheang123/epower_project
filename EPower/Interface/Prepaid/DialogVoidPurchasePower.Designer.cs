﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.PrePaid
{
    partial class DialogVoidBuyPower
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogVoidBuyPower));
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblGENERAL_INFORMATION = new System.Windows.Forms.Label();
            this.txtPoleCode = new System.Windows.Forms.TextBox();
            this.txtMeterCode = new System.Windows.Forms.TextBox();
            this.lblBOX = new System.Windows.Forms.Label();
            this.lblMETER_CODE = new System.Windows.Forms.Label();
            this.txtBoxCode = new System.Windows.Forms.TextBox();
            this.lblPOLE = new System.Windows.Forms.Label();
            this.lblAREA = new System.Windows.Forms.Label();
            this.lblCUSTOMER_NAME = new System.Windows.Forms.Label();
            this.txtAreaCode = new System.Windows.Forms.TextBox();
            this.txtFullName = new System.Windows.Forms.TextBox();
            this.txtCustomerCode = new System.Windows.Forms.TextBox();
            this.lblCUSTOMER = new System.Windows.Forms.Label();
            this.lblUSAGE_INFORMATION = new System.Windows.Forms.Label();
            this.txtTotalBuyPower = new System.Windows.Forms.TextBox();
            this.lblTOTAL_BUY_POWER = new System.Windows.Forms.Label();
            this.lblPOWER_COMPENSATED = new System.Windows.Forms.Label();
            this.txtVoidCompensated = new System.Windows.Forms.TextBox();
            this.txtPriceType = new System.Windows.Forms.TextBox();
            this.lblPRICE_TYPE = new System.Windows.Forms.Label();
            this.lblLAST_BUY_POWER = new System.Windows.Forms.Label();
            this.lblLAST_BUY_DATE = new System.Windows.Forms.Label();
            this.lblBUY_TIMES = new System.Windows.Forms.Label();
            this.txtPurchaseTimes = new System.Windows.Forms.TextBox();
            this.txtLastPurchasePower = new System.Windows.Forms.TextBox();
            this.lblINVOICE_DATE = new System.Windows.Forms.Label();
            this.lblCASH_DRAWER = new System.Windows.Forms.Label();
            this.lblCASHIER = new System.Windows.Forms.Label();
            this.txtCashDrawerName = new System.Windows.Forms.TextBox();
            this.txtUserCashDrawerName = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblCARD_TYPE_ = new System.Windows.Forms.Label();
            this.btnREAD_CARD = new SoftTech.Component.ExButton();
            this.dtpPurchaseDate = new System.Windows.Forms.DateTimePicker();
            this.dtpLastPurchaseDate = new System.Windows.Forms.DateTimePicker();
            this.lblREAD_AMOUNT_ = new System.Windows.Forms.Label();
            this.lblBUY_POWER = new System.Windows.Forms.Label();
            this.lblPRICE = new System.Windows.Forms.Label();
            this.txtVoidPurchasePower = new System.Windows.Forms.TextBox();
            this.txtVoidDiscount = new System.Windows.Forms.TextBox();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.lblDISCOUNT = new System.Windows.Forms.Label();
            this.lblTOTAL_AMOUNT = new System.Windows.Forms.Label();
            this.txtVoidTotalAmount = new System.Windows.Forms.TextBox();
            this.lblVOID_POWER_INFORMATION = new System.Windows.Forms.Label();
            this.txtSignUnitPrice = new System.Windows.Forms.TextBox();
            this.txtCredit = new System.Windows.Forms.TextBox();
            this.lblAMOUNT_SUBSIDY = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.txtCredit);
            this.content.Controls.Add(this.lblAMOUNT_SUBSIDY);
            this.content.Controls.Add(this.txtSignUnitPrice);
            this.content.Controls.Add(this.lblVOID_POWER_INFORMATION);
            this.content.Controls.Add(this.lblREAD_AMOUNT_);
            this.content.Controls.Add(this.dtpLastPurchaseDate);
            this.content.Controls.Add(this.dtpPurchaseDate);
            this.content.Controls.Add(this.panel2);
            this.content.Controls.Add(this.txtCashDrawerName);
            this.content.Controls.Add(this.txtUserCashDrawerName);
            this.content.Controls.Add(this.lblCASHIER);
            this.content.Controls.Add(this.lblCASH_DRAWER);
            this.content.Controls.Add(this.txtVoidTotalAmount);
            this.content.Controls.Add(this.lblTOTAL_AMOUNT);
            this.content.Controls.Add(this.lblDISCOUNT);
            this.content.Controls.Add(this.txtPrice);
            this.content.Controls.Add(this.txtVoidDiscount);
            this.content.Controls.Add(this.txtVoidPurchasePower);
            this.content.Controls.Add(this.lblPRICE);
            this.content.Controls.Add(this.lblBUY_POWER);
            this.content.Controls.Add(this.lblINVOICE_DATE);
            this.content.Controls.Add(this.txtPurchaseTimes);
            this.content.Controls.Add(this.txtLastPurchasePower);
            this.content.Controls.Add(this.lblBUY_TIMES);
            this.content.Controls.Add(this.lblLAST_BUY_DATE);
            this.content.Controls.Add(this.lblLAST_BUY_POWER);
            this.content.Controls.Add(this.txtTotalBuyPower);
            this.content.Controls.Add(this.lblTOTAL_BUY_POWER);
            this.content.Controls.Add(this.lblPOWER_COMPENSATED);
            this.content.Controls.Add(this.txtVoidCompensated);
            this.content.Controls.Add(this.txtPriceType);
            this.content.Controls.Add(this.lblPRICE_TYPE);
            this.content.Controls.Add(this.lblUSAGE_INFORMATION);
            this.content.Controls.Add(this.txtPoleCode);
            this.content.Controls.Add(this.txtMeterCode);
            this.content.Controls.Add(this.lblBOX);
            this.content.Controls.Add(this.lblMETER_CODE);
            this.content.Controls.Add(this.txtBoxCode);
            this.content.Controls.Add(this.lblPOLE);
            this.content.Controls.Add(this.lblAREA);
            this.content.Controls.Add(this.lblCUSTOMER_NAME);
            this.content.Controls.Add(this.txtAreaCode);
            this.content.Controls.Add(this.txtFullName);
            this.content.Controls.Add(this.txtCustomerCode);
            this.content.Controls.Add(this.lblCUSTOMER);
            this.content.Controls.Add(this.lblGENERAL_INFORMATION);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.panel1);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.lblGENERAL_INFORMATION, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerCode, 0);
            this.content.Controls.SetChildIndex(this.txtFullName, 0);
            this.content.Controls.SetChildIndex(this.txtAreaCode, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblAREA, 0);
            this.content.Controls.SetChildIndex(this.lblPOLE, 0);
            this.content.Controls.SetChildIndex(this.txtBoxCode, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_CODE, 0);
            this.content.Controls.SetChildIndex(this.lblBOX, 0);
            this.content.Controls.SetChildIndex(this.txtMeterCode, 0);
            this.content.Controls.SetChildIndex(this.txtPoleCode, 0);
            this.content.Controls.SetChildIndex(this.lblUSAGE_INFORMATION, 0);
            this.content.Controls.SetChildIndex(this.lblPRICE_TYPE, 0);
            this.content.Controls.SetChildIndex(this.txtPriceType, 0);
            this.content.Controls.SetChildIndex(this.txtVoidCompensated, 0);
            this.content.Controls.SetChildIndex(this.lblPOWER_COMPENSATED, 0);
            this.content.Controls.SetChildIndex(this.lblTOTAL_BUY_POWER, 0);
            this.content.Controls.SetChildIndex(this.txtTotalBuyPower, 0);
            this.content.Controls.SetChildIndex(this.lblLAST_BUY_POWER, 0);
            this.content.Controls.SetChildIndex(this.lblLAST_BUY_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblBUY_TIMES, 0);
            this.content.Controls.SetChildIndex(this.txtLastPurchasePower, 0);
            this.content.Controls.SetChildIndex(this.txtPurchaseTimes, 0);
            this.content.Controls.SetChildIndex(this.lblINVOICE_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblBUY_POWER, 0);
            this.content.Controls.SetChildIndex(this.lblPRICE, 0);
            this.content.Controls.SetChildIndex(this.txtVoidPurchasePower, 0);
            this.content.Controls.SetChildIndex(this.txtVoidDiscount, 0);
            this.content.Controls.SetChildIndex(this.txtPrice, 0);
            this.content.Controls.SetChildIndex(this.lblDISCOUNT, 0);
            this.content.Controls.SetChildIndex(this.lblTOTAL_AMOUNT, 0);
            this.content.Controls.SetChildIndex(this.txtVoidTotalAmount, 0);
            this.content.Controls.SetChildIndex(this.lblCASH_DRAWER, 0);
            this.content.Controls.SetChildIndex(this.lblCASHIER, 0);
            this.content.Controls.SetChildIndex(this.txtUserCashDrawerName, 0);
            this.content.Controls.SetChildIndex(this.txtCashDrawerName, 0);
            this.content.Controls.SetChildIndex(this.panel2, 0);
            this.content.Controls.SetChildIndex(this.dtpPurchaseDate, 0);
            this.content.Controls.SetChildIndex(this.dtpLastPurchaseDate, 0);
            this.content.Controls.SetChildIndex(this.lblREAD_AMOUNT_, 0);
            this.content.Controls.SetChildIndex(this.lblVOID_POWER_INFORMATION, 0);
            this.content.Controls.SetChildIndex(this.txtSignUnitPrice, 0);
            this.content.Controls.SetChildIndex(this.lblAMOUNT_SUBSIDY, 0);
            this.content.Controls.SetChildIndex(this.txtCredit, 0);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // lblGENERAL_INFORMATION
            // 
            resources.ApplyResources(this.lblGENERAL_INFORMATION, "lblGENERAL_INFORMATION");
            this.lblGENERAL_INFORMATION.Name = "lblGENERAL_INFORMATION";
            // 
            // txtPoleCode
            // 
            resources.ApplyResources(this.txtPoleCode, "txtPoleCode");
            this.txtPoleCode.Name = "txtPoleCode";
            this.txtPoleCode.ReadOnly = true;
            // 
            // txtMeterCode
            // 
            resources.ApplyResources(this.txtMeterCode, "txtMeterCode");
            this.txtMeterCode.Name = "txtMeterCode";
            this.txtMeterCode.ReadOnly = true;
            // 
            // lblBOX
            // 
            resources.ApplyResources(this.lblBOX, "lblBOX");
            this.lblBOX.Name = "lblBOX";
            // 
            // lblMETER_CODE
            // 
            resources.ApplyResources(this.lblMETER_CODE, "lblMETER_CODE");
            this.lblMETER_CODE.Name = "lblMETER_CODE";
            // 
            // txtBoxCode
            // 
            resources.ApplyResources(this.txtBoxCode, "txtBoxCode");
            this.txtBoxCode.Name = "txtBoxCode";
            this.txtBoxCode.ReadOnly = true;
            // 
            // lblPOLE
            // 
            resources.ApplyResources(this.lblPOLE, "lblPOLE");
            this.lblPOLE.Name = "lblPOLE";
            // 
            // lblAREA
            // 
            resources.ApplyResources(this.lblAREA, "lblAREA");
            this.lblAREA.Name = "lblAREA";
            // 
            // lblCUSTOMER_NAME
            // 
            resources.ApplyResources(this.lblCUSTOMER_NAME, "lblCUSTOMER_NAME");
            this.lblCUSTOMER_NAME.Name = "lblCUSTOMER_NAME";
            // 
            // txtAreaCode
            // 
            resources.ApplyResources(this.txtAreaCode, "txtAreaCode");
            this.txtAreaCode.Name = "txtAreaCode";
            this.txtAreaCode.ReadOnly = true;
            // 
            // txtFullName
            // 
            resources.ApplyResources(this.txtFullName, "txtFullName");
            this.txtFullName.Name = "txtFullName";
            this.txtFullName.ReadOnly = true;
            // 
            // txtCustomerCode
            // 
            resources.ApplyResources(this.txtCustomerCode, "txtCustomerCode");
            this.txtCustomerCode.Name = "txtCustomerCode";
            this.txtCustomerCode.ReadOnly = true;
            // 
            // lblCUSTOMER
            // 
            resources.ApplyResources(this.lblCUSTOMER, "lblCUSTOMER");
            this.lblCUSTOMER.Name = "lblCUSTOMER";
            // 
            // lblUSAGE_INFORMATION
            // 
            resources.ApplyResources(this.lblUSAGE_INFORMATION, "lblUSAGE_INFORMATION");
            this.lblUSAGE_INFORMATION.Name = "lblUSAGE_INFORMATION";
            // 
            // txtTotalBuyPower
            // 
            resources.ApplyResources(this.txtTotalBuyPower, "txtTotalBuyPower");
            this.txtTotalBuyPower.Name = "txtTotalBuyPower";
            this.txtTotalBuyPower.ReadOnly = true;
            // 
            // lblTOTAL_BUY_POWER
            // 
            resources.ApplyResources(this.lblTOTAL_BUY_POWER, "lblTOTAL_BUY_POWER");
            this.lblTOTAL_BUY_POWER.Name = "lblTOTAL_BUY_POWER";
            // 
            // lblPOWER_COMPENSATED
            // 
            resources.ApplyResources(this.lblPOWER_COMPENSATED, "lblPOWER_COMPENSATED");
            this.lblPOWER_COMPENSATED.Name = "lblPOWER_COMPENSATED";
            // 
            // txtVoidCompensated
            // 
            resources.ApplyResources(this.txtVoidCompensated, "txtVoidCompensated");
            this.txtVoidCompensated.Name = "txtVoidCompensated";
            this.txtVoidCompensated.ReadOnly = true;
            // 
            // txtPriceType
            // 
            resources.ApplyResources(this.txtPriceType, "txtPriceType");
            this.txtPriceType.Name = "txtPriceType";
            this.txtPriceType.ReadOnly = true;
            // 
            // lblPRICE_TYPE
            // 
            resources.ApplyResources(this.lblPRICE_TYPE, "lblPRICE_TYPE");
            this.lblPRICE_TYPE.Name = "lblPRICE_TYPE";
            // 
            // lblLAST_BUY_POWER
            // 
            resources.ApplyResources(this.lblLAST_BUY_POWER, "lblLAST_BUY_POWER");
            this.lblLAST_BUY_POWER.Name = "lblLAST_BUY_POWER";
            // 
            // lblLAST_BUY_DATE
            // 
            resources.ApplyResources(this.lblLAST_BUY_DATE, "lblLAST_BUY_DATE");
            this.lblLAST_BUY_DATE.Name = "lblLAST_BUY_DATE";
            // 
            // lblBUY_TIMES
            // 
            resources.ApplyResources(this.lblBUY_TIMES, "lblBUY_TIMES");
            this.lblBUY_TIMES.Name = "lblBUY_TIMES";
            // 
            // txtPurchaseTimes
            // 
            resources.ApplyResources(this.txtPurchaseTimes, "txtPurchaseTimes");
            this.txtPurchaseTimes.Name = "txtPurchaseTimes";
            this.txtPurchaseTimes.ReadOnly = true;
            // 
            // txtLastPurchasePower
            // 
            resources.ApplyResources(this.txtLastPurchasePower, "txtLastPurchasePower");
            this.txtLastPurchasePower.Name = "txtLastPurchasePower";
            this.txtLastPurchasePower.ReadOnly = true;
            // 
            // lblINVOICE_DATE
            // 
            resources.ApplyResources(this.lblINVOICE_DATE, "lblINVOICE_DATE");
            this.lblINVOICE_DATE.Name = "lblINVOICE_DATE";
            // 
            // lblCASH_DRAWER
            // 
            resources.ApplyResources(this.lblCASH_DRAWER, "lblCASH_DRAWER");
            this.lblCASH_DRAWER.Name = "lblCASH_DRAWER";
            // 
            // lblCASHIER
            // 
            resources.ApplyResources(this.lblCASHIER, "lblCASHIER");
            this.lblCASHIER.Name = "lblCASHIER";
            // 
            // txtCashDrawerName
            // 
            resources.ApplyResources(this.txtCashDrawerName, "txtCashDrawerName");
            this.txtCashDrawerName.Name = "txtCashDrawerName";
            this.txtCashDrawerName.ReadOnly = true;
            // 
            // txtUserCashDrawerName
            // 
            resources.ApplyResources(this.txtUserCashDrawerName, "txtUserCashDrawerName");
            this.txtUserCashDrawerName.Name = "txtUserCashDrawerName";
            this.txtUserCashDrawerName.ReadOnly = true;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel2.Controls.Add(this.lblCARD_TYPE_);
            this.panel2.Controls.Add(this.btnREAD_CARD);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // lblCARD_TYPE_
            // 
            this.lblCARD_TYPE_.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblCARD_TYPE_, "lblCARD_TYPE_");
            this.lblCARD_TYPE_.Name = "lblCARD_TYPE_";
            // 
            // btnREAD_CARD
            // 
            resources.ApplyResources(this.btnREAD_CARD, "btnREAD_CARD");
            this.btnREAD_CARD.Name = "btnREAD_CARD";
            this.btnREAD_CARD.UseVisualStyleBackColor = true;
            this.btnREAD_CARD.Click += new System.EventHandler(this.btnReadCard_Click);
            // 
            // dtpPurchaseDate
            // 
            resources.ApplyResources(this.dtpPurchaseDate, "dtpPurchaseDate");
            this.dtpPurchaseDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPurchaseDate.Name = "dtpPurchaseDate";
            // 
            // dtpLastPurchaseDate
            // 
            resources.ApplyResources(this.dtpLastPurchaseDate, "dtpLastPurchaseDate");
            this.dtpLastPurchaseDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpLastPurchaseDate.Name = "dtpLastPurchaseDate";
            // 
            // lblREAD_AMOUNT_
            // 
            resources.ApplyResources(this.lblREAD_AMOUNT_, "lblREAD_AMOUNT_");
            this.lblREAD_AMOUNT_.Name = "lblREAD_AMOUNT_";
            // 
            // lblBUY_POWER
            // 
            resources.ApplyResources(this.lblBUY_POWER, "lblBUY_POWER");
            this.lblBUY_POWER.Name = "lblBUY_POWER";
            // 
            // lblPRICE
            // 
            resources.ApplyResources(this.lblPRICE, "lblPRICE");
            this.lblPRICE.Name = "lblPRICE";
            // 
            // txtVoidPurchasePower
            // 
            resources.ApplyResources(this.txtVoidPurchasePower, "txtVoidPurchasePower");
            this.txtVoidPurchasePower.Name = "txtVoidPurchasePower";
            this.txtVoidPurchasePower.ReadOnly = true;
            this.txtVoidPurchasePower.Enter += new System.EventHandler(this.txtEnterEnglish);
            // 
            // txtVoidDiscount
            // 
            resources.ApplyResources(this.txtVoidDiscount, "txtVoidDiscount");
            this.txtVoidDiscount.Name = "txtVoidDiscount";
            this.txtVoidDiscount.ReadOnly = true;
            this.txtVoidDiscount.Enter += new System.EventHandler(this.txtEnterEnglish);
            this.txtVoidDiscount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtKeyNumber);
            // 
            // txtPrice
            // 
            resources.ApplyResources(this.txtPrice, "txtPrice");
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.ReadOnly = true;
            // 
            // lblDISCOUNT
            // 
            resources.ApplyResources(this.lblDISCOUNT, "lblDISCOUNT");
            this.lblDISCOUNT.Name = "lblDISCOUNT";
            // 
            // lblTOTAL_AMOUNT
            // 
            resources.ApplyResources(this.lblTOTAL_AMOUNT, "lblTOTAL_AMOUNT");
            this.lblTOTAL_AMOUNT.Name = "lblTOTAL_AMOUNT";
            // 
            // txtVoidTotalAmount
            // 
            resources.ApplyResources(this.txtVoidTotalAmount, "txtVoidTotalAmount");
            this.txtVoidTotalAmount.Name = "txtVoidTotalAmount";
            this.txtVoidTotalAmount.ReadOnly = true;
            // 
            // lblVOID_POWER_INFORMATION
            // 
            resources.ApplyResources(this.lblVOID_POWER_INFORMATION, "lblVOID_POWER_INFORMATION");
            this.lblVOID_POWER_INFORMATION.Name = "lblVOID_POWER_INFORMATION";
            // 
            // txtSignUnitPrice
            // 
            resources.ApplyResources(this.txtSignUnitPrice, "txtSignUnitPrice");
            this.txtSignUnitPrice.Name = "txtSignUnitPrice";
            this.txtSignUnitPrice.ReadOnly = true;
            this.txtSignUnitPrice.TabStop = false;
            // 
            // txtCredit
            // 
            this.txtCredit.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.txtCredit, "txtCredit");
            this.txtCredit.Name = "txtCredit";
            this.txtCredit.ReadOnly = true;
            // 
            // lblAMOUNT_SUBSIDY
            // 
            resources.ApplyResources(this.lblAMOUNT_SUBSIDY, "lblAMOUNT_SUBSIDY");
            this.lblAMOUNT_SUBSIDY.Name = "lblAMOUNT_SUBSIDY";
            // 
            // DialogVoidBuyPower
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogVoidBuyPower";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Panel panel1;
        private Label lblGENERAL_INFORMATION;
        private TextBox txtPoleCode;
        private TextBox txtMeterCode;
        private Label lblBOX;
        private Label lblMETER_CODE;
        private TextBox txtBoxCode;
        private Label lblPOLE;
        private Label lblAREA;
        private Label lblCUSTOMER_NAME;
        private TextBox txtAreaCode;
        private TextBox txtFullName;
        private TextBox txtCustomerCode;
        private Label lblCUSTOMER;
        private TextBox txtTotalBuyPower;
        private Label lblTOTAL_BUY_POWER;
        private Label lblPOWER_COMPENSATED;
        private TextBox txtVoidCompensated;
        private TextBox txtPriceType;
        private Label lblPRICE_TYPE;
        private Label lblUSAGE_INFORMATION;
        private TextBox txtPurchaseTimes;
        private TextBox txtLastPurchasePower;
        private Label lblBUY_TIMES;
        private Label lblLAST_BUY_DATE;
        private Label lblLAST_BUY_POWER;
        private Label lblINVOICE_DATE;
        private TextBox txtCashDrawerName;
        private TextBox txtUserCashDrawerName;
        private Label lblCASHIER;
        private Label lblCASH_DRAWER;
        private Panel panel2;
        private Label lblCARD_TYPE_;
        private ExButton btnREAD_CARD;
        private DateTimePicker dtpLastPurchaseDate;
        private DateTimePicker dtpPurchaseDate;
        private Label lblREAD_AMOUNT_;
        private TextBox txtVoidTotalAmount;
        private Label lblTOTAL_AMOUNT;
        private Label lblDISCOUNT;
        private TextBox txtPrice;
        private TextBox txtVoidDiscount;
        private TextBox txtVoidPurchasePower;
        private Label lblPRICE;
        private Label lblBUY_POWER;
        private Label lblVOID_POWER_INFORMATION;
        private TextBox txtSignUnitPrice;
        private TextBox txtCredit;
        private Label lblAMOUNT_SUBSIDY;
    }
}