﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.PrePaid
{
    partial class DialogRunPrepaidCredit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMONTH = new System.Windows.Forms.Label();
            this.lblSTART_DATE = new System.Windows.Forms.Label();
            this.dtpRunMonth = new System.Windows.Forms.DateTimePicker();
            this.dtpFrom = new System.Windows.Forms.DateTimePicker();
            this.lblEND_DATE = new System.Windows.Forms.Label();
            this.dtpTo = new System.Windows.Forms.DateTimePicker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnRUN = new SoftTech.Component.ExButton();
            this.lblCYCLE_NAME = new System.Windows.Forms.Label();
            this.cboBillingCycle = new System.Windows.Forms.ComboBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.cboBillingCycle);
            this.content.Controls.Add(this.lblCYCLE_NAME);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnRUN);
            this.content.Controls.Add(this.dtpTo);
            this.content.Controls.Add(this.dtpFrom);
            this.content.Controls.Add(this.dtpRunMonth);
            this.content.Controls.Add(this.lblSTART_DATE);
            this.content.Controls.Add(this.lblMONTH);
            this.content.Controls.Add(this.lblEND_DATE);
            this.content.Size = new System.Drawing.Size(410, 175);
            this.content.TabIndex = 0;
            this.content.Text = "\'CONTENT\'";
            this.content.Controls.SetChildIndex(this.lblEND_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblMONTH, 0);
            this.content.Controls.SetChildIndex(this.lblSTART_DATE, 0);
            this.content.Controls.SetChildIndex(this.dtpRunMonth, 0);
            this.content.Controls.SetChildIndex(this.dtpFrom, 0);
            this.content.Controls.SetChildIndex(this.dtpTo, 0);
            this.content.Controls.SetChildIndex(this.btnRUN, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.lblCYCLE_NAME, 0);
            this.content.Controls.SetChildIndex(this.cboBillingCycle, 0);
            // 
            // lblMONTH
            // 
            this.lblMONTH.AutoSize = true;
            this.lblMONTH.Location = new System.Drawing.Point(26, 22);
            this.lblMONTH.Name = "lblMONTH";
            this.lblMONTH.Size = new System.Drawing.Size(45, 19);
            this.lblMONTH.TabIndex = 6;
            this.lblMONTH.Text = "ប្រចាំខែ";
            // 
            // lblSTART_DATE
            // 
            this.lblSTART_DATE.AutoSize = true;
            this.lblSTART_DATE.Location = new System.Drawing.Point(25, 85);
            this.lblSTART_DATE.Name = "lblSTART_DATE";
            this.lblSTART_DATE.Size = new System.Drawing.Size(49, 19);
            this.lblSTART_DATE.TabIndex = 8;
            this.lblSTART_DATE.Text = "ចាប់ពីថ្ងៃ";
            // 
            // dtpRunMonth
            // 
            this.dtpRunMonth.CustomFormat = "MM-yyyy";
            this.dtpRunMonth.Enabled = false;
            this.dtpRunMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpRunMonth.Location = new System.Drawing.Point(148, 18);
            this.dtpRunMonth.Name = "dtpRunMonth";
            this.dtpRunMonth.Size = new System.Drawing.Size(233, 27);
            this.dtpRunMonth.TabIndex = 0;
            // 
            // dtpFrom
            // 
            this.dtpFrom.CustomFormat = "dd-MM-yyyy";
            this.dtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFrom.Location = new System.Drawing.Point(148, 81);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(102, 27);
            this.dtpFrom.TabIndex = 2;
            // 
            // lblEND_DATE
            // 
            this.lblEND_DATE.AutoSize = true;
            this.lblEND_DATE.Location = new System.Drawing.Point(250, 85);
            this.lblEND_DATE.Name = "lblEND_DATE";
            this.lblEND_DATE.Size = new System.Drawing.Size(29, 19);
            this.lblEND_DATE.TabIndex = 15;
            this.lblEND_DATE.Text = "ដល់";
            // 
            // dtpTo
            // 
            this.dtpTo.CustomFormat = "dd-MM-yyyy";
            this.dtpTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTo.Location = new System.Drawing.Point(279, 81);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(102, 27);
            this.dtpTo.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Location = new System.Drawing.Point(1, 139);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(409, 1);
            this.panel1.TabIndex = 24;
            // 
            // btnCLOSE
            // 
            this.btnCLOSE.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnCLOSE.Location = new System.Drawing.Point(329, 145);
            this.btnCLOSE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.Size = new System.Drawing.Size(75, 23);
            this.btnCLOSE.TabIndex = 6;
            this.btnCLOSE.Text = "បោះបង់";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnRUN
            // 
            this.btnRUN.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnRUN.Location = new System.Drawing.Point(250, 145);
            this.btnRUN.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRUN.Name = "btnRUN";
            this.btnRUN.Size = new System.Drawing.Size(75, 23);
            this.btnRUN.TabIndex = 5;
            this.btnRUN.Text = "តំណើរការ";
            this.btnRUN.UseVisualStyleBackColor = true;
            this.btnRUN.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblCYCLE_NAME
            // 
            this.lblCYCLE_NAME.AutoSize = true;
            this.lblCYCLE_NAME.Location = new System.Drawing.Point(26, 53);
            this.lblCYCLE_NAME.Name = "lblCYCLE_NAME";
            this.lblCYCLE_NAME.Size = new System.Drawing.Size(45, 19);
            this.lblCYCLE_NAME.TabIndex = 25;
            this.lblCYCLE_NAME.Text = "ជុំទូទាត់";
            // 
            // cboBillingCycle
            // 
            this.cboBillingCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBillingCycle.Enabled = false;
            this.cboBillingCycle.FormattingEnabled = true;
            this.cboBillingCycle.Location = new System.Drawing.Point(148, 49);
            this.cboBillingCycle.Name = "cboBillingCycle";
            this.cboBillingCycle.Size = new System.Drawing.Size(233, 27);
            this.cboBillingCycle.TabIndex = 1;
            // 
            // DialogRunPrepaidCredit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(410, 198);
            this.Name = "DialogRunPrepaidCredit";
            this.Text = "ការទូទាត់ឧបត្ថម្ភធនប្រចាំខែ";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Label lblSTART_DATE;
        private Label lblMONTH;
        private Label lblEND_DATE;
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnRUN;
        public DateTimePicker dtpTo;
        public DateTimePicker dtpFrom;
        public DateTimePicker dtpRunMonth;
        private Label lblCYCLE_NAME;
        public ComboBox cboBillingCycle;
    }
}