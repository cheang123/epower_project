﻿using EPower.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface.PrePaid
{
    public partial class PageReadDataFromMeter : UserControl
    {  
        public PageReadDataFromMeter()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgvCus, dgvMeter);
        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            dgvCus.Rows.Clear();
            dgvMeter.Rows.Clear();
            ICCard obj = ReadCardInfo();
            if (((CardType)obj.ICCardType)!= CardType.TestCard)
            {
                return;
            }  

            TestCutCard objTest = (TestCutCard)obj;            
            var objCus= (from c in DBDataContext.Db.TBL_CUSTOMERs
                                 join pc in DBDataContext.Db.TBL_PREPAID_CUSTOMERs on c.CUSTOMER_ID equals pc.CUSTOMER_ID
                                 where pc.CARD_CODE.ToLower()==objTest.CustomerCode.ToLower()
                                 select new 
                                     {
                                         c,
                                         pc                                         
                                     }).FirstOrDefault(); 

            dgvMeter.Rows.Add(objCus==null?0: objCus.c.CUSTOMER_ID ,objCus==null?"": string.Concat(objCus.c.LAST_NAME_KH,' ',objCus.c.FIRST_NAME_KH), objTest.LastPurchasePower
                ,objTest.RemainPower,objTest.TotalPower,objTest.BuyTimes);

            if (objCus!=null)
            {
                decimal buyqty = 0;
                TBL_CUSTOMER_BUY objBuy= (from cb in DBDataContext.Db.TBL_CUSTOMER_BUYs
                                          where cb.PREPAID_CUS_ID == objCus.pc.PREPAID_CUS_ID && cb.IS_VOID == false
                                          orderby cb.CREATE_ON descending
                                          select new
                                          {
                                              CUSTOMER_LAST_BUY = cb ?? new TBL_CUSTOMER_BUY() { CREATE_ON = UIHelper._DefaultDate }
                                          }).FirstOrDefault().CUSTOMER_LAST_BUY;

                DBDataContext.Db.TBL_CUSTOMER_BUYs.OrderByDescending(x=>x.BUY_TIMES).FirstOrDefault(x=>x.PREPAID_CUS_ID==objCus.pc.PREPAID_CUS_ID && x.IS_VOID==false);
                
                if (objBuy!=null)
                {
                    buyqty = objBuy.BUY_QTY;
                }


                dgvCus.Rows.Add(objCus.c.CUSTOMER_ID, string.Concat(objCus.c.LAST_NAME_KH, ' ', objCus.c.FIRST_NAME_KH)
                   , buyqty, objCus.pc.TOTAL_BUY_POWER, objCus.pc.BUY_TIMES);
            }  
        }

        private ICCard ReadCardInfo()
        {
            ICCard objReadCard = ICCard.Read();
            switch ((CardType)objReadCard.ICCardType)
            {
                case CardType.NoCard:
                    lblCARD_TYPE_.Text = Resources.MS_PLEASE_INSERT_NO_POWER_CARD_TO_DEVICE;
                    break;
                case CardType.BlankCard:
                    lblCARD_TYPE_.Text = Resources.IC_CARD_BLANK;
                    break;
                case CardType.FormattedCard:
                    lblCARD_TYPE_.Text = Resources.IC_CARD_BLANK;
                    break;
                case CardType.HavePowerCard:
                    lblCARD_TYPE_.Text = Resources.IC_CARD_HAVE_POWER;
                    break;
                case CardType.NoPowerCard:
                    lblCARD_TYPE_.Text = Resources.IC_CARD_NO_POWER;
                    break;
                case CardType.ResetCard:
                    lblCARD_TYPE_.Text = Resources.IC_CARD_RESET;
                    break;
                case CardType.PresetCard:
                case CardType.TestCard:
                    lblCARD_TYPE_.Text = Resources.IC_CARD_PRESET;
                    break;
                case CardType.CutCard:
                    lblCARD_TYPE_.Text = Resources.IC_CARD_CUT;
                    break;
                default:
                    break;
            }

            return objReadCard;
        }
    }
}
