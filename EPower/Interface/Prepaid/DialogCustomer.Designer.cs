﻿using System.ComponentModel;
using System.Windows.Forms;
using dotnetCHARTING.WinForms;
using SoftTech.Component;
using Label = System.Windows.Forms.Label;

namespace EPower.Interface.PrePaid
{
    partial class DialogCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            dotnetCHARTING.WinForms.ChartArea chartArea1 = new dotnetCHARTING.WinForms.ChartArea();
            dotnetCHARTING.WinForms.Background background1 = new dotnetCHARTING.WinForms.Background();
            dotnetCHARTING.WinForms.Element element1 = new dotnetCHARTING.WinForms.Element();
            dotnetCHARTING.WinForms.SubValue subValue1 = new dotnetCHARTING.WinForms.SubValue();
            dotnetCHARTING.WinForms.Line line1 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Hotspot hotspot1 = new dotnetCHARTING.WinForms.Hotspot();
            dotnetCHARTING.WinForms.LegendEntry legendEntry1 = new dotnetCHARTING.WinForms.LegendEntry();
            dotnetCHARTING.WinForms.Background background2 = new dotnetCHARTING.WinForms.Background();
            dotnetCHARTING.WinForms.Line line2 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Hotspot hotspot2 = new dotnetCHARTING.WinForms.Hotspot();
            dotnetCHARTING.WinForms.Line line3 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Label label1 = new dotnetCHARTING.WinForms.Label();
            dotnetCHARTING.WinForms.Hotspot hotspot3 = new dotnetCHARTING.WinForms.Hotspot();
            dotnetCHARTING.WinForms.Truncation truncation1 = new dotnetCHARTING.WinForms.Truncation();
            dotnetCHARTING.WinForms.LegendBox legendBox1 = new dotnetCHARTING.WinForms.LegendBox();
            dotnetCHARTING.WinForms.Background background3 = new dotnetCHARTING.WinForms.Background();
            dotnetCHARTING.WinForms.LegendEntry legendEntry2 = new dotnetCHARTING.WinForms.LegendEntry();
            dotnetCHARTING.WinForms.Background background4 = new dotnetCHARTING.WinForms.Background();
            dotnetCHARTING.WinForms.Line line4 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Hotspot hotspot4 = new dotnetCHARTING.WinForms.Hotspot();
            dotnetCHARTING.WinForms.Label label6 = new dotnetCHARTING.WinForms.Label();
            dotnetCHARTING.WinForms.Hotspot hotspot5 = new dotnetCHARTING.WinForms.Hotspot();
            dotnetCHARTING.WinForms.Truncation truncation2 = new dotnetCHARTING.WinForms.Truncation();
            dotnetCHARTING.WinForms.LegendEntry legendEntry3 = new dotnetCHARTING.WinForms.LegendEntry();
            dotnetCHARTING.WinForms.Background background5 = new dotnetCHARTING.WinForms.Background();
            dotnetCHARTING.WinForms.Line line5 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Hotspot hotspot6 = new dotnetCHARTING.WinForms.Hotspot();
            dotnetCHARTING.WinForms.Line line6 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Line line7 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Shadow shadow1 = new dotnetCHARTING.WinForms.Shadow();
            dotnetCHARTING.WinForms.Line line8 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Shadow shadow2 = new dotnetCHARTING.WinForms.Shadow();
            dotnetCHARTING.WinForms.Box box1 = new dotnetCHARTING.WinForms.Box();
            dotnetCHARTING.WinForms.Background background6 = new dotnetCHARTING.WinForms.Background();
            dotnetCHARTING.WinForms.Line line9 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Label label7 = new dotnetCHARTING.WinForms.Label();
            dotnetCHARTING.WinForms.Hotspot hotspot7 = new dotnetCHARTING.WinForms.Hotspot();
            dotnetCHARTING.WinForms.Truncation truncation3 = new dotnetCHARTING.WinForms.Truncation();
            dotnetCHARTING.WinForms.Line line10 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Shadow shadow3 = new dotnetCHARTING.WinForms.Shadow();
            dotnetCHARTING.WinForms.Axis axis1 = new dotnetCHARTING.WinForms.Axis();
            dotnetCHARTING.WinForms.AxisTick axisTick1 = new dotnetCHARTING.WinForms.AxisTick();
            dotnetCHARTING.WinForms.Line line11 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Line line12 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.TimeIntervalAdvanced timeIntervalAdvanced1 = new dotnetCHARTING.WinForms.TimeIntervalAdvanced();
            dotnetCHARTING.WinForms.ScaleRange scaleRange1 = new dotnetCHARTING.WinForms.ScaleRange();
            dotnetCHARTING.WinForms.Line line13 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.TimeIntervalAdvanced timeIntervalAdvanced2 = new dotnetCHARTING.WinForms.TimeIntervalAdvanced();
            dotnetCHARTING.WinForms.AxisTick axisTick2 = new dotnetCHARTING.WinForms.AxisTick();
            dotnetCHARTING.WinForms.Line line14 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Line line15 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Axis axis2 = new dotnetCHARTING.WinForms.Axis();
            dotnetCHARTING.WinForms.AxisTick axisTick3 = new dotnetCHARTING.WinForms.AxisTick();
            dotnetCHARTING.WinForms.Line line16 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Line line17 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.ScaleRange scaleRange2 = new dotnetCHARTING.WinForms.ScaleRange();
            dotnetCHARTING.WinForms.Line line18 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.TimeIntervalAdvanced timeIntervalAdvanced3 = new dotnetCHARTING.WinForms.TimeIntervalAdvanced();
            dotnetCHARTING.WinForms.AxisTick axisTick4 = new dotnetCHARTING.WinForms.AxisTick();
            dotnetCHARTING.WinForms.Line line19 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Line line20 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Axis axis3 = new dotnetCHARTING.WinForms.Axis();
            dotnetCHARTING.WinForms.AxisTick axisTick5 = new dotnetCHARTING.WinForms.AxisTick();
            dotnetCHARTING.WinForms.Line line21 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Line line22 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.ScaleRange scaleRange3 = new dotnetCHARTING.WinForms.ScaleRange();
            dotnetCHARTING.WinForms.Line line23 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.TimeIntervalAdvanced timeIntervalAdvanced4 = new dotnetCHARTING.WinForms.TimeIntervalAdvanced();
            dotnetCHARTING.WinForms.AxisTick axisTick6 = new dotnetCHARTING.WinForms.AxisTick();
            dotnetCHARTING.WinForms.Line line24 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Line line25 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Element element2 = new dotnetCHARTING.WinForms.Element();
            dotnetCHARTING.WinForms.SubValue subValue2 = new dotnetCHARTING.WinForms.SubValue();
            dotnetCHARTING.WinForms.Line line26 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Hotspot hotspot8 = new dotnetCHARTING.WinForms.Hotspot();
            dotnetCHARTING.WinForms.LegendEntry legendEntry4 = new dotnetCHARTING.WinForms.LegendEntry();
            dotnetCHARTING.WinForms.Background background7 = new dotnetCHARTING.WinForms.Background();
            dotnetCHARTING.WinForms.Line line27 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Hotspot hotspot9 = new dotnetCHARTING.WinForms.Hotspot();
            dotnetCHARTING.WinForms.SmartLabel smartLabel1 = new dotnetCHARTING.WinForms.SmartLabel();
            dotnetCHARTING.WinForms.Hotspot hotspot10 = new dotnetCHARTING.WinForms.Hotspot();
            dotnetCHARTING.WinForms.Line line28 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Truncation truncation4 = new dotnetCHARTING.WinForms.Truncation();
            dotnetCHARTING.WinForms.Shadow shadow4 = new dotnetCHARTING.WinForms.Shadow();
            dotnetCHARTING.WinForms.Hotspot hotspot11 = new dotnetCHARTING.WinForms.Hotspot();
            dotnetCHARTING.WinForms.Truncation truncation5 = new dotnetCHARTING.WinForms.Truncation();
            dotnetCHARTING.WinForms.ChartArea chartArea2 = new dotnetCHARTING.WinForms.ChartArea();
            dotnetCHARTING.WinForms.Background background8 = new dotnetCHARTING.WinForms.Background();
            dotnetCHARTING.WinForms.Element element3 = new dotnetCHARTING.WinForms.Element();
            dotnetCHARTING.WinForms.SubValue subValue3 = new dotnetCHARTING.WinForms.SubValue();
            dotnetCHARTING.WinForms.Line line29 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Hotspot hotspot12 = new dotnetCHARTING.WinForms.Hotspot();
            dotnetCHARTING.WinForms.LegendEntry legendEntry5 = new dotnetCHARTING.WinForms.LegendEntry();
            dotnetCHARTING.WinForms.Background background9 = new dotnetCHARTING.WinForms.Background();
            dotnetCHARTING.WinForms.Line line30 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Hotspot hotspot13 = new dotnetCHARTING.WinForms.Hotspot();
            dotnetCHARTING.WinForms.Line line31 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Label label8 = new dotnetCHARTING.WinForms.Label();
            dotnetCHARTING.WinForms.Hotspot hotspot14 = new dotnetCHARTING.WinForms.Hotspot();
            dotnetCHARTING.WinForms.Truncation truncation6 = new dotnetCHARTING.WinForms.Truncation();
            dotnetCHARTING.WinForms.Line line32 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Shadow shadow5 = new dotnetCHARTING.WinForms.Shadow();
            dotnetCHARTING.WinForms.Box box2 = new dotnetCHARTING.WinForms.Box();
            dotnetCHARTING.WinForms.Background background10 = new dotnetCHARTING.WinForms.Background();
            dotnetCHARTING.WinForms.Line line33 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Label label9 = new dotnetCHARTING.WinForms.Label();
            dotnetCHARTING.WinForms.Hotspot hotspot15 = new dotnetCHARTING.WinForms.Hotspot();
            dotnetCHARTING.WinForms.Truncation truncation7 = new dotnetCHARTING.WinForms.Truncation();
            dotnetCHARTING.WinForms.Line line34 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Shadow shadow6 = new dotnetCHARTING.WinForms.Shadow();
            dotnetCHARTING.WinForms.Axis axis4 = new dotnetCHARTING.WinForms.Axis();
            dotnetCHARTING.WinForms.AxisTick axisTick7 = new dotnetCHARTING.WinForms.AxisTick();
            dotnetCHARTING.WinForms.Line line35 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Line line36 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.TimeIntervalAdvanced timeIntervalAdvanced5 = new dotnetCHARTING.WinForms.TimeIntervalAdvanced();
            dotnetCHARTING.WinForms.ScaleRange scaleRange4 = new dotnetCHARTING.WinForms.ScaleRange();
            dotnetCHARTING.WinForms.Line line37 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.TimeIntervalAdvanced timeIntervalAdvanced6 = new dotnetCHARTING.WinForms.TimeIntervalAdvanced();
            dotnetCHARTING.WinForms.AxisTick axisTick8 = new dotnetCHARTING.WinForms.AxisTick();
            dotnetCHARTING.WinForms.Line line38 = new dotnetCHARTING.WinForms.Line();
            dotnetCHARTING.WinForms.Line line39 = new dotnetCHARTING.WinForms.Line();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCustomer));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnSAVE = new SoftTech.Component.ExButton();
            this.cboArea = new System.Windows.Forms.ComboBox();
            this.lblAREA = new System.Windows.Forms.Label();
            this.txtPlaceofBirth = new System.Windows.Forms.TextBox();
            this.lblPLACE_OF_BIRTH = new System.Windows.Forms.Label();
            this.txtCustomerCode = new System.Windows.Forms.TextBox();
            this.lblCUSTOMER_CODE = new System.Windows.Forms.Label();
            this.picPhoto = new System.Windows.Forms.PictureBox();
            this.dtpDOB = new System.Windows.Forms.DateTimePicker();
            this.lblDATE_OF_BIRTH = new System.Windows.Forms.Label();
            this.lblADDRESS_INFORMATION = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.lblADDRESS = new System.Windows.Forms.Label();
            this.cboVillage = new System.Windows.Forms.ComboBox();
            this.cboCommune = new System.Windows.Forms.ComboBox();
            this.cboDistrict = new System.Windows.Forms.ComboBox();
            this.cboProvince = new System.Windows.Forms.ComboBox();
            this.lblVILLAGE = new System.Windows.Forms.Label();
            this.lblCOMMUNE = new System.Windows.Forms.Label();
            this.lblDISTRICT = new System.Windows.Forms.Label();
            this.lblPROVINCE = new System.Windows.Forms.Label();
            this.txtPhone2 = new System.Windows.Forms.TextBox();
            this.txtPhone1 = new System.Windows.Forms.TextBox();
            this.txtCompanyName = new System.Windows.Forms.TextBox();
            this.txtIdNumber = new System.Windows.Forms.TextBox();
            this.cboSex = new System.Windows.Forms.ComboBox();
            this.txtLastNameKH = new System.Windows.Forms.TextBox();
            this.txtFirstNameKH = new System.Windows.Forms.TextBox();
            this.lblCOMPANY_NAME = new System.Windows.Forms.Label();
            this.lblID_CARD_NUMBER = new System.Windows.Forms.Label();
            this.lblPHONE_II = new System.Windows.Forms.Label();
            this.lblPHONE_I = new System.Windows.Forms.Label();
            this.lblSEX = new System.Windows.Forms.Label();
            this.lblLAST_NAME_KH = new System.Windows.Forms.Label();
            this.lblFIRST_NAME_KH = new System.Windows.Forms.Label();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.lblHOUSE_NO = new System.Windows.Forms.Label();
            this.lblSTREET_NO = new System.Windows.Forms.Label();
            this.txtHouseNo = new System.Windows.Forms.TextBox();
            this.txtStreetNo = new System.Windows.Forms.TextBox();
            this.lblLAST_NAME = new System.Windows.Forms.Label();
            this.lblFIRST_NAME = new System.Windows.Forms.Label();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.btnPHOTO = new SoftTech.Component.ExLinkLabel(this.components);
            this.label25 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.lblJOB = new System.Windows.Forms.Label();
            this.txtJob = new System.Windows.Forms.TextBox();
            this.txtCheckDigit = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabGENERAL_INFORMATION = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cboTitles = new System.Windows.Forms.ComboBox();
            this.lblTITLES = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cboIdCardType = new System.Windows.Forms.ComboBox();
            this.lblID_CARD_NUMBER_TYPE = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cboCustomerType = new System.Windows.Forms.ComboBox();
            this.lblTYPE_OF_CUSTOMERS = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.txtTotalFamily = new System.Windows.Forms.TextBox();
            this.lblTOTAL_FAMILY = new System.Windows.Forms.Label();
            this.tabUSAGE_INFORMATION = new System.Windows.Forms.TabPage();
            this.cboCustomerGroup = new System.Windows.Forms.ComboBox();
            this.cboConnectionType = new System.Windows.Forms.ComboBox();
            this.lblCONNECTION = new System.Windows.Forms.Label();
            this.dtpREQUEST_CONNECTION_DATE = new System.Windows.Forms.DateTimePicker();
            this.lblREQUEST_CONNECTION_DATE = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnADD = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnEDIT = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnREMOVE = new SoftTech.Component.ExLinkLabel(this.components);
            this.cboBillingCycle = new System.Windows.Forms.ComboBox();
            this.lblPRICE_AND_BILLING_CYCLE = new System.Windows.Forms.Label();
            this.btnREPORT_CONNECTION = new SoftTech.Component.ExButton();
            this.btnREPORT_AGREEMENT = new SoftTech.Component.ExButton();
            this.label30 = new System.Windows.Forms.Label();
            this.cboPrice = new System.Windows.Forms.ComboBox();
            this.txtTotalPower = new System.Windows.Forms.TextBox();
            this.lblTOTAL_WATT = new System.Windows.Forms.Label();
            this.dgvEquitment = new System.Windows.Forms.DataGridView();
            this.CUS_EQUIPMENT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EQUIPMENT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EQUIPMENT_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EQUIPMENT_QUANTITY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WATT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_WATT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COMMENT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._DB = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._EDITED = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.lblCUSTOMER_TYPE_AND_AMPERE = new System.Windows.Forms.Label();
            this.lblPHASE = new System.Windows.Forms.Label();
            this.lblVOLTAGE = new System.Windows.Forms.Label();
            this.lblEQUIPMENT = new System.Windows.Forms.Label();
            this.cboPhase = new System.Windows.Forms.ComboBox();
            this.cboAMP = new System.Windows.Forms.ComboBox();
            this.cboVol = new System.Windows.Forms.ComboBox();
            this.label68 = new System.Windows.Forms.Label();
            this.tabDEPOSIT = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnADD_DEPOSIT = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnADJUST_DEPOSIT = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnAPPLY_PAYMENT = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnREFUND_DEPOSIT = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnPRINT = new SoftTech.Component.ExLinkLabel(this.components);
            this.dgvBalanceDeposit = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblTOTAL_DEPOSIT_AMOUNT = new System.Windows.Forms.Label();
            this.dgvDeposit = new System.Windows.Forms.DataGridView();
            this.CUS_DEPOSIT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DEPOSIT_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_ON_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_BY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DEPOSIT_ACTION_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BALANCE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D_CURRENCY_SING1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_PAID = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tabBILLING_AND_METER = new System.Windows.Forms.TabPage();
            this.btnREACTIVATE = new SoftTech.Component.ExButton();
            this.btnCHANGE_BOX = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnCHANGE_BREAKER = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnCHANGE_METER = new SoftTech.Component.ExLinkLabel(this.components);
            this.txtPoleCode = new System.Windows.Forms.TextBox();
            this.lblPOLE_AND_BOX_INSTALLATION = new System.Windows.Forms.Label();
            this.txtCutter = new System.Windows.Forms.TextBox();
            this.txtBiller = new System.Windows.Forms.TextBox();
            this.txtCollector = new System.Windows.Forms.TextBox();
            this.label71 = new System.Windows.Forms.Label();
            this.lblCUTTER = new System.Windows.Forms.Label();
            this.lblBILLER = new System.Windows.Forms.Label();
            this.lblCOLLECTOR = new System.Windows.Forms.Label();
            this.txtBox = new SoftTech.Component.ExTextbox();
            this.lblBOX_CODE = new System.Windows.Forms.Label();
            this.lblPOLE_CODE = new System.Windows.Forms.Label();
            this.lblCONSTANT_2 = new System.Windows.Forms.Label();
            this.txtBreakerConstant = new System.Windows.Forms.TextBox();
            this.txtBreakerType = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.lblBREAKER_INSTALLATION = new System.Windows.Forms.Label();
            this.lblAMPARE_2 = new System.Windows.Forms.Label();
            this.lblVOLTAGE_2 = new System.Windows.Forms.Label();
            this.lblPHASE_2 = new System.Windows.Forms.Label();
            this.txtBreakerAmp = new System.Windows.Forms.TextBox();
            this.txtBreakerVol = new System.Windows.Forms.TextBox();
            this.txtBreakerPhase = new System.Windows.Forms.TextBox();
            this.lblBREAKER_CODE = new System.Windows.Forms.Label();
            this.txtBreaker = new SoftTech.Component.ExTextbox();
            this.lblBREAKER_TYPE = new System.Windows.Forms.Label();
            this.lblCONSTANT = new System.Windows.Forms.Label();
            this.txtMeterConstant = new System.Windows.Forms.TextBox();
            this.txtMeterType = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.lblAMPARE = new System.Windows.Forms.Label();
            this.lblVOLTAGE_1 = new System.Windows.Forms.Label();
            this.lblPHASE_1 = new System.Windows.Forms.Label();
            this.lblCABLE_SHIELD = new System.Windows.Forms.Label();
            this.lblSHIELD = new System.Windows.Forms.Label();
            this.txtMeterAmp = new System.Windows.Forms.TextBox();
            this.txtMeterVol = new System.Windows.Forms.TextBox();
            this.txtMeterPhase = new System.Windows.Forms.TextBox();
            this.lblMETER_CODE = new System.Windows.Forms.Label();
            this.lblMETER_INSTALLATION = new System.Windows.Forms.Label();
            this.lblMETER_TYPE = new System.Windows.Forms.Label();
            this.cboCableShield = new System.Windows.Forms.ComboBox();
            this.cboMeterShield = new System.Windows.Forms.ComboBox();
            this.txtMeter = new SoftTech.Component.ExTextbox();
            this.dtpActivateDate = new System.Windows.Forms.DateTimePicker();
            this.label45 = new System.Windows.Forms.Label();
            this.lblACTIVATE_DATE = new System.Windows.Forms.Label();
            this.nudCutoffDays = new System.Windows.Forms.NumericUpDown();
            this.lblCUT_DAY = new System.Windows.Forms.Label();
            this.lblUNLIMITE_CUT_DAY = new System.Windows.Forms.Label();
            this.tabINVOICE = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnADJUST_USAGE = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnADJUST_AMOUNT = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnPRINT_1 = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnREPORT_CUSTOMER_AR = new SoftTech.Component.ExButton();
            this.lblTOTAL_DUE_AMOUNT = new System.Windows.Forms.Label();
            this.dgvTotalBalance = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_BALANCE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvInvoiceList = new System.Windows.Forms.DataGridView();
            this.INVOICE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICE_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICE_TITLE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICE_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SETTLE_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAID_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DUE_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_SING_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DUE_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DUE_DAY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnADD_SERVICE = new SoftTech.Component.ExButton();
            this.btnAPPLY_PAYMENT_1 = new SoftTech.Component.ExButton();
            this.cboInvoiceStatus = new System.Windows.Forms.ComboBox();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.lblSTART_DATE = new System.Windows.Forms.Label();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.lblEND_DATE_ = new System.Windows.Forms.Label();
            this.tabBUY_POWER_MONTHLY = new System.Windows.Forms.TabPage();
            this.btnPRINT_RECEIPT = new SoftTech.Component.ExLinkLabel(this.components);
            this.txtTotalPurchasePower = new System.Windows.Forms.TextBox();
            this.lblTOTAL_BUY_POWER = new System.Windows.Forms.Label();
            this.cboIsVoidPurchase = new System.Windows.Forms.ComboBox();
            this.dgvCusPurchase = new System.Windows.Forms.DataGridView();
            this.CUSTOMER_BUY_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PREPAID_CUS_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BUY_TIMES = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_ON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BUY_POWER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.POWER_COMPENSATED = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DISCOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_SUBSIDY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BUY_CURRENCY_SING_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_VOID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblINVOICE_DATE = new System.Windows.Forms.Label();
            this.dtPurchaseMonth = new System.Windows.Forms.DateTimePicker();
            this.tabPREPAID_USAGE = new System.Windows.Forms.TabPage();
            this.dtpYear = new System.Windows.Forms.DateTimePicker();
            this.btnADJUST_USAGE_1 = new SoftTech.Component.ExLinkLabel(this.components);
            this.txtTotalRemainCreditCurrencySign = new System.Windows.Forms.TextBox();
            this.txtTotalRemainCredit = new System.Windows.Forms.TextBox();
            this.lblLAST_SUBSIDY_BALANCE = new System.Windows.Forms.Label();
            this.dgvPrepaidUsage = new System.Windows.Forms.DataGridView();
            this.lblINVOICE_DATE_1 = new System.Windows.Forms.Label();
            this.tabBUY_POWER_YEARLY = new System.Windows.Forms.TabPage();
            this.chart1 = new dotnetCHARTING.WinForms.Chart();
            this.btnActivate = new SoftTech.Component.ExButton();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.cmsStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ChangeConnectionType = new System.Windows.Forms.ToolStripMenuItem();
            this.PREPAID_CREDIT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.START_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.END_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MONTH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.USAGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRICE_CREDIT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_SUBSIDY_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SETTLE_AMOUNT_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BALANCE_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREDIT_CURRENCY_SING_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPhoto)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabGENERAL_INFORMATION.SuspendLayout();
            this.tabUSAGE_INFORMATION.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEquitment)).BeginInit();
            this.tabDEPOSIT.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBalanceDeposit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeposit)).BeginInit();
            this.tabBILLING_AND_METER.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCutoffDays)).BeginInit();
            this.tabINVOICE.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTotalBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInvoiceList)).BeginInit();
            this.tabBUY_POWER_MONTHLY.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCusPurchase)).BeginInit();
            this.tabPREPAID_USAGE.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrepaidUsage)).BeginInit();
            this.tabBUY_POWER_YEARLY.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.cmsStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.tabControl1);
            this.content.Controls.Add(this.btnActivate);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnSAVE);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnSAVE, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.btnActivate, 0);
            this.content.Controls.SetChildIndex(this.tabControl1, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            // 
            // btnSAVE
            // 
            resources.ApplyResources(this.btnSAVE, "btnSAVE");
            this.btnSAVE.Name = "btnSAVE";
            this.btnSAVE.UseVisualStyleBackColor = true;
            this.btnSAVE.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cboArea
            // 
            this.cboArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboArea, "cboArea");
            this.cboArea.FormattingEnabled = true;
            this.cboArea.Name = "cboArea";
            this.cboArea.Enter += new System.EventHandler(this.txtLastNameKH_Enter);
            // 
            // lblAREA
            // 
            resources.ApplyResources(this.lblAREA, "lblAREA");
            this.lblAREA.Name = "lblAREA";
            // 
            // txtPlaceofBirth
            // 
            resources.ApplyResources(this.txtPlaceofBirth, "txtPlaceofBirth");
            this.txtPlaceofBirth.Name = "txtPlaceofBirth";
            this.txtPlaceofBirth.Enter += new System.EventHandler(this.InputKhmer);
            // 
            // lblPLACE_OF_BIRTH
            // 
            resources.ApplyResources(this.lblPLACE_OF_BIRTH, "lblPLACE_OF_BIRTH");
            this.lblPLACE_OF_BIRTH.Name = "lblPLACE_OF_BIRTH";
            // 
            // txtCustomerCode
            // 
            resources.ApplyResources(this.txtCustomerCode, "txtCustomerCode");
            this.txtCustomerCode.Name = "txtCustomerCode";
            this.txtCustomerCode.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // lblCUSTOMER_CODE
            // 
            resources.ApplyResources(this.lblCUSTOMER_CODE, "lblCUSTOMER_CODE");
            this.lblCUSTOMER_CODE.Name = "lblCUSTOMER_CODE";
            // 
            // picPhoto
            // 
            this.picPhoto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            resources.ApplyResources(this.picPhoto, "picPhoto");
            this.picPhoto.Name = "picPhoto";
            this.picPhoto.TabStop = false;
            // 
            // dtpDOB
            // 
            this.dtpDOB.CausesValidation = false;
            this.dtpDOB.Checked = false;
            resources.ApplyResources(this.dtpDOB, "dtpDOB");
            this.dtpDOB.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDOB.Name = "dtpDOB";
            this.dtpDOB.ShowCheckBox = true;
            this.dtpDOB.Enter += new System.EventHandler(this.InputEnglish);
            this.dtpDOB.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dtpDOB_MouseDown);
            // 
            // lblDATE_OF_BIRTH
            // 
            resources.ApplyResources(this.lblDATE_OF_BIRTH, "lblDATE_OF_BIRTH");
            this.lblDATE_OF_BIRTH.Name = "lblDATE_OF_BIRTH";
            // 
            // lblADDRESS_INFORMATION
            // 
            resources.ApplyResources(this.lblADDRESS_INFORMATION, "lblADDRESS_INFORMATION");
            this.lblADDRESS_INFORMATION.Name = "lblADDRESS_INFORMATION";
            // 
            // txtAddress
            // 
            resources.ApplyResources(this.txtAddress, "txtAddress");
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Enter += new System.EventHandler(this.InputKhmer);
            // 
            // lblADDRESS
            // 
            resources.ApplyResources(this.lblADDRESS, "lblADDRESS");
            this.lblADDRESS.Name = "lblADDRESS";
            // 
            // cboVillage
            // 
            this.cboVillage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboVillage, "cboVillage");
            this.cboVillage.FormattingEnabled = true;
            this.cboVillage.Name = "cboVillage";
            // 
            // cboCommune
            // 
            this.cboCommune.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboCommune, "cboCommune");
            this.cboCommune.FormattingEnabled = true;
            this.cboCommune.Name = "cboCommune";
            this.cboCommune.SelectedIndexChanged += new System.EventHandler(this.cboCommune_SelectedIndexChanged);
            // 
            // cboDistrict
            // 
            this.cboDistrict.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboDistrict, "cboDistrict");
            this.cboDistrict.FormattingEnabled = true;
            this.cboDistrict.Name = "cboDistrict";
            this.cboDistrict.SelectedIndexChanged += new System.EventHandler(this.cboDistrict_SelectedIndexChanged);
            // 
            // cboProvince
            // 
            this.cboProvince.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboProvince, "cboProvince");
            this.cboProvince.FormattingEnabled = true;
            this.cboProvince.Name = "cboProvince";
            this.cboProvince.SelectedIndexChanged += new System.EventHandler(this.cboProvince_SelectedIndexChanged);
            // 
            // lblVILLAGE
            // 
            resources.ApplyResources(this.lblVILLAGE, "lblVILLAGE");
            this.lblVILLAGE.Name = "lblVILLAGE";
            // 
            // lblCOMMUNE
            // 
            resources.ApplyResources(this.lblCOMMUNE, "lblCOMMUNE");
            this.lblCOMMUNE.Name = "lblCOMMUNE";
            // 
            // lblDISTRICT
            // 
            resources.ApplyResources(this.lblDISTRICT, "lblDISTRICT");
            this.lblDISTRICT.Name = "lblDISTRICT";
            // 
            // lblPROVINCE
            // 
            resources.ApplyResources(this.lblPROVINCE, "lblPROVINCE");
            this.lblPROVINCE.Name = "lblPROVINCE";
            // 
            // txtPhone2
            // 
            resources.ApplyResources(this.txtPhone2, "txtPhone2");
            this.txtPhone2.Name = "txtPhone2";
            this.txtPhone2.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // txtPhone1
            // 
            resources.ApplyResources(this.txtPhone1, "txtPhone1");
            this.txtPhone1.Name = "txtPhone1";
            this.txtPhone1.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // txtCompanyName
            // 
            resources.ApplyResources(this.txtCompanyName, "txtCompanyName");
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Enter += new System.EventHandler(this.InputKhmer);
            // 
            // txtIdNumber
            // 
            resources.ApplyResources(this.txtIdNumber, "txtIdNumber");
            this.txtIdNumber.Name = "txtIdNumber";
            this.txtIdNumber.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // cboSex
            // 
            this.cboSex.AllowDrop = true;
            this.cboSex.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboSex, "cboSex");
            this.cboSex.FormattingEnabled = true;
            this.cboSex.Name = "cboSex";
            // 
            // txtLastNameKH
            // 
            resources.ApplyResources(this.txtLastNameKH, "txtLastNameKH");
            this.txtLastNameKH.Name = "txtLastNameKH";
            this.txtLastNameKH.Enter += new System.EventHandler(this.InputKhmer);
            // 
            // txtFirstNameKH
            // 
            resources.ApplyResources(this.txtFirstNameKH, "txtFirstNameKH");
            this.txtFirstNameKH.Name = "txtFirstNameKH";
            this.txtFirstNameKH.Enter += new System.EventHandler(this.InputKhmer);
            // 
            // lblCOMPANY_NAME
            // 
            resources.ApplyResources(this.lblCOMPANY_NAME, "lblCOMPANY_NAME");
            this.lblCOMPANY_NAME.Name = "lblCOMPANY_NAME";
            // 
            // lblID_CARD_NUMBER
            // 
            resources.ApplyResources(this.lblID_CARD_NUMBER, "lblID_CARD_NUMBER");
            this.lblID_CARD_NUMBER.Name = "lblID_CARD_NUMBER";
            // 
            // lblPHONE_II
            // 
            resources.ApplyResources(this.lblPHONE_II, "lblPHONE_II");
            this.lblPHONE_II.Name = "lblPHONE_II";
            // 
            // lblPHONE_I
            // 
            resources.ApplyResources(this.lblPHONE_I, "lblPHONE_I");
            this.lblPHONE_I.Name = "lblPHONE_I";
            // 
            // lblSEX
            // 
            resources.ApplyResources(this.lblSEX, "lblSEX");
            this.lblSEX.Name = "lblSEX";
            // 
            // lblLAST_NAME_KH
            // 
            resources.ApplyResources(this.lblLAST_NAME_KH, "lblLAST_NAME_KH");
            this.lblLAST_NAME_KH.Name = "lblLAST_NAME_KH";
            // 
            // lblFIRST_NAME_KH
            // 
            resources.ApplyResources(this.lblFIRST_NAME_KH, "lblFIRST_NAME_KH");
            this.lblFIRST_NAME_KH.Name = "lblFIRST_NAME_KH";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblHOUSE_NO
            // 
            resources.ApplyResources(this.lblHOUSE_NO, "lblHOUSE_NO");
            this.lblHOUSE_NO.Name = "lblHOUSE_NO";
            // 
            // lblSTREET_NO
            // 
            resources.ApplyResources(this.lblSTREET_NO, "lblSTREET_NO");
            this.lblSTREET_NO.Name = "lblSTREET_NO";
            // 
            // txtHouseNo
            // 
            resources.ApplyResources(this.txtHouseNo, "txtHouseNo");
            this.txtHouseNo.Name = "txtHouseNo";
            this.txtHouseNo.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // txtStreetNo
            // 
            resources.ApplyResources(this.txtStreetNo, "txtStreetNo");
            this.txtStreetNo.Name = "txtStreetNo";
            this.txtStreetNo.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // lblLAST_NAME
            // 
            resources.ApplyResources(this.lblLAST_NAME, "lblLAST_NAME");
            this.lblLAST_NAME.Name = "lblLAST_NAME";
            // 
            // lblFIRST_NAME
            // 
            resources.ApplyResources(this.lblFIRST_NAME, "lblFIRST_NAME");
            this.lblFIRST_NAME.Name = "lblFIRST_NAME";
            // 
            // txtLastName
            // 
            resources.ApplyResources(this.txtLastName, "txtLastName");
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // txtFirstName
            // 
            resources.ApplyResources(this.txtFirstName, "txtFirstName");
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // btnPHOTO
            // 
            resources.ApplyResources(this.btnPHOTO, "btnPHOTO");
            this.btnPHOTO.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnPHOTO.Name = "btnPHOTO";
            this.btnPHOTO.TabStop = true;
            this.btnPHOTO.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkPhoto_LinkClicked);
            // 
            // label25
            // 
            resources.ApplyResources(this.label25, "label25");
            this.label25.ForeColor = System.Drawing.Color.Red;
            this.label25.Name = "label25";
            // 
            // label27
            // 
            resources.ApplyResources(this.label27, "label27");
            this.label27.ForeColor = System.Drawing.Color.Red;
            this.label27.Name = "label27";
            // 
            // label28
            // 
            resources.ApplyResources(this.label28, "label28");
            this.label28.ForeColor = System.Drawing.Color.Red;
            this.label28.Name = "label28";
            // 
            // label34
            // 
            resources.ApplyResources(this.label34, "label34");
            this.label34.ForeColor = System.Drawing.Color.Red;
            this.label34.Name = "label34";
            // 
            // label36
            // 
            resources.ApplyResources(this.label36, "label36");
            this.label36.ForeColor = System.Drawing.Color.Red;
            this.label36.Name = "label36";
            // 
            // label37
            // 
            resources.ApplyResources(this.label37, "label37");
            this.label37.ForeColor = System.Drawing.Color.Red;
            this.label37.Name = "label37";
            // 
            // label38
            // 
            resources.ApplyResources(this.label38, "label38");
            this.label38.ForeColor = System.Drawing.Color.Red;
            this.label38.Name = "label38";
            // 
            // label39
            // 
            resources.ApplyResources(this.label39, "label39");
            this.label39.ForeColor = System.Drawing.Color.Red;
            this.label39.Name = "label39";
            // 
            // lblJOB
            // 
            resources.ApplyResources(this.lblJOB, "lblJOB");
            this.lblJOB.Name = "lblJOB";
            // 
            // txtJob
            // 
            resources.ApplyResources(this.txtJob, "txtJob");
            this.txtJob.Name = "txtJob";
            this.txtJob.Enter += new System.EventHandler(this.InputKhmer);
            // 
            // txtCheckDigit
            // 
            resources.ApplyResources(this.txtCheckDigit, "txtCheckDigit");
            this.txtCheckDigit.Name = "txtCheckDigit";
            this.txtCheckDigit.ReadOnly = true;
            this.txtCheckDigit.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabGENERAL_INFORMATION);
            this.tabControl1.Controls.Add(this.tabUSAGE_INFORMATION);
            this.tabControl1.Controls.Add(this.tabDEPOSIT);
            this.tabControl1.Controls.Add(this.tabBILLING_AND_METER);
            this.tabControl1.Controls.Add(this.tabINVOICE);
            this.tabControl1.Controls.Add(this.tabBUY_POWER_MONTHLY);
            this.tabControl1.Controls.Add(this.tabPREPAID_USAGE);
            this.tabControl1.Controls.Add(this.tabBUY_POWER_YEARLY);
            resources.ApplyResources(this.tabControl1, "tabControl1");
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            // 
            // tabGENERAL_INFORMATION
            // 
            this.tabGENERAL_INFORMATION.Controls.Add(this.label2);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label4);
            this.tabGENERAL_INFORMATION.Controls.Add(this.cboTitles);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblTITLES);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label3);
            this.tabGENERAL_INFORMATION.Controls.Add(this.cboIdCardType);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblID_CARD_NUMBER_TYPE);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label5);
            this.tabGENERAL_INFORMATION.Controls.Add(this.cboCustomerType);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblTYPE_OF_CUSTOMERS);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label46);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtTotalFamily);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblTOTAL_FAMILY);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label38);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label27);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label25);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblFIRST_NAME_KH);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblLAST_NAME_KH);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtJob);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblSEX);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblPHONE_I);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblJOB);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblPHONE_II);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label39);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblID_CARD_NUMBER);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblCOMPANY_NAME);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label37);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtFirstNameKH);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label36);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtLastNameKH);
            this.tabGENERAL_INFORMATION.Controls.Add(this.cboSex);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label34);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtIdNumber);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtCompanyName);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtPhone1);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtPhone2);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblPROVINCE);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblDISTRICT);
            this.tabGENERAL_INFORMATION.Controls.Add(this.label28);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblCOMMUNE);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblVILLAGE);
            this.tabGENERAL_INFORMATION.Controls.Add(this.cboProvince);
            this.tabGENERAL_INFORMATION.Controls.Add(this.cboDistrict);
            this.tabGENERAL_INFORMATION.Controls.Add(this.cboCommune);
            this.tabGENERAL_INFORMATION.Controls.Add(this.cboVillage);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblADDRESS);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtAddress);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblADDRESS_INFORMATION);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblDATE_OF_BIRTH);
            this.tabGENERAL_INFORMATION.Controls.Add(this.dtpDOB);
            this.tabGENERAL_INFORMATION.Controls.Add(this.picPhoto);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblCUSTOMER_CODE);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtCustomerCode);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblPLACE_OF_BIRTH);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtPlaceofBirth);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblAREA);
            this.tabGENERAL_INFORMATION.Controls.Add(this.cboArea);
            this.tabGENERAL_INFORMATION.Controls.Add(this.btnPHOTO);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblHOUSE_NO);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtFirstName);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblSTREET_NO);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtLastName);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtHouseNo);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblFIRST_NAME);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtStreetNo);
            this.tabGENERAL_INFORMATION.Controls.Add(this.lblLAST_NAME);
            this.tabGENERAL_INFORMATION.Controls.Add(this.txtCheckDigit);
            resources.ApplyResources(this.tabGENERAL_INFORMATION, "tabGENERAL_INFORMATION");
            this.tabGENERAL_INFORMATION.Name = "tabGENERAL_INFORMATION";
            this.tabGENERAL_INFORMATION.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Name = "label2";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Name = "label4";
            // 
            // cboTitles
            // 
            this.cboTitles.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboTitles, "cboTitles");
            this.cboTitles.FormattingEnabled = true;
            this.cboTitles.Name = "cboTitles";
            // 
            // lblTITLES
            // 
            resources.ApplyResources(this.lblTITLES, "lblTITLES");
            this.lblTITLES.Name = "lblTITLES";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Name = "label3";
            // 
            // cboIdCardType
            // 
            this.cboIdCardType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboIdCardType, "cboIdCardType");
            this.cboIdCardType.FormattingEnabled = true;
            this.cboIdCardType.Name = "cboIdCardType";
            // 
            // lblID_CARD_NUMBER_TYPE
            // 
            resources.ApplyResources(this.lblID_CARD_NUMBER_TYPE, "lblID_CARD_NUMBER_TYPE");
            this.lblID_CARD_NUMBER_TYPE.Name = "lblID_CARD_NUMBER_TYPE";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Name = "label5";
            // 
            // cboCustomerType
            // 
            this.cboCustomerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboCustomerType, "cboCustomerType");
            this.cboCustomerType.FormattingEnabled = true;
            this.cboCustomerType.Name = "cboCustomerType";
            // 
            // lblTYPE_OF_CUSTOMERS
            // 
            resources.ApplyResources(this.lblTYPE_OF_CUSTOMERS, "lblTYPE_OF_CUSTOMERS");
            this.lblTYPE_OF_CUSTOMERS.Name = "lblTYPE_OF_CUSTOMERS";
            // 
            // label46
            // 
            resources.ApplyResources(this.label46, "label46");
            this.label46.ForeColor = System.Drawing.Color.Red;
            this.label46.Name = "label46";
            // 
            // txtTotalFamily
            // 
            resources.ApplyResources(this.txtTotalFamily, "txtTotalFamily");
            this.txtTotalFamily.Name = "txtTotalFamily";
            // 
            // lblTOTAL_FAMILY
            // 
            resources.ApplyResources(this.lblTOTAL_FAMILY, "lblTOTAL_FAMILY");
            this.lblTOTAL_FAMILY.Name = "lblTOTAL_FAMILY";
            // 
            // tabUSAGE_INFORMATION
            // 
            this.tabUSAGE_INFORMATION.Controls.Add(this.cboCustomerGroup);
            this.tabUSAGE_INFORMATION.Controls.Add(this.cboConnectionType);
            this.tabUSAGE_INFORMATION.Controls.Add(this.lblCONNECTION);
            this.tabUSAGE_INFORMATION.Controls.Add(this.dtpREQUEST_CONNECTION_DATE);
            this.tabUSAGE_INFORMATION.Controls.Add(this.lblREQUEST_CONNECTION_DATE);
            this.tabUSAGE_INFORMATION.Controls.Add(this.panel1);
            this.tabUSAGE_INFORMATION.Controls.Add(this.cboBillingCycle);
            this.tabUSAGE_INFORMATION.Controls.Add(this.lblPRICE_AND_BILLING_CYCLE);
            this.tabUSAGE_INFORMATION.Controls.Add(this.btnREPORT_CONNECTION);
            this.tabUSAGE_INFORMATION.Controls.Add(this.btnREPORT_AGREEMENT);
            this.tabUSAGE_INFORMATION.Controls.Add(this.label30);
            this.tabUSAGE_INFORMATION.Controls.Add(this.cboPrice);
            this.tabUSAGE_INFORMATION.Controls.Add(this.txtTotalPower);
            this.tabUSAGE_INFORMATION.Controls.Add(this.lblTOTAL_WATT);
            this.tabUSAGE_INFORMATION.Controls.Add(this.dgvEquitment);
            this.tabUSAGE_INFORMATION.Controls.Add(this.label33);
            this.tabUSAGE_INFORMATION.Controls.Add(this.label32);
            this.tabUSAGE_INFORMATION.Controls.Add(this.label31);
            this.tabUSAGE_INFORMATION.Controls.Add(this.lblCUSTOMER_TYPE_AND_AMPERE);
            this.tabUSAGE_INFORMATION.Controls.Add(this.lblPHASE);
            this.tabUSAGE_INFORMATION.Controls.Add(this.lblVOLTAGE);
            this.tabUSAGE_INFORMATION.Controls.Add(this.lblEQUIPMENT);
            this.tabUSAGE_INFORMATION.Controls.Add(this.cboPhase);
            this.tabUSAGE_INFORMATION.Controls.Add(this.cboAMP);
            this.tabUSAGE_INFORMATION.Controls.Add(this.cboVol);
            this.tabUSAGE_INFORMATION.Controls.Add(this.label68);
            resources.ApplyResources(this.tabUSAGE_INFORMATION, "tabUSAGE_INFORMATION");
            this.tabUSAGE_INFORMATION.Name = "tabUSAGE_INFORMATION";
            this.tabUSAGE_INFORMATION.UseVisualStyleBackColor = true;
            // 
            // cboCustomerGroup
            // 
            this.cboCustomerGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomerGroup.DropDownWidth = 350;
            this.cboCustomerGroup.FormattingEnabled = true;
            resources.ApplyResources(this.cboCustomerGroup, "cboCustomerGroup");
            this.cboCustomerGroup.Name = "cboCustomerGroup";
            this.cboCustomerGroup.SelectedIndexChanged += new System.EventHandler(this.cboCustomerGroup_SelectedIndexChanged);
            // 
            // cboConnectionType
            // 
            this.cboConnectionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboConnectionType.DropDownWidth = 350;
            this.cboConnectionType.FormattingEnabled = true;
            resources.ApplyResources(this.cboConnectionType, "cboConnectionType");
            this.cboConnectionType.Name = "cboConnectionType";
            // 
            // lblCONNECTION
            // 
            resources.ApplyResources(this.lblCONNECTION, "lblCONNECTION");
            this.lblCONNECTION.Name = "lblCONNECTION";
            // 
            // dtpREQUEST_CONNECTION_DATE
            // 
            resources.ApplyResources(this.dtpREQUEST_CONNECTION_DATE, "dtpREQUEST_CONNECTION_DATE");
            this.dtpREQUEST_CONNECTION_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpREQUEST_CONNECTION_DATE.Name = "dtpREQUEST_CONNECTION_DATE";
            // 
            // lblREQUEST_CONNECTION_DATE
            // 
            resources.ApplyResources(this.lblREQUEST_CONNECTION_DATE, "lblREQUEST_CONNECTION_DATE");
            this.lblREQUEST_CONNECTION_DATE.Name = "lblREQUEST_CONNECTION_DATE";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnADD);
            this.panel1.Controls.Add(this.btnEDIT);
            this.panel1.Controls.Add(this.btnREMOVE);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnADD
            // 
            resources.ApplyResources(this.btnADD, "btnADD");
            this.btnADD.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnADD.Name = "btnADD";
            this.btnADD.TabStop = true;
            this.btnADD.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnAdd_LinkClicked);
            // 
            // btnEDIT
            // 
            resources.ApplyResources(this.btnEDIT, "btnEDIT");
            this.btnEDIT.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnEDIT.Name = "btnEDIT";
            this.btnEDIT.TabStop = true;
            this.btnEDIT.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnEdit_LinkClicked);
            // 
            // btnREMOVE
            // 
            resources.ApplyResources(this.btnREMOVE, "btnREMOVE");
            this.btnREMOVE.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnREMOVE.Name = "btnREMOVE";
            this.btnREMOVE.TabStop = true;
            this.btnREMOVE.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnRemove_LinkClicked);
            // 
            // cboBillingCycle
            // 
            this.cboBillingCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBillingCycle.FormattingEnabled = true;
            resources.ApplyResources(this.cboBillingCycle, "cboBillingCycle");
            this.cboBillingCycle.Name = "cboBillingCycle";
            // 
            // lblPRICE_AND_BILLING_CYCLE
            // 
            resources.ApplyResources(this.lblPRICE_AND_BILLING_CYCLE, "lblPRICE_AND_BILLING_CYCLE");
            this.lblPRICE_AND_BILLING_CYCLE.Name = "lblPRICE_AND_BILLING_CYCLE";
            // 
            // btnREPORT_CONNECTION
            // 
            resources.ApplyResources(this.btnREPORT_CONNECTION, "btnREPORT_CONNECTION");
            this.btnREPORT_CONNECTION.Name = "btnREPORT_CONNECTION";
            this.btnREPORT_CONNECTION.UseVisualStyleBackColor = true;
            this.btnREPORT_CONNECTION.Click += new System.EventHandler(this.btnConnection_Click);
            // 
            // btnREPORT_AGREEMENT
            // 
            resources.ApplyResources(this.btnREPORT_AGREEMENT, "btnREPORT_AGREEMENT");
            this.btnREPORT_AGREEMENT.Name = "btnREPORT_AGREEMENT";
            this.btnREPORT_AGREEMENT.UseVisualStyleBackColor = true;
            this.btnREPORT_AGREEMENT.Click += new System.EventHandler(this.btnAgreement_Click);
            // 
            // label30
            // 
            resources.ApplyResources(this.label30, "label30");
            this.label30.ForeColor = System.Drawing.Color.Red;
            this.label30.Name = "label30";
            // 
            // cboPrice
            // 
            this.cboPrice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPrice.DropDownWidth = 350;
            this.cboPrice.FormattingEnabled = true;
            resources.ApplyResources(this.cboPrice, "cboPrice");
            this.cboPrice.Name = "cboPrice";
            // 
            // txtTotalPower
            // 
            resources.ApplyResources(this.txtTotalPower, "txtTotalPower");
            this.txtTotalPower.Name = "txtTotalPower";
            // 
            // lblTOTAL_WATT
            // 
            resources.ApplyResources(this.lblTOTAL_WATT, "lblTOTAL_WATT");
            this.lblTOTAL_WATT.Name = "lblTOTAL_WATT";
            // 
            // dgvEquitment
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvEquitment.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvEquitment.BackgroundColor = System.Drawing.Color.White;
            this.dgvEquitment.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvEquitment.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvEquitment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEquitment.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CUS_EQUIPMENT_ID,
            this.EQUIPMENT_ID,
            this.EQUIPMENT_NAME,
            this.EQUIPMENT_QUANTITY,
            this.WATT,
            this.TOTAL_WATT,
            this.COMMENT,
            this._DB,
            this._EDITED});
            this.dgvEquitment.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgvEquitment, "dgvEquitment");
            this.dgvEquitment.Name = "dgvEquitment";
            this.dgvEquitment.RowHeadersVisible = false;
            // 
            // CUS_EQUIPMENT_ID
            // 
            resources.ApplyResources(this.CUS_EQUIPMENT_ID, "CUS_EQUIPMENT_ID");
            this.CUS_EQUIPMENT_ID.Name = "CUS_EQUIPMENT_ID";
            // 
            // EQUIPMENT_ID
            // 
            resources.ApplyResources(this.EQUIPMENT_ID, "EQUIPMENT_ID");
            this.EQUIPMENT_ID.Name = "EQUIPMENT_ID";
            // 
            // EQUIPMENT_NAME
            // 
            this.EQUIPMENT_NAME.DataPropertyName = "EQUIPMENT_NAME";
            resources.ApplyResources(this.EQUIPMENT_NAME, "EQUIPMENT_NAME");
            this.EQUIPMENT_NAME.Name = "EQUIPMENT_NAME";
            // 
            // EQUIPMENT_QUANTITY
            // 
            this.EQUIPMENT_QUANTITY.DataPropertyName = "QUANTITY";
            resources.ApplyResources(this.EQUIPMENT_QUANTITY, "EQUIPMENT_QUANTITY");
            this.EQUIPMENT_QUANTITY.Name = "EQUIPMENT_QUANTITY";
            // 
            // WATT
            // 
            this.WATT.DataPropertyName = "POWER";
            resources.ApplyResources(this.WATT, "WATT");
            this.WATT.Name = "WATT";
            // 
            // TOTAL_WATT
            // 
            this.TOTAL_WATT.DataPropertyName = "TOTAL_POWER";
            resources.ApplyResources(this.TOTAL_WATT, "TOTAL_WATT");
            this.TOTAL_WATT.Name = "TOTAL_WATT";
            // 
            // COMMENT
            // 
            this.COMMENT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.COMMENT.DataPropertyName = "COMMENT";
            resources.ApplyResources(this.COMMENT, "COMMENT");
            this.COMMENT.Name = "COMMENT";
            // 
            // _DB
            // 
            this._DB.DataPropertyName = "_DB";
            resources.ApplyResources(this._DB, "_DB");
            this._DB.Name = "_DB";
            this._DB.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._DB.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // _EDITED
            // 
            resources.ApplyResources(this._EDITED, "_EDITED");
            this._EDITED.Name = "_EDITED";
            // 
            // label33
            // 
            resources.ApplyResources(this.label33, "label33");
            this.label33.ForeColor = System.Drawing.Color.Red;
            this.label33.Name = "label33";
            // 
            // label32
            // 
            resources.ApplyResources(this.label32, "label32");
            this.label32.ForeColor = System.Drawing.Color.Red;
            this.label32.Name = "label32";
            // 
            // label31
            // 
            resources.ApplyResources(this.label31, "label31");
            this.label31.ForeColor = System.Drawing.Color.Red;
            this.label31.Name = "label31";
            // 
            // lblCUSTOMER_TYPE_AND_AMPERE
            // 
            resources.ApplyResources(this.lblCUSTOMER_TYPE_AND_AMPERE, "lblCUSTOMER_TYPE_AND_AMPERE");
            this.lblCUSTOMER_TYPE_AND_AMPERE.Name = "lblCUSTOMER_TYPE_AND_AMPERE";
            // 
            // lblPHASE
            // 
            resources.ApplyResources(this.lblPHASE, "lblPHASE");
            this.lblPHASE.Name = "lblPHASE";
            // 
            // lblVOLTAGE
            // 
            resources.ApplyResources(this.lblVOLTAGE, "lblVOLTAGE");
            this.lblVOLTAGE.Name = "lblVOLTAGE";
            // 
            // lblEQUIPMENT
            // 
            resources.ApplyResources(this.lblEQUIPMENT, "lblEQUIPMENT");
            this.lblEQUIPMENT.Name = "lblEQUIPMENT";
            // 
            // cboPhase
            // 
            this.cboPhase.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPhase.FormattingEnabled = true;
            resources.ApplyResources(this.cboPhase, "cboPhase");
            this.cboPhase.Name = "cboPhase";
            this.cboPhase.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // cboAMP
            // 
            this.cboAMP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAMP.FormattingEnabled = true;
            resources.ApplyResources(this.cboAMP, "cboAMP");
            this.cboAMP.Name = "cboAMP";
            this.cboAMP.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // cboVol
            // 
            this.cboVol.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboVol.FormattingEnabled = true;
            resources.ApplyResources(this.cboVol, "cboVol");
            this.cboVol.Name = "cboVol";
            this.cboVol.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // label68
            // 
            resources.ApplyResources(this.label68, "label68");
            this.label68.Name = "label68";
            // 
            // tabDEPOSIT
            // 
            this.tabDEPOSIT.Controls.Add(this.panel2);
            this.tabDEPOSIT.Controls.Add(this.dgvBalanceDeposit);
            this.tabDEPOSIT.Controls.Add(this.lblTOTAL_DEPOSIT_AMOUNT);
            this.tabDEPOSIT.Controls.Add(this.dgvDeposit);
            resources.ApplyResources(this.tabDEPOSIT, "tabDEPOSIT");
            this.tabDEPOSIT.Name = "tabDEPOSIT";
            this.tabDEPOSIT.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnADD_DEPOSIT);
            this.panel2.Controls.Add(this.btnADJUST_DEPOSIT);
            this.panel2.Controls.Add(this.btnAPPLY_PAYMENT);
            this.panel2.Controls.Add(this.btnREFUND_DEPOSIT);
            this.panel2.Controls.Add(this.btnPRINT);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // btnADD_DEPOSIT
            // 
            resources.ApplyResources(this.btnADD_DEPOSIT, "btnADD_DEPOSIT");
            this.btnADD_DEPOSIT.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnADD_DEPOSIT.Name = "btnADD_DEPOSIT";
            this.btnADD_DEPOSIT.TabStop = true;
            this.btnADD_DEPOSIT.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkAddDeposit_LinkClicked);
            // 
            // btnADJUST_DEPOSIT
            // 
            resources.ApplyResources(this.btnADJUST_DEPOSIT, "btnADJUST_DEPOSIT");
            this.btnADJUST_DEPOSIT.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnADJUST_DEPOSIT.Name = "btnADJUST_DEPOSIT";
            this.btnADJUST_DEPOSIT.TabStop = true;
            this.btnADJUST_DEPOSIT.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkAdjustDeposit_LinkClicked);
            // 
            // btnAPPLY_PAYMENT
            // 
            resources.ApplyResources(this.btnAPPLY_PAYMENT, "btnAPPLY_PAYMENT");
            this.btnAPPLY_PAYMENT.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnAPPLY_PAYMENT.Name = "btnAPPLY_PAYMENT";
            this.btnAPPLY_PAYMENT.TabStop = true;
            this.btnAPPLY_PAYMENT.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkApplyPayment_LinkClicked);
            // 
            // btnREFUND_DEPOSIT
            // 
            resources.ApplyResources(this.btnREFUND_DEPOSIT, "btnREFUND_DEPOSIT");
            this.btnREFUND_DEPOSIT.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnREFUND_DEPOSIT.Name = "btnREFUND_DEPOSIT";
            this.btnREFUND_DEPOSIT.TabStop = true;
            this.btnREFUND_DEPOSIT.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkRefundDeposit_LinkClicked);
            // 
            // btnPRINT
            // 
            resources.ApplyResources(this.btnPRINT, "btnPRINT");
            this.btnPRINT.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnPRINT.Name = "btnPRINT";
            this.btnPRINT.TabStop = true;
            this.btnPRINT.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnPrintReceipt_LinkClicked);
            // 
            // dgvBalanceDeposit
            // 
            this.dgvBalanceDeposit.BackgroundColor = System.Drawing.Color.White;
            this.dgvBalanceDeposit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvBalanceDeposit.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvBalanceDeposit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBalanceDeposit.ColumnHeadersVisible = false;
            this.dgvBalanceDeposit.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvBalanceDeposit.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvBalanceDeposit.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgvBalanceDeposit, "dgvBalanceDeposit");
            this.dgvBalanceDeposit.Name = "dgvBalanceDeposit";
            this.dgvBalanceDeposit.RowHeadersVisible = false;
            this.dgvBalanceDeposit.RowTemplate.Height = 25;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "CURRENCY_ID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn2, "dataGridViewTextBoxColumn2");
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "BALANCE";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "#,##0.####";
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.dataGridViewTextBoxColumn3, "dataGridViewTextBoxColumn3");
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "CURRENCY_SING";
            this.dataGridViewTextBoxColumn4.FillWeight = 65F;
            resources.ApplyResources(this.dataGridViewTextBoxColumn4, "dataGridViewTextBoxColumn4");
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // lblTOTAL_DEPOSIT_AMOUNT
            // 
            resources.ApplyResources(this.lblTOTAL_DEPOSIT_AMOUNT, "lblTOTAL_DEPOSIT_AMOUNT");
            this.lblTOTAL_DEPOSIT_AMOUNT.Name = "lblTOTAL_DEPOSIT_AMOUNT";
            // 
            // dgvDeposit
            // 
            this.dgvDeposit.AllowUserToAddRows = false;
            this.dgvDeposit.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvDeposit.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvDeposit.BackgroundColor = System.Drawing.Color.White;
            this.dgvDeposit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvDeposit.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvDeposit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDeposit.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CUS_DEPOSIT_ID,
            this.DEPOSIT_NO,
            this.CREATE_ON_1,
            this.CREATE_BY,
            this.DEPOSIT_ACTION_NAME,
            this.AMOUNT,
            this.BALANCE,
            this.D_CURRENCY_SING1,
            this.IS_PAID});
            this.dgvDeposit.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgvDeposit, "dgvDeposit");
            this.dgvDeposit.Name = "dgvDeposit";
            this.dgvDeposit.ReadOnly = true;
            this.dgvDeposit.RowHeadersVisible = false;
            // 
            // CUS_DEPOSIT_ID
            // 
            this.CUS_DEPOSIT_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.CUS_DEPOSIT_ID.DataPropertyName = "CUS_DEPOSIT_ID";
            resources.ApplyResources(this.CUS_DEPOSIT_ID, "CUS_DEPOSIT_ID");
            this.CUS_DEPOSIT_ID.Name = "CUS_DEPOSIT_ID";
            this.CUS_DEPOSIT_ID.ReadOnly = true;
            // 
            // DEPOSIT_NO
            // 
            this.DEPOSIT_NO.DataPropertyName = "DEPOSIT_NO";
            resources.ApplyResources(this.DEPOSIT_NO, "DEPOSIT_NO");
            this.DEPOSIT_NO.Name = "DEPOSIT_NO";
            this.DEPOSIT_NO.ReadOnly = true;
            // 
            // CREATE_ON_1
            // 
            this.CREATE_ON_1.DataPropertyName = "CREATE_ON";
            dataGridViewCellStyle5.Format = "dd-MM-yyyy";
            this.CREATE_ON_1.DefaultCellStyle = dataGridViewCellStyle5;
            resources.ApplyResources(this.CREATE_ON_1, "CREATE_ON_1");
            this.CREATE_ON_1.Name = "CREATE_ON_1";
            this.CREATE_ON_1.ReadOnly = true;
            // 
            // CREATE_BY
            // 
            this.CREATE_BY.DataPropertyName = "CREATE_BY";
            resources.ApplyResources(this.CREATE_BY, "CREATE_BY");
            this.CREATE_BY.Name = "CREATE_BY";
            this.CREATE_BY.ReadOnly = true;
            // 
            // DEPOSIT_ACTION_NAME
            // 
            this.DEPOSIT_ACTION_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DEPOSIT_ACTION_NAME.DataPropertyName = "DEPOSIT_ACTION_NAME_KH";
            resources.ApplyResources(this.DEPOSIT_ACTION_NAME, "DEPOSIT_ACTION_NAME");
            this.DEPOSIT_ACTION_NAME.Name = "DEPOSIT_ACTION_NAME";
            this.DEPOSIT_ACTION_NAME.ReadOnly = true;
            // 
            // AMOUNT
            // 
            this.AMOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AMOUNT.DataPropertyName = "AMOUNT";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "#,##0.##";
            this.AMOUNT.DefaultCellStyle = dataGridViewCellStyle6;
            resources.ApplyResources(this.AMOUNT, "AMOUNT");
            this.AMOUNT.Name = "AMOUNT";
            this.AMOUNT.ReadOnly = true;
            // 
            // BALANCE
            // 
            this.BALANCE.DataPropertyName = "BALANCE";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "#,##0.##";
            this.BALANCE.DefaultCellStyle = dataGridViewCellStyle7;
            resources.ApplyResources(this.BALANCE, "BALANCE");
            this.BALANCE.Name = "BALANCE";
            this.BALANCE.ReadOnly = true;
            // 
            // D_CURRENCY_SING1
            // 
            this.D_CURRENCY_SING1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.D_CURRENCY_SING1.DataPropertyName = "CURRENCY_SING";
            this.D_CURRENCY_SING1.FillWeight = 65F;
            resources.ApplyResources(this.D_CURRENCY_SING1, "D_CURRENCY_SING1");
            this.D_CURRENCY_SING1.Name = "D_CURRENCY_SING1";
            this.D_CURRENCY_SING1.ReadOnly = true;
            // 
            // IS_PAID
            // 
            this.IS_PAID.DataPropertyName = "IS_PAID";
            resources.ApplyResources(this.IS_PAID, "IS_PAID");
            this.IS_PAID.Name = "IS_PAID";
            this.IS_PAID.ReadOnly = true;
            // 
            // tabBILLING_AND_METER
            // 
            this.tabBILLING_AND_METER.Controls.Add(this.btnREACTIVATE);
            this.tabBILLING_AND_METER.Controls.Add(this.btnCHANGE_BOX);
            this.tabBILLING_AND_METER.Controls.Add(this.btnCHANGE_BREAKER);
            this.tabBILLING_AND_METER.Controls.Add(this.btnCHANGE_METER);
            this.tabBILLING_AND_METER.Controls.Add(this.txtPoleCode);
            this.tabBILLING_AND_METER.Controls.Add(this.lblPOLE_AND_BOX_INSTALLATION);
            this.tabBILLING_AND_METER.Controls.Add(this.txtCutter);
            this.tabBILLING_AND_METER.Controls.Add(this.txtBiller);
            this.tabBILLING_AND_METER.Controls.Add(this.txtCollector);
            this.tabBILLING_AND_METER.Controls.Add(this.label71);
            this.tabBILLING_AND_METER.Controls.Add(this.lblCUTTER);
            this.tabBILLING_AND_METER.Controls.Add(this.lblBILLER);
            this.tabBILLING_AND_METER.Controls.Add(this.lblCOLLECTOR);
            this.tabBILLING_AND_METER.Controls.Add(this.txtBox);
            this.tabBILLING_AND_METER.Controls.Add(this.lblBOX_CODE);
            this.tabBILLING_AND_METER.Controls.Add(this.lblPOLE_CODE);
            this.tabBILLING_AND_METER.Controls.Add(this.lblCONSTANT_2);
            this.tabBILLING_AND_METER.Controls.Add(this.txtBreakerConstant);
            this.tabBILLING_AND_METER.Controls.Add(this.txtBreakerType);
            this.tabBILLING_AND_METER.Controls.Add(this.label53);
            this.tabBILLING_AND_METER.Controls.Add(this.lblBREAKER_INSTALLATION);
            this.tabBILLING_AND_METER.Controls.Add(this.lblAMPARE_2);
            this.tabBILLING_AND_METER.Controls.Add(this.lblVOLTAGE_2);
            this.tabBILLING_AND_METER.Controls.Add(this.lblPHASE_2);
            this.tabBILLING_AND_METER.Controls.Add(this.txtBreakerAmp);
            this.tabBILLING_AND_METER.Controls.Add(this.txtBreakerVol);
            this.tabBILLING_AND_METER.Controls.Add(this.txtBreakerPhase);
            this.tabBILLING_AND_METER.Controls.Add(this.lblBREAKER_CODE);
            this.tabBILLING_AND_METER.Controls.Add(this.txtBreaker);
            this.tabBILLING_AND_METER.Controls.Add(this.lblBREAKER_TYPE);
            this.tabBILLING_AND_METER.Controls.Add(this.lblCONSTANT);
            this.tabBILLING_AND_METER.Controls.Add(this.txtMeterConstant);
            this.tabBILLING_AND_METER.Controls.Add(this.txtMeterType);
            this.tabBILLING_AND_METER.Controls.Add(this.label50);
            this.tabBILLING_AND_METER.Controls.Add(this.lblAMPARE);
            this.tabBILLING_AND_METER.Controls.Add(this.lblVOLTAGE_1);
            this.tabBILLING_AND_METER.Controls.Add(this.lblPHASE_1);
            this.tabBILLING_AND_METER.Controls.Add(this.lblCABLE_SHIELD);
            this.tabBILLING_AND_METER.Controls.Add(this.lblSHIELD);
            this.tabBILLING_AND_METER.Controls.Add(this.txtMeterAmp);
            this.tabBILLING_AND_METER.Controls.Add(this.txtMeterVol);
            this.tabBILLING_AND_METER.Controls.Add(this.txtMeterPhase);
            this.tabBILLING_AND_METER.Controls.Add(this.lblMETER_CODE);
            this.tabBILLING_AND_METER.Controls.Add(this.lblMETER_INSTALLATION);
            this.tabBILLING_AND_METER.Controls.Add(this.lblMETER_TYPE);
            this.tabBILLING_AND_METER.Controls.Add(this.cboCableShield);
            this.tabBILLING_AND_METER.Controls.Add(this.cboMeterShield);
            this.tabBILLING_AND_METER.Controls.Add(this.txtMeter);
            this.tabBILLING_AND_METER.Controls.Add(this.dtpActivateDate);
            this.tabBILLING_AND_METER.Controls.Add(this.label45);
            this.tabBILLING_AND_METER.Controls.Add(this.lblACTIVATE_DATE);
            this.tabBILLING_AND_METER.Controls.Add(this.nudCutoffDays);
            this.tabBILLING_AND_METER.Controls.Add(this.lblCUT_DAY);
            this.tabBILLING_AND_METER.Controls.Add(this.lblUNLIMITE_CUT_DAY);
            resources.ApplyResources(this.tabBILLING_AND_METER, "tabBILLING_AND_METER");
            this.tabBILLING_AND_METER.Name = "tabBILLING_AND_METER";
            this.tabBILLING_AND_METER.UseVisualStyleBackColor = true;
            // 
            // btnREACTIVATE
            // 
            resources.ApplyResources(this.btnREACTIVATE, "btnREACTIVATE");
            this.btnREACTIVATE.Name = "btnREACTIVATE";
            this.btnREACTIVATE.UseVisualStyleBackColor = true;
            this.btnREACTIVATE.Click += new System.EventHandler(this.btnReactivate_Click);
            // 
            // btnCHANGE_BOX
            // 
            this.btnCHANGE_BOX.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            resources.ApplyResources(this.btnCHANGE_BOX, "btnCHANGE_BOX");
            this.btnCHANGE_BOX.Name = "btnCHANGE_BOX";
            this.btnCHANGE_BOX.TabStop = true;
            this.btnCHANGE_BOX.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnChangeBox_LinkClicked);
            // 
            // btnCHANGE_BREAKER
            // 
            this.btnCHANGE_BREAKER.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            resources.ApplyResources(this.btnCHANGE_BREAKER, "btnCHANGE_BREAKER");
            this.btnCHANGE_BREAKER.Name = "btnCHANGE_BREAKER";
            this.btnCHANGE_BREAKER.TabStop = true;
            this.btnCHANGE_BREAKER.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnChangeBreaker_LinkClicked);
            // 
            // btnCHANGE_METER
            // 
            this.btnCHANGE_METER.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            resources.ApplyResources(this.btnCHANGE_METER, "btnCHANGE_METER");
            this.btnCHANGE_METER.Name = "btnCHANGE_METER";
            this.btnCHANGE_METER.TabStop = true;
            this.btnCHANGE_METER.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnChangeMeter_LinkClicked);
            // 
            // txtPoleCode
            // 
            resources.ApplyResources(this.txtPoleCode, "txtPoleCode");
            this.txtPoleCode.Name = "txtPoleCode";
            this.txtPoleCode.ReadOnly = true;
            // 
            // lblPOLE_AND_BOX_INSTALLATION
            // 
            resources.ApplyResources(this.lblPOLE_AND_BOX_INSTALLATION, "lblPOLE_AND_BOX_INSTALLATION");
            this.lblPOLE_AND_BOX_INSTALLATION.Name = "lblPOLE_AND_BOX_INSTALLATION";
            // 
            // txtCutter
            // 
            resources.ApplyResources(this.txtCutter, "txtCutter");
            this.txtCutter.Name = "txtCutter";
            this.txtCutter.ReadOnly = true;
            // 
            // txtBiller
            // 
            resources.ApplyResources(this.txtBiller, "txtBiller");
            this.txtBiller.Name = "txtBiller";
            this.txtBiller.ReadOnly = true;
            // 
            // txtCollector
            // 
            resources.ApplyResources(this.txtCollector, "txtCollector");
            this.txtCollector.Name = "txtCollector";
            this.txtCollector.ReadOnly = true;
            // 
            // label71
            // 
            resources.ApplyResources(this.label71, "label71");
            this.label71.ForeColor = System.Drawing.Color.Red;
            this.label71.Name = "label71";
            // 
            // lblCUTTER
            // 
            resources.ApplyResources(this.lblCUTTER, "lblCUTTER");
            this.lblCUTTER.Name = "lblCUTTER";
            // 
            // lblBILLER
            // 
            resources.ApplyResources(this.lblBILLER, "lblBILLER");
            this.lblBILLER.Name = "lblBILLER";
            // 
            // lblCOLLECTOR
            // 
            resources.ApplyResources(this.lblCOLLECTOR, "lblCOLLECTOR");
            this.lblCOLLECTOR.Name = "lblCOLLECTOR";
            // 
            // txtBox
            // 
            this.txtBox.BackColor = System.Drawing.Color.White;
            this.txtBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtBox, "txtBox");
            this.txtBox.Name = "txtBox";
            this.txtBox.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtBox.AdvanceSearch += new System.EventHandler(this.txtBox_AdvanceSearch);
            this.txtBox.CancelAdvanceSearch += new System.EventHandler(this.txtBox_CancelAdvanceSearch);
            this.txtBox.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // lblBOX_CODE
            // 
            resources.ApplyResources(this.lblBOX_CODE, "lblBOX_CODE");
            this.lblBOX_CODE.Name = "lblBOX_CODE";
            // 
            // lblPOLE_CODE
            // 
            resources.ApplyResources(this.lblPOLE_CODE, "lblPOLE_CODE");
            this.lblPOLE_CODE.Name = "lblPOLE_CODE";
            // 
            // lblCONSTANT_2
            // 
            resources.ApplyResources(this.lblCONSTANT_2, "lblCONSTANT_2");
            this.lblCONSTANT_2.Name = "lblCONSTANT_2";
            // 
            // txtBreakerConstant
            // 
            resources.ApplyResources(this.txtBreakerConstant, "txtBreakerConstant");
            this.txtBreakerConstant.Name = "txtBreakerConstant";
            this.txtBreakerConstant.ReadOnly = true;
            // 
            // txtBreakerType
            // 
            resources.ApplyResources(this.txtBreakerType, "txtBreakerType");
            this.txtBreakerType.Name = "txtBreakerType";
            this.txtBreakerType.ReadOnly = true;
            // 
            // label53
            // 
            resources.ApplyResources(this.label53, "label53");
            this.label53.ForeColor = System.Drawing.Color.Red;
            this.label53.Name = "label53";
            // 
            // lblBREAKER_INSTALLATION
            // 
            resources.ApplyResources(this.lblBREAKER_INSTALLATION, "lblBREAKER_INSTALLATION");
            this.lblBREAKER_INSTALLATION.Name = "lblBREAKER_INSTALLATION";
            // 
            // lblAMPARE_2
            // 
            resources.ApplyResources(this.lblAMPARE_2, "lblAMPARE_2");
            this.lblAMPARE_2.Name = "lblAMPARE_2";
            // 
            // lblVOLTAGE_2
            // 
            resources.ApplyResources(this.lblVOLTAGE_2, "lblVOLTAGE_2");
            this.lblVOLTAGE_2.Name = "lblVOLTAGE_2";
            // 
            // lblPHASE_2
            // 
            resources.ApplyResources(this.lblPHASE_2, "lblPHASE_2");
            this.lblPHASE_2.Name = "lblPHASE_2";
            // 
            // txtBreakerAmp
            // 
            resources.ApplyResources(this.txtBreakerAmp, "txtBreakerAmp");
            this.txtBreakerAmp.Name = "txtBreakerAmp";
            this.txtBreakerAmp.ReadOnly = true;
            // 
            // txtBreakerVol
            // 
            resources.ApplyResources(this.txtBreakerVol, "txtBreakerVol");
            this.txtBreakerVol.Name = "txtBreakerVol";
            this.txtBreakerVol.ReadOnly = true;
            // 
            // txtBreakerPhase
            // 
            resources.ApplyResources(this.txtBreakerPhase, "txtBreakerPhase");
            this.txtBreakerPhase.Name = "txtBreakerPhase";
            this.txtBreakerPhase.ReadOnly = true;
            // 
            // lblBREAKER_CODE
            // 
            resources.ApplyResources(this.lblBREAKER_CODE, "lblBREAKER_CODE");
            this.lblBREAKER_CODE.Name = "lblBREAKER_CODE";
            // 
            // txtBreaker
            // 
            this.txtBreaker.BackColor = System.Drawing.Color.White;
            this.txtBreaker.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtBreaker, "txtBreaker");
            this.txtBreaker.Name = "txtBreaker";
            this.txtBreaker.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtBreaker.AdvanceSearch += new System.EventHandler(this.txtBreaker_AdvanceSearch);
            this.txtBreaker.CancelAdvanceSearch += new System.EventHandler(this.txtBreaker_CancelAdvanceSearch);
            this.txtBreaker.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // lblBREAKER_TYPE
            // 
            resources.ApplyResources(this.lblBREAKER_TYPE, "lblBREAKER_TYPE");
            this.lblBREAKER_TYPE.Name = "lblBREAKER_TYPE";
            // 
            // lblCONSTANT
            // 
            resources.ApplyResources(this.lblCONSTANT, "lblCONSTANT");
            this.lblCONSTANT.Name = "lblCONSTANT";
            // 
            // txtMeterConstant
            // 
            resources.ApplyResources(this.txtMeterConstant, "txtMeterConstant");
            this.txtMeterConstant.Name = "txtMeterConstant";
            this.txtMeterConstant.ReadOnly = true;
            // 
            // txtMeterType
            // 
            resources.ApplyResources(this.txtMeterType, "txtMeterType");
            this.txtMeterType.Name = "txtMeterType";
            this.txtMeterType.ReadOnly = true;
            // 
            // label50
            // 
            resources.ApplyResources(this.label50, "label50");
            this.label50.ForeColor = System.Drawing.Color.Red;
            this.label50.Name = "label50";
            // 
            // lblAMPARE
            // 
            resources.ApplyResources(this.lblAMPARE, "lblAMPARE");
            this.lblAMPARE.Name = "lblAMPARE";
            // 
            // lblVOLTAGE_1
            // 
            resources.ApplyResources(this.lblVOLTAGE_1, "lblVOLTAGE_1");
            this.lblVOLTAGE_1.Name = "lblVOLTAGE_1";
            // 
            // lblPHASE_1
            // 
            resources.ApplyResources(this.lblPHASE_1, "lblPHASE_1");
            this.lblPHASE_1.Name = "lblPHASE_1";
            // 
            // lblCABLE_SHIELD
            // 
            resources.ApplyResources(this.lblCABLE_SHIELD, "lblCABLE_SHIELD");
            this.lblCABLE_SHIELD.Name = "lblCABLE_SHIELD";
            // 
            // lblSHIELD
            // 
            resources.ApplyResources(this.lblSHIELD, "lblSHIELD");
            this.lblSHIELD.Name = "lblSHIELD";
            // 
            // txtMeterAmp
            // 
            resources.ApplyResources(this.txtMeterAmp, "txtMeterAmp");
            this.txtMeterAmp.Name = "txtMeterAmp";
            this.txtMeterAmp.ReadOnly = true;
            // 
            // txtMeterVol
            // 
            resources.ApplyResources(this.txtMeterVol, "txtMeterVol");
            this.txtMeterVol.Name = "txtMeterVol";
            this.txtMeterVol.ReadOnly = true;
            // 
            // txtMeterPhase
            // 
            resources.ApplyResources(this.txtMeterPhase, "txtMeterPhase");
            this.txtMeterPhase.Name = "txtMeterPhase";
            this.txtMeterPhase.ReadOnly = true;
            // 
            // lblMETER_CODE
            // 
            resources.ApplyResources(this.lblMETER_CODE, "lblMETER_CODE");
            this.lblMETER_CODE.Name = "lblMETER_CODE";
            // 
            // lblMETER_INSTALLATION
            // 
            resources.ApplyResources(this.lblMETER_INSTALLATION, "lblMETER_INSTALLATION");
            this.lblMETER_INSTALLATION.Name = "lblMETER_INSTALLATION";
            // 
            // lblMETER_TYPE
            // 
            resources.ApplyResources(this.lblMETER_TYPE, "lblMETER_TYPE");
            this.lblMETER_TYPE.Name = "lblMETER_TYPE";
            // 
            // cboCableShield
            // 
            this.cboCableShield.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboCableShield, "cboCableShield");
            this.cboCableShield.FormattingEnabled = true;
            this.cboCableShield.Name = "cboCableShield";
            this.cboCableShield.Enter += new System.EventHandler(this.InputKhmer);
            // 
            // cboMeterShield
            // 
            this.cboMeterShield.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboMeterShield, "cboMeterShield");
            this.cboMeterShield.FormattingEnabled = true;
            this.cboMeterShield.Name = "cboMeterShield";
            this.cboMeterShield.Enter += new System.EventHandler(this.InputKhmer);
            // 
            // txtMeter
            // 
            this.txtMeter.BackColor = System.Drawing.Color.White;
            this.txtMeter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtMeter, "txtMeter");
            this.txtMeter.Name = "txtMeter";
            this.txtMeter.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtMeter.AdvanceSearch += new System.EventHandler(this.txtMeter_AdvanceSearch);
            this.txtMeter.CancelAdvanceSearch += new System.EventHandler(this.txtMeter_CancelAdvanceSearch);
            this.txtMeter.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // dtpActivateDate
            // 
            resources.ApplyResources(this.dtpActivateDate, "dtpActivateDate");
            this.dtpActivateDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpActivateDate.Name = "dtpActivateDate";
            // 
            // label45
            // 
            resources.ApplyResources(this.label45, "label45");
            this.label45.ForeColor = System.Drawing.Color.Red;
            this.label45.Name = "label45";
            // 
            // lblACTIVATE_DATE
            // 
            resources.ApplyResources(this.lblACTIVATE_DATE, "lblACTIVATE_DATE");
            this.lblACTIVATE_DATE.Name = "lblACTIVATE_DATE";
            // 
            // nudCutoffDays
            // 
            resources.ApplyResources(this.nudCutoffDays, "nudCutoffDays");
            this.nudCutoffDays.Name = "nudCutoffDays";
            this.nudCutoffDays.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblCUT_DAY
            // 
            resources.ApplyResources(this.lblCUT_DAY, "lblCUT_DAY");
            this.lblCUT_DAY.Name = "lblCUT_DAY";
            // 
            // lblUNLIMITE_CUT_DAY
            // 
            resources.ApplyResources(this.lblUNLIMITE_CUT_DAY, "lblUNLIMITE_CUT_DAY");
            this.lblUNLIMITE_CUT_DAY.Name = "lblUNLIMITE_CUT_DAY";
            // 
            // tabINVOICE
            // 
            this.tabINVOICE.Controls.Add(this.panel3);
            this.tabINVOICE.Controls.Add(this.btnREPORT_CUSTOMER_AR);
            this.tabINVOICE.Controls.Add(this.lblTOTAL_DUE_AMOUNT);
            this.tabINVOICE.Controls.Add(this.dgvTotalBalance);
            this.tabINVOICE.Controls.Add(this.dgvInvoiceList);
            this.tabINVOICE.Controls.Add(this.btnADD_SERVICE);
            this.tabINVOICE.Controls.Add(this.btnAPPLY_PAYMENT_1);
            this.tabINVOICE.Controls.Add(this.cboInvoiceStatus);
            this.tabINVOICE.Controls.Add(this.dtpEndDate);
            this.tabINVOICE.Controls.Add(this.lblSTART_DATE);
            this.tabINVOICE.Controls.Add(this.dtpStartDate);
            this.tabINVOICE.Controls.Add(this.lblEND_DATE_);
            resources.ApplyResources(this.tabINVOICE, "tabINVOICE");
            this.tabINVOICE.Name = "tabINVOICE";
            this.tabINVOICE.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnADJUST_USAGE);
            this.panel3.Controls.Add(this.btnADJUST_AMOUNT);
            this.panel3.Controls.Add(this.btnPRINT_1);
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            // 
            // btnADJUST_USAGE
            // 
            resources.ApplyResources(this.btnADJUST_USAGE, "btnADJUST_USAGE");
            this.btnADJUST_USAGE.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnADJUST_USAGE.Name = "btnADJUST_USAGE";
            this.btnADJUST_USAGE.TabStop = true;
            this.btnADJUST_USAGE.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkAdjustUsage_LinkClicked);
            // 
            // btnADJUST_AMOUNT
            // 
            resources.ApplyResources(this.btnADJUST_AMOUNT, "btnADJUST_AMOUNT");
            this.btnADJUST_AMOUNT.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnADJUST_AMOUNT.Name = "btnADJUST_AMOUNT";
            this.btnADJUST_AMOUNT.TabStop = true;
            this.btnADJUST_AMOUNT.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkAdjustAmount_LinkClicked);
            // 
            // btnPRINT_1
            // 
            resources.ApplyResources(this.btnPRINT_1, "btnPRINT_1");
            this.btnPRINT_1.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnPRINT_1.Name = "btnPRINT_1";
            this.btnPRINT_1.TabStop = true;
            this.btnPRINT_1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkPrintInvoice_LinkClicked);
            // 
            // btnREPORT_CUSTOMER_AR
            // 
            resources.ApplyResources(this.btnREPORT_CUSTOMER_AR, "btnREPORT_CUSTOMER_AR");
            this.btnREPORT_CUSTOMER_AR.Name = "btnREPORT_CUSTOMER_AR";
            this.btnREPORT_CUSTOMER_AR.UseVisualStyleBackColor = true;
            // 
            // lblTOTAL_DUE_AMOUNT
            // 
            resources.ApplyResources(this.lblTOTAL_DUE_AMOUNT, "lblTOTAL_DUE_AMOUNT");
            this.lblTOTAL_DUE_AMOUNT.Name = "lblTOTAL_DUE_AMOUNT";
            // 
            // dgvTotalBalance
            // 
            this.dgvTotalBalance.BackgroundColor = System.Drawing.Color.White;
            this.dgvTotalBalance.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvTotalBalance.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvTotalBalance.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTotalBalance.ColumnHeadersVisible = false;
            this.dgvTotalBalance.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn8,
            this.TOTAL_BALANCE,
            this.dataGridViewTextBoxColumn9});
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvTotalBalance.DefaultCellStyle = dataGridViewCellStyle9;
            this.dgvTotalBalance.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgvTotalBalance, "dgvTotalBalance");
            this.dgvTotalBalance.Name = "dgvTotalBalance";
            this.dgvTotalBalance.RowHeadersVisible = false;
            this.dgvTotalBalance.RowTemplate.Height = 25;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "CURRENCY_ID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn8, "dataGridViewTextBoxColumn8");
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // TOTAL_BALANCE
            // 
            this.TOTAL_BALANCE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TOTAL_BALANCE.DataPropertyName = "TOTAL_BALANCE";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "#,##0.####";
            this.TOTAL_BALANCE.DefaultCellStyle = dataGridViewCellStyle8;
            resources.ApplyResources(this.TOTAL_BALANCE, "TOTAL_BALANCE");
            this.TOTAL_BALANCE.Name = "TOTAL_BALANCE";
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.dataGridViewTextBoxColumn9.DataPropertyName = "CURRENCY_SING";
            this.dataGridViewTextBoxColumn9.FillWeight = 30F;
            resources.ApplyResources(this.dataGridViewTextBoxColumn9, "dataGridViewTextBoxColumn9");
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // dgvInvoiceList
            // 
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvInvoiceList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle10;
            this.dgvInvoiceList.BackgroundColor = System.Drawing.Color.White;
            this.dgvInvoiceList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvInvoiceList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvInvoiceList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInvoiceList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.INVOICE_ID,
            this.CURRENCY_ID,
            this.INVOICE_NO,
            this.INVOICE_TITLE,
            this.INVOICE_DATE,
            this.SETTLE_AMOUNT,
            this.PAID_AMOUNT,
            this.DUE_AMOUNT,
            this.CURRENCY_SING_,
            this.DUE_DATE,
            this.DUE_DAY});
            this.dgvInvoiceList.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgvInvoiceList, "dgvInvoiceList");
            this.dgvInvoiceList.Name = "dgvInvoiceList";
            this.dgvInvoiceList.RowHeadersVisible = false;
            this.dgvInvoiceList.RowTemplate.Height = 25;
            // 
            // INVOICE_ID
            // 
            this.INVOICE_ID.DataPropertyName = "INVOICE_ID";
            resources.ApplyResources(this.INVOICE_ID, "INVOICE_ID");
            this.INVOICE_ID.Name = "INVOICE_ID";
            // 
            // CURRENCY_ID
            // 
            this.CURRENCY_ID.DataPropertyName = "CURRENCY_ID";
            resources.ApplyResources(this.CURRENCY_ID, "CURRENCY_ID");
            this.CURRENCY_ID.Name = "CURRENCY_ID";
            // 
            // INVOICE_NO
            // 
            this.INVOICE_NO.DataPropertyName = "INVOICE_NO";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.Format = "dd-MM-yyyy";
            this.INVOICE_NO.DefaultCellStyle = dataGridViewCellStyle11;
            resources.ApplyResources(this.INVOICE_NO, "INVOICE_NO");
            this.INVOICE_NO.Name = "INVOICE_NO";
            // 
            // INVOICE_TITLE
            // 
            this.INVOICE_TITLE.DataPropertyName = "INVOICE_TITLE";
            resources.ApplyResources(this.INVOICE_TITLE, "INVOICE_TITLE");
            this.INVOICE_TITLE.Name = "INVOICE_TITLE";
            // 
            // INVOICE_DATE
            // 
            this.INVOICE_DATE.DataPropertyName = "INVOICE_DATE";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.Format = "dd-MM-yyyy";
            this.INVOICE_DATE.DefaultCellStyle = dataGridViewCellStyle12;
            resources.ApplyResources(this.INVOICE_DATE, "INVOICE_DATE");
            this.INVOICE_DATE.Name = "INVOICE_DATE";
            // 
            // SETTLE_AMOUNT
            // 
            this.SETTLE_AMOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.SETTLE_AMOUNT.DataPropertyName = "SETTLE_AMOUNT";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle13.Format = "#,##0.####";
            dataGridViewCellStyle13.NullValue = null;
            this.SETTLE_AMOUNT.DefaultCellStyle = dataGridViewCellStyle13;
            resources.ApplyResources(this.SETTLE_AMOUNT, "SETTLE_AMOUNT");
            this.SETTLE_AMOUNT.Name = "SETTLE_AMOUNT";
            // 
            // PAID_AMOUNT
            // 
            this.PAID_AMOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.PAID_AMOUNT.DataPropertyName = "PAID_AMOUNT";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle14.Format = "#,##0.####";
            this.PAID_AMOUNT.DefaultCellStyle = dataGridViewCellStyle14;
            resources.ApplyResources(this.PAID_AMOUNT, "PAID_AMOUNT");
            this.PAID_AMOUNT.Name = "PAID_AMOUNT";
            // 
            // DUE_AMOUNT
            // 
            this.DUE_AMOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.DUE_AMOUNT.DataPropertyName = "DUE_AMOUNT";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle15.Format = "#,##0.####";
            dataGridViewCellStyle15.NullValue = null;
            this.DUE_AMOUNT.DefaultCellStyle = dataGridViewCellStyle15;
            resources.ApplyResources(this.DUE_AMOUNT, "DUE_AMOUNT");
            this.DUE_AMOUNT.Name = "DUE_AMOUNT";
            // 
            // CURRENCY_SING_
            // 
            this.CURRENCY_SING_.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.CURRENCY_SING_.DataPropertyName = "CURRENCY_SING";
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.CURRENCY_SING_.DefaultCellStyle = dataGridViewCellStyle16;
            this.CURRENCY_SING_.FillWeight = 30F;
            resources.ApplyResources(this.CURRENCY_SING_, "CURRENCY_SING_");
            this.CURRENCY_SING_.Name = "CURRENCY_SING_";
            // 
            // DUE_DATE
            // 
            this.DUE_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.DUE_DATE.DataPropertyName = "DUE_DATE";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.Format = "dd-MM-yyyy";
            this.DUE_DATE.DefaultCellStyle = dataGridViewCellStyle17;
            resources.ApplyResources(this.DUE_DATE, "DUE_DATE");
            this.DUE_DATE.Name = "DUE_DATE";
            // 
            // DUE_DAY
            // 
            this.DUE_DAY.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.DUE_DAY.DataPropertyName = "DUE_DAY";
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DUE_DAY.DefaultCellStyle = dataGridViewCellStyle18;
            resources.ApplyResources(this.DUE_DAY, "DUE_DAY");
            this.DUE_DAY.Name = "DUE_DAY";
            // 
            // btnADD_SERVICE
            // 
            resources.ApplyResources(this.btnADD_SERVICE, "btnADD_SERVICE");
            this.btnADD_SERVICE.Name = "btnADD_SERVICE";
            this.btnADD_SERVICE.UseVisualStyleBackColor = true;
            this.btnADD_SERVICE.Click += new System.EventHandler(this.btnCharge_Click);
            // 
            // btnAPPLY_PAYMENT_1
            // 
            resources.ApplyResources(this.btnAPPLY_PAYMENT_1, "btnAPPLY_PAYMENT_1");
            this.btnAPPLY_PAYMENT_1.Name = "btnAPPLY_PAYMENT_1";
            this.btnAPPLY_PAYMENT_1.UseVisualStyleBackColor = true;
            this.btnAPPLY_PAYMENT_1.Click += new System.EventHandler(this.btnPay_Click);
            // 
            // cboInvoiceStatus
            // 
            this.cboInvoiceStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboInvoiceStatus.FormattingEnabled = true;
            resources.ApplyResources(this.cboInvoiceStatus, "cboInvoiceStatus");
            this.cboInvoiceStatus.Name = "cboInvoiceStatus";
            this.cboInvoiceStatus.SelectedIndexChanged += new System.EventHandler(this.cboInvoiceStatus_SelectedIndexChanged);
            // 
            // dtpEndDate
            // 
            resources.ApplyResources(this.dtpEndDate, "dtpEndDate");
            this.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.ValueChanged += new System.EventHandler(this.cboInvoiceStatus_SelectedIndexChanged);
            // 
            // lblSTART_DATE
            // 
            resources.ApplyResources(this.lblSTART_DATE, "lblSTART_DATE");
            this.lblSTART_DATE.Name = "lblSTART_DATE";
            // 
            // dtpStartDate
            // 
            this.dtpStartDate.Checked = false;
            resources.ApplyResources(this.dtpStartDate, "dtpStartDate");
            this.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.ShowCheckBox = true;
            this.dtpStartDate.ValueChanged += new System.EventHandler(this.cboInvoiceStatus_SelectedIndexChanged);
            // 
            // lblEND_DATE_
            // 
            resources.ApplyResources(this.lblEND_DATE_, "lblEND_DATE_");
            this.lblEND_DATE_.Name = "lblEND_DATE_";
            // 
            // tabBUY_POWER_MONTHLY
            // 
            this.tabBUY_POWER_MONTHLY.Controls.Add(this.btnPRINT_RECEIPT);
            this.tabBUY_POWER_MONTHLY.Controls.Add(this.txtTotalPurchasePower);
            this.tabBUY_POWER_MONTHLY.Controls.Add(this.lblTOTAL_BUY_POWER);
            this.tabBUY_POWER_MONTHLY.Controls.Add(this.cboIsVoidPurchase);
            this.tabBUY_POWER_MONTHLY.Controls.Add(this.dgvCusPurchase);
            this.tabBUY_POWER_MONTHLY.Controls.Add(this.lblINVOICE_DATE);
            this.tabBUY_POWER_MONTHLY.Controls.Add(this.dtPurchaseMonth);
            resources.ApplyResources(this.tabBUY_POWER_MONTHLY, "tabBUY_POWER_MONTHLY");
            this.tabBUY_POWER_MONTHLY.Name = "tabBUY_POWER_MONTHLY";
            this.tabBUY_POWER_MONTHLY.UseVisualStyleBackColor = true;
            // 
            // btnPRINT_RECEIPT
            // 
            this.btnPRINT_RECEIPT.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            resources.ApplyResources(this.btnPRINT_RECEIPT, "btnPRINT_RECEIPT");
            this.btnPRINT_RECEIPT.Name = "btnPRINT_RECEIPT";
            this.btnPRINT_RECEIPT.TabStop = true;
            this.btnPRINT_RECEIPT.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblPrintReciptPurchasePower_LinkClicked);
            // 
            // txtTotalPurchasePower
            // 
            resources.ApplyResources(this.txtTotalPurchasePower, "txtTotalPurchasePower");
            this.txtTotalPurchasePower.Name = "txtTotalPurchasePower";
            this.txtTotalPurchasePower.ReadOnly = true;
            // 
            // lblTOTAL_BUY_POWER
            // 
            resources.ApplyResources(this.lblTOTAL_BUY_POWER, "lblTOTAL_BUY_POWER");
            this.lblTOTAL_BUY_POWER.Name = "lblTOTAL_BUY_POWER";
            // 
            // cboIsVoidPurchase
            // 
            this.cboIsVoidPurchase.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboIsVoidPurchase.FormattingEnabled = true;
            resources.ApplyResources(this.cboIsVoidPurchase, "cboIsVoidPurchase");
            this.cboIsVoidPurchase.Name = "cboIsVoidPurchase";
            this.cboIsVoidPurchase.SelectedIndexChanged += new System.EventHandler(this.cboIsVoidPurchase_SelectedIndexChanged);
            // 
            // dgvCusPurchase
            // 
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvCusPurchase.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle19;
            this.dgvCusPurchase.BackgroundColor = System.Drawing.Color.White;
            this.dgvCusPurchase.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvCusPurchase.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvCusPurchase.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCusPurchase.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CUSTOMER_BUY_ID,
            this.PREPAID_CUS_ID,
            this.BUY_TIMES,
            this.CREATE_ON,
            this.BUY_POWER,
            this.POWER_COMPENSATED,
            this.DISCOUNT,
            this.PRICE,
            this.AMOUNT_SUBSIDY,
            this.TOTAL_AMOUNT,
            this.BUY_CURRENCY_SING_,
            this.IS_VOID});
            this.dgvCusPurchase.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgvCusPurchase, "dgvCusPurchase");
            this.dgvCusPurchase.Name = "dgvCusPurchase";
            this.dgvCusPurchase.RowHeadersVisible = false;
            // 
            // CUSTOMER_BUY_ID
            // 
            this.CUSTOMER_BUY_ID.DataPropertyName = "CUSTOMER_BUY_ID";
            resources.ApplyResources(this.CUSTOMER_BUY_ID, "CUSTOMER_BUY_ID");
            this.CUSTOMER_BUY_ID.Name = "CUSTOMER_BUY_ID";
            // 
            // PREPAID_CUS_ID
            // 
            this.PREPAID_CUS_ID.DataPropertyName = "PREPAID_CUS_ID";
            resources.ApplyResources(this.PREPAID_CUS_ID, "PREPAID_CUS_ID");
            this.PREPAID_CUS_ID.Name = "PREPAID_CUS_ID";
            // 
            // BUY_TIMES
            // 
            this.BUY_TIMES.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.BUY_TIMES.DataPropertyName = "BUY_TIMES";
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle20.Format = "N0";
            dataGridViewCellStyle20.NullValue = null;
            this.BUY_TIMES.DefaultCellStyle = dataGridViewCellStyle20;
            resources.ApplyResources(this.BUY_TIMES, "BUY_TIMES");
            this.BUY_TIMES.Name = "BUY_TIMES";
            // 
            // CREATE_ON
            // 
            this.CREATE_ON.DataPropertyName = "CREATE_ON";
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle21.Format = "dd-MM-yyyy";
            this.CREATE_ON.DefaultCellStyle = dataGridViewCellStyle21;
            resources.ApplyResources(this.CREATE_ON, "CREATE_ON");
            this.CREATE_ON.Name = "CREATE_ON";
            // 
            // BUY_POWER
            // 
            this.BUY_POWER.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.BUY_POWER.DataPropertyName = "BUY_QTY";
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle22.Format = "N0";
            dataGridViewCellStyle22.NullValue = null;
            this.BUY_POWER.DefaultCellStyle = dataGridViewCellStyle22;
            this.BUY_POWER.FillWeight = 120F;
            resources.ApplyResources(this.BUY_POWER, "BUY_POWER");
            this.BUY_POWER.Name = "BUY_POWER";
            // 
            // POWER_COMPENSATED
            // 
            this.POWER_COMPENSATED.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.POWER_COMPENSATED.DataPropertyName = "COMPENSATED";
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle23.Format = "N0";
            dataGridViewCellStyle23.NullValue = null;
            this.POWER_COMPENSATED.DefaultCellStyle = dataGridViewCellStyle23;
            this.POWER_COMPENSATED.FillWeight = 120F;
            resources.ApplyResources(this.POWER_COMPENSATED, "POWER_COMPENSATED");
            this.POWER_COMPENSATED.Name = "POWER_COMPENSATED";
            // 
            // DISCOUNT
            // 
            this.DISCOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.DISCOUNT.DataPropertyName = "DISCOUNT_QTY";
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle24.Format = "N0";
            dataGridViewCellStyle24.NullValue = null;
            this.DISCOUNT.DefaultCellStyle = dataGridViewCellStyle24;
            resources.ApplyResources(this.DISCOUNT, "DISCOUNT");
            this.DISCOUNT.Name = "DISCOUNT";
            // 
            // PRICE
            // 
            this.PRICE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.PRICE.DataPropertyName = "PRICE";
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle25.Format = "N0";
            dataGridViewCellStyle25.NullValue = null;
            this.PRICE.DefaultCellStyle = dataGridViewCellStyle25;
            this.PRICE.FillWeight = 130F;
            resources.ApplyResources(this.PRICE, "PRICE");
            this.PRICE.Name = "PRICE";
            // 
            // AMOUNT_SUBSIDY
            // 
            this.AMOUNT_SUBSIDY.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.AMOUNT_SUBSIDY.DataPropertyName = "PAID_CREDIT_AMOUNT";
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle26.Format = "N0";
            this.AMOUNT_SUBSIDY.DefaultCellStyle = dataGridViewCellStyle26;
            resources.ApplyResources(this.AMOUNT_SUBSIDY, "AMOUNT_SUBSIDY");
            this.AMOUNT_SUBSIDY.Name = "AMOUNT_SUBSIDY";
            // 
            // TOTAL_AMOUNT
            // 
            this.TOTAL_AMOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TOTAL_AMOUNT.DataPropertyName = "BUY_AMOUNT";
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle27.Format = "N0";
            this.TOTAL_AMOUNT.DefaultCellStyle = dataGridViewCellStyle27;
            resources.ApplyResources(this.TOTAL_AMOUNT, "TOTAL_AMOUNT");
            this.TOTAL_AMOUNT.Name = "TOTAL_AMOUNT";
            // 
            // BUY_CURRENCY_SING_
            // 
            this.BUY_CURRENCY_SING_.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.BUY_CURRENCY_SING_.DataPropertyName = "CURRENCY_SING";
            resources.ApplyResources(this.BUY_CURRENCY_SING_, "BUY_CURRENCY_SING_");
            this.BUY_CURRENCY_SING_.Name = "BUY_CURRENCY_SING_";
            // 
            // IS_VOID
            // 
            this.IS_VOID.DataPropertyName = "IS_VOID";
            resources.ApplyResources(this.IS_VOID, "IS_VOID");
            this.IS_VOID.Name = "IS_VOID";
            // 
            // lblINVOICE_DATE
            // 
            resources.ApplyResources(this.lblINVOICE_DATE, "lblINVOICE_DATE");
            this.lblINVOICE_DATE.Name = "lblINVOICE_DATE";
            // 
            // dtPurchaseMonth
            // 
            resources.ApplyResources(this.dtPurchaseMonth, "dtPurchaseMonth");
            this.dtPurchaseMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtPurchaseMonth.Name = "dtPurchaseMonth";
            this.dtPurchaseMonth.ValueChanged += new System.EventHandler(this.dtPurchaseMonth_ValueChanged);
            // 
            // tabPREPAID_USAGE
            // 
            this.tabPREPAID_USAGE.Controls.Add(this.dtpYear);
            this.tabPREPAID_USAGE.Controls.Add(this.btnADJUST_USAGE_1);
            this.tabPREPAID_USAGE.Controls.Add(this.txtTotalRemainCreditCurrencySign);
            this.tabPREPAID_USAGE.Controls.Add(this.txtTotalRemainCredit);
            this.tabPREPAID_USAGE.Controls.Add(this.lblLAST_SUBSIDY_BALANCE);
            this.tabPREPAID_USAGE.Controls.Add(this.dgvPrepaidUsage);
            this.tabPREPAID_USAGE.Controls.Add(this.lblINVOICE_DATE_1);
            resources.ApplyResources(this.tabPREPAID_USAGE, "tabPREPAID_USAGE");
            this.tabPREPAID_USAGE.Name = "tabPREPAID_USAGE";
            this.tabPREPAID_USAGE.UseVisualStyleBackColor = true;
            // 
            // dtpYear
            // 
            this.dtpYear.Checked = false;
            resources.ApplyResources(this.dtpYear, "dtpYear");
            this.dtpYear.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpYear.Name = "dtpYear";
            this.dtpYear.ShowCheckBox = true;
            this.dtpYear.ValueChanged += new System.EventHandler(this.dtpYear_ValueChanged);
            // 
            // btnADJUST_USAGE_1
            // 
            resources.ApplyResources(this.btnADJUST_USAGE_1, "btnADJUST_USAGE_1");
            this.btnADJUST_USAGE_1.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnADJUST_USAGE_1.Name = "btnADJUST_USAGE_1";
            this.btnADJUST_USAGE_1.TabStop = true;
            this.btnADJUST_USAGE_1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnPrepaidAdjustment_LinkClicked);
            // 
            // txtTotalRemainCreditCurrencySign
            // 
            resources.ApplyResources(this.txtTotalRemainCreditCurrencySign, "txtTotalRemainCreditCurrencySign");
            this.txtTotalRemainCreditCurrencySign.Name = "txtTotalRemainCreditCurrencySign";
            this.txtTotalRemainCreditCurrencySign.ReadOnly = true;
            // 
            // txtTotalRemainCredit
            // 
            resources.ApplyResources(this.txtTotalRemainCredit, "txtTotalRemainCredit");
            this.txtTotalRemainCredit.Name = "txtTotalRemainCredit";
            this.txtTotalRemainCredit.ReadOnly = true;
            // 
            // lblLAST_SUBSIDY_BALANCE
            // 
            resources.ApplyResources(this.lblLAST_SUBSIDY_BALANCE, "lblLAST_SUBSIDY_BALANCE");
            this.lblLAST_SUBSIDY_BALANCE.Name = "lblLAST_SUBSIDY_BALANCE";
            // 
            // dgvPrepaidUsage
            // 
            dataGridViewCellStyle28.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvPrepaidUsage.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle28;
            this.dgvPrepaidUsage.BackgroundColor = System.Drawing.Color.White;
            this.dgvPrepaidUsage.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvPrepaidUsage.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvPrepaidUsage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPrepaidUsage.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PREPAID_CREDIT_ID,
            this.START_DATE,
            this.END_DATE,
            this.MONTH,
            this.USAGE,
            this.PRICE_CREDIT,
            this.AMOUNT_SUBSIDY_1,
            this.SETTLE_AMOUNT_1,
            this.BALANCE_1,
            this.CREDIT_CURRENCY_SING_});
            this.dgvPrepaidUsage.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgvPrepaidUsage, "dgvPrepaidUsage");
            this.dgvPrepaidUsage.Name = "dgvPrepaidUsage";
            this.dgvPrepaidUsage.RowHeadersVisible = false;
            this.dgvPrepaidUsage.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvPrepaidUsage_CellMouseClick);
            // 
            // lblINVOICE_DATE_1
            // 
            resources.ApplyResources(this.lblINVOICE_DATE_1, "lblINVOICE_DATE_1");
            this.lblINVOICE_DATE_1.Name = "lblINVOICE_DATE_1";
            // 
            // tabBUY_POWER_YEARLY
            // 
            this.tabBUY_POWER_YEARLY.Controls.Add(this.chart1);
            resources.ApplyResources(this.tabBUY_POWER_YEARLY, "tabBUY_POWER_YEARLY");
            this.tabBUY_POWER_YEARLY.Name = "tabBUY_POWER_YEARLY";
            this.tabBUY_POWER_YEARLY.UseVisualStyleBackColor = true;
            // 
            // chart1
            // 
            this.chart1.Application = global::EPower.Properties.Resources.IC_CARD_FORMATED;
            this.chart1.ApplicationDNC = global::EPower.Properties.Resources.IC_CARD_FORMATED;
            this.chart1.Background.Color = System.Drawing.Color.White;
            background1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(235)))), ((int)(((byte)(238)))));
            chartArea1.Background = background1;
            line1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(28)))), ((int)(((byte)(59)))));
            subValue1.Line = line1;
            subValue1.Name = global::EPower.Properties.Resources.IC_CARD_FORMATED;
            element1.DefaultSubValue = subValue1;
            element1.Hotspot = hotspot1;
            legendEntry1.Background = background2;
            line2.Color = System.Drawing.Color.Empty;
            legendEntry1.DividerLine = line2;
            legendEntry1.Hotspot = hotspot2;
            legendEntry1.ShapeType = dotnetCHARTING.WinForms.ShapeType.None;
            element1.LegendEntry = legendEntry1;
            element1.ShapeType = dotnetCHARTING.WinForms.ShapeType.None;
            element1.XAxisTick = null;
            element1.YAxisTick = null;
            chartArea1.DefaultElement = element1;
            line3.Color = System.Drawing.Color.LightGray;
            chartArea1.InteriorLine = line3;
            label1.Font = new System.Drawing.Font("Tahoma", 8F);
            label1.Hotspot = hotspot3;
            label1.Truncation = truncation1;
            chartArea1.Label = label1;
            background3.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(219)))));
            legendBox1.Background = background3;
            legendBox1.CornerBottomRight = dotnetCHARTING.WinForms.BoxCorner.Cut;
            legendEntry2.Background = background4;
            line4.Color = System.Drawing.Color.Empty;
            legendEntry2.DividerLine = line4;
            legendEntry2.Hotspot = hotspot4;
            label6.Hotspot = hotspot5;
            label6.Truncation = truncation2;
            legendEntry2.LabelStyle = label6;
            legendEntry2.ShapeType = dotnetCHARTING.WinForms.ShapeType.None;
            legendBox1.DefaultEntry = legendEntry2;
            legendEntry3.Background = background5;
            line5.Color = System.Drawing.Color.Gray;
            legendEntry3.DividerLine = line5;
            legendEntry3.Hotspot = hotspot6;
            legendEntry3.Name = "Name";
            legendEntry3.ShapeType = dotnetCHARTING.WinForms.ShapeType.None;
            legendEntry3.SortOrder = -1;
            legendEntry3.Value = "Value";
            legendEntry3.Visible = false;
            legendBox1.HeaderEntry = legendEntry3;
            line6.Color = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            legendBox1.InteriorLine = line6;
            legendBox1.LabelStyle = label6;
            line7.Color = System.Drawing.Color.Gray;
            legendBox1.Line = line7;
            legendBox1.Padding = 4;
            legendBox1.Position = dotnetCHARTING.WinForms.LegendBoxPosition.Top;
            legendBox1.Shadow = shadow1;
            legendBox1.Visible = true;
            chartArea1.LegendBox = legendBox1;
            line8.Color = System.Drawing.Color.Gray;
            chartArea1.Line = line8;
            chartArea1.Shadow = shadow2;
            chartArea1.StartDateOfYear = new System.DateTime(((long)(0)));
            background6.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(219)))));
            box1.Background = background6;
            line9.Color = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            box1.InteriorLine = line9;
            label7.Color = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(45)))), ((int)(((byte)(38)))));
            label7.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            label7.Hotspot = hotspot7;
            label7.Truncation = truncation3;
            box1.Label = label7;
            line10.Color = System.Drawing.Color.Gray;
            box1.Line = line10;
            box1.Position = null;
            box1.Shadow = shadow3;
            box1.Visible = true;
            chartArea1.TitleBox = box1;
            line11.Color = System.Drawing.Color.LightGray;
            axisTick1.GridLine = line11;
            line12.Length = 3;
            axisTick1.Line = line12;
            axis1.DefaultTick = axisTick1;
            axis1.GaugeLabelMode = dotnetCHARTING.WinForms.GaugeLabelMode.Default;
            axis1.GaugeNeedleType = dotnetCHARTING.WinForms.GaugeNeedleType.One;
            axis1.LabelMarker = null;
            timeIntervalAdvanced1.Start = new System.DateTime(((long)(0)));
            timeIntervalAdvanced1.TimeSpan = System.TimeSpan.Parse("00:00:00");
            axis1.MinorTimeIntervalAdvanced = timeIntervalAdvanced1;
            axis1.ScaleRange = scaleRange1;
            axis1.SmartScaleBreakLimit = 2;
            axis1.TickLabelSeparatorLine = line13;
            axis1.TimeInterval = dotnetCHARTING.WinForms.TimeInterval.Minutes;
            timeIntervalAdvanced2.Start = new System.DateTime(((long)(0)));
            timeIntervalAdvanced2.TimeSpan = System.TimeSpan.Parse("00:00:00");
            axis1.TimeIntervalAdvanced = timeIntervalAdvanced2;
            line14.Color = System.Drawing.Color.Red;
            axisTick2.GridLine = line14;
            line15.Length = 3;
            axisTick2.Line = line15;
            axis1.ZeroTick = axisTick2;
            chartArea1.XAxis = axis1;
            line16.Color = System.Drawing.Color.LightGray;
            axisTick3.GridLine = line16;
            line17.Length = 3;
            axisTick3.Line = line17;
            axis2.DefaultTick = axisTick3;
            axis2.GaugeLabelMode = dotnetCHARTING.WinForms.GaugeLabelMode.Default;
            axis2.GaugeNeedleType = dotnetCHARTING.WinForms.GaugeNeedleType.One;
            axis2.LabelMarker = null;
            axis2.MinorTimeIntervalAdvanced = timeIntervalAdvanced1;
            axis2.ScaleRange = scaleRange2;
            axis2.SmartScaleBreakLimit = 2;
            axis2.TickLabelSeparatorLine = line18;
            axis2.TimeInterval = dotnetCHARTING.WinForms.TimeInterval.Minutes;
            timeIntervalAdvanced3.Start = new System.DateTime(((long)(0)));
            timeIntervalAdvanced3.TimeSpan = System.TimeSpan.Parse("00:00:00");
            axis2.TimeIntervalAdvanced = timeIntervalAdvanced3;
            line19.Color = System.Drawing.Color.Red;
            axisTick4.GridLine = line19;
            line20.Length = 3;
            axisTick4.Line = line20;
            axis2.ZeroTick = axisTick4;
            chartArea1.YAxis = axis2;
            this.chart1.ChartArea = chartArea1;
            this.chart1.DataGrid = null;
            line21.Color = System.Drawing.Color.LightGray;
            axisTick5.GridLine = line21;
            line22.Length = 3;
            axisTick5.Line = line22;
            axis3.DefaultTick = axisTick5;
            axis3.GaugeLabelMode = dotnetCHARTING.WinForms.GaugeLabelMode.Default;
            axis3.GaugeNeedleType = dotnetCHARTING.WinForms.GaugeNeedleType.One;
            axis3.LabelMarker = null;
            axis3.MinorTimeIntervalAdvanced = timeIntervalAdvanced1;
            axis3.ScaleRange = scaleRange3;
            axis3.SmartScaleBreakLimit = 2;
            axis3.TickLabelSeparatorLine = line23;
            axis3.TimeInterval = dotnetCHARTING.WinForms.TimeInterval.Minutes;
            timeIntervalAdvanced4.Start = new System.DateTime(((long)(0)));
            timeIntervalAdvanced4.TimeSpan = System.TimeSpan.Parse("00:00:00");
            axis3.TimeIntervalAdvanced = timeIntervalAdvanced4;
            line24.Color = System.Drawing.Color.Red;
            axisTick6.GridLine = line24;
            line25.Length = 3;
            axisTick6.Line = line25;
            axis3.ZeroTick = axisTick6;
            this.chart1.DefaultAxis = axis3;
            this.chart1.DefaultCultureName = "";
            line26.Color = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(28)))), ((int)(((byte)(59)))));
            subValue2.Line = line26;
            subValue2.Name = global::EPower.Properties.Resources.IC_CARD_FORMATED;
            element2.DefaultSubValue = subValue2;
            hotspot8.ToolTip = global::EPower.Properties.Resources.IC_CARD_FORMATED;
            element2.Hotspot = hotspot8;
            legendEntry4.Background = background7;
            line27.Color = System.Drawing.Color.Empty;
            legendEntry4.DividerLine = line27;
            legendEntry4.Hotspot = hotspot9;
            legendEntry4.ShapeType = dotnetCHARTING.WinForms.ShapeType.None;
            element2.LegendEntry = legendEntry4;
            element2.ShapeType = dotnetCHARTING.WinForms.ShapeType.None;
            smartLabel1.Color = System.Drawing.Color.Empty;
            smartLabel1.Hotspot = hotspot10;
            smartLabel1.Line = line28;
            smartLabel1.Truncation = truncation4;
            element2.SmartLabel = smartLabel1;
            element2.ToolTip = global::EPower.Properties.Resources.IC_CARD_FORMATED;
            element2.XAxisTick = null;
            element2.YAxisTick = null;
            this.chart1.DefaultElement = element2;
            this.chart1.DefaultShadow = shadow4;
            this.chart1.FunnelNozzleWidthPercentage = 35;
            resources.ApplyResources(this.chart1, "chart1");
            this.chart1.Name = "chart1";
            this.chart1.NoDataLabel.Hotspot = hotspot11;
            this.chart1.NoDataLabel.Text = "No Data";
            this.chart1.NoDataLabel.Truncation = truncation5;
            this.chart1.Palette = new System.Drawing.Color[] {
        System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(154)))), ((int)(((byte)(255))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(48)))), ((int)(((byte)(99))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(206))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(255)))), ((int)(((byte)(255))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(0)))), ((int)(((byte)(99))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(130)))), ((int)(((byte)(132))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(101)))), ((int)(((byte)(206))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(207)))), ((int)(((byte)(255))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(132))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(255))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(0))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(0)))), ((int)(((byte)(132))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(0)))), ((int)(((byte)(0))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(130)))), ((int)(((byte)(132))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(207)))), ((int)(((byte)(255))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(255)))), ((int)(((byte)(255))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(255)))), ((int)(((byte)(206))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(156))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(207)))), ((int)(((byte)(255))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(154)))), ((int)(((byte)(206))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(154)))), ((int)(((byte)(255))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(207)))), ((int)(((byte)(156))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(101)))), ((int)(((byte)(255))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(207)))), ((int)(((byte)(206))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(207)))), ((int)(((byte)(0))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(207)))), ((int)(((byte)(0))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(154)))), ((int)(((byte)(0))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(101)))), ((int)(((byte)(0))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(101)))), ((int)(((byte)(156))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(150)))), ((int)(((byte)(148))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(48)))), ((int)(((byte)(99))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(154)))), ((int)(((byte)(99))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(48)))), ((int)(((byte)(0))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(48)))), ((int)(((byte)(0))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(48)))), ((int)(((byte)(0))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(48)))), ((int)(((byte)(99))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(48)))), ((int)(((byte)(156))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(48)))), ((int)(((byte)(49))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(0))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(0))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(255))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(0)))), ((int)(((byte)(0))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(130)))), ((int)(((byte)(0))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(132))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(130)))), ((int)(((byte)(0))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(0)))), ((int)(((byte)(132))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(130)))), ((int)(((byte)(132))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(195)))), ((int)(((byte)(198))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(130)))), ((int)(((byte)(132)))))};
            this.chart1.SmartLabelLine = line28;
            this.chart1.SpacingPercentageNested = 4;
            this.chart1.StartDateOfYear = new System.DateTime(((long)(0)));
            this.chart1.TempDirectory = "C:\\Users\\dev\\AppData\\Local\\Temp\\";
            this.chart1.ToolTipTemplate = global::EPower.Properties.Resources.IC_CARD_FORMATED;
            background8.Color = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(235)))), ((int)(((byte)(238)))));
            chartArea2.Background = background8;
            subValue3.Line = line29;
            subValue3.Name = global::EPower.Properties.Resources.IC_CARD_FORMATED;
            element3.DefaultSubValue = subValue3;
            element3.Hotspot = hotspot12;
            legendEntry5.Background = background9;
            line30.Color = System.Drawing.Color.Empty;
            legendEntry5.DividerLine = line30;
            legendEntry5.Hotspot = hotspot13;
            legendEntry5.ShapeType = dotnetCHARTING.WinForms.ShapeType.None;
            element3.LegendEntry = legendEntry5;
            element3.ShapeType = dotnetCHARTING.WinForms.ShapeType.None;
            element3.XAxisTick = null;
            element3.YAxisTick = null;
            chartArea2.DefaultElement = element3;
            line31.Color = System.Drawing.Color.LightGray;
            chartArea2.InteriorLine = line31;
            label8.Font = new System.Drawing.Font("Tahoma", 8F);
            label8.Hotspot = hotspot14;
            label8.Truncation = truncation6;
            chartArea2.Label = label8;
            chartArea2.LegendBox = null;
            line32.Color = System.Drawing.Color.Gray;
            chartArea2.Line = line32;
            chartArea2.Shadow = shadow5;
            chartArea2.StartDateOfYear = new System.DateTime(((long)(0)));
            background10.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(219)))));
            box2.Background = background10;
            line33.Color = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            box2.InteriorLine = line33;
            label9.Color = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(45)))), ((int)(((byte)(38)))));
            label9.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            label9.Hotspot = hotspot15;
            label9.Truncation = truncation7;
            box2.Label = label9;
            line34.Color = System.Drawing.Color.Gray;
            box2.Line = line34;
            box2.Position = null;
            box2.Shadow = shadow6;
            box2.Visible = true;
            chartArea2.TitleBox = box2;
            chartArea2.XAxis = axis1;
            line35.Color = System.Drawing.Color.LightGray;
            axisTick7.GridLine = line35;
            line36.Length = 3;
            axisTick7.Line = line36;
            axis4.DefaultTick = axisTick7;
            axis4.GaugeLabelMode = dotnetCHARTING.WinForms.GaugeLabelMode.Default;
            axis4.GaugeNeedleType = dotnetCHARTING.WinForms.GaugeNeedleType.One;
            axis4.LabelMarker = null;
            timeIntervalAdvanced5.Start = new System.DateTime(((long)(0)));
            timeIntervalAdvanced5.TimeSpan = System.TimeSpan.Parse("00:00:00");
            axis4.MinorTimeIntervalAdvanced = timeIntervalAdvanced5;
            axis4.ScaleRange = scaleRange4;
            axis4.SmartScaleBreakLimit = 2;
            axis4.TickLabelSeparatorLine = line37;
            axis4.TimeInterval = dotnetCHARTING.WinForms.TimeInterval.Minutes;
            timeIntervalAdvanced6.Start = new System.DateTime(((long)(0)));
            timeIntervalAdvanced6.TimeSpan = System.TimeSpan.Parse("00:00:00");
            axis4.TimeIntervalAdvanced = timeIntervalAdvanced6;
            line38.Color = System.Drawing.Color.Red;
            axisTick8.GridLine = line38;
            line39.Length = 3;
            axisTick8.Line = line39;
            axis4.ZeroTick = axisTick8;
            chartArea2.YAxis = axis4;
            this.chart1.VolumeArea = chartArea2;
            // 
            // btnActivate
            // 
            resources.ApplyResources(this.btnActivate, "btnActivate");
            this.btnActivate.Name = "btnActivate";
            this.btnActivate.UseVisualStyleBackColor = true;
            this.btnActivate.Click += new System.EventHandler(this.btnActivate_Click);
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // cmsStrip
            // 
            resources.ApplyResources(this.cmsStrip, "cmsStrip");
            this.cmsStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cmsStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ChangeConnectionType});
            this.cmsStrip.Name = "cmsStrip";
            this.cmsStrip.ShowCheckMargin = true;
            // 
            // ChangeConnectionType
            // 
            this.ChangeConnectionType.Name = "ChangeConnectionType";
            resources.ApplyResources(this.ChangeConnectionType, "ChangeConnectionType");
            this.ChangeConnectionType.Click += new System.EventHandler(this.ChangeConnectionType_Click);
            // 
            // PREPAID_CREDIT_ID
            // 
            this.PREPAID_CREDIT_ID.DataPropertyName = "PREPAID_CREDIT_ID";
            resources.ApplyResources(this.PREPAID_CREDIT_ID, "PREPAID_CREDIT_ID");
            this.PREPAID_CREDIT_ID.Name = "PREPAID_CREDIT_ID";
            // 
            // START_DATE
            // 
            this.START_DATE.DataPropertyName = "START_DATE";
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle29.Format = "dd-MM-yyyy";
            this.START_DATE.DefaultCellStyle = dataGridViewCellStyle29;
            resources.ApplyResources(this.START_DATE, "START_DATE");
            this.START_DATE.Name = "START_DATE";
            // 
            // END_DATE
            // 
            this.END_DATE.DataPropertyName = "END_DATE";
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle30.Format = "dd-MM-yyyy";
            dataGridViewCellStyle30.NullValue = null;
            this.END_DATE.DefaultCellStyle = dataGridViewCellStyle30;
            resources.ApplyResources(this.END_DATE, "END_DATE");
            this.END_DATE.Name = "END_DATE";
            // 
            // MONTH
            // 
            this.MONTH.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.MONTH.DataPropertyName = "CREDIT_MONTH";
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomCenter;
            dataGridViewCellStyle31.Format = "MM-yyyy";
            this.MONTH.DefaultCellStyle = dataGridViewCellStyle31;
            resources.ApplyResources(this.MONTH, "MONTH");
            this.MONTH.Name = "MONTH";
            // 
            // USAGE
            // 
            this.USAGE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.USAGE.DataPropertyName = "USAGE";
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle32.Format = "N0";
            this.USAGE.DefaultCellStyle = dataGridViewCellStyle32;
            resources.ApplyResources(this.USAGE, "USAGE");
            this.USAGE.Name = "USAGE";
            // 
            // PRICE_CREDIT
            // 
            this.PRICE_CREDIT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PRICE_CREDIT.DataPropertyName = "PRICE_CREDIT";
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle33.Format = "N0";
            dataGridViewCellStyle33.NullValue = null;
            this.PRICE_CREDIT.DefaultCellStyle = dataGridViewCellStyle33;
            this.PRICE_CREDIT.FillWeight = 120F;
            resources.ApplyResources(this.PRICE_CREDIT, "PRICE_CREDIT");
            this.PRICE_CREDIT.Name = "PRICE_CREDIT";
            // 
            // AMOUNT_SUBSIDY_1
            // 
            this.AMOUNT_SUBSIDY_1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.AMOUNT_SUBSIDY_1.DataPropertyName = "CREDIT_AMOUNT";
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle34.Format = "N0";
            dataGridViewCellStyle34.NullValue = null;
            this.AMOUNT_SUBSIDY_1.DefaultCellStyle = dataGridViewCellStyle34;
            this.AMOUNT_SUBSIDY_1.FillWeight = 120F;
            resources.ApplyResources(this.AMOUNT_SUBSIDY_1, "AMOUNT_SUBSIDY_1");
            this.AMOUNT_SUBSIDY_1.Name = "AMOUNT_SUBSIDY_1";
            // 
            // SETTLE_AMOUNT_1
            // 
            this.SETTLE_AMOUNT_1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.SETTLE_AMOUNT_1.DataPropertyName = "CREDIT_PAID_AMOUNT";
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle35.Format = "N0";
            dataGridViewCellStyle35.NullValue = null;
            this.SETTLE_AMOUNT_1.DefaultCellStyle = dataGridViewCellStyle35;
            resources.ApplyResources(this.SETTLE_AMOUNT_1, "SETTLE_AMOUNT_1");
            this.SETTLE_AMOUNT_1.Name = "SETTLE_AMOUNT_1";
            // 
            // BALANCE_1
            // 
            this.BALANCE_1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.BALANCE_1.DataPropertyName = "CREDIT_BALANCE";
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle36.Format = "N0";
            this.BALANCE_1.DefaultCellStyle = dataGridViewCellStyle36;
            resources.ApplyResources(this.BALANCE_1, "BALANCE_1");
            this.BALANCE_1.Name = "BALANCE_1";
            // 
            // CREDIT_CURRENCY_SING_
            // 
            this.CREDIT_CURRENCY_SING_.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.CREDIT_CURRENCY_SING_.DataPropertyName = "CREDIT_CURRENCY_SING";
            resources.ApplyResources(this.CREDIT_CURRENCY_SING_, "CREDIT_CURRENCY_SING_");
            this.CREDIT_CURRENCY_SING_.Name = "CREDIT_CURRENCY_SING_";
            // 
            // DialogCustomer
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogCustomer";
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPhoto)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabGENERAL_INFORMATION.ResumeLayout(false);
            this.tabGENERAL_INFORMATION.PerformLayout();
            this.tabUSAGE_INFORMATION.ResumeLayout(false);
            this.tabUSAGE_INFORMATION.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEquitment)).EndInit();
            this.tabDEPOSIT.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBalanceDeposit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeposit)).EndInit();
            this.tabBILLING_AND_METER.ResumeLayout(false);
            this.tabBILLING_AND_METER.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCutoffDays)).EndInit();
            this.tabINVOICE.ResumeLayout(false);
            this.tabINVOICE.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTotalBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInvoiceList)).EndInit();
            this.tabBUY_POWER_MONTHLY.ResumeLayout(false);
            this.tabBUY_POWER_MONTHLY.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCusPurchase)).EndInit();
            this.tabPREPAID_USAGE.ResumeLayout(false);
            this.tabPREPAID_USAGE.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrepaidUsage)).EndInit();
            this.tabBUY_POWER_YEARLY.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.cmsStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnSAVE;
        private ComboBox cboArea;
        private Label lblAREA;
        private TextBox txtPlaceofBirth;
        private Label lblPLACE_OF_BIRTH;
        private TextBox txtCustomerCode;
        private Label lblCUSTOMER_CODE;
        private PictureBox picPhoto;
        private DateTimePicker dtpDOB;
        private Label lblDATE_OF_BIRTH;
        private Label lblADDRESS_INFORMATION;
        private TextBox txtAddress;
        private Label lblADDRESS;
        private ComboBox cboVillage;
        private ComboBox cboCommune;
        private ComboBox cboDistrict;
        private ComboBox cboProvince;
        private Label lblVILLAGE;
        private Label lblCOMMUNE;
        private Label lblDISTRICT;
        private Label lblPROVINCE;
        private TextBox txtPhone2;
        private TextBox txtPhone1;
        private TextBox txtCompanyName;
        private TextBox txtIdNumber;
        private ComboBox cboSex;
        private TextBox txtLastNameKH;
        private TextBox txtFirstNameKH;
        private Label lblCOMPANY_NAME;
        private Label lblID_CARD_NUMBER;
        private Label lblPHONE_II;
        private Label lblPHONE_I;
        private Label lblSEX;
        private Label lblLAST_NAME_KH;
        private Label lblFIRST_NAME_KH;
        private ExButton btnCLOSE;
        private Label lblSTREET_NO;
        private Label lblHOUSE_NO;
        private TextBox txtStreetNo;
        private TextBox txtHouseNo;
        private Label lblLAST_NAME;
        private TextBox txtLastName;
        private Label lblFIRST_NAME;
        private TextBox txtFirstName;
        private ExLinkLabel btnPHOTO;
        private Label label28;
        private Label label27;
        private Label label25;
        private Label label39;
        private Label label38;
        private Label label37;
        private Label label36;
        private Label label34;
        private TextBox txtJob;
        private Label lblJOB;
        private TextBox txtCheckDigit;
        private TabControl tabControl1;
        private TabPage tabGENERAL_INFORMATION;
        private TabPage tabUSAGE_INFORMATION;
        private Label label33;
        private Label label32;
        private Label label31;
        private Label label30;
        private Label lblCUSTOMER_TYPE_AND_AMPERE;
        private Label lblPHASE;
        private Label lblVOLTAGE;
        private Label lblEQUIPMENT;
        private ComboBox cboPhase;
        private ComboBox cboAMP;
        private ComboBox cboVol;
        private ExLinkLabel btnEDIT;
        private ExLinkLabel btnREMOVE;
        private ExLinkLabel btnADD;
        private TextBox txtTotalPower;
        private Label lblTOTAL_WATT;
        private DataGridView dgvEquitment;
        private TabPage tabBILLING_AND_METER;
        private Label lblCONSTANT;
        private TextBox txtMeterConstant;
        private TextBox txtMeterType;
        private Label label50;
        private Label lblAMPARE;
        private Label lblVOLTAGE_1;
        private Label lblPHASE_1;
        private Label lblCABLE_SHIELD;
        private Label lblSHIELD;
        private TextBox txtMeterAmp;
        private TextBox txtMeterVol;
        private TextBox txtMeterPhase;
        private Label lblMETER_CODE;
        private Label lblMETER_INSTALLATION;
        private Label lblMETER_TYPE;
        private ComboBox cboCableShield;
        private ComboBox cboMeterShield;
        private TextBox txtBreakerType;
        private Label label53;
        private Label lblBREAKER_INSTALLATION;
        private Label lblAMPARE_2;
        private Label lblVOLTAGE_2;
        private Label lblPHASE_2;
        private TextBox txtBreakerAmp;
        private TextBox txtBreakerVol;
        private TextBox txtBreakerPhase;
        private Label lblBREAKER_CODE;
        private Label lblBREAKER_TYPE;
        private Label lblCONSTANT_2;
        private TextBox txtBreakerConstant;
        private TextBox txtPoleCode;
        private Label lblPOLE_AND_BOX_INSTALLATION;
        private TextBox txtCutter;
        private TextBox txtBiller;
        private TextBox txtCollector;
        private Label label71;
        private Label lblCUTTER;
        private Label lblBILLER;
        private Label lblCOLLECTOR;
        private Label lblBOX_CODE;
        private Label lblPOLE_CODE;
        private TabPage tabINVOICE;
        private ComboBox cboInvoiceStatus;
        private DateTimePicker dtpEndDate;
        private Label lblSTART_DATE;
        private DateTimePicker dtpStartDate;
        private Label lblEND_DATE_;
        private ExButton btnActivate;
        private ExTextbox txtBox;
        private ExTextbox txtBreaker;
        private ExTextbox txtMeter;
        private ExLinkLabel btnCHANGE_BOX;
        private ExLinkLabel btnCHANGE_BREAKER;
        private ExLinkLabel btnCHANGE_METER;
        private ExButton btnCHANGE_LOG;
        private ExLinkLabel btnADJUST_AMOUNT;
        private ExLinkLabel btnADJUST_USAGE;
        private ExLinkLabel btnPRINT_1;
        private DateTimePicker dtpActivateDate;
        private Label label45;
        private Label lblACTIVATE_DATE;
        private NumericUpDown nudCutoffDays;
        private Label lblCUT_DAY;
        private Label lblUNLIMITE_CUT_DAY;
        private TabPage tabBUY_POWER_YEARLY;
        private Chart chart1;
        private TabPage tabBUY_POWER_MONTHLY;
        private TextBox txtTotalPurchasePower;
        private Label lblTOTAL_BUY_POWER;
        private ComboBox cboIsVoidPurchase;
        private DataGridView dgvCusPurchase;
        private Label lblINVOICE_DATE;
        private DateTimePicker dtPurchaseMonth;
        private ExButton btnADD_SERVICE;
        private ExButton btnAPPLY_PAYMENT_1;
        private ExLinkLabel btnPRINT_RECEIPT;
        private TabPage tabDEPOSIT;
        private ExLinkLabel btnPRINT;
        private ExLinkLabel btnREFUND_DEPOSIT;
        private ExLinkLabel btnAPPLY_PAYMENT;
        private ExLinkLabel btnADJUST_DEPOSIT;
        private ExLinkLabel btnADD_DEPOSIT;
        private DataGridView dgvDeposit;
        private ComboBox cboPrice;
        private ExButton btnREPORT_CONNECTION;
        private ExButton btnREPORT_AGREEMENT;
        private Label label46;
        private TextBox txtTotalFamily;
        private Label lblTOTAL_FAMILY;
        private DataGridView dgvInvoiceList;
        private DataGridView dgvTotalBalance;
        private Label lblTOTAL_DUE_AMOUNT;
        private DataGridView dgvBalanceDeposit;
        private Label lblTOTAL_DEPOSIT_AMOUNT;
        private TabPage tabPREPAID_USAGE;
        private TextBox txtTotalRemainCredit;
        private Label lblLAST_SUBSIDY_BALANCE;
        private DataGridView dgvPrepaidUsage;
        private Label lblINVOICE_DATE_1;
        private TextBox txtTotalRemainCreditCurrencySign;
        private Label lblPRICE_AND_BILLING_CYCLE;
        private ComboBox cboBillingCycle;
        private Label label68;
        private ExButton btnREACTIVATE;
        private ExButton btnREPORT_CUSTOMER_AR;
        private ExLinkLabel btnADJUST_USAGE_1;
        private DataGridViewTextBoxColumn CUS_EQUIPMENT_ID;
        private DataGridViewTextBoxColumn EQUIPMENT_ID;
        private DataGridViewTextBoxColumn EQUIPMENT_NAME;
        private DataGridViewTextBoxColumn EQUIPMENT_QUANTITY;
        private DataGridViewTextBoxColumn WATT;
        private DataGridViewTextBoxColumn TOTAL_WATT;
        private DataGridViewTextBoxColumn COMMENT;
        private DataGridViewCheckBoxColumn _DB;
        private DataGridViewCheckBoxColumn _EDITED;
        private DataGridViewTextBoxColumn CUS_DEPOSIT_ID;
        private DataGridViewTextBoxColumn DEPOSIT_NO;
        private DataGridViewTextBoxColumn CREATE_ON_1;
        private DataGridViewTextBoxColumn CREATE_BY;
        private DataGridViewTextBoxColumn DEPOSIT_ACTION_NAME;
        private DataGridViewTextBoxColumn AMOUNT;
        private DataGridViewTextBoxColumn BALANCE;
        private DataGridViewTextBoxColumn D_CURRENCY_SING1;
        private DataGridViewCheckBoxColumn IS_PAID;
        private Panel panel1;
        private Panel panel2;
        private Panel panel3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private DataGridViewTextBoxColumn TOTAL_BALANCE;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DateTimePicker dtpREQUEST_CONNECTION_DATE;
        private Label lblREQUEST_CONNECTION_DATE;
        private Label lblCONNECTION;
        private ComboBox cboCustomerGroup;
        private ComboBox cboConnectionType;
        private Label label5;
        private ComboBox cboCustomerType;
        private Label lblTYPE_OF_CUSTOMERS;
        private ComboBox cboIdCardType;
        private Label lblID_CARD_NUMBER_TYPE;
        private ComboBox cboTitles;
        private Label lblTITLES;
        private Label label3;
        private Label label4;
        private Label label2;
        private ContextMenuStrip cmsStrip;
        private ToolStripMenuItem ChangeConnectionType;
        private DataGridViewTextBoxColumn INVOICE_ID;
        private DataGridViewTextBoxColumn CURRENCY_ID;
        private DataGridViewTextBoxColumn INVOICE_NO;
        private DataGridViewTextBoxColumn INVOICE_TITLE;
        private DataGridViewTextBoxColumn INVOICE_DATE;
        private DataGridViewTextBoxColumn SETTLE_AMOUNT;
        private DataGridViewTextBoxColumn PAID_AMOUNT;
        private DataGridViewTextBoxColumn DUE_AMOUNT;
        private DataGridViewTextBoxColumn CURRENCY_SING_;
        private DataGridViewTextBoxColumn DUE_DATE;
        private DataGridViewTextBoxColumn DUE_DAY;
        private DateTimePicker dtpYear;
        private DataGridViewTextBoxColumn CUSTOMER_BUY_ID;
        private DataGridViewTextBoxColumn PREPAID_CUS_ID;
        private DataGridViewTextBoxColumn BUY_TIMES;
        private DataGridViewTextBoxColumn CREATE_ON;
        private DataGridViewTextBoxColumn BUY_POWER;
        private DataGridViewTextBoxColumn POWER_COMPENSATED;
        private DataGridViewTextBoxColumn DISCOUNT;
        private DataGridViewTextBoxColumn PRICE;
        private DataGridViewTextBoxColumn AMOUNT_SUBSIDY;
        private DataGridViewTextBoxColumn TOTAL_AMOUNT;
        private DataGridViewTextBoxColumn BUY_CURRENCY_SING_;
        private DataGridViewTextBoxColumn IS_VOID;
        private DataGridViewTextBoxColumn PREPAID_CREDIT_ID;
        private DataGridViewTextBoxColumn START_DATE;
        private DataGridViewTextBoxColumn END_DATE;
        private DataGridViewTextBoxColumn MONTH;
        private DataGridViewTextBoxColumn USAGE;
        private DataGridViewTextBoxColumn PRICE_CREDIT;
        private DataGridViewTextBoxColumn AMOUNT_SUBSIDY_1;
        private DataGridViewTextBoxColumn SETTLE_AMOUNT_1;
        private DataGridViewTextBoxColumn BALANCE_1;
        private DataGridViewTextBoxColumn CREDIT_CURRENCY_SING_;
    }
}