﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.PrePaid
{
    partial class PageRunPrepaidCreditHistory
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageRunPrepaidCreditHistory));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pHeader = new System.Windows.Forms.Panel();
            this.btnADJUST_DATE = new SoftTech.Component.ExButton();
            this.btnREPORT = new SoftTech.Component.ExButton();
            this.lblYEAR = new System.Windows.Forms.Label();
            this.dtpRunYear = new System.Windows.Forms.DateTimePicker();
            this.cboBillingCycle = new System.Windows.Forms.ComboBox();
            this.lblCYCLE_NAME = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.calendarColumn1 = new SoftTech.Component.CalendarColumn();
            this.calendarColumn2 = new SoftTech.Component.CalendarColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTimeColumn1 = new SoftTech.Component.DataGridViewTimeColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvBilling = new System.Windows.Forms.DataGridView();
            this.RUN_PREPAID_CREDIT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CYCLE_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MONTH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_ON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_BY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_CUSTOMER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_CREDIT_USAGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_CREDIT_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBilling)).BeginInit();
            this.SuspendLayout();
            // 
            // pHeader
            // 
            this.pHeader.BackColor = System.Drawing.Color.Transparent;
            this.pHeader.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.pHeader.Controls.Add(this.btnADJUST_DATE);
            this.pHeader.Controls.Add(this.btnREPORT);
            this.pHeader.Controls.Add(this.lblYEAR);
            this.pHeader.Controls.Add(this.dtpRunYear);
            this.pHeader.Controls.Add(this.cboBillingCycle);
            this.pHeader.Controls.Add(this.lblCYCLE_NAME);
            resources.ApplyResources(this.pHeader, "pHeader");
            this.pHeader.Name = "pHeader";
            // 
            // btnADJUST_DATE
            // 
            resources.ApplyResources(this.btnADJUST_DATE, "btnADJUST_DATE");
            this.btnADJUST_DATE.Name = "btnADJUST_DATE";
            this.btnADJUST_DATE.UseVisualStyleBackColor = true;
            this.btnADJUST_DATE.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnREPORT
            // 
            resources.ApplyResources(this.btnREPORT, "btnREPORT");
            this.btnREPORT.Name = "btnREPORT";
            this.btnREPORT.UseVisualStyleBackColor = true;
            this.btnREPORT.Click += new System.EventHandler(this.btnMail_Click);
            // 
            // lblYEAR
            // 
            resources.ApplyResources(this.lblYEAR, "lblYEAR");
            this.lblYEAR.Name = "lblYEAR";
            // 
            // dtpRunYear
            // 
            resources.ApplyResources(this.dtpRunYear, "dtpRunYear");
            this.dtpRunYear.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpRunYear.Name = "dtpRunYear";
            this.dtpRunYear.ShowUpDown = true;
            this.dtpRunYear.ValueChanged += new System.EventHandler(this.dtpRunYear_ValueChanged);
            // 
            // cboBillingCycle
            // 
            this.cboBillingCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBillingCycle.FormattingEnabled = true;
            resources.ApplyResources(this.cboBillingCycle, "cboBillingCycle");
            this.cboBillingCycle.Name = "cboBillingCycle";
            this.cboBillingCycle.SelectedIndexChanged += new System.EventHandler(this.cboBillingCycle_SelectedIndexChanged);
            // 
            // lblCYCLE_NAME
            // 
            resources.ApplyResources(this.lblCYCLE_NAME, "lblCYCLE_NAME");
            this.lblCYCLE_NAME.Name = "lblCYCLE_NAME";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "CYCLE_ID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn1, "dataGridViewTextBoxColumn1");
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "CYCLE_NAME";
            resources.ApplyResources(this.dataGridViewTextBoxColumn2, "dataGridViewTextBoxColumn2");
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // calendarColumn1
            // 
            this.calendarColumn1.DataPropertyName = "BILLING_MONTH";
            dataGridViewCellStyle1.Format = "ខែ MM - ឆ្នាំ yyyy";
            this.calendarColumn1.DefaultCellStyle = dataGridViewCellStyle1;
            resources.ApplyResources(this.calendarColumn1, "calendarColumn1");
            this.calendarColumn1.Name = "calendarColumn1";
            this.calendarColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.calendarColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // calendarColumn2
            // 
            dataGridViewCellStyle2.Format = "ថ្ងៃទី dd - ខែ MM - ឆ្នាំ yyyy";
            this.calendarColumn2.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.calendarColumn2, "calendarColumn2");
            this.calendarColumn2.Name = "calendarColumn2";
            this.calendarColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.calendarColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "BILLING_MONTH";
            dataGridViewCellStyle3.Format = "ថ្ងៃទី dd - ខែ MM - ឆ្នាំ yyyy";
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.dataGridViewTextBoxColumn3, "dataGridViewTextBoxColumn3");
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "END_DATE";
            dataGridViewCellStyle4.Format = "ថ្ងៃទី 0";
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.dataGridViewTextBoxColumn4, "dataGridViewTextBoxColumn4");
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "COLLECT_DAY";
            dataGridViewCellStyle5.Format = "0 នាក់";
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle5;
            resources.ApplyResources(this.dataGridViewTextBoxColumn5, "dataGridViewTextBoxColumn5");
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "TOTAL_CUSTOMER";
            dataGridViewCellStyle6.Format = "0 នាក់";
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle6;
            resources.ApplyResources(this.dataGridViewTextBoxColumn6, "dataGridViewTextBoxColumn6");
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTimeColumn1
            // 
            resources.ApplyResources(this.dataGridViewTimeColumn1, "dataGridViewTimeColumn1");
            this.dataGridViewTimeColumn1.Name = "dataGridViewTimeColumn1";
            this.dataGridViewTimeColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTimeColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "NO_USAGE";
            resources.ApplyResources(this.dataGridViewTextBoxColumn7, "dataGridViewTextBoxColumn7");
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dgvBilling
            // 
            this.dgvBilling.AllowUserToAddRows = false;
            this.dgvBilling.AllowUserToDeleteRows = false;
            this.dgvBilling.AllowUserToResizeRows = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvBilling.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvBilling.BackgroundColor = System.Drawing.Color.White;
            this.dgvBilling.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvBilling.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvBilling.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBilling.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RUN_PREPAID_CREDIT_ID,
            this.CYCLE_NAME,
            this.MONTH,
            this.CREATE_ON,
            this.CREATE_BY,
            this.TOTAL_CUSTOMER,
            this.TOTAL_CREDIT_USAGE,
            this.TOTAL_CREDIT_AMOUNT});
            resources.ApplyResources(this.dgvBilling, "dgvBilling");
            this.dgvBilling.EnableHeadersVisualStyles = false;
            this.dgvBilling.Name = "dgvBilling";
            this.dgvBilling.ReadOnly = true;
            this.dgvBilling.RowHeadersVisible = false;
            this.dgvBilling.RowTemplate.Height = 25;
            // 
            // RUN_PREPAID_CREDIT_ID
            // 
            this.RUN_PREPAID_CREDIT_ID.DataPropertyName = "RUN_PREPAID_CREDIT_ID";
            resources.ApplyResources(this.RUN_PREPAID_CREDIT_ID, "RUN_PREPAID_CREDIT_ID");
            this.RUN_PREPAID_CREDIT_ID.Name = "RUN_PREPAID_CREDIT_ID";
            this.RUN_PREPAID_CREDIT_ID.ReadOnly = true;
            // 
            // CYCLE_NAME
            // 
            this.CYCLE_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CYCLE_NAME.DataPropertyName = "CYCLE_NAME";
            resources.ApplyResources(this.CYCLE_NAME, "CYCLE_NAME");
            this.CYCLE_NAME.Name = "CYCLE_NAME";
            this.CYCLE_NAME.ReadOnly = true;
            // 
            // MONTH
            // 
            this.MONTH.DataPropertyName = "CREDIT_MONTH";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.Format = "ខែ MM";
            this.MONTH.DefaultCellStyle = dataGridViewCellStyle8;
            resources.ApplyResources(this.MONTH, "MONTH");
            this.MONTH.Name = "MONTH";
            this.MONTH.ReadOnly = true;
            // 
            // CREATE_ON
            // 
            this.CREATE_ON.DataPropertyName = "CREATE_ON";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.Format = "yyyy-MM-dd hh:mm tt";
            this.CREATE_ON.DefaultCellStyle = dataGridViewCellStyle9;
            resources.ApplyResources(this.CREATE_ON, "CREATE_ON");
            this.CREATE_ON.Name = "CREATE_ON";
            this.CREATE_ON.ReadOnly = true;
            // 
            // CREATE_BY
            // 
            this.CREATE_BY.DataPropertyName = "CREATE_BY";
            resources.ApplyResources(this.CREATE_BY, "CREATE_BY");
            this.CREATE_BY.Name = "CREATE_BY";
            this.CREATE_BY.ReadOnly = true;
            // 
            // TOTAL_CUSTOMER
            // 
            this.TOTAL_CUSTOMER.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.TOTAL_CUSTOMER.DataPropertyName = "TOTAL_CUSTOMER";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle10.Format = "N0";
            this.TOTAL_CUSTOMER.DefaultCellStyle = dataGridViewCellStyle10;
            resources.ApplyResources(this.TOTAL_CUSTOMER, "TOTAL_CUSTOMER");
            this.TOTAL_CUSTOMER.Name = "TOTAL_CUSTOMER";
            this.TOTAL_CUSTOMER.ReadOnly = true;
            // 
            // TOTAL_CREDIT_USAGE
            // 
            this.TOTAL_CREDIT_USAGE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.TOTAL_CREDIT_USAGE.DataPropertyName = "TOTAL_CREDIT_USAGE";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle11.Format = "N0";
            this.TOTAL_CREDIT_USAGE.DefaultCellStyle = dataGridViewCellStyle11;
            resources.ApplyResources(this.TOTAL_CREDIT_USAGE, "TOTAL_CREDIT_USAGE");
            this.TOTAL_CREDIT_USAGE.Name = "TOTAL_CREDIT_USAGE";
            this.TOTAL_CREDIT_USAGE.ReadOnly = true;
            // 
            // TOTAL_CREDIT_AMOUNT
            // 
            this.TOTAL_CREDIT_AMOUNT.DataPropertyName = "TOTAL_CREDIT_AMOUNT";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle12.Format = "N0";
            this.TOTAL_CREDIT_AMOUNT.DefaultCellStyle = dataGridViewCellStyle12;
            resources.ApplyResources(this.TOTAL_CREDIT_AMOUNT, "TOTAL_CREDIT_AMOUNT");
            this.TOTAL_CREDIT_AMOUNT.Name = "TOTAL_CREDIT_AMOUNT";
            this.TOTAL_CREDIT_AMOUNT.ReadOnly = true;
            // 
            // PageRunPrepaidCreditHistory
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dgvBilling);
            this.Controls.Add(this.pHeader);
            this.Name = "PageRunPrepaidCreditHistory";
            this.pHeader.ResumeLayout(false);
            this.pHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBilling)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel pHeader;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTimeColumn dataGridViewTimeColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private ComboBox cboBillingCycle;
        private Label lblCYCLE_NAME;
        private CalendarColumn calendarColumn1;
        private CalendarColumn calendarColumn2;
        private DateTimePicker dtpRunYear;
        private Label lblYEAR;
        private ExButton btnREPORT;
        private ExButton btnADJUST_DATE;
        private DataGridView dgvBilling;
        private DataGridViewTextBoxColumn RUN_PREPAID_CREDIT_ID;
        private DataGridViewTextBoxColumn CYCLE_NAME;
        private DataGridViewTextBoxColumn MONTH;
        private DataGridViewTextBoxColumn CREATE_ON;
        private DataGridViewTextBoxColumn CREATE_BY;
        private DataGridViewTextBoxColumn TOTAL_CUSTOMER;
        private DataGridViewTextBoxColumn TOTAL_CREDIT_USAGE;
        private DataGridViewTextBoxColumn TOTAL_CREDIT_AMOUNT;
    }
}
