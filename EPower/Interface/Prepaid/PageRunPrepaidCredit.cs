﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface.PrePaid
{
    public partial class PageRunPrepaidCredit : Form
    {
        #region Constructor
        public PageRunPrepaidCredit()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgvBillingCycle); 
        }
        #endregion Constructor

        #region Method

        public void bind()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(CYCLE_ID.Name, typeof(int));
            dt.Columns.Add(CYCLE_NAME.Name, typeof(string));
            dt.Columns.Add(MONTH.Name, typeof(DateTime));
            dt.Columns.Add(START_DATE.Name, typeof(DateTime));
            dt.Columns.Add(END_DATE.Name, typeof(DateTime));
            dt.Columns.Add(COLLECT_DATE.Name, typeof(int));
            dt.Columns.Add(TOTAL_CUSTOMER.Name, typeof(int));
            dt.Columns.Add(BILLING_CUSTOMER.Name, typeof(int));
            dt.Columns.Add(NO_USAGE.Name, typeof(int));
            DataRow dr=dt.NewRow();

            dgvBillingCycle.Rows.Clear();

            Runner.RunNewThread(delegate
            {
                foreach (TBL_BILLING_CYCLE bc in DBDataContext.Db.TBL_BILLING_CYCLEs.Where(b => b.IS_ACTIVE))
                {
                    int MAX_DAY_TO_IGNORE_BILLING = DataHelper.ParseToInt(Method.Utilities[Utility.MAX_DAY_TO_IGNORE_BILLING]);
                      
                    int totalCustomer = (from c in DBDataContext.Db.TBL_CUSTOMERs
                                        join cm in DBDataContext.Db.TBL_CUSTOMER_METERs on c.CUSTOMER_ID equals cm.CUSTOMER_ID
                                        where c.BILLING_CYCLE_ID == bc.CYCLE_ID
                                                && cm.IS_ACTIVE
                                                && c.IS_POST_PAID == false
                                                && (c.STATUS_ID == (int)CustomerStatus.Active || c.STATUS_ID == (int)CustomerStatus.Blocked)
                                        select c).Count();

                    //if have billing cycle to run
                    if (totalCustomer > 0)
                    {
                        DateTime datStart = UIHelper._DefaultDate;
                        DateTime datEnd = UIHelper._DefaultDate;
                        DateTime datRuningMonth = Method.GetNextBillingMonth(bc.CYCLE_ID, ref datStart, ref datEnd);

                        int billingCustomer = (from c in DBDataContext.Db.TBL_CUSTOMERs
                                                join cm in DBDataContext.Db.TBL_CUSTOMER_METERs on c.CUSTOMER_ID equals cm.CUSTOMER_ID
                                                where c.BILLING_CYCLE_ID == bc.CYCLE_ID
                                                    && cm.IS_ACTIVE
                                                    && c.IS_POST_PAID == false
                                                    && (c.STATUS_ID == (int)CustomerStatus.Active || c.STATUS_ID == (int)CustomerStatus.Blocked)
                                                    && (DBDataContext.Db.GetSystemDate() - c.ACTIVATE_DATE).TotalDays >= MAX_DAY_TO_IGNORE_BILLING
                                                select c.CUSTOMER_ID).Count();

                        int intNoneUsageCustomer = Method.GetNoneUsageCustomerPrepaid(bc.CYCLE_ID, datRuningMonth.Date); 
                        
                        dr = dt.NewRow();
                        dr[CYCLE_ID.Name] = bc.CYCLE_ID;
                        dr[CYCLE_NAME.Name] = bc.CYCLE_NAME;
                        dr[MONTH.Name] = datRuningMonth.Date;
                        dr[START_DATE.Name] = datStart;
                        dr[END_DATE.Name] = datEnd;
                        dr[COLLECT_DATE.Name] = bc.COLLECT_DAY;
                        dr[TOTAL_CUSTOMER.Name] = totalCustomer;
                        dr[BILLING_CUSTOMER.Name] = billingCustomer;
                        dr[NO_USAGE.Name] = intNoneUsageCustomer;
                        dt.Rows.Add(dr);
                    }
                }
            });
            foreach (DataRow row in dt.Rows)
            {
                dgvBillingCycle.Rows.Add(DataHelper.ParseToInt(row[0].ToString()),row[1].ToString(),row[2],row[3],row[4],row[5],row[6],row[7],row[8]);
            }
            
        }

        #endregion Method

        private void btnCheck_Click(object sender, EventArgs e)
        {
            if (dgvBillingCycle.SelectedRows.Count > 0)
            {
                int intBillingCycle = DataHelper.ParseToInt(dgvBillingCycle.SelectedRows[0].Cells[CYCLE_ID.Name].Value.ToString());
                DateTime datRunMonth = DateTime.Parse(dgvBillingCycle.SelectedRows[0].Cells[MONTH.Name].Value.ToString());
                (new DialogCustomerNoUsage(intBillingCycle)).ShowDialog();
                bind();
            }
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            if (dgvBillingCycle.SelectedRows.Count <= 0) return;
            // customer no usage.                
            var intNoneUsageCus = int.Parse(dgvBillingCycle.SelectedRows[0].Cells[NO_USAGE.Name].Value.ToString());
            if (intNoneUsageCus > 0)
            {
                MsgBox.ShowInformation(string.Format(Resources.MS_CANNOT_RUN_PREPAID_CREDIT_BECAUSE_HAVE_CUSTOMER_NO_USAGE, intNoneUsageCus));
                return;
            }
            var diag = new DialogRunPrepaidCredit
            {
                cboBillingCycle =
                {
                    SelectedValue = (int) dgvBillingCycle.SelectedRows[0].Cells[CYCLE_ID.Name].Value
                },
                dtpRunMonth = {Value = (DateTime) dgvBillingCycle.SelectedRows[0].Cells[MONTH.Name].Value},
                dtpFrom = {Value = (DateTime) dgvBillingCycle.SelectedRows[0].Cells[START_DATE.Name].Value},
                dtpTo = {Value = (DateTime) dgvBillingCycle.SelectedRows[0].Cells[END_DATE.Name].Value}
            };
            if (diag.ShowDialog() == DialogResult.OK)
            {
                bind();
            }
        }

        private void btnViewUnregister_Click(object sender, EventArgs e)
        {
            var diag = new DialogMeterUnregister();
            diag.ShowDialog();
        }

        private void btnSpecialUsage_Click(object sender, EventArgs e)
        {
            var diag = new DialogCustomerSpecialUsage();
            diag.cboCycle.SelectedValue = (int)dgvBillingCycle.SelectedRows[0].Cells[CYCLE_ID.Name].Value;
            diag.ShowDialog();
        }
    }
}
