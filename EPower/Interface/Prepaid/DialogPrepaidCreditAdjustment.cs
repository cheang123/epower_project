﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface.PrePaid
{
    public partial class DialogPrepaidCreditAdjustment : ExDialog
    {
        #region Private Data
        TBL_CUSTOMER _objCustomer;
        TBL_PREPAID_CREDIT _objPrepaidCredit = new TBL_PREPAID_CREDIT();

        TBL_USAGE _objUsageNew = new TBL_USAGE();
        TBL_USAGE _objUsageOld = new TBL_USAGE();

        private TBL_USAGE _objEditUsage;

        bool _blnStart = true;
        private bool _isReading = true;
        private Dictionary<string, TBL_USAGE> USAGEs = new Dictionary<string, TBL_USAGE>();

        private DataTable dtSubsidyTariff = new DataTable();
        #endregion

        #region Constructor
        public DialogPrepaidCreditAdjustment(TBL_CUSTOMER objCustomer, TBL_PREPAID_CREDIT objPrepaidCredit, int intPriceId)
        {
            InitializeComponent();
            _objCustomer = objCustomer;
            objPrepaidCredit._CopyTo(_objPrepaidCredit);
            loadUsage();
            createTmpSubsidyTable(_objPrepaidCredit);
            read();
        }

        #endregion Constructor

        #region Event
        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtNewUsage_TextChanged(object sender, EventArgs e)
        {
            calculateTotalUsage();
        }

        private void txtDescription_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void chkMETER_IS_RENEW_CYCLE_CheckedChanged(object sender, EventArgs e)
        {
            calculateTotalUsage();
        }
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (txtDescription.Text.Trim() == "")
            {
                txtDescription.SetValidation(string.Format(Resources.REQUIRED, lblNOTE.Text));
                return;
            }

            Runner.RunNewThread(SaveAdjustment);
        }
        #endregion Event

        #region Method
        private void loadUsage()
        {
            try
            {
                var usages = from u in DBDataContext.Db.TBL_USAGEs
                             join m in DBDataContext.Db.TBL_METERs on u.METER_ID equals m.METER_ID
                             join pu in DBDataContext.Db.TBL_PREPAID_CREDIT_USAGEs on m.METER_ID equals pu.METER_ID
                             where pu.PREPAID_CREDIT_ID == _objPrepaidCredit.PREPAID_CREDIT_ID
                                   && u.USAGE_MONTH.Date == _objPrepaidCredit.CREDIT_MONTH.Date
                             orderby u.USAGE_MONTH descending
                             select new { m.METER_CODE, TBL_USAGE = u };

                foreach (var item in usages)
                {
                    var u = new TBL_USAGE();
                    item.TBL_USAGE._CopyTo(u);
                    USAGEs.Add(item.METER_CODE, u);
                }

                //set meter code to combo box
                UIHelper.SetDataSourceToComboBox(cboMeterCode, USAGEs.Select(x => new { Display = x.Key, Value = x.Key }));
                if (cboMeterCode.Items.Count > 0)
                {
                    cboMeterCode.SelectedIndex = 0;
                }

                // total amount before adjustment.
                txtTotalAmount.Text = UIHelper.FormatCurrency(_objPrepaidCredit.CREDIT_AMOUNT, _objPrepaidCredit.CURRENCY_ID);
                // keep it blank for confirm user to change amount
                txtTotalAmountNew.Text = "";
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp);
            }
        }

        private void read()
        {
            _isReading = true;
            dtpStartDate.Value = _objPrepaidCredit.START_DATE;
            dtpEndDate.Value = _objPrepaidCredit.END_DATE;
            txtStartUsage.Text = _objEditUsage.START_USAGE.ToString(UIHelper._DefaultUsageFormat);
            txtEndUsage.Text = _objEditUsage.END_USAGE.ToString(UIHelper._DefaultUsageFormat);
            txtMultiplier.Text = _objEditUsage.MULTIPLIER.ToString("N0");
            txtTotalUsage.Text = Method.GetTotalUsage(_objEditUsage).ToString("N0");
            txtSUBSIDY_TARIFF.Text = _objPrepaidCredit.PRICE.ToString("#,###.####");
            _isReading = false;
            calculateTotalUsage();
        }

        private void write()
        {
            _objEditUsage.START_USAGE = DataHelper.ParseToDecimal(txtStartUsage.Text);
            _objEditUsage.END_USAGE = DataHelper.ParseToDecimal(txtEndUsage.Text);
            _objEditUsage.MULTIPLIER = DataHelper.ParseToInt(txtMultiplier.Text);
            _objEditUsage.IS_METER_RENEW_CYCLE = chkIS_NEW_CYCLE.Checked;
            _objPrepaidCredit.PRICE = DataHelper.ParseToDecimal(txtSUBSIDY_TARIFF.Text);
            _objPrepaidCredit.BASED_PRICE = DataHelper.ParseToDecimal(txtSUBSIDY_TARIFF.Text);
            // _objPrepaidCredit.CURRENCY_ID = (int)this.cboCurrency.SelectedValue;
        }

        private bool invalid()
        {
            this.ClearValidation();
            if (!DataHelper.IsNumber(txtEndUsage.Text))
            {
                txtEndUsage.SetValidation(string.Format(Resources.REQUIRED, lblEND_USAGE.Text));
                return true;
            }
            if (DataHelper.ParseToInt(txtMultiplier.Text) <= 0)
            {
                txtMultiplier.SetValidation(Resources.REQUEST_GRATER_THAN_ZERO);
                return true;
            }
            return false;
        }
        private void SaveAdjustment()
        {
            try
            {
                using (var tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    var now = DBDataContext.Db.GetSystemDate();
                    var adjustAmount = DataHelper.ParseToDecimal(txtTotalAmountNew.Text);
                    var adjustUsage = DataHelper.ParseToDecimal(txtTotalUsage.Text);
                    var beforeAdjustAmount = DataHelper.ParseToDecimal(txtTotalAmount.Text);

                    /*
                     * Update TBL_PREPAID_CREDIT
                     */
                    var objOldPrepaidCredit = new TBL_PREPAID_CREDIT();
                    _objPrepaidCredit._CopyTo(objOldPrepaidCredit);

                    _objPrepaidCredit.CREDIT_AMOUNT = adjustAmount;
                    _objPrepaidCredit.USAGE = adjustUsage;
                    _objPrepaidCredit.PRICE = DataHelper.ParseToDecimal(txtSUBSIDY_TARIFF.Text);

                    var objChange = DBDataContext.Db.Update(objOldPrepaidCredit, _objPrepaidCredit);

                    foreach (var item in USAGEs)
                    {
                        var objUsage =
                            DBDataContext.Db.TBL_USAGEs.FirstOrDefault(x => x.USAGE_ID == item.Value.USAGE_ID);
                        var objOldUsage = new TBL_USAGE();
                        objUsage._CopyTo(objOldUsage);

                        var objPUsage =
                            DBDataContext.Db.TBL_PREPAID_CREDIT_USAGEs.FirstOrDefault(
                                x => x.PREPAID_CREDIT_ID == _objPrepaidCredit.PREPAID_CREDIT_ID &&
                                     x.METER_ID == objUsage.METER_ID);
                        var objOldPUsage = new TBL_PREPAID_CREDIT_USAGE();
                        objPUsage._CopyTo(objOldPUsage);


                        /*
                         * Add new adjustment.
                         */
                        var objAdjustment = new TBL_PREPAID_CREDIT_ADJUSTMENT
                        {
                            PREPAID_CREDIT_USAGE_ID = objPUsage.PREPAID_CREDIT_USAGE_ID,
                            METER_ID = objPUsage.METER_ID,
                            USER_CASH_DRAWER_ID = Login.CurrentCashDrawer.USER_CASH_DRAWER_ID,
                            CREATE_BY = SoftTech.Security.Logic.Login.CurrentLogin.LOGIN_NAME,
                            CREATE_ON = now,
                            BEFORE_ADJUST_USAGE = objPUsage.TOTAL_USAGE,
                            BEFORE_ADJUST_AMOUNT = beforeAdjustAmount,
                            ADJUST_REASON = txtDescription.Text,
                            ADJUST_USAGE = Method.GetTotalUsage(item.Value)
                        };

                        if (adjustAmount > 0)
                        {
                            objAdjustment.ADJUST_AMOUNT = objAdjustment.ADJUST_USAGE * DataHelper.ParseToDecimal(txtSUBSIDY_TARIFF.Text);
                        }

                        DBDataContext.Db.InsertChild(objAdjustment, _objPrepaidCredit, ref objChange);

                        /*
                         * Update Usage.
                         */
                        objUsage.START_USAGE = item.Value.START_USAGE;
                        objUsage.END_USAGE = item.Value.END_USAGE;
                        objUsage.MULTIPLIER = item.Value.MULTIPLIER;
                        objUsage.IS_METER_RENEW_CYCLE = item.Value.IS_METER_RENEW_CYCLE;
                        DBDataContext.Db.UpdateChild(objOldUsage, objUsage, _objPrepaidCredit, ref objChange);

                        /*
                         * Update Prepaid Usage.
                         */
                        objPUsage.START_USAGE = objUsage.START_USAGE;
                        objPUsage.END_USAGE = objUsage.END_USAGE;
                        objPUsage.IS_METER_RENEW_CYCLE = objUsage.IS_METER_RENEW_CYCLE;
                        objPUsage.MULTIPLIER = objUsage.MULTIPLIER;
                        objPUsage.TOTAL_USAGE = Method.GetTotalUsage(objUsage);
                        DBDataContext.Db.UpdateChild(objOldPUsage, objPUsage, _objPrepaidCredit, ref objChange);
                    }
                    DBDataContext.Db.SubmitChanges();
                    tran.Complete();
                }
                DialogResult = DialogResult.OK;
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp);
            }
        }
        private void createTmpSubsidyTable(TBL_PREPAID_CREDIT obj)
        {
            //bind columns
            dtSubsidyTariff.Columns.Add("SUBSIDY_TYPE_NAME", typeof(string));
            dtSubsidyTariff.Columns.Add("START_USAGE", typeof(decimal));
            dtSubsidyTariff.Columns.Add("END_USAGE", typeof(decimal));
            dtSubsidyTariff.Columns.Add("PRICE_CREDIT", typeof(decimal));
            dtSubsidyTariff.Columns.Add("SUBSIDY_TARIFF", typeof(decimal));
            dtSubsidyTariff.Columns.Add("CUSTOMER_CONNECTION_TYPE_ID", typeof(int));

            if (obj.CREDIT_MONTH <= new DateTime(2017, 2, 1))
            {
                dtSubsidyTariff.Rows.Add("លក់ឱ្យលំនៅដ្ឋានប្រើតិចជាង  10 kWh/ខែ", 0, 10, 480, 800, 1);
                dtSubsidyTariff.Rows.Add("លក់ឱ្យលំនៅដ្ឋានប្រើធំជាង 10 kWh/ខែ", 11, 99999999999999.9999, 800, 800, 1);
            }
            else if (obj.CREDIT_MONTH >= new DateTime(2017, 3, 1))
            {
                dtSubsidyTariff.Rows.Add("លក់ឱ្យលំនៅដ្ឋានប្រើតិចជាង  10 kWh/ខែ", 0, 10, 480, 790, 1);
                dtSubsidyTariff.Rows.Add("លក់ឱ្យលំនៅដ្ឋានប្រើពី 11 ដល់ 50 kWh/ខែ", 11, 50, 610, 790, 1);
                dtSubsidyTariff.Rows.Add("លក់ឱ្យលំនៅដ្ឋានប្រើធំជាង 51 kWh/ខែ", 51, 99999999999999.9999, 790, 790, 1);
            }
        }
        private void calculateTotalUsage()
        {
            if (invalid())
            {
                return;
            }
            if (_isReading)
            {
                return;
            }
            write();

            txtMeterUsage.Text = Method.GetTotalUsage(_objEditUsage).ToString(UIHelper._DefaultUsageFormat);

            var totalUsage = USAGEs.Select(x => x.Value).Sum(x => Method.GetTotalUsage(x));
            txtTotalUsage.Text = totalUsage.ToString(UIHelper._DefaultUsageFormat);

            var tmpTariffsPrePaidType = (from x in dtSubsidyTariff.AsEnumerable()
                                         where totalUsage >= x.Field<decimal>("START_USAGE")
                                             && totalUsage <= x.Field<decimal>("END_USAGE")
                                             && x.Field<int>("CUSTOMER_CONNECTION_TYPE_ID") == _objCustomer.CUSTOMER_CONNECTION_TYPE_ID
                                         select new
                                         {
                                             PRICE_CREDIT = x.Field<decimal>("PRICE_CREDIT"),
                                             SUBSIDY_TARIFF = x.Field<decimal>("SUBSIDY_TARIFF")
                                         }).FirstOrDefault();
            decimal price = 0;
            if (tmpTariffsPrePaidType != null)
            {
                price = tmpTariffsPrePaidType.SUBSIDY_TARIFF - tmpTariffsPrePaidType.PRICE_CREDIT;
            }
            txtSUBSIDY_TARIFF.Text = price.ToString("#,###.####");
            txtTotalAmountNew.Text = (totalUsage * price).ToString(UIHelper._DefaultUsageFormat);
            this.ClearAllValidation();
        }

        private void cboMeterCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboMeterCode.SelectedIndex == -1)
            {
                return;
            }
            if (_objEditUsage != null)
            {
                if (invalid())
                {
                    return;
                }
                write();
            }
            _objEditUsage = USAGEs[cboMeterCode.SelectedValue.ToString()];
            read();
        }

        private void EnglishKey(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        private void txtNumber(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        }
        #endregion Method  
    }
}
