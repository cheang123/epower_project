﻿using System;
using System.IO;
using System.Windows.Forms;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface.PrePaid
{
    public partial class PageReportNotBuyPower : UserControl
    {
        private CrystalReportHelper ch;
        public PageReportNotBuyPower()
        {
            InitializeComponent();
            viewer.DefaultView();
            dtpEndDate.Value = DBDataContext.Db.GetSystemDate();
            dtpDate.Value = new DateTime(dtpEndDate.Value.Year, dtpEndDate.Value.Month, 1);
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            Runner.Run(viewReport);
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            sendMail();
        }

        private void viewReport()
        {            
            ch = ViewReportNotBuyPower(dtpDate.Value.Date, dtpEndDate.Value.Date);
            viewer.ReportSource = ch.Report;
        }

        public CrystalReportHelper ViewReportNotBuyPower(DateTime dtStart,DateTime dtEnd)
        {
            CrystalReportHelper c = new CrystalReportHelper("ReportPrepaidNotBuyPower.rpt");
            c.SetParameter("@D1", dtStart);
            c.SetParameter("@D2", dtEnd);
            return c; 
        } 
        private void sendMail()
        {
            try
            {
                viewReport();
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + ch.ReportName + ".xls").FullName;
                ch.ExportToExcel(exportPath);
                DialogSendMail.Instance.Add(ch.ReportName, exportPath);
                DialogSendMail.Instance.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }
    }
}
