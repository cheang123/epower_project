﻿using EPower.Base.Logic;
using EPower.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface.PrePaid
{
    public partial class DialogPurchasePower : ExDialog
    {
        #region Private Data
        TBL_CUSTOMER _objCusReadCard;
        TBL_CUSTOMER_METER _objCustomerMeter;
        TBL_METER _objMeter;
        TBL_PREPAID_CUSTOMER _objPrePaidCustomer;
        TBL_PREPAID_CUSTOMER _objOldPrePaidCustomer = new TBL_PREPAID_CUSTOMER();

        TBL_CUSTOMER_BUY _objCusBuyPow;

        TBL_AREA _objArea;
        TBL_POLE _objPole;
        TBL_BOX _objBox;
        TBL_PRICE _objPrice;
        TBL_PRICE_DETAIL _objPriceDetail;

        PowerCard _objReadCard;
        TLKP_CURRENCY _objKhCurrency;
        private decimal _creditPaidAmount;
        #endregion


        public DialogPurchasePower()
        {
            InitializeComponent();
            _objKhCurrency = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.CURRENCY_ID == 1);
            ResetObject();
            read();
        }



        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }


        private void txtEnterEnglish(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void txtKeyNumber(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputNumberOnly(sender, e);
        }

        private void btnReadCard_Click(object sender, EventArgs e)
        {
            try
            {
                ReadCardInfo();
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp);
            }
        }

        #region Method
        private void read()
        {
            txtCashDrawerName.Text = DBDataContext.Db.TBL_CASH_DRAWERs.FirstOrDefault(x => x.CASH_DRAWER_ID == Login.CurrentCashDrawer.CASH_DRAWER_ID).CASH_DRAWER_NAME;
            txtUserCashDrawerName.Text = Login.CurrentLogin.LOGIN_NAME;
        }

        private void CalculatedAmount()
        {
            if (_objPrice == null)
            {
                return;
            }

            decimal decBuyPower = 0,
                decPrice = 0,
                decDiscount = 0,
                decTotalAmount = 0,
                decCredit = 0;
            if (string.IsNullOrEmpty(txtPurchasePower.Text))
            {
                txtPurchasePower.Text = "0";
                txtPurchasePower.SelectAll();
            }
            if (string.IsNullOrEmpty(txtPrice.Text))
            {
                txtPrice.Text = "0";
                txtPrice.SelectAll();
            }
            if (string.IsNullOrEmpty(txtDiscount.Text))
            {
                txtDiscount.Text = "0";
                txtDiscount.SelectAll();
            }
            decBuyPower = DataHelper.ParseToDecimal(txtPurchasePower.Text);
            decPrice = DataHelper.ParseToDecimal(txtPrice.Text);
            decDiscount = DataHelper.ParseToDecimal(txtDiscount.Text);
            decCredit = DataHelper.ParseToDecimal(txtCredit.Text);
            decTotalAmount = (decBuyPower - decDiscount) * decPrice;
            /*
             * Price currency and Credit currency is the same.
             */
            if (_objPrice.CURRENCY_ID == _objKhCurrency.CURRENCY_ID)
            {
                _creditPaidAmount = decTotalAmount > decCredit ? decCredit : decTotalAmount;
                decTotalAmount -= _creditPaidAmount;
            }

            txtTotalAmount.Text = UIHelper.FormatCurrency(UIHelper.RoundUp(decTotalAmount, _objPrice.CURRENCY_ID));

            //lblReadPower.Text = decBuyPower <= 0 ? string.Empty : string.Format("{0} (គីឡូវ៉ាត់ម៉ោង)", DataHelper.NumberToWord(decBuyPower));
            lblREAD_AMOUNT_.Text = decTotalAmount <= 0 ? string.Empty : string.Format("{0} {1}", DataHelper.NumberToWord(DataHelper.ParseToDecimal(txtTotalAmount.Text)), DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.CURRENCY_ID == _objPrice.CURRENCY_ID).CURRENCY_NAME);
        }

        private ICCard ReadCardInfo()
        {
            ResetObject();

            ICCard objReadCard = ICCard.Read();
            switch ((CardType)objReadCard.ICCardType)
            {
                case CardType.NoCard:
                    lblCARD_TYPE_.Text = Resources.MS_PLEASE_INSERT_NO_POWER_CARD_TO_DEVICE;
                    break;
                case CardType.BlankCard:
                case CardType.FormattedCard:
                    lblCARD_TYPE_.Text = string.Format("{0}។ {1}", Resources.NEW_CARD, Resources.MS_PLEASE_INSERT_NO_POWER_CARD_TO_DEVICE);
                    break;
                case CardType.HavePowerCard:
                    lblCARD_TYPE_.Text = Resources.IC_CARD_HAVE_POWER;
                    _objReadCard = (PowerCard)objReadCard;
                    bind();
                    break;
                case CardType.NoPowerCard:
                    lblCARD_TYPE_.Text = Resources.VALID_CARD;
                    _objReadCard = (PowerCard)objReadCard;
                    bind();
                    break;
                case CardType.ResetCard:
                case CardType.PresetCard:
                case CardType.TestCard:
                case CardType.CutCard:
                    lblCARD_TYPE_.Text = Resources.MS_CARD_CANNOT_USE;
                    break;
                default:
                    break;
            }

            return objReadCard;
        }

        private void bind()
        {

            var objReadInfo = (from c in DBDataContext.Db.TBL_CUSTOMERs
                               join pc in DBDataContext.Db.TBL_PREPAID_CUSTOMERs on c.CUSTOMER_ID equals pc.CUSTOMER_ID
                               join cb in DBDataContext.Db.TBL_CUSTOMER_BUYs on pc.PREPAID_CUS_ID equals cb.PREPAID_CUS_ID
                               where Prepaid.GetAreaCode() == _objReadCard.AreaCode && cb.IS_VOID == false
                               && pc.CARD_CODE.ToLower() == _objReadCard.CustomerCode.ToLower()
                               orderby cb.CREATE_ON descending
                               select new
                               {
                                   CUSTOMER = c,
                                   PREPAID_CUSTOMER = pc,
                                   CUSTOMER_LAST_BUY = cb ?? new TBL_CUSTOMER_BUY() { CREATE_ON = UIHelper._DefaultDate }
                               }).FirstOrDefault();




            //(from c in DBDataContext.Db.TBL_CUSTOMERs
            //               join pc in DBDataContext.Db.TBL_PREPAID_CUSTOMERs on c.CUSTOMER_ID equals pc.CUSTOMER_ID
            //               where pc.CARD_CODE.ToLower()==_objReadCard.CustomerCode.ToLower()
            //               && Prepaid.GetAreaCode()==_objReadCard.AreaCode
            //               select new
            //               {
            //                   CUSTOMER = c,
            //                   PREPAID_CUSTOMER = pc,
            //                   CUSTOMER_LAST_BUY = DBDataContext.Db.TBL_CUSTOMER_BUYs
            //                                        .OrderByDescending(x => x.BUY_TIMES)
            //                                        .FirstOrDefault(x => x.PREPAID_CUS_ID == pc.PREPAID_CUS_ID && !x.IS_VOID) ?? new TBL_CUSTOMER_BUY() 
            //               }
            //               ).FirstOrDefault();
            if (objReadInfo == null)
            {
                if (Prepaid.GetAreaCode() != _objReadCard.AreaCode)
                {
                    lblCARD_TYPE_.Text = Resources.MS_PREPAID_CUSTOMER_WRONG_AREA_CODE;
                }

                return;
            }

            _objCusReadCard = objReadInfo.CUSTOMER;
            _objPrePaidCustomer = objReadInfo.PREPAID_CUSTOMER;

            txtCustomerCode.Text = _objCusReadCard.CUSTOMER_CODE;
            txtFullName.Text = string.Concat(_objCusReadCard.LAST_NAME_KH, " ", _objCusReadCard.FIRST_NAME_KH);

            //used info
            txtTotalBuyPower.Text = _objPrePaidCustomer.TOTAL_BUY_POWER.ToString("N0");
            txtCompensated.Text = _objPrePaidCustomer.COMPENSATED.ToString("N0");
            txtLastPurchasePower.Text = objReadInfo.CUSTOMER_LAST_BUY.BUY_QTY.ToString("N0");
            dtpLastPurchaseDate.Value = objReadInfo.CUSTOMER_LAST_BUY.CREATE_ON < dtpLastPurchaseDate.MinDate ? _objCusReadCard.ACTIVATE_DATE : objReadInfo.CUSTOMER_LAST_BUY.CREATE_ON;
            txtPurchaseTimes.Text = objReadInfo.CUSTOMER_LAST_BUY.BUY_TIMES.ToString();

            //new purchase
            txtPurchasePower.Text = txtLastPurchasePower.Text;
            txtDiscount.Text = string.Empty;
            readCustomerMeter();
            txtPurchasePower.Focus();

            /*
             * Total credit balance.
             */
            var credit =
                DBDataContext.Db.TBL_PREPAID_CREDITs.Where(x => x.CUSTOMER_ID == _objPrePaidCustomer.CUSTOMER_ID);
            if (credit.Any())
            {
                txtCredit.Text = UIHelper.FormatCurrency(credit.Sum(x => x.CREDIT_AMOUNT - x.PAID_AMOUNT), _objKhCurrency.CURRENCY_ID);
                txtCreditSign.Text = _objKhCurrency.CURRENCY_SING;
            }


            CalculatedAmount();
        }

        private void readCustomerMeter()
        {
            _objCustomerMeter = DBDataContext.Db.TBL_CUSTOMER_METERs.FirstOrDefault(r => r.CUSTOMER_ID == _objCusReadCard.CUSTOMER_ID && r.IS_ACTIVE);
            if (_objCustomerMeter != null)
            {
                _objMeter = DBDataContext.Db.TBL_METERs.FirstOrDefault(m => m.METER_ID == _objCustomerMeter.METER_ID);
                txtMeterCode.Text = _objMeter.METER_CODE;

                _objArea = DBDataContext.Db.TBL_AREAs.FirstOrDefault(a => a.AREA_ID == _objCusReadCard.AREA_ID);
                txtAreaCode.Text = _objArea.AREA_CODE;

                _objPole = DBDataContext.Db.TBL_POLEs.FirstOrDefault(p => p.POLE_ID == _objCustomerMeter.POLE_ID);
                txtPoleCode.Text = _objPole.POLE_CODE;

                _objBox = DBDataContext.Db.TBL_BOXes.FirstOrDefault(b => b.BOX_ID == _objCustomerMeter.BOX_ID);
                txtBoxCode.Text = _objBox.BOX_CODE;

                _objPrice = DBDataContext.Db.TBL_PRICEs.FirstOrDefault(x => x.PRICE_ID == _objCusReadCard.PRICE_ID);
                txtPriceType.Text = _objPrice.PRICE_NAME;
                txtSignTotalAmount.Text = txtSignUnitPrice.Text = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.CURRENCY_ID == _objPrice.CURRENCY_ID).CURRENCY_SING;

                _objPriceDetail = DBDataContext.Db.TBL_PRICE_DETAILs.FirstOrDefault(x => x.PRICE_ID == _objPrice.PRICE_ID);
                txtPrice.Text = _objPriceDetail.PRICE.ToString("N0");

            }
        }

        private void ResetObject()
        {
            //Release object
            _objCusReadCard = null;
            _objCustomerMeter = null;
            _objPrePaidCustomer = null;
            _objMeter = null;
            _objArea = null;
            _objPole = null;
            _objBox = null;
            _objPrice = null;
            _objPriceDetail = null;
            _objReadCard = null;
            _creditPaidAmount = 0;
            //Reset general info
            txtCustomerCode.Text =
            txtFullName.Text =
            txtMeterCode.Text =

            txtAreaCode.Text =
            txtPoleCode.Text =
            txtBoxCode.Text =
            //Reset last purchase
            txtPriceType.Text =
            txtTotalBuyPower.Text =
            txtCompensated.Text =

            txtLastPurchasePower.Text = string.Empty;
            dtpLastPurchaseDate.Value = UIHelper._DefaultDate;
            txtPurchaseTimes.Text =
            //Reset Purchase power
            txtPurchasePower.Text =
            txtPrice.Text =
            txtDiscount.Text =
            txtTotalAmount.Text =
            txtCredit.Text =
            lblCARD_TYPE_.Text = string.Empty;


        }

        private bool Invalid()
        {
            this.ClearAllValidation();

            if (string.IsNullOrEmpty(txtCustomerCode.Text.Trim()))
            {
                txtCustomerCode.SetValidation(string.Format(Resources.REQUIRED, lblCUSTOMER.Text));
                return true;
            }

            if (string.IsNullOrEmpty(txtFullName.Text.Trim()))
            {
                txtFullName.SetValidation(string.Format(Resources.REQUIRED, lblCUSTOMER_NAME.Text));
                return true;
            }

            if (txtPurchasePower.Text.Trim() == string.Empty)
            {
                txtPurchasePower.SetValidation(string.Format(Resources.REQUIRED, lblBUY_POWER.Text));
                return true;
            }

            if (txtDiscount.Text.Trim() == string.Empty)
            {
                txtDiscount.SetValidation(string.Format(Resources.REQUIRED, lblDISCOUNT.Text));
                return true;
            }

            if (int.Parse(txtDiscount.Text) > int.Parse(txtPurchasePower.Text))
            {
                txtDiscount.SetValidation(string.Format(Resources.MS_VALIDATE_START_END_DATE, lblBUY_POWER.Text, lblDISCOUNT.Text));
                return true;
            }
            if (int.Parse(txtPurchasePower.Text) <= 0)
            {
                txtPurchasePower.SetValidation(Resources.REQUEST_GRATER_THAN_ZERO);
                return true;
            }


            ICCard objReadCard = ICCard.Read();
            if ((CardType)objReadCard.ICCardType != CardType.NoPowerCard)
            {
                MsgBox.ShowInformation(Resources.MS_PLEASE_INSERT_NO_POWER_CARD_TO_DEVICE);
                return true;
            }

            return false;

        }
        #endregion

        private void txtCalcAmount(object sender, EventArgs e)
        {
            CalculatedAmount();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (Invalid())
            {
                return;
            }
            bool blnSaleSuccess = false;
            Runner.RunNewThread(() =>
            {
                try
                {
                    DateTime dtNow = DBDataContext.Db.GetSystemDate();
                    var exchangeRate = new ExchangeRateLogic().findLastExchangeRate(DBDataContext.Db.GetSystemDate(), _objPrice.CURRENCY_ID);
                    Sequence sequence = Method.GetSequenceId(SequenceType.Prepaid);
                    using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                    {
                        _objPrePaidCustomer._CopyTo(_objOldPrePaidCustomer);

                        _objCusBuyPow = new TBL_CUSTOMER_BUY();
                        _objCusBuyPow.BUY_NO = Method.GetNextSequence(sequence, true);
                        _objCusBuyPow.PREPAID_CUS_ID = _objPrePaidCustomer.PREPAID_CUS_ID;
                        _objCusBuyPow.USER_CASH_DRAWER_ID = Login.CurrentCashDrawer.USER_CASH_DRAWER_ID;
                        _objCusBuyPow.PRICE = DataHelper.ParseToDecimal(txtPrice.Text);
                        _objCusBuyPow.DISCOUNT_QTY = DataHelper.ParseToDecimal(txtDiscount.Text);
                        _objCusBuyPow.CREATE_ON = dtNow;
                        _objCusBuyPow.CREATE_BY = Login.CurrentLogin.LOGIN_NAME;
                        _objCusBuyPow.BUY_TIMES = Prepaid.GetNexPurchasingTimes(_objPrePaidCustomer.BUY_TIMES);
                        _objCusBuyPow.BUY_QTY = DataHelper.ParseToDecimal(txtPurchasePower.Text);
                        _objCusBuyPow.CURRENCY_ID = _objPrice.CURRENCY_ID;
                        _objCusBuyPow.COMPENSATED = _objPrePaidCustomer.COMPENSATED;
                        _objCusBuyPow.BUY_AMOUNT = DataHelper.ParseToDecimal(txtTotalAmount.Text);
                        _objCusBuyPow.PAID_CREDIT_AMOUNT = _creditPaidAmount;
                        _objCusBuyPow.IS_VOID = false;
                        _objCusBuyPow.PARENT_ID = 0;
                        _objPrePaidCustomer.BUY_TIMES = Prepaid.GetNexPurchasingTimes(_objPrePaidCustomer.BUY_TIMES);
                        _objPrePaidCustomer.TOTAL_BUY_POWER += _objCusBuyPow.BUY_QTY;
                        _objPrePaidCustomer.COMPENSATED = 0;
                        _objCusBuyPow.EXCHANGE_RATE = exchangeRate.EXCHANGE_RATE;
                        _objCusBuyPow.EXCHANGE_RATE_DATE = exchangeRate.CREATE_ON;

                        TBL_CHANGE_LOG objChangelog = DBDataContext.Db.Update(_objOldPrePaidCustomer, _objPrePaidCustomer);
                        DBDataContext.Db.InsertChild(_objCusBuyPow, _objPrePaidCustomer, ref objChangelog);
                        /*
                         * Applied credit amount.
                         */
                        var credit = _creditPaidAmount;
                        foreach (TBL_PREPAID_CREDIT item in DBDataContext.Db.TBL_PREPAID_CREDITs.Where(x =>
                            x.CUSTOMER_ID == _objPrePaidCustomer.CUSTOMER_ID
                            && x.CREDIT_AMOUNT - x.PAID_AMOUNT > 0).OrderBy(x => x.CREDIT_MONTH))
                        {
                            if (credit <= 0)
                            {
                                break;
                            }
                            /*
                             * Currency not same
                             */
                            if (_objPrice.CURRENCY_ID != _objKhCurrency.CURRENCY_ID)
                            {
                                break;
                            }
                            var objNewItem = new TBL_PREPAID_CREDIT();
                            item._CopyTo(objNewItem);
                            /*
                             * Calculate credit amount
                             */
                            var balance = item.CREDIT_AMOUNT - item.PAID_AMOUNT;
                            var appliedAmount = 0m;
                            appliedAmount = credit > balance ? balance : credit;
                            credit -= appliedAmount;
                            /*
                             * Update TBL_PREPAID_CREDIT
                             */
                            objNewItem.PAID_AMOUNT += appliedAmount;
                            DBDataContext.Db.UpdateChild(item, objNewItem, _objPrePaidCustomer, ref objChangelog);

                            /*
                             * Insert TBL_PREPAID_CREDIT_PAID_AMOUNT;
                             */
                            var objCreditPaidAmount = new TBL_PREPAID_CREDIT_PAID_AMOUNT
                            {
                                CUSTOMER_BUY_ID = (int)_objCusBuyPow.CUSTOMER_BUY_ID,
                                PREPAID_CREDIT_ID = item.PREPAID_CREDIT_ID,
                                PAID_AMOUNT = appliedAmount,
                                IS_ACTIVE = true
                            };
                            DBDataContext.Db.InsertChild(objCreditPaidAmount, _objPrePaidCustomer, ref objChangelog);
                        }

                        //write card 
                        string strReturn = string.Empty;
                        int intReturn = 1;
                        intReturn = _objReadCard.WritePurchaseCard(_objPrePaidCustomer.CARD_CODE
                                    , _objPrePaidCustomer.BUY_TIMES, dtNow,
                                    (_objCusBuyPow.BUY_QTY + _objCusBuyPow.COMPENSATED), out strReturn);

                        if (intReturn != 0)
                        {
                            return;
                        }
                        //Validate Data in IC card
                        PowerCard objValPower = (PowerCard)ICCard.Read();
                        if (objValPower.PurchasePower != _objCusBuyPow.BUY_QTY + _objCusBuyPow.COMPENSATED)
                        {
                            throw new Exception(Resources.MS_WRITE_POWER_CARD_FAIL);
                        }
                        //if (objValPower.PurchasingTimes!=_objCusBuyPow.BUY_TIMES-1) 
                        //{
                        //    MsgBox.ShowInformation(string.Format("ចំនួនដងនៃការទិញថាមពលអគិសនីមិនបានបញ្ចូលទៅក្នុងកាត់  {0}!={1} សូមសំអាតកាត់ឡើងវិញនឹងព្យាយាមម្តង់ទៀត", objValPower.PurchasingTimes+1, _objCusBuyPow.BUY_TIMES), this.Text);
                        //    return;
                        //}  

                        tran.Complete();
                        blnSaleSuccess = true;
                    }
                }
                catch (Exception exp)
                {
                    throw exp;
                }

            });

            if (blnSaleSuccess)
            {

                try
                {
                    if (chkPRINT_RECEIPT.Checked)
                    {
                        var ch = new CrystalReportHelper("ReportReceiptPruchasePower.rpt");
                        ch.SetParameter("@CUSTOMER_BUY_ID", (int)_objCusBuyPow.CUSTOMER_BUY_ID);
                        ch.PrintReport(Settings.Default.PRINTER_RECIEPT);
                    }
                }
                catch (Exception exp)
                {
                    MsgBox.ShowDialog(string.Format(Resources.MS_FAIL_TO_PRINT_RECEIPT_PREPAID, exp.Message), "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                finally
                {
                    ResetObject();
                    btnREAD_CARD.Focus();
                }
            }


        }





    }
}