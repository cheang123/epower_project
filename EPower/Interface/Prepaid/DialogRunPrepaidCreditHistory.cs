﻿using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface.PrePaid
{
    public partial class DialogRunPrepaidCreditHistory : ExDialog
    { 
        TBL_RUN_PREPAID_CREDIT objPrepaidCredit = new TBL_RUN_PREPAID_CREDIT();
        DateTime _invoiceDate,
                 _startDate,
                 _endDate,
                 _dueDate,
                 _startPayDate = new DateTime();
        public DialogRunPrepaidCreditHistory(int runId)
        {
            InitializeComponent();

            UIHelper.SetDataSourceToComboBox(cboBillingCycle, DBDataContext.Db.TBL_BILLING_CYCLEs.Where(row => row.IS_ACTIVE));

            objPrepaidCredit = DBDataContext.Db.TBL_RUN_PREPAID_CREDITs.FirstOrDefault(row => row.RUN_PREPAID_CREDIT_ID == runId);
            var objRun = DBDataContext.Db.TBL_RUN_BILLs.FirstOrDefault(x => x.RUN_ID == objPrepaidCredit.RUN_ID);

            var objCredit =
                DBDataContext.Db.TBL_PREPAID_CREDITs.FirstOrDefault(
                    x => x.RUN_PREPAID_CREDIT_ID == objPrepaidCredit.RUN_PREPAID_CREDIT_ID);  

            if (objCredit != null)
            {
                dtpFrom.Value = objCredit.START_DATE;
                dtpTo.Value = objCredit.END_DATE;
                _invoiceDate = objCredit.CREDIT_MONTH;
                _startDate = objCredit.START_DATE;
                _endDate = objCredit.END_DATE;
            }
            dtpRunMonth.Value = objPrepaidCredit.CREDIT_MONTH;
            cboBillingCycle.SelectedValue = objRun.CYCLE_ID;
        } 

        private void btnOK_Click(object sender, EventArgs e)
        {
            DateTime fromDate = dtpFrom.Value,
                toDate = dtpTo.Value;

            Runner.RunNewThread(delegate
            {
                using (var tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    foreach (var objInv in DBDataContext.Db.TBL_PREPAID_CREDITs.Where(row => row.RUN_PREPAID_CREDIT_ID == objPrepaidCredit.RUN_PREPAID_CREDIT_ID))
                    {
                         
                        objInv.START_DATE = fromDate;
                        objInv.END_DATE = toDate;
                    }
                    DBDataContext.Db.SubmitChanges();

                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            });
        }
          

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnHistory_Click(object sender, EventArgs e)
        {

        }
        
 
    }
}
