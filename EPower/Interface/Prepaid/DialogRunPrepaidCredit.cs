﻿using System;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using Process = System.Diagnostics.Process;

namespace EPower.Interface.PrePaid
{
    public partial class DialogRunPrepaidCredit : ExDialog
    {
        bool load;
        public DialogRunPrepaidCredit()
        {
            InitializeComponent();
            load = false;
            UIHelper.SetDataSourceToComboBox(cboBillingCycle, DBDataContext.Db.TBL_BILLING_CYCLEs.Where(row => row.IS_ACTIVE));                                      
            load = true;
        }  

        private void btnOK_Click(object sender, EventArgs e)
        {
            int intBillingCycle =(int)cboBillingCycle.SelectedValue;
            DateTime datRunMonth = dtpRunMonth.Value.Date;
            DateTime datStart = dtpFrom.Value.Date;
            DateTime datEnd = dtpTo.Value.Date;

            // backup database before run bill.
            // if system could not backup database
            // billing process will be ignored.

            Runner.RunNewThread(BackupBeforeRun, Resources.MS_RUN_BILL_BACKUP);

            if (Runner.Instance.Error != null)
            {
                return;
            }
            // start running bill.
            bool success = false;
            Runner.RunNewThread(delegate
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(1, 0, 0)))
                {
                    DBDataContext.Db.CommandTimeout = 3600;
                    DBDataContext.Db.ExecuteCommand(@"EXEC RUN_PREPAID_CREDIT @MONTH={0},@START_DATE={1},@END_DATE={2},@CYCLE_ID={3},@CREATE_BY={4}",
                                                     datRunMonth,
                                                     datStart,
                                                     datEnd,
                                                     intBillingCycle,
                                                     Login.CurrentLogin.LOGIN_NAME);
                    tran.Complete();
                    success = true;
                } 
            }, Resources.MS_RUN_BILL_IN_PROGRESS);

            // if run bill is success.
            if (success)
            {
                DialogResult = DialogResult.OK; 
                sendReport();
            }
        }  

        private void sendReport()
        {
            //Format: "D:\Backup\RSC Feb\Oyadav\EPowerReporting\ReportAgent.exe" 1
            //Index0 Full name execute: "D:\Backup\RSC Feb\Oyadav\EPowerReporting\ReportAgent.exe"
            //Index1 Argument: 1
            string[] exeSchedule = Method.Utilities[Utility.EXE_SCHEDULE].Split(new[] {'"'}, StringSplitOptions.RemoveEmptyEntries);

            if (exeSchedule.Length==2&&File.Exists(exeSchedule[0]))
            {
                Process.Start(exeSchedule[0], exeSchedule[1]);   
            }    

            MsgBox.ShowInformation(Resources.MS_RUN_BILL_SUCCESS);
            DialogResult = DialogResult.OK;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }


        #region BackupFunction

        private void BackupBeforeRun()
        {
            BackupDatabase("BeforeRunPrepaidCredit");
        }
        private void BackupAfterRun()
        {
            BackupDatabase("BeforeRunPrepaidCredit");
        }

        private void BackupDatabase(string flag)
        {
            Application.DoEvents();
            string strBackupFileName = DBDataContext.Db.Connection.Database + " " + DBDataContext.Db.GetSystemDate().ToString("yyyyMMddHHmmss") + " " + flag + ".bak";
            SoftTech.Security.Logic.DBA.BackupToDisk(Settings.Default.PATH_BACKUP, strBackupFileName);
        }
        #endregion
    }
}
