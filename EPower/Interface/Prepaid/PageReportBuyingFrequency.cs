﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.IO;
using System.Windows.Forms;

namespace EPower.Interface.PrePaid
{
    public partial class PageReportBuyingFrequency : UserControl
    {
        private CrystalReportHelper ch;
        public PageReportBuyingFrequency()
        {
            InitializeComponent();
            viewer.DefaultView();
            dtpEndDate.Value = DBDataContext.Db.GetSystemDate();
            dtpDate.Value = new DateTime(dtpEndDate.Value.Year, dtpEndDate.Value.Month, 1);
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            Runner.Run(viewReport);
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            sendMail();
        }

        private void viewReport()
        {
            string strTitle = string.Format(Resources.BUYING_FREQUENCY_I, dtpDate.Value, dtpEndDate.Value);
            ch = ViewReportBuyingFrequency(dtpDate.Value.Date, dtpEndDate.Value.Date,strTitle);
            viewer.ReportSource = ch.Report;
        }

        public CrystalReportHelper ViewReportBuyingFrequency(DateTime dtStart,DateTime dtEnd,string strTitle)
        {
            CrystalReportHelper c = new CrystalReportHelper("ReportPrepaidBuyingFrequency.rpt");
            c.SetParameter("@START_DATE", dtStart);
            c.SetParameter("@END_DATE", dtEnd);
            c.SetParameter("@TITLE", strTitle);
            return c; 
        }
        private void sendMail()
        {
            try
            {
                viewReport();
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + ch.ReportName + ".xls").FullName;
                ch.ExportToExcel(exportPath);
                DialogSendMail.Instance.Add(ch.ReportName, exportPath);
                DialogSendMail.Instance.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }
    }
}
