﻿using EPower.Base.Logic;
using EPower.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Login = SoftTech.Security.Logic.Login;

namespace EPower.Interface.PrePaid
{
    public partial class PageCustomerPrepaid : UserControl
    {
        /// <summary>
        /// Determines the binding are being process on Status ComboBox and Type ComboBox.
        /// </summary>
        private bool _binding;
        private bool _searchByCard;
        private DataTable _ds;

        public PageCustomerPrepaid()
        {
            InitializeComponent();

            //Assign permission
            btnADD_SERVICE.Enabled = Login.IsAuthorized(Permission.PREPAID_REGISTER_FIXCHARGE);
            btnADD.Enabled = Login.IsAuthorized(Permission.PREPAID_REGISTER_INSERT);
            btnEDIT.Enabled = Login.IsAuthorized(Permission.PREPAID_REGISTER_UPDATE);
            btnCLOSE_CUSTOMER.Enabled = Login.IsAuthorized(Permission.PREPAID_REGISTER_CLOSE);


            UIHelper.DataGridViewProperties(dgv);

            BindCustomerType();

            BindCustomerStatus();

            txtStatus.SelectedValue = (int)CustomerStatus.Active;

            BindData();
        }

        public void BindCustomerStatus()
        {
            _binding = true;

            int backup = 0;
            if (txtStatus.SelectedIndex != -1)
            {
                backup = (int)txtStatus.SelectedValue;
            }
            DataTable source = DBDataContext.Db.TLKP_CUS_STATUS._ToDataTable();
            DataRow newRow = source.NewRow();
            newRow["CUS_STATUS_ID"] = 0;
            newRow["CUS_STATUS_NAME"] = Resources.ALL_STATUS;
            source.Rows.InsertAt(newRow, 0);
            UIHelper.SetDataSourceToComboBox(txtStatus, source);
            txtStatus.SelectedValue = backup;

            _binding = false;
        }
        public void BindCustomerType()
        {
            _binding = true;
            UIHelper.SetDataSourceToComboBox(this.cboCustomerGroup, Lookup.GetCustomerGroup(), Resources.All_CUSTOMER_GROUP);
            _binding = false;
        }
        public void BindData()
        {
            // if statust and type is in binding mode 
            // then do not bind customer list
            // wait until status and customer type 
            // are completed bound.
            if (_binding)
            {
                return;
            }

            // lock to make sure only one search wash hit to server.
            _binding = true;

            lblROW_NOT_FOUND.Hide();
            lblTYPE_TO_SEARCH.Hide();
            if (txtSearch.Text == "")
            {
                if (_ds != null)
                {
                    _ds.Rows.Clear();
                }
                lblTYPE_TO_SEARCH.Show();
                _binding = false;
                return;
            }

            int statusID = (int)txtStatus.SelectedValue;
            int typeID = (int)cboConnectionType.SelectedValue;
            int cusGroupId = (int)cboCustomerGroup.SelectedValue;

            _ds = (from cus in DBDataContext.Db.TBL_CUSTOMERs.Where(x => !x.IS_POST_PAID)
                   join area in DBDataContext.Db.TBL_AREAs on cus.AREA_ID equals area.AREA_ID
                   join connectioType in DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs on cus.CUSTOMER_CONNECTION_TYPE_ID equals connectioType.CUSTOMER_CONNECTION_TYPE_ID
                   join g in DBDataContext.Db.TLKP_CUSTOMER_GROUPs on connectioType.NONLICENSE_CUSTOMER_GROUP_ID equals g.CUSTOMER_GROUP_ID
                   join cm in
                            (
                                from m in DBDataContext.Db.TBL_METERs
                                join cm in DBDataContext.Db.TBL_CUSTOMER_METERs on m.METER_ID equals cm.METER_ID
                                join b in DBDataContext.Db.TBL_BOXes on cm.BOX_ID equals b.BOX_ID
                                join p in DBDataContext.Db.TBL_POLEs on cm.POLE_ID equals p.POLE_ID
                                where cm.IS_ACTIVE
                                select new { cm.CUSTOMER_ID, m.METER_CODE, p.POLE_CODE, b.BOX_CODE }
                                ) on cus.CUSTOMER_ID equals cm.CUSTOMER_ID into cmTmp
                   from cmt in cmTmp.DefaultIfEmpty()
                   where (cusGroupId == 0 || connectioType.NONLICENSE_CUSTOMER_GROUP_ID == cusGroupId)
                     && (typeID == 0 || cus.CUSTOMER_CONNECTION_TYPE_ID == typeID)
                     && (statusID == 0 || cus.STATUS_ID == statusID)
                     && ((cus.CUSTOMER_CODE + " " + cus.LAST_NAME_KH + " " + cus.FIRST_NAME_KH + " " + cus.LAST_NAME + " " + cus.FIRST_NAME + " " + cus.PHONE_1 + " " + cus.PHONE_2 + " " +
                          (cmt == null ? "" : cmt.POLE_CODE) + " " +
                          (cmt == null ? "" : cmt.BOX_CODE) + " " +
                          (cmt == null ? "" : cmt.METER_CODE)
                        ).ToLower().Contains(txtSearch.Text.ToLower()))
                   select new
                   {
                       cus.CUSTOMER_ID,
                       cus.CUSTOMER_CODE,
                       CUSTOMER_NAME = cus.LAST_NAME_KH + " " + cus.FIRST_NAME_KH,
                       CUSTOMER_TYPE = connectioType.CUSTOMER_CONNECTION_TYPE_NAME,
                       PHONE = cus.PHONE_2 != "" ? cus.PHONE_1 + ", " + cus.PHONE_2 : cus.PHONE_1,
                       AREA = area.AREA_NAME,
                       POLE = cmt == null ? "" : cmt.POLE_CODE,
                       BOX = cmt == null ? "" : cmt.BOX_CODE,
                       METER = cmt == null ? "" : cmt.METER_CODE,
                       CUSTOMER_STATUS = cus.STATUS_ID
                   }).Take(24)._ToDataTable();

            dgv.DataSource = _ds;
            if (dgv.Rows.Count == 0)
            {
                lblROW_NOT_FOUND.Show();
            }

            //set ForeColor to Red if customer is close.
            foreach (DataGridViewRow item in dgv.Rows)
            {
                if ((int)item.Cells["CUSTOMER_STATUS"].Value == (int)CustomerStatus.Closed)
                {
                    item.DefaultCellStyle.ForeColor = Color.Red;
                }
            }
            _binding = false;

        }
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count == 0)
            {
                return;
            }
            int id = (int)dgv.SelectedRows[0].Cells["CUSTOMER_ID"].Value;
            TBL_CUSTOMER obj = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == id);
            if (obj != null)
            {
                DialogCustomer diag = new DialogCustomer(GeneralProcess.Update, obj);
                if (diag.ShowDialog(this) == DialogResult.OK)
                {
                    BindData();
                    UIHelper.SelectRow(dgv, diag.Object.CUSTOMER_ID);
                }
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            DialogCustomer diag = new DialogCustomer(GeneralProcess.Insert, new TBL_CUSTOMER());
            if (diag.ShowDialog(this) == DialogResult.OK)
            {
                BindData();
                UIHelper.SelectRow(dgv, diag.Object.CUSTOMER_ID);
            }
        }

        private void btnAddCharge_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count == 0)
            {
                return;
            }

            int id = (int)dgv.SelectedRows[0].Cells["CUSTOMER_ID"].Value;

            TBL_CUSTOMER obj = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == id);

            if (obj.STATUS_ID == (int)CustomerStatus.Closed)
            {
                MsgBox.ShowInformation(Resources.MS_CANNOT_ADD_CHARGE_TO_CLOSE_CUSTOMER);
                return;
            }
            if (obj.STATUS_ID == (int)CustomerStatus.Cancelled)
            {
                MsgBox.ShowInformation(Resources.MS_CANNOT_ADD_CHARGE_TO_CANCEL_CUSTOMER);
                return;
            }
            if (obj != null)
            {
                (new DialogCustomerCharge(obj, GeneralProcess.Insert)).ShowDialog();
            }
        }

        private void txtCustomerType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

        private void txtStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (txtStatus.SelectedIndex != -1)
            {
                int statusID = (int)txtStatus.SelectedValue;
                btnADD_SERVICE.Enabled = statusID._In(0, (int)CustomerStatus.Active, (int)CustomerStatus.Blocked)
                    && Login.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_REGISTER_FIXCHARGE);

                btnCLOSE_CUSTOMER.Enabled = statusID._In(0, (int)CustomerStatus.Active, (int)CustomerStatus.Blocked)
                    && Login.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_REGISTER_CLOSE);
            }
            BindData();
        }

        private void exTextbox1_QuickSearch(object sender, EventArgs e)
        {
            BindData();
        }

        private void btnCloseCustomer_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count == 0)
            {
                return;
            }
            int intCustomerId = int.Parse(dgv.SelectedRows[0].Cells["CUSTOMER_ID"].Value.ToString());
            TBL_CUSTOMER obj = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(c => c.CUSTOMER_ID == intCustomerId);

            if (obj.STATUS_ID == (int)CustomerStatus.Closed)
            {
                MsgBox.ShowInformation(Resources.MS_CUSTOMER_ALREADY_CLOSE);
                return;
            }

            if (obj.STATUS_ID == (int)CustomerStatus.Cancelled)
            {
                MsgBox.ShowInformation(Resources.MS_CUSTOMER_ALREDY_CANCEL);
                return;
            }
            if (obj.STATUS_ID == (int)CustomerStatus.Pending)
            {
                MsgBox.ShowInformation(Resources.MS_CUSTOMER_PENDING);
                return;
            }


            DialogCloseCustomer diaglogCloseCustomer = new DialogCloseCustomer(obj);
            if (diaglogCloseCustomer.ShowDialog() == DialogResult.OK)
            {
                BindData();
            }
        }

        private void txtSearch_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void btnSwitch_Click(object sender, EventArgs e)
        {
            _searchByCard = !_searchByCard;
            pnlGeneral.Visible = !_searchByCard;

        }

        private void btnReadCard_Click(object sender, EventArgs e)
        {
            ICCard objReadCard = ICCard.Read();
            switch ((CardType)objReadCard.ICCardType)
            {
                case CardType.HavePowerCard:
                case CardType.NoPowerCard:
                    string strCusCode = ((PowerCard)objReadCard).CustomerCode;
                    TBL_CUSTOMER cusInfo = (from c in DBDataContext.Db.TBL_CUSTOMERs
                                            join pc in DBDataContext.Db.TBL_PREPAID_CUSTOMERs on c.CUSTOMER_ID equals pc.CUSTOMER_ID
                                            where pc.CARD_CODE.ToLower().Contains(strCusCode.ToLower())
                                            select c).FirstOrDefault();

                    txtSearch.Text = cusInfo.CUSTOMER_CODE;
                    BindData();
                    break;
                case CardType.FormattedCard:
                case CardType.ResetCard:
                case CardType.PresetCard:
                case CardType.TestCard:
                case CardType.CutCard:
                    break;
                default:
                    break;
            }
        }

        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }

        private void cboCustomerGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            int groupId = DataHelper.ParseToInt(cboCustomerGroup.SelectedValue.ToString());
            UIHelper.SetDataSourceToComboBox(cboConnectionType, DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs
                                                                .Where(x => x.NONLICENSE_CUSTOMER_GROUP_ID == groupId)
                                                                .OrderBy(x => x.DESCRIPTION)
                                                                , Resources.ALL_CONNECTION_TYPE);
            if (cboConnectionType.DataSource != null)
            {
                cboConnectionType.SelectedIndex = 0;
            }
        }

        private void cboConnectionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }
    }
}
