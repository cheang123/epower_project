﻿using EPower.Base.Logic;
using EPower.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface.PrePaid
{
    public partial class DialogCustomerActivatePrepaid : ExDialog
    {
        #region Data
        TBL_CUSTOMER _objCustomerNew = new TBL_CUSTOMER();
        TBL_CUSTOMER _objCustomerOld = new TBL_CUSTOMER();
        TBL_CUSTOMER _objCusReadCard;
        TBL_PRICE _objPrice;

        TBL_PREPAID_CUSTOMER _objPreCustomer = new TBL_PREPAID_CUSTOMER();
        TBL_CUSTOMER_BUY _objCusBuyPow = new TBL_CUSTOMER_BUY();

        TBL_CUSTOMER_METER _objCustomerMeterNew = new TBL_CUSTOMER_METER();
        TBL_CUS_STATUS_CHANGE _objCustomerStatusChange = new TBL_CUS_STATUS_CHANGE();

        TBL_BOX _objBoxNew;
        TBL_BOX _objBoxOld;
        TBL_USAGE _objUsageNew;
        TBL_METER _objMeterNew;
        TBL_METER _objMeterOld;
        TBL_CIRCUIT_BREAKER _objBreakerNew;
        TBL_CIRCUIT_BREAKER _objBreakerOld;
        TBL_POLE _objPole;

        TBL_USER_CASH_DRAWER _objUserCashDrawer;
        #endregion Data

        #region Constructor
        public DialogCustomerActivatePrepaid(TBL_CUSTOMER objCustomer)
        {
            InitializeComponent();

            objCustomer._CopyTo(_objCustomerNew);
            objCustomer._CopyTo(_objCustomerOld);
            read();

        }
        #endregion Constructor

        #region Method  
        bool invalid()
        {
            bool blnReturn = false;


            if (txtMeterType.Text.Trim() == string.Empty)
            {
                txtMeter.SetValidation(string.Format(Resources.REQUIRED, lblMETER_CODE.Text));
                blnReturn = true;
            }
            if (cboMeterShield.SelectedIndex == -1)
            {
                cboMeterShield.SetValidation(string.Format(Resources.REQUIRED, lblSHIELD.Text));
                blnReturn = true;
            }
            if (cboCableShield.SelectedIndex == -1)
            {
                cboCableShield.SetValidation(string.Format(Resources.REQUIRED, lblCABLE_SHIELD.Text));
                blnReturn = true;
            }
            if (txtBreakerType.Text.Trim() == string.Empty)
            {
                txtBreaker.SetValidation(string.Format(Resources.REQUIRED, lblBREAKER_CODE.Text));
                blnReturn = true;
            }
            if (txtPoleCode.Text.Trim() == string.Empty)
            {
                txtBox.SetValidation(string.Format(Resources.REQUIRED, lblBOX.Text));
                blnReturn = true;
            }

            if (!DataHelper.IsNumber(txtBuyPower.Text))
            {
                txtBuyPower.SetValidation(string.Format(Resources.REQUIRED, lblBUY_POWER.Text));
                return true;
            }
            if (int.Parse(txtBuyPower.Text) < 0)
            {
                txtBuyPower.SetValidation(Resources.REQUEST_GRATER_THAN_ZERO);
                return true;
            }
            if (!DataHelper.IsNumber(txtDiscount.Text))
            {
                txtDiscount.SetValidation(string.Format(Resources.REQUIRED, lblDISCOUNT.Text));
                return true;
            }
            if (int.Parse(txtDiscount.Text) < 0)
            {
                txtDiscount.SetValidation(Resources.REQUEST_GRATER_THAN_ZERO);
                return true;
            }

            if (!DataHelper.IsNumber(txtSTART_USAGE.Text))
            {
                txtSTART_USAGE.SetValidation(string.Format(Resources.REQUIRED, lblSTART_USAGE.Text));
                blnReturn = true;
            }

            if (int.Parse(txtDiscount.Text) > int.Parse(txtBuyPower.Text))
            {
                txtBuyPower.SetValidation(string.Format(Resources.MS_VALIDATE_START_END_DATE, lblBUY_POWER.Text, lblDISCOUNT.Text));
                blnReturn = true;
            }

            try
            {
                ICCard objReadCard = ICCard.Read();
                if ((CardType)objReadCard.ICCardType != CardType.FormattedCard && (CardType)objReadCard.ICCardType != CardType.BlankCard)
                {
                    MsgBox.ShowInformation(Resources.MS_PLEASE_INSERT_NEW_CARD);
                    blnReturn = true;
                }
            }
            catch (Exception e)
            {
                MsgBox.ShowError(e);
                blnReturn = true;
            }

            return blnReturn;
        }

        void bind()
        {
            _objPrice = DBDataContext.Db.TBL_PRICEs.FirstOrDefault(x => x.PRICE_ID == _objCustomerNew.PRICE_ID);
            txtPriceName.Text = _objPrice.PRICE_NAME;
            if (_objPrice.CURRENCY_ID == 1)
            {
                txtUnitPrice.Text = txtPrice.Text = DBDataContext.Db.TBL_PRICE_DETAILs.FirstOrDefault(x => x.PRICE_ID == _objCustomerNew.PRICE_ID).PRICE.ToString("N0");
            }
            else
            {
                txtUnitPrice.Text = txtPrice.Text = DBDataContext.Db.TBL_PRICE_DETAILs.FirstOrDefault(x => x.PRICE_ID == _objCustomerNew.PRICE_ID).PRICE.ToString("N5");
            }
            txtSignPrice.Text = txtSignTotalAmount.Text = txtSignUnitPrice.Text = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.CURRENCY_ID == _objPrice.CURRENCY_ID).CURRENCY_SING;

            UIHelper.SetDataSourceToComboBox(cboMeterShield, DBDataContext.Db.TBL_SEALs.Where(s => s.IS_ACTIVE));
            UIHelper.SetDataSourceToComboBox(cboCableShield, DBDataContext.Db.TBL_SEALs.Where(s => s.IS_ACTIVE));
            _objUserCashDrawer = Login.CurrentCashDrawer;
        }

        private void read()
        {
            bind();
            dtpActivateDate.Value = DBDataContext.Db.GetSystemDate();
            txtCashDrawerName.Text =
                DBDataContext.Db.TBL_CASH_DRAWERs.FirstOrDefault(
                    x => x.CASH_DRAWER_ID == _objUserCashDrawer.CASH_DRAWER_ID).CASH_DRAWER_NAME;
            txtUserCashDrawerName.Text = Login.CurrentLogin.LOGIN_NAME;
            txtLastNamekh.Text = _objCustomerNew.LAST_NAME_KH + " " + _objCustomerNew.FIRST_NAME_KH;
            txtCustomerType.Text =
                DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs.Single(cct => cct.CUSTOMER_CONNECTION_TYPE_ID == _objCustomerNew.CUSTOMER_CONNECTION_TYPE_ID)
                    .CUSTOMER_CONNECTION_TYPE_NAME;
            // reactivate
            if (_objCustomerNew.STATUS_ID == (int)CustomerStatus.Closed)
            {
                var objLastCusMeter = DBDataContext.Db.TBL_CUSTOMER_METERs
                    .Where(x => x.CUSTOMER_ID == _objCustomerNew.CUSTOMER_ID)
                    .OrderByDescending(x => x.CUS_METER_ID)
                    .FirstOrDefault();
                var objMeter = DBDataContext.Db.TBL_METERs.FirstOrDefault(x => x.METER_ID == objLastCusMeter.METER_ID);
                var objBreaker =
                    DBDataContext.Db.TBL_CIRCUIT_BREAKERs.FirstOrDefault(x => x.BREAKER_ID == objLastCusMeter.BREAKER_ID);
                var objBox = DBDataContext.Db.TBL_BOXes.FirstOrDefault(x => x.BOX_ID == objLastCusMeter.BOX_ID);

                if (objMeter != null)
                {
                    txtMeter.Text = objMeter.METER_CODE;
                    txtMeter.AcceptSearch(true);
                }
                if (objBreaker != null)
                {
                    txtBreaker.Text = objBreaker.BREAKER_CODE;
                    txtBreaker.AcceptSearch(true);
                }
                if (objBox != null)
                {
                    txtBox.Text = objBox.BOX_CODE;
                    txtBox.AcceptSearch(true);
                }


                //find last usage
                var objUsage = DBDataContext.Db.TBL_USAGEs.Where(x => x.CUSTOMER_ID == _objCustomerNew.CUSTOMER_ID)
                    .OrderByDescending(x => x.USAGE_MONTH)
                    .ThenByDescending(x => x.POSTING_DATE)
                    .FirstOrDefault();
                if (objUsage != null)
                {
                    txtSTART_USAGE.Text = objUsage.END_USAGE.ToString("#");
                }
            }
        }

        #endregion Method

        #region Event
        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (invalid())
            {
                return;
            }

            write();

            saveData();
        }

        private void write()
        {
            DateTime dtNow = DBDataContext.Db.GetSystemDate();
            // update customer.
            _objCustomerNew.ACTIVATE_DATE = dtNow;
            _objCustomerNew.STATUS_ID = (int)CustomerStatus.Active;
            // insert customer meter.
            _objCustomerMeterNew.BOX_ID = _objBoxNew.BOX_ID;
            _objCustomerMeterNew.BREAKER_ID = _objBreakerNew.BREAKER_ID;
            _objCustomerMeterNew.CABLE_SHIELD_ID = int.Parse(cboCableShield.SelectedValue.ToString());
            _objCustomerMeterNew.CUSTOMER_ID = _objCustomerNew.CUSTOMER_ID;
            _objCustomerMeterNew.METER_ID = _objMeterNew.METER_ID;
            _objCustomerMeterNew.METER_SHIELD_ID = int.Parse(cboMeterShield.SelectedValue.ToString());
            _objCustomerMeterNew.POLE_ID = _objPole.POLE_ID;
            _objCustomerMeterNew.IS_ACTIVE = true;
            _objCustomerMeterNew.USED_DATE = _objCustomerNew.ACTIVATE_DATE;
            _objCustomerMeterNew.REMAIN_USAGE = 0;

            // inser start usage
            DateTime datStart = UIHelper._DefaultDate;
            DateTime datEnd = UIHelper._DefaultDate;
            DateTime datMonth = Method.GetNextBillingMonth(_objCustomerNew.BILLING_CYCLE_ID, ref datStart, ref datEnd);
            _objUsageNew = new TBL_USAGE();
            _objUsageNew.COLLECTOR_ID = 0;
            _objUsageNew.CREATE_BY = Login.CurrentLogin.LOGIN_NAME;
            _objUsageNew.CUSTOMER_ID = _objCustomerNew.CUSTOMER_ID;
            _objUsageNew.DEVICE_ID = 0;
            _objUsageNew.END_USAGE = DataHelper.ParseToDecimal(txtSTART_USAGE.Text); ;
            _objUsageNew.END_USE_DATE = datEnd;
            _objUsageNew.IS_METER_RENEW_CYCLE = false;
            _objUsageNew.METER_ID = _objMeterNew.METER_ID;
            _objUsageNew.POSTING_DATE = DBDataContext.Db.GetSystemDate();
            _objUsageNew.START_USAGE = DataHelper.ParseToDecimal(txtSTART_USAGE.Text);
            _objUsageNew.START_USE_DATE = datStart;
            _objUsageNew.USAGE_MONTH = datMonth;
            _objUsageNew.MULTIPLIER = _objMeterNew.MULTIPLIER;
            // insert customer status change
            _objCustomerStatusChange.CUSTOMER_ID = _objCustomerNew.CUSTOMER_ID;
            _objCustomerStatusChange.CHNAGE_DATE = _objCustomerNew.ACTIVATE_DATE;
            _objCustomerStatusChange.CREATE_BY = SoftTech.Security.Logic.Login.CurrentLogin.LOGIN_NAME;
            _objCustomerStatusChange.NEW_STATUS_ID = _objCustomerNew.STATUS_ID;
            _objCustomerStatusChange.OLD_STATUS_ID = _objCustomerOld.STATUS_ID;
            _objCustomerStatusChange.INVOICE_ID = 0;
            // insert Prepaid customer
            _objPreCustomer = DBDataContext.Db.TBL_PREPAID_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == _objCustomerNew.CUSTOMER_ID);
            if (_objPreCustomer == null)
            {
                _objPreCustomer = new TBL_PREPAID_CUSTOMER();
                _objPreCustomer.CUSTOMER_ID = _objCustomerNew.CUSTOMER_ID;
                _objPreCustomer.CARD_CODE = _objCustomerNew.CUSTOMER_ID.ToString();
                _objPreCustomer.BUY_TIMES = 1;
                _objPreCustomer.ALARM_METHOD = 0;//Not cut Power                    
                _objPreCustomer.PRICE_ID = _objCustomerNew.PRICE_ID;
                _objPreCustomer.TOTAL_BUY_POWER = DataHelper.ParseToDecimal(txtBuyPower.Text);
            }
            else
            {
                _objPreCustomer.BUY_TIMES = _objPreCustomer.BUY_TIMES + 1;
                _objPreCustomer.TOTAL_BUY_POWER = _objPreCustomer.TOTAL_BUY_POWER + DataHelper.ParseToDecimal(txtBuyPower.Text);
            }

            // insert customer buy
            var exchangeRate = new ExchangeRateLogic().findLastExchangeRate(DBDataContext.Db.GetSystemDate(), _objPrice.CURRENCY_ID);
            Sequence sequence = Method.GetSequenceId(SequenceType.Prepaid);
            _objCusBuyPow = new TBL_CUSTOMER_BUY();
            _objCusBuyPow.BUY_NO = Method.GetNextSequence(sequence, true);
            _objCusBuyPow.USER_CASH_DRAWER_ID = Login.CurrentCashDrawer.USER_CASH_DRAWER_ID;
            _objCusBuyPow.CREATE_ON = dtNow;
            _objCusBuyPow.CREATE_BY = Login.CurrentLogin.LOGIN_NAME;
            _objCusBuyPow.BUY_TIMES = _objPreCustomer.BUY_TIMES;
            _objCusBuyPow.BUY_QTY = DataHelper.ParseToDecimal(txtBuyPower.Text);
            _objCusBuyPow.PRICE = DataHelper.ParseToDecimal(txtUnitPrice.Text);
            _objCusBuyPow.CURRENCY_ID = _objPrice.CURRENCY_ID;
            _objCusBuyPow.DISCOUNT_QTY = DataHelper.ParseToDecimal(txtDiscount.Text);
            _objCusBuyPow.BUY_AMOUNT = DataHelper.ParseToDecimal(txtTotalAmount.Text);
            _objCusBuyPow.IS_VOID = false;
            _objCusBuyPow.PARENT_ID = 0;
            _objCusBuyPow.EXCHANGE_RATE = exchangeRate.EXCHANGE_RATE;
            _objCusBuyPow.EXCHANGE_RATE_DATE = exchangeRate.CREATE_ON;
        }

        private void saveData()
        {
            bool blnSaleSuccess = false;

            Runner.RunNewThread(() =>
            {
                try
                {

                    using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                    {
                        // update customer.
                        TBL_CHANGE_LOG objLog = DBDataContext.Db.Update(_objCustomerOld, _objCustomerNew);

                        // insert customer meter.
                        DBDataContext.Db.InsertChild(_objCustomerMeterNew, _objCustomerOld, ref objLog);

                        // inser start usage
                        DBDataContext.Db.TBL_USAGEs.InsertOnSubmit(_objUsageNew);

                        // insert customer status change
                        DBDataContext.Db.TBL_CUS_STATUS_CHANGEs.InsertOnSubmit(_objCustomerStatusChange);

                        // insert Prepaid customer
                        if (_objPreCustomer.PREPAID_CUS_ID == 0)
                        {
                            DBDataContext.Db.TBL_PREPAID_CUSTOMERs.InsertOnSubmit(_objPreCustomer);
                        }

                        DBDataContext.Db.SubmitChanges();

                        _objCusBuyPow.PREPAID_CUS_ID = _objPreCustomer.PREPAID_CUS_ID;
                        DBDataContext.Db.TBL_CUSTOMER_BUYs.InsertOnSubmit(_objCusBuyPow);
                        DBDataContext.Db.SubmitChanges();

                        //write card 
                        string strReturn = "";
                        int intReturn = 1;

                        //write new cusotmer
                        intReturn = new PowerCard().WriteNewCustomer(Prepaid.GetAreaCode()
                                        , _objPreCustomer.CUSTOMER_ID.ToString(), Prepaid.GetMeterType(_objMeterNew)
                                        , Prepaid.GetAlarmQTY(), _objCusBuyPow.CREATE_ON, out strReturn);
                        //Purchase power
                        if (intReturn != 0) //if write new cutomer fail!
                        {
                            return;
                        }

                        ICCard objReadCard = ICCard.Read();
                        PowerCard objCard = (PowerCard)objReadCard;

                        intReturn = new PowerCard().WritePurchaseCard(objCard.CustomerCode
                                    , _objCusBuyPow.BUY_TIMES
                                    , _objCusBuyPow.CREATE_ON, _objCusBuyPow.BUY_QTY, out strReturn);

                        if (intReturn != 0) //if purchase power fail!
                        {
                            return;
                        }

                        //Validate Data in IC card
                        PowerCard objValPower = (PowerCard)ICCard.Read();
                        if (objValPower.PurchasePower != _objCusBuyPow.BUY_QTY)
                        {
                            throw new Exception(Resources.MS_WRITE_POWER_CARD_FAIL);
                            //MsgBox.ShowInformation(Resources.MS_WRITE_POWER_CARD_FAIL, Resources.IC_CARD_MAKE_FAIL);
                            //return;
                        }
                        //if (objValPower.PurchasingTimes != _objCusBuyPow.BUY_TIMES - 1)
                        //{
                        //    MsgBox.ShowInformation(string.Format("ចំនួនដងនៃការទិញថាមពលអគិសនីមិនបានបញ្ចូលទៅក្នុងកាត់  {0}!={1} សូមសំអាតកាត់ឡើងវិញនឹងព្យាយាមម្តង់ទៀត", objValPower.PurchasingTimes + 1, _objCusBuyPow.BUY_TIMES), "បង្កើតកាត់អតិថិជនថ្មីមិនជោគជ័យ");
                        //    return;
                        //} 


                        // update meter status
                        _objBoxNew.STATUS_ID = (int)BoxStatus.Used;
                        _objMeterNew.STATUS_ID = (int)MeterStatus.Used;
                        _objBreakerNew.STATUS_ID = (int)BreakerStatus.Used;
                        // update card code
                        _objPreCustomer.CARD_CODE = objCard.CustomerCode;
                        DBDataContext.Db.UpdateChild(_objMeterOld, _objMeterNew, _objCustomerOld, ref objLog);
                        DBDataContext.Db.UpdateChild(_objBreakerOld, _objBreakerNew, _objCustomerOld, ref objLog);
                        DBDataContext.Db.UpdateChild(_objBoxOld, _objBoxNew, _objCustomerOld, ref objLog);

                        // log mete status change
                        DBDataContext.Db.TBL_STOCK_TRANs.InsertOnSubmit(new TBL_STOCK_TRAN
                        {
                            STOCK_TRAN_TYPE_ID = (int)StockTranType.Use,
                            FROM_STOCK_TYPE_ID = (int)StockType.Stock,
                            TO_STOCK_TYPE_ID = (int)StockType.Used,
                            CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                            CREATE_ON = DBDataContext.Db.GetSystemDate(),
                            ITEM_ID = _objMeterNew.METER_TYPE_ID,
                            ITEM_TYPE_ID = (int)StockItemType.Meter,
                            REMARK = _objMeterNew.METER_CODE,
                            QTY = 1,
                            UNIT_PRICE = 0
                        });
                        DBDataContext.Db.SubmitChanges();

                        // log mete status change
                        DBDataContext.Db.TBL_STOCK_TRANs.InsertOnSubmit(new TBL_STOCK_TRAN
                        {
                            STOCK_TRAN_TYPE_ID = (int)StockTranType.Use,
                            FROM_STOCK_TYPE_ID = (int)StockType.Stock,
                            TO_STOCK_TYPE_ID = (int)StockType.Used,
                            CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                            CREATE_ON = DBDataContext.Db.GetSystemDate(),
                            ITEM_ID = _objBreakerNew.BREAKER_TYPE_ID,
                            ITEM_TYPE_ID = (int)StockItemType.Breaker,
                            REMARK = _objBreakerNew.BREAKER_CODE,
                            QTY = 1,
                            UNIT_PRICE = 0
                        });
                        DBDataContext.Db.SubmitChanges();

                        //remove meter from TBL_METER_UNKNOWN
                        Method.RemoveUnknownMeter(_objMeterNew.METER_CODE);
                        tran.Complete();
                        blnSaleSuccess = true;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            });

            if (blnSaleSuccess)
            {
                if (chkPRINT_RECEIPT.Checked)
                {
                    CrystalReportHelper ch = new CrystalReportHelper("ReportReceiptPruchasePower.rpt");
                    int i = 0, buy = 0, dis = 0, com = 0;
                    if (_objCusBuyPow.BUY_QTY > 0)
                    {
                        buy = ++i;
                    }
                    if (_objCusBuyPow.DISCOUNT_QTY > 0)
                    {
                        dis = ++i;
                    }
                    if (_objCusBuyPow.COMPENSATED > 0)
                    {
                        com = ++i;
                    }

                    ch.SetParameter("@CUSTOMER_BUY_ID", (int)_objCusBuyPow.CUSTOMER_BUY_ID);
                    ch.SetParameter("@BUY", buy);
                    ch.SetParameter("@DISCOUNT", dis);
                    ch.SetParameter("@COMPENSATED", com);
                    ch.PrintReport();
                }
                DialogResult = DialogResult.OK;
            }
        }

        private void txtMeter_AdvanceSearch(object sender, EventArgs e)
        {
            // if not entry.
            if (txtMeter.Text.Trim() == "")
            {
                txtMeter.CancelSearch();
                return;
            }

            string strMeterCode = Method.FormatMeterCode(txtMeter.Text);
            TBL_METER tmp = DBDataContext.Db.TBL_METERs.FirstOrDefault(m => m.METER_CODE == strMeterCode);

            // if not contain in database.
            if (tmp == null)
            {
                //if user have permission to add new meter
                if (SoftTech.Security.Logic.Login.IsAuthorized(Permission.ADMIN_METER))
                {
                    //if user agree to add new meter
                    if (MsgBox.ShowQuestion(string.Format(Resources.MSQ_CREATE_METER, strMeterCode), "") == DialogResult.Yes)
                    {
                        DialogMeter diagMeter = new DialogMeter(GeneralProcess.Insert, new TBL_METER { METER_CODE = strMeterCode });
                        diagMeter.ShowDialog();
                        if (diagMeter.DialogResult == DialogResult.OK)
                        {
                            tmp = diagMeter.Meter;
                        }
                        else
                        {
                            txtMeter.CancelSearch();
                            return;
                        }
                    }
                    else
                    {
                        txtMeter.CancelSearch();
                        return;
                    }
                }
                else
                {
                    MsgBox.ShowInformation(Resources.MS_METER_NOT_FOUND);
                    txtMeter.CancelSearch();
                    return;
                }
            }

            // if meter is inuse.
            if (tmp.STATUS_ID == (int)MeterStatus.Used)
            {
                MsgBox.ShowInformation(Resources.MS_METER_STATUS_IN_USE);
                txtMeter.CancelSearch();
                return;
            }

            if (tmp.STATUS_ID == (int)MeterStatus.Unavailable)
            {
                MsgBox.ShowInformation(Resources.MS_METER_IS_UNAVAILABLE);
                txtMeter.CancelSearch();
                return;
            }

            _objMeterNew = new TBL_METER();
            _objMeterOld = new TBL_METER();
            tmp._CopyTo(_objMeterNew);
            tmp._CopyTo(_objMeterOld);

            TBL_METER_TYPE objMeterType = DBDataContext.Db.TBL_METER_TYPEs.FirstOrDefault(mt => mt.METER_TYPE_ID == _objMeterNew.METER_TYPE_ID);
            txtMeterType.Text = objMeterType.METER_TYPE_NAME;
            txtSTART_USAGE.Focus();
            //txtBreaker.Focus();
        }

        private void txtBreaker_AdvanceSearch(object sender, EventArgs e)
        {
            if (txtBreaker.Text.Trim() == "")
            {
                txtBreaker.CancelSearch();
                return;
            }

            string strBreakerCode = txtBreaker.Text;
            TBL_CIRCUIT_BREAKER tmp = DBDataContext.Db.TBL_CIRCUIT_BREAKERs.FirstOrDefault(b => b.BREAKER_CODE == strBreakerCode);

            if (tmp == null)
            {
                //if user have permission to add new breaker
                if (SoftTech.Security.Logic.Login.IsAuthorized(Permission.ADMIN_CIRCUITBREAKER))
                {
                    //if user agree to add new breaker
                    if (MsgBox.ShowQuestion(string.Format(Resources.MSQ_CREATE_BREAKER, strBreakerCode), "") == DialogResult.Yes)
                    {
                        DialogCircuitBreaker diagBreaker = new DialogCircuitBreaker(GeneralProcess.Insert, new TBL_CIRCUIT_BREAKER { BREAKER_CODE = strBreakerCode });
                        diagBreaker.ShowDialog();
                        if (diagBreaker.DialogResult == DialogResult.OK)
                        {
                            tmp = diagBreaker.CircuitBraker;
                        }
                        else
                        {
                            txtBreaker.CancelSearch();
                            return;
                        }
                    }
                    else
                    {
                        txtBreaker.CancelSearch();
                        return;
                    }
                }
                else
                {
                    MsgBox.ShowInformation(Resources.MS_BREAKER_NOT_FOUND);
                    txtBreaker.CancelSearch();
                    return;
                }
            }

            if (tmp.STATUS_ID == (int)BreakerStatus.Used)
            {
                MsgBox.ShowInformation(Resources.MS_BREAKER_IS_IN_USE);
                txtBreaker.CancelSearch();
                return;
            }

            if (tmp.STATUS_ID == (int)BreakerStatus.Unavailable)
            {
                MsgBox.ShowInformation(Resources.MS_BREAKER_IS_UNAVAILABLE);
                txtBreaker.CancelSearch();
                return;
            }
            _objBreakerNew = new TBL_CIRCUIT_BREAKER();
            _objBreakerOld = new TBL_CIRCUIT_BREAKER();
            tmp._CopyTo(_objBreakerOld);
            tmp._CopyTo(_objBreakerNew);

            TBL_CIRCUIT_BREAKER_TYPE objBreakerType = DBDataContext.Db.TBL_CIRCUIT_BREAKER_TYPEs.FirstOrDefault(bt => bt.BREAKER_TYPE_ID == _objBreakerNew.BREAKER_TYPE_ID);
            txtBreakerType.Text = objBreakerType.BREAKER_TYPE_NAME;
            txtBox.Focus();
        }

        private void txtBox_AdvanceSearch(object sender, EventArgs e)
        {
            if (txtBox.Text.Trim() == "")
            {
                txtBox.CancelSearch();
                return;
            }

            string strBox = txtBox.Text;
            TBL_BOX tmp = (from b in DBDataContext.Db.TBL_BOXes
                           join p in DBDataContext.Db.TBL_POLEs on b.POLE_ID equals p.POLE_ID
                           join a in DBDataContext.Db.TBL_AREAs on p.AREA_ID equals a.AREA_ID
                           where b.BOX_CODE.ToLower() == strBox.ToLower()// && a.AREA_ID == _objCustomerNew.AREA_ID
                           select b).FirstOrDefault();

            if (tmp == null)
            {
                //if user have permission to add new breaker
                if (SoftTech.Security.Logic.Login.IsAuthorized(Permission.ADMIN_POLEANDBOX))
                {
                    //if user agree to add new breaker
                    if (MsgBox.ShowQuestion(string.Format(Resources.MSQ_CREATE_BOX, strBox), "") == DialogResult.Yes)
                    {
                        DialogBox diagBox = new DialogBox(GeneralProcess.Insert, new TBL_BOX() { BOX_CODE = strBox, STARTED_DATE = DBDataContext.Db.GetSystemDate(), END_DATE = UIHelper._DefaultDate });
                        diagBox.ShowDialog();
                        if (diagBox.DialogResult == DialogResult.OK)
                        {
                            tmp = diagBox.Box;
                        }
                        else
                        {
                            txtBox.CancelSearch();
                            return;
                        }
                    }
                    else
                    {
                        txtBox.CancelSearch();
                        return;
                    }
                }
                else
                {
                    MsgBox.ShowInformation(Resources.MS_BOX_NOT_FOUND);
                    txtBox.CancelSearch();
                    return;
                }
            }

            if (tmp.STATUS_ID == (int)BoxStatus.Unavailable)
            {
                MsgBox.ShowInformation(Resources.MS_BOX_IS_UNAVAILABLE);
                txtBox.CancelSearch();
                return;
            }

            _objBoxNew = new TBL_BOX();
            _objBoxOld = new TBL_BOX();
            tmp._CopyTo(_objBoxOld);
            tmp._CopyTo(_objBoxNew);

            _objPole = DBDataContext.Db.TBL_POLEs.FirstOrDefault(p => p.POLE_ID == _objBoxNew.POLE_ID);

            if (_objCustomerNew.AREA_ID != _objPole.AREA_ID)
            {
                if (MsgBox.ShowQuestion(Resources.MSQ_NEW_BOX_NOT_IN_AREA_OF_CUSTOMER, string.Empty) != DialogResult.Yes)
                {
                    txtBox.CancelSearch();
                    return;
                }
            }

            txtPoleCode.Text = _objPole.POLE_CODE;
            txtBuyPower.Focus();
        }

        private void txtMeter_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void txtMeter_CancelAdvanceSearch(object sender, EventArgs e)
        {
            _objMeterNew = null;
            _objMeterOld = null;

            txtMeterType.Text = string.Empty;
        }

        private void txtBreaker_CancelAdvanceSearch(object sender, EventArgs e)
        {
            _objBreakerNew = null;
            _objBreakerOld = null;

            txtBreakerType.Text = string.Empty;
        }

        private void txtBox_CancelAdvanceSearch(object sender, EventArgs e)
        {
            _objPole = null;
            _objBoxNew = null;
            _objBoxOld = null;
            txtPoleCode.Text = string.Empty;
        }



        private void txtCalAmount(object sender, EventArgs e)
        {
            CalculatedAmount();
        }

        private void CalculatedAmount()
        {
            decimal decBuyPower = 0,
                decPrice = 0,
                decDiscount = 0,
                decTotalAmount = 0;
            if (txtBuyPower.Text.Trim() == string.Empty)
            {
                txtBuyPower.Text = "0";
                txtBuyPower.SelectAll();
            }
            if (txtUnitPrice.Text.Trim() == string.Empty)
            {
                txtUnitPrice.Text = "0";
                txtUnitPrice.SelectAll();
            }
            if (txtDiscount.Text.Trim() == string.Empty)
            {
                txtDiscount.Text = "0";
                txtDiscount.SelectAll();
            }
            decBuyPower = DataHelper.ParseToDecimal(txtBuyPower.Text);
            decPrice = DataHelper.ParseToDecimal(txtUnitPrice.Text);
            decDiscount = DataHelper.ParseToDecimal(txtDiscount.Text);

            decTotalAmount = (decBuyPower - decDiscount) * decPrice;

            _objPrice = DBDataContext.Db.TBL_PRICEs.FirstOrDefault(x => x.PRICE_ID == _objCustomerNew.PRICE_ID);
            if (_objPrice.CURRENCY_ID == 1)
            {
                txtTotalAmount.Text = UIHelper.RoundUp(decTotalAmount, _objPrice.CURRENCY_ID).ToString("N0");
            }
            else
            {
                txtTotalAmount.Text = UIHelper.RoundUp(decTotalAmount, _objPrice.CURRENCY_ID).ToString("N2");
            }
            lblREAD_POWER_.Text = decBuyPower <= 0 ? string.Empty : string.Format("{0} ({1})", DataHelper.NumberToWord(decBuyPower), Resources.KWH);
            lblREAD_AMOUNT_.Text = decTotalAmount <= 0 ? string.Empty : string.Format("{0} ({1})", DataHelper.NumberToWord(DataHelper.ParseToDecimal(txtTotalAmount.Text)), Resources.RIEL);
        }

        private void txtEnterEnglish(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void txtKeyNumber(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputNumberOnly(sender, e);
        }
        #endregion Event

        private void btnReadCard_Click(object sender, EventArgs e)
        {
            ReadCardInfo();
        }

        private ICCard ReadCardInfo()
        {
            ICCard objReadCard = ICCard.Read();
            _objCusReadCard = null;
            switch ((CardType)objReadCard.ICCardType)
            {
                case CardType.NoCard:
                    lblCARD_TYPE_.Text = Resources.MS_PLEASE_INSERT_NEW_CARD;
                    break;
                case CardType.BlankCard:
                case CardType.FormattedCard:
                    lblCARD_TYPE_.Text = Resources.NEW_CARD;
                    break;
                case CardType.HavePowerCard:
                case CardType.NoPowerCard:
                    lblCARD_TYPE_.Text = Resources.MS_CARD_INUSE;
                    string strCusCode = ((PowerCard)objReadCard).CustomerCode;
                    _objCusReadCard = (from c in DBDataContext.Db.TBL_CUSTOMERs
                                       join pc in DBDataContext.Db.TBL_PREPAID_CUSTOMERs on c.CUSTOMER_ID equals pc.CUSTOMER_ID
                                       where pc.CARD_CODE.ToLower().Contains(strCusCode.ToLower())
                                       select c).FirstOrDefault();
                    break;
                case CardType.ResetCard:
                case CardType.PresetCard:
                case CardType.TestCard:
                case CardType.CutCard:
                    lblCARD_TYPE_.Text = Resources.MS_CARD_CANNOT_USE;
                    break;
                default:
                    break;
            }

            return objReadCard;
        }

        private void lblCardDetailInfo_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (_objCusReadCard == null)
            {
                return;
            }

            DialogCustomer dig = new DialogCustomer(GeneralProcess.Update, _objCusReadCard);
            dig.ShowDialog();
        }

        private void txtSTART_USAGE_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtBreaker.Focus();
            }
        }

    }
}
