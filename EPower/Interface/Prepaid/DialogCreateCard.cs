﻿using System;
using System.Windows.Forms;
using EPower.Logic;
using EPower.Properties;
using SoftTech.Component;

namespace EPower.Interface.PrePaid
{
    public partial class DialogCreateCard : ExDialog
    {
        public DialogCreateCard()
        {
            InitializeComponent();
        }         
        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            ReadCardInfo();
        }           
       
        private ICCard ReadCardInfo()
        {            
            var objReadCard = ICCard.Read();
            switch ((CardType)objReadCard.ICCardType)
            {
                case CardType.NoCard:
                    lblCARD_TYPE_.Text = Resources.MS_PLEASE_INSERT_NO_POWER_CARD_TO_DEVICE;
                    break;
                case CardType.BlankCard:
                    lblCARD_TYPE_.Text = Resources.IC_CARD_BLANK;
                    break;
                case CardType.FormattedCard:
                    lblCARD_TYPE_.Text = Resources.IC_CARD_FORMATED;
                    rdoFORMAT_CARD.Checked = true;
                    break;
                case CardType.HavePowerCard:
                    lblCARD_TYPE_.Text = Resources.IC_CARD_HAVE_POWER;
                    break;
                case CardType.NoPowerCard:
                    lblCARD_TYPE_.Text = Resources.IC_CARD_NO_POWER ; 
                    break;
                case CardType.ResetCard:
                    lblCARD_TYPE_.Text = Resources.IC_CARD_RESET;
                    rdoRESET_CARD.Checked = true;
                    break;  
                case CardType.PresetCard:
                case CardType.TestCard:
                    lblCARD_TYPE_.Text = Resources.IC_CARD_PRESET;
                    rdoTEST_CARD.Checked = true;
                    break;
                case CardType.CutCard:
                    lblCARD_TYPE_.Text = Resources.IC_CARD_CUT;
                    break;
                default:
                    break;
            }

            return objReadCard;
        }
         

        private void btnOK_Click(object sender, EventArgs e)
        {
            string strReturn;
            var intReturn = 1;
            CardType makeCardType = CardType.BlankCard;
            if (rdoFORMAT_CARD.Checked)
            {
                makeCardType = CardType.FormattedCard;
            }
            else if (rdoTEST_CARD.Checked)
            {
                makeCardType = CardType.TestCard;
            }
            else if (rdoRESET_CARD.Checked)
            {
                makeCardType = CardType.ResetCard;
            }

            Runner.RunNewThread(() =>
            {
                if (makeCardType == CardType.FormattedCard)
                {
                    intReturn = new ICCard().FormatCard(out strReturn, Prepaid.GetAreaCode());
                }
                else if (makeCardType == CardType.TestCard)
                {
                    intReturn = new TestCutCard().WriteReadCard(out strReturn);
                }
                else if (makeCardType == CardType.ResetCard)
                {
                    intReturn = new PreReSetCard().WriteResetCard(Prepaid.GetResetTimes(), MeterType.ThreePhase, out strReturn);
                }
            });

            
            if (intReturn!=0)
            {
                MsgBox.ShowInformation(Resources.IC_CARD_MAKE_FAIL);
            }
            else
            {
                var outStr = "";
                ICHelper.BeepSound(out outStr);
                MsgBox.ShowInformation(Resources.IC_CARD_MAKE_SUCCESS);
                DialogResult = DialogResult.OK;
            }            
        }
    }   
}
