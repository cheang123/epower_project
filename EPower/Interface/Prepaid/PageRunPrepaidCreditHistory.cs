﻿using System;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface.PrePaid
{
    public partial class PageRunPrepaidCreditHistory : Form
    {
        #region Constructor
        public PageRunPrepaidCreditHistory()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgvBilling);
            bindBillingCycle();
        }
        #endregion Constructor

        #region Method

        private void bindBillingCycle()
        {
            var q = from b in DBDataContext.Db.TBL_BILLING_CYCLEs
                join r in DBDataContext.Db.TBL_RUN_BILLs on b.CYCLE_ID equals r.CYCLE_ID
                where b.IS_ACTIVE
                      && DBDataContext.Db.TBL_RUN_PREPAID_CREDITs.Any(x => x.RUN_ID == r.RUN_ID)
                select new
                {
                    b.CYCLE_ID,
                    b.CYCLE_NAME
                }; 
            UIHelper.SetDataSourceToComboBox(cboBillingCycle, q.Distinct()._ToDataTable(),Resources.ALL_CYCLE);
            cboBillingCycle.SelectedIndex = 0;
        }

        public void bind()
        {
            if(cboBillingCycle.SelectedIndex == -1)
            {
                return;
            }
            int intYear = dtpRunYear.Value.Year;
            int intBillingCycle = int.Parse(cboBillingCycle.SelectedValue.ToString());

            dgvBilling.DataSource = from r in DBDataContext.Db.TBL_RUN_BILLs
                                    join c in DBDataContext.Db.TBL_BILLING_CYCLEs on r.CYCLE_ID equals c.CYCLE_ID
                                    join p in DBDataContext.Db.TBL_RUN_PREPAID_CREDITs on r.RUN_ID equals p.RUN_ID
                                    where p.CREDIT_MONTH.Year == intYear && (r.CYCLE_ID == intBillingCycle || intBillingCycle == 0)
                                    select new
                                    {
                                        p.RUN_PREPAID_CREDIT_ID,
                                        c.CYCLE_NAME,
                                        p.CREATE_BY,
                                        p.CREATE_ON,
                                        p.CREDIT_MONTH,
                                        p.TOTAL_CUSTOMER,
                                        p.TOTAL_CREDIT_USAGE,
                                        p.TOTAL_CREDIT_AMOUNT
                                    };

        }
        #endregion Method

        #region Event
        private void cboBillingCycle_SelectedIndexChanged(object sender, EventArgs e)
        {
            bind();
        }

        private void dtpRunYear_ValueChanged(object sender, EventArgs e)
        {
            bind();
        }
        #endregion Event   

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgvBilling.SelectedRows.Count == 0) 
                return;

            var diag = new DialogRunPrepaidCreditHistory((int)dgvBilling.SelectedRows[0].Cells[RUN_PREPAID_CREDIT_ID.Name].Value);
            if (diag.ShowDialog() == DialogResult.OK)
            {
                bind();
            }
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            if (dgvBilling.SelectedRows.Count <= 0) return;
            try
            {
                var intRunID = (int)dgvBilling.SelectedRows[0].Cells[RUN_PREPAID_CREDIT_ID.Name].Value;
                var month = (DateTime)dgvBilling.SelectedRows[0].Cells[MONTH.Name].Value;
                var c = new CrystalReportHelper("ReportPrepaidCreditMonthly.rpt");
                c.SetParameter("@RUN_PREPAID_CREDIT_ID", intRunID);
                c.SetParameter("@MONTH", month);

                c.ViewReport(Resources.REPORT);
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp);
            }                   
        }
    }
}
