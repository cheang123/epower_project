﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.PrePaid
{
    partial class DialogPrepaidCreditAdjustment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogPrepaidCreditAdjustment));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.lblSTART_DATE = new System.Windows.Forms.Label();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.lblSTART_USAGE = new System.Windows.Forms.Label();
            this.lblEND_USAGE = new System.Windows.Forms.Label();
            this.txtStartUsage = new System.Windows.Forms.TextBox();
            this.txtEndUsage = new System.Windows.Forms.TextBox();
            this.lblMETER_CODE = new System.Windows.Forms.Label();
            this.txtTotalUsage = new System.Windows.Forms.TextBox();
            this.lblUSAGE = new System.Windows.Forms.Label();
            this.cboMeterCode = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.lblNOTE = new System.Windows.Forms.Label();
            this.txtTotalAmountNew = new System.Windows.Forms.TextBox();
            this.lblAMOUNT_SUBSIDY = new System.Windows.Forms.Label();
            this.lblMULTIPLIER = new System.Windows.Forms.Label();
            this.txtTotalAmount = new System.Windows.Forms.TextBox();
            this.lblSUBSIDY_BEFORE_ADJUST = new System.Windows.Forms.Label();
            this.txtMultiplier = new System.Windows.Forms.TextBox();
            this.chkIS_NEW_CYCLE = new System.Windows.Forms.CheckBox();
            this.txtMeterUsage = new System.Windows.Forms.TextBox();
            this.lblMETER_USAGE = new System.Windows.Forms.Label();
            this.lblSUBSIDY = new System.Windows.Forms.Label();
            this.txtSUBSIDY_TARIFF = new System.Windows.Forms.TextBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.txtSUBSIDY_TARIFF);
            this.content.Controls.Add(this.lblSUBSIDY);
            this.content.Controls.Add(this.txtMeterUsage);
            this.content.Controls.Add(this.lblMETER_USAGE);
            this.content.Controls.Add(this.chkIS_NEW_CYCLE);
            this.content.Controls.Add(this.lblSUBSIDY_BEFORE_ADJUST);
            this.content.Controls.Add(this.txtTotalAmount);
            this.content.Controls.Add(this.txtMultiplier);
            this.content.Controls.Add(this.lblMULTIPLIER);
            this.content.Controls.Add(this.lblNOTE);
            this.content.Controls.Add(this.txtTotalAmountNew);
            this.content.Controls.Add(this.lblAMOUNT_SUBSIDY);
            this.content.Controls.Add(this.cboMeterCode);
            this.content.Controls.Add(this.txtTotalUsage);
            this.content.Controls.Add(this.lblUSAGE);
            this.content.Controls.Add(this.lblMETER_CODE);
            this.content.Controls.Add(this.txtEndUsage);
            this.content.Controls.Add(this.txtStartUsage);
            this.content.Controls.Add(this.lblEND_USAGE);
            this.content.Controls.Add(this.lblSTART_USAGE);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.dtpEndDate);
            this.content.Controls.Add(this.lblSTART_DATE);
            this.content.Controls.Add(this.dtpStartDate);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.txtDescription);
            this.content.Controls.Add(this.label13);
            this.content.Controls.Add(this.label14);
            this.content.Controls.Add(this.label12);
            this.content.Controls.Add(this.label3);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.label3, 0);
            this.content.Controls.SetChildIndex(this.label12, 0);
            this.content.Controls.SetChildIndex(this.label14, 0);
            this.content.Controls.SetChildIndex(this.label13, 0);
            this.content.Controls.SetChildIndex(this.txtDescription, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.dtpStartDate, 0);
            this.content.Controls.SetChildIndex(this.lblSTART_DATE, 0);
            this.content.Controls.SetChildIndex(this.dtpEndDate, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.lblSTART_USAGE, 0);
            this.content.Controls.SetChildIndex(this.lblEND_USAGE, 0);
            this.content.Controls.SetChildIndex(this.txtStartUsage, 0);
            this.content.Controls.SetChildIndex(this.txtEndUsage, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_CODE, 0);
            this.content.Controls.SetChildIndex(this.lblUSAGE, 0);
            this.content.Controls.SetChildIndex(this.txtTotalUsage, 0);
            this.content.Controls.SetChildIndex(this.cboMeterCode, 0);
            this.content.Controls.SetChildIndex(this.lblAMOUNT_SUBSIDY, 0);
            this.content.Controls.SetChildIndex(this.txtTotalAmountNew, 0);
            this.content.Controls.SetChildIndex(this.lblNOTE, 0);
            this.content.Controls.SetChildIndex(this.lblMULTIPLIER, 0);
            this.content.Controls.SetChildIndex(this.txtMultiplier, 0);
            this.content.Controls.SetChildIndex(this.txtTotalAmount, 0);
            this.content.Controls.SetChildIndex(this.lblSUBSIDY_BEFORE_ADJUST, 0);
            this.content.Controls.SetChildIndex(this.chkIS_NEW_CYCLE, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_USAGE, 0);
            this.content.Controls.SetChildIndex(this.txtMeterUsage, 0);
            this.content.Controls.SetChildIndex(this.lblSUBSIDY, 0);
            this.content.Controls.SetChildIndex(this.txtSUBSIDY_TARIFF, 0);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblSTART_DATE
            // 
            resources.ApplyResources(this.lblSTART_DATE, "lblSTART_DATE");
            this.lblSTART_DATE.Name = "lblSTART_DATE";
            // 
            // dtpStartDate
            // 
            resources.ApplyResources(this.dtpStartDate, "dtpStartDate");
            this.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartDate.Name = "dtpStartDate";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // dtpEndDate
            // 
            resources.ApplyResources(this.dtpEndDate, "dtpEndDate");
            this.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndDate.Name = "dtpEndDate";
            // 
            // lblSTART_USAGE
            // 
            resources.ApplyResources(this.lblSTART_USAGE, "lblSTART_USAGE");
            this.lblSTART_USAGE.Name = "lblSTART_USAGE";
            // 
            // lblEND_USAGE
            // 
            resources.ApplyResources(this.lblEND_USAGE, "lblEND_USAGE");
            this.lblEND_USAGE.Name = "lblEND_USAGE";
            // 
            // txtStartUsage
            // 
            resources.ApplyResources(this.txtStartUsage, "txtStartUsage");
            this.txtStartUsage.Name = "txtStartUsage";
            this.txtStartUsage.TextChanged += new System.EventHandler(this.txtNewUsage_TextChanged);
            this.txtStartUsage.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumber);
            // 
            // txtEndUsage
            // 
            resources.ApplyResources(this.txtEndUsage, "txtEndUsage");
            this.txtEndUsage.Name = "txtEndUsage";
            this.txtEndUsage.TextChanged += new System.EventHandler(this.txtNewUsage_TextChanged);
            this.txtEndUsage.Enter += new System.EventHandler(this.EnglishKey);
            this.txtEndUsage.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumber);
            // 
            // lblMETER_CODE
            // 
            resources.ApplyResources(this.lblMETER_CODE, "lblMETER_CODE");
            this.lblMETER_CODE.Name = "lblMETER_CODE";
            // 
            // txtTotalUsage
            // 
            resources.ApplyResources(this.txtTotalUsage, "txtTotalUsage");
            this.txtTotalUsage.Name = "txtTotalUsage";
            this.txtTotalUsage.ReadOnly = true;
            // 
            // lblUSAGE
            // 
            resources.ApplyResources(this.lblUSAGE, "lblUSAGE");
            this.lblUSAGE.Name = "lblUSAGE";
            // 
            // cboMeterCode
            // 
            this.cboMeterCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMeterCode.FormattingEnabled = true;
            resources.ApplyResources(this.cboMeterCode, "cboMeterCode");
            this.cboMeterCode.Name = "cboMeterCode";
            this.cboMeterCode.SelectedIndexChanged += new System.EventHandler(this.cboMeterCode_SelectedIndexChanged);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Name = "label3";
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Name = "label12";
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Name = "label13";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Name = "label14";
            // 
            // txtDescription
            // 
            resources.ApplyResources(this.txtDescription, "txtDescription");
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Enter += new System.EventHandler(this.txtDescription_Enter);
            // 
            // lblNOTE
            // 
            resources.ApplyResources(this.lblNOTE, "lblNOTE");
            this.lblNOTE.Name = "lblNOTE";
            // 
            // txtTotalAmountNew
            // 
            resources.ApplyResources(this.txtTotalAmountNew, "txtTotalAmountNew");
            this.txtTotalAmountNew.Name = "txtTotalAmountNew";
            this.txtTotalAmountNew.ReadOnly = true;
            this.txtTotalAmountNew.Enter += new System.EventHandler(this.EnglishKey);
            this.txtTotalAmountNew.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumber);
            // 
            // lblAMOUNT_SUBSIDY
            // 
            resources.ApplyResources(this.lblAMOUNT_SUBSIDY, "lblAMOUNT_SUBSIDY");
            this.lblAMOUNT_SUBSIDY.Name = "lblAMOUNT_SUBSIDY";
            // 
            // lblMULTIPLIER
            // 
            resources.ApplyResources(this.lblMULTIPLIER, "lblMULTIPLIER");
            this.lblMULTIPLIER.Name = "lblMULTIPLIER";
            // 
            // txtTotalAmount
            // 
            resources.ApplyResources(this.txtTotalAmount, "txtTotalAmount");
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.ReadOnly = true;
            // 
            // lblSUBSIDY_BEFORE_ADJUST
            // 
            resources.ApplyResources(this.lblSUBSIDY_BEFORE_ADJUST, "lblSUBSIDY_BEFORE_ADJUST");
            this.lblSUBSIDY_BEFORE_ADJUST.Name = "lblSUBSIDY_BEFORE_ADJUST";
            // 
            // txtMultiplier
            // 
            resources.ApplyResources(this.txtMultiplier, "txtMultiplier");
            this.txtMultiplier.Name = "txtMultiplier";
            this.txtMultiplier.TextChanged += new System.EventHandler(this.txtNewUsage_TextChanged);
            this.txtMultiplier.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumber);
            // 
            // chkIS_NEW_CYCLE
            // 
            resources.ApplyResources(this.chkIS_NEW_CYCLE, "chkIS_NEW_CYCLE");
            this.chkIS_NEW_CYCLE.Name = "chkIS_NEW_CYCLE";
            this.chkIS_NEW_CYCLE.UseVisualStyleBackColor = true;
            this.chkIS_NEW_CYCLE.CheckedChanged += new System.EventHandler(this.chkMETER_IS_RENEW_CYCLE_CheckedChanged);
            // 
            // txtMeterUsage
            // 
            resources.ApplyResources(this.txtMeterUsage, "txtMeterUsage");
            this.txtMeterUsage.Name = "txtMeterUsage";
            this.txtMeterUsage.ReadOnly = true;
            // 
            // lblMETER_USAGE
            // 
            resources.ApplyResources(this.lblMETER_USAGE, "lblMETER_USAGE");
            this.lblMETER_USAGE.Name = "lblMETER_USAGE";
            // 
            // lblSUBSIDY
            // 
            resources.ApplyResources(this.lblSUBSIDY, "lblSUBSIDY");
            this.lblSUBSIDY.Name = "lblSUBSIDY";
            // 
            // txtSUBSIDY_TARIFF
            // 
            this.txtSUBSIDY_TARIFF.BackColor = System.Drawing.SystemColors.Control;
            resources.ApplyResources(this.txtSUBSIDY_TARIFF, "txtSUBSIDY_TARIFF");
            this.txtSUBSIDY_TARIFF.Name = "txtSUBSIDY_TARIFF";
            // 
            // DialogPrepaidCreditAdjustment
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogPrepaidCreditAdjustment";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label label1;
        private DateTimePicker dtpEndDate;
        private Label lblSTART_DATE;
        private DateTimePicker dtpStartDate;
        private Label lblMETER_CODE;
        private TextBox txtEndUsage;
        private TextBox txtStartUsage;
        private Label lblEND_USAGE;
        private Label lblSTART_USAGE;
        private TextBox txtTotalUsage;
        private Label lblUSAGE;
        private ComboBox cboMeterCode;
        private Label label3;
        private Label label12;
        private Label label13;
        private Label label14;
        private TextBox txtDescription;
        private Label lblNOTE;
        private TextBox txtTotalAmountNew;
        private Label lblAMOUNT_SUBSIDY;
        private Label lblSUBSIDY_BEFORE_ADJUST;
        private TextBox txtTotalAmount;
        private Label lblMULTIPLIER;
        private TextBox txtMultiplier;
        private CheckBox chkIS_NEW_CYCLE;
        private TextBox txtMeterUsage;
        private Label lblMETER_USAGE;
        private Label lblSUBSIDY;
        private TextBox txtSUBSIDY_TARIFF;
    }
}
