﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.PrePaid
{
    partial class DialogRunPrepaidCreditHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMONTH = new System.Windows.Forms.Label();
            this.lblSTART_DATE = new System.Windows.Forms.Label();
            this.dtpRunMonth = new System.Windows.Forms.DateTimePicker();
            this.dtpFrom = new System.Windows.Forms.DateTimePicker();
            this.lblEND_DATE_ = new System.Windows.Forms.Label();
            this.dtpTo = new System.Windows.Forms.DateTimePicker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnSAVE = new SoftTech.Component.ExButton();
            this.lblCYCLE_NAME = new System.Windows.Forms.Label();
            this.cboBillingCycle = new System.Windows.Forms.ComboBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.cboBillingCycle);
            this.content.Controls.Add(this.lblCYCLE_NAME);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnSAVE);
            this.content.Controls.Add(this.dtpTo);
            this.content.Controls.Add(this.dtpFrom);
            this.content.Controls.Add(this.dtpRunMonth);
            this.content.Controls.Add(this.lblSTART_DATE);
            this.content.Controls.Add(this.lblMONTH);
            this.content.Controls.Add(this.lblEND_DATE_);
            this.content.Size = new System.Drawing.Size(415, 207);
            this.content.TabIndex = 0;
            this.content.Text = "\'CONTENT\'";
            this.content.Controls.SetChildIndex(this.lblEND_DATE_, 0);
            this.content.Controls.SetChildIndex(this.lblMONTH, 0);
            this.content.Controls.SetChildIndex(this.lblSTART_DATE, 0);
            this.content.Controls.SetChildIndex(this.dtpRunMonth, 0);
            this.content.Controls.SetChildIndex(this.dtpFrom, 0);
            this.content.Controls.SetChildIndex(this.dtpTo, 0);
            this.content.Controls.SetChildIndex(this.btnSAVE, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.lblCYCLE_NAME, 0);
            this.content.Controls.SetChildIndex(this.cboBillingCycle, 0);
            // 
            // lblMONTH
            // 
            this.lblMONTH.AutoSize = true;
            this.lblMONTH.Location = new System.Drawing.Point(18, 26);
            this.lblMONTH.Name = "lblMONTH";
            this.lblMONTH.Size = new System.Drawing.Size(45, 19);
            this.lblMONTH.TabIndex = 6;
            this.lblMONTH.Text = "ប្រចាំខែ";
            // 
            // lblSTART_DATE
            // 
            this.lblSTART_DATE.AutoSize = true;
            this.lblSTART_DATE.Location = new System.Drawing.Point(17, 89);
            this.lblSTART_DATE.Name = "lblSTART_DATE";
            this.lblSTART_DATE.Size = new System.Drawing.Size(49, 19);
            this.lblSTART_DATE.TabIndex = 8;
            this.lblSTART_DATE.Text = "ចាប់ពីថ្ងៃ";
            // 
            // dtpRunMonth
            // 
            this.dtpRunMonth.CustomFormat = "MM-yyyy";
            this.dtpRunMonth.Enabled = false;
            this.dtpRunMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpRunMonth.Location = new System.Drawing.Point(158, 22);
            this.dtpRunMonth.Name = "dtpRunMonth";
            this.dtpRunMonth.Size = new System.Drawing.Size(235, 27);
            this.dtpRunMonth.TabIndex = 0;
            // 
            // dtpFrom
            // 
            this.dtpFrom.CustomFormat = "dd-MM-yyyy";
            this.dtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFrom.Location = new System.Drawing.Point(158, 85);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(103, 27);
            this.dtpFrom.TabIndex = 2;
            // 
            // lblEND_DATE_
            // 
            this.lblEND_DATE_.AutoSize = true;
            this.lblEND_DATE_.Location = new System.Drawing.Point(269, 89);
            this.lblEND_DATE_.Name = "lblEND_DATE_";
            this.lblEND_DATE_.Size = new System.Drawing.Size(13, 19);
            this.lblEND_DATE_.TabIndex = 15;
            this.lblEND_DATE_.Text = "-";
            // 
            // dtpTo
            // 
            this.dtpTo.CustomFormat = "dd-MM-yyyy";
            this.dtpTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTo.Location = new System.Drawing.Point(289, 85);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(103, 27);
            this.dtpTo.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Location = new System.Drawing.Point(1, 169);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(419, 1);
            this.panel1.TabIndex = 24;
            // 
            // btnCLOSE
            // 
            this.btnCLOSE.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnCLOSE.Location = new System.Drawing.Point(333, 176);
            this.btnCLOSE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.Size = new System.Drawing.Size(75, 23);
            this.btnCLOSE.TabIndex = 6;
            this.btnCLOSE.Text = "បោះបង់";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSAVE
            // 
            this.btnSAVE.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnSAVE.Location = new System.Drawing.Point(254, 176);
            this.btnSAVE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSAVE.Name = "btnSAVE";
            this.btnSAVE.Size = new System.Drawing.Size(75, 23);
            this.btnSAVE.TabIndex = 5;
            this.btnSAVE.Text = "រក្សាទុក";
            this.btnSAVE.UseVisualStyleBackColor = true;
            this.btnSAVE.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblCYCLE_NAME
            // 
            this.lblCYCLE_NAME.AutoSize = true;
            this.lblCYCLE_NAME.Location = new System.Drawing.Point(18, 57);
            this.lblCYCLE_NAME.Name = "lblCYCLE_NAME";
            this.lblCYCLE_NAME.Size = new System.Drawing.Size(45, 19);
            this.lblCYCLE_NAME.TabIndex = 25;
            this.lblCYCLE_NAME.Text = "ជុំទូទាត់";
            // 
            // cboBillingCycle
            // 
            this.cboBillingCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBillingCycle.Enabled = false;
            this.cboBillingCycle.FormattingEnabled = true;
            this.cboBillingCycle.Location = new System.Drawing.Point(158, 53);
            this.cboBillingCycle.Name = "cboBillingCycle";
            this.cboBillingCycle.Size = new System.Drawing.Size(235, 27);
            this.cboBillingCycle.TabIndex = 1;
            // 
            // DialogRunPrepaidCreditHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(415, 230);
            this.Name = "DialogRunPrepaidCreditHistory";
            this.Text = "ប្រវិត្តទូទាត់ឧវត្ថម្ភធនបង់ប្រាក់មុន";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Label lblSTART_DATE;
        private Label lblMONTH;
        private Label lblEND_DATE_;
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnSAVE;
        public DateTimePicker dtpTo;
        public DateTimePicker dtpFrom;
        public DateTimePicker dtpRunMonth;
        private Label lblCYCLE_NAME;
        public ComboBox cboBillingCycle;
    }
}