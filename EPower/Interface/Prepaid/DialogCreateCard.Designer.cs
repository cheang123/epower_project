﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.PrePaid
{
    partial class DialogCreateCard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCreateCard));
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.timeConnection = new System.Windows.Forms.Timer(this.components);
            this.lblCARD_TYPE = new System.Windows.Forms.Label();
            this.rdoTEST_CARD = new System.Windows.Forms.RadioButton();
            this.rdoFORMAT_CARD = new System.Windows.Forms.RadioButton();
            this.rdoRESET_CARD = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblCARD_TYPE_ = new System.Windows.Forms.Label();
            this.btnREAD_CARD = new SoftTech.Component.ExButton();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.panel2);
            this.content.Controls.Add(this.rdoRESET_CARD);
            this.content.Controls.Add(this.rdoFORMAT_CARD);
            this.content.Controls.Add(this.rdoTEST_CARD);
            this.content.Controls.Add(this.lblCARD_TYPE);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.panel1);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.lblCARD_TYPE, 0);
            this.content.Controls.SetChildIndex(this.rdoTEST_CARD, 0);
            this.content.Controls.SetChildIndex(this.rdoFORMAT_CARD, 0);
            this.content.Controls.SetChildIndex(this.rdoRESET_CARD, 0);
            this.content.Controls.SetChildIndex(this.panel2, 0);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // timeConnection
            // 
            this.timeConnection.Interval = 3000;
            // 
            // lblCARD_TYPE
            // 
            resources.ApplyResources(this.lblCARD_TYPE, "lblCARD_TYPE");
            this.lblCARD_TYPE.Name = "lblCARD_TYPE";
            // 
            // rdoTEST_CARD
            // 
            resources.ApplyResources(this.rdoTEST_CARD, "rdoTEST_CARD");
            this.rdoTEST_CARD.Name = "rdoTEST_CARD";
            this.rdoTEST_CARD.UseVisualStyleBackColor = true;
            // 
            // rdoFORMAT_CARD
            // 
            resources.ApplyResources(this.rdoFORMAT_CARD, "rdoFORMAT_CARD");
            this.rdoFORMAT_CARD.Checked = true;
            this.rdoFORMAT_CARD.Name = "rdoFORMAT_CARD";
            this.rdoFORMAT_CARD.TabStop = true;
            this.rdoFORMAT_CARD.UseVisualStyleBackColor = true;
            // 
            // rdoRESET_CARD
            // 
            resources.ApplyResources(this.rdoRESET_CARD, "rdoRESET_CARD");
            this.rdoRESET_CARD.Name = "rdoRESET_CARD";
            this.rdoRESET_CARD.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel2.Controls.Add(this.lblCARD_TYPE_);
            this.panel2.Controls.Add(this.btnREAD_CARD);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // lblCARD_TYPE_
            // 
            this.lblCARD_TYPE_.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblCARD_TYPE_, "lblCARD_TYPE_");
            this.lblCARD_TYPE_.Name = "lblCARD_TYPE_";
            // 
            // btnREAD_CARD
            // 
            resources.ApplyResources(this.btnREAD_CARD, "btnREAD_CARD");
            this.btnREAD_CARD.Name = "btnREAD_CARD";
            this.btnREAD_CARD.UseVisualStyleBackColor = true;
            this.btnREAD_CARD.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // DialogCreateCard
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogCreateCard";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Panel panel1;
        private Timer timeConnection;
        private Label lblCARD_TYPE;
        private RadioButton rdoTEST_CARD;
        private RadioButton rdoFORMAT_CARD;
        private RadioButton rdoRESET_CARD;
        private Panel panel2;
        private Label lblCARD_TYPE_;
        private ExButton btnREAD_CARD;
    }
}