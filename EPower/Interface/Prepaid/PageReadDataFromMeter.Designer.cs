﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.PrePaid
{
    partial class PageReadDataFromMeter
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageReadDataFromMeter));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvMeter = new System.Windows.Forms.DataGridView();
            this.CUSTOMER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LAST_BUY_POWER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REMAIN_POWER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_BUY_POWER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BUY_TIMES = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblCARD_TYPE_ = new System.Windows.Forms.Label();
            this.btnREAD_CARD = new SoftTech.Component.ExButton();
            this.lblSYSTEM_INFORMATION = new System.Windows.Forms.Label();
            this.lblMETER_INFORMATION = new System.Windows.Forms.Label();
            this.dgvCus = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_NAME_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LAST_BUY_POWER_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_BUY_POWER_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BUY_TIMES_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMeter)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCus)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvMeter
            // 
            this.dgvMeter.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvMeter.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            resources.ApplyResources(this.dgvMeter, "dgvMeter");
            this.dgvMeter.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvMeter.BackgroundColor = System.Drawing.Color.White;
            this.dgvMeter.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvMeter.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvMeter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMeter.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CUSTOMER_ID,
            this.CUSTOMER_NAME,
            this.LAST_BUY_POWER,
            this.REMAIN_POWER,
            this.TOTAL_BUY_POWER,
            this.BUY_TIMES});
            this.dgvMeter.EnableHeadersVisualStyles = false;
            this.dgvMeter.Name = "dgvMeter";
            this.dgvMeter.RowHeadersVisible = false;
            this.dgvMeter.RowTemplate.Height = 25;
            // 
            // CUSTOMER_ID
            // 
            this.CUSTOMER_ID.DataPropertyName = "CUSTOMER_ID";
            resources.ApplyResources(this.CUSTOMER_ID, "CUSTOMER_ID");
            this.CUSTOMER_ID.Name = "CUSTOMER_ID";
            // 
            // CUSTOMER_NAME
            // 
            this.CUSTOMER_NAME.DataPropertyName = "CUSTOMER_NAME";
            resources.ApplyResources(this.CUSTOMER_NAME, "CUSTOMER_NAME");
            this.CUSTOMER_NAME.Name = "CUSTOMER_NAME";
            // 
            // LAST_BUY_POWER
            // 
            resources.ApplyResources(this.LAST_BUY_POWER, "LAST_BUY_POWER");
            this.LAST_BUY_POWER.Name = "LAST_BUY_POWER";
            // 
            // REMAIN_POWER
            // 
            resources.ApplyResources(this.REMAIN_POWER, "REMAIN_POWER");
            this.REMAIN_POWER.Name = "REMAIN_POWER";
            // 
            // TOTAL_BUY_POWER
            // 
            resources.ApplyResources(this.TOTAL_BUY_POWER, "TOTAL_BUY_POWER");
            this.TOTAL_BUY_POWER.Name = "TOTAL_BUY_POWER";
            // 
            // BUY_TIMES
            // 
            resources.ApplyResources(this.BUY_TIMES, "BUY_TIMES");
            this.BUY_TIMES.Name = "BUY_TIMES";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.lblCARD_TYPE_);
            this.panel1.Controls.Add(this.btnREAD_CARD);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // lblCARD_TYPE_
            // 
            this.lblCARD_TYPE_.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblCARD_TYPE_, "lblCARD_TYPE_");
            this.lblCARD_TYPE_.Name = "lblCARD_TYPE_";
            // 
            // btnREAD_CARD
            // 
            resources.ApplyResources(this.btnREAD_CARD, "btnREAD_CARD");
            this.btnREAD_CARD.Name = "btnREAD_CARD";
            this.btnREAD_CARD.UseVisualStyleBackColor = true;
            this.btnREAD_CARD.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // lblSYSTEM_INFORMATION
            // 
            resources.ApplyResources(this.lblSYSTEM_INFORMATION, "lblSYSTEM_INFORMATION");
            this.lblSYSTEM_INFORMATION.Name = "lblSYSTEM_INFORMATION";
            // 
            // lblMETER_INFORMATION
            // 
            resources.ApplyResources(this.lblMETER_INFORMATION, "lblMETER_INFORMATION");
            this.lblMETER_INFORMATION.Name = "lblMETER_INFORMATION";
            // 
            // dgvCus
            // 
            this.dgvCus.AllowUserToResizeRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvCus.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.dgvCus, "dgvCus");
            this.dgvCus.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCus.BackgroundColor = System.Drawing.Color.White;
            this.dgvCus.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvCus.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvCus.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCus.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.CUSTOMER_NAME_1,
            this.LAST_BUY_POWER_1,
            this.TOTAL_BUY_POWER_1,
            this.BUY_TIMES_1});
            this.dgvCus.EnableHeadersVisualStyles = false;
            this.dgvCus.Name = "dgvCus";
            this.dgvCus.RowHeadersVisible = false;
            this.dgvCus.RowTemplate.Height = 25;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "CUSTOMER_ID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn1, "dataGridViewTextBoxColumn1");
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // CUSTOMER_NAME_1
            // 
            this.CUSTOMER_NAME_1.DataPropertyName = "CUSTOMER_NAME";
            resources.ApplyResources(this.CUSTOMER_NAME_1, "CUSTOMER_NAME_1");
            this.CUSTOMER_NAME_1.Name = "CUSTOMER_NAME_1";
            // 
            // LAST_BUY_POWER_1
            // 
            resources.ApplyResources(this.LAST_BUY_POWER_1, "LAST_BUY_POWER_1");
            this.LAST_BUY_POWER_1.Name = "LAST_BUY_POWER_1";
            // 
            // TOTAL_BUY_POWER_1
            // 
            resources.ApplyResources(this.TOTAL_BUY_POWER_1, "TOTAL_BUY_POWER_1");
            this.TOTAL_BUY_POWER_1.Name = "TOTAL_BUY_POWER_1";
            // 
            // BUY_TIMES_1
            // 
            resources.ApplyResources(this.BUY_TIMES_1, "BUY_TIMES_1");
            this.BUY_TIMES_1.Name = "BUY_TIMES_1";
            // 
            // PageReadDataFromMeter
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dgvCus);
            this.Controls.Add(this.lblMETER_INFORMATION);
            this.Controls.Add(this.lblSYSTEM_INFORMATION);
            this.Controls.Add(this.dgvMeter);
            this.Controls.Add(this.panel1);
            this.Name = "PageReadDataFromMeter";
            ((System.ComponentModel.ISupportInitialize)(this.dgvMeter)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCus)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Panel panel1;
        private DataGridView dgvMeter;
        private Label lblCARD_TYPE_;
        private ExButton btnREAD_CARD;
        private Label lblSYSTEM_INFORMATION;
        private Label lblMETER_INFORMATION;
        private DataGridView dgvCus;
        private DataGridViewTextBoxColumn CUSTOMER_ID;
        private DataGridViewTextBoxColumn CUSTOMER_NAME;
        private DataGridViewTextBoxColumn LAST_BUY_POWER;
        private DataGridViewTextBoxColumn REMAIN_POWER;
        private DataGridViewTextBoxColumn TOTAL_BUY_POWER;
        private DataGridViewTextBoxColumn BUY_TIMES;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn CUSTOMER_NAME_1;
        private DataGridViewTextBoxColumn LAST_BUY_POWER_1;
        private DataGridViewTextBoxColumn TOTAL_BUY_POWER_1;
        private DataGridViewTextBoxColumn BUY_TIMES_1;
    }
}
