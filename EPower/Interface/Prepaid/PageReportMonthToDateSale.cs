﻿using System;
using System.IO;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface.PrePaid
{
    public partial class PageReportMonthToDateSale : UserControl
    {
        private CrystalReportHelper ch;
        public PageReportMonthToDateSale()
        {
            InitializeComponent();
            dtpDate.Value = DBDataContext.Db.GetSystemDate();
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            Runner.Run(viewReport);
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            sendMail();
        }

        private void viewReport()
        {
            ch = new CrystalReportHelper("ReportPrepaidMonthToDateSale.rpt");
            DateTime dtStart = new DateTime(dtpDate.Value.Year, dtpDate.Value.Month, 1);
            ch.SetParameter("@START_DATE", dtStart);
            ch.SetParameter("@END_DATE", dtpDate.Value.AddDays(-1));            
            viewer.ReportSource = ch.Report;
            viewer.ViewReport();
        }

        private void sendMail()
        {
            try
            {
                viewReport();
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + ch.ReportName + ".xls").FullName;
                ch.ExportToExcel(exportPath);
                DialogSendMail.Instance.Add(ch.ReportName, exportPath);
                DialogSendMail.Instance.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }
    }
}
