﻿using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Login = SoftTech.Security.Logic.Login;

namespace EPower.Interface
{
    public partial class PageCustomerBatchChange : Form
    {
        /// <summary>
        /// Determines the binding are being process on Status ComboBox and Type ComboBox.
        /// </summary>
        private bool _binding = false;

        private DataTable _ds = null;
        List<TBL_CUSTOMER> lstCus = DBDataContext.Db.TBL_CUSTOMERs.ToList();

        DialogCustomerBatchChangeOption diaOption = new DialogCustomerBatchChangeOption();
        public PageCustomerBatchChange()
        {
            InitializeComponent();

            _binding = true;
            try
            {
                this.btnEDIT.Enabled = Login.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_REGISTER_UPDATE);

                UIHelper.DataGridViewProperties(dgv);
                this.dgv.MultiSelect = true;

                //UIHelper.SetDataSourceToComboBox(cboArea, Lookup.GetAreas(), Resources.ALL_AREA);
                //UIHelper.SetDataSourceToComboBox(cboCustomerStatus, Logic.Lookup.GetCustomerStatuses(), Properties.Resources.ALL_STATUS);
                //var villages = lstCus.Select(x => x.VILLAGE_CODE).Distinct()
                //               .Union(DBDataContext.Db.TBL_LICENSE_VILLAGEs.Where(x => x.IS_ACTIVE).Select(x => x.VILLAGE_CODE).Distinct()).ToArray();
                //var communes = DBDataContext.Db.TLKP_VILLAGEs.Where(x => villages.Contains(x.VILLAGE_CODE)).Select(x => x.COMMUNE_CODE).Distinct().ToList();
                //UIHelper.SetDataSourceToComboBox(cboCommune, DBDataContext.Db.TLKP_COMMUNEs.Where(x => communes.Contains(x.COMMUNE_CODE)), Resources.ALL_COMMUNE);
                //UIHelper.SetDataSourceToComboBox(cboCustomerGroup, Lookup.GetCustomerGroup(), Resources.All_CUSTOMER_GROUP);
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
            _binding = false;
        }

        int areaId = 0;
        int poleId = 0;
        string villageCode = "";
        string communeCode = "";
        int cusStatusId = 0;
        int customerGroupId = 0;
        int customerConnectionTypeId = 0;
        bool chkmartket;
        public void BindData()
        {
            // if statust and type is in binding mode 
            // then do not bind customer list
            // wait until status and customer type 
            // are completed bound.
            if (this._binding)
            {
                return;
            }

            // lock to make sure only one search wash hit to server.
            this._binding = true;

            this.lblROW_NOT_FOUND.Hide();
            this.lblTypeToSearch.Hide();

            if (diaOption.cboArea.SelectedIndex != -1)
            {
                areaId = (int)diaOption.cboArea.SelectedValue;
            }
            if (diaOption.cboPole.SelectedIndex != -1)
            {
                poleId = diaOption.cboPole.SelectedValue == null ? 0 : (int)diaOption.cboPole.SelectedValue;
            }
            if (diaOption.cboCommune.SelectedIndex != -1)
            {
                communeCode = diaOption.cboCommune.SelectedValue.ToString();
            }
            if (diaOption.cboVillage.SelectedIndex != -1)
            {
                villageCode = diaOption.cboVillage.SelectedValue.ToString();
            }
            if (diaOption.cboCustomerStatus.SelectedIndex != -1)
            {
                cusStatusId = (int)diaOption.cboCustomerStatus.SelectedValue;
            }
            if (diaOption.cboCustomerGroup.SelectedIndex != -1)
            {
                customerGroupId = (int)diaOption.cboCustomerGroup.SelectedValue;
            }
            if (diaOption.cboCustomerConnectionType.SelectedIndex != -1)
            {
                customerConnectionTypeId = diaOption.cboCustomerConnectionType == null ? 0 : (int)diaOption.cboCustomerConnectionType.SelectedValue;
            }

            chkmartket = !diaOption.chkIsmarketVendor.Checked;

            Runner.RunNewThread(delegate ()
            {
                this.bind(areaId, poleId, communeCode, villageCode, this.txtSearch.Text.ToLower(), cusStatusId, customerGroupId, customerConnectionTypeId, chkmartket);
            });

            this.dgv.DataSource = this._ds;
            if (this.dgv.Rows.Count == 0)
            {
                lblROW_NOT_FOUND.Show();
            }

            //set ForeColor to Red if customer is close.
            foreach (DataGridViewRow item in dgv.Rows)
            {
                int status_id = (int)item.Cells[CUSTOMER_STATUS.Name].Value;
                if (status_id == (int)CustomerStatus.Closed || status_id == (int)CustomerStatus.Cancelled)
                {
                    item.DefaultCellStyle.ForeColor = Color.Red;
                    item.DefaultCellStyle.SelectionForeColor = Color.Red;
                }
                else if (status_id == (int)CustomerStatus.Blocked)
                {
                    item.DefaultCellStyle.ForeColor = Color.DarkOrange;
                    item.DefaultCellStyle.SelectionForeColor = Color.DarkOrange;
                }
                else if (status_id == (int)CustomerStatus.Pending)
                {
                    item.DefaultCellStyle.ForeColor = Color.DarkBlue;
                    item.DefaultCellStyle.SelectionForeColor = Color.DarkBlue;
                }
            }

            this._binding = false;

        }

        private void bind(int areaId, int poleId, string communeCode, string villageCode, string search, int cusStatusId, int cusGroupId, int customerConnectionTypeId, bool chkmarket)
        {
            if (chkCUSTOMER_OUTSIDE_LICENSE_VILLAGE.Checked)
            {
                var licenseVillage = DBDataContext.Db.TBL_LICENSE_VILLAGEs.Where(x => x.IS_ACTIVE).Select(x => x.VILLAGE_CODE).ToList();
                var villages = lstCus.Select(x => x.VILLAGE_CODE)
                               .Union(DBDataContext.Db.TBL_LICENSE_VILLAGEs.Where(x => x.IS_ACTIVE).Select(x => x.VILLAGE_CODE).Distinct()).ToList();
                var UncoverLicensevillage = villages.Where(x => !licenseVillage.Contains(x)).ToList();
                var UncoverCus = lstCus.Where(x => UncoverLicensevillage.Contains(x.VILLAGE_CODE));

                var q = from cus in UncoverCus
                        join type in DBDataContext.Db.TBL_CUSTOMER_TYPEs on cus.CUSTOMER_TYPE_ID equals type.CUSTOMER_TYPE_ID
                        join area in DBDataContext.Db.TBL_AREAs on cus.AREA_ID equals area.AREA_ID
                        join cusConnectionType in DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs on cus.CUSTOMER_CONNECTION_TYPE_ID equals cusConnectionType.CUSTOMER_CONNECTION_TYPE_ID
                        join cusGroup in DBDataContext.Db.TLKP_CUSTOMER_GROUPs on cusConnectionType.NONLICENSE_CUSTOMER_GROUP_ID equals cusGroup.CUSTOMER_GROUP_ID
                        join cm in
                            (
                                from m in DBDataContext.Db.TBL_METERs
                                join cm in DBDataContext.Db.TBL_CUSTOMER_METERs on m.METER_ID equals cm.METER_ID
                                join b in DBDataContext.Db.TBL_BOXes on cm.BOX_ID equals b.BOX_ID
                                join p in DBDataContext.Db.TBL_POLEs on b.POLE_ID equals p.POLE_ID
                                where cm.IS_ACTIVE
                                select new { cm.CUSTOMER_ID, m.METER_CODE, p.POLE_CODE, b.BOX_CODE, p.POLE_ID }
                                ) on cus.CUSTOMER_ID equals cm.CUSTOMER_ID into cmTmp
                        join v in DBDataContext.Db.TLKP_VILLAGEs on cus.VILLAGE_CODE equals v.VILLAGE_CODE into g
                        from l in g.DefaultIfEmpty()
                        from cmt in cmTmp.DefaultIfEmpty()
                        where
                            cus.IS_POST_PAID
                            && ((cus.CUSTOMER_CODE + " " + cus.LAST_NAME_KH + " " + cus.FIRST_NAME_KH + " " + cus.LAST_NAME + " " + cus.FIRST_NAME + " "
                                + cus.PHONE_1 + " " + cus.PHONE_2 + " "
                                + (cmt == null ? "" : cmt.POLE_CODE) + " " + (cmt == null ? "" : cmt.BOX_CODE) + " " + (cmt == null ? "" : cmt.METER_CODE)).ToLower().Contains(this.txtSearch.Text.ToLower()))
                            && (areaId == 0 || cus.AREA_ID == areaId)
                            && (poleId == 0 || (cmt != null && cmt.POLE_ID == poleId))
                            && (communeCode == "0" || l.COMMUNE_CODE == communeCode)
                            && (villageCode == "0" || l.VILLAGE_CODE == villageCode)
                            && (cusStatusId == 0 || cus.STATUS_ID == cusStatusId)
                            && (cusGroupId == 0 || cusGroup.CUSTOMER_GROUP_ID == cusGroupId)
                            && (customerConnectionTypeId == 0 || cus.CUSTOMER_CONNECTION_TYPE_ID == customerConnectionTypeId)
                            && (chkmarket == true || cus.CUSTOMER_TYPE_ID == -2)
                        select new
                        {
                            CUSTOMER_ID = cus.CUSTOMER_ID,
                            CUSTOMER_CODE = cus.CUSTOMER_CODE,
                            CUSTOMER_NAME = cus.LAST_NAME_KH + " " + cus.FIRST_NAME_KH,
                            //CUSTOMER_TYPE = cusConnectionType.CUSTOMER_CONNECTION_TYPE_NAME,
                            CUSTOMER_TYPE = cusGroup.CUSTOMER_GROUP_ID != 4 && cusGroup.CUSTOMER_GROUP_ID != 5 ? cusGroup.CUSTOMER_GROUP_NAME + " (" + cusConnectionType.CUSTOMER_CONNECTION_TYPE_NAME + ")" : cusConnectionType.CUSTOMER_CONNECTION_TYPE_NAME,
                            cus.ADDRESS,
                            AREA = area.AREA_NAME,
                            POLE = cmt == null ? "" : cmt.POLE_CODE,
                            BOX = cmt == null ? "" : cmt.BOX_CODE,
                            METER = cmt == null ? "" : cmt.METER_CODE,
                            CUSTOMER_STATUS = cus.STATUS_ID
                        };

                this._ds = q.OrderBy(x => x.AREA).ThenBy(x => x.POLE).ThenBy(x => x.CUSTOMER_CODE)
                           ._ToDataTable();
            }
            else
            {

                var q = from cus in lstCus
                        join type in DBDataContext.Db.TBL_CUSTOMER_TYPEs on cus.CUSTOMER_TYPE_ID equals type.CUSTOMER_TYPE_ID
                        join area in DBDataContext.Db.TBL_AREAs on cus.AREA_ID equals area.AREA_ID
                        join cusConnectionType in DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs on cus.CUSTOMER_CONNECTION_TYPE_ID equals cusConnectionType.CUSTOMER_CONNECTION_TYPE_ID
                        join cusGroup in DBDataContext.Db.TLKP_CUSTOMER_GROUPs on cusConnectionType.NONLICENSE_CUSTOMER_GROUP_ID equals cusGroup.CUSTOMER_GROUP_ID
                        join cm in
                            (
                                from m in DBDataContext.Db.TBL_METERs
                                join cm in DBDataContext.Db.TBL_CUSTOMER_METERs on m.METER_ID equals cm.METER_ID
                                join b in DBDataContext.Db.TBL_BOXes on cm.BOX_ID equals b.BOX_ID
                                join p in DBDataContext.Db.TBL_POLEs on b.POLE_ID equals p.POLE_ID
                                where cm.IS_ACTIVE
                                select new { cm.CUSTOMER_ID, m.METER_CODE, p.POLE_CODE, b.BOX_CODE, p.POLE_ID }
                                ) on cus.CUSTOMER_ID equals cm.CUSTOMER_ID into cmTmp
                        join v in DBDataContext.Db.TLKP_VILLAGEs on cus.VILLAGE_CODE equals v.VILLAGE_CODE into g
                        from l in g.DefaultIfEmpty()
                        from cmt in cmTmp.DefaultIfEmpty()
                        where
                            cus.IS_POST_PAID
                            && ((cus.CUSTOMER_CODE + " " + cus.LAST_NAME_KH + " " + cus.FIRST_NAME_KH + " " + cus.LAST_NAME + " " + cus.FIRST_NAME + " "
                                + cus.PHONE_1 + " " + cus.PHONE_2 + " "
                                + (cmt == null ? "" : cmt.POLE_CODE) + " " + (cmt == null ? "" : cmt.BOX_CODE) + " " + (cmt == null ? "" : cmt.METER_CODE)).ToLower().Contains(this.txtSearch.Text.ToLower()))
                            && (areaId == 0 || cus.AREA_ID == areaId)
                            && (poleId == 0 || (cmt != null && cmt.POLE_ID == poleId))
                            && (communeCode == "0" || l.COMMUNE_CODE == communeCode)
                            && (villageCode == "0" || l.VILLAGE_CODE == villageCode)
                            && (cusStatusId == 0 || cus.STATUS_ID == cusStatusId)
                            && (cusGroupId == 0 || cusGroup.CUSTOMER_GROUP_ID == cusGroupId)
                            && (customerConnectionTypeId == 0 || cus.CUSTOMER_CONNECTION_TYPE_ID == customerConnectionTypeId)
                            && (chkmarket == true || cus.CUSTOMER_TYPE_ID == -2)
                        select new
                        {
                            CUSTOMER_ID = cus.CUSTOMER_ID,
                            CUSTOMER_CODE = cus.CUSTOMER_CODE,
                            CUSTOMER_NAME = cus.LAST_NAME_KH + " " + cus.FIRST_NAME_KH,
                            //CUSTOMER_TYPE = cusConnectionType.CUSTOMER_CONNECTION_TYPE_NAME,
                            CUSTOMER_TYPE = cusGroup.CUSTOMER_GROUP_ID != 4 && cusGroup.CUSTOMER_GROUP_ID != 5 ? cusGroup.CUSTOMER_GROUP_NAME + " (" + cusConnectionType.CUSTOMER_CONNECTION_TYPE_NAME + ")" : cusConnectionType.CUSTOMER_CONNECTION_TYPE_NAME,
                            cus.ADDRESS,
                            AREA = area.AREA_NAME,
                            POLE = cmt == null ? "" : cmt.POLE_CODE,
                            BOX = cmt == null ? "" : cmt.BOX_CODE,
                            METER = cmt == null ? "" : cmt.METER_CODE,
                            CUSTOMER_STATUS = cus.STATUS_ID
                        };

                this._ds = q.OrderBy(x => x.AREA).ThenBy(x => x.POLE).ThenBy(x => x.CUSTOMER_CODE)
                           ._ToDataTable();

            }

        }
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (this.dgv.SelectedRows.Count == 0)
            {
                return;
            }
            int id = (int)this.dgv.SelectedRows[0].Cells[CUSTOMER_ID.Name].Value;
            TBL_CUSTOMER obj = lstCus.FirstOrDefault(row => row.CUSTOMER_ID == id);
            if (obj != null)
            {
                DialogCustomer diag = new DialogCustomer(GeneralProcess.Update, obj);
                if (diag.ShowDialog(this) == DialogResult.OK)
                {
                    BindData();
                    UIHelper.SelectRow(dgv, diag.Object.CUSTOMER_ID);
                }
            }
        }

        private void exTextbox1_QuickSearch(object sender, EventArgs e)
        {
            BindData();
        }

        private void txtSearch_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            if (!Login.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_REGISTER_UPDATE))
            {
                return;
            }

            btnEdit_Click(null, null);
        }

        private void btnBatchChange_Click(object sender, EventArgs e)
        {
            var customers = (from c in lstCus
                             join i in (this.dgv.SelectedRows.Cast<DataGridViewRow>().ToList()) on c.CUSTOMER_ID equals i.Cells[CUSTOMER_ID.Name].Value
                             select c).ToList();

            var diag = new DialogCustomerBatchChange_2(customers);
            if (diag.ShowDialog() == DialogResult.OK)
            {
                BindData();
                foreach (DataGridViewRow row in this.dgv.Rows)
                {
                    if (customers.Select(x => x.CUSTOMER_ID).Contains((int)row.Cells[this.CUSTOMER_ID.Name].Value))
                    {
                        row.Selected = true;
                        this.dgv.CurrentCell = row.Cells[1];
                    }
                }
            }
        }

        private void btnSetUp_Click(object sender, EventArgs e)
        {
            if (diaOption.ShowDialog() == DialogResult.OK)
            {
                this.BindData();
            }
        }

        private void chkCUSTOMER_OUTSIDE_LICENSE_VILLAGE_CheckedChanged(object sender, EventArgs e)
        {
            BindData();
        }
    }
}
