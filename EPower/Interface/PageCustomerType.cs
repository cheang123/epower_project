﻿using System;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PageCustomerType : Form
    {
        
       public TBL_CUSTOMER_TYPE CustomerType
        {
            get
            {
                TBL_CUSTOMER_TYPE objCustomerType = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    int intCustomerTypeID = (int)dgv.SelectedRows[0].Cells["CUSTOMER_TYPE_ID"].Value;
                    try
                    {
                        objCustomerType = DBDataContext.Db.TBL_CUSTOMER_TYPEs.FirstOrDefault(x => x.CUSTOMER_TYPE_ID == intCustomerTypeID);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                    
                }
                return objCustomerType;
            }
        }

        #region Constructor
        public PageCustomerType()
        {
            InitializeComponent();
            txt_QuickSearch(null, null);
            UIHelper.DataGridViewProperties(dgv);
        }
        #endregion

        #region Operation
        /// <summary>
        /// Load data from database.
        /// </summary>
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            try
            {
                dgv.DataSource = from t in DBDataContext.Db.TBL_CUSTOMER_TYPEs
                                 join i in DBDataContext.Db.TBL_ACCOUNT_CHARTs on t.INCOME_ACCOUNT_ID equals i.ACCOUNT_ID
                                 where t.IS_ACTIVE && t.CUSTOMER_TYPE_NAME.ToLower().Contains(txtQuickSearch.Text.ToLower().Trim())
                                 select new{
                                     t.CUSTOMER_TYPE_ID,
                                     t.CUSTOMER_TYPE_NAME,
                                     t.DAILY_SUPPLY_HOUR,
                                     t.DAILY_SUPPLY_SCHEDULE,
                                     t.DEPOSIT_USAGE,
                                     INCOME_ACCOUNT_NAME = i.ACCOUNT_CODE+" "+i.ACCOUNT_NAME,
                                     t.NOTE
                                 };               
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
            
        }

        /// <summary>
        /// Add new area.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNew_Click(object sender, EventArgs e)
        {            
            DialogCustomerType dig = new DialogCustomerType(GeneralProcess.Insert, new TBL_CUSTOMER_TYPE() { CUSTOMER_TYPE_NAME = "", DAILY_SUPPLY_SCHEDULE=string.Empty });
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.CustomerType.CUSTOMER_TYPE_ID);
            }
        }

        /// <summary>
        /// Edit area.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                DialogCustomerType dig = new DialogCustomerType(GeneralProcess.Update, CustomerType);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.CustomerType.CUSTOMER_TYPE_ID);
                }
            }
        }

        /// <summary>
        /// Remove area.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if ( IsDeletable())
            {
                DialogCustomerType dig = new DialogCustomerType(GeneralProcess.Delete, CustomerType);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.CustomerType.CUSTOMER_TYPE_ID - 1);
                }
            }
        }

        /// <summary>
        /// Change current keyboard layout to khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtQuickSearch_QuickSearch(object sender, EventArgs e)
        {
            txt_QuickSearch(sender, e);
        }

        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }
        #endregion

        #region Method
        private bool IsDeletable()
        {
            bool val = false;
            if (dgv.SelectedRows.Count>0)
            {
                val = true;
                if (DBDataContext.Db.TBL_CUSTOMERs.Count(row => row.CUSTOMER_TYPE_ID == this.CustomerType.CUSTOMER_TYPE_ID) > 0)
                {
                    MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE, Resources.INFORMATION);
                    val = false;
                }
            }            
            return val;
        }
        #endregion

        
    }
}
