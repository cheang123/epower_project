﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageCustomerBlock
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageCustomerBlock));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSETTING = new SoftTech.Component.ExButton();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.dpExport = new DevExpress.XtraEditors.DropDownButton();
            this.popupExport = new DevExpress.XtraBars.PopupMenu(this.components);
            this.btnExcelLandscape = new DevExpress.XtraBars.BarButtonItem();
            this.btnPdfLandscape = new DevExpress.XtraBars.BarButtonItem();
            this.btnExcelPortrait = new DevExpress.XtraBars.BarButtonItem();
            this.btnPdfPortrait = new DevExpress.XtraBars.BarButtonItem();
            this.btnCsv = new DevExpress.XtraBars.BarButtonItem();
            this.btnPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.btnBLOCK = new DevExpress.XtraBars.BarButtonItem();
            this.btnCUSTOMER_PENALTY = new DevExpress.XtraBars.BarButtonItem();
            this.btnSAVE_TEMPLATE = new DevExpress.XtraBars.BarButtonItem();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemMRUEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMRUEdit();
            this.dpAction = new DevExpress.XtraEditors.DropDownButton();
            this.popActions = new DevExpress.XtraBars.PopupMenu(this.components);
            this.searchControl = new DevExpress.XtraEditors.SearchControl();
            this.dgv = new DevExpress.XtraGrid.GridControl();
            this.dgvCustomerBlock = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCUSTOMER_CODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCUSTOMER_NAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMETER_CODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCUSTOMER_GROUP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCUSTOMER_CONNECTION_TYPE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAREA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCYCLE_NAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDUE_DATE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDAY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSTATUS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDUE_AMOUNT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCURRENCY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.resCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colCURRENCY_SIGN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.resCurrencySign = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colPOLE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPHONE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPENALTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colROW_NO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnCASH_COLLECTION_DETAIL = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.panel1.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupExport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMRUEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popActions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchControl.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomerBlock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resCurrencySign)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.btnSETTING);
            this.panel1.Controls.Add(this.flowLayoutPanel3);
            this.panel1.Controls.Add(this.searchControl);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnSETTING
            // 
            resources.ApplyResources(this.btnSETTING, "btnSETTING");
            this.btnSETTING.Name = "btnSETTING";
            this.btnSETTING.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel3
            // 
            resources.ApplyResources(this.flowLayoutPanel3, "flowLayoutPanel3");
            this.flowLayoutPanel3.BackColor = System.Drawing.Color.Transparent;
            this.flowLayoutPanel3.Controls.Add(this.dpExport);
            this.flowLayoutPanel3.Controls.Add(this.dpAction);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            // 
            // dpExport
            // 
            resources.ApplyResources(this.dpExport, "dpExport");
            this.dpExport.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.dpExport.Appearance.BorderColor = System.Drawing.Color.Transparent;
            this.dpExport.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("dpExport.Appearance.Font")));
            this.dpExport.Appearance.Options.UseBackColor = true;
            this.dpExport.Appearance.Options.UseBorderColor = true;
            this.dpExport.Appearance.Options.UseFont = true;
            this.dpExport.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            this.dpExport.DropDownControl = this.popupExport;
            this.dpExport.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("dpExport.ImageOptions.Image")));
            this.dpExport.ImageOptions.ImageUri.Uri = "SendCSV;Size32x32;GrayScaled";
            this.dpExport.ImageOptions.SvgImageSize = new System.Drawing.Size(10, 10);
            this.dpExport.LookAndFeel.SkinMaskColor = System.Drawing.Color.Transparent;
            this.dpExport.LookAndFeel.SkinMaskColor2 = System.Drawing.Color.Transparent;
            this.dpExport.LookAndFeel.UseDefaultLookAndFeel = false;
            this.dpExport.Name = "dpExport";
            this.dpExport.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            // 
            // popupExport
            // 
            this.popupExport.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExcelLandscape),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnPdfLandscape),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExcelPortrait, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnPdfPortrait),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnCsv, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnPrint)});
            this.popupExport.Manager = this.barManager1;
            this.popupExport.Name = "popupExport";
            // 
            // btnExcelLandscape
            // 
            resources.ApplyResources(this.btnExcelLandscape, "btnExcelLandscape");
            this.btnExcelLandscape.Id = 0;
            this.btnExcelLandscape.ItemAppearance.Disabled.Font = ((System.Drawing.Font)(resources.GetObject("btnExcelLandscape.ItemAppearance.Disabled.Font")));
            this.btnExcelLandscape.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnExcelLandscape.ItemAppearance.Hovered.Font = ((System.Drawing.Font)(resources.GetObject("btnExcelLandscape.ItemAppearance.Hovered.Font")));
            this.btnExcelLandscape.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnExcelLandscape.ItemAppearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("btnExcelLandscape.ItemAppearance.Normal.Font")));
            this.btnExcelLandscape.ItemAppearance.Normal.Options.UseFont = true;
            this.btnExcelLandscape.ItemAppearance.Pressed.Font = ((System.Drawing.Font)(resources.GetObject("btnExcelLandscape.ItemAppearance.Pressed.Font")));
            this.btnExcelLandscape.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnExcelLandscape.ItemInMenuAppearance.Disabled.Font = ((System.Drawing.Font)(resources.GetObject("btnExcelLandscape.ItemInMenuAppearance.Disabled.Font")));
            this.btnExcelLandscape.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.btnExcelLandscape.ItemInMenuAppearance.Hovered.Font = ((System.Drawing.Font)(resources.GetObject("btnExcelLandscape.ItemInMenuAppearance.Hovered.Font")));
            this.btnExcelLandscape.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.btnExcelLandscape.ItemInMenuAppearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("btnExcelLandscape.ItemInMenuAppearance.Normal.Font")));
            this.btnExcelLandscape.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnExcelLandscape.ItemInMenuAppearance.Pressed.Font = ((System.Drawing.Font)(resources.GetObject("btnExcelLandscape.ItemInMenuAppearance.Pressed.Font")));
            this.btnExcelLandscape.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.btnExcelLandscape.Name = "btnExcelLandscape";
            // 
            // btnPdfLandscape
            // 
            resources.ApplyResources(this.btnPdfLandscape, "btnPdfLandscape");
            this.btnPdfLandscape.Id = 2;
            this.btnPdfLandscape.ItemAppearance.Disabled.Font = ((System.Drawing.Font)(resources.GetObject("btnPdfLandscape.ItemAppearance.Disabled.Font")));
            this.btnPdfLandscape.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnPdfLandscape.ItemAppearance.Hovered.Font = ((System.Drawing.Font)(resources.GetObject("btnPdfLandscape.ItemAppearance.Hovered.Font")));
            this.btnPdfLandscape.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnPdfLandscape.ItemAppearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("btnPdfLandscape.ItemAppearance.Normal.Font")));
            this.btnPdfLandscape.ItemAppearance.Normal.Options.UseFont = true;
            this.btnPdfLandscape.ItemAppearance.Pressed.Font = ((System.Drawing.Font)(resources.GetObject("btnPdfLandscape.ItemAppearance.Pressed.Font")));
            this.btnPdfLandscape.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnPdfLandscape.ItemInMenuAppearance.Disabled.Font = ((System.Drawing.Font)(resources.GetObject("btnPdfLandscape.ItemInMenuAppearance.Disabled.Font")));
            this.btnPdfLandscape.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.btnPdfLandscape.ItemInMenuAppearance.Hovered.Font = ((System.Drawing.Font)(resources.GetObject("btnPdfLandscape.ItemInMenuAppearance.Hovered.Font")));
            this.btnPdfLandscape.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.btnPdfLandscape.ItemInMenuAppearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("btnPdfLandscape.ItemInMenuAppearance.Normal.Font")));
            this.btnPdfLandscape.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnPdfLandscape.ItemInMenuAppearance.Pressed.Font = ((System.Drawing.Font)(resources.GetObject("btnPdfLandscape.ItemInMenuAppearance.Pressed.Font")));
            this.btnPdfLandscape.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.btnPdfLandscape.Name = "btnPdfLandscape";
            // 
            // btnExcelPortrait
            // 
            resources.ApplyResources(this.btnExcelPortrait, "btnExcelPortrait");
            this.btnExcelPortrait.Id = 18;
            this.btnExcelPortrait.ItemAppearance.Disabled.Font = ((System.Drawing.Font)(resources.GetObject("btnExcelPortrait.ItemAppearance.Disabled.Font")));
            this.btnExcelPortrait.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnExcelPortrait.ItemAppearance.Hovered.Font = ((System.Drawing.Font)(resources.GetObject("btnExcelPortrait.ItemAppearance.Hovered.Font")));
            this.btnExcelPortrait.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnExcelPortrait.ItemAppearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("btnExcelPortrait.ItemAppearance.Normal.Font")));
            this.btnExcelPortrait.ItemAppearance.Normal.Options.UseFont = true;
            this.btnExcelPortrait.ItemAppearance.Pressed.Font = ((System.Drawing.Font)(resources.GetObject("btnExcelPortrait.ItemAppearance.Pressed.Font")));
            this.btnExcelPortrait.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnExcelPortrait.ItemInMenuAppearance.Disabled.Font = ((System.Drawing.Font)(resources.GetObject("btnExcelPortrait.ItemInMenuAppearance.Disabled.Font")));
            this.btnExcelPortrait.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.btnExcelPortrait.ItemInMenuAppearance.Hovered.Font = ((System.Drawing.Font)(resources.GetObject("btnExcelPortrait.ItemInMenuAppearance.Hovered.Font")));
            this.btnExcelPortrait.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.btnExcelPortrait.ItemInMenuAppearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("btnExcelPortrait.ItemInMenuAppearance.Normal.Font")));
            this.btnExcelPortrait.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnExcelPortrait.ItemInMenuAppearance.Pressed.Font = ((System.Drawing.Font)(resources.GetObject("btnExcelPortrait.ItemInMenuAppearance.Pressed.Font")));
            this.btnExcelPortrait.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.btnExcelPortrait.Name = "btnExcelPortrait";
            // 
            // btnPdfPortrait
            // 
            resources.ApplyResources(this.btnPdfPortrait, "btnPdfPortrait");
            this.btnPdfPortrait.Id = 19;
            this.btnPdfPortrait.ItemAppearance.Disabled.Font = ((System.Drawing.Font)(resources.GetObject("btnPdfPortrait.ItemAppearance.Disabled.Font")));
            this.btnPdfPortrait.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnPdfPortrait.ItemAppearance.Hovered.Font = ((System.Drawing.Font)(resources.GetObject("btnPdfPortrait.ItemAppearance.Hovered.Font")));
            this.btnPdfPortrait.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnPdfPortrait.ItemAppearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("btnPdfPortrait.ItemAppearance.Normal.Font")));
            this.btnPdfPortrait.ItemAppearance.Normal.Options.UseFont = true;
            this.btnPdfPortrait.ItemAppearance.Pressed.Font = ((System.Drawing.Font)(resources.GetObject("btnPdfPortrait.ItemAppearance.Pressed.Font")));
            this.btnPdfPortrait.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnPdfPortrait.ItemInMenuAppearance.Disabled.Font = ((System.Drawing.Font)(resources.GetObject("btnPdfPortrait.ItemInMenuAppearance.Disabled.Font")));
            this.btnPdfPortrait.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.btnPdfPortrait.ItemInMenuAppearance.Hovered.Font = ((System.Drawing.Font)(resources.GetObject("btnPdfPortrait.ItemInMenuAppearance.Hovered.Font")));
            this.btnPdfPortrait.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.btnPdfPortrait.ItemInMenuAppearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("btnPdfPortrait.ItemInMenuAppearance.Normal.Font")));
            this.btnPdfPortrait.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnPdfPortrait.ItemInMenuAppearance.Pressed.Font = ((System.Drawing.Font)(resources.GetObject("btnPdfPortrait.ItemInMenuAppearance.Pressed.Font")));
            this.btnPdfPortrait.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.btnPdfPortrait.Name = "btnPdfPortrait";
            // 
            // btnCsv
            // 
            resources.ApplyResources(this.btnCsv, "btnCsv");
            this.btnCsv.Id = 1;
            this.btnCsv.ItemAppearance.Disabled.Font = ((System.Drawing.Font)(resources.GetObject("btnCsv.ItemAppearance.Disabled.Font")));
            this.btnCsv.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnCsv.ItemAppearance.Hovered.Font = ((System.Drawing.Font)(resources.GetObject("btnCsv.ItemAppearance.Hovered.Font")));
            this.btnCsv.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnCsv.ItemAppearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("btnCsv.ItemAppearance.Normal.Font")));
            this.btnCsv.ItemAppearance.Normal.Options.UseFont = true;
            this.btnCsv.ItemAppearance.Pressed.Font = ((System.Drawing.Font)(resources.GetObject("btnCsv.ItemAppearance.Pressed.Font")));
            this.btnCsv.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnCsv.ItemInMenuAppearance.Disabled.Font = ((System.Drawing.Font)(resources.GetObject("btnCsv.ItemInMenuAppearance.Disabled.Font")));
            this.btnCsv.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.btnCsv.ItemInMenuAppearance.Hovered.Font = ((System.Drawing.Font)(resources.GetObject("btnCsv.ItemInMenuAppearance.Hovered.Font")));
            this.btnCsv.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.btnCsv.ItemInMenuAppearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("btnCsv.ItemInMenuAppearance.Normal.Font")));
            this.btnCsv.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnCsv.ItemInMenuAppearance.Pressed.Font = ((System.Drawing.Font)(resources.GetObject("btnCsv.ItemInMenuAppearance.Pressed.Font")));
            this.btnCsv.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.btnCsv.Name = "btnCsv";
            // 
            // btnPrint
            // 
            resources.ApplyResources(this.btnPrint, "btnPrint");
            this.btnPrint.Id = 3;
            this.btnPrint.ItemAppearance.Disabled.Font = ((System.Drawing.Font)(resources.GetObject("btnPrint.ItemAppearance.Disabled.Font")));
            this.btnPrint.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnPrint.ItemAppearance.Hovered.Font = ((System.Drawing.Font)(resources.GetObject("btnPrint.ItemAppearance.Hovered.Font")));
            this.btnPrint.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnPrint.ItemAppearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("btnPrint.ItemAppearance.Normal.Font")));
            this.btnPrint.ItemAppearance.Normal.Options.UseFont = true;
            this.btnPrint.ItemAppearance.Pressed.Font = ((System.Drawing.Font)(resources.GetObject("btnPrint.ItemAppearance.Pressed.Font")));
            this.btnPrint.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnPrint.ItemInMenuAppearance.Disabled.Font = ((System.Drawing.Font)(resources.GetObject("btnPrint.ItemInMenuAppearance.Disabled.Font")));
            this.btnPrint.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.btnPrint.ItemInMenuAppearance.Hovered.Font = ((System.Drawing.Font)(resources.GetObject("btnPrint.ItemInMenuAppearance.Hovered.Font")));
            this.btnPrint.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.btnPrint.ItemInMenuAppearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("btnPrint.ItemInMenuAppearance.Normal.Font")));
            this.btnPrint.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnPrint.ItemInMenuAppearance.Pressed.Font = ((System.Drawing.Font)(resources.GetObject("btnPrint.ItemInMenuAppearance.Pressed.Font")));
            this.btnPrint.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.btnPrint.Name = "btnPrint";
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControl1);
            this.barManager1.DockWindowTabFont = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnExcelLandscape,
            this.btnCsv,
            this.btnPdfLandscape,
            this.btnPrint,
            this.btnBLOCK,
            this.btnCUSTOMER_PENALTY,
            this.btnSAVE_TEMPLATE,
            this.btnExcelPortrait,
            this.btnPdfPortrait});
            this.barManager1.MaxItemId = 20;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1,
            this.repositoryItemMRUEdit1});
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            resources.ApplyResources(this.barDockControlTop, "barDockControlTop");
            this.barDockControlTop.Manager = this.barManager1;
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            resources.ApplyResources(this.barDockControlBottom, "barDockControlBottom");
            this.barDockControlBottom.Manager = this.barManager1;
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            resources.ApplyResources(this.barDockControlLeft, "barDockControlLeft");
            this.barDockControlLeft.Manager = this.barManager1;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            resources.ApplyResources(this.barDockControl1, "barDockControl1");
            this.barDockControl1.Manager = this.barManager1;
            // 
            // btnBLOCK
            // 
            resources.ApplyResources(this.btnBLOCK, "btnBLOCK");
            this.btnBLOCK.Id = 5;
            this.btnBLOCK.ItemAppearance.Disabled.Font = ((System.Drawing.Font)(resources.GetObject("btnBLOCK.ItemAppearance.Disabled.Font")));
            this.btnBLOCK.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnBLOCK.ItemAppearance.Hovered.Font = ((System.Drawing.Font)(resources.GetObject("btnBLOCK.ItemAppearance.Hovered.Font")));
            this.btnBLOCK.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnBLOCK.ItemAppearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("btnBLOCK.ItemAppearance.Normal.Font")));
            this.btnBLOCK.ItemAppearance.Normal.Options.UseFont = true;
            this.btnBLOCK.ItemAppearance.Pressed.Font = ((System.Drawing.Font)(resources.GetObject("btnBLOCK.ItemAppearance.Pressed.Font")));
            this.btnBLOCK.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnBLOCK.ItemInMenuAppearance.Disabled.Font = ((System.Drawing.Font)(resources.GetObject("btnBLOCK.ItemInMenuAppearance.Disabled.Font")));
            this.btnBLOCK.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.btnBLOCK.ItemInMenuAppearance.Hovered.Font = ((System.Drawing.Font)(resources.GetObject("btnBLOCK.ItemInMenuAppearance.Hovered.Font")));
            this.btnBLOCK.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.btnBLOCK.ItemInMenuAppearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("btnBLOCK.ItemInMenuAppearance.Normal.Font")));
            this.btnBLOCK.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnBLOCK.ItemInMenuAppearance.Pressed.Font = ((System.Drawing.Font)(resources.GetObject("btnBLOCK.ItemInMenuAppearance.Pressed.Font")));
            this.btnBLOCK.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.btnBLOCK.Name = "btnBLOCK";
            // 
            // btnCUSTOMER_PENALTY
            // 
            resources.ApplyResources(this.btnCUSTOMER_PENALTY, "btnCUSTOMER_PENALTY");
            this.btnCUSTOMER_PENALTY.Id = 7;
            this.btnCUSTOMER_PENALTY.ItemAppearance.Disabled.Font = ((System.Drawing.Font)(resources.GetObject("btnCUSTOMER_PENALTY.ItemAppearance.Disabled.Font")));
            this.btnCUSTOMER_PENALTY.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnCUSTOMER_PENALTY.ItemAppearance.Hovered.Font = ((System.Drawing.Font)(resources.GetObject("btnCUSTOMER_PENALTY.ItemAppearance.Hovered.Font")));
            this.btnCUSTOMER_PENALTY.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnCUSTOMER_PENALTY.ItemAppearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("btnCUSTOMER_PENALTY.ItemAppearance.Normal.Font")));
            this.btnCUSTOMER_PENALTY.ItemAppearance.Normal.Options.UseFont = true;
            this.btnCUSTOMER_PENALTY.ItemAppearance.Pressed.Font = ((System.Drawing.Font)(resources.GetObject("btnCUSTOMER_PENALTY.ItemAppearance.Pressed.Font")));
            this.btnCUSTOMER_PENALTY.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnCUSTOMER_PENALTY.ItemInMenuAppearance.Disabled.Font = ((System.Drawing.Font)(resources.GetObject("btnCUSTOMER_PENALTY.ItemInMenuAppearance.Disabled.Font")));
            this.btnCUSTOMER_PENALTY.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.btnCUSTOMER_PENALTY.ItemInMenuAppearance.Hovered.Font = ((System.Drawing.Font)(resources.GetObject("btnCUSTOMER_PENALTY.ItemInMenuAppearance.Hovered.Font")));
            this.btnCUSTOMER_PENALTY.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.btnCUSTOMER_PENALTY.ItemInMenuAppearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("btnCUSTOMER_PENALTY.ItemInMenuAppearance.Normal.Font")));
            this.btnCUSTOMER_PENALTY.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnCUSTOMER_PENALTY.ItemInMenuAppearance.Pressed.Font = ((System.Drawing.Font)(resources.GetObject("btnCUSTOMER_PENALTY.ItemInMenuAppearance.Pressed.Font")));
            this.btnCUSTOMER_PENALTY.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.btnCUSTOMER_PENALTY.Name = "btnCUSTOMER_PENALTY";
            // 
            // btnSAVE_TEMPLATE
            // 
            resources.ApplyResources(this.btnSAVE_TEMPLATE, "btnSAVE_TEMPLATE");
            this.btnSAVE_TEMPLATE.Id = 10;
            this.btnSAVE_TEMPLATE.ItemAppearance.Disabled.Font = ((System.Drawing.Font)(resources.GetObject("btnSAVE_TEMPLATE.ItemAppearance.Disabled.Font")));
            this.btnSAVE_TEMPLATE.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnSAVE_TEMPLATE.ItemAppearance.Hovered.Font = ((System.Drawing.Font)(resources.GetObject("btnSAVE_TEMPLATE.ItemAppearance.Hovered.Font")));
            this.btnSAVE_TEMPLATE.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnSAVE_TEMPLATE.ItemAppearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("btnSAVE_TEMPLATE.ItemAppearance.Normal.Font")));
            this.btnSAVE_TEMPLATE.ItemAppearance.Normal.Options.UseFont = true;
            this.btnSAVE_TEMPLATE.ItemAppearance.Pressed.Font = ((System.Drawing.Font)(resources.GetObject("btnSAVE_TEMPLATE.ItemAppearance.Pressed.Font")));
            this.btnSAVE_TEMPLATE.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnSAVE_TEMPLATE.ItemInMenuAppearance.Disabled.Font = ((System.Drawing.Font)(resources.GetObject("btnSAVE_TEMPLATE.ItemInMenuAppearance.Disabled.Font")));
            this.btnSAVE_TEMPLATE.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.btnSAVE_TEMPLATE.ItemInMenuAppearance.Hovered.Font = ((System.Drawing.Font)(resources.GetObject("btnSAVE_TEMPLATE.ItemInMenuAppearance.Hovered.Font")));
            this.btnSAVE_TEMPLATE.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.btnSAVE_TEMPLATE.ItemInMenuAppearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("btnSAVE_TEMPLATE.ItemInMenuAppearance.Normal.Font")));
            this.btnSAVE_TEMPLATE.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnSAVE_TEMPLATE.ItemInMenuAppearance.Pressed.Font = ((System.Drawing.Font)(resources.GetObject("btnSAVE_TEMPLATE.ItemInMenuAppearance.Pressed.Font")));
            this.btnSAVE_TEMPLATE.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.btnSAVE_TEMPLATE.Name = "btnSAVE_TEMPLATE";
            // 
            // repositoryItemButtonEdit1
            // 
            resources.ApplyResources(this.repositoryItemButtonEdit1, "repositoryItemButtonEdit1");
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            // 
            // repositoryItemMRUEdit1
            // 
            resources.ApplyResources(this.repositoryItemMRUEdit1, "repositoryItemMRUEdit1");
            this.repositoryItemMRUEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("repositoryItemMRUEdit1.Buttons"))))});
            this.repositoryItemMRUEdit1.Name = "repositoryItemMRUEdit1";
            // 
            // dpAction
            // 
            resources.ApplyResources(this.dpAction, "dpAction");
            this.dpAction.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.dpAction.Appearance.BorderColor = System.Drawing.Color.Transparent;
            this.dpAction.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("dpAction.Appearance.Font")));
            this.dpAction.Appearance.Options.UseBackColor = true;
            this.dpAction.Appearance.Options.UseBorderColor = true;
            this.dpAction.Appearance.Options.UseFont = true;
            this.dpAction.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            this.dpAction.DropDownControl = this.popActions;
            this.dpAction.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("dpAction.ImageOptions.Image")));
            this.dpAction.ImageOptions.ImageUri.Uri = "WrapText;Size32x32;GrayScaled";
            this.dpAction.ImageOptions.SvgImageColorizationMode = DevExpress.Utils.SvgImageColorizationMode.Full;
            this.dpAction.LookAndFeel.SkinMaskColor = System.Drawing.Color.Transparent;
            this.dpAction.LookAndFeel.SkinMaskColor2 = System.Drawing.Color.Transparent;
            this.dpAction.LookAndFeel.UseDefaultLookAndFeel = false;
            this.dpAction.Name = "dpAction";
            this.dpAction.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            // 
            // popActions
            // 
            this.popActions.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnBLOCK, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnCUSTOMER_PENALTY),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSAVE_TEMPLATE, true)});
            this.popActions.Manager = this.barManager1;
            this.popActions.Name = "popActions";
            // 
            // searchControl
            // 
            this.searchControl.Client = this.dgv;
            resources.ApplyResources(this.searchControl, "searchControl");
            this.searchControl.Name = "searchControl";
            this.searchControl.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("searchControl.Properties.Appearance.Font")));
            this.searchControl.Properties.Appearance.Options.UseFont = true;
            this.searchControl.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Repository.ClearButton(),
            new DevExpress.XtraEditors.Repository.SearchButton()});
            this.searchControl.Properties.Client = this.dgv;
            this.searchControl.Properties.DropDownRows = 10;
            this.searchControl.Properties.FindDelay = 500;
            this.searchControl.Properties.Padding = new System.Windows.Forms.Padding(1);
            // 
            // dgv
            // 
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.MainView = this.dgvCustomerBlock;
            this.dgv.Name = "dgv";
            this.dgv.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.resCurrency,
            this.resCurrencySign});
            this.dgv.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.dgvCustomerBlock});
            // 
            // dgvCustomerBlock
            // 
            this.dgvCustomerBlock.Appearance.GroupFooter.BackColor = System.Drawing.Color.Transparent;
            this.dgvCustomerBlock.Appearance.GroupFooter.BackColor2 = ((System.Drawing.Color)(resources.GetObject("dgvCustomerBlock.Appearance.GroupFooter.BackColor2")));
            this.dgvCustomerBlock.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Transparent;
            this.dgvCustomerBlock.Appearance.GroupFooter.Options.UseBackColor = true;
            this.dgvCustomerBlock.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.dgvCustomerBlock.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.dgvCustomerBlock.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCUSTOMER_CODE,
            this.colCUSTOMER_NAME,
            this.colMETER_CODE,
            this.colCUSTOMER_GROUP,
            this.colCUSTOMER_CONNECTION_TYPE,
            this.colAREA,
            this.colCYCLE_NAME,
            this.colDUE_DATE,
            this.colDAY,
            this.colSTATUS,
            this.colDUE_AMOUNT,
            this.colCURRENCY,
            this.colCURRENCY_SIGN,
            this.colPOLE,
            this.colBOX,
            this.colPHONE,
            this.colPENALTY,
            this.colROW_NO});
            this.dgvCustomerBlock.FooterPanelHeight = 10;
            this.dgvCustomerBlock.GridControl = this.dgv;
            this.dgvCustomerBlock.GroupCount = 2;
            this.dgvCustomerBlock.GroupRowHeight = 20;
            this.dgvCustomerBlock.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("dgvCustomerBlock.GroupSummary"))), resources.GetString("dgvCustomerBlock.GroupSummary1"), ((DevExpress.XtraGrid.Columns.GridColumn)(resources.GetObject("dgvCustomerBlock.GroupSummary2"))), resources.GetString("dgvCustomerBlock.GroupSummary3"), resources.GetString("dgvCustomerBlock.GroupSummary4")),
            new DevExpress.XtraGrid.GridGroupSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("dgvCustomerBlock.GroupSummary5"))), resources.GetString("dgvCustomerBlock.GroupSummary6"), ((DevExpress.XtraGrid.Columns.GridColumn)(resources.GetObject("dgvCustomerBlock.GroupSummary7"))), resources.GetString("dgvCustomerBlock.GroupSummary8"), resources.GetString("dgvCustomerBlock.GroupSummary9"))});
            this.dgvCustomerBlock.LevelIndent = 1;
            this.dgvCustomerBlock.Name = "dgvCustomerBlock";
            this.dgvCustomerBlock.OptionsBehavior.Editable = false;
            this.dgvCustomerBlock.OptionsBehavior.ReadOnly = true;
            this.dgvCustomerBlock.OptionsDetail.EnableMasterViewMode = false;
            this.dgvCustomerBlock.OptionsDetail.SmartDetailExpand = false;
            this.dgvCustomerBlock.OptionsMenu.EnableFooterMenu = false;
            this.dgvCustomerBlock.OptionsMenu.EnableGroupPanelMenu = false;
            this.dgvCustomerBlock.OptionsPrint.EnableAppearanceEvenRow = true;
            this.dgvCustomerBlock.OptionsPrint.EnableAppearanceOddRow = true;
            this.dgvCustomerBlock.OptionsPrint.ExpandAllGroups = false;
            this.dgvCustomerBlock.OptionsPrint.PrintFooter = false;
            this.dgvCustomerBlock.OptionsPrint.PrintGroupFooter = false;
            this.dgvCustomerBlock.OptionsPrint.PrintSelectedGroupChildren = DevExpress.Utils.DefaultBoolean.True;
            this.dgvCustomerBlock.OptionsPrint.SplitCellPreviewAcrossPages = true;
            this.dgvCustomerBlock.OptionsPrint.SplitDataCellAcrossPages = true;
            this.dgvCustomerBlock.OptionsSelection.CheckBoxSelectorColumnWidth = 50;
            this.dgvCustomerBlock.OptionsSelection.MultiSelect = true;
            this.dgvCustomerBlock.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.dgvCustomerBlock.OptionsSelection.ResetSelectionClickOutsideCheckboxSelector = true;
            this.dgvCustomerBlock.OptionsSelection.ShowCheckBoxSelectorInGroupRow = DevExpress.Utils.DefaultBoolean.True;
            this.dgvCustomerBlock.OptionsSelection.ShowCheckBoxSelectorInPrintExport = DevExpress.Utils.DefaultBoolean.False;
            this.dgvCustomerBlock.OptionsView.BestFitMode = DevExpress.XtraGrid.Views.Grid.GridBestFitMode.Full;
            this.dgvCustomerBlock.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.dgvCustomerBlock.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.Hidden;
            this.dgvCustomerBlock.OptionsView.RowAutoHeight = true;
            this.dgvCustomerBlock.OptionsView.ShowGroupPanel = false;
            this.dgvCustomerBlock.OptionsView.ShowIndicator = false;
            this.dgvCustomerBlock.RowHeight = 20;
            this.dgvCustomerBlock.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCURRENCY, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAREA, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.dgvCustomerBlock.GroupLevelStyle += new DevExpress.XtraGrid.Views.Grid.GroupLevelStyleEventHandler(this.dgvCustomerBlock_GroupLevelStyle);
            this.dgvCustomerBlock.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.dgvCustomerBlock_CustomUnboundColumnData);
            // 
            // colCUSTOMER_CODE
            // 
            this.colCUSTOMER_CODE.AppearanceCell.Font = ((System.Drawing.Font)(resources.GetObject("colCUSTOMER_CODE.AppearanceCell.Font")));
            this.colCUSTOMER_CODE.AppearanceCell.Options.UseFont = true;
            resources.ApplyResources(this.colCUSTOMER_CODE, "colCUSTOMER_CODE");
            this.colCUSTOMER_CODE.FieldName = "CUSTOMER_CODE";
            this.colCUSTOMER_CODE.MinWidth = 50;
            this.colCUSTOMER_CODE.Name = "colCUSTOMER_CODE";
            this.colCUSTOMER_CODE.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("colCUSTOMER_CODE.Summary"))), resources.GetString("colCUSTOMER_CODE.Summary1"), resources.GetString("colCUSTOMER_CODE.Summary2"))});
            // 
            // colCUSTOMER_NAME
            // 
            this.colCUSTOMER_NAME.AppearanceCell.Font = ((System.Drawing.Font)(resources.GetObject("colCUSTOMER_NAME.AppearanceCell.Font")));
            this.colCUSTOMER_NAME.AppearanceCell.Options.UseFont = true;
            resources.ApplyResources(this.colCUSTOMER_NAME, "colCUSTOMER_NAME");
            this.colCUSTOMER_NAME.FieldName = "CUSTOMER_NAME";
            this.colCUSTOMER_NAME.Name = "colCUSTOMER_NAME";
            // 
            // colMETER_CODE
            // 
            this.colMETER_CODE.AppearanceCell.Font = ((System.Drawing.Font)(resources.GetObject("colMETER_CODE.AppearanceCell.Font")));
            this.colMETER_CODE.AppearanceCell.Options.UseFont = true;
            resources.ApplyResources(this.colMETER_CODE, "colMETER_CODE");
            this.colMETER_CODE.FieldName = "METER_CODE";
            this.colMETER_CODE.MinWidth = 100;
            this.colMETER_CODE.Name = "colMETER_CODE";
            // 
            // colCUSTOMER_GROUP
            // 
            this.colCUSTOMER_GROUP.AppearanceCell.Font = ((System.Drawing.Font)(resources.GetObject("colCUSTOMER_GROUP.AppearanceCell.Font")));
            this.colCUSTOMER_GROUP.AppearanceCell.Options.UseFont = true;
            resources.ApplyResources(this.colCUSTOMER_GROUP, "colCUSTOMER_GROUP");
            this.colCUSTOMER_GROUP.FieldName = "CUSTOMER_GROUP";
            this.colCUSTOMER_GROUP.Name = "colCUSTOMER_GROUP";
            // 
            // colCUSTOMER_CONNECTION_TYPE
            // 
            this.colCUSTOMER_CONNECTION_TYPE.AppearanceCell.Font = ((System.Drawing.Font)(resources.GetObject("colCUSTOMER_CONNECTION_TYPE.AppearanceCell.Font")));
            this.colCUSTOMER_CONNECTION_TYPE.AppearanceCell.Options.UseFont = true;
            resources.ApplyResources(this.colCUSTOMER_CONNECTION_TYPE, "colCUSTOMER_CONNECTION_TYPE");
            this.colCUSTOMER_CONNECTION_TYPE.FieldName = "CUSTOMER_CONNECTION_TYPE";
            this.colCUSTOMER_CONNECTION_TYPE.Name = "colCUSTOMER_CONNECTION_TYPE";
            // 
            // colAREA
            // 
            this.colAREA.AppearanceCell.Font = ((System.Drawing.Font)(resources.GetObject("colAREA.AppearanceCell.Font")));
            this.colAREA.AppearanceCell.Options.UseFont = true;
            resources.ApplyResources(this.colAREA, "colAREA");
            this.colAREA.FieldName = "AREA";
            this.colAREA.Name = "colAREA";
            // 
            // colCYCLE_NAME
            // 
            this.colCYCLE_NAME.AppearanceCell.Font = ((System.Drawing.Font)(resources.GetObject("colCYCLE_NAME.AppearanceCell.Font")));
            this.colCYCLE_NAME.AppearanceCell.Options.UseFont = true;
            resources.ApplyResources(this.colCYCLE_NAME, "colCYCLE_NAME");
            this.colCYCLE_NAME.FieldName = "CYCLE_NAME";
            this.colCYCLE_NAME.Name = "colCYCLE_NAME";
            // 
            // colDUE_DATE
            // 
            this.colDUE_DATE.AppearanceCell.Font = ((System.Drawing.Font)(resources.GetObject("colDUE_DATE.AppearanceCell.Font")));
            this.colDUE_DATE.AppearanceCell.Options.UseFont = true;
            resources.ApplyResources(this.colDUE_DATE, "colDUE_DATE");
            this.colDUE_DATE.DisplayFormat.FormatString = "dd-MM-yyyy";
            this.colDUE_DATE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colDUE_DATE.FieldName = "DUE_DATE";
            this.colDUE_DATE.Name = "colDUE_DATE";
            // 
            // colDAY
            // 
            this.colDAY.AppearanceCell.Font = ((System.Drawing.Font)(resources.GetObject("colDAY.AppearanceCell.Font")));
            this.colDAY.AppearanceCell.Options.UseFont = true;
            resources.ApplyResources(this.colDAY, "colDAY");
            this.colDAY.FieldName = "DAY";
            this.colDAY.Name = "colDAY";
            // 
            // colSTATUS
            // 
            this.colSTATUS.AppearanceCell.Font = ((System.Drawing.Font)(resources.GetObject("colSTATUS.AppearanceCell.Font")));
            this.colSTATUS.AppearanceCell.Options.UseFont = true;
            resources.ApplyResources(this.colSTATUS, "colSTATUS");
            this.colSTATUS.FieldName = "STATUS";
            this.colSTATUS.Name = "colSTATUS";
            // 
            // colDUE_AMOUNT
            // 
            this.colDUE_AMOUNT.AppearanceCell.Font = ((System.Drawing.Font)(resources.GetObject("colDUE_AMOUNT.AppearanceCell.Font")));
            this.colDUE_AMOUNT.AppearanceCell.Options.UseFont = true;
            resources.ApplyResources(this.colDUE_AMOUNT, "colDUE_AMOUNT");
            this.colDUE_AMOUNT.DisplayFormat.FormatString = "#,##0.####";
            this.colDUE_AMOUNT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colDUE_AMOUNT.FieldName = "DUE_AMOUNT";
            this.colDUE_AMOUNT.MinWidth = 100;
            this.colDUE_AMOUNT.Name = "colDUE_AMOUNT";
            this.colDUE_AMOUNT.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colDUE_AMOUNT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(((DevExpress.Data.SummaryItemType)(resources.GetObject("colDUE_AMOUNT.Summary"))), resources.GetString("colDUE_AMOUNT.Summary1"), resources.GetString("colDUE_AMOUNT.Summary2"), resources.GetString("colDUE_AMOUNT.Summary3"))});
            // 
            // colCURRENCY
            // 
            this.colCURRENCY.AppearanceCell.Font = ((System.Drawing.Font)(resources.GetObject("colCURRENCY.AppearanceCell.Font")));
            this.colCURRENCY.AppearanceCell.Options.UseFont = true;
            resources.ApplyResources(this.colCURRENCY, "colCURRENCY");
            this.colCURRENCY.ColumnEdit = this.resCurrency;
            this.colCURRENCY.FieldName = "CURRENCY";
            this.colCURRENCY.MinWidth = 50;
            this.colCURRENCY.Name = "colCURRENCY";
            // 
            // resCurrency
            // 
            resources.ApplyResources(this.resCurrency, "resCurrency");
            this.resCurrency.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("resCurrency.Buttons"))))});
            this.resCurrency.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("resCurrency.Columns"), resources.GetString("resCurrency.Columns1")),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("resCurrency.Columns2"), resources.GetString("resCurrency.Columns3")),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("resCurrency.Columns4"), resources.GetString("resCurrency.Columns5")),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("resCurrency.Columns6"), resources.GetString("resCurrency.Columns7"))});
            this.resCurrency.DisplayMember = "CURRENCY_NAME";
            this.resCurrency.Name = "resCurrency";
            this.resCurrency.ValueMember = "CURRENCY_ID";
            // 
            // colCURRENCY_SIGN
            // 
            this.colCURRENCY_SIGN.AppearanceCell.Font = ((System.Drawing.Font)(resources.GetObject("colCURRENCY_SIGN.AppearanceCell.Font")));
            this.colCURRENCY_SIGN.AppearanceCell.Options.UseFont = true;
            resources.ApplyResources(this.colCURRENCY_SIGN, "colCURRENCY_SIGN");
            this.colCURRENCY_SIGN.ColumnEdit = this.resCurrencySign;
            this.colCURRENCY_SIGN.FieldName = "CURRENCY_ID";
            this.colCURRENCY_SIGN.MaxWidth = 20;
            this.colCURRENCY_SIGN.MinWidth = 15;
            this.colCURRENCY_SIGN.Name = "colCURRENCY_SIGN";
            this.colCURRENCY_SIGN.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colCURRENCY_SIGN.OptionsColumn.ShowCaption = false;
            // 
            // resCurrencySign
            // 
            resources.ApplyResources(this.resCurrencySign, "resCurrencySign");
            this.resCurrencySign.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("resCurrencySign.Buttons"))))});
            this.resCurrencySign.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("resCurrencySign.Columns"), resources.GetString("resCurrencySign.Columns1")),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("resCurrencySign.Columns2"), resources.GetString("resCurrencySign.Columns3"))});
            this.resCurrencySign.DisplayMember = "CURRENCY_SIGN";
            this.resCurrencySign.Name = "resCurrencySign";
            this.resCurrencySign.ValueMember = "CURRENCY_ID";
            // 
            // colPOLE
            // 
            this.colPOLE.AppearanceCell.Font = ((System.Drawing.Font)(resources.GetObject("colPOLE.AppearanceCell.Font")));
            this.colPOLE.AppearanceCell.Options.UseFont = true;
            resources.ApplyResources(this.colPOLE, "colPOLE");
            this.colPOLE.FieldName = "POLE";
            this.colPOLE.Name = "colPOLE";
            // 
            // colBOX
            // 
            this.colBOX.AppearanceCell.Font = ((System.Drawing.Font)(resources.GetObject("colBOX.AppearanceCell.Font")));
            this.colBOX.AppearanceCell.Options.UseFont = true;
            resources.ApplyResources(this.colBOX, "colBOX");
            this.colBOX.FieldName = "BOX";
            this.colBOX.Name = "colBOX";
            // 
            // colPHONE
            // 
            this.colPHONE.AppearanceCell.Font = ((System.Drawing.Font)(resources.GetObject("colPHONE.AppearanceCell.Font")));
            this.colPHONE.AppearanceCell.Options.UseFont = true;
            resources.ApplyResources(this.colPHONE, "colPHONE");
            this.colPHONE.FieldName = "PHONE";
            this.colPHONE.Name = "colPHONE";
            // 
            // colPENALTY
            // 
            this.colPENALTY.AppearanceCell.Font = ((System.Drawing.Font)(resources.GetObject("colPENALTY.AppearanceCell.Font")));
            this.colPENALTY.AppearanceCell.Options.UseFont = true;
            resources.ApplyResources(this.colPENALTY, "colPENALTY");
            this.colPENALTY.FieldName = "PENALTY";
            this.colPENALTY.Name = "colPENALTY";
            // 
            // colROW_NO
            // 
            this.colROW_NO.AppearanceCell.Options.UseTextOptions = true;
            this.colROW_NO.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colROW_NO.AppearanceHeader.Options.UseTextOptions = true;
            this.colROW_NO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            resources.ApplyResources(this.colROW_NO, "colROW_NO");
            this.colROW_NO.FieldName = "colROW_NO";
            this.colROW_NO.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colROW_NO.ImageOptions.Alignment = ((System.Drawing.StringAlignment)(resources.GetObject("colROW_NO.ImageOptions.Alignment")));
            this.colROW_NO.MinWidth = 45;
            this.colROW_NO.Name = "colROW_NO";
            this.colROW_NO.OptionsColumn.AllowEdit = false;
            this.colROW_NO.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colROW_NO.OptionsColumn.AllowMove = false;
            this.colROW_NO.OptionsColumn.AllowShowHide = false;
            this.colROW_NO.OptionsColumn.AllowSize = false;
            this.colROW_NO.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colROW_NO.OptionsColumn.FixedWidth = true;
            this.colROW_NO.OptionsColumn.Printable = DevExpress.Utils.DefaultBoolean.True;
            this.colROW_NO.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "CUSTOMER_ID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn1, "dataGridViewTextBoxColumn1");
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "CUSTOMER_CODE";
            resources.ApplyResources(this.dataGridViewTextBoxColumn2, "dataGridViewTextBoxColumn2");
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "CUSTOMER_NAME";
            resources.ApplyResources(this.dataGridViewTextBoxColumn3, "dataGridViewTextBoxColumn3");
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "CUSTOMER_TYPE_NAME";
            resources.ApplyResources(this.dataGridViewTextBoxColumn4, "dataGridViewTextBoxColumn4");
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "AREA_NAME";
            resources.ApplyResources(this.dataGridViewTextBoxColumn5, "dataGridViewTextBoxColumn5");
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "DUE_DATE";
            dataGridViewCellStyle1.Format = "dd-MMM-yyyy";
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle1;
            resources.ApplyResources(this.dataGridViewTextBoxColumn6, "dataGridViewTextBoxColumn6");
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "Day";
            resources.ApplyResources(this.dataGridViewTextBoxColumn7, "dataGridViewTextBoxColumn7");
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "CUT_OFF_DAY";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N2";
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.dataGridViewTextBoxColumn8, "dataGridViewTextBoxColumn8");
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // btnCASH_COLLECTION_DETAIL
            // 
            resources.ApplyResources(this.btnCASH_COLLECTION_DETAIL, "btnCASH_COLLECTION_DETAIL");
            this.btnCASH_COLLECTION_DETAIL.Id = 4;
            this.btnCASH_COLLECTION_DETAIL.ItemAppearance.Disabled.Font = ((System.Drawing.Font)(resources.GetObject("btnCASH_COLLECTION_DETAIL.ItemAppearance.Disabled.Font")));
            this.btnCASH_COLLECTION_DETAIL.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnCASH_COLLECTION_DETAIL.ItemAppearance.Hovered.Font = ((System.Drawing.Font)(resources.GetObject("btnCASH_COLLECTION_DETAIL.ItemAppearance.Hovered.Font")));
            this.btnCASH_COLLECTION_DETAIL.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnCASH_COLLECTION_DETAIL.ItemAppearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("btnCASH_COLLECTION_DETAIL.ItemAppearance.Normal.Font")));
            this.btnCASH_COLLECTION_DETAIL.ItemAppearance.Normal.Options.UseFont = true;
            this.btnCASH_COLLECTION_DETAIL.ItemAppearance.Pressed.Font = ((System.Drawing.Font)(resources.GetObject("btnCASH_COLLECTION_DETAIL.ItemAppearance.Pressed.Font")));
            this.btnCASH_COLLECTION_DETAIL.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnCASH_COLLECTION_DETAIL.ItemInMenuAppearance.Disabled.Font = ((System.Drawing.Font)(resources.GetObject("btnCASH_COLLECTION_DETAIL.ItemInMenuAppearance.Disabled.Font")));
            this.btnCASH_COLLECTION_DETAIL.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.btnCASH_COLLECTION_DETAIL.ItemInMenuAppearance.Hovered.Font = ((System.Drawing.Font)(resources.GetObject("btnCASH_COLLECTION_DETAIL.ItemInMenuAppearance.Hovered.Font")));
            this.btnCASH_COLLECTION_DETAIL.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.btnCASH_COLLECTION_DETAIL.ItemInMenuAppearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("btnCASH_COLLECTION_DETAIL.ItemInMenuAppearance.Normal.Font")));
            this.btnCASH_COLLECTION_DETAIL.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnCASH_COLLECTION_DETAIL.ItemInMenuAppearance.Pressed.Font = ((System.Drawing.Font)(resources.GetObject("btnCASH_COLLECTION_DETAIL.ItemInMenuAppearance.Pressed.Font")));
            this.btnCASH_COLLECTION_DETAIL.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.btnCASH_COLLECTION_DETAIL.Name = "btnCASH_COLLECTION_DETAIL";
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            resources.ApplyResources(this.barDockControlRight, "barDockControlRight");
            this.barDockControlRight.Manager = null;
            // 
            // PageCustomerBlock
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControl1);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "PageCustomerBlock";
            this.panel1.ResumeLayout(false);
            this.flowLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupExport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMRUEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popActions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchControl.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomerBlock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resCurrencySign)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Panel panel1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private DevExpress.XtraGrid.GridControl dgv;
        private DevExpress.XtraGrid.Views.Grid.GridView dgvCustomerBlock;
        private DevExpress.XtraGrid.Columns.GridColumn colCUSTOMER_CODE;
        private DevExpress.XtraGrid.Columns.GridColumn colCUSTOMER_NAME;
        private DevExpress.XtraGrid.Columns.GridColumn colCUSTOMER_GROUP;
        private DevExpress.XtraGrid.Columns.GridColumn colCUSTOMER_CONNECTION_TYPE;
        private DevExpress.XtraGrid.Columns.GridColumn colAREA;
        private DevExpress.XtraGrid.Columns.GridColumn colCYCLE_NAME;
        private DevExpress.XtraGrid.Columns.GridColumn colDUE_DATE;
        private DevExpress.XtraGrid.Columns.GridColumn colDAY;
        private DevExpress.XtraGrid.Columns.GridColumn colDUE_AMOUNT;
        private DevExpress.XtraGrid.Columns.GridColumn colCURRENCY;
        private DevExpress.XtraEditors.SearchControl searchControl;
        private FlowLayoutPanel flowLayoutPanel3;
        private DevExpress.XtraEditors.DropDownButton dpExport;
        private DevExpress.XtraEditors.DropDownButton dpAction;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit resCurrency;
        private DevExpress.XtraBars.BarButtonItem btnCASH_COLLECTION_DETAIL;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.PopupMenu popActions;
        private DevExpress.XtraBars.BarButtonItem btnBLOCK;
        private DevExpress.XtraBars.BarButtonItem btnCUSTOMER_PENALTY;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarButtonItem btnExcelLandscape;
        private DevExpress.XtraBars.BarButtonItem btnCsv;
        private DevExpress.XtraBars.BarButtonItem btnPdfLandscape;
        private DevExpress.XtraBars.BarButtonItem btnPrint;
        private DevExpress.XtraBars.PopupMenu popupExport;
        private DevExpress.XtraBars.BarButtonItem btnSAVE_TEMPLATE;
        private DevExpress.XtraGrid.Columns.GridColumn colMETER_CODE;
        private DevExpress.XtraGrid.Columns.GridColumn colSTATUS;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMRUEdit repositoryItemMRUEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colCURRENCY_SIGN;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit resCurrencySign;
        private DevExpress.XtraGrid.Columns.GridColumn colPOLE;
        private DevExpress.XtraGrid.Columns.GridColumn colBOX;
        private ExButton btnSETTING;
        private DevExpress.XtraGrid.Columns.GridColumn colPHONE;
        private DevExpress.XtraGrid.Columns.GridColumn colPENALTY;
        private DevExpress.XtraBars.BarButtonItem btnExcelPortrait;
        private DevExpress.XtraBars.BarButtonItem btnPdfPortrait;
        private DevExpress.XtraGrid.Columns.GridColumn colROW_NO;
    }
}
