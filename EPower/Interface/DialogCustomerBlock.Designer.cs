﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogCustomerBlock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCustomerBlock));
            this.lblBLOCK_DATE = new System.Windows.Forms.Label();
            this.lblCUSTOMER_INFORMATION = new System.Windows.Forms.Label();
            this.txtMeterAmp = new System.Windows.Forms.TextBox();
            this.txtMeterVol = new System.Windows.Forms.TextBox();
            this.txtMeterPhase = new System.Windows.Forms.TextBox();
            this.lblCUSTOMER_CONNECTION_TYPE = new System.Windows.Forms.Label();
            this.lblMETER_CODE = new System.Windows.Forms.Label();
            this.lblPOLE = new System.Windows.Forms.Label();
            this.lblMETER_INSTALLATION = new System.Windows.Forms.Label();
            this.lblMETER_TYPE = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.lblAMPARE = new System.Windows.Forms.Label();
            this.lblVOLTAGE = new System.Windows.Forms.Label();
            this.lblPHASE = new System.Windows.Forms.Label();
            this.lblCUSTOMER_NAME = new System.Windows.Forms.Label();
            this.txtFullName = new System.Windows.Forms.TextBox();
            this.lblBOX = new System.Windows.Forms.Label();
            this.lblPOLE_AND_BOX_INSTALLATION = new System.Windows.Forms.Label();
            this.txtMeterType = new System.Windows.Forms.TextBox();
            this.txtPoleCode = new System.Windows.Forms.TextBox();
            this.txtCustomerConnectionType = new System.Windows.Forms.TextBox();
            this.dtpBlockDate = new System.Windows.Forms.DateTimePicker();
            this.lblCONSTANT = new System.Windows.Forms.Label();
            this.txtMeterConstant = new System.Windows.Forms.TextBox();
            this.txtMeterCode = new System.Windows.Forms.TextBox();
            this.txtBoxCode = new System.Windows.Forms.TextBox();
            this.dtpDueDate = new System.Windows.Forms.DateTimePicker();
            this.lblDUE_DATE = new System.Windows.Forms.Label();
            this.panelService = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.lblCURRENCY = new System.Windows.Forms.Label();
            this.lblCUSTOMER_CHARGE = new System.Windows.Forms.Label();
            this.lblDUE_AMOUNT = new System.Windows.Forms.Label();
            this.lblPAY_AMOUNT = new System.Windows.Forms.Label();
            this.txtDueAmount = new System.Windows.Forms.TextBox();
            this.lblReadAmount_ = new System.Windows.Forms.Label();
            this.dtpInvoice = new System.Windows.Forms.DateTimePicker();
            this.txtPayment = new System.Windows.Forms.TextBox();
            this.lblAMOUNT = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.cboInvoiceItem = new System.Windows.Forms.ComboBox();
            this.lblSERVICE = new System.Windows.Forms.Label();
            this.chkPrint = new System.Windows.Forms.CheckBox();
            this.txtCashDrawerName = new System.Windows.Forms.TextBox();
            this.txtUserCashDrawerName = new System.Windows.Forms.TextBox();
            this.lblCASHIER = new System.Windows.Forms.Label();
            this.lblCASH_DRAWER = new System.Windows.Forms.Label();
            this.lblDATE = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.txtCustomerGroup = new System.Windows.Forms.TextBox();
            this.lblCUSTOMER_TYPE = new System.Windows.Forms.Label();
            this.cboPaymentMethod = new System.Windows.Forms.ComboBox();
            this.lblPAYMENT_METHOD = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panelService.SuspendLayout();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.lblCUSTOMER_TYPE);
            this.content.Controls.Add(this.txtCustomerGroup);
            this.content.Controls.Add(this.panelService);
            this.content.Controls.Add(this.dtpDueDate);
            this.content.Controls.Add(this.lblDUE_DATE);
            this.content.Controls.Add(this.txtBoxCode);
            this.content.Controls.Add(this.txtMeterCode);
            this.content.Controls.Add(this.lblCONSTANT);
            this.content.Controls.Add(this.txtMeterConstant);
            this.content.Controls.Add(this.dtpBlockDate);
            this.content.Controls.Add(this.txtCustomerConnectionType);
            this.content.Controls.Add(this.txtPoleCode);
            this.content.Controls.Add(this.txtMeterType);
            this.content.Controls.Add(this.lblPOLE_AND_BOX_INSTALLATION);
            this.content.Controls.Add(this.lblBOX);
            this.content.Controls.Add(this.txtFullName);
            this.content.Controls.Add(this.lblCUSTOMER_NAME);
            this.content.Controls.Add(this.lblAMPARE);
            this.content.Controls.Add(this.lblVOLTAGE);
            this.content.Controls.Add(this.lblPHASE);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.lblBLOCK_DATE);
            this.content.Controls.Add(this.lblCUSTOMER_INFORMATION);
            this.content.Controls.Add(this.txtMeterAmp);
            this.content.Controls.Add(this.txtMeterVol);
            this.content.Controls.Add(this.txtMeterPhase);
            this.content.Controls.Add(this.lblCUSTOMER_CONNECTION_TYPE);
            this.content.Controls.Add(this.lblMETER_CODE);
            this.content.Controls.Add(this.lblPOLE);
            this.content.Controls.Add(this.lblMETER_INSTALLATION);
            this.content.Controls.Add(this.lblMETER_TYPE);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblMETER_TYPE, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_INSTALLATION, 0);
            this.content.Controls.SetChildIndex(this.lblPOLE, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_CODE, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_CONNECTION_TYPE, 0);
            this.content.Controls.SetChildIndex(this.txtMeterPhase, 0);
            this.content.Controls.SetChildIndex(this.txtMeterVol, 0);
            this.content.Controls.SetChildIndex(this.txtMeterAmp, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_INFORMATION, 0);
            this.content.Controls.SetChildIndex(this.lblBLOCK_DATE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.lblPHASE, 0);
            this.content.Controls.SetChildIndex(this.lblVOLTAGE, 0);
            this.content.Controls.SetChildIndex(this.lblAMPARE, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_NAME, 0);
            this.content.Controls.SetChildIndex(this.txtFullName, 0);
            this.content.Controls.SetChildIndex(this.lblBOX, 0);
            this.content.Controls.SetChildIndex(this.lblPOLE_AND_BOX_INSTALLATION, 0);
            this.content.Controls.SetChildIndex(this.txtMeterType, 0);
            this.content.Controls.SetChildIndex(this.txtPoleCode, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerConnectionType, 0);
            this.content.Controls.SetChildIndex(this.dtpBlockDate, 0);
            this.content.Controls.SetChildIndex(this.txtMeterConstant, 0);
            this.content.Controls.SetChildIndex(this.lblCONSTANT, 0);
            this.content.Controls.SetChildIndex(this.txtMeterCode, 0);
            this.content.Controls.SetChildIndex(this.txtBoxCode, 0);
            this.content.Controls.SetChildIndex(this.lblDUE_DATE, 0);
            this.content.Controls.SetChildIndex(this.dtpDueDate, 0);
            this.content.Controls.SetChildIndex(this.panelService, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerGroup, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_TYPE, 0);
            // 
            // lblBLOCK_DATE
            // 
            resources.ApplyResources(this.lblBLOCK_DATE, "lblBLOCK_DATE");
            this.lblBLOCK_DATE.Name = "lblBLOCK_DATE";
            // 
            // lblCUSTOMER_INFORMATION
            // 
            resources.ApplyResources(this.lblCUSTOMER_INFORMATION, "lblCUSTOMER_INFORMATION");
            this.lblCUSTOMER_INFORMATION.Name = "lblCUSTOMER_INFORMATION";
            // 
            // txtMeterAmp
            // 
            resources.ApplyResources(this.txtMeterAmp, "txtMeterAmp");
            this.txtMeterAmp.Name = "txtMeterAmp";
            this.txtMeterAmp.ReadOnly = true;
            this.txtMeterAmp.TabStop = false;
            // 
            // txtMeterVol
            // 
            resources.ApplyResources(this.txtMeterVol, "txtMeterVol");
            this.txtMeterVol.Name = "txtMeterVol";
            this.txtMeterVol.ReadOnly = true;
            this.txtMeterVol.TabStop = false;
            // 
            // txtMeterPhase
            // 
            resources.ApplyResources(this.txtMeterPhase, "txtMeterPhase");
            this.txtMeterPhase.Name = "txtMeterPhase";
            this.txtMeterPhase.ReadOnly = true;
            this.txtMeterPhase.TabStop = false;
            // 
            // lblCUSTOMER_CONNECTION_TYPE
            // 
            resources.ApplyResources(this.lblCUSTOMER_CONNECTION_TYPE, "lblCUSTOMER_CONNECTION_TYPE");
            this.lblCUSTOMER_CONNECTION_TYPE.Name = "lblCUSTOMER_CONNECTION_TYPE";
            // 
            // lblMETER_CODE
            // 
            resources.ApplyResources(this.lblMETER_CODE, "lblMETER_CODE");
            this.lblMETER_CODE.Name = "lblMETER_CODE";
            // 
            // lblPOLE
            // 
            resources.ApplyResources(this.lblPOLE, "lblPOLE");
            this.lblPOLE.Name = "lblPOLE";
            // 
            // lblMETER_INSTALLATION
            // 
            resources.ApplyResources(this.lblMETER_INSTALLATION, "lblMETER_INSTALLATION");
            this.lblMETER_INSTALLATION.Name = "lblMETER_INSTALLATION";
            // 
            // lblMETER_TYPE
            // 
            resources.ApplyResources(this.lblMETER_TYPE, "lblMETER_TYPE");
            this.lblMETER_TYPE.Name = "lblMETER_TYPE";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblAMPARE
            // 
            resources.ApplyResources(this.lblAMPARE, "lblAMPARE");
            this.lblAMPARE.Name = "lblAMPARE";
            // 
            // lblVOLTAGE
            // 
            resources.ApplyResources(this.lblVOLTAGE, "lblVOLTAGE");
            this.lblVOLTAGE.Name = "lblVOLTAGE";
            // 
            // lblPHASE
            // 
            resources.ApplyResources(this.lblPHASE, "lblPHASE");
            this.lblPHASE.Name = "lblPHASE";
            // 
            // lblCUSTOMER_NAME
            // 
            resources.ApplyResources(this.lblCUSTOMER_NAME, "lblCUSTOMER_NAME");
            this.lblCUSTOMER_NAME.Name = "lblCUSTOMER_NAME";
            // 
            // txtFullName
            // 
            resources.ApplyResources(this.txtFullName, "txtFullName");
            this.txtFullName.Name = "txtFullName";
            this.txtFullName.ReadOnly = true;
            this.txtFullName.TabStop = false;
            // 
            // lblBOX
            // 
            resources.ApplyResources(this.lblBOX, "lblBOX");
            this.lblBOX.Name = "lblBOX";
            // 
            // lblPOLE_AND_BOX_INSTALLATION
            // 
            resources.ApplyResources(this.lblPOLE_AND_BOX_INSTALLATION, "lblPOLE_AND_BOX_INSTALLATION");
            this.lblPOLE_AND_BOX_INSTALLATION.Name = "lblPOLE_AND_BOX_INSTALLATION";
            // 
            // txtMeterType
            // 
            resources.ApplyResources(this.txtMeterType, "txtMeterType");
            this.txtMeterType.Name = "txtMeterType";
            this.txtMeterType.ReadOnly = true;
            this.txtMeterType.TabStop = false;
            // 
            // txtPoleCode
            // 
            resources.ApplyResources(this.txtPoleCode, "txtPoleCode");
            this.txtPoleCode.Name = "txtPoleCode";
            this.txtPoleCode.ReadOnly = true;
            this.txtPoleCode.TabStop = false;
            // 
            // txtCustomerConnectionType
            // 
            resources.ApplyResources(this.txtCustomerConnectionType, "txtCustomerConnectionType");
            this.txtCustomerConnectionType.Name = "txtCustomerConnectionType";
            this.txtCustomerConnectionType.ReadOnly = true;
            this.txtCustomerConnectionType.TabStop = false;
            // 
            // dtpBlockDate
            // 
            resources.ApplyResources(this.dtpBlockDate, "dtpBlockDate");
            this.dtpBlockDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpBlockDate.Name = "dtpBlockDate";
            this.dtpBlockDate.Enter += new System.EventHandler(this.dtpBlockDate_Enter);
            // 
            // lblCONSTANT
            // 
            resources.ApplyResources(this.lblCONSTANT, "lblCONSTANT");
            this.lblCONSTANT.Name = "lblCONSTANT";
            // 
            // txtMeterConstant
            // 
            resources.ApplyResources(this.txtMeterConstant, "txtMeterConstant");
            this.txtMeterConstant.Name = "txtMeterConstant";
            this.txtMeterConstant.ReadOnly = true;
            this.txtMeterConstant.TabStop = false;
            // 
            // txtMeterCode
            // 
            resources.ApplyResources(this.txtMeterCode, "txtMeterCode");
            this.txtMeterCode.Name = "txtMeterCode";
            this.txtMeterCode.ReadOnly = true;
            this.txtMeterCode.TabStop = false;
            // 
            // txtBoxCode
            // 
            resources.ApplyResources(this.txtBoxCode, "txtBoxCode");
            this.txtBoxCode.Name = "txtBoxCode";
            this.txtBoxCode.ReadOnly = true;
            this.txtBoxCode.TabStop = false;
            // 
            // dtpDueDate
            // 
            resources.ApplyResources(this.dtpDueDate, "dtpDueDate");
            this.dtpDueDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDueDate.Name = "dtpDueDate";
            // 
            // lblDUE_DATE
            // 
            resources.ApplyResources(this.lblDUE_DATE, "lblDUE_DATE");
            this.lblDUE_DATE.Name = "lblDUE_DATE";
            // 
            // panelService
            // 
            this.panelService.Controls.Add(this.cboPaymentMethod);
            this.panelService.Controls.Add(this.lblPAYMENT_METHOD);
            this.panelService.Controls.Add(this.label2);
            this.panelService.Controls.Add(this.cboCurrency);
            this.panelService.Controls.Add(this.lblCURRENCY);
            this.panelService.Controls.Add(this.lblCUSTOMER_CHARGE);
            this.panelService.Controls.Add(this.lblDUE_AMOUNT);
            this.panelService.Controls.Add(this.lblPAY_AMOUNT);
            this.panelService.Controls.Add(this.txtDueAmount);
            this.panelService.Controls.Add(this.lblReadAmount_);
            this.panelService.Controls.Add(this.dtpInvoice);
            this.panelService.Controls.Add(this.txtPayment);
            this.panelService.Controls.Add(this.lblAMOUNT);
            this.panelService.Controls.Add(this.txtPrice);
            this.panelService.Controls.Add(this.cboInvoiceItem);
            this.panelService.Controls.Add(this.lblSERVICE);
            this.panelService.Controls.Add(this.chkPrint);
            this.panelService.Controls.Add(this.txtCashDrawerName);
            this.panelService.Controls.Add(this.txtUserCashDrawerName);
            this.panelService.Controls.Add(this.lblCASHIER);
            this.panelService.Controls.Add(this.lblCASH_DRAWER);
            this.panelService.Controls.Add(this.lblDATE);
            this.panelService.Controls.Add(this.label9);
            this.panelService.Controls.Add(this.label1);
            this.panelService.Controls.Add(this.label30);
            resources.ApplyResources(this.panelService, "panelService");
            this.panelService.Name = "panelService";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Name = "label2";
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            // 
            // lblCURRENCY
            // 
            resources.ApplyResources(this.lblCURRENCY, "lblCURRENCY");
            this.lblCURRENCY.Name = "lblCURRENCY";
            // 
            // lblCUSTOMER_CHARGE
            // 
            resources.ApplyResources(this.lblCUSTOMER_CHARGE, "lblCUSTOMER_CHARGE");
            this.lblCUSTOMER_CHARGE.Name = "lblCUSTOMER_CHARGE";
            // 
            // lblDUE_AMOUNT
            // 
            resources.ApplyResources(this.lblDUE_AMOUNT, "lblDUE_AMOUNT");
            this.lblDUE_AMOUNT.Name = "lblDUE_AMOUNT";
            // 
            // lblPAY_AMOUNT
            // 
            resources.ApplyResources(this.lblPAY_AMOUNT, "lblPAY_AMOUNT");
            this.lblPAY_AMOUNT.Name = "lblPAY_AMOUNT";
            // 
            // txtDueAmount
            // 
            resources.ApplyResources(this.txtDueAmount, "txtDueAmount");
            this.txtDueAmount.Name = "txtDueAmount";
            this.txtDueAmount.ReadOnly = true;
            // 
            // lblReadAmount_
            // 
            resources.ApplyResources(this.lblReadAmount_, "lblReadAmount_");
            this.lblReadAmount_.Name = "lblReadAmount_";
            // 
            // dtpInvoice
            // 
            resources.ApplyResources(this.dtpInvoice, "dtpInvoice");
            this.dtpInvoice.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpInvoice.Name = "dtpInvoice";
            // 
            // txtPayment
            // 
            resources.ApplyResources(this.txtPayment, "txtPayment");
            this.txtPayment.Name = "txtPayment";
            this.txtPayment.TextChanged += new System.EventHandler(this.txtPayment_TextChanged);
            this.txtPayment.Enter += new System.EventHandler(this.EnterEnglishKey);
            this.txtPayment.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrice_KeyPress);
            // 
            // lblAMOUNT
            // 
            resources.ApplyResources(this.lblAMOUNT, "lblAMOUNT");
            this.lblAMOUNT.Name = "lblAMOUNT";
            // 
            // txtPrice
            // 
            resources.ApplyResources(this.txtPrice, "txtPrice");
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.TextChanged += new System.EventHandler(this.txtPayment_TextChanged);
            this.txtPrice.Enter += new System.EventHandler(this.EnterEnglishKey);
            this.txtPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrice_KeyPress);
            // 
            // cboInvoiceItem
            // 
            this.cboInvoiceItem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboInvoiceItem, "cboInvoiceItem");
            this.cboInvoiceItem.FormattingEnabled = true;
            this.cboInvoiceItem.Name = "cboInvoiceItem";
            // 
            // lblSERVICE
            // 
            resources.ApplyResources(this.lblSERVICE, "lblSERVICE");
            this.lblSERVICE.Name = "lblSERVICE";
            // 
            // chkPrint
            // 
            resources.ApplyResources(this.chkPrint, "chkPrint");
            this.chkPrint.Checked = true;
            this.chkPrint.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkPrint.Name = "chkPrint";
            this.chkPrint.UseVisualStyleBackColor = true;
            this.chkPrint.CheckedChanged += new System.EventHandler(this.chkPrint_CheckedChanged);
            // 
            // txtCashDrawerName
            // 
            resources.ApplyResources(this.txtCashDrawerName, "txtCashDrawerName");
            this.txtCashDrawerName.Name = "txtCashDrawerName";
            this.txtCashDrawerName.ReadOnly = true;
            // 
            // txtUserCashDrawerName
            // 
            resources.ApplyResources(this.txtUserCashDrawerName, "txtUserCashDrawerName");
            this.txtUserCashDrawerName.Name = "txtUserCashDrawerName";
            this.txtUserCashDrawerName.ReadOnly = true;
            // 
            // lblCASHIER
            // 
            resources.ApplyResources(this.lblCASHIER, "lblCASHIER");
            this.lblCASHIER.Name = "lblCASHIER";
            // 
            // lblCASH_DRAWER
            // 
            resources.ApplyResources(this.lblCASH_DRAWER, "lblCASH_DRAWER");
            this.lblCASH_DRAWER.Name = "lblCASH_DRAWER";
            // 
            // lblDATE
            // 
            resources.ApplyResources(this.lblDATE, "lblDATE");
            this.lblDATE.Name = "lblDATE";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // label30
            // 
            resources.ApplyResources(this.label30, "label30");
            this.label30.ForeColor = System.Drawing.Color.Red;
            this.label30.Name = "label30";
            // 
            // txtCustomerGroup
            // 
            resources.ApplyResources(this.txtCustomerGroup, "txtCustomerGroup");
            this.txtCustomerGroup.Name = "txtCustomerGroup";
            this.txtCustomerGroup.ReadOnly = true;
            this.txtCustomerGroup.TabStop = false;
            // 
            // lblCUSTOMER_TYPE
            // 
            resources.ApplyResources(this.lblCUSTOMER_TYPE, "lblCUSTOMER_TYPE");
            this.lblCUSTOMER_TYPE.Name = "lblCUSTOMER_TYPE";
            // 
            // cboPaymentMethod
            // 
            this.cboPaymentMethod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPaymentMethod.FormattingEnabled = true;
            resources.ApplyResources(this.cboPaymentMethod, "cboPaymentMethod");
            this.cboPaymentMethod.Name = "cboPaymentMethod";
            // 
            // lblPAYMENT_METHOD
            // 
            resources.ApplyResources(this.lblPAYMENT_METHOD, "lblPAYMENT_METHOD");
            this.lblPAYMENT_METHOD.Name = "lblPAYMENT_METHOD";
            // 
            // DialogCustomerBlock
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogCustomerBlock";
            this.Load += new System.EventHandler(this.DialogCustomerBlock_Load);
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panelService.ResumeLayout(false);
            this.panelService.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Label lblBLOCK_DATE;
        private Label lblCUSTOMER_INFORMATION;
        private TextBox txtMeterAmp;
        private TextBox txtMeterVol;
        private TextBox txtMeterPhase;
        private Label lblCUSTOMER_CONNECTION_TYPE;
        private Label lblMETER_CODE;
        private Label lblPOLE;
        private Label lblMETER_INSTALLATION;
        private Label lblMETER_TYPE;
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label lblAMPARE;
        private Label lblVOLTAGE;
        private Label lblPHASE;
        private Label lblCUSTOMER_NAME;
        private TextBox txtFullName;
        private Label lblBOX;
        private Label lblPOLE_AND_BOX_INSTALLATION;
        private TextBox txtPoleCode;
        private TextBox txtMeterType;
        private TextBox txtCustomerConnectionType;
        private DateTimePicker dtpBlockDate;
        private Label lblCONSTANT;
        private TextBox txtMeterConstant;
        private TextBox txtBoxCode;
        private TextBox txtMeterCode;
        private DateTimePicker dtpDueDate;
        private Label lblDUE_DATE;
        private Panel panelService;
        private Label lblCUSTOMER_CHARGE;
        private Label label1;
        private Label lblDUE_AMOUNT;
        private Label lblPAY_AMOUNT;
        private TextBox txtDueAmount;
        private Label lblReadAmount_;
        private DateTimePicker dtpInvoice;
        private TextBox txtPayment;
        private Label label30;
        private Label lblAMOUNT;
        private TextBox txtPrice;
        private ComboBox cboInvoiceItem;
        private Label lblSERVICE;
        private CheckBox chkPrint;
        private TextBox txtCashDrawerName;
        private TextBox txtUserCashDrawerName;
        private Label lblCASHIER;
        private Label lblCASH_DRAWER;
        private Label lblDATE;
        private Label label9;
        private ComboBox cboCurrency;
        private Label lblCURRENCY;
        private Label label2;
        private Label lblCUSTOMER_TYPE;
        private TextBox txtCustomerGroup;
        private ComboBox cboPaymentMethod;
        private Label lblPAYMENT_METHOD;
    }
}
