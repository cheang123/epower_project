﻿
using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.panel5 = new System.Windows.Forms.Panel();
            this.ads = new System.Windows.Forms.Panel();
            this.lblCLOSE2 = new System.Windows.Forms.Label();
            this.pbAds = new System.Windows.Forms.PictureBox();
            this.adsRight = new System.Windows.Forms.Panel();
            this.adsTop = new System.Windows.Forms.Panel();
            this.adsLeft = new System.Windows.Forms.Panel();
            this.adsBottom = new System.Windows.Forms.Panel();
            this.main = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblDEMO = new System.Windows.Forms.Label();
            this.lblGENERAL_TASK = new System.Windows.Forms.Label();
            this.lblMINIMIZE_ = new System.Windows.Forms.Label();
            this.lblTITLE_ = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnReports = new SoftTech.Component.ExTabItem();
            this.btnSYSTEM_SECURITY = new SoftTech.Component.ExTabItem();
            this.btnACCOUNTING = new SoftTech.Component.ExTabItem();
            this.btnPOWER_MANAGEMENT = new SoftTech.Component.ExTabItem();
            this.btnADMIN_TASKS = new SoftTech.Component.ExTabItem();
            this.btnCUSTOMER_BILLING = new SoftTech.Component.ExTabItem();
            this.btnPOSTPAID = new SoftTech.Component.ExTabItem();
            this.btnPREPAID = new SoftTech.Component.ExTabItem();
            this.lblCLOSE_ = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cnmStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnOPEN_CASH_DRAWER = new System.Windows.Forms.ToolStripMenuItem();
            this.btnCLOSE_CASH_DRAWER = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnCHANGE_PASSWORD = new System.Windows.Forms.ToolStripMenuItem();
            this.btnLOG_OUT = new System.Windows.Forms.ToolStripMenuItem();
            this.btnCHANGE_LANGUAGE = new System.Windows.Forms.ToolStripMenuItem();
            this.btnKHMER_LANGUAGE = new System.Windows.Forms.ToolStripMenuItem();
            this.btnENGLISH_LANGUAGE = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnABOUT_EPOWER = new System.Windows.Forms.ToolStripMenuItem();
            this.timerAds = new System.Windows.Forms.Timer(this.components);
            this.panel5.SuspendLayout();
            this.ads.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbAds)).BeginInit();
            this.panel6.SuspendLayout();
            this.panel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.cnmStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.ads);
            this.panel5.Controls.Add(this.main);
            this.panel5.Controls.Add(this.panel8);
            this.panel5.Controls.Add(this.panel7);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 59);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1262, 481);
            this.panel5.TabIndex = 1;
            // 
            // ads
            // 
            this.ads.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ads.BackColor = System.Drawing.Color.Transparent;
            this.ads.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ads.Controls.Add(this.lblCLOSE2);
            this.ads.Controls.Add(this.pbAds);
            this.ads.Controls.Add(this.adsRight);
            this.ads.Controls.Add(this.adsTop);
            this.ads.Controls.Add(this.adsLeft);
            this.ads.Controls.Add(this.adsBottom);
            this.ads.Location = new System.Drawing.Point(6, 342);
            this.ads.Name = "ads";
            this.ads.Size = new System.Drawing.Size(204, 130);
            this.ads.TabIndex = 3;
            // 
            // lblCLOSE2
            // 
            this.lblCLOSE2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCLOSE2.AutoSize = true;
            this.lblCLOSE2.BackColor = System.Drawing.Color.Transparent;
            this.lblCLOSE2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblCLOSE2.ForeColor = System.Drawing.Color.Black;
            this.lblCLOSE2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblCLOSE2.Location = new System.Drawing.Point(189, 2);
            this.lblCLOSE2.Name = "lblCLOSE2";
            this.lblCLOSE2.Size = new System.Drawing.Size(13, 13);
            this.lblCLOSE2.TabIndex = 6;
            this.lblCLOSE2.Text = "×";
            this.lblCLOSE2.Click += new System.EventHandler(this.lblClose_Click);
            // 
            // pbAds
            // 
            this.pbAds.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbAds.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pbAds.Location = new System.Drawing.Point(1, 1);
            this.pbAds.Name = "pbAds";
            this.pbAds.Size = new System.Drawing.Size(202, 128);
            this.pbAds.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbAds.TabIndex = 11;
            this.pbAds.TabStop = false;
            this.pbAds.LoadCompleted += new System.ComponentModel.AsyncCompletedEventHandler(this.pbAds_LoadCompleted);
            // 
            // adsRight
            // 
            this.adsRight.BackColor = System.Drawing.Color.Silver;
            this.adsRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.adsRight.Location = new System.Drawing.Point(203, 1);
            this.adsRight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.adsRight.Name = "adsRight";
            this.adsRight.Size = new System.Drawing.Size(1, 128);
            this.adsRight.TabIndex = 9;
            // 
            // adsTop
            // 
            this.adsTop.BackColor = System.Drawing.Color.Silver;
            this.adsTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.adsTop.Location = new System.Drawing.Point(1, 0);
            this.adsTop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.adsTop.Name = "adsTop";
            this.adsTop.Size = new System.Drawing.Size(203, 1);
            this.adsTop.TabIndex = 8;
            // 
            // adsLeft
            // 
            this.adsLeft.BackColor = System.Drawing.Color.Silver;
            this.adsLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.adsLeft.Location = new System.Drawing.Point(0, 0);
            this.adsLeft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.adsLeft.Name = "adsLeft";
            this.adsLeft.Size = new System.Drawing.Size(1, 129);
            this.adsLeft.TabIndex = 7;
            // 
            // adsBottom
            // 
            this.adsBottom.BackColor = System.Drawing.Color.Silver;
            this.adsBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.adsBottom.Location = new System.Drawing.Point(0, 129);
            this.adsBottom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.adsBottom.Name = "adsBottom";
            this.adsBottom.Size = new System.Drawing.Size(204, 1);
            this.adsBottom.TabIndex = 5;
            // 
            // main
            // 
            this.main.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.main.Location = new System.Drawing.Point(6, 4);
            this.main.Name = "main";
            this.main.Size = new System.Drawing.Size(1250, 468);
            this.main.TabIndex = 2;
            // 
            // panel8
            // 
            this.panel8.BackgroundImage = global::EPower.Properties.Resources.main_middleright;
            this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel8.Location = new System.Drawing.Point(1255, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(7, 481);
            this.panel8.TabIndex = 1;
            // 
            // panel7
            // 
            this.panel7.BackgroundImage = global::EPower.Properties.Resources.main_middleleft;
            this.panel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(5, 481);
            this.panel7.TabIndex = 0;
            // 
            // panel6
            // 
            this.panel6.BackgroundImage = global::EPower.Properties.Resources.main_bottommiddle;
            this.panel6.Controls.Add(this.panel10);
            this.panel6.Controls.Add(this.panel9);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(0, 533);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1262, 7);
            this.panel6.TabIndex = 2;
            // 
            // panel10
            // 
            this.panel10.BackgroundImage = global::EPower.Properties.Resources.main_bottomleft;
            this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(5, 7);
            this.panel10.TabIndex = 3;
            // 
            // panel9
            // 
            this.panel9.BackgroundImage = global::EPower.Properties.Resources.main_bottomright;
            this.panel9.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel9.Location = new System.Drawing.Point(1255, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(7, 7);
            this.panel9.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.main_topmiddle;
            this.panel1.Controls.Add(this.lblDEMO);
            this.panel1.Controls.Add(this.lblGENERAL_TASK);
            this.panel1.Controls.Add(this.lblMINIMIZE_);
            this.panel1.Controls.Add(this.lblTITLE_);
            this.panel1.Controls.Add(this.panel11);
            this.panel1.Controls.Add(this.flowLayoutPanel1);
            this.panel1.Controls.Add(this.lblCLOSE_);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1262, 59);
            this.panel1.TabIndex = 0;
            // 
            // lblDEMO
            // 
            this.lblDEMO.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDEMO.Font = new System.Drawing.Font("Khmer OS System", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblDEMO.ForeColor = System.Drawing.Color.DarkRed;
            this.lblDEMO.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblDEMO.Location = new System.Drawing.Point(873, -2);
            this.lblDEMO.Name = "lblDEMO";
            this.lblDEMO.Size = new System.Drawing.Size(267, 32);
            this.lblDEMO.TabIndex = 0;
            this.lblDEMO.Text = "កម្មវិធីសំរាប់សាកល្បង";
            this.lblDEMO.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblDEMO.Visible = false;
            // 
            // lblGENERAL_TASK
            // 
            this.lblGENERAL_TASK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGENERAL_TASK.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblGENERAL_TASK.Font = new System.Drawing.Font("Khmer OS System", 7F);
            this.lblGENERAL_TASK.ForeColor = System.Drawing.Color.DimGray;
            this.lblGENERAL_TASK.Image = ((System.Drawing.Image)(resources.GetObject("lblGENERAL_TASK.Image")));
            this.lblGENERAL_TASK.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblGENERAL_TASK.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblGENERAL_TASK.Location = new System.Drawing.Point(1130, 4);
            this.lblGENERAL_TASK.Name = "lblGENERAL_TASK";
            this.lblGENERAL_TASK.Size = new System.Drawing.Size(96, 15);
            this.lblGENERAL_TASK.TabIndex = 17;
            this.lblGENERAL_TASK.Text = "ការងារទូទៅ";
            this.lblGENERAL_TASK.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblGENERAL_TASK.Click += new System.EventHandler(this.label1_Click_1);
            this.lblGENERAL_TASK.MouseLeave += new System.EventHandler(this.label1_MouseLeave);
            this.lblGENERAL_TASK.MouseHover += new System.EventHandler(this.label1_MouseHover);
            // 
            // lblMINIMIZE_
            // 
            this.lblMINIMIZE_.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMINIMIZE_.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblMINIMIZE_.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.lblMINIMIZE_.ForeColor = System.Drawing.Color.Black;
            this.lblMINIMIZE_.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblMINIMIZE_.Location = new System.Drawing.Point(1227, 3);
            this.lblMINIMIZE_.Name = "lblMINIMIZE_";
            this.lblMINIMIZE_.Size = new System.Drawing.Size(14, 16);
            this.lblMINIMIZE_.TabIndex = 16;
            this.lblMINIMIZE_.Text = "-";
            this.lblMINIMIZE_.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblMINIMIZE_.Click += new System.EventHandler(this.label1_Click);
            this.lblMINIMIZE_.MouseLeave += new System.EventHandler(this.lblMinimize_MouseLeave);
            this.lblMINIMIZE_.MouseHover += new System.EventHandler(this.lblMinimize_MouseHover);
            // 
            // lblTITLE_
            // 
            this.lblTITLE_.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTITLE_.Font = new System.Drawing.Font("Khmer OS System", 9F);
            this.lblTITLE_.ForeColor = System.Drawing.Color.DimGray;
            this.lblTITLE_.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblTITLE_.Location = new System.Drawing.Point(9, 1);
            this.lblTITLE_.Name = "lblTITLE_";
            this.lblTITLE_.Size = new System.Drawing.Size(1224, 21);
            this.lblTITLE_.TabIndex = 15;
            this.lblTITLE_.Text = "គ្រប់គ្រងការចែកចាយថាមពលអគ្គិសនី";
            this.lblTITLE_.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel11
            // 
            this.panel11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel11.BackgroundImage")));
            this.panel11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel11.Location = new System.Drawing.Point(10, 26);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(110, 28);
            this.panel11.TabIndex = 14;
            this.panel11.Click += new System.EventHandler(this.panel11_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Controls.Add(this.btnReports);
            this.flowLayoutPanel1.Controls.Add(this.btnSYSTEM_SECURITY);
            this.flowLayoutPanel1.Controls.Add(this.btnACCOUNTING);
            this.flowLayoutPanel1.Controls.Add(this.btnPOWER_MANAGEMENT);
            this.flowLayoutPanel1.Controls.Add(this.btnADMIN_TASKS);
            this.flowLayoutPanel1.Controls.Add(this.btnCUSTOMER_BILLING);
            this.flowLayoutPanel1.Controls.Add(this.btnPOSTPAID);
            this.flowLayoutPanel1.Controls.Add(this.btnPREPAID);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(31, 35);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1229, 24);
            this.flowLayoutPanel1.TabIndex = 13;
            // 
            // btnReports
            // 
            this.btnReports.BackColor = System.Drawing.Color.Transparent;
            this.btnReports.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnReports.Image = global::EPower.Properties.Resources.icon_report;
            this.btnReports.IsTitle = false;
            this.btnReports.Location = new System.Drawing.Point(1125, 1);
            this.btnReports.Margin = new System.Windows.Forms.Padding(1);
            this.btnReports.Name = "btnReports";
            this.btnReports.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnReports.Selected = false;
            this.btnReports.Size = new System.Drawing.Size(103, 23);
            this.btnReports.TabIndex = 6;
            this.btnReports.Text = "របាយការណ៍";
            this.btnReports.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnReports.Click += new System.EventHandler(this.btnReports_Click);
            // 
            // btnSYSTEM_SECURITY
            // 
            this.btnSYSTEM_SECURITY.BackColor = System.Drawing.Color.Transparent;
            this.btnSYSTEM_SECURITY.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnSYSTEM_SECURITY.Image = global::EPower.Properties.Resources.key;
            this.btnSYSTEM_SECURITY.IsTitle = false;
            this.btnSYSTEM_SECURITY.Location = new System.Drawing.Point(1009, 1);
            this.btnSYSTEM_SECURITY.Margin = new System.Windows.Forms.Padding(1);
            this.btnSYSTEM_SECURITY.Name = "btnSYSTEM_SECURITY";
            this.btnSYSTEM_SECURITY.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnSYSTEM_SECURITY.Selected = false;
            this.btnSYSTEM_SECURITY.Size = new System.Drawing.Size(114, 23);
            this.btnSYSTEM_SECURITY.TabIndex = 9;
            this.btnSYSTEM_SECURITY.Text = "សុវត្ថភាពប្រពន្ធ័";
            this.btnSYSTEM_SECURITY.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnSYSTEM_SECURITY.Click += new System.EventHandler(this.btnSecurity_Click);
            // 
            // btnACCOUNTING
            // 
            this.btnACCOUNTING.BackColor = System.Drawing.Color.Transparent;
            this.btnACCOUNTING.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnACCOUNTING.Image = global::EPower.Properties.Resources.Money;
            this.btnACCOUNTING.IsTitle = false;
            this.btnACCOUNTING.Location = new System.Drawing.Point(891, 1);
            this.btnACCOUNTING.Margin = new System.Windows.Forms.Padding(1);
            this.btnACCOUNTING.Name = "btnACCOUNTING";
            this.btnACCOUNTING.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnACCOUNTING.Selected = false;
            this.btnACCOUNTING.Size = new System.Drawing.Size(116, 25);
            this.btnACCOUNTING.TabIndex = 12;
            this.btnACCOUNTING.Text = "ផ្នែកគណនេយ្យ";
            this.btnACCOUNTING.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnACCOUNTING.Click += new System.EventHandler(this.btnACCOUNTING_Click);
            // 
            // btnPOWER_MANAGEMENT
            // 
            this.btnPOWER_MANAGEMENT.BackColor = System.Drawing.Color.Transparent;
            this.btnPOWER_MANAGEMENT.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnPOWER_MANAGEMENT.Image = global::EPower.Properties.Resources.img_power;
            this.btnPOWER_MANAGEMENT.IsTitle = false;
            this.btnPOWER_MANAGEMENT.Location = new System.Drawing.Point(764, 1);
            this.btnPOWER_MANAGEMENT.Margin = new System.Windows.Forms.Padding(1);
            this.btnPOWER_MANAGEMENT.Name = "btnPOWER_MANAGEMENT";
            this.btnPOWER_MANAGEMENT.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnPOWER_MANAGEMENT.Selected = false;
            this.btnPOWER_MANAGEMENT.Size = new System.Drawing.Size(125, 23);
            this.btnPOWER_MANAGEMENT.TabIndex = 11;
            this.btnPOWER_MANAGEMENT.Text = "គ្រប់គ្រងថាមពល";
            this.btnPOWER_MANAGEMENT.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnPOWER_MANAGEMENT.Click += new System.EventHandler(this.btnPurchasePower_Click);
            // 
            // btnADMIN_TASKS
            // 
            this.btnADMIN_TASKS.BackColor = System.Drawing.Color.Transparent;
            this.btnADMIN_TASKS.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnADMIN_TASKS.Image = global::EPower.Properties.Resources.icon_admin;
            this.btnADMIN_TASKS.IsTitle = false;
            this.btnADMIN_TASKS.Location = new System.Drawing.Point(656, 1);
            this.btnADMIN_TASKS.Margin = new System.Windows.Forms.Padding(1);
            this.btnADMIN_TASKS.Name = "btnADMIN_TASKS";
            this.btnADMIN_TASKS.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnADMIN_TASKS.Selected = false;
            this.btnADMIN_TASKS.Size = new System.Drawing.Size(106, 23);
            this.btnADMIN_TASKS.TabIndex = 5;
            this.btnADMIN_TASKS.Text = "គ្រប់គ្រងទូទៅ";
            this.btnADMIN_TASKS.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnADMIN_TASKS.Click += new System.EventHandler(this.btnAdmin_Click);
            // 
            // btnCUSTOMER_BILLING
            // 
            this.btnCUSTOMER_BILLING.BackColor = System.Drawing.Color.Transparent;
            this.btnCUSTOMER_BILLING.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnCUSTOMER_BILLING.Image = global::EPower.Properties.Resources.logo_customer;
            this.btnCUSTOMER_BILLING.IsTitle = false;
            this.btnCUSTOMER_BILLING.Location = new System.Drawing.Point(506, 1);
            this.btnCUSTOMER_BILLING.Margin = new System.Windows.Forms.Padding(1);
            this.btnCUSTOMER_BILLING.Name = "btnCUSTOMER_BILLING";
            this.btnCUSTOMER_BILLING.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnCUSTOMER_BILLING.Selected = false;
            this.btnCUSTOMER_BILLING.Size = new System.Drawing.Size(148, 23);
            this.btnCUSTOMER_BILLING.TabIndex = 7;
            this.btnCUSTOMER_BILLING.Text = "អតិថិជន និង ការទូទាត់";
            this.btnCUSTOMER_BILLING.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnCUSTOMER_BILLING.Click += new System.EventHandler(this.btnBilling_Click);
            // 
            // btnPOSTPAID
            // 
            this.btnPOSTPAID.BackColor = System.Drawing.Color.Transparent;
            this.btnPOSTPAID.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnPOSTPAID.Image = global::EPower.Properties.Resources.home;
            this.btnPOSTPAID.IsTitle = false;
            this.btnPOSTPAID.Location = new System.Drawing.Point(391, 1);
            this.btnPOSTPAID.Margin = new System.Windows.Forms.Padding(1);
            this.btnPOSTPAID.Name = "btnPOSTPAID";
            this.btnPOSTPAID.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnPOSTPAID.Selected = false;
            this.btnPOSTPAID.Size = new System.Drawing.Size(113, 23);
            this.btnPOSTPAID.TabIndex = 8;
            this.btnPOSTPAID.Text = "បង់ប្រាក់ក្រោយ";
            this.btnPOSTPAID.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnPOSTPAID.Click += new System.EventHandler(this.btnPostpaidTasks_Click);
            // 
            // btnPREPAID
            // 
            this.btnPREPAID.BackColor = System.Drawing.Color.Transparent;
            this.btnPREPAID.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnPREPAID.Image = ((System.Drawing.Image)(resources.GetObject("btnPREPAID.Image")));
            this.btnPREPAID.IsTitle = false;
            this.btnPREPAID.Location = new System.Drawing.Point(292, 1);
            this.btnPREPAID.Margin = new System.Windows.Forms.Padding(1);
            this.btnPREPAID.Name = "btnPREPAID";
            this.btnPREPAID.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnPREPAID.Selected = false;
            this.btnPREPAID.Size = new System.Drawing.Size(97, 23);
            this.btnPREPAID.TabIndex = 10;
            this.btnPREPAID.Text = "បង់ប្រាក់មុន";
            this.btnPREPAID.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnPREPAID.Visible = false;
            this.btnPREPAID.Click += new System.EventHandler(this.btnPrepaid_Click);
            // 
            // lblCLOSE_
            // 
            this.lblCLOSE_.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCLOSE_.AutoSize = true;
            this.lblCLOSE_.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblCLOSE_.ForeColor = System.Drawing.Color.Black;
            this.lblCLOSE_.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblCLOSE_.Location = new System.Drawing.Point(1239, 4);
            this.lblCLOSE_.Name = "lblCLOSE_";
            this.lblCLOSE_.Size = new System.Drawing.Size(13, 13);
            this.lblCLOSE_.TabIndex = 3;
            this.lblCLOSE_.Text = "×";
            this.lblCLOSE_.Click += new System.EventHandler(this.lblExit_Click);
            this.lblCLOSE_.MouseEnter += new System.EventHandler(this.label5_MouseEnter);
            this.lblCLOSE_.MouseLeave += new System.EventHandler(this.label5_MouseLeave);
            // 
            // panel4
            // 
            this.panel4.Location = new System.Drawing.Point(2, 75);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(233, 115);
            this.panel4.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.BackgroundImage = global::EPower.Properties.Resources.main_topright;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.panel3.Location = new System.Drawing.Point(1255, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(7, 59);
            this.panel3.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::EPower.Properties.Resources.main_topleft;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(6, 59);
            this.panel2.TabIndex = 0;
            // 
            // cnmStrip
            // 
            this.cnmStrip.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.cnmStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnOPEN_CASH_DRAWER,
            this.btnCLOSE_CASH_DRAWER,
            this.toolStripSeparator1,
            this.btnCHANGE_PASSWORD,
            this.btnLOG_OUT,
            this.btnCHANGE_LANGUAGE,
            this.toolStripSeparator2,
            this.btnABOUT_EPOWER});
            this.cnmStrip.Name = "cnmStrip";
            this.cnmStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.cnmStrip.Size = new System.Drawing.Size(181, 182);
            this.cnmStrip.Opening += new System.ComponentModel.CancelEventHandler(this.cnmStrip_Opening);
            // 
            // btnOPEN_CASH_DRAWER
            // 
            this.btnOPEN_CASH_DRAWER.Name = "btnOPEN_CASH_DRAWER";
            this.btnOPEN_CASH_DRAWER.Size = new System.Drawing.Size(180, 24);
            this.btnOPEN_CASH_DRAWER.Text = "បើកកេះដាក់ប្រាក់";
            this.btnOPEN_CASH_DRAWER.Click += new System.EventHandler(this.mnuOpenCashDrawer_Click);
            // 
            // btnCLOSE_CASH_DRAWER
            // 
            this.btnCLOSE_CASH_DRAWER.Name = "btnCLOSE_CASH_DRAWER";
            this.btnCLOSE_CASH_DRAWER.Size = new System.Drawing.Size(180, 24);
            this.btnCLOSE_CASH_DRAWER.Text = "បិទកេះដាក់ប្រាក់";
            this.btnCLOSE_CASH_DRAWER.Click += new System.EventHandler(this.mnuCloseCashDrawer_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(177, 6);
            // 
            // btnCHANGE_PASSWORD
            // 
            this.btnCHANGE_PASSWORD.Name = "btnCHANGE_PASSWORD";
            this.btnCHANGE_PASSWORD.Size = new System.Drawing.Size(180, 24);
            this.btnCHANGE_PASSWORD.Text = "ប្តូរលេខសម្ងាត់";
            this.btnCHANGE_PASSWORD.Click += new System.EventHandler(this.mnuChangePassword_Click);
            // 
            // btnLOG_OUT
            // 
            this.btnLOG_OUT.Name = "btnLOG_OUT";
            this.btnLOG_OUT.Size = new System.Drawing.Size(180, 24);
            this.btnLOG_OUT.Text = "ចាកចេញ";
            this.btnLOG_OUT.Click += new System.EventHandler(this.mnuLogOut_Click);
            // 
            // btnCHANGE_LANGUAGE
            // 
            this.btnCHANGE_LANGUAGE.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnKHMER_LANGUAGE,
            this.btnENGLISH_LANGUAGE});
            this.btnCHANGE_LANGUAGE.Name = "btnCHANGE_LANGUAGE";
            this.btnCHANGE_LANGUAGE.Size = new System.Drawing.Size(180, 24);
            this.btnCHANGE_LANGUAGE.Text = "ប្តូរភាសារ";
            // 
            // btnKHMER_LANGUAGE
            // 
            this.btnKHMER_LANGUAGE.Name = "btnKHMER_LANGUAGE";
            this.btnKHMER_LANGUAGE.Size = new System.Drawing.Size(112, 24);
            this.btnKHMER_LANGUAGE.Text = "ខ្មែរ";
            this.btnKHMER_LANGUAGE.Click += new System.EventHandler(this.mnuLanguageKH_Click);
            // 
            // btnENGLISH_LANGUAGE
            // 
            this.btnENGLISH_LANGUAGE.Name = "btnENGLISH_LANGUAGE";
            this.btnENGLISH_LANGUAGE.Size = new System.Drawing.Size(112, 24);
            this.btnENGLISH_LANGUAGE.Text = "English";
            this.btnENGLISH_LANGUAGE.Click += new System.EventHandler(this.mnuLanguageEN_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(177, 6);
            // 
            // btnABOUT_EPOWER
            // 
            this.btnABOUT_EPOWER.Name = "btnABOUT_EPOWER";
            this.btnABOUT_EPOWER.Size = new System.Drawing.Size(180, 24);
            this.btnABOUT_EPOWER.Text = "អំពី E-Power";
            this.btnABOUT_EPOWER.Click += new System.EventHandler(this.mnuAbout_Click);
            // 
            // timerAds
            // 
            this.timerAds.Interval = 5000;
            // 
            // FormMain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1262, 540);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormMain";
            this.Text = "E Power [ The popular power management system]";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.panel5.ResumeLayout(false);
            this.ads.ResumeLayout(false);
            this.ads.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbAds)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.cnmStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        #endregion
        private Panel panel1;
        private Panel panel3;
        private Panel panel2;
        private Panel panel4;
        private Panel panel5;
        private Panel panel7;
        private Panel panel6;
        private Panel panel10;
        private Panel panel9;
        private Label lblCLOSE_;
        private FlowLayoutPanel flowLayoutPanel1;
        private Panel panel11;
        private Label lblTITLE_;
        private ExTabItem btnADMIN_TASKS;
        private ExTabItem btnReports;
        private Panel panel8;
        private Panel main;
        private ExTabItem btnCUSTOMER_BILLING;
        private ExTabItem btnPOSTPAID;
        private ExTabItem btnSYSTEM_SECURITY;
        private ExTabItem btnPREPAID;
        private Label lblMINIMIZE_;
        private Label lblGENERAL_TASK;
        private ContextMenuStrip cnmStrip;
        private ToolStripMenuItem btnOPEN_CASH_DRAWER;
        private ToolStripMenuItem btnCLOSE_CASH_DRAWER;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripMenuItem btnCHANGE_PASSWORD;
        private ToolStripMenuItem btnLOG_OUT;
        private ToolStripSeparator toolStripSeparator2;
        private ToolStripMenuItem btnABOUT_EPOWER;
        private ToolStripMenuItem btnCHANGE_LANGUAGE;
        private ToolStripMenuItem btnKHMER_LANGUAGE;
        private ToolStripMenuItem btnENGLISH_LANGUAGE;
        private ExTabItem btnPOWER_MANAGEMENT;
        private Panel ads;
        private Panel adsBottom;
        private Label lblCLOSE2;
        private Panel adsRight;
        private Panel adsTop;
        private Panel adsLeft;
        private Timer timerAds;
        private Label lblDEMO;
        private ExTabItem btnACCOUNTING;
        private PictureBox pbAds;
    }
}