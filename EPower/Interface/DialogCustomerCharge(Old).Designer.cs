﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogCustomerCharge
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCustomerCharge));
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.dgvInvoiceItem = new System.Windows.Forms.DataGridView();
            this.INVOICE_ITEM_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SERVICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UNIT_PRICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_SING_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnADD = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnREMOVE = new SoftTech.Component.ExLinkLabel(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDisplay = new System.Windows.Forms.TextBox();
            this.lblSHOW_ON_INVOICE = new System.Windows.Forms.Label();
            this.lblUNIT_PRICE = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.cboInvoiceItem = new System.Windows.Forms.ComboBox();
            this.lblSERVICE = new System.Windows.Forms.Label();
            this.chkPRINT = new System.Windows.Forms.CheckBox();
            this.chkIS_PAID = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.lblCURRENCY = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblINVOICE_DATE = new System.Windows.Forms.Label();
            this.dtpINVOICE_DATE = new System.Windows.Forms.DateTimePicker();
            this.label15 = new System.Windows.Forms.Label();
            this.lblTOTAL = new System.Windows.Forms.Label();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtTax = new System.Windows.Forms.TextBox();
            this.lblPAYMENT_METHOD = new System.Windows.Forms.Label();
            this.lblTAX = new System.Windows.Forms.Label();
            this.cboTax = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.lblGRAND_TOTAL = new System.Windows.Forms.Label();
            this.txtGrandTotal = new System.Windows.Forms.TextBox();
            this.cboPaymentMethod = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInvoiceItem)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboTax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboPaymentMethod.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.cboPaymentMethod);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.lblGRAND_TOTAL);
            this.content.Controls.Add(this.txtGrandTotal);
            this.content.Controls.Add(this.cboTax);
            this.content.Controls.Add(this.lblTAX);
            this.content.Controls.Add(this.lblPAYMENT_METHOD);
            this.content.Controls.Add(this.label12);
            this.content.Controls.Add(this.label14);
            this.content.Controls.Add(this.txtTax);
            this.content.Controls.Add(this.label15);
            this.content.Controls.Add(this.lblTOTAL);
            this.content.Controls.Add(this.txtTotal);
            this.content.Controls.Add(this.dtpINVOICE_DATE);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.chkIS_PAID);
            this.content.Controls.Add(this.chkPRINT);
            this.content.Controls.Add(this.cboCurrency);
            this.content.Controls.Add(this.lblCURRENCY);
            this.content.Controls.Add(this.lblINVOICE_DATE);
            this.content.Controls.Add(this.lblSHOW_ON_INVOICE);
            this.content.Controls.Add(this.lblUNIT_PRICE);
            this.content.Controls.Add(this.txtPrice);
            this.content.Controls.Add(this.cboInvoiceItem);
            this.content.Controls.Add(this.lblSERVICE);
            this.content.Controls.Add(this.groupBox2);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.dgvInvoiceItem);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.label2);
            this.content.Controls.Add(this.label3);
            this.content.Controls.Add(this.txtDisplay);
            this.content.Controls.Add(this.label5);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.label5, 0);
            this.content.Controls.SetChildIndex(this.txtDisplay, 0);
            this.content.Controls.SetChildIndex(this.label3, 0);
            this.content.Controls.SetChildIndex(this.label2, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.dgvInvoiceItem, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.groupBox2, 0);
            this.content.Controls.SetChildIndex(this.lblSERVICE, 0);
            this.content.Controls.SetChildIndex(this.cboInvoiceItem, 0);
            this.content.Controls.SetChildIndex(this.txtPrice, 0);
            this.content.Controls.SetChildIndex(this.lblUNIT_PRICE, 0);
            this.content.Controls.SetChildIndex(this.lblSHOW_ON_INVOICE, 0);
            this.content.Controls.SetChildIndex(this.lblINVOICE_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblCURRENCY, 0);
            this.content.Controls.SetChildIndex(this.cboCurrency, 0);
            this.content.Controls.SetChildIndex(this.chkPRINT, 0);
            this.content.Controls.SetChildIndex(this.chkIS_PAID, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.dtpINVOICE_DATE, 0);
            this.content.Controls.SetChildIndex(this.txtTotal, 0);
            this.content.Controls.SetChildIndex(this.lblTOTAL, 0);
            this.content.Controls.SetChildIndex(this.label15, 0);
            this.content.Controls.SetChildIndex(this.txtTax, 0);
            this.content.Controls.SetChildIndex(this.label14, 0);
            this.content.Controls.SetChildIndex(this.label12, 0);
            this.content.Controls.SetChildIndex(this.lblPAYMENT_METHOD, 0);
            this.content.Controls.SetChildIndex(this.lblTAX, 0);
            this.content.Controls.SetChildIndex(this.cboTax, 0);
            this.content.Controls.SetChildIndex(this.txtGrandTotal, 0);
            this.content.Controls.SetChildIndex(this.lblGRAND_TOTAL, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.cboPaymentMethod, 0);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // dgvInvoiceItem
            // 
            this.dgvInvoiceItem.AllowUserToAddRows = false;
            this.dgvInvoiceItem.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvInvoiceItem.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvInvoiceItem.BackgroundColor = System.Drawing.Color.White;
            this.dgvInvoiceItem.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvInvoiceItem.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvInvoiceItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInvoiceItem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.INVOICE_ITEM_ID,
            this.CURRENCY_ID,
            this.SERVICE,
            this.UNIT_PRICE,
            this.CURRENCY_SING_});
            resources.ApplyResources(this.dgvInvoiceItem, "dgvInvoiceItem");
            this.dgvInvoiceItem.Name = "dgvInvoiceItem";
            this.dgvInvoiceItem.ReadOnly = true;
            this.dgvInvoiceItem.RowHeadersVisible = false;
            this.dgvInvoiceItem.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvInvoiceItem_CellFormatting);
            this.dgvInvoiceItem.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.dgvInvoiceItem_RowStateChanged);
            // 
            // INVOICE_ITEM_ID
            // 
            resources.ApplyResources(this.INVOICE_ITEM_ID, "INVOICE_ITEM_ID");
            this.INVOICE_ITEM_ID.Name = "INVOICE_ITEM_ID";
            this.INVOICE_ITEM_ID.ReadOnly = true;
            // 
            // CURRENCY_ID
            // 
            resources.ApplyResources(this.CURRENCY_ID, "CURRENCY_ID");
            this.CURRENCY_ID.Name = "CURRENCY_ID";
            this.CURRENCY_ID.ReadOnly = true;
            // 
            // SERVICE
            // 
            this.SERVICE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            resources.ApplyResources(this.SERVICE, "SERVICE");
            this.SERVICE.Name = "SERVICE";
            this.SERVICE.ReadOnly = true;
            // 
            // UNIT_PRICE
            // 
            this.UNIT_PRICE.DataPropertyName = "PRICE";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.UNIT_PRICE.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.UNIT_PRICE, "UNIT_PRICE");
            this.UNIT_PRICE.Name = "UNIT_PRICE";
            this.UNIT_PRICE.ReadOnly = true;
            // 
            // CURRENCY_SING_
            // 
            this.CURRENCY_SING_.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.CURRENCY_SING_.DataPropertyName = "CURRENCY_SING";
            resources.ApplyResources(this.CURRENCY_SING_, "CURRENCY_SING_");
            this.CURRENCY_SING_.Name = "CURRENCY_SING_";
            this.CURRENCY_SING_.ReadOnly = true;
            // 
            // groupBox2
            // 
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // btnADD
            // 
            resources.ApplyResources(this.btnADD, "btnADD");
            this.btnADD.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnADD.ForeColor = System.Drawing.Color.Red;
            this.btnADD.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnADD.Name = "btnADD";
            this.btnADD.TabStop = true;
            this.btnADD.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnADD.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkAdd_LinkClicked);
            // 
            // btnREMOVE
            // 
            resources.ApplyResources(this.btnREMOVE, "btnREMOVE");
            this.btnREMOVE.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnREMOVE.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnREMOVE.Name = "btnREMOVE";
            this.btnREMOVE.TabStop = true;
            this.btnREMOVE.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnREMOVE.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkRemove_LinkClicked);
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Name = "label5";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Name = "label2";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Name = "label3";
            // 
            // txtDisplay
            // 
            resources.ApplyResources(this.txtDisplay, "txtDisplay");
            this.txtDisplay.Name = "txtDisplay";
            // 
            // lblSHOW_ON_INVOICE
            // 
            resources.ApplyResources(this.lblSHOW_ON_INVOICE, "lblSHOW_ON_INVOICE");
            this.lblSHOW_ON_INVOICE.Name = "lblSHOW_ON_INVOICE";
            // 
            // lblUNIT_PRICE
            // 
            resources.ApplyResources(this.lblUNIT_PRICE, "lblUNIT_PRICE");
            this.lblUNIT_PRICE.Name = "lblUNIT_PRICE";
            // 
            // txtPrice
            // 
            resources.ApplyResources(this.txtPrice, "txtPrice");
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.ReadOnly = true;
            this.txtPrice.Enter += new System.EventHandler(this.txtPrice_Enter);
            this.txtPrice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPrice_KeyDown);
            // 
            // cboInvoiceItem
            // 
            this.cboInvoiceItem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboInvoiceItem, "cboInvoiceItem");
            this.cboInvoiceItem.Name = "cboInvoiceItem";
            this.cboInvoiceItem.SelectedIndexChanged += new System.EventHandler(this.cboInvoiceItem_SelectedIndexChanged);
            this.cboInvoiceItem.Enter += new System.EventHandler(this.cboInvoiceItem_Enter);
            // 
            // lblSERVICE
            // 
            resources.ApplyResources(this.lblSERVICE, "lblSERVICE");
            this.lblSERVICE.Name = "lblSERVICE";
            // 
            // chkPRINT
            // 
            resources.ApplyResources(this.chkPRINT, "chkPRINT");
            this.chkPRINT.Name = "chkPRINT";
            this.chkPRINT.UseVisualStyleBackColor = true;
            this.chkPRINT.CheckedChanged += new System.EventHandler(this.chkPrintInvoice_CheckedChanged);
            // 
            // chkIS_PAID
            // 
            resources.ApplyResources(this.chkIS_PAID, "chkIS_PAID");
            this.chkIS_PAID.Name = "chkIS_PAID";
            this.chkIS_PAID.UseVisualStyleBackColor = true;
            this.chkIS_PAID.CheckedChanged += new System.EventHandler(this.chkPayment_CheckedChanged);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.SelectedIndexChanged += new System.EventHandler(this.cboCurrency_SelectedIndexChanged);
            // 
            // lblCURRENCY
            // 
            resources.ApplyResources(this.lblCURRENCY, "lblCURRENCY");
            this.lblCURRENCY.Name = "lblCURRENCY";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnADD);
            this.panel1.Controls.Add(this.btnREMOVE);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // lblINVOICE_DATE
            // 
            resources.ApplyResources(this.lblINVOICE_DATE, "lblINVOICE_DATE");
            this.lblINVOICE_DATE.Name = "lblINVOICE_DATE";
            // 
            // dtpINVOICE_DATE
            // 
            resources.ApplyResources(this.dtpINVOICE_DATE, "dtpINVOICE_DATE");
            this.dtpINVOICE_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpINVOICE_DATE.Name = "dtpINVOICE_DATE";
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.Name = "label15";
            // 
            // lblTOTAL
            // 
            resources.ApplyResources(this.lblTOTAL, "lblTOTAL");
            this.lblTOTAL.Name = "lblTOTAL";
            // 
            // txtTotal
            // 
            resources.ApplyResources(this.txtTotal, "txtTotal");
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.ReadOnly = true;
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.Name = "label12";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.Name = "label14";
            // 
            // txtTax
            // 
            resources.ApplyResources(this.txtTax, "txtTax");
            this.txtTax.Name = "txtTax";
            this.txtTax.ReadOnly = true;
            // 
            // lblPAYMENT_METHOD
            // 
            resources.ApplyResources(this.lblPAYMENT_METHOD, "lblPAYMENT_METHOD");
            this.lblPAYMENT_METHOD.Name = "lblPAYMENT_METHOD";
            // 
            // lblTAX
            // 
            resources.ApplyResources(this.lblTAX, "lblTAX");
            this.lblTAX.Name = "lblTAX";
            // 
            // cboTax
            // 
            resources.ApplyResources(this.cboTax, "cboTax");
            this.cboTax.Name = "cboTax";
            this.cboTax.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("cboTax.Properties.Appearance.Font")));
            this.cboTax.Properties.Appearance.Options.UseFont = true;
            this.cboTax.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cboTax.Properties.Buttons"))))});
            this.cboTax.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("cboTax.Properties.Columns"), resources.GetString("cboTax.Properties.Columns1"), ((int)(resources.GetObject("cboTax.Properties.Columns2"))), ((DevExpress.Utils.FormatType)(resources.GetObject("cboTax.Properties.Columns3"))), resources.GetString("cboTax.Properties.Columns4"), ((bool)(resources.GetObject("cboTax.Properties.Columns5"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("cboTax.Properties.Columns6"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("cboTax.Properties.Columns7"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("cboTax.Properties.Columns8"))))});
            this.cboTax.Properties.DropDownRows = 5;
            this.cboTax.Properties.NullText = resources.GetString("cboTax.Properties.NullText");
            this.cboTax.Properties.PopupWidth = 300;
            this.cboTax.Properties.PopupWidthMode = DevExpress.XtraEditors.PopupWidthMode.ContentWidth;
            this.cboTax.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.cboTax.Required = false;
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // lblGRAND_TOTAL
            // 
            resources.ApplyResources(this.lblGRAND_TOTAL, "lblGRAND_TOTAL");
            this.lblGRAND_TOTAL.Name = "lblGRAND_TOTAL";
            // 
            // txtGrandTotal
            // 
            resources.ApplyResources(this.txtGrandTotal, "txtGrandTotal");
            this.txtGrandTotal.Name = "txtGrandTotal";
            this.txtGrandTotal.ReadOnly = true;
            // 
            // cboPaymentMethod
            // 
            resources.ApplyResources(this.cboPaymentMethod, "cboPaymentMethod");
            this.cboPaymentMethod.Name = "cboPaymentMethod";
            this.cboPaymentMethod.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("cboPaymentMethod.Properties.Appearance.Font")));
            this.cboPaymentMethod.Properties.Appearance.Options.UseFont = true;
            this.cboPaymentMethod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cboPaymentMethod.Properties.Buttons"))))});
            this.cboPaymentMethod.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("cboPaymentMethod.Properties.Columns"), resources.GetString("cboPaymentMethod.Properties.Columns1"), ((int)(resources.GetObject("cboPaymentMethod.Properties.Columns2"))), ((DevExpress.Utils.FormatType)(resources.GetObject("cboPaymentMethod.Properties.Columns3"))), resources.GetString("cboPaymentMethod.Properties.Columns4"), ((bool)(resources.GetObject("cboPaymentMethod.Properties.Columns5"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("cboPaymentMethod.Properties.Columns6"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("cboPaymentMethod.Properties.Columns7"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("cboPaymentMethod.Properties.Columns8"))))});
            this.cboPaymentMethod.Properties.DisplayMember = "PAYMENT_METHOD_NAME";
            this.cboPaymentMethod.Properties.DropDownRows = 5;
            this.cboPaymentMethod.Properties.NullText = resources.GetString("cboPaymentMethod.Properties.NullText");
            this.cboPaymentMethod.Properties.PopupWidth = 300;
            this.cboPaymentMethod.Properties.PopupWidthMode = DevExpress.XtraEditors.PopupWidthMode.ContentWidth;
            this.cboPaymentMethod.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.cboPaymentMethod.Properties.ValueMember = "PAYMENT_METHOD_ID";
            this.cboPaymentMethod.Required = false;
            // 
            // DialogCustomerCharge
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogCustomerCharge";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInvoiceItem)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboTax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboPaymentMethod.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnCLOSE;
        private ExButton btnOK;
        private DataGridView dgvInvoiceItem;
        private GroupBox groupBox2;
        private ExLinkLabel btnADD;
        private Label label5;
        private Label label2;
        private Label label3;
        private TextBox txtDisplay;
        private Label lblSHOW_ON_INVOICE;
        private Label lblUNIT_PRICE;
        private TextBox txtPrice;
        private Label lblSERVICE;
        private CheckBox chkPRINT;
        private CheckBox chkIS_PAID;
        private Label label9;
        private ComboBox cboCurrency;
        private Label lblCURRENCY;
        private Panel panel1;
        private Label lblINVOICE_DATE;
        private DateTimePicker dtpINVOICE_DATE;
        public ComboBox cboInvoiceItem;
        public ExLinkLabel btnREMOVE;
        private Label label15;
        private Label lblTOTAL;
        private TextBox txtTotal;
        private Label label12;
        private Label label14;
        private TextBox txtTax;
        private DataGridViewTextBoxColumn INVOICE_ITEM_ID;
        private DataGridViewTextBoxColumn CURRENCY_ID;
        private DataGridViewTextBoxColumn SERVICE;
        private DataGridViewTextBoxColumn UNIT_PRICE;
        private DataGridViewTextBoxColumn CURRENCY_SING_;
        private Label lblPAYMENT_METHOD;
        private Label lblTAX;
        public HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit cboTax;
        private Label label1;
        private Label lblGRAND_TOTAL;
        private TextBox txtGrandTotal;
        public HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit cboPaymentMethod;
    }
}