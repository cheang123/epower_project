﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogBillingCycle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogBillingCycle));
            this.lblCYCLE_NAME = new System.Windows.Forms.Label();
            this.txtCycleName = new System.Windows.Forms.TextBox();
            this.lblCollectDate = new System.Windows.Forms.Label();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.numCollectDate = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.numEndDate = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.radIsEndOfMoth = new System.Windows.Forms.RadioButton();
            this.radSpecificDay = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCollectDate)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numEndDate)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.txtCycleName);
            this.content.Controls.Add(this.lblCYCLE_NAME);
            this.content.Controls.Add(this.groupBox1);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.groupBox1, 0);
            this.content.Controls.SetChildIndex(this.lblCYCLE_NAME, 0);
            this.content.Controls.SetChildIndex(this.txtCycleName, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            // 
            // lblCYCLE_NAME
            // 
            resources.ApplyResources(this.lblCYCLE_NAME, "lblCYCLE_NAME");
            this.lblCYCLE_NAME.Name = "lblCYCLE_NAME";
            // 
            // txtCycleName
            // 
            resources.ApplyResources(this.txtCycleName, "txtCycleName");
            this.txtCycleName.Name = "txtCycleName";
            this.txtCycleName.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // lblCollectDate
            // 
            resources.ApplyResources(this.lblCollectDate, "lblCollectDate");
            this.lblCollectDate.Name = "lblCollectDate";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // numCollectDate
            // 
            this.numCollectDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.numCollectDate, "numCollectDate");
            this.numCollectDate.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numCollectDate.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numCollectDate.Name = "numCollectDate";
            this.numCollectDate.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numCollectDate.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.numEndDate);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.radIsEndOfMoth);
            this.groupBox1.Controls.Add(this.radSpecificDay);
            this.groupBox1.Controls.Add(this.numCollectDate);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.lblCollectDate);
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Name = "label5";
            // 
            // numEndDate
            // 
            this.numEndDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.numEndDate, "numEndDate");
            this.numEndDate.Maximum = new decimal(new int[] {
            28,
            0,
            0,
            0});
            this.numEndDate.Name = "numEndDate";
            this.numEndDate.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numEndDate.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Name = "label3";
            // 
            // radIsEndOfMoth
            // 
            resources.ApplyResources(this.radIsEndOfMoth, "radIsEndOfMoth");
            this.radIsEndOfMoth.Checked = true;
            this.radIsEndOfMoth.Name = "radIsEndOfMoth";
            this.radIsEndOfMoth.TabStop = true;
            this.radIsEndOfMoth.UseVisualStyleBackColor = true;
            // 
            // radSpecificDay
            // 
            resources.ApplyResources(this.radSpecificDay, "radSpecificDay");
            this.radSpecificDay.Name = "radSpecificDay";
            this.radSpecificDay.UseVisualStyleBackColor = true;
            this.radSpecificDay.CheckedChanged += new System.EventHandler(this.radSpecificDay_CheckedChanged);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // DialogBillingCycle
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogBillingCycle";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCollectDate)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numEndDate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Label lblCollectDate;
        private TextBox txtCycleName;
        private Label lblCYCLE_NAME;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Panel panel1;
        private NumericUpDown numCollectDate;
        private GroupBox groupBox1;
        private NumericUpDown numEndDate;
        private RadioButton radSpecificDay;
        private RadioButton radIsEndOfMoth;
        private Label label3;
        private Label label9;
        private Label label4;
        private Label label5;
        private Label label1;
        private ExButton btnCHANGE_LOG;
    }
}