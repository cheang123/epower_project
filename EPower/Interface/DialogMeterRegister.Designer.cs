﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogMeterRegister
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnOK = new SoftTech.Component.ExButton();
            this.txtFileName = new System.Windows.Forms.TextBox();
            this.lblFileName = new System.Windows.Forms.Label();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.lblError = new System.Windows.Forms.Label();
            this.lblSuccess = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtNew = new System.Windows.Forms.Label();
            this.txtOld = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cboMeterType = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtError = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btnShowDetail = new SoftTech.Component.ExLinkLabel(this.components);
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.btnShowDetail);
            this.content.Controls.Add(this.txtError);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.label10);
            this.content.Controls.Add(this.label5);
            this.content.Controls.Add(this.label4);
            this.content.Controls.Add(this.cboMeterType);
            this.content.Controls.Add(this.label3);
            this.content.Controls.Add(this.label2);
            this.content.Controls.Add(this.txtOld);
            this.content.Controls.Add(this.txtNew);
            this.content.Controls.Add(this.label7);
            this.content.Controls.Add(this.label6);
            this.content.Controls.Add(this.lblSuccess);
            this.content.Controls.Add(this.lblError);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.panel2);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnBrowse);
            this.content.Controls.Add(this.lblFileName);
            this.content.Controls.Add(this.txtFileName);
            this.content.Controls.Add(this.btnOK);
            this.content.Size = new System.Drawing.Size(511, 313);
            this.content.Text = "\'CONTENT\'";
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.txtFileName, 0);
            this.content.Controls.SetChildIndex(this.lblFileName, 0);
            this.content.Controls.SetChildIndex(this.btnBrowse, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel2, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.lblError, 0);
            this.content.Controls.SetChildIndex(this.lblSuccess, 0);
            this.content.Controls.SetChildIndex(this.label6, 0);
            this.content.Controls.SetChildIndex(this.label7, 0);
            this.content.Controls.SetChildIndex(this.txtNew, 0);
            this.content.Controls.SetChildIndex(this.txtOld, 0);
            this.content.Controls.SetChildIndex(this.label2, 0);
            this.content.Controls.SetChildIndex(this.label3, 0);
            this.content.Controls.SetChildIndex(this.cboMeterType, 0);
            this.content.Controls.SetChildIndex(this.label4, 0);
            this.content.Controls.SetChildIndex(this.label5, 0);
            this.content.Controls.SetChildIndex(this.label10, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.txtError, 0);
            this.content.Controls.SetChildIndex(this.btnShowDetail, 0);
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnOK.Location = new System.Drawing.Point(349, 282);
            this.btnOK.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 3;
            this.btnOK.Text = "យល់ព្រម";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // txtFileName
            // 
            this.txtFileName.BackColor = System.Drawing.Color.LightYellow;
            this.txtFileName.Location = new System.Drawing.Point(130, 52);
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.ReadOnly = true;
            this.txtFileName.Size = new System.Drawing.Size(299, 27);
            this.txtFileName.TabIndex = 0;
            // 
            // lblFileName
            // 
            this.lblFileName.AutoSize = true;
            this.lblFileName.Location = new System.Drawing.Point(29, 52);
            this.lblFileName.Name = "lblFileName";
            this.lblFileName.Size = new System.Drawing.Size(74, 19);
            this.lblFileName.TabIndex = 8;
            this.lblFileName.Text = "ទីតាំងឯកសារ";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(432, 52);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(50, 27);
            this.btnBrowse.TabIndex = 1;
            this.btnBrowse.Text = "រើស";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // btnCLOSE
            // 
            this.btnCLOSE.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnCLOSE.Location = new System.Drawing.Point(430, 282);
            this.btnCLOSE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.Size = new System.Drawing.Size(75, 23);
            this.btnCLOSE.TabIndex = 4;
            this.btnCLOSE.Text = "បិទ";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Silver;
            this.panel2.Location = new System.Drawing.Point(2, 276);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(507, 1);
            this.panel2.TabIndex = 22;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(29, 128);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 19);
            this.label1.TabIndex = 23;
            this.label1.Text = "លទ្ធផល";
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.Location = new System.Drawing.Point(29, 181);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(113, 19);
            this.lblError.TabIndex = 24;
            this.lblError.Text = "កុងទ័រដែលធ្លាប់មានរួច";
            // 
            // lblSuccess
            // 
            this.lblSuccess.AutoSize = true;
            this.lblSuccess.Location = new System.Drawing.Point(29, 153);
            this.lblSuccess.Name = "lblSuccess";
            this.lblSuccess.Size = new System.Drawing.Size(123, 19);
            this.lblSuccess.TabIndex = 25;
            this.lblSuccess.Text = "កុងទ័រថ្មីដែលបានចុះបញ្ជី";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(228, 153);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(15, 19);
            this.label6.TabIndex = 28;
            this.label6.Text = "៖";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(228, 181);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(15, 19);
            this.label7.TabIndex = 29;
            this.label7.Text = "៖";
            // 
            // txtNew
            // 
            this.txtNew.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txtNew.Location = new System.Drawing.Point(258, 153);
            this.txtNew.Name = "txtNew";
            this.txtNew.Size = new System.Drawing.Size(171, 21);
            this.txtNew.TabIndex = 39;
            this.txtNew.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtOld
            // 
            this.txtOld.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txtOld.Location = new System.Drawing.Point(257, 180);
            this.txtOld.Name = "txtOld";
            this.txtOld.Size = new System.Drawing.Size(171, 21);
            this.txtOld.TabIndex = 39;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 19);
            this.label2.TabIndex = 40;
            this.label2.Text = "ប្រភេទកុងទ័រ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(109, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 19);
            this.label3.TabIndex = 41;
            this.label3.Text = "៖";
            // 
            // cboMeterType
            // 
            this.cboMeterType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMeterType.FormattingEnabled = true;
            this.cboMeterType.Location = new System.Drawing.Point(130, 85);
            this.cboMeterType.Name = "cboMeterType";
            this.cboMeterType.Size = new System.Drawing.Size(299, 27);
            this.cboMeterType.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(109, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(15, 19);
            this.label4.TabIndex = 43;
            this.label4.Text = "៖";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(29, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(125, 19);
            this.label5.TabIndex = 44;
            this.label5.Text = "កុងទ័រដែលត្រូវចុះបញ្ជី";
            // 
            // txtError
            // 
            this.txtError.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txtError.Location = new System.Drawing.Point(258, 207);
            this.txtError.Name = "txtError";
            this.txtError.Size = new System.Drawing.Size(171, 19);
            this.txtError.TabIndex = 47;
            this.txtError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(228, 207);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(15, 19);
            this.label9.TabIndex = 46;
            this.label9.Text = "៖";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(29, 207);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(106, 19);
            this.label10.TabIndex = 45;
            this.label10.Text = "កុងទ័រដែលមានបញ្ហា";
            // 
            // btnShowDetail
            // 
            this.btnShowDetail.AutoSize = true;
            this.btnShowDetail.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnShowDetail.Location = new System.Drawing.Point(255, 236);
            this.btnShowDetail.Name = "btnShowDetail";
            this.btnShowDetail.Size = new System.Drawing.Size(77, 19);
            this.btnShowDetail.TabIndex = 48;
            this.btnShowDetail.TabStop = true;
            this.btnShowDetail.Text = "លំអិតលទ្ធផល";
            this.btnShowDetail.Visible = false;
            // 
            // DialogMeterRegister
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(511, 336);
            this.Name = "DialogMeterRegister";
            this.Text = "ចុះបញ្ជីកុងទ័រ";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnOK;
        private Label lblFileName;
        private TextBox txtFileName;
        private ExButton btnCLOSE;
        private Button btnBrowse;
        private Panel panel2;
        private Label lblError;
        private Label label1;
        private Label lblSuccess;
        private Label label7;
        private Label label6; 
        private Label txtNew;
        private Label txtOld;
        private ComboBox cboMeterType;
        private Label label3;
        private Label label2;
        private Label label4;
        private Label label5;
        private Label txtError;
        private Label label9;
        private Label label10;
        private ExLinkLabel btnShowDetail;
    }
}