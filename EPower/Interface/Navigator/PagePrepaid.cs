﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using EPower.Base.Logic;
using EPower.Interface.Report;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface.PrePaid
{
    public partial class PagePrePaid : UserControl
    { 
        public PagePrePaid()
        {
            InitializeComponent();

            btnPURCHASE_POWER.Enabled = SoftTech.Security.Logic.Login.IsAuthorized(Permission.PREPAID_DIALYOPERATION_SALEPOWER);
            btnVOID_CARD.Enabled = SoftTech.Security.Logic.Login.IsAuthorized(Permission.PREPAID_DIALYOPERATION_VOIDPOWER);
            btnRENEW_POWER_CARC.Enabled = SoftTech.Security.Logic.Login.IsAuthorized(Permission.PREPAID_DIALYOPERATION_RENEWCARD);
            btnCREATE_SPECIAL_CARD.Enabled = SoftTech.Security.Logic.Login.IsAuthorized(Permission.PREPAID_DIALYOPERATION_CREATESPECIALCARD);
            btnREAD_DATA_FROM_METER.Enabled  = SoftTech.Security.Logic.Login.IsAuthorized(Permission.PREPAID_DIALYOPERATION_READDATA);
            
            btnNEW_CUSTOMER.Enabled = SoftTech.Security.Logic.Login.IsAuthorized(Permission.PREPAID_REGISTER_INSERT); 
            btnACTIVATE_CUSTOMER.Enabled = SoftTech.Security.Logic.Login.IsAuthorized(Permission.PREPAID_REGISTER_ACTIVATE);
            btnMANAGE_CUSTOMER.Enabled = SoftTech.Security.Logic.Login.IsAuthorized(Permission.PREPAID_REGISTER);
        }

        #region Show Page
        private Control currentPage = null;
        private Dictionary<Type, Control> pages = new Dictionary<Type, Control>();
        private void showPage(Type type, object sender)
        {
            if (!this.pages.ContainsKey(type))
            {
                this.pages[type] = (Control)Activator.CreateInstance(type);
                this.pages[type].Size = this.main.Size;
                if (this.pages[type] is Form)
                {
                    Form frm = (Form)this.pages[type];
                    frm.TopLevel = false;
                    frm.Visible = true;
                    frm.FormBorderStyle = FormBorderStyle.None;
                }
                this.main.Controls.Add(this.pages[type]);
                this.pages[type].Dock = DockStyle.Fill;

                ResourceHelper.ApplyResource(this.pages[type]);
            }
            if (this.currentPage != this.pages[type])
            {
                this.pages[type].Show();
                if (this.currentPage != null)
                {
                    this.currentPage.Hide();
                }
                this.currentPage = this.pages[type];
            }
            this.btnPREPAID.Text = ((Control)sender).Text;
        }
        #endregion ShowPage()

        private void btnNewCustomer_Click(object sender, EventArgs e)
        {
            new DialogCustomer(GeneralProcess.Insert, new TBL_CUSTOMER()).ShowDialog();
        }

        private void btnPurchasePower_Click(object sender, EventArgs e)
        {
            if (Login.CurrentCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_SELL_POWER, string.Empty) == DialogResult.Yes)
                    new DialogOpenCashDrawer().ShowDialog();
                if (Login.CurrentCashDrawer == null)
                    return;
            }
            new DialogPurchasePower().ShowDialog(); 
        }

        private void btnRenewCard_Click(object sender, EventArgs e)
        {
            if (Login.CurrentCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_REMAKE_CARD, string.Empty) == DialogResult.Yes)
                    new DialogOpenCashDrawer().ShowDialog();
                if (Login.CurrentCashDrawer == null)
                    return;
            }
            new DialogRenewCard().ShowDialog();
        }

        private void btnCustomer_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageCustomerPrepaid), sender);
        }

        private void btnCreateCard_Click(object sender, EventArgs e)
        {
            new DialogCreateCard().ShowDialog();
        }

        private void showCustomerActivateList()
        {
            this.showPage(typeof(PageCustomerActivateList), btnACTIVATE_CUSTOMER);
            ((PageCustomerActivateList)this.currentPage).BindData();
        }

        private void btnCustomerActivate_Click(object sender, EventArgs e)
        {
            showCustomerActivateList();
        }

        private void btnVoidCard_Click(object sender, EventArgs e)
        {
            if (Login.CurrentCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_VOID_CARD, string.Empty) == DialogResult.Yes)
                    new DialogOpenCashDrawer().ShowDialog();
                if (Login.CurrentCashDrawer == null)
                    return;
            }
            DialogVoidBuyPower dig = new DialogVoidBuyPower();
            dig.ShowDialog();
        }
 
        private void btnReportDailySaleDetail_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportDailySale), sender);
        } 
        private void btnReportPrepaidTotalSale_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportTotalSaleSummary), sender);
        }
        private void btnReportPrepaidMonthToDateSaleSummary_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportMonthToDateSaleSummary), sender);
        } 
        private void btnReportPrepaidBuyingFrequency_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportBuyingFrequency), sender);
        } 
        private void btnNotBuyPower_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportNotBuyPower), sender);
        }

        private void btnReadDataFromMeter_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReadDataFromMeter),sender);
        }

        private void btnReportVoidPower_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportVoidPower), sender);
        }

        private void btnPrepaidRunCredit_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageRunPrepaidCredit), sender);
            ((PageRunPrepaidCredit)this.currentPage).bind(); 
        }

        private void exMenuItem1_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageRunPrepaidCreditHistory), sender);
            ((PageRunPrepaidCreditHistory)this.currentPage).bind(); 

        }

        private void btnREPORT_PREPAID_PAYMENT_HISTORY_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportPrepaidPaymentHistory), sender);
        }
    }
}
