﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageAccounting
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageAccounting));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.lblTRANSACTION = new System.Windows.Forms.Label();
            this.btnINCOME = new SoftTech.Component.ExMenuItem();
            this.btnEXPENSE = new SoftTech.Component.ExMenuItem();
            this.btnSTAFF_EXPENSE = new SoftTech.Component.ExMenuItem();
            this.btnLOAN = new SoftTech.Component.ExMenuItem();
            this.btnEXCHANGE_RATE = new SoftTech.Component.ExMenuItem();
            this.lblSETUP_ACCOUNT_CHART = new System.Windows.Forms.Label();
            this.btnACCOUNT_CHART = new SoftTech.Component.ExMenuItem();
            this.btnACCOUNT_SETTING = new SoftTech.Component.ExMenuItem();
            this.lblFIX_ASSET = new System.Windows.Forms.Label();
            this.btnFIX_ASSET_TYPE = new SoftTech.Component.ExMenuItem();
            this.btnFIX_ASSET_ITEM = new SoftTech.Component.ExMenuItem();
            this.lblREPORT = new System.Windows.Forms.Label();
            this.btnREPORT_INCOME = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_EXPENSE = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_PROFIT_LOSS = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_FIX_ASSET = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_FIX_ASSET_DEPRECIATION = new SoftTech.Component.ExMenuItem();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.lblACCOUNTING = new SoftTech.Component.ExTabItem();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.main = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.lblACCOUNTING_1 = new SoftTech.Component.ExTabItem();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel20.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            resources.ApplyResources(this.splitContainer1, "splitContainer1");
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel12);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel16);
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.panel2);
            this.panel12.Controls.Add(this.lblACCOUNTING);
            resources.ApplyResources(this.panel12, "panel12");
            this.panel12.Name = "panel12";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.panel15);
            this.panel2.Controls.Add(this.panel13);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightGray;
            this.panel1.Controls.Add(this.lblTRANSACTION);
            this.panel1.Controls.Add(this.btnINCOME);
            this.panel1.Controls.Add(this.btnEXPENSE);
            this.panel1.Controls.Add(this.btnSTAFF_EXPENSE);
            this.panel1.Controls.Add(this.btnLOAN);
            this.panel1.Controls.Add(this.btnEXCHANGE_RATE);
            this.panel1.Controls.Add(this.lblSETUP_ACCOUNT_CHART);
            this.panel1.Controls.Add(this.btnACCOUNT_CHART);
            this.panel1.Controls.Add(this.btnACCOUNT_SETTING);
            this.panel1.Controls.Add(this.lblFIX_ASSET);
            this.panel1.Controls.Add(this.btnFIX_ASSET_TYPE);
            this.panel1.Controls.Add(this.btnFIX_ASSET_ITEM);
            this.panel1.Controls.Add(this.lblREPORT);
            this.panel1.Controls.Add(this.btnREPORT_INCOME);
            this.panel1.Controls.Add(this.btnREPORT_EXPENSE);
            this.panel1.Controls.Add(this.btnREPORT_PROFIT_LOSS);
            this.panel1.Controls.Add(this.btnREPORT_FIX_ASSET);
            this.panel1.Controls.Add(this.btnREPORT_FIX_ASSET_DEPRECIATION);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // lblTRANSACTION
            // 
            resources.ApplyResources(this.lblTRANSACTION, "lblTRANSACTION");
            this.lblTRANSACTION.BackColor = System.Drawing.Color.Silver;
            this.lblTRANSACTION.Name = "lblTRANSACTION";
            // 
            // btnINCOME
            // 
            resources.ApplyResources(this.btnINCOME, "btnINCOME");
            this.btnINCOME.BackColor = System.Drawing.Color.Transparent;
            this.btnINCOME.Image = ((System.Drawing.Image)(resources.GetObject("btnINCOME.Image")));
            this.btnINCOME.Name = "btnINCOME";
            this.btnINCOME.Selected = false;
            this.btnINCOME.Click += new System.EventHandler(this.btnTranIncome_Click);
            // 
            // btnEXPENSE
            // 
            resources.ApplyResources(this.btnEXPENSE, "btnEXPENSE");
            this.btnEXPENSE.BackColor = System.Drawing.Color.Transparent;
            this.btnEXPENSE.Image = ((System.Drawing.Image)(resources.GetObject("btnEXPENSE.Image")));
            this.btnEXPENSE.Name = "btnEXPENSE";
            this.btnEXPENSE.Selected = false;
            this.btnEXPENSE.Click += new System.EventHandler(this.btnTranExpense_Click);
            // 
            // btnSTAFF_EXPENSE
            // 
            resources.ApplyResources(this.btnSTAFF_EXPENSE, "btnSTAFF_EXPENSE");
            this.btnSTAFF_EXPENSE.BackColor = System.Drawing.Color.Transparent;
            this.btnSTAFF_EXPENSE.Image = ((System.Drawing.Image)(resources.GetObject("btnSTAFF_EXPENSE.Image")));
            this.btnSTAFF_EXPENSE.Name = "btnSTAFF_EXPENSE";
            this.btnSTAFF_EXPENSE.Selected = false;
            this.btnSTAFF_EXPENSE.Click += new System.EventHandler(this.btnStaffExpense_Click);
            // 
            // btnLOAN
            // 
            resources.ApplyResources(this.btnLOAN, "btnLOAN");
            this.btnLOAN.BackColor = System.Drawing.Color.Transparent;
            this.btnLOAN.Image = ((System.Drawing.Image)(resources.GetObject("btnLOAN.Image")));
            this.btnLOAN.Name = "btnLOAN";
            this.btnLOAN.Selected = false;
            this.btnLOAN.Click += new System.EventHandler(this.bntLoan_Click);
            // 
            // btnEXCHANGE_RATE
            // 
            resources.ApplyResources(this.btnEXCHANGE_RATE, "btnEXCHANGE_RATE");
            this.btnEXCHANGE_RATE.BackColor = System.Drawing.Color.Transparent;
            this.btnEXCHANGE_RATE.Image = ((System.Drawing.Image)(resources.GetObject("btnEXCHANGE_RATE.Image")));
            this.btnEXCHANGE_RATE.Name = "btnEXCHANGE_RATE";
            this.btnEXCHANGE_RATE.Selected = false;
            this.btnEXCHANGE_RATE.Click += new System.EventHandler(this.btnExchangeRate_Click);
            // 
            // lblSETUP_ACCOUNT_CHART
            // 
            resources.ApplyResources(this.lblSETUP_ACCOUNT_CHART, "lblSETUP_ACCOUNT_CHART");
            this.lblSETUP_ACCOUNT_CHART.BackColor = System.Drawing.Color.Silver;
            this.lblSETUP_ACCOUNT_CHART.Name = "lblSETUP_ACCOUNT_CHART";
            this.lblSETUP_ACCOUNT_CHART.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lblSETUP_ACCOUNT_CHART_MouseDoubleClick);
            // 
            // btnACCOUNT_CHART
            // 
            resources.ApplyResources(this.btnACCOUNT_CHART, "btnACCOUNT_CHART");
            this.btnACCOUNT_CHART.BackColor = System.Drawing.Color.Transparent;
            this.btnACCOUNT_CHART.Image = ((System.Drawing.Image)(resources.GetObject("btnACCOUNT_CHART.Image")));
            this.btnACCOUNT_CHART.Name = "btnACCOUNT_CHART";
            this.btnACCOUNT_CHART.Selected = false;
            this.btnACCOUNT_CHART.Click += new System.EventHandler(this.btnAccountItem_Click);
            // 
            // btnACCOUNT_SETTING
            // 
            resources.ApplyResources(this.btnACCOUNT_SETTING, "btnACCOUNT_SETTING");
            this.btnACCOUNT_SETTING.BackColor = System.Drawing.Color.Transparent;
            this.btnACCOUNT_SETTING.Image = ((System.Drawing.Image)(resources.GetObject("btnACCOUNT_SETTING.Image")));
            this.btnACCOUNT_SETTING.Name = "btnACCOUNT_SETTING";
            this.btnACCOUNT_SETTING.Selected = false;
            this.btnACCOUNT_SETTING.Click += new System.EventHandler(this.btnACCOUNT_SETTING_Click);
            // 
            // lblFIX_ASSET
            // 
            resources.ApplyResources(this.lblFIX_ASSET, "lblFIX_ASSET");
            this.lblFIX_ASSET.BackColor = System.Drawing.Color.Silver;
            this.lblFIX_ASSET.Name = "lblFIX_ASSET";
            // 
            // btnFIX_ASSET_TYPE
            // 
            resources.ApplyResources(this.btnFIX_ASSET_TYPE, "btnFIX_ASSET_TYPE");
            this.btnFIX_ASSET_TYPE.BackColor = System.Drawing.Color.Transparent;
            this.btnFIX_ASSET_TYPE.Image = ((System.Drawing.Image)(resources.GetObject("btnFIX_ASSET_TYPE.Image")));
            this.btnFIX_ASSET_TYPE.Name = "btnFIX_ASSET_TYPE";
            this.btnFIX_ASSET_TYPE.Selected = false;
            this.btnFIX_ASSET_TYPE.Click += new System.EventHandler(this.btnFixAssetCategory_Click);
            // 
            // btnFIX_ASSET_ITEM
            // 
            resources.ApplyResources(this.btnFIX_ASSET_ITEM, "btnFIX_ASSET_ITEM");
            this.btnFIX_ASSET_ITEM.BackColor = System.Drawing.Color.Transparent;
            this.btnFIX_ASSET_ITEM.Image = ((System.Drawing.Image)(resources.GetObject("btnFIX_ASSET_ITEM.Image")));
            this.btnFIX_ASSET_ITEM.Name = "btnFIX_ASSET_ITEM";
            this.btnFIX_ASSET_ITEM.Selected = false;
            this.btnFIX_ASSET_ITEM.Click += new System.EventHandler(this.btnFixAssetItem_Click);
            // 
            // lblREPORT
            // 
            resources.ApplyResources(this.lblREPORT, "lblREPORT");
            this.lblREPORT.BackColor = System.Drawing.Color.Silver;
            this.lblREPORT.Name = "lblREPORT";
            // 
            // btnREPORT_INCOME
            // 
            resources.ApplyResources(this.btnREPORT_INCOME, "btnREPORT_INCOME");
            this.btnREPORT_INCOME.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_INCOME.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_INCOME.Image")));
            this.btnREPORT_INCOME.Name = "btnREPORT_INCOME";
            this.btnREPORT_INCOME.Selected = false;
            this.btnREPORT_INCOME.Click += new System.EventHandler(this.btnReportIncome_Click);
            // 
            // btnREPORT_EXPENSE
            // 
            resources.ApplyResources(this.btnREPORT_EXPENSE, "btnREPORT_EXPENSE");
            this.btnREPORT_EXPENSE.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_EXPENSE.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_EXPENSE.Image")));
            this.btnREPORT_EXPENSE.Name = "btnREPORT_EXPENSE";
            this.btnREPORT_EXPENSE.Selected = false;
            this.btnREPORT_EXPENSE.Click += new System.EventHandler(this.btnReportExpense_Click);
            // 
            // btnREPORT_PROFIT_LOSS
            // 
            resources.ApplyResources(this.btnREPORT_PROFIT_LOSS, "btnREPORT_PROFIT_LOSS");
            this.btnREPORT_PROFIT_LOSS.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_PROFIT_LOSS.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_PROFIT_LOSS.Image")));
            this.btnREPORT_PROFIT_LOSS.Name = "btnREPORT_PROFIT_LOSS";
            this.btnREPORT_PROFIT_LOSS.Selected = false;
            this.btnREPORT_PROFIT_LOSS.Click += new System.EventHandler(this.btnReportProfitLoss_Click);
            // 
            // btnREPORT_FIX_ASSET
            // 
            resources.ApplyResources(this.btnREPORT_FIX_ASSET, "btnREPORT_FIX_ASSET");
            this.btnREPORT_FIX_ASSET.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_FIX_ASSET.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_FIX_ASSET.Image")));
            this.btnREPORT_FIX_ASSET.Name = "btnREPORT_FIX_ASSET";
            this.btnREPORT_FIX_ASSET.Selected = false;
            this.btnREPORT_FIX_ASSET.Click += new System.EventHandler(this.btnReportFixAsset_Click);
            // 
            // btnREPORT_FIX_ASSET_DEPRECIATION
            // 
            resources.ApplyResources(this.btnREPORT_FIX_ASSET_DEPRECIATION, "btnREPORT_FIX_ASSET_DEPRECIATION");
            this.btnREPORT_FIX_ASSET_DEPRECIATION.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_FIX_ASSET_DEPRECIATION.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_FIX_ASSET_DEPRECIATION.Image")));
            this.btnREPORT_FIX_ASSET_DEPRECIATION.Name = "btnREPORT_FIX_ASSET_DEPRECIATION";
            this.btnREPORT_FIX_ASSET_DEPRECIATION.Selected = false;
            this.btnREPORT_FIX_ASSET_DEPRECIATION.Click += new System.EventHandler(this.btnReportFixAssetDepreciation_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel15, "panel15");
            this.panel15.Name = "panel15";
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel13, "panel13");
            this.panel13.Name = "panel13";
            // 
            // lblACCOUNTING
            // 
            this.lblACCOUNTING.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblACCOUNTING, "lblACCOUNTING");
            this.lblACCOUNTING.Image = global::EPower.Properties.Resources.Money;
            this.lblACCOUNTING.IsTitle = true;
            this.lblACCOUNTING.Name = "lblACCOUNTING";
            this.lblACCOUNTING.Selected = false;
            this.lblACCOUNTING.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.Transparent;
            this.panel16.Controls.Add(this.panel20);
            this.panel16.Controls.Add(this.lblACCOUNTING_1);
            resources.ApplyResources(this.panel16, "panel16");
            this.panel16.Name = "panel16";
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.main);
            this.panel20.Controls.Add(this.panel17);
            this.panel20.Controls.Add(this.panel18);
            this.panel20.Controls.Add(this.panel19);
            resources.ApplyResources(this.panel20, "panel20");
            this.panel20.Name = "panel20";
            // 
            // main
            // 
            this.main.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.main, "main");
            this.main.Name = "main";
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel17, "panel17");
            this.panel17.Name = "panel17";
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel18, "panel18");
            this.panel18.Name = "panel18";
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel19, "panel19");
            this.panel19.Name = "panel19";
            // 
            // lblACCOUNTING_1
            // 
            this.lblACCOUNTING_1.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblACCOUNTING_1, "lblACCOUNTING_1");
            this.lblACCOUNTING_1.Image = null;
            this.lblACCOUNTING_1.IsTitle = true;
            this.lblACCOUNTING_1.Name = "lblACCOUNTING_1";
            this.lblACCOUNTING_1.Selected = false;
            this.lblACCOUNTING_1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PageAccounting
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "PageAccounting";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel16;
        private Panel panel20;
        private ExTabItem lblACCOUNTING_1;
        private Panel panel12;
        private ExTabItem lblACCOUNTING;
        private SplitContainer splitContainer1;
        private Panel main;
        private Panel panel17;
        private Panel panel18;
        private Panel panel19;
        private Panel panel2;
        private Panel panel15;
        private Panel panel13;
        private Panel panel3;
        private FlowLayoutPanel panel1;
        private Label lblSETUP_ACCOUNT_CHART;
        private ExMenuItem btnINCOME;
        private ExMenuItem btnACCOUNT_CHART;
        private ExMenuItem btnEXPENSE;
        private Label lblTRANSACTION;
        private Label lblREPORT;
        private ExMenuItem btnREPORT_INCOME;
        private ExMenuItem btnREPORT_EXPENSE;
        private ExMenuItem btnREPORT_PROFIT_LOSS;
        private Label lblFIX_ASSET;
        private ExMenuItem btnFIX_ASSET_TYPE;
        private ExMenuItem btnFIX_ASSET_ITEM;
        private ExMenuItem btnREPORT_FIX_ASSET;
        private ExMenuItem btnEXCHANGE_RATE;
        private ExMenuItem btnACCOUNT_SETTING;
        private ExMenuItem btnREPORT_FIX_ASSET_DEPRECIATION;
        private ExMenuItem btnSTAFF_EXPENSE;
        private ExMenuItem btnLOAN;
    }
}
