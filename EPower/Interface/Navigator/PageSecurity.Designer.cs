﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageSecurity
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageSecurity));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.lblUSER_PERMISSION = new System.Windows.Forms.Label();
            this.btnROLE = new SoftTech.Component.ExMenuItem();
            this.btnLOGIN = new SoftTech.Component.ExMenuItem();
            this.btnPERMISSION = new SoftTech.Component.ExMenuItem();
            this.lblDATA = new System.Windows.Forms.Label();
            this.btnBACKUP = new SoftTech.Component.ExMenuItem();
            this.btnRESTORE = new SoftTech.Component.ExMenuItem();
            this.btnAUDIT_TRIAL = new SoftTech.Component.ExMenuItem();
            this.btnMAINTENANCE = new SoftTech.Component.ExMenuItem();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.lblSYSTEM_SECURITY = new SoftTech.Component.ExTabItem();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.main = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.lblSYSTEM_SECURITY_1 = new SoftTech.Component.ExTabItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel20.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            resources.ApplyResources(this.splitContainer1, "splitContainer1");
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel12);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel16);
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.panel2);
            this.panel12.Controls.Add(this.lblSYSTEM_SECURITY);
            resources.ApplyResources(this.panel12, "panel12");
            this.panel12.Name = "panel12";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.panel15);
            this.panel2.Controls.Add(this.panel13);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightGray;
            this.panel1.Controls.Add(this.lblUSER_PERMISSION);
            this.panel1.Controls.Add(this.btnROLE);
            this.panel1.Controls.Add(this.btnLOGIN);
            this.panel1.Controls.Add(this.btnPERMISSION);
            this.panel1.Controls.Add(this.lblDATA);
            this.panel1.Controls.Add(this.btnBACKUP);
            this.panel1.Controls.Add(this.btnRESTORE);
            this.panel1.Controls.Add(this.btnAUDIT_TRIAL);
            this.panel1.Controls.Add(this.btnMAINTENANCE);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // lblUSER_PERMISSION
            // 
            resources.ApplyResources(this.lblUSER_PERMISSION, "lblUSER_PERMISSION");
            this.lblUSER_PERMISSION.BackColor = System.Drawing.Color.Silver;
            this.lblUSER_PERMISSION.Name = "lblUSER_PERMISSION";
            this.lblUSER_PERMISSION.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lblUSER_PERMISSION_MouseDoubleClick);
            // 
            // btnROLE
            // 
            resources.ApplyResources(this.btnROLE, "btnROLE");
            this.btnROLE.BackColor = System.Drawing.Color.Transparent;
            this.btnROLE.Image = ((System.Drawing.Image)(resources.GetObject("btnROLE.Image")));
            this.btnROLE.Name = "btnROLE";
            this.btnROLE.Selected = false;
            this.btnROLE.Click += new System.EventHandler(this.btnRole_Click);
            // 
            // btnLOGIN
            // 
            resources.ApplyResources(this.btnLOGIN, "btnLOGIN");
            this.btnLOGIN.BackColor = System.Drawing.Color.Transparent;
            this.btnLOGIN.Image = ((System.Drawing.Image)(resources.GetObject("btnLOGIN.Image")));
            this.btnLOGIN.Name = "btnLOGIN";
            this.btnLOGIN.Selected = false;
            this.btnLOGIN.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // btnPERMISSION
            // 
            resources.ApplyResources(this.btnPERMISSION, "btnPERMISSION");
            this.btnPERMISSION.BackColor = System.Drawing.Color.Transparent;
            this.btnPERMISSION.Image = ((System.Drawing.Image)(resources.GetObject("btnPERMISSION.Image")));
            this.btnPERMISSION.Name = "btnPERMISSION";
            this.btnPERMISSION.Selected = false;
            this.btnPERMISSION.Click += new System.EventHandler(this.btnPermission_Click);
            // 
            // lblDATA
            // 
            resources.ApplyResources(this.lblDATA, "lblDATA");
            this.lblDATA.BackColor = System.Drawing.Color.Silver;
            this.lblDATA.Name = "lblDATA";
            this.lblDATA.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lblData_MouseDoubleClick);
            // 
            // btnBACKUP
            // 
            resources.ApplyResources(this.btnBACKUP, "btnBACKUP");
            this.btnBACKUP.BackColor = System.Drawing.Color.Transparent;
            this.btnBACKUP.Image = ((System.Drawing.Image)(resources.GetObject("btnBACKUP.Image")));
            this.btnBACKUP.Name = "btnBACKUP";
            this.btnBACKUP.Selected = false;
            this.btnBACKUP.Click += new System.EventHandler(this.btnBackup_Click);
            // 
            // btnRESTORE
            // 
            resources.ApplyResources(this.btnRESTORE, "btnRESTORE");
            this.btnRESTORE.BackColor = System.Drawing.Color.Transparent;
            this.btnRESTORE.Image = ((System.Drawing.Image)(resources.GetObject("btnRESTORE.Image")));
            this.btnRESTORE.Name = "btnRESTORE";
            this.btnRESTORE.Selected = false;
            this.btnRESTORE.Click += new System.EventHandler(this.btnRestore_Click);
            // 
            // btnAUDIT_TRIAL
            // 
            resources.ApplyResources(this.btnAUDIT_TRIAL, "btnAUDIT_TRIAL");
            this.btnAUDIT_TRIAL.BackColor = System.Drawing.Color.Transparent;
            this.btnAUDIT_TRIAL.Image = ((System.Drawing.Image)(resources.GetObject("btnAUDIT_TRIAL.Image")));
            this.btnAUDIT_TRIAL.Name = "btnAUDIT_TRIAL";
            this.btnAUDIT_TRIAL.Selected = false;
            this.btnAUDIT_TRIAL.Click += new System.EventHandler(this.btnAuditrial_Click);
            // 
            // btnMAINTENANCE
            // 
            resources.ApplyResources(this.btnMAINTENANCE, "btnMAINTENANCE");
            this.btnMAINTENANCE.BackColor = System.Drawing.Color.Transparent;
            this.btnMAINTENANCE.Image = ((System.Drawing.Image)(resources.GetObject("btnMAINTENANCE.Image")));
            this.btnMAINTENANCE.Name = "btnMAINTENANCE";
            this.btnMAINTENANCE.Selected = false;
            this.btnMAINTENANCE.Click += new System.EventHandler(this.btnMAINTENANCE_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel15, "panel15");
            this.panel15.Name = "panel15";
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel13, "panel13");
            this.panel13.Name = "panel13";
            // 
            // lblSYSTEM_SECURITY
            // 
            this.lblSYSTEM_SECURITY.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblSYSTEM_SECURITY, "lblSYSTEM_SECURITY");
            this.lblSYSTEM_SECURITY.Image = global::EPower.Properties.Resources.key;
            this.lblSYSTEM_SECURITY.IsTitle = true;
            this.lblSYSTEM_SECURITY.Name = "lblSYSTEM_SECURITY";
            this.lblSYSTEM_SECURITY.Selected = false;
            this.lblSYSTEM_SECURITY.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.Transparent;
            this.panel16.Controls.Add(this.panel20);
            this.panel16.Controls.Add(this.lblSYSTEM_SECURITY_1);
            resources.ApplyResources(this.panel16, "panel16");
            this.panel16.Name = "panel16";
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.main);
            this.panel20.Controls.Add(this.panel17);
            this.panel20.Controls.Add(this.panel18);
            this.panel20.Controls.Add(this.panel19);
            resources.ApplyResources(this.panel20, "panel20");
            this.panel20.Name = "panel20";
            // 
            // main
            // 
            this.main.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.main, "main");
            this.main.Name = "main";
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel17, "panel17");
            this.panel17.Name = "panel17";
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel18, "panel18");
            this.panel18.Name = "panel18";
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel19, "panel19");
            this.panel19.Name = "panel19";
            // 
            // lblSYSTEM_SECURITY_1
            // 
            this.lblSYSTEM_SECURITY_1.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblSYSTEM_SECURITY_1, "lblSYSTEM_SECURITY_1");
            this.lblSYSTEM_SECURITY_1.Image = null;
            this.lblSYSTEM_SECURITY_1.IsTitle = true;
            this.lblSYSTEM_SECURITY_1.Name = "lblSYSTEM_SECURITY_1";
            this.lblSYSTEM_SECURITY_1.Selected = false;
            this.lblSYSTEM_SECURITY_1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PageSecurity
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "PageSecurity";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel16;
        private Panel panel20;
        private ExTabItem lblSYSTEM_SECURITY_1;
        private Panel panel12;
        private ExMenuItem btnLOGIN;
        private ExTabItem lblSYSTEM_SECURITY;
        private SplitContainer splitContainer1;
        private Panel panel17;
        private Panel panel18;
        private Panel panel19;
        private Panel panel2;
        private Panel panel15;
        private Panel panel13;
        private Panel panel3;
        private ExMenuItem btnROLE;
        private FlowLayoutPanel panel1;
        private ExMenuItem btnPERMISSION;
        private ExMenuItem btnBACKUP;
        private Label lblDATA;
        private ExMenuItem btnRESTORE;
        private Label lblUSER_PERMISSION;
        private ExMenuItem btnAUDIT_TRIAL;
        private Panel main;
        private ExMenuItem btnMAINTENANCE;
    }
}
