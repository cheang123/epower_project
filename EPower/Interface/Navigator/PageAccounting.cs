﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using EPower.Accounting.Interface;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Logic;

namespace EPower.Interface
{
    public partial class PageAccounting : UserControl
    {

        public PageAccounting()
        {
            InitializeComponent();

            this.btnINCOME.Enabled = Login.IsAuthorized(Permission.ACC_TRANS_INCOME);
            this.btnEXPENSE.Enabled = Login.IsAuthorized(Permission.ACC_TRANS_EXPENSE);
            this.btnSTAFF_EXPENSE.Enabled = Login.IsAuthorized(Permission.ACC_TRANS_STAFFEXPENSE);
            this.btnLOAN.Enabled = Login.IsAuthorized(Permission.ACC_TRANS_LOAN);
            this.btnEXCHANGE_RATE.Enabled = Login.IsAuthorized(Permission.ACC_TRANS_EXCHANGERATE);

            this.btnACCOUNT_CHART.Enabled = Login.IsAuthorized(Permission.ACC_CHART_ACCOUNTCHART);
            this.btnACCOUNT_SETTING.Enabled = Login.IsAuthorized(Permission.ACC_CHART_CONFIG);

            this.btnFIX_ASSET_TYPE.Enabled = Login.IsAuthorized(Permission.ACC_FIXASSET_CATEGORY);
            this.btnFIX_ASSET_ITEM.Enabled = Login.IsAuthorized(Permission.ACC_FIXASSET_ITEM);

            this.btnREPORT_INCOME.Enabled = Login.IsAuthorized(Permission.ACC_REPORT_INCOME);
            this.btnREPORT_EXPENSE.Enabled = Login.IsAuthorized(Permission.ACC_REPORT_EXPENSE);
            this.btnREPORT_PROFIT_LOSS.Enabled = Login.IsAuthorized(Permission.ACC_REPORT_PL);
            this.btnREPORT_FIX_ASSET.Enabled = Login.IsAuthorized(Permission.ACC_REPORT_FIXASSET_SUMMARY);
            this.btnREPORT_FIX_ASSET_DEPRECIATION.Enabled = Login.IsAuthorized(Permission.ACC_REPORT_DEPRECIATION);

            // by default show expense page
            this.showPage(typeof(PageAccountTransactionExpense), btnEXPENSE);
        }


        #region Show Page
        public Control currentPage = null;
        private Dictionary<Type, Control> pages = new Dictionary<Type, Control>();
        private void showPage(Type type, object sender)
        {
            if (!this.pages.ContainsKey(type))
            {
                this.pages[type] = (Control)Activator.CreateInstance(type);
                this.pages[type].Size = this.main.Size;
                if (this.pages[type] is Form)
                {
                    Form frm = (Form)this.pages[type];
                    frm.TopLevel = false;
                    frm.Visible = true;
                    frm.FormBorderStyle = FormBorderStyle.None;
                }
                this.main.Controls.Add(this.pages[type]);
                this.pages[type].Dock = DockStyle.Fill;
                ResourceHelper.ApplyResource(this.pages[type]);
            }
            if (this.currentPage != this.pages[type])
            {
                this.pages[type].Show();
                if (this.currentPage != null)
                {
                    this.currentPage.Hide();
                }
                this.currentPage = this.pages[type];
            }
            this.lblACCOUNTING_1.Text = ((Control)sender).Text;
            ((ExMenuItem)sender).Selected = true;
        }
        #endregion ShowPage() 


        private void btnAccountItem_Click(object sender, EventArgs e)
        {
            //this.showPage(typeof(PageAccountItem), sender);
            //this.showPage(typeof(PageAccountChart), sender);
            this.showPage(typeof(PageAccount), sender);
        }

        private void btnTranIncome_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageAccountTransactionIncome), btnINCOME);
        }

        private void btnTranExpense_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageAccountTransactionExpense), btnEXPENSE);
        }

        private void btnReportProfitLoss_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportProfitLoss), sender);
        }

        private void btnReportIncome_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportTranIncomeDetail), sender);
        }

        private void btnReportExpense_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportTranExpenseDetail), sender);
        }

        private void btnFixAssetCategory_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageFixAssetCategory), sender);
        }


        private void btnFixAssetItem_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageFixAssetItem), sender);
        }

        private void btnReportFixAsset_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportFixAsset), sender);
            ((PageReportFixAsset)this.currentPage).lookup();
        }

        private void btnReportFixAssetDepreciation_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportFixAssetDepreciation), sender);
            ((PageReportFixAssetDepreciation)this.currentPage).lookup();
        }

        private void btnExchangeRate_Click(object sender, EventArgs e)
        {
            new DialogExchangeRate().ShowDialog();
        }

        private void btnStaffExpense_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageStaffExpense), sender);
        }

        private void bntLoan_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageLoan), sender);
            ((PageLoan)this.currentPage).lookup();
        }

        private void btnACCOUNT_SETTING_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageAccountSetting), sender);
        }

        private void lblSETUP_ACCOUNT_CHART_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (btnACCOUNT_SETTING.Visible)
                {
                    btnACCOUNT_SETTING.Visible = false;
                }
                else
                {
                    btnACCOUNT_SETTING.Visible = true;
                }
            }
        }
    }
}
