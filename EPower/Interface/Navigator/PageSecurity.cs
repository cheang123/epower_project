﻿using EPower.Base.Logic;
using EPower.Properties;
using HB01.Logics.Security;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using SoftTech.Security.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Login = SoftTech.Security.Logic.Login;

namespace EPower.Interface
{
    public partial class PageSecurity : UserControl
    {
        public PageSecurity()
        {
            InitializeComponent();

            // Assign permission.
            this.btnLOGIN.Enabled = Login.IsAuthorized(Permission.SECURITY_USER);
            this.btnROLE.Enabled = Login.IsAuthorized(Permission.SECURITY_PERMISION);

            this.btnBACKUP.Enabled = Login.IsAuthorized(Permission.SECURITY_DATA_DATABACKUP);
            this.btnRESTORE.Enabled = Login.IsAuthorized(Permission.SECURITY_DATA_DATARESTORE);

            this.btnAUDIT_TRIAL.Enabled = Login.IsAuthorized(Permission.SECURITY_USING_VIEWCHANGELOG);

            //this.btnPermission.Visible = true;
            this.btnRESTORE.Visible = !DataHelper.ParseToBoolean(Method.Utilities[Utility.BANK_PAYMENT_ENABLE]);

            //#if DEBUG
            //this.btnPermission.Visible = true;
            //#endif 
        }

        #region Show Page
        private Control currentPage = null;
        private Dictionary<Type, Control> pages = new Dictionary<Type, Control>();
        private void showPage(Type type, object sender)
        {
            if (!this.pages.ContainsKey(type))
            {
                this.pages[type] = (Control)Activator.CreateInstance(type);
                this.pages[type].Size = this.main.Size;
                if (this.pages[type] is Form)
                {
                    Form frm = (Form)this.pages[type];
                    frm.TopLevel = false;
                    frm.Visible = true;
                    frm.FormBorderStyle = FormBorderStyle.None;
                }
                this.main.Controls.Add(this.pages[type]);
                this.pages[type].Dock = DockStyle.Fill;
                ResourceHelper.ApplyResource(this.pages[type]);
            }
            if (this.currentPage != this.pages[type])
            {
                this.pages[type].Show();
                if (this.currentPage != null)
                {
                    this.currentPage.Hide();
                }
                this.currentPage = this.pages[type];
            }
            this.lblSYSTEM_SECURITY_1.Text = ((Control)sender).Text;
        }
        #endregion ShowPage()


        private void btnLogin_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageLogin), sender);
            ((PageLogin)this.currentPage).LoadRole();
        }

        private void btnRole_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageRole), sender);
        }
        private void btnPermission_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PagePermission), sender);
        }
        private void btnBackup_Click(object sender, EventArgs e)
        {
            this.btnBACKUP.Selected = false;
            
            var diag = new DialogBackup(Settings.Default.PATH_BACKUP, PointerLogic.isConnectedPointer);

            diag.BackupPointer += (object sender1, EventArgs e1) => 
            {
                if (PointerLogic.isConnectedPointer)
                {
                    try
                    {
                        var fileName = AppLogic.Instance.BackUpDatabase(diag.GetFilePath(), diag.GetFileName());
                        diag.SetFileName(fileName);
                    }
                    catch (Exception ex)
                    { 
                        MsgBox.ShowWarning(ex.Message, Resources.BACKUP);
                    }  
                }
            };
            diag.ShowDialog();
        }

        private void btnRestore_Click(object sender, EventArgs e)
        {
            this.btnRESTORE.Selected = false;
            (new DialogRestore(Settings.Default.PATH_BACKUP)).ShowDialog();
        }

        private void btnAuditrial_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageAuditrial), sender);
            ((PageAuditrial)this.currentPage).BindData();
        }

        private void btnLogOut_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void btnChangePassword_Click(object sender, EventArgs e)
        {
            new DialogChangePassword().ShowDialog();
        }

        private void btnChangeLanguge_Click(object sender, EventArgs e)
        {
            DialogChangeLanguage dig = new DialogChangeLanguage();
            if (dig.ShowDialog() == DialogResult.OK)
            {
                Application.Restart();
            }
        }

        private void lblData_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (btnRESTORE.Visible)
                {
                    btnRESTORE.Visible = false;
                }
                else
                {
                    btnRESTORE.Visible = true;
                }
            }
        }

        private void lblUSER_PERMISSION_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && Control.ModifierKeys == Keys.Alt)
            {
                if (btnPERMISSION.Visible)
                {
                    btnPERMISSION.Visible = false;
                }
                else
                {
                    btnPERMISSION.Visible = true;
                }
            }
        }

        private void btnMAINTENANCE_Click(object sender, EventArgs e)
        {
            if (MsgBox.ShowQuestion(Resources.MS_START_PROCESS, Resources.MAINTENANCE) == DialogResult.Yes)
            {
                bool isSuccess = false;
                try
                {
                    Runner.RunNewThread(delegate ()
                {
                    DBDataContext.Db.CommandTimeout = 36000;
                    Runner.Instance.Text = Resources.MS_RUN_BILL_BACKUP;
                    Application.DoEvents();
                    string strBackupFileName = DBDataContext.Db.Connection.Database + " " + DBDataContext.Db.GetSystemDate().ToString("yyyyMMddHHmmss") + " Backup before maintenance.bak";
                    DBA.BackupToDisk(Settings.Default.PATH_BACKUP, strBackupFileName);

                    Runner.Instance.Text = Resources.PROCESSING;
                    string dbName = DBDataContext.Db.Connection.Database;
                    Application.DoEvents();
                    var logName = DBDataContext.Db.ExecuteQuery<string>("SELECT TOP 1 name FROM " + dbName + ".sys.database_files ORDER BY name DESC").First();
                    DBDataContext.Db.ExecuteCommand("EXEC dbo.REBUILD_INDEX @Database=@p0, @Logfile=@p1", dbName, logName);
                    isSuccess = true;

                }, Resources.PROCESSING);

                    if (isSuccess)
                    {
                        MsgBox.ShowInformation(Resources.SUCCESS, Resources.MS_RUN_BILL_SUCCESS);
                        Application.Restart();
                    }
                }
                catch (Exception ex)
                {
                    MsgBox.ShowWarning(ex.Message, Resources.WARNING);
                }
            }
        }
    }
}
