﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageReport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageReport));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnREPORT_TRIALBALANCE_DAILY = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_TRIAL_BALANCE_CUSTOMER_DETAIL = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_AR_DETAIL = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_AR_DETAIL_NEW = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_DEPOSIT_PREPAYMENT = new SoftTech.Component.ExMenuItem();
            this.btnBLOCK_AND_RECONNECT = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_AGING = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_CASH_DAILY_SUMMARY = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_CASH_DAILY = new SoftTech.Component.ExMenuItem();
            this.lblREPORT_SUMMARY = new System.Windows.Forms.Label();
            this.btnREPORT_CASH_RECEIVED = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_CASH_COLLECTION = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_MONTHLY_POWER_INCOME = new SoftTech.Component.ExMenuItem();
            this.btnSOLD_BY_CUSTOMER_TYPE = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_MONTHLY_OTHER_INCOME = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_MONTHLY_DISCOUNT = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_YEARLY_POWER_USAGE = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_CUSTOMER = new SoftTech.Component.ExMenuItem();
            this.btnGENERAL_CUSTOMER_INFO = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_METER = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_MONTHLY_POWER_USAGE = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_CUSTOMER_USAGE_SUMMARY = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_RECURRING_SERVING = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_BANK_TRANSACTION = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_QUARTERLY = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_ANNUAL = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_CUSTOMER_USAGE_STATISTIC = new SoftTech.Component.ExMenuItem();
            this.lblE_FILLINGS = new System.Windows.Forms.Label();
            this.btnE_FILLING_SALE = new SoftTech.Component.ExMenuItem();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.lblREPORTS = new SoftTech.Component.ExTabItem();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.main = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.lblREPORT_1 = new SoftTech.Component.ExTabItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel20.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            resources.ApplyResources(this.splitContainer1, "splitContainer1");
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel12);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel16);
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.panel2);
            this.panel12.Controls.Add(this.lblREPORTS);
            resources.ApplyResources(this.panel12, "panel12");
            this.panel12.Name = "panel12";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.panel15);
            this.panel2.Controls.Add(this.panel13);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // panel1
            // 
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.BackColor = System.Drawing.Color.LightGray;
            this.panel1.Controls.Add(this.btnREPORT_TRIALBALANCE_DAILY);
            this.panel1.Controls.Add(this.btnREPORT_TRIAL_BALANCE_CUSTOMER_DETAIL);
            this.panel1.Controls.Add(this.btnREPORT_AR_DETAIL);
            this.panel1.Controls.Add(this.btnREPORT_AR_DETAIL_NEW);
            this.panel1.Controls.Add(this.btnREPORT_DEPOSIT_PREPAYMENT);
            this.panel1.Controls.Add(this.btnBLOCK_AND_RECONNECT);
            this.panel1.Controls.Add(this.btnREPORT_AGING);
            this.panel1.Controls.Add(this.btnREPORT_CASH_DAILY_SUMMARY);
            this.panel1.Controls.Add(this.btnREPORT_CASH_DAILY);
            this.panel1.Controls.Add(this.lblREPORT_SUMMARY);
            this.panel1.Controls.Add(this.btnREPORT_CASH_RECEIVED);
            this.panel1.Controls.Add(this.btnREPORT_CASH_COLLECTION);
            this.panel1.Controls.Add(this.btnREPORT_MONTHLY_POWER_INCOME);
            this.panel1.Controls.Add(this.btnSOLD_BY_CUSTOMER_TYPE);
            this.panel1.Controls.Add(this.btnREPORT_MONTHLY_OTHER_INCOME);
            this.panel1.Controls.Add(this.btnREPORT_MONTHLY_DISCOUNT);
            this.panel1.Controls.Add(this.btnREPORT_YEARLY_POWER_USAGE);
            this.panel1.Controls.Add(this.btnREPORT_CUSTOMER);
            this.panel1.Controls.Add(this.btnGENERAL_CUSTOMER_INFO);
            this.panel1.Controls.Add(this.btnREPORT_METER);
            this.panel1.Controls.Add(this.btnREPORT_MONTHLY_POWER_USAGE);
            this.panel1.Controls.Add(this.btnREPORT_CUSTOMER_USAGE_SUMMARY);
            this.panel1.Controls.Add(this.btnREPORT_RECURRING_SERVING);
            this.panel1.Controls.Add(this.btnREPORT_BANK_TRANSACTION);
            this.panel1.Controls.Add(this.btnREPORT_QUARTERLY);
            this.panel1.Controls.Add(this.btnREPORT_ANNUAL);
            this.panel1.Controls.Add(this.btnREPORT_CUSTOMER_USAGE_STATISTIC);
            this.panel1.Controls.Add(this.lblE_FILLINGS);
            this.panel1.Controls.Add(this.btnE_FILLING_SALE);
            this.panel1.Name = "panel1";
            // 
            // btnREPORT_TRIALBALANCE_DAILY
            // 
            resources.ApplyResources(this.btnREPORT_TRIALBALANCE_DAILY, "btnREPORT_TRIALBALANCE_DAILY");
            this.btnREPORT_TRIALBALANCE_DAILY.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_TRIALBALANCE_DAILY.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_TRIALBALANCE_DAILY.Image")));
            this.btnREPORT_TRIALBALANCE_DAILY.Name = "btnREPORT_TRIALBALANCE_DAILY";
            this.btnREPORT_TRIALBALANCE_DAILY.Selected = false;
            this.btnREPORT_TRIALBALANCE_DAILY.Click += new System.EventHandler(this.btnReportTrialBalanceDaily_Click);
            // 
            // btnREPORT_TRIAL_BALANCE_CUSTOMER_DETAIL
            // 
            resources.ApplyResources(this.btnREPORT_TRIAL_BALANCE_CUSTOMER_DETAIL, "btnREPORT_TRIAL_BALANCE_CUSTOMER_DETAIL");
            this.btnREPORT_TRIAL_BALANCE_CUSTOMER_DETAIL.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_TRIAL_BALANCE_CUSTOMER_DETAIL.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_TRIAL_BALANCE_CUSTOMER_DETAIL.Image")));
            this.btnREPORT_TRIAL_BALANCE_CUSTOMER_DETAIL.Name = "btnREPORT_TRIAL_BALANCE_CUSTOMER_DETAIL";
            this.btnREPORT_TRIAL_BALANCE_CUSTOMER_DETAIL.Selected = false;
            this.btnREPORT_TRIAL_BALANCE_CUSTOMER_DETAIL.Click += new System.EventHandler(this.btnReportTrialBalanceDetailDaily_Click);
            // 
            // btnREPORT_AR_DETAIL
            // 
            resources.ApplyResources(this.btnREPORT_AR_DETAIL, "btnREPORT_AR_DETAIL");
            this.btnREPORT_AR_DETAIL.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_AR_DETAIL.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_AR_DETAIL.Image")));
            this.btnREPORT_AR_DETAIL.Name = "btnREPORT_AR_DETAIL";
            this.btnREPORT_AR_DETAIL.Selected = false;
            this.btnREPORT_AR_DETAIL.Click += new System.EventHandler(this.btnARDetail_Click);
            // 
            // btnREPORT_AR_DETAIL_NEW
            // 
            resources.ApplyResources(this.btnREPORT_AR_DETAIL_NEW, "btnREPORT_AR_DETAIL_NEW");
            this.btnREPORT_AR_DETAIL_NEW.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_AR_DETAIL_NEW.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_AR_DETAIL_NEW.Image")));
            this.btnREPORT_AR_DETAIL_NEW.Name = "btnREPORT_AR_DETAIL_NEW";
            this.btnREPORT_AR_DETAIL_NEW.Selected = false;
            this.btnREPORT_AR_DETAIL_NEW.Click += new System.EventHandler(this.btnREPORT_AR_DETAIL_NEW_Click);
            // 
            // btnREPORT_DEPOSIT_PREPAYMENT
            // 
            resources.ApplyResources(this.btnREPORT_DEPOSIT_PREPAYMENT, "btnREPORT_DEPOSIT_PREPAYMENT");
            this.btnREPORT_DEPOSIT_PREPAYMENT.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_DEPOSIT_PREPAYMENT.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_DEPOSIT_PREPAYMENT.Image")));
            this.btnREPORT_DEPOSIT_PREPAYMENT.Name = "btnREPORT_DEPOSIT_PREPAYMENT";
            this.btnREPORT_DEPOSIT_PREPAYMENT.Selected = false;
            this.btnREPORT_DEPOSIT_PREPAYMENT.Click += new System.EventHandler(this.btnDepositDetail_Click);
            // 
            // btnBLOCK_AND_RECONNECT
            // 
            resources.ApplyResources(this.btnBLOCK_AND_RECONNECT, "btnBLOCK_AND_RECONNECT");
            this.btnBLOCK_AND_RECONNECT.BackColor = System.Drawing.Color.Transparent;
            this.btnBLOCK_AND_RECONNECT.Image = ((System.Drawing.Image)(resources.GetObject("btnBLOCK_AND_RECONNECT.Image")));
            this.btnBLOCK_AND_RECONNECT.Name = "btnBLOCK_AND_RECONNECT";
            this.btnBLOCK_AND_RECONNECT.Selected = false;
            this.btnBLOCK_AND_RECONNECT.Click += new System.EventHandler(this.btnBlockAndReconnect_Click);
            // 
            // btnREPORT_AGING
            // 
            resources.ApplyResources(this.btnREPORT_AGING, "btnREPORT_AGING");
            this.btnREPORT_AGING.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_AGING.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_AGING.Image")));
            this.btnREPORT_AGING.Name = "btnREPORT_AGING";
            this.btnREPORT_AGING.Selected = false;
            this.btnREPORT_AGING.Click += new System.EventHandler(this.btnReportAging_Click);
            // 
            // btnREPORT_CASH_DAILY_SUMMARY
            // 
            resources.ApplyResources(this.btnREPORT_CASH_DAILY_SUMMARY, "btnREPORT_CASH_DAILY_SUMMARY");
            this.btnREPORT_CASH_DAILY_SUMMARY.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_CASH_DAILY_SUMMARY.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_CASH_DAILY_SUMMARY.Image")));
            this.btnREPORT_CASH_DAILY_SUMMARY.Name = "btnREPORT_CASH_DAILY_SUMMARY";
            this.btnREPORT_CASH_DAILY_SUMMARY.Selected = false;
            this.btnREPORT_CASH_DAILY_SUMMARY.Click += new System.EventHandler(this.btnCashDailySummaryReport_Click);
            // 
            // btnREPORT_CASH_DAILY
            // 
            resources.ApplyResources(this.btnREPORT_CASH_DAILY, "btnREPORT_CASH_DAILY");
            this.btnREPORT_CASH_DAILY.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_CASH_DAILY.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_CASH_DAILY.Image")));
            this.btnREPORT_CASH_DAILY.Name = "btnREPORT_CASH_DAILY";
            this.btnREPORT_CASH_DAILY.Selected = false;
            this.btnREPORT_CASH_DAILY.Click += new System.EventHandler(this.btnCloseCashDrawer_Click);
            // 
            // lblREPORT_SUMMARY
            // 
            resources.ApplyResources(this.lblREPORT_SUMMARY, "lblREPORT_SUMMARY");
            this.lblREPORT_SUMMARY.BackColor = System.Drawing.Color.Silver;
            this.lblREPORT_SUMMARY.Name = "lblREPORT_SUMMARY";
            // 
            // btnREPORT_CASH_RECEIVED
            // 
            resources.ApplyResources(this.btnREPORT_CASH_RECEIVED, "btnREPORT_CASH_RECEIVED");
            this.btnREPORT_CASH_RECEIVED.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_CASH_RECEIVED.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_CASH_RECEIVED.Image")));
            this.btnREPORT_CASH_RECEIVED.Name = "btnREPORT_CASH_RECEIVED";
            this.btnREPORT_CASH_RECEIVED.Selected = false;
            this.btnREPORT_CASH_RECEIVED.Click += new System.EventHandler(this.btnIncomeMonthly_Click);
            // 
            // btnREPORT_CASH_COLLECTION
            // 
            resources.ApplyResources(this.btnREPORT_CASH_COLLECTION, "btnREPORT_CASH_COLLECTION");
            this.btnREPORT_CASH_COLLECTION.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_CASH_COLLECTION.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_CASH_COLLECTION.Image")));
            this.btnREPORT_CASH_COLLECTION.Name = "btnREPORT_CASH_COLLECTION";
            this.btnREPORT_CASH_COLLECTION.Selected = false;
            this.btnREPORT_CASH_COLLECTION.Click += new System.EventHandler(this.btnREPORT_CASH_COLLECTION_Click);
            // 
            // btnREPORT_MONTHLY_POWER_INCOME
            // 
            resources.ApplyResources(this.btnREPORT_MONTHLY_POWER_INCOME, "btnREPORT_MONTHLY_POWER_INCOME");
            this.btnREPORT_MONTHLY_POWER_INCOME.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_MONTHLY_POWER_INCOME.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_MONTHLY_POWER_INCOME.Image")));
            this.btnREPORT_MONTHLY_POWER_INCOME.Name = "btnREPORT_MONTHLY_POWER_INCOME";
            this.btnREPORT_MONTHLY_POWER_INCOME.Selected = false;
            this.btnREPORT_MONTHLY_POWER_INCOME.Click += new System.EventHandler(this.btnMonthlyPowerIncome_Click);
            // 
            // btnSOLD_BY_CUSTOMER_TYPE
            // 
            resources.ApplyResources(this.btnSOLD_BY_CUSTOMER_TYPE, "btnSOLD_BY_CUSTOMER_TYPE");
            this.btnSOLD_BY_CUSTOMER_TYPE.BackColor = System.Drawing.Color.Transparent;
            this.btnSOLD_BY_CUSTOMER_TYPE.Image = ((System.Drawing.Image)(resources.GetObject("btnSOLD_BY_CUSTOMER_TYPE.Image")));
            this.btnSOLD_BY_CUSTOMER_TYPE.Name = "btnSOLD_BY_CUSTOMER_TYPE";
            this.btnSOLD_BY_CUSTOMER_TYPE.Selected = false;
            this.btnSOLD_BY_CUSTOMER_TYPE.Click += new System.EventHandler(this.btnSOLD_BY_CUSTOMER_TYPE_Click);
            // 
            // btnREPORT_MONTHLY_OTHER_INCOME
            // 
            resources.ApplyResources(this.btnREPORT_MONTHLY_OTHER_INCOME, "btnREPORT_MONTHLY_OTHER_INCOME");
            this.btnREPORT_MONTHLY_OTHER_INCOME.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_MONTHLY_OTHER_INCOME.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_MONTHLY_OTHER_INCOME.Image")));
            this.btnREPORT_MONTHLY_OTHER_INCOME.Name = "btnREPORT_MONTHLY_OTHER_INCOME";
            this.btnREPORT_MONTHLY_OTHER_INCOME.Selected = false;
            this.btnREPORT_MONTHLY_OTHER_INCOME.Click += new System.EventHandler(this.btnOtherRevenueByMonth_Click);
            // 
            // btnREPORT_MONTHLY_DISCOUNT
            // 
            this.btnREPORT_MONTHLY_DISCOUNT.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnREPORT_MONTHLY_DISCOUNT, "btnREPORT_MONTHLY_DISCOUNT");
            this.btnREPORT_MONTHLY_DISCOUNT.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_MONTHLY_DISCOUNT.Image")));
            this.btnREPORT_MONTHLY_DISCOUNT.Name = "btnREPORT_MONTHLY_DISCOUNT";
            this.btnREPORT_MONTHLY_DISCOUNT.Selected = false;
            this.btnREPORT_MONTHLY_DISCOUNT.Click += new System.EventHandler(this.btnDiscountDetail_Click);
            // 
            // btnREPORT_YEARLY_POWER_USAGE
            // 
            resources.ApplyResources(this.btnREPORT_YEARLY_POWER_USAGE, "btnREPORT_YEARLY_POWER_USAGE");
            this.btnREPORT_YEARLY_POWER_USAGE.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_YEARLY_POWER_USAGE.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_YEARLY_POWER_USAGE.Image")));
            this.btnREPORT_YEARLY_POWER_USAGE.Name = "btnREPORT_YEARLY_POWER_USAGE";
            this.btnREPORT_YEARLY_POWER_USAGE.Selected = false;
            this.btnREPORT_YEARLY_POWER_USAGE.Click += new System.EventHandler(this.btnYearlyPowerUsage_Click);
            // 
            // btnREPORT_CUSTOMER
            // 
            resources.ApplyResources(this.btnREPORT_CUSTOMER, "btnREPORT_CUSTOMER");
            this.btnREPORT_CUSTOMER.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_CUSTOMER.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_CUSTOMER.Image")));
            this.btnREPORT_CUSTOMER.Name = "btnREPORT_CUSTOMER";
            this.btnREPORT_CUSTOMER.Selected = false;
            this.btnREPORT_CUSTOMER.Click += new System.EventHandler(this.btnCustomer_Click);
            // 
            // btnGENERAL_CUSTOMER_INFO
            // 
            resources.ApplyResources(this.btnGENERAL_CUSTOMER_INFO, "btnGENERAL_CUSTOMER_INFO");
            this.btnGENERAL_CUSTOMER_INFO.BackColor = System.Drawing.Color.Transparent;
            this.btnGENERAL_CUSTOMER_INFO.Image = ((System.Drawing.Image)(resources.GetObject("btnGENERAL_CUSTOMER_INFO.Image")));
            this.btnGENERAL_CUSTOMER_INFO.Name = "btnGENERAL_CUSTOMER_INFO";
            this.btnGENERAL_CUSTOMER_INFO.Selected = false;
            this.btnGENERAL_CUSTOMER_INFO.Click += new System.EventHandler(this.btnGENERAL_CUSTOMER_Click);
            // 
            // btnREPORT_METER
            // 
            resources.ApplyResources(this.btnREPORT_METER, "btnREPORT_METER");
            this.btnREPORT_METER.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_METER.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_METER.Image")));
            this.btnREPORT_METER.Name = "btnREPORT_METER";
            this.btnREPORT_METER.Selected = false;
            this.btnREPORT_METER.Click += new System.EventHandler(this.btnREPORT_METER_Click);
            // 
            // btnREPORT_MONTHLY_POWER_USAGE
            // 
            resources.ApplyResources(this.btnREPORT_MONTHLY_POWER_USAGE, "btnREPORT_MONTHLY_POWER_USAGE");
            this.btnREPORT_MONTHLY_POWER_USAGE.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_MONTHLY_POWER_USAGE.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_MONTHLY_POWER_USAGE.Image")));
            this.btnREPORT_MONTHLY_POWER_USAGE.Name = "btnREPORT_MONTHLY_POWER_USAGE";
            this.btnREPORT_MONTHLY_POWER_USAGE.Selected = false;
            this.btnREPORT_MONTHLY_POWER_USAGE.Click += new System.EventHandler(this.btnUsageMonthly_Click);
            // 
            // btnREPORT_CUSTOMER_USAGE_SUMMARY
            // 
            resources.ApplyResources(this.btnREPORT_CUSTOMER_USAGE_SUMMARY, "btnREPORT_CUSTOMER_USAGE_SUMMARY");
            this.btnREPORT_CUSTOMER_USAGE_SUMMARY.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_CUSTOMER_USAGE_SUMMARY.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_CUSTOMER_USAGE_SUMMARY.Image")));
            this.btnREPORT_CUSTOMER_USAGE_SUMMARY.Name = "btnREPORT_CUSTOMER_USAGE_SUMMARY";
            this.btnREPORT_CUSTOMER_USAGE_SUMMARY.Selected = false;
            this.btnREPORT_CUSTOMER_USAGE_SUMMARY.Click += new System.EventHandler(this.btnCustomerUsageSummary_Click);
            // 
            // btnREPORT_RECURRING_SERVING
            // 
            resources.ApplyResources(this.btnREPORT_RECURRING_SERVING, "btnREPORT_RECURRING_SERVING");
            this.btnREPORT_RECURRING_SERVING.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_RECURRING_SERVING.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_RECURRING_SERVING.Image")));
            this.btnREPORT_RECURRING_SERVING.Name = "btnREPORT_RECURRING_SERVING";
            this.btnREPORT_RECURRING_SERVING.Selected = false;
            this.btnREPORT_RECURRING_SERVING.Click += new System.EventHandler(this.btnRecurringServing_Click);
            // 
            // btnREPORT_BANK_TRANSACTION
            // 
            resources.ApplyResources(this.btnREPORT_BANK_TRANSACTION, "btnREPORT_BANK_TRANSACTION");
            this.btnREPORT_BANK_TRANSACTION.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_BANK_TRANSACTION.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_BANK_TRANSACTION.Image")));
            this.btnREPORT_BANK_TRANSACTION.Name = "btnREPORT_BANK_TRANSACTION";
            this.btnREPORT_BANK_TRANSACTION.Selected = false;
            this.btnREPORT_BANK_TRANSACTION.Click += new System.EventHandler(this.btnReportBankTran_Click);
            // 
            // btnREPORT_QUARTERLY
            // 
            resources.ApplyResources(this.btnREPORT_QUARTERLY, "btnREPORT_QUARTERLY");
            this.btnREPORT_QUARTERLY.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_QUARTERLY.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_QUARTERLY.Image")));
            this.btnREPORT_QUARTERLY.Name = "btnREPORT_QUARTERLY";
            this.btnREPORT_QUARTERLY.Selected = false;
            this.btnREPORT_QUARTERLY.Click += new System.EventHandler(this.exMenuItem1_Click);
            // 
            // btnREPORT_ANNUAL
            // 
            resources.ApplyResources(this.btnREPORT_ANNUAL, "btnREPORT_ANNUAL");
            this.btnREPORT_ANNUAL.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_ANNUAL.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_ANNUAL.Image")));
            this.btnREPORT_ANNUAL.Name = "btnREPORT_ANNUAL";
            this.btnREPORT_ANNUAL.Selected = false;
            this.btnREPORT_ANNUAL.Click += new System.EventHandler(this.btnAnnualReport_Click);
            // 
            // btnREPORT_CUSTOMER_USAGE_STATISTIC
            // 
            resources.ApplyResources(this.btnREPORT_CUSTOMER_USAGE_STATISTIC, "btnREPORT_CUSTOMER_USAGE_STATISTIC");
            this.btnREPORT_CUSTOMER_USAGE_STATISTIC.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_CUSTOMER_USAGE_STATISTIC.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_CUSTOMER_USAGE_STATISTIC.Image")));
            this.btnREPORT_CUSTOMER_USAGE_STATISTIC.Name = "btnREPORT_CUSTOMER_USAGE_STATISTIC";
            this.btnREPORT_CUSTOMER_USAGE_STATISTIC.Selected = false;
            this.btnREPORT_CUSTOMER_USAGE_STATISTIC.Click += new System.EventHandler(this.btnReportCustomerUsageStatistic_Click);
            // 
            // lblE_FILLINGS
            // 
            resources.ApplyResources(this.lblE_FILLINGS, "lblE_FILLINGS");
            this.lblE_FILLINGS.BackColor = System.Drawing.Color.Silver;
            this.lblE_FILLINGS.Name = "lblE_FILLINGS";
            // 
            // btnE_FILLING_SALE
            // 
            resources.ApplyResources(this.btnE_FILLING_SALE, "btnE_FILLING_SALE");
            this.btnE_FILLING_SALE.BackColor = System.Drawing.Color.Transparent;
            this.btnE_FILLING_SALE.Image = ((System.Drawing.Image)(resources.GetObject("btnE_FILLING_SALE.Image")));
            this.btnE_FILLING_SALE.Name = "btnE_FILLING_SALE";
            this.btnE_FILLING_SALE.Selected = false;
            this.btnE_FILLING_SALE.Click += new System.EventHandler(this.btnE_FILLING_SALE_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel15, "panel15");
            this.panel15.Name = "panel15";
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel13, "panel13");
            this.panel13.Name = "panel13";
            // 
            // lblREPORTS
            // 
            this.lblREPORTS.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblREPORTS, "lblREPORTS");
            this.lblREPORTS.Image = global::EPower.Properties.Resources.icon_report;
            this.lblREPORTS.IsTitle = true;
            this.lblREPORTS.Name = "lblREPORTS";
            this.lblREPORTS.Selected = false;
            this.lblREPORTS.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.Transparent;
            this.panel16.Controls.Add(this.panel20);
            this.panel16.Controls.Add(this.lblREPORT_1);
            resources.ApplyResources(this.panel16, "panel16");
            this.panel16.Name = "panel16";
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.main);
            this.panel20.Controls.Add(this.panel17);
            this.panel20.Controls.Add(this.panel18);
            this.panel20.Controls.Add(this.panel19);
            resources.ApplyResources(this.panel20, "panel20");
            this.panel20.Name = "panel20";
            // 
            // main
            // 
            this.main.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.main, "main");
            this.main.Name = "main";
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel17, "panel17");
            this.panel17.Name = "panel17";
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel18, "panel18");
            this.panel18.Name = "panel18";
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel19, "panel19");
            this.panel19.Name = "panel19";
            // 
            // lblREPORT_1
            // 
            this.lblREPORT_1.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblREPORT_1, "lblREPORT_1");
            this.lblREPORT_1.Image = null;
            this.lblREPORT_1.IsTitle = true;
            this.lblREPORT_1.Name = "lblREPORT_1";
            this.lblREPORT_1.Selected = false;
            this.lblREPORT_1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PageReport
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "PageReport";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel16;
        private Panel panel20;
        private ExTabItem lblREPORT_1;
        private Panel panel12;
        private ExTabItem lblREPORTS;
        private SplitContainer splitContainer1;
        private Panel main;
        private Panel panel17;
        private Panel panel18;
        private Panel panel19;
        private Panel panel2;
        private Panel panel15;
        private Panel panel13;
        private FlowLayoutPanel panel1;
        private ExMenuItem btnREPORT_MONTHLY_OTHER_INCOME;
        private Label lblREPORT_SUMMARY;
        private ExMenuItem btnREPORT_YEARLY_POWER_USAGE;
        private Panel panel3;
        private ExMenuItem btnREPORT_TRIALBALANCE_DAILY;
        private ExMenuItem btnREPORT_TRIAL_BALANCE_CUSTOMER_DETAIL;
        private ExMenuItem btnREPORT_AGING;
        private ExMenuItem btnREPORT_DEPOSIT_PREPAYMENT;
        private ExMenuItem btnREPORT_AR_DETAIL;
        private ExMenuItem btnREPORT_CASH_DAILY;
        private ExMenuItem btnREPORT_MONTHLY_POWER_INCOME;
        private ExMenuItem btnREPORT_CASH_DAILY_SUMMARY;
        private ExMenuItem btnREPORT_CUSTOMER;
        private ExMenuItem btnREPORT_MONTHLY_DISCOUNT;
        private ExMenuItem btnREPORT_MONTHLY_POWER_USAGE;
        private ExMenuItem btnREPORT_CASH_RECEIVED;
        private ExMenuItem btnBLOCK_AND_RECONNECT;
        private ExMenuItem btnREPORT_BANK_TRANSACTION;
        private ExMenuItem btnREPORT_CUSTOMER_USAGE_SUMMARY;
        private ExMenuItem btnREPORT_RECURRING_SERVING;
        private ExMenuItem btnREPORT_QUARTERLY;
        private ExMenuItem btnREPORT_ANNUAL;
        private ExMenuItem btnREPORT_CUSTOMER_USAGE_STATISTIC;
        private ExMenuItem btnREPORT_METER;
        private Label lblE_FILLINGS;
        private ExMenuItem btnE_FILLING_SALE;
        private ExMenuItem btnSOLD_BY_CUSTOMER_TYPE;
        private ExMenuItem btnREPORT_CASH_COLLECTION;
        private ExMenuItem btnREPORT_AR_DETAIL_NEW;
        private ExMenuItem btnGENERAL_CUSTOMER_INFO;
    }
}
