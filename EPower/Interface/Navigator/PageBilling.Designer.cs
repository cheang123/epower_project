﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageBilling
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageBilling));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.lblCUSTOMER = new System.Windows.Forms.Label();
            this.btnMANAGE_CUSTOMER = new SoftTech.Component.ExMenuItem();
            this.btnActivate = new SoftTech.Component.ExMenuItem();
            this.btnBLOCK_CUSTOMER = new SoftTech.Component.ExMenuItem();
            this.btnUNBLOCK_CUSTOMER = new SoftTech.Component.ExMenuItem();
            this.btnMERG_CUSTOMER = new SoftTech.Component.ExMenuItem();
            this.lblBILLING_SETTING = new System.Windows.Forms.Label();
            this.btnPRICE = new SoftTech.Component.ExMenuItem();
            this.btnBILLING_CYCLE = new SoftTech.Component.ExMenuItem();
            this.btnSERVICE_TYPE = new SoftTech.Component.ExMenuItem();
            this.btnSERVICE = new SoftTech.Component.ExMenuItem();
            this.btnDISCOUNT = new SoftTech.Component.ExMenuItem();
            this.btnCONNECTION_FEE = new SoftTech.Component.ExMenuItem();
            this.btnDEPOSIT = new SoftTech.Component.ExMenuItem();
            this.lblBilling = new System.Windows.Forms.Label();
            this.btnRUN_BILL = new SoftTech.Component.ExMenuItem();
            this.btnPRINT_BILL = new SoftTech.Component.ExMenuItem();
            this.btnBILLING_HISTORY = new SoftTech.Component.ExMenuItem();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.btnBILLING = new SoftTech.Component.ExTabItem();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.main = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.btnBILLING_1 = new SoftTech.Component.ExTabItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel20.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            resources.ApplyResources(this.splitContainer1, "splitContainer1");
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel12);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel16);
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.panel2);
            this.panel12.Controls.Add(this.btnBILLING);
            resources.ApplyResources(this.panel12, "panel12");
            this.panel12.Name = "panel12";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.panel15);
            this.panel2.Controls.Add(this.panel13);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightGray;
            this.panel1.Controls.Add(this.lblCUSTOMER);
            this.panel1.Controls.Add(this.btnMANAGE_CUSTOMER);
            this.panel1.Controls.Add(this.btnActivate);
            this.panel1.Controls.Add(this.btnBLOCK_CUSTOMER);
            this.panel1.Controls.Add(this.btnUNBLOCK_CUSTOMER);
            this.panel1.Controls.Add(this.btnMERG_CUSTOMER);
            this.panel1.Controls.Add(this.lblBILLING_SETTING);
            this.panel1.Controls.Add(this.btnPRICE);
            this.panel1.Controls.Add(this.btnBILLING_CYCLE);
            this.panel1.Controls.Add(this.btnSERVICE_TYPE);
            this.panel1.Controls.Add(this.btnSERVICE);
            this.panel1.Controls.Add(this.btnDISCOUNT);
            this.panel1.Controls.Add(this.btnCONNECTION_FEE);
            this.panel1.Controls.Add(this.btnDEPOSIT);
            this.panel1.Controls.Add(this.lblBilling);
            this.panel1.Controls.Add(this.btnRUN_BILL);
            this.panel1.Controls.Add(this.btnPRINT_BILL);
            this.panel1.Controls.Add(this.btnBILLING_HISTORY);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // lblCUSTOMER
            // 
            resources.ApplyResources(this.lblCUSTOMER, "lblCUSTOMER");
            this.lblCUSTOMER.BackColor = System.Drawing.Color.Silver;
            this.lblCUSTOMER.Name = "lblCUSTOMER";
            // 
            // btnMANAGE_CUSTOMER
            // 
            resources.ApplyResources(this.btnMANAGE_CUSTOMER, "btnMANAGE_CUSTOMER");
            this.btnMANAGE_CUSTOMER.BackColor = System.Drawing.Color.Transparent;
            this.btnMANAGE_CUSTOMER.Image = ((System.Drawing.Image)(resources.GetObject("btnMANAGE_CUSTOMER.Image")));
            this.btnMANAGE_CUSTOMER.Name = "btnMANAGE_CUSTOMER";
            this.btnMANAGE_CUSTOMER.Selected = false;
            this.btnMANAGE_CUSTOMER.Click += new System.EventHandler(this.menuFind_Click);
            // 
            // btnActivate
            // 
            resources.ApplyResources(this.btnActivate, "btnActivate");
            this.btnActivate.BackColor = System.Drawing.Color.Transparent;
            this.btnActivate.Image = ((System.Drawing.Image)(resources.GetObject("btnActivate.Image")));
            this.btnActivate.Name = "btnActivate";
            this.btnActivate.Selected = false;
            this.btnActivate.Click += new System.EventHandler(this.menuActivate_Click);
            // 
            // btnBLOCK_CUSTOMER
            // 
            resources.ApplyResources(this.btnBLOCK_CUSTOMER, "btnBLOCK_CUSTOMER");
            this.btnBLOCK_CUSTOMER.BackColor = System.Drawing.Color.Transparent;
            this.btnBLOCK_CUSTOMER.Image = ((System.Drawing.Image)(resources.GetObject("btnBLOCK_CUSTOMER.Image")));
            this.btnBLOCK_CUSTOMER.Name = "btnBLOCK_CUSTOMER";
            this.btnBLOCK_CUSTOMER.Selected = false;
            this.btnBLOCK_CUSTOMER.Click += new System.EventHandler(this.manuBlock_Click);
            // 
            // btnUNBLOCK_CUSTOMER
            // 
            resources.ApplyResources(this.btnUNBLOCK_CUSTOMER, "btnUNBLOCK_CUSTOMER");
            this.btnUNBLOCK_CUSTOMER.BackColor = System.Drawing.Color.Transparent;
            this.btnUNBLOCK_CUSTOMER.Image = ((System.Drawing.Image)(resources.GetObject("btnUNBLOCK_CUSTOMER.Image")));
            this.btnUNBLOCK_CUSTOMER.Name = "btnUNBLOCK_CUSTOMER";
            this.btnUNBLOCK_CUSTOMER.Selected = false;
            this.btnUNBLOCK_CUSTOMER.Click += new System.EventHandler(this.menuConnection_Click);
            // 
            // btnMERG_CUSTOMER
            // 
            resources.ApplyResources(this.btnMERG_CUSTOMER, "btnMERG_CUSTOMER");
            this.btnMERG_CUSTOMER.BackColor = System.Drawing.Color.Transparent;
            this.btnMERG_CUSTOMER.Image = ((System.Drawing.Image)(resources.GetObject("btnMERG_CUSTOMER.Image")));
            this.btnMERG_CUSTOMER.Name = "btnMERG_CUSTOMER";
            this.btnMERG_CUSTOMER.Selected = false;
            this.btnMERG_CUSTOMER.Click += new System.EventHandler(this.btnMergeCustomer_Click);
            // 
            // lblBILLING_SETTING
            // 
            resources.ApplyResources(this.lblBILLING_SETTING, "lblBILLING_SETTING");
            this.lblBILLING_SETTING.BackColor = System.Drawing.Color.Silver;
            this.lblBILLING_SETTING.Name = "lblBILLING_SETTING";
            // 
            // btnPRICE
            // 
            resources.ApplyResources(this.btnPRICE, "btnPRICE");
            this.btnPRICE.BackColor = System.Drawing.Color.Transparent;
            this.btnPRICE.Image = ((System.Drawing.Image)(resources.GetObject("btnPRICE.Image")));
            this.btnPRICE.Name = "btnPRICE";
            this.btnPRICE.Selected = false;
            this.btnPRICE.Click += new System.EventHandler(this.menuPrice_Click);
            // 
            // btnBILLING_CYCLE
            // 
            resources.ApplyResources(this.btnBILLING_CYCLE, "btnBILLING_CYCLE");
            this.btnBILLING_CYCLE.BackColor = System.Drawing.Color.Transparent;
            this.btnBILLING_CYCLE.Image = ((System.Drawing.Image)(resources.GetObject("btnBILLING_CYCLE.Image")));
            this.btnBILLING_CYCLE.Name = "btnBILLING_CYCLE";
            this.btnBILLING_CYCLE.Selected = false;
            this.btnBILLING_CYCLE.Click += new System.EventHandler(this.menuBillingCycle_Click);
            // 
            // btnSERVICE_TYPE
            // 
            resources.ApplyResources(this.btnSERVICE_TYPE, "btnSERVICE_TYPE");
            this.btnSERVICE_TYPE.BackColor = System.Drawing.Color.Transparent;
            this.btnSERVICE_TYPE.Image = ((System.Drawing.Image)(resources.GetObject("btnSERVICE_TYPE.Image")));
            this.btnSERVICE_TYPE.Name = "btnSERVICE_TYPE";
            this.btnSERVICE_TYPE.Selected = false;
            this.btnSERVICE_TYPE.Click += new System.EventHandler(this.menuInvoiceItem_Click);
            // 
            // btnSERVICE
            // 
            resources.ApplyResources(this.btnSERVICE, "btnSERVICE");
            this.btnSERVICE.BackColor = System.Drawing.Color.Transparent;
            this.btnSERVICE.Image = ((System.Drawing.Image)(resources.GetObject("btnSERVICE.Image")));
            this.btnSERVICE.Name = "btnSERVICE";
            this.btnSERVICE.Selected = false;
            this.btnSERVICE.Click += new System.EventHandler(this.btnPayrollItem_Click);
            // 
            // btnDISCOUNT
            // 
            resources.ApplyResources(this.btnDISCOUNT, "btnDISCOUNT");
            this.btnDISCOUNT.BackColor = System.Drawing.Color.Transparent;
            this.btnDISCOUNT.Image = ((System.Drawing.Image)(resources.GetObject("btnDISCOUNT.Image")));
            this.btnDISCOUNT.Name = "btnDISCOUNT";
            this.btnDISCOUNT.Selected = false;
            this.btnDISCOUNT.Click += new System.EventHandler(this.btnDiscount_Click);
            // 
            // btnCONNECTION_FEE
            // 
            resources.ApplyResources(this.btnCONNECTION_FEE, "btnCONNECTION_FEE");
            this.btnCONNECTION_FEE.BackColor = System.Drawing.Color.Transparent;
            this.btnCONNECTION_FEE.Image = ((System.Drawing.Image)(resources.GetObject("btnCONNECTION_FEE.Image")));
            this.btnCONNECTION_FEE.Name = "btnCONNECTION_FEE";
            this.btnCONNECTION_FEE.Selected = false;
            this.btnCONNECTION_FEE.Click += new System.EventHandler(this.btnDeposit_Click);
            // 
            // btnDEPOSIT
            // 
            resources.ApplyResources(this.btnDEPOSIT, "btnDEPOSIT");
            this.btnDEPOSIT.BackColor = System.Drawing.Color.Transparent;
            this.btnDEPOSIT.Image = ((System.Drawing.Image)(resources.GetObject("btnDEPOSIT.Image")));
            this.btnDEPOSIT.Name = "btnDEPOSIT";
            this.btnDEPOSIT.Selected = false;
            this.btnDEPOSIT.Click += new System.EventHandler(this.btnDeposit_Click_1);
            // 
            // lblBilling
            // 
            resources.ApplyResources(this.lblBilling, "lblBilling");
            this.lblBilling.BackColor = System.Drawing.Color.Silver;
            this.lblBilling.Name = "lblBilling";
            // 
            // btnRUN_BILL
            // 
            resources.ApplyResources(this.btnRUN_BILL, "btnRUN_BILL");
            this.btnRUN_BILL.BackColor = System.Drawing.Color.Transparent;
            this.btnRUN_BILL.Image = ((System.Drawing.Image)(resources.GetObject("btnRUN_BILL.Image")));
            this.btnRUN_BILL.Name = "btnRUN_BILL";
            this.btnRUN_BILL.Selected = false;
            this.btnRUN_BILL.Click += new System.EventHandler(this.btnRunBill_Click);
            // 
            // btnPRINT_BILL
            // 
            resources.ApplyResources(this.btnPRINT_BILL, "btnPRINT_BILL");
            this.btnPRINT_BILL.BackColor = System.Drawing.Color.Transparent;
            this.btnPRINT_BILL.Image = ((System.Drawing.Image)(resources.GetObject("btnPRINT_BILL.Image")));
            this.btnPRINT_BILL.Name = "btnPRINT_BILL";
            this.btnPRINT_BILL.Selected = false;
            this.btnPRINT_BILL.Click += new System.EventHandler(this.btnPrintBill_Click);
            // 
            // btnBILLING_HISTORY
            // 
            resources.ApplyResources(this.btnBILLING_HISTORY, "btnBILLING_HISTORY");
            this.btnBILLING_HISTORY.BackColor = System.Drawing.Color.Transparent;
            this.btnBILLING_HISTORY.Image = ((System.Drawing.Image)(resources.GetObject("btnBILLING_HISTORY.Image")));
            this.btnBILLING_HISTORY.Name = "btnBILLING_HISTORY";
            this.btnBILLING_HISTORY.Selected = false;
            this.btnBILLING_HISTORY.Click += new System.EventHandler(this.btnRunBillingHistory_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel15, "panel15");
            this.panel15.Name = "panel15";
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel13, "panel13");
            this.panel13.Name = "panel13";
            // 
            // btnBILLING
            // 
            this.btnBILLING.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnBILLING, "btnBILLING");
            this.btnBILLING.Image = global::EPower.Properties.Resources.logo_customer;
            this.btnBILLING.IsTitle = true;
            this.btnBILLING.Name = "btnBILLING";
            this.btnBILLING.Selected = false;
            this.btnBILLING.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.Transparent;
            this.panel16.Controls.Add(this.panel20);
            this.panel16.Controls.Add(this.btnBILLING_1);
            resources.ApplyResources(this.panel16, "panel16");
            this.panel16.Name = "panel16";
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.main);
            this.panel20.Controls.Add(this.panel17);
            this.panel20.Controls.Add(this.panel18);
            this.panel20.Controls.Add(this.panel19);
            resources.ApplyResources(this.panel20, "panel20");
            this.panel20.Name = "panel20";
            // 
            // main
            // 
            this.main.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.main, "main");
            this.main.Name = "main";
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel17, "panel17");
            this.panel17.Name = "panel17";
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel18, "panel18");
            this.panel18.Name = "panel18";
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel19, "panel19");
            this.panel19.Name = "panel19";
            // 
            // btnBILLING_1
            // 
            this.btnBILLING_1.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnBILLING_1, "btnBILLING_1");
            this.btnBILLING_1.Image = null;
            this.btnBILLING_1.IsTitle = true;
            this.btnBILLING_1.Name = "btnBILLING_1";
            this.btnBILLING_1.Selected = false;
            this.btnBILLING_1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PageBilling
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "PageBilling";
            this.Load += new System.EventHandler(this.PageBilling_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel16;
        private Panel panel20;
        private ExTabItem btnBILLING_1;
        private Panel panel12;
        private ExMenuItem btnPRICE;
        private ExTabItem btnBILLING;
        private SplitContainer splitContainer1;
        private Panel main;
        private Panel panel17;
        private Panel panel18;
        private Panel panel19;
        private Panel panel2;
        private Panel panel15;
        private Panel panel13;
        private Panel panel3;
        private ExMenuItem btnBILLING_CYCLE;
        private FlowLayoutPanel panel1;
        private ExMenuItem btnSERVICE_TYPE;
        private ExMenuItem btnSERVICE;
        private ExMenuItem btnRUN_BILL;
        private Label lblBilling;
        private ExMenuItem btnPRINT_BILL;
        private ExMenuItem btnBILLING_HISTORY;
        private Label lblCUSTOMER;
        private ExMenuItem btnUNBLOCK_CUSTOMER;
        private ExMenuItem btnBLOCK_CUSTOMER;
        private ExMenuItem btnMANAGE_CUSTOMER;
        private ExMenuItem btnActivate;
        private Label lblBILLING_SETTING;
        private ExMenuItem btnDISCOUNT;
        private ExMenuItem btnCONNECTION_FEE;
        private ExMenuItem btnDEPOSIT;
        private ExMenuItem btnMERG_CUSTOMER;
    }
}
