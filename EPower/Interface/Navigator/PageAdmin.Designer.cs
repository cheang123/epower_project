﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageAdmin
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageAdmin));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnCompany = new SoftTech.Component.ExMenuItem();
            this.btnArea = new SoftTech.Component.ExMenuItem();
            this.btnTransformer = new SoftTech.Component.ExMenuItem();
            this.btnPOLE_AND_BOX = new SoftTech.Component.ExMenuItem();
            this.btnMETER_TYPE = new SoftTech.Component.ExMenuItem();
            this.btnMETER = new SoftTech.Component.ExMenuItem();
            this.btnBREAKER_TYPE = new SoftTech.Component.ExMenuItem();
            this.btnBREAKER = new SoftTech.Component.ExMenuItem();
            this.btnCUSTOMER_TYPE = new SoftTech.Component.ExMenuItem();
            this.btnEMPLOYEE = new SoftTech.Component.ExMenuItem();
            this.btnCASH_DRAWER = new SoftTech.Component.ExMenuItem();
            this.btnDEVICE = new SoftTech.Component.ExMenuItem();
            this.btnEQUIPMENT = new SoftTech.Component.ExMenuItem();
            this.btnSEQUENCE_FORMAT = new SoftTech.Component.ExMenuItem();
            this.lblBANK_TRANSACTION = new System.Windows.Forms.Label();
            this.btnBANK_DEPOSIT = new SoftTech.Component.ExMenuItem();
            this.lblSTOCK = new System.Windows.Forms.Label();
            this.btnSTOCK_ITEM = new SoftTech.Component.ExMenuItem();
            this.btnSTOCK = new SoftTech.Component.ExMenuItem();
            this.lblOTHERS = new System.Windows.Forms.Label();
            this.btnUTILITY = new SoftTech.Component.ExMenuItem();
            this.btnCUSTOMER_BATCH_UPDATE = new SoftTech.Component.ExMenuItem();
            this.btnPOLE_BOX_BATCH_UPDATE = new SoftTech.Component.ExMenuItem();
            this.btnFORMULA_CALCULATION = new SoftTech.Component.ExMenuItem();
            this.btnPAYMENT_SETTING = new SoftTech.Component.ExMenuItem();
            this.btnPAYMENT_METHOD = new SoftTech.Component.ExMenuItem();
            this.btnTAX = new SoftTech.Component.ExMenuItem();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.btnADMIN_TASKS = new SoftTech.Component.ExTabItem();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.main = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.lblADMIN_TASKS = new SoftTech.Component.ExTabItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel20.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            resources.ApplyResources(this.splitContainer1, "splitContainer1");
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel12);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel16);
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.panel2);
            this.panel12.Controls.Add(this.btnADMIN_TASKS);
            resources.ApplyResources(this.panel12, "panel12");
            this.panel12.Name = "panel12";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.panel15);
            this.panel2.Controls.Add(this.panel13);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // panel1
            // 
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.BackColor = System.Drawing.Color.LightGray;
            this.panel1.Controls.Add(this.btnCompany);
            this.panel1.Controls.Add(this.btnArea);
            this.panel1.Controls.Add(this.btnTransformer);
            this.panel1.Controls.Add(this.btnPOLE_AND_BOX);
            this.panel1.Controls.Add(this.btnMETER_TYPE);
            this.panel1.Controls.Add(this.btnMETER);
            this.panel1.Controls.Add(this.btnBREAKER_TYPE);
            this.panel1.Controls.Add(this.btnBREAKER);
            this.panel1.Controls.Add(this.btnCUSTOMER_TYPE);
            this.panel1.Controls.Add(this.btnEMPLOYEE);
            this.panel1.Controls.Add(this.btnCASH_DRAWER);
            this.panel1.Controls.Add(this.btnDEVICE);
            this.panel1.Controls.Add(this.btnEQUIPMENT);
            this.panel1.Controls.Add(this.btnSEQUENCE_FORMAT);
            this.panel1.Controls.Add(this.lblBANK_TRANSACTION);
            this.panel1.Controls.Add(this.btnBANK_DEPOSIT);
            this.panel1.Controls.Add(this.lblSTOCK);
            this.panel1.Controls.Add(this.btnSTOCK_ITEM);
            this.panel1.Controls.Add(this.btnSTOCK);
            this.panel1.Controls.Add(this.lblOTHERS);
            this.panel1.Controls.Add(this.btnUTILITY);
            this.panel1.Controls.Add(this.btnCUSTOMER_BATCH_UPDATE);
            this.panel1.Controls.Add(this.btnPOLE_BOX_BATCH_UPDATE);
            this.panel1.Controls.Add(this.btnFORMULA_CALCULATION);
            this.panel1.Controls.Add(this.btnPAYMENT_SETTING);
            this.panel1.Controls.Add(this.btnPAYMENT_METHOD);
            this.panel1.Controls.Add(this.btnTAX);
            this.panel1.Name = "panel1";
            // 
            // btnCompany
            // 
            resources.ApplyResources(this.btnCompany, "btnCompany");
            this.btnCompany.BackColor = System.Drawing.Color.Transparent;
            this.btnCompany.Image = ((System.Drawing.Image)(resources.GetObject("btnCompany.Image")));
            this.btnCompany.Name = "btnCompany";
            this.btnCompany.Selected = false;
            this.btnCompany.Click += new System.EventHandler(this.btnCompany_Click);
            // 
            // btnArea
            // 
            resources.ApplyResources(this.btnArea, "btnArea");
            this.btnArea.BackColor = System.Drawing.Color.Transparent;
            this.btnArea.Image = ((System.Drawing.Image)(resources.GetObject("btnArea.Image")));
            this.btnArea.Name = "btnArea";
            this.btnArea.Selected = false;
            this.btnArea.Click += new System.EventHandler(this.btnArea_Click);
            // 
            // btnTransformer
            // 
            this.btnTransformer.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnTransformer, "btnTransformer");
            this.btnTransformer.Image = ((System.Drawing.Image)(resources.GetObject("btnTransformer.Image")));
            this.btnTransformer.Name = "btnTransformer";
            this.btnTransformer.Selected = false;
            this.btnTransformer.Click += new System.EventHandler(this.btnTransformer_Click);
            // 
            // btnPOLE_AND_BOX
            // 
            resources.ApplyResources(this.btnPOLE_AND_BOX, "btnPOLE_AND_BOX");
            this.btnPOLE_AND_BOX.BackColor = System.Drawing.Color.Transparent;
            this.btnPOLE_AND_BOX.Image = ((System.Drawing.Image)(resources.GetObject("btnPOLE_AND_BOX.Image")));
            this.btnPOLE_AND_BOX.Name = "btnPOLE_AND_BOX";
            this.btnPOLE_AND_BOX.Selected = false;
            this.btnPOLE_AND_BOX.Click += new System.EventHandler(this.btnPole_Click);
            // 
            // btnMETER_TYPE
            // 
            resources.ApplyResources(this.btnMETER_TYPE, "btnMETER_TYPE");
            this.btnMETER_TYPE.BackColor = System.Drawing.Color.Transparent;
            this.btnMETER_TYPE.Image = ((System.Drawing.Image)(resources.GetObject("btnMETER_TYPE.Image")));
            this.btnMETER_TYPE.Name = "btnMETER_TYPE";
            this.btnMETER_TYPE.Selected = false;
            this.btnMETER_TYPE.Click += new System.EventHandler(this.btnMeterType_Click);
            // 
            // btnMETER
            // 
            resources.ApplyResources(this.btnMETER, "btnMETER");
            this.btnMETER.BackColor = System.Drawing.Color.Transparent;
            this.btnMETER.Image = global::EPower.Properties.Resources.bullet;
            this.btnMETER.Name = "btnMETER";
            this.btnMETER.Selected = false;
            this.btnMETER.Click += new System.EventHandler(this.btnMeter_Click);
            // 
            // btnBREAKER_TYPE
            // 
            resources.ApplyResources(this.btnBREAKER_TYPE, "btnBREAKER_TYPE");
            this.btnBREAKER_TYPE.BackColor = System.Drawing.Color.Transparent;
            this.btnBREAKER_TYPE.Image = global::EPower.Properties.Resources.bullet;
            this.btnBREAKER_TYPE.Name = "btnBREAKER_TYPE";
            this.btnBREAKER_TYPE.Selected = false;
            this.btnBREAKER_TYPE.Click += new System.EventHandler(this.btnCircuitBreakerType_Click);
            // 
            // btnBREAKER
            // 
            resources.ApplyResources(this.btnBREAKER, "btnBREAKER");
            this.btnBREAKER.BackColor = System.Drawing.Color.Transparent;
            this.btnBREAKER.Image = global::EPower.Properties.Resources.bullet;
            this.btnBREAKER.Name = "btnBREAKER";
            this.btnBREAKER.Selected = false;
            this.btnBREAKER.Click += new System.EventHandler(this.btnCircuitBreaker_Click);
            // 
            // btnCUSTOMER_TYPE
            // 
            resources.ApplyResources(this.btnCUSTOMER_TYPE, "btnCUSTOMER_TYPE");
            this.btnCUSTOMER_TYPE.BackColor = System.Drawing.Color.Transparent;
            this.btnCUSTOMER_TYPE.Image = ((System.Drawing.Image)(resources.GetObject("btnCUSTOMER_TYPE.Image")));
            this.btnCUSTOMER_TYPE.Name = "btnCUSTOMER_TYPE";
            this.btnCUSTOMER_TYPE.Selected = false;
            this.btnCUSTOMER_TYPE.Click += new System.EventHandler(this.btnCustomerType_Click);
            // 
            // btnEMPLOYEE
            // 
            resources.ApplyResources(this.btnEMPLOYEE, "btnEMPLOYEE");
            this.btnEMPLOYEE.BackColor = System.Drawing.Color.Transparent;
            this.btnEMPLOYEE.Image = global::EPower.Properties.Resources.bullet;
            this.btnEMPLOYEE.Name = "btnEMPLOYEE";
            this.btnEMPLOYEE.Selected = false;
            this.btnEMPLOYEE.Click += new System.EventHandler(this.btnCollector_Click);
            // 
            // btnCASH_DRAWER
            // 
            resources.ApplyResources(this.btnCASH_DRAWER, "btnCASH_DRAWER");
            this.btnCASH_DRAWER.BackColor = System.Drawing.Color.Transparent;
            this.btnCASH_DRAWER.Image = ((System.Drawing.Image)(resources.GetObject("btnCASH_DRAWER.Image")));
            this.btnCASH_DRAWER.Name = "btnCASH_DRAWER";
            this.btnCASH_DRAWER.Selected = false;
            this.btnCASH_DRAWER.Click += new System.EventHandler(this.btnCashDrawer_Click);
            // 
            // btnDEVICE
            // 
            resources.ApplyResources(this.btnDEVICE, "btnDEVICE");
            this.btnDEVICE.BackColor = System.Drawing.Color.Transparent;
            this.btnDEVICE.Image = global::EPower.Properties.Resources.bullet;
            this.btnDEVICE.Name = "btnDEVICE";
            this.btnDEVICE.Selected = false;
            this.btnDEVICE.Click += new System.EventHandler(this.btnDevice_Click);
            // 
            // btnEQUIPMENT
            // 
            resources.ApplyResources(this.btnEQUIPMENT, "btnEQUIPMENT");
            this.btnEQUIPMENT.BackColor = System.Drawing.Color.Transparent;
            this.btnEQUIPMENT.Image = global::EPower.Properties.Resources.bullet;
            this.btnEQUIPMENT.Name = "btnEQUIPMENT";
            this.btnEQUIPMENT.Selected = false;
            this.btnEQUIPMENT.Click += new System.EventHandler(this.btnEquipment_Click);
            // 
            // btnSEQUENCE_FORMAT
            // 
            resources.ApplyResources(this.btnSEQUENCE_FORMAT, "btnSEQUENCE_FORMAT");
            this.btnSEQUENCE_FORMAT.BackColor = System.Drawing.Color.Transparent;
            this.btnSEQUENCE_FORMAT.Image = global::EPower.Properties.Resources.bullet;
            this.btnSEQUENCE_FORMAT.Name = "btnSEQUENCE_FORMAT";
            this.btnSEQUENCE_FORMAT.Selected = false;
            this.btnSEQUENCE_FORMAT.Click += new System.EventHandler(this.btnSEQUENCE_FORMAT_Click);
            // 
            // lblBANK_TRANSACTION
            // 
            resources.ApplyResources(this.lblBANK_TRANSACTION, "lblBANK_TRANSACTION");
            this.lblBANK_TRANSACTION.BackColor = System.Drawing.Color.Silver;
            this.lblBANK_TRANSACTION.Name = "lblBANK_TRANSACTION";
            // 
            // btnBANK_DEPOSIT
            // 
            resources.ApplyResources(this.btnBANK_DEPOSIT, "btnBANK_DEPOSIT");
            this.btnBANK_DEPOSIT.BackColor = System.Drawing.Color.Transparent;
            this.btnBANK_DEPOSIT.Image = ((System.Drawing.Image)(resources.GetObject("btnBANK_DEPOSIT.Image")));
            this.btnBANK_DEPOSIT.Name = "btnBANK_DEPOSIT";
            this.btnBANK_DEPOSIT.Selected = false;
            this.btnBANK_DEPOSIT.Click += new System.EventHandler(this.btnBankDeposit_Click);
            // 
            // lblSTOCK
            // 
            resources.ApplyResources(this.lblSTOCK, "lblSTOCK");
            this.lblSTOCK.BackColor = System.Drawing.Color.Silver;
            this.lblSTOCK.Name = "lblSTOCK";
            // 
            // btnSTOCK_ITEM
            // 
            resources.ApplyResources(this.btnSTOCK_ITEM, "btnSTOCK_ITEM");
            this.btnSTOCK_ITEM.BackColor = System.Drawing.Color.Transparent;
            this.btnSTOCK_ITEM.Image = ((System.Drawing.Image)(resources.GetObject("btnSTOCK_ITEM.Image")));
            this.btnSTOCK_ITEM.Name = "btnSTOCK_ITEM";
            this.btnSTOCK_ITEM.Selected = false;
            this.btnSTOCK_ITEM.Click += new System.EventHandler(this.btnStockItem_Click);
            // 
            // btnSTOCK
            // 
            resources.ApplyResources(this.btnSTOCK, "btnSTOCK");
            this.btnSTOCK.BackColor = System.Drawing.Color.Transparent;
            this.btnSTOCK.Image = ((System.Drawing.Image)(resources.GetObject("btnSTOCK.Image")));
            this.btnSTOCK.Name = "btnSTOCK";
            this.btnSTOCK.Selected = false;
            this.btnSTOCK.Click += new System.EventHandler(this.btnManageStock_Click);
            // 
            // lblOTHERS
            // 
            resources.ApplyResources(this.lblOTHERS, "lblOTHERS");
            this.lblOTHERS.BackColor = System.Drawing.Color.Silver;
            this.lblOTHERS.Name = "lblOTHERS";
            // 
            // btnUTILITY
            // 
            resources.ApplyResources(this.btnUTILITY, "btnUTILITY");
            this.btnUTILITY.BackColor = System.Drawing.Color.Transparent;
            this.btnUTILITY.Image = ((System.Drawing.Image)(resources.GetObject("btnUTILITY.Image")));
            this.btnUTILITY.Name = "btnUTILITY";
            this.btnUTILITY.Selected = false;
            this.btnUTILITY.Click += new System.EventHandler(this.btnUtility_Click);
            // 
            // btnCUSTOMER_BATCH_UPDATE
            // 
            resources.ApplyResources(this.btnCUSTOMER_BATCH_UPDATE, "btnCUSTOMER_BATCH_UPDATE");
            this.btnCUSTOMER_BATCH_UPDATE.BackColor = System.Drawing.Color.Transparent;
            this.btnCUSTOMER_BATCH_UPDATE.Image = ((System.Drawing.Image)(resources.GetObject("btnCUSTOMER_BATCH_UPDATE.Image")));
            this.btnCUSTOMER_BATCH_UPDATE.Name = "btnCUSTOMER_BATCH_UPDATE";
            this.btnCUSTOMER_BATCH_UPDATE.Selected = false;
            this.btnCUSTOMER_BATCH_UPDATE.Click += new System.EventHandler(this.btnCustomerBatchUpdate_Click);
            // 
            // btnPOLE_BOX_BATCH_UPDATE
            // 
            resources.ApplyResources(this.btnPOLE_BOX_BATCH_UPDATE, "btnPOLE_BOX_BATCH_UPDATE");
            this.btnPOLE_BOX_BATCH_UPDATE.BackColor = System.Drawing.Color.Transparent;
            this.btnPOLE_BOX_BATCH_UPDATE.Image = ((System.Drawing.Image)(resources.GetObject("btnPOLE_BOX_BATCH_UPDATE.Image")));
            this.btnPOLE_BOX_BATCH_UPDATE.Name = "btnPOLE_BOX_BATCH_UPDATE";
            this.btnPOLE_BOX_BATCH_UPDATE.Selected = false;
            this.btnPOLE_BOX_BATCH_UPDATE.Click += new System.EventHandler(this.btnPOLE_BOX_BATCH_UPDATE_Click);
            // 
            // btnFORMULA_CALCULATION
            // 
            resources.ApplyResources(this.btnFORMULA_CALCULATION, "btnFORMULA_CALCULATION");
            this.btnFORMULA_CALCULATION.BackColor = System.Drawing.Color.Transparent;
            this.btnFORMULA_CALCULATION.Image = ((System.Drawing.Image)(resources.GetObject("btnFORMULA_CALCULATION.Image")));
            this.btnFORMULA_CALCULATION.Name = "btnFORMULA_CALCULATION";
            this.btnFORMULA_CALCULATION.Selected = false;
            this.btnFORMULA_CALCULATION.Click += new System.EventHandler(this.btnFormulaCalculation_Click);
            // 
            // btnPAYMENT_SETTING
            // 
            resources.ApplyResources(this.btnPAYMENT_SETTING, "btnPAYMENT_SETTING");
            this.btnPAYMENT_SETTING.BackColor = System.Drawing.Color.Transparent;
            this.btnPAYMENT_SETTING.Image = ((System.Drawing.Image)(resources.GetObject("btnPAYMENT_SETTING.Image")));
            this.btnPAYMENT_SETTING.Name = "btnPAYMENT_SETTING";
            this.btnPAYMENT_SETTING.Selected = false;
            this.btnPAYMENT_SETTING.Click += new System.EventHandler(this.btnPAYMENT_SETTING_Click);
            // 
            // btnPAYMENT_METHOD
            // 
            resources.ApplyResources(this.btnPAYMENT_METHOD, "btnPAYMENT_METHOD");
            this.btnPAYMENT_METHOD.BackColor = System.Drawing.Color.Transparent;
            this.btnPAYMENT_METHOD.Image = ((System.Drawing.Image)(resources.GetObject("btnPAYMENT_METHOD.Image")));
            this.btnPAYMENT_METHOD.Name = "btnPAYMENT_METHOD";
            this.btnPAYMENT_METHOD.Selected = false;
            this.btnPAYMENT_METHOD.Click += new System.EventHandler(this.btnPAYMENT_METHOD_Click);
            // 
            // btnTAX
            // 
            resources.ApplyResources(this.btnTAX, "btnTAX");
            this.btnTAX.BackColor = System.Drawing.Color.Transparent;
            this.btnTAX.Image = ((System.Drawing.Image)(resources.GetObject("btnTAX.Image")));
            this.btnTAX.Name = "btnTAX";
            this.btnTAX.Selected = false;
            this.btnTAX.Click += new System.EventHandler(this.btnTAX_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel15, "panel15");
            this.panel15.Name = "panel15";
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel13, "panel13");
            this.panel13.Name = "panel13";
            // 
            // btnADMIN_TASKS
            // 
            this.btnADMIN_TASKS.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnADMIN_TASKS, "btnADMIN_TASKS");
            this.btnADMIN_TASKS.Image = global::EPower.Properties.Resources.icon_admin;
            this.btnADMIN_TASKS.IsTitle = true;
            this.btnADMIN_TASKS.Name = "btnADMIN_TASKS";
            this.btnADMIN_TASKS.Selected = false;
            this.btnADMIN_TASKS.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.Transparent;
            this.panel16.Controls.Add(this.panel20);
            this.panel16.Controls.Add(this.lblADMIN_TASKS);
            resources.ApplyResources(this.panel16, "panel16");
            this.panel16.Name = "panel16";
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.main);
            this.panel20.Controls.Add(this.panel17);
            this.panel20.Controls.Add(this.panel18);
            this.panel20.Controls.Add(this.panel19);
            resources.ApplyResources(this.panel20, "panel20");
            this.panel20.Name = "panel20";
            // 
            // main
            // 
            this.main.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.main, "main");
            this.main.Name = "main";
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel17, "panel17");
            this.panel17.Name = "panel17";
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel18, "panel18");
            this.panel18.Name = "panel18";
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel19, "panel19");
            this.panel19.Name = "panel19";
            // 
            // lblADMIN_TASKS
            // 
            this.lblADMIN_TASKS.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblADMIN_TASKS, "lblADMIN_TASKS");
            this.lblADMIN_TASKS.Image = null;
            this.lblADMIN_TASKS.IsTitle = true;
            this.lblADMIN_TASKS.Name = "lblADMIN_TASKS";
            this.lblADMIN_TASKS.Selected = false;
            this.lblADMIN_TASKS.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PageAdmin
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "PageAdmin";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel16;
        private Panel panel20;
        private ExTabItem lblADMIN_TASKS;
        private Panel panel12;
        private ExMenuItem btnPOLE_AND_BOX;
        private ExMenuItem btnDEVICE;
        private ExTabItem btnADMIN_TASKS;
        private SplitContainer splitContainer1;
        private Panel main;
        private Panel panel17;
        private Panel panel18;
        private Panel panel19;
        private Panel panel2;
        private Panel panel15;
        private Panel panel13;
        private Panel panel3;
        private ExMenuItem btnArea;
        private ExMenuItem btnCASH_DRAWER;
        private ExMenuItem btnMETER;
        private FlowLayoutPanel panel1;
        private ExMenuItem btnMETER_TYPE;
        private ExMenuItem btnEMPLOYEE;
        private ExMenuItem btnCUSTOMER_TYPE;
        private ExMenuItem btnBREAKER;
        private ExMenuItem btnBREAKER_TYPE;
        private ExMenuItem btnEQUIPMENT;
        private ExMenuItem btnTransformer;
        private ExMenuItem btnUTILITY;
        private Label lblOTHERS;
        private ExMenuItem btnCompany;
        private ExMenuItem btnSTOCK;
        private Label lblSTOCK;
        private ExMenuItem btnSTOCK_ITEM;
        private ExMenuItem btnBANK_DEPOSIT;
        private Label lblBANK_TRANSACTION;
        private ExMenuItem btnCUSTOMER_BATCH_UPDATE;
        private ExMenuItem btnFORMULA_CALCULATION;
        private ExMenuItem btnPOLE_BOX_BATCH_UPDATE;
        private ExMenuItem btnSEQUENCE_FORMAT;
        private ExMenuItem btnPAYMENT_SETTING;
        private ExMenuItem btnPAYMENT_METHOD;
        private ExMenuItem btnTAX;
    }
}
