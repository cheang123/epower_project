﻿using EPower.Base.Logic;
using EPower.HB02.REST;
using EPower.Interface.BankPayment;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using Login = SoftTech.Security.Logic.Login;
using Process = System.Diagnostics.Process;

namespace EPower.Interface
{
    public partial class PagePostPaid : UserControl
    {
        public PagePostPaid()
        {
            InitializeComponent();
            ResourceHelper.ApplyResource(this);
            /*
             * Register Service
             */
            var url = Cryption.Cryption.DecryptString(Method.Utilities[Utility.HB02_SERVICE_URL]);
            new HB02_API(url, "ep", Method.Utilities[Utility.IR_H203_ACTIVATE_CODE]);
            //Assign permission
            this.btnRECEIVE_PAYMENT.Enabled = Login.IsAuthorized(Permission.POSTPAID_PAYMENT);
            this.btnPRINT_PAYMENT_RECEIPT.Enabled = Login.IsAuthorized(Permission.POSTPAID_REPRINTRECIEPT);
            this.btnVOID_PAYMENT.Enabled = Login.IsAuthorized(Permission.POSTPAID_CANCELPAYMENT);
            this.btnINPUT_USAGE.Enabled = Login.IsAuthorized(Permission.POSTPAID_INPUTUSAGE);
             
            this.btnDOWNLOAD_UPLOAD_VIA_WIRE.Enabled = Login.IsAuthorized(Permission.POSTPAID_IRREADER_COLLECTUSAGE);
            this.btnDOWNLOAD_UPLOAD_VIA_INTERNET.Visible = !string.IsNullOrEmpty(Method.Utilities[Utility.IR_H203_ACTIVATE_CODE]);
            this.btnIR_REGISTER_DIGITAL_METER.Enabled = Login.IsAuthorized(Permission.POSTPAID_IRREADER_REGISTERMETER);


            this.lblIR_RELAY_CONTROLLER.Enabled
                = this.btnSEND_AND_RECEIVE_CUSTOMER.Enabled
                = 
            this.lblIR_RELAY_CONTROLLER.Visible
                = this.btnSEND_AND_RECEIVE_CUSTOMER.Visible
                = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_ORDER_RELAY_METER]);
             

            this.btnGPRS_USAGE.Enabled = Login.IsAuthorized(Permission.POSTPAID_GPRS_USE);

            this.btnBANK_PAYMENT_HISTORY.Enabled = Login.IsAuthorized(Permission.POSTPAID_BANKPAYMENT_SETTLEMENT);
            this.btnBANK_PAYMENT_SUMMARY.Enabled = Login.IsAuthorized(Permission.POSTPAID_BANKPAYMENT_SUMMARY);


            this.lblBARCODE_USAGE.Visible =
            this.btnPRINT_BARCODE_USAGE.Visible =
            this.btnBARCODE_COLLECT_USAGE.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_BARCODE_COLLECTION]);


            this.btnMOBILE_USAGE.Visible =
            this.btnMOBILE_SEND_USAGE.Visible =
            this.btnMOBILE_RECEIVE_USAGE.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_MOBILE_COLLECTION]);

            this.btnBANK_PAYMENT_SUMMARY.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.BANK_PAYMENT_ENABLE]);

            this.lblGPRS.Visible =
            this.btnGPRS_USAGE.Visible = !string.IsNullOrEmpty(Method.Utilities[Utility.USAGE_SERVICE_URL]);

            lblAMR.Visible =
                btnAMR_USAGE.Visible =
                lblAMR.Enabled =
                btnAMR_USAGE.Enabled = !string.IsNullOrEmpty(Method.Utilities[Utility.AMR_ACCESS_KEY]);

            this.btnPAYMENT_EPOWER.Visible = !string.IsNullOrEmpty(Method.Utilities[Utility.B24_CUSTOMER_URL_PAYMENT]);
        }

        #region Show Page
        private Control currentPage = null;
        private Dictionary<Type, Control> pages = new Dictionary<Type, Control>();
        private void showPage(Type type, object sender)
        {
            if (!this.pages.ContainsKey(type))
            {
                this.pages[type] = (Control)Activator.CreateInstance(type);
                this.pages[type].Size = this.main.Size;
                if (this.pages[type] is Form)
                {
                    Form frm = (Form)this.pages[type];
                    frm.TopLevel = false;
                    frm.Visible = true;
                    frm.FormBorderStyle = FormBorderStyle.None;
                }
                this.main.Controls.Add(this.pages[type]);
                this.pages[type].Dock = DockStyle.Fill;
                ResourceHelper.ApplyResource(this.pages[type]);
            }
            if (this.currentPage != this.pages[type])
            {
                this.pages[type].Show();
                if (this.currentPage != null)
                {
                    this.currentPage.Hide();
                }
                this.currentPage = this.pages[type];
            }
            this.lblPOSTPAID.Text = ((Control)sender).Text;
        }
        #endregion ShowPage()


        private void btnReceivePayment_Click(object sender, EventArgs e)
        {
            btnRECEIVE_PAYMENT.Selected = false; 
            if (Base.Logic.Login.CurrentCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_RECEIVE_PAYMENT, string.Empty) == DialogResult.Yes)
                {
                    new DialogOpenCashDrawer().ShowDialog();
                }
                if (Base.Logic.Login.CurrentCashDrawer == null) 
                {
                    return;
                }
                else
                {
                    new DialogReceivePayment().ShowDialog();
                }
            }
            else
            {
                new DialogReceivePayment().ShowDialog();
            }
            PostingLogic.Instance.PostToPointerBg(DateTime.Now.Date, DateTime.Now.Date.AddDays(1).AddSeconds(-1));
        }

        private void btnCollectUsage_Click(object sender, EventArgs e)
        {
            btnINPUT_USAGE.Selected = false;
            new DialogCollectUsageManually().ShowDialog();
        }

        private void btnCollectUsageByDevice_Click(object sender, EventArgs e)
        {
            btnMOBILE_RECEIVE_USAGE.Selected = false;
            new DialogCollectUsageByDevice().ShowDialog();
        }
  
        private void exMenuItem1_Click(object sender, EventArgs e)
        {
            btnMOBILE_SEND_USAGE.Selected = false;
            new DialogImportDataToDevice().ShowDialog();
        }

        private void btnDOWNLOAD_UPLOAD_VIA_WIRE_Click(object sender, EventArgs e)
        {
            showPage(typeof(PageUsageWire), sender);
        }

        private void btnRegisterMeter_Click(object sender, EventArgs e)
        {
            new DialogMeterRegister().ShowDialog();
        }

        private void btnPaymentCancel_Click(object sender, EventArgs e)
        {
            btnVOID_PAYMENT.Selected = false;
            if (Base.Logic.Login.CurrentCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_RECEIVE_PAYMENT, string.Empty) == DialogResult.Yes)
                    new DialogOpenCashDrawer().ShowDialog();
                if (Base.Logic.Login.CurrentCashDrawer == null)
                    return;
                else
                    new DialogPaymentCancel().ShowDialog();
            }
            else
            {
                new DialogPaymentCancel().ShowDialog();
            }
            PostingLogic.Instance.PostToPointerBg(DateTime.Now.Date, DateTime.Now.Date.AddDays(1).AddSeconds(-1));
        }

        private void btnPrintReceipt_Click(object sender, EventArgs e)
        {
            btnPRINT_PAYMENT_RECEIPT.Selected = false;
            new DialogPrintReceipt().ShowDialog();
        }

        private void exMenuItem1_Click_1(object sender, EventArgs e)
        {
            btnPRINT_PAYMENT_RECEIPT.Selected = false;
            if (Base.Logic.Login.CurrentCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_RECEIVE_PAYMENT, string.Empty) == DialogResult.Yes)
                    new DialogOpenCashDrawer().ShowDialog();
                if (Base.Logic.Login.CurrentCashDrawer == null)
                    return;
                else
                    new DialogPrintReceipt().ShowDialog();
            }
            else
            {
                new DialogPrintReceipt().ShowDialog();
                //new DialogArea(GeneralProcess .Insert,new TBL_AREA()).ShowDialog();
            }
        }

        private void btnImportUsageToIRReader_Click(object sender, EventArgs e)
        {
            new DialogImportDataToIRReader().ShowDialog();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            new DialogExport().ShowDialog();
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            if (Base.Logic.Login.CurrentCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_RECEIVE_PAYMENT, string.Empty) == DialogResult.Yes)
                    new DialogOpenCashDrawer().ShowDialog();
                if (Base.Logic.Login.CurrentCashDrawer == null)
                    return;
                else
                    new DialogImport().ShowDialog();
            }
            else
            {
                new DialogImport().ShowDialog();
            }
        }

        private void btnCollectByMetrologic_Click(object sender, EventArgs e)
        {
            new DialogCollectUsageMetrologic().ShowDialog();
        }

        private void btnPrintBarcode_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PagePrintBarcode), sender);
        }

        private void btnBankPaymentHistory_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageHistory), sender);
            ((PageHistory)this.currentPage).LoadData();
        }

        private void btnBankPaymentSummary_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageSummary), sender);
            ((PageSummary)this.currentPage).LoadData();
        }

        private void btnUsageGPRS_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageUsageGPRS), sender);
        }

        private void btnIrRelayController_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageIrRelayMeterController), sender);
        }

        private void btnAMRSystem_Click(object sender, EventArgs e)
        {
            var path = Application.StartupPath + "\\EPower.AMR\\EPower.AMR.exe";
            var connectionString = Method.GetConnectionString(Settings.Default.CONNECTION);
            var userLogId = Login.CurrentLogin.LOGIN_ID;
            ProcessStartInfo pi = new ProcessStartInfo(path,connectionString+" "+userLogId.ToString()); 
            Process.Start(pi);
        }

        private void btnPAYMENT_EPOWER_Click(object sender, EventArgs e)
        {
            showPage(typeof(PageB24), sender);
        }

        private void btnDOWNLOAD_UPLOAD_INTERNET_Click(object sender, EventArgs e)
        {
            showPage(typeof(PageUsageH203), sender);
        }

        private void btnSEND_AND_RECEIVE_TAMPER_Click(object sender, EventArgs e)
        {
            showPage(typeof(PageTamper), sender);
            ((PageTamper)this.currentPage).BindData();
        }

        private void btnREPORT_CASH_COLLECTION_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportPayment), sender);
        }
    }
}
