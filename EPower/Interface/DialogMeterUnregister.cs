﻿using EPower.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogMeterUnregister : ExDialog
    {
        #region Constructor
        public DialogMeterUnregister()
        {
            InitializeComponent();
            //Runner.RunNewThread(()=>
            //{
            try
            {
                RegisterMeter.CheckUnregisterMeter();
            }
            catch (Exception)
            {
                MsgBox.ShowInformation(string.Format(Resources.DUPLICATE_METER_CODE, Resources.CONTACT_SUPPORT), Resources.WARNING);
            }
            //});
            this.bindMeterUnregister();
        }
        #endregion Constructor

        #region Method 
        void bindMeterUnregister()
        {
            DBDataContext.Db = null;
            var cusM = from m in DBDataContext.Db.TBL_METERs
                       join tmcm in
                           (from cm in DBDataContext.Db.TBL_CUSTOMER_METERs
                            join c in DBDataContext.Db.TBL_CUSTOMERs on cm.CUSTOMER_ID equals c.CUSTOMER_ID
                            join a in DBDataContext.Db.TBL_AREAs on c.AREA_ID equals a.AREA_ID
                            join b in DBDataContext.Db.TBL_BOXes on cm.BOX_ID equals b.BOX_ID
                            where cm.IS_ACTIVE
                            select new
                            {
                                c.CUSTOMER_CODE,
                                cm.METER_ID,
                                a.AREA_CODE,
                                CUS_NAME = c.LAST_NAME_KH + " " + c.FIRST_NAME_KH,
                                b.BOX_CODE
                            }) on m.METER_ID equals tmcm.METER_ID into cusMeter
                       where m.REGISTER_CODE == string.Empty
                       from cm in cusMeter.DefaultIfEmpty()
                       orderby m.METER_CODE
                       select new
                       {
                           CUSTOMER_CODE = cm.CUSTOMER_CODE,
                           m.METER_ID,
                           m.METER_CODE,
                           AREA_CODE = cm == null ? string.Empty : cm.AREA_CODE,
                           CUS_NAME = cm == null ? string.Empty : cm.CUS_NAME,
                           BOX_CODE = cm == null ? string.Empty : cm.BOX_CODE,
                           m.IS_DIGITAL,
                           METER = m
                       };
            dgv.DataSource = cusM;

            this.lblCOUNT_.Text = cusM.Count().ToString();
        }

        void bindMeterRegistered(IEnumerable<TBL_METER> listMeter)
        {
            var mm = from m in listMeter
                     join tmcm in
                         (from cm in DBDataContext.Db.TBL_CUSTOMER_METERs
                          join c in DBDataContext.Db.TBL_CUSTOMERs on cm.CUSTOMER_ID equals c.CUSTOMER_ID
                          join a in DBDataContext.Db.TBL_AREAs on c.AREA_ID equals a.AREA_ID
                          join b in DBDataContext.Db.TBL_BOXes on cm.BOX_ID equals b.BOX_ID
                          where cm.IS_ACTIVE
                          select new
                          {
                              cm.METER_ID,
                              a.AREA_CODE,
                              CUS_NAME = c.LAST_NAME_KH + " " + c.FIRST_NAME_KH,
                              b.BOX_CODE
                          }).ToList() on m.METER_ID equals tmcm.METER_ID into cusMeter
                     from cm in cusMeter.DefaultIfEmpty()
                     select new
                     {
                         m.METER_ID,
                         m.METER_CODE,
                         AREA_CODE = cm == null ? string.Empty : cm.AREA_CODE,
                         CUS_NAME = cm == null ? string.Empty : cm.CUS_NAME,
                         BOX_CODE = cm == null ? string.Empty : cm.BOX_CODE
                     };
            lblCOUNT_.Text = mm.Count().ToString();
            //dgv.DataSource = mm.ToList(); 
        }
        #endregion Method

        #region Event
        private void btnRegister_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count == 0)
            {
                return;
            }
            var lstRegister = this.dgv.SelectedRows.Cast<DataGridViewRow>().Select(x => (TBL_METER)x.Cells[this.METER.Name].Value).ToList();
            /*
             * Chuck 300 meters per process...
             */
            Runner.RunNewThread(delegate ()
            {
                try
                {
                    bool success = false;
                    var total = lstRegister.Count();

                    var i = 0;
                    foreach (var meters in lstRegister.ToArray().Split(300))
                    {
                        i += meters.Count();
                        Runner.Instance.Text = string.Format(Properties.Resources.MS_REGISTERING_METER, i * 100 / total);
                        success = CRMService.Intance.RegisterMeter(meters.ToList());
                    }
                }
                catch (Exception ex)
                {
                    MsgBox.LogError(ex);
                    throw new Exception(string.Format(Base.Properties.Resources.YOU_CANNOT_PROCESS, "", Base.Properties.Resources.REGISTER));
                }
            });
            bindMeterUnregister();
        }

        private void DialogMeterUnregister_Load(object sender, EventArgs e)
        {
            //   bindMeterUnregister();
        }

        private void btnNewMeter_Click(object sender, EventArgs e)
        {
            try
            {
                var x = CRMService.Intance.GetNewMeters();
                if (x != null)
                {
                    new DialogMeterRegisterNewMeter(x).ShowDialog();
                    bindMeterUnregister();
                }
            }
            catch (Exception exp)
            {
                MsgBox.LogError(exp);
                MsgBox.ShowWarning((string.Format(Base.Properties.Resources.YOU_CANNOT_PROCESS, "", Base.Properties.Resources.DOWNLOAD_METER)), Resources.WARNING);
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            CrystalReportHelper ch = ViewReportMeterUnregister();
            ch.ViewReport("");
        }

        public CrystalReportHelper ViewReportMeterUnregister()
        {
            return new CrystalReportHelper("ReportMeterUnregister.rpt");
        }
        #endregion Event



    }
}
