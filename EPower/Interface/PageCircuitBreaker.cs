﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PageCircuitBreaker : Form
    {
        #region Data
        int CircuitBrakerTypeId = 0;
        public int BrakerTypeID
        {
            get { return CircuitBrakerTypeId; }
            private set { CircuitBrakerTypeId = value; }
        }

        int statusId = 0;
        public int StatusID
        {
            get { return statusId; }
            private set { statusId = value; }
        }
        
        public TBL_CIRCUIT_BREAKER CircuitBraker
        {
            get
            {
                TBL_CIRCUIT_BREAKER objCircuitBraker = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    try
                    {
                        int intCircuitBrakerID = (int)dgv.SelectedRows[0].Cells["BREAKER_ID"].Value;
                        objCircuitBraker = DBDataContext.Db.TBL_CIRCUIT_BREAKERs.FirstOrDefault(x => x.BREAKER_ID == intCircuitBrakerID);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objCircuitBraker;
            }
        }
        #endregion

        #region Method
        public void loadBreakerType()
        {
            int tempBrakID = BrakerTypeID;
            DataTable dt = (from a in DBDataContext.Db.TBL_CIRCUIT_BREAKER_TYPEs
                            where a.IS_ACTIVE
                            select a)._ToDataTable();
            //Create Row Templet
            DataRow dr = dt.NewRow();
            dr["BREAKER_TYPE_ID"] = 0;
            dr["BREAKER_TYPE_CODE"] = Resources.ALL_TYPE;
            dt.Rows.InsertAt(dr, 0);
            UIHelper.SetDataSourceToComboBox(cboBrakerType, dt, "BREAKER_TYPE_ID", "BREAKER_TYPE_CODE");
            cboBrakerType.SelectedValue = tempBrakID;
        }

        public void loadBrakerStatus()
        {
            int tempStatus = StatusID;
            DataTable dt = (from a in DBDataContext.Db.TLKP_BRAKER_STATUS
                            select a)._ToDataTable();
            //Create Row Templet
            DataRow dr = dt.NewRow();
            dr["STATUS_ID"] = 0;
            dr["STATUS_NAME"] = Resources.ALL_STATUS;
            dt.Rows.InsertAt(dr, 0);
            UIHelper.SetDataSourceToComboBox(cboStatus, dt, "STATUS_ID", "STATUS_NAME");
            cboStatus.SelectedValue = tempStatus;
        }
        #endregion

        #region Constructor
        public PageCircuitBreaker()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            txt_QuickSearch(null, null);            
        }
        #endregion

        #region Operation
        private void btnNew_Click(object sender, EventArgs e)
        {            
            TBL_CIRCUIT_BREAKER objCircuitBraker = new TBL_CIRCUIT_BREAKER() { BREAKER_CODE = "" };
            if (cboBrakerType.SelectedIndex != -1)
            {
                objCircuitBraker.BREAKER_TYPE_ID = (int)cboBrakerType.SelectedValue;
            }
            if (cboStatus.SelectedIndex != -1)
            {
                objCircuitBraker.STATUS_ID = (int)cboStatus.SelectedValue;
            }
            
            DialogCircuitBreaker dig = new DialogCircuitBreaker(GeneralProcess.Insert, objCircuitBraker);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.CircuitBraker.BREAKER_ID);
            }
        }


        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                DialogCircuitBreaker dig = new DialogCircuitBreaker(GeneralProcess.Update, CircuitBraker);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.CircuitBraker.BREAKER_ID);
                }
            }
        }

        private void txt_QuickSearch(object sender, EventArgs e)
        {
            try
            {                
                if (cboBrakerType.SelectedIndex !=-1)
                {
                    CircuitBrakerTypeId = (int)cboBrakerType.SelectedValue;
                }
                if (cboStatus.SelectedIndex != -1)
                {
                    statusId = (int)cboStatus.SelectedValue;
                }

                dgv.DataSource = (from CircuitBraker in DBDataContext.Db.TBL_CIRCUIT_BREAKERs
                                 join CircuitBrakerType in DBDataContext.Db.TBL_CIRCUIT_BREAKER_TYPEs on CircuitBraker.BREAKER_TYPE_ID equals CircuitBrakerType.BREAKER_TYPE_ID
                                 join status in DBDataContext.Db.TLKP_BRAKER_STATUS on CircuitBraker.STATUS_ID equals status.STATUS_ID
                                 join cm in
                                     (from m in DBDataContext.Db.TBL_CIRCUIT_BREAKERs
                                      join cm in DBDataContext.Db.TBL_CUSTOMER_METERs on m.BREAKER_ID equals cm.BREAKER_ID
                                      join c in DBDataContext.Db.TBL_CUSTOMERs on cm.CUSTOMER_ID equals c.CUSTOMER_ID
                                      where cm.IS_ACTIVE
                                      select new
                                      {
                                          m.BREAKER_ID,
                                          CUSTOMER_NAME = c.LAST_NAME_KH + " " + c.FIRST_NAME_KH
                                      }) on CircuitBraker.BREAKER_ID equals cm.BREAKER_ID into cms
                                 from x in cms.DefaultIfEmpty()
                                 where
                                 (CircuitBrakerTypeId == 0 || CircuitBrakerType.BREAKER_TYPE_ID == CircuitBrakerTypeId) &&
                                 (statusId == 0 || status.STATUS_ID == statusId) &&
                                 ((x == null ? "" : x.CUSTOMER_NAME)+CircuitBraker.BREAKER_CODE).ToLower().Contains(txtQuickSearch.Text.ToLower().Trim())  
                                 select new
                                 {
                                     CircuitBraker.BREAKER_ID,
                                     CircuitBraker.BREAKER_CODE,
                                     CircuitBrakerType.BREAKER_TYPE_CODE,
                                     status.STATUS_NAME,
                                     CUSTOMER_NAME = x==null?"":x.CUSTOMER_NAME
                                 }).Take(200);
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        /// <summary>
        /// Change current keyboard layout to khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }       
        #endregion        

       
        
    }
}
