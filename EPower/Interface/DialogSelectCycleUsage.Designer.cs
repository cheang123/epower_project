﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogSelectCycleUsage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogSelectCycleUsage));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.CYCLE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SELECT_ = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.CYCLE_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.START_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.END_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MONTH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.dgv);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.dgv, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CYCLE_ID,
            this.SELECT_,
            this.CYCLE_NAME,
            this.START_DATE,
            this.END_DATE,
            this.MONTH});
            this.dgv.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellClick);
            this.dgv.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellEndEdit);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // CYCLE_ID
            // 
            this.CYCLE_ID.DataPropertyName = "CYCLE_ID";
            resources.ApplyResources(this.CYCLE_ID, "CYCLE_ID");
            this.CYCLE_ID.Name = "CYCLE_ID";
            this.CYCLE_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CYCLE_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // SELECT_
            // 
            this.SELECT_.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.SELECT_.DataPropertyName = "SELECT_";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.NullValue = false;
            this.SELECT_.DefaultCellStyle = dataGridViewCellStyle2;
            this.SELECT_.FillWeight = 75F;
            resources.ApplyResources(this.SELECT_, "SELECT_");
            this.SELECT_.Name = "SELECT_";
            // 
            // CYCLE_NAME
            // 
            this.CYCLE_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CYCLE_NAME.DataPropertyName = "CYCLE_NAME";
            resources.ApplyResources(this.CYCLE_NAME, "CYCLE_NAME");
            this.CYCLE_NAME.Name = "CYCLE_NAME";
            this.CYCLE_NAME.ReadOnly = true;
            // 
            // START_DATE
            // 
            this.START_DATE.DataPropertyName = "START_DATE";
            dataGridViewCellStyle3.Format = "dd - MM - yyyy";
            this.START_DATE.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.START_DATE, "START_DATE");
            this.START_DATE.Name = "START_DATE";
            this.START_DATE.ReadOnly = true;
            this.START_DATE.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // END_DATE
            // 
            this.END_DATE.DataPropertyName = "END_DATE";
            dataGridViewCellStyle4.Format = "dd - MM - yyyy";
            this.END_DATE.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.END_DATE, "END_DATE");
            this.END_DATE.Name = "END_DATE";
            // 
            // MONTH
            // 
            this.MONTH.DataPropertyName = "BILLING_MONTH";
            dataGridViewCellStyle5.Format = "MM - yyyy";
            this.MONTH.DefaultCellStyle = dataGridViewCellStyle5;
            resources.ApplyResources(this.MONTH, "MONTH");
            this.MONTH.Name = "MONTH";
            // 
            // DialogSelectCycleUsage
            // 
            resources.ApplyResources(this, "$this");
            this.Name = "DialogSelectCycleUsage";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DialogSelectAreaUsage_FormClosing);
            this.Load += new System.EventHandler(this.DialogSelectCycleUsage_Load);
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataGridView dgv;
        private ExButton btnCLOSE;
        private DataGridViewTextBoxColumn CYCLE_ID;
        private DataGridViewCheckBoxColumn SELECT_;
        private DataGridViewTextBoxColumn CYCLE_NAME;
        private DataGridViewTextBoxColumn START_DATE;
        private DataGridViewTextBoxColumn END_DATE;
        private DataGridViewTextBoxColumn MONTH;
    }
}
