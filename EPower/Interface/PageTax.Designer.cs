﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageTax
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageTax));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            this.panel1 = new System.Windows.Forms.Panel();
            this.searchControl = new DevExpress.XtraEditors.SearchControl();
            this.dgv = new DevExpress.XtraGrid.GridControl();
            this.dgvPaymentMethod = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTAX_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTAX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCOMPUTATION = new DevExpress.XtraGrid.Columns.GridColumn();
            this.resComputations = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colACCOUNT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.resAccounts = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colTAX_AMOUNT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.resCurrencies = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colIS_ACTIVE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCREATE_BY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCREATE_ON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnREMOVE = new SoftTech.Component.ExButton();
            this.btnADD = new SoftTech.Component.ExButton();
            this.btnEDIT = new SoftTech.Component.ExButton();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchControl.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPaymentMethod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resComputations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resAccounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resCurrencies)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.searchControl);
            this.panel1.Controls.Add(this.btnREMOVE);
            this.panel1.Controls.Add(this.btnADD);
            this.panel1.Controls.Add(this.btnEDIT);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // searchControl
            // 
            this.searchControl.Client = this.dgv;
            resources.ApplyResources(this.searchControl, "searchControl");
            this.searchControl.Name = "searchControl";
            this.searchControl.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("searchControl.Properties.Appearance.Font")));
            this.searchControl.Properties.Appearance.Options.UseFont = true;
            this.searchControl.Properties.AppearanceDisabled.Font = ((System.Drawing.Font)(resources.GetObject("searchControl.Properties.AppearanceDisabled.Font")));
            this.searchControl.Properties.AppearanceDisabled.Options.UseFont = true;
            this.searchControl.Properties.AppearanceDropDown.Font = ((System.Drawing.Font)(resources.GetObject("searchControl.Properties.AppearanceDropDown.Font")));
            this.searchControl.Properties.AppearanceDropDown.Options.UseFont = true;
            this.searchControl.Properties.AppearanceFocused.Font = ((System.Drawing.Font)(resources.GetObject("searchControl.Properties.AppearanceFocused.Font")));
            this.searchControl.Properties.AppearanceFocused.Options.UseFont = true;
            this.searchControl.Properties.AppearanceItemDisabled.Font = ((System.Drawing.Font)(resources.GetObject("searchControl.Properties.AppearanceItemDisabled.Font")));
            this.searchControl.Properties.AppearanceItemDisabled.Options.UseFont = true;
            this.searchControl.Properties.AppearanceItemHighlight.Font = ((System.Drawing.Font)(resources.GetObject("searchControl.Properties.AppearanceItemHighlight.Font")));
            this.searchControl.Properties.AppearanceItemHighlight.Options.UseFont = true;
            this.searchControl.Properties.AppearanceItemSelected.Font = ((System.Drawing.Font)(resources.GetObject("searchControl.Properties.AppearanceItemSelected.Font")));
            this.searchControl.Properties.AppearanceItemSelected.Options.UseFont = true;
            this.searchControl.Properties.AppearanceReadOnly.Font = ((System.Drawing.Font)(resources.GetObject("searchControl.Properties.AppearanceReadOnly.Font")));
            this.searchControl.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.searchControl.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Repository.ClearButton(),
            new DevExpress.XtraEditors.Repository.SearchButton()});
            this.searchControl.Properties.Client = this.dgv;
            this.searchControl.Properties.DropDownRows = 10;
            this.searchControl.Properties.Padding = new System.Windows.Forms.Padding(1);
            // 
            // dgv
            // 
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.EmbeddedNavigator.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("dgv.EmbeddedNavigator.Appearance.Font")));
            this.dgv.EmbeddedNavigator.Appearance.Options.UseFont = true;
            this.dgv.MainView = this.dgvPaymentMethod;
            this.dgv.Name = "dgv";
            this.dgv.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.resAccounts,
            this.resCurrencies,
            this.resComputations});
            this.dgv.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.dgvPaymentMethod});
            // 
            // dgvPaymentMethod
            // 
            this.dgvPaymentMethod.Appearance.ColumnFilterButton.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.ColumnFilterButton.Font")));
            this.dgvPaymentMethod.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.ColumnFilterButtonActive.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.ColumnFilterButtonActive.Font")));
            this.dgvPaymentMethod.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.CustomizationFormHint.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.CustomizationFormHint.Font")));
            this.dgvPaymentMethod.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.DetailTip.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.DetailTip.Font")));
            this.dgvPaymentMethod.Appearance.DetailTip.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.Empty.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.Empty.Font")));
            this.dgvPaymentMethod.Appearance.Empty.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.EvenRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.EvenRow.Font")));
            this.dgvPaymentMethod.Appearance.EvenRow.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.FilterCloseButton.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.FilterCloseButton.Font")));
            this.dgvPaymentMethod.Appearance.FilterCloseButton.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.FilterPanel.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.FilterPanel.Font")));
            this.dgvPaymentMethod.Appearance.FilterPanel.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.FixedLine.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.FixedLine.Font")));
            this.dgvPaymentMethod.Appearance.FixedLine.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.FocusedCell.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.FocusedCell.Font")));
            this.dgvPaymentMethod.Appearance.FocusedCell.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.FocusedRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.FocusedRow.Font")));
            this.dgvPaymentMethod.Appearance.FocusedRow.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.FooterPanel.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.FooterPanel.Font")));
            this.dgvPaymentMethod.Appearance.FooterPanel.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.GroupButton.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.GroupButton.Font")));
            this.dgvPaymentMethod.Appearance.GroupButton.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.GroupFooter.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.GroupFooter.Font")));
            this.dgvPaymentMethod.Appearance.GroupFooter.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.GroupPanel.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.GroupPanel.Font")));
            this.dgvPaymentMethod.Appearance.GroupPanel.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.GroupRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.GroupRow.Font")));
            this.dgvPaymentMethod.Appearance.GroupRow.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.HeaderPanel.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.HeaderPanel.Font")));
            this.dgvPaymentMethod.Appearance.HeaderPanel.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.HideSelectionRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.HideSelectionRow.Font")));
            this.dgvPaymentMethod.Appearance.HideSelectionRow.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.HorzLine.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.HorzLine.Font")));
            this.dgvPaymentMethod.Appearance.HorzLine.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.HotTrackedRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.HotTrackedRow.Font")));
            this.dgvPaymentMethod.Appearance.HotTrackedRow.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.OddRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.OddRow.Font")));
            this.dgvPaymentMethod.Appearance.OddRow.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.Preview.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.Preview.Font")));
            this.dgvPaymentMethod.Appearance.Preview.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.Row.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.Row.Font")));
            this.dgvPaymentMethod.Appearance.Row.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.RowSeparator.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.RowSeparator.Font")));
            this.dgvPaymentMethod.Appearance.RowSeparator.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.SelectedRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.SelectedRow.Font")));
            this.dgvPaymentMethod.Appearance.SelectedRow.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.TopNewRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.TopNewRow.Font")));
            this.dgvPaymentMethod.Appearance.TopNewRow.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.VertLine.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.VertLine.Font")));
            this.dgvPaymentMethod.Appearance.VertLine.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.ViewCaption.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.ViewCaption.Font")));
            this.dgvPaymentMethod.Appearance.ViewCaption.Options.UseFont = true;
            this.dgvPaymentMethod.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTAX_ID,
            this.colTAX,
            this.colCOMPUTATION,
            this.colACCOUNT,
            this.colTAX_AMOUNT,
            this.colIS_ACTIVE,
            this.colCREATE_BY,
            this.colCREATE_ON});
            this.dgvPaymentMethod.GridControl = this.dgv;
            this.dgvPaymentMethod.Name = "dgvPaymentMethod";
            this.dgvPaymentMethod.OptionsBehavior.Editable = false;
            this.dgvPaymentMethod.OptionsBehavior.ReadOnly = true;
            this.dgvPaymentMethod.OptionsView.ShowGroupPanel = false;
            this.dgvPaymentMethod.OptionsView.ShowIndicator = false;
            // 
            // colTAX_ID
            // 
            resources.ApplyResources(this.colTAX_ID, "colTAX_ID");
            this.colTAX_ID.FieldName = "TAX_ID";
            this.colTAX_ID.Name = "colTAX_ID";
            this.colTAX_ID.OptionsColumn.ReadOnly = true;
            // 
            // colTAX
            // 
            resources.ApplyResources(this.colTAX, "colTAX");
            this.colTAX.FieldName = "TAX_NAME";
            this.colTAX.Name = "colTAX";
            this.colTAX.OptionsColumn.ReadOnly = true;
            // 
            // colCOMPUTATION
            // 
            resources.ApplyResources(this.colCOMPUTATION, "colCOMPUTATION");
            this.colCOMPUTATION.ColumnEdit = this.resComputations;
            this.colCOMPUTATION.FieldName = "COMPUTATION";
            this.colCOMPUTATION.Name = "colCOMPUTATION";
            this.colCOMPUTATION.OptionsColumn.ReadOnly = true;
            // 
            // resComputations
            // 
            this.resComputations.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("resComputations.Appearance.Font")));
            this.resComputations.Appearance.Options.UseFont = true;
            this.resComputations.AppearanceFocused.Font = ((System.Drawing.Font)(resources.GetObject("resComputations.AppearanceFocused.Font")));
            this.resComputations.AppearanceFocused.Options.UseFont = true;
            this.resComputations.AppearanceReadOnly.Font = ((System.Drawing.Font)(resources.GetObject("resComputations.AppearanceReadOnly.Font")));
            this.resComputations.AppearanceReadOnly.Options.UseFont = true;
            resources.ApplyResources(this.resComputations, "resComputations");
            this.resComputations.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("resComputations.Buttons"))))});
            this.resComputations.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("resComputations.Columns"), resources.GetString("resComputations.Columns1"))});
            this.resComputations.DisplayMember = "Name";
            this.resComputations.Name = "resComputations";
            this.resComputations.ValueMember = "Id";
            // 
            // colACCOUNT
            // 
            resources.ApplyResources(this.colACCOUNT, "colACCOUNT");
            this.colACCOUNT.ColumnEdit = this.resAccounts;
            this.colACCOUNT.FieldName = "ACCOUNT_ID";
            this.colACCOUNT.Name = "colACCOUNT";
            this.colACCOUNT.OptionsColumn.ReadOnly = true;
            // 
            // resAccounts
            // 
            this.resAccounts.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("resAccounts.Appearance.Font")));
            this.resAccounts.Appearance.Options.UseFont = true;
            this.resAccounts.AppearanceFocused.Font = ((System.Drawing.Font)(resources.GetObject("resAccounts.AppearanceFocused.Font")));
            this.resAccounts.AppearanceFocused.Options.UseFont = true;
            this.resAccounts.AppearanceReadOnly.Font = ((System.Drawing.Font)(resources.GetObject("resAccounts.AppearanceReadOnly.Font")));
            this.resAccounts.AppearanceReadOnly.Options.UseFont = true;
            resources.ApplyResources(this.resAccounts, "resAccounts");
            resources.ApplyResources(serializableAppearanceObject1, "serializableAppearanceObject1");
            serializableAppearanceObject1.Options.UseFont = true;
            resources.ApplyResources(serializableAppearanceObject3, "serializableAppearanceObject3");
            serializableAppearanceObject3.Options.UseFont = true;
            this.resAccounts.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("resAccounts.Buttons"))), resources.GetString("resAccounts.Buttons1"), ((int)(resources.GetObject("resAccounts.Buttons2"))), ((bool)(resources.GetObject("resAccounts.Buttons3"))), ((bool)(resources.GetObject("resAccounts.Buttons4"))), ((bool)(resources.GetObject("resAccounts.Buttons5"))), editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, resources.GetString("resAccounts.Buttons6"), ((object)(resources.GetObject("resAccounts.Buttons7"))), ((DevExpress.Utils.SuperToolTip)(resources.GetObject("resAccounts.Buttons8"))), ((DevExpress.Utils.ToolTipAnchor)(resources.GetObject("resAccounts.Buttons9"))))});
            this.resAccounts.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("resAccounts.Columns"), resources.GetString("resAccounts.Columns1")),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("resAccounts.Columns2"), resources.GetString("resAccounts.Columns3"))});
            this.resAccounts.DisplayMember = "AccountName";
            this.resAccounts.Name = "resAccounts";
            this.resAccounts.ValueMember = "Id";
            // 
            // colTAX_AMOUNT
            // 
            resources.ApplyResources(this.colTAX_AMOUNT, "colTAX_AMOUNT");
            this.colTAX_AMOUNT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTAX_AMOUNT.FieldName = "TAX_AMOUNT";
            this.colTAX_AMOUNT.Name = "colTAX_AMOUNT";
            this.colTAX_AMOUNT.OptionsColumn.ReadOnly = true;
            // 
            // resCurrencies
            // 
            this.resCurrencies.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("resCurrencies.Appearance.Font")));
            this.resCurrencies.Appearance.Options.UseFont = true;
            this.resCurrencies.AppearanceFocused.Font = ((System.Drawing.Font)(resources.GetObject("resCurrencies.AppearanceFocused.Font")));
            this.resCurrencies.AppearanceFocused.Options.UseFont = true;
            this.resCurrencies.AppearanceReadOnly.Font = ((System.Drawing.Font)(resources.GetObject("resCurrencies.AppearanceReadOnly.Font")));
            this.resCurrencies.AppearanceReadOnly.Options.UseFont = true;
            resources.ApplyResources(this.resCurrencies, "resCurrencies");
            this.resCurrencies.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("resCurrencies.Buttons"))))});
            this.resCurrencies.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("resCurrencies.Columns"), resources.GetString("resCurrencies.Columns1")),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("resCurrencies.Columns2"), resources.GetString("resCurrencies.Columns3"))});
            this.resCurrencies.DisplayMember = "CURRENCY_CODE";
            this.resCurrencies.Name = "resCurrencies";
            this.resCurrencies.ValueMember = "CURRENCY_ID";
            // 
            // colIS_ACTIVE
            // 
            resources.ApplyResources(this.colIS_ACTIVE, "colIS_ACTIVE");
            this.colIS_ACTIVE.FieldName = "IS_ACTIVE";
            this.colIS_ACTIVE.Name = "colIS_ACTIVE";
            this.colIS_ACTIVE.OptionsColumn.ReadOnly = true;
            // 
            // colCREATE_BY
            // 
            resources.ApplyResources(this.colCREATE_BY, "colCREATE_BY");
            this.colCREATE_BY.FieldName = "CREATE_BY";
            this.colCREATE_BY.Name = "colCREATE_BY";
            this.colCREATE_BY.OptionsColumn.ReadOnly = true;
            // 
            // colCREATE_ON
            // 
            resources.ApplyResources(this.colCREATE_ON, "colCREATE_ON");
            this.colCREATE_ON.FieldName = "CREATE_ON";
            this.colCREATE_ON.Name = "colCREATE_ON";
            this.colCREATE_ON.OptionsColumn.ReadOnly = true;
            // 
            // btnREMOVE
            // 
            resources.ApplyResources(this.btnREMOVE, "btnREMOVE");
            this.btnREMOVE.Name = "btnREMOVE";
            this.btnREMOVE.UseVisualStyleBackColor = true;
            this.btnREMOVE.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnADD
            // 
            resources.ApplyResources(this.btnADD, "btnADD");
            this.btnADD.Name = "btnADD";
            this.btnADD.UseVisualStyleBackColor = true;
            this.btnADD.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnEDIT
            // 
            resources.ApplyResources(this.btnEDIT, "btnEDIT");
            this.btnEDIT.Name = "btnEDIT";
            this.btnEDIT.UseVisualStyleBackColor = true;
            this.btnEDIT.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // PageTax
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.panel1);
            this.Name = "PageTax";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.searchControl.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPaymentMethod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resComputations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resAccounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resCurrencies)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnADD;
        private ExButton btnEDIT;
        private ExButton btnREMOVE;
        private DevExpress.XtraGrid.GridControl dgv;
        private DevExpress.XtraGrid.Views.Grid.GridView dgvPaymentMethod;
        private DevExpress.XtraGrid.Columns.GridColumn colTAX;
        private DevExpress.XtraGrid.Columns.GridColumn colCREATE_BY;
        private DevExpress.XtraGrid.Columns.GridColumn colTAX_ID;
        private DevExpress.XtraGrid.Columns.GridColumn colCREATE_ON;
        private DevExpress.XtraGrid.Columns.GridColumn colIS_ACTIVE;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit resAccounts;
        private DevExpress.XtraGrid.Columns.GridColumn colCOMPUTATION;
        private DevExpress.XtraEditors.SearchControl searchControl;
        private DevExpress.XtraGrid.Columns.GridColumn colACCOUNT;
        private DevExpress.XtraGrid.Columns.GridColumn colTAX_AMOUNT;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit resCurrencies;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit resComputations;
    }
}
