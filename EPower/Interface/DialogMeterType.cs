﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogMeterType : ExDialog
    {
        GeneralProcess _flag;
        TBL_METER_TYPE _objMeterType = new TBL_METER_TYPE();
        public TBL_METER_TYPE MeterType
        {
            get { return _objMeterType; }
        }
        TBL_METER_TYPE _oldObjMeterType = new TBL_METER_TYPE();

        #region Constructor
        public DialogMeterType(GeneralProcess flag, TBL_METER_TYPE objMeterType)
        {
            InitializeComponent();
            btnAddAmpare.Enabled = SoftTech.Security.Logic.Login.IsAuthorized(Permission.POWER_UNITELECTRICITY_AMPARE);
            btnAddContant.Enabled = SoftTech.Security.Logic.Login.IsAuthorized(Permission.POWER_UNITELECTRICITY_CONTANT);
            btnAddPhase.Enabled = SoftTech.Security.Logic.Login.IsAuthorized(Permission.POWER_UNITELECTRICITY_PHASE);
            btnAddVoltage.Enabled = SoftTech.Security.Logic.Login.IsAuthorized(Permission.POWER_UNITELECTRICITY_VOLTAGE);





            _flag = flag;

            dataLookUp();
            objMeterType._CopyTo(_objMeterType);
            objMeterType._CopyTo(_oldObjMeterType);

            if (flag == GeneralProcess.Insert)
            {
                _objMeterType.IS_ACTIVE = true;
                this.Text = string.Concat(Resources.INSERT, this.Text);
            }
            else if (flag == GeneralProcess.Update)
            {
                this.Text = string.Concat(Resources.UPDATE, this.Text);
            }
            else if (flag == GeneralProcess.Delete)
            {
                this.Text = string.Concat(Resources.DELETE, this.Text);
                UIHelper.SetEnabled(this, false);
            }
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
            read();
            btnAddPhase.Enabled = false;
            btnAddVoltage.Enabled = false;
        }
        #endregion

        #region Method
        private void read()
        {
            txtMeterTypeCode.Text = _objMeterType.METER_TYPE_CODE;
            txtMeterTypeName.Text = _objMeterType.METER_TYPE_NAME;
            cboConstant.SelectedValue = _objMeterType.METER_CONST_ID;
            cboPhase.SelectedValue = _objMeterType.METER_PHASE_ID;
            cboVoltage.SelectedValue = _objMeterType.METER_VOL_ID;
            cboAmpera.SelectedValue = _objMeterType.METER_AMP_ID;
        }

        private void write()
        {
            _objMeterType.METER_TYPE_CODE = txtMeterTypeCode.Text.Trim();
            _objMeterType.METER_TYPE_NAME = txtMeterTypeName.Text.Trim();
            _objMeterType.METER_CONST_ID = (int)cboConstant.SelectedValue;
            _objMeterType.METER_PHASE_ID = (int)cboPhase.SelectedValue;
            _objMeterType.METER_VOL_ID = (int)cboVoltage.SelectedValue;
            _objMeterType.METER_AMP_ID = (int)cboAmpera.SelectedValue;
            _objMeterType.METER_VERSION = 0;
            _objMeterType.METER_IMAX = 0;
            _objMeterType.MAX_USAGE = 99999;
        }

        private void dataLookUp()
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                //Get constant.
                UIHelper.SetDataSourceToComboBox(cboConstant, Lookup.GetConstant());
                //Get phase.
                UIHelper.SetDataSourceToComboBox(cboPhase, Lookup.GetPowerPhases());
                //Get voltage.
                UIHelper.SetDataSourceToComboBox(cboVoltage, Lookup.GetPowerVoltage());
                //Get ampare.
                UIHelper.SetDataSourceToComboBox(cboAmpera, Lookup.GetPowerAmpare());
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
            this.Cursor = Cursors.Default;
        }

        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();

            if (txtMeterTypeCode.Text.Trim() == string.Empty)
            {
                txtMeterTypeCode.SetValidation(string.Format(Resources.REQUIRED, lblMETER_TYPE_CODE.Text));
                val = true;
            }
            if (txtMeterTypeName.Text.Trim() == string.Empty)
            {
                txtMeterTypeName.SetValidation(string.Format(Resources.REQUIRED, lblMETER_TYPE_NAME.Text));
                val = true;
            }
            //if (txtMeterVersion.Text.Trim() == string.Empty)
            //{
            //    txtMeterVersion.SetValidation(string.Format(Resources.RequestInput, lblVer.Text));
            //    val = true;
            //}
            //if (txtMeterIMax.Text.Trim() == string.Empty)
            //{
            //    txtMeterIMax.SetValidation(string.Format(Resources.RequestInput, lblIMax.Text));
            //    val = true;
            //}
            if (cboConstant.SelectedIndex == -1)
            {
                cboConstant.SetValidation(string.Format(Resources.REQUIRED, lblCONSTANT.Text));
                val = true;
            }
            if (cboPhase.SelectedIndex == -1)
            {
                cboPhase.SetValidation(string.Format(Resources.REQUIRED, lblPHASE.Text));
                val = true;
            }
            if (cboVoltage.SelectedIndex == -1)
            {
                cboVoltage.SetValidation(string.Format(Resources.REQUIRED, lblVOLTAGE.Text));
                val = true;
            }
            if (cboVoltage.SelectedIndex == -1)
            {
                cboVoltage.SetValidation(string.Format(Resources.REQUIRED, lblVOLTAGE.Text));
                val = true;
            }
            if (cboAmpera.SelectedIndex == -1)
            {
                cboAmpera.SetValidation(string.Format(Resources.REQUIRED, lblAMPARE.Text));
                val = true;
            }

            return val;
        }
        #endregion

        #region Operation
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
            txtMeterTypeCode.ClearValidation();
            if (DBDataContext.Db.IsExits(_objMeterType, "METER_TYPE_CODE"))
            {
                txtMeterTypeCode.SetValidation(string.Format(Resources.MS_IS_EXISTS, lblMETER_TYPE_CODE.Text));
                return;
            }
            try
            {

                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objMeterType);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_oldObjMeterType, _objMeterType);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objMeterType);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// Change current keyboard layout to khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        /// <summary>
        /// Input number integer only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNumber(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputNumberOnly(sender, e);
        }
        #endregion                

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objMeterType);
        }

        private void btnAddContant_AddItem(object sender, EventArgs e)
        {
            DialogConstant dig = new DialogConstant(GeneralProcess.Insert, new TBL_CONSTANT());
            if (dig.ShowDialog() == DialogResult.OK)
            {
                UIHelper.SetDataSourceToComboBox(cboConstant, Lookup.GetConstant());
                cboConstant.SelectedValue = dig.Constant.CONSTANT_ID;
            }
        }

        private void btnAddPhase_AddItem(object sender, EventArgs e)
        {
            DialogPhase dig = new DialogPhase(GeneralProcess.Insert, new TBL_PHASE());
            if (dig.ShowDialog() == DialogResult.OK)
            {
                UIHelper.SetDataSourceToComboBox(cboPhase, Lookup.GetPowerPhases());
                cboPhase.SelectedValue = dig.Phase.PHASE_ID;
            }
        }

        private void btnAddVoltage_AddItem(object sender, EventArgs e)
        {
            DialogVoltage dig = new DialogVoltage(new TBL_VOLTAGE(), GeneralProcess.Insert);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                UIHelper.SetDataSourceToComboBox(cboVoltage, Lookup.GetPowerVoltage());
                cboVoltage.SelectedValue = dig.Voltage.VOLTAGE_ID;
            }
        }

        private void btnAddAmpare_AddItem(object sender, EventArgs e)
        {
            DialogAmpare dig = new DialogAmpare(GeneralProcess.Insert, new TBL_AMPARE());
            if (dig.ShowDialog() == DialogResult.OK)
            {
                UIHelper.SetDataSourceToComboBox(cboAmpera, Lookup.GetPowerAmpare());
                cboAmpera.SelectedValue = dig.Ampare.AMPARE_ID;
            }
        }


    }
}