﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogCompany
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCompany));
            this.lblCOMPANY_NAME = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.cboVillage = new System.Windows.Forms.ComboBox();
            this.cboCommune = new System.Windows.Forms.ComboBox();
            this.cboDistrict = new System.Windows.Forms.ComboBox();
            this.cboProvince = new System.Windows.Forms.ComboBox();
            this.lblVILLAGE = new System.Windows.Forms.Label();
            this.lblCOMMUNE = new System.Windows.Forms.Label();
            this.lblDISTRICT = new System.Windows.Forms.Label();
            this.lblPROVINCE = new System.Windows.Forms.Label();
            this.txtAddressCompany = new System.Windows.Forms.TextBox();
            this.lblADDRESS_COMPANY = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCompanyName = new System.Windows.Forms.TextBox();
            this.lblLICENSEE_NAME_KH_AND_EN = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtRequestTitle = new System.Windows.Forms.TextBox();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.txtLicenseName = new System.Windows.Forms.TextBox();
            this.txtLiceseNo = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.lblLICENSE_NO_AND_COMPANY_TYPE = new System.Windows.Forms.Label();
            this.txtCompanyEng = new System.Windows.Forms.TextBox();
            this.lblCOMPANY_NAME_EN = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblBASE_PRICE_AND_SUBSIDY = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtBasedTariff = new System.Windows.Forms.TextBox();
            this.txtSubsidyTariff = new System.Windows.Forms.TextBox();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.txtAddressReceiver = new System.Windows.Forms.TextBox();
            this.lblADDRESS_RECEIVER = new System.Windows.Forms.Label();
            this.txtFacebookLink = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblFACEBOOK_ACCOUNT = new System.Windows.Forms.Label();
            this.lblLINK = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.cboLicenseType = new System.Windows.Forms.ComboBox();
            this.txtVAT_TIN = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lblVAT_TIN = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.panel2);
            this.content.Controls.Add(this.txtVAT_TIN);
            this.content.Controls.Add(this.label8);
            this.content.Controls.Add(this.lblVAT_TIN);
            this.content.Controls.Add(this.cboLicenseType);
            this.content.Controls.Add(this.lblLINK);
            this.content.Controls.Add(this.txtFacebookLink);
            this.content.Controls.Add(this.label4);
            this.content.Controls.Add(this.lblFACEBOOK_ACCOUNT);
            this.content.Controls.Add(this.txtAddressReceiver);
            this.content.Controls.Add(this.lblADDRESS_RECEIVER);
            this.content.Controls.Add(this.txtCompanyEng);
            this.content.Controls.Add(this.lblCOMPANY_NAME_EN);
            this.content.Controls.Add(this.txtLiceseNo);
            this.content.Controls.Add(this.label7);
            this.content.Controls.Add(this.lblLICENSE_NO_AND_COMPANY_TYPE);
            this.content.Controls.Add(this.txtSubsidyTariff);
            this.content.Controls.Add(this.txtLicenseName);
            this.content.Controls.Add(this.txtBasedTariff);
            this.content.Controls.Add(this.picLogo);
            this.content.Controls.Add(this.label17);
            this.content.Controls.Add(this.txtRequestTitle);
            this.content.Controls.Add(this.label3);
            this.content.Controls.Add(this.lblBASE_PRICE_AND_SUBSIDY);
            this.content.Controls.Add(this.lblLICENSEE_NAME_KH_AND_EN);
            this.content.Controls.Add(this.txtCompanyName);
            this.content.Controls.Add(this.label2);
            this.content.Controls.Add(this.txtAddressCompany);
            this.content.Controls.Add(this.lblADDRESS_COMPANY);
            this.content.Controls.Add(this.label39);
            this.content.Controls.Add(this.label38);
            this.content.Controls.Add(this.label37);
            this.content.Controls.Add(this.label36);
            this.content.Controls.Add(this.cboVillage);
            this.content.Controls.Add(this.cboCommune);
            this.content.Controls.Add(this.cboCurrency);
            this.content.Controls.Add(this.cboDistrict);
            this.content.Controls.Add(this.cboProvince);
            this.content.Controls.Add(this.lblVILLAGE);
            this.content.Controls.Add(this.lblCOMMUNE);
            this.content.Controls.Add(this.lblDISTRICT);
            this.content.Controls.Add(this.lblPROVINCE);
            this.content.Controls.Add(this.lblCOMPANY_NAME);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.label6);
            this.content.Controls.Add(this.label5);
            this.content.Controls.Add(this.label1);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.label5, 0);
            this.content.Controls.SetChildIndex(this.label6, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.lblCOMPANY_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblPROVINCE, 0);
            this.content.Controls.SetChildIndex(this.lblDISTRICT, 0);
            this.content.Controls.SetChildIndex(this.lblCOMMUNE, 0);
            this.content.Controls.SetChildIndex(this.lblVILLAGE, 0);
            this.content.Controls.SetChildIndex(this.cboProvince, 0);
            this.content.Controls.SetChildIndex(this.cboDistrict, 0);
            this.content.Controls.SetChildIndex(this.cboCurrency, 0);
            this.content.Controls.SetChildIndex(this.cboCommune, 0);
            this.content.Controls.SetChildIndex(this.cboVillage, 0);
            this.content.Controls.SetChildIndex(this.label36, 0);
            this.content.Controls.SetChildIndex(this.label37, 0);
            this.content.Controls.SetChildIndex(this.label38, 0);
            this.content.Controls.SetChildIndex(this.label39, 0);
            this.content.Controls.SetChildIndex(this.lblADDRESS_COMPANY, 0);
            this.content.Controls.SetChildIndex(this.txtAddressCompany, 0);
            this.content.Controls.SetChildIndex(this.label2, 0);
            this.content.Controls.SetChildIndex(this.txtCompanyName, 0);
            this.content.Controls.SetChildIndex(this.lblLICENSEE_NAME_KH_AND_EN, 0);
            this.content.Controls.SetChildIndex(this.lblBASE_PRICE_AND_SUBSIDY, 0);
            this.content.Controls.SetChildIndex(this.label3, 0);
            this.content.Controls.SetChildIndex(this.txtRequestTitle, 0);
            this.content.Controls.SetChildIndex(this.label17, 0);
            this.content.Controls.SetChildIndex(this.picLogo, 0);
            this.content.Controls.SetChildIndex(this.txtBasedTariff, 0);
            this.content.Controls.SetChildIndex(this.txtLicenseName, 0);
            this.content.Controls.SetChildIndex(this.txtSubsidyTariff, 0);
            this.content.Controls.SetChildIndex(this.lblLICENSE_NO_AND_COMPANY_TYPE, 0);
            this.content.Controls.SetChildIndex(this.label7, 0);
            this.content.Controls.SetChildIndex(this.txtLiceseNo, 0);
            this.content.Controls.SetChildIndex(this.lblCOMPANY_NAME_EN, 0);
            this.content.Controls.SetChildIndex(this.txtCompanyEng, 0);
            this.content.Controls.SetChildIndex(this.lblADDRESS_RECEIVER, 0);
            this.content.Controls.SetChildIndex(this.txtAddressReceiver, 0);
            this.content.Controls.SetChildIndex(this.lblFACEBOOK_ACCOUNT, 0);
            this.content.Controls.SetChildIndex(this.label4, 0);
            this.content.Controls.SetChildIndex(this.txtFacebookLink, 0);
            this.content.Controls.SetChildIndex(this.lblLINK, 0);
            this.content.Controls.SetChildIndex(this.cboLicenseType, 0);
            this.content.Controls.SetChildIndex(this.lblVAT_TIN, 0);
            this.content.Controls.SetChildIndex(this.label8, 0);
            this.content.Controls.SetChildIndex(this.txtVAT_TIN, 0);
            this.content.Controls.SetChildIndex(this.panel2, 0);
            // 
            // lblCOMPANY_NAME
            // 
            resources.ApplyResources(this.lblCOMPANY_NAME, "lblCOMPANY_NAME");
            this.lblCOMPANY_NAME.Name = "lblCOMPANY_NAME";
            // 
            // label39
            // 
            resources.ApplyResources(this.label39, "label39");
            this.label39.ForeColor = System.Drawing.Color.Red;
            this.label39.Name = "label39";
            // 
            // label38
            // 
            resources.ApplyResources(this.label38, "label38");
            this.label38.ForeColor = System.Drawing.Color.Red;
            this.label38.Name = "label38";
            // 
            // label37
            // 
            resources.ApplyResources(this.label37, "label37");
            this.label37.ForeColor = System.Drawing.Color.Red;
            this.label37.Name = "label37";
            // 
            // label36
            // 
            resources.ApplyResources(this.label36, "label36");
            this.label36.ForeColor = System.Drawing.Color.Red;
            this.label36.Name = "label36";
            // 
            // cboVillage
            // 
            this.cboVillage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboVillage, "cboVillage");
            this.cboVillage.FormattingEnabled = true;
            this.cboVillage.Name = "cboVillage";
            this.cboVillage.Enter += new System.EventHandler(this.txtCompanyName_Enter);
            // 
            // cboCommune
            // 
            this.cboCommune.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboCommune, "cboCommune");
            this.cboCommune.FormattingEnabled = true;
            this.cboCommune.Name = "cboCommune";
            this.cboCommune.SelectedIndexChanged += new System.EventHandler(this.cboCommune_SelectedIndexChanged);
            this.cboCommune.Enter += new System.EventHandler(this.txtCompanyName_Enter);
            // 
            // cboDistrict
            // 
            this.cboDistrict.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboDistrict, "cboDistrict");
            this.cboDistrict.FormattingEnabled = true;
            this.cboDistrict.Name = "cboDistrict";
            this.cboDistrict.SelectedIndexChanged += new System.EventHandler(this.cboDistrict_SelectedIndexChanged);
            this.cboDistrict.Enter += new System.EventHandler(this.txtCompanyName_Enter);
            // 
            // cboProvince
            // 
            this.cboProvince.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboProvince, "cboProvince");
            this.cboProvince.FormattingEnabled = true;
            this.cboProvince.Name = "cboProvince";
            this.cboProvince.SelectedIndexChanged += new System.EventHandler(this.cboProvince_SelectedIndexChanged);
            this.cboProvince.Enter += new System.EventHandler(this.txtCompanyName_Enter);
            // 
            // lblVILLAGE
            // 
            resources.ApplyResources(this.lblVILLAGE, "lblVILLAGE");
            this.lblVILLAGE.Name = "lblVILLAGE";
            // 
            // lblCOMMUNE
            // 
            resources.ApplyResources(this.lblCOMMUNE, "lblCOMMUNE");
            this.lblCOMMUNE.Name = "lblCOMMUNE";
            // 
            // lblDISTRICT
            // 
            resources.ApplyResources(this.lblDISTRICT, "lblDISTRICT");
            this.lblDISTRICT.Name = "lblDISTRICT";
            // 
            // lblPROVINCE
            // 
            resources.ApplyResources(this.lblPROVINCE, "lblPROVINCE");
            this.lblPROVINCE.Name = "lblPROVINCE";
            // 
            // txtAddressCompany
            // 
            resources.ApplyResources(this.txtAddressCompany, "txtAddressCompany");
            this.txtAddressCompany.Name = "txtAddressCompany";
            this.txtAddressCompany.Enter += new System.EventHandler(this.txtCompanyName_Enter);
            // 
            // lblADDRESS_COMPANY
            // 
            resources.ApplyResources(this.lblADDRESS_COMPANY, "lblADDRESS_COMPANY");
            this.lblADDRESS_COMPANY.Name = "lblADDRESS_COMPANY";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Name = "label2";
            // 
            // txtCompanyName
            // 
            resources.ApplyResources(this.txtCompanyName, "txtCompanyName");
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Enter += new System.EventHandler(this.txtCompanyName_Enter);
            // 
            // lblLICENSEE_NAME_KH_AND_EN
            // 
            resources.ApplyResources(this.lblLICENSEE_NAME_KH_AND_EN, "lblLICENSEE_NAME_KH_AND_EN");
            this.lblLICENSEE_NAME_KH_AND_EN.Name = "lblLICENSEE_NAME_KH_AND_EN";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Name = "label3";
            // 
            // txtRequestTitle
            // 
            resources.ApplyResources(this.txtRequestTitle, "txtRequestTitle");
            this.txtRequestTitle.Name = "txtRequestTitle";
            this.txtRequestTitle.Enter += new System.EventHandler(this.txtCompanyName_Enter);
            // 
            // picLogo
            // 
            this.picLogo.BackColor = System.Drawing.Color.White;
            this.picLogo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picLogo.Image = global::EPower.Properties.Resources.index;
            resources.ApplyResources(this.picLogo, "picLogo");
            this.picLogo.Name = "picLogo";
            this.picLogo.TabStop = false;
            this.picLogo.DoubleClick += new System.EventHandler(this.picLogo_DoubleClick);
            // 
            // txtLicenseName
            // 
            resources.ApplyResources(this.txtLicenseName, "txtLicenseName");
            this.txtLicenseName.Name = "txtLicenseName";
            this.txtLicenseName.Enter += new System.EventHandler(this.txt_Enter_EnglishKey);
            // 
            // txtLiceseNo
            // 
            resources.ApplyResources(this.txtLiceseNo, "txtLiceseNo");
            this.txtLiceseNo.Name = "txtLiceseNo";
            this.txtLiceseNo.Enter += new System.EventHandler(this.txt_Enter_EnglishKey);
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Name = "label7";
            // 
            // lblLICENSE_NO_AND_COMPANY_TYPE
            // 
            resources.ApplyResources(this.lblLICENSE_NO_AND_COMPANY_TYPE, "lblLICENSE_NO_AND_COMPANY_TYPE");
            this.lblLICENSE_NO_AND_COMPANY_TYPE.Name = "lblLICENSE_NO_AND_COMPANY_TYPE";
            // 
            // txtCompanyEng
            // 
            resources.ApplyResources(this.txtCompanyEng, "txtCompanyEng");
            this.txtCompanyEng.Name = "txtCompanyEng";
            // 
            // lblCOMPANY_NAME_EN
            // 
            resources.ApplyResources(this.lblCOMPANY_NAME_EN, "lblCOMPANY_NAME_EN");
            this.lblCOMPANY_NAME_EN.Name = "lblCOMPANY_NAME_EN";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // lblBASE_PRICE_AND_SUBSIDY
            // 
            resources.ApplyResources(this.lblBASE_PRICE_AND_SUBSIDY, "lblBASE_PRICE_AND_SUBSIDY");
            this.lblBASE_PRICE_AND_SUBSIDY.Name = "lblBASE_PRICE_AND_SUBSIDY";
            // 
            // label17
            // 
            resources.ApplyResources(this.label17, "label17");
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Name = "label17";
            // 
            // txtBasedTariff
            // 
            resources.ApplyResources(this.txtBasedTariff, "txtBasedTariff");
            this.txtBasedTariff.Name = "txtBasedTariff";
            this.txtBasedTariff.Enter += new System.EventHandler(this.txt_Enter_EnglishKey);
            this.txtBasedTariff.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.InputDecimalOnly);
            // 
            // txtSubsidyTariff
            // 
            resources.ApplyResources(this.txtSubsidyTariff, "txtSubsidyTariff");
            this.txtSubsidyTariff.Name = "txtSubsidyTariff";
            this.txtSubsidyTariff.Enter += new System.EventHandler(this.txt_Enter_EnglishKey);
            this.txtSubsidyTariff.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.InputDecimalOnly);
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.FormattingEnabled = true;
            this.cboCurrency.Name = "cboCurrency";
            // 
            // txtAddressReceiver
            // 
            resources.ApplyResources(this.txtAddressReceiver, "txtAddressReceiver");
            this.txtAddressReceiver.Name = "txtAddressReceiver";
            // 
            // lblADDRESS_RECEIVER
            // 
            resources.ApplyResources(this.lblADDRESS_RECEIVER, "lblADDRESS_RECEIVER");
            this.lblADDRESS_RECEIVER.Name = "lblADDRESS_RECEIVER";
            // 
            // txtFacebookLink
            // 
            resources.ApplyResources(this.txtFacebookLink, "txtFacebookLink");
            this.txtFacebookLink.Name = "txtFacebookLink";
            this.txtFacebookLink.MouseEnter += new System.EventHandler(this.TxtFacebookLink_MouseEnter);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Name = "label4";
            // 
            // lblFACEBOOK_ACCOUNT
            // 
            resources.ApplyResources(this.lblFACEBOOK_ACCOUNT, "lblFACEBOOK_ACCOUNT");
            this.lblFACEBOOK_ACCOUNT.Name = "lblFACEBOOK_ACCOUNT";
            // 
            // lblLINK
            // 
            resources.ApplyResources(this.lblLINK, "lblLINK");
            this.lblLINK.Name = "lblLINK";
            this.lblLINK.TabStop = true;
            this.lblLINK.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblLINK_LinkClicked);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // cboLicenseType
            // 
            this.cboLicenseType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboLicenseType, "cboLicenseType");
            this.cboLicenseType.FormattingEnabled = true;
            this.cboLicenseType.Name = "cboLicenseType";
            // 
            // txtVAT_TIN
            // 
            resources.ApplyResources(this.txtVAT_TIN, "txtVAT_TIN");
            this.txtVAT_TIN.Name = "txtVAT_TIN";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Name = "label8";
            // 
            // lblVAT_TIN
            // 
            resources.ApplyResources(this.lblVAT_TIN, "lblVAT_TIN");
            this.lblVAT_TIN.Name = "lblVAT_TIN";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnCHANGE_LOG);
            this.panel2.Controls.Add(this.btnCLOSE);
            this.panel2.Controls.Add(this.btnOK);
            this.panel2.Controls.Add(this.panel1);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.ForeColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // DialogCompany
            // 
            resources.ApplyResources(this, "$this");
            this.Name = "DialogCompany";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Label lblCOMPANY_NAME;
        private Label label39;
        private Label label38;
        private Label label37;
        private Label label36;
        private ComboBox cboVillage;
        private ComboBox cboCommune;
        private ComboBox cboDistrict;
        private ComboBox cboProvince;
        private Label lblVILLAGE;
        private Label lblCOMMUNE;
        private Label lblDISTRICT;
        private Label lblPROVINCE;
        private TextBox txtCompanyName;
        private Label label2;
        private TextBox txtAddressCompany;
        private Label lblADDRESS_COMPANY;
        private TextBox txtRequestTitle;
        private Label label3;
        private Label lblLICENSEE_NAME_KH_AND_EN;
        private PictureBox picLogo;
        private TextBox txtLicenseName;
        private TextBox txtLiceseNo;
        private Label label7;
        private Label lblLICENSE_NO_AND_COMPANY_TYPE;
        private TextBox txtCompanyEng;
        private Label lblCOMPANY_NAME_EN;
        private Label label5;
        private Label label6;
        private TextBox txtSubsidyTariff;
        private TextBox txtBasedTariff;
        private Label label17;
        private Label lblBASE_PRICE_AND_SUBSIDY;
        private Label label9;
        private ComboBox cboCurrency;
        private TextBox txtFacebookLink;
        private Label label4;
        private Label lblFACEBOOK_ACCOUNT;
        private TextBox txtAddressReceiver;
        private Label lblADDRESS_RECEIVER;
        private LinkLabel lblLINK;
        private Label label1;
        private ComboBox cboLicenseType;
        private TextBox txtVAT_TIN;
        private Label label8;
        private Label lblVAT_TIN;
        private Panel panel2;
        private ExButton btnCHANGE_LOG;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Panel panel1;
    }
}
