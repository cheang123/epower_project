﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SoftTech;
using SoftTech.Helper;
using SoftTech.Component;

namespace EPower
{
    public partial class DialogChangeConnectionTypeHistory : ExDialog
    {
        long invID;
        public DialogChangeConnectionTypeHistory(long invId)
        {
            InitializeComponent();
            invID = invId;
        }

        void bindInfo()
        {
            var result = (from inv in DBDataContext.Db.TBL_INVOICEs
                          join c in DBDataContext.Db.TBL_CUSTOMERs on inv.CUSTOMER_ID equals c.CUSTOMER_ID
                          join ct in DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs on c.CUSTOMER_CONNECTION_TYPE_ID equals ct.CUSTOMER_CONNECTION_TYPE_ID
                          join cg in DBDataContext.Db.TLKP_CUSTOMER_GROUPs on ct.NONLICENSE_CUSTOMER_GROUP_ID equals cg.CUSTOMER_GROUP_ID
                          where inv.INVOICE_ID == invID
                          select new
                          {
                              c.CUSTOMER_CODE,
                              inv.INVOICE_NO,
                              ct.CUSTOMER_CONNECTION_TYPE_NAME,
                              cg.CUSTOMER_GROUP_NAME,
                              CUSTOMER_NAME = c.LAST_NAME_KH + " " + c.FIRST_NAME_KH
                          }).FirstOrDefault();
            txtCustomerCode.Text = result.CUSTOMER_CODE;
            txtCustomerConnection.Text = result.CUSTOMER_CONNECTION_TYPE_NAME;
            txtCustomerGroup.Text = result.CUSTOMER_GROUP_NAME;
            //txtINVOICE_NO.Text = result.INVOICE_NO;
            txtCustomerName.Text = result.CUSTOMER_NAME;
        }

        void binddgv()
        {
            var result = from h in DBDataContext.Db.TBL_CHANGE_INVOICE_CONNECTION_TYPEs
                         join ct in DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs on h.CUSTOMER_CONNECTION_TYPE_ID equals ct.CUSTOMER_CONNECTION_TYPE_ID
                         join ct1 in DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs on h.CUSTOMER_CONNECTION_TYPE_ID_OLD equals ct1.CUSTOMER_CONNECTION_TYPE_ID
                         where h.INVOICE_ID == invID
                         select new
                         {
                             OLD_CONNECTION_TYPE = ct1.CUSTOMER_CONNECTION_TYPE_NAME,
                             NEW_CUSTOMER_CONNECTION_TYPE = ct.CUSTOMER_CONNECTION_TYPE_NAME,
                             CREATE_BY = h.CREATE_BY,
                             CHANGE_DATE = h.CREATE_ON,
                             NOTE = h.NOTE
                         };
            dgvHistory.DataSource = result;
        }

        private void btnCLOSE_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DialogChangeConnectionTypeHistory_Load(object sender, EventArgs e)
        {
            binddgv();
            bindInfo();
        }
    }
}
