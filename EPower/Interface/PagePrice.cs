﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PagePrice : Form
    {
        public PagePrice()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgvPrice);
            this.dgvPrice.SelectionMode = DataGridViewSelectionMode.FullRowSelect;


            this.btnEXTRA_CHARGE.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_PRICE_EXTRA_CHARGE]); 

            LoadPrice(); 
            this.txtSearchPrice.Focus();
            UIHelper.SetDataSourceToComboBox(cboCurrency, DBDataContext.Db.TLKP_CURRENCies.Select(x => new { x.CURRENCY_ID,x.CURRENCY_NAME})._ToDataTable(),Resources.ALL_CURRENCY);
            this.cboCurrency.SelectedIndex = 0;
        }

        #region Method
        public void LoadPrice()
        {
            if (cboCurrency.SelectedIndex==-1)
            {
                return;
            }

            dgvPrice.DataSource = from p in DBDataContext.Db.TBL_PRICEs
                                  join c in DBDataContext.Db.TLKP_CURRENCies on p.CURRENCY_ID equals c.CURRENCY_ID
                                  where (p.PRICE_NAME.ToLower().Contains(txtSearchPrice.Text.ToLower().Trim()))
                                  && p.IS_ACTIVE 
                                  && (c.CURRENCY_ID ==(int)cboCurrency.SelectedValue||(int)cboCurrency.SelectedValue==0)
                                  select new { p.PRICE_ID, p.PRICE_NAME, c.CURRENCY_NAME };
        }
        #endregion Method

        #region Event
        private void btnExtraCharge_Click(object sender, EventArgs e)
        {
            if (dgvPrice.Rows.Count > 0)
            {
                int priceId = (int)dgvPrice.SelectedRows[0].Cells[PRICE_ID.Name].Value;
                TBL_PRICE objPrice = DBDataContext.Db.TBL_PRICEs.FirstOrDefault(x => x.PRICE_ID == priceId);
                new DialogPriceExtraChargeList(objPrice).ShowDialog();
            }
        }

        private void dgvPrice_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgvPrice.SelectedRows.Count > 0)
            {
                int intPriceId = int.Parse(dgvPrice.SelectedRows[0].Cells["PRICE_ID"].Value.ToString());
                TBL_PRICE objPrice = DBDataContext.Db.TBL_PRICEs.FirstOrDefault(p => p.PRICE_ID == intPriceId);
                DialogPrice objDialog = new DialogPrice(objPrice, GeneralProcess.Update);
                objDialog.ShowDialog();
                if (objDialog.DialogResult == DialogResult.OK)
                {
                    LoadPrice();
                    UIHelper.SelectRow(this.dgvPrice, objDialog.Price.PRICE_ID);
                }
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            DialogPrice objDialog = new DialogPrice(new TBL_PRICE(), GeneralProcess.Insert);
            objDialog.ShowDialog();
            if (objDialog.DialogResult == DialogResult.OK)
            {
                LoadPrice();
                UIHelper.SelectRow(this.dgvPrice, objDialog.Price.PRICE_ID);
            }
        }

        private void txtSearchPrice_QuickSearch(object sender, EventArgs e)
        {
            LoadPrice();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgvPrice.SelectedRows.Count > 0)
            {
                int intPriceId = int.Parse(dgvPrice.SelectedRows[0].Cells["PRICE_ID"].Value.ToString());

                var lstPriceId = from c in DBDataContext.Db.TBL_CUSTOMERs
                                 where c.PRICE_ID == intPriceId && c.STATUS_ID!=(int)CustomerStatus.Cancelled && c.STATUS_ID!=(int)CustomerStatus.Closed
                                 select c.PRICE_ID;

                TBL_PRICE objPrice = DBDataContext.Db.TBL_PRICEs.FirstOrDefault(p => p.PRICE_ID == intPriceId);
                if (lstPriceId.Count() > 0)
                {
                    MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE);
                    return;
                }

                
                DialogPrice objDialog = new DialogPrice(objPrice, GeneralProcess.Delete);
                objDialog.ShowDialog();
                if (objDialog.DialogResult == DialogResult.OK)
                {
                    LoadPrice();
                    UIHelper.SelectRow(this.dgvPrice, objDialog.Price.PRICE_ID);
                }
            }
        }

        #endregion Event

        
         
    }
}
