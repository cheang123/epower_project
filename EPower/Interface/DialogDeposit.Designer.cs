﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogDeposit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogDeposit));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.label9 = new System.Windows.Forms.Label();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.lblAMPARE = new System.Windows.Forms.Label();
            this.cboAmpere = new System.Windows.Forms.ComboBox();
            this.lblPHASE = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cboPhase = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtNewConnection = new System.Windows.Forms.TextBox();
            this.lblDEPOSIT = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cboCustomerGroup = new System.Windows.Forms.ComboBox();
            this.lblCUSTOMER_GROUP = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.lblCURRENCY = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cboCustomerConnectionType = new System.Windows.Forms.ComboBox();
            this.lblCUSTOMER_CONNECTION_TYPE = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.lblCUSTOMER_CONNECTION_TYPE);
            this.content.Controls.Add(this.label2);
            this.content.Controls.Add(this.cboCustomerConnectionType);
            this.content.Controls.Add(this.label4);
            this.content.Controls.Add(this.cboCurrency);
            this.content.Controls.Add(this.lblCURRENCY);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.cboCustomerGroup);
            this.content.Controls.Add(this.lblCUSTOMER_GROUP);
            this.content.Controls.Add(this.label8);
            this.content.Controls.Add(this.txtNewConnection);
            this.content.Controls.Add(this.lblDEPOSIT);
            this.content.Controls.Add(this.cboPhase);
            this.content.Controls.Add(this.label5);
            this.content.Controls.Add(this.lblPHASE);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.cboAmpere);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.lblAMPARE);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.lblAMPARE, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.cboAmpere, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.lblPHASE, 0);
            this.content.Controls.SetChildIndex(this.label5, 0);
            this.content.Controls.SetChildIndex(this.cboPhase, 0);
            this.content.Controls.SetChildIndex(this.lblDEPOSIT, 0);
            this.content.Controls.SetChildIndex(this.txtNewConnection, 0);
            this.content.Controls.SetChildIndex(this.label8, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_GROUP, 0);
            this.content.Controls.SetChildIndex(this.cboCustomerGroup, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.lblCURRENCY, 0);
            this.content.Controls.SetChildIndex(this.cboCurrency, 0);
            this.content.Controls.SetChildIndex(this.label4, 0);
            this.content.Controls.SetChildIndex(this.cboCustomerConnectionType, 0);
            this.content.Controls.SetChildIndex(this.label2, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_CONNECTION_TYPE, 0);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // lblAMPARE
            // 
            resources.ApplyResources(this.lblAMPARE, "lblAMPARE");
            this.lblAMPARE.Name = "lblAMPARE";
            // 
            // cboAmpere
            // 
            this.cboAmpere.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAmpere.FormattingEnabled = true;
            resources.ApplyResources(this.cboAmpere, "cboAmpere");
            this.cboAmpere.Name = "cboAmpere";
            // 
            // lblPHASE
            // 
            resources.ApplyResources(this.lblPHASE, "lblPHASE");
            this.lblPHASE.Name = "lblPHASE";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Name = "label5";
            // 
            // cboPhase
            // 
            this.cboPhase.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPhase.FormattingEnabled = true;
            resources.ApplyResources(this.cboPhase, "cboPhase");
            this.cboPhase.Name = "cboPhase";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Name = "label8";
            // 
            // txtNewConnection
            // 
            resources.ApplyResources(this.txtNewConnection, "txtNewConnection");
            this.txtNewConnection.Name = "txtNewConnection";
            this.txtNewConnection.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // lblDEPOSIT
            // 
            resources.ApplyResources(this.lblDEPOSIT, "lblDEPOSIT");
            this.lblDEPOSIT.Name = "lblDEPOSIT";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // cboCustomerGroup
            // 
            this.cboCustomerGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomerGroup.FormattingEnabled = true;
            resources.ApplyResources(this.cboCustomerGroup, "cboCustomerGroup");
            this.cboCustomerGroup.Name = "cboCustomerGroup";
            this.cboCustomerGroup.SelectedIndexChanged += new System.EventHandler(this.cboCustomerGroup_SelectedIndexChanged);
            // 
            // lblCUSTOMER_GROUP
            // 
            resources.ApplyResources(this.lblCUSTOMER_GROUP, "lblCUSTOMER_GROUP");
            this.lblCUSTOMER_GROUP.Name = "lblCUSTOMER_GROUP";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Name = "label4";
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            // 
            // lblCURRENCY
            // 
            resources.ApplyResources(this.lblCURRENCY, "lblCURRENCY");
            this.lblCURRENCY.Name = "lblCURRENCY";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Name = "label2";
            // 
            // cboCustomerConnectionType
            // 
            this.cboCustomerConnectionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomerConnectionType.FormattingEnabled = true;
            resources.ApplyResources(this.cboCustomerConnectionType, "cboCustomerConnectionType");
            this.cboCustomerConnectionType.Name = "cboCustomerConnectionType";
            // 
            // lblCUSTOMER_CONNECTION_TYPE
            // 
            resources.ApplyResources(this.lblCUSTOMER_CONNECTION_TYPE, "lblCUSTOMER_CONNECTION_TYPE");
            this.lblCUSTOMER_CONNECTION_TYPE.Name = "lblCUSTOMER_CONNECTION_TYPE";
            // 
            // DialogDeposit
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogDeposit";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label label9;
        private ExButton btnCHANGE_LOG;
        private Label lblAMPARE;
        private ComboBox cboAmpere;
        private ComboBox cboPhase;
        private Label label5;
        private Label lblPHASE;
        private Label label8;
        private TextBox txtNewConnection;
        private Label lblDEPOSIT;
        private Label label1;
        private ComboBox cboCustomerGroup;
        private Label lblCUSTOMER_GROUP;
        private Label label4;
        private ComboBox cboCurrency;
        private Label lblCURRENCY;
        private Label label2;
        private ComboBox cboCustomerConnectionType;
        private Label lblCUSTOMER_CONNECTION_TYPE;
    }
}