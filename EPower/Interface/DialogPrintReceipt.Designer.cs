﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogPrintReceipt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogPrintReceipt));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvPayment = new System.Windows.Forms.DataGridView();
            this.txtBox = new System.Windows.Forms.TextBox();
            this.lblBOX = new System.Windows.Forms.Label();
            this.txtCustomerName = new SoftTech.Component.ExTextbox();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnPRINT = new SoftTech.Component.ExButton();
            this.lblCUSTOMER_INFORMATION = new System.Windows.Forms.Label();
            this.txtCustomerCode = new SoftTech.Component.ExTextbox();
            this.txtAreaName = new System.Windows.Forms.TextBox();
            this.lblArea = new System.Windows.Forms.Label();
            this.lblCUSTOMER_NAME = new System.Windows.Forms.Label();
            this.lblCUSTOMER_CODE = new System.Windows.Forms.Label();
            this.lblRECEIPT_NO = new System.Windows.Forms.Label();
            this.txtPaymentNumber = new SoftTech.Component.ExTextbox();
            this.dtpMonth = new System.Windows.Forms.DateTimePicker();
            this.lblMONTH = new System.Windows.Forms.Label();
            this.lblINVOICE_DETAIL = new SoftTech.Component.ExLinkLabel(this.components);
            this.lblSELECT_PAYMENT_TO_PRINT = new System.Windows.Forms.Label();
            this.PAYMENT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RECEIPT_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_BY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAYMENT_ACCOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_INVOICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAID_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_SING_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUS_PREPAYMENT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPayment)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.lblSELECT_PAYMENT_TO_PRINT);
            this.content.Controls.Add(this.lblINVOICE_DETAIL);
            this.content.Controls.Add(this.lblMONTH);
            this.content.Controls.Add(this.dtpMonth);
            this.content.Controls.Add(this.txtPaymentNumber);
            this.content.Controls.Add(this.lblRECEIPT_NO);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.dgvPayment);
            this.content.Controls.Add(this.txtBox);
            this.content.Controls.Add(this.lblBOX);
            this.content.Controls.Add(this.txtCustomerName);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnPRINT);
            this.content.Controls.Add(this.lblCUSTOMER_INFORMATION);
            this.content.Controls.Add(this.txtCustomerCode);
            this.content.Controls.Add(this.txtAreaName);
            this.content.Controls.Add(this.lblArea);
            this.content.Controls.Add(this.lblCUSTOMER_NAME);
            this.content.Controls.Add(this.lblCUSTOMER_CODE);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_CODE, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblArea, 0);
            this.content.Controls.SetChildIndex(this.txtAreaName, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerCode, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_INFORMATION, 0);
            this.content.Controls.SetChildIndex(this.btnPRINT, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerName, 0);
            this.content.Controls.SetChildIndex(this.lblBOX, 0);
            this.content.Controls.SetChildIndex(this.txtBox, 0);
            this.content.Controls.SetChildIndex(this.dgvPayment, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.lblRECEIPT_NO, 0);
            this.content.Controls.SetChildIndex(this.txtPaymentNumber, 0);
            this.content.Controls.SetChildIndex(this.dtpMonth, 0);
            this.content.Controls.SetChildIndex(this.lblMONTH, 0);
            this.content.Controls.SetChildIndex(this.lblINVOICE_DETAIL, 0);
            this.content.Controls.SetChildIndex(this.lblSELECT_PAYMENT_TO_PRINT, 0);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // dgvPayment
            // 
            this.dgvPayment.AllowUserToAddRows = false;
            this.dgvPayment.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvPayment.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPayment.BackgroundColor = System.Drawing.Color.White;
            this.dgvPayment.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvPayment.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvPayment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPayment.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PAYMENT_ID,
            this.RECEIPT_NO,
            this.DATE,
            this.CREATE_BY,
            this.PAYMENT_ACCOUNT,
            this.TOTAL_INVOICE,
            this.PAID_AMOUNT,
            this.CURRENCY_SING_,
            this.CUS_PREPAYMENT_ID});
            this.dgvPayment.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgvPayment, "dgvPayment");
            this.dgvPayment.Name = "dgvPayment";
            this.dgvPayment.ReadOnly = true;
            this.dgvPayment.RowHeadersVisible = false;
            // 
            // txtBox
            // 
            resources.ApplyResources(this.txtBox, "txtBox");
            this.txtBox.Name = "txtBox";
            // 
            // lblBOX
            // 
            resources.ApplyResources(this.lblBOX, "lblBOX");
            this.lblBOX.Name = "lblBOX";
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.BackColor = System.Drawing.Color.White;
            this.txtCustomerName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtCustomerName, "txtCustomerName");
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtCustomerName.AdvanceSearch += new System.EventHandler(this.txtCustomerName_AdvanceSearch);
            this.txtCustomerName.CancelAdvanceSearch += new System.EventHandler(this.txtCustomerCode_CancelAdvanceSearch);
            this.txtCustomerName.Enter += new System.EventHandler(this.InputKhmer);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnPRINT
            // 
            resources.ApplyResources(this.btnPRINT, "btnPRINT");
            this.btnPRINT.Name = "btnPRINT";
            this.btnPRINT.UseVisualStyleBackColor = true;
            this.btnPRINT.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblCUSTOMER_INFORMATION
            // 
            resources.ApplyResources(this.lblCUSTOMER_INFORMATION, "lblCUSTOMER_INFORMATION");
            this.lblCUSTOMER_INFORMATION.Name = "lblCUSTOMER_INFORMATION";
            // 
            // txtCustomerCode
            // 
            this.txtCustomerCode.BackColor = System.Drawing.Color.White;
            this.txtCustomerCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtCustomerCode, "txtCustomerCode");
            this.txtCustomerCode.Name = "txtCustomerCode";
            this.txtCustomerCode.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtCustomerCode.AdvanceSearch += new System.EventHandler(this.txtCustomerCode_AdvanceSearch);
            this.txtCustomerCode.CancelAdvanceSearch += new System.EventHandler(this.txtCustomerCode_CancelAdvanceSearch);
            this.txtCustomerCode.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // txtAreaName
            // 
            resources.ApplyResources(this.txtAreaName, "txtAreaName");
            this.txtAreaName.Name = "txtAreaName";
            // 
            // lblArea
            // 
            resources.ApplyResources(this.lblArea, "lblArea");
            this.lblArea.Name = "lblArea";
            // 
            // lblCUSTOMER_NAME
            // 
            resources.ApplyResources(this.lblCUSTOMER_NAME, "lblCUSTOMER_NAME");
            this.lblCUSTOMER_NAME.Name = "lblCUSTOMER_NAME";
            // 
            // lblCUSTOMER_CODE
            // 
            resources.ApplyResources(this.lblCUSTOMER_CODE, "lblCUSTOMER_CODE");
            this.lblCUSTOMER_CODE.Name = "lblCUSTOMER_CODE";
            // 
            // lblRECEIPT_NO
            // 
            resources.ApplyResources(this.lblRECEIPT_NO, "lblRECEIPT_NO");
            this.lblRECEIPT_NO.Name = "lblRECEIPT_NO";
            // 
            // txtPaymentNumber
            // 
            this.txtPaymentNumber.BackColor = System.Drawing.Color.White;
            this.txtPaymentNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtPaymentNumber, "txtPaymentNumber");
            this.txtPaymentNumber.Name = "txtPaymentNumber";
            this.txtPaymentNumber.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtPaymentNumber.AdvanceSearch += new System.EventHandler(this.txtPaymentNumber_AdvanceSearch);
            this.txtPaymentNumber.CancelAdvanceSearch += new System.EventHandler(this.txtCustomerCode_CancelAdvanceSearch);
            this.txtPaymentNumber.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // dtpMonth
            // 
            resources.ApplyResources(this.dtpMonth, "dtpMonth");
            this.dtpMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMonth.Name = "dtpMonth";
            this.dtpMonth.ShowUpDown = true;
            this.dtpMonth.ValueChanged += new System.EventHandler(this.dtpMonth_ValueChanged);
            // 
            // lblMONTH
            // 
            resources.ApplyResources(this.lblMONTH, "lblMONTH");
            this.lblMONTH.Name = "lblMONTH";
            // 
            // lblINVOICE_DETAIL
            // 
            this.lblINVOICE_DETAIL.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            resources.ApplyResources(this.lblINVOICE_DETAIL, "lblINVOICE_DETAIL");
            this.lblINVOICE_DETAIL.Name = "lblINVOICE_DETAIL";
            this.lblINVOICE_DETAIL.TabStop = true;
            this.lblINVOICE_DETAIL.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkPaymentDetail_LinkClicked);
            // 
            // lblSELECT_PAYMENT_TO_PRINT
            // 
            resources.ApplyResources(this.lblSELECT_PAYMENT_TO_PRINT, "lblSELECT_PAYMENT_TO_PRINT");
            this.lblSELECT_PAYMENT_TO_PRINT.Name = "lblSELECT_PAYMENT_TO_PRINT";
            // 
            // PAYMENT_ID
            // 
            this.PAYMENT_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PAYMENT_ID.DataPropertyName = "PAYMENT_ID";
            resources.ApplyResources(this.PAYMENT_ID, "PAYMENT_ID");
            this.PAYMENT_ID.Name = "PAYMENT_ID";
            this.PAYMENT_ID.ReadOnly = true;
            // 
            // RECEIPT_NO
            // 
            this.RECEIPT_NO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.RECEIPT_NO.DataPropertyName = "PAYMENT_NO";
            resources.ApplyResources(this.RECEIPT_NO, "RECEIPT_NO");
            this.RECEIPT_NO.Name = "RECEIPT_NO";
            this.RECEIPT_NO.ReadOnly = true;
            // 
            // DATE
            // 
            this.DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.DATE.DataPropertyName = "PAY_DATE";
            dataGridViewCellStyle2.Format = "dd-MM-yyyy";
            this.DATE.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.DATE, "DATE");
            this.DATE.Name = "DATE";
            this.DATE.ReadOnly = true;
            // 
            // CREATE_BY
            // 
            this.CREATE_BY.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.CREATE_BY.DataPropertyName = "CREATE_BY";
            resources.ApplyResources(this.CREATE_BY, "CREATE_BY");
            this.CREATE_BY.Name = "CREATE_BY";
            this.CREATE_BY.ReadOnly = true;
            // 
            // PAYMENT_ACCOUNT
            // 
            this.PAYMENT_ACCOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PAYMENT_ACCOUNT.DataPropertyName = "ACCOUNT_NAME";
            resources.ApplyResources(this.PAYMENT_ACCOUNT, "PAYMENT_ACCOUNT");
            this.PAYMENT_ACCOUNT.Name = "PAYMENT_ACCOUNT";
            this.PAYMENT_ACCOUNT.ReadOnly = true;
            // 
            // TOTAL_INVOICE
            // 
            this.TOTAL_INVOICE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.TOTAL_INVOICE.DataPropertyName = "NO_INVOICE";
            resources.ApplyResources(this.TOTAL_INVOICE, "TOTAL_INVOICE");
            this.TOTAL_INVOICE.Name = "TOTAL_INVOICE";
            this.TOTAL_INVOICE.ReadOnly = true;
            // 
            // PAID_AMOUNT
            // 
            this.PAID_AMOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.PAID_AMOUNT.DataPropertyName = "PAY_AMOUNT";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "#,##0.####";
            this.PAID_AMOUNT.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.PAID_AMOUNT, "PAID_AMOUNT");
            this.PAID_AMOUNT.Name = "PAID_AMOUNT";
            this.PAID_AMOUNT.ReadOnly = true;
            // 
            // CURRENCY_SING_
            // 
            this.CURRENCY_SING_.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.CURRENCY_SING_.DataPropertyName = "CURRENCY_SING";
            resources.ApplyResources(this.CURRENCY_SING_, "CURRENCY_SING_");
            this.CURRENCY_SING_.Name = "CURRENCY_SING_";
            this.CURRENCY_SING_.ReadOnly = true;
            // 
            // CUS_PREPAYMENT_ID
            // 
            this.CUS_PREPAYMENT_ID.DataPropertyName = "CUS_PREPAYMENT_ID";
            resources.ApplyResources(this.CUS_PREPAYMENT_ID, "CUS_PREPAYMENT_ID");
            this.CUS_PREPAYMENT_ID.Name = "CUS_PREPAYMENT_ID";
            this.CUS_PREPAYMENT_ID.ReadOnly = true;
            // 
            // DialogPrintReceipt
            // 
            resources.ApplyResources(this, "$this");
            this.Name = "DialogPrintReceipt";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPayment)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private DataGridView dgvPayment;
        private TextBox txtBox;
        private Label lblBOX;
        private ExTextbox txtCustomerName;
        private ExButton btnCLOSE;
        private ExButton btnPRINT;
        private Label lblCUSTOMER_INFORMATION;
        private ExTextbox txtCustomerCode;
        private TextBox txtAreaName;
        private Label lblArea;
        private Label lblCUSTOMER_NAME;
        private Label lblCUSTOMER_CODE;
        private ExTextbox txtPaymentNumber;
        private Label lblRECEIPT_NO;
        private Label lblMONTH;
        private DateTimePicker dtpMonth;
        private ExLinkLabel lblINVOICE_DETAIL;
        private Label lblSELECT_PAYMENT_TO_PRINT;
        private DataGridViewTextBoxColumn PAYMENT_ID;
        private DataGridViewTextBoxColumn RECEIPT_NO;
        private DataGridViewTextBoxColumn DATE;
        private DataGridViewTextBoxColumn CREATE_BY;
        private DataGridViewTextBoxColumn PAYMENT_ACCOUNT;
        private DataGridViewTextBoxColumn TOTAL_INVOICE;
        private DataGridViewTextBoxColumn PAID_AMOUNT;
        private DataGridViewTextBoxColumn CURRENCY_SING_;
        private DataGridViewTextBoxColumn CUS_PREPAYMENT_ID;
    }
}
