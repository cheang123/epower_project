﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PageCollector : Form
    {
        #region Data
        int intPositionID = 0;
        public int PositionID
        {
            get { return intPositionID; }
            private set { intPositionID = value; }
        }
        int intDeviceID = 0;
        public int DeviceID
        {
            get { return intDeviceID; }
            private set { intDeviceID = value; }
        }

        public TBL_EMPLOYEE Employee
        {
            get
            {
                TBL_EMPLOYEE objEmp = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    try
                    {
                        int intEmpId = (int)dgv.SelectedRows[0].Cells["EMPLOYEE_ID"].Value;
                        objEmp = DBDataContext.Db.TBL_EMPLOYEEs.FirstOrDefault(x => x.EMPLOYEE_ID == intEmpId && x.IS_ACTIVE);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objEmp;
            }
        }
        #endregion

        #region Constructor
        public PageCollector()
        {
            InitializeComponent();
            txt_QuickSearch(null, null);
            UIHelper.DataGridViewProperties(dgv);
           
        }
        #endregion

        #region Operation
        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }
        private void btnNew_Click(object sender, EventArgs e)
        {
            DialogCollector dig = new DialogCollector(GeneralProcess.Insert, new TBL_EMPLOYEE() { EMPLOYEE_NAME = "", PASSWORD="" });
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.Employee.EMPLOYEE_ID);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                DialogCollector dig = new DialogCollector(GeneralProcess.Update, Employee);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.Employee.EMPLOYEE_ID);
                }
            }
        }

        private void Remove_Click(object sender, EventArgs e)
        {
            if (IsDeletable())
            {
                DialogCollector dig = new DialogCollector(GeneralProcess.Delete, Employee);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.Employee.EMPLOYEE_ID-1);
                }
            }
        }

        private void txt_QuickSearch(object sender, EventArgs e)
        {
            if (cboPosition.SelectedIndex!=-1)
            {
                intPositionID = (int)cboPosition.SelectedValue;
            }

            if (cboDevice.SelectedIndex!=-1)
            {
                intDeviceID = (int)cboDevice.SelectedValue;
            }

            try
            {                
                dgv.DataSource = from emp in DBDataContext.Db.TBL_EMPLOYEEs
                                 where (emp.IS_ACTIVE && 
                                 emp.EMPLOYEE_NAME.ToLower().Contains(txtQuickSearch.Text.Trim().ToLower())) &&

                                 ((DBDataContext.Db.TBL_EMPLOYEE_DEVICEs.
                                 Where(x => x.DEVICE_ID == intDeviceID ).
                                 Any(x => x.EMPLOYEE_ID == emp.EMPLOYEE_ID) ||intDeviceID == 0) &&
                                 (DBDataContext.Db.TBL_EMPLOYEE_POSITIONs.
                                 Where(x => x.POSITION_ID == intPositionID).
                                 Any(x => x.EMPLOYEE_ID == emp.EMPLOYEE_ID) || intPositionID == 0))                               
                                 
                                 select emp;
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        /// <summary>
        /// Change current keyboard layout to khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        #endregion

        #region Method
        private bool IsDeletable()
        { 
            bool val = false;
            if (dgv.SelectedRows.Count > 0)
            {
                val = true;
                if (DBDataContext.Db.TBL_POLEs.Where(x=>(x.COLLECTOR_ID==Employee.EMPLOYEE_ID || x.BILLER_ID==Employee.EMPLOYEE_ID || x.CUTTER_ID==Employee.EMPLOYEE_ID) && x.IS_ACTIVE).Count()>0)
                {
                    MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE);
                    val = false;
                }
            }
            return val;
        }

        public void loadDevice()
        {
            int tempDevice = DeviceID;
            DataTable dt = (from d in DBDataContext.Db.TBL_DEVICEs
                            where d.STATUS_ID != (int)DeviceStatus.Unavailable
                            select new
                            {
                                d.DEVICE_ID,
                                d.DEVICE_CODE,
                            })._ToDataTable();
            DataRow dr = dt.NewRow();
            dr["DEVICE_ID"] = 0;
            dr["DEVICE_CODE"] = Resources.ALL_DEVICE;
            dt.Rows.InsertAt(dr, 0);
            UIHelper.SetDataSourceToComboBox(cboDevice, dt);
            cboDevice.SelectedValue = tempDevice;
        }

        public void loadPosition()
        {
            int tempPosition = PositionID;
            DataTable dt = (from p in DBDataContext.Db.TBL_POSITIONs
                            where p.IS_ACTIVE
                            select new
                            {
                                p.POSITION_ID,
                                p.POSITION_NAME
                            })._ToDataTable();
            DataRow dr = dt.NewRow();
            dr["POSITION_ID"] = 0;
            dr["POSITION_NAME"] = Resources.ALL_POSITION;
            dt.Rows.InsertAt(dr, 0);
            UIHelper.SetDataSourceToComboBox(cboPosition, dt);
            cboPosition.SelectedValue = tempPosition;
        }
        #endregion

       
    }
}
