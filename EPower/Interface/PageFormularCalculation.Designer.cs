﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageFormulaCalculation
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageFormulaCalculation));
            this.lblFORMULAR_MV_RATIO = new System.Windows.Forms.GroupBox();
            this.txtMVCapacity = new System.Windows.Forms.TextBox();
            this.cboMVTransfomerType = new System.Windows.Forms.ComboBox();
            this.txtMVCT_VT = new System.Windows.Forms.TextBox();
            this.txtMVResult = new System.Windows.Forms.TextBox();
            this.lblCT_VT = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblRESULT = new System.Windows.Forms.Label();
            this.label3_ = new System.Windows.Forms.Label();
            this.lblCAPACITY_1 = new System.Windows.Forms.Label();
            this.lblFORMULAR_LV_RATIO = new System.Windows.Forms.GroupBox();
            this.txtLVCapacity = new System.Windows.Forms.TextBox();
            this.txtLVCT_VT = new System.Windows.Forms.TextBox();
            this.txtLVResult = new System.Windows.Forms.TextBox();
            this.lblCT_VT_2 = new System.Windows.Forms.Label();
            this.lblRESULT_2 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lblCAPACITY_2 = new System.Windows.Forms.Label();
            this.lblFORMULAR_AMPERE_TRANSFORMER_RATIO = new System.Windows.Forms.GroupBox();
            this.txtTransfoCapacity = new System.Windows.Forms.TextBox();
            this.cboPhase = new System.Windows.Forms.ComboBox();
            this.txtResultAmpareTransformer = new System.Windows.Forms.TextBox();
            this.lblRESULT_3 = new System.Windows.Forms.Label();
            this.lblMultiplier_ = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.lblPHASE = new System.Windows.Forms.Label();
            this.lblCAPACITY = new System.Windows.Forms.Label();
            this.lblFORMULAR_MV_RATIO.SuspendLayout();
            this.lblFORMULAR_LV_RATIO.SuspendLayout();
            this.lblFORMULAR_AMPERE_TRANSFORMER_RATIO.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblFORMULAR_MV_RATIO
            // 
            resources.ApplyResources(this.lblFORMULAR_MV_RATIO, "lblFORMULAR_MV_RATIO");
            this.lblFORMULAR_MV_RATIO.Controls.Add(this.txtMVCapacity);
            this.lblFORMULAR_MV_RATIO.Controls.Add(this.cboMVTransfomerType);
            this.lblFORMULAR_MV_RATIO.Controls.Add(this.txtMVCT_VT);
            this.lblFORMULAR_MV_RATIO.Controls.Add(this.txtMVResult);
            this.lblFORMULAR_MV_RATIO.Controls.Add(this.lblCT_VT);
            this.lblFORMULAR_MV_RATIO.Controls.Add(this.label4);
            this.lblFORMULAR_MV_RATIO.Controls.Add(this.lblRESULT);
            this.lblFORMULAR_MV_RATIO.Controls.Add(this.label3_);
            this.lblFORMULAR_MV_RATIO.Controls.Add(this.lblCAPACITY_1);
            this.lblFORMULAR_MV_RATIO.Name = "lblFORMULAR_MV_RATIO";
            this.lblFORMULAR_MV_RATIO.TabStop = false;
            // 
            // txtMVCapacity
            // 
            resources.ApplyResources(this.txtMVCapacity, "txtMVCapacity");
            this.txtMVCapacity.Name = "txtMVCapacity";
            this.txtMVCapacity.TextChanged += new System.EventHandler(this.cboMVKVA_SelectedIndexChanged);
            this.txtMVCapacity.Enter += new System.EventHandler(this.txtMVCapacity_Enter);
            this.txtMVCapacity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMVCapacity_KeyPress);
            // 
            // cboMVTransfomerType
            // 
            this.cboMVTransfomerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMVTransfomerType.FormattingEnabled = true;
            this.cboMVTransfomerType.Items.AddRange(new object[] {
            resources.GetString("cboMVTransfomerType.Items"),
            resources.GetString("cboMVTransfomerType.Items1")});
            resources.ApplyResources(this.cboMVTransfomerType, "cboMVTransfomerType");
            this.cboMVTransfomerType.Name = "cboMVTransfomerType";
            this.cboMVTransfomerType.SelectedIndexChanged += new System.EventHandler(this.cboMVKVA_SelectedIndexChanged);
            // 
            // txtMVCT_VT
            // 
            resources.ApplyResources(this.txtMVCT_VT, "txtMVCT_VT");
            this.txtMVCT_VT.Name = "txtMVCT_VT";
            this.txtMVCT_VT.ReadOnly = true;
            // 
            // txtMVResult
            // 
            resources.ApplyResources(this.txtMVResult, "txtMVResult");
            this.txtMVResult.Name = "txtMVResult";
            this.txtMVResult.ReadOnly = true;
            // 
            // lblCT_VT
            // 
            resources.ApplyResources(this.lblCT_VT, "lblCT_VT");
            this.lblCT_VT.Name = "lblCT_VT";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // lblRESULT
            // 
            resources.ApplyResources(this.lblRESULT, "lblRESULT");
            this.lblRESULT.Name = "lblRESULT";
            // 
            // label3_
            // 
            resources.ApplyResources(this.label3_, "label3_");
            this.label3_.Name = "label3_";
            // 
            // lblCAPACITY_1
            // 
            resources.ApplyResources(this.lblCAPACITY_1, "lblCAPACITY_1");
            this.lblCAPACITY_1.Name = "lblCAPACITY_1";
            // 
            // lblFORMULAR_LV_RATIO
            // 
            resources.ApplyResources(this.lblFORMULAR_LV_RATIO, "lblFORMULAR_LV_RATIO");
            this.lblFORMULAR_LV_RATIO.Controls.Add(this.txtLVCapacity);
            this.lblFORMULAR_LV_RATIO.Controls.Add(this.txtLVCT_VT);
            this.lblFORMULAR_LV_RATIO.Controls.Add(this.txtLVResult);
            this.lblFORMULAR_LV_RATIO.Controls.Add(this.lblCT_VT_2);
            this.lblFORMULAR_LV_RATIO.Controls.Add(this.lblRESULT_2);
            this.lblFORMULAR_LV_RATIO.Controls.Add(this.label15);
            this.lblFORMULAR_LV_RATIO.Controls.Add(this.lblCAPACITY_2);
            this.lblFORMULAR_LV_RATIO.Name = "lblFORMULAR_LV_RATIO";
            this.lblFORMULAR_LV_RATIO.TabStop = false;
            // 
            // txtLVCapacity
            // 
            resources.ApplyResources(this.txtLVCapacity, "txtLVCapacity");
            this.txtLVCapacity.Name = "txtLVCapacity";
            this.txtLVCapacity.TextChanged += new System.EventHandler(this.cboLVKVA_SelectedIndexChanged);
            this.txtLVCapacity.Enter += new System.EventHandler(this.txtMVCapacity_Enter);
            this.txtLVCapacity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLVCapacity_KeyPress);
            // 
            // txtLVCT_VT
            // 
            resources.ApplyResources(this.txtLVCT_VT, "txtLVCT_VT");
            this.txtLVCT_VT.Name = "txtLVCT_VT";
            this.txtLVCT_VT.ReadOnly = true;
            // 
            // txtLVResult
            // 
            resources.ApplyResources(this.txtLVResult, "txtLVResult");
            this.txtLVResult.Name = "txtLVResult";
            this.txtLVResult.ReadOnly = true;
            // 
            // lblCT_VT_2
            // 
            resources.ApplyResources(this.lblCT_VT_2, "lblCT_VT_2");
            this.lblCT_VT_2.Name = "lblCT_VT_2";
            // 
            // lblRESULT_2
            // 
            resources.ApplyResources(this.lblRESULT_2, "lblRESULT_2");
            this.lblRESULT_2.Name = "lblRESULT_2";
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.Name = "label15";
            // 
            // lblCAPACITY_2
            // 
            resources.ApplyResources(this.lblCAPACITY_2, "lblCAPACITY_2");
            this.lblCAPACITY_2.Name = "lblCAPACITY_2";
            // 
            // lblFORMULAR_AMPERE_TRANSFORMER_RATIO
            // 
            resources.ApplyResources(this.lblFORMULAR_AMPERE_TRANSFORMER_RATIO, "lblFORMULAR_AMPERE_TRANSFORMER_RATIO");
            this.lblFORMULAR_AMPERE_TRANSFORMER_RATIO.Controls.Add(this.txtTransfoCapacity);
            this.lblFORMULAR_AMPERE_TRANSFORMER_RATIO.Controls.Add(this.cboPhase);
            this.lblFORMULAR_AMPERE_TRANSFORMER_RATIO.Controls.Add(this.txtResultAmpareTransformer);
            this.lblFORMULAR_AMPERE_TRANSFORMER_RATIO.Controls.Add(this.lblRESULT_3);
            this.lblFORMULAR_AMPERE_TRANSFORMER_RATIO.Controls.Add(this.lblMultiplier_);
            this.lblFORMULAR_AMPERE_TRANSFORMER_RATIO.Controls.Add(this.label18);
            this.lblFORMULAR_AMPERE_TRANSFORMER_RATIO.Controls.Add(this.lblPHASE);
            this.lblFORMULAR_AMPERE_TRANSFORMER_RATIO.Controls.Add(this.lblCAPACITY);
            this.lblFORMULAR_AMPERE_TRANSFORMER_RATIO.Name = "lblFORMULAR_AMPERE_TRANSFORMER_RATIO";
            this.lblFORMULAR_AMPERE_TRANSFORMER_RATIO.TabStop = false;
            // 
            // txtTransfoCapacity
            // 
            resources.ApplyResources(this.txtTransfoCapacity, "txtTransfoCapacity");
            this.txtTransfoCapacity.Name = "txtTransfoCapacity";
            this.txtTransfoCapacity.TextChanged += new System.EventHandler(this.cboPhase_SelectedIndexChanged);
            this.txtTransfoCapacity.Enter += new System.EventHandler(this.txtMVCapacity_Enter);
            this.txtTransfoCapacity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTransfoCapacity_KeyPress);
            // 
            // cboPhase
            // 
            this.cboPhase.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPhase.FormattingEnabled = true;
            this.cboPhase.Items.AddRange(new object[] {
            resources.GetString("cboPhase.Items")});
            resources.ApplyResources(this.cboPhase, "cboPhase");
            this.cboPhase.Name = "cboPhase";
            this.cboPhase.SelectedIndexChanged += new System.EventHandler(this.cboPhase_SelectedIndexChanged);
            this.cboPhase.TextChanged += new System.EventHandler(this.cboPhase_SelectedIndexChanged);
            // 
            // txtResultAmpareTransformer
            // 
            resources.ApplyResources(this.txtResultAmpareTransformer, "txtResultAmpareTransformer");
            this.txtResultAmpareTransformer.Name = "txtResultAmpareTransformer";
            this.txtResultAmpareTransformer.ReadOnly = true;
            // 
            // lblRESULT_3
            // 
            resources.ApplyResources(this.lblRESULT_3, "lblRESULT_3");
            this.lblRESULT_3.Name = "lblRESULT_3";
            // 
            // lblMultiplier_
            // 
            resources.ApplyResources(this.lblMultiplier_, "lblMultiplier_");
            this.lblMultiplier_.Name = "lblMultiplier_";
            // 
            // label18
            // 
            resources.ApplyResources(this.label18, "label18");
            this.label18.Name = "label18";
            // 
            // lblPHASE
            // 
            resources.ApplyResources(this.lblPHASE, "lblPHASE");
            this.lblPHASE.Name = "lblPHASE";
            // 
            // lblCAPACITY
            // 
            resources.ApplyResources(this.lblCAPACITY, "lblCAPACITY");
            this.lblCAPACITY.Name = "lblCAPACITY";
            // 
            // PageFormulaCalculation
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.Controls.Add(this.lblFORMULAR_AMPERE_TRANSFORMER_RATIO);
            this.Controls.Add(this.lblFORMULAR_LV_RATIO);
            this.Controls.Add(this.lblFORMULAR_MV_RATIO);
            this.DoubleBuffered = true;
            this.Name = "PageFormulaCalculation";
            this.lblFORMULAR_MV_RATIO.ResumeLayout(false);
            this.lblFORMULAR_MV_RATIO.PerformLayout();
            this.lblFORMULAR_LV_RATIO.ResumeLayout(false);
            this.lblFORMULAR_LV_RATIO.PerformLayout();
            this.lblFORMULAR_AMPERE_TRANSFORMER_RATIO.ResumeLayout(false);
            this.lblFORMULAR_AMPERE_TRANSFORMER_RATIO.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private GroupBox lblFORMULAR_MV_RATIO;
        private Label lblCAPACITY_1;
        private ComboBox cboMVTransfomerType;
        private TextBox txtMVCT_VT;
        private TextBox txtMVResult;
        private Label lblCT_VT;
        private Label label4;
        private Label lblRESULT;
        private Label label3_;
        private GroupBox lblFORMULAR_LV_RATIO;
        private TextBox txtLVCT_VT;
        private TextBox txtLVResult;
        private Label lblCT_VT_2;
        private Label lblRESULT_2;
        private Label label15;
        private Label lblCAPACITY_2;
        private TextBox txtMVCapacity;
        private TextBox txtLVCapacity;
        private GroupBox lblFORMULAR_AMPERE_TRANSFORMER_RATIO;
        private TextBox txtTransfoCapacity;
        private ComboBox cboPhase;
        private TextBox txtResultAmpareTransformer;
        private Label lblRESULT_3;
        private Label lblCAPACITY;
        private Label lblPHASE;
        private Label lblMultiplier_;
        private Label label18;

    }
}
