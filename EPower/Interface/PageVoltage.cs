﻿using System;
using System.Linq;
using System.Windows.Forms;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PageVoltage : Form
    {
        public PageVoltage()
        {
            InitializeComponent();

            bind(); 
        }

        private void bind()
        {
            btnADD.Visible = false;
            btnEDIT.Visible = false;
            btnREMOVE.Visible = false;
            try
            {
                dgv.DataSource = (from sa in DBDataContext.Db.TBL_VOLTAGEs
                                 where sa.IS_ACTIVE &&
                                 sa.VOLTAGE_NAME.ToLower().Contains(txtSearch.Text.ToLower().Trim())
                                 select sa)
                                 .ToList()
                                 .OrderBy(x=>Lookup.ParseVoltage (x.VOLTAGE_NAME ))
                                 .ToList();
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }     
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (this.dgv.SelectedRows.Count > 0)
            {
                int id = (int) this.dgv.SelectedRows[0].Cells[0].Value;
                TBL_VOLTAGE obj = DBDataContext.Db.TBL_VOLTAGEs.Single(row => row.VOLTAGE_ID == id);
                DialogVoltage diag = new DialogVoltage(obj, GeneralProcess.Update);
                if (diag.ShowDialog() == DialogResult.OK)
                {
                    this.bind();
                    UIHelper.SelectRow(this.dgv,diag.Voltage.VOLTAGE_ID);
                }
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            DialogVoltage diag = new DialogVoltage(new TBL_VOLTAGE()
            {
                VOLTAGE_ID=0,
                VOLTAGE_NAME="",
                IS_ACTIVE=true 
            }, GeneralProcess.Insert);

            if (diag.ShowDialog() == DialogResult.OK)
            {
                bind();
                UIHelper.SelectRow(this.dgv,diag.Voltage.VOLTAGE_ID);
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (this.dgv.SelectedRows.Count > 0)
            {
                int id = (int)this.dgv.SelectedRows[0].Cells[0].Value;
                if (isDeletable(id))
                {
                    TBL_VOLTAGE obj = DBDataContext.Db.TBL_VOLTAGEs.Single(row => row.VOLTAGE_ID == id);
                    DialogVoltage diag = new DialogVoltage(obj, GeneralProcess.Delete);
                    if (diag.ShowDialog() == DialogResult.OK)
                    {
                        this.bind();
                        UIHelper.SelectRow(this.dgv, diag.Voltage.VOLTAGE_ID - 1);
                    }
                }
                else
                {
                    MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE); 
                }
            }
        }

        private void txtSearch_QuickSearch(object sender, EventArgs e)
        {
            bind();
        }

        private bool isDeletable(int id)
        {
            return DBDataContext.Db.TBL_CIRCUIT_BREAKER_TYPEs.Count(row => row.IS_ACTIVE && row.BREAKER_VOL_ID == id) == 0
                    && DBDataContext.Db.TBL_METER_TYPEs.Count(row => row.IS_ACTIVE && row.METER_VOL_ID == id) == 0;
        }

        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //if (e.RowIndex == -1)
            //{
            //    return;
            //}
            //btnEdit_Click(null, null);
        }

      
    }
}
