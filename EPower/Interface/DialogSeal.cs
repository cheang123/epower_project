﻿using System;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;

namespace EPower.Interface
{
    public partial class DialogSeal: ExDialog
    {
        GeneralProcess _flag;
        TBL_SEAL _objSeal = new TBL_SEAL();
        public TBL_SEAL Seal
        {
            get { return _objSeal; }
        }
        TBL_SEAL _oldObjSeal = new TBL_SEAL();

        #region Constructor
        public DialogSeal(GeneralProcess flag, TBL_SEAL objSeal)
        {
            InitializeComponent();

            _flag = flag;
            objSeal._CopyTo(_objSeal);
            objSeal._CopyTo(_oldObjSeal);
            
            if (flag == GeneralProcess.Insert)
            {
                _objSeal.IS_ACTIVE = true;
                this.Text = string.Concat(Resources.INSERT, this.Text);
            }
            else if (flag == GeneralProcess.Update)
            {
                this.Text = string.Concat(Resources.UPDATE, this.Text);
            }
            else if (flag == GeneralProcess.Delete)
            {
                this.Text = string.Concat(Resources.DELETE, this.Text);
                UIHelper.SetEnabled(this, false);
            }
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
            read();
        }
        #endregion

        #region Operation
        /// <summary>
        /// Data transaction.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (invalid())
            {
                return;
            }

            write();
            //If record is duplicate.
            txtSeal.ClearValidation();
            if (DBDataContext.Db.IsExits(_objSeal, "SEAL_NAME"))
            {
                txtSeal.SetValidation(string.Format( Resources.MS_IS_EXISTS,lblSHIELD.Text));
                return;
            }
         
            try
            {
                using (TransactionScope tran=new TransactionScope( TransactionScopeOption.Required,TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objSeal);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_oldObjSeal, _objSeal);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objSeal);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }                                                
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            } 
        }

        /// <summary>
        /// User read only
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// Change current keyboard layout to khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        #endregion

        #region Method
        /// <summary>
        /// Read data from object.
        /// </summary>
        private void read()
        {
            txtSeal.Text = _objSeal.SEAL_NAME;
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            _objSeal.SEAL_NAME = txtSeal.Text.Trim();
        }

        private bool invalid()
        {
            bool val = false;
            if (txtSeal.Text.Trim()==string.Empty)
            {
                txtSeal.SetValidation(string.Format(Resources.REQUIRED, lblSHIELD.Text));
                val = true;
            }
            return val;
        } 
        #endregion

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objSeal);
        }
    }
}