﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogDevice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogDevice));
            this.lblDEVICE_CODE = new System.Windows.Forms.Label();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.lblDEVICE = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.lblDEVICE_TYPE = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblDeviceOemInfo_ = new System.Windows.Forms.Label();
            this.lblDeviceType_ = new System.Windows.Forms.Label();
            this.timeConnection = new System.Windows.Forms.Timer(this.components);
            this.pbSync = new System.Windows.Forms.PictureBox();
            this.btnCONNECT_TO_DEVICE = new SoftTech.Component.ExButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblConnectStatus_ = new System.Windows.Forms.Label();
            this.lblSTATUS = new System.Windows.Forms.Label();
            this.cboStatus = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.chkREINSTALL_APPLICATION_TO_DEVICE = new System.Windows.Forms.CheckBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSync)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.chkREINSTALL_APPLICATION_TO_DEVICE);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.label10);
            this.content.Controls.Add(this.cboStatus);
            this.content.Controls.Add(this.lblSTATUS);
            this.content.Controls.Add(this.panel2);
            this.content.Controls.Add(this.lblDeviceType_);
            this.content.Controls.Add(this.lblDeviceOemInfo_);
            this.content.Controls.Add(this.label5);
            this.content.Controls.Add(this.lblDEVICE_TYPE);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.lblDEVICE);
            this.content.Controls.Add(this.txtCode);
            this.content.Controls.Add(this.lblDEVICE_CODE);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblDEVICE_CODE, 0);
            this.content.Controls.SetChildIndex(this.txtCode, 0);
            this.content.Controls.SetChildIndex(this.lblDEVICE, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.lblDEVICE_TYPE, 0);
            this.content.Controls.SetChildIndex(this.label5, 0);
            this.content.Controls.SetChildIndex(this.lblDeviceOemInfo_, 0);
            this.content.Controls.SetChildIndex(this.lblDeviceType_, 0);
            this.content.Controls.SetChildIndex(this.panel2, 0);
            this.content.Controls.SetChildIndex(this.lblSTATUS, 0);
            this.content.Controls.SetChildIndex(this.cboStatus, 0);
            this.content.Controls.SetChildIndex(this.label10, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.chkREINSTALL_APPLICATION_TO_DEVICE, 0);
            // 
            // lblDEVICE_CODE
            // 
            resources.ApplyResources(this.lblDEVICE_CODE, "lblDEVICE_CODE");
            this.lblDEVICE_CODE.Name = "lblDEVICE_CODE";
            // 
            // txtCode
            // 
            resources.ApplyResources(this.txtCode, "txtCode");
            this.txtCode.Name = "txtCode";
            this.txtCode.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // lblDEVICE
            // 
            resources.ApplyResources(this.lblDEVICE, "lblDEVICE");
            this.lblDEVICE.Name = "lblDEVICE";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lblDEVICE_TYPE
            // 
            resources.ApplyResources(this.lblDEVICE_TYPE, "lblDEVICE_TYPE");
            this.lblDEVICE_TYPE.Name = "lblDEVICE_TYPE";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Name = "label5";
            // 
            // lblDeviceOemInfo_
            // 
            resources.ApplyResources(this.lblDeviceOemInfo_, "lblDeviceOemInfo_");
            this.lblDeviceOemInfo_.Name = "lblDeviceOemInfo_";
            // 
            // lblDeviceType_
            // 
            resources.ApplyResources(this.lblDeviceType_, "lblDeviceType_");
            this.lblDeviceType_.Name = "lblDeviceType_";
            // 
            // timeConnection
            // 
            this.timeConnection.Interval = 3000;
            this.timeConnection.Tick += new System.EventHandler(this.timeConnection_Tick);
            // 
            // pbSync
            // 
            this.pbSync.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.pbSync, "pbSync");
            this.pbSync.Image = global::EPower.Properties.Resources.Sync;
            this.pbSync.Name = "pbSync";
            this.pbSync.TabStop = false;
            // 
            // btnCONNECT_TO_DEVICE
            // 
            resources.ApplyResources(this.btnCONNECT_TO_DEVICE, "btnCONNECT_TO_DEVICE");
            this.btnCONNECT_TO_DEVICE.Name = "btnCONNECT_TO_DEVICE";
            this.btnCONNECT_TO_DEVICE.UseVisualStyleBackColor = true;
            this.btnCONNECT_TO_DEVICE.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel2.Controls.Add(this.lblConnectStatus_);
            this.panel2.Controls.Add(this.btnCONNECT_TO_DEVICE);
            this.panel2.Controls.Add(this.pbSync);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // lblConnectStatus_
            // 
            this.lblConnectStatus_.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblConnectStatus_, "lblConnectStatus_");
            this.lblConnectStatus_.Name = "lblConnectStatus_";
            // 
            // lblSTATUS
            // 
            resources.ApplyResources(this.lblSTATUS, "lblSTATUS");
            this.lblSTATUS.Name = "lblSTATUS";
            // 
            // cboStatus
            // 
            this.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboStatus.FormattingEnabled = true;
            this.cboStatus.Items.AddRange(new object[] {
            resources.GetString("cboStatus.Items"),
            resources.GetString("cboStatus.Items1"),
            resources.GetString("cboStatus.Items2")});
            resources.ApplyResources(this.cboStatus, "cboStatus");
            this.cboStatus.Name = "cboStatus";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Name = "label10";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // chkREINSTALL_APPLICATION_TO_DEVICE
            // 
            resources.ApplyResources(this.chkREINSTALL_APPLICATION_TO_DEVICE, "chkREINSTALL_APPLICATION_TO_DEVICE");
            this.chkREINSTALL_APPLICATION_TO_DEVICE.Name = "chkREINSTALL_APPLICATION_TO_DEVICE";
            this.chkREINSTALL_APPLICATION_TO_DEVICE.UseVisualStyleBackColor = true;
            // 
            // DialogDevice
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogDevice";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSync)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Label lblDEVICE;
        private TextBox txtCode;
        private Label lblDEVICE_CODE;
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label lblDEVICE_TYPE;
        private Label lblDeviceType_;
        private Label lblDeviceOemInfo_;
        private Label label5;
        private Timer timeConnection;
        private PictureBox pbSync;
        private ExButton btnCONNECT_TO_DEVICE;
        private Panel panel2;
        private Label lblConnectStatus_;
        private Label lblSTATUS;
        private Label label10;
        private ComboBox cboStatus;
        private ExButton btnCHANGE_LOG;
        private CheckBox chkREINSTALL_APPLICATION_TO_DEVICE;
    }
}