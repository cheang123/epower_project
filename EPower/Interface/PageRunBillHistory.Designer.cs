﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageRunBillHistory
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageRunBillHistory));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pHeader = new System.Windows.Forms.Panel();
            this.btnVIEW = new SoftTech.Component.ExButton();
            this.cboReport = new System.Windows.Forms.ComboBox();
            this.btnREVERSE_BILL = new SoftTech.Component.ExButton();
            this.btnADJUST_DATE = new SoftTech.Component.ExButton();
            this.btnREPORT_SUMMARY = new SoftTech.Component.ExButton();
            this.btnREPORT_POWER = new SoftTech.Component.ExButton();
            this.lblYEAR = new System.Windows.Forms.Label();
            this.dtpRunYear = new System.Windows.Forms.DateTimePicker();
            this.cboBillingCycle = new System.Windows.Forms.ComboBox();
            this.dgvBilling = new System.Windows.Forms.DataGridView();
            this.RUN_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CYCLE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CYCLE_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MONTH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_ON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_BY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_INVOICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.POWER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.calendarColumn1 = new SoftTech.Component.CalendarColumn();
            this.calendarColumn2 = new SoftTech.Component.CalendarColumn();
            this.dataGridViewTimeColumn1 = new SoftTech.Component.DataGridViewTimeColumn();
            this.pHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBilling)).BeginInit();
            this.SuspendLayout();
            // 
            // pHeader
            // 
            this.pHeader.BackColor = System.Drawing.Color.Transparent;
            this.pHeader.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.pHeader.Controls.Add(this.btnVIEW);
            this.pHeader.Controls.Add(this.cboReport);
            this.pHeader.Controls.Add(this.btnREVERSE_BILL);
            this.pHeader.Controls.Add(this.btnADJUST_DATE);
            this.pHeader.Controls.Add(this.btnREPORT_SUMMARY);
            this.pHeader.Controls.Add(this.btnREPORT_POWER);
            this.pHeader.Controls.Add(this.lblYEAR);
            this.pHeader.Controls.Add(this.dtpRunYear);
            this.pHeader.Controls.Add(this.cboBillingCycle);
            resources.ApplyResources(this.pHeader, "pHeader");
            this.pHeader.Name = "pHeader";
            this.pHeader.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pHeader_MouseDoubleClick);
            // 
            // btnVIEW
            // 
            resources.ApplyResources(this.btnVIEW, "btnVIEW");
            this.btnVIEW.Name = "btnVIEW";
            this.btnVIEW.UseVisualStyleBackColor = true;
            this.btnVIEW.Click += new System.EventHandler(this.btnViewReport_Click_1);
            // 
            // cboReport
            // 
            this.cboReport.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboReport.FormattingEnabled = true;
            resources.ApplyResources(this.cboReport, "cboReport");
            this.cboReport.Name = "cboReport";
            this.cboReport.SelectedIndexChanged += new System.EventHandler(this.cboReport_SelectedIndexChanged_1);
            // 
            // btnREVERSE_BILL
            // 
            resources.ApplyResources(this.btnREVERSE_BILL, "btnREVERSE_BILL");
            this.btnREVERSE_BILL.Name = "btnREVERSE_BILL";
            this.btnREVERSE_BILL.UseVisualStyleBackColor = true;
            this.btnREVERSE_BILL.Click += new System.EventHandler(this.btnReverseBill_Click);
            // 
            // btnADJUST_DATE
            // 
            resources.ApplyResources(this.btnADJUST_DATE, "btnADJUST_DATE");
            this.btnADJUST_DATE.Name = "btnADJUST_DATE";
            this.btnADJUST_DATE.UseVisualStyleBackColor = true;
            this.btnADJUST_DATE.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnREPORT_SUMMARY
            // 
            resources.ApplyResources(this.btnREPORT_SUMMARY, "btnREPORT_SUMMARY");
            this.btnREPORT_SUMMARY.Name = "btnREPORT_SUMMARY";
            this.btnREPORT_SUMMARY.UseVisualStyleBackColor = true;
            this.btnREPORT_SUMMARY.Click += new System.EventHandler(this.btnMail_Click);
            // 
            // btnREPORT_POWER
            // 
            resources.ApplyResources(this.btnREPORT_POWER, "btnREPORT_POWER");
            this.btnREPORT_POWER.Name = "btnREPORT_POWER";
            this.btnREPORT_POWER.UseVisualStyleBackColor = true;
            this.btnREPORT_POWER.Click += new System.EventHandler(this.btnViewReport_Click);
            // 
            // lblYEAR
            // 
            resources.ApplyResources(this.lblYEAR, "lblYEAR");
            this.lblYEAR.Name = "lblYEAR";
            // 
            // dtpRunYear
            // 
            resources.ApplyResources(this.dtpRunYear, "dtpRunYear");
            this.dtpRunYear.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpRunYear.Name = "dtpRunYear";
            this.dtpRunYear.ShowUpDown = true;
            this.dtpRunYear.ValueChanged += new System.EventHandler(this.dtpRunYear_ValueChanged);
            // 
            // cboBillingCycle
            // 
            this.cboBillingCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBillingCycle.FormattingEnabled = true;
            resources.ApplyResources(this.cboBillingCycle, "cboBillingCycle");
            this.cboBillingCycle.Name = "cboBillingCycle";
            this.cboBillingCycle.SelectedIndexChanged += new System.EventHandler(this.cboBillingCycle_SelectedIndexChanged);
            // 
            // dgvBilling
            // 
            this.dgvBilling.AllowUserToAddRows = false;
            this.dgvBilling.AllowUserToDeleteRows = false;
            this.dgvBilling.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvBilling.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvBilling.BackgroundColor = System.Drawing.Color.White;
            this.dgvBilling.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvBilling.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvBilling.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBilling.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RUN_ID,
            this.CYCLE_ID,
            this.CYCLE_NAME,
            this.MONTH,
            this.CREATE_ON,
            this.CREATE_BY,
            this.TOTAL_INVOICE,
            this.POWER});
            resources.ApplyResources(this.dgvBilling, "dgvBilling");
            this.dgvBilling.EnableHeadersVisualStyles = false;
            this.dgvBilling.Name = "dgvBilling";
            this.dgvBilling.ReadOnly = true;
            this.dgvBilling.RowHeadersVisible = false;
            this.dgvBilling.RowTemplate.Height = 25;
            // 
            // RUN_ID
            // 
            this.RUN_ID.DataPropertyName = "RUN_ID";
            resources.ApplyResources(this.RUN_ID, "RUN_ID");
            this.RUN_ID.Name = "RUN_ID";
            this.RUN_ID.ReadOnly = true;
            // 
            // CYCLE_ID
            // 
            this.CYCLE_ID.DataPropertyName = "CYCLE_ID";
            resources.ApplyResources(this.CYCLE_ID, "CYCLE_ID");
            this.CYCLE_ID.Name = "CYCLE_ID";
            this.CYCLE_ID.ReadOnly = true;
            // 
            // CYCLE_NAME
            // 
            this.CYCLE_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CYCLE_NAME.DataPropertyName = "CYCLE_NAME";
            resources.ApplyResources(this.CYCLE_NAME, "CYCLE_NAME");
            this.CYCLE_NAME.Name = "CYCLE_NAME";
            this.CYCLE_NAME.ReadOnly = true;
            // 
            // MONTH
            // 
            this.MONTH.DataPropertyName = "BILLING_MONTH";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.Format = "MM-yyyy";
            this.MONTH.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.MONTH, "MONTH");
            this.MONTH.Name = "MONTH";
            this.MONTH.ReadOnly = true;
            // 
            // CREATE_ON
            // 
            this.CREATE_ON.DataPropertyName = "CREATE_ON";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.Format = "dd-MM-yyyy hh:mm tt";
            this.CREATE_ON.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.CREATE_ON, "CREATE_ON");
            this.CREATE_ON.Name = "CREATE_ON";
            this.CREATE_ON.ReadOnly = true;
            // 
            // CREATE_BY
            // 
            this.CREATE_BY.DataPropertyName = "CREATE_BY";
            resources.ApplyResources(this.CREATE_BY, "CREATE_BY");
            this.CREATE_BY.Name = "CREATE_BY";
            this.CREATE_BY.ReadOnly = true;
            // 
            // TOTAL_INVOICE
            // 
            this.TOTAL_INVOICE.DataPropertyName = "TOTAL_INVOICE";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "#,##0";
            this.TOTAL_INVOICE.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.TOTAL_INVOICE, "TOTAL_INVOICE");
            this.TOTAL_INVOICE.Name = "TOTAL_INVOICE";
            this.TOTAL_INVOICE.ReadOnly = true;
            // 
            // POWER
            // 
            this.POWER.DataPropertyName = "TOTAL_POWER";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "#,##0.00";
            this.POWER.DefaultCellStyle = dataGridViewCellStyle5;
            resources.ApplyResources(this.POWER, "POWER");
            this.POWER.Name = "POWER";
            this.POWER.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "CYCLE_ID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn1, "dataGridViewTextBoxColumn1");
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "CYCLE_NAME";
            resources.ApplyResources(this.dataGridViewTextBoxColumn2, "dataGridViewTextBoxColumn2");
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "BILLING_MONTH";
            dataGridViewCellStyle6.Format = "ថ្ងៃទី dd - ខែ MM - ឆ្នាំ yyyy";
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle6;
            resources.ApplyResources(this.dataGridViewTextBoxColumn3, "dataGridViewTextBoxColumn3");
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "END_DATE";
            dataGridViewCellStyle7.Format = "ថ្ងៃទី 0";
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle7;
            resources.ApplyResources(this.dataGridViewTextBoxColumn4, "dataGridViewTextBoxColumn4");
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "COLLECT_DAY";
            dataGridViewCellStyle8.Format = "0 នាក់";
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle8;
            resources.ApplyResources(this.dataGridViewTextBoxColumn5, "dataGridViewTextBoxColumn5");
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "TOTAL_CUSTOMER";
            dataGridViewCellStyle9.Format = "0 នាក់";
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle9;
            resources.ApplyResources(this.dataGridViewTextBoxColumn6, "dataGridViewTextBoxColumn6");
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "NO_USAGE";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle10.Format = "#,##0";
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle10;
            resources.ApplyResources(this.dataGridViewTextBoxColumn7, "dataGridViewTextBoxColumn7");
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "TOTAL_POWER";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle11.Format = "#,##0.00";
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle11;
            resources.ApplyResources(this.dataGridViewTextBoxColumn8, "dataGridViewTextBoxColumn8");
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // calendarColumn1
            // 
            this.calendarColumn1.DataPropertyName = "BILLING_MONTH";
            dataGridViewCellStyle12.Format = "ខែ MM - ឆ្នាំ yyyy";
            this.calendarColumn1.DefaultCellStyle = dataGridViewCellStyle12;
            resources.ApplyResources(this.calendarColumn1, "calendarColumn1");
            this.calendarColumn1.Name = "calendarColumn1";
            this.calendarColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.calendarColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // calendarColumn2
            // 
            dataGridViewCellStyle13.Format = "ថ្ងៃទី dd - ខែ MM - ឆ្នាំ yyyy";
            this.calendarColumn2.DefaultCellStyle = dataGridViewCellStyle13;
            resources.ApplyResources(this.calendarColumn2, "calendarColumn2");
            this.calendarColumn2.Name = "calendarColumn2";
            this.calendarColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.calendarColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewTimeColumn1
            // 
            resources.ApplyResources(this.dataGridViewTimeColumn1, "dataGridViewTimeColumn1");
            this.dataGridViewTimeColumn1.Name = "dataGridViewTimeColumn1";
            this.dataGridViewTimeColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTimeColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // PageRunBillHistory
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dgvBilling);
            this.Controls.Add(this.pHeader);
            this.Name = "PageRunBillHistory";
            this.pHeader.ResumeLayout(false);
            this.pHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBilling)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel pHeader;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTimeColumn dataGridViewTimeColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private ComboBox cboBillingCycle;
        private CalendarColumn calendarColumn1;
        private CalendarColumn calendarColumn2;
        private DateTimePicker dtpRunYear;
        private Label lblYEAR;
        private ExButton btnREPORT_SUMMARY;
        private ExButton btnREPORT_POWER;
        private ExButton btnADJUST_DATE;
        private DataGridView dgvBilling;
        private ExButton btnREVERSE_BILL;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private DataGridViewTextBoxColumn RUN_ID;
        private DataGridViewTextBoxColumn CYCLE_ID;
        private DataGridViewTextBoxColumn CYCLE_NAME;
        private DataGridViewTextBoxColumn MONTH;
        private DataGridViewTextBoxColumn CREATE_ON;
        private DataGridViewTextBoxColumn CREATE_BY;
        private DataGridViewTextBoxColumn TOTAL_INVOICE;
        private DataGridViewTextBoxColumn POWER;
        private ComboBox cboReport;
        private ExButton btnVIEW;
    }
}
