﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class DialogCustomerMeterUsage : SoftTech.Component.ExDialog
    {
        private TBL_METER _objMeter = new TBL_METER();
        public DialogCustomerMeterUsage(TBL_METER objMeter)
        {
            InitializeComponent();
            objMeter._CopyTo(this._objMeter);
            UIHelper.DataGridViewProperties(dgv);
            this.read();
        }
        #region Method
        private void read()
        {
            this.dgv.DataSource = from cm in DBDataContext.Db.TBL_CUSTOMER_METERs
                                  join m in DBDataContext.Db.TBL_METERs on cm.METER_ID equals m.METER_ID
                                  join c in DBDataContext.Db.TBL_CUSTOMERs on cm.CUSTOMER_ID equals c.CUSTOMER_ID
                                  join p in DBDataContext.Db.TBL_POLEs on cm.POLE_ID equals p.POLE_ID
                                  join b in DBDataContext.Db.TBL_BOXes on cm.BOX_ID equals b.BOX_ID
                                  where cm.METER_ID == this._objMeter.METER_ID
                                  orderby cm.USED_DATE,c.CUSTOMER_CODE ascending
                                  select new
                                  {
                                      c.CUSTOMER_ID,
                                      c.CUSTOMER_CODE,
                                      CUSTOMER_NAME = c.LAST_NAME_KH + " " + c.FIRST_NAME_KH,
                                      METER=m.METER_CODE,
                                      cm.USED_DATE,
                                      BOX=b.BOX_CODE,
                                      POLE=p.POLE_CODE,
                                      CUSTOMER_STATUS=c.STATUS_ID
                                  };

        }
        #endregion
        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgv_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            //set ForeColor to Red if customer is close.
            foreach (DataGridViewRow item in dgv.Rows)
            {
                int status_id = (int)item.Cells["CUSTOMER_STATUS"].Value;
                if (status_id == (int)CustomerStatus.Closed || status_id == (int)CustomerStatus.Cancelled)
                {
                    item.DefaultCellStyle.ForeColor = Color.Red;
                    item.DefaultCellStyle.SelectionForeColor = Color.Red;
                }
                else if (status_id == (int)CustomerStatus.Blocked)
                {
                    item.DefaultCellStyle.ForeColor = Color.DarkOrange;
                    item.DefaultCellStyle.SelectionForeColor = Color.DarkOrange;
                }
                else if (status_id == (int)CustomerStatus.Pending)
                {
                    item.DefaultCellStyle.ForeColor = Color.DarkBlue;
                    item.DefaultCellStyle.SelectionForeColor = Color.DarkBlue;
                }
            }
        }
    }
       
}
