﻿using EPower.Base.Logic;
using EPower.Logic;
using EPower.Base.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using EPower.Base.Helper;
using System.Drawing;

namespace EPower.Interface
{
    public partial class DialogReceivePayment : ExDialog
    {
        TBL_CUSTOMER _objCustomer = null;
        TBL_CUSTOMER_METER _objCustomerMeter = null;
        TBL_METER _objMeter = null;
        TBL_USER_CASH_DRAWER _objUserCashDrawer = null;
        TBL_CASH_DRAWER _objCashDrawer = null;
        DataTable source = new DataTable();
        DataTable _dtInvoice = new DataTable();
        DataTable _dtDeposit = new DataTable();
        int _intCurrencyId = 0;
        int _accountId = 0;
        private bool _isCheckedPrepayment = false;
        TBL_PAYMENT_METHOD payMethod = new TBL_PAYMENT_METHOD();

        private bool _allowShow = true;
        public bool _blnIsPayByInvoice = false;
        public bool _blnIsCheckedPendingPayment = false;
        bool isEnableBankPayment = false;
        private bool _isFromPrepayment = false;

        public DialogReceivePayment()
        {
            InitializeComponent();

            // enable auto move to next customer
            var enableAutoFill = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_AUTO_FILL_DUE_AMOUNT]);
            this.chkAUTO_FILL_DUE_AMOUNT.Visible = enableAutoFill;

            this.isEnableBankPayment = DataHelper.ParseToBoolean(Method.Utilities[Utility.BANK_PAYMENT_ENABLE]);
            this.chkAUTO_FILL_DUE_AMOUNT.Checked = enableAutoFill; //Settings.Default.AUTO_FILL_DUE_AMOUNT;


            this.chkPRINT_RECEIPT.Checked = Properties.Settings.Default.PRINT_RECIEPT;
            this._objUserCashDrawer = Login.CurrentCashDrawer;

            this.txtPayDate.Enabled = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_EDIT_PAYMENT_DATE]) && SoftTech.Security.Logic.Login.IsAuthorized(Permission.POSTPAID_PAYMENT_DATE);
            this.txtPayDate.EditValue = DBDataContext.Db.GetSystemDate();

            // Accounts
            //var pAcc = AccountChartHelper.GetAccounts(new AccountSearchParam { Natures = new List<AccountNatures> { AccountNatures.Asset } });
            //this.cboPaymentMethod.SetDatasourceList(pAcc, nameof(AccountListModel.DisplayAccountName));
            //this.cboPaymentMethod.EditValue = 72;


            if (this._objUserCashDrawer != null)
            {
                this._objCashDrawer = DBDataContext.Db.TBL_CASH_DRAWERs.FirstOrDefault(row => row.CASH_DRAWER_ID == this._objUserCashDrawer.CASH_DRAWER_ID);
            }

            // if no cashdrawer is open then lock user edit.
            if (this._objCashDrawer != null)
            {
                this.txtLogin.Text = Login.CurrentLogin.LOGIN_NAME;
            }
            else
            {
                UIHelper.SetEnabled(this, false);
                this.btnOK.Enabled = false;
            }
            UIHelper.SetDataSourceToComboBox(cboCurrency, DBDataContext.Db.TLKP_CURRENCies.Select(x => new { x.CURRENCY_ID, x.CURRENCY_NAME }));
            UIHelper.SetDataSourceToComboBox(cboPaymentMethod, DBDataContext.Db.TBL_PAYMENT_METHODs.Where(x => x.IS_ACTIVE).Select(x => new { x.PAYMENT_METHOD_ID, x.PAYMENT_METHOD_NAME }));
        }

        public DialogReceivePayment(TBL_CUSTOMER objCustomer)
            : this()
        {
            _blnIsPayByInvoice = true;
            _objCustomer = objCustomer;
            txtCustomerCode.Text = _objCustomer.CUSTOMER_CODE;
            txtCustomerCode_AdvanceSearch(null, null);
            txtCustomerCode.Enabled =
            txtMeterCode.Enabled =
            txtCustomerName.Enabled = false;
        }

        public DialogReceivePayment(TBL_CUSTOMER objCustomer, bool isFromPrePayment)
            : this()
        {
            _blnIsPayByInvoice = true;
            _isFromPrepayment = isFromPrePayment;
            _objCustomer = objCustomer;
            txtCustomerCode.Text = _objCustomer.CUSTOMER_CODE;
            txtCustomerCode_AdvanceSearch(null, null);
            txtCustomerCode.Enabled =
            txtMeterCode.Enabled =
            txtCustomerName.Enabled = false;
            txtTotalPay.Focus();
        }

        private void newPayment()
        {
            this._objCustomer = null;
            this._objCustomerMeter = null;
            this._objMeter = null;

            this.txtCustomerCode.CancelSearch(false);
            this.txtCustomerName.CancelSearch(false);
            this.txtMeterCode.CancelSearch(false);

            txtPrepayBalance.Text = "";
            this.txtCustomerCode.Text = "";
            this.txtCustomerName.Text = "";
            this.txtMeterCode.Text = "";
            this.txtAreaName.Text = "";
            this.txtPole.Text = "";
            this.txtBox.Text = "";

            // clear date only when user don't change the date.
            //if (this.txtPayDate.Value.Date == DBDataContext.Db.GetSystemDate().Date)
            //{
            //  this.txtPayDate.ClearValue();
            //}

            this.txtTotalDue.Text = "";
            this.txtTotalPay.Text = "";
            this.txtTotalBalance.Text = "";
            this.source.Rows.Clear();
            this.chkPARTIAL_PAYMENT.Checked = true;
            this.txtCustomerCode.Focus();
            this.Refresh();
        }

        /// <summary>
        /// Reads object to display to the form.
        /// This form require tree objects to be not null
        /// this._objCustomer,
        /// this._objCustomerMeter, and 
        /// this._objMeter
        /// </summary>
        private void read()
        {
            if (cboCurrency.SelectedIndex == -1)
            {
                cboCurrency.SetValidation(string.Format(Resources.REQUIRED, lblCURRENCY.Text));
                return;
            }

            // accpet entry.
            this.txtCustomerCode.AcceptSearch(false);
            this.txtCustomerName.AcceptSearch(false);
            this.txtMeterCode.AcceptSearch(false);

            //get customer currency
            _intCurrencyId = (int)cboCurrency.SelectedValue;

            // show customer information.
            this.txtCustomerName.Text = this._objCustomer.LAST_NAME_KH + " " + this._objCustomer.FIRST_NAME_KH;
            this.txtCustomerCode.Text = this._objCustomer.CUSTOMER_CODE;
            this.txtAreaName.Text = DBDataContext.Db.TBL_AREAs.Where(row => row.AREA_ID == this._objCustomer.AREA_ID).FirstOrDefault().AREA_NAME;
            if (this._objMeter != null)
            {
                this.txtMeterCode.Text = this._objMeter.METER_CODE;
                this.txtPole.Text = DBDataContext.Db.TBL_POLEs.Where(row => row.POLE_ID == this._objCustomerMeter.POLE_ID).FirstOrDefault().POLE_CODE;
                this.txtBox.Text = DBDataContext.Db.TBL_BOXes.Where(row => row.BOX_ID == this._objCustomerMeter.BOX_ID).FirstOrDefault().BOX_CODE;
            }

            // invoice.
            var query = (from inv in DBDataContext.Db.TBL_INVOICEs
                         join cus in DBDataContext.Db.TBL_CUSTOMERs on inv.CUSTOMER_ID equals cus.CUSTOMER_ID
                         join c in DBDataContext.Db.TLKP_CURRENCies on inv.CURRENCY_ID equals c.CURRENCY_ID
                         where (inv.CUSTOMER_ID == this._objCustomer.CUSTOMER_ID || cus.INVOICE_CUSTOMER_ID == this._objCustomer.CUSTOMER_ID)
                               && (inv.INVOICE_STATUS == (int)InvoiceStatus.Open || inv.INVOICE_STATUS == (int)InvoiceStatus.Pay)
                         //&& inv.CURRENCY_ID ==_intCurrencyId
                         orderby inv.INVOICE_DATE
                         select new
                         {
                             INVOICE_ID = inv.INVOICE_ID,
                             INVOICE_NO = inv.INVOICE_NO,
                             INVOICE_DATE = inv.INVOICE_DATE,
                             INVOICE_TITLE = inv.INVOICE_TITLE,
                             SETTLE_AMOUNT = inv.SETTLE_AMOUNT,
                             PAID_AMOUNT = inv.PAID_AMOUNT,
                             DUE_AMOUNT = inv.SETTLE_AMOUNT - inv.PAID_AMOUNT,
                             c.CURRENCY_SING,
                             IS_INVOICE = true,
                             IS_PAY = true,
                             c.CURRENCY_ID,
                             c.FORMAT
                         }).ToList();

            _dtInvoice = new DataTable();
            _dtInvoice = query._ToDataTable();
            _dtDeposit = _dtInvoice.Clone();
            foreach (
                var objCusDeposit in (from d in DBDataContext.Db.TBL_CUS_DEPOSITs
                                      join c in DBDataContext.Db.TLKP_CURRENCies on d.CURRENCY_ID equals c.CURRENCY_ID
                                      where d.DEPOSIT_ACTION_ID == (int)DepositAction.AddDeposit && d.IS_PAID == false
                                      && d.CUSTOMER_ID == _objCustomer.CUSTOMER_ID
                                      //&& d.CURRENCY_ID==_intCurrencyId
                                      select new { d, c.CURRENCY_ID, c.CURRENCY_SING }).ToList())
            {
                //Customer have deposit to settle
                if (objCusDeposit != null)
                {
                    DataRow drDepost = _dtDeposit.NewRow();
                    drDepost[this.INVOICE_ID.Name] = objCusDeposit.d.CUS_DEPOSIT_ID;
                    drDepost[this.INVOICE_NO.Name] = objCusDeposit.d.DEPOSIT_NO;
                    drDepost[this.INVOICE_DATE.Name] = objCusDeposit.d.DEPOSIT_DATE;
                    drDepost[this.INVOICE_TITLE.Name] = Resources.DEPOSIT;
                    drDepost[this.SETTLE_AMOUNT.Name] = objCusDeposit.d.AMOUNT;
                    drDepost[this.PAID_AMOUNT.Name] = 0;
                    drDepost[this.DUE_AMOUNT.Name] = objCusDeposit.d.AMOUNT;
                    drDepost[this.CURRENCY_SING_.DataPropertyName] = objCusDeposit.CURRENCY_SING;
                    drDepost[this.CHK_.DataPropertyName] = true;
                    drDepost[this.IS_INVOICE.Name] = false;
                    drDepost[this.CURRENCY_ID.Name] = objCusDeposit.CURRENCY_ID;
                    _dtDeposit.Rows.Add(drDepost);
                }
            }

            this.source = _dtDeposit.Copy();
            this.source.Merge(_dtInvoice);

            int oldSelect = cboCurrency.SelectedIndex;
            int newSelect = 0;
            var t = source.Select().GroupBy(x => x["CURRENCY_ID"]);
            if (t.Count() == 1)
            {
                cboCurrency.SelectedValue = (int)t.FirstOrDefault().Key;
                newSelect = cboCurrency.SelectedIndex;
                //cboCurrency.Enabled = false;
            }
            else
            {
                cboCurrency.SelectedValue = DBDataContext.Db.TLKP_CURRENCies.Where(x => x.IS_DEFAULT_CURRENCY).FirstOrDefault().CURRENCY_ID;
                //cboCurrency.Enabled = true;
            }
            if (oldSelect == newSelect)
            {
                cboCurrency_SelectedValueChanged(cboCurrency, EventArgs.Empty);
            }
            //var currency = StaticVariable.Currencies.FirstOrDefault(x => x.CURRENCY_ID == _intCurrencyId);
            //var defaultCurrency = StaticVariable.Currencies.FirstOrDefault(x=>x.IS_DEFAULT_CURRENCY);
            //var format = defaultCurrency.FORMAT;
            //if(currency != null)
            //{
            //    format = currency.FORMAT;
            //}

            //this.txtPrepayBalance.Text = new PrepaymentLogic().GetCustomerPrepayment(_objCustomer.CUSTOMER_ID, _intCurrencyId).ToString(format); 
            formatCurrency();
            var currSelectedDate = txtPayDate.DateTime.Date;
            txtPayDate.EditValue = new DateTime(currSelectedDate.Year, currSelectedDate.Month, currSelectedDate.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
        }

        public decimal getTotalDue(bool blnInvoiceOnly)
        {
            decimal tmp = 0.0m;
            foreach (DataGridViewRow row in this.dgv.Rows)
            {
                //Only sum all invoice due amount
                if ((!blnInvoiceOnly || DataHelper.ParseToBoolean(row.Cells[this.IS_INVOICE.Name].Value.ToString()))
                    && DataHelper.ParseToBoolean(row.Cells[this.CHK_.Name].Value.ToString()))
                {
                    tmp += (decimal)row.Cells[this.DUE_AMOUNT.Name].Value;
                }
            }
            return tmp;
        }

        public decimal getTotalDepositDue()
        {
            decimal tmp = 0.0m;
            foreach (DataGridViewRow row in this.dgv.Rows)
            {
                //Only sum all deposit amount that check 
                if (!DataHelper.ParseToBoolean(row.Cells[this.IS_INVOICE.Name].Value.ToString())
                    && DataHelper.ParseToBoolean(row.Cells[this.CHK_.Name].Value.ToString()))
                {
                    tmp += (decimal)row.Cells[this.DUE_AMOUNT.Name].Value;
                }
            }
            return tmp;
        }

        public decimal getTotalPay()
        {
            return UIHelper.Round(DataHelper.ParseToDecimal(this.txtTotalPay.Text), this._intCurrencyId);
        }

        public decimal getTotalBalance()
        {
            return this.getTotalPay() - getTotalDue(false);
        }

        private bool invalid()
        {
            bool val = false;
            this.ClearAllValidation();
            payMethod = DBDataContext.Db.TBL_PAYMENT_METHODs.ToList().FirstOrDefault(x => x.PAYMENT_METHOD_ID == (int)cboPaymentMethod.SelectedValue);

            if (this._objCustomer == null)
            {
                this.txtCustomerCode.SetValidation(string.Format(Resources.REQUIRED, this.lblCUSTOMER_CODE.Text));
                return true;
            }

            if (!DataHelper.IsNumber(this.txtTotalPay.Text))
            {
                this.txtTotalPay.SetValidation(string.Format(Resources.REQUIRED, this.lblPAY_AMOUNT.Text));
                val = true;
            }
            if (DataHelper.ParseToDecimal(txtTotalPay.Text) < 0)
            {
                this.txtTotalPay.SetValidation(string.Format(Resources.REQUEST_GRATER_THAN_ZERO, this.lblPAY_AMOUNT.Text));
                val = true;
            }

            if (this.getTotalPay() == 0 && this.txtTotalDue.Text != "")
            {
                this.txtTotalPay.SetValidation(string.Format(Resources.REQUIRED, this.lblPAY_AMOUNT.Text));
                val = true;
            }
            if (this.cboPaymentMethod.SelectedIndex == -1 || payMethod == null)
            {
                this.cboPaymentMethod.SetValidation(string.Format(Resources.REQUIRED, lblPAYMENT_METHOD.Text));
                val = true;
            }

            _accountId = _intCurrencyId == (int)Currency.KHR ? payMethod.ACCOUNT_ID_KHR : _intCurrencyId == (int)Currency.USD ? payMethod.ACCOUNT_ID_USD :
                            _intCurrencyId == (int)Currency.THB ? payMethod.ACCOUNT_ID_THB : _intCurrencyId == (int)Currency.VND ? payMethod.ACCOUNT_ID_VND : 0;
            if (_accountId <= 0)
            {
                if (MsgBox.ShowQuestion(String.Format(Resources.MSG_PAYMENT_METHOD_NOT_SET_ACCOUNT, cboCurrency.Text), Resources.INFORMATION) == DialogResult.Yes)
                {
                    new DialogPaymentMethod(payMethod, GeneralProcess.Update).ShowDialog();
                }
                val = true;
            }
            if (this.txtPayDate.EditValue == null)
            {
                this.txtPayDate.SetValidation(String.Format(Resources.REQUIRED, lblPAY_DATE.Text));
                val = true;
            }
            //Boolean chk = false;
            //foreach (DataGridViewRow row in this.dgv.Rows)
            //{
            //    if (Convert.ToBoolean((row.Cells[this.CHK_.Name] as DataGridViewCheckBoxCell).Value) == true)
            //    {
            //        chk = true;
            //        break;
            //    }
            //}

            //if (chk == false)
            //{
            //    this.dgv.SetValidation(string.Format(Resources.REQUIRED_SELECT_VALUE,this.lblINVOICE_TO_PAY.Text));
            //    result = true;
            //}

            //if (this.getTotalBalance() < 0)
            //{
            //    this.txtTotalPay.SetValidation(Resources.MS_PAYMENT_OVER_AMOUNT);
            //    result = true;
            //}

            if (this.chkPARTIAL_PAYMENT.Checked && this.getTotalDepositDue() > this.getTotalPay())
            {
                this.txtTotalPay.SetValidation(Resources.MS_PAID_AMOUNT_MUST_MORE_THAN_DEPOSIT);
                val = true;
            }

            if (this.chkPARTIAL_PAYMENT.Checked && this.getTotalBalance() != 0)
            {
                this.txtTotalPay.SetValidation(Resources.MS_PAID_FULL_AMOUNT);
                val = true;
            }

            return val;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtCustomerCode_AdvanceSearch(object sender, EventArgs e)
        {
            string customerCode = txtCustomerCode.Text;
            if (customerCode == "")
            {
                this.txtCustomerCode.CancelSearch(false);
                return;
            }

            //if (isEnableBankPayment)
            //{ 
            customerCode = txtCustomerCode.Text.Split('-').Count() > 1 ? txtCustomerCode.Text.Split('-')[1].ToString() : txtCustomerCode.Text;
            //}

            _objCustomer = DBDataContext.Db.TBL_CUSTOMERs.Where(row => row.CUSTOMER_CODE == Method.FormatCustomerCode(customerCode)).ToList().FirstOrDefault();
            if (_objCustomer == null)
            {
                this.txtCustomerCode.CancelSearch(false);
                MsgBox.ShowInformation(Resources.MS_CUSTOMER_NOT_EXISTS);
                return;
            }

            _objCustomerMeter = DBDataContext.Db.TBL_CUSTOMER_METERs.Where(row => row.CUSTOMER_ID == this._objCustomer.CUSTOMER_ID && row.IS_ACTIVE).ToList().FirstOrDefault();
            if (_objCustomerMeter == null)
            {
                this.txtCustomerCode.CancelSearch(false);
            }
            else
            {
                _objMeter = DBDataContext.Db.TBL_METERs.Where(row => row.METER_ID == this._objCustomerMeter.METER_ID).ToList().FirstOrDefault();
                if (_objMeter == null)
                {
                    this.txtCustomerCode.CancelSearch(false);
                }
            }
            checkPendingPayment(_objCustomer);
            this.read();
        }

        private void txtMeterCode_CancelAdvanceSearch(object sender, EventArgs e)
        {
            newPayment();
        }

        private void txtPayAmount_TextChanged(object sender, EventArgs e)
        {
            decimal dueAmount = DataHelper.ParseToDecimal(this.txtTotalDue.Text) - DataHelper.ParseToDecimal(this.txtTotalPay.Text);
            this.txtTotalBalance.Text = UIHelper.FormatCurrency(dueAmount, _intCurrencyId);
            this.lblREADING_TEXT_.Text = DataHelper.NumberToWord(DataHelper.ParseToDecimal(this.txtTotalPay.Text));
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            SupportTaker objTC = new SupportTaker(SupportTaker.Process.RecivePayment);
            if (objTC.Check())
            {
                return;
            }
            this.savePayment();
        }

        private void checkPendingPayment(TBL_CUSTOMER obj)
        {
            decimal? bankRemainAmount = DBDataContext.Db.TBL_BANK_PAYMENT_DETAILs
                                        .Where(x => x.CUSTOMER_ID == obj.CUSTOMER_ID && x.BANK != "NA" && !x.IS_VOID && x.RESULT == (int)BankPaymentImportResult.RecordSuccess).ToList()
                                        .Sum(s => (decimal?)s.PAY_AMOUNT - (decimal?)s.PAID_AMOUNT);
            if (bankRemainAmount != null && bankRemainAmount > 0)
            {
                if (MsgBox.ShowQuestion(string.Format(Resources.MS_CUSTOMER_PENDING_PAYMENT, UIHelper.FormatCurrency((decimal)bankRemainAmount)), Resources.WARNING) == DialogResult.Yes)
                {
                    BankPayment.DialogPendingPayment dig = new BankPayment.DialogPendingPayment();
                    if (dig.ShowDialog() == DialogResult.OK)
                    {
                        this._blnIsCheckedPendingPayment = true;
                    }
                }
            }
        }

        public bool IsValidShow()
        {
            return _allowShow;
        }

        private void SettingPayment(List<TBL_INVOICE> invoices)
        {
            invoices = invoices ?? new List<TBL_INVOICE>();

            if (!invoices.Any())
            {
                return;
            }
            var currencyId = DataHelper.ParseToInt(cboCurrency.SelectedValue?.ToString() ?? "0");
            var balance = PrepaymentLogic.Instance.GetCustomerPrepayment(_objCustomer.CUSTOMER_ID, currencyId);
            if (balance <= 0)
            {
                return;
            }

            if (_isCheckedPrepayment && _isFromPrepayment)
            {
                return;
            }
            var msg = MsgBox.ShowQuestion(Resources.DOYOUWANTTOUSEPREPAYMENT, Resources.INFORMATION);
            if (msg == DialogResult.Yes)
            {
                PrepaymentLogic.Instance.ClearPaymentWithPrepay(invoices);
                _isCheckedPrepayment = true;
                this._allowShow = false;
            }
            else
            {
                if (msg == DialogResult.Cancel)
                {
                    if (_isFromPrepayment)
                    {
                        this._allowShow = false;
                    }
                }
                return;
            }
            if (invoices.Sum(x => x.SETTLE_AMOUNT - x.PAID_AMOUNT) <= balance)
            {
                if (!_isFromPrepayment)
                {
                    newPayment();
                }
                else
                {
                    _dtInvoice.Rows.Clear();
                    dgv.DataSource = _dtInvoice;
                    Runner.Run(delegate ()
                    {
                        read();
                    });

                }
            }
            else
            {
                Runner.Run(delegate ()
                {
                    read();
                });
            }
        }
        private void savePayment()
        {

            if (invalid())
            {
                return;
            }

            // PROCESS PAYMENT
            // ========================
            // 1. SETTLE DEPOSIT FIRST
            // 2. ADD A RECORD PAYMENT.
            // 3. FOR EACH INVOICE.
            //    >> ADD PAYMENT_DETAIL.
            //    >> UPDATE INVOICE. 

            // amoun to pay FIFO.
            try
            {
                decimal remainAmount = this.getTotalPay();
                decimal overAmount = this.getTotalBalance();
                if (overAmount > 0)
                {
                    remainAmount = remainAmount - overAmount;
                }
                DateTime now = DBDataContext.Db.GetSystemDate();
                TBL_PAYMENT objPayment = new TBL_PAYMENT();
                int cus_PrepaymentId = 0;
                TBL_CUS_DEPOSIT objCusDeposit = new TBL_CUS_DEPOSIT();
                var isValid = true;
                var exchangeRate = new ExchangeRateLogic().findLastExchangeRate(this.txtPayDate.DateTime, _intCurrencyId);

                var cashDrawerId = this._objUserCashDrawer.USER_CASH_DRAWER_ID;
                var loginName = SoftTech.Security.Logic.Login.CurrentLogin.LOGIN_NAME;


                if (overAmount > 0)
                {
                    if (MsgBox.ShowQuestion(Resources.MS_PREPAYMENT_DIALOG_MESSAGE, Resources.WARNING) != DialogResult.Yes)
                    {
                        btnOK.Enabled = true;
                        isValid = false;
                        return;
                    }
                    else
                    {
                        isValid = true;
                    }
                }

                Runner.Run(delegate ()
                {
                    using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                    {
                        // deposit
                        foreach (DataGridViewRow dgvr in dgv.Rows)
                        {
                            bool blnIsInvoice = DataHelper.ParseToBoolean(dgvr.Cells[this.IS_INVOICE.Name].Value.ToString());
                            bool blnIsCheck = DataHelper.ParseToBoolean(dgvr.Cells[this.CHK_.Name].Value.ToString());
                            // 2018-Jan-26 Update by Morm Raksmey :  if user not check Deposit will not pay deposit
                            if (!blnIsCheck || blnIsInvoice)
                            {
                                continue;
                            }
                            int intCusDepID = DataHelper.ParseToInt(dgvr.Cells[this.INVOICE_ID.Name].Value.ToString());
                            TBL_CUS_DEPOSIT objOld = DBDataContext.Db.TBL_CUS_DEPOSITs.Where(row => row.CUS_DEPOSIT_ID == intCusDepID).ToList().FirstOrDefault();

                            objOld._CopyTo(objCusDeposit);

                            objCusDeposit.IS_PAID = true;
                            objCusDeposit.DEPOSIT_DATE = now;
                            objCusDeposit.USER_CASH_DRAWER_ID = cashDrawerId;
                            objCusDeposit.CREATE_BY = loginName;
                            DBDataContext.Db.Update(objOld, objCusDeposit);
                            DBDataContext.Db.SubmitChanges();
                            remainAmount = remainAmount - objCusDeposit.AMOUNT;
                        }

                        //SETTLE FOR INVOICE PAYMENT
                        if (_dtInvoice.Rows.Count > 0 && remainAmount != 0)
                        {
                            // 1. ADD PAYMENT.
                            // TBL_CHANGE_LOG log = new TBL_CHANGE_LOG();
                            objPayment = new TBL_PAYMENT()
                            {
                                CREATE_BY = SoftTech.Security.Logic.Login.CurrentLogin.LOGIN_NAME,
                                CREATE_ON = now,
                                CURRENCY_ID = _intCurrencyId,
                                PAY_DATE = this.txtPayDate.DateTime,
                                CUSTOMER_ID = this._objCustomer.CUSTOMER_ID,
                                IS_ACTIVE = true,
                                PAY_AMOUNT = (remainAmount),
                                DUE_AMOUNT = this.getTotalDue(true),
                                PAYMENT_ID = 0,
                                // do it at last
                                PAYMENT_NO = "",// Method.GetNextSequence(Sequence.Receipt, false),
                                USER_CASH_DRAWER_ID = this._objUserCashDrawer.USER_CASH_DRAWER_ID,
                                PAYMENT_ACCOUNT_ID = _accountId,
                                //PAYMENT_TYPE_ID = 1,
                                EXCHANGE_RATE = exchangeRate.EXCHANGE_RATE,
                                EXCHANGE_RATE_DATE = exchangeRate.CREATE_ON,
                                PAYMENT_METHOD_ID = payMethod.PAYMENT_METHOD_ID
                            };
                            DBDataContext.Db.TBL_PAYMENTs.InsertOnSubmit(objPayment);
                            DBDataContext.Db.SubmitChanges();

                            foreach (DataGridViewRow dgvr in dgv.Rows)
                            {
                                bool blnIsInvoice = DataHelper.ParseToBoolean(dgvr.Cells[this.IS_INVOICE.Name].Value.ToString());
                                bool blnIsPay = DataHelper.ParseToBoolean(dgvr.Cells[this.CHK_.Name].Value.ToString());
                                if (!blnIsInvoice || !blnIsPay)
                                {
                                    continue;
                                }
                                TBL_INVOICE objInvoice = DBDataContext.Db.TBL_INVOICEs.Where(inv => inv.INVOICE_ID == (long)dgvr.Cells[this.INVOICE_ID.Name].Value).ToList().FirstOrDefault();
                                // calculate due amount and pay amount
                                decimal dueAmount = (decimal)dgvr.Cells[this.DUE_AMOUNT.Name].Value;
                                decimal payAmount;
                                if (remainAmount >= dueAmount)
                                {
                                    payAmount = dueAmount;
                                    remainAmount -= dueAmount;
                                }
                                else
                                {
                                    payAmount = remainAmount;
                                    remainAmount = 0;
                                }

                                // add payment detail.
                                TBL_PAYMENT_DETAIL objPaymentDetail = new TBL_PAYMENT_DETAIL()
                                {
                                    PAYMENT_DETAIL_ID = 0,
                                    PAYMENT_ID = objPayment.PAYMENT_ID,
                                    INVOICE_ID = objInvoice.INVOICE_ID,
                                    DUE_AMOUNT = dueAmount,
                                    PAY_AMOUNT = payAmount
                                };
                                DBDataContext.Db.TBL_PAYMENT_DETAILs.InsertOnSubmit(objPaymentDetail);
                                DBDataContext.Db.SubmitChanges();

                                // update invoice.
                                TBL_INVOICE objInvoiceBackup = new TBL_INVOICE();
                                objInvoice._CopyTo(objInvoiceBackup);
                                objInvoice.PAID_AMOUNT += payAmount;
                                objInvoice.INVOICE_STATUS = objInvoice.SETTLE_AMOUNT == objInvoice.PAID_AMOUNT ? (int)InvoiceStatus.Close : (int)InvoiceStatus.Open;
                                objInvoice.ROW_DATE = now; // update ROW_DATE for B24 Intergration 
                                DBDataContext.Db.UpdateWithoutChangeLog(objInvoiceBackup, objInvoice);
                                DBDataContext.Db.SubmitChanges();
                                if (remainAmount == 0)
                                {
                                    break;
                                }
                            }
                        }

                        if (overAmount > 0 && isValid)
                        {
                            // Add prepayment
                            TBL_CUS_PREPAYMENT _objCusPrepayment = new TBL_CUS_PREPAYMENT();
                            var receiptNo = objPayment.PAYMENT_NO;
                            if (string.IsNullOrEmpty(objPayment.PAYMENT_NO))
                            {
                                receiptNo = Method.GetNextSequence(Sequence.Receipt, true);
                            }
                            var PAYMENT_ACCOUNT_ID = _accountId;
                            decimal oldBalance = DBDataContext.Db.TBL_CUS_PREPAYMENTs.Where(x => x.CUSTOMER_ID == _objCustomer.CUSTOMER_ID && x.CURRENCY_ID == _intCurrencyId).OrderByDescending(x => x.CREATE_ON).Select(x => x.BALANCE).FirstOrDefault();
                            _objCusPrepayment = new PrepaymentLogic().AddCustomerPrepayment(txtPayDate.DateTime, _objCustomer.CUSTOMER_ID, overAmount, (oldBalance + overAmount), _intCurrencyId, receiptNo, "", PrepaymentAction.AddPrepayment, objPayment.PAYMENT_ID);
                            _objCusPrepayment.PAYMENT_ACCOUNT_ID = PAYMENT_ACCOUNT_ID;
                            DBDataContext.Db.Insert(_objCusPrepayment);
                            DBDataContext.Db.SubmitChanges();
                            cus_PrepaymentId = _objCusPrepayment.CUS_PREPAYMENT_ID;
                        }

                        //var paymentNo = Method.GetNextSequence(Sequence.Receipt, true);
                        //DBDataContext.Db.SubmitChanges(); 
                        //var paymentNo = DBDataContext.Db.ExecuteQuery<string>($"EXEC dbo.NEXT_SEQUENCE @SEQUENCE_ID ={(int)Sequence.Receipt}").FirstOrDefault();
                        var paymentNo = Method.GetNextSequence(Sequence.Receipt, true);
                        var newPayment = new TBL_PAYMENT();
                        Copier.CopyTo<TBL_PAYMENT>(objPayment, newPayment);
                        newPayment.PAYMENT_NO = paymentNo;
                        DBDataContext.Db.UpdateWithoutChangeLog(objPayment, newPayment);
                        DBDataContext.Db.SubmitChanges();
                        tran.Complete();
                    }
                });
                //RENEW PAYMENT
                this.newPayment();
                this.txtPrepayBalance.Text = "";

                if (chkPRINT_RECEIPT.Checked)
                {
                    foreach (DataRow drDeposit in _dtDeposit.Rows)
                    {
                        Printing.DepositReceipt(DataHelper.ParseToInt(drDeposit[this.INVOICE_ID.Name].ToString()));
                    }
                    if (objPayment.PAYMENT_ID != 0)
                    {
                        Printing.ReceiptPayment((int)objPayment.PAYMENT_ID);
                    }
                    else
                    {
                        if (overAmount > 0)
                        {
                            CrystalReportHelper ch = new CrystalReportHelper("ReportReceiptPrepayment.rpt");
                            ch.SetParameter("CUS_PREPAYMENT_ID", cus_PrepaymentId);
                            ch.PrintReport(Properties.Settings.Default.PRINTER_RECIEPT);
                        }
                    }
                }
                if (_blnIsPayByInvoice)
                {
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                this.btnOK.Enabled = false;
                MsgBox.LogError(ex);
                MsgBox.ShowWarning((string.Format(Resources.YOU_CANNOT_PROCESS, "", Resources.RECEIVE_PAYMENT)), Resources.WARNING);
            }
        }

        private void txtPayAmount_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                this.savePayment();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                this.newPayment();
            }
        }

        private void txtMeterCode_AdvanceSearch(object sender, EventArgs e)
        {
            // if not entry.
            if (txtMeterCode.Text.Trim() == "")
            {
                this.txtMeterCode.CancelSearch(false);
                return;
            }

            string strMeterCode = Method.FormatMeterCode(this.txtMeterCode.Text);
            this._objMeter = DBDataContext.Db.TBL_METERs.FirstOrDefault(m => m.METER_CODE == strMeterCode);

            // if not contain in database.
            if (this._objMeter == null)
            {
                MsgBox.ShowInformation(Resources.MS_METER_CODE_NOT_FOUND);
                this.txtMeterCode.CancelSearch(false);
                return;
            }

            // if meter is inuse.
            if (this._objMeter.STATUS_ID != (int)MeterStatus.Used)
            {
                MsgBox.ShowInformation(Resources.MS_METER_STATUS_IN_USE);
                this.txtMeterCode.CancelSearch(false);
                return;
            }

            this._objCustomerMeter = DBDataContext.Db.TBL_CUSTOMER_METERs.FirstOrDefault(row => row.IS_ACTIVE && row.METER_ID == this._objMeter.METER_ID);

            this._objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == this._objCustomerMeter.CUSTOMER_ID);
            checkPendingPayment(_objCustomer);
            this.read();

        }

        private void txtCustomerName_AdvanceSearch(object sender, EventArgs e)
        {
            DialogCustomerSearch diag = new DialogCustomerSearch(this.txtCustomerName.Text, DialogCustomerSearch.PowerType.AllType);
            if (diag.ShowDialog() != DialogResult.OK)
            {
                this.txtCustomerName.CancelSearch(false);
                return;
            }
            this._objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == diag.CustomerID);
            if (_objCustomer == null)
            {
                this.txtCustomerName.CancelSearch(false);
                return;
            }
            _objCustomerMeter = DBDataContext.Db.TBL_CUSTOMER_METERs.FirstOrDefault(row => row.CUSTOMER_ID == this._objCustomer.CUSTOMER_ID && row.IS_ACTIVE);
            if (_objCustomerMeter == null)
            {
                this.txtCustomerName.CancelSearch(false);
            }
            else
            {
                _objMeter = DBDataContext.Db.TBL_METERs.FirstOrDefault(row => row.METER_ID == this._objCustomerMeter.METER_ID);
                if (_objMeter == null)
                {
                    this.txtCustomerName.CancelSearch(false);
                }
            }
            checkPendingPayment(_objCustomer);
            this.read();
        }

        private void InputEnglish(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void InputKhmer(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1 && e.ColumnIndex == dgv.Columns[this.CHK_.Name].Index
          // && (bool)dgv.Rows[e.RowIndex].Cells[this.IS_INVOICE.Name].Value
          )
            {
                dgv.Rows[e.RowIndex].Cells[this.CHK_.Name].Value = !(bool)dgv.Rows[e.RowIndex].Cells[this.CHK_.Name].Value;
                this.txtTotalDue.Text = UIHelper.FormatCurrency(this.getTotalDue(false), _intCurrencyId);

                decimal dueAmount = DataHelper.ParseToDecimal(this.txtTotalDue.Text) - DataHelper.ParseToDecimal(this.txtTotalPay.Text);
                this.txtTotalBalance.Text = UIHelper.FormatCurrency(dueAmount, _intCurrencyId);
            }
        }

        private void chkCheckAll_CheckedChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in this.dgv.Rows)
            {
                if ((bool)row.Cells[this.IS_INVOICE.Name].Value)
                {
                    row.Cells[this.CHK_.Name].Value = this.chkCHECK_ALL_.Checked;
                }
            }
            this.txtTotalDue.Text = UIHelper.FormatCurrency(this.getTotalDue(false), _intCurrencyId);
            decimal dueAmount = DataHelper.ParseToDecimal(this.txtTotalDue.Text) - DataHelper.ParseToDecimal(this.txtTotalPay.Text);
            this.txtTotalBalance.Text = UIHelper.FormatCurrency(dueAmount, _intCurrencyId);

        }

        private void chkPrintReceipt_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.PRINT_RECIEPT = this.chkPRINT_RECEIPT.Checked;
            Properties.Settings.Default.Save();
        }

        private void chkAUTO_FILL_DUE_AMOUNT_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.AUTO_FILL_DUE_AMOUNT = this.chkAUTO_FILL_DUE_AMOUNT.Checked;
            Properties.Settings.Default.Save();
        }

        private void cboCurrency_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cboCurrency.SelectedIndex == -1)
            {
                return;
            }
            _intCurrencyId = (int)cboCurrency.SelectedValue;

            var customerDepositAmount = 0m;
            if (_objCustomer != null)
            {
                customerDepositAmount = new PrepaymentLogic().GetCustomerPrepayment(_objCustomer.CUSTOMER_ID, _intCurrencyId);
            }
            this.txtPrepayBalance.Text = customerDepositAmount.ToString(DataHelper.MoneyFormat);
            txtSignTotalDue.Text = txtSignDepositBalance.Text = txtSignTotalPay.Text = txtSignTotalBalance.Text = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.CURRENCY_ID == _intCurrencyId).CURRENCY_SING;
            txtTotalDue.Text = txtTotalBalance.Text = txtTotalPay.Text = "";
            if (this.source.Rows.Count == 0)
            {
                txtTotalPay.Focus();
                return;
            }


            this.source.DefaultView.RowFilter = string.Format("CURRENCY_ID={0}", _intCurrencyId);
            this.dgv.DataSource = this.source;

            if (this.dgv.Rows.Count == 0 && !_isFromPrepayment)
            {
                txtTotalPay.Focus();
                //MsgBox.ShowInformation(Resources.MS_CUSTOMER_NO_INVOICE);
                return;
            }

            if (this.dgv.Rows.Count > 0)
            {
                txtTotalPay.Focus();
            }

            txtTotalPay.Focus();

            foreach (DataGridViewRow dgvr in dgv.Rows)
            {
                bool blnIsInvoice = DataHelper.ParseToBoolean(dgvr.Cells[this.IS_INVOICE.Name].Value.ToString());
                if (!blnIsInvoice)
                {
                    dgvr.ReadOnly = true;
                }
            }

            this.txtTotalDue.Text = UIHelper.FormatCurrency(this.getTotalDue(false), _intCurrencyId);
            this.txtTotalBalance.Text = UIHelper.FormatCurrency(this.getTotalDue(false), _intCurrencyId);
            if (this.chkAUTO_FILL_DUE_AMOUNT.Checked && chkAUTO_FILL_DUE_AMOUNT.Visible)
            {
                this.txtTotalPay.Text = this.txtTotalBalance.Text;
            }

            // if user doesn't change the date so use system datetime
            // but keep last change date if user change
            if (this.txtPayDate.DateTime.Date == UIHelper._DefaultDate.Date)
            {
                this.txtPayDate.EditValue = DBDataContext.Db.GetSystemDate();
            }

            this.chkCHECK_ALL_.Checked = true;

            //this.btnOK.Enabled = true;
            formatCurrency();

            //If has any prepayment => check if user want to settle invoice using prepayment
            var invoices = GetInvoices();
            SettingPayment(invoices);
        }

        private void dgv_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            var formatAmount = dgv.Rows[e.RowIndex].Cells[FORMAT.Name].Value?.ToString() ?? "";
            if (e.ColumnIndex != dgv.Columns[SETTLE_AMOUNT.Name].Index && e.ColumnIndex != dgv.Columns[PAID_AMOUNT.Name].Index && e.ColumnIndex != dgv.Columns[DUE_AMOUNT.Name].Index)
            {
                return;
            }
            e.CellStyle.Format = formatAmount;
        }
        private void formatCurrency()
        {
            var currency = SettingLogic.Currencies.FirstOrDefault(x => x.CURRENCY_ID == _intCurrencyId);
            var defaultCurrency = SettingLogic.Currencies.FirstOrDefault(x => x.IS_DEFAULT_CURRENCY);
            var format = defaultCurrency.FORMAT;
            if (currency != null)
            {
                format = currency.FORMAT;
            }

            if (_isFromPrepayment)
            {
                this.txtPrepayBalance.Text = new PrepaymentLogic().GetCustomerPrepayment(_objCustomer.CUSTOMER_ID, _intCurrencyId).ToString(format);
            }
        }

        private List<TBL_INVOICE> GetInvoices()
        {
            DateTime now = DBDataContext.Db.GetSystemDate();
            var invoices = new List<TBL_INVOICE>();
            foreach (DataGridViewRow dgvr in dgv.Rows)
            {
                bool blnIsInvoice = DataHelper.ParseToBoolean(dgvr.Cells[this.IS_INVOICE.Name].Value.ToString());
                bool blnIsPay = DataHelper.ParseToBoolean(dgvr.Cells[this.CHK_.Name].Value.ToString());
                if (!blnIsInvoice || !blnIsPay)
                {
                    continue;
                }
                TBL_INVOICE objInvoice = DBDataContext.Db.TBL_INVOICEs.FirstOrDefault(inv => inv.INVOICE_ID == (long)dgvr.Cells[this.INVOICE_ID.Name].Value);
                // calculate due amount and pay amount
                decimal dueAmount = (decimal)dgvr.Cells[this.DUE_AMOUNT.Name].Value;
                // update invoice.
                TBL_INVOICE objInvoiceBackup = new TBL_INVOICE();
                objInvoice._CopyTo(objInvoiceBackup);
                objInvoice.INVOICE_STATUS = objInvoice.SETTLE_AMOUNT == objInvoice.PAID_AMOUNT ? (int)InvoiceStatus.Close : (int)InvoiceStatus.Open;
                objInvoice.ROW_DATE = now;
                invoices.Add(objInvoice);
            }
            return invoices;
        }

        private void DialogReceivePayment_Activated_1(object sender, EventArgs e)
        {
            if (_isFromPrepayment)
            {
                this.txtTotalPay.Focus();
            }
        }

        private void DialogReceivePayment_Load(object sender, EventArgs e)
        {
            if (this.chkAUTO_FILL_DUE_AMOUNT.Checked && chkAUTO_FILL_DUE_AMOUNT.Visible)
            {
                this.txtTotalPay.Text = this.txtTotalBalance.Text;
            }
        }

        private void txtPayDate_EditValueChanged(object sender, EventArgs e)
        {
            var systemDate = DBDataContext.Db.GetSystemDate().Date;
            var valueDate = txtPayDate.DateTime.Date;
            if ((valueDate.Date - systemDate.Date).Days != 0)
            {
                txtPayDate.ForeColor = Color.Red;
            }
            else
            {
                txtPayDate.ForeColor = Color.Black;
            }
        }
    }
}
