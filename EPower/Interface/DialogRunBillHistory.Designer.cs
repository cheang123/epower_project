﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogRunBillHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMONTH = new System.Windows.Forms.Label();
            this.lblDUE_DATE = new System.Windows.Forms.Label();
            this.lblSTART_DATE = new System.Windows.Forms.Label();
            this.dtpRunMonth = new System.Windows.Forms.DateTimePicker();
            this.dtpFrom = new System.Windows.Forms.DateTimePicker();
            this.lblEND_DATE = new System.Windows.Forms.Label();
            this.dtpTo = new System.Windows.Forms.DateTimePicker();
            this.dtpDueDate = new System.Windows.Forms.DateTimePicker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnSAVE = new SoftTech.Component.ExButton();
            this.lblCYCLE_NAME = new System.Windows.Forms.Label();
            this.cboBillingCycle = new System.Windows.Forms.ComboBox();
            this.dtpSTART_PAY_DATE = new System.Windows.Forms.DateTimePicker();
            this.lblPAY_DATE = new System.Windows.Forms.Label();
            this.lblINVOICE_DATE_BILLING = new System.Windows.Forms.Label();
            this.dtpInvoiceDate = new System.Windows.Forms.DateTimePicker();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.lblPAY_DATE);
            this.content.Controls.Add(this.dtpSTART_PAY_DATE);
            this.content.Controls.Add(this.cboBillingCycle);
            this.content.Controls.Add(this.lblCYCLE_NAME);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.btnSAVE);
            this.content.Controls.Add(this.dtpInvoiceDate);
            this.content.Controls.Add(this.dtpDueDate);
            this.content.Controls.Add(this.dtpTo);
            this.content.Controls.Add(this.dtpFrom);
            this.content.Controls.Add(this.dtpRunMonth);
            this.content.Controls.Add(this.lblINVOICE_DATE_BILLING);
            this.content.Controls.Add(this.lblSTART_DATE);
            this.content.Controls.Add(this.lblDUE_DATE);
            this.content.Controls.Add(this.lblMONTH);
            this.content.Controls.Add(this.lblEND_DATE);
            this.content.Size = new System.Drawing.Size(415, 260);
            this.content.TabIndex = 0;
            this.content.Text = "\'CONTENT\'";
            this.content.Controls.SetChildIndex(this.lblEND_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblMONTH, 0);
            this.content.Controls.SetChildIndex(this.lblDUE_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblSTART_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblINVOICE_DATE_BILLING, 0);
            this.content.Controls.SetChildIndex(this.dtpRunMonth, 0);
            this.content.Controls.SetChildIndex(this.dtpFrom, 0);
            this.content.Controls.SetChildIndex(this.dtpTo, 0);
            this.content.Controls.SetChildIndex(this.dtpDueDate, 0);
            this.content.Controls.SetChildIndex(this.dtpInvoiceDate, 0);
            this.content.Controls.SetChildIndex(this.btnSAVE, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.lblCYCLE_NAME, 0);
            this.content.Controls.SetChildIndex(this.cboBillingCycle, 0);
            this.content.Controls.SetChildIndex(this.dtpSTART_PAY_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblPAY_DATE, 0);
            // 
            // lblMONTH
            // 
            this.lblMONTH.AutoSize = true;
            this.lblMONTH.Location = new System.Drawing.Point(17, 24);
            this.lblMONTH.Margin = new System.Windows.Forms.Padding(2);
            this.lblMONTH.Name = "lblMONTH";
            this.lblMONTH.Size = new System.Drawing.Size(60, 25);
            this.lblMONTH.TabIndex = 6;
            this.lblMONTH.Text = "ប្រចាំខែ";
            // 
            // lblDUE_DATE
            // 
            this.lblDUE_DATE.AutoSize = true;
            this.lblDUE_DATE.Location = new System.Drawing.Point(17, 146);
            this.lblDUE_DATE.Margin = new System.Windows.Forms.Padding(2);
            this.lblDUE_DATE.Name = "lblDUE_DATE";
            this.lblDUE_DATE.Size = new System.Drawing.Size(93, 25);
            this.lblDUE_DATE.TabIndex = 7;
            this.lblDUE_DATE.Text = "ថ្ងៃផុតកំណត់";
            // 
            // lblSTART_DATE
            // 
            this.lblSTART_DATE.AutoSize = true;
            this.lblSTART_DATE.Location = new System.Drawing.Point(17, 84);
            this.lblSTART_DATE.Margin = new System.Windows.Forms.Padding(2);
            this.lblSTART_DATE.Name = "lblSTART_DATE";
            this.lblSTART_DATE.Size = new System.Drawing.Size(66, 25);
            this.lblSTART_DATE.TabIndex = 8;
            this.lblSTART_DATE.Text = "ចាប់ពីថ្ងៃ";
            // 
            // dtpRunMonth
            // 
            this.dtpRunMonth.CustomFormat = "MM-yyyy";
            this.dtpRunMonth.Enabled = false;
            this.dtpRunMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpRunMonth.Location = new System.Drawing.Point(166, 18);
            this.dtpRunMonth.Margin = new System.Windows.Forms.Padding(2);
            this.dtpRunMonth.Name = "dtpRunMonth";
            this.dtpRunMonth.Size = new System.Drawing.Size(229, 32);
            this.dtpRunMonth.TabIndex = 0;
            // 
            // dtpFrom
            // 
            this.dtpFrom.CustomFormat = "dd-MM-yyyy";
            this.dtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFrom.Location = new System.Drawing.Point(166, 80);
            this.dtpFrom.Margin = new System.Windows.Forms.Padding(2);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(103, 32);
            this.dtpFrom.TabIndex = 2;
            // 
            // lblEND_DATE
            // 
            this.lblEND_DATE.AutoSize = true;
            this.lblEND_DATE.Location = new System.Drawing.Point(267, 84);
            this.lblEND_DATE.Margin = new System.Windows.Forms.Padding(2);
            this.lblEND_DATE.Name = "lblEND_DATE";
            this.lblEND_DATE.Size = new System.Drawing.Size(39, 25);
            this.lblEND_DATE.TabIndex = 15;
            this.lblEND_DATE.Text = "ដល់";
            // 
            // dtpTo
            // 
            this.dtpTo.CustomFormat = "dd-MM-yyyy";
            this.dtpTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTo.Location = new System.Drawing.Point(292, 80);
            this.dtpTo.Margin = new System.Windows.Forms.Padding(2);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(103, 32);
            this.dtpTo.TabIndex = 3;
            // 
            // dtpDueDate
            // 
            this.dtpDueDate.CustomFormat = "dd-MM-yyyy";
            this.dtpDueDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDueDate.Location = new System.Drawing.Point(166, 142);
            this.dtpDueDate.Margin = new System.Windows.Forms.Padding(2);
            this.dtpDueDate.Name = "dtpDueDate";
            this.dtpDueDate.Size = new System.Drawing.Size(229, 32);
            this.dtpDueDate.TabIndex = 4;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Location = new System.Drawing.Point(1, 223);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(419, 1);
            this.panel1.TabIndex = 24;
            // 
            // btnCLOSE
            // 
            this.btnCLOSE.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnCLOSE.Location = new System.Drawing.Point(332, 230);
            this.btnCLOSE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.Size = new System.Drawing.Size(75, 23);
            this.btnCLOSE.TabIndex = 6;
            this.btnCLOSE.Text = "បោះបង់";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSAVE
            // 
            this.btnSAVE.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnSAVE.Location = new System.Drawing.Point(253, 230);
            this.btnSAVE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSAVE.Name = "btnSAVE";
            this.btnSAVE.Size = new System.Drawing.Size(75, 23);
            this.btnSAVE.TabIndex = 5;
            this.btnSAVE.Text = "រក្សាទុក";
            this.btnSAVE.UseVisualStyleBackColor = true;
            this.btnSAVE.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblCYCLE_NAME
            // 
            this.lblCYCLE_NAME.AutoSize = true;
            this.lblCYCLE_NAME.Location = new System.Drawing.Point(17, 53);
            this.lblCYCLE_NAME.Margin = new System.Windows.Forms.Padding(2);
            this.lblCYCLE_NAME.Name = "lblCYCLE_NAME";
            this.lblCYCLE_NAME.Size = new System.Drawing.Size(61, 25);
            this.lblCYCLE_NAME.TabIndex = 25;
            this.lblCYCLE_NAME.Text = "ជុំទូទាត់";
            // 
            // cboBillingCycle
            // 
            this.cboBillingCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBillingCycle.Enabled = false;
            this.cboBillingCycle.FormattingEnabled = true;
            this.cboBillingCycle.Location = new System.Drawing.Point(166, 49);
            this.cboBillingCycle.Margin = new System.Windows.Forms.Padding(2);
            this.cboBillingCycle.Name = "cboBillingCycle";
            this.cboBillingCycle.Size = new System.Drawing.Size(229, 33);
            this.cboBillingCycle.TabIndex = 1;
            // 
            // dtpSTART_PAY_DATE
            // 
            this.dtpSTART_PAY_DATE.CustomFormat = "dd-MM-yyyy";
            this.dtpSTART_PAY_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSTART_PAY_DATE.Location = new System.Drawing.Point(166, 173);
            this.dtpSTART_PAY_DATE.Margin = new System.Windows.Forms.Padding(2);
            this.dtpSTART_PAY_DATE.Name = "dtpSTART_PAY_DATE";
            this.dtpSTART_PAY_DATE.Size = new System.Drawing.Size(229, 32);
            this.dtpSTART_PAY_DATE.TabIndex = 27;
            // 
            // lblPAY_DATE
            // 
            this.lblPAY_DATE.AutoSize = true;
            this.lblPAY_DATE.Location = new System.Drawing.Point(17, 177);
            this.lblPAY_DATE.Margin = new System.Windows.Forms.Padding(2);
            this.lblPAY_DATE.Name = "lblPAY_DATE";
            this.lblPAY_DATE.Size = new System.Drawing.Size(152, 25);
            this.lblPAY_DATE.TabIndex = 29;
            this.lblPAY_DATE.Text = "ថ្ងៃចាប់ផ្តើមទទួលប្រាក់";
            // 
            // lblINVOICE_DATE_BILLING
            // 
            this.lblINVOICE_DATE_BILLING.AutoSize = true;
            this.lblINVOICE_DATE_BILLING.Location = new System.Drawing.Point(17, 115);
            this.lblINVOICE_DATE_BILLING.Margin = new System.Windows.Forms.Padding(2);
            this.lblINVOICE_DATE_BILLING.Name = "lblINVOICE_DATE_BILLING";
            this.lblINVOICE_DATE_BILLING.Size = new System.Drawing.Size(119, 25);
            this.lblINVOICE_DATE_BILLING.TabIndex = 7;
            this.lblINVOICE_DATE_BILLING.Text = "ថ្ងៃចេញវិក្កយបត្រ";
            // 
            // dtpInvoiceDate
            // 
            this.dtpInvoiceDate.CustomFormat = "dd-MM-yyyy";
            this.dtpInvoiceDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpInvoiceDate.Location = new System.Drawing.Point(166, 111);
            this.dtpInvoiceDate.Margin = new System.Windows.Forms.Padding(2);
            this.dtpInvoiceDate.Name = "dtpInvoiceDate";
            this.dtpInvoiceDate.Size = new System.Drawing.Size(229, 32);
            this.dtpInvoiceDate.TabIndex = 4;
            // 
            // btnCHANGE_LOG
            // 
            this.btnCHANGE_LOG.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnCHANGE_LOG.Location = new System.Drawing.Point(8, 230);
            this.btnCHANGE_LOG.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.Size = new System.Drawing.Size(75, 23);
            this.btnCHANGE_LOG.TabIndex = 5;
            this.btnCHANGE_LOG.Text = "ប្រវត្តិកែប្រែ";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnHistory_Click);
            // 
            // DialogRunBillHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(415, 283);
            this.Name = "DialogRunBillHistory";
            this.Text = "ចេញវិក្កយបត្រ";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Label lblSTART_DATE;
        private Label lblDUE_DATE;
        private Label lblMONTH;
        private Label lblEND_DATE;
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnSAVE;
        public DateTimePicker dtpDueDate;
        public DateTimePicker dtpTo;
        public DateTimePicker dtpFrom;
        public DateTimePicker dtpRunMonth;
        private Label lblCYCLE_NAME;
        public ComboBox cboBillingCycle;
        private Label lblPAY_DATE;
        public DateTimePicker dtpSTART_PAY_DATE;
        public DateTimePicker dtpInvoiceDate;
        private Label lblINVOICE_DATE_BILLING;
        private ExButton btnCHANGE_LOG;
    }
}