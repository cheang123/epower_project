﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogDepositAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogDepositAdd));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkPRINT_RECEIPT = new System.Windows.Forms.CheckBox();
            this.lblCUSTOMER_TYPE = new System.Windows.Forms.Label();
            this.lblAMPARE = new System.Windows.Forms.Label();
            this.lblPHASE = new System.Windows.Forms.Label();
            this.lblLAST_NAME_KH = new System.Windows.Forms.Label();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.lblPRICE = new System.Windows.Forms.Label();
            this.txtCustomerType = new System.Windows.Forms.TextBox();
            this.txtAmp = new System.Windows.Forms.TextBox();
            this.txtPhase = new System.Windows.Forms.TextBox();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.lblFIRST_NAME_KH = new System.Windows.Forms.Label();
            this.txtLastBalance = new System.Windows.Forms.TextBox();
            this.txtNode = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtAdjustAmount = new System.Windows.Forms.TextBox();
            this.lblBALANCE = new System.Windows.Forms.Label();
            this.lblAMOUNT = new System.Windows.Forms.Label();
            this.lblDEPOSIT_AMOUNT = new System.Windows.Forms.Label();
            this.txtCurrentBalance = new System.Windows.Forms.TextBox();
            this.chkIS_PAID = new System.Windows.Forms.CheckBox();
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.lblCURRENCY = new System.Windows.Forms.Label();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.dtpDepositDate = new System.Windows.Forms.DateTimePicker();
            this.lblDATE = new System.Windows.Forms.Label();
            this.lblNOTE = new System.Windows.Forms.Label();
            this.lblREQUEST_REFUND_DATE = new System.Windows.Forms.Label();
            this.dtpREQUEST_REFUND_DATE = new System.Windows.Forms.DateTimePicker();
            this.picHelp = new System.Windows.Forms.PictureBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picHelp)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.picHelp);
            this.content.Controls.Add(this.dtpREQUEST_REFUND_DATE);
            this.content.Controls.Add(this.lblREQUEST_REFUND_DATE);
            this.content.Controls.Add(this.dtpDepositDate);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.cboCurrency);
            this.content.Controls.Add(this.lblCURRENCY);
            this.content.Controls.Add(this.txtNode);
            this.content.Controls.Add(this.txtLastBalance);
            this.content.Controls.Add(this.chkIS_PAID);
            this.content.Controls.Add(this.groupBox2);
            this.content.Controls.Add(this.label12);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.chkPRINT_RECEIPT);
            this.content.Controls.Add(this.txtAdjustAmount);
            this.content.Controls.Add(this.txtCurrentBalance);
            this.content.Controls.Add(this.lblBALANCE);
            this.content.Controls.Add(this.lblDATE);
            this.content.Controls.Add(this.lblNOTE);
            this.content.Controls.Add(this.lblAMOUNT);
            this.content.Controls.Add(this.txtFirstName);
            this.content.Controls.Add(this.lblDEPOSIT_AMOUNT);
            this.content.Controls.Add(this.lblFIRST_NAME_KH);
            this.content.Controls.Add(this.txtPrice);
            this.content.Controls.Add(this.txtPhase);
            this.content.Controls.Add(this.txtAmp);
            this.content.Controls.Add(this.txtCustomerType);
            this.content.Controls.Add(this.txtLastName);
            this.content.Controls.Add(this.lblLAST_NAME_KH);
            this.content.Controls.Add(this.lblPRICE);
            this.content.Controls.Add(this.lblCUSTOMER_TYPE);
            this.content.Controls.Add(this.lblAMPARE);
            this.content.Controls.Add(this.lblPHASE);
            this.content.Controls.Add(this.btnOK);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.lblPHASE, 0);
            this.content.Controls.SetChildIndex(this.lblAMPARE, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_TYPE, 0);
            this.content.Controls.SetChildIndex(this.lblPRICE, 0);
            this.content.Controls.SetChildIndex(this.lblLAST_NAME_KH, 0);
            this.content.Controls.SetChildIndex(this.txtLastName, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerType, 0);
            this.content.Controls.SetChildIndex(this.txtAmp, 0);
            this.content.Controls.SetChildIndex(this.txtPhase, 0);
            this.content.Controls.SetChildIndex(this.txtPrice, 0);
            this.content.Controls.SetChildIndex(this.lblFIRST_NAME_KH, 0);
            this.content.Controls.SetChildIndex(this.lblDEPOSIT_AMOUNT, 0);
            this.content.Controls.SetChildIndex(this.txtFirstName, 0);
            this.content.Controls.SetChildIndex(this.lblAMOUNT, 0);
            this.content.Controls.SetChildIndex(this.lblNOTE, 0);
            this.content.Controls.SetChildIndex(this.lblDATE, 0);
            this.content.Controls.SetChildIndex(this.lblBALANCE, 0);
            this.content.Controls.SetChildIndex(this.txtCurrentBalance, 0);
            this.content.Controls.SetChildIndex(this.txtAdjustAmount, 0);
            this.content.Controls.SetChildIndex(this.chkPRINT_RECEIPT, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.label12, 0);
            this.content.Controls.SetChildIndex(this.groupBox2, 0);
            this.content.Controls.SetChildIndex(this.chkIS_PAID, 0);
            this.content.Controls.SetChildIndex(this.txtLastBalance, 0);
            this.content.Controls.SetChildIndex(this.txtNode, 0);
            this.content.Controls.SetChildIndex(this.lblCURRENCY, 0);
            this.content.Controls.SetChildIndex(this.cboCurrency, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.dtpDepositDate, 0);
            this.content.Controls.SetChildIndex(this.lblREQUEST_REFUND_DATE, 0);
            this.content.Controls.SetChildIndex(this.dtpREQUEST_REFUND_DATE, 0);
            this.content.Controls.SetChildIndex(this.picHelp, 0);
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            // 
            // groupBox2
            // 
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // chkPRINT_RECEIPT
            // 
            resources.ApplyResources(this.chkPRINT_RECEIPT, "chkPRINT_RECEIPT");
            this.chkPRINT_RECEIPT.Name = "chkPRINT_RECEIPT";
            this.chkPRINT_RECEIPT.UseVisualStyleBackColor = true;
            this.chkPRINT_RECEIPT.CheckedChanged += new System.EventHandler(this.chkPrintInvoice_CheckedChanged);
            // 
            // lblCUSTOMER_TYPE
            // 
            resources.ApplyResources(this.lblCUSTOMER_TYPE, "lblCUSTOMER_TYPE");
            this.lblCUSTOMER_TYPE.Name = "lblCUSTOMER_TYPE";
            // 
            // lblAMPARE
            // 
            resources.ApplyResources(this.lblAMPARE, "lblAMPARE");
            this.lblAMPARE.Name = "lblAMPARE";
            // 
            // lblPHASE
            // 
            resources.ApplyResources(this.lblPHASE, "lblPHASE");
            this.lblPHASE.Name = "lblPHASE";
            // 
            // lblLAST_NAME_KH
            // 
            resources.ApplyResources(this.lblLAST_NAME_KH, "lblLAST_NAME_KH");
            this.lblLAST_NAME_KH.Name = "lblLAST_NAME_KH";
            // 
            // txtLastName
            // 
            resources.ApplyResources(this.txtLastName, "txtLastName");
            this.txtLastName.Name = "txtLastName";
            // 
            // lblPRICE
            // 
            resources.ApplyResources(this.lblPRICE, "lblPRICE");
            this.lblPRICE.Name = "lblPRICE";
            // 
            // txtCustomerType
            // 
            resources.ApplyResources(this.txtCustomerType, "txtCustomerType");
            this.txtCustomerType.Name = "txtCustomerType";
            // 
            // txtAmp
            // 
            resources.ApplyResources(this.txtAmp, "txtAmp");
            this.txtAmp.Name = "txtAmp";
            // 
            // txtPhase
            // 
            resources.ApplyResources(this.txtPhase, "txtPhase");
            this.txtPhase.Name = "txtPhase";
            // 
            // txtPrice
            // 
            resources.ApplyResources(this.txtPrice, "txtPrice");
            this.txtPrice.Name = "txtPrice";
            // 
            // txtFirstName
            // 
            resources.ApplyResources(this.txtFirstName, "txtFirstName");
            this.txtFirstName.Name = "txtFirstName";
            // 
            // lblFIRST_NAME_KH
            // 
            resources.ApplyResources(this.lblFIRST_NAME_KH, "lblFIRST_NAME_KH");
            this.lblFIRST_NAME_KH.Name = "lblFIRST_NAME_KH";
            // 
            // txtLastBalance
            // 
            resources.ApplyResources(this.txtLastBalance, "txtLastBalance");
            this.txtLastBalance.Name = "txtLastBalance";
            // 
            // txtNode
            // 
            resources.ApplyResources(this.txtNode, "txtNode");
            this.txtNode.Name = "txtNode";
            this.txtNode.Enter += new System.EventHandler(this.InputKhmer);
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Name = "label12";
            // 
            // txtAdjustAmount
            // 
            resources.ApplyResources(this.txtAdjustAmount, "txtAdjustAmount");
            this.txtAdjustAmount.Name = "txtAdjustAmount";
            this.txtAdjustAmount.TextChanged += new System.EventHandler(this.txtAdjustAmount_TextChanged);
            this.txtAdjustAmount.Enter += new System.EventHandler(this.InputEnglish);
            this.txtAdjustAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDecimalOnly);
            // 
            // lblBALANCE
            // 
            resources.ApplyResources(this.lblBALANCE, "lblBALANCE");
            this.lblBALANCE.Name = "lblBALANCE";
            // 
            // lblAMOUNT
            // 
            resources.ApplyResources(this.lblAMOUNT, "lblAMOUNT");
            this.lblAMOUNT.Name = "lblAMOUNT";
            // 
            // lblDEPOSIT_AMOUNT
            // 
            resources.ApplyResources(this.lblDEPOSIT_AMOUNT, "lblDEPOSIT_AMOUNT");
            this.lblDEPOSIT_AMOUNT.Name = "lblDEPOSIT_AMOUNT";
            // 
            // txtCurrentBalance
            // 
            this.txtCurrentBalance.AcceptsReturn = true;
            resources.ApplyResources(this.txtCurrentBalance, "txtCurrentBalance");
            this.txtCurrentBalance.Name = "txtCurrentBalance";
            // 
            // chkIS_PAID
            // 
            resources.ApplyResources(this.chkIS_PAID, "chkIS_PAID");
            this.chkIS_PAID.Name = "chkIS_PAID";
            this.chkIS_PAID.UseVisualStyleBackColor = true;
            this.chkIS_PAID.CheckedChanged += new System.EventHandler(this.chkIsPaid_CheckedChanged);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblCURRENCY
            // 
            resources.ApplyResources(this.lblCURRENCY, "lblCURRENCY");
            this.lblCURRENCY.Name = "lblCURRENCY";
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.SelectedIndexChanged += new System.EventHandler(this.cboCurrency_SelectedIndexChanged);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // dtpDepositDate
            // 
            resources.ApplyResources(this.dtpDepositDate, "dtpDepositDate");
            this.dtpDepositDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDepositDate.Name = "dtpDepositDate";
            this.dtpDepositDate.Enter += new System.EventHandler(this.dtpDepositDate_Enter);
            // 
            // lblDATE
            // 
            resources.ApplyResources(this.lblDATE, "lblDATE");
            this.lblDATE.Name = "lblDATE";
            // 
            // lblNOTE
            // 
            resources.ApplyResources(this.lblNOTE, "lblNOTE");
            this.lblNOTE.Name = "lblNOTE";
            // 
            // lblREQUEST_REFUND_DATE
            // 
            resources.ApplyResources(this.lblREQUEST_REFUND_DATE, "lblREQUEST_REFUND_DATE");
            this.lblREQUEST_REFUND_DATE.Name = "lblREQUEST_REFUND_DATE";
            // 
            // dtpREQUEST_REFUND_DATE
            // 
            resources.ApplyResources(this.dtpREQUEST_REFUND_DATE, "dtpREQUEST_REFUND_DATE");
            this.dtpREQUEST_REFUND_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpREQUEST_REFUND_DATE.Name = "dtpREQUEST_REFUND_DATE";
            this.dtpREQUEST_REFUND_DATE.Enter += new System.EventHandler(this.dtpREQUEST_REFUND_DATE_Enter);
            // 
            // picHelp
            // 
            this.picHelp.Cursor = System.Windows.Forms.Cursors.Default;
            this.picHelp.Image = global::EPower.Properties.Resources.question_mark;
            resources.ApplyResources(this.picHelp, "picHelp");
            this.picHelp.Name = "picHelp";
            this.picHelp.TabStop = false;
            this.picHelp.MouseEnter += new System.EventHandler(this.picHelp_MouseEnter);
            // 
            // DialogDepositAdd
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogDepositAdd";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picHelp)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private GroupBox groupBox2;
        private CheckBox chkPRINT_RECEIPT;
        private Label lblCUSTOMER_TYPE;
        private Label lblAMPARE;
        private Label lblPHASE;
        private Label lblPRICE;
        private TextBox txtLastName;
        private Label lblLAST_NAME_KH;
        private TextBox txtPhase;
        private TextBox txtAmp;
        private TextBox txtCustomerType;
        private TextBox txtPrice;
        private TextBox txtFirstName;
        private Label lblFIRST_NAME_KH;
        private TextBox txtLastBalance;
        private TextBox txtNode;
        private Label label12;
        private TextBox txtAdjustAmount;
        private Label lblBALANCE;
        private Label lblAMOUNT;
        private Label lblDEPOSIT_AMOUNT;
        private TextBox txtCurrentBalance;
        private CheckBox chkIS_PAID;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private ComboBox cboCurrency;
        private Label lblCURRENCY;
        private Label label9;
        private DateTimePicker dtpDepositDate;
        private Label lblDATE;
        private Label lblNOTE;
        private DateTimePicker dtpREQUEST_REFUND_DATE;
        private Label lblREQUEST_REFUND_DATE;
        private PictureBox picHelp;
    }
}