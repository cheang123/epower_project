﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using EPower.Helper;
using EPower.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Security.Logic;
using Process = System.Diagnostics.Process;
using Version = EPower.Update.Version;

namespace EPower.Interface
{
    public partial class DialogAboutEPower : ExDialog
    {
        TBL_CONFIG _objConfNew = new TBL_CONFIG();
        TBL_CONFIG _objConfOld = new TBL_CONFIG(); 
        private string nextVersion = "";

        public DialogAboutEPower( )
        {
            InitializeComponent();
            tabControl1.TabPages.Remove(tabMAINTENANCE);
            var obj = DBDataContext.Db.TBL_UTILITies.FirstOrDefault(row => row.UTILITY_ID == (int)Utility.VERSION);
            if (obj == null)
            {
                lblV_.Text = "N.A" ;
            }
            else
            {
                lblV_.Text = obj.UTILITY_VALUE == Version.ClientVersion ? obj.UTILITY_VALUE : string.Format("Client: {0},Server :{1}", Version.ClientVersion, obj.UTILITY_VALUE);
            }
            SupportInfo();
            _objConfNew._CopyTo(_objConfOld);


            // checking for new avaliable update.
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += delegate (object s1, DoWorkEventArgs e1)
            {
                e1.Result = CRMService.Intance.GetNextVersion(Version.ClientVersion);
            };
            worker.RunWorkerCompleted += delegate (object s1, RunWorkerCompletedEventArgs e1)
            {
                pbSync.Visible = false;
                nextVersion = e1.Result.ToString();
                if (e1.Result == null || e1.Error != null || nextVersion.Length > 10)
                {
                    lblVersioning_.Text = Resources.MS_CANNOT_CONNECT_SERVER;
                    lblVersioning_.ForeColor = Color.Red;
                    if (e1.Result.ToString().Length > 10)
                    {
                        if (e1.Result.ToString() == Resources.CANNOT_CONECT_TO_SERVER)
                        {
                            return;
                        }
                        MsgBox.ShowError(e1.Result.ToString(), Resources.WARNING);
                    }
                    return;
                }

                if (Version.ConvertToLong(nextVersion) <= Version.ConvertToLong(Version.ClientVersion))
                {
                    lblVersioning_.Text = String.Format(Resources.MS_LAST_VERSION, nextVersion);
                    lblVersioning_.ForeColor = Color.Green;
                }
                else
                {
                    lblVersioning_.Visible = false;
                    btnUPDATE_.Visible = true;
                    btnUPDATE_.Text = Resources.MS_UPDATE_NEW_VERSION + nextVersion;
                }
            };
            worker.RunWorkerAsync();
        }

        private void SupportInfo()
        {
            DBDataContext.Db.TBL_CONFIGs.FirstOrDefault()._CopyTo(_objConfNew);             
            txtActivateDate_.Text = _objConfNew.ACTIVATE_DATE.ToString("dd - MM - yyyy");
            txtMonth_.Text = string.Format(Resources.TOTAL_MONTH, _objConfNew.TOTAL_MONTH);
            txtDueDate_.Text = string.Format(Resources.REMAIN_DUE_DATE, _objConfNew.ACTIVATE_DATE.AddMonths(_objConfNew.TOTAL_MONTH)
                , (_objConfNew.ACTIVATE_DATE.AddMonths(_objConfNew.TOTAL_MONTH).Date - DBDataContext.Db.GetSystemDate().Date).TotalDays.ToString());
            lblTotalCus_.Text = _objConfNew.TOTAL_CUTOMER.ToString();
        } 

        private void btnAdd_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnUpdateSupport_Click(object sender, EventArgs e)
        {
            if (CheckSum.UpdateSM(txtSerial.Text))
            {
                SupportInfo();
                int month=_objConfNew.TOTAL_MONTH -_objConfOld.TOTAL_MONTH;
                MsgBox.ShowInformation(string.Format(Resources.MS_ACTIVATE_SUPPORT_AND_MAINTENANCE, month, _objConfNew.TOTAL_CUTOMER));
            }
        }
         

        private void btnInstall_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        { 
            new DialogUpdate(nextVersion).ShowDialog(); 
        }

        private void lblLINK_EPOWER__LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://www.e-power.com.kh/");
        }

        private void picFb_MouseClick(object sender, MouseEventArgs e)
        {
            Process.Start("https://www.facebook.com/epowerccl/");
        }

        private void picIe_MouseClick(object sender, MouseEventArgs e)
        {
            Process.Start("http://www.e-power.com.kh/");
        }

        private void DialogAboutEPower_Load(object sender, EventArgs e)
        {
            lblCopy_.Text = string.Format(lblCopy_.Text, DateTime.Now.Year);
        }
    }
}