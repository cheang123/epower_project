﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.BankPayment
{
    partial class DialogBankPaymentVoid
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogBankPaymentVoid));
            this.btnOK = new SoftTech.Component.ExButton();
            this.lblBRANCH = new System.Windows.Forms.Label();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblCUSTOMER_CODE = new System.Windows.Forms.Label();
            this.lblPAY_AMOUNT = new System.Windows.Forms.Label();
            this.lblPAY_DATE = new System.Windows.Forms.Label();
            this.lblBANK = new System.Windows.Forms.Label();
            this.txtBank = new System.Windows.Forms.TextBox();
            this.txtBranch = new System.Windows.Forms.TextBox();
            this.txtCustomerCode = new System.Windows.Forms.TextBox();
            this.txtPayDate = new System.Windows.Forms.TextBox();
            this.txtPayAmount = new System.Windows.Forms.TextBox();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.lblNOTE = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.txtNote);
            this.content.Controls.Add(this.txtPayAmount);
            this.content.Controls.Add(this.txtPayDate);
            this.content.Controls.Add(this.txtCustomerCode);
            this.content.Controls.Add(this.txtBranch);
            this.content.Controls.Add(this.txtBank);
            this.content.Controls.Add(this.lblBANK);
            this.content.Controls.Add(this.lblPAY_DATE);
            this.content.Controls.Add(this.lblNOTE);
            this.content.Controls.Add(this.lblPAY_AMOUNT);
            this.content.Controls.Add(this.lblCUSTOMER_CODE);
            this.content.Controls.Add(this.panel2);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.lblBRANCH);
            this.content.Controls.Add(this.btnOK);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.lblBRANCH, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel2, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_CODE, 0);
            this.content.Controls.SetChildIndex(this.lblPAY_AMOUNT, 0);
            this.content.Controls.SetChildIndex(this.lblNOTE, 0);
            this.content.Controls.SetChildIndex(this.lblPAY_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblBANK, 0);
            this.content.Controls.SetChildIndex(this.txtBank, 0);
            this.content.Controls.SetChildIndex(this.txtBranch, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerCode, 0);
            this.content.Controls.SetChildIndex(this.txtPayDate, 0);
            this.content.Controls.SetChildIndex(this.txtPayAmount, 0);
            this.content.Controls.SetChildIndex(this.txtNote, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblBRANCH
            // 
            resources.ApplyResources(this.lblBRANCH, "lblBRANCH");
            this.lblBRANCH.Name = "lblBRANCH";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnCLOSE_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // lblCUSTOMER_CODE
            // 
            resources.ApplyResources(this.lblCUSTOMER_CODE, "lblCUSTOMER_CODE");
            this.lblCUSTOMER_CODE.Name = "lblCUSTOMER_CODE";
            // 
            // lblPAY_AMOUNT
            // 
            resources.ApplyResources(this.lblPAY_AMOUNT, "lblPAY_AMOUNT");
            this.lblPAY_AMOUNT.Name = "lblPAY_AMOUNT";
            // 
            // lblPAY_DATE
            // 
            resources.ApplyResources(this.lblPAY_DATE, "lblPAY_DATE");
            this.lblPAY_DATE.Name = "lblPAY_DATE";
            // 
            // lblBANK
            // 
            resources.ApplyResources(this.lblBANK, "lblBANK");
            this.lblBANK.Name = "lblBANK";
            // 
            // txtBank
            // 
            resources.ApplyResources(this.txtBank, "txtBank");
            this.txtBank.Name = "txtBank";
            this.txtBank.ReadOnly = true;
            // 
            // txtBranch
            // 
            resources.ApplyResources(this.txtBranch, "txtBranch");
            this.txtBranch.Name = "txtBranch";
            this.txtBranch.ReadOnly = true;
            // 
            // txtCustomerCode
            // 
            resources.ApplyResources(this.txtCustomerCode, "txtCustomerCode");
            this.txtCustomerCode.Name = "txtCustomerCode";
            this.txtCustomerCode.ReadOnly = true;
            // 
            // txtPayDate
            // 
            resources.ApplyResources(this.txtPayDate, "txtPayDate");
            this.txtPayDate.Name = "txtPayDate";
            this.txtPayDate.ReadOnly = true;
            // 
            // txtPayAmount
            // 
            resources.ApplyResources(this.txtPayAmount, "txtPayAmount");
            this.txtPayAmount.Name = "txtPayAmount";
            this.txtPayAmount.ReadOnly = true;
            // 
            // txtNote
            // 
            resources.ApplyResources(this.txtNote, "txtNote");
            this.txtNote.Name = "txtNote";
            // 
            // lblNOTE
            // 
            resources.ApplyResources(this.lblNOTE, "lblNOTE");
            this.lblNOTE.Name = "lblNOTE";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // DialogBankPaymentVoid
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogBankPaymentVoid";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnOK;
        private Label lblBRANCH;
        private ExButton btnCLOSE;
        private Panel panel2;
        private Label lblCUSTOMER_CODE;
        private Label lblPAY_DATE;
        private Label lblPAY_AMOUNT;
        private Label txtError;
        private Label lblBANK;
        private TextBox txtNote;
        private TextBox txtPayAmount;
        private TextBox txtPayDate;
        private TextBox txtCustomerCode;
        private TextBox txtBranch;
        private TextBox txtBank;
        private Label lblNOTE;
        private Label label9;
    }
}