﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System.Transactions;

namespace EPower.Interface.BankPayment
{
    public partial class DialogSummaryDetail : ExDialog
    {
        DateTime d1, d2;
        string bank, branch;
        int currencyId;
        int areaId;
        int billingCycleId;
        public DialogSummaryDetail(DateTime d1,DateTime d2,string bank,string branch,int currencyId,int areaId,int billingCycleId)
        {
            InitializeComponent();

            this.d1 = d1;
            this.d2 = d2;
            this.bank = bank;
            this.branch = branch;
            this.currencyId = currencyId;
            this.areaId = areaId;
            this.billingCycleId = billingCycleId;
            UIHelper.SetDataSourceToComboBox(this.cboResult,DBDataContext.Db.TBL_BANK_PAYMENT_RESULTs,lblRESULT.Text);  
            UIHelper.DataGridViewProperties(dgv);
            this.dgvCurrencySummary.DefaultCellStyle.SelectionBackColor = this.dgvCurrencySummary.DefaultCellStyle.BackColor;
            this.dgvCurrencySummary.DefaultCellStyle.SelectionForeColor = this.dgvCurrencySummary.DefaultCellStyle.ForeColor;

            this.dgvResultSummary.DefaultCellStyle.SelectionBackColor = this.dgvResultSummary.DefaultCellStyle.BackColor;
            this.dgvResultSummary.DefaultCellStyle.SelectionForeColor = this.dgvResultSummary.DefaultCellStyle.ForeColor;
            
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        } 

        private void txtSearch_QuickSearch(object sender, EventArgs e)
        {
            quickSearch();
        }

        private void quickSearch()
        {
            int result = (int)(this.cboResult.SelectedValue ?? 0);

            var db = (from p in DBDataContext.Db.TBL_BANK_PAYMENT_DETAILs
                      join r in DBDataContext.Db.TBL_BANK_PAYMENT_RESULTs on p.RESULT equals r.RESULT_ID
                      join c in DBDataContext.Db.TBL_CUSTOMERs on p.CUSTOMER_ID equals c.CUSTOMER_ID into JoinCust
                      from dt in JoinCust.DefaultIfEmpty()
                      where p.RESULT == (int)BankPaymentImportResult.RecordSuccess
                          && p.PAY_DATE >= d1 && p.PAY_DATE <= d2
                          && (bank == "" || p.BANK_ALIAS == bank)
                          && (branch == "" || p.BRANCH == branch)
                          && (currencyId == 0 || p.CURRENCY_ID == currencyId)
                          && (areaId == 0 || dt.AREA_ID == areaId)
                          && (billingCycleId == 0 || dt.BILLING_CYCLE_ID == billingCycleId)
                          && (p.CUSTOMER_CODE + dt.CUSTOMER_CODE + dt.LAST_NAME_KH + dt.FIRST_NAME_KH).ToUpper().Contains(this.txtSearch.Text.ToUpper())
                          && (result == 0 || p.RESULT == result)
                          && !p.IS_VOID
                      orderby p.CURRENCY, p.PAY_DATE
                      select new
                      {
                          p.BANK_PAYMENT_DETAIL_ID,
                          p.CUSTOMER_CODE,
                          CUSTOMER_NAME = dt.LAST_NAME_KH + " " + dt.FIRST_NAME_KH,
                          p.PAY_AMOUNT,
                          p.PAY_DATE,
                          p.CURRENCY,
                          p.BRANCH,
                          BANK = p.BANK_ALIAS,
                          p.PAID_AMOUNT,
                          p.PAYMENT_TYPE,
                          p.PAYMENT_METHOD,
                          p.CASHIER,
                          r.RESULT_ID,
                          r.RESULT_NAME,
                          p.RESULT_NOTE
                      });
            dgv.DataSource = db;
            this.dgvCurrencySummary.DataSource = db.GroupBy(x => x.CURRENCY).Select(x => new
            {
                CURRENCY_CODE = x.Key,
                TOTAL_RECORD = x.Count(),
                TOTAL_PAID = x.Sum(r => r.PAID_AMOUNT),
                TOTAL_PAY = x.Sum(r => r.PAY_AMOUNT)
            })._ToDataTable();

            markColor();
        }

        private void DialogBankPaymentHistoryDetail_Load(object sender, EventArgs e)
        {
            txtSearch_QuickSearch(null, null);
        }
        private void markColor()
        {
            this.dgv.Refresh();
            foreach (DataGridViewRow row in this.dgv.Rows)
            {
                var result = (int)row.Cells[this.RESULT_ID.Name].Value;
                var color = Color.Red;
                if (result == 1)
                {  
                    color = Color.Red;
                }else if (result==2){
                    color = Color.Blue;
                }else if (result == 3){
                    color = Color.Green;
                }
                row.DefaultCellStyle.SelectionForeColor
                    = row.DefaultCellStyle.ForeColor
                    = color;
            }
        }

        private void dgv_Sorted(object sender, EventArgs e)
        {
            markColor();
        }

        private void dgv_SelectionChanged(object sender, EventArgs e)
        {
            if (this.dgv.SelectedRows.Count == 0) return;
            var row = this.dgv.SelectedRows[0];
            var id = (int)row.Cells[this.BANK_PAYMENT_DETAIL_ID.Name].Value;
            var payments = from p in DBDataContext.Db.TBL_PAYMENTs 
                    where p.BANK_PAYMENT_DETAIL_ID == id
                    select new
                    {
                        p.PAYMENT_ID,
                        p.PAYMENT_NO,
                        p.PAY_AMOUNT,
                        p.CREATE_ON
                    };
            var deposits = from d in DBDataContext.Db.TBL_CUS_DEPOSITs
                    where d.BANK_PAYMENT_DETAIL_ID == id
                    select new
                    {
                        d.CUS_DEPOSIT_ID,
                        d.DEPOSIT_NO,
                        d.AMOUNT,
                        d.CREATE_ON
                    };

            this.dgvResultSummary.Rows.Clear();
            foreach (var p in payments)
            {
                this.dgvResultSummary.Rows.Add(p.PAYMENT_ID, p.PAYMENT_NO, p.PAY_AMOUNT, p.CREATE_ON, Resources.PAYMENT, 1);
            }
            foreach (var d in deposits)
            {
                this.dgvResultSummary.Rows.Add(d.CUS_DEPOSIT_ID, d.DEPOSIT_NO, d.AMOUNT, d.CREATE_ON, Resources.DEPOSIT, 2); 
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && this.dgvResultSummary.Columns[e.ColumnIndex].Name == this.NO.Name)
            {
                var row = this.dgvResultSummary.Rows[e.RowIndex];
                var id = (int)row.Cells[this.PAYMENT_ID.Name].Value;
                var type = (int)row.Cells[this.TYPE.Name].Value;
                if (type == 1)   // payment
                {
                    new DialogPayment(id).ShowDialog();
                }
                else if(type==2) // deposit 
                {
                    // do nothing
                }
            }
        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            Runner.Run(delegate
            {
                if (this.bank == "") this.bank = null;
                if (this.branch == "") this.branch = null;

                var cr = new CrystalReportHelper("ReportBankPaymentDetail.rpt");
                cr.SetParameter("@D1", this.d1);
                cr.SetParameter("@D2", this.d2);
                cr.SetParameter("@BANK", this.bank ?? "");
                cr.SetParameter("@BRANCH", this.branch ?? "");
                cr.SetParameter("@CURRENCY_ID", this.currencyId);
                cr.SetParameter("@CURRENCY_NAME", this.currencyId == 0 ? Resources.ALL_CURRENCY : DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.CURRENCY_ID == this.currencyId).CURRENCY_NAME);
                cr.SetParameter("@BANK_TEXT", this.bank ?? Resources.ALL_BANK);
                cr.SetParameter("@BRANCH_TEXT", this.branch ?? Resources.ALL_BRANCH);
                cr.SetParameter("@AREA_ID", this.areaId);
                cr.SetParameter("@AREA_TEXT", this.areaId == 0 ? Resources.ALL_AREA : DBDataContext.Db.TBL_AREAs.FirstOrDefault(x => x.AREA_ID == areaId).AREA_NAME);
                cr.SetParameter("@BILLING_CYCLE_ID", this.billingCycleId);
                cr.SetParameter("@BILLING_CYCLE_TEXT", this.billingCycleId == 0 ? Resources.ALL_CYCLE : DBDataContext.Db.TBL_BILLING_CYCLEs.FirstOrDefault(x => x.CYCLE_ID == billingCycleId).CYCLE_NAME);
                cr.ViewReport(Resources.SHOW_DETAIL);
            }, Resources.PROCESSING);
            
        }
        private void cboResult_SelectedIndexChanged(object sender, EventArgs e)
        {
            quickSearch();
        }

        private void content_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                btnREMOVE.Visible = 
                btnHISTORY_DELETE.Visible=!btnREMOVE.Visible;
            }
        }

        private void btnREMOVE_Click(object sender, EventArgs e)
        {
            if (dgv.Rows.Count > 0)
            {
                int id = (int)dgv.SelectedRows[0].Cells[this.BANK_PAYMENT_DETAIL_ID.Name].Value;
                TBL_BANK_PAYMENT_DETAIL objBankPaymentDetail = DBDataContext.Db.TBL_BANK_PAYMENT_DETAILs.FirstOrDefault(x => x.BANK_PAYMENT_DETAIL_ID == id);
                DialogBankPaymentVoid dia = new DialogBankPaymentVoid(objBankPaymentDetail);
                if (dia.ShowDialog() == DialogResult.OK)
                {
                    quickSearch();
                    UIHelper.SelectRow(dgv, objBankPaymentDetail.BANK_PAYMENT_DETAIL_ID - 1);
                } 
            }
        }

        private void btnHISTORY_DELETE_Click(object sender, EventArgs e)
        {
            new DialogHistoryDelete(d1, d2).ShowDialog();
        }
         
    }
}
