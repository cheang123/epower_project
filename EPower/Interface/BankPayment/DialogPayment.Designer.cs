﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.BankPayment
{
    partial class DialogPayment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogPayment));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnClose = new SoftTech.Component.ExButton();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.lblRECEIPT_NO = new System.Windows.Forms.Label();
            this.lblPAY_DATE = new System.Windows.Forms.Label();
            this.lblCREATE_BY = new System.Windows.Forms.Label();
            this.lblCUSTOMER_CODE = new System.Windows.Forms.Label();
            this.lblCUSTOMER_NAME = new System.Windows.Forms.Label();
            this.lblPAY_AMOUNT = new System.Windows.Forms.Label();
            this.txtPaymentNo = new System.Windows.Forms.TextBox();
            this.txtPayDate = new System.Windows.Forms.TextBox();
            this.txtCreateBy = new System.Windows.Forms.TextBox();
            this.txtCustomerCode = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtPayAmount = new System.Windows.Forms.TextBox();
            this.PAYMENT_DETAIL_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICE_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICE_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICE_TITLE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SETTLE_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAY_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.txtPayAmount);
            this.content.Controls.Add(this.txtName);
            this.content.Controls.Add(this.txtCustomerCode);
            this.content.Controls.Add(this.txtCreateBy);
            this.content.Controls.Add(this.txtPayDate);
            this.content.Controls.Add(this.txtPaymentNo);
            this.content.Controls.Add(this.lblPAY_AMOUNT);
            this.content.Controls.Add(this.lblCUSTOMER_NAME);
            this.content.Controls.Add(this.lblCUSTOMER_CODE);
            this.content.Controls.Add(this.lblCREATE_BY);
            this.content.Controls.Add(this.lblPAY_DATE);
            this.content.Controls.Add(this.lblRECEIPT_NO);
            this.content.Controls.Add(this.dgv);
            this.content.Controls.Add(this.btnClose);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnClose, 0);
            this.content.Controls.SetChildIndex(this.dgv, 0);
            this.content.Controls.SetChildIndex(this.lblRECEIPT_NO, 0);
            this.content.Controls.SetChildIndex(this.lblPAY_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblCREATE_BY, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_CODE, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblPAY_AMOUNT, 0);
            this.content.Controls.SetChildIndex(this.txtPaymentNo, 0);
            this.content.Controls.SetChildIndex(this.txtPayDate, 0);
            this.content.Controls.SetChildIndex(this.txtCreateBy, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerCode, 0);
            this.content.Controls.SetChildIndex(this.txtName, 0);
            this.content.Controls.SetChildIndex(this.txtPayAmount, 0);
            // 
            // btnClose
            // 
            resources.ApplyResources(this.btnClose, "btnClose");
            this.btnClose.Name = "btnClose";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PAYMENT_DETAIL_ID,
            this.INVOICE_ID,
            this.INVOICE_NO,
            this.INVOICE_DATE,
            this.INVOICE_TITLE,
            this.SETTLE_AMOUNT,
            this.PAY_AMOUNT});
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellClick);
            this.dgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellContentClick);
            // 
            // lblRECEIPT_NO
            // 
            resources.ApplyResources(this.lblRECEIPT_NO, "lblRECEIPT_NO");
            this.lblRECEIPT_NO.Name = "lblRECEIPT_NO";
            // 
            // lblPAY_DATE
            // 
            resources.ApplyResources(this.lblPAY_DATE, "lblPAY_DATE");
            this.lblPAY_DATE.Name = "lblPAY_DATE";
            // 
            // lblCREATE_BY
            // 
            resources.ApplyResources(this.lblCREATE_BY, "lblCREATE_BY");
            this.lblCREATE_BY.Name = "lblCREATE_BY";
            // 
            // lblCUSTOMER_CODE
            // 
            resources.ApplyResources(this.lblCUSTOMER_CODE, "lblCUSTOMER_CODE");
            this.lblCUSTOMER_CODE.Name = "lblCUSTOMER_CODE";
            // 
            // lblCUSTOMER_NAME
            // 
            resources.ApplyResources(this.lblCUSTOMER_NAME, "lblCUSTOMER_NAME");
            this.lblCUSTOMER_NAME.Name = "lblCUSTOMER_NAME";
            // 
            // lblPAY_AMOUNT
            // 
            resources.ApplyResources(this.lblPAY_AMOUNT, "lblPAY_AMOUNT");
            this.lblPAY_AMOUNT.Name = "lblPAY_AMOUNT";
            // 
            // txtPaymentNo
            // 
            resources.ApplyResources(this.txtPaymentNo, "txtPaymentNo");
            this.txtPaymentNo.Name = "txtPaymentNo";
            // 
            // txtPayDate
            // 
            resources.ApplyResources(this.txtPayDate, "txtPayDate");
            this.txtPayDate.Name = "txtPayDate";
            // 
            // txtCreateBy
            // 
            resources.ApplyResources(this.txtCreateBy, "txtCreateBy");
            this.txtCreateBy.Name = "txtCreateBy";
            // 
            // txtCustomerCode
            // 
            resources.ApplyResources(this.txtCustomerCode, "txtCustomerCode");
            this.txtCustomerCode.Name = "txtCustomerCode";
            // 
            // txtName
            // 
            resources.ApplyResources(this.txtName, "txtName");
            this.txtName.Name = "txtName";
            // 
            // txtPayAmount
            // 
            resources.ApplyResources(this.txtPayAmount, "txtPayAmount");
            this.txtPayAmount.Name = "txtPayAmount";
            // 
            // PAYMENT_DETAIL_ID
            // 
            this.PAYMENT_DETAIL_ID.DataPropertyName = "PAYMENT_DETAIL_ID";
            resources.ApplyResources(this.PAYMENT_DETAIL_ID, "PAYMENT_DETAIL_ID");
            this.PAYMENT_DETAIL_ID.Name = "PAYMENT_DETAIL_ID";
            this.PAYMENT_DETAIL_ID.ReadOnly = true;
            // 
            // INVOICE_ID
            // 
            this.INVOICE_ID.DataPropertyName = "INVOICE_ID";
            resources.ApplyResources(this.INVOICE_ID, "INVOICE_ID");
            this.INVOICE_ID.Name = "INVOICE_ID";
            this.INVOICE_ID.ReadOnly = true;
            // 
            // INVOICE_NO
            // 
            this.INVOICE_NO.DataPropertyName = "INVOICE_NO";
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Blue;
            this.INVOICE_NO.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.INVOICE_NO, "INVOICE_NO");
            this.INVOICE_NO.Name = "INVOICE_NO";
            this.INVOICE_NO.ReadOnly = true;
            // 
            // INVOICE_DATE
            // 
            this.INVOICE_DATE.DataPropertyName = "INVOICE_DATE";
            dataGridViewCellStyle3.Format = "dd-MM-yyyy";
            this.INVOICE_DATE.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.INVOICE_DATE, "INVOICE_DATE");
            this.INVOICE_DATE.Name = "INVOICE_DATE";
            this.INVOICE_DATE.ReadOnly = true;
            // 
            // INVOICE_TITLE
            // 
            this.INVOICE_TITLE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.INVOICE_TITLE.DataPropertyName = "INVOICE_TITLE";
            resources.ApplyResources(this.INVOICE_TITLE, "INVOICE_TITLE");
            this.INVOICE_TITLE.Name = "INVOICE_TITLE";
            this.INVOICE_TITLE.ReadOnly = true;
            // 
            // SETTLE_AMOUNT
            // 
            this.SETTLE_AMOUNT.DataPropertyName = "SETTLE_AMOUNT";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "#,##0.####";
            this.SETTLE_AMOUNT.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.SETTLE_AMOUNT, "SETTLE_AMOUNT");
            this.SETTLE_AMOUNT.Name = "SETTLE_AMOUNT";
            this.SETTLE_AMOUNT.ReadOnly = true;
            // 
            // PAY_AMOUNT
            // 
            this.PAY_AMOUNT.DataPropertyName = "PAY_AMOUNT";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "#,##0.####";
            this.PAY_AMOUNT.DefaultCellStyle = dataGridViewCellStyle5;
            resources.ApplyResources(this.PAY_AMOUNT, "PAY_AMOUNT");
            this.PAY_AMOUNT.Name = "PAY_AMOUNT";
            this.PAY_AMOUNT.ReadOnly = true;
            // 
            // DialogPayment
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogPayment";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnClose;
        private DataGridView dgv;
        private TextBox txtPayAmount;
        private TextBox txtName;
        private TextBox txtCustomerCode;
        private TextBox txtCreateBy;
        private TextBox txtPayDate;
        private TextBox txtPaymentNo;
        private Label lblPAY_AMOUNT;
        private Label lblCUSTOMER_NAME;
        private Label lblCUSTOMER_CODE;
        private Label lblCREATE_BY;
        private Label lblPAY_DATE;
        private Label lblRECEIPT_NO;
        private DataGridViewTextBoxColumn PAYMENT_DETAIL_ID;
        private DataGridViewTextBoxColumn INVOICE_ID;
        private DataGridViewTextBoxColumn INVOICE_NO;
        private DataGridViewTextBoxColumn INVOICE_DATE;
        private DataGridViewTextBoxColumn INVOICE_TITLE;
        private DataGridViewTextBoxColumn SETTLE_AMOUNT;
        private DataGridViewTextBoxColumn PAY_AMOUNT;
    }
}