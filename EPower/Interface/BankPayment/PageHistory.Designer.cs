﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.BankPayment
{
    partial class PageHistory
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageHistory));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.BANK_PAYMENT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_ON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_BY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NAME_FILE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_RECORD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnBANK_PAYMENT = new SoftTech.Component.ExButton();
            this.btnACLEDA_AGREEMENT = new SoftTech.Component.ExButton();
            this.btnEMAIL_TO_BANK = new SoftTech.Component.ExButton();
            this.btnPENDING_PAYMENT = new SoftTech.Component.ExButton();
            this.btnIMPORT = new SoftTech.Component.ExButton();
            this.btnEXPORT = new SoftTech.Component.ExButton();
            this.txtSearch = new SoftTech.Component.ExTextbox();
            this.dtpD2 = new System.Windows.Forms.DateTimePicker();
            this.dtpD1 = new System.Windows.Forms.DateTimePicker();
            this.btnDETAIL = new SoftTech.Component.ExButton();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BANK_PAYMENT_ID,
            this.CREATE_ON,
            this.CREATE_BY,
            this.NAME_FILE,
            this.TYPE,
            this.TOTAL_RECORD});
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellContentClick);
            this.dgv.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellDoubleClick);
            this.dgv.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellMouseEnter);
            this.dgv.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellMouseLeave);
            // 
            // BANK_PAYMENT_ID
            // 
            this.BANK_PAYMENT_ID.DataPropertyName = "BANK_PAYMENT_ID";
            resources.ApplyResources(this.BANK_PAYMENT_ID, "BANK_PAYMENT_ID");
            this.BANK_PAYMENT_ID.Name = "BANK_PAYMENT_ID";
            // 
            // CREATE_ON
            // 
            this.CREATE_ON.DataPropertyName = "CREATE_ON";
            dataGridViewCellStyle2.Format = "dd-MM-yyyy HH:mm:ss";
            dataGridViewCellStyle2.NullValue = null;
            this.CREATE_ON.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.CREATE_ON, "CREATE_ON");
            this.CREATE_ON.Name = "CREATE_ON";
            // 
            // CREATE_BY
            // 
            this.CREATE_BY.DataPropertyName = "CREATE_BY";
            resources.ApplyResources(this.CREATE_BY, "CREATE_BY");
            this.CREATE_BY.Name = "CREATE_BY";
            // 
            // NAME_FILE
            // 
            this.NAME_FILE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NAME_FILE.DataPropertyName = "NAME_FILE";
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Blue;
            this.NAME_FILE.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.NAME_FILE, "NAME_FILE");
            this.NAME_FILE.Name = "NAME_FILE";
            // 
            // TYPE
            // 
            this.TYPE.DataPropertyName = "BANK_PAYMENT_TYPE_NAME";
            resources.ApplyResources(this.TYPE, "TYPE");
            this.TYPE.Name = "TYPE";
            // 
            // TOTAL_RECORD
            // 
            this.TOTAL_RECORD.DataPropertyName = "TOTAL_RECORD";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.Format = "#";
            this.TOTAL_RECORD.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.TOTAL_RECORD, "TOTAL_RECORD");
            this.TOTAL_RECORD.Name = "TOTAL_RECORD";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.btnBANK_PAYMENT);
            this.panel1.Controls.Add(this.btnACLEDA_AGREEMENT);
            this.panel1.Controls.Add(this.btnEMAIL_TO_BANK);
            this.panel1.Controls.Add(this.btnPENDING_PAYMENT);
            this.panel1.Controls.Add(this.btnIMPORT);
            this.panel1.Controls.Add(this.btnEXPORT);
            this.panel1.Controls.Add(this.txtSearch);
            this.panel1.Controls.Add(this.dtpD2);
            this.panel1.Controls.Add(this.dtpD1);
            this.panel1.Controls.Add(this.btnDETAIL);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnBANK_PAYMENT
            // 
            resources.ApplyResources(this.btnBANK_PAYMENT, "btnBANK_PAYMENT");
            this.btnBANK_PAYMENT.Name = "btnBANK_PAYMENT";
            this.btnBANK_PAYMENT.UseVisualStyleBackColor = true;
            this.btnBANK_PAYMENT.Click += new System.EventHandler(this.btnBankPayment_Click);
            // 
            // btnACLEDA_AGREEMENT
            // 
            resources.ApplyResources(this.btnACLEDA_AGREEMENT, "btnACLEDA_AGREEMENT");
            this.btnACLEDA_AGREEMENT.Name = "btnACLEDA_AGREEMENT";
            this.btnACLEDA_AGREEMENT.UseVisualStyleBackColor = true;
            this.btnACLEDA_AGREEMENT.Click += new System.EventHandler(this.btnAcledaAgreement_Click);
            // 
            // btnEMAIL_TO_BANK
            // 
            resources.ApplyResources(this.btnEMAIL_TO_BANK, "btnEMAIL_TO_BANK");
            this.btnEMAIL_TO_BANK.Name = "btnEMAIL_TO_BANK";
            this.btnEMAIL_TO_BANK.UseVisualStyleBackColor = true;
            this.btnEMAIL_TO_BANK.Click += new System.EventHandler(this.btnEMAIL_TO_BANK_Click);
            // 
            // btnPENDING_PAYMENT
            // 
            resources.ApplyResources(this.btnPENDING_PAYMENT, "btnPENDING_PAYMENT");
            this.btnPENDING_PAYMENT.Name = "btnPENDING_PAYMENT";
            this.btnPENDING_PAYMENT.UseVisualStyleBackColor = true;
            this.btnPENDING_PAYMENT.Click += new System.EventHandler(this.btnPendingPayment_Click);
            // 
            // btnIMPORT
            // 
            resources.ApplyResources(this.btnIMPORT, "btnIMPORT");
            this.btnIMPORT.Name = "btnIMPORT";
            this.btnIMPORT.UseVisualStyleBackColor = true;
            this.btnIMPORT.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // btnEXPORT
            // 
            resources.ApplyResources(this.btnEXPORT, "btnEXPORT");
            this.btnEXPORT.Name = "btnEXPORT";
            this.btnEXPORT.UseVisualStyleBackColor = true;
            this.btnEXPORT.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.BackColor = System.Drawing.Color.White;
            this.txtSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtSearch, "txtSearch");
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            // 
            // dtpD2
            // 
            resources.ApplyResources(this.dtpD2, "dtpD2");
            this.dtpD2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpD2.Name = "dtpD2";
            // 
            // dtpD1
            // 
            resources.ApplyResources(this.dtpD1, "dtpD1");
            this.dtpD1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpD1.Name = "dtpD1";
            // 
            // btnDETAIL
            // 
            resources.ApplyResources(this.btnDETAIL, "btnDETAIL");
            this.btnDETAIL.Name = "btnDETAIL";
            this.btnDETAIL.UseVisualStyleBackColor = true;
            this.btnDETAIL.Click += new System.EventHandler(this.btnDetail_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "BANK_PAYMENT_ID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn1, "dataGridViewTextBoxColumn1");
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "CREATE_ON";
            dataGridViewCellStyle5.Format = "yyyy-MM-dd hh:mm tt";
            dataGridViewCellStyle5.NullValue = null;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle5;
            resources.ApplyResources(this.dataGridViewTextBoxColumn2, "dataGridViewTextBoxColumn2");
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "CREATE_BY";
            resources.ApplyResources(this.dataGridViewTextBoxColumn3, "dataGridViewTextBoxColumn3");
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "NAME_FILE";
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Blue;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle6;
            resources.ApplyResources(this.dataGridViewTextBoxColumn4, "dataGridViewTextBoxColumn4");
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "BANK_PAYMENT_TYPE_NAME";
            resources.ApplyResources(this.dataGridViewTextBoxColumn5, "dataGridViewTextBoxColumn5");
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "TOTAL_RECORD";
            resources.ApplyResources(this.dataGridViewTextBoxColumn6, "dataGridViewTextBoxColumn6");
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn7.DataPropertyName = "TOTAL_AMOUNT";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N2";
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle7;
            resources.ApplyResources(this.dataGridViewTextBoxColumn7, "dataGridViewTextBoxColumn7");
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "TOTAL_PAID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn8, "dataGridViewTextBoxColumn8");
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // PageHistory
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.panel1);
            this.Name = "PageHistory";
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private DataGridView dgv;
        private DateTimePicker dtpD2;
        private DateTimePicker dtpD1;
        private ExButton btnDETAIL;
        private ExTextbox txtSearch;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private ExButton btnBANK_PAYMENT;
        private ExButton btnIMPORT;
        private ExButton btnEXPORT;
        private ExButton btnPENDING_PAYMENT;
        private ExButton btnEMAIL_TO_BANK;
        private ExButton btnACLEDA_AGREEMENT;
        private DataGridViewTextBoxColumn BANK_PAYMENT_ID;
        private DataGridViewTextBoxColumn CREATE_ON;
        private DataGridViewTextBoxColumn CREATE_BY;
        private DataGridViewTextBoxColumn NAME_FILE;
        private DataGridViewTextBoxColumn TYPE;
        private DataGridViewTextBoxColumn TOTAL_RECORD;
    }
}
