﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.BankPayment
{
    partial class DialogHistoryDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogHistoryDetail));
            this.btnClose = new SoftTech.Component.ExButton();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.CUSTOMER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAY_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAID_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAY_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CASHIER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BANK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BRANCH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RESULT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RESULT_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NOTE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAYMENT_METHOD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtSearch = new SoftTech.Component.ExTextbox();
            this.dgvSummary = new System.Windows.Forms.DataGridView();
            this.CURRENCY_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_RECORD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_PAY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_PAID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblTOTAL = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.lblSTATUS = new System.Windows.Forms.Label();
            this.cboResult = new System.Windows.Forms.ComboBox();
            this.lblRESULT = new System.Windows.Forms.Label();
            this.RESULT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.lblRESULT);
            this.content.Controls.Add(this.cboResult);
            this.content.Controls.Add(this.lblSTATUS);
            this.content.Controls.Add(this.dataGridView1);
            this.content.Controls.Add(this.lblTOTAL);
            this.content.Controls.Add(this.dgvSummary);
            this.content.Controls.Add(this.txtSearch);
            this.content.Controls.Add(this.dgv);
            this.content.Controls.Add(this.btnClose);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnClose, 0);
            this.content.Controls.SetChildIndex(this.dgv, 0);
            this.content.Controls.SetChildIndex(this.txtSearch, 0);
            this.content.Controls.SetChildIndex(this.dgvSummary, 0);
            this.content.Controls.SetChildIndex(this.lblTOTAL, 0);
            this.content.Controls.SetChildIndex(this.dataGridView1, 0);
            this.content.Controls.SetChildIndex(this.lblSTATUS, 0);
            this.content.Controls.SetChildIndex(this.cboResult, 0);
            this.content.Controls.SetChildIndex(this.lblRESULT, 0);
            // 
            // btnClose
            // 
            resources.ApplyResources(this.btnClose, "btnClose");
            this.btnClose.Name = "btnClose";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CUSTOMER_CODE,
            this.CUSTOMER_NAME,
            this.CURRENCY_,
            this.PAY_AMOUNT,
            this.PAID_AMOUNT,
            this.PAY_DATE,
            this.CASHIER,
            this.BANK,
            this.BRANCH,
            this.RESULT_ID,
            this.RESULT_1,
            this.NOTE,
            this.TYPE,
            this.PAYMENT_METHOD});
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Sorted += new System.EventHandler(this.dgv_Sorted);
            // 
            // CUSTOMER_CODE
            // 
            this.CUSTOMER_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.CUSTOMER_CODE.DataPropertyName = "CUSTOMER_CODE";
            resources.ApplyResources(this.CUSTOMER_CODE, "CUSTOMER_CODE");
            this.CUSTOMER_CODE.Name = "CUSTOMER_CODE";
            this.CUSTOMER_CODE.ReadOnly = true;
            // 
            // CUSTOMER_NAME
            // 
            this.CUSTOMER_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CUSTOMER_NAME.DataPropertyName = "CUSTOMER_NAME";
            resources.ApplyResources(this.CUSTOMER_NAME, "CUSTOMER_NAME");
            this.CUSTOMER_NAME.Name = "CUSTOMER_NAME";
            this.CUSTOMER_NAME.ReadOnly = true;
            // 
            // CURRENCY_
            // 
            this.CURRENCY_.DataPropertyName = "CURRENCY";
            resources.ApplyResources(this.CURRENCY_, "CURRENCY_");
            this.CURRENCY_.Name = "CURRENCY_";
            this.CURRENCY_.ReadOnly = true;
            // 
            // PAY_AMOUNT
            // 
            this.PAY_AMOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.PAY_AMOUNT.DataPropertyName = "PAY_AMOUNT";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "#,##0.####";
            dataGridViewCellStyle5.NullValue = null;
            this.PAY_AMOUNT.DefaultCellStyle = dataGridViewCellStyle5;
            resources.ApplyResources(this.PAY_AMOUNT, "PAY_AMOUNT");
            this.PAY_AMOUNT.Name = "PAY_AMOUNT";
            this.PAY_AMOUNT.ReadOnly = true;
            // 
            // PAID_AMOUNT
            // 
            this.PAID_AMOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.PAID_AMOUNT.DataPropertyName = "PAID_AMOUNT";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "#,##0.####";
            this.PAID_AMOUNT.DefaultCellStyle = dataGridViewCellStyle6;
            resources.ApplyResources(this.PAID_AMOUNT, "PAID_AMOUNT");
            this.PAID_AMOUNT.Name = "PAID_AMOUNT";
            this.PAID_AMOUNT.ReadOnly = true;
            // 
            // PAY_DATE
            // 
            this.PAY_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.PAY_DATE.DataPropertyName = "PAY_DATE";
            dataGridViewCellStyle7.Format = "dd-MM-yyyy";
            this.PAY_DATE.DefaultCellStyle = dataGridViewCellStyle7;
            resources.ApplyResources(this.PAY_DATE, "PAY_DATE");
            this.PAY_DATE.Name = "PAY_DATE";
            this.PAY_DATE.ReadOnly = true;
            // 
            // CASHIER
            // 
            this.CASHIER.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.CASHIER.DataPropertyName = "CASHIER";
            resources.ApplyResources(this.CASHIER, "CASHIER");
            this.CASHIER.Name = "CASHIER";
            this.CASHIER.ReadOnly = true;
            // 
            // BANK
            // 
            this.BANK.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.BANK.DataPropertyName = "BANK";
            resources.ApplyResources(this.BANK, "BANK");
            this.BANK.Name = "BANK";
            this.BANK.ReadOnly = true;
            // 
            // BRANCH
            // 
            this.BRANCH.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.BRANCH.DataPropertyName = "BRANCH";
            resources.ApplyResources(this.BRANCH, "BRANCH");
            this.BRANCH.Name = "BRANCH";
            this.BRANCH.ReadOnly = true;
            // 
            // RESULT_ID
            // 
            this.RESULT_ID.DataPropertyName = "RESULT_ID";
            resources.ApplyResources(this.RESULT_ID, "RESULT_ID");
            this.RESULT_ID.Name = "RESULT_ID";
            this.RESULT_ID.ReadOnly = true;
            // 
            // RESULT_1
            // 
            this.RESULT_1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.RESULT_1.DataPropertyName = "RESULT_NAME";
            resources.ApplyResources(this.RESULT_1, "RESULT_1");
            this.RESULT_1.Name = "RESULT_1";
            this.RESULT_1.ReadOnly = true;
            // 
            // NOTE
            // 
            this.NOTE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.NOTE.DataPropertyName = "RESULT_NOTE";
            resources.ApplyResources(this.NOTE, "NOTE");
            this.NOTE.Name = "NOTE";
            this.NOTE.ReadOnly = true;
            // 
            // TYPE
            // 
            this.TYPE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.TYPE.DataPropertyName = "PAYMENT_TYPE";
            resources.ApplyResources(this.TYPE, "TYPE");
            this.TYPE.Name = "TYPE";
            this.TYPE.ReadOnly = true;
            // 
            // PAYMENT_METHOD
            // 
            this.PAYMENT_METHOD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.PAYMENT_METHOD.DataPropertyName = "PAYMENT_METHOD";
            resources.ApplyResources(this.PAYMENT_METHOD, "PAYMENT_METHOD");
            this.PAYMENT_METHOD.Name = "PAYMENT_METHOD";
            this.PAYMENT_METHOD.ReadOnly = true;
            // 
            // txtSearch
            // 
            this.txtSearch.BackColor = System.Drawing.Color.White;
            this.txtSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtSearch, "txtSearch");
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtSearch.QuickSearch += new System.EventHandler(this.txtSearch_QuickSearch);
            // 
            // dgvSummary
            // 
            this.dgvSummary.AllowUserToAddRows = false;
            this.dgvSummary.AllowUserToDeleteRows = false;
            this.dgvSummary.AllowUserToResizeRows = false;
            this.dgvSummary.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgvSummary.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvSummary.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSummary.ColumnHeadersVisible = false;
            this.dgvSummary.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CURRENCY_CODE,
            this.TOTAL_RECORD,
            this.TOTAL_PAY,
            this.TOTAL_PAID});
            resources.ApplyResources(this.dgvSummary, "dgvSummary");
            this.dgvSummary.Name = "dgvSummary";
            this.dgvSummary.ReadOnly = true;
            this.dgvSummary.RowHeadersVisible = false;
            this.dgvSummary.RowTemplate.Height = 25;
            this.dgvSummary.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // CURRENCY_CODE
            // 
            this.CURRENCY_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.CURRENCY_CODE.DataPropertyName = "CURRENCY_CODE";
            resources.ApplyResources(this.CURRENCY_CODE, "CURRENCY_CODE");
            this.CURRENCY_CODE.Name = "CURRENCY_CODE";
            this.CURRENCY_CODE.ReadOnly = true;
            // 
            // TOTAL_RECORD
            // 
            this.TOTAL_RECORD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.TOTAL_RECORD.DataPropertyName = "TOTAL_RECORD";
            dataGridViewCellStyle1.Format = "#";
            this.TOTAL_RECORD.DefaultCellStyle = dataGridViewCellStyle1;
            resources.ApplyResources(this.TOTAL_RECORD, "TOTAL_RECORD");
            this.TOTAL_RECORD.Name = "TOTAL_RECORD";
            this.TOTAL_RECORD.ReadOnly = true;
            // 
            // TOTAL_PAY
            // 
            this.TOTAL_PAY.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TOTAL_PAY.DataPropertyName = "TOTAL_PAY";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "#,##0.####";
            this.TOTAL_PAY.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.TOTAL_PAY, "TOTAL_PAY");
            this.TOTAL_PAY.Name = "TOTAL_PAY";
            this.TOTAL_PAY.ReadOnly = true;
            // 
            // TOTAL_PAID
            // 
            this.TOTAL_PAID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TOTAL_PAID.DataPropertyName = "TOTAL_PAID";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "#,##0.####";
            this.TOTAL_PAID.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.TOTAL_PAID, "TOTAL_PAID");
            this.TOTAL_PAID.Name = "TOTAL_PAID";
            this.TOTAL_PAID.ReadOnly = true;
            // 
            // lblTOTAL
            // 
            resources.ApplyResources(this.lblTOTAL, "lblTOTAL");
            this.lblTOTAL.Name = "lblTOTAL";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.ColumnHeadersVisible = false;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RESULT,
            this.TOTAL});
            resources.ApplyResources(this.dataGridView1, "dataGridView1");
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 25;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // lblSTATUS
            // 
            resources.ApplyResources(this.lblSTATUS, "lblSTATUS");
            this.lblSTATUS.Name = "lblSTATUS";
            // 
            // cboResult
            // 
            this.cboResult.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboResult.FormattingEnabled = true;
            resources.ApplyResources(this.cboResult, "cboResult");
            this.cboResult.Name = "cboResult";
            this.cboResult.SelectedIndexChanged += new System.EventHandler(this.cboResult_SelectedIndexChanged);
            // 
            // lblRESULT
            // 
            resources.ApplyResources(this.lblRESULT, "lblRESULT");
            this.lblRESULT.Name = "lblRESULT";
            // 
            // RESULT
            // 
            this.RESULT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.RESULT.DataPropertyName = "RESULT";
            resources.ApplyResources(this.RESULT, "RESULT");
            this.RESULT.Name = "RESULT";
            this.RESULT.ReadOnly = true;
            // 
            // TOTAL
            // 
            this.TOTAL.DataPropertyName = "TOTAL";
            resources.ApplyResources(this.TOTAL, "TOTAL");
            this.TOTAL.Name = "TOTAL";
            this.TOTAL.ReadOnly = true;
            // 
            // DialogHistoryDetail
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogHistoryDetail";
            this.Load += new System.EventHandler(this.DialogBankPaymentHistoryDetail_Load);
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnClose;
        private DataGridView dgv;
        private ExTextbox txtSearch;
        private DataGridView dgvSummary;
        private Label lblTOTAL;
        private Label lblSTATUS;
        private DataGridView dataGridView1;
        private ComboBox cboResult;
        private Label lblRESULT;
        private DataGridViewTextBoxColumn CUSTOMER_CODE;
        private DataGridViewTextBoxColumn CUSTOMER_NAME;
        private DataGridViewTextBoxColumn CURRENCY_;
        private DataGridViewTextBoxColumn PAY_AMOUNT;
        private DataGridViewTextBoxColumn PAID_AMOUNT;
        private DataGridViewTextBoxColumn PAY_DATE;
        private DataGridViewTextBoxColumn CASHIER;
        private DataGridViewTextBoxColumn BANK;
        private DataGridViewTextBoxColumn BRANCH;
        private DataGridViewTextBoxColumn RESULT_ID;
        private DataGridViewTextBoxColumn RESULT_1;
        private DataGridViewTextBoxColumn NOTE;
        private DataGridViewTextBoxColumn TYPE;
        private DataGridViewTextBoxColumn PAYMENT_METHOD;
        private DataGridViewTextBoxColumn CURRENCY_CODE;
        private DataGridViewTextBoxColumn TOTAL_RECORD;
        private DataGridViewTextBoxColumn TOTAL_PAY;
        private DataGridViewTextBoxColumn TOTAL_PAID;
        private DataGridViewTextBoxColumn RESULT;
        private DataGridViewTextBoxColumn TOTAL;
    }
}