﻿using B24.Customer;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace EPower.Interface.BankPayment
{

    public partial class DialogPaymentOnline : ExDialog
    {
        string lnkPayment = "";
        public DialogPaymentOnline()
        {
            InitializeComponent();
            load();
        }

        #region Method

        private void load()
        {
            TBL_CONFIG objConfig = DBDataContext.Db.TBL_CONFIGs.FirstOrDefault();
            string md5 = MD5Hash(objConfig.AREA_CODE + "-B24");
            var client = new CustomerAPIClient("http://96.9.67.98:50025/api/v1", "1111");
            var who = client.Who();
            var bills = client.UnpaidBill();
            var payinfo = client.Payment();

            this.txtAccountNo.Text = who.code;
            this.txtAccountName.Text = who.name;
            this.txtContact.Text = who.email;
            this.txtPhone.Text = who.phone;
            this.txtTotalDueInvoice.Text = bills.Count.ToString();
            this.txtTotalDueAmount.Text = bills.Sum(x => x.total_amount).ToString("N2");
            lnkPayment = payinfo.payment_url;
        }

        #endregion

        private void btnCLOSE_Click(object sender, EventArgs e)
        {
            this.Close();
        } 

        private void btnGET_INFO_Click(object sender, EventArgs e)
        {
            
        }

        private void lblPAY_Click(object sender, EventArgs e)
        {
            if (txtAccountNo.Text.Trim() == string.Empty)
            {
                MsgBox.ShowInformation(Resources.MSG_GET_EPOWER_PAYMENT_BEFORE_PAY);
                return;
            }
            System.Diagnostics.Process.Start(lnkPayment);
        }

        public static String MD5Hash(String TextToHash)
        {
            //Check wether data was passed
            if ((TextToHash == null) || (TextToHash.Length == 0))
            {
                return String.Empty;
            }

            //Calculate MD5 hash. This requires that the string is splitted into a byte[].
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] textToHash = Encoding.Default.GetBytes(TextToHash);
            byte[] result = md5.ComputeHash(textToHash);

            //Convert result back to string.
            return System.BitConverter.ToString(result);
        }
    }
}
