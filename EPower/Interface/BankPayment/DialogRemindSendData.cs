﻿using EPower.Base.Logic;
using SoftTech.Component;
using SoftTech.Helper;
using System;

namespace EPower.Interface.BankPayment
{
    public partial class DialogRemindSendData : ExDialog
    {
        public static bool IsRequiredReminder()
        {
            var autoSend = DataHelper.ParseToBoolean(Method.GetUtilityValue(Utility.AUTO_SEND_DATA_TO_BANK));
            if (autoSend)
            {
                PaymentOperation.RunAutoSendDataInNewThread();
                return false;
            }
            return DataHelper.ParseToBoolean(Method.GetUtilityValue(Utility.BANK_PAYMENT_ENABLE));
        }

        public DialogRemindSendData()
        {
            InitializeComponent();
            // if no Auto Send it will Check Auto
            //this.chkAUTO_SEND.Checked = DataHelper.ParseToBoolean(Method.GetUtilityValue(Utility.AUTO_SEND_DATA_TO_BANK));
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSendNow_Click(object sender, EventArgs e)
        {
            var diag = new FormEPowerClient();
            var result = diag.ShowDialog();
        }

        private void chkAutoSend_CheckedChanged(object sender, EventArgs e)
        {
            Method.SetUtilityValue(Utility.AUTO_SEND_DATA_TO_BANK, this.chkAUTO_SEND.Checked ? "1" : "0");
        }
    }
}
