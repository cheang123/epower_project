﻿namespace EPower.Interface.BankPayment
{
    partial class DialogAcledaAgreement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCOMPANY = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.txtCompany = new System.Windows.Forms.TextBox();
            this.btnPRINT = new SoftTech.Component.ExButton();
            this.txtBuildingNo = new System.Windows.Forms.TextBox();
            this.lblBUIDING_NO = new System.Windows.Forms.Label();
            this.lblSTREET = new System.Windows.Forms.Label();
            this.txtStreet = new System.Windows.Forms.TextBox();
            this.lblDISTRICT = new System.Windows.Forms.Label();
            this.lblProvince = new System.Windows.Forms.Label();
            this.lblREPRESENTATIVE = new System.Windows.Forms.Label();
            this.txtRepresentative = new System.Windows.Forms.TextBox();
            this.lblPOSITION = new System.Windows.Forms.Label();
            this.txtPosition = new System.Windows.Forms.TextBox();
            this.lblFAX = new System.Windows.Forms.Label();
            this.txtFax = new System.Windows.Forms.TextBox();
            this.lblPHONE = new System.Windows.Forms.Label();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.lblEMAIL = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lblACCOUNT_NAME_KHR = new System.Windows.Forms.Label();
            this.txtAccNameKHR = new System.Windows.Forms.TextBox();
            this.lblACCOUNT_NO = new System.Windows.Forms.Label();
            this.txtAccNoKHR = new System.Windows.Forms.TextBox();
            this.lblCOMMUNE = new System.Windows.Forms.Label();
            this.lblCOMPANY_INFO = new System.Windows.Forms.Label();
            this.lblBANK_INFO = new System.Windows.Forms.Label();
            this.cboProvince = new System.Windows.Forms.ComboBox();
            this.cboDistrict = new System.Windows.Forms.ComboBox();
            this.cboCommune = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtAccNoUSD = new System.Windows.Forms.TextBox();
            this.lblACCOUNT_NAME_USD = new System.Windows.Forms.Label();
            this.txtAccNameUSD = new System.Windows.Forms.TextBox();
            this.btnEMAIL_TO_BANK = new SoftTech.Component.ExButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.txtAccNameTHB = new System.Windows.Forms.TextBox();
            this.lblACCOUNT_NAME_THB = new System.Windows.Forms.Label();
            this.txtAccNoTHB = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnCLEAR_FLAG = new SoftTech.Component.ExButton();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.label8);
            this.content.Controls.Add(this.label4);
            this.content.Controls.Add(this.txtAccNoTHB);
            this.content.Controls.Add(this.lblACCOUNT_NAME_THB);
            this.content.Controls.Add(this.txtAccNoUSD);
            this.content.Controls.Add(this.txtAccNameTHB);
            this.content.Controls.Add(this.lblACCOUNT_NAME_USD);
            this.content.Controls.Add(this.txtAccNameUSD);
            this.content.Controls.Add(this.label6);
            this.content.Controls.Add(this.label2);
            this.content.Controls.Add(this.cboCommune);
            this.content.Controls.Add(this.cboDistrict);
            this.content.Controls.Add(this.cboProvince);
            this.content.Controls.Add(this.lblBANK_INFO);
            this.content.Controls.Add(this.lblCOMPANY_INFO);
            this.content.Controls.Add(this.lblACCOUNT_NO);
            this.content.Controls.Add(this.txtAccNoKHR);
            this.content.Controls.Add(this.lblACCOUNT_NAME_KHR);
            this.content.Controls.Add(this.txtAccNameKHR);
            this.content.Controls.Add(this.lblEMAIL);
            this.content.Controls.Add(this.txtEmail);
            this.content.Controls.Add(this.lblFAX);
            this.content.Controls.Add(this.txtFax);
            this.content.Controls.Add(this.lblPHONE);
            this.content.Controls.Add(this.txtPhone);
            this.content.Controls.Add(this.lblPOSITION);
            this.content.Controls.Add(this.txtPosition);
            this.content.Controls.Add(this.lblREPRESENTATIVE);
            this.content.Controls.Add(this.txtRepresentative);
            this.content.Controls.Add(this.lblProvince);
            this.content.Controls.Add(this.lblDISTRICT);
            this.content.Controls.Add(this.lblCOMMUNE);
            this.content.Controls.Add(this.lblSTREET);
            this.content.Controls.Add(this.txtStreet);
            this.content.Controls.Add(this.lblBUIDING_NO);
            this.content.Controls.Add(this.txtBuildingNo);
            this.content.Controls.Add(this.lblCOMPANY);
            this.content.Controls.Add(this.panel2);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.btnCLEAR_FLAG);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.txtCompany);
            this.content.Controls.Add(this.btnEMAIL_TO_BANK);
            this.content.Controls.Add(this.btnPRINT);
            this.content.Size = new System.Drawing.Size(640, 428);
            this.content.Text = "CONTENT";
            this.content.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            this.content.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.content_MouseDoubleClick);
            this.content.Controls.SetChildIndex(this.btnPRINT, 0);
            this.content.Controls.SetChildIndex(this.btnEMAIL_TO_BANK, 0);
            this.content.Controls.SetChildIndex(this.txtCompany, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.btnCLEAR_FLAG, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.panel2, 0);
            this.content.Controls.SetChildIndex(this.lblCOMPANY, 0);
            this.content.Controls.SetChildIndex(this.txtBuildingNo, 0);
            this.content.Controls.SetChildIndex(this.lblBUIDING_NO, 0);
            this.content.Controls.SetChildIndex(this.txtStreet, 0);
            this.content.Controls.SetChildIndex(this.lblSTREET, 0);
            this.content.Controls.SetChildIndex(this.lblCOMMUNE, 0);
            this.content.Controls.SetChildIndex(this.lblDISTRICT, 0);
            this.content.Controls.SetChildIndex(this.lblProvince, 0);
            this.content.Controls.SetChildIndex(this.txtRepresentative, 0);
            this.content.Controls.SetChildIndex(this.lblREPRESENTATIVE, 0);
            this.content.Controls.SetChildIndex(this.txtPosition, 0);
            this.content.Controls.SetChildIndex(this.lblPOSITION, 0);
            this.content.Controls.SetChildIndex(this.txtPhone, 0);
            this.content.Controls.SetChildIndex(this.lblPHONE, 0);
            this.content.Controls.SetChildIndex(this.txtFax, 0);
            this.content.Controls.SetChildIndex(this.lblFAX, 0);
            this.content.Controls.SetChildIndex(this.txtEmail, 0);
            this.content.Controls.SetChildIndex(this.lblEMAIL, 0);
            this.content.Controls.SetChildIndex(this.txtAccNameKHR, 0);
            this.content.Controls.SetChildIndex(this.lblACCOUNT_NAME_KHR, 0);
            this.content.Controls.SetChildIndex(this.txtAccNoKHR, 0);
            this.content.Controls.SetChildIndex(this.lblACCOUNT_NO, 0);
            this.content.Controls.SetChildIndex(this.lblCOMPANY_INFO, 0);
            this.content.Controls.SetChildIndex(this.lblBANK_INFO, 0);
            this.content.Controls.SetChildIndex(this.cboProvince, 0);
            this.content.Controls.SetChildIndex(this.cboDistrict, 0);
            this.content.Controls.SetChildIndex(this.cboCommune, 0);
            this.content.Controls.SetChildIndex(this.label2, 0);
            this.content.Controls.SetChildIndex(this.label6, 0);
            this.content.Controls.SetChildIndex(this.txtAccNameUSD, 0);
            this.content.Controls.SetChildIndex(this.lblACCOUNT_NAME_USD, 0);
            this.content.Controls.SetChildIndex(this.txtAccNameTHB, 0);
            this.content.Controls.SetChildIndex(this.txtAccNoUSD, 0);
            this.content.Controls.SetChildIndex(this.lblACCOUNT_NAME_THB, 0);
            this.content.Controls.SetChildIndex(this.txtAccNoTHB, 0);
            this.content.Controls.SetChildIndex(this.label4, 0);
            this.content.Controls.SetChildIndex(this.label8, 0);
            // 
            // lblCOMPANY
            // 
            this.lblCOMPANY.AutoSize = true;
            this.lblCOMPANY.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblCOMPANY.Location = new System.Drawing.Point(11, 45);
            this.lblCOMPANY.Margin = new System.Windows.Forms.Padding(2);
            this.lblCOMPANY.Name = "lblCOMPANY";
            this.lblCOMPANY.Size = new System.Drawing.Size(80, 19);
            this.lblCOMPANY.TabIndex = 68;
            this.lblCOMPANY.Text = "ឈ្មោះក្រុមហ៊ុន";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Silver;
            this.panel2.Location = new System.Drawing.Point(0, 393);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(630, 1);
            this.panel2.TabIndex = 51;
            // 
            // btnCLOSE
            // 
            this.btnCLOSE.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnCLOSE.Location = new System.Drawing.Point(553, 399);
            this.btnCLOSE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.Size = new System.Drawing.Size(75, 23);
            this.btnCLOSE.TabIndex = 19;
            this.btnCLOSE.Text = "បិទ";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnCLOSE_Click);
            // 
            // txtCompany
            // 
            this.txtCompany.BackColor = System.Drawing.Color.White;
            this.txtCompany.Location = new System.Drawing.Point(152, 42);
            this.txtCompany.Margin = new System.Windows.Forms.Padding(2);
            this.txtCompany.Name = "txtCompany";
            this.txtCompany.Size = new System.Drawing.Size(476, 27);
            this.txtCompany.TabIndex = 0;
            this.txtCompany.Enter += new System.EventHandler(this.ChangeKeyboard);
            // 
            // btnPRINT
            // 
            this.btnPRINT.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnPRINT.Location = new System.Drawing.Point(339, 399);
            this.btnPRINT.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPRINT.Name = "btnPRINT";
            this.btnPRINT.Size = new System.Drawing.Size(75, 23);
            this.btnPRINT.TabIndex = 18;
            this.btnPRINT.Text = "បោះពុម្ភ";
            this.btnPRINT.UseVisualStyleBackColor = true;
            this.btnPRINT.Click += new System.EventHandler(this.btnPRINT_Click);
            // 
            // txtBuildingNo
            // 
            this.txtBuildingNo.BackColor = System.Drawing.Color.White;
            this.txtBuildingNo.Location = new System.Drawing.Point(152, 76);
            this.txtBuildingNo.Margin = new System.Windows.Forms.Padding(2);
            this.txtBuildingNo.Name = "txtBuildingNo";
            this.txtBuildingNo.Size = new System.Drawing.Size(190, 27);
            this.txtBuildingNo.TabIndex = 1;
            this.txtBuildingNo.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // lblBUIDING_NO
            // 
            this.lblBUIDING_NO.AutoSize = true;
            this.lblBUIDING_NO.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblBUIDING_NO.Location = new System.Drawing.Point(11, 79);
            this.lblBUIDING_NO.Margin = new System.Windows.Forms.Padding(2);
            this.lblBUIDING_NO.Name = "lblBUIDING_NO";
            this.lblBUIDING_NO.Size = new System.Drawing.Size(57, 19);
            this.lblBUIDING_NO.TabIndex = 70;
            this.lblBUIDING_NO.Text = "អគារលេខ";
            // 
            // lblSTREET
            // 
            this.lblSTREET.AutoSize = true;
            this.lblSTREET.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblSTREET.Location = new System.Drawing.Point(353, 80);
            this.lblSTREET.Margin = new System.Windows.Forms.Padding(2);
            this.lblSTREET.Name = "lblSTREET";
            this.lblSTREET.Size = new System.Drawing.Size(21, 19);
            this.lblSTREET.TabIndex = 72;
            this.lblSTREET.Text = "វិថី";
            // 
            // txtStreet
            // 
            this.txtStreet.BackColor = System.Drawing.Color.White;
            this.txtStreet.Location = new System.Drawing.Point(438, 76);
            this.txtStreet.Margin = new System.Windows.Forms.Padding(2);
            this.txtStreet.Name = "txtStreet";
            this.txtStreet.Size = new System.Drawing.Size(190, 27);
            this.txtStreet.TabIndex = 2;
            this.txtStreet.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // lblDISTRICT
            // 
            this.lblDISTRICT.AutoSize = true;
            this.lblDISTRICT.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblDISTRICT.Location = new System.Drawing.Point(353, 113);
            this.lblDISTRICT.Margin = new System.Windows.Forms.Padding(2);
            this.lblDISTRICT.Name = "lblDISTRICT";
            this.lblDISTRICT.Size = new System.Drawing.Size(61, 19);
            this.lblDISTRICT.TabIndex = 76;
            this.lblDISTRICT.Text = "ស្រុក/ខណ្ឌ";
            // 
            // lblProvince
            // 
            this.lblProvince.AutoSize = true;
            this.lblProvince.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblProvince.Location = new System.Drawing.Point(11, 113);
            this.lblProvince.Margin = new System.Windows.Forms.Padding(2);
            this.lblProvince.Name = "lblProvince";
            this.lblProvince.Size = new System.Drawing.Size(52, 19);
            this.lblProvince.TabIndex = 78;
            this.lblProvince.Text = "ខេត្ត/ក្រុង";
            // 
            // lblREPRESENTATIVE
            // 
            this.lblREPRESENTATIVE.AutoSize = true;
            this.lblREPRESENTATIVE.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblREPRESENTATIVE.Location = new System.Drawing.Point(11, 214);
            this.lblREPRESENTATIVE.Margin = new System.Windows.Forms.Padding(2);
            this.lblREPRESENTATIVE.Name = "lblREPRESENTATIVE";
            this.lblREPRESENTATIVE.Size = new System.Drawing.Size(62, 19);
            this.lblREPRESENTATIVE.TabIndex = 82;
            this.lblREPRESENTATIVE.Text = "អ្នកតំណាង";
            // 
            // txtRepresentative
            // 
            this.txtRepresentative.BackColor = System.Drawing.Color.White;
            this.txtRepresentative.Location = new System.Drawing.Point(152, 213);
            this.txtRepresentative.Margin = new System.Windows.Forms.Padding(2);
            this.txtRepresentative.Name = "txtRepresentative";
            this.txtRepresentative.Size = new System.Drawing.Size(190, 27);
            this.txtRepresentative.TabIndex = 9;
            this.txtRepresentative.Enter += new System.EventHandler(this.ChangeKeyboard);
            // 
            // lblPOSITION
            // 
            this.lblPOSITION.AutoSize = true;
            this.lblPOSITION.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblPOSITION.Location = new System.Drawing.Point(354, 216);
            this.lblPOSITION.Margin = new System.Windows.Forms.Padding(2);
            this.lblPOSITION.Name = "lblPOSITION";
            this.lblPOSITION.Size = new System.Drawing.Size(41, 19);
            this.lblPOSITION.TabIndex = 84;
            this.lblPOSITION.Text = "មុខងារ";
            // 
            // txtPosition
            // 
            this.txtPosition.BackColor = System.Drawing.Color.White;
            this.txtPosition.Location = new System.Drawing.Point(438, 211);
            this.txtPosition.Margin = new System.Windows.Forms.Padding(2);
            this.txtPosition.Name = "txtPosition";
            this.txtPosition.Size = new System.Drawing.Size(190, 27);
            this.txtPosition.TabIndex = 10;
            this.txtPosition.Enter += new System.EventHandler(this.ChangeKeyboard);
            // 
            // lblFAX
            // 
            this.lblFAX.AutoSize = true;
            this.lblFAX.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblFAX.Location = new System.Drawing.Point(353, 181);
            this.lblFAX.Margin = new System.Windows.Forms.Padding(2);
            this.lblFAX.Name = "lblFAX";
            this.lblFAX.Size = new System.Drawing.Size(42, 19);
            this.lblFAX.TabIndex = 88;
            this.lblFAX.Text = "ទូរសារ";
            // 
            // txtFax
            // 
            this.txtFax.BackColor = System.Drawing.Color.White;
            this.txtFax.Location = new System.Drawing.Point(438, 178);
            this.txtFax.Margin = new System.Windows.Forms.Padding(2);
            this.txtFax.Name = "txtFax";
            this.txtFax.Size = new System.Drawing.Size(190, 27);
            this.txtFax.TabIndex = 8;
            this.txtFax.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // lblPHONE
            // 
            this.lblPHONE.AutoSize = true;
            this.lblPHONE.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblPHONE.Location = new System.Drawing.Point(354, 147);
            this.lblPHONE.Margin = new System.Windows.Forms.Padding(2);
            this.lblPHONE.Name = "lblPHONE";
            this.lblPHONE.Size = new System.Drawing.Size(65, 19);
            this.lblPHONE.TabIndex = 86;
            this.lblPHONE.Text = "លេខទូរសព្ទ";
            // 
            // txtPhone
            // 
            this.txtPhone.BackColor = System.Drawing.Color.White;
            this.txtPhone.Location = new System.Drawing.Point(438, 144);
            this.txtPhone.Margin = new System.Windows.Forms.Padding(2);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(190, 27);
            this.txtPhone.TabIndex = 6;
            this.txtPhone.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // lblEMAIL
            // 
            this.lblEMAIL.AutoSize = true;
            this.lblEMAIL.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblEMAIL.Location = new System.Drawing.Point(13, 182);
            this.lblEMAIL.Margin = new System.Windows.Forms.Padding(2);
            this.lblEMAIL.Name = "lblEMAIL";
            this.lblEMAIL.Size = new System.Drawing.Size(41, 19);
            this.lblEMAIL.TabIndex = 90;
            this.lblEMAIL.Text = "អ៊ីម៉ែល";
            // 
            // txtEmail
            // 
            this.txtEmail.BackColor = System.Drawing.Color.White;
            this.txtEmail.Location = new System.Drawing.Point(152, 178);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(2);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(190, 27);
            this.txtEmail.TabIndex = 7;
            this.txtEmail.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // lblACCOUNT_NAME_KHR
            // 
            this.lblACCOUNT_NAME_KHR.AutoSize = true;
            this.lblACCOUNT_NAME_KHR.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblACCOUNT_NAME_KHR.Location = new System.Drawing.Point(13, 296);
            this.lblACCOUNT_NAME_KHR.Margin = new System.Windows.Forms.Padding(2);
            this.lblACCOUNT_NAME_KHR.Name = "lblACCOUNT_NAME_KHR";
            this.lblACCOUNT_NAME_KHR.Size = new System.Drawing.Size(105, 19);
            this.lblACCOUNT_NAME_KHR.TabIndex = 92;
            this.lblACCOUNT_NAME_KHR.Text = "ឈ្មោះគណនី(រៀល)";
            // 
            // txtAccNameKHR
            // 
            this.txtAccNameKHR.BackColor = System.Drawing.Color.White;
            this.txtAccNameKHR.Location = new System.Drawing.Point(153, 293);
            this.txtAccNameKHR.Margin = new System.Windows.Forms.Padding(2);
            this.txtAccNameKHR.Name = "txtAccNameKHR";
            this.txtAccNameKHR.Size = new System.Drawing.Size(190, 27);
            this.txtAccNameKHR.TabIndex = 11;
            this.txtAccNameKHR.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // lblACCOUNT_NO
            // 
            this.lblACCOUNT_NO.AutoSize = true;
            this.lblACCOUNT_NO.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblACCOUNT_NO.Location = new System.Drawing.Point(353, 296);
            this.lblACCOUNT_NO.Margin = new System.Windows.Forms.Padding(2);
            this.lblACCOUNT_NO.Name = "lblACCOUNT_NO";
            this.lblACCOUNT_NO.Size = new System.Drawing.Size(66, 19);
            this.lblACCOUNT_NO.TabIndex = 94;
            this.lblACCOUNT_NO.Text = "លេខគណនី";
            // 
            // txtAccNoKHR
            // 
            this.txtAccNoKHR.BackColor = System.Drawing.Color.White;
            this.txtAccNoKHR.Location = new System.Drawing.Point(438, 293);
            this.txtAccNoKHR.Margin = new System.Windows.Forms.Padding(2);
            this.txtAccNoKHR.Name = "txtAccNoKHR";
            this.txtAccNoKHR.Size = new System.Drawing.Size(190, 27);
            this.txtAccNoKHR.TabIndex = 12;
            this.txtAccNoKHR.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // lblCOMMUNE
            // 
            this.lblCOMMUNE.AutoSize = true;
            this.lblCOMMUNE.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblCOMMUNE.Location = new System.Drawing.Point(11, 147);
            this.lblCOMMUNE.Margin = new System.Windows.Forms.Padding(2);
            this.lblCOMMUNE.Name = "lblCOMMUNE";
            this.lblCOMMUNE.Size = new System.Drawing.Size(56, 19);
            this.lblCOMMUNE.TabIndex = 74;
            this.lblCOMMUNE.Text = "ឃុំ/សង្កាត់";
            // 
            // lblCOMPANY_INFO
            // 
            this.lblCOMPANY_INFO.AutoSize = true;
            this.lblCOMPANY_INFO.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblCOMPANY_INFO.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblCOMPANY_INFO.Location = new System.Drawing.Point(11, 16);
            this.lblCOMPANY_INFO.Margin = new System.Windows.Forms.Padding(2);
            this.lblCOMPANY_INFO.Name = "lblCOMPANY_INFO";
            this.lblCOMPANY_INFO.Size = new System.Drawing.Size(94, 19);
            this.lblCOMPANY_INFO.TabIndex = 95;
            this.lblCOMPANY_INFO.Text = "ព័ត៌មានក្រុមហ៊ុន";
            // 
            // lblBANK_INFO
            // 
            this.lblBANK_INFO.AutoSize = true;
            this.lblBANK_INFO.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblBANK_INFO.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblBANK_INFO.Location = new System.Drawing.Point(11, 260);
            this.lblBANK_INFO.Margin = new System.Windows.Forms.Padding(2);
            this.lblBANK_INFO.Name = "lblBANK_INFO";
            this.lblBANK_INFO.Size = new System.Drawing.Size(89, 19);
            this.lblBANK_INFO.TabIndex = 96;
            this.lblBANK_INFO.Text = "ព័ត៌មានធនាគារ";
            // 
            // cboProvince
            // 
            this.cboProvince.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboProvince.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.cboProvince.FormattingEnabled = true;
            this.cboProvince.Location = new System.Drawing.Point(152, 110);
            this.cboProvince.Margin = new System.Windows.Forms.Padding(2);
            this.cboProvince.Name = "cboProvince";
            this.cboProvince.Size = new System.Drawing.Size(190, 27);
            this.cboProvince.TabIndex = 3;
            this.cboProvince.SelectedIndexChanged += new System.EventHandler(this.cboProvince_SelectedIndexChanged);
            // 
            // cboDistrict
            // 
            this.cboDistrict.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDistrict.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.cboDistrict.FormattingEnabled = true;
            this.cboDistrict.Location = new System.Drawing.Point(438, 110);
            this.cboDistrict.Margin = new System.Windows.Forms.Padding(2);
            this.cboDistrict.Name = "cboDistrict";
            this.cboDistrict.Size = new System.Drawing.Size(190, 27);
            this.cboDistrict.TabIndex = 4;
            this.cboDistrict.SelectedIndexChanged += new System.EventHandler(this.cboDistrict_SelectedIndexChanged);
            // 
            // cboCommune
            // 
            this.cboCommune.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCommune.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.cboCommune.FormattingEnabled = true;
            this.cboCommune.Location = new System.Drawing.Point(152, 144);
            this.cboCommune.Margin = new System.Windows.Forms.Padding(2);
            this.cboCommune.Name = "cboCommune";
            this.cboCommune.Size = new System.Drawing.Size(190, 27);
            this.cboCommune.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label4.Location = new System.Drawing.Point(353, 328);
            this.label4.Margin = new System.Windows.Forms.Padding(2);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 19);
            this.label4.TabIndex = 303;
            this.label4.Text = "លេខគណនី";
            // 
            // txtAccNoUSD
            // 
            this.txtAccNoUSD.BackColor = System.Drawing.Color.White;
            this.txtAccNoUSD.Location = new System.Drawing.Point(438, 325);
            this.txtAccNoUSD.Margin = new System.Windows.Forms.Padding(2);
            this.txtAccNoUSD.Name = "txtAccNoUSD";
            this.txtAccNoUSD.Size = new System.Drawing.Size(190, 27);
            this.txtAccNoUSD.TabIndex = 14;
            this.txtAccNoUSD.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // lblACCOUNT_NAME_USD
            // 
            this.lblACCOUNT_NAME_USD.AutoSize = true;
            this.lblACCOUNT_NAME_USD.CausesValidation = false;
            this.lblACCOUNT_NAME_USD.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblACCOUNT_NAME_USD.Location = new System.Drawing.Point(13, 328);
            this.lblACCOUNT_NAME_USD.Margin = new System.Windows.Forms.Padding(2);
            this.lblACCOUNT_NAME_USD.Name = "lblACCOUNT_NAME_USD";
            this.lblACCOUNT_NAME_USD.Size = new System.Drawing.Size(106, 19);
            this.lblACCOUNT_NAME_USD.TabIndex = 302;
            this.lblACCOUNT_NAME_USD.Text = "ឈ្មោះគណនី(ដុល្លា)";
            // 
            // txtAccNameUSD
            // 
            this.txtAccNameUSD.BackColor = System.Drawing.Color.White;
            this.txtAccNameUSD.Location = new System.Drawing.Point(153, 325);
            this.txtAccNameUSD.Margin = new System.Windows.Forms.Padding(2);
            this.txtAccNameUSD.Name = "txtAccNameUSD";
            this.txtAccNameUSD.Size = new System.Drawing.Size(190, 27);
            this.txtAccNameUSD.TabIndex = 13;
            this.txtAccNameUSD.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // btnEMAIL_TO_BANK
            // 
            this.btnEMAIL_TO_BANK.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnEMAIL_TO_BANK.Location = new System.Drawing.Point(420, 399);
            this.btnEMAIL_TO_BANK.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnEMAIL_TO_BANK.Name = "btnEMAIL_TO_BANK";
            this.btnEMAIL_TO_BANK.Size = new System.Drawing.Size(127, 23);
            this.btnEMAIL_TO_BANK.TabIndex = 17;
            this.btnEMAIL_TO_BANK.Text = "ចុះឈ្មោះសេវាធនាគារ";
            this.btnEMAIL_TO_BANK.UseVisualStyleBackColor = true;
            this.btnEMAIL_TO_BANK.Click += new System.EventHandler(this.btnEMAIL_TO_BANK_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(90, 39);
            this.label2.Margin = new System.Windows.Forms.Padding(2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 20);
            this.label2.TabIndex = 298;
            this.label2.Text = "*";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label6.Location = new System.Drawing.Point(414, 142);
            this.label6.Margin = new System.Windows.Forms.Padding(2);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(15, 20);
            this.label6.TabIndex = 299;
            this.label6.Text = "*";
            // 
            // btnCHANGE_LOG
            // 
            this.btnCHANGE_LOG.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnCHANGE_LOG.Location = new System.Drawing.Point(10, 399);
            this.btnCHANGE_LOG.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.Size = new System.Drawing.Size(75, 23);
            this.btnCHANGE_LOG.TabIndex = 16;
            this.btnCHANGE_LOG.Text = "ប្រវត្តិកែប្រែ";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Visible = false;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnCHANGE_LOG_Click);
            // 
            // txtAccNameTHB
            // 
            this.txtAccNameTHB.BackColor = System.Drawing.Color.White;
            this.txtAccNameTHB.Location = new System.Drawing.Point(153, 356);
            this.txtAccNameTHB.Margin = new System.Windows.Forms.Padding(2);
            this.txtAccNameTHB.Name = "txtAccNameTHB";
            this.txtAccNameTHB.Size = new System.Drawing.Size(190, 27);
            this.txtAccNameTHB.TabIndex = 15;
            this.txtAccNameTHB.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // lblACCOUNT_NAME_THB
            // 
            this.lblACCOUNT_NAME_THB.AutoSize = true;
            this.lblACCOUNT_NAME_THB.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblACCOUNT_NAME_THB.Location = new System.Drawing.Point(13, 359);
            this.lblACCOUNT_NAME_THB.Margin = new System.Windows.Forms.Padding(2);
            this.lblACCOUNT_NAME_THB.Name = "lblACCOUNT_NAME_THB";
            this.lblACCOUNT_NAME_THB.Size = new System.Drawing.Size(101, 19);
            this.lblACCOUNT_NAME_THB.TabIndex = 302;
            this.lblACCOUNT_NAME_THB.Text = "ឈ្មោះគណនី(បាត)";
            // 
            // txtAccNoTHB
            // 
            this.txtAccNoTHB.BackColor = System.Drawing.Color.White;
            this.txtAccNoTHB.Location = new System.Drawing.Point(438, 356);
            this.txtAccNoTHB.Margin = new System.Windows.Forms.Padding(2);
            this.txtAccNoTHB.Name = "txtAccNoTHB";
            this.txtAccNoTHB.Size = new System.Drawing.Size(190, 27);
            this.txtAccNoTHB.TabIndex = 16;
            this.txtAccNoTHB.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label8.Location = new System.Drawing.Point(353, 359);
            this.label8.Margin = new System.Windows.Forms.Padding(2);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 19);
            this.label8.TabIndex = 303;
            this.label8.Text = "លេខគណនី";
            // 
            // btnCLEAR_FLAG
            // 
            this.btnCLEAR_FLAG.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnCLEAR_FLAG.Location = new System.Drawing.Point(242, 399);
            this.btnCLEAR_FLAG.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCLEAR_FLAG.Name = "btnCLEAR_FLAG";
            this.btnCLEAR_FLAG.Size = new System.Drawing.Size(91, 23);
            this.btnCLEAR_FLAG.TabIndex = 19;
            this.btnCLEAR_FLAG.Text = "លុបកាចងចាំ";
            this.btnCLEAR_FLAG.UseVisualStyleBackColor = true;
            this.btnCLEAR_FLAG.Visible = false;
            this.btnCLEAR_FLAG.Click += new System.EventHandler(this.btnCLEAR_FLAG_Click);
            // 
            // DialogAcledaAgreement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(640, 451);
            this.Name = "DialogAcledaAgreement";
            this.Text = "កិច្ចព្រមព្រៀងអេស៊ីលីដា";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtBuildingNo;
        private System.Windows.Forms.Label lblCOMPANY;
        private System.Windows.Forms.Panel panel2;
        private SoftTech.Component.ExButton btnCLOSE;
        private System.Windows.Forms.TextBox txtCompany;
        private SoftTech.Component.ExButton btnPRINT;
        private System.Windows.Forms.Label lblDISTRICT;
        private System.Windows.Forms.Label lblSTREET;
        private System.Windows.Forms.TextBox txtStreet;
        private System.Windows.Forms.Label lblBUIDING_NO;
        private System.Windows.Forms.Label lblProvince;
        private System.Windows.Forms.Label lblFAX;
        private System.Windows.Forms.TextBox txtFax;
        private System.Windows.Forms.Label lblPHONE;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.Label lblPOSITION;
        private System.Windows.Forms.TextBox txtPosition;
        private System.Windows.Forms.Label lblREPRESENTATIVE;
        private System.Windows.Forms.TextBox txtRepresentative;
        private System.Windows.Forms.Label lblEMAIL;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblACCOUNT_NO;
        private System.Windows.Forms.TextBox txtAccNoKHR;
        private System.Windows.Forms.Label lblACCOUNT_NAME_KHR;
        private System.Windows.Forms.TextBox txtAccNameKHR;
        private System.Windows.Forms.Label lblCOMMUNE;
        private System.Windows.Forms.Label lblBANK_INFO;
        private System.Windows.Forms.Label lblCOMPANY_INFO;
        private System.Windows.Forms.ComboBox cboProvince;
        private System.Windows.Forms.ComboBox cboCommune;
        private System.Windows.Forms.ComboBox cboDistrict;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtAccNoUSD;
        private System.Windows.Forms.Label lblACCOUNT_NAME_USD;
        private System.Windows.Forms.TextBox txtAccNameUSD;
        private SoftTech.Component.ExButton btnEMAIL_TO_BANK;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private SoftTech.Component.ExButton btnCHANGE_LOG;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtAccNoTHB;
        private System.Windows.Forms.Label lblACCOUNT_NAME_THB;
        private System.Windows.Forms.TextBox txtAccNameTHB;
        private SoftTech.Component.ExButton btnCLEAR_FLAG;
    }
}