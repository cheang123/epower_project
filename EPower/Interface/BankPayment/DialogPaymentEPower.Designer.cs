﻿namespace EPower.Interface.BankPayment
{
    partial class DialogPaymentOnline
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblACCOUNT_NO = new System.Windows.Forms.Label();
            this.txtAccountNo = new System.Windows.Forms.TextBox();
            this.txtContact = new System.Windows.Forms.TextBox();
            this.lblCONTACT = new System.Windows.Forms.Label();
            this.lblPHONE = new System.Windows.Forms.Label();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.lblADDRESS = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.lblTOTAL_DUE_INVOICE = new System.Windows.Forms.Label();
            this.txtTotalDueInvoice = new System.Windows.Forms.TextBox();
            this.lblTOTAL_DUE_AMOUNT1 = new System.Windows.Forms.Label();
            this.txtTotalDueAmount = new System.Windows.Forms.TextBox();
            this.lblCOMPANY_INFO = new System.Windows.Forms.Label();
            this.lblDUE_INVOICE_INFO = new System.Windows.Forms.Label();
            this.lblACCOUNT_NAME = new System.Windows.Forms.Label();
            this.txtAccountName = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnGET_INFO = new System.Windows.Forms.Label();
            this.lblPAY = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.panel2);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.lblDUE_INVOICE_INFO);
            this.content.Controls.Add(this.lblCOMPANY_INFO);
            this.content.Controls.Add(this.lblTOTAL_DUE_AMOUNT1);
            this.content.Controls.Add(this.txtTotalDueAmount);
            this.content.Controls.Add(this.lblPAY);
            this.content.Controls.Add(this.btnGET_INFO);
            this.content.Controls.Add(this.lblTOTAL_DUE_INVOICE);
            this.content.Controls.Add(this.txtTotalDueInvoice);
            this.content.Controls.Add(this.lblADDRESS);
            this.content.Controls.Add(this.txtAddress);
            this.content.Controls.Add(this.lblACCOUNT_NAME);
            this.content.Controls.Add(this.lblPHONE);
            this.content.Controls.Add(this.txtAccountName);
            this.content.Controls.Add(this.txtPhone);
            this.content.Controls.Add(this.lblCONTACT);
            this.content.Controls.Add(this.txtContact);
            this.content.Controls.Add(this.lblACCOUNT_NO);
            this.content.Controls.Add(this.txtAccountNo);
            this.content.Size = new System.Drawing.Size(576, 310);
            this.content.Text = "CONTENT";
            this.content.Controls.SetChildIndex(this.txtAccountNo, 0);
            this.content.Controls.SetChildIndex(this.lblACCOUNT_NO, 0);
            this.content.Controls.SetChildIndex(this.txtContact, 0);
            this.content.Controls.SetChildIndex(this.lblCONTACT, 0);
            this.content.Controls.SetChildIndex(this.txtPhone, 0);
            this.content.Controls.SetChildIndex(this.txtAccountName, 0);
            this.content.Controls.SetChildIndex(this.lblPHONE, 0);
            this.content.Controls.SetChildIndex(this.lblACCOUNT_NAME, 0);
            this.content.Controls.SetChildIndex(this.txtAddress, 0);
            this.content.Controls.SetChildIndex(this.lblADDRESS, 0);
            this.content.Controls.SetChildIndex(this.txtTotalDueInvoice, 0);
            this.content.Controls.SetChildIndex(this.lblTOTAL_DUE_INVOICE, 0);
            this.content.Controls.SetChildIndex(this.btnGET_INFO, 0);
            this.content.Controls.SetChildIndex(this.lblPAY, 0);
            this.content.Controls.SetChildIndex(this.txtTotalDueAmount, 0);
            this.content.Controls.SetChildIndex(this.lblTOTAL_DUE_AMOUNT1, 0);
            this.content.Controls.SetChildIndex(this.lblCOMPANY_INFO, 0);
            this.content.Controls.SetChildIndex(this.lblDUE_INVOICE_INFO, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel2, 0);
            // 
            // lblACCOUNT_NO
            // 
            this.lblACCOUNT_NO.AutoSize = true;
            this.lblACCOUNT_NO.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblACCOUNT_NO.Location = new System.Drawing.Point(11, 45);
            this.lblACCOUNT_NO.Margin = new System.Windows.Forms.Padding(2);
            this.lblACCOUNT_NO.Name = "lblACCOUNT_NO";
            this.lblACCOUNT_NO.Size = new System.Drawing.Size(49, 19);
            this.lblACCOUNT_NO.TabIndex = 68;
            this.lblACCOUNT_NO.Text = "លេខកូដ";
            // 
            // txtAccountNo
            // 
            this.txtAccountNo.BackColor = System.Drawing.Color.White;
            this.txtAccountNo.Enabled = false;
            this.txtAccountNo.Location = new System.Drawing.Point(91, 42);
            this.txtAccountNo.Margin = new System.Windows.Forms.Padding(2);
            this.txtAccountNo.Name = "txtAccountNo";
            this.txtAccountNo.ReadOnly = true;
            this.txtAccountNo.Size = new System.Drawing.Size(191, 27);
            this.txtAccountNo.TabIndex = 0;
            // 
            // txtContact
            // 
            this.txtContact.BackColor = System.Drawing.Color.White;
            this.txtContact.Enabled = false;
            this.txtContact.Location = new System.Drawing.Point(91, 76);
            this.txtContact.Margin = new System.Windows.Forms.Padding(2);
            this.txtContact.Name = "txtContact";
            this.txtContact.ReadOnly = true;
            this.txtContact.Size = new System.Drawing.Size(190, 27);
            this.txtContact.TabIndex = 1;
            // 
            // lblCONTACT
            // 
            this.lblCONTACT.AutoSize = true;
            this.lblCONTACT.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblCONTACT.Location = new System.Drawing.Point(11, 79);
            this.lblCONTACT.Margin = new System.Windows.Forms.Padding(2);
            this.lblCONTACT.Name = "lblCONTACT";
            this.lblCONTACT.Size = new System.Drawing.Size(61, 19);
            this.lblCONTACT.TabIndex = 70;
            this.lblCONTACT.Text = "ទំនាក់ទំនង";
            // 
            // lblPHONE
            // 
            this.lblPHONE.AutoSize = true;
            this.lblPHONE.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblPHONE.Location = new System.Drawing.Point(292, 80);
            this.lblPHONE.Margin = new System.Windows.Forms.Padding(2);
            this.lblPHONE.Name = "lblPHONE";
            this.lblPHONE.Size = new System.Drawing.Size(65, 19);
            this.lblPHONE.TabIndex = 72;
            this.lblPHONE.Text = "លេខទូរស័ព្ទ";
            // 
            // txtPhone
            // 
            this.txtPhone.BackColor = System.Drawing.Color.White;
            this.txtPhone.Enabled = false;
            this.txtPhone.Location = new System.Drawing.Point(377, 76);
            this.txtPhone.Margin = new System.Windows.Forms.Padding(2);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.ReadOnly = true;
            this.txtPhone.Size = new System.Drawing.Size(190, 27);
            this.txtPhone.TabIndex = 2;
            // 
            // lblADDRESS
            // 
            this.lblADDRESS.AutoSize = true;
            this.lblADDRESS.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblADDRESS.Location = new System.Drawing.Point(11, 110);
            this.lblADDRESS.Margin = new System.Windows.Forms.Padding(2);
            this.lblADDRESS.Name = "lblADDRESS";
            this.lblADDRESS.Size = new System.Drawing.Size(65, 19);
            this.lblADDRESS.TabIndex = 82;
            this.lblADDRESS.Text = "អាសយដ្ឋាន";
            // 
            // txtAddress
            // 
            this.txtAddress.BackColor = System.Drawing.Color.White;
            this.txtAddress.Enabled = false;
            this.txtAddress.Location = new System.Drawing.Point(91, 109);
            this.txtAddress.Margin = new System.Windows.Forms.Padding(2);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.ReadOnly = true;
            this.txtAddress.Size = new System.Drawing.Size(476, 27);
            this.txtAddress.TabIndex = 9;
            // 
            // lblTOTAL_DUE_INVOICE
            // 
            this.lblTOTAL_DUE_INVOICE.AutoSize = true;
            this.lblTOTAL_DUE_INVOICE.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblTOTAL_DUE_INVOICE.Location = new System.Drawing.Point(11, 203);
            this.lblTOTAL_DUE_INVOICE.Margin = new System.Windows.Forms.Padding(2);
            this.lblTOTAL_DUE_INVOICE.Name = "lblTOTAL_DUE_INVOICE";
            this.lblTOTAL_DUE_INVOICE.Size = new System.Drawing.Size(77, 19);
            this.lblTOTAL_DUE_INVOICE.TabIndex = 92;
            this.lblTOTAL_DUE_INVOICE.Text = "ចំនួនវិក្កយបត្រ";
            // 
            // txtTotalDueInvoice
            // 
            this.txtTotalDueInvoice.BackColor = System.Drawing.Color.White;
            this.txtTotalDueInvoice.Enabled = false;
            this.txtTotalDueInvoice.Location = new System.Drawing.Point(91, 200);
            this.txtTotalDueInvoice.Margin = new System.Windows.Forms.Padding(2);
            this.txtTotalDueInvoice.Name = "txtTotalDueInvoice";
            this.txtTotalDueInvoice.ReadOnly = true;
            this.txtTotalDueInvoice.Size = new System.Drawing.Size(190, 27);
            this.txtTotalDueInvoice.TabIndex = 11;
            this.txtTotalDueInvoice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblTOTAL_DUE_AMOUNT1
            // 
            this.lblTOTAL_DUE_AMOUNT1.AutoSize = true;
            this.lblTOTAL_DUE_AMOUNT1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblTOTAL_DUE_AMOUNT1.Location = new System.Drawing.Point(292, 203);
            this.lblTOTAL_DUE_AMOUNT1.Margin = new System.Windows.Forms.Padding(2);
            this.lblTOTAL_DUE_AMOUNT1.Name = "lblTOTAL_DUE_AMOUNT1";
            this.lblTOTAL_DUE_AMOUNT1.Size = new System.Drawing.Size(73, 19);
            this.lblTOTAL_DUE_AMOUNT1.TabIndex = 94;
            this.lblTOTAL_DUE_AMOUNT1.Text = "ទឹកប្រាក់សរុប";
            // 
            // txtTotalDueAmount
            // 
            this.txtTotalDueAmount.BackColor = System.Drawing.Color.White;
            this.txtTotalDueAmount.Enabled = false;
            this.txtTotalDueAmount.Location = new System.Drawing.Point(377, 200);
            this.txtTotalDueAmount.Margin = new System.Windows.Forms.Padding(2);
            this.txtTotalDueAmount.Name = "txtTotalDueAmount";
            this.txtTotalDueAmount.ReadOnly = true;
            this.txtTotalDueAmount.Size = new System.Drawing.Size(190, 27);
            this.txtTotalDueAmount.TabIndex = 12;
            this.txtTotalDueAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblCOMPANY_INFO
            // 
            this.lblCOMPANY_INFO.AutoSize = true;
            this.lblCOMPANY_INFO.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblCOMPANY_INFO.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblCOMPANY_INFO.Location = new System.Drawing.Point(11, 16);
            this.lblCOMPANY_INFO.Margin = new System.Windows.Forms.Padding(2);
            this.lblCOMPANY_INFO.Name = "lblCOMPANY_INFO";
            this.lblCOMPANY_INFO.Size = new System.Drawing.Size(94, 19);
            this.lblCOMPANY_INFO.TabIndex = 95;
            this.lblCOMPANY_INFO.Text = "ព័ត៌មានក្រុមហ៊ុន";
            // 
            // lblDUE_INVOICE_INFO
            // 
            this.lblDUE_INVOICE_INFO.AutoSize = true;
            this.lblDUE_INVOICE_INFO.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblDUE_INVOICE_INFO.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblDUE_INVOICE_INFO.Location = new System.Drawing.Point(11, 167);
            this.lblDUE_INVOICE_INFO.Margin = new System.Windows.Forms.Padding(2);
            this.lblDUE_INVOICE_INFO.Name = "lblDUE_INVOICE_INFO";
            this.lblDUE_INVOICE_INFO.Size = new System.Drawing.Size(170, 19);
            this.lblDUE_INVOICE_INFO.TabIndex = 96;
            this.lblDUE_INVOICE_INFO.Text = "ព័ត៌មានវិក្កយបត្រមិនទាន់ទូទាត់";
            // 
            // lblACCOUNT_NAME
            // 
            this.lblACCOUNT_NAME.AutoSize = true;
            this.lblACCOUNT_NAME.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblACCOUNT_NAME.Location = new System.Drawing.Point(292, 46);
            this.lblACCOUNT_NAME.Margin = new System.Windows.Forms.Padding(2);
            this.lblACCOUNT_NAME.Name = "lblACCOUNT_NAME";
            this.lblACCOUNT_NAME.Size = new System.Drawing.Size(73, 19);
            this.lblACCOUNT_NAME.TabIndex = 72;
            this.lblACCOUNT_NAME.Text = "ឈ្មោះគណនី";
            // 
            // txtAccountName
            // 
            this.txtAccountName.BackColor = System.Drawing.Color.White;
            this.txtAccountName.Enabled = false;
            this.txtAccountName.Location = new System.Drawing.Point(377, 42);
            this.txtAccountName.Margin = new System.Windows.Forms.Padding(2);
            this.txtAccountName.Name = "txtAccountName";
            this.txtAccountName.ReadOnly = true;
            this.txtAccountName.Size = new System.Drawing.Size(190, 27);
            this.txtAccountName.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Silver;
            this.panel2.Location = new System.Drawing.Point(1, 277);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(576, 1);
            this.panel2.TabIndex = 313;
            // 
            // btnCLOSE
            // 
            this.btnCLOSE.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnCLOSE.Location = new System.Drawing.Point(495, 281);
            this.btnCLOSE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.Size = new System.Drawing.Size(75, 23);
            this.btnCLOSE.TabIndex = 312;
            this.btnCLOSE.Text = "បិទ";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnCLOSE_Click);
            // 
            // btnGET_INFO
            // 
            this.btnGET_INFO.AutoSize = true;
            this.btnGET_INFO.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGET_INFO.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Underline);
            this.btnGET_INFO.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnGET_INFO.Location = new System.Drawing.Point(175, 241);
            this.btnGET_INFO.Margin = new System.Windows.Forms.Padding(2);
            this.btnGET_INFO.Name = "btnGET_INFO";
            this.btnGET_INFO.Size = new System.Drawing.Size(89, 19);
            this.btnGET_INFO.TabIndex = 92;
            this.btnGET_INFO.Text = "ទាញយកព័ត៌មាន";
            this.btnGET_INFO.Visible = false;
            this.btnGET_INFO.Click += new System.EventHandler(this.btnGET_INFO_Click);
            // 
            // lblPAY
            // 
            this.lblPAY.AutoSize = true;
            this.lblPAY.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblPAY.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Underline);
            this.lblPAY.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblPAY.Location = new System.Drawing.Point(90, 241);
            this.lblPAY.Margin = new System.Windows.Forms.Padding(2);
            this.lblPAY.Name = "lblPAY";
            this.lblPAY.Size = new System.Drawing.Size(81, 19);
            this.lblPAY.TabIndex = 92;
            this.lblPAY.Text = "ទូទាត់វិក្កយបត្រ";
            this.lblPAY.Click += new System.EventHandler(this.lblPAY_Click);
            // 
            // DialogPaymentOnline
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(576, 333);
            this.Name = "DialogPaymentOnline";
            this.Text = "ទូទាត់វិក្កយបត្រ ក្រុមហ៊ុន EPower";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtContact;
        private System.Windows.Forms.Label lblACCOUNT_NO;
        private System.Windows.Forms.TextBox txtAccountNo;
        private System.Windows.Forms.Label lblPHONE;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.Label lblCONTACT;
        private System.Windows.Forms.Label lblADDRESS;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Label lblTOTAL_DUE_AMOUNT1;
        private System.Windows.Forms.TextBox txtTotalDueAmount;
        private System.Windows.Forms.Label lblTOTAL_DUE_INVOICE;
        private System.Windows.Forms.TextBox txtTotalDueInvoice;
        private System.Windows.Forms.Label lblDUE_INVOICE_INFO;
        private System.Windows.Forms.Label lblCOMPANY_INFO;
        private System.Windows.Forms.Label lblACCOUNT_NAME;
        private System.Windows.Forms.TextBox txtAccountName;
        private System.Windows.Forms.Panel panel2;
        private SoftTech.Component.ExButton btnCLOSE;
        private System.Windows.Forms.Label btnGET_INFO;
        private System.Windows.Forms.Label lblPAY;
    }
}