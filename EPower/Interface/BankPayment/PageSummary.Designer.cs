﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.BankPayment
{
    partial class PageSummary
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageSummary));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cboReport = new System.Windows.Forms.ComboBox();
            this.btnPRINT_REPORT_AREA = new SoftTech.Component.ExButton();
            this.btnPRINT_REPORT_DATE = new SoftTech.Component.ExButton();
            this.btnSHOW_DETAIL = new SoftTech.Component.ExButton();
            this.cboBillingCycle = new System.Windows.Forms.ComboBox();
            this.cboArea = new System.Windows.Forms.ComboBox();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.cboBranch = new System.Windows.Forms.ComboBox();
            this.cboBank = new System.Windows.Forms.ComboBox();
            this.dtpD2 = new System.Windows.Forms.DateTimePicker();
            this.dtpD1 = new System.Windows.Forms.DateTimePicker();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.CURRENCY_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_RECORD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_PAY_FROM_BANK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_PAID_IN_SYSTEM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnViewReport = new SoftTech.Component.ExButton();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Controls.Add(this.btnViewReport);
            this.panel1.Controls.Add(this.cboReport);
            this.panel1.Controls.Add(this.btnPRINT_REPORT_AREA);
            this.panel1.Controls.Add(this.btnPRINT_REPORT_DATE);
            this.panel1.Controls.Add(this.btnSHOW_DETAIL);
            this.panel1.Controls.Add(this.cboBillingCycle);
            this.panel1.Controls.Add(this.cboArea);
            this.panel1.Controls.Add(this.cboCurrency);
            this.panel1.Controls.Add(this.cboBranch);
            this.panel1.Controls.Add(this.cboBank);
            this.panel1.Controls.Add(this.dtpD2);
            this.panel1.Controls.Add(this.dtpD1);
            this.panel1.Name = "panel1";
            // 
            // cboReport
            // 
            this.cboReport.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboReport.FormattingEnabled = true;
            resources.ApplyResources(this.cboReport, "cboReport");
            this.cboReport.Name = "cboReport";
            this.cboReport.SelectedIndexChanged += new System.EventHandler(this.cboReport_SelectedIndexChanged);
            // 
            // btnPRINT_REPORT_AREA
            // 
            resources.ApplyResources(this.btnPRINT_REPORT_AREA, "btnPRINT_REPORT_AREA");
            this.btnPRINT_REPORT_AREA.Name = "btnPRINT_REPORT_AREA";
            this.btnPRINT_REPORT_AREA.UseVisualStyleBackColor = true;
            this.btnPRINT_REPORT_AREA.Click += new System.EventHandler(this.btnReportSummaryByArea_Click);
            // 
            // btnPRINT_REPORT_DATE
            // 
            resources.ApplyResources(this.btnPRINT_REPORT_DATE, "btnPRINT_REPORT_DATE");
            this.btnPRINT_REPORT_DATE.Name = "btnPRINT_REPORT_DATE";
            this.btnPRINT_REPORT_DATE.UseVisualStyleBackColor = true;
            this.btnPRINT_REPORT_DATE.Click += new System.EventHandler(this.btnReport_Click);
            // 
            // btnSHOW_DETAIL
            // 
            resources.ApplyResources(this.btnSHOW_DETAIL, "btnSHOW_DETAIL");
            this.btnSHOW_DETAIL.Name = "btnSHOW_DETAIL";
            this.btnSHOW_DETAIL.UseVisualStyleBackColor = true;
            this.btnSHOW_DETAIL.Click += new System.EventHandler(this.btnDetail_Click);
            // 
            // cboBillingCycle
            // 
            this.cboBillingCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBillingCycle.FormattingEnabled = true;
            resources.ApplyResources(this.cboBillingCycle, "cboBillingCycle");
            this.cboBillingCycle.Name = "cboBillingCycle";
            this.cboBillingCycle.SelectedIndexChanged += new System.EventHandler(this.cboCurrency_SelectedIndexChanged);
            // 
            // cboArea
            // 
            this.cboArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboArea.FormattingEnabled = true;
            resources.ApplyResources(this.cboArea, "cboArea");
            this.cboArea.Name = "cboArea";
            this.cboArea.SelectedIndexChanged += new System.EventHandler(this.cboCurrency_SelectedIndexChanged);
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.SelectedIndexChanged += new System.EventHandler(this.cboCurrency_SelectedIndexChanged);
            // 
            // cboBranch
            // 
            this.cboBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBranch.FormattingEnabled = true;
            resources.ApplyResources(this.cboBranch, "cboBranch");
            this.cboBranch.Name = "cboBranch";
            this.cboBranch.SelectedIndexChanged += new System.EventHandler(this.cboBranch_SelectedIndexChanged);
            // 
            // cboBank
            // 
            this.cboBank.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBank.FormattingEnabled = true;
            resources.ApplyResources(this.cboBank, "cboBank");
            this.cboBank.Name = "cboBank";
            this.cboBank.SelectedIndexChanged += new System.EventHandler(this.cboBank_SelectedIndexChanged);
            // 
            // dtpD2
            // 
            resources.ApplyResources(this.dtpD2, "dtpD2");
            this.dtpD2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpD2.Name = "dtpD2";
            this.dtpD2.ValueChanged += new System.EventHandler(this.dtpD2_ValueChanged);
            // 
            // dtpD1
            // 
            resources.ApplyResources(this.dtpD1, "dtpD1");
            this.dtpD1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpD1.Name = "dtpD1";
            this.dtpD1.ValueChanged += new System.EventHandler(this.dtpD1_ValueChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "BANK_PAYMENT_ID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn1, "dataGridViewTextBoxColumn1");
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "CREATE_ON";
            dataGridViewCellStyle1.Format = "yyyy-MM-dd hh:mm tt";
            dataGridViewCellStyle1.NullValue = null;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle1;
            resources.ApplyResources(this.dataGridViewTextBoxColumn2, "dataGridViewTextBoxColumn2");
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "CREATE_BY";
            resources.ApplyResources(this.dataGridViewTextBoxColumn3, "dataGridViewTextBoxColumn3");
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "NAME_FILE";
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Blue;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.dataGridViewTextBoxColumn4, "dataGridViewTextBoxColumn4");
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "BANK_PAYMENT_TYPE_NAME";
            resources.ApplyResources(this.dataGridViewTextBoxColumn5, "dataGridViewTextBoxColumn5");
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "TOTAL_RECORD";
            resources.ApplyResources(this.dataGridViewTextBoxColumn6, "dataGridViewTextBoxColumn6");
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn7.DataPropertyName = "TOTAL_AMOUNT";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N2";
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.dataGridViewTextBoxColumn7, "dataGridViewTextBoxColumn7");
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "TOTAL_PAID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn8, "dataGridViewTextBoxColumn8");
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CURRENCY_ID,
            this.CURRENCY,
            this.DATE,
            this.TOTAL_RECORD,
            this.TOTAL_PAY_FROM_BANK,
            this.TOTAL_PAID_IN_SYSTEM});
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellContentClick);
            this.dgv.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellDoubleClick);
            // 
            // CURRENCY_ID
            // 
            this.CURRENCY_ID.DataPropertyName = "CURRENCY_ID";
            resources.ApplyResources(this.CURRENCY_ID, "CURRENCY_ID");
            this.CURRENCY_ID.Name = "CURRENCY_ID";
            this.CURRENCY_ID.ReadOnly = true;
            // 
            // CURRENCY
            // 
            this.CURRENCY.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.CURRENCY.DataPropertyName = "CURRENCY_NAME";
            resources.ApplyResources(this.CURRENCY, "CURRENCY");
            this.CURRENCY.Name = "CURRENCY";
            this.CURRENCY.ReadOnly = true;
            // 
            // DATE
            // 
            this.DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DATE.DataPropertyName = "DATE";
            dataGridViewCellStyle5.Format = "dd-MM-yyyy HH:mm:ss";
            this.DATE.DefaultCellStyle = dataGridViewCellStyle5;
            resources.ApplyResources(this.DATE, "DATE");
            this.DATE.Name = "DATE";
            this.DATE.ReadOnly = true;
            // 
            // TOTAL_RECORD
            // 
            this.TOTAL_RECORD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.TOTAL_RECORD.DataPropertyName = "TOTAL_RECORD";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle6.Format = "#";
            this.TOTAL_RECORD.DefaultCellStyle = dataGridViewCellStyle6;
            resources.ApplyResources(this.TOTAL_RECORD, "TOTAL_RECORD");
            this.TOTAL_RECORD.Name = "TOTAL_RECORD";
            this.TOTAL_RECORD.ReadOnly = true;
            // 
            // TOTAL_PAY_FROM_BANK
            // 
            this.TOTAL_PAY_FROM_BANK.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.TOTAL_PAY_FROM_BANK.DataPropertyName = "TOTAL_PAY";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "#,##0.####";
            this.TOTAL_PAY_FROM_BANK.DefaultCellStyle = dataGridViewCellStyle7;
            resources.ApplyResources(this.TOTAL_PAY_FROM_BANK, "TOTAL_PAY_FROM_BANK");
            this.TOTAL_PAY_FROM_BANK.Name = "TOTAL_PAY_FROM_BANK";
            this.TOTAL_PAY_FROM_BANK.ReadOnly = true;
            // 
            // TOTAL_PAID_IN_SYSTEM
            // 
            this.TOTAL_PAID_IN_SYSTEM.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.TOTAL_PAID_IN_SYSTEM.DataPropertyName = "TOTAL_PAID";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "#,##0.####";
            this.TOTAL_PAID_IN_SYSTEM.DefaultCellStyle = dataGridViewCellStyle8;
            resources.ApplyResources(this.TOTAL_PAID_IN_SYSTEM, "TOTAL_PAID_IN_SYSTEM");
            this.TOTAL_PAID_IN_SYSTEM.Name = "TOTAL_PAID_IN_SYSTEM";
            this.TOTAL_PAID_IN_SYSTEM.ReadOnly = true;
            // 
            // btnViewReport
            // 
            resources.ApplyResources(this.btnViewReport, "btnViewReport");
            this.btnViewReport.Name = "btnViewReport";
            this.btnViewReport.UseVisualStyleBackColor = true;
            this.btnViewReport.Click += new System.EventHandler(this.btnViewReport_Click);
            // 
            // PageSummary
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.panel1);
            this.Name = "PageSummary";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private ComboBox cboCurrency;
        private ComboBox cboBranch;
        private ComboBox cboBank;
        private DateTimePicker dtpD2;
        private DateTimePicker dtpD1;
        private DataGridView dgv;
        private ExButton btnPRINT_REPORT_DATE;
        private ExButton btnSHOW_DETAIL;
        private ExButton btnPRINT_REPORT_AREA;
        private ComboBox cboArea;
        private ComboBox cboBillingCycle;
        private DataGridViewTextBoxColumn CURRENCY_ID;
        private DataGridViewTextBoxColumn CURRENCY;
        private DataGridViewTextBoxColumn DATE;
        private DataGridViewTextBoxColumn TOTAL_RECORD;
        private DataGridViewTextBoxColumn TOTAL_PAY_FROM_BANK;
        private DataGridViewTextBoxColumn TOTAL_PAID_IN_SYSTEM;
        private ComboBox cboReport;
        private ExButton btnViewReport;
    }
}
