﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface.BankPayment
{
    public partial class PageSummary : Form
    {
        bool _loading = false; 
        public PageSummary()
        {
            InitializeComponent();
            try
            {
                UIHelper.DataGridViewProperties(dgv);

                this._loading = true;
                DateTime now = DBDataContext.Db.GetSystemDate();
                dtpD1.Value = new DateTime(now.Year, now.Month, 1);
                dtpD2.Value = dtpD1.Value.AddMonths(1).AddSeconds(-1);
                UIHelper.SetDataSourceToComboBox(this.cboBank, DBDataContext.Db.TBL_BANK_PAYMENT_DETAILs.Select(x => x.BANK_ALIAS).Distinct().Select(x => new { BANK = x }), Resources.ALL_BANK);
                UIHelper.SetDataSourceToComboBox(this.cboCurrency, Lookup.GetCurrencies(), Resources.ALL_CURRENCY);
                UIHelper.SetDataSourceToComboBox(this.cboArea, Lookup.GetAreas(), Resources.ALL_AREA);
                UIHelper.SetDataSourceToComboBox(this.cboBillingCycle, Lookup.GetBillingCycles(), Resources.ALL_CYCLE);
                this._loading = false;
                BindReport();
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void BindReport()
        {
            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("REPORT_ID", typeof(int));
            dtReport.Columns.Add("REPORT_NAME", typeof(string));
            dtReport.Rows.Add(0, Resources.SHOW_DETAIL);
            dtReport.Rows.Add(1, Resources.PRINT_REPORT_DATE);
            dtReport.Rows.Add(2, Resources.PRINT_REPORT_AREA);
            UIHelper.SetDataSourceToComboBox(cboReport, dtReport, "REPORT_ID", "REPORT_NAME");
            cboReport.SelectedIndex = 0;
        }

        public void LoadData()
        { 
            if (_loading) return; 
            var d1 = this.dtpD1.Value;
            var d2 = this.dtpD2.Value;
            var bank = this.cboBank.SelectedIndex <= 0 ? "" : this.cboBank.SelectedValue.ToString();
            var branch = this.cboBranch.SelectedIndex <= 0 ? "" : this.cboBranch.SelectedValue.ToString();
            var currencyId = (int)this.cboCurrency.SelectedValue;
            var areaId=(int)this.cboArea.SelectedValue;
            var billingCycleId=(int)this.cboBillingCycle.SelectedValue;


            var q = from p in DBDataContext.Db.TBL_BANK_PAYMENT_DETAILs
                    join c in DBDataContext.Db.TBL_CUSTOMERs on p.CUSTOMER_ID equals c.CUSTOMER_ID into JoinCust
                    from dt in JoinCust.DefaultIfEmpty()
                    where p.RESULT == (int)BankPaymentImportResult.RecordSuccess
                         && p.PAY_DATE >= d1 && p.PAY_DATE <= d2
                         && (bank == "" || p.BANK_ALIAS == bank)
                         && (branch == "" || p.BRANCH == branch)
                         && (currencyId == 0 || p.CURRENCY_ID == currencyId)
                         && (areaId==0 || dt.AREA_ID==areaId)
                         && (billingCycleId==0 ||dt.BILLING_CYCLE_ID==billingCycleId )
                         && !p.IS_VOID
                    group p by new { p.CURRENCY_ID, p.CURRENCY, DATE = p.PAY_DATE.Date } into g
                    orderby g.Key.CURRENCY_ID,g.Key.DATE descending
                    select new
                    {
                        CURRENCY_ID = g.Key.CURRENCY_ID,
                        CURRENCY_NAME = g.Key.CURRENCY,
                        DATE = g.Key.DATE,
                        TOTAL_RECORD = g.Count(),
                        TOTAL_PAY = g.Sum(x => x.PAY_AMOUNT),
                        TOTAL_PAID = g.Sum(x => x.PAID_AMOUNT)
                    };
            this.dgv.DataSource = q;
        }

        private void cboBank_SelectedIndexChanged(object sender, EventArgs e)
        {
            var bank = (this.cboBank.SelectedValue ?? "").ToString();
            var branches = DBDataContext.Db.TBL_BANK_PAYMENT_DETAILs
                .Where(x => x.BANK_ALIAS == bank)
                .Select(x => x.BRANCH).Distinct().Select(x => new { BRANCH = x });
            UIHelper.SetDataSourceToComboBox(this.cboBranch, branches, Resources.ALL_BRANCH);
            this.cboBranch.SelectedIndex = 0;
            LoadData(); 
        }

        private void cboBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        private void cboCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        private void dtpD2_ValueChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        private void dtpD1_ValueChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnDetail_Click(object sender, EventArgs e)
        {
            var d1 = this.dtpD1.Value;
            var d2 = this.dtpD2.Value;
            var bank = this.cboBank.SelectedIndex == 0 ? "" : this.cboBank.SelectedValue.ToString();
            var branch = this.cboBranch.SelectedIndex == 0 ? "" : this.cboBranch.SelectedValue.ToString();
            var currencyId = (int)this.cboCurrency.SelectedValue;
            var areaId = (int)cboArea.SelectedValue;
            var billingCycleId = (int)cboBillingCycle.SelectedValue;
            new DialogSummaryDetail(d1, d2, bank, branch, currencyId, areaId, billingCycleId).ShowDialog();
        }

        private void showDetailByRow(int index)
        {
            var row = this.dgv.Rows[index];
            var d1 = ((DateTime)row.Cells[this.DATE.Name].Value).Date;
            var d2 = ((DateTime)row.Cells[this.DATE.Name].Value).Date.AddDays(1).AddSeconds(-1);
            var bank = this.cboBank.SelectedIndex == 0 ? "" : this.cboBank.SelectedValue.ToString();
            var branch = this.cboBranch.SelectedIndex == 0 ? "" : this.cboBranch.SelectedValue.ToString();
            var currencyId = (int)this.cboCurrency.SelectedValue;
            var areaId = (int)cboArea.SelectedValue;
            var billingCycleId = (int)cboBillingCycle.SelectedValue;
            new DialogSummaryDetail(d1, d2, bank, branch, currencyId,areaId,billingCycleId).ShowDialog();
        }
         
        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                showDetailByRow(e.RowIndex);
            }
        }

        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        { 
            if (e.RowIndex != -1 && this.dgv.Columns[e.ColumnIndex].Name == this.TOTAL_RECORD.Name)
            {
                showDetailByRow(e.RowIndex);
            }
        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            Runner.Run(delegate
            {
                var cr = new CrystalReportHelper("ReportBankPaymentSummary.rpt");
                cr.SetParameter("@D1", dtpD1.Value);
                cr.SetParameter("@D2", dtpD2.Value);
                cr.SetParameter("@BANK", cboBank.SelectedIndex <= 0 ? "" : cboBank.Text);
                cr.SetParameter("@BRANCH", cboBranch.SelectedIndex <= 0 ? "" : cboBranch.Text);
                cr.SetParameter("@CURRENCY_ID", (int)cboCurrency.SelectedValue);
                cr.SetParameter("@CURRENCY_NAME", cboCurrency.Text);
                cr.SetParameter("@BANK_TEXT", cboBank.Text);
                cr.SetParameter("@BRANCH_TEXT", cboBranch.Text);
                cr.SetParameter("@AREA_ID", (int)cboArea.SelectedValue);
                cr.SetParameter("@AREA_TEXT", cboArea.Text);
                cr.SetParameter("@BILLING_CYCLE_ID", (int)cboBillingCycle.SelectedValue);
                cr.SetParameter("@BILLING_CYCLE_TEXT", cboBillingCycle.Text);
                cr.ViewReport(""); 
            },Resources.PROCESSING);
            
        }

        private void btnReportSummaryByArea_Click(object sender, EventArgs e)
        {
            Runner.Run(delegate
            {
                var cr = new CrystalReportHelper("ReportBankPaymentSummaryByArea.rpt");
                cr.SetParameter("@D1", dtpD1.Value);
                cr.SetParameter("@D2", dtpD2.Value);
                cr.SetParameter("@BANK", cboBank.SelectedIndex <= 0 ? "" : cboBank.Text);
                cr.SetParameter("@BRANCH", cboBranch.SelectedIndex <= 0 ? "" : cboBranch.Text);
                cr.SetParameter("@CURRENCY_ID", (int)cboCurrency.SelectedValue);
                cr.SetParameter("@CURRENCY_NAME", cboCurrency.Text);
                cr.SetParameter("@BANK_TEXT", cboBank.Text);
                cr.SetParameter("@BRANCH_TEXT", cboBranch.Text);
                cr.SetParameter("@AREA_ID", (int)cboArea.SelectedValue);
                cr.SetParameter("@AREA_TEXT", cboArea.Text);
                cr.SetParameter("@BILLING_CYCLE_ID", (int)cboBillingCycle.SelectedValue);
                cr.SetParameter("@BILLING_CYCLE_TEXT", cboBillingCycle.Text);
                cr.ViewReport(""); 
            },Resources.PROCESSING);
        }

        void ViewReport()
        {
            if(dgv.SelectedRows.Count==0)
            {
                return;
            }

            if(cboReport.SelectedIndex ==0)
            {
                var d1 = this.dtpD1.Value;
                var d2 = this.dtpD2.Value;
                var bank = this.cboBank.SelectedIndex == 0 ? "" : this.cboBank.SelectedValue.ToString();
                var branch = this.cboBranch.SelectedIndex == 0 ? "" : this.cboBranch.SelectedValue.ToString();
                var currencyId = (int)this.cboCurrency.SelectedValue;
                var areaId = (int)cboArea.SelectedValue;
                var billingCycleId = (int)cboBillingCycle.SelectedValue;
                new DialogSummaryDetail(d1, d2, bank, branch, currencyId, areaId, billingCycleId).ShowDialog();
            }

            else if (cboReport.SelectedIndex == 1)
            {
                Runner.Run(delegate
                {
                    var cr = new CrystalReportHelper("ReportBankPaymentSummary.rpt");
                    cr.SetParameter("@D1", dtpD1.Value);
                    cr.SetParameter("@D2", dtpD2.Value);
                    cr.SetParameter("@BANK", cboBank.SelectedIndex <= 0 ? "" : cboBank.Text);
                    cr.SetParameter("@BRANCH", cboBranch.SelectedIndex <= 0 ? "" : cboBranch.Text);
                    cr.SetParameter("@CURRENCY_ID", (int)cboCurrency.SelectedValue);
                    cr.SetParameter("@CURRENCY_NAME", cboCurrency.Text);
                    cr.SetParameter("@BANK_TEXT", cboBank.Text);
                    cr.SetParameter("@BRANCH_TEXT", cboBranch.Text);
                    cr.SetParameter("@AREA_ID", (int)cboArea.SelectedValue);
                    cr.SetParameter("@AREA_TEXT", cboArea.Text);
                    cr.SetParameter("@BILLING_CYCLE_ID", (int)cboBillingCycle.SelectedValue);
                    cr.SetParameter("@BILLING_CYCLE_TEXT", cboBillingCycle.Text);
                    cr.ViewReport(Resources.PRINT_REPORT_DATE);
                }, Resources.PROCESSING);
            }

            else if (cboReport.SelectedIndex == 2)
            {
                Runner.Run(delegate
                {
                    var cr = new CrystalReportHelper("ReportBankPaymentSummaryByArea.rpt");
                    cr.SetParameter("@D1", dtpD1.Value);
                    cr.SetParameter("@D2", dtpD2.Value);
                    cr.SetParameter("@BANK", cboBank.SelectedIndex <= 0 ? "" : cboBank.Text);
                    cr.SetParameter("@BRANCH", cboBranch.SelectedIndex <= 0 ? "" : cboBranch.Text);
                    cr.SetParameter("@CURRENCY_ID", (int)cboCurrency.SelectedValue);
                    cr.SetParameter("@CURRENCY_NAME", cboCurrency.Text);
                    cr.SetParameter("@BANK_TEXT", cboBank.Text);
                    cr.SetParameter("@BRANCH_TEXT", cboBranch.Text);
                    cr.SetParameter("@AREA_ID", (int)cboArea.SelectedValue);
                    cr.SetParameter("@AREA_TEXT", cboArea.Text);
                    cr.SetParameter("@BILLING_CYCLE_ID", (int)cboBillingCycle.SelectedValue);
                    cr.SetParameter("@BILLING_CYCLE_TEXT", cboBillingCycle.Text);
                    cr.ViewReport(Resources.PRINT_REPORT_AREA);
                }, Resources.PROCESSING);
            }
        }

        private void cboReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ViewReport();
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            try
            {
                ViewReport();
            }
            catch(Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_VIEW_REPORT, Base.Properties.Resources.WARNING);        
            }
        }
    }
}
