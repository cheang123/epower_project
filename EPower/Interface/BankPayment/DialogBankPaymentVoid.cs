﻿using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface.BankPayment
{
    public partial class DialogBankPaymentVoid : ExDialog
    {
        TBL_BANK_PAYMENT_DETAIL _objBankPaymentDetail = new TBL_BANK_PAYMENT_DETAIL();
        public DialogBankPaymentVoid(TBL_BANK_PAYMENT_DETAIL objBankPaymentDetail)
        {
            InitializeComponent();
            objBankPaymentDetail._CopyTo(_objBankPaymentDetail);
            read();
        }

        #region Method

        private void read()
        {
            txtBank.Text = _objBankPaymentDetail.BANK;
            txtBranch.Text = _objBankPaymentDetail.BRANCH;
            txtCustomerCode.Text = _objBankPaymentDetail.CUSTOMER_CODE;
            txtPayDate.Text = _objBankPaymentDetail.PAY_DATE.ToString("yyyy-MM-dd hh:mm tt");
            txtPayAmount.Text = UIHelper.FormatCurrency(_objBankPaymentDetail.PAY_AMOUNT, _objBankPaymentDetail.CURRENCY_ID) + " " + _objBankPaymentDetail.CURRENCY;
        }

        private bool invalid()
        {
            bool val = false;
            if (txtNote.Text.Trim().Length <= 0)
            {
                txtNote.SetValidation(string.Format(Resources.REQUIRED, lblNOTE.Text));
                val = true;
            }     
            return val;
        }

        #endregion

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (invalid()) return;
            using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
            {
                // Update Bank Payment Detail
                TBL_BANK_PAYMENT_DETAIL objBankPaymentDetail = DBDataContext.Db.TBL_BANK_PAYMENT_DETAILs.FirstOrDefault(x => x.BANK_PAYMENT_DETAIL_ID == _objBankPaymentDetail.BANK_PAYMENT_DETAIL_ID);
                objBankPaymentDetail.IS_VOID = true;

                // Update Bank Payment
                TBL_BANK_PAYMENT objBankPayment = DBDataContext.Db.TBL_BANK_PAYMENTs.FirstOrDefault(x => x.BANK_PAYMENT_ID == objBankPaymentDetail.BANK_PAYMENT_ID);
                objBankPayment.TOTAL_AMOUNT -= objBankPaymentDetail.PAY_AMOUNT;
                objBankPayment.TOTAL_PAID -= objBankPaymentDetail.PAY_AMOUNT;
                objBankPayment.TOTAL_RECORD -= 1;

                // Log Remove Bank Payment
                DBDataContext.Db.TBL_BANK_PAYMENT_VOIDs.InsertOnSubmit(new TBL_BANK_PAYMENT_VOID()
                {
                    AMOUNT = objBankPaymentDetail.PAY_AMOUNT,
                    BANK_PAYMENT_DETAIL_ID = objBankPaymentDetail.BANK_PAYMENT_DETAIL_ID,
                    CREATE_BY = SoftTech.Security.Logic.Login.CurrentLogin.LOGIN_NAME,
                    CREATE_ON = DBDataContext.Db.GetSystemDate(),
                    IS_ACTIVE = true,
                    NOTE = txtNote.Text,
                });

                DBDataContext.Db.SubmitChanges();
                tran.Complete();
            }
            this.DialogResult=DialogResult.OK;
        }

        private void btnCLOSE_Click(object sender, EventArgs e)
        {
            this.Close();
        }


    }
}
