﻿using EPower.Logic;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;

namespace EPower.Interface.BankPayment
{
    public partial class DialogB24LogDetailCustomer : ExDialog
    {
        int b24LogId = 0;
        public DialogB24LogDetailCustomer(int _b24LogId)
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            b24LogId = _b24LogId;
            load();
        }

        private void load()
        {
            var db = from b in DBDataContext.Db.TBL_B24_LOG_DETAILs
                     join c in DBDataContext.Db.TBL_CUSTOMERs on b.REF_ID equals c.CUSTOMER_ID
                     join a in DBDataContext.Db.TBL_AREAs on c.AREA_ID equals a.AREA_ID
                     join p in DBDataContext.Db.TBL_PRICEs on c.PRICE_ID equals p.PRICE_ID
                     join cu in DBDataContext.Db.TLKP_CURRENCies on p.CURRENCY_ID equals cu.CURRENCY_ID
                     where b.IS_ACTIVE && (c.LAST_NAME_KH + " " + c.FIRST_NAME_KH + " " + a.AREA_NAME + " " + p.PRICE_NAME).ToUpper().Contains(txtSearch.Text.ToUpper())
                        && b.REF_TYPE_ID == (int)B24_REF_TYPE.ACCOUNT
                        && b.B24_LOG_ID == b24LogId
                     select new
                     {
                         b.B24_LOG_DETAIL_ID,
                         c.CUSTOMER_ID,
                         c.CUSTOMER_CODE,
                         CUSTOMER_NAME = c.LAST_NAME_KH + " " + c.FIRST_NAME_KH,
                         a.AREA_NAME,
                         PRICE_NAME = p.PRICE_NAME + "(" + cu.CURRENCY_SING + ")"
                     };
            dgv.DataSource = db;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtSearch_QuickSearch(object sender, EventArgs e)
        {
            load();
        }
    }
}
