﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface.BankPayment
{
    public partial class DialogImport : ExDialog
    {
        DataTable dtResult = new DataTable();
        int BankPaymentId = 0; 
        string file_name = "";

        public DialogImport()
        {
            InitializeComponent();
            UIHelper.SetDataSourceToComboBox(cboImportType, DBDataContext.Db.TBL_BANK_PAYMENT_TYPEs.Where(x => x.IS_ACTIVE));
            if (cboImportType.Items.Count == 1)
            {
                cboImportType.SelectedIndex = 0;
            } 
        }


        private void btnImport_Click(object sender, EventArgs e)
        {
            try
            {
                if (Login.CurrentCashDrawer == null)
                {
                    if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER, string.Empty) == DialogResult.Yes)
                        new DialogOpenCashDrawer().ShowDialog();
                    if(Login.CurrentCashDrawer == null)
                        return;
                }
                 
                // Validation 
                if (cboImportType.SelectedIndex == -1)
                {
                    cboImportType.SetValidation(string.Format(Resources.REQUIRED, lblTYPE.Text));
                    return;
                }
                this.ClearAllValidation();

                string checkSum = Method.CheckSumFile(txtFileName.Text);
                TBL_BANK_PAYMENT objBankPayment = DBDataContext.Db.TBL_BANK_PAYMENTs.FirstOrDefault(x => x.CHECK_SUM_FILE == checkSum);
                if (objBankPayment != null)
                {
                    MsgBox.ShowInformation(Resources.MS_FILE_UPLOAD_ALREADY_EXISTS);
                    return;
                }
                // Backup DB
                Runner.RunNewThread(delegate()
                {
                    Runner.Instance.Text = Resources.MS_RUN_BILL_BACKUP;
                    SoftTech.Security.Logic.DBA.BackupToDisk(Settings.Default.PATH_BACKUP, String.Format("{0} {1:yyyyMMddHHmmss} Before bank payment.bak", DBDataContext.Db.Connection.Database, DBDataContext.Db.GetSystemDate()));
                });
                if (Runner.Instance.Error != null) return;

                // Import Data
                if ((int)cboImportType.SelectedValue == (int)BankPaymentTypes.EXCEL)
                {
                    EXCEL();
                }
                else if ((int)cboImportType.SelectedValue == (int)BankPaymentTypes.ACLEDA)
                {
                    //CSV();
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void EXCEL()
        {
            // false here and true if thread is success
            bool success = false;

            string fileName = this.txtFileName.Text;

            // this is to lock user for 
            FileInfo fi = new FileInfo(fileName);
            var interval = fi.LastWriteTimeUtc - fi.CreationTimeUtc;
            if (interval.TotalDays > 5)
            { 
                if (Method.Utilities[Utility.BANK_PAYMENT_CODE] == "000")
                {
                    MsgBox.ShowError("External table is not in the expected format!", "ERROR");
                    return;
                }
            }


            ExcelOledb xls = new ExcelOledb(fileName,false);

            this.dtResult = xls.ExecuteDataTable(@"SELECT   INVOICE_ID,
                                                            CUSTOMER_CODE,
                                                            CUSTOMER_NAME,
                                                            DUE_AMOUNT,
                                                            PAY_AMOUNT,
                                                            PAY_DATE,
                                                            RECEIVE_BY,
                                                            CURRENCY_ID,
                                                            CURRENCY_NAME,
                                                            -1 AS RESULT,
                                                            ''  AS ERROR,
                                                            0.0 AS PAY_AMOUNT2
                                                         FROM TBL_PAYMENT 
                                                         WHERE (PAY_AMOUNT IS NOT NULL OR PAY_DATE IS NOT NULL)
                                                                AND INVOICE_ID <> 0 ;"); // INVOICE_ID=0 is dummy record for datatype.
            xls.Dispose();



            Runner.Run(delegate()
            {
                int i = 0;
                // start validating data
                foreach (DataRow row in this.dtResult.Rows)
                {
                    Application.DoEvents(); 
                    // update status
                    i++;
                    Runner.Instance.Text = "Validating..." + string.Format("{0}/{1} ({2}%)", i, dtResult.Rows.Count, i * 100 / dtResult.Rows.Count);

                    // begin validating
                    if (row["PAY_DATE"] == DBNull.Value)
                    {
                        row["RESULT"] = (int)BankPaymentImportResult.RecordError;
                        row["ERROR"] = "NULL (PAY DATE)";
                        continue;
                    }
                    if (row["PAY_AMOUNT"] == DBNull.Value)
                    {
                        row["RESULT"] = (int)BankPaymentImportResult.RecordError;
                        row["ERROR"] = "NULL (PAY AMOUNT)";
                        continue;
                    }
                    if (!DataHelper.IsInteger(row["INVOICE_ID"].ToString()))
                    {
                        row["RESULT"] = (int)BankPaymentImportResult.RecordError;
                        row["ERROR"] = "INVALID INVOICE";
                        continue;
                    } 
                    if (!DataHelper.IsNumber(row["CURRENCY_ID"].ToString()))
                    {
                        row["RESULT"] = (int)BankPaymentImportResult.RecordError;
                        row["ERROR"] = "INVALID CURRENCY";
                        continue;
                    }
                    if (!DataHelper.IsNumber(row["DUE_AMOUNT"].ToString()))
                    {
                        row["RESULT"] = (int)BankPaymentImportResult.RecordError;
                        row["ERROR"] = "INVALID DUE_AMOUNT";
                        continue;
                    }
                    if (!DataHelper.IsNumber(row["PAY_AMOUNT"].ToString()))
                    {
                        row["RESULT"] = (int)BankPaymentImportResult.RecordError;
                        row["ERROR"] = "INVALID PAY AMOUNT";
                        continue;
                    } 
                    // exel numeric datetime
                    if (DataHelper.IsNumber(row["PAY_DATE"].ToString()))
                    {
                        row["PAY_DATE"] = new DateTime(1900, 1, 1).AddDays((int)DataHelper.ParseToDouble(row["PAY_DATE"].ToString())).ToString("yyyy-MM-dd");
                        // to make more CUSTOMER i
                    }
                    // test try to parse datetime.
                    try
                    {
                        DateTime.Parse(row["PAY_DATE"].ToString());
                    }
                    catch (Exception)
                    {
                        row["RESULT"] = (int)BankPaymentImportResult.RecordError;
                        row["ERROR"] = "INVALID PAY DATE";
                        continue;
                    }

                    row["PAY_AMOUNT2"] = DataHelper.ParseToDouble(row["PAY_AMOUNT"].ToString());

                    var INVOICE_ID = DataHelper.ParseToInt(row["INVOICE_ID"].ToString());
                    TBL_INVOICE objInvoice = DBDataContext.Db.TBL_INVOICEs.FirstOrDefault(x => x.INVOICE_ID == INVOICE_ID);
                    // check invoice exist?
                    if (objInvoice == null)
                    {
                        row["RESULT"] = (int)BankPaymentImportResult.RecordNotFound;
                        continue;
                    }

                    var DUE_AMOUNT = DataHelper.ParseToDecimal(row["DUE_AMOUNT"].ToString());

                    // check for due amount.
                    if (objInvoice.SETTLE_AMOUNT - objInvoice.PAID_AMOUNT != DUE_AMOUNT)
                    {
                        row["RESULT"] = (int)BankPaymentImportResult.RecordPaid;
                        continue;
                    }

                   
                    // mark result ok
                    row["RESULT"] = (int)BankPaymentImportResult.RecordSuccess; 
                }

                // process payment.
                DateTime now = DBDataContext.Db.GetSystemDate();
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    // INSERT TBL_BANK_PAYMENT 
                    TBL_BANK_PAYMENT objBankPayment = new TBL_BANK_PAYMENT()
                    {
                        CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                        CREATE_ON = now,
                        TOTAL_RECORD = dtResult.Rows.Count,
                        TOTAL_AMOUNT = DataHelper.ParseToDecimal(this.dtResult.Compute("SUM(PAY_AMOUNT2)", string.Format("RESULT={0}", (int)BankPaymentImportResult.RecordSuccess)).ToString()),
                        TOTAL_PAID = DataHelper.ParseToDecimal(this.dtResult.Compute("SUM(PAY_AMOUNT2)", string.Format("RESULT={0}", (int)BankPaymentImportResult.RecordSuccess)).ToString()),
                        BANK_PAYMENT_TYPE_ID = (int)BankPaymentTypes.EXCEL,
                        CHECK_SUM_FILE = Method.CheckSumFile(txtFileName.Text),
                        NAME_FILE = file_name,
                        DATA_FILE = File.ReadAllBytes(txtFileName.Text)
                    };
                    DBDataContext.Db.TBL_BANK_PAYMENTs.InsertOnSubmit(objBankPayment);
                    DBDataContext.Db.SubmitChanges(); 

                    dtResult.DefaultView.RowFilter = string.Format("RESULT={0}", (int)BankPaymentImportResult.RecordSuccess);
                    int n = dtResult.DefaultView.Count;
                    i = 0;
                    foreach (DataRowView row in dtResult.DefaultView)
                    {
                        Application.DoEvents();
                        i++;
                        Runner.Instance.Text = "Processing..." + string.Format("{0}/{1} ({2}%)", i, dtResult.Rows.Count, i * 100 / dtResult.Rows.Count);
                        TBL_CUSTOMER objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_CODE == row["CUSTOMER_CODE"].ToString());
                        var tmp = new
                        {
                            CUSTOMER_CODE = row["CUSTOMER_CODE"].ToString(),
                            CUSTOMER_ID = objCustomer.CUSTOMER_ID,
                            INVOICE_ID = DataHelper.ParseToInt(row["INVOICE_ID"].ToString()),
                            DUE_AMOUNT = DataHelper.ParseToDecimal(row["DUE_AMOUNT"].ToString()),
                            PAY_AMOUNT = DataHelper.ParseToDecimal(row["PAY_AMOUNT"].ToString()),
                            RECEIVE_BY = row["RECEIVE_BY"].ToString(),

                            // in excel has many of one customer with multiple invoices
                            // to force same customer and same payment date to import to system 
                            // we add dummy date as invoice date. 
                            PAY_DATE = DateTime.Parse(row["PAY_DATE"].ToString()).AddMilliseconds(DataHelper.ParseToInt(row["INVOICE_ID"].ToString()) % 1000),
                            CURRENCY_ID =DataHelper.ParseToInt(row["CURRENCY_ID"].ToString()),
                            CURRENCY_NAME = row["CURRENCY_NAME"].ToString()
                        };

  
                        TBL_BANK_PAYMENT_DETAIL objBankPaymentDetail = new TBL_BANK_PAYMENT_DETAIL()
                        {
                            BANK_PAYMENT_ID = objBankPayment.BANK_PAYMENT_ID,
                            CUSTOMER_ID = tmp.CUSTOMER_ID,
                            CUSTOMER_CODE = tmp.CUSTOMER_CODE,
                            PAY_AMOUNT = tmp.PAY_AMOUNT,
                            PAID_AMOUNT = 0,
                            PAY_DATE = tmp.PAY_DATE,
                            UPDATE_DATE = now,
                            CASHIER = tmp.RECEIVE_BY,
                            RESULT = (int)BankPaymentImportResult.RecordSuccess,

                            BANK = "NA",
                            BRANCH = "NA",
                            CURRENCY = tmp.CURRENCY_NAME,
                            CURRENCY_ID = tmp.CURRENCY_ID,
                            PAYMENT_METHOD = "NA",
                            PAYMENT_TYPE = "NA"
                        };
                        DBDataContext.Db.TBL_BANK_PAYMENT_DETAILs.InsertOnSubmit(objBankPaymentDetail);
                        DBDataContext.Db.SubmitChanges();  
                    }
                    Runner.Instance.Text = "Applying payment...";
                    var paymentOperation = new PaymentOperation();
                    paymentOperation.UpdateLabel = (string val) => { Application.DoEvents(); Runner.Instance.Text = val; };
                    paymentOperation.ShowMessage = (string val) => MsgBox.ShowInformation(val);

                    paymentOperation.ProcessPayment(objBankPayment);
                    //DBDataContext.Db.ExecuteCommand("EXEC RUN_BANK_PAYMENT @p0;", objBankPayment.BANK_PAYMENT_ID);

                    tran.Complete();
                }
                success = true;
            });

            //this.txtErrors.Text = this.dtResult.Compute("COUNT(INVOICE_ID)", "RESULT=" + ((int)BankPaymentImportResult.RecordError).ToString()).ToString();
            //this.txtSuccess.Text = this.dtResult.Compute("COUNT(INVOICE_ID)", "RESULT=" + ((int)BankPaymentImportResult.RecordSuccess).ToString()).ToString();
            //this.txtPaid.Text = this.dtResult.Compute("COUNT(INVOICE_ID)", "RESULT=" + ((int)BankPaymentImportResult.RecordPaid).ToString()).ToString();
            //this.txtNotFound.Text = this.dtResult.Compute("COUNT(INVOICE_ID)", "RESULT=" + ((int)BankPaymentImportResult.RecordNotFound).ToString()).ToString();

            //this.txtErrorAmount.Text = DataHelper.ParseToDecimal(this.dtResult.Compute("SUM(PAY_AMOUNT2)", "RESULT=" + ((int)BankPaymentImportResult.RecordError).ToString()).ToString()).ToString("N2");
            //this.txtSuccessAmount.Text = DataHelper.ParseToDecimal(this.dtResult.Compute("SUM(PAY_AMOUNT2)", "RESULT=" + ((int)BankPaymentImportResult.RecordSuccess).ToString()).ToString()).ToString("N2");
            //this.txtPaidAmount.Text = DataHelper.ParseToDecimal(this.dtResult.Compute("SUM(PAY_AMOUNT2)", "RESULT=" + ((int)BankPaymentImportResult.RecordPaid).ToString()).ToString()).ToString("N2");
            //this.txtNotFoundAmount.Text = DataHelper.ParseToDecimal(this.dtResult.Compute("SUM(PAY_AMOUNT2)", "RESULT=" + ((int)BankPaymentImportResult.RecordNotFound).ToString()).ToString()).ToString("N2");
           
            var error = Runner.Instance.Error;
            if (error == null)
            {
                var id = DBDataContext.Db.TBL_BANK_PAYMENTs.Select(x => x.BANK_PAYMENT_ID).OrderByDescending(x => x).FirstOrDefault();
                var summary = (from d in DBDataContext.Db.TBL_BANK_PAYMENT_DETAILs
                               where d.IS_VOID == false && d.BANK_PAYMENT_ID == id
                               group d by d.RESULT into g
                               select new ImportResult
                               {
                                   RESULT = g.Key,
                                   COUNT = g.Count(),
                                   SUM = g.Sum(x => x.PAY_AMOUNT)
                               }).ToList().Union(new ImportResult[] { new ImportResult() { RESULT = 0, COUNT = 0, SUM = 0 } });

                BankPaymentId = id;

                this.txtSuccess_.Text = summary.FirstOrDefault(x => x.RESULT == (int)BankPaymentImportResult.RecordSuccess || x.RESULT == 0).COUNT.ToString();
                this.txtErrors_.Text = summary.FirstOrDefault(x => x.RESULT == (int)BankPaymentImportResult.RecordError || x.RESULT == 0).COUNT.ToString();
                this.txtPaid_.Text = summary.FirstOrDefault(x => x.RESULT == (int)BankPaymentImportResult.RecordPaid || x.RESULT == 0).COUNT.ToString();
                this.txtNotFound_.Text = summary.FirstOrDefault(x => x.RESULT == (int)BankPaymentImportResult.RecordNotFound || x.RESULT == 0).COUNT.ToString();

                this.txtSuccessAmount_.Text = summary.FirstOrDefault(x => x.RESULT == (int)BankPaymentImportResult.RecordSuccess || x.RESULT == 0).SUM.ToString("N2");
                this.txtErrorAmount_.Text = summary.FirstOrDefault(x => x.RESULT == (int)BankPaymentImportResult.RecordError || x.RESULT == 0).SUM.ToString("N2");
                this.txtPaidAmount_.Text = summary.FirstOrDefault(x => x.RESULT == (int)BankPaymentImportResult.RecordPaid || x.RESULT == 0).SUM.ToString("N2");
                this.txtNotFoundAmount_.Text = summary.FirstOrDefault(x => x.RESULT == (int)BankPaymentImportResult.RecordNotFound || x.RESULT == 0).SUM.ToString("N2");
            } 
            if (success)
            {
                MsgBox.ShowInformation(Resources.SUCCESS);
            }
        }

        //private void CSV()
        //{
        //    var csv = File.ReadAllText(this.txtFileName.Text);
        //    var operation = new PaymentOperation();
        //    operation.UpdateLabel = (string val) =>
        //    {
        //        Application.DoEvents();
        //        Runner.Instance.Text = val;
        //    };
        //    var lastMessage = "";
        //    operation.ShowMessage = (string val) =>
        //    {
        //        //MsgBox.ShowInformation(val);
        //        lastMessage = val;
        //    };
        //    Runner.RunNewThread(delegate()
        //    { 
        //        operation.ProcessPaymentFromCSV(csv);
        //    });

        //    if (lastMessage != "")
        //    {
        //        MsgBox.ShowInformation(lastMessage);
        //    }

        //    var error = Runner.Instance.Error;
        //    if (error == null)
        //    {
        //        var id = DBDataContext.Db.TBL_BANK_PAYMENTs.Select(x => x.BANK_PAYMENT_ID).OrderByDescending(x => x).FirstOrDefault();
        //        var summary =   (from d in DBDataContext.Db.TBL_BANK_PAYMENT_DETAILs
        //                        where d.IS_VOID==false && d.BANK_PAYMENT_ID==id 
        //                        group d by d.RESULT into g
        //                         select new ImportResult
        //                         {
        //                            RESULT = g.Key,
        //                            COUNT = g.Count(),
        //                            SUM = g.Sum(x=>x.PAY_AMOUNT)
        //                        }).ToList().Union(new ImportResult[]{new ImportResult(){ RESULT=0,COUNT=0,SUM=0}});
                
        //        BankPaymentId = id;

        //        this.txtSuccess_.Text = summary.FirstOrDefault(x =>  x.RESULT == (int)BankPaymentImportResult.RecordSuccess || x.RESULT==0).COUNT.ToString();
        //        this.txtErrors_.Text = summary.FirstOrDefault(x => x.RESULT == (int)BankPaymentImportResult.RecordError || x.RESULT == 0).COUNT.ToString();
        //        this.txtPaid_.Text = summary.FirstOrDefault(x => x.RESULT == (int)BankPaymentImportResult.RecordPaid || x.RESULT == 0).COUNT.ToString();
        //        this.txtNotFound_.Text = summary.FirstOrDefault(x => x.RESULT == (int)BankPaymentImportResult.RecordNotFound || x.RESULT == 0).COUNT.ToString();

        //        this.txtSuccessAmount_.Text = summary.FirstOrDefault(x => x.RESULT == (int)BankPaymentImportResult.RecordSuccess || x.RESULT == 0).SUM.ToString("N2");
        //        this.txtErrorAmount_.Text = summary.FirstOrDefault(x => x.RESULT == (int)BankPaymentImportResult.RecordError || x.RESULT == 0).SUM.ToString("N2");
        //        this.txtPaidAmount_.Text = summary.FirstOrDefault(x => x.RESULT == (int)BankPaymentImportResult.RecordPaid || x.RESULT == 0).SUM.ToString("N2");
        //        this.txtNotFoundAmount_.Text = summary.FirstOrDefault(x => x.RESULT == (int)BankPaymentImportResult.RecordNotFound || x.RESULT == 0).SUM.ToString("N2");
        //    } 
        //}

        class ImportResult
        {
            public int RESULT { get; set; }
            public int COUNT { get; set; }
            public decimal  SUM { get; set; }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            if (cboImportType.SelectedIndex == -1)
            {
                cboImportType.SetValidation(string.Format(Resources.REQUIRED, lblTYPE.Text));
                return;
            }
            cboImportType.ClearValidation();
            OpenFileDialog diag = new OpenFileDialog();
            diag.Filter = cboImportType.Text == BankPaymentTypes.EXCEL.ToString() ? "Excel Worksheets|*.xls" : "Plain text files (*.csv;*.txt)|*.csv;*.txt";
            if (diag.ShowDialog() == DialogResult.OK)
            {
                this.txtFileName.Text = diag.FileName;
                file_name = diag.SafeFileName;
            }
        }
       
        private void btnShowDetail_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //if ((int)cboImportType.SelectedValue == (int)BankPaymentTypes.EXCEL)
            //{
            //    if (dtResult != null)
            //    {
            //        DialogImportResult diag = new DialogImportResult(this.dtResult);
            //        diag.ShowDialog();
            //    }
            //}
            //else
            //{
                new DialogHistoryDetail(BankPaymentId).ShowDialog();
            //}
        }

        private void cboImportType_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtFileName.Clear();
        }
    }
}
