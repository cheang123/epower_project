﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.BankPayment
{
    partial class DialogB24LogDetailCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogB24LogDetailCustomer));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.txtSearch = new SoftTech.Component.ExTextbox();
            this.B24_LOG_DETAIL_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AREA_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRICE_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.txtSearch);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.dgv);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.dgv, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.txtSearch, 0);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.B24_LOG_DETAIL_ID,
            this.CUSTOMER_ID,
            this.CUSTOMER_CODE,
            this.CUSTOMER_NAME,
            this.AREA_NAME,
            this.PRICE_NAME});
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.BackColor = System.Drawing.Color.White;
            this.txtSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtSearch, "txtSearch");
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtSearch.QuickSearch += new System.EventHandler(this.txtSearch_QuickSearch);
            // 
            // B24_LOG_DETAIL_ID
            // 
            this.B24_LOG_DETAIL_ID.DataPropertyName = "B24_LOG_DETAIL_ID";
            resources.ApplyResources(this.B24_LOG_DETAIL_ID, "B24_LOG_DETAIL_ID");
            this.B24_LOG_DETAIL_ID.Name = "B24_LOG_DETAIL_ID";
            this.B24_LOG_DETAIL_ID.ReadOnly = true;
            // 
            // CUSTOMER_ID
            // 
            this.CUSTOMER_ID.DataPropertyName = "CUSTOMER_ID";
            resources.ApplyResources(this.CUSTOMER_ID, "CUSTOMER_ID");
            this.CUSTOMER_ID.Name = "CUSTOMER_ID";
            this.CUSTOMER_ID.ReadOnly = true;
            // 
            // CUSTOMER_CODE
            // 
            this.CUSTOMER_CODE.DataPropertyName = "CUSTOMER_CODE";
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Blue;
            this.CUSTOMER_CODE.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.CUSTOMER_CODE, "CUSTOMER_CODE");
            this.CUSTOMER_CODE.Name = "CUSTOMER_CODE";
            this.CUSTOMER_CODE.ReadOnly = true;
            // 
            // CUSTOMER_NAME
            // 
            this.CUSTOMER_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CUSTOMER_NAME.DataPropertyName = "CUSTOMER_NAME";
            dataGridViewCellStyle3.NullValue = null;
            this.CUSTOMER_NAME.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.CUSTOMER_NAME, "CUSTOMER_NAME");
            this.CUSTOMER_NAME.Name = "CUSTOMER_NAME";
            this.CUSTOMER_NAME.ReadOnly = true;
            // 
            // AREA_NAME
            // 
            this.AREA_NAME.DataPropertyName = "AREA_NAME";
            dataGridViewCellStyle4.NullValue = null;
            this.AREA_NAME.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.AREA_NAME, "AREA_NAME");
            this.AREA_NAME.Name = "AREA_NAME";
            this.AREA_NAME.ReadOnly = true;
            // 
            // PRICE_NAME
            // 
            this.PRICE_NAME.DataPropertyName = "PRICE_NAME";
            resources.ApplyResources(this.PRICE_NAME, "PRICE_NAME");
            this.PRICE_NAME.Name = "PRICE_NAME";
            this.PRICE_NAME.ReadOnly = true;
            // 
            // DialogB24LogDetailCustomer
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogB24LogDetailCustomer";
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DataGridView dgv;
        private ExButton btnCLOSE;
        private ExTextbox txtSearch;
        private DataGridViewTextBoxColumn B24_LOG_DETAIL_ID;
        private DataGridViewTextBoxColumn CUSTOMER_ID;
        private DataGridViewTextBoxColumn CUSTOMER_CODE;
        private DataGridViewTextBoxColumn CUSTOMER_NAME;
        private DataGridViewTextBoxColumn AREA_NAME;
        private DataGridViewTextBoxColumn PRICE_NAME;
    }
}