﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface.BankPayment
{
    public partial class DialogHistoryDelete : ExDialog
    {
        public DialogHistoryDelete(DateTime _d1, DateTime _d2)
        {
            InitializeComponent();
            dtpD1.Value = _d1;
            dtpD2.Value = _d2;
            load();
        }

        #region Method

        private void load()
        {
            var db = from v in DBDataContext.Db.TBL_BANK_PAYMENT_VOIDs
                     join b in DBDataContext.Db.TBL_BANK_PAYMENT_DETAILs on v.BANK_PAYMENT_DETAIL_ID equals b.BANK_PAYMENT_DETAIL_ID
                     join c in DBDataContext.Db.TBL_CUSTOMERs on b.CUSTOMER_ID equals c.CUSTOMER_ID
                     join cu in DBDataContext.Db.TLKP_CURRENCies on b.CURRENCY_ID equals cu.CURRENCY_ID
                     where v.IS_ACTIVE
                            && v.CREATE_ON.Date >= dtpD1.Value && v.CREATE_ON <= dtpD2.Value
                            && (c.CUSTOMER_CODE + " " + c.LAST_NAME_KH + " " + c.FIRST_NAME_KH).ToUpper().Contains(txtSearch.Text.ToUpper())
                     select new
                     {
                         c.CUSTOMER_ID,
                         c.CUSTOMER_CODE,
                         CUSTOMER_NAME = c.LAST_NAME_KH + " " + c.FIRST_NAME_KH,
                         v.CREATE_ON,
                         v.CREATE_BY,
                         cu.CURRENCY_SING,
                         AMOUNT = UIHelper.FormatCurrency(v.AMOUNT, b.CURRENCY_ID),
                         v.NOTE
                     };
            dgv.DataSource = db;
        }


        #endregion

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtSearch_QuickSearch(object sender, EventArgs e)
        {
            load();
        }

        private void dtpD1_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        } 
    }
}
