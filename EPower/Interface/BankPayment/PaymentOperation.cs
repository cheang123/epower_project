﻿using EPower.EPowerServiceProxy;
using EPower.Base.Logic;
using EPower.Logic;
using EPower.Base.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading;
using System.Transactions;
using System.Windows.Forms;
using System.Xml;
using TBL_INVOICE = SoftTech.TBL_INVOICE;

//using EPower.Security;

namespace EPower.Interface.BankPayment
{
    internal class PaymentOperation
    {
        public delegate void Func(string value);
        public Func UpdateLabel;
        public Func ShowMessage;

        EPowerServiceClient client
        {
            get
            {
                return getClient();
            }
        }
        //string accessKey = null;
        //string cryptoKey = null;
        DBDataContext db = DBDataContext.Db; //new DBDataContext(Method.GetConnectionString());
        public string LoginName = "";
        public int UserCashDrawerId = 0;
        //private void InitializeService()
        //{
        //    UpdateLabel("Initializing...");
        //    this.cryptoKey = Method.Utilities[Utility.BANK_PAYMENT_CRYPTO_KEY];
        //    this.accessKey = Method.Utilities[Utility.BANK_PAYMENT_ACCESS_KEY];

        //    client = getClient();
        //    UpdateLabel("Connecting to server...");
        //    client.TestConnection();
        //}

        public void UpdateCustomerData(bool forceAllCustomer)
        {
            try
            {
                var lastDate = new DateTime(2000, 1, 1);
                // if upload all customer, don't care about last uncomlete task.
                if (!forceAllCustomer)
                {
                    var objLastLog = this.db.TBL_BANK_PAYMENT_LOGs
                                                .OrderByDescending(x => x.LOG_DATE)
                                                .FirstOrDefault(x => x.ACTION_TYPE == (int)BankPaymentAction.EPW_UPDATE_CUSTOMER);
                    if (objLastLog != null)
                    {
                        // last operation is not complete then force it's again.
                        if (objLastLog.IS_COMPLETED == false)
                        {
                            var lastLogCustomers = this.db.TBL_BANK_PAYMENT_LOG_CUSTOMERs.Where(x => x.LOG_ID == objLastLog.LOG_ID && x.IS_COMPLETED == false).ToArray();
                            this.updateCustomerDataToServer(objLastLog, lastLogCustomers);
                        }
                        lastDate = objLastLog.LOG_DATE;
                    }
                }

                var logId = Guid.NewGuid();
                db.CommandTimeout = 10 * 60 * 1000;
                //var rows = this.db.GET_PENDING_UPDATE_CUSTOMER(logId,lastDate, DateTime.Now).ToList();
                var rows = this.db.ExecuteQuery<TBL_BANK_PAYMENT_LOG_CUSTOMER>("EXEC GET_PENDING_UPDATE_CUSTOMER @p0,@p1,@p2,@p3", logId, lastDate, DateTime.Now, forceAllCustomer).ToList();

                if (rows.Count() == 0)
                {
                    ShowMessage(Resources.MS_NO_CUSTOMER_TO_UPDATE);
                    return;
                }

                var objLog = new TBL_BANK_PAYMENT_LOG();
                objLog.LOG_ID = logId;
                objLog.ACTION_TYPE = (int)BankPaymentAction.EPW_UPDATE_CUSTOMER;
                objLog.USER = Login.CurrentLogin.LOGIN_NAME;
                objLog.IS_COMPLETED = false;
                objLog.LOG_DATE = DateTime.Now;
                objLog.LOG_START_DATE = DateTime.Now;
                objLog.LOG_END_DATE = DateTime.Now;
                this.db.TBL_BANK_PAYMENT_LOGs.InsertOnSubmit(objLog);
                this.db.SubmitChanges();

                // customer log detail already added while call procedure so we don't need any insert
                var logCustomers = rows;
                // no need transaction because it's take too long time to get response from server
                updateCustomerDataToServer(objLog, logCustomers.ToArray());

                // Send Data to B24 if ENABLE
                if (DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_B24]))
                {
                    EPower.Logic.B24_Integration push = new EPower.Logic.B24_Integration();
                    push.Push(forceAllCustomer);
                }

                ShowMessage(Resources.SUCCESS);

            }
            catch (Exception ex)
            {
                MsgBox.LogError(ex);
                throw new Exception(string.Format(Resources.MSG_ERROR, Resources.SEND_DATA));
            }

        }

        private void updateCustomerDataToServer(TBL_BANK_PAYMENT_LOG objLog, TBL_BANK_PAYMENT_LOG_CUSTOMER[] logCustomers)
        {
            UpdateLabel("Initializing...");
            var key = Method.Utilities[Utility.BANK_PAYMENT_CRYPTO_KEY];
            //var client = getClient();

            UpdateLabel("Connecting to server...");
            client.TestConnection();
            string areaCode = Method.Utilities[Utility.BANK_PAYMENT_CODE];
            var customers = logCustomers.Select(x => new TBL_LOG_CUSTOMER()
            {
                CUSTOMER_ID = x.CUSTOMER_ID,
                CUSTOMER_CODE = x.CUSTOMER_CODE,
                CUSTOMER_NAME = x.CUSTOMER_NAME,
                METER_CODE = x.METER_CODE,
                STATUS_ID = x.STATUS_ID,
                CURRENCY = x.CURRENCY,
                TOTAL_RECORD = x.TOTAL_RECORD,
                TOTAL_AMOUNT = x.TOTAL_AMOUNT,
                NEW_CODE = areaCode + "-" + x.CUSTOMER_CODE,
                DUE_DATE = x.DUE_DATE,
                PHONE_NO = x.PHONE_NO,
                EXT_DATA = x.EXT_DATA

            }).ToList();

            var chunksize = 500;
            var process = 0;
            var tmpLst = new List<TBL_LOG_CUSTOMER>();
            foreach (var c in customers)
            {
                process++;
                UpdateLabel(string.Format("Processing... {0}/{1}", process, customers.Count));

                tmpLst.Add(c);
                if (process % chunksize == 0 || process >= customers.Count)
                {
                    UpdateLabel(string.Format("Uploading... {0}/{1}", process, customers.Count));

                    //var xml = Serializer.Serialize(tmpLst.ToArray());
                    //// csv = CUSTOMER_ID,CUSTOMER_CODE,CUSTOMER_NAME,CURRENCY,TOTAL_RECORD,TOTAL_AMOUNT,DUE_DATE,NEW_CODE,METER_CODE,STATUS_ID
                    var xml = string.Join(Environment.NewLine, tmpLst.Select(x => string.Format("{0},{1},{2},{3},{4:0},{5:0.00},{6:yyyy-MM-dd},{7},{8},{9},{10},{11}",
                        x.CUSTOMER_ID, x.CUSTOMER_CODE, x.CUSTOMER_NAME.Replace(",", "").Replace("\"", "").Replace("\n", "")
                        , x.CURRENCY, x.TOTAL_RECORD, x.TOTAL_AMOUNT, x.DUE_DATE, x.NEW_CODE
                        , x.METER_CODE.Replace(",", "").Replace("\"", "").Replace("\n", "")
                        , x.STATUS_ID, (x.PHONE_NO + "").Replace(",", "").Replace("\"", "").Replace("\n", "")
                        , (x.EXT_DATA + "").Replace(",", "").Replace("\"", "").Replace("\n", ""))).ToArray());

                    var encryptedXml = Crypto.Encrypt(xml, key);

                    client.UpdateCustomerData(objLog.LOG_ID, encryptedXml);

                    UpdateLabel(string.Format("Updating ... {0}/{1}", process, customers.Count));

                    var dt = tmpLst.Select(x => new TBL_BANK_PAYMENT_LOG_CUSTOMER_SUCCESS
                    {
                        LOG_ID = objLog.LOG_ID,
                        CUSTOMER_ID = x.CUSTOMER_ID,
                        CURRENCY = x.CURRENCY
                    });

                    DBDataContext.Db.TBL_BANK_PAYMENT_LOG_CUSTOMER_SUCCESSes.InsertAllOnSubmit(dt);
                    //var dt1 = tmpLst.Select(x => new TBL_BANK_PAYMENT_LOG_CUSTOMER_SUCCESS
                    //{
                    //    LOG_ID = objLog.LOG_ID,
                    //    CUSTOMER_ID = x.CUSTOMER_ID,
                    //    CURRENCY = x.CURRENCY
                    //}); 
                    //  db.TBL_BANK_PAYMENT_LOG_CUSTOMER_SUCCESSes.InsertAllOnSubmit(dt1);
                    // db.TBL_BANK_PAYMENT_LOG_CUSTOMER_SUCCESSes.InsertAllOnSubmit(dt);
                    db.ExecuteCommand(@"
UPDATE TBL_BANK_PAYMENT_LOG_CUSTOMER
SET IS_COMPLETED=1,
    LOG_DATE=GETDATE()
FROM TBL_BANK_PAYMENT_LOG_CUSTOMER c 
WHERE c.LOG_ID=@p0
AND EXISTS( SELECT *
			FROM TBL_BANK_PAYMENT_LOG_CUSTOMER_SUCCESS t
			WHERE t.CUSTOMER_ID=c.CUSTOMER_ID AND t.CURRENCY=c.CURRENCY);

DELETE FROM TBL_BANK_PAYMENT_LOG_CUSTOMER_SUCCESS
WHERE LOG_ID=@p0;", objLog.LOG_ID);
                    objLog.LOG_END_DATE = DateTime.Now;
                    this.db.SubmitChanges(ConflictMode.ContinueOnConflict);
                    tmpLst.Clear();
                }
            }
            objLog.LOG_END_DATE = DateTime.Now;
            objLog.IS_COMPLETED = this.db.TBL_BANK_PAYMENT_LOG_CUSTOMERs.Where(x => x.LOG_ID == objLog.LOG_ID && !x.IS_COMPLETED).Count() == 0;
            this.db.SubmitChanges();
            UpdateLabel("Success!");
        }

        public List<LICENSE_BANK_STATUS> GetActivatedBank()
        {
            client.TestConnection();
            var json = client.GetBankStatus();
            var result = json._ToObject<List<LICENSE_BANK_STATUS>>();
            return result;
        }

        ////public void GetPayment() {

        ////    this.InitializeService();

        ////    UpdateLabel("Downloading...");
        ////    var bytes = client.GetPaymentData();
        ////    if (bytes == null)
        ////    {
        ////        UpdateLabel(Resources.MS_DONT_HAVE_PENDING_PAYMENT_FOR_DOWNLOAD);
        ////        return;
        ////    }

        ////    var encrypted = Encoding.UTF8.GetString(bytes);
        ////    UpdateLabel("Decrypt data...");
        ////    var raw = Crypto.Decrypt(encrypted, this.cryptoKey);

        ////    ProcessPaymentFromCSV(raw);

        ////}
        //public void ProcessPaymentFromCSV(string raw)
        //{
        //    var stopwatch = new Stopwatch();
        //    stopwatch.Start();

        //    bool success = false;
        //    var rawBytes = Serializer.GetBytes(raw);
        //    var transOptions = new TransactionOptions()
        //    {
        //        IsolationLevel = IsolationLevel.ReadUncommitted,
        //        Timeout = TimeSpan.FromMinutes(10)
        //    };
        //    this.db.CommandTimeout = 10 * 60;//30minute;

        //    //using (var tran = new TransactionScope(TransactionScopeOption.Required, transOptions))
        //    //{ 
        //    // save data to payment
        //    var objBankPayment = new TBL_BANK_PAYMENT();
        //    objBankPayment.BANK_PAYMENT_TYPE_ID = (int)BankPaymentTypes.SERVICE;
        //    objBankPayment.CHECK_SUM_FILE = FileHelper.Checksum(rawBytes);
        //    objBankPayment.CREATE_BY = Login.CurrentLogin.LOGIN_NAME;
        //    objBankPayment.CREATE_ON = this.db.GetSystemDate();
        //    objBankPayment.DATA_FILE = rawBytes;
        //    objBankPayment.NAME_FILE = objBankPayment.CREATE_ON.ToString("yyMMddhhmmss") + ".csv";
        //    this.db.TBL_BANK_PAYMENTs.InsertOnSubmit(objBankPayment);
        //    this.db.SubmitChanges();

        //    var lines = raw.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries);
        //    var i = 0;
        //    var now = this.db.GetSystemDate();
        //    var areaCode = Method.Utilities[Utility.BANK_PAYMENT_CODE];
        //    var lst = new List<TBL_BANK_PAYMENT_DETAIL>();
        //    foreach (var line in lines)
        //    {
        //        UpdateLabel(string.Format("Parsing ... {0}/{1}", i++, lines.Count()));

        //        var objDetail = parseToBankPayment(line);
        //        objDetail.BANK_PAYMENT_ID = objBankPayment.BANK_PAYMENT_ID;
        //        objDetail.UPDATE_DATE = now;

        //        lst.Add(objDetail);
        //    }

        //    UpdateLabel(string.Format("Bulk copying..."));
        //    var dt = lst._ToDataTable();
        //    var bulk = new SqlBulkCopy(Method.GetConnectionString());
        //    bulk.BulkCopyTimeout = 60 * 60;// 30-minute
        //    bulk.NotifyAfter = 100;
        //    bulk.DestinationTableName = "TBL_BANK_PAYMENT_DETAIL";
        //    bulk.SqlRowsCopied += delegate (object sender, SqlRowsCopiedEventArgs e)
        //    {
        //        UpdateLabel(string.Format("Bulk copying...{0:#,##0} ", e.RowsCopied));
        //    };
        //    bulk.WriteToServer(dt);
        //    bulk.Close();

        //    UpdateLabel("Process settlement...");
        //    var BPDetails = ProcessPayment(objBankPayment);

        //    stopwatch.Stop();
        //    MessageBox.Show(stopwatch.Elapsed.ToString());

        //    // if client is initialized then it call from services.
        //    // update data back to server.
        //    if (client != null)
        //    {
        //        UpdateLabel("Update to server...");
        //        StringBuilder sb = new StringBuilder();
        //        foreach (var item in BPDetails.Where(x => x.EXT_ID != null))
        //        {
        //            // use PAY_AMOUNT because every payment that download from the server 
        //            // must be handle at the client. we don't need to re-download it again.
        //            // Mark the PAID_AMOUNT equal PAY_AMOUNT means the server already
        //            // allow client to download all payment data.
        //            sb.AppendLine(string.Format("{0},{1}", item.EXT_ID, item.PAY_AMOUNT));
        //        }
        //        client.UpdatePaymentData(Crypto.Encrypt(sb.ToString(), this.cryptoKey));
        //    }


        //    //  tran.Complete(); 
        //    UpdateLabel(string.Format("Success : {0} record(s) found!", lines.Length));
        //    success = true;
        //    //}

        //    // if success continue update customer.
        //    if (success && client != null)
        //    {
        //        UpdateLabel("Update customer...");
        //        UpdateCustoemrData(false);
        //    }
        //}

        //public IEnumerable<TBL_BANK_PAYMENT_DETAIL> ProcessPayment(TBL_BANK_PAYMENT objBP)
        //{
        //    //this.db.CommandTimeout = 60 * 60;//30minute;
        //    //var cn = (SqlConnection)db.Connection;
        //    //cn.FireInfoMessageEventOnUserErrors = true;
        //    //cn.InfoMessage += cn_InfoMessage; 
        //    db.ExecuteCommand("EXEC RUN_BANK_PAYMENT @p0;", objBP.BANK_PAYMENT_ID);
        //    //using (var sup = new TransactionScope(TransactionScopeOption.Suppress))
        //    //{
        //    db.ExecuteCommand("EXEC RUN_BANK_PAYMENT_VERIFY @p0;", objBP.BANK_PAYMENT_ID);
        //    //}
        //    //cn.FireInfoMessageEventOnUserErrors = false;
        //    // cn.InfoMessage -= cn_InfoMessage; 
        //    return db.TBL_BANK_PAYMENT_DETAILs.Where(x => x.BANK_PAYMENT_ID == objBP.BANK_PAYMENT_ID);
        //}

        //void cn_InfoMessage(object sender, SqlInfoMessageEventArgs e)
        //{
        //    if (e.Errors[0].Class != 1)
        //    {
        //        Runner.Instance.Error = new Exception(e.Message);
        //    }
        //    else
        //    {
        //        UpdateLabel(e.Message);
        //    }
        //}

        //private TBL_BANK_PAYMENT_DETAIL parseToBankPayment(string value)
        //{
        //    // BANK,BRANCH,CUSTOMER_CODE,CURRENCY,AMOUNT,DATE,TIME,NOTE,CASHIER,METHOD,TYPE
        //    //   0  , 1    ,    2       , 3      ,    4 ,  5 , 6  , 7  ,  8    ,  9   ,10
        //    // AMRET,AMR-HEAD,002035-NA1,R,626400.0000,2014-04-02,16:04,This is note,CHENDA,B,P
        //    var arr = Serializer.SplitCsv(value).ToArray();
        //    var obj = new TBL_BANK_PAYMENT_DETAIL()
        //    {
        //        BANK = arr[0],
        //        BRANCH = arr[1],
        //        CUSTOMER_CODE = arr[2],
        //        CURRENCY = arr[3],
        //        PAY_AMOUNT = decimal.Parse(arr[4]),
        //        PAY_DATE = DateTime.Parse(arr[5]).Add(TimeSpan.Parse(arr[6])),
        //        NOTE = arr[7],
        //        CASHIER = arr[8],
        //        PAYMENT_METHOD = arr[9],
        //        PAYMENT_TYPE = arr[10],
        //        EXT_ID = arr.Length < 11 || arr[11] == "" ? Guid.Empty : new Guid(arr[11]),
        //        RESULT = 0,
        //        RESULT_NOTE = ""
        //    };
        //    return obj;
        //}

        public int GetPendingPaymentCount()
        {
            //var client = getClient();
            return client.GetPendingPaymentCount();
        }

        private EPowerServiceClient getClient()
        {
            var url = Method.Utilities[Utility.BANK_PAYMENT_SERVICE_URL]; ;
            Uri address = new Uri(url);
            var endpoint = new EndpointAddress(address.AbsoluteUri);
            var binding = new WSHttpBinding()
            {
                CloseTimeout = TimeSpan.FromSeconds(20),
                OpenTimeout = TimeSpan.FromSeconds(20),
                ReceiveTimeout = TimeSpan.FromHours(2),
                SendTimeout = TimeSpan.FromHours(2),
                MaxBufferPoolSize = 2147483647,
                MaxReceivedMessageSize = 2147483647,
                BypassProxyOnLocal = false,
                HostNameComparisonMode = HostNameComparisonMode.StrongWildcard,
                ReaderQuotas = new XmlDictionaryReaderQuotas()
                {
                    MaxArrayLength = 2147483647,
                    MaxBytesPerRead = 2147483647,
                    MaxStringContentLength = 2147483647,
                    MaxNameTableCharCount = 2147483647
                },
                //AllowCookies = false
            };
            //binding.Security.Mode = SecurityMode.None;
            binding.Security.Mode = SecurityMode.Message;
            binding.Security.Message.ClientCredentialType = MessageCredentialType.Windows;

            var client = new EPowerServiceClient(binding, endpoint);
            client.ClientCredentials.Windows.AllowedImpersonationLevel = TokenImpersonationLevel.Impersonation;
            if (!url.Contains("localhost"))
            {
                client.ClientCredentials.Windows.ClientCredential = new NetworkCredential("wcf", "wcf");
            }
            var accessKey = Method.Utilities[Utility.BANK_PAYMENT_ACCESS_KEY];
            var scope = new OperationContextScope(client.InnerChannel);
            var ip = Dns.GetHostAddresses(Dns.GetHostName())[0].ToString();
            OperationContext.Current.OutgoingMessageHeaders.Add(MessageHeader.CreateHeader("AccessKey", "", accessKey));
            OperationContext.Current.OutgoingMessageHeaders.Add(MessageHeader.CreateHeader("user", "", Login.CurrentLogin.LOGIN_NAME));
            OperationContext.Current.OutgoingMessageHeaders.Add(MessageHeader.CreateHeader("host", "", Environment.MachineName));
            OperationContext.Current.OutgoingMessageHeaders.Add(MessageHeader.CreateHeader("ip", "", ip));
            OperationContext.Current.OutgoingMessageHeaders.Add(MessageHeader.CreateHeader("clientVersion", "", Update.Version.GetCurrentVersion()));
            return client;
        }

        public static void RunAutoSendDataInNewThread()
        {
            // if not use bank payment
            if (DataHelper.ParseToBoolean(Method.GetUtilityValue(Utility.BANK_PAYMENT_ENABLE)) == false)
            {
                return;
            }

            // if not set to be auto send
            if (DataHelper.ParseToBoolean(Method.GetUtilityValue(Utility.AUTO_SEND_DATA_TO_BANK)) == false)
            {
                return;
            }

            // if no internet connection
            if (TCPHelper.IsInternetConnected() == false)
            {
                return;
            }

            var thread = new Thread(delegate ()
            {
                Thread.Sleep(1 * 60 * 1000);
                try
                {
                    var po = new PaymentOperation();

                    //VPN.Instance.Connect();
                    //Thread.Sleep(1 * 60 * 100);
                    //if (VPN.Instance.State == VpnClient.VpnState.Connected)
                    //{
                    po.UpdateLabel = (string msg) => { };
                    po.ShowMessage = (string msg) => { };
                    po.UpdateCustomerData(false);
                    //VPN.Instance.Disconnect();
                    //}
                }
                catch (Exception ex)
                {
                    MsgBox.LogError(ex);
                }
            });
            thread.Start();

            Application.ApplicationExit += (object sender, EventArgs e) =>
            {
                thread.Abort();
            };
        }

        public void GetPayment()
        {
            bool success = false;
            UpdateLabel("Initializing...");
            var key = Method.Utilities[Utility.BANK_PAYMENT_CRYPTO_KEY];
            //var client = getClient();
            UpdateLabel("Connecting to server...");
            client.TestConnection();

            UpdateLabel("Downloading...");
            TBL_BANK_PAYMENT objBankPayment = null;
            var bytes = client.GetPaymentData();
            var currentPending = 0;
            var totalPending = client.GetPendingPaymentCount();
            var totalFinish = 0;
            while (bytes != null && currentPending != (currentPending = client.GetPendingPaymentCount()))
            {
                UpdateLabel("Decrypt data...");
                var encrypted = Encoding.UTF8.GetString(bytes);
                var raw = Crypto.Decrypt(encrypted, key);
                var rawBytes = Serializer.GetBytes(raw);

                var options = new TransactionOptions()
                {
                    Timeout = TimeSpan.FromMinutes(10),
                    IsolationLevel = IsolationLevel.ReadUncommitted
                };
                LoginName = Login.CurrentLogin.LOGIN_NAME;
                UserCashDrawerId = Login.CurrentCashDrawer.USER_CASH_DRAWER_ID;
                using (var tran = new TransactionScope(TransactionScopeOption.Required, options))
                {
                    // save data to payment
                    if (objBankPayment == null)
                    {
                        objBankPayment = new TBL_BANK_PAYMENT();
                        objBankPayment.BANK_PAYMENT_TYPE_ID = (int)BankPaymentTypes.SERVICE;
                        objBankPayment.CHECK_SUM_FILE = FileHelper.Checksum(rawBytes);
                        objBankPayment.CREATE_BY = LoginName;
                        objBankPayment.CREATE_ON = this.db.GetSystemDate();
                        objBankPayment.DATA_FILE = rawBytes;
                        objBankPayment.NAME_FILE = objBankPayment.CREATE_ON.ToString("yyMMddhhmmss") + ".csv";
                        this.db.TBL_BANK_PAYMENTs.InsertOnSubmit(objBankPayment);
                        this.db.SubmitChanges();
                    }

                    var lines = raw.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries);
                    var i = 0;
                    var now = this.db.GetSystemDate();
                    var areaCode = Method.Utilities[Utility.BANK_PAYMENT_CODE];
                    foreach (var line in lines)
                    {
                        UpdateLabel(string.Format("Parsing ... {0}/{1}", totalFinish + i++, totalPending));

                        var objDetail = parseToBankPayment(line);
                        objDetail.BANK_PAYMENT_ID = objBankPayment.BANK_PAYMENT_ID;
                        objDetail.UPDATE_DATE = now;

                        var errors = new List<string>();
                        var code = objDetail.CUSTOMER_CODE.Replace(areaCode + "-", "");
                        var objCustomer = this.db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_CODE == code);
                        if (objCustomer == null)
                        {
                            errors.Add("Customer");
                        }
                        else
                        {
                            objDetail.CUSTOMER_ID = objCustomer.CUSTOMER_ID;
                        }
                        var objCurrency = this.db.TLKP_CURRENCies.FirstOrDefault(x => x.CURRENCY_CODE == objDetail.CURRENCY);
                        if (objCurrency == null)
                        {
                            errors.Add("Currency");
                        }
                        else
                        {
                            objDetail.CURRENCY_ID = objCurrency.CURRENCY_ID;
                        }
                        if (errors.Count > 0)
                        {
                            objDetail.RESULT = (int)BankPaymentImportResult.RecordError;
                            objDetail.RESULT_NOTE = string.Format("Invalid {0}", string.Join(",", errors.ToArray()));
                        }
                        else
                        {
                            var objOldDetail = this.db.TBL_BANK_PAYMENT_DETAILs
                                                     .FirstOrDefault(x => x.CUSTOMER_ID == objDetail.CUSTOMER_ID
                                                        && x.PAY_DATE == objDetail.PAY_DATE
                                                        && x.PAY_AMOUNT == objDetail.PAID_AMOUNT
                                                        && x.CURRENCY_ID == objDetail.CURRENCY_ID
                                                        && x.RESULT != (int)BankPaymentImportResult.RecordError);
                            if (objOldDetail == null)
                            {
                                objDetail.RESULT = (int)BankPaymentImportResult.RecordSuccess;
                            }
                            else
                            {
                                objDetail.RESULT = (int)BankPaymentImportResult.RecordPaid;
                            }
                        }
                        this.db.TBL_BANK_PAYMENT_DETAILs.InsertOnSubmit(objDetail);
                        this.db.SubmitChanges();
                    }

                    //UpdateLabel("Process payment...");
                    //var BPDetails = ProcessPayment(objBankPayment, totalFinish, totalPending);

                    var BPDetails = DBDataContext.Db.TBL_BANK_PAYMENT_DETAILs.Where(x => x.BANK_PAYMENT_ID == objBankPayment.BANK_PAYMENT_ID);
                    objBankPayment.TOTAL_RECORD = BPDetails.Count();
                    this.db.SubmitChanges();

                    // update data back to server.
                    UpdateLabel("Update payment to server...");
                    StringBuilder sb = new StringBuilder();
                    foreach (var item in BPDetails.Where(x => x.EXT_ID != null))
                    {
                        // use PAY_AMOUNT because every payment that download from the server 
                        // must be handle at the client. we don't need to re-download it again.
                        // Mark the PAID_AMOUNT equal PAY_AMOUNT means the server already
                        // allow client to download all payment data.
                        sb.AppendLine(string.Format("{0},{1}", item.EXT_ID, item.PAY_AMOUNT));
                    }
                    //client = getClient();
                    client.UpdatePaymentData(Crypto.Encrypt(sb.ToString(), key));

                    tran.Complete();
                    success = true;
                    UpdateLabel(string.Format("Success : {0} record(s) found!", lines.Length));
                    totalFinish += lines.Count();
                }

                // iterate next result
                //client = getClient();
                bytes = client.GetPaymentData();
            }
            if (bytes == null && currentPending == 0)
            {
                UpdateLabel(Resources.MS_DONT_HAVE_PENDING_PAYMENT_FOR_DOWNLOAD);
                return;
            }

            // if success continue update customer.
            //if (totalFinish > 0)
            //{
            //    // raise new method.
            //    var po = new PaymentOperation();
            //    po.UpdateLabel = this.UpdateLabel;
            //    po.ShowMessage = this.ShowMessage;
            //    po.UpdateCustoemrData(false);
            //    //UpdateCustoemrData(false);
            //}

        }

        public IEnumerable<TBL_BANK_PAYMENT_DETAIL> ProcessPayment(TBL_BANK_PAYMENT objBP, int totalFinish = 0, int totalPending = 0)
        {
            try
            {
                var now = this.db.GetSystemDate();
                var BPDetails = (from d in this.db.TBL_BANK_PAYMENT_DETAILs
                                 join p in this.db.TBL_BANK_PAYMENTs on d.BANK_PAYMENT_ID equals p.BANK_PAYMENT_ID
                                 where d.RESULT == (int)BankPaymentImportResult.RecordSuccess
                                        && d.IS_VOID == false
                                        && p.BANK_PAYMENT_TYPE_ID == objBP.BANK_PAYMENT_TYPE_ID
                                        && d.BANK_PAYMENT_ID <= objBP.BANK_PAYMENT_ID
                                        && d.PAY_AMOUNT != d.PAID_AMOUNT
                                 select d).ToList();
                if (totalPending == 0)
                {
                    totalPending = BPDetails.Count();
                }
                bool includeDeposit = DataHelper.ParseToBoolean(Method.GetUtilityValue(Utility.BANK_PAYMENT_INCLUDE_DEPOSIT));
                int i = 1;
                foreach (var objBPD in BPDetails)
                {
                    using (var tran = new TransactionScope(TransactionScopeOption.Required, DBDataContext.Db.TransactionOption()))
                    {
                        UpdateLabel(string.Format("Settlementing...{0}/{1}", totalFinish + i++, totalPending));
                        settlePayment(objBPD, includeDeposit);
                        tran.Complete();
                    }
                }

                // summary
                //objBP.TOTAL_RECORD = this.db.TBL_BANK_PAYMENT_DETAILs.Where(x => x.BANK_PAYMENT_ID == objBP.BANK_PAYMENT_ID).Count();
                this.db.SubmitChanges();

                return BPDetails;
            }
            catch (Exception ex)
            {
                MsgBox.LogError(ex);
                throw new Exception(string.Format(Resources.MSG_ERROR, Resources.SETTLEMENT));
            }

        }

        private void settlePayment(TBL_BANK_PAYMENT_DETAIL objBPD, bool includeDeposit)
        {
            //check if negative is the cancel
            //if found the last payment with the same amount then cancel 
            //else if not found mark that payment detail result as ERROR.
            if (objBPD.PAY_AMOUNT < 0)
            {
                var objBPDToCancel = this.db.TBL_BANK_PAYMENT_DETAILs
                                            .OrderByDescending(x => x.PAY_DATE)
                                            .FirstOrDefault(x => x.PAY_AMOUNT == -objBPD.PAY_AMOUNT
                                                                && x.CURRENCY_ID == objBPD.CURRENCY_ID
                                                                && x.CUSTOMER_ID == objBPD.CUSTOMER_ID
                                                                && x.IS_VOID == false
                                                                && x.PAY_DATE == objBPD.PAY_DATE);
                if (objBPDToCancel == null)
                {
                    objBPD.RESULT = (int)BankPaymentImportResult.RecordError;
                    objBPD.RESULT_NOTE = "Fail to find last payment to cancel.";
                    return;
                }
                var cancelAmount = 0m;
                foreach (var objPaymentToCancel in this.db.TBL_PAYMENTs.Where(x => x.BANK_PAYMENT_DETAIL_ID == objBPDToCancel.BANK_PAYMENT_DETAIL_ID && !x.IS_VOID))
                {
                    if (objPaymentToCancel != null)
                    {
                        Payment.CancelPayment(objPaymentToCancel, this.db, "");
                        cancelAmount += objPaymentToCancel.PAY_AMOUNT;
                    }
                }
                foreach (var objDepositToCancel in this.db.TBL_CUS_DEPOSITs.Where(x => x.BANK_PAYMENT_DETAIL_ID == objBPDToCancel.BANK_PAYMENT_DETAIL_ID))
                {
                    cancelAmount += objDepositToCancel.AMOUNT;
                    objDepositToCancel.IS_PAID = false;
                    db.SubmitChanges();
                }

                // PAID_AMOUNT equals PAY_AMOUNT for cancel payment record.
                // that means no outstanding amount for next settlement.
                objBPD.PAID_AMOUNT = objBPD.PAY_AMOUNT;

                // cancel parent payment record.
                objBPDToCancel.IS_VOID = true;
                // cancel child payment record
                objBPD.IS_VOID = true;
                this.db.SubmitChanges();
                return;
            }

            UserCashDrawerId = Login.CurrentCashDrawer.USER_CASH_DRAWER_ID;
            var customerIds = new List<int>();
            //using (TransactionScope sup = new TransactionScope(TransactionScopeOption.Suppress))
            //{
            customerIds = this.db.TBL_CUSTOMERs.Where(x => x.CUSTOMER_ID == objBPD.CUSTOMER_ID || x.INVOICE_CUSTOMER_ID == objBPD.CUSTOMER_ID)
                                                         .Select(x => x.CUSTOMER_ID).ToList();
            //}
            var availabeAmount = objBPD.PAY_AMOUNT - objBPD.PAID_AMOUNT;


            // settle with deposit.
            if (includeDeposit)
            {
                var deposits = this.db.TBL_CUS_DEPOSITs.Where(x => customerIds.Contains(x.CUSTOMER_ID) && x.IS_PAID == false);
                foreach (var deposit in deposits)
                {
                    if (deposit.AMOUNT > 0 && deposit.AMOUNT <= availabeAmount)
                    {
                        deposit.BANK_PAYMENT_DETAIL_ID = objBPD.BANK_PAYMENT_DETAIL_ID;
                        deposit.IS_PAID = true;
                        deposit.DEPOSIT_DATE = objBPD.PAY_DATE;
                        deposit.USER_CASH_DRAWER_ID = UserCashDrawerId;
                        objBPD.PAID_AMOUNT += deposit.AMOUNT;
                        db.SubmitChanges();

                        availabeAmount -= deposit.AMOUNT;
                    }
                }
            }

            // settle with invoice.
            var Invoices = new List<TBL_INVOICE>();
            //using (TransactionScope sup = new TransactionScope(TransactionScopeOption.Suppress))
            //{
            Invoices = this.db.TBL_INVOICEs.Where(x => x.INVOICE_STATUS == (int)InvoiceStatus.Open
                                                          && x.PAID_AMOUNT < x.SETTLE_AMOUNT
                                                          && customerIds.Contains(x.CUSTOMER_ID)
                                                          && x.CURRENCY_ID == objBPD.CURRENCY_ID)
                                          .ToList();
            //}
            // if no invoice for payment then jumpt to next detail.
            if (Invoices.Count() == 0)
            {
                return;
            }

            DateTime now = this.db.GetSystemDate();
            var paymentConfig = DBDataContext.Db.TBL_PAYMENT_CONFIGs.FirstOrDefault(x => x.PAYMENT_TYPE == objBPD.BANK);
            var accountID = 73;
            if (paymentConfig != null)
            {
                if (objBPD.CURRENCY_ID == (int)Currency.KHR)
                {
                    accountID = paymentConfig.ACCOUNT_ID_KHR;
                }
                else if (objBPD.CURRENCY_ID == (int)Currency.USD)
                {
                    accountID = paymentConfig.ACCOUNT_ID_USD;
                }
                else if (objBPD.CURRENCY_ID == (int)Currency.THB)
                {
                    accountID = paymentConfig.ACCOUNT_ID_THB;
                }
                else if (objBPD.CURRENCY_ID == (int)Currency.VND)
                {
                    accountID = paymentConfig.ACCOUNT_ID_VND;
                }
            }

            var exchangeRate = new ExchangeRateLogic().findLastExchangeRate(objBPD.PAY_DATE, objBPD.CURRENCY_ID);
            var objPayment = new TBL_PAYMENT()
            {
                CREATE_BY = LoginName + "(BANK)",
                CREATE_ON = now,
                CUSTOMER_ID = objBPD.CUSTOMER_ID,
                DUE_AMOUNT = 0,
                IS_ACTIVE = true,
                PAY_AMOUNT = 0,
                PAY_DATE = objBPD.PAY_DATE,
                PAYMENT_ID = 0,
                PAYMENT_NO = "EBP-" + objBPD.BANK_PAYMENT_DETAIL_ID.ToString("000000"),//Logic.Method.GetNextSequence(Sequence.Receipt,true),
                USER_CASH_DRAWER_ID = UserCashDrawerId,
                BANK_PAYMENT_DETAIL_ID = objBPD.BANK_PAYMENT_DETAIL_ID,
                CURRENCY_ID = objBPD.CURRENCY_ID,
                PAYMENT_ACCOUNT_ID = accountID, ///73 // CASH ON BANK
                NOTE = "",
                EXCHANGE_RATE = exchangeRate.EXCHANGE_RATE,
                EXCHANGE_RATE_DATE = exchangeRate.CREATE_ON,
                PAYMENT_METHOD_ID = (int)PaymentMethod.Bank
            };
            this.db.TBL_PAYMENTs.InsertOnSubmit(objPayment);
            this.db.SubmitChanges();


            availabeAmount = objBPD.PAY_AMOUNT - objBPD.PAID_AMOUNT;
            var totalDue = 0.0m;
            var totalPay = 0.0m;
            foreach (var objInvoice in Invoices)
            {
                if (availabeAmount <= 0) break;

                var dueAmount = objInvoice.SETTLE_AMOUNT - objInvoice.PAID_AMOUNT;
                var payAmount = availabeAmount > dueAmount ? dueAmount : availabeAmount;
                availabeAmount = availabeAmount - payAmount;

                totalDue += dueAmount;
                totalPay += payAmount;

                var objPaymentDetail = new TBL_PAYMENT_DETAIL()
                {
                    DUE_AMOUNT = dueAmount,
                    INVOICE_ID = objInvoice.INVOICE_ID,
                    PAY_AMOUNT = payAmount,
                    PAYMENT_DETAIL_ID = 0,
                    PAYMENT_ID = objPayment.PAYMENT_ID
                };
                this.db.TBL_PAYMENT_DETAILs.InsertOnSubmit(objPaymentDetail);
                this.db.SubmitChanges();

                objInvoice.PAID_AMOUNT += payAmount;
                objInvoice.INVOICE_STATUS = objInvoice.SETTLE_AMOUNT == objInvoice.PAID_AMOUNT ? (int)InvoiceStatus.Close : (int)InvoiceStatus.Open;
                objInvoice.ROW_DATE = now;
                this.db.SubmitChanges();
            }

            objPayment.PAY_AMOUNT = totalPay;
            objPayment.DUE_AMOUNT = totalDue;
            this.db.SubmitChanges();

            objBPD.PAID_AMOUNT += objPayment.PAY_AMOUNT;
            this.db.SubmitChanges();
        }

        private TBL_BANK_PAYMENT_DETAIL parseToBankPayment(string value)
        {
            // BANK,BRANCH,CUSTOMER_CODE,CURRENCY,AMOUNT,DATE,TIME,NOTE,CASHIER,METHOD,TYPE
            //   0  , 1    ,    2       , 3      ,    4 ,  5 , 6  , 7  ,  8    ,  9   ,10
            // AMRET,AMR-HEAD,002035-NA1,R,626400.0000,2014-04-02,16:04,This is note,CHENDA,B,P
            var arr = Serializer.SplitCsv(value).ToArray();
            var obj = new TBL_BANK_PAYMENT_DETAIL()
            {
                BANK = arr[0],
                BRANCH = arr[1],
                CUSTOMER_CODE = arr[2],
                CURRENCY = arr[3],
                PAY_AMOUNT = decimal.Parse(arr[4]),
                PAY_DATE = DateTime.Parse(arr[5]).Add(TimeSpan.Parse(arr[6])),
                NOTE = arr[7],
                CASHIER = arr[8],
                PAYMENT_METHOD = arr[9],
                PAYMENT_TYPE = arr[10],
                EXT_ID = new Guid(arr[11]),
                BRANCH_ALIAS = arr[12].Trim()
            };
            obj.BANK_ALIAS = obj.BRANCH_ALIAS != obj.BRANCH ? obj.BRANCH_ALIAS : obj.BANK;
            return obj;
        }

        public void ClearPayment()
        {
            UpdateLabel("Process payment...");
            IEnumerable<TBL_BANK_PAYMENT_DETAIL> result = null;

            result = this.ProcessPayment(new TBL_BANK_PAYMENT()
            {
                BANK_PAYMENT_ID = int.MaxValue,
                BANK_PAYMENT_TYPE_ID = (int)BankPaymentTypes.SERVICE
            });

            if (result.Count() > 0)
            {
                // when download data already send amount to server
                // after settle complete will send customer to server
                if (TCPHelper.IsInternetConnected())
                {
                    this.UpdateCustomerData(false);
                }
            }
        }
    }
    public class LICENSE_BANK_STATUS
    {
        public string BANK_NAME { get; set; }
        public string BANK_CODE { get; set; }
        public bool ACTIVATED { get; set; }
    }
}