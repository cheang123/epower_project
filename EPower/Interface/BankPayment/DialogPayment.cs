﻿using EPower.Base.Logic;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface.BankPayment
{
    public partial class DialogPayment : ExDialog
    {
        int id = 0;
        public DialogPayment(int id)
        {
            InitializeComponent();
            var objPayment = DBDataContext.Db.TBL_PAYMENTs.FirstOrDefault(x => x.PAYMENT_ID == id);
            var objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == objPayment.CUSTOMER_ID);
            var objCurrency = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.CURRENCY_ID == objPayment.CURRENCY_ID);

            this.txtCreateBy.Text = objPayment.CREATE_BY;
            this.txtPayDate.Text = objPayment.PAY_DATE.ToString("dd-MM-yyyy");
            this.txtPaymentNo.Text = objPayment.PAYMENT_NO;
            this.txtCustomerCode.Text = objCustomer.CUSTOMER_CODE;
            this.txtName.Text = objCustomer.LAST_NAME_KH + " " + objCustomer.FIRST_NAME_KH;
            this.txtPayAmount.Text = objPayment.PAY_AMOUNT.ToString("N2") + " " + objCurrency.CURRENCY_SING;

            var q = from d in DBDataContext.Db.TBL_PAYMENT_DETAILs
                    join i in DBDataContext.Db.TBL_INVOICEs on d.INVOICE_ID equals i.INVOICE_ID
                    where d.PAYMENT_ID == id
                    select new
                    {
                        i.INVOICE_ID,
                        i.INVOICE_NO,
                        i.INVOICE_DATE,
                        i.INVOICE_TITLE,
                        i.SETTLE_AMOUNT,
                        d.PAY_AMOUNT
                    };
            this.dgv.DataSource = q;
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && this.dgv.Columns[e.ColumnIndex].Name == this.INVOICE_NO.Name)
            {
                var row = this.dgv.Rows[e.RowIndex];
                var invoiceID = (long)row.Cells[this.INVOICE_ID.Name].Value;
                TBL_PRINT_INVOICE objPrintInvoice = new TBL_PRINT_INVOICE()
                {
                    PRINT_DATE = DBDataContext.Db.GetSystemDate(),
                    LOGIN_ID = Login.CurrentLogin.LOGIN_ID
                };
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    DBDataContext.Db.TBL_PRINT_INVOICEs.InsertOnSubmit(objPrintInvoice);
                    DBDataContext.Db.SubmitChanges();
                    int i = 1;
                    DBDataContext.Db.TBL_PRINT_INVOICE_DETAILs.InsertOnSubmit(new TBL_PRINT_INVOICE_DETAIL()
                    {
                        PRINT_INVOICE_ID = objPrintInvoice.PRINT_INVOICE_ID,
                        INVOICE_ID = DataHelper.ParseToInt(invoiceID.ToString()),
                        PRINT_ORDER = i++
                    });
                    DBDataContext.Db.SubmitChanges();
                    tran.Complete();
                }
                new DialogCustomerPrintInvoice(true, objPrintInvoice.PRINT_INVOICE_ID).ShowDialog();

            }
        }
    }
}
