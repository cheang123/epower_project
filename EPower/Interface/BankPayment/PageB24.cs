﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface.BankPayment
{
    public partial class PageB24 : Form
    {
        bool _loading = false;

        public PageB24()
        {
            InitializeComponent();
            try
            {
                btnPayment.Visible= !string.IsNullOrEmpty(Method.Utilities[Utility.B24_CUSTOMER_URL_PAYMENT]);
                btnTO_B24.Visible= btnB24_CUSTOMER.Visible= btnSEND_DATA.Visible= !string.IsNullOrEmpty(Method.Utilities[Utility.B24_SUPPLIER_API_KEY]);

                UIHelper.DataGridViewProperties(dgv);
                this._loading = true;
                DateTime now = DBDataContext.Db.GetSystemDate();
                dtpD1.Value = new DateTime(now.Year, now.Month, 1);
                dtpD2.Value = now;
                this._loading = false;
                LoadData();
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        public void LoadData()
        { 
            if (_loading) return; 
            var d1 = this.dtpD1.Value.Date;
            var d2 = this.dtpD2.Value.Date.AddDays(1).AddSeconds(-1);

            var q = DBDataContext.Db.TBL_B24_LOGs.Where(x => x.IS_ACTIVE && (x.CREATE_ON >= d1 && x.CREATE_ON <= d2)).OrderByDescending(x=>x.CREATE_ON);
            this.dgv.DataSource = q;
        }
        
        private void dtpD2_ValueChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        private void dtpD1_ValueChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnSEND_DATA_Click(object sender, EventArgs e)
        {
            if (new DialogB24SendData().ShowDialog() == DialogResult.OK)
            {
                LoadData();   
            }
        }

        private void btnPayment_Click(object sender, EventArgs e)
        {
            //new DialogWebView(Method.utilities[Utility.B24_CUSTOMER_URL_PAYMENT]).ShowDialog();
            string url = Method.utilities[Utility.B24_CUSTOMER_URL_PAYMENT];
            System.Diagnostics.Process.Start(url);
        }

        private void btnB24_CUSTOMER_Click(object sender, EventArgs e)
        {
            new DialogB24Customer().ShowDialog();
        }

        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != -1 && e.RowIndex != -1)
            {
                int b24LogId = (int)dgv.SelectedRows[0].Cells[B24_LOG_ID.Name].Value;                
                if (this.dgv.Columns[e.ColumnIndex].Name == this.TOTAL_CUSTOMER.Name)
                {
                    if ((int)dgv.SelectedRows[0].Cells[TOTAL_CUSTOMER.Name].Value == 0)
                    {
                        MsgBox.ShowInformation(Resources.MS_NO_CUSTOMER_TO_UPDATE);
                        return;
                    }
                    new DialogB24LogDetailCustomer(b24LogId).ShowDialog();
                }
                else 
                {
                    if ((int)dgv.SelectedRows[0].Cells[TOTAL_INVOICE.Name].Value == 0)
                    {
                        MsgBox.ShowInformation(Resources.MSG_B24_NO_INVOICE_SEND);
                        return;
                    }
                    new DialogB24LogDetailInvoice(b24LogId).ShowDialog();
                }
            }
        }

        private void btnTO_B24_Click(object sender, EventArgs e)
        {
            //new DialogWebView(Method.Utilities[Utility.B24_SUPPLIER_URL]).ShowDialog();
            string url = Method.Utilities[Utility.B24_SUPPLIER_URL];
            System.Diagnostics.Process.Start(url);
        }
    }
}
