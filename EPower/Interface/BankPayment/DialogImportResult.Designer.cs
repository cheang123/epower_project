﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.BankPayment
{
    partial class DialogImportResult
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogImportResult));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnClose = new SoftTech.Component.ExButton();
            this.dgvInvoiceItem = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.cboResult = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblAmount = new System.Windows.Forms.Label();
            this.lblCount = new System.Windows.Forms.Label();
            this.INVOICE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DUE_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAY_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAY_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RECEIVE_BY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RESULT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ERROR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAY_AMOUNT2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInvoiceItem)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.lblCount);
            this.content.Controls.Add(this.lblAmount);
            this.content.Controls.Add(this.label5);
            this.content.Controls.Add(this.label6);
            this.content.Controls.Add(this.label3);
            this.content.Controls.Add(this.label4);
            this.content.Controls.Add(this.label2);
            this.content.Controls.Add(this.cboResult);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.dgvInvoiceItem);
            this.content.Controls.Add(this.btnClose);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnClose, 0);
            this.content.Controls.SetChildIndex(this.dgvInvoiceItem, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.cboResult, 0);
            this.content.Controls.SetChildIndex(this.label2, 0);
            this.content.Controls.SetChildIndex(this.label4, 0);
            this.content.Controls.SetChildIndex(this.label3, 0);
            this.content.Controls.SetChildIndex(this.label6, 0);
            this.content.Controls.SetChildIndex(this.label5, 0);
            this.content.Controls.SetChildIndex(this.lblAmount, 0);
            this.content.Controls.SetChildIndex(this.lblCount, 0);
            // 
            // btnClose
            // 
            resources.ApplyResources(this.btnClose, "btnClose");
            this.btnClose.Name = "btnClose";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // dgvInvoiceItem
            // 
            this.dgvInvoiceItem.AllowUserToAddRows = false;
            this.dgvInvoiceItem.AllowUserToDeleteRows = false;
            this.dgvInvoiceItem.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvInvoiceItem.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvInvoiceItem.BackgroundColor = System.Drawing.Color.White;
            this.dgvInvoiceItem.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvInvoiceItem.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvInvoiceItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInvoiceItem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.INVOICE_ID,
            this.CUSTOMER_CODE,
            this.CUSTOMER_NAME,
            this.DUE_AMOUNT,
            this.PAY_AMOUNT,
            this.PAY_DATE,
            this.RECEIVE_BY,
            this.RESULT,
            this.ERROR,
            this.PAY_AMOUNT2});
            resources.ApplyResources(this.dgvInvoiceItem, "dgvInvoiceItem");
            this.dgvInvoiceItem.Name = "dgvInvoiceItem";
            this.dgvInvoiceItem.ReadOnly = true;
            this.dgvInvoiceItem.RowHeadersVisible = false;
            this.dgvInvoiceItem.RowTemplate.Height = 25;
            this.dgvInvoiceItem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // cboResult
            // 
            this.cboResult.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboResult.FormattingEnabled = true;
            resources.ApplyResources(this.cboResult, "cboResult");
            this.cboResult.Name = "cboResult";
            this.cboResult.SelectedIndexChanged += new System.EventHandler(this.cboResult_SelectedIndexChanged);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // lblAmount
            // 
            resources.ApplyResources(this.lblAmount, "lblAmount");
            this.lblAmount.Name = "lblAmount";
            // 
            // lblCount
            // 
            resources.ApplyResources(this.lblCount, "lblCount");
            this.lblCount.Name = "lblCount";
            // 
            // INVOICE_ID
            // 
            this.INVOICE_ID.DataPropertyName = "INVOICE_ID";
            resources.ApplyResources(this.INVOICE_ID, "INVOICE_ID");
            this.INVOICE_ID.Name = "INVOICE_ID";
            this.INVOICE_ID.ReadOnly = true;
            // 
            // CUSTOMER_CODE
            // 
            this.CUSTOMER_CODE.DataPropertyName = "CUSTOMER_CODE";
            resources.ApplyResources(this.CUSTOMER_CODE, "CUSTOMER_CODE");
            this.CUSTOMER_CODE.Name = "CUSTOMER_CODE";
            this.CUSTOMER_CODE.ReadOnly = true;
            // 
            // CUSTOMER_NAME
            // 
            this.CUSTOMER_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CUSTOMER_NAME.DataPropertyName = "CUSTOMER_NAME";
            resources.ApplyResources(this.CUSTOMER_NAME, "CUSTOMER_NAME");
            this.CUSTOMER_NAME.Name = "CUSTOMER_NAME";
            this.CUSTOMER_NAME.ReadOnly = true;
            // 
            // DUE_AMOUNT
            // 
            this.DUE_AMOUNT.DataPropertyName = "DUE_AMOUNT";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "#,##0.####";
            this.DUE_AMOUNT.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.DUE_AMOUNT, "DUE_AMOUNT");
            this.DUE_AMOUNT.Name = "DUE_AMOUNT";
            this.DUE_AMOUNT.ReadOnly = true;
            // 
            // PAY_AMOUNT
            // 
            this.PAY_AMOUNT.DataPropertyName = "PAY_AMOUNT";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "#,##0.####";
            this.PAY_AMOUNT.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.PAY_AMOUNT, "PAY_AMOUNT");
            this.PAY_AMOUNT.Name = "PAY_AMOUNT";
            this.PAY_AMOUNT.ReadOnly = true;
            // 
            // PAY_DATE
            // 
            this.PAY_DATE.DataPropertyName = "PAY_DATE";
            dataGridViewCellStyle4.Format = "dd-MMM-yyyy";
            this.PAY_DATE.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.PAY_DATE, "PAY_DATE");
            this.PAY_DATE.Name = "PAY_DATE";
            this.PAY_DATE.ReadOnly = true;
            // 
            // RECEIVE_BY
            // 
            this.RECEIVE_BY.DataPropertyName = "RECEIVE_BY";
            resources.ApplyResources(this.RECEIVE_BY, "RECEIVE_BY");
            this.RECEIVE_BY.Name = "RECEIVE_BY";
            this.RECEIVE_BY.ReadOnly = true;
            // 
            // RESULT
            // 
            this.RESULT.DataPropertyName = "RESULT";
            resources.ApplyResources(this.RESULT, "RESULT");
            this.RESULT.Name = "RESULT";
            this.RESULT.ReadOnly = true;
            // 
            // ERROR
            // 
            this.ERROR.DataPropertyName = "ERROR";
            resources.ApplyResources(this.ERROR, "ERROR");
            this.ERROR.Name = "ERROR";
            this.ERROR.ReadOnly = true;
            // 
            // PAY_AMOUNT2
            // 
            this.PAY_AMOUNT2.DataPropertyName = "PAY_AMOUNT2";
            resources.ApplyResources(this.PAY_AMOUNT2, "PAY_AMOUNT2");
            this.PAY_AMOUNT2.Name = "PAY_AMOUNT2";
            this.PAY_AMOUNT2.ReadOnly = true;
            // 
            // DialogImportResult
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogImportResult";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInvoiceItem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnClose;
        private Label label1;
        private DataGridView dgvInvoiceItem;
        private ComboBox cboResult;
        private Label label2;
        private Label lblCount;
        private Label lblAmount;
        private Label label5;
        private Label label6;
        private Label label3;
        private Label label4;
        private DataGridViewTextBoxColumn INVOICE_ID;
        private DataGridViewTextBoxColumn CUSTOMER_CODE;
        private DataGridViewTextBoxColumn CUSTOMER_NAME;
        private DataGridViewTextBoxColumn DUE_AMOUNT;
        private DataGridViewTextBoxColumn PAY_AMOUNT;
        private DataGridViewTextBoxColumn PAY_DATE;
        private DataGridViewTextBoxColumn RECEIVE_BY;
        private DataGridViewTextBoxColumn RESULT;
        private DataGridViewTextBoxColumn ERROR;
        private DataGridViewTextBoxColumn PAY_AMOUNT2;
    }
}