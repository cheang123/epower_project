﻿using System.ComponentModel;
using System.Windows.Forms;

namespace EPower.Interface.BankPayment
{
    partial class FormEPowerClient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormEPowerClient));
            this.lblDOWNLOAD_PAYMENT_ = new System.Windows.Forms.Label();
            this.btnDOWNLOAD = new System.Windows.Forms.Button();
            this.lblInvoice = new System.Windows.Forms.Label();
            this.lblCustomer = new System.Windows.Forms.Label();
            this.btnUPDATE_CUSTOMER = new System.Windows.Forms.Button();
            this.grpDATA = new System.Windows.Forms.GroupBox();
            this.btnAREA_SETTING = new System.Windows.Forms.Button();
            this.chkRESEND_ALL_DATA = new System.Windows.Forms.CheckBox();
            this.grpNETWORK = new System.Windows.Forms.GroupBox();
            this.btnVPN_CONNECT = new System.Windows.Forms.Button();
            this.btnVPN_CHECK_STATUS = new System.Windows.Forms.Button();
            this.btnVPN_DISCONNECT = new System.Windows.Forms.Button();
            this.lblVPN_STATUS_ = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.grpDATA.SuspendLayout();
            this.grpNETWORK.SuspendLayout();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.grpNETWORK);
            this.content.Controls.Add(this.grpDATA);
            this.content.Size = new System.Drawing.Size(468, 295);
            this.content.Text = "\'CONTENT\'";
            this.content.Controls.SetChildIndex(this.grpDATA, 0);
            this.content.Controls.SetChildIndex(this.grpNETWORK, 0);
            // 
            // lblDOWNLOAD_PAYMENT_
            // 
            this.lblDOWNLOAD_PAYMENT_.AutoSize = true;
            this.lblDOWNLOAD_PAYMENT_.ForeColor = System.Drawing.Color.Gray;
            this.lblDOWNLOAD_PAYMENT_.Location = new System.Drawing.Point(207, 119);
            this.lblDOWNLOAD_PAYMENT_.Name = "lblDOWNLOAD_PAYMENT_";
            this.lblDOWNLOAD_PAYMENT_.Size = new System.Drawing.Size(94, 19);
            this.lblDOWNLOAD_PAYMENT_.TabIndex = 17;
            this.lblDOWNLOAD_PAYMENT_.Text = "កំពុងពិនិត្យមើល...";
            // 
            // btnDOWNLOAD
            // 
            this.btnDOWNLOAD.Location = new System.Drawing.Point(23, 114);
            this.btnDOWNLOAD.Name = "btnDOWNLOAD";
            this.btnDOWNLOAD.Size = new System.Drawing.Size(179, 28);
            this.btnDOWNLOAD.TabIndex = 15;
            this.btnDOWNLOAD.Text = "ទាញយកប្រាក់បង់ពីធនាគារ";
            this.btnDOWNLOAD.UseVisualStyleBackColor = true;
            this.btnDOWNLOAD.Click += new System.EventHandler(this.btnDownload_Click);
            // 
            // lblInvoice
            // 
            this.lblInvoice.AutoSize = true;
            this.lblInvoice.ForeColor = System.Drawing.Color.Gray;
            this.lblInvoice.Location = new System.Drawing.Point(159, 145);
            this.lblInvoice.Name = "lblInvoice";
            this.lblInvoice.Size = new System.Drawing.Size(0, 19);
            this.lblInvoice.TabIndex = 14;
            this.lblInvoice.Visible = false;
            // 
            // lblCustomer
            // 
            this.lblCustomer.AutoSize = true;
            this.lblCustomer.ForeColor = System.Drawing.Color.Gray;
            this.lblCustomer.Location = new System.Drawing.Point(216, 38);
            this.lblCustomer.Name = "lblCustomer";
            this.lblCustomer.Size = new System.Drawing.Size(0, 19);
            this.lblCustomer.TabIndex = 11;
            // 
            // btnUPDATE_CUSTOMER
            // 
            this.btnUPDATE_CUSTOMER.Location = new System.Drawing.Point(24, 33);
            this.btnUPDATE_CUSTOMER.Name = "btnUPDATE_CUSTOMER";
            this.btnUPDATE_CUSTOMER.Size = new System.Drawing.Size(179, 28);
            this.btnUPDATE_CUSTOMER.TabIndex = 9;
            this.btnUPDATE_CUSTOMER.Text = "បញ្ជូនអតិថិជនទៅធនាគារ";
            this.btnUPDATE_CUSTOMER.UseVisualStyleBackColor = true;
            this.btnUPDATE_CUSTOMER.Click += new System.EventHandler(this.btnUpdateCustomer_Click);
            this.btnUPDATE_CUSTOMER.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btnUPDATE_CUSTOMER_MouseClick);
            // 
            // grpDATA
            // 
            this.grpDATA.Controls.Add(this.btnAREA_SETTING);
            this.grpDATA.Controls.Add(this.chkRESEND_ALL_DATA);
            this.grpDATA.Controls.Add(this.btnDOWNLOAD);
            this.grpDATA.Controls.Add(this.lblDOWNLOAD_PAYMENT_);
            this.grpDATA.Controls.Add(this.btnUPDATE_CUSTOMER);
            this.grpDATA.Controls.Add(this.lblCustomer);
            this.grpDATA.Controls.Add(this.lblInvoice);
            this.grpDATA.Enabled = false;
            this.grpDATA.Location = new System.Drawing.Point(12, 6);
            this.grpDATA.Name = "grpDATA";
            this.grpDATA.Size = new System.Drawing.Size(444, 173);
            this.grpDATA.TabIndex = 18;
            this.grpDATA.TabStop = false;
            this.grpDATA.Text = "ទិន្នន័យ";
            // 
            // btnAREA_SETTING
            // 
            this.btnAREA_SETTING.Location = new System.Drawing.Point(328, 36);
            this.btnAREA_SETTING.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAREA_SETTING.Name = "btnAREA_SETTING";
            this.btnAREA_SETTING.Size = new System.Drawing.Size(110, 25);
            this.btnAREA_SETTING.TabIndex = 5;
            this.btnAREA_SETTING.Text = "កំណត់តំបន់";
            this.btnAREA_SETTING.UseVisualStyleBackColor = true;
            this.btnAREA_SETTING.Click += new System.EventHandler(this.btnAREA_SETTING_Click);
            // 
            // chkRESEND_ALL_DATA
            // 
            this.chkRESEND_ALL_DATA.AutoSize = true;
            this.chkRESEND_ALL_DATA.Location = new System.Drawing.Point(26, 62);
            this.chkRESEND_ALL_DATA.Name = "chkRESEND_ALL_DATA";
            this.chkRESEND_ALL_DATA.Size = new System.Drawing.Size(172, 23);
            this.chkRESEND_ALL_DATA.TabIndex = 18;
            this.chkRESEND_ALL_DATA.Text = "បញ្ជូនទិន្នន័យអតិថិជនទាំងអស់";
            this.chkRESEND_ALL_DATA.UseVisualStyleBackColor = true;
            // 
            // grpNETWORK
            // 
            this.grpNETWORK.Controls.Add(this.btnVPN_CONNECT);
            this.grpNETWORK.Controls.Add(this.btnVPN_CHECK_STATUS);
            this.grpNETWORK.Controls.Add(this.btnVPN_DISCONNECT);
            this.grpNETWORK.Controls.Add(this.lblVPN_STATUS_);
            this.grpNETWORK.Location = new System.Drawing.Point(12, 186);
            this.grpNETWORK.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grpNETWORK.Name = "grpNETWORK";
            this.grpNETWORK.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grpNETWORK.Size = new System.Drawing.Size(444, 92);
            this.grpNETWORK.TabIndex = 20;
            this.grpNETWORK.TabStop = false;
            this.grpNETWORK.Text = "បណ្តាញ";
            // 
            // btnVPN_CONNECT
            // 
            this.btnVPN_CONNECT.Location = new System.Drawing.Point(19, 31);
            this.btnVPN_CONNECT.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnVPN_CONNECT.Name = "btnVPN_CONNECT";
            this.btnVPN_CONNECT.Size = new System.Drawing.Size(87, 25);
            this.btnVPN_CONNECT.TabIndex = 1;
            this.btnVPN_CONNECT.Text = "ភ្ជាប់";
            this.btnVPN_CONNECT.UseVisualStyleBackColor = true;
            this.btnVPN_CONNECT.Click += new System.EventHandler(this.btnVpnConnect_Click);
            // 
            // btnVPN_CHECK_STATUS
            // 
            this.btnVPN_CHECK_STATUS.Location = new System.Drawing.Point(210, 31);
            this.btnVPN_CHECK_STATUS.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnVPN_CHECK_STATUS.Name = "btnVPN_CHECK_STATUS";
            this.btnVPN_CHECK_STATUS.Size = new System.Drawing.Size(120, 25);
            this.btnVPN_CHECK_STATUS.TabIndex = 4;
            this.btnVPN_CHECK_STATUS.Text = "ពិនិត្យស្ថានភាព";
            this.btnVPN_CHECK_STATUS.UseVisualStyleBackColor = true;
            this.btnVPN_CHECK_STATUS.Click += new System.EventHandler(this.btnVpnCheckStatus_Click);
            // 
            // btnVPN_DISCONNECT
            // 
            this.btnVPN_DISCONNECT.Location = new System.Drawing.Point(113, 31);
            this.btnVPN_DISCONNECT.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnVPN_DISCONNECT.Name = "btnVPN_DISCONNECT";
            this.btnVPN_DISCONNECT.Size = new System.Drawing.Size(90, 25);
            this.btnVPN_DISCONNECT.TabIndex = 3;
            this.btnVPN_DISCONNECT.Text = "ផ្តាច់";
            this.btnVPN_DISCONNECT.UseVisualStyleBackColor = true;
            this.btnVPN_DISCONNECT.Click += new System.EventHandler(this.btnVpnDisconnect_Click);
            // 
            // lblVPN_STATUS_
            // 
            this.lblVPN_STATUS_.AutoSize = true;
            this.lblVPN_STATUS_.Location = new System.Drawing.Point(19, 61);
            this.lblVPN_STATUS_.Name = "lblVPN_STATUS_";
            this.lblVPN_STATUS_.Size = new System.Drawing.Size(75, 19);
            this.lblVPN_STATUS_.TabIndex = 2;
            this.lblVPN_STATUS_.Text = "Connecting ...";
            // 
            // FormEPowerClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(468, 318);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormEPowerClient";
            this.ShowIcon = true;
            this.ShowInTaskbar = true;
            this.Text = "ការទូទាត់តាមធនាគារ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormEPowerClient_FormClosing);
            this.Load += new System.EventHandler(this.FormEPowerClient_Load);
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.grpDATA.ResumeLayout(false);
            this.grpDATA.PerformLayout();
            this.grpNETWORK.ResumeLayout(false);
            this.grpNETWORK.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Button btnDOWNLOAD;
        private Label lblDOWNLOAD_PAYMENT_;
        private GroupBox grpDATA;
        private Button btnUPDATE_CUSTOMER;
        private Label lblCustomer;
        private Label lblInvoice;
        private GroupBox grpNETWORK;
        private Button btnVPN_CONNECT;
        private Button btnVPN_CHECK_STATUS;
        private Button btnVPN_DISCONNECT;
        private Label lblVPN_STATUS_;
        private CheckBox chkRESEND_ALL_DATA;
        private Button btnAREA_SETTING;
    }
}

