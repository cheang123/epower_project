﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogSelectAreaToBank
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogSelectAreaToBank));
            this.dgv = new System.Windows.Forms.DataGridView();
            this.AREA_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SELECT_ = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.AREA_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AREA_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_CUSTOMER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.CHK_ALL = new System.Windows.Forms.CheckBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.CHK_ALL);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.dgv);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.dgv, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.CHK_ALL, 0);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AREA_ID,
            this.SELECT_,
            this.AREA_CODE,
            this.AREA_NAME,
            this.TOTAL_CUSTOMER});
            this.dgv.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellContentClick);
            // 
            // AREA_ID
            // 
            this.AREA_ID.DataPropertyName = "AREA_ID";
            resources.ApplyResources(this.AREA_ID, "AREA_ID");
            this.AREA_ID.Name = "AREA_ID";
            // 
            // SELECT_
            // 
            this.SELECT_.DataPropertyName = "SELECT_";
            this.SELECT_.FillWeight = 90F;
            resources.ApplyResources(this.SELECT_, "SELECT_");
            this.SELECT_.Name = "SELECT_";
            // 
            // AREA_CODE
            // 
            this.AREA_CODE.DataPropertyName = "AREA_CODE";
            resources.ApplyResources(this.AREA_CODE, "AREA_CODE");
            this.AREA_CODE.Name = "AREA_CODE";
            // 
            // AREA_NAME
            // 
            this.AREA_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AREA_NAME.DataPropertyName = "AREA_NAME";
            resources.ApplyResources(this.AREA_NAME, "AREA_NAME");
            this.AREA_NAME.Name = "AREA_NAME";
            // 
            // TOTAL_CUSTOMER
            // 
            this.TOTAL_CUSTOMER.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.TOTAL_CUSTOMER.DataPropertyName = "TOTAL_CUSTOMER";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.TOTAL_CUSTOMER.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.TOTAL_CUSTOMER, "TOTAL_CUSTOMER");
            this.TOTAL_CUSTOMER.Name = "TOTAL_CUSTOMER";
            this.TOTAL_CUSTOMER.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnCLOSE_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // CHK_ALL
            // 
            resources.ApplyResources(this.CHK_ALL, "CHK_ALL");
            this.CHK_ALL.Name = "CHK_ALL";
            this.CHK_ALL.UseVisualStyleBackColor = true;
            this.CHK_ALL.CheckedChanged += new System.EventHandler(this.CHK_ALL_CheckedChanged);
            // 
            // DialogSelectAreaToBank
            // 
            resources.ApplyResources(this, "$this");
            this.Name = "DialogSelectAreaToBank";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.DialogSelectAreaToBank_Load);
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataGridView dgv;
        private ExButton btnCHANGE_LOG;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private CheckBox CHK_ALL;
        private DataGridViewTextBoxColumn AREA_ID;
        private DataGridViewCheckBoxColumn SELECT_;
        private DataGridViewTextBoxColumn AREA_CODE;
        private DataGridViewTextBoxColumn AREA_NAME;
        private DataGridViewTextBoxColumn TOTAL_CUSTOMER;
    }
}
