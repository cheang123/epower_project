﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using Process = System.Diagnostics.Process;

namespace EPower.Interface.BankPayment
{
    public partial class DialogExport : ExDialog
    {
        public DialogExport()
        {
            InitializeComponent();
            UIHelper.SetDataSourceToComboBox(cboExportType, DBDataContext.Db.TBL_BANK_PAYMENT_TYPEs.Where(x => x.IS_ACTIVE));
            if (cboExportType.Items.Count == 1)
            {
                cboExportType.SelectedIndex = 0;
            }
        }

        private void ExportExcel()
        {
            // check for folder exists
            if (!Directory.Exists(this.txtFileName.Text))
            {
                MsgBox.ShowInformation(string.Format(Resources.MS_FOLDER_NOT_FOUND, this.txtFileName.Text));
                return;
            }

            string templateFileName = Application.StartupPath + "/Template/BankPayment.xls";
            string newFileName = this.txtFileName.Text + "/BankPayment.xls";
            if (!File.Exists(templateFileName))
            {
                MsgBox.ShowInformation(Resources.MS_BANK_PAYMENT_TEMPLATE_NOT_FOUND);
                return;
            }

            if (File.Exists(newFileName))
            {
                if (MsgBox.ShowQuestion(string.Format(Resources.MSQ_OVERRIDE_EXISTS_FILE, newFileName), this.Text) == DialogResult.No)
                {
                    return;
                }
            }

            bool success = false;

            Runner.RunNewThread(delegate()
            {
                File.Copy(templateFileName, newFileName, true);

                ExcelOledb xls = new ExcelOledb(newFileName);

                var invs = from i in DBDataContext.Db.TBL_INVOICEs
                           join c in DBDataContext.Db.TBL_CUSTOMERs on i.CUSTOMER_ID equals c.CUSTOMER_ID
                           join a in DBDataContext.Db.TBL_AREAs on c.AREA_ID equals a.AREA_ID
                           join x in DBDataContext.Db.TLKP_CURRENCies on i.CURRENCY_ID equals x.CURRENCY_ID
                           where i.INVOICE_STATUS == (int)InvoiceStatus.Open
                           select new
                           {
                               i.INVOICE_ID,
                               c.CUSTOMER_CODE,
                               CUSTOMER_NAME = c.LAST_NAME_KH + " " + c.FIRST_NAME_KH,
                               i.METER_CODE,
                               a.AREA_NAME,
                               i.INVOICE_MONTH,
                               i.START_USAGE,
                               i.END_USAGE,
                               i.TOTAL_USAGE,
                               TOTAL_AMOUNT = i.SETTLE_AMOUNT,
                               i.PAID_AMOUNT,
                               DUE_AMOUNT = i.SETTLE_AMOUNT - i.PAID_AMOUNT,
                               x.CURRENCY_ID,
                               x.CURRENCY_NAME
                           };

                foreach (var inv in invs)
                {
                    xls.ExecuteNoneQuery(@"INSERT INTO [TBL_PAYMENT](
                                            INVOICE_ID,CUSTOMER_CODE,CUSTOMER_NAME,
                                            METER_CODE,AREA_NAME,INVOICE_MONTH,
                                            START_USAGE,END_USAGE,TOTAL_USAGE,
                                            CURRENCY_ID,CURRENCY_NAME,
                                            TOTAL_AMOUNT,PAID_AMOUNT,DUE_AMOUNT)
                                          VALUES(
                                            {0},{1},{2},
                                            {3},{4},{5},
                                            {6},{7},{8},
                                            {9},{10},
                                            {11},{12},{13});",
                                 inv.INVOICE_ID, inv.CUSTOMER_CODE, inv.CUSTOMER_NAME,
                                 inv.METER_CODE, inv.AREA_NAME, inv.INVOICE_MONTH,
                                 inv.START_USAGE, inv.END_USAGE, inv.TOTAL_USAGE,
                                 inv.CURRENCY_ID,inv.CURRENCY_NAME,
                                 inv.TOTAL_AMOUNT, inv.PAID_AMOUNT, inv.DUE_AMOUNT);

                }

                xls.Dispose();

                success = true;

            });

            if (success)
            {
                MsgBox.ShowInformation(Resources.SUCCESS);


                try
                {
                    // try opening newfile after complete 
                    string programFiles = Environment.GetEnvironmentVariable("ProgramFiles");
                    const string excelRelativePath = @"Microsoft Office\Office12\excel.exe";
                    string excel = Path.Combine(programFiles, excelRelativePath);
                    ProcessStartInfo startInfo = new ProcessStartInfo(excel, newFileName);
                    Process.Start(startInfo);
                }
                catch { }

            }
        }

        private void ExportAcleda()
        {
            // check for folder exists
            if (!Directory.Exists(this.txtFileName.Text))
            {
                MsgBox.ShowInformation(string.Format(Resources.MS_FOLDER_NOT_FOUND, this.txtFileName.Text));
                return;
            }

            string newFileName = this.txtFileName.Text + "/ACLEDA.txt";
            string line = Settings.Default.BANK_PAYMENT_SYMBOL;
            if (File.Exists(newFileName))
            {
                if (MsgBox.ShowQuestion(string.Format(Resources.MSQ_OVERRIDE_EXISTS_FILE, newFileName), this.Text) == DialogResult.No)
                {
                    return;
                }
            }

            bool success = false;

            Runner.RunNewThread(delegate()
            {
                var invs = from i in DBDataContext.Db.TBL_INVOICEs
                           join c in DBDataContext.Db.TBL_CUSTOMERs on i.CUSTOMER_ID equals c.CUSTOMER_ID
                           join a in DBDataContext.Db.TBL_AREAs on c.AREA_ID equals a.AREA_ID
                           where i.INVOICE_STATUS == (int)InvoiceStatus.Open
                           select new
                           {
                               i.INVOICE_ID,
                               c.CUSTOMER_CODE,
                               CUSTOMER_NAME = c.LAST_NAME_KH + " " + c.FIRST_NAME_KH,
                               i.METER_CODE,
                               a.AREA_NAME,
                               i.INVOICE_MONTH,
                               i.START_USAGE,
                               i.END_USAGE,
                               i.TOTAL_USAGE,
                               TOTAL_AMOUNT = i.SETTLE_AMOUNT,
                               i.PAID_AMOUNT,
                               DUE_AMOUNT = i.SETTLE_AMOUNT - i.PAID_AMOUNT
                           };
                TLKP_CURRENCY objCurrency = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.IS_DEFAULT_CURRENCY);
                string Currency=objCurrency.CURRENCY_ID==1?"R":"US";
                using (StreamWriter file = new StreamWriter(newFileName))
                {
                    file.WriteLine(line + "Customer_ID" + line + "," + line + "Customer_Name" + line + "," + line + "Currency" + line);
                    foreach (var item in invs)
                    {
                        string text = line + item.CUSTOMER_CODE + line + "," + line + item.CUSTOMER_NAME + line + "," + line + Currency + line;
                        file.WriteLine(text);
                    }
                }

                success = true;

            });

            if (success)
            {
                MsgBox.ShowInformation(Resources.SUCCESS);
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            if (cboExportType.SelectedIndex == -1)
            {
                cboExportType.SetValidation(string.Format(Resources.REQUIRED, lblTYPE.Text));
                return;
            }
            this.ClearAllValidation();
            if ((int)cboExportType.SelectedValue == (int)BankPaymentTypes.EXCEL)
            {
                ExportExcel();
            }
            else if ((int)cboExportType.SelectedValue == (int)BankPaymentTypes.ACLEDA)
            {
                ExportAcleda();
            }
        } 

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        { 
            FolderBrowserDialog diag = new FolderBrowserDialog();
            if (diag.ShowDialog() == DialogResult.OK)
            {
                this.txtFileName.Text = diag.SelectedPath;
            }
        }
    }
}
