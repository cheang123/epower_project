﻿using EPower.Logic;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;

namespace EPower.Interface.BankPayment
{
    public partial class DialogB24LogDetailInvoice : ExDialog
    {
        int b24LogId = 0;
        public DialogB24LogDetailInvoice(int _b24LogId)
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            b24LogId = _b24LogId;
            load();
        }

        private void load()
        {
            var db = from b in DBDataContext.Db.TBL_B24_LOG_DETAILs
                     join i in DBDataContext.Db.TBL_INVOICEs on b.REF_ID equals i.INVOICE_ID
                     join c in DBDataContext.Db.TBL_CUSTOMERs on i.CUSTOMER_ID equals c.CUSTOMER_ID
                     join a in DBDataContext.Db.TBL_AREAs on c.AREA_ID equals a.AREA_ID
                     join cu in DBDataContext.Db.TLKP_CURRENCies on i.CURRENCY_ID equals cu.CURRENCY_ID
                     where b.IS_ACTIVE && (i.INVOICE_NO + " " + c.LAST_NAME_KH + " " + c.FIRST_NAME_KH + " " + a.AREA_NAME).ToUpper().Contains(txtSearch.Text.ToUpper())
                        && b.REF_TYPE_ID == (int)B24_REF_TYPE.INVOICE
                        && b.B24_LOG_ID == b24LogId
                     select new
                     {
                         b.B24_LOG_DETAIL_ID,
                         c.CUSTOMER_ID,
                         c.CUSTOMER_CODE,
                         CUSTOMER_NAME = c.LAST_NAME_KH + " " + c.FIRST_NAME_KH,
                         i.INVOICE_NO,
                         i.INVOICE_TITLE,
                         i.SETTLE_AMOUNT,
                         i.PAID_AMOUNT,
                         DUE_AMOUNT = i.SETTLE_AMOUNT - i.PAID_AMOUNT,
                         cu.CURRENCY_SING
                     };
            dgv.DataSource = db;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtSearch_QuickSearch(object sender, EventArgs e)
        {
            load();
        }
    }
}
