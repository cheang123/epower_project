﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.BankPayment
{
    partial class DialogHistoryDelete
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogHistoryDelete));
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.CUSTOMER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_ON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_BY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_SING_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NOTE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtpD2 = new System.Windows.Forms.DateTimePicker();
            this.dtpD1 = new System.Windows.Forms.DateTimePicker();
            this.txtSearch = new SoftTech.Component.ExTextbox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.dtpD2);
            this.content.Controls.Add(this.dtpD1);
            this.content.Controls.Add(this.txtSearch);
            this.content.Controls.Add(this.dgv);
            this.content.Controls.Add(this.btnCLOSE);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.dgv, 0);
            this.content.Controls.SetChildIndex(this.txtSearch, 0);
            this.content.Controls.SetChildIndex(this.dtpD1, 0);
            this.content.Controls.SetChildIndex(this.dtpD2, 0);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CUSTOMER_ID,
            this.CUSTOMER_CODE,
            this.CUSTOMER_NAME,
            this.CREATE_ON,
            this.CREATE_BY,
            this.CURRENCY_SING_,
            this.AMOUNT,
            this.NOTE});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv.DefaultCellStyle = dataGridViewCellStyle6;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // CUSTOMER_ID
            // 
            this.CUSTOMER_ID.DataPropertyName = "CUSTOMER_ID";
            resources.ApplyResources(this.CUSTOMER_ID, "CUSTOMER_ID");
            this.CUSTOMER_ID.Name = "CUSTOMER_ID";
            this.CUSTOMER_ID.ReadOnly = true;
            // 
            // CUSTOMER_CODE
            // 
            this.CUSTOMER_CODE.DataPropertyName = "CUSTOMER_CODE";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.CUSTOMER_CODE.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.CUSTOMER_CODE, "CUSTOMER_CODE");
            this.CUSTOMER_CODE.Name = "CUSTOMER_CODE";
            this.CUSTOMER_CODE.ReadOnly = true;
            // 
            // CUSTOMER_NAME
            // 
            this.CUSTOMER_NAME.DataPropertyName = "CUSTOMER_NAME";
            resources.ApplyResources(this.CUSTOMER_NAME, "CUSTOMER_NAME");
            this.CUSTOMER_NAME.Name = "CUSTOMER_NAME";
            this.CUSTOMER_NAME.ReadOnly = true;
            // 
            // CREATE_ON
            // 
            this.CREATE_ON.DataPropertyName = "CREATE_ON";
            dataGridViewCellStyle3.Format = "dd-MM-yyyy hh:mm tt";
            this.CREATE_ON.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.CREATE_ON, "CREATE_ON");
            this.CREATE_ON.Name = "CREATE_ON";
            this.CREATE_ON.ReadOnly = true;
            // 
            // CREATE_BY
            // 
            this.CREATE_BY.DataPropertyName = "CREATE_BY";
            resources.ApplyResources(this.CREATE_BY, "CREATE_BY");
            this.CREATE_BY.Name = "CREATE_BY";
            this.CREATE_BY.ReadOnly = true;
            // 
            // CURRENCY_SING_
            // 
            this.CURRENCY_SING_.DataPropertyName = "CURRENCY_SING";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.CURRENCY_SING_.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.CURRENCY_SING_, "CURRENCY_SING_");
            this.CURRENCY_SING_.Name = "CURRENCY_SING_";
            this.CURRENCY_SING_.ReadOnly = true;
            // 
            // AMOUNT
            // 
            this.AMOUNT.DataPropertyName = "AMOUNT";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "#,##0.####";
            dataGridViewCellStyle5.NullValue = null;
            this.AMOUNT.DefaultCellStyle = dataGridViewCellStyle5;
            resources.ApplyResources(this.AMOUNT, "AMOUNT");
            this.AMOUNT.Name = "AMOUNT";
            this.AMOUNT.ReadOnly = true;
            // 
            // NOTE
            // 
            this.NOTE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NOTE.DataPropertyName = "NOTE";
            resources.ApplyResources(this.NOTE, "NOTE");
            this.NOTE.Name = "NOTE";
            this.NOTE.ReadOnly = true;
            // 
            // dtpD2
            // 
            resources.ApplyResources(this.dtpD2, "dtpD2");
            this.dtpD2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpD2.Name = "dtpD2";
            // 
            // dtpD1
            // 
            resources.ApplyResources(this.dtpD1, "dtpD1");
            this.dtpD1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpD1.Name = "dtpD1";
            this.dtpD1.Enter += new System.EventHandler(this.dtpD1_Enter);
            // 
            // txtSearch
            // 
            this.txtSearch.BackColor = System.Drawing.Color.White;
            this.txtSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtSearch, "txtSearch");
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtSearch.QuickSearch += new System.EventHandler(this.txtSearch_QuickSearch);
            // 
            // DialogHistoryDelete
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogHistoryDelete";
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnCLOSE;
        private DateTimePicker dtpD2;
        private DateTimePicker dtpD1;
        private ExTextbox txtSearch;
        private DataGridView dgv;
        private DataGridViewTextBoxColumn CUSTOMER_ID;
        private DataGridViewTextBoxColumn CUSTOMER_CODE;
        private DataGridViewTextBoxColumn CUSTOMER_NAME;
        private DataGridViewTextBoxColumn CREATE_ON;
        private DataGridViewTextBoxColumn CREATE_BY;
        private DataGridViewTextBoxColumn CURRENCY_SING_;
        private DataGridViewTextBoxColumn AMOUNT;
        private DataGridViewTextBoxColumn NOTE;
    }
}