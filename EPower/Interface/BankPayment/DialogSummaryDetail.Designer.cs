﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.BankPayment
{
    partial class DialogSummaryDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogSummaryDetail));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnClose = new SoftTech.Component.ExButton();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.txtSearch = new SoftTech.Component.ExTextbox();
            this.dgvCurrencySummary = new System.Windows.Forms.DataGridView();
            this.CURRENCY_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_RECORD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_PAY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_PAID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblTOTAL = new System.Windows.Forms.Label();
            this.dgvResultSummary = new System.Windows.Forms.DataGridView();
            this.lblWITHDRAWAL = new System.Windows.Forms.Label();
            this.btnREPORT = new SoftTech.Component.ExButton();
            this.cboResult = new System.Windows.Forms.ComboBox();
            this.lblRESULT = new System.Windows.Forms.Label();
            this.btnREMOVE = new SoftTech.Component.ExButton();
            this.btnHISTORY_DELETE = new SoftTech.Component.ExButton();
            this.BANK_PAYMENT_DETAIL_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAY_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAID_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAY_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CASHIER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RESULT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RESULT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NOTE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TYPE_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAYMENT_METHOD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAYMENT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_ON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TYPE_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCurrencySummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResultSummary)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.btnHISTORY_DELETE);
            this.content.Controls.Add(this.btnREMOVE);
            this.content.Controls.Add(this.lblRESULT);
            this.content.Controls.Add(this.cboResult);
            this.content.Controls.Add(this.btnREPORT);
            this.content.Controls.Add(this.lblWITHDRAWAL);
            this.content.Controls.Add(this.dgvResultSummary);
            this.content.Controls.Add(this.lblTOTAL);
            this.content.Controls.Add(this.dgvCurrencySummary);
            this.content.Controls.Add(this.txtSearch);
            this.content.Controls.Add(this.dgv);
            this.content.Controls.Add(this.btnClose);
            resources.ApplyResources(this.content, "content");
            this.content.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.content_MouseDoubleClick);
            this.content.Controls.SetChildIndex(this.btnClose, 0);
            this.content.Controls.SetChildIndex(this.dgv, 0);
            this.content.Controls.SetChildIndex(this.txtSearch, 0);
            this.content.Controls.SetChildIndex(this.dgvCurrencySummary, 0);
            this.content.Controls.SetChildIndex(this.lblTOTAL, 0);
            this.content.Controls.SetChildIndex(this.dgvResultSummary, 0);
            this.content.Controls.SetChildIndex(this.lblWITHDRAWAL, 0);
            this.content.Controls.SetChildIndex(this.btnREPORT, 0);
            this.content.Controls.SetChildIndex(this.cboResult, 0);
            this.content.Controls.SetChildIndex(this.lblRESULT, 0);
            this.content.Controls.SetChildIndex(this.btnREMOVE, 0);
            this.content.Controls.SetChildIndex(this.btnHISTORY_DELETE, 0);
            // 
            // btnClose
            // 
            resources.ApplyResources(this.btnClose, "btnClose");
            this.btnClose.Name = "btnClose";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BANK_PAYMENT_DETAIL_ID,
            this.CUSTOMER_CODE,
            this.CUSTOMER_NAME,
            this.CURRENCY_,
            this.PAY_AMOUNT,
            this.PAID_AMOUNT,
            this.PAY_DATE,
            this.CASHIER,
            this.RESULT_ID,
            this.RESULT,
            this.NOTE,
            this.TYPE_1,
            this.PAYMENT_METHOD});
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.SelectionChanged += new System.EventHandler(this.dgv_SelectionChanged);
            this.dgv.Sorted += new System.EventHandler(this.dgv_Sorted);
            // 
            // txtSearch
            // 
            this.txtSearch.BackColor = System.Drawing.Color.White;
            this.txtSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtSearch, "txtSearch");
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtSearch.QuickSearch += new System.EventHandler(this.txtSearch_QuickSearch);
            // 
            // dgvCurrencySummary
            // 
            this.dgvCurrencySummary.AllowUserToAddRows = false;
            this.dgvCurrencySummary.AllowUserToDeleteRows = false;
            this.dgvCurrencySummary.AllowUserToResizeRows = false;
            this.dgvCurrencySummary.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgvCurrencySummary.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvCurrencySummary.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvCurrencySummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCurrencySummary.ColumnHeadersVisible = false;
            this.dgvCurrencySummary.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CURRENCY_CODE,
            this.TOTAL_RECORD,
            this.TOTAL_PAY,
            this.TOTAL_PAID});
            resources.ApplyResources(this.dgvCurrencySummary, "dgvCurrencySummary");
            this.dgvCurrencySummary.Name = "dgvCurrencySummary";
            this.dgvCurrencySummary.ReadOnly = true;
            this.dgvCurrencySummary.RowHeadersVisible = false;
            this.dgvCurrencySummary.RowTemplate.Height = 25;
            this.dgvCurrencySummary.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // CURRENCY_CODE
            // 
            this.CURRENCY_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.CURRENCY_CODE.DataPropertyName = "CURRENCY_CODE";
            resources.ApplyResources(this.CURRENCY_CODE, "CURRENCY_CODE");
            this.CURRENCY_CODE.Name = "CURRENCY_CODE";
            this.CURRENCY_CODE.ReadOnly = true;
            // 
            // TOTAL_RECORD
            // 
            this.TOTAL_RECORD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.TOTAL_RECORD.DataPropertyName = "TOTAL_RECORD";
            dataGridViewCellStyle4.Format = "#";
            this.TOTAL_RECORD.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.TOTAL_RECORD, "TOTAL_RECORD");
            this.TOTAL_RECORD.Name = "TOTAL_RECORD";
            this.TOTAL_RECORD.ReadOnly = true;
            // 
            // TOTAL_PAY
            // 
            this.TOTAL_PAY.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TOTAL_PAY.DataPropertyName = "TOTAL_PAY";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "#,##0.####";
            this.TOTAL_PAY.DefaultCellStyle = dataGridViewCellStyle5;
            resources.ApplyResources(this.TOTAL_PAY, "TOTAL_PAY");
            this.TOTAL_PAY.Name = "TOTAL_PAY";
            this.TOTAL_PAY.ReadOnly = true;
            // 
            // TOTAL_PAID
            // 
            this.TOTAL_PAID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TOTAL_PAID.DataPropertyName = "TOTAL_PAID";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "#,##0.####";
            this.TOTAL_PAID.DefaultCellStyle = dataGridViewCellStyle6;
            resources.ApplyResources(this.TOTAL_PAID, "TOTAL_PAID");
            this.TOTAL_PAID.Name = "TOTAL_PAID";
            this.TOTAL_PAID.ReadOnly = true;
            // 
            // lblTOTAL
            // 
            resources.ApplyResources(this.lblTOTAL, "lblTOTAL");
            this.lblTOTAL.Name = "lblTOTAL";
            // 
            // dgvResultSummary
            // 
            this.dgvResultSummary.AllowUserToAddRows = false;
            this.dgvResultSummary.AllowUserToDeleteRows = false;
            this.dgvResultSummary.AllowUserToResizeRows = false;
            this.dgvResultSummary.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgvResultSummary.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvResultSummary.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvResultSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvResultSummary.ColumnHeadersVisible = false;
            this.dgvResultSummary.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PAYMENT_ID,
            this.NO,
            this.AMOUNT,
            this.CREATE_ON,
            this.TYPE_NAME,
            this.TYPE});
            resources.ApplyResources(this.dgvResultSummary, "dgvResultSummary");
            this.dgvResultSummary.Name = "dgvResultSummary";
            this.dgvResultSummary.ReadOnly = true;
            this.dgvResultSummary.RowHeadersVisible = false;
            this.dgvResultSummary.RowTemplate.Height = 25;
            this.dgvResultSummary.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvResultSummary.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // lblWITHDRAWAL
            // 
            resources.ApplyResources(this.lblWITHDRAWAL, "lblWITHDRAWAL");
            this.lblWITHDRAWAL.Name = "lblWITHDRAWAL";
            // 
            // btnREPORT
            // 
            resources.ApplyResources(this.btnREPORT, "btnREPORT");
            this.btnREPORT.Name = "btnREPORT";
            this.btnREPORT.UseVisualStyleBackColor = true;
            this.btnREPORT.Click += new System.EventHandler(this.btnReport_Click);
            // 
            // cboResult
            // 
            this.cboResult.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboResult.FormattingEnabled = true;
            resources.ApplyResources(this.cboResult, "cboResult");
            this.cboResult.Name = "cboResult";
            this.cboResult.SelectedIndexChanged += new System.EventHandler(this.cboResult_SelectedIndexChanged);
            // 
            // lblRESULT
            // 
            resources.ApplyResources(this.lblRESULT, "lblRESULT");
            this.lblRESULT.Name = "lblRESULT";
            // 
            // btnREMOVE
            // 
            resources.ApplyResources(this.btnREMOVE, "btnREMOVE");
            this.btnREMOVE.Name = "btnREMOVE";
            this.btnREMOVE.UseVisualStyleBackColor = true;
            this.btnREMOVE.Click += new System.EventHandler(this.btnREMOVE_Click);
            // 
            // btnHISTORY_DELETE
            // 
            resources.ApplyResources(this.btnHISTORY_DELETE, "btnHISTORY_DELETE");
            this.btnHISTORY_DELETE.Name = "btnHISTORY_DELETE";
            this.btnHISTORY_DELETE.UseVisualStyleBackColor = true;
            this.btnHISTORY_DELETE.Click += new System.EventHandler(this.btnHISTORY_DELETE_Click);
            // 
            // BANK_PAYMENT_DETAIL_ID
            // 
            this.BANK_PAYMENT_DETAIL_ID.DataPropertyName = "BANK_PAYMENT_DETAIL_ID";
            resources.ApplyResources(this.BANK_PAYMENT_DETAIL_ID, "BANK_PAYMENT_DETAIL_ID");
            this.BANK_PAYMENT_DETAIL_ID.Name = "BANK_PAYMENT_DETAIL_ID";
            this.BANK_PAYMENT_DETAIL_ID.ReadOnly = true;
            // 
            // CUSTOMER_CODE
            // 
            this.CUSTOMER_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.CUSTOMER_CODE.DataPropertyName = "CUSTOMER_CODE";
            resources.ApplyResources(this.CUSTOMER_CODE, "CUSTOMER_CODE");
            this.CUSTOMER_CODE.Name = "CUSTOMER_CODE";
            this.CUSTOMER_CODE.ReadOnly = true;
            // 
            // CUSTOMER_NAME
            // 
            this.CUSTOMER_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CUSTOMER_NAME.DataPropertyName = "CUSTOMER_NAME";
            resources.ApplyResources(this.CUSTOMER_NAME, "CUSTOMER_NAME");
            this.CUSTOMER_NAME.Name = "CUSTOMER_NAME";
            this.CUSTOMER_NAME.ReadOnly = true;
            // 
            // CURRENCY_
            // 
            this.CURRENCY_.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.CURRENCY_.DataPropertyName = "CURRENCY";
            resources.ApplyResources(this.CURRENCY_, "CURRENCY_");
            this.CURRENCY_.Name = "CURRENCY_";
            this.CURRENCY_.ReadOnly = true;
            // 
            // PAY_AMOUNT
            // 
            this.PAY_AMOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.PAY_AMOUNT.DataPropertyName = "PAY_AMOUNT";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "#,##0.####";
            dataGridViewCellStyle8.NullValue = null;
            this.PAY_AMOUNT.DefaultCellStyle = dataGridViewCellStyle8;
            resources.ApplyResources(this.PAY_AMOUNT, "PAY_AMOUNT");
            this.PAY_AMOUNT.Name = "PAY_AMOUNT";
            this.PAY_AMOUNT.ReadOnly = true;
            // 
            // PAID_AMOUNT
            // 
            this.PAID_AMOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.PAID_AMOUNT.DataPropertyName = "PAID_AMOUNT";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.Format = "#,##0.####";
            this.PAID_AMOUNT.DefaultCellStyle = dataGridViewCellStyle9;
            resources.ApplyResources(this.PAID_AMOUNT, "PAID_AMOUNT");
            this.PAID_AMOUNT.Name = "PAID_AMOUNT";
            this.PAID_AMOUNT.ReadOnly = true;
            // 
            // PAY_DATE
            // 
            this.PAY_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.PAY_DATE.DataPropertyName = "PAY_DATE";
            dataGridViewCellStyle10.Format = "dd-MM-yyyy HH:mm:ss";
            this.PAY_DATE.DefaultCellStyle = dataGridViewCellStyle10;
            resources.ApplyResources(this.PAY_DATE, "PAY_DATE");
            this.PAY_DATE.Name = "PAY_DATE";
            this.PAY_DATE.ReadOnly = true;
            // 
            // CASHIER
            // 
            this.CASHIER.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.CASHIER.DataPropertyName = "CASHIER";
            resources.ApplyResources(this.CASHIER, "CASHIER");
            this.CASHIER.Name = "CASHIER";
            this.CASHIER.ReadOnly = true;
            // 
            // RESULT_ID
            // 
            this.RESULT_ID.DataPropertyName = "RESULT_ID";
            resources.ApplyResources(this.RESULT_ID, "RESULT_ID");
            this.RESULT_ID.Name = "RESULT_ID";
            this.RESULT_ID.ReadOnly = true;
            // 
            // RESULT
            // 
            this.RESULT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.RESULT.DataPropertyName = "RESULT_NAME";
            resources.ApplyResources(this.RESULT, "RESULT");
            this.RESULT.Name = "RESULT";
            this.RESULT.ReadOnly = true;
            // 
            // NOTE
            // 
            this.NOTE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.NOTE.DataPropertyName = "RESULT_NOTE";
            resources.ApplyResources(this.NOTE, "NOTE");
            this.NOTE.Name = "NOTE";
            this.NOTE.ReadOnly = true;
            // 
            // TYPE_1
            // 
            this.TYPE_1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.TYPE_1.DataPropertyName = "PAYMENT_TYPE";
            resources.ApplyResources(this.TYPE_1, "TYPE_1");
            this.TYPE_1.Name = "TYPE_1";
            this.TYPE_1.ReadOnly = true;
            // 
            // PAYMENT_METHOD
            // 
            this.PAYMENT_METHOD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.PAYMENT_METHOD.DataPropertyName = "PAYMENT_METHOD";
            resources.ApplyResources(this.PAYMENT_METHOD, "PAYMENT_METHOD");
            this.PAYMENT_METHOD.Name = "PAYMENT_METHOD";
            this.PAYMENT_METHOD.ReadOnly = true;
            // 
            // PAYMENT_ID
            // 
            this.PAYMENT_ID.DataPropertyName = "PAYMENT_ID";
            resources.ApplyResources(this.PAYMENT_ID, "PAYMENT_ID");
            this.PAYMENT_ID.Name = "PAYMENT_ID";
            this.PAYMENT_ID.ReadOnly = true;
            // 
            // NO
            // 
            this.NO.DataPropertyName = "PAYMENT_NO";
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Blue;
            this.NO.DefaultCellStyle = dataGridViewCellStyle1;
            resources.ApplyResources(this.NO, "NO");
            this.NO.Name = "NO";
            this.NO.ReadOnly = true;
            // 
            // AMOUNT
            // 
            this.AMOUNT.DataPropertyName = "PAY_AMOUNT";
            dataGridViewCellStyle2.Format = "#,##0.####";
            this.AMOUNT.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.AMOUNT, "AMOUNT");
            this.AMOUNT.Name = "AMOUNT";
            this.AMOUNT.ReadOnly = true;
            // 
            // CREATE_ON
            // 
            this.CREATE_ON.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CREATE_ON.DataPropertyName = "CREATE_ON";
            dataGridViewCellStyle3.Format = "dd-MM-yyyy HH:mm:ss";
            this.CREATE_ON.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.CREATE_ON, "CREATE_ON");
            this.CREATE_ON.Name = "CREATE_ON";
            this.CREATE_ON.ReadOnly = true;
            // 
            // TYPE_NAME
            // 
            this.TYPE_NAME.DataPropertyName = "TYPE_NAME";
            this.TYPE_NAME.FillWeight = 80F;
            resources.ApplyResources(this.TYPE_NAME, "TYPE_NAME");
            this.TYPE_NAME.Name = "TYPE_NAME";
            this.TYPE_NAME.ReadOnly = true;
            // 
            // TYPE
            // 
            this.TYPE.DataPropertyName = "TYPE";
            resources.ApplyResources(this.TYPE, "TYPE");
            this.TYPE.Name = "TYPE";
            this.TYPE.ReadOnly = true;
            // 
            // DialogSummaryDetail
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogSummaryDetail";
            this.Load += new System.EventHandler(this.DialogBankPaymentHistoryDetail_Load);
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCurrencySummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResultSummary)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnClose;
        private DataGridView dgv;
        private ExTextbox txtSearch;
        private DataGridView dgvCurrencySummary;
        private Label lblTOTAL;
        private Label lblWITHDRAWAL;
        private DataGridView dgvResultSummary;
        private ExButton btnREPORT;
        private ComboBox cboResult;
        private Label lblRESULT;
        private ExButton btnREMOVE;
        private ExButton btnHISTORY_DELETE;
        private DataGridViewTextBoxColumn CURRENCY_CODE;
        private DataGridViewTextBoxColumn TOTAL_RECORD;
        private DataGridViewTextBoxColumn TOTAL_PAY;
        private DataGridViewTextBoxColumn TOTAL_PAID;
        private DataGridViewTextBoxColumn BANK_PAYMENT_DETAIL_ID;
        private DataGridViewTextBoxColumn CUSTOMER_CODE;
        private DataGridViewTextBoxColumn CUSTOMER_NAME;
        private DataGridViewTextBoxColumn CURRENCY_;
        private DataGridViewTextBoxColumn PAY_AMOUNT;
        private DataGridViewTextBoxColumn PAID_AMOUNT;
        private DataGridViewTextBoxColumn PAY_DATE;
        private DataGridViewTextBoxColumn CASHIER;
        private DataGridViewTextBoxColumn RESULT_ID;
        private DataGridViewTextBoxColumn RESULT;
        private DataGridViewTextBoxColumn NOTE;
        private DataGridViewTextBoxColumn TYPE_1;
        private DataGridViewTextBoxColumn PAYMENT_METHOD;
        private DataGridViewTextBoxColumn PAYMENT_ID;
        private DataGridViewTextBoxColumn NO;
        private DataGridViewTextBoxColumn AMOUNT;
        private DataGridViewTextBoxColumn CREATE_ON;
        private DataGridViewTextBoxColumn TYPE_NAME;
        private DataGridViewTextBoxColumn TYPE;
    }
}