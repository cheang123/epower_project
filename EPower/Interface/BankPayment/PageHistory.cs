﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Process = System.Diagnostics.Process;

namespace EPower.Interface.BankPayment
{
    public partial class PageHistory : Form
    {

        #region Constructor
        DateTime now = DateTime.Now;
        public PageHistory()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            btnPENDING_PAYMENT.Visible =
            btnBANK_PAYMENT.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.BANK_PAYMENT_ENABLE]);
            dtpD1.Value = new DateTime(now.Year, now.Month, 1);
            dtpD2.Value = dtpD1.Value.AddMonths(1).AddSeconds(-1);
            
            this.dtpD1.ValueChanged += dtp_ValueChanged;
            this.dtpD2.ValueChanged += dtp_ValueChanged;
            this.txtSearch.QuickSearch += txt_QuickSearch;
        }
        #endregion 

        public void LoadData()
        {
            DataTable dt = new DataTable();
            Runner.RunNewThread(delegate ()
            {
                dt = (from b in DBDataContext.Db.TBL_BANK_PAYMENTs
                      join t in DBDataContext.Db.TBL_BANK_PAYMENT_TYPEs on b.BANK_PAYMENT_TYPE_ID equals t.BANK_PAYMENT_TYPE_ID
                      where b.CREATE_ON >= dtpD1.Value && b.CREATE_ON <= dtpD2.Value &&
                           (b.CREATE_BY + " " + b.NAME_FILE).ToLower().Contains(txtSearch.Text.ToLower())
                      select new
                      {
                          b.BANK_PAYMENT_ID,
                          b.CREATE_BY,
                          b.CREATE_ON,
                          b.TOTAL_RECORD,
                          t.BANK_PAYMENT_TYPE_NAME,
                          b.NAME_FILE
                      })._ToDataTable();
            });
            dgv.DataSource = dt;
        }

        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void txt_QuickSearch(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnDetail_Click(object sender, EventArgs e)
        {
            if (dgv.Rows.Count > 0)
            {
                showDetail();
            }
        }

        private void showDetail()
        {
            int id = (int)dgv.SelectedRows[0].Cells[BANK_PAYMENT_ID.Name].Value;
            new DialogHistoryDetail(id).Show();
        }

        private void dtp_ValueChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnGetFile_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgv.Rows.Count > 0)
                {
                    int id = (int)dgv.SelectedRows[0].Cells[BANK_PAYMENT_ID.Name].Value;
                    TBL_BANK_PAYMENT objBankPayment = DBDataContext.Db.TBL_BANK_PAYMENTs.FirstOrDefault(x => x.BANK_PAYMENT_ID == id);
                    string fileName = Path.Combine(Application.StartupPath + "\\TEMP", objBankPayment.NAME_FILE);
                    using (FileStream stream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.Read))
                    {
                        stream.Write(objBankPayment.DATA_FILE.ToArray(), 0, objBankPayment.DATA_FILE.Length);
                    }
                    // try opening newfile after complete  
                    ProcessStartInfo startInfo = new ProcessStartInfo(fileName);
                    Process.Start(startInfo);
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }

        }

        private void dgv_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (dgv.Rows.Count == 0)
            {
                return;
            }

            if (e.ColumnIndex == dgv.Columns[NAME_FILE.Name].Index)
            {
                Cursor = Cursors.Hand;
            }

        }

        private void dgv_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (dgv.Rows.Count == 0)
            {
                return;
            }
            if (e.ColumnIndex == dgv.Columns[NAME_FILE.Name].Index)
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dgv.Columns[NAME_FILE.Name].Index)
            {
                try
                {
                    if (dgv.Rows.Count > 0)
                    {
                        int id = (int)dgv.SelectedRows[0].Cells[BANK_PAYMENT_ID.Name].Value;
                        TBL_BANK_PAYMENT objBankPayment = DBDataContext.Db.TBL_BANK_PAYMENTs.FirstOrDefault(x => x.BANK_PAYMENT_ID == id);
                        string fileName = Path.Combine(Application.StartupPath + "\\TEMP", objBankPayment.NAME_FILE);
                        using (FileStream stream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.Read))
                        {
                            stream.Write(objBankPayment.DATA_FILE.ToArray(), 0, objBankPayment.DATA_FILE.Length);
                        }
                        // try opening newfile after complete  
                        ProcessStartInfo startInfo = new ProcessStartInfo(fileName);
                        Process.Start(startInfo);
                    }
                }
                catch (Exception ex)
                {
                    MsgBox.ShowError(ex);
                }
            }
        }

        private void btnBankPayment_Click(object sender, EventArgs e)
        {
            new FormEPowerClient().ShowDialog();
            LoadData();
        }

        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (this.dgv.SelectedRows.Count > 0)
            {
                showDetail();
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            new DialogExport().ShowDialog();
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            if (Login.CurrentCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_RECEIVE_PAYMENT, string.Empty) == DialogResult.Yes)
                    new DialogOpenCashDrawer().ShowDialog();
                if (Login.CurrentCashDrawer == null)
                    return;
                else
                {
                    new DialogImport().ShowDialog();
                    LoadData();
                }
            }
            else
            {
                new DialogImport().ShowDialog();
                LoadData();
            }
        }

        private void btnPendingPayment_Click(object sender, EventArgs e)
        {
            new DialogPendingPayment().ShowDialog();
        }

        private void btnEMAIL_TO_BANK_Click(object sender, EventArgs e)
        {
            new DialogEmailToBank().ShowDialog();
        }

        private void btnAcledaAgreement_Click(object sender, EventArgs e)
        {
            new DialogAcledaAgreement().ShowDialog();
        }
    }
}
