﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.BankPayment
{
    partial class DialogUserAgreement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogUserAgreement));
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCancel = new SoftTech.Component.ExButton();
            this.txtUserAgreeement = new System.Windows.Forms.RichTextBox();
            this.chkREMEMBER_MY_AGREE = new System.Windows.Forms.CheckBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.chkREMEMBER_MY_AGREE);
            this.content.Controls.Add(this.txtUserAgreeement);
            this.content.Controls.Add(this.btnCancel);
            this.content.Controls.Add(this.btnOK);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCancel, 0);
            this.content.Controls.SetChildIndex(this.txtUserAgreeement, 0);
            this.content.Controls.SetChildIndex(this.chkREMEMBER_MY_AGREE, 0);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            resources.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtUserAgreeement
            // 
            this.txtUserAgreeement.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.txtUserAgreeement, "txtUserAgreeement");
            this.txtUserAgreeement.Name = "txtUserAgreeement";
            this.txtUserAgreeement.ReadOnly = true;
            // 
            // chkREMEMBER_MY_AGREE
            // 
            resources.ApplyResources(this.chkREMEMBER_MY_AGREE, "chkREMEMBER_MY_AGREE");
            this.chkREMEMBER_MY_AGREE.Checked = true;
            this.chkREMEMBER_MY_AGREE.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkREMEMBER_MY_AGREE.Name = "chkREMEMBER_MY_AGREE";
            this.chkREMEMBER_MY_AGREE.UseVisualStyleBackColor = true;
            // 
            // DialogUserAgreement
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogUserAgreement";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnOK;
        private ExButton btnCancel;
        private CheckBox chkREMEMBER_MY_AGREE;
        private RichTextBox txtUserAgreeement;
    }
}