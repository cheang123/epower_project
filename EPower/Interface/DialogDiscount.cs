﻿using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;

namespace EPower.Interface
{
    public partial class DialogDiscount : ExDialog
    { 
        GeneralProcess _flag;
        TBL_DISCOUNT _objNew = new TBL_DISCOUNT();
        TBL_DISCOUNT _objOld = new TBL_DISCOUNT();
        public TBL_DISCOUNT Discount
        {
            get { return _objNew; }
        }

        #region Constructor
        public DialogDiscount(GeneralProcess flag, TBL_DISCOUNT objArea)
        {
            InitializeComponent();

            UIHelper.SetDataSourceToComboBox(cboDiscountType, DBDataContext.Db.TLKP_DISCOUNT_TYPEs);

            _flag = flag;
            objArea._CopyTo(_objNew);
            objArea._CopyTo(_objOld);

            if (flag == GeneralProcess.Insert)
            {
                _objNew.IS_ACTIVE = true;
                this.Text = string.Concat(Resources.INSERT, this.Text);
            }
            else if (flag == GeneralProcess.Update)
            {
                this.Text = string.Concat(Resources.UPDATE, this.Text);
            }
            else if (flag == GeneralProcess.Delete)
            {
                this.Text = string.Concat(Resources.DELETE, this.Text);
                UIHelper.SetEnabled(this, false);
            }

            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;

            read();
        }        
        #endregion

        #region Operation
        /// <summary>
        /// Change Keyboard to Khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        } 

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        /// <summary>
        /// Data transaction.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
             //If record is duplicate.
            txtName.ClearValidation();
            if (DBDataContext.Db.IsExits(_objNew, "DISCOUNT_NAME"))
            {
                txtName.SetValidation(string.Format( Resources.MS_IS_EXISTS,lblDISCOUNT_NAME.Text));
                return;
            }
            try
            {
                using (TransactionScope tran=new TransactionScope( TransactionScopeOption.Required,TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objNew);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objNew);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }                                               
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }            
        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }

        private void cboDiscountType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cboDiscountType.SelectedIndex != -1)
            {
                this.cboDiscountUnit.Items[0] = (int)this.cboDiscountType.SelectedValue == (int)DiscounType.DiscountUsage ? "kWh" : DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(c => c.IS_DEFAULT_CURRENCY).CURRENCY_NAME;
            }
        }

        private void txtDiscount_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        } 
        #endregion

        #region Method
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();

            if (txtName.Text.Trim().Length <= 0)
            {
                txtName.SetValidation(string.Format(Resources.REQUIRED, lblDISCOUNT_NAME.Text));
                val = true;
            }

            if (this.cboDiscountType.SelectedIndex == -1)
            {
                cboDiscountType.SetValidation(string.Format(Resources.REQUIRED, lblDISCOUNT_TYPE.Text));
                val = true;
            }

            if (!DataHelper.IsNumber(this.txtDiscount.Text))
            {
                txtDiscount.SetValidation(string.Format(Resources.REQUIRED, lblDISCOUNT.Text));
                val = true;
            }
            if (DataHelper.ParseToDecimal(txtDiscount.Text)<=0)
            {
                txtDiscount.SetValidation(string.Format(Resources.REQUEST_GRATER_THAN_ZERO, lblDISCOUNT.Text));
                val = true;
            }
            if (DataHelper.ParseToDecimal(txtDiscount.Text) > 100 && cboDiscountUnit.SelectedIndex == 1)
            {
                txtDiscount.SetValidation(Resources.REQUEST_MAXIMUM_DISCOUNT_ONLY_100_PERCENTAGE);
                val = true;
            }
            return val;
        }

        /// <summary>
        /// Read data from object.
        /// </summary>
        private void read()
        {
            txtName.Text = _objNew.DISCOUNT_NAME;

            if (this._objNew.IS_PERCENTAGE)
            {
                txtDiscount.Text = _objNew.DISCOUNT.ToString("N2");
            }
            else
            {
                txtDiscount.Text = _objNew.DISCOUNT_TYPE_ID == (int)DiscounType.DiscountAmount ? UIHelper.FormatCurrency(_objNew.DISCOUNT) : _objNew.DISCOUNT.ToString(UIHelper._DefaultUsageFormat);
            }

            cboDiscountType.SelectedValue = _objNew.DISCOUNT_TYPE_ID;
            cboDiscountUnit.SelectedIndex = _objNew.IS_PERCENTAGE ? 1 : 0;
            txtDescription.Text = _objNew.DESCRIPTION;
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            _objNew.DISCOUNT_NAME = txtName.Text;
            _objNew.DISCOUNT_TYPE_ID = (int)cboDiscountType.SelectedValue;
            _objNew.DISCOUNT = DataHelper.ParseToDecimal(txtDiscount.Text);
            _objNew.IS_PERCENTAGE = cboDiscountUnit.SelectedIndex == 1;
            _objNew.DESCRIPTION = txtDescription.Text;

        }
        #endregion

    }
}