﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogCustomerChangeMeter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCustomerChangeMeter));
            this.lblCONSTANT = new System.Windows.Forms.Label();
            this.txtOLD_METER_CONSTANT = new System.Windows.Forms.TextBox();
            this.txtOLD_METER_METER_TYPE = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.lblAMPARE = new System.Windows.Forms.Label();
            this.lblVOLTAGE = new System.Windows.Forms.Label();
            this.lblPHASE = new System.Windows.Forms.Label();
            this.lblCABLE_SHIELD = new System.Windows.Forms.Label();
            this.lblSHIELD = new System.Windows.Forms.Label();
            this.txtOLD_METER_AMPHERE = new System.Windows.Forms.TextBox();
            this.txtOLD_METER_VOLTAGE = new System.Windows.Forms.TextBox();
            this.txtOLD_METER_PHASE = new System.Windows.Forms.TextBox();
            this.lblMETER_CODE = new System.Windows.Forms.Label();
            this.lblOLD_METER = new System.Windows.Forms.Label();
            this.lblMETER_TYPE = new System.Windows.Forms.Label();
            this.cboOLD_METER_CABLE_SEAL = new System.Windows.Forms.ComboBox();
            this.cboOLD_METER_METER_SEAL = new System.Windows.Forms.ComboBox();
            this.txtOLD_METER = new SoftTech.Component.ExTextbox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblNEW_METER = new System.Windows.Forms.Label();
            this.lblCUSTOMER_INFORMATION = new System.Windows.Forms.Label();
            this.txtCUSTOMER_NAME = new System.Windows.Forms.TextBox();
            this.lblCUSTOMER_NAME = new System.Windows.Forms.Label();
            this.txtCUSTOMER_CODE = new System.Windows.Forms.TextBox();
            this.lblCUSTOMER_CODE = new System.Windows.Forms.Label();
            this.lblMETER_STATUS = new System.Windows.Forms.Label();
            this.cboOLD_METER_STATUS = new System.Windows.Forms.ComboBox();
            this.lblEND_USAGE_I = new System.Windows.Forms.Label();
            this.txtOLD_METER_USAGE = new System.Windows.Forms.TextBox();
            this.lblSTART_USAGE_I = new System.Windows.Forms.Label();
            this.txtNEW_METER_USAGE = new System.Windows.Forms.TextBox();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblCONSTANT_1 = new System.Windows.Forms.Label();
            this.txtNEW_METER_CONSTANT = new System.Windows.Forms.TextBox();
            this.txtNEW_METER_TYPE = new System.Windows.Forms.TextBox();
            this.lblAMPARE_2 = new System.Windows.Forms.Label();
            this.lblVOLTAGE_1 = new System.Windows.Forms.Label();
            this.lblPHASE_1 = new System.Windows.Forms.Label();
            this.lblCABLE_SHIELD_1 = new System.Windows.Forms.Label();
            this.lblSHIELD_1 = new System.Windows.Forms.Label();
            this.txtNEW_METER_AMPARE = new System.Windows.Forms.TextBox();
            this.txtNEW_METER_VOLTAGE = new System.Windows.Forms.TextBox();
            this.txtNEW_METER_PHASE = new System.Windows.Forms.TextBox();
            this.lblMETER_CODE_1 = new System.Windows.Forms.Label();
            this.lblMETER_TYPE_1 = new System.Windows.Forms.Label();
            this.cboNEW_METER_CABLE_SEAL = new System.Windows.Forms.ComboBox();
            this.cboNEW_METER_METER_SEAL = new System.Windows.Forms.ComboBox();
            this.txtNEW_METER = new SoftTech.Component.ExTextbox();
            this.lblCHANGE_DATE = new System.Windows.Forms.Label();
            this.dtpCHANGE_DATE = new System.Windows.Forms.DateTimePicker();
            this.lblSTART_USAGE = new System.Windows.Forms.Label();
            this.txtOLD_METER_LAST_USAGE = new System.Windows.Forms.TextBox();
            this.dtpOLD_METER_LAST_USAGE_DATE = new System.Windows.Forms.DateTimePicker();
            this.lblCOLLECT_DATE = new System.Windows.Forms.Label();
            this.chkIS_NEW_CYCLE = new System.Windows.Forms.CheckBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.lblREASON = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cboReason = new System.Windows.Forms.ComboBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.cboReason);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.lblREASON);
            this.content.Controls.Add(this.label3);
            this.content.Controls.Add(this.label32);
            this.content.Controls.Add(this.label26);
            this.content.Controls.Add(this.label25);
            this.content.Controls.Add(this.label24);
            this.content.Controls.Add(this.label23);
            this.content.Controls.Add(this.chkIS_NEW_CYCLE);
            this.content.Controls.Add(this.dtpOLD_METER_LAST_USAGE_DATE);
            this.content.Controls.Add(this.lblCOLLECT_DATE);
            this.content.Controls.Add(this.lblSTART_USAGE);
            this.content.Controls.Add(this.txtOLD_METER_LAST_USAGE);
            this.content.Controls.Add(this.dtpCHANGE_DATE);
            this.content.Controls.Add(this.lblCHANGE_DATE);
            this.content.Controls.Add(this.lblCONSTANT_1);
            this.content.Controls.Add(this.txtNEW_METER_CONSTANT);
            this.content.Controls.Add(this.txtNEW_METER_TYPE);
            this.content.Controls.Add(this.lblAMPARE_2);
            this.content.Controls.Add(this.lblVOLTAGE_1);
            this.content.Controls.Add(this.lblPHASE_1);
            this.content.Controls.Add(this.lblCABLE_SHIELD_1);
            this.content.Controls.Add(this.lblSHIELD_1);
            this.content.Controls.Add(this.txtNEW_METER_AMPARE);
            this.content.Controls.Add(this.txtNEW_METER_VOLTAGE);
            this.content.Controls.Add(this.txtNEW_METER_PHASE);
            this.content.Controls.Add(this.lblMETER_CODE_1);
            this.content.Controls.Add(this.lblMETER_TYPE_1);
            this.content.Controls.Add(this.cboNEW_METER_CABLE_SEAL);
            this.content.Controls.Add(this.cboNEW_METER_METER_SEAL);
            this.content.Controls.Add(this.txtNEW_METER);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.lblSTART_USAGE_I);
            this.content.Controls.Add(this.txtNEW_METER_USAGE);
            this.content.Controls.Add(this.lblEND_USAGE_I);
            this.content.Controls.Add(this.txtOLD_METER_USAGE);
            this.content.Controls.Add(this.lblMETER_STATUS);
            this.content.Controls.Add(this.cboOLD_METER_STATUS);
            this.content.Controls.Add(this.txtCUSTOMER_CODE);
            this.content.Controls.Add(this.lblCUSTOMER_CODE);
            this.content.Controls.Add(this.txtCUSTOMER_NAME);
            this.content.Controls.Add(this.lblCUSTOMER_NAME);
            this.content.Controls.Add(this.lblCUSTOMER_INFORMATION);
            this.content.Controls.Add(this.lblNEW_METER);
            this.content.Controls.Add(this.lblCONSTANT);
            this.content.Controls.Add(this.txtOLD_METER_CONSTANT);
            this.content.Controls.Add(this.txtOLD_METER_METER_TYPE);
            this.content.Controls.Add(this.label14);
            this.content.Controls.Add(this.lblAMPARE);
            this.content.Controls.Add(this.lblVOLTAGE);
            this.content.Controls.Add(this.lblPHASE);
            this.content.Controls.Add(this.lblCABLE_SHIELD);
            this.content.Controls.Add(this.lblSHIELD);
            this.content.Controls.Add(this.txtOLD_METER_AMPHERE);
            this.content.Controls.Add(this.txtOLD_METER_VOLTAGE);
            this.content.Controls.Add(this.txtOLD_METER_PHASE);
            this.content.Controls.Add(this.lblMETER_CODE);
            this.content.Controls.Add(this.lblOLD_METER);
            this.content.Controls.Add(this.lblMETER_TYPE);
            this.content.Controls.Add(this.cboOLD_METER_CABLE_SEAL);
            this.content.Controls.Add(this.cboOLD_METER_METER_SEAL);
            this.content.Controls.Add(this.txtOLD_METER);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.txtOLD_METER, 0);
            this.content.Controls.SetChildIndex(this.cboOLD_METER_METER_SEAL, 0);
            this.content.Controls.SetChildIndex(this.cboOLD_METER_CABLE_SEAL, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_TYPE, 0);
            this.content.Controls.SetChildIndex(this.lblOLD_METER, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_CODE, 0);
            this.content.Controls.SetChildIndex(this.txtOLD_METER_PHASE, 0);
            this.content.Controls.SetChildIndex(this.txtOLD_METER_VOLTAGE, 0);
            this.content.Controls.SetChildIndex(this.txtOLD_METER_AMPHERE, 0);
            this.content.Controls.SetChildIndex(this.lblSHIELD, 0);
            this.content.Controls.SetChildIndex(this.lblCABLE_SHIELD, 0);
            this.content.Controls.SetChildIndex(this.lblPHASE, 0);
            this.content.Controls.SetChildIndex(this.lblVOLTAGE, 0);
            this.content.Controls.SetChildIndex(this.lblAMPARE, 0);
            this.content.Controls.SetChildIndex(this.label14, 0);
            this.content.Controls.SetChildIndex(this.txtOLD_METER_METER_TYPE, 0);
            this.content.Controls.SetChildIndex(this.txtOLD_METER_CONSTANT, 0);
            this.content.Controls.SetChildIndex(this.lblCONSTANT, 0);
            this.content.Controls.SetChildIndex(this.lblNEW_METER, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_INFORMATION, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_NAME, 0);
            this.content.Controls.SetChildIndex(this.txtCUSTOMER_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_CODE, 0);
            this.content.Controls.SetChildIndex(this.txtCUSTOMER_CODE, 0);
            this.content.Controls.SetChildIndex(this.cboOLD_METER_STATUS, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_STATUS, 0);
            this.content.Controls.SetChildIndex(this.txtOLD_METER_USAGE, 0);
            this.content.Controls.SetChildIndex(this.lblEND_USAGE_I, 0);
            this.content.Controls.SetChildIndex(this.txtNEW_METER_USAGE, 0);
            this.content.Controls.SetChildIndex(this.lblSTART_USAGE_I, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.txtNEW_METER, 0);
            this.content.Controls.SetChildIndex(this.cboNEW_METER_METER_SEAL, 0);
            this.content.Controls.SetChildIndex(this.cboNEW_METER_CABLE_SEAL, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_TYPE_1, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_CODE_1, 0);
            this.content.Controls.SetChildIndex(this.txtNEW_METER_PHASE, 0);
            this.content.Controls.SetChildIndex(this.txtNEW_METER_VOLTAGE, 0);
            this.content.Controls.SetChildIndex(this.txtNEW_METER_AMPARE, 0);
            this.content.Controls.SetChildIndex(this.lblSHIELD_1, 0);
            this.content.Controls.SetChildIndex(this.lblCABLE_SHIELD_1, 0);
            this.content.Controls.SetChildIndex(this.lblPHASE_1, 0);
            this.content.Controls.SetChildIndex(this.lblVOLTAGE_1, 0);
            this.content.Controls.SetChildIndex(this.lblAMPARE_2, 0);
            this.content.Controls.SetChildIndex(this.txtNEW_METER_TYPE, 0);
            this.content.Controls.SetChildIndex(this.txtNEW_METER_CONSTANT, 0);
            this.content.Controls.SetChildIndex(this.lblCONSTANT_1, 0);
            this.content.Controls.SetChildIndex(this.lblCHANGE_DATE, 0);
            this.content.Controls.SetChildIndex(this.dtpCHANGE_DATE, 0);
            this.content.Controls.SetChildIndex(this.txtOLD_METER_LAST_USAGE, 0);
            this.content.Controls.SetChildIndex(this.lblSTART_USAGE, 0);
            this.content.Controls.SetChildIndex(this.lblCOLLECT_DATE, 0);
            this.content.Controls.SetChildIndex(this.dtpOLD_METER_LAST_USAGE_DATE, 0);
            this.content.Controls.SetChildIndex(this.chkIS_NEW_CYCLE, 0);
            this.content.Controls.SetChildIndex(this.label23, 0);
            this.content.Controls.SetChildIndex(this.label24, 0);
            this.content.Controls.SetChildIndex(this.label25, 0);
            this.content.Controls.SetChildIndex(this.label26, 0);
            this.content.Controls.SetChildIndex(this.label32, 0);
            this.content.Controls.SetChildIndex(this.label3, 0);
            this.content.Controls.SetChildIndex(this.lblREASON, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.cboReason, 0);
            // 
            // lblCONSTANT
            // 
            resources.ApplyResources(this.lblCONSTANT, "lblCONSTANT");
            this.lblCONSTANT.Name = "lblCONSTANT";
            // 
            // txtOLD_METER_CONSTANT
            // 
            resources.ApplyResources(this.txtOLD_METER_CONSTANT, "txtOLD_METER_CONSTANT");
            this.txtOLD_METER_CONSTANT.Name = "txtOLD_METER_CONSTANT";
            this.txtOLD_METER_CONSTANT.ReadOnly = true;
            this.txtOLD_METER_CONSTANT.TabStop = false;
            // 
            // txtOLD_METER_METER_TYPE
            // 
            resources.ApplyResources(this.txtOLD_METER_METER_TYPE, "txtOLD_METER_METER_TYPE");
            this.txtOLD_METER_METER_TYPE.Name = "txtOLD_METER_METER_TYPE";
            this.txtOLD_METER_METER_TYPE.ReadOnly = true;
            this.txtOLD_METER_METER_TYPE.TabStop = false;
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Name = "label14";
            // 
            // lblAMPARE
            // 
            resources.ApplyResources(this.lblAMPARE, "lblAMPARE");
            this.lblAMPARE.Name = "lblAMPARE";
            // 
            // lblVOLTAGE
            // 
            resources.ApplyResources(this.lblVOLTAGE, "lblVOLTAGE");
            this.lblVOLTAGE.Name = "lblVOLTAGE";
            // 
            // lblPHASE
            // 
            resources.ApplyResources(this.lblPHASE, "lblPHASE");
            this.lblPHASE.Name = "lblPHASE";
            // 
            // lblCABLE_SHIELD
            // 
            resources.ApplyResources(this.lblCABLE_SHIELD, "lblCABLE_SHIELD");
            this.lblCABLE_SHIELD.Name = "lblCABLE_SHIELD";
            // 
            // lblSHIELD
            // 
            resources.ApplyResources(this.lblSHIELD, "lblSHIELD");
            this.lblSHIELD.Name = "lblSHIELD";
            // 
            // txtOLD_METER_AMPHERE
            // 
            resources.ApplyResources(this.txtOLD_METER_AMPHERE, "txtOLD_METER_AMPHERE");
            this.txtOLD_METER_AMPHERE.Name = "txtOLD_METER_AMPHERE";
            this.txtOLD_METER_AMPHERE.ReadOnly = true;
            this.txtOLD_METER_AMPHERE.TabStop = false;
            // 
            // txtOLD_METER_VOLTAGE
            // 
            resources.ApplyResources(this.txtOLD_METER_VOLTAGE, "txtOLD_METER_VOLTAGE");
            this.txtOLD_METER_VOLTAGE.Name = "txtOLD_METER_VOLTAGE";
            this.txtOLD_METER_VOLTAGE.ReadOnly = true;
            this.txtOLD_METER_VOLTAGE.TabStop = false;
            // 
            // txtOLD_METER_PHASE
            // 
            resources.ApplyResources(this.txtOLD_METER_PHASE, "txtOLD_METER_PHASE");
            this.txtOLD_METER_PHASE.Name = "txtOLD_METER_PHASE";
            this.txtOLD_METER_PHASE.ReadOnly = true;
            this.txtOLD_METER_PHASE.TabStop = false;
            // 
            // lblMETER_CODE
            // 
            resources.ApplyResources(this.lblMETER_CODE, "lblMETER_CODE");
            this.lblMETER_CODE.Name = "lblMETER_CODE";
            // 
            // lblOLD_METER
            // 
            resources.ApplyResources(this.lblOLD_METER, "lblOLD_METER");
            this.lblOLD_METER.Name = "lblOLD_METER";
            // 
            // lblMETER_TYPE
            // 
            resources.ApplyResources(this.lblMETER_TYPE, "lblMETER_TYPE");
            this.lblMETER_TYPE.Name = "lblMETER_TYPE";
            // 
            // cboOLD_METER_CABLE_SEAL
            // 
            this.cboOLD_METER_CABLE_SEAL.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboOLD_METER_CABLE_SEAL, "cboOLD_METER_CABLE_SEAL");
            this.cboOLD_METER_CABLE_SEAL.FormattingEnabled = true;
            this.cboOLD_METER_CABLE_SEAL.Name = "cboOLD_METER_CABLE_SEAL";
            this.cboOLD_METER_CABLE_SEAL.TabStop = false;
            // 
            // cboOLD_METER_METER_SEAL
            // 
            this.cboOLD_METER_METER_SEAL.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboOLD_METER_METER_SEAL, "cboOLD_METER_METER_SEAL");
            this.cboOLD_METER_METER_SEAL.FormattingEnabled = true;
            this.cboOLD_METER_METER_SEAL.Name = "cboOLD_METER_METER_SEAL";
            this.cboOLD_METER_METER_SEAL.TabStop = false;
            // 
            // txtOLD_METER
            // 
            this.txtOLD_METER.BackColor = System.Drawing.Color.White;
            this.txtOLD_METER.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtOLD_METER, "txtOLD_METER");
            this.txtOLD_METER.Name = "txtOLD_METER";
            this.txtOLD_METER.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtOLD_METER.AdvanceSearch += new System.EventHandler(this.txtMeter_AdvanceSearch);
            this.txtOLD_METER.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Name = "label3";
            // 
            // lblNEW_METER
            // 
            resources.ApplyResources(this.lblNEW_METER, "lblNEW_METER");
            this.lblNEW_METER.Name = "lblNEW_METER";
            // 
            // lblCUSTOMER_INFORMATION
            // 
            resources.ApplyResources(this.lblCUSTOMER_INFORMATION, "lblCUSTOMER_INFORMATION");
            this.lblCUSTOMER_INFORMATION.Name = "lblCUSTOMER_INFORMATION";
            // 
            // txtCUSTOMER_NAME
            // 
            resources.ApplyResources(this.txtCUSTOMER_NAME, "txtCUSTOMER_NAME");
            this.txtCUSTOMER_NAME.Name = "txtCUSTOMER_NAME";
            this.txtCUSTOMER_NAME.ReadOnly = true;
            this.txtCUSTOMER_NAME.TabStop = false;
            // 
            // lblCUSTOMER_NAME
            // 
            resources.ApplyResources(this.lblCUSTOMER_NAME, "lblCUSTOMER_NAME");
            this.lblCUSTOMER_NAME.Name = "lblCUSTOMER_NAME";
            // 
            // txtCUSTOMER_CODE
            // 
            resources.ApplyResources(this.txtCUSTOMER_CODE, "txtCUSTOMER_CODE");
            this.txtCUSTOMER_CODE.Name = "txtCUSTOMER_CODE";
            this.txtCUSTOMER_CODE.ReadOnly = true;
            this.txtCUSTOMER_CODE.TabStop = false;
            // 
            // lblCUSTOMER_CODE
            // 
            resources.ApplyResources(this.lblCUSTOMER_CODE, "lblCUSTOMER_CODE");
            this.lblCUSTOMER_CODE.Name = "lblCUSTOMER_CODE";
            // 
            // lblMETER_STATUS
            // 
            resources.ApplyResources(this.lblMETER_STATUS, "lblMETER_STATUS");
            this.lblMETER_STATUS.Name = "lblMETER_STATUS";
            // 
            // cboOLD_METER_STATUS
            // 
            this.cboOLD_METER_STATUS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboOLD_METER_STATUS.FormattingEnabled = true;
            resources.ApplyResources(this.cboOLD_METER_STATUS, "cboOLD_METER_STATUS");
            this.cboOLD_METER_STATUS.Name = "cboOLD_METER_STATUS";
            // 
            // lblEND_USAGE_I
            // 
            resources.ApplyResources(this.lblEND_USAGE_I, "lblEND_USAGE_I");
            this.lblEND_USAGE_I.Name = "lblEND_USAGE_I";
            // 
            // txtOLD_METER_USAGE
            // 
            this.txtOLD_METER_USAGE.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.txtOLD_METER_USAGE, "txtOLD_METER_USAGE");
            this.txtOLD_METER_USAGE.Name = "txtOLD_METER_USAGE";
            this.txtOLD_METER_USAGE.Enter += new System.EventHandler(this.InputEnglish);
            this.txtOLD_METER_USAGE.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtOLD_METER_USAGE_KeyDown);
            // 
            // lblSTART_USAGE_I
            // 
            resources.ApplyResources(this.lblSTART_USAGE_I, "lblSTART_USAGE_I");
            this.lblSTART_USAGE_I.Name = "lblSTART_USAGE_I";
            // 
            // txtNEW_METER_USAGE
            // 
            this.txtNEW_METER_USAGE.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.txtNEW_METER_USAGE, "txtNEW_METER_USAGE");
            this.txtNEW_METER_USAGE.Name = "txtNEW_METER_USAGE";
            this.txtNEW_METER_USAGE.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // lblCONSTANT_1
            // 
            resources.ApplyResources(this.lblCONSTANT_1, "lblCONSTANT_1");
            this.lblCONSTANT_1.Name = "lblCONSTANT_1";
            // 
            // txtNEW_METER_CONSTANT
            // 
            resources.ApplyResources(this.txtNEW_METER_CONSTANT, "txtNEW_METER_CONSTANT");
            this.txtNEW_METER_CONSTANT.Name = "txtNEW_METER_CONSTANT";
            this.txtNEW_METER_CONSTANT.ReadOnly = true;
            this.txtNEW_METER_CONSTANT.TabStop = false;
            // 
            // txtNEW_METER_TYPE
            // 
            resources.ApplyResources(this.txtNEW_METER_TYPE, "txtNEW_METER_TYPE");
            this.txtNEW_METER_TYPE.Name = "txtNEW_METER_TYPE";
            this.txtNEW_METER_TYPE.ReadOnly = true;
            this.txtNEW_METER_TYPE.TabStop = false;
            // 
            // lblAMPARE_2
            // 
            resources.ApplyResources(this.lblAMPARE_2, "lblAMPARE_2");
            this.lblAMPARE_2.Name = "lblAMPARE_2";
            // 
            // lblVOLTAGE_1
            // 
            resources.ApplyResources(this.lblVOLTAGE_1, "lblVOLTAGE_1");
            this.lblVOLTAGE_1.Name = "lblVOLTAGE_1";
            // 
            // lblPHASE_1
            // 
            resources.ApplyResources(this.lblPHASE_1, "lblPHASE_1");
            this.lblPHASE_1.Name = "lblPHASE_1";
            // 
            // lblCABLE_SHIELD_1
            // 
            resources.ApplyResources(this.lblCABLE_SHIELD_1, "lblCABLE_SHIELD_1");
            this.lblCABLE_SHIELD_1.Name = "lblCABLE_SHIELD_1";
            // 
            // lblSHIELD_1
            // 
            resources.ApplyResources(this.lblSHIELD_1, "lblSHIELD_1");
            this.lblSHIELD_1.Name = "lblSHIELD_1";
            // 
            // txtNEW_METER_AMPARE
            // 
            resources.ApplyResources(this.txtNEW_METER_AMPARE, "txtNEW_METER_AMPARE");
            this.txtNEW_METER_AMPARE.Name = "txtNEW_METER_AMPARE";
            this.txtNEW_METER_AMPARE.ReadOnly = true;
            this.txtNEW_METER_AMPARE.TabStop = false;
            // 
            // txtNEW_METER_VOLTAGE
            // 
            resources.ApplyResources(this.txtNEW_METER_VOLTAGE, "txtNEW_METER_VOLTAGE");
            this.txtNEW_METER_VOLTAGE.Name = "txtNEW_METER_VOLTAGE";
            this.txtNEW_METER_VOLTAGE.ReadOnly = true;
            this.txtNEW_METER_VOLTAGE.TabStop = false;
            // 
            // txtNEW_METER_PHASE
            // 
            resources.ApplyResources(this.txtNEW_METER_PHASE, "txtNEW_METER_PHASE");
            this.txtNEW_METER_PHASE.Name = "txtNEW_METER_PHASE";
            this.txtNEW_METER_PHASE.ReadOnly = true;
            this.txtNEW_METER_PHASE.TabStop = false;
            // 
            // lblMETER_CODE_1
            // 
            resources.ApplyResources(this.lblMETER_CODE_1, "lblMETER_CODE_1");
            this.lblMETER_CODE_1.Name = "lblMETER_CODE_1";
            // 
            // lblMETER_TYPE_1
            // 
            resources.ApplyResources(this.lblMETER_TYPE_1, "lblMETER_TYPE_1");
            this.lblMETER_TYPE_1.Name = "lblMETER_TYPE_1";
            // 
            // cboNEW_METER_CABLE_SEAL
            // 
            this.cboNEW_METER_CABLE_SEAL.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboNEW_METER_CABLE_SEAL.FormattingEnabled = true;
            resources.ApplyResources(this.cboNEW_METER_CABLE_SEAL, "cboNEW_METER_CABLE_SEAL");
            this.cboNEW_METER_CABLE_SEAL.Name = "cboNEW_METER_CABLE_SEAL";
            // 
            // cboNEW_METER_METER_SEAL
            // 
            this.cboNEW_METER_METER_SEAL.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboNEW_METER_METER_SEAL.FormattingEnabled = true;
            resources.ApplyResources(this.cboNEW_METER_METER_SEAL, "cboNEW_METER_METER_SEAL");
            this.cboNEW_METER_METER_SEAL.Name = "cboNEW_METER_METER_SEAL";
            // 
            // txtNEW_METER
            // 
            this.txtNEW_METER.BackColor = System.Drawing.Color.White;
            this.txtNEW_METER.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtNEW_METER, "txtNEW_METER");
            this.txtNEW_METER.Name = "txtNEW_METER";
            this.txtNEW_METER.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtNEW_METER.AdvanceSearch += new System.EventHandler(this.txtNEW_METER_AdvanceSearch);
            this.txtNEW_METER.CancelAdvanceSearch += new System.EventHandler(this.txtNEW_METER_CancelAdvanceSearch);
            // 
            // lblCHANGE_DATE
            // 
            resources.ApplyResources(this.lblCHANGE_DATE, "lblCHANGE_DATE");
            this.lblCHANGE_DATE.Name = "lblCHANGE_DATE";
            // 
            // dtpCHANGE_DATE
            // 
            resources.ApplyResources(this.dtpCHANGE_DATE, "dtpCHANGE_DATE");
            this.dtpCHANGE_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpCHANGE_DATE.Name = "dtpCHANGE_DATE";
            this.dtpCHANGE_DATE.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // lblSTART_USAGE
            // 
            resources.ApplyResources(this.lblSTART_USAGE, "lblSTART_USAGE");
            this.lblSTART_USAGE.Name = "lblSTART_USAGE";
            // 
            // txtOLD_METER_LAST_USAGE
            // 
            this.txtOLD_METER_LAST_USAGE.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.txtOLD_METER_LAST_USAGE, "txtOLD_METER_LAST_USAGE");
            this.txtOLD_METER_LAST_USAGE.Name = "txtOLD_METER_LAST_USAGE";
            this.txtOLD_METER_LAST_USAGE.TabStop = false;
            this.txtOLD_METER_LAST_USAGE.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // dtpOLD_METER_LAST_USAGE_DATE
            // 
            resources.ApplyResources(this.dtpOLD_METER_LAST_USAGE_DATE, "dtpOLD_METER_LAST_USAGE_DATE");
            this.dtpOLD_METER_LAST_USAGE_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpOLD_METER_LAST_USAGE_DATE.Name = "dtpOLD_METER_LAST_USAGE_DATE";
            this.dtpOLD_METER_LAST_USAGE_DATE.TabStop = false;
            this.dtpOLD_METER_LAST_USAGE_DATE.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // lblCOLLECT_DATE
            // 
            resources.ApplyResources(this.lblCOLLECT_DATE, "lblCOLLECT_DATE");
            this.lblCOLLECT_DATE.Name = "lblCOLLECT_DATE";
            // 
            // chkIS_NEW_CYCLE
            // 
            resources.ApplyResources(this.chkIS_NEW_CYCLE, "chkIS_NEW_CYCLE");
            this.chkIS_NEW_CYCLE.Name = "chkIS_NEW_CYCLE";
            this.chkIS_NEW_CYCLE.UseVisualStyleBackColor = true;
            // 
            // label23
            // 
            resources.ApplyResources(this.label23, "label23");
            this.label23.ForeColor = System.Drawing.Color.Red;
            this.label23.Name = "label23";
            // 
            // label24
            // 
            resources.ApplyResources(this.label24, "label24");
            this.label24.ForeColor = System.Drawing.Color.Red;
            this.label24.Name = "label24";
            // 
            // label25
            // 
            resources.ApplyResources(this.label25, "label25");
            this.label25.ForeColor = System.Drawing.Color.Red;
            this.label25.Name = "label25";
            // 
            // label26
            // 
            resources.ApplyResources(this.label26, "label26");
            this.label26.ForeColor = System.Drawing.Color.Red;
            this.label26.Name = "label26";
            // 
            // label32
            // 
            resources.ApplyResources(this.label32, "label32");
            this.label32.ForeColor = System.Drawing.Color.Red;
            this.label32.Name = "label32";
            // 
            // lblREASON
            // 
            resources.ApplyResources(this.lblREASON, "lblREASON");
            this.lblREASON.Name = "lblREASON";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // cboReason
            // 
            this.cboReason.DropDownHeight = 150;
            this.cboReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboReason.DropDownWidth = 300;
            this.cboReason.FormattingEnabled = true;
            resources.ApplyResources(this.cboReason, "cboReason");
            this.cboReason.Name = "cboReason";
            // 
            // DialogCustomerChangeMeter
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogCustomerChangeMeter";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBox txtCUSTOMER_CODE;
        private Label lblCUSTOMER_CODE;
        private TextBox txtCUSTOMER_NAME;
        private Label lblCUSTOMER_NAME;
        private Label lblCUSTOMER_INFORMATION;
        private Label label3;
        private Label lblNEW_METER;
        private Label lblCONSTANT;
        private TextBox txtOLD_METER_CONSTANT;
        private TextBox txtOLD_METER_METER_TYPE;
        private Label label14;
        private Label lblAMPARE;
        private Label lblVOLTAGE;
        private Label lblPHASE;
        private Label lblCABLE_SHIELD;
        private Label lblSHIELD;
        private TextBox txtOLD_METER_AMPHERE;
        private TextBox txtOLD_METER_VOLTAGE;
        private TextBox txtOLD_METER_PHASE;
        private Label lblMETER_CODE;
        private Label lblOLD_METER;
        private Label lblMETER_TYPE;
        private ComboBox cboOLD_METER_CABLE_SEAL;
        private ComboBox cboOLD_METER_METER_SEAL;
        private ExTextbox txtOLD_METER;
        private Label lblMETER_STATUS;
        private ComboBox cboOLD_METER_STATUS;
        private Label lblSTART_USAGE_I;
        private TextBox txtNEW_METER_USAGE;
        private Label lblEND_USAGE_I;
        private TextBox txtOLD_METER_USAGE;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Panel panel1;
        private Label lblCHANGE_DATE;
        private Label lblCONSTANT_1;
        private TextBox txtNEW_METER_CONSTANT;
        private TextBox txtNEW_METER_TYPE;
        private Label lblAMPARE_2;
        private Label lblVOLTAGE_1;
        private Label lblPHASE_1;
        private Label lblCABLE_SHIELD_1;
        private Label lblSHIELD_1;
        private TextBox txtNEW_METER_AMPARE;
        private TextBox txtNEW_METER_VOLTAGE;
        private TextBox txtNEW_METER_PHASE;
        private Label lblMETER_CODE_1;
        private Label lblMETER_TYPE_1;
        private ComboBox cboNEW_METER_CABLE_SEAL;
        private ComboBox cboNEW_METER_METER_SEAL;
        private ExTextbox txtNEW_METER;
        private DateTimePicker dtpCHANGE_DATE;
        private DateTimePicker dtpOLD_METER_LAST_USAGE_DATE;
        private Label lblCOLLECT_DATE;
        private Label lblSTART_USAGE;
        private TextBox txtOLD_METER_LAST_USAGE;
        private CheckBox chkIS_NEW_CYCLE;
        private Label label24;
        private Label label23;
        private Label label25;
        private Label label32;
        private Label label26;
        private Label lblREASON;
        private Label label1;
        private ComboBox cboReason;
    }
}