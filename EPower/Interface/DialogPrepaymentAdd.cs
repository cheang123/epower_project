﻿using EPower.Base.Helper;
using EPower.Base.Logic;
using EPower.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogPrepaymentAdd : ExDialog
    {
        #region Data
        private PrepaymentAction _PrepaymentAction = PrepaymentAction.AddPrepayment;
        private TBL_CUSTOMER _objCustomer = new TBL_CUSTOMER();
        private TBL_CUS_PREPAYMENT _objCusPrepayment = new TBL_CUS_PREPAYMENT();
        //private TBL_CUS_PREPAYMENT _objOldCusPrepayment = new TBL_CUS_PREPAYMENT();
        int _intCurrencyID = 0;
        GeneralProcess _Flag = GeneralProcess.Insert;
        #endregion Data

        #region Constructor
        public DialogPrepaymentAdd(TBL_CUSTOMER objCus, PrepaymentAction prepaymentAction, GeneralProcess Flag, int curId = 1)
        {
            InitializeComponent();

            //// SET PERMISSION
            //this.dtpDepositDate.Visible =
            ////this.lblDepositDatePoint.Visible =
            //this.lblDATE.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_DEPOSIT_DATE]);
            //this.dtpDepositDate.Enabled = SoftTech.Security.Logic.Login.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_REGISTER_UPDATE_DEPOSIT_DATE);

            bindCurrency();
            cboCurrency.SelectedValue = curId;
            this.chkPRINT_RECEIPT.Checked = Settings.Default.PRINT_DEPOSIT;
            objCus._CopyTo(_objCustomer);
            _PrepaymentAction = prepaymentAction;
            _Flag = Flag;
            read();

        }
        #endregion Constructor

        #region Method

        private void bindCurrency()
        {
            UIHelper.SetDataSourceToComboBox(cboCurrency, DBDataContext.Db.TLKP_CURRENCies.Select(x => new { x.CURRENCY_ID, x.CURRENCY_NAME }));
            cboCurrency.Enabled = false;
        }

        private void read()
        {
            var objCus = (from c in DBDataContext.Db.TBL_CUSTOMERs
                          join a in DBDataContext.Db.TBL_AMPAREs on c.AMP_ID equals a.AMPARE_ID
                          join p in DBDataContext.Db.TBL_PHASEs on c.PHASE_ID equals p.PHASE_ID
                          join ct in DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs on c.CUSTOMER_CONNECTION_TYPE_ID equals ct.CUSTOMER_CONNECTION_TYPE_ID
                          join pr in DBDataContext.Db.TBL_PRICEs on c.PRICE_ID equals pr.PRICE_ID
                          join ar in DBDataContext.Db.TBL_AREAs on c.AREA_ID equals ar.AREA_ID
                          where c.CUSTOMER_ID == _objCustomer.CUSTOMER_ID
                          select new
                          {
                              c.LAST_NAME_KH,
                              c.FIRST_NAME_KH,
                              a.AMPARE_NAME,
                              p.PHASE_NAME,
                              pr.PRICE_NAME,
                              ct.CUSTOMER_CONNECTION_TYPE_NAME,
                              pr.CURRENCY_ID,
                              ar.AREA_NAME
                          }).FirstOrDefault();


            if (objCus != null)
            {
                txtCustomerName.Text = objCus.LAST_NAME_KH + " " + objCus.FIRST_NAME_KH;
                txtCustomerType.Text = objCus.CUSTOMER_CONNECTION_TYPE_NAME;
                _intCurrencyID = objCus.CURRENCY_ID;
                txtAREA.Text = objCus.AREA_NAME;
                loadBalancePrepayment();
            }

            // Account 
            new AccountChartPopulator().PopluateTree(cboPaymentAccount.TreeView, AccountChartHelper.GetAccounts(AccountConfig.PAYMENT_ACCOUNTS));
            cboPaymentAccount.TreeView.SelectedNode = cboPaymentAccount.TreeView.GetNodesIncludeAncestors().FirstOrDefault(x => (int)x.Tag == AccountChartHelper.GetAccounts(AccountConfig.PAYMENT_DEFAULT_ACCOUNTS).FirstOrDefault().ACCOUNT_ID);
            cboPaymentAccount.Text = cboPaymentAccount.TreeView.SelectedNode != null ? cboPaymentAccount.TreeView.SelectedNode.Text : "";
        }

        private void loadBalancePrepayment()
        {
            if (cboCurrency.SelectedIndex == -1)
            {
                return;
            }

            int nCurrency = (int)cboCurrency.SelectedValue;

            //Display customer's currently deposit balance
            decimal decLastBalance = new PrepaymentLogic().GetCustomerPrepayment(_objCustomer.CUSTOMER_ID, nCurrency);
            txtCurrentBalance.Text = UIHelper.FormatCurrency(decLastBalance, nCurrency);
            txtAdjustAmount_TextChanged(txtAdjustAmount, EventArgs.Empty);
        }

        private bool invalid()
        {
            bool blnReturn = false;
            this.ClearAllValidation();

            if (cboCurrency.SelectedIndex == -1)
            {
                cboCurrency.SetValidation(string.Format(Properties.Resources.REQUIRED, lblCURRENCY.Text));
                return true;
            }

            if (txtAdjustAmount.Text == string.Empty)
            {
                txtAdjustAmount.SetValidation(string.Format(Properties.Resources.REQUIRED_INPUT_NUMBER, lblAMOUNT.Text));
                blnReturn = true;
            }

            if (UIHelper.Round(DataHelper.ParseToDecimal(txtLastBalance.Text), (int)cboCurrency.SelectedValue) < 0)
            {
                txtAdjustAmount.SetValidation(string.Format(Properties.Resources.MSG_PLEASE_CHECK_AGAIN, lblAMOUNT.Text));
                blnReturn = true;
            }

            if (_PrepaymentAction == PrepaymentAction.RefundPrepayment)
            {
                if (UIHelper.Round(DataHelper.ParseToDecimal(txtAdjustAmount.Text.Trim()), (int)cboCurrency.SelectedValue) <= 0)
                {
                    txtAdjustAmount.SetValidation(string.Format(Resources.MSG_PLEASE_CHECK_AGAIN, lblAMOUNT.Text));
                    blnReturn = true;
                }
            }

            if (_PrepaymentAction == PrepaymentAction.AddPrepayment)
            {
                if (UIHelper.Round(DataHelper.ParseToDecimal(txtAdjustAmount.Text.Trim()), (int)cboCurrency.SelectedValue) < 0)
                {
                    txtAdjustAmount.SetValidation(string.Format(Resources.MSG_PLEASE_CHECK_AGAIN, lblAMOUNT.Text));
                    blnReturn = true;
                }
                if (UIHelper.Round(DataHelper.ParseToDecimal(txtAdjustAmount.Text.Trim()), (int)cboCurrency.SelectedValue) <= 0)
                {
                    txtAdjustAmount.SetValidation(string.Format(Properties.Resources.MSG_PLEASE_CHECK_AGAIN, lblAMOUNT.Text));
                    blnReturn = true;
                }
            }
            return blnReturn;
        }

        #endregion Method

        #region Event

        private void InputEnglish(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void InputKhmer(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void txtDecimalOnly(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (Login.CurrentCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Properties.Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_RECEIVE_PAYMENT, string.Empty) == DialogResult.Yes)
                {
                    new DialogOpenCashDrawer().ShowDialog();
                }
                return;
            }
            if (invalid())
            {
                return;
            }
            //try
            //{
            using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
            {
                //DateTime datNow = dtpDepositDate.Value;
                decimal decAdjustAmount = UIHelper.Round(DataHelper.ParseToDecimal(txtAdjustAmount.Text), (int)cboCurrency.SelectedValue);
                decimal refundAmount = UIHelper.Round(DataHelper.ParseToDecimal(txtAdjustAmount.Text), (int)cboCurrency.SelectedValue);
                if (_Flag == GeneralProcess.Insert)
                {
                    //if the deposit is refund 
                    DateTime now = DBDataContext.Db.GetSystemDate();
                    var payablePrepayment = DBDataContext.Db.TBL_CUS_PREPAYMENTs.Where(x => x.CUSTOMER_ID == _objCustomer.CUSTOMER_ID && x.CURRENCY_ID == (int)cboCurrency.SelectedValue && x.OUT_STANDAING_BALANCE > 0).ToList();
                    //prevent null object reference not set. 
                    payablePrepayment = payablePrepayment ?? new List<TBL_CUS_PREPAYMENT>();

                    //history prepayment 
                    var histories = new List<TBL_APPLY_PAYMENT_HISTORY>();

                    //FIRST IN FIRST SETTLE ...
                    payablePrepayment = payablePrepayment.OrderBy(x => x.CUS_PREPAYMENT_ID).ToList();

                    foreach (var prepayment in payablePrepayment)
                    {
                        if (decAdjustAmount == 0)
                        {
                            break;
                        }
                        if (prepayment.OUT_STANDAING_BALANCE == 0)
                        {
                            break;
                        }
                        var amountToSettle = decAdjustAmount;
                        if (decAdjustAmount > prepayment.OUT_STANDAING_BALANCE)
                        {
                            amountToSettle = prepayment.OUT_STANDAING_BALANCE ?? 0;
                        }
                        prepayment.OUT_STANDAING_BALANCE -= amountToSettle;
                        decAdjustAmount -= amountToSettle;

                        //OLD OBJECT AND NEW OBJECT ARE THE SAME = NO CHANGE LOG 
                        DBDataContext.Db.Update(prepayment, prepayment);
                        var history = new TBL_APPLY_PAYMENT_HISTORY()
                        {
                            AMOUNT = amountToSettle,
                            IS_ACTIVE = true,
                            INVOICE_ID = 0,
                            APPLY_FROM_PREPAYMENT_ID = prepayment.CUS_PREPAYMENT_ID,
                            PREPAYMENT_ID = 0
                        };
                        histories.Add(history);
                    }
                    //_objCusPrepayment = new PrepaymentLogic().AddCustomerPrepayment(now,_objCustomer.CUSTOMER_ID,-decAdjustAmount, DataHelper.ParseToDecimal(txtLastBalance.Text), (int)cboCurrency.SelectedValue, Logic.Method.GetNextSequence(Sequence.Receipt, true), txtNode.Text, _PrepaymentAction);
                    //DBDataContext.Db.Insert(_objCusPrepayment);

                    //Update PrePayment
                    _objCusPrepayment = new PrepaymentLogic().AddCustomerPrepayment(now, _objCustomer.CUSTOMER_ID, -refundAmount, DataHelper.ParseToDecimal(txtLastBalance.Text), (int)cboCurrency.SelectedValue, Method.GetNextSequence(Sequence.Receipt, true), "", PrepaymentAction.RefundPrepayment, 0);
                    _objCusPrepayment.NOTE = txtNode.Text; //string.IsNullOrEmpty(txtNode.Text) ? _objCusPrepayment.PREPAYMENT_NO : txtNode.Text;
                    _objCusPrepayment.PAYMENT_ACCOUNT_ID = (int)cboPaymentAccount.TreeView.SelectedNode.Tag;
                    DBDataContext.Db.TBL_CUS_PREPAYMENTs.InsertOnSubmit(_objCusPrepayment);
                    DBDataContext.Db.SubmitChanges();


                    //LOG HISTORY 
                    histories.ForEach(x =>
                    {
                        x.PREPAYMENT_ID = _objCusPrepayment.CUS_PREPAYMENT_ID;
                    });

                    DBDataContext.Db.TBL_APPLY_PAYMENT_HISTORies.InsertAllOnSubmit<TBL_APPLY_PAYMENT_HISTORY>(histories);
                    DBDataContext.Db.SubmitChanges();
                }
                else
                {
                    //_objCusPrepayment = Logic.Method.NewCustomerPrepayment(_objCustomer.CUSTOMER_ID, decAdjustAmount, DataHelper.ParseToDecimal(txtLastBalance.Text), (int)cboCurrency.SelectedValue, _objCusPrepayment.PREPAYMENT_NO, txtNode.Text);
                    //_objCusPrepayment.CUS_PREPAYMENT_ID = _objOldCusPrepayment.CUS_PREPAYMENT_ID;
                    //DBDataContext.Db.Update(_objOldCusPrepayment, _objCusPrepayment);
                }
                tran.Complete();
                this.DialogResult = DialogResult.OK;
            }
            //if (this.chkPRINT_RECEIPT.Checked)
            //{
            //    printInvoice();
            //}
        }

        private void printInvoice()
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                if (_objCusPrepayment.CUS_PREPAYMENT_ID > 0)
                {
                    if (_PrepaymentAction == PrepaymentAction.AddPrepayment)
                    {
                        CrystalReportHelper ch = new CrystalReportHelper("ReportDepositReceipt.rpt");
                        ch.SetParameter("@CUS_DEPOSIT_ID", _objCusPrepayment.CUS_PREPAYMENT_ID);
                        ch.PrintReport(Settings.Default.PRINTER_RECIEPT);
                    }
                    else if (_PrepaymentAction == PrepaymentAction.RefundPrepayment)
                    {
                        CrystalReportHelper ch = new CrystalReportHelper("ReportDepositRefund.rpt");
                        ch.SetParameter("@CUS_DEPOSIT_ID", _objCusPrepayment.CUS_PREPAYMENT_ID);
                        ch.PrintReport(Settings.Default.PRINTER_INVOICE);
                    }
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
            Cursor.Current = Cursors.Default;
        }

        private void txtAdjustAmount_TextChanged(object sender, EventArgs e)
        {
            if (_PrepaymentAction == PrepaymentAction.RefundPrepayment)
            {
                txtLastBalance.Text = UIHelper.FormatCurrency((DataHelper.ParseToDecimal(txtCurrentBalance.Text) + -DataHelper.ParseToDecimal(txtAdjustAmount.Text)), (int)cboCurrency.SelectedValue);
            }
            else
            {
                txtLastBalance.Text = UIHelper.FormatCurrency((DataHelper.ParseToDecimal(txtCurrentBalance.Text) + DataHelper.ParseToDecimal(txtAdjustAmount.Text)), (int)cboCurrency.SelectedValue);
            }
        }
        #endregion Event 

        private void chkPrintInvoice_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Default.PRINT_DEPOSIT = this.chkPRINT_RECEIPT.Checked;
            Settings.Default.Save();
        }

        private void cboCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadBalancePrepayment();
        }

        private void dtpDepositDate_Enter(object sender, EventArgs e)
        {
            InputEnglish(null, null);
        }
    }
}
