﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogCustomerNoUsage : ExDialog
    {
        #region Data 
        DataTable _dtNoUsageCus = new DataTable();

        #endregion Data

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="intBillingCycleID">Zero is all billing cycle</param>
        public DialogCustomerNoUsage(int intBillingCycleID)
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);

            getBillingCycle(intBillingCycleID);

        }
        public DialogCustomerNoUsage(List<int> cycleIds)
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            var cycle = from bc in DBDataContext.Db.TBL_BILLING_CYCLEs
                        where bc.IS_ACTIVE
                        && (cycleIds.Contains(bc.CYCLE_ID))
                        select new { bc.CYCLE_ID, bc.CYCLE_NAME, NEXT_MONTH = Method.GetNextBillingMonth(bc.CYCLE_ID) };
            UIHelper.SetDataSourceToComboBox(cboCycle, cycle);
            cboCycle.Enabled = true;
            dtpMonth.Value = Method.GetNextBillingMonth((int)cboCycle.SelectedValue);
            bind();
        }
        public DialogCustomerNoUsage()
        {
            InitializeComponent();
        }

        private void getBillingCycle(int intBillingCycleID)
        {
            var cycle = from bc in DBDataContext.Db.TBL_BILLING_CYCLEs
                        where bc.IS_ACTIVE
                        && (bc.CYCLE_ID == intBillingCycleID || intBillingCycleID == 0)
                        select new { bc.CYCLE_ID, bc.CYCLE_NAME, NEXT_MONTH = Method.GetNextBillingMonth(bc.CYCLE_ID) };

            if (intBillingCycleID == 0)
            {
                UIHelper.SetDataSourceToComboBox(cboCycle, cycle, Resources.ALL_CYCLE);
                cboCycle.SelectedIndex = 1;
            }
            else
            {
                UIHelper.SetDataSourceToComboBox(cboCycle, cycle);
                cboCycle.Enabled = false;
            }
            bind();
        }

        #endregion Constructor

        #region Method

        void bind()
        {

            int MAX_DAY_TO_IGNORE_BILLING = DataHelper.ParseToInt(Method.Utilities[Utility.MAX_DAY_TO_IGNORE_BILLING]);
            int cycleId = (int)cboCycle.SelectedValue;
            var nextBillMonth = Method.GetNextBillingMonth(cycleId);
            this.dtpMonth.Value = nextBillMonth;
            _dtNoUsageCus = new DataTable();

            var sysDate = DBDataContext.Db.GetSystemDate();
            var iCustomerMeter = DBDataContext.Db.TBL_USAGEs.
                                    Where(u => u.COLLECTOR_ID != 0 && u.USAGE_MONTH.Date == nextBillMonth.Date)
                                    .Select(x => new { x.CUSTOMER_ID, x.METER_ID });
            

            DataTable tmpData = new DataTable();
            //var cycle = from bc in DBDataContext.Db.TBL_BILLING_CYCLEs
            //            where bc.IS_ACTIVE
            //            && (bc.CYCLE_ID == intCycleID || intCycleID == 0)
            //            select new { bc.CYCLE_ID, bc.CYCLE_NAME, NEXT_MONTH = Method.GetNextBillingMonth(bc.CYCLE_ID) };
            //foreach (var item in cycle)
            //{

            //&& !(DBDataContext.Db.TBL_USAGEs.Where(u => u.COLLECTOR_ID != 0 && u.USAGE_MONTH.Date == nextBillMonth.Date).Any(x => x.CUSTOMER_ID == cus.CUSTOMER_ID && x.METER_ID == cusmeter.METER_ID))
            DataTable dt = new DataTable();
            //select customer that not yet input usage
            Runner.RunNewThread(delegate ()
            {
                var data = (from cus in DBDataContext.Db.TBL_CUSTOMERs
                            join area in DBDataContext.Db.TBL_AREAs on cus.AREA_ID equals area.AREA_ID
                            join cusmeter in DBDataContext.Db.TBL_CUSTOMER_METERs on cus.CUSTOMER_ID equals cusmeter.CUSTOMER_ID
                            join pole in DBDataContext.Db.TBL_POLEs on cusmeter.POLE_ID equals pole.POLE_ID
                            join box in DBDataContext.Db.TBL_BOXes on cusmeter.BOX_ID equals box.BOX_ID
                            join meter in DBDataContext.Db.TBL_METERs on cusmeter.METER_ID equals meter.METER_ID
                            join emp in DBDataContext.Db.TBL_EMPLOYEEs on pole.COLLECTOR_ID equals emp.EMPLOYEE_ID
                            where cus.BILLING_CYCLE_ID == cycleId //&& cus.IS_POST_PAID
                                && cusmeter.IS_ACTIVE
                                && (cus.STATUS_ID == (int)CustomerStatus.Active || cus.STATUS_ID == (int)CustomerStatus.Blocked)
                                && !iCustomerMeter.Any(x=>x.CUSTOMER_ID == cus.CUSTOMER_ID  && x.METER_ID == cusmeter.METER_ID)
                                && (sysDate - cus.ACTIVATE_DATE).TotalDays >= MAX_DAY_TO_IGNORE_BILLING
                                && ((cus.CUSTOMER_CODE + " " + cus.LAST_NAME_KH + " " + cus.FIRST_NAME_KH + " " + cus.LAST_NAME + " " + cus.FIRST_NAME + " " + cus.PHONE_1 + " " + cus.PHONE_2 + " " +
                                         (pole == null ? "" : pole.POLE_CODE) + " " +
                                         (box == null ? "" : box.BOX_CODE) + " " +
                                         (meter == null ? "" : meter.METER_CODE)
                                       ).ToLower().Contains(this.txtSearch.Text.ToLower())
                                       || txtSearch.Text.Trim() == "")
                            select new
                            {
                                cus.CUSTOMER_ID,
                                area.AREA_ID,
                                area.AREA_NAME,
                                cus.CUSTOMER_CODE,
                                CUSTOMER_NAME = cus.LAST_NAME_KH + " " + cus.FIRST_NAME_KH,
                                emp.EMPLOYEE_ID,
                                emp.EMPLOYEE_NAME,
                                meter.METER_CODE
                            });
                //dt = data.Take(100)._ToDataTable();
                dt = data._ToDataTable();
            }); 
            tmpData = dt;
            if (_dtNoUsageCus.Columns.Count == 0)
            {
                _dtNoUsageCus = dt.Clone();
            }
            foreach (DataRow dr in dt.Rows)
            {
                DataRow drNUsage = _dtNoUsageCus.NewRow();
                foreach (DataColumn dc in _dtNoUsageCus.Columns)
                {
                    drNUsage[dc.ColumnName] = dr[dc.ColumnName];
                }
                _dtNoUsageCus.Rows.Add(drNUsage);
            }

            dgv.DataSource = _dtNoUsageCus;
            UIHelper.SetDataSourceToComboBox(cboArea, (from a in tmpData.Select()
                                                       select new
                                                       { AREA_ID = a["AREA_ID"], AREA_NAME = a["AREA_NAME"] })
                                                            .Distinct().OrderBy(row => row.AREA_NAME), Resources.ALL_AREA);
            UIHelper.SetDataSourceToComboBox(cboCollector, (from e in _dtNoUsageCus.Select()
                                                            select new { EMPLOYEE_ID = e["EMPLOYEE_ID"], EMPLOYEE_NAME = e["EMPLOYEE_NAME"] })
                                                            .Distinct(), Resources.ALL_EMPLOYEE);
            txtNo.Text = _dtNoUsageCus.Rows.Count.ToString();
        }

        void RebindToGrid()
        {
            if (cboArea.SelectedIndex != -1 && cboCollector.SelectedIndex != -1)
            {
                int intAreaId = int.Parse(cboArea.SelectedValue.ToString());
                int intEmpId = int.Parse(cboCollector.SelectedValue.ToString());
                _dtNoUsageCus.DefaultView.RowFilter = string.Format("(AREA_ID={0} OR {0}=0) and (EMPLOYEE_ID={1} OR {1}=0) ", intAreaId, intEmpId);
            }
            txtNo.Text = dgv.RowCount.ToString();
        }

        #endregion Method

        #region Event
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
        private void cboArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            RebindToGrid();
        }

        private void cboCollector_SelectedIndexChanged(object sender, EventArgs e)
        {
            RebindToGrid();
        }

        private void cboCycle_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboCycle.SelectedIndex != -1)
            {
                bind();
            }
        }

        private void btnInputUsage_Click(object sender, EventArgs e)
        {
            if (this.dgv.SelectedRows.Count > 0)
            {
                int CustomerID = (int)this.dgv.SelectedRows[0].Cells[CUSTOMER_ID.Name].Value;
                if (new DialogCollectUsageManually(CustomerID).ShowDialog() == DialogResult.OK)
                {
                    this.bind();
                }
            }
        }

        private void txtSearch_QuickSearch(object sender, EventArgs e)
        {
            if (cboCycle.SelectedIndex != -1)
            {
                bind();
            }
        }

        private void txtSearch_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            try
            {
                CrystalReportHelper cr = ViewReportCustomerNoUsage(dtpMonth.Value.Date);
                cr.ViewReport("");
                cr.Dispose();
            }
            catch (Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_VIEW_REPORT, Base.Properties.Resources.WARNING);
            }
        }

        public CrystalReportHelper ViewReportCustomerNoUsage(DateTime dtMonth)
        {
            CrystalReportHelper c = new CrystalReportHelper("ReportCustomerNoUsage.rpt");
            c.SetParameter("@MONTH", dtMonth);
            c.SetParameter("@CYCLE_ID", (int)this.cboCycle.SelectedValue);
            c.SetParameter("@AREA_ID", (int)this.cboArea.SelectedValue);
            return c;
        }
        #endregion Event
    }
}
