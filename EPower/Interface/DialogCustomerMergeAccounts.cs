﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Logic;
using SoftTech.Security.Model;

namespace EPower.Interface
{
    public partial class DialogCustomerMergeAccounts : ExDialog
    {
        private GeneralProcess _flag;
        private bool newReative;
        private string newRuleName;
        private bool oldReative;
        private string oldRuleName;
        private List<ChangeLog> _customerMergeChangeLogs = new List<ChangeLog>();
        public enum CustomerMergeType
        {
            Invoice,
            Usage
        }
        public TBL_CUSTOMER Customer
        {
            get
            {
                return _objCustomer;
            }
        }
        TBL_CUSTOMER _objCustomer = new TBL_CUSTOMER();
        TBL_CUSTOMER _objOldCustomer = new TBL_CUSTOMER();
        CustomerMergeType _flagMerge;

        public DialogCustomerMergeAccounts(GeneralProcess flag, int CustomerId, CustomerMergeType flagMerge)
        {
            InitializeComponent();
            _objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == CustomerId);
            _flagMerge = flagMerge;
            _flag = flag;
            if (_objCustomer != null)
            {
                _objCustomer._CopyTo(_objOldCustomer);
                readCustomer();
                this.txtCustomerCode.Enabled = false;
            }
            else
            {
                _objCustomer = new TBL_CUSTOMER();
            }
            // load DataGridViewComboBox
            this.IS_REACTIVE.Visible =
                this.REACTIVE_RULE.Visible = _flagMerge == CustomerMergeType.Invoice;

            this.REACTIVE_RULE.ValueMember = "REACTIVE_RULE_ID";
            this.REACTIVE_RULE.DisplayMember = "RULE_NAME";
            var lst = DBDataContext.Db.TBL_REACTIVE_RULEs.Where(x => x.IS_ACTIVE).ToList();
            lst.Insert(0, new TBL_REACTIVE_RULE() { REACTIVE_RULE_ID = 0, RULE_NAME = "" });
            this.REACTIVE_RULE.DataSource = lst.ToArray();
            this.REACTIVE_RULE.ValueType = typeof(int);
            this.IS_REACTIVE.ValueType = typeof(bool);

            btnCHANGE_LOG.Visible = (_flag != GeneralProcess.Insert);

            read(_objCustomer);
            this.ActiveControl = lblCUSTOMER;
        }

        private void read(TBL_CUSTOMER objCus)
        {
            if (objCus.CUSTOMER_ID == 0)
            {
                return;
            }

            var cus = from c in DBDataContext.Db.TBL_CUSTOMERs
                      join a in DBDataContext.Db.TBL_AREAs on c.AREA_ID equals a.AREA_ID
                      join m in (from a in DBDataContext.Db.TBL_CUSTOMER_METERs
                                 join m in DBDataContext.Db.TBL_METERs on a.METER_ID equals m.METER_ID
                                 where a.IS_ACTIVE
                                 select new { a.CUSTOMER_ID, m.METER_CODE }) on c.CUSTOMER_ID equals m.CUSTOMER_ID into cm
                      from x in cm.DefaultIfEmpty()
                      where c.STATUS_ID != (int)CustomerStatus.Cancelled
                      && (_flagMerge == CustomerMergeType.Invoice || c.STATUS_ID != (int)CustomerStatus.Closed)
                      && ((c.USAGE_CUSTOMER_ID == objCus.CUSTOMER_ID && _flagMerge == CustomerMergeType.Usage)
                      || (c.INVOICE_CUSTOMER_ID == objCus.CUSTOMER_ID && _flagMerge == CustomerMergeType.Invoice))
                      && c.CUSTOMER_ID != _objCustomer.CUSTOMER_ID || (c.CUSTOMER_ID == objCus.CUSTOMER_ID && objCus.CUSTOMER_ID != _objCustomer.CUSTOMER_ID)
                      select new
                      {
                          c.CUSTOMER_ID,
                          c.STATUS_ID,
                          c.CUSTOMER_CODE,
                          CUSTOMER_NAME = string.Concat(c.LAST_NAME_KH, " ", c.FIRST_NAME_KH),
                          a.AREA_CODE,
                          METER_CODE = x == null ? "" : x.METER_CODE,
                          c.IS_REACTIVE,
                          c.REACTIVE_RULE_ID
                      };

            foreach (var item in cus)
            {
                dgvArea.Rows.Add(item.CUSTOMER_ID, item.STATUS_ID, item.CUSTOMER_CODE, item.CUSTOMER_NAME, item.AREA_CODE, item.METER_CODE, item.IS_REACTIVE, item.REACTIVE_RULE_ID);
            }


            foreach (DataGridViewRow item in dgvArea.Rows)
            {
                int status_id = (int)item.Cells[STATUS_ID.Name].Value;
                if (status_id == (int)CustomerStatus.Closed || status_id == (int)CustomerStatus.Cancelled)
                {
                    item.DefaultCellStyle.ForeColor = Color.Red;
                    item.DefaultCellStyle.SelectionForeColor = Color.Red;
                }
                else if (status_id == (int)CustomerStatus.Blocked)
                {
                    item.DefaultCellStyle.ForeColor = Color.DarkOrange;
                    item.DefaultCellStyle.SelectionForeColor = Color.DarkOrange;
                }
                else if (status_id == (int)CustomerStatus.Pending)
                {
                    item.DefaultCellStyle.ForeColor = Color.DarkBlue;
                    item.DefaultCellStyle.SelectionForeColor = Color.DarkBlue;
                }
            }
        }

        private void txtMasterCustomer_AdvanceSearch(object sender, EventArgs e)
        {
            this.txtCustomerCode.ClearValidation();
            DialogCustomerSearch diag = new DialogCustomerSearch(this.txtCustomerCode.Text, DialogCustomerSearch.PowerType.Postpaid);
            
            if (diag.ShowDialog() != DialogResult.OK)
            {
                txtCustomerCode.CancelSearch();
                return;
            }

            _objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == diag.CustomerID);
            readCustomer();
        }

        private void readCustomer()
        {
            this.txtCustomerCode.Text = _objCustomer.CUSTOMER_CODE;
            this.txtCustomerName.Text = _objCustomer.LAST_NAME_KH + " " + _objCustomer.FIRST_NAME_KH;
            var objArea = DBDataContext.Db.TBL_AREAs.FirstOrDefault(row => row.AREA_ID == _objCustomer.AREA_ID);
            this.txtArea.Text = objArea == null ? "" : objArea.AREA_NAME;
            var objCusMeter = DBDataContext.Db.TBL_CUSTOMER_METERs.FirstOrDefault(row => row.CUSTOMER_ID == _objCustomer.CUSTOMER_ID && row.IS_ACTIVE);
            this.txtMeter.Text = (objCusMeter == null) ? "" : DBDataContext.Db.TBL_METERs.FirstOrDefault(row => row.METER_ID == objCusMeter.METER_ID).METER_CODE;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
        }

        private void txtMasterCustomer_CancelAdvanceSearch(object sender, EventArgs e)
        {
            _objCustomer = new TBL_CUSTOMER();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            // remove focus from datagridview to btnSave

            this.btnOK.Focus();

            if (inValid())
            {
                return;
            }

            UpdateAuditTrail();

            IEnumerable<TBL_CUSTOMER> objCus = from c in DBDataContext.Db.TBL_CUSTOMERs
                                               where c.USAGE_CUSTOMER_ID == _objCustomer.CUSTOMER_ID || c.INVOICE_CUSTOMER_ID == _objCustomer.CUSTOMER_ID
                                               select c;
            
            List<TBL_CUSTOMER> objListCus = new List<TBL_CUSTOMER>();

            foreach (DataGridViewRow row in dgvArea.Rows)
            {
                TBL_CUSTOMER objc = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == DataHelper.ParseToInt(row.Cells["CUSTOMER_ID"].Value.ToString()));
                if (_flagMerge == CustomerMergeType.Usage)
                {
                    objc.USAGE_CUSTOMER_ID = _objCustomer.CUSTOMER_ID;
                    objc.IS_REACTIVE = false;
                    objc.REACTIVE_RULE_ID = 0;
                }
                else
                {
                    objc.INVOICE_CUSTOMER_ID = _objCustomer.CUSTOMER_ID;
                    objc.IS_REACTIVE = (bool)row.Cells[this.IS_REACTIVE.Name].Value;
                    objc.REACTIVE_RULE_ID = objc.IS_REACTIVE ? (int)(row.Cells[this.REACTIVE_RULE.Name].Value ?? 0) : 0;
                }
                objc.BILLING_CYCLE_ID = _objCustomer.BILLING_CYCLE_ID;

                //change log 
                var customerCode = row.Cells[CUSTOMER_CODE.Name].Value.ToString()??"";
                var customerId = Dynamic.Helpers.Parse.ToInt(row.Cells[CUSTOMER_ID.Name].Value?.ToString());
                var customerName = row.Cells[CUSTOMER_NAME.Name].Value?.ToString()??"";
                var meterCode = row.Cells[METER_CODE.Name].Value?.ToString()??"";
                var area = row.Cells[AREA.Name].Value?.ToString()??"";

                if (!objCus.Select(x => x.CUSTOMER_ID).ToList().Contains((customerId)))
                {
                    if(_flag == GeneralProcess.Insert)
                    {
                        //Customer merge 
                        if (_flagMerge == CustomerMergeType.Invoice)
                        {
                            var reative = (bool)row.Cells[IS_REACTIVE.Name].Value;
                            newReative = reative;

                            var ruleName = new TBL_REACTIVE_RULE();
                            var ruleValue = row.Cells[REACTIVE_RULE.Name].Value?.ToString() ?? "0";
                            if (DataHelper.ParseToInt(ruleValue.ToString()) > 0)
                            {
                                ruleName = DBDataContext.Db.TBL_REACTIVE_RULEs.FirstOrDefault(x => x.IS_ACTIVE && x.REACTIVE_RULE_ID == (int)row.Cells[REACTIVE_RULE.Name].Value);
                                newRuleName = ruleName?.RULE_NAME ?? "គ្មាន";
                            }

                            if (!reative)
                            {
                                newRuleName = "គ្មាន";
                            }

                            var customerMerge = new ChangeLog()
                            {
                                DisplayName = $"បន្ថែម អតិថិជន {customerCode} {customerName}",
                                Details = new List<ChangeLog>()
                            {
                                 new ChangeLog()
                                 {
                                     DisplayName =Resources.CUS_NO,
                                     OldValue ="",
                                     NewValue = customerCode,
                                 },
                                 new ChangeLog()
                                 {
                                     DisplayName =Resources.CUSTOMER,
                                     OldValue ="",
                                     NewValue = customerName,
                                 },
                                 new ChangeLog()
                                 {
                                     DisplayName =Resources.AREA,
                                     OldValue ="",
                                     NewValue = area,
                                 },
                                 new ChangeLog()
                                 {
                                     DisplayName =Resources.METER_CODE,
                                     OldValue ="",
                                     NewValue =meterCode
                                 },
                                 new ChangeLog()
                                 {
                                     DisplayName =Resources.IS_REACTIVE,
                                     OldValue ="",
                                     NewValue =ResourceHelper.Translate(newReative.ToString())
                                 },
                                 new ChangeLog()
                                 {
                                     DisplayName =Resources.REACTIVE_RULE,
                                     OldValue ="",
                                     NewValue =newRuleName??"គ្មាន"
                                 }
                                }
                            };
                            _customerMergeChangeLogs.Add(customerMerge);
                        }
                        else
                        {
                            var customerMerge = new ChangeLog()
                            {
                                DisplayName = $"បន្ថែម អតិថិជន {customerCode} {customerName}",
                                Details = new List<ChangeLog>()
                            {
                                 new ChangeLog()
                                 {
                                     DisplayName =Resources.CUS_NO,
                                     OldValue ="",
                                     NewValue = customerCode,
                                 },
                                 new ChangeLog()
                                 {
                                     DisplayName =Resources.CUSTOMER,
                                     OldValue ="",
                                     NewValue = customerName,
                                 },
                                 new ChangeLog()
                                 {
                                     DisplayName =Resources.AREA,
                                     OldValue ="",
                                     NewValue = area,
                                 },
                                 new ChangeLog()
                                 {
                                     DisplayName =Resources.METER_CODE,
                                     OldValue ="",
                                     NewValue =meterCode
                                 }
                           }
                            };
                            _customerMergeChangeLogs.Add(customerMerge);
                        }
                    }                               
                }
                objListCus.Add(objc);
            }

            foreach (TBL_CUSTOMER item in objCus)
            {
                if (objListCus.Where(x => x.CUSTOMER_ID == item.CUSTOMER_ID).Count() == 0)
                {
                    TBL_CUSTOMER objC = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == item.CUSTOMER_ID);
                    if (_flagMerge == CustomerMergeType.Usage)
                    {
                        if (item.CUSTOMER_CODE != txtCustomerCode.Text) { UpdateRemoveMergeCustomerChangeLog(item); }
                        item.USAGE_CUSTOMER_ID = 0;
                    }
                    else
                    {
                        if (item.CUSTOMER_CODE != txtCustomerCode.Text) { UpdateRemoveMergeCustomerChangeLog(item); }
                        item.INVOICE_CUSTOMER_ID = 0;
                        item.IS_REACTIVE = false;
                        item.REACTIVE_RULE_ID = 0;
                    }
                }
            }

            if (_flagMerge == CustomerMergeType.Usage)
            {
                _objCustomer.USAGE_CUSTOMER_ID = objListCus.Count == 0 ? 0 : _objCustomer.CUSTOMER_ID;
                if (_objCustomer.CUSTOMER_ID != _objOldCustomer.CUSTOMER_ID && _objOldCustomer.CUSTOMER_ID != 0)
                {
                    _objOldCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == _objOldCustomer.CUSTOMER_ID);
                    _objOldCustomer.USAGE_CUSTOMER_ID = 0;
                }
            }
            else
            {
                _objCustomer.INVOICE_CUSTOMER_ID = objListCus.Count == 0 ? 0 : _objCustomer.CUSTOMER_ID;
                if (_objCustomer.CUSTOMER_ID != _objOldCustomer.CUSTOMER_ID && _objOldCustomer.CUSTOMER_ID != 0)
                {
                    _objOldCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == _objOldCustomer.CUSTOMER_ID);
                    _objOldCustomer.INVOICE_CUSTOMER_ID = 0;
                }
            }

            AddAuditTrail();

            if (_flag == GeneralProcess.Update)
            {
                var changeLogs = new List<ChangeLog>()
                {

                };

                var parentCustomer = new ChangeLog()
                {
                    DisplayName = $"កែប្រែ អតិថិជនគោល {txtCustomerCode.Text} {txtCustomerName.Text}",
                    Details = new List<ChangeLog>()
                        {
                             new ChangeLog()
                             {
                                 DisplayName =Resources.CUS_NO,
                                 OldValue ="",
                                 NewValue = txtCustomerCode.Text,
                             },
                             new ChangeLog()
                             {
                                 DisplayName =Resources.CUSTOMER,
                                 OldValue ="",
                                 NewValue = txtCustomerName.Text,
                             },
                             new ChangeLog()
                             {
                                 DisplayName =Resources.AREA,
                                 OldValue ="",
                                 NewValue = txtArea.Text,
                             },
                             new ChangeLog()
                             {
                                 DisplayName =Resources.METER_CODE,
                                 OldValue ="",
                                 NewValue = txtMeter.Text,
                             }
                          }
                };
                changeLogs.Add(parentCustomer);
                if (_customerMergeChangeLogs.Count > 0)
                {
                    changeLogs.AddRange(_customerMergeChangeLogs);
                    var auditTrail = new OutFaceAuditTrail()
                    {
                        AuditDate = DateTime.Now,
                        Context = _objCustomer.USAGE_CUSTOMER_ID > 0 ? $"{ResourceHelper.Translate(_flag.ToString())} អតិថិជនរួម ការប្រើប្រាស់រួម" : $"{ResourceHelper.Translate(_flag.ToString())} អតិថិជនរួម គិតទឹកប្រាក់រួម",
                        PrimaryKey = _objCustomer.CUSTOMER_ID,
                        TableName = nameof(TBL_CUSTOMER),
                        OldObject = Newtonsoft.Json.JsonConvert.SerializeObject(_objCustomer),
                        NewObject = Newtonsoft.Json.JsonConvert.SerializeObject(_objOldCustomer),
                        ChangeLog = Newtonsoft.Json.JsonConvert.SerializeObject(changeLogs),
                    };
                    AuditTrailLogic.Instance.Add(auditTrail);
                }
            }

            DBDataContext.Db.SubmitChanges();
            this.DialogResult = DialogResult.OK;
        }
        public void AddAuditTrail()
        {
            if (_flag == GeneralProcess.Insert)
            {
                //Parent
                var changeLogs = new List<ChangeLog>()
                {

                };

                var parentCustomer = new ChangeLog()
                {
                    DisplayName = _flag == GeneralProcess.Insert ? $"បន្ថែម អតិថិជនគោល {txtCustomerCode.Text} {txtCustomerName.Text}" : $"កែប្រែ អតិថិជនគោល {txtCustomerCode.Text} {txtCustomerName.Text}",
                    Details = new List<ChangeLog>()
                        {
                             new ChangeLog()
                             {
                                 DisplayName =Resources.CUS_NO,
                                 OldValue ="",
                                 NewValue = txtCustomerCode.Text,
                             },
                             new ChangeLog()
                             {
                                 DisplayName =Resources.CUSTOMER,
                                 OldValue ="",
                                 NewValue = txtCustomerName.Text,
                             },
                             new ChangeLog()
                             {
                                 DisplayName =Resources.AREA,
                                 OldValue ="",
                                 NewValue = txtArea.Text,
                             },
                             new ChangeLog()
                             {
                                 DisplayName =Resources.METER_CODE,
                                 OldValue ="",
                                 NewValue = txtMeter.Text,
                             }
                          }
                };
                changeLogs.Add(parentCustomer);
                changeLogs.AddRange(_customerMergeChangeLogs);
                var auditTrail = new OutFaceAuditTrail()
                {
                    AuditDate = DateTime.Now,
                    Context = _objCustomer.USAGE_CUSTOMER_ID > 0 ? $"{ResourceHelper.Translate(_flag.ToString())}អតិថិជនរួម ការប្រើប្រាស់រួម" : $"{ResourceHelper.Translate(_flag.ToString())} អតិថិជនរួម គិតទឹកប្រាក់រួម",
                    PrimaryKey = _objCustomer.CUSTOMER_ID,
                    TableName = nameof(TBL_CUSTOMER),
                    OldObject = Newtonsoft.Json.JsonConvert.SerializeObject(_objCustomer),
                    NewObject = Newtonsoft.Json.JsonConvert.SerializeObject(_objOldCustomer),
                    ChangeLog = Newtonsoft.Json.JsonConvert.SerializeObject(changeLogs),
                };
                AuditTrailLogic.Instance.Add(auditTrail);
            }        
        }


        public void UpdateAuditTrail()
        {
            try
            {
                if (_flag == GeneralProcess.Update)
                {
                    //Log when add new customer when using flag update
                    if (_flagMerge == CustomerMergeType.Usage)
                    {
                        foreach (DataGridViewRow Row in dgvArea.Rows)
                        {
                            int cusId = DataHelper.ParseToInt(Row.Cells[CUSTOMER_ID.Name].Value.ToString());
                            TBL_CUSTOMER _objCus = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == cusId && x.USAGE_CUSTOMER_ID == _objOldCustomer.CUSTOMER_ID);

                            if (_objCus == null)
                            {
                                TBL_CUSTOMER objCus = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == cusId);
                                var customerMerge = new ChangeLog()
                                {
                                    DisplayName = $"បន្ថែម អតិថិជន {objCus.CUSTOMER_CODE} {objCus.LAST_NAME_KH + objCus.FIRST_NAME_KH}",
                                    Details = new List<ChangeLog>()
                                {
                                     new ChangeLog()
                                     {
                                         DisplayName =Resources.CUS_NO,
                                         OldValue ="",
                                         NewValue = objCus.CUSTOMER_CODE,
                                     },
                                     new ChangeLog()
                                     {
                                         DisplayName =Resources.CUSTOMER,
                                         OldValue ="",
                                         NewValue = objCus.LAST_NAME_KH + objCus.FIRST_NAME_KH,
                                     },
                                     new ChangeLog()
                                     {
                                         DisplayName =Resources.AREA,
                                         OldValue ="",
                                         NewValue = Row.Cells[AREA.Name].Value?.ToString()??"",
                                     },
                                           new ChangeLog()
                                     {
                                         DisplayName =Resources.METER_CODE,
                                         OldValue ="",
                                         NewValue = Row.Cells[METER_CODE.Name].Value?.ToString()??"",
                                     }
                                }
                                };
                                _customerMergeChangeLogs.Add(customerMerge);
                            }
                        }
                    }

                    else
                    {
                        foreach (DataGridViewRow Row in dgvArea.Rows)
                        {
                            int cusId = DataHelper.ParseToInt(Row.Cells[CUSTOMER_ID.Name].Value.ToString());
                            TBL_CUSTOMER _objCus = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == cusId && x.INVOICE_CUSTOMER_ID == _objOldCustomer.CUSTOMER_ID);

                            if (_objCus == null)
                            {
                                //change log 
                                var customerCode = Row.Cells[CUSTOMER_CODE.Name].Value.ToString() ?? "";
                                var customerId = Dynamic.Helpers.Parse.ToInt(Row.Cells[CUSTOMER_ID.Name].Value?.ToString());
                                var customerName = Row.Cells[CUSTOMER_NAME.Name].Value?.ToString() ?? "";
                                var meterCode = Row.Cells[METER_CODE.Name].Value?.ToString() ?? "";
                                var area = Row.Cells[AREA.Name].Value?.ToString() ?? "";

                                TBL_CUSTOMER objCus = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == cusId);
                                var reative = (bool)Row.Cells[IS_REACTIVE.Name].Value;
                                newReative = reative;

                                var ruleName = new TBL_REACTIVE_RULE();
                                var ruleValue = Row.Cells[REACTIVE_RULE.Name].Value?.ToString() ?? "";
                                if (DataHelper.ParseToInt(ruleValue.ToString()) > 0)
                                {
                                    ruleName = DBDataContext.Db.TBL_REACTIVE_RULEs.FirstOrDefault(x => x.IS_ACTIVE && x.REACTIVE_RULE_ID == (int)Row.Cells[REACTIVE_RULE.Name].Value);
                                    newRuleName = ruleName?.RULE_NAME ?? "គ្មាន";
                                }

                                if (!reative)
                                {
                                    newRuleName = "គ្មាន";
                                }

                                var customerMerge = new ChangeLog()
                                {
                                    DisplayName = $"បន្ថែម អតិថិជន {customerCode} {customerName}",
                                    Details = new List<ChangeLog>()
                                {
                                     new ChangeLog()
                                     {
                                         DisplayName =Resources.CUS_NO,
                                         OldValue ="",
                                         NewValue = customerCode,
                                     },
                                     new ChangeLog()
                                     {
                                         DisplayName =Resources.CUSTOMER,
                                         OldValue ="",
                                         NewValue = customerName,
                                     },
                                     new ChangeLog()
                                     {
                                         DisplayName =Resources.AREA,
                                         OldValue ="",
                                         NewValue = area,
                                     },
                                     new ChangeLog()
                                     {
                                         DisplayName =Resources.METER_CODE,
                                         OldValue ="",
                                         NewValue =meterCode
                                     },
                                     new ChangeLog()
                                     {
                                         DisplayName =Resources.IS_REACTIVE,
                                         OldValue ="",
                                         NewValue =ResourceHelper.Translate(newReative.ToString())
                                     },
                                     new ChangeLog()
                                     {
                                         DisplayName =Resources.REACTIVE_RULE,
                                         OldValue ="",
                                         NewValue =newRuleName??"គ្មាន"
                                     }
                                    }
                                };
                                _customerMergeChangeLogs.Add(customerMerge);
                            }

                            else
                            {
                                var objReative = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == _objCus.CUSTOMER_ID);
                                var objRuleName = DBDataContext.Db.TBL_REACTIVE_RULEs.FirstOrDefault(row => row.IS_ACTIVE && row.REACTIVE_RULE_ID == _objCus.REACTIVE_RULE_ID);
                                var oldRuleName = objRuleName?.RULE_NAME?.ToString() ?? "គ្មាន";
                                var NewRuleName = "គ្មាន";

                                if (Row.Cells[REACTIVE_RULE.Name].Value != null)
                                {
                                    var _NewRuleName = DBDataContext.Db.TBL_REACTIVE_RULEs.FirstOrDefault(row => row.IS_ACTIVE && row.REACTIVE_RULE_ID == (int)Row.Cells[REACTIVE_RULE.Name].Value);
                                    NewRuleName = _NewRuleName?.RULE_NAME?.ToString()??"គ្មាន";
                                }

                                if (objReative.IS_REACTIVE != (bool)Row.Cells[IS_REACTIVE.Name].Value || oldRuleName != NewRuleName)
                                {
                                    var customerMerge = new ChangeLog()
                                    {
                                        DisplayName = $"កែប្រែ អតិថិជនកូន {_objCus.CUSTOMER_CODE} {_objCus.LAST_NAME_KH + " " + _objCus.FIRST_NAME_KH}",
                                        Details = new List<ChangeLog>()
                                            {
                                             new ChangeLog()
                                             {
                                                DisplayName = Resources.IS_REACTIVE,
                                                OldValue = ResourceHelper.Translate(objReative.IS_REACTIVE.ToString()??"0"),
                                                NewValue =ResourceHelper.Translate(Row.Cells[IS_REACTIVE.Name].Value?.ToString()??"0")
                                             },
                                             new ChangeLog()
                                             {
                                                DisplayName = Resources.REACTIVE_RULE,
                                                OldValue = oldRuleName,
                                                NewValue = NewRuleName
                                             }
                                            }
                                    };
                                    _customerMergeChangeLogs.Add(customerMerge);
                                }
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {

            }
        }

        public void UpdateRemoveMergeCustomerChangeLog(TBL_CUSTOMER item)
        {
            try
            {
                //Remove
                var objArea = DBDataContext.Db.TBL_AREAs.FirstOrDefault(row => row.AREA_ID == item.AREA_ID);
                var area = (objArea == null) ? "" : objArea.AREA_NAME;
                var objCusMeter = DBDataContext.Db.TBL_CUSTOMER_METERs.FirstOrDefault(row => row.CUSTOMER_ID == item.CUSTOMER_ID && row.IS_ACTIVE);
                var meter = (objCusMeter == null) ? "" : DBDataContext.Db.TBL_METERs.FirstOrDefault(row => row.METER_ID == objCusMeter.METER_ID).METER_CODE;


                //Remove Customer merge 
                if (_flagMerge == CustomerMergeType.Invoice)
                {
                    var objReative = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == item.CUSTOMER_ID);
                    var reative = (objReative == null) ? false : objReative.IS_REACTIVE;
                    var objRuleName = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == item.CUSTOMER_ID);
                    var ruleName = (objRuleName == null) ? "គ្មាន" : DBDataContext.Db.TBL_REACTIVE_RULEs.FirstOrDefault(row => row.REACTIVE_RULE_ID == objRuleName.REACTIVE_RULE_ID)?.RULE_NAME ?? "គ្មាន";

                    var customerMerge = new ChangeLog()
                    {
                        DisplayName = $"លុប អតិថិជន {item.CUSTOMER_CODE} {item.LAST_NAME_KH + item.FIRST_NAME_KH}",
                        Details = new List<ChangeLog>()
                    {
                         new ChangeLog()
                         {
                             DisplayName =Resources.CUS_NO,
                             OldValue =item.CUSTOMER_CODE,
                             NewValue = "",
                         },
                         new ChangeLog()
                         {
                             DisplayName =Resources.CUSTOMER,
                             OldValue =item.LAST_NAME_KH + item.FIRST_NAME_KH,
                             NewValue ="",
                         },
                         new ChangeLog()
                         {
                             DisplayName =Resources.AREA,
                             OldValue =area,
                             NewValue ="",
                         },
                         new ChangeLog()
                         {
                             DisplayName =Resources.METER_CODE,
                             OldValue = meter,
                             NewValue = ""
                         },
                         new ChangeLog()
                         {
                             DisplayName =Resources.IS_REACTIVE,
                             OldValue = ResourceHelper.Translate(reative.ToString()),
                             NewValue = ""
                         },
                         new ChangeLog()
                         {
                             DisplayName =Resources.REACTIVE_RULE,
                             OldValue = ruleName??"គ្មាន",
                             NewValue = ""
                         }
                    }
                    };
                    _customerMergeChangeLogs.Add(customerMerge);
                }
                else
                {
                    var customerMerge = new ChangeLog()
                    {
                        DisplayName = $"លុប អតិថិជន {item.CUSTOMER_CODE} {item.LAST_NAME_KH + item.FIRST_NAME_KH}",
                        Details = new List<ChangeLog>()
                    {
                         new ChangeLog()
                         {
                             DisplayName =Resources.CUS_NO,
                             OldValue =item.CUSTOMER_CODE,
                             NewValue = "",
                         },
                         new ChangeLog()
                         {
                             DisplayName =Resources.CUSTOMER,
                             OldValue =item.LAST_NAME_KH + item.FIRST_NAME_KH,
                             NewValue ="",
                         },
                         new ChangeLog()
                         {
                             DisplayName =Resources.AREA,
                             OldValue =area,
                             NewValue ="",
                         },
                         new ChangeLog()
                         {
                             DisplayName =Resources.METER_CODE,
                             OldValue = meter,
                             NewValue = ""
                         }
                    }
                    };
                    _customerMergeChangeLogs.Add(customerMerge);
                }
            }
            catch(Exception ex)
            {

            }        
        }

        private bool inValid()
        {
            bool val = false;
            txtCustomerCode.ClearAllValidation();
            if (_objCustomer.CUSTOMER_ID == 0)
            {
                txtCustomerCode.SetValidation(string.Format(Resources.REQUIRED, lblCUSTOMER_CODE.Text));
                val = true;
            }
            foreach (DataGridViewRow row in this.dgvArea.Rows)
            {
                if ((bool)row.Cells[this.IS_REACTIVE.Name].Value)
                {
                    if (0 == (int)(row.Cells[this.REACTIVE_RULE.Name].Value ?? 0))
                    {
                        MsgBox.ShowInformation(string.Format(Resources.MS_CUSTOMER_MUST_HAVE_REACTIVE_RULE, row.Cells[this.CUSTOMER_NAME.Name].Value));
                        return true;
                    }
                }
            }
            return val;
        }

        private void btnAdd_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (txtCustomerCode.Text.Trim() == string.Empty || _objCustomer.ADDRESS == null)
            {
                txtCustomerCode.SetValidation(string.Format(Resources.REQUIRED, lblCUSTOMER_CODE.Text));
                return;
            }
            DialogCustomerSearch diag = new DialogCustomerSearch("", DialogCustomerSearch.PowerType.Postpaid);
            if (diag.ShowDialog() != DialogResult.OK) return;
            TBL_CUSTOMER objCus = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == diag.CustomerID);
            foreach (DataGridViewRow item in dgvArea.Rows)
            {
                int cusId = DataHelper.ParseToInt(item.Cells["CUSTOMER_ID"].Value.ToString());
                if (cusId == objCus.CUSTOMER_ID || objCus.CUSTOMER_ID == _objCustomer.CUSTOMER_ID)
                {
                    MsgBox.ShowInformation(string.Format(Resources.MS_CUSTOMER_ALREADY_EXISTS_IN_MERGE_CUSTOMER, string.Concat(objCus.LAST_NAME_KH, " ", objCus.FIRST_NAME_KH)), this.Text);
                    return;
                }
            }
            if (objCus.BILLING_CYCLE_ID != _objCustomer.BILLING_CYCLE_ID && _objCustomer.CUSTOMER_ID != 0)
            {
                DialogResult dimsg = MsgBox.ShowQuestion(Resources.MS_CUSTOMER_CYCLE_NOT_MATCH_IN_BASE_CUSTOMER, Text);
                if (dimsg != DialogResult.Yes)
                {
                    return;
                }
            }
            if (objCus.INVOICE_CUSTOMER_ID != 0)
            {
                MsgBox.ShowInformation(string.Format(Resources.MS_CUSTOMER_ALREADY_IN_MERGE_INVOICE_CUSTOMER, string.Concat(objCus.LAST_NAME_KH, " ", objCus.FIRST_NAME_KH), Text));
                return;
            }
            if (objCus.USAGE_CUSTOMER_ID != 0)
            {
                MsgBox.ShowInformation(string.Format(Resources.MS_CUSTOMER_ALREDY_IN_MERGE_UAGE_CUSTOMER, string.Concat(objCus.LAST_NAME_KH, " ", objCus.FIRST_NAME_KH), Text));
                return;
            }

            //Currency id not match.
            var cp = DBDataContext.Db.TBL_PRICEs.FirstOrDefault(x => x.PRICE_ID == objCus.PRICE_ID);
            var mp = DBDataContext.Db.TBL_PRICEs.FirstOrDefault(x => x.PRICE_ID == _objCustomer.PRICE_ID);
            if (cp.CURRENCY_ID != mp.CURRENCY_ID)
            {
                MsgBox.ShowInformation(string.Format(Resources.MS_CURRENCY_CUSTOMER_NOT_MATCH_IN_BASE_CUTOMER, string.Concat(objCus.LAST_NAME_KH, " ", objCus.FIRST_NAME_KH), string.Concat(_objCustomer.LAST_NAME_KH, " ", _objCustomer.FIRST_NAME_KH)));
                return;
            }

            read(objCus);
        }

        private void btnRemove_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (this.dgvArea.SelectedRows.Count == 0) return;
            dgvArea.Rows.Remove(dgvArea.SelectedRows[0]);
        }

        private void txtMasterCustomer_QuickSearch(object sender, EventArgs e)
        {

        }

        private void btnCHANGE_LOG_Click(object sender, EventArgs e)
        {
            var dialog = new Setting.DialogHistory(nameof(TBL_CUSTOMER), _objCustomer.CUSTOMER_ID, Resources.CUSTOMER_MERGE_ACCOUNTS) { };
            dialog.ShowDialog();
        }
    }
}