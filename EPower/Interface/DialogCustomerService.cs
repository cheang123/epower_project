﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogCustomerService : ExDialog
    {
        GeneralProcess _flag;
        TBL_CUSTOMER_SERVICE _objNew = new TBL_CUSTOMER_SERVICE();
        public TBL_CUSTOMER_SERVICE Object
        {
            get { return _objNew; }
        }
        TBL_CUSTOMER_SERVICE _objOld = new TBL_CUSTOMER_SERVICE();

        #region Constructor
        public DialogCustomerService(GeneralProcess flag, TBL_CUSTOMER_SERVICE obj)
        {
            InitializeComponent();
            _flag = flag;
            obj._CopyTo(_objNew);
            obj._CopyTo(_objOld);

            if (flag == GeneralProcess.Insert)
            {
                _objNew.IS_ACTIVE = true;
                this.Text = string.Concat(Resources.INSERT, this.Text);

                var objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == obj.CUSTOMER_ID);
                _objNew.ACTIVATE_MONTH = Method.GetNextBillingMonth(objCustomer.BILLING_CYCLE_ID);
                _objNew.LAST_BILLING_MONTH = _objNew.ACTIVATE_MONTH;
                _objNew.NEXT_BILLING_MONTH = _objNew.ACTIVATE_MONTH;
                _objNew.QTY = 1;
            }
            else if (flag == GeneralProcess.Update)
            {
                this.Text = string.Concat(Resources.UPDATE, this.Text);
                this.dtpACTIVATE_MONTH.Enabled = false;
                this.cboService.Enabled = false;
            }
            else if (flag == GeneralProcess.Delete)
            {
                this.Text = string.Concat(Resources.DELETE, this.Text);
                UIHelper.SetEnabled(this, false);
            }
            this.btnChange_log.Visible = this._flag != GeneralProcess.Insert;
            UIHelper.SetDataSourceToComboBox(this.cboService, DBDataContext.Db.TBL_INVOICE_ITEMs.Where(row => row.IS_RECURRING_SERVICE).Select(row => new { row.INVOICE_ITEM_ID, row.INVOICE_ITEM_NAME }));
            read();
        }
        #endregion

        #region Operation
        /// <summary>
        /// Data transaction
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objNew);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objNew);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }

        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        #endregion

        #region Method
        /// <summary>
        /// Load data from object.
        /// </summary>
        private void read()
        {
            this.cboService.SelectedValue = _objNew.INVOICE_ITEM_ID;
            this.txtQTY.Text = _objNew.QTY.ToString("#");
            this.dtpACTIVATE_MONTH.Value = _objNew.ACTIVATE_MONTH;
            this.dtpLAST_BILLING_MONTH.Value = _objNew.LAST_BILLING_MONTH;
            this.dtpNEXT_BILLING_MONTH.Value = _objNew.NEXT_BILLING_MONTH;
            var objItem = DBDataContext.Db.TBL_INVOICE_ITEMs.FirstOrDefault(row => row.INVOICE_ITEM_ID == _objNew.INVOICE_ITEM_ID);
            if (objItem != null)
            {
                this.txtRECURRING_MONTH.Text = objItem.RECURRING_MONTH.ToString("#");
            }
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            this._objNew.INVOICE_ITEM_ID = (int)this.cboService.SelectedValue;
            this._objNew.QTY = DataHelper.ParseToDecimal(this.txtQTY.Text);
            if (_flag == GeneralProcess.Insert)
            {
                this._objNew.ACTIVATE_MONTH = new DateTime(this.dtpACTIVATE_MONTH.Value.Year, this.dtpACTIVATE_MONTH.Value.Month, 1);
                this._objNew.LAST_BILLING_MONTH = new DateTime(this.dtpACTIVATE_MONTH.Value.Year, this.dtpACTIVATE_MONTH.Value.Month, 1);
                this._objNew.NEXT_BILLING_MONTH = new DateTime(this.dtpACTIVATE_MONTH.Value.Year, this.dtpACTIVATE_MONTH.Value.Month, 1);
                this._objNew.CREATE_BY = Login.CurrentLogin.LOGIN_NAME;
                this._objNew.CREATE_ON = DBDataContext.Db.GetSystemDate();
            }
        }

        private bool inValid()
        {
            bool val = false;
            txtQTY.ClearValidation();
            if (!DataHelper.IsNumber(txtQTY.Text.Trim().ToUpper().Replace("A", "")))
            {
                txtQTY.SetValidation(string.Format(Resources.REQUEST_GRATER_THAN_ZERO, lblQTY.Text));
                val = true;
            }
            return val;
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }
        #endregion

        private void cboService_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cboService.SelectedIndex == -1) return;
            var objItem = DBDataContext.Db.TBL_INVOICE_ITEMs.FirstOrDefault(row => row.INVOICE_ITEM_ID == (int)this.cboService.SelectedValue);
            if (objItem != null)
            {
                this.txtRECURRING_MONTH.Text = objItem.RECURRING_MONTH.ToString("#");
            }
        }

        private void dtpACTIVATE_MONTH_ValueChanged(object sender, EventArgs e)
        {
            if (_flag == GeneralProcess.Insert)
            {
                DateTime month = new DateTime(dtpACTIVATE_MONTH.Value.Year, dtpACTIVATE_MONTH.Value.Month, 1);
                this.dtpLAST_BILLING_MONTH.Value = month;
                this.dtpNEXT_BILLING_MONTH.Value = month;
            }
        }

    }
}