﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogCustomerNoUsage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCustomerNoUsage));
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.CUSTOMER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.METER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AREA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COLLECTOR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMPLOYEE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AREA_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblMONTH = new System.Windows.Forms.Label();
            this.lblCYCLE_NAME = new System.Windows.Forms.Label();
            this.lblAREA = new System.Windows.Forms.Label();
            this.lblCOLLECTOR = new System.Windows.Forms.Label();
            this.cboArea = new System.Windows.Forms.ComboBox();
            this.cboCollector = new System.Windows.Forms.ComboBox();
            this.lblSHOW_ALL = new System.Windows.Forms.Label();
            this.txtNo = new System.Windows.Forms.TextBox();
            this.cboCycle = new System.Windows.Forms.ComboBox();
            this.dtpMonth = new System.Windows.Forms.DateTimePicker();
            this.btnINPUT_USAGE = new SoftTech.Component.ExButton();
            this.txtSearch = new SoftTech.Component.ExTextbox();
            this.lblSEARCH = new System.Windows.Forms.Label();
            this.btnREPORT = new SoftTech.Component.ExButton();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.btnREPORT);
            this.content.Controls.Add(this.lblSEARCH);
            this.content.Controls.Add(this.txtSearch);
            this.content.Controls.Add(this.btnINPUT_USAGE);
            this.content.Controls.Add(this.dtpMonth);
            this.content.Controls.Add(this.cboCycle);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.lblSHOW_ALL);
            this.content.Controls.Add(this.txtNo);
            this.content.Controls.Add(this.cboCollector);
            this.content.Controls.Add(this.cboArea);
            this.content.Controls.Add(this.lblCOLLECTOR);
            this.content.Controls.Add(this.lblAREA);
            this.content.Controls.Add(this.lblCYCLE_NAME);
            this.content.Controls.Add(this.lblMONTH);
            this.content.Controls.Add(this.dgv);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.dgv, 0);
            this.content.Controls.SetChildIndex(this.lblMONTH, 0);
            this.content.Controls.SetChildIndex(this.lblCYCLE_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblAREA, 0);
            this.content.Controls.SetChildIndex(this.lblCOLLECTOR, 0);
            this.content.Controls.SetChildIndex(this.cboArea, 0);
            this.content.Controls.SetChildIndex(this.cboCollector, 0);
            this.content.Controls.SetChildIndex(this.txtNo, 0);
            this.content.Controls.SetChildIndex(this.lblSHOW_ALL, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.cboCycle, 0);
            this.content.Controls.SetChildIndex(this.dtpMonth, 0);
            this.content.Controls.SetChildIndex(this.btnINPUT_USAGE, 0);
            this.content.Controls.SetChildIndex(this.txtSearch, 0);
            this.content.Controls.SetChildIndex(this.lblSEARCH, 0);
            this.content.Controls.SetChildIndex(this.btnREPORT, 0);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CUSTOMER_CODE,
            this.CUSTOMER_NAME,
            this.METER_CODE,
            this.AREA,
            this.COLLECTOR,
            this.EMPLOYEE_ID,
            this.CUSTOMER_ID,
            this.AREA_ID});
            this.dgv.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            // 
            // CUSTOMER_CODE
            // 
            this.CUSTOMER_CODE.DataPropertyName = "CUSTOMER_CODE";
            resources.ApplyResources(this.CUSTOMER_CODE, "CUSTOMER_CODE");
            this.CUSTOMER_CODE.Name = "CUSTOMER_CODE";
            this.CUSTOMER_CODE.ReadOnly = true;
            // 
            // CUSTOMER_NAME
            // 
            this.CUSTOMER_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CUSTOMER_NAME.DataPropertyName = "CUSTOMER_NAME";
            resources.ApplyResources(this.CUSTOMER_NAME, "CUSTOMER_NAME");
            this.CUSTOMER_NAME.Name = "CUSTOMER_NAME";
            this.CUSTOMER_NAME.ReadOnly = true;
            // 
            // METER_CODE
            // 
            this.METER_CODE.DataPropertyName = "METER_CODE";
            resources.ApplyResources(this.METER_CODE, "METER_CODE");
            this.METER_CODE.Name = "METER_CODE";
            this.METER_CODE.ReadOnly = true;
            // 
            // AREA
            // 
            this.AREA.DataPropertyName = "AREA_NAME";
            resources.ApplyResources(this.AREA, "AREA");
            this.AREA.Name = "AREA";
            this.AREA.ReadOnly = true;
            // 
            // COLLECTOR
            // 
            this.COLLECTOR.DataPropertyName = "EMPLOYEE_NAME";
            resources.ApplyResources(this.COLLECTOR, "COLLECTOR");
            this.COLLECTOR.Name = "COLLECTOR";
            this.COLLECTOR.ReadOnly = true;
            // 
            // EMPLOYEE_ID
            // 
            this.EMPLOYEE_ID.DataPropertyName = "EMPLOYEE_ID";
            resources.ApplyResources(this.EMPLOYEE_ID, "EMPLOYEE_ID");
            this.EMPLOYEE_ID.Name = "EMPLOYEE_ID";
            this.EMPLOYEE_ID.ReadOnly = true;
            // 
            // CUSTOMER_ID
            // 
            this.CUSTOMER_ID.DataPropertyName = "CUSTOMER_ID";
            resources.ApplyResources(this.CUSTOMER_ID, "CUSTOMER_ID");
            this.CUSTOMER_ID.Name = "CUSTOMER_ID";
            this.CUSTOMER_ID.ReadOnly = true;
            // 
            // AREA_ID
            // 
            this.AREA_ID.DataPropertyName = "AREA_ID";
            resources.ApplyResources(this.AREA_ID, "AREA_ID");
            this.AREA_ID.Name = "AREA_ID";
            this.AREA_ID.ReadOnly = true;
            // 
            // lblMONTH
            // 
            resources.ApplyResources(this.lblMONTH, "lblMONTH");
            this.lblMONTH.Name = "lblMONTH";
            // 
            // lblCYCLE_NAME
            // 
            resources.ApplyResources(this.lblCYCLE_NAME, "lblCYCLE_NAME");
            this.lblCYCLE_NAME.Name = "lblCYCLE_NAME";
            // 
            // lblAREA
            // 
            resources.ApplyResources(this.lblAREA, "lblAREA");
            this.lblAREA.Name = "lblAREA";
            // 
            // lblCOLLECTOR
            // 
            resources.ApplyResources(this.lblCOLLECTOR, "lblCOLLECTOR");
            this.lblCOLLECTOR.Name = "lblCOLLECTOR";
            // 
            // cboArea
            // 
            this.cboArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboArea.FormattingEnabled = true;
            resources.ApplyResources(this.cboArea, "cboArea");
            this.cboArea.Name = "cboArea";
            this.cboArea.SelectedIndexChanged += new System.EventHandler(this.cboArea_SelectedIndexChanged);
            // 
            // cboCollector
            // 
            this.cboCollector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCollector.FormattingEnabled = true;
            resources.ApplyResources(this.cboCollector, "cboCollector");
            this.cboCollector.Name = "cboCollector";
            this.cboCollector.SelectedIndexChanged += new System.EventHandler(this.cboCollector_SelectedIndexChanged);
            // 
            // lblSHOW_ALL
            // 
            resources.ApplyResources(this.lblSHOW_ALL, "lblSHOW_ALL");
            this.lblSHOW_ALL.Name = "lblSHOW_ALL";
            // 
            // txtNo
            // 
            resources.ApplyResources(this.txtNo, "txtNo");
            this.txtNo.Name = "txtNo";
            this.txtNo.ReadOnly = true;
            // 
            // cboCycle
            // 
            this.cboCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCycle.FormattingEnabled = true;
            resources.ApplyResources(this.cboCycle, "cboCycle");
            this.cboCycle.Name = "cboCycle";
            this.cboCycle.SelectedIndexChanged += new System.EventHandler(this.cboCycle_SelectedIndexChanged);
            // 
            // dtpMonth
            // 
            resources.ApplyResources(this.dtpMonth, "dtpMonth");
            this.dtpMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMonth.Name = "dtpMonth";
            // 
            // btnINPUT_USAGE
            // 
            resources.ApplyResources(this.btnINPUT_USAGE, "btnINPUT_USAGE");
            this.btnINPUT_USAGE.Name = "btnINPUT_USAGE";
            this.btnINPUT_USAGE.UseVisualStyleBackColor = true;
            this.btnINPUT_USAGE.Click += new System.EventHandler(this.btnInputUsage_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.BackColor = System.Drawing.Color.White;
            this.txtSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtSearch, "txtSearch");
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtSearch.QuickSearch += new System.EventHandler(this.txtSearch_QuickSearch);
            this.txtSearch.Enter += new System.EventHandler(this.txtSearch_Enter);
            // 
            // lblSEARCH
            // 
            resources.ApplyResources(this.lblSEARCH, "lblSEARCH");
            this.lblSEARCH.Name = "lblSEARCH";
            // 
            // btnREPORT
            // 
            resources.ApplyResources(this.btnREPORT, "btnREPORT");
            this.btnREPORT.Name = "btnREPORT";
            this.btnREPORT.UseVisualStyleBackColor = true;
            this.btnREPORT.Click += new System.EventHandler(this.btnReport_Click);
            // 
            // DialogCustomerNoUsage
            // 
            resources.ApplyResources(this, "$this");
            this.Name = "DialogCustomerNoUsage";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnCLOSE;
        private DataGridView dgv;
        private Label lblCYCLE_NAME;
        private Label lblMONTH;
        private Label lblCOLLECTOR;
        private Label lblAREA;
        private ComboBox cboCollector;
        private ComboBox cboArea;
        private TextBox txtNo;
        private Label lblSHOW_ALL;
        private DateTimePicker dtpMonth;
        private ComboBox cboCycle;
        private ExButton btnINPUT_USAGE;
        private ExTextbox txtSearch;
        private Label lblSEARCH;
        private ExButton btnREPORT;
        private DataGridViewTextBoxColumn CUSTOMER_CODE;
        private DataGridViewTextBoxColumn CUSTOMER_NAME;
        private DataGridViewTextBoxColumn METER_CODE;
        private DataGridViewTextBoxColumn AREA;
        private DataGridViewTextBoxColumn COLLECTOR;
        private DataGridViewTextBoxColumn EMPLOYEE_ID;
        private DataGridViewTextBoxColumn CUSTOMER_ID;
        private DataGridViewTextBoxColumn AREA_ID;
    }
}
