﻿using EPower.Base.Logic;
using EPower.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Process = System.Diagnostics.Process;

namespace EPower.Interface
{
    public partial class DialogRunBill : ExDialog
    {
        bool load = false;
        int totalDueDate = 0;
        DataTable dt = new DataTable();
        DataRow dr;
        public List<int> cycleIds = new List<int>();
        public DialogRunBill()
        {
            InitializeComponent();
            load = false;

            totalDueDate = DataHelper.ParseToInt(DBDataContext.Db.TBL_UTILITies.FirstOrDefault(x => x.UTILITY_ID == (int)Utility.INVOICE_DUE_DATE).UTILITY_VALUE);
            DateTime now = DBDataContext.Db.GetSystemDate();
            this.dtpDueDate.Value = now.AddDays(totalDueDate);
            dt.Columns.Add("HOLIDAY_ID", typeof(int));
            dt.Columns.Add("HOLIDAY_DATE", typeof(DateTime));
            dt.Columns.Add("HOLIDAY_NAME", typeof(string));
            load = true;

            //Remove duplicated 
            BillLogic.RemoveDuplicatedUsage();
        }

        private void calculateHoliday()
        {
            dt.Rows.Clear();
            DateTime startPayDate = new DateTime(dtpSTART_PAY_DATE.Value.Year, dtpSTART_PAY_DATE.Value.Month, dtpSTART_PAY_DATE.Value.Day);
            DateTime dueDate = new DateTime(dtpDueDate.Value.Year, dtpDueDate.Value.Month, dtpDueDate.Value.Day);
            TimeSpan ts = dueDate - startPayDate;
            double totalDays = ts.TotalDays + 1;

            double holiday = 0;
            for (int i = 0; i < totalDays; i++)
            {
                TBL_HOLIDAY objHoliday = DBDataContext.Db.TBL_HOLIDAYs.FirstOrDefault(x => x.HOLIDAY_DATE == startPayDate.AddDays(i));
                bool isWeekend = startPayDate.AddDays(i).DayOfWeek == DayOfWeek.Saturday || startPayDate.AddDays(i).DayOfWeek == DayOfWeek.Sunday ? true : false;
                if (objHoliday != null || isWeekend)
                {
                    holiday += 1;
                    if (objHoliday != null)
                    {
                        dr = dt.NewRow();
                        dr["HOLIDAY_ID"] = objHoliday.HOLIDAY_ID;
                        dr["HOLIDAY_DATE"] = objHoliday.HOLIDAY_DATE;
                        dr["HOLIDAY_NAME"] = objHoliday.HOLIDAY_NAME;
                        dt.Rows.Add(dr);
                    }
                    else
                    {
                        dr = dt.NewRow();
                        dr["HOLIDAY_ID"] = startPayDate.AddDays(i).DayOfWeek == DayOfWeek.Saturday ? -1 : -2;
                        dr["HOLIDAY_DATE"] = startPayDate.AddDays(i);
                        dr["HOLIDAY_NAME"] = startPayDate.AddDays(i).DayOfWeek == DayOfWeek.Saturday ? Resources.SATURDAY : Resources.SUNDAY;
                        dt.Rows.Add(dr);
                    }
                }
            }
            lblTOTAL_WORK_DAY.Text = string.Format(Resources.MS_TOTAL_WORKING_DAY, totalDays - holiday);
            lblTOTAL_HOLIDAY.Text = string.Format(Resources.MS_TOTAL_HOLIDAY, holiday);
            if (totalDays - holiday < totalDueDate)
            {
                lblTOTAL_WORK_DAY.ForeColor = Color.Red;
                lblTOTAL_WORK_DAY.Font = new Font("Khmer OS System", 9F, FontStyle.Bold);
            }
            else
            {
                lblTOTAL_WORK_DAY.ForeColor = Color.Black;
                lblTOTAL_WORK_DAY.Font = new Font("Khmer OS System", 8.25F);
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            //var mail = new PageReportSubsidy() { };
            //mail.sendMailToEAC(this.dtpInvoice_Date.Value.Date); 
            //int intBillingCycle = (int)this.cboBillingCycle.SelectedValue; 

            this.dtpInvoice_Date.ClearValidation();
            if (string.IsNullOrEmpty(this.dtpInvoice_Date.Text.Trim()))
            {
                this.dtpInvoice_Date.SetValidation(string.Format(Resources.REQUIRED, lblINVOICE_DATE_BILLING.Text));
                return;
            }

            int usd_currencyId = 2;
            int bth_currencyId = 3;
            DateTime datRunMonth = this.dtpRunMonth.Value.Date;
            DateTime datStart = this.dtpFrom.Value.Date;
            DateTime datEnd = this.dtpTo.Value.Date;
            DateTime datDueDate = this.dtpDueDate.Value.Date;
            DateTime datStartPayDate = this.dtpSTART_PAY_DATE.Value.Date;
            DateTime dtpInvoiceDate = this.dtpInvoice_Date.Value.Date;

            var toUsdExchangeRate = new ExchangeRateLogic().findLastExchangeRate(dtpInvoiceDate, usd_currencyId);
            var toBthExchangeRate = new ExchangeRateLogic().findLastExchangeRate(dtpInvoiceDate, bth_currencyId);

            bool isWeekend = dtpInvoiceDate.DayOfWeek == DayOfWeek.Saturday || dtpInvoiceDate.DayOfWeek == DayOfWeek.Sunday ? true : false;
            var objectHoliday = DBDataContext.Db.TBL_HOLIDAYs.OrderByDescending(x => x.HOLIDAY_DATE).FirstOrDefault(x => x.HOLIDAY_DATE <= dtpInvoiceDate);

            var IgnoreStatusids = new List<int>() { 4, 5 };
            var allCurrencyIds = (from c in DBDataContext.Db.TBL_CUSTOMERs.Where(x => cycleIds.Contains(x.BILLING_CYCLE_ID) && !IgnoreStatusids.Contains(x.STATUS_ID))
                                  join p in DBDataContext.Db.TBL_PRICEs on c.PRICE_ID equals p.PRICE_ID
                                  select new { CurrencyId = p.CURRENCY_ID }).ToList();
            allCurrencyIds = allCurrencyIds.Distinct().ToList();

            var allCurrencies = DBDataContext.Db.TLKP_CURRENCies.ToList();
            var defaultCurrency = allCurrencies.FirstOrDefault(x => x.IS_DEFAULT_CURRENCY);
            //CREATE_ON => RateDate 
            foreach (var currencyId in allCurrencyIds.Select(x => x.CurrencyId))
            {
                var currency = allCurrencies.FirstOrDefault(x => x.CURRENCY_ID == currencyId);
                if (currencyId == usd_currencyId)
                {
                    if (toUsdExchangeRate.CREATE_ON.Date != dtpInvoiceDate && !isWeekend && objectHoliday.HOLIDAY_DATE != dtpInvoiceDate)
                    {
                        var message = MsgBox.ShowQuestion(string.Format(Resources.MS_QUESTION_NO_DATA_ABOUT_EXCHANGE_RATE, currency.CURRENCY_NAME, defaultCurrency.CURRENCY_NAME, (dtpInvoice_Date.Value).ToString("dd-MM-yyyy HH:mm:ss")), Resources.WARNING);
                        if (message == DialogResult.Yes)
                        {
                            var dialogExchangeRate = new DialogExchangeRate(dtpInvoice_Date.Value, usd_currencyId);
                            if (dialogExchangeRate.ShowDialog() != DialogResult.OK)
                            {
                                return;
                            }
                        }
                        else if (message != DialogResult.No)
                        {
                            return;
                        }
                    }
                }
                else if (currencyId == bth_currencyId)
                {
                    if (toBthExchangeRate.CREATE_ON.Date != dtpInvoiceDate && !isWeekend && objectHoliday.HOLIDAY_DATE != dtpInvoiceDate)
                    {
                        var message = MsgBox.ShowQuestion(string.Format(Resources.MS_QUESTION_NO_DATA_ABOUT_EXCHANGE_RATE, currency.CURRENCY_NAME, defaultCurrency.CURRENCY_NAME, (dtpInvoice_Date.Value).ToString("dd-MM-yyyy HH:mm:ss")), Resources.WARNING);
                        if (message == DialogResult.Yes)
                        {
                            var dialogExchangeRate = new DialogExchangeRate(dtpInvoice_Date.Value, bth_currencyId);
                            if (dialogExchangeRate.ShowDialog() != DialogResult.OK)
                            {
                                return;
                            }
                        }
                        else if (message != DialogResult.No)
                        {
                            return;
                        }
                    }
                }
            }

            // if run bill is success.
            if (new BillLogic().Runbill(cycleIds, datRunMonth, datStart, datEnd, datDueDate, datStartPayDate, dtpInvoiceDate))
            {
                this.DialogResult = DialogResult.OK;
                this.sendReport();
            }
            else
            {
                //MsgBox.ShowInformation(Resources.MS_RUN_BILL_SUCCESS);
                this.DialogResult = DialogResult.OK;
            }
        }

        private void sendReport()
        {
            //Format: "D:\Backup\RSC Feb\Oyadav\EPowerReporting\ReportAgent.exe" 1
            //Index0 Full name execute: "D:\Backup\RSC Feb\Oyadav\EPowerReporting\ReportAgent.exe"
            //Index1 Argument: 1
            string[] exeSchedule = Method.Utilities[Utility.EXE_SCHEDULE].Split(new char[] { '"' }, StringSplitOptions.RemoveEmptyEntries);

            if (exeSchedule.Length == 2 && File.Exists(exeSchedule[0]))
            {
                Process.Start(exeSchedule[0], exeSchedule[1]);
            }

            MsgBox.ShowInformation(Resources.MS_RUN_BILL_SUCCESS);
            this.DialogResult = DialogResult.OK;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dtpDueDate_ValueChanged(object sender, EventArgs e)
        {
            if (!load) return;
            calculateHoliday();
        }

        private void lblTotalHoliday_Click(object sender, EventArgs e)
        {
            new DialogHolidayOnPaidDate(dt).ShowDialog();
        }

        private void DialogRunBill_Load(object sender, EventArgs e)
        {
            calculateHoliday();
        }

        private void lblEXCHANGE_RATE_Click(object sender, EventArgs e)
        {
            new DialogExchangeRate(dtpInvoice_Date.Value, 2).ShowDialog();
        }

        private void dtpInvoice_Date_ValueChanged(object sender, EventArgs e)
        {
            if (load)
            {
                this.dtpInvoice_Date.CustomFormat = @"dd-MM-yyyy";
            }
        }

        private void dtpInvoice_Date_Enter(object sender, EventArgs e)
        {
            this.dtpInvoice_Date.Select();
            SendKeys.Send("%{DOWN}");
        }
    }
}
