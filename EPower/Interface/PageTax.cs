﻿using EPower.Base.Helper;
using EPower.Base.Logic;
using EPower.Base.Properties;
using HB01.Domain.ListModels;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageTax : Form
    {
        List<Base.Logic.Currency> currencies = new List<Base.Logic.Currency>();
        public PageTax()
        {
            InitializeComponent();
            this.searchControl.Properties.NullValuePrompt = Resources.SEARCH;
            currencies = SettingLogic.Currencies;
            bind();
            colTAX_AMOUNT.FormatNumeric();
        }

        #region Method
        private void bind()
        {
            resAccounts.DataSource = AccountChartHelper.GetAccounts(new AccountSearchParam { });
            resCurrencies.DataSource = currencies;
            var Computations = Enum.GetValues(typeof(TaxComputations)).Cast<Enum>().Select(x => new Base.Helper.DevExpressCustomize.LookUpEditModel
            {
                Name = ResourceHelper.Translate(x.ToString()),
                Id = Convert.ToInt32(x)
            });
            resComputations.DataSource = Computations;
            try
            {
                dgv.DataSource = DBDataContext.Db.TBL_TAXes.Where(x => x.IS_ACTIVE).ToList();
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        #endregion

        #region Operation
        private void btnEdit_Click(object sender, EventArgs e)
        {
            TBL_TAX obj = (TBL_TAX)dgvPaymentMethod.GetFocusedRow();
            if (obj != null)
            {
                DialogTax diag = new DialogTax(obj, GeneralProcess.Update);
                if (diag.ShowDialog() == DialogResult.OK)
                {
                    bind();
                }
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            DialogTax diag = new DialogTax(new TBL_TAX()
            {
                TAX_ID = 0,
                IS_ACTIVE = true,
                CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                CREATE_ON = DBDataContext.Db.GetSystemDate()
            }, GeneralProcess.Insert);

            if (diag.ShowDialog() == DialogResult.OK)
            {
                bind();
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            TBL_TAX obj = (TBL_TAX)dgvPaymentMethod.GetFocusedRow();
            if (obj != null)
            {
                DialogTax diag = new DialogTax(obj, GeneralProcess.Delete);
                if (diag.ShowDialog() == DialogResult.OK)
                {
                    this.bind();
                }
            }
        }

        #endregion

    }
}
