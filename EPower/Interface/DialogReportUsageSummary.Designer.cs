﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogReportUsageSummary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOK = new SoftTech.Component.ExButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.lblCOLUMN_I = new System.Windows.Forms.Label();
            this.lblCOLUMN_II = new System.Windows.Forms.Label();
            this.lblCOLUMN_III = new System.Windows.Forms.Label();
            this.lblCOLUMN_IV = new System.Windows.Forms.Label();
            this.txtFrom1 = new System.Windows.Forms.TextBox();
            this.lblTO = new System.Windows.Forms.Label();
            this.txtTo1 = new System.Windows.Forms.TextBox();
            this.txtTo2 = new System.Windows.Forms.TextBox();
            this.lblTO_1 = new System.Windows.Forms.Label();
            this.txtFrom2 = new System.Windows.Forms.TextBox();
            this.txtTo3 = new System.Windows.Forms.TextBox();
            this.lblTO_2 = new System.Windows.Forms.Label();
            this.txtFrom3 = new System.Windows.Forms.TextBox();
            this.txtTo4 = new System.Windows.Forms.TextBox();
            this.lblTO_3 = new System.Windows.Forms.Label();
            this.txtFrom4 = new System.Windows.Forms.TextBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.txtTo4);
            this.content.Controls.Add(this.lblTO_3);
            this.content.Controls.Add(this.txtFrom4);
            this.content.Controls.Add(this.txtTo3);
            this.content.Controls.Add(this.lblTO_2);
            this.content.Controls.Add(this.txtFrom3);
            this.content.Controls.Add(this.txtTo2);
            this.content.Controls.Add(this.lblTO_1);
            this.content.Controls.Add(this.txtFrom2);
            this.content.Controls.Add(this.txtTo1);
            this.content.Controls.Add(this.lblTO);
            this.content.Controls.Add(this.txtFrom1);
            this.content.Controls.Add(this.lblCOLUMN_IV);
            this.content.Controls.Add(this.lblCOLUMN_III);
            this.content.Controls.Add(this.lblCOLUMN_II);
            this.content.Controls.Add(this.lblCOLUMN_I);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Size = new System.Drawing.Size(371, 231);
            this.content.Text = "\'CONTENT\'";
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.lblCOLUMN_I, 0);
            this.content.Controls.SetChildIndex(this.lblCOLUMN_II, 0);
            this.content.Controls.SetChildIndex(this.lblCOLUMN_III, 0);
            this.content.Controls.SetChildIndex(this.lblCOLUMN_IV, 0);
            this.content.Controls.SetChildIndex(this.txtFrom1, 0);
            this.content.Controls.SetChildIndex(this.lblTO, 0);
            this.content.Controls.SetChildIndex(this.txtTo1, 0);
            this.content.Controls.SetChildIndex(this.txtFrom2, 0);
            this.content.Controls.SetChildIndex(this.lblTO_1, 0);
            this.content.Controls.SetChildIndex(this.txtTo2, 0);
            this.content.Controls.SetChildIndex(this.txtFrom3, 0);
            this.content.Controls.SetChildIndex(this.lblTO_2, 0);
            this.content.Controls.SetChildIndex(this.txtTo3, 0);
            this.content.Controls.SetChildIndex(this.txtFrom4, 0);
            this.content.Controls.SetChildIndex(this.lblTO_3, 0);
            this.content.Controls.SetChildIndex(this.txtTo4, 0);
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnOK.Location = new System.Drawing.Point(212, 204);
            this.btnOK.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "យល់ព្រម";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Location = new System.Drawing.Point(0, 199);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(370, 1);
            this.panel1.TabIndex = 21;
            // 
            // btnCLOSE
            // 
            this.btnCLOSE.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnCLOSE.Location = new System.Drawing.Point(292, 204);
            this.btnCLOSE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.Size = new System.Drawing.Size(75, 23);
            this.btnCLOSE.TabIndex = 2;
            this.btnCLOSE.Text = "បិទ";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblCOLUMN_I
            // 
            this.lblCOLUMN_I.AutoSize = true;
            this.lblCOLUMN_I.Location = new System.Drawing.Point(16, 27);
            this.lblCOLUMN_I.Margin = new System.Windows.Forms.Padding(2);
            this.lblCOLUMN_I.Name = "lblCOLUMN_I";
            this.lblCOLUMN_I.Size = new System.Drawing.Size(61, 19);
            this.lblCOLUMN_I.TabIndex = 22;
            this.lblCOLUMN_I.Text = "ជួរឈរទី១ ";
            // 
            // lblCOLUMN_II
            // 
            this.lblCOLUMN_II.AutoSize = true;
            this.lblCOLUMN_II.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblCOLUMN_II.Location = new System.Drawing.Point(16, 58);
            this.lblCOLUMN_II.Margin = new System.Windows.Forms.Padding(2);
            this.lblCOLUMN_II.Name = "lblCOLUMN_II";
            this.lblCOLUMN_II.Size = new System.Drawing.Size(59, 19);
            this.lblCOLUMN_II.TabIndex = 23;
            this.lblCOLUMN_II.Text = "ជួរឈរទី២";
            // 
            // lblCOLUMN_III
            // 
            this.lblCOLUMN_III.AutoSize = true;
            this.lblCOLUMN_III.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblCOLUMN_III.Location = new System.Drawing.Point(16, 89);
            this.lblCOLUMN_III.Margin = new System.Windows.Forms.Padding(2);
            this.lblCOLUMN_III.Name = "lblCOLUMN_III";
            this.lblCOLUMN_III.Size = new System.Drawing.Size(60, 19);
            this.lblCOLUMN_III.TabIndex = 24;
            this.lblCOLUMN_III.Text = "ជួរឈរទី៣";
            // 
            // lblCOLUMN_IV
            // 
            this.lblCOLUMN_IV.AutoSize = true;
            this.lblCOLUMN_IV.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblCOLUMN_IV.Location = new System.Drawing.Point(16, 120);
            this.lblCOLUMN_IV.Margin = new System.Windows.Forms.Padding(2);
            this.lblCOLUMN_IV.Name = "lblCOLUMN_IV";
            this.lblCOLUMN_IV.Size = new System.Drawing.Size(58, 19);
            this.lblCOLUMN_IV.TabIndex = 25;
            this.lblCOLUMN_IV.Text = "ជួរឈរទី៤";
            // 
            // txtFrom1
            // 
            this.txtFrom1.Location = new System.Drawing.Point(117, 23);
            this.txtFrom1.Margin = new System.Windows.Forms.Padding(2);
            this.txtFrom1.Name = "txtFrom1";
            this.txtFrom1.Size = new System.Drawing.Size(100, 27);
            this.txtFrom1.TabIndex = 26;
            // 
            // lblTO
            // 
            this.lblTO.AutoSize = true;
            this.lblTO.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblTO.Location = new System.Drawing.Point(219, 27);
            this.lblTO.Margin = new System.Windows.Forms.Padding(2);
            this.lblTO.Name = "lblTO";
            this.lblTO.Size = new System.Drawing.Size(29, 19);
            this.lblTO.TabIndex = 27;
            this.lblTO.Text = "ដល់";
            // 
            // txtTo1
            // 
            this.txtTo1.Location = new System.Drawing.Point(249, 23);
            this.txtTo1.Margin = new System.Windows.Forms.Padding(2);
            this.txtTo1.Name = "txtTo1";
            this.txtTo1.Size = new System.Drawing.Size(100, 27);
            this.txtTo1.TabIndex = 28;
            // 
            // txtTo2
            // 
            this.txtTo2.Location = new System.Drawing.Point(249, 54);
            this.txtTo2.Margin = new System.Windows.Forms.Padding(2);
            this.txtTo2.Name = "txtTo2";
            this.txtTo2.Size = new System.Drawing.Size(100, 27);
            this.txtTo2.TabIndex = 31;
            // 
            // lblTO_1
            // 
            this.lblTO_1.AutoSize = true;
            this.lblTO_1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblTO_1.Location = new System.Drawing.Point(219, 58);
            this.lblTO_1.Margin = new System.Windows.Forms.Padding(2);
            this.lblTO_1.Name = "lblTO_1";
            this.lblTO_1.Size = new System.Drawing.Size(29, 19);
            this.lblTO_1.TabIndex = 30;
            this.lblTO_1.Text = "ដល់";
            // 
            // txtFrom2
            // 
            this.txtFrom2.Location = new System.Drawing.Point(117, 54);
            this.txtFrom2.Margin = new System.Windows.Forms.Padding(2);
            this.txtFrom2.Name = "txtFrom2";
            this.txtFrom2.Size = new System.Drawing.Size(100, 27);
            this.txtFrom2.TabIndex = 29;
            // 
            // txtTo3
            // 
            this.txtTo3.Location = new System.Drawing.Point(249, 85);
            this.txtTo3.Margin = new System.Windows.Forms.Padding(2);
            this.txtTo3.Name = "txtTo3";
            this.txtTo3.Size = new System.Drawing.Size(100, 27);
            this.txtTo3.TabIndex = 34;
            // 
            // lblTO_2
            // 
            this.lblTO_2.AutoSize = true;
            this.lblTO_2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblTO_2.Location = new System.Drawing.Point(219, 89);
            this.lblTO_2.Margin = new System.Windows.Forms.Padding(2);
            this.lblTO_2.Name = "lblTO_2";
            this.lblTO_2.Size = new System.Drawing.Size(29, 19);
            this.lblTO_2.TabIndex = 33;
            this.lblTO_2.Text = "ដល់";
            // 
            // txtFrom3
            // 
            this.txtFrom3.Location = new System.Drawing.Point(117, 85);
            this.txtFrom3.Margin = new System.Windows.Forms.Padding(2);
            this.txtFrom3.Name = "txtFrom3";
            this.txtFrom3.Size = new System.Drawing.Size(100, 27);
            this.txtFrom3.TabIndex = 32;
            // 
            // txtTo4
            // 
            this.txtTo4.Location = new System.Drawing.Point(249, 116);
            this.txtTo4.Margin = new System.Windows.Forms.Padding(2);
            this.txtTo4.Name = "txtTo4";
            this.txtTo4.Size = new System.Drawing.Size(100, 27);
            this.txtTo4.TabIndex = 37;
            // 
            // lblTO_3
            // 
            this.lblTO_3.AutoSize = true;
            this.lblTO_3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblTO_3.Location = new System.Drawing.Point(219, 120);
            this.lblTO_3.Margin = new System.Windows.Forms.Padding(2);
            this.lblTO_3.Name = "lblTO_3";
            this.lblTO_3.Size = new System.Drawing.Size(29, 19);
            this.lblTO_3.TabIndex = 36;
            this.lblTO_3.Text = "ដល់";
            // 
            // txtFrom4
            // 
            this.txtFrom4.Location = new System.Drawing.Point(117, 116);
            this.txtFrom4.Margin = new System.Windows.Forms.Padding(2);
            this.txtFrom4.Name = "txtFrom4";
            this.txtFrom4.Size = new System.Drawing.Size(100, 27);
            this.txtFrom4.TabIndex = 35;
            // 
            // DialogReportUsageSummary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(371, 254);
            this.Name = "DialogReportUsageSummary";
            this.Text = "កំណត់ថាមពលដែលត្រូវបង្ហាញ";
            this.TopMost = true;
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label lblCOLUMN_IV;
        private Label lblCOLUMN_III;
        private Label lblCOLUMN_II;
        private Label lblCOLUMN_I;
        private TextBox txtTo4;
        private Label lblTO_3;
        private TextBox txtFrom4;
        private TextBox txtTo3;
        private Label lblTO_2;
        private TextBox txtFrom3;
        private TextBox txtTo2;
        private Label lblTO_1;
        private TextBox txtFrom2;
        private TextBox txtTo1;
        private Label lblTO;
        private TextBox txtFrom1;

    }
}