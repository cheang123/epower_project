﻿using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Logic;

namespace EPower.Interface
{
    public partial class DialogCustomerChangeBreaker : ExDialog
    {
        TBL_CUSTOMER _objCustomer = new TBL_CUSTOMER(); 
        TBL_CUSTOMER_METER _objCustomerMeterOld = new TBL_CUSTOMER_METER();
        TBL_CUSTOMER_METER _objCustomerMeterNew = new TBL_CUSTOMER_METER(); 
        TBL_CIRCUIT_BREAKER _objBreakerOld = null;
        TBL_CIRCUIT_BREAKER _objBreakerNew = null;

        public DialogCustomerChangeBreaker(TBL_CUSTOMER objCustomer)
        {
            InitializeComponent();
            //permission
            this.dtpCHANGE_DATE.Enabled = SoftTech.Security.Logic.Login.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_REGISTER_UPDATE_CIRCUITBREAKER_DATE);

            this._objCustomer = objCustomer;
            this.bind();
            this.read();
        } 

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bind()
        { 
            UIHelper.SetDataSourceToComboBox(this.cboOLD_BREAKER_STATUS, DBDataContext.Db.TLKP_METER_STATUS.Where(s => s.STATUS_ID != (int)MeterStatus.Used));
            this.cboOLD_BREAKER_STATUS.SelectedValue = (int)MeterStatus.Unavailable;
        }

        private void read()
        {
            this.txtCUSTOMER_CODE.Text = this._objCustomer.CUSTOMER_CODE;
            this.txtCUSTOMER_NAME.Text = this._objCustomer.LAST_NAME_KH + " " + this._objCustomer.FIRST_NAME_KH;

             // read customer meter.
            this._objCustomerMeterOld= DBDataContext.Db.TBL_CUSTOMER_METERs.FirstOrDefault(r => r.CUSTOMER_ID == this._objCustomer.CUSTOMER_ID && r.IS_ACTIVE);
            if (this._objCustomerMeterOld != null)
            { 
                this._objBreakerOld = DBDataContext.Db.TBL_CIRCUIT_BREAKERs.FirstOrDefault(m => m.BREAKER_ID == this._objCustomerMeterOld.BREAKER_ID);
                this.txtOLD_BREAKER.Text = this._objBreakerOld.BREAKER_CODE;
                this.txtOLD_BREAKER.AcceptSearch();
            }                                             
        } 

        private bool invalid()
        {
            bool result = false;
            this.ClearAllValidation(); 

            if (this.cboOLD_BREAKER_STATUS.SelectedIndex == -1)
            {
                cboOLD_BREAKER_STATUS.SetValidation(string.Format(Resources.REQUIRED, this.lblBREAKER_STATUS.Text));
                result = true;
            }

            if (this._objBreakerNew == null)
            {
                txtNEW_BREAKER.SetValidation(string.Format(Resources.REQUIRED,this.lblBREAKER_CODE_1.Text));
                result =true ;
            } 
            return result;
        }
        
        private void txtMeter_AdvanceSearch(object sender, EventArgs e)
        {
            if (txtOLD_BREAKER.Text.Trim() == "")
            {
                txtOLD_BREAKER.CancelSearch();
                return ;
            }

            string strMeterCode = txtOLD_BREAKER.Text;
            _objBreakerOld = DBDataContext.Db.TBL_CIRCUIT_BREAKERs.FirstOrDefault(m => m.BREAKER_CODE == strMeterCode);

            if (_objBreakerOld == null)
            {
                
                this.txtOLD_BREAKER.CancelSearch();
                return ;
            }

            TBL_CIRCUIT_BREAKER_TYPE objMeterType = DBDataContext.Db.TBL_CIRCUIT_BREAKER_TYPEs.FirstOrDefault(mt => mt.BREAKER_TYPE_ID == _objBreakerOld.BREAKER_TYPE_ID);
            txtOLD_BREAKER_TYPE.Text = objMeterType.BREAKER_TYPE_NAME;
            txtOLD_BREAKER_AMPHERE.Text = DBDataContext.Db.TBL_AMPAREs.FirstOrDefault(a => a.AMPARE_ID == objMeterType.BREAKER_AMP_ID).AMPARE_NAME;
            txtOLD_BREAKER_PHASE.Text = DBDataContext.Db.TBL_PHASEs.FirstOrDefault(p => p.PHASE_ID == objMeterType.BREAKER_PHASE_ID).PHASE_NAME;
            txtOLD_BREAKER_VOLTAGE.Text = DBDataContext.Db.TBL_VOLTAGEs.FirstOrDefault(v => v.VOLTAGE_ID == objMeterType.BREAKER_VOL_ID).VOLTAGE_NAME;
            txtOLD_BREAKER_CONSTANT.Text = DBDataContext.Db.TBL_CONSTANTs.FirstOrDefault(c => c.CONSTANT_ID == objMeterType.BREAKER_CONST_ID).CONSTANT_NAME;     
        }

        private void dtpActivateDate_ValueChanged(object sender, EventArgs e)
        {

        }

        private void txtNEW_METER_AdvanceSearch(object sender, EventArgs e)
        {
            // if not entry.
            if (txtNEW_BREAKER.Text.Trim() == "")
            {
                txtNEW_BREAKER.CancelSearch();
                return;
            }

            
 
            string strMeterCode = txtNEW_BREAKER.Text;
            TBL_CIRCUIT_BREAKER tmp = DBDataContext.Db.TBL_CIRCUIT_BREAKERs.FirstOrDefault(m => m.BREAKER_CODE.ToLower() == txtNEW_BREAKER.Text.Trim().ToLower());

            // if not contain in database.
            if (tmp == null)
            {
                //if user have permission to add new meter
                if (Login.IsAuthorized(Permission.ADMIN_CIRCUITBREAKER))
                {
                    //if user agree to add new meter
                    if (MsgBox.ShowQuestion(string.Format(Resources.MSQ_CREATE_BREAKER, strMeterCode), "") == DialogResult.Yes)
                    {
                         DialogCircuitBreaker diagMeter = new DialogCircuitBreaker(GeneralProcess.Insert, new TBL_CIRCUIT_BREAKER() { BREAKER_CODE = strMeterCode });
                        diagMeter.ShowDialog();
                        if (diagMeter.DialogResult == DialogResult.OK)
                        {
                            tmp = diagMeter.CircuitBraker;
                        }
                        else
                        {
                            this.txtNEW_BREAKER.CancelSearch();
                            return;
                        }
                    }
                    else
                    {
                        this.txtNEW_BREAKER.CancelSearch();
                        return;
                    }
                }
                else
                {
                    MsgBox.ShowInformation(Resources.MS_BREAKER_NOT_FOUND);
                    this.txtNEW_BREAKER.CancelSearch();
                    return;
                } 
            }

            // if meter is inused.
            if (tmp.STATUS_ID == (int)MeterStatus.Used)
            {
                MsgBox.ShowInformation(Resources.MS_BREAKER_IS_IN_USE);
                this.txtNEW_BREAKER.CancelSearch();
                return;
            }

            // if meter is unavailable.
            if (tmp.STATUS_ID == (int)MeterStatus.Unavailable)
            {
                MsgBox.ShowInformation(Resources.MS_BOX_IS_UNAVAILABLE);
                this.txtNEW_BREAKER.CancelSearch();
                return;
            }

            // meter is selected.
            _objBreakerNew = new TBL_CIRCUIT_BREAKER();
            tmp._CopyTo(_objBreakerNew);

            TBL_CIRCUIT_BREAKER_TYPE objMeterType = DBDataContext.Db.TBL_CIRCUIT_BREAKER_TYPEs.FirstOrDefault(mt => mt.BREAKER_TYPE_ID == _objBreakerNew.BREAKER_TYPE_ID);
            txtNEW_BREAKER_TYPE.Text = objMeterType.BREAKER_TYPE_NAME;
            txtNEW_BREAKER_AMPARE.Text = DBDataContext.Db.TBL_AMPAREs.FirstOrDefault(a => a.AMPARE_ID == objMeterType.BREAKER_AMP_ID).AMPARE_NAME;
            txtNEW_METER_PHASE.Text = DBDataContext.Db.TBL_PHASEs.FirstOrDefault(p => p.PHASE_ID == objMeterType.BREAKER_PHASE_ID).PHASE_NAME;
            txtNEW_BREAKER_VOLTAGE.Text = DBDataContext.Db.TBL_VOLTAGEs.FirstOrDefault(v => v.VOLTAGE_ID == objMeterType.BREAKER_VOL_ID).VOLTAGE_NAME;
            txtNEW_BREAKER_CONSTANT.Text = DBDataContext.Db.TBL_CONSTANTs.FirstOrDefault(c => c.CONSTANT_ID == objMeterType.BREAKER_CONST_ID).CONSTANT_NAME;
            
        }

        private void txtNEW_METER_CancelAdvanceSearch(object sender, EventArgs e)
        {
            txtNEW_BREAKER_TYPE.Text ="";
            txtNEW_BREAKER_AMPARE.Text ="";
            txtNEW_METER_PHASE.Text = "";
            txtNEW_BREAKER_VOLTAGE.Text = "";
            txtNEW_BREAKER_CONSTANT.Text = "";
            _objBreakerNew = null;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            // validate it first.
            if (invalid())
            {
                return;
            }

            this.saveData();
        }

        private void saveData()
        {
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    // TO CHANGE METER OF A CUSTOMER
                    // *********************************************
                    // 1. INSERT NEW CUSTOMER_METER
                    // 2. DELETE OLD CUSTOMER_METER
                    // 3. UPDATE OLD BREAKER STATUS
                    // 4. UPDATE NEW BREAKER STATUS
                    // ********************************************* 
                    // create a blank change log.
                    TBL_CHANGE_LOG log = null;

                    // 1. DELETE OLD CUSTOMER METER
                    DBDataContext.Db.DeleteChild(this._objCustomerMeterOld, this._objCustomer, ref log);
                    

                    // 2. INSERT NEW CUSTOMER METER
                    this._objCustomerMeterNew = new TBL_CUSTOMER_METER()
                    {
                        BOX_ID = this._objCustomerMeterOld.BOX_ID,
                        BREAKER_ID = this._objBreakerNew.BREAKER_ID,
                        CABLE_SHIELD_ID = this._objCustomerMeterOld.CABLE_SHIELD_ID,
                        CUS_METER_ID = 0,
                        CUSTOMER_ID = this._objCustomer.CUSTOMER_ID,
                        IS_ACTIVE = true,
                        METER_ID = this._objCustomerMeterOld.METER_ID,
                        METER_SHIELD_ID = this._objCustomerMeterOld.METER_SHIELD_ID,
                        POLE_ID = this._objCustomerMeterOld.POLE_ID,
                        REMAIN_USAGE =this._objCustomerMeterOld.REMAIN_USAGE,
                        USED_DATE = this.dtpCHANGE_DATE.Value
                    };
                    DBDataContext.Db.InsertChild(this._objCustomerMeterNew, this._objCustomer, ref log);
                     
                    // 3. UPDATE OLD METER STATUS
                    TBL_CIRCUIT_BREAKER tmpOld = new TBL_CIRCUIT_BREAKER();
                    _objBreakerOld._CopyTo(tmpOld);
                    _objBreakerOld.STATUS_ID = (int)this.cboOLD_BREAKER_STATUS.SelectedValue;
                    DBDataContext.Db.Update(tmpOld, _objBreakerOld);
                     
                    // 4. UPDATE NEW METER STATUS
                    TBL_CIRCUIT_BREAKER tmpNew = new TBL_CIRCUIT_BREAKER();
                    _objBreakerNew._CopyTo(tmpNew);
                    _objBreakerNew.STATUS_ID = (int)MeterStatus.Used;
                    DBDataContext.Db.Update(tmpNew, _objBreakerNew);

                    //// log meter status change
                    //DBDataContext.Db.TBL_STOCK_TRANs.InsertOnSubmit(new TBL_STOCK_TRAN()
                    //{
                    //    STOCK_TRAN_TYPE_ID = (int)StockTranType.Use,
                    //    FROM_STOCK_TYPE_ID = (int)StockType.Stock,
                    //    TO_STOCK_TYPE_ID = (int)StockType.Used,
                    //    CREATE_BY = Logic.Login.CurrentLogin.LOGIN_NAME,
                    //    CREATE_ON = DBDataContext.Db.GetSystemDate(),
                    //    ITEM_ID = _objBreakerNew.BREAKER_TYPE_ID,
                    //    REMARK = _objBreakerNew.BREAKER_CODE,
                    //    ITEM_TYPE_ID = (int)StockItemType.Breaker
                    //});
                    //DBDataContext.Db.SubmitChanges();
                    //DBDataContext.Db.TBL_STOCK_TRANs.InsertOnSubmit(new TBL_STOCK_TRAN()
                    //{
                    //    STOCK_TRAN_TYPE_ID = (int)StockTranType.StockIn,
                    //    FROM_STOCK_TYPE_ID = (int)StockType.Used,
                    //    TO_STOCK_TYPE_ID = (int)_objBreakerOld.STATUS_ID,
                    //    CREATE_BY = Logic.Login.CurrentLogin.LOGIN_NAME,
                    //    CREATE_ON = DBDataContext.Db.GetSystemDate(),
                    //    ITEM_ID = _objBreakerOld.BREAKER_TYPE_ID,
                    //    REMARK = _objBreakerOld.BREAKER_CODE,
                    //    ITEM_TYPE_ID = (int)StockItemType.Breaker
                    //});
                    //DBDataContext.Db.SubmitChanges();

                    tran.Complete();
                }

                // if succes than close form.
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void InputEnglish(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        private void InputKhmer(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        } 
    }
}
