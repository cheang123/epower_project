﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface.AnnualReport
{
    public partial class PageAnnalReportViewer : Form
    {
        private string tableCode = "AS01";
        public PageAnnalReportViewer(string tableCode)
        {
            InitializeComponent();
            viewer.DefaultView();

            this.FormBorderStyle = FormBorderStyle.None;
            this.Dock = DockStyle.Fill;

            this.tableCode = tableCode;
            var table = DBDataContext.Db.TBL_TABLEs.FirstOrDefault(x => x.TABLE_CODE == tableCode);
            this.lblBUY_POWER_DETAIL_.Text = table.TABLE_CODE + " " + table.TABLE_NAME;

            ResourceHelper.ApplyResource(this);
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            Runner.Run(delegate ()
            {
                try
                {
                    if (this.chkRUN_BEFFORE_VIEW.Checked)
                    {
                        DBDataContext.Db.CommandTimeout = 3600;
                        DBDataContext.Db.ExecuteCommand(string.Format("IF OBJECT_ID('ANR.RUN_{0}') IS NOT NULL EXEC ANR.RUN_{0} @YEAR_ID=@p0,@CURRENCY_ID=@p1;", tableCode.Replace("AS", "AS_")), PageAnnualReportGroup.YEAR_ID, PageAnnualReportGroup.CURRENCY_ID);

                        // summary result
                        DBDataContext.Db.ExecuteCommand("EXEC ANR.RUN_AS_09 @YEAR_ID=@p0,@CURRENCY_ID=@p1", PageAnnualReportGroup.YEAR_ID, PageAnnualReportGroup.CURRENCY_ID);
                        DBDataContext.Db.ExecuteCommand("EXEC ANR.RUN_AS_10 @YEAR_ID=@p0,@CURRENCY_ID=@p1", PageAnnualReportGroup.YEAR_ID, PageAnnualReportGroup.CURRENCY_ID);
                        DBDataContext.Db.ExecuteCommand("EXEC ANR.RUN_AS_00B @YEAR_ID=@p0,@CURRENCY_ID=@p1", PageAnnualReportGroup.YEAR_ID, PageAnnualReportGroup.CURRENCY_ID);
                    }

                    var currency = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.CURRENCY_ID == PageAnnualReportGroup.CURRENCY_ID);
                    var table = DBDataContext.Db.TBL_TABLEs.FirstOrDefault(x => x.TABLE_CODE == tableCode);
                    var company = DBDataContext.Db.TBL_COMPANies.FirstOrDefault();
                    var year = DBDataContext.Db.TBL_YEARs.FirstOrDefault(x => x.YEAR_ID == PageAnnualReportGroup.YEAR_ID);

                    var cr = new CrystalReportHelper();
                    var reportName = string.Format("REPORT_{0}.rpt", tableCode.Replace("AS", "AS_"));
                    cr.OpenReport(reportName);
                    cr.SetParameter("@CURRENCY_ID", PageAnnualReportGroup.CURRENCY_ID);
                    cr.SetParameter("@YEAR_ID", PageAnnualReportGroup.YEAR_ID);
                    cr.SetParameter("@CURRENCY_NAME", currency.CURRENCY_NAME);
                    cr.SetParameter("@LICENSE_NO", company.LICENSE_NUMBER);
                    cr.SetParameter("@LICENSE_NAME", company.LICENSE_NAME_KH);
                    cr.SetParameter("@YEAR_NAME", year.YEAR_NAME);
                    this.viewer.ReportSource = cr.Report;
                }
                catch (Exception ex)
                {
                    MsgBox.LogError(ex);
                    MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_VIEW_REPORT, Base.Properties.Resources.WARNING);
                }
            });

        }

    }
}
