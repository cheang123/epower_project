﻿using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface.AnnualReport
{
    public partial class PageBussinessPlan : Form
    {
        public PageBussinessPlan()
        {
            InitializeComponent();
            UIHelper.SetDataSourceToComboBox(cboYear, DBDataContext.Db.TBL_YEARs);
            cboYear.SelectedValue = DBDataContext.Db.TBL_YEARs.Max(x => x.YEAR_ID) - 1;
        }

        private void clearData()
        {
            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    var t = (TextBox)c;
                    t.Text = "";
                }
            }
        }

        private void cboYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.clearData();
            var yearId = (int)this.cboYear.SelectedValue;
            var nextYearId = yearId + 1;
            this.lblThisYear_.Text = yearId.ToString().Replace("0", "០").Replace("1", "១")
                                           .Replace("2", "២").Replace("3", "៣")
                                           .Replace("4", "៤").Replace("5", "៥")
                                           .Replace("6", "៦").Replace("7", "៧")
                                           .Replace("8", "៨").Replace("9", "៩");
            this.lblNextYear_.Text = nextYearId.ToString().Replace("0", "០").Replace("1", "១")
                                           .Replace("2", "២").Replace("3", "៣")
                                           .Replace("4", "៤").Replace("5", "៥")
                                           .Replace("6", "៦").Replace("7", "៧")
                                           .Replace("8", "៨").Replace("9", "៩");

            var objLastPlan = DBDataContext.Db.TBL_BUSINESS_PLANs.FirstOrDefault(x => x.IS_ACTIVE && x.YEAR_ID == yearId - 1);
            if (objLastPlan != null)
            {
                this.txtThisYearIncome.Text = objLastPlan.NEXT_YEAR_INCOME.ToString("N2");
                this.txtThisYearExpense.Text = objLastPlan.NEXT_YEAR_EXPENSE.ToString("N2");
                this.updateThisYearProfit();
            }
            var objCurrentPlan = DBDataContext.Db.TBL_BUSINESS_PLANs.FirstOrDefault(x => x.IS_ACTIVE && x.YEAR_ID == yearId);
            if (objCurrentPlan != null)
            {
                this.txtThisYearReason.Text = objCurrentPlan.THIS_YEAR_REASON;
                this.txtThisYearImprovement.Text = objCurrentPlan.THIS_YEAR_IMPROVEMENT;
                this.txtNextYearIncome.Text = objCurrentPlan.NEXT_YEAR_INCOME.ToString("N2");
                this.txtNextYearExpense.Text = objCurrentPlan.NEXT_YEAR_EXPENSE.ToString("N2");
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var yearId = (int)this.cboYear.SelectedValue;
            // save last plan 
            var objLastPlan = DBDataContext.Db.TBL_BUSINESS_PLANs.FirstOrDefault(x => x.IS_ACTIVE && x.YEAR_ID == yearId - 1);
            if (objLastPlan == null)
            {
                objLastPlan = new TBL_BUSINESS_PLAN()
                {
                    IS_ACTIVE = true,
                    THIS_YEAR_REASON = "",
                    THIS_YEAR_IMPROVEMENT = "",
                    YEAR_ID = yearId - 1
                };
                DBDataContext.Db.TBL_BUSINESS_PLANs.InsertOnSubmit(objLastPlan);
                DBDataContext.Db.SubmitChanges();
            }
            objLastPlan.NEXT_YEAR_INCOME = DataHelper.ParseToDecimal(this.txtThisYearIncome.Text);
            objLastPlan.NEXT_YEAR_EXPENSE = DataHelper.ParseToDecimal(this.txtThisYearExpense.Text);
            DBDataContext.Db.SubmitChanges();

            var objCurrentPlan = DBDataContext.Db.TBL_BUSINESS_PLANs.FirstOrDefault(x => x.IS_ACTIVE && x.YEAR_ID == yearId);
            if (objCurrentPlan == null)
            {
                objCurrentPlan = new TBL_BUSINESS_PLAN()
                {
                    IS_ACTIVE = true,
                    THIS_YEAR_REASON = "",
                    THIS_YEAR_IMPROVEMENT = "",
                    YEAR_ID = yearId
                };
                DBDataContext.Db.TBL_BUSINESS_PLANs.InsertOnSubmit(objCurrentPlan);
                DBDataContext.Db.SubmitChanges();
            }
            objCurrentPlan.NEXT_YEAR_INCOME = DataHelper.ParseToDecimal(this.txtNextYearIncome.Text);
            objCurrentPlan.NEXT_YEAR_EXPENSE = DataHelper.ParseToDecimal(this.txtNextYearExpense.Text);
            objCurrentPlan.THIS_YEAR_IMPROVEMENT = this.txtThisYearImprovement.Text;
            objCurrentPlan.THIS_YEAR_REASON = this.txtThisYearReason.Text;
            DBDataContext.Db.SubmitChanges();
            MsgBox.ShowInformation(Resources.SUCCESS);
        }

        private void txtThisYearIncome_TextChanged(object sender, EventArgs e)
        {
            updateThisYearProfit();
        }

        private void txtThisYearExpense_TextChanged(object sender, EventArgs e)
        {
            updateThisYearProfit();
        }
        private void updateThisYearProfit()
        {
            var income = DataHelper.ParseToDecimal(this.txtThisYearIncome.Text);
            var expense = DataHelper.ParseToDecimal(this.txtThisYearExpense.Text);
            var prof = income - expense;
            this.txtThisYearProfit.Text = prof.ToString("N2");
        }
    }
}
