﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.AnnualReport
{
    partial class PageAnnualReportRun_
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblRUN_REPORT = new System.Windows.Forms.Label();
            this.lblANNUAL_REPORT = new System.Windows.Forms.Label();
            this.lblSHOW_ = new System.Windows.Forms.Label();
            this.lblYear = new System.Windows.Forms.Label();
            this.lblCurrency_ = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblTable_ = new System.Windows.Forms.Label();
            this.btnRUN = new SoftTech.Component.ExButton();
            this.SuspendLayout();
            // 
            // lblRUN_REPORT
            // 
            this.lblRUN_REPORT.AutoSize = true;
            this.lblRUN_REPORT.Font = new System.Drawing.Font("Khmer OS Muol Light", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRUN_REPORT.Location = new System.Drawing.Point(126, 110);
            this.lblRUN_REPORT.Name = "lblRUN_REPORT";
            this.lblRUN_REPORT.Size = new System.Drawing.Size(272, 40);
            this.lblRUN_REPORT.TabIndex = 0;
            this.lblRUN_REPORT.Text = "តំណើរការ របាយការណ៍";
            // 
            // lblANNUAL_REPORT
            // 
            this.lblANNUAL_REPORT.AutoSize = true;
            this.lblANNUAL_REPORT.Font = new System.Drawing.Font("Khmer OS System", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblANNUAL_REPORT.Location = new System.Drawing.Point(131, 152);
            this.lblANNUAL_REPORT.Name = "lblANNUAL_REPORT";
            this.lblANNUAL_REPORT.Size = new System.Drawing.Size(130, 24);
            this.lblANNUAL_REPORT.TabIndex = 1;
            this.lblANNUAL_REPORT.Text = "របាយការណ៍ប្រចាំឆ្នាំ";
            // 
            // lblSHOW_
            // 
            this.lblSHOW_.AutoSize = true;
            this.lblSHOW_.Font = new System.Drawing.Font("Khmer OS System", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSHOW_.Location = new System.Drawing.Point(131, 180);
            this.lblSHOW_.Name = "lblSHOW_";
            this.lblSHOW_.Size = new System.Drawing.Size(125, 24);
            this.lblSHOW_.TabIndex = 3;
            this.lblSHOW_.Text = "បង្ហាញជារូបិយបណ្ណ័";
            // 
            // lblYear
            // 
            this.lblYear.AutoSize = true;
            this.lblYear.Font = new System.Drawing.Font("Khmer OS System", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblYear.Location = new System.Drawing.Point(346, 152);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(38, 24);
            this.lblYear.TabIndex = 6;
            this.lblYear.Text = "2015";
            // 
            // lblCurrency_
            // 
            this.lblCurrency_.AutoSize = true;
            this.lblCurrency_.Font = new System.Drawing.Font("Khmer OS System", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrency_.Location = new System.Drawing.Point(346, 180);
            this.lblCurrency_.Name = "lblCurrency_";
            this.lblCurrency_.Size = new System.Drawing.Size(40, 24);
            this.lblCurrency_.TabIndex = 7;
            this.lblCurrency_.Text = "រៀល";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Khmer OS System", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(131, 213);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(125, 24);
            this.label6.TabIndex = 8;
            this.label6.Text = "តារាងត្រូវតំណើរការ";
            // 
            // lblTable_
            // 
            this.lblTable_.AutoSize = true;
            this.lblTable_.Font = new System.Drawing.Font("Khmer OS System", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTable_.Location = new System.Drawing.Point(346, 213);
            this.lblTable_.Name = "lblTable_";
            this.lblTable_.Size = new System.Drawing.Size(42, 24);
            this.lblTable_.TabIndex = 10;
            this.lblTable_.Text = "AS01";
            // 
            // btnRUN
            // 
            this.btnRUN.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRUN.Location = new System.Drawing.Point(275, 250);
            this.btnRUN.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRUN.Name = "btnRUN";
            this.btnRUN.Size = new System.Drawing.Size(102, 23);
            this.btnRUN.TabIndex = 11;
            this.btnRUN.Text = "តំណើរការ";
            this.btnRUN.UseVisualStyleBackColor = true;
            this.btnRUN.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // PageAnnualReportRun_
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(677, 423);
            this.Controls.Add(this.btnRUN);
            this.Controls.Add(this.lblTable_);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblCurrency_);
            this.Controls.Add(this.lblYear);
            this.Controls.Add(this.lblSHOW_);
            this.Controls.Add(this.lblANNUAL_REPORT);
            this.Controls.Add(this.lblRUN_REPORT);
            this.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "PageAnnualReportRun_";
            this.Text = "PageRunReport";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label lblRUN_REPORT;
        private Label lblANNUAL_REPORT;
        private Label lblSHOW_;
        private Label lblYear;
        private Label lblCurrency_;
        private Label label6;
        private Label lblTable_;
        private ExButton btnRUN;
    }
}