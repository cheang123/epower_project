﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using Process = System.Diagnostics.Process;

namespace EPower.Interface.AnnualReport

{
    public partial class PageAnnualReportGroup : Form
    {

        public static int YEAR_ID = 2021;
        public static int CURRENCY_ID = 1;
        public static void InializeReportData ()
        {
            if (CURRENCY_ID == 0)
            {
                CURRENCY_ID = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.IS_DEFAULT_CURRENCY).CURRENCY_ID;
            }
            if (YEAR_ID == 0){
                YEAR_ID = DBDataContext.Db.GetSystemDate().Year;
            } 
        }

        bool loading = true;
        string tableCode = "AS99";
        public PageAnnualReportGroup()
        {
            InitializeComponent();

            
            try
            {
                loading = true;
                UIHelper.SetDataSourceToComboBox(cboYear, DBDataContext.Db.TBL_YEARs.Where(x => x.IS_ACTIVE));
                UIHelper.SetDataSourceToComboBox(cboCurrency, DBDataContext.Db.TLKP_CURRENCies);

                InializeReportData();
                this.cboYear.SelectedValue = YEAR_ID;
                this.cboCurrency.SelectedValue = CURRENCY_ID;

                loading = false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ViewReport(string tableCode)
        { 
            this.cboYear.SelectedValue = YEAR_ID;
            this.cboCurrency.SelectedValue = CURRENCY_ID;
             
            var tables = DBDataContext.Db.TBL_TABLEs
                                         .Where(x => x.IS_ACTIVE && (tableCode == "AS99" || x.TABLE_CODE == tableCode))
                                         .OrderBy(x=> x.TABLE_CODE);

            this.tabControl1.TabPages.Clear();
            foreach (var table in tables)
            {
                var tabPage = new TabPage(table.TABLE_CODE);
                tabControl1.TabPages.Add(tabPage);

                var frm = new PageAnnalReportViewer(table.TABLE_CODE);
                frm.TopLevel = false;
                frm.Show();

                tabPage.Controls.Add(frm);
            }

            this.btnPRINT_ALL.Visible =
                btnRUN_ALL_BEFORE_VIEW.Visible =
                btnEXPORT_TO_EXCEL.Visible = tableCode == "AS99";
        }

        private void btnRunAll_Click(object sender, EventArgs e)
        {
            var tables = DBDataContext.Db.TBL_TABLEs.Where(t => t.IS_ACTIVE && (tableCode == "AS99" || t.TABLE_CODE == tableCode))
                .OrderBy(x=>x.TABLE_CODE);
            var result = false;

            Runner.RunNewThread(delegate()
            {
                DBDataContext.Db.CommandTimeout = 3600;
                foreach (var table in tables)
                {
                    Runner.Instance.Text = string.Format("កំពុងដំណើរការ {0}...", table.TABLE_CODE);
                    Application.DoEvents();

                    DBDataContext.Db.ExecuteCommand("EXEC ANR.RUN_" + table.TABLE_CODE.Replace("AS", "AS_") + " @YEAR_ID=@p0,@CURRENCY_ID=@p1", YEAR_ID, CURRENCY_ID);
                }
                // summary result
                DBDataContext.Db.ExecuteCommand("EXEC ANR.RUN_AS_09 @YEAR_ID=@p0,@CURRENCY_ID=@p1", YEAR_ID, CURRENCY_ID);
                DBDataContext.Db.ExecuteCommand("EXEC ANR.RUN_AS_10 @YEAR_ID=@p0,@CURRENCY_ID=@p1", YEAR_ID, CURRENCY_ID);
                DBDataContext.Db.ExecuteCommand("EXEC ANR.RUN_AS_00B @YEAR_ID=@p0,@CURRENCY_ID=@p1", YEAR_ID, CURRENCY_ID);

                result = true;
            });

            if (result == true)
            {
                MsgBox.ShowInformation("ដំណើរការបញ្ចប់ដោយជោគជ័យ!");
            }
        }

    
        private void btnPrintAll_Click(object sender, EventArgs e)
        {
            var diag = new PrintDialog();
            if (diag.ShowDialog() != DialogResult.OK)
            {
                return;
            }
            var printerName = diag.PrinterSettings.PrinterName;


            // print both Landscape & Portrait require default Printer
            // so 1. find original default printer
            //    2. set default printer as new selected pritner
            //    3. printer report to default printer ("")
            //    4. set default printer to the original
             
            var defaultPrinter = GetDefaultPrinter();
            SetDefaultPrinter(printerName);
            foreach (var table in DBDataContext.Db.TBL_TABLEs.Where(x=>x.IS_ACTIVE).OrderBy(x=>x.TABLE_CODE))
            {
                // printer to default printer
                printReport(table.TABLE_CODE, ""); 
            }
            SetDefaultPrinter(defaultPrinter);
        }

        void printReport(string tableCode,string printerName)
        {
            var currency = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.CURRENCY_ID == CURRENCY_ID);
            var table = DBDataContext.Db.TBL_TABLEs.FirstOrDefault(x => x.TABLE_CODE == tableCode);
            var company = DBDataContext.Db.TBL_COMPANies.FirstOrDefault();
            var year = DBDataContext.Db.TBL_YEARs.FirstOrDefault(x => x.YEAR_ID == YEAR_ID);

            var cr = new CrystalReportHelper();
            
            cr.OpenReport(string.Format("REPORT_{0}.rpt", tableCode.Replace("AS", "AS_")));
            cr.SetParameter("@CURRENCY_ID", CURRENCY_ID);
            cr.SetParameter("@YEAR_ID", YEAR_ID);
            cr.SetParameter("@CURRENCY_NAME", currency.CURRENCY_NAME);
            cr.SetParameter("@LICENSE_NO", company.LICENSE_NUMBER);
            cr.SetParameter("@LICENSE_NAME", company.LICENSE_NAME_KH);
            cr.SetParameter("@YEAR_NAME", year.YEAR_NAME);
            cr.PrintReport(printerName);
            
        }

        private void cboYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!loading)
            {
                YEAR_ID = (int)cboYear.SelectedValue;
            }
        }

        private void cboCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!loading)
            {
                CURRENCY_ID = (int)cboCurrency.SelectedValue;
            }
        }


        [DllImport("Winspool.drv")]
        private static extern bool SetDefaultPrinter(string printerName);

        string GetDefaultPrinter()
        {
            PrinterSettings settings = new PrinterSettings();
            foreach (string printer in PrinterSettings.InstalledPrinters)
            {
                settings.PrinterName = printer;
                if (settings.IsDefaultPrinter)
                    return printer;
            }
            return string.Empty;
        }

        private void btnExportToExcel_Click(object sender, EventArgs e)
        {
            List<string> fileReport = new List<string>();
            var currency = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.CURRENCY_ID == CURRENCY_ID);
            var table = DBDataContext.Db.TBL_TABLEs.FirstOrDefault(x => x.TABLE_CODE == tableCode);
            var company = DBDataContext.Db.TBL_COMPANies.FirstOrDefault();
            var year = DBDataContext.Db.TBL_YEARs.FirstOrDefault(x => x.YEAR_ID == YEAR_ID);
            CrystalReportHelper report = null;
            int i = 0;
            foreach (var itm in DBDataContext.Db.TBL_TABLEs.Where(x => x.IS_ACTIVE).OrderBy(x => x.TABLE_CODE))
            {
                // printer to default printer
                report = new CrystalReportHelper(string.Format("REPORT_{0}.rpt", itm.TABLE_CODE.Replace("AS", "AS_")));
                report.SetParameter("@CURRENCY_ID", CURRENCY_ID);
                report.SetParameter("@YEAR_ID", YEAR_ID);
                report.SetParameter("@CURRENCY_NAME", currency.CURRENCY_NAME);
                report.SetParameter("@LICENSE_NO", company.LICENSE_NUMBER);
                report.SetParameter("@LICENSE_NAME", company.LICENSE_NAME_KH);
                report.SetParameter("@YEAR_NAME", year.YEAR_NAME);
                fileReport.Add(string.Concat(Application.StartupPath, "\\", Settings.Default.PATH_TEMP, string.Format("REPORT_{0}", itm.TABLE_CODE.Replace("AS", "AS_")), ".xls"));
                report.ExportToExcel(fileReport[i]);
                i++;
            }
            bool result = false;
            SaveFileDialog sdig = new SaveFileDialog();
            sdig.Filter = "Excel 2007|*.xlsx|Excel 2003-97|*.xls";
            sdig.FileName = string.Format("Annual Report {0}.xlsx", year.YEAR_ID);
            if (sdig.ShowDialog() == DialogResult.OK)
            {
                result = SoftTech.Helper.ExcelEngine.CombineWorkBooks(sdig.FileName, fileReport.ToArray(), false);
            }
            if (result)
            {
                Process.Start("explorer.exe", "/select," + sdig.FileName);
            }
        }

    }
}
