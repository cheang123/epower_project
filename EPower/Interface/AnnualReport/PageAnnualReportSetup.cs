﻿using System;
using System.Linq;
using System.Windows.Forms;
using SoftTech;
using SoftTech.Helper;

namespace EPower.Interface.AnnualReport
{
    public partial class PageAnnualReportSetup : Form
    {
        bool loading = true;
        public PageAnnualReportSetup()
        {
            InitializeComponent();

            try
            {
                this.loading = true;
                UIHelper.SetDataSourceToComboBox(cboYear, DBDataContext.Db.TBL_YEARs.Where(x => x.IS_ACTIVE));
                UIHelper.SetDataSourceToComboBox(cboCurrency, DBDataContext.Db.TLKP_CURRENCies);
               
                PageAnnualReportGroup.InializeReportData();
                this.cboYear.SelectedValue = PageAnnualReportGroup.YEAR_ID;
                this.cboCurrency.SelectedValue = PageAnnualReportGroup.CURRENCY_ID;

                this.loading = false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateUI()
        {
            this.cboYear.SelectedValue = PageAnnualReportGroup.YEAR_ID;
            this.cboCurrency.SelectedValue = PageAnnualReportGroup.CURRENCY_ID;
        }

        private void cboYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!loading)
            {
                PageAnnualReportGroup.YEAR_ID = (int)cboYear.SelectedValue;
            }
        }

        private void cboCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!loading)
            {
                PageAnnualReportGroup.CURRENCY_ID = (int)cboCurrency.SelectedValue;
            }
        } 
      
    }
}
