﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface.AnnualReport
{
    public partial class DialogAnnualReport : ExDialog
    {
        public DialogAnnualReport()
        {
            InitializeComponent();

            // resize screen
            WindowState = FormWindowState.Normal;
            StartPosition = FormStartPosition.Manual;
            Top = 0;
            Left = 0;
            Height = Screen.GetWorkingArea(this).Height;
            Width = Screen.GetWorkingArea(this).Width;

            // add last,current and next year
            var year = DBDataContext.Db.GetSystemDate().Year;
            addIfNotExist(year - 1);
            addIfNotExist(year);
            addIfNotExist(year + 1);



            var root = new TreeNode
            {
                Tag = new TBL_TREE() {CLASS = "EPower.Interface.AnnualReport.PageAnnualReportSetup"},
                Text = Text,
                ImageKey = "FOLDER",
                SelectedImageKey = "FOLDER"
            };

            this.tvw.Nodes.Add(root);

            foreach (var obj1 in DBDataContext.Db.TBL_TREEs.OrderBy(x => x.TABLE_CODE).Where(x=>x.PARENT_ID==0))
            {
                var n1 = new TreeNode
                {
                    Tag = obj1,
                    Text = obj1.TREE_NAME,
                    ImageKey = obj1.ICON_KEY,
                    SelectedImageKey = obj1.ICON_KEY
                };
                root.Nodes.Add(n1);

                foreach (var obj2 in DBDataContext.Db.TBL_TREEs.Where(x => x.PARENT_ID == obj1.TREE_ID))
                {
                    var n2 = new TreeNode
                    {
                        Tag = obj2,
                        Text = obj2.TREE_NAME,
                        ImageKey = obj2.ICON_KEY,
                        SelectedImageKey = obj2.ICON_KEY
                    };
                    n1.Nodes.Add(n2);
                }
            }
            root.Expand();

        }
        private void addIfNotExist(int year)
        {
            // add current year  
            var objYear = DBDataContext.Db.TBL_YEARs.FirstOrDefault(x => x.YEAR_ID == year);

            var yearKhmer = year.ToString().Replace("0", "០").Replace("1", "១")
                                           .Replace("2", "២").Replace("3", "៣")
                                           .Replace("4", "៤").Replace("5", "៥")
                                           .Replace("6", "៦").Replace("7", "៧")
                                           .Replace("8", "៨").Replace("9", "៩");
            if (objYear != null) return;
            objYear = new TBL_YEAR()
            {
                YEAR_ID = year,
                YEAR_NAME = yearKhmer.ToString(),
                IS_ACTIVE = true
            };
            DBDataContext.Db.TBL_YEARs.InsertOnSubmit(objYear);
            DBDataContext.Db.SubmitChanges();
        }

        private void tvw_AfterSelect(object sender, TreeViewEventArgs e)
        {
            var obj = e.Node.Tag as TBL_TREE;
            if (obj == null)
            {
                return;
            }
           
            if (obj.ICON_KEY == "RUN")
            {
                showPage(typeof(PageAnnualReportRun_), e.Node);
                var pageRunReport = this.currentPage as PageAnnualReportRun_;
                pageRunReport.Update(obj.TABLE_CODE);
                return;
            }

            if (obj.ICON_KEY == "PRINT")
            {
                showPage(typeof(PageAnnualReportGroup), e.Node);
                var pageRunReport = this.currentPage as PageAnnualReportGroup;
                pageRunReport.ViewReport(obj.TABLE_CODE);
                return;
            }

         
            if (string.IsNullOrEmpty(obj.CLASS))
            {
                return;
            }
            if (obj.CLASS.ToLower().Contains("dialog"))
            {
                var type = Type.GetType(obj.CLASS);
                var diag = Activator.CreateInstance(type) as Form;
                diag.ShowDialog(); 
            }
            else
            {
                var type = Type.GetType(obj.CLASS);
                showPage(type,e.Node);

                if (this.currentPage is PageAnnualReportSetup)
                {
                    ((PageAnnualReportSetup)this.currentPage).UpdateUI();
                }
            }
        }


        #region showPage
        private Control currentPage = null;
        private Dictionary<Type, Control> pages = new Dictionary<Type, Control>();
        private void showPage(Type type, object sender)
        {
            if (!this.pages.ContainsKey(type))
            {
                this.pages[type] = (Control)Activator.CreateInstance(type);
                this.pages[type].Size = this.panelContainer.Size;
                if (this.pages[type] is Form)
                {
                    Form frm = (Form)this.pages[type];
                    frm.TopLevel = false;
                    frm.Visible = true;
                    frm.FormBorderStyle = FormBorderStyle.None;
                }
                this.panelContainer.Controls.Add(this.pages[type]);
                this.pages[type].Dock = DockStyle.Fill;
                ResourceHelper.ApplyResource(this.pages[type]);
            }
            if (this.currentPage != this.pages[type])
            {
                this.pages[type].Show();
                if (this.currentPage != null)
                {
                    this.currentPage.Hide();
                }
                this.currentPage = this.pages[type];
            }
            //this.title.Text = ((Control)sender).Text;
        }
        #endregion ShowPage()
    }
}
