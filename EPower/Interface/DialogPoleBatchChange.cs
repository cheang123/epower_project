﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using Login = SoftTech.Security.Logic.Login;

namespace EPower.Interface
{
    public partial class DialogPoleBatchChange : ExDialog
    {
        #region get default collector, biller, cutter
        int collectorId = (from em in DBDataContext.Db.TBL_EMPLOYEEs
                           join p in DBDataContext.Db.TBL_EMPLOYEE_POSITIONs on em.EMPLOYEE_ID equals p.EMPLOYEE_ID
                           where p.POSITION_ID == (int)EmpPosition.Collector
                           && em.IS_ACTIVE
                           orderby em.EMPLOYEE_NAME
                           select new { em.EMPLOYEE_ID }).FirstOrDefault().EMPLOYEE_ID;
        int billerId = (from em in DBDataContext.Db.TBL_EMPLOYEEs
                        join p in DBDataContext.Db.TBL_EMPLOYEE_POSITIONs on em.EMPLOYEE_ID equals p.EMPLOYEE_ID
                        where p.POSITION_ID == (int)EmpPosition.Biller
                        && em.IS_ACTIVE
                        orderby em.EMPLOYEE_NAME
                        select new { em.EMPLOYEE_ID }).FirstOrDefault().EMPLOYEE_ID;
        int cutterId = (from em in DBDataContext.Db.TBL_EMPLOYEEs
                        join p in DBDataContext.Db.TBL_EMPLOYEE_POSITIONs on em.EMPLOYEE_ID equals p.EMPLOYEE_ID
                        where p.POSITION_ID == (int)EmpPosition.Cutter
                        && em.IS_ACTIVE
                        orderby em.EMPLOYEE_NAME
                        select new { em.EMPLOYEE_ID }).FirstOrDefault().EMPLOYEE_ID;

        #endregion


        #region Data 
        bool _loading = false;
        List<int> poles = new List<int>();
        bool isCheckProperties = false;
        #endregion Data

        #region Constructor
        public DialogPoleBatchChange()
        {
            InitializeComponent();

            btnAddArea.Enabled = Login.IsAuthorized(Permission.ADMIN_AREA);
            btnAddCollector.Enabled =
            btnAddCutter.Enabled =
            btnAddBiller.Enabled = Login.IsAuthorized(Permission.ADMIN_EMPLOYEE);
            btnAddTransfo.Enabled = Login.IsAuthorized(Permission.ADMIN_TRANSFORMER);

            read();
            var p = DBDataContext.Db.TBL_POLEs.Where(row => row.IS_ACTIVE == true).FirstOrDefault();
            var v = p.VILLAGE_CODE;
            cboAreaName.SelectedIndex = 1;
            cboCollector.SelectedValue = 1;
            cboCutter.SelectedValue = 1;
            cboBiller.SelectedValue = 1;
            cboTransfo.SelectedIndex = 1;
            this.loadVillage(v);
            this._loading = false;
        }

        #endregion

        #region Method
        public void SetPole(List<int> pole)
        {
            this.poles = pole;
            this.lblPole_.Text = poles.Count.ToString("0");
        }
        void bind()
        {
            bindCutter();

            bindBiller();

            bindCollector();

            bindTransfo();

            bindArea();

            UIHelper.SetDataSourceToComboBox(cboProvince, DBDataContext.Db.TLKP_PROVINCEs, "");
        }
        private void loadVillage(string villageCode)
        {
            this.cboProvince.SelectedIndex = -1;
            this.cboDistrict.SelectedIndex = -1;
            this.cboCommune.SelectedIndex = -1;
            this.cboVillage.SelectedIndex = -1;

            if (DBDataContext.Db.TLKP_VILLAGEs.FirstOrDefault(row => row.VILLAGE_CODE == villageCode) != null)
            {
                string communeCode = DBDataContext.Db.TLKP_VILLAGEs.FirstOrDefault(row => row.VILLAGE_CODE == villageCode).COMMUNE_CODE;
                string districCode = DBDataContext.Db.TLKP_COMMUNEs.FirstOrDefault(row => row.COMMUNE_CODE == communeCode).DISTRICT_CODE;
                string province = DBDataContext.Db.TLKP_DISTRICTs.FirstOrDefault(row => row.DISTRICT_CODE == districCode).PROVINCE_CODE;

                UIHelper.SetDataSourceToComboBox(this.cboDistrict, DBDataContext.Db.TLKP_DISTRICTs.Where(row => row.PROVINCE_CODE == province));
                UIHelper.SetDataSourceToComboBox(this.cboCommune, DBDataContext.Db.TLKP_COMMUNEs.Where(row => row.DISTRICT_CODE == districCode));
                UIHelper.SetDataSourceToComboBox(this.cboVillage, DBDataContext.Db.TLKP_VILLAGEs.Where(row => row.COMMUNE_CODE == communeCode));

                this.cboProvince.SelectedValue = province;
                this.cboDistrict.SelectedValue = districCode;
                this.cboCommune.SelectedValue = communeCode;
                this.cboVillage.SelectedValue = villageCode;
            }
        }

        private void bindArea()
        {
            UIHelper.SetDataSourceToComboBox(cboAreaName, Lookup.GetAreas(), "");
        }

        private void bindTransfo()
        {
            UIHelper.SetDataSourceToComboBox(cboTransfo, Lookup.GetTransformers(), "");
        }

        private void bindCollector()
        {
            UIHelper.SetDataSourceToComboBox(cboCollector, Lookup.GetEmployeeCollectors(), "");
        }

        private void bindBiller()
        {
            UIHelper.SetDataSourceToComboBox(cboBiller, Lookup.GetEmployeeDistributers(), "");
        }

        private void bindCutter()
        {
            UIHelper.SetDataSourceToComboBox(cboCutter, Lookup.GetEmployeeCutters(), "");
        }

        private void read()
        {
            bind();
            cboAreaName.SelectedValue = 0;
        }
        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();
            if (cboAreaName.SelectedIndex == -1)
            {
                cboAreaName.SetValidation(string.Format(Resources.REQUIRED, lblAREA.Text));
                val = true;
            }
            if (cboTransfo.SelectedIndex == -1)
            {
                cboTransfo.SetValidation(string.Format(Resources.REQUIRED, lblTRANSFORMER.Text));
                val = true;
            }
            if (cboCollector.SelectedIndex == -1)
            {
                cboCollector.SelectedValue = collectorId;
                //cboCollector.SetValidation(string.Format(Resources.REQUIRED, lblCOLLECTOR.Text));
                //val = true;
            }
            if (cboBiller.SelectedIndex == -1)
            {
                cboBiller.SelectedValue = billerId;
                //cboBiller.SetValidation(string.Format(Resources.REQUIRED, lblBILLER.Text));
                //val = true;
            }
            if (cboCutter.SelectedIndex == -1)
            {
                cboCutter.SelectedValue = cutterId;
                //cboCutter.SetValidation(string.Format(Resources.REQUIRED, lblCUTTER.Text));
                //val = true;
            }
            return val;
        }
        private void updateInfo()
        {
            using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
            {
                int i = 0;
                foreach (var poleId in this.poles)
                {
                    TBL_POLE objPole = DBDataContext.Db.TBL_POLEs.FirstOrDefault(x => x.POLE_ID == poleId);
                    TBL_POLE objOldPole = new TBL_POLE();
                    objPole._CopyTo(objOldPole);

                    if ((int)cboAreaName.SelectedValue != 0)
                    {
                        objPole.AREA_ID = (int)cboAreaName.SelectedValue;
                    }
                    if ((int)cboTransfo.SelectedValue != 0)
                    {
                        objPole.TRANSFORMER_ID = (int)cboTransfo.SelectedValue;
                    }
                    if ((int)cboBiller.SelectedValue != 0)
                    {
                        objPole.BILLER_ID = (int)cboBiller.SelectedValue;
                    }
                    if ((int)cboCollector.SelectedValue != 0)
                    {
                        objPole.COLLECTOR_ID = (int)cboCollector.SelectedValue;
                    }
                    if ((int)cboCutter.SelectedValue != 0)
                    {
                        objPole.CUTTER_ID = (int)cboCutter.SelectedValue;
                    }
                    if (isCheckProperties)
                    {
                        objPole.IS_PROPERTY = lblOWN.Checked;
                    }
                    if (cboVillage.SelectedValue.ToString() != "")
                    {
                        objPole.VILLAGE_CODE = cboVillage.SelectedValue.ToString();
                    }
                    if (dtpStartDate.Checked)
                    {
                        objPole.STARTED_DATE = dtpStartDate.Value;
                    }
                    if (dtpEndDate.Checked)
                    {
                        objPole.END_DATE = dtpEndDate.Value;
                    }
                    DBDataContext.Db.Update(objOldPole, objPole);
                    if (objPole.AREA_ID != objOldPole.AREA_ID)
                    {
                        var cusInThisPole = from m in DBDataContext.Db.TBL_CUSTOMER_METERs
                                            join b in DBDataContext.Db.TBL_BOXes on m.BOX_ID equals b.BOX_ID
                                            where b.POLE_ID == objPole.POLE_ID && m.IS_ACTIVE
                                            select m.CUSTOMER_ID;
                        foreach (var cid in cusInThisPole)
                        {
                            TBL_CUSTOMER objCus = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == cid);
                            objCus.AREA_ID = objPole.AREA_ID;
                        }
                        DBDataContext.Db.SubmitChanges();
                    }
                    Application.DoEvents();
                    Runner.Instance.Text = string.Format("កំពុងកែប្រែព័ត៌មាន....{0}%", (100 * i++ / poles.Count));
                }
                success = true;
                tran.Complete();
            }
        }

        #endregion
        bool success = false;
        private void btnAdd_Click(object sender, EventArgs e)
        {
            success = false;
            Runner.Run(updateInfo);
            if (success)
            {
                this.DialogResult = DialogResult.OK;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnAddBiller_AddItem(object sender, EventArgs e)
        {
            int intLastCollectorID = 0, intLastCutterId = 0;
            if (cboCollector.SelectedIndex != -1)
            {
                intLastCollectorID = DataHelper.ParseToInt(cboCollector.SelectedValue.ToString());
            }
            if (cboCutter.SelectedIndex != -1)
            {
                intLastCutterId = DataHelper.ParseToInt(cboCutter.SelectedValue.ToString());
            }
            DialogCollector dig = new DialogCollector(new TBL_EMPLOYEE(), EmpPosition.Biller);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                bind();
                cboCutter.SelectedValue = intLastCutterId;
                cboCollector.SelectedValue = intLastCollectorID;
                cboBiller.SelectedValue = dig.Employee.EMPLOYEE_ID;
            }
        }

        private void btnAddCollector_AddItem(object sender, EventArgs e)
        {
            int intLastBillerID = 0, intLastCutterId = 0;
            if (cboBiller.SelectedIndex != -1)
            {
                intLastBillerID = DataHelper.ParseToInt(cboBiller.SelectedValue.ToString());
            }
            if (cboCutter.SelectedIndex != -1)
            {
                intLastCutterId = DataHelper.ParseToInt(cboCutter.SelectedValue.ToString());
            }
            DialogCollector dig = new DialogCollector(new TBL_EMPLOYEE(), EmpPosition.Collector);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                bind();
                cboBiller.SelectedValue = intLastBillerID;
                cboCutter.SelectedValue = intLastCutterId;
                cboCollector.SelectedValue = dig.Employee.EMPLOYEE_ID;
            }
        }

        private void btnAddCutter_AddItem(object sender, EventArgs e)
        {
            int intLastCollectorID = 0, intLastBillerId = 0;
            if (cboCollector.SelectedIndex != -1)
            {
                intLastCollectorID = DataHelper.ParseToInt(cboCollector.SelectedValue.ToString());
            }
            if (cboBiller.SelectedIndex != -1)
            {
                intLastBillerId = DataHelper.ParseToInt(cboBiller.SelectedValue.ToString());
            }
            DialogCollector dig = new DialogCollector(new TBL_EMPLOYEE(), EmpPosition.Cutter);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                bind();

                cboBiller.SelectedValue = intLastBillerId;
                cboCollector.SelectedValue = intLastCollectorID;
                cboCutter.SelectedValue = dig.Employee.EMPLOYEE_ID;
            }
        }

        private void btnAddArea_AddItem(object sender, EventArgs e)
        {
            DialogArea dig = new DialogArea(GeneralProcess.Insert, new TBL_AREA());
            if (dig.ShowDialog() == DialogResult.OK)
            {
                bindArea();
                cboAreaName.SelectedValue = dig.Area.AREA_ID;
            }
        }

        private void btnAddTransfo_AddItem(object sender, EventArgs e)
        {
            DialogTransformer diag = new DialogTransformer(GeneralProcess.Insert, new TBL_TRANSFORMER()
            {
                TRANSFORMER_CODE = string.Empty,
                START_DATE = DBDataContext.Db.GetSystemDate(),
                END_DATE = UIHelper._DefaultDate,
                IS_INUSED = true
            });
            if (diag.ShowDialog() == DialogResult.OK)
            {
                bindTransfo();
                cboTransfo.SelectedValue = diag.Transformer.TRANSFORMER_ID;
            }
        }

        private void dtpEndDate_MouseDown(object sender, MouseEventArgs e)
        {
            if (dtpEndDate.Checked)
            {
                if (dtpEndDate.Value.Date.Equals(UIHelper._DefaultDate))
                {
                    dtpEndDate.SetValue(DBDataContext.Db.GetSystemDate());
                }
                else
                {
                    dtpEndDate.CustomFormat = UIHelper._DefaultDateFormat;
                }
            }
            else
            {
                dtpEndDate.ClearValue();
            }
        }

        private void cboProvince_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!this._loading && cboProvince.SelectedIndex != -1)
            {
                string strProvinceCode = cboProvince.SelectedValue.ToString();
                // distict
                UIHelper.SetDataSourceToComboBox(cboDistrict, DBDataContext.Db.TLKP_DISTRICTs.Where(d => d.PROVINCE_CODE == strProvinceCode));
            }
        }

        private void cboDistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!this._loading && cboProvince.SelectedIndex != -1)
            {
                string strDisCode = cboDistrict.SelectedValue.ToString();
                // communte
                UIHelper.SetDataSourceToComboBox(cboCommune, DBDataContext.Db.TLKP_COMMUNEs.Where(c => c.DISTRICT_CODE == strDisCode));
            }
        }

        private void cboCommune_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!this._loading && cboCommune.SelectedIndex != -1)
            {
                string strComCode = cboCommune.SelectedValue.ToString();
                // village
                UIHelper.SetDataSourceToComboBox(cboVillage, DBDataContext.Db.TLKP_VILLAGEs.Where(v => v.COMMUNE_CODE == strComCode));
            }
        }

        private void lblOWN_CheckedChanged(object sender, EventArgs e)
        {
            isCheckProperties = true;
        }
    }
}