﻿using System;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PageInvoiceItemType : Form
    {        
        public TBL_INVOICE_ITEM_TYPE InvioceItemType
        {
            get
            {
                TBL_INVOICE_ITEM_TYPE objItemType = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    try
                    {
                        int itemTypeID = (int)dgv.SelectedRows[0].Cells[INVOICE_ITEM_TYPE_ID.Name].Value;
                        objItemType = DBDataContext.Db.TBL_INVOICE_ITEM_TYPEs.FirstOrDefault(x => x.INVOICE_ITEM_TYPE_ID == itemTypeID);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objItemType;
            }
        }

        #region Constructor
        public PageInvoiceItemType()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            txt_QuickSearch(null, null);
        }
        #endregion

        #region Operation
        /// <summary>
        /// Add new ampare.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNew_Click(object sender, EventArgs e)
        {

            DialogInvoiceItemType dig = new DialogInvoiceItemType(GeneralProcess.Insert, new TBL_INVOICE_ITEM_TYPE() { INVOICE_ITEM_TYPE_NAME = "" });
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                SetSelectedRow(dig.InvioceItemType.INVOICE_ITEM_TYPE_ID);
                //UIHelper.SelectRow(dgv, dig.InvioceItemType.INVOICE_ITEM_TYPE_ID);
            }
        }
        private void SetSelectedRow(int Id)
        {
            foreach (DataGridViewRow row in dgv.Rows)
            {
                // 0 is the column index
                if (DataHelper.ParseToInt(row.Cells[INVOICE_ITEM_TYPE_ID.Name].Value?.ToString() ?? "0") == Id)
                {
                    row.Selected = true;
                    break;
                }
            }
        }

        /// <summary>
        /// Edit ampare
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                DialogInvoiceItemType dig = new DialogInvoiceItemType(GeneralProcess.Update, InvioceItemType);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    SetSelectedRow(dig.InvioceItemType.INVOICE_ITEM_TYPE_ID);
                    // UIHelper.SelectRow(dgv, dig.InvioceItemType.INVOICE_ITEM_TYPE_ID);
                }
            }
        }

        /// <summary>
        /// Remove ampare
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (IsDeletable())
            {
                DialogInvoiceItemType dig = new DialogInvoiceItemType(GeneralProcess.Delete, InvioceItemType);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.InvioceItemType.INVOICE_ITEM_TYPE_ID - 1);
                }
            }
        }

        /// <summary>
        /// Load data from database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            dgv.DataSource = (from vit in DBDataContext.Db.TBL_INVOICE_ITEM_TYPEs
                             where vit.IS_ACTIVE &&
                             vit.INVOICE_ITEM_TYPE_NAME.ToLower().Contains(txtQuickSearch.Text.ToLower().Trim())
                             select vit).ToList().OrderBy(x=>x.INVOICE_ITEM_TYPE_ID).ToList();
        }

        /// <summary>
        /// Change current keyboard layout to khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }
        #endregion

        #region Method
        private bool IsDeletable()
        {
            //TODO: can delete record.
            bool val = false;
            if (dgv.SelectedRows.Count > 0)
            {
                val = true;
                if (DBDataContext.Db.TBL_INVOICE_ITEMs.Where(x=>x.INVOICE_ITEM_TYPE_ID==InvioceItemType.INVOICE_ITEM_TYPE_ID).Count()>0)
                {
                    MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE);
                    val = false;
                }
            }
            return val;
        }        
        #endregion

       
    }
}
