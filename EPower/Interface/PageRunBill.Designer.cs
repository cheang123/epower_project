﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageRunBill
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageRunBill));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pHeader = new System.Windows.Forms.Panel();
            this.searchControl = new DevExpress.XtraEditors.SearchControl();
            this.dgv = new DevExpress.XtraGrid.GridControl();
            this.dgvBillingCycle = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRowNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCYCLE_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCYCLE_NAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBILLING_MONTH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSTART_DATE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEND_DATE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTOTAL_CUSTOMER = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBILLING_CUSTOMER = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNO_USAGE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnCHECK_USAGE = new SoftTech.Component.ExButton();
            this.btnMETER_UNREGISTER = new SoftTech.Component.ExButton();
            this.btnCUSTOMER_NO_USAGE = new SoftTech.Component.ExButton();
            this.btnRUN_BILL = new SoftTech.Component.ExButton();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.calendarColumn1 = new SoftTech.Component.CalendarColumn();
            this.calendarColumn2 = new SoftTech.Component.CalendarColumn();
            this.dataGridViewTimeColumn1 = new SoftTech.Component.DataGridViewTimeColumn();
            this.pHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchControl.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBillingCycle)).BeginInit();
            this.SuspendLayout();
            // 
            // pHeader
            // 
            this.pHeader.BackColor = System.Drawing.Color.Transparent;
            this.pHeader.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.pHeader.Controls.Add(this.searchControl);
            this.pHeader.Controls.Add(this.btnCHECK_USAGE);
            this.pHeader.Controls.Add(this.btnMETER_UNREGISTER);
            this.pHeader.Controls.Add(this.btnCUSTOMER_NO_USAGE);
            this.pHeader.Controls.Add(this.btnRUN_BILL);
            resources.ApplyResources(this.pHeader, "pHeader");
            this.pHeader.Name = "pHeader";
            // 
            // searchControl
            // 
            this.searchControl.Client = this.dgv;
            resources.ApplyResources(this.searchControl, "searchControl");
            this.searchControl.Name = "searchControl";
            this.searchControl.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("searchControl.Properties.Appearance.Font")));
            this.searchControl.Properties.Appearance.Options.UseFont = true;
            this.searchControl.Properties.AppearanceDisabled.Font = ((System.Drawing.Font)(resources.GetObject("searchControl.Properties.AppearanceDisabled.Font")));
            this.searchControl.Properties.AppearanceDisabled.Options.UseFont = true;
            this.searchControl.Properties.AppearanceDropDown.Font = ((System.Drawing.Font)(resources.GetObject("searchControl.Properties.AppearanceDropDown.Font")));
            this.searchControl.Properties.AppearanceDropDown.Options.UseFont = true;
            this.searchControl.Properties.AppearanceFocused.Font = ((System.Drawing.Font)(resources.GetObject("searchControl.Properties.AppearanceFocused.Font")));
            this.searchControl.Properties.AppearanceFocused.Options.UseFont = true;
            this.searchControl.Properties.AppearanceItemDisabled.Font = ((System.Drawing.Font)(resources.GetObject("searchControl.Properties.AppearanceItemDisabled.Font")));
            this.searchControl.Properties.AppearanceItemDisabled.Options.UseFont = true;
            this.searchControl.Properties.AppearanceItemHighlight.Font = ((System.Drawing.Font)(resources.GetObject("searchControl.Properties.AppearanceItemHighlight.Font")));
            this.searchControl.Properties.AppearanceItemHighlight.Options.UseFont = true;
            this.searchControl.Properties.AppearanceItemSelected.Font = ((System.Drawing.Font)(resources.GetObject("searchControl.Properties.AppearanceItemSelected.Font")));
            this.searchControl.Properties.AppearanceItemSelected.Options.UseFont = true;
            this.searchControl.Properties.AppearanceReadOnly.Font = ((System.Drawing.Font)(resources.GetObject("searchControl.Properties.AppearanceReadOnly.Font")));
            this.searchControl.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.searchControl.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Repository.ClearButton(),
            new DevExpress.XtraEditors.Repository.SearchButton()});
            this.searchControl.Properties.Client = this.dgv;
            this.searchControl.Properties.DropDownRows = 10;
            this.searchControl.Properties.Padding = new System.Windows.Forms.Padding(1);
            // 
            // dgv
            // 
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.EmbeddedNavigator.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("dgv.EmbeddedNavigator.Appearance.Font")));
            this.dgv.EmbeddedNavigator.Appearance.Options.UseFont = true;
            this.dgv.MainView = this.dgvBillingCycle;
            this.dgv.Name = "dgv";
            this.dgv.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.dgvBillingCycle});
            // 
            // dgvBillingCycle
            // 
            this.dgvBillingCycle.Appearance.ColumnFilterButton.Font = ((System.Drawing.Font)(resources.GetObject("dgvBillingCycle.Appearance.ColumnFilterButton.Font")));
            this.dgvBillingCycle.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.dgvBillingCycle.Appearance.ColumnFilterButtonActive.Font = ((System.Drawing.Font)(resources.GetObject("dgvBillingCycle.Appearance.ColumnFilterButtonActive.Font")));
            this.dgvBillingCycle.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.dgvBillingCycle.Appearance.CustomizationFormHint.Font = ((System.Drawing.Font)(resources.GetObject("dgvBillingCycle.Appearance.CustomizationFormHint.Font")));
            this.dgvBillingCycle.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.dgvBillingCycle.Appearance.DetailTip.Font = ((System.Drawing.Font)(resources.GetObject("dgvBillingCycle.Appearance.DetailTip.Font")));
            this.dgvBillingCycle.Appearance.DetailTip.Options.UseFont = true;
            this.dgvBillingCycle.Appearance.Empty.Font = ((System.Drawing.Font)(resources.GetObject("dgvBillingCycle.Appearance.Empty.Font")));
            this.dgvBillingCycle.Appearance.Empty.Options.UseFont = true;
            this.dgvBillingCycle.Appearance.EvenRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvBillingCycle.Appearance.EvenRow.Font")));
            this.dgvBillingCycle.Appearance.EvenRow.Options.UseFont = true;
            this.dgvBillingCycle.Appearance.FilterCloseButton.Font = ((System.Drawing.Font)(resources.GetObject("dgvBillingCycle.Appearance.FilterCloseButton.Font")));
            this.dgvBillingCycle.Appearance.FilterCloseButton.Options.UseFont = true;
            this.dgvBillingCycle.Appearance.FilterPanel.Font = ((System.Drawing.Font)(resources.GetObject("dgvBillingCycle.Appearance.FilterPanel.Font")));
            this.dgvBillingCycle.Appearance.FilterPanel.Options.UseFont = true;
            this.dgvBillingCycle.Appearance.FixedLine.Font = ((System.Drawing.Font)(resources.GetObject("dgvBillingCycle.Appearance.FixedLine.Font")));
            this.dgvBillingCycle.Appearance.FixedLine.Options.UseFont = true;
            this.dgvBillingCycle.Appearance.FocusedCell.Font = ((System.Drawing.Font)(resources.GetObject("dgvBillingCycle.Appearance.FocusedCell.Font")));
            this.dgvBillingCycle.Appearance.FocusedCell.Options.UseFont = true;
            this.dgvBillingCycle.Appearance.FocusedRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvBillingCycle.Appearance.FocusedRow.Font")));
            this.dgvBillingCycle.Appearance.FocusedRow.Options.UseFont = true;
            this.dgvBillingCycle.Appearance.FooterPanel.Font = ((System.Drawing.Font)(resources.GetObject("dgvBillingCycle.Appearance.FooterPanel.Font")));
            this.dgvBillingCycle.Appearance.FooterPanel.Options.UseFont = true;
            this.dgvBillingCycle.Appearance.GroupButton.Font = ((System.Drawing.Font)(resources.GetObject("dgvBillingCycle.Appearance.GroupButton.Font")));
            this.dgvBillingCycle.Appearance.GroupButton.Options.UseFont = true;
            this.dgvBillingCycle.Appearance.GroupFooter.Font = ((System.Drawing.Font)(resources.GetObject("dgvBillingCycle.Appearance.GroupFooter.Font")));
            this.dgvBillingCycle.Appearance.GroupFooter.Options.UseFont = true;
            this.dgvBillingCycle.Appearance.GroupPanel.Font = ((System.Drawing.Font)(resources.GetObject("dgvBillingCycle.Appearance.GroupPanel.Font")));
            this.dgvBillingCycle.Appearance.GroupPanel.Options.UseFont = true;
            this.dgvBillingCycle.Appearance.GroupRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvBillingCycle.Appearance.GroupRow.Font")));
            this.dgvBillingCycle.Appearance.GroupRow.Options.UseFont = true;
            this.dgvBillingCycle.Appearance.HeaderPanel.Font = ((System.Drawing.Font)(resources.GetObject("dgvBillingCycle.Appearance.HeaderPanel.Font")));
            this.dgvBillingCycle.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.dgvBillingCycle.Appearance.HeaderPanel.Options.UseFont = true;
            this.dgvBillingCycle.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.dgvBillingCycle.Appearance.HideSelectionRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvBillingCycle.Appearance.HideSelectionRow.Font")));
            this.dgvBillingCycle.Appearance.HideSelectionRow.Options.UseFont = true;
            this.dgvBillingCycle.Appearance.HorzLine.Font = ((System.Drawing.Font)(resources.GetObject("dgvBillingCycle.Appearance.HorzLine.Font")));
            this.dgvBillingCycle.Appearance.HorzLine.Options.UseFont = true;
            this.dgvBillingCycle.Appearance.HotTrackedRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvBillingCycle.Appearance.HotTrackedRow.Font")));
            this.dgvBillingCycle.Appearance.HotTrackedRow.Options.UseFont = true;
            this.dgvBillingCycle.Appearance.OddRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvBillingCycle.Appearance.OddRow.Font")));
            this.dgvBillingCycle.Appearance.OddRow.Options.UseFont = true;
            this.dgvBillingCycle.Appearance.Preview.Font = ((System.Drawing.Font)(resources.GetObject("dgvBillingCycle.Appearance.Preview.Font")));
            this.dgvBillingCycle.Appearance.Preview.Options.UseFont = true;
            this.dgvBillingCycle.Appearance.Row.Font = ((System.Drawing.Font)(resources.GetObject("dgvBillingCycle.Appearance.Row.Font")));
            this.dgvBillingCycle.Appearance.Row.Options.UseFont = true;
            this.dgvBillingCycle.Appearance.RowSeparator.Font = ((System.Drawing.Font)(resources.GetObject("dgvBillingCycle.Appearance.RowSeparator.Font")));
            this.dgvBillingCycle.Appearance.RowSeparator.Options.UseFont = true;
            this.dgvBillingCycle.Appearance.SelectedRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvBillingCycle.Appearance.SelectedRow.Font")));
            this.dgvBillingCycle.Appearance.SelectedRow.Options.UseFont = true;
            this.dgvBillingCycle.Appearance.TopNewRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvBillingCycle.Appearance.TopNewRow.Font")));
            this.dgvBillingCycle.Appearance.TopNewRow.Options.UseFont = true;
            this.dgvBillingCycle.Appearance.VertLine.Font = ((System.Drawing.Font)(resources.GetObject("dgvBillingCycle.Appearance.VertLine.Font")));
            this.dgvBillingCycle.Appearance.VertLine.Options.UseFont = true;
            this.dgvBillingCycle.Appearance.ViewCaption.Font = ((System.Drawing.Font)(resources.GetObject("dgvBillingCycle.Appearance.ViewCaption.Font")));
            this.dgvBillingCycle.Appearance.ViewCaption.Options.UseFont = true;
            this.dgvBillingCycle.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRowNo,
            this.colCYCLE_ID,
            this.colCYCLE_NAME,
            this.colBILLING_MONTH,
            this.colSTART_DATE,
            this.colEND_DATE,
            this.colTOTAL_CUSTOMER,
            this.colBILLING_CUSTOMER,
            this.colNO_USAGE});
            this.dgvBillingCycle.GridControl = this.dgv;
            this.dgvBillingCycle.Name = "dgvBillingCycle";
            this.dgvBillingCycle.OptionsCustomization.AllowColumnMoving = false;
            this.dgvBillingCycle.OptionsCustomization.AllowGroup = false;
            this.dgvBillingCycle.OptionsMenu.EnableFooterMenu = false;
            this.dgvBillingCycle.OptionsView.ShowGroupPanel = false;
            // 
            // colRowNo
            // 
            resources.ApplyResources(this.colRowNo, "colRowNo");
            this.colRowNo.Name = "colRowNo";
            // 
            // colCYCLE_ID
            // 
            resources.ApplyResources(this.colCYCLE_ID, "colCYCLE_ID");
            this.colCYCLE_ID.FieldName = "CYCLE_ID";
            this.colCYCLE_ID.Name = "colCYCLE_ID";
            // 
            // colCYCLE_NAME
            // 
            resources.ApplyResources(this.colCYCLE_NAME, "colCYCLE_NAME");
            this.colCYCLE_NAME.FieldName = "CYCLE_NAME";
            this.colCYCLE_NAME.Name = "colCYCLE_NAME";
            // 
            // colBILLING_MONTH
            // 
            resources.ApplyResources(this.colBILLING_MONTH, "colBILLING_MONTH");
            this.colBILLING_MONTH.DisplayFormat.FormatString = "MM-yyyy";
            this.colBILLING_MONTH.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colBILLING_MONTH.FieldName = "BILLING_MONTH";
            this.colBILLING_MONTH.Name = "colBILLING_MONTH";
            // 
            // colSTART_DATE
            // 
            resources.ApplyResources(this.colSTART_DATE, "colSTART_DATE");
            this.colSTART_DATE.DisplayFormat.FormatString = "dd-MM-yyyy";
            this.colSTART_DATE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colSTART_DATE.FieldName = "START_DATE";
            this.colSTART_DATE.Name = "colSTART_DATE";
            // 
            // colEND_DATE
            // 
            resources.ApplyResources(this.colEND_DATE, "colEND_DATE");
            this.colEND_DATE.DisplayFormat.FormatString = "dd-MM-yyyy";
            this.colEND_DATE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colEND_DATE.FieldName = "END_DATE";
            this.colEND_DATE.Name = "colEND_DATE";
            // 
            // colTOTAL_CUSTOMER
            // 
            resources.ApplyResources(this.colTOTAL_CUSTOMER, "colTOTAL_CUSTOMER");
            this.colTOTAL_CUSTOMER.FieldName = "TOTAL_CUSTOMER";
            this.colTOTAL_CUSTOMER.Name = "colTOTAL_CUSTOMER";
            // 
            // colBILLING_CUSTOMER
            // 
            resources.ApplyResources(this.colBILLING_CUSTOMER, "colBILLING_CUSTOMER");
            this.colBILLING_CUSTOMER.FieldName = "BILLING_CUSTOMER";
            this.colBILLING_CUSTOMER.Name = "colBILLING_CUSTOMER";
            // 
            // colNO_USAGE
            // 
            resources.ApplyResources(this.colNO_USAGE, "colNO_USAGE");
            this.colNO_USAGE.FieldName = "NO_USAGE";
            this.colNO_USAGE.Name = "colNO_USAGE";
            // 
            // btnCHECK_USAGE
            // 
            resources.ApplyResources(this.btnCHECK_USAGE, "btnCHECK_USAGE");
            this.btnCHECK_USAGE.Name = "btnCHECK_USAGE";
            this.btnCHECK_USAGE.UseVisualStyleBackColor = true;
            this.btnCHECK_USAGE.Click += new System.EventHandler(this.btnSpecialUsage_Click);
            // 
            // btnMETER_UNREGISTER
            // 
            resources.ApplyResources(this.btnMETER_UNREGISTER, "btnMETER_UNREGISTER");
            this.btnMETER_UNREGISTER.Name = "btnMETER_UNREGISTER";
            this.btnMETER_UNREGISTER.UseVisualStyleBackColor = true;
            this.btnMETER_UNREGISTER.Click += new System.EventHandler(this.btnViewUnregister_Click);
            // 
            // btnCUSTOMER_NO_USAGE
            // 
            resources.ApplyResources(this.btnCUSTOMER_NO_USAGE, "btnCUSTOMER_NO_USAGE");
            this.btnCUSTOMER_NO_USAGE.Name = "btnCUSTOMER_NO_USAGE";
            this.btnCUSTOMER_NO_USAGE.UseVisualStyleBackColor = true;
            this.btnCUSTOMER_NO_USAGE.Click += new System.EventHandler(this.btnCheck_Click);
            // 
            // btnRUN_BILL
            // 
            resources.ApplyResources(this.btnRUN_BILL, "btnRUN_BILL");
            this.btnRUN_BILL.Name = "btnRUN_BILL";
            this.btnRUN_BILL.UseVisualStyleBackColor = true;
            this.btnRUN_BILL.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "CYCLE_ID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn1, "dataGridViewTextBoxColumn1");
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "CYCLE_NAME";
            resources.ApplyResources(this.dataGridViewTextBoxColumn2, "dataGridViewTextBoxColumn2");
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "BILLING_MONTH";
            dataGridViewCellStyle9.Format = "ថ្ងៃទី dd - ខែ MM - ឆ្នាំ yyyy";
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle9;
            resources.ApplyResources(this.dataGridViewTextBoxColumn3, "dataGridViewTextBoxColumn3");
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "END_DATE";
            dataGridViewCellStyle10.Format = "ថ្ងៃទី 0";
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle10;
            resources.ApplyResources(this.dataGridViewTextBoxColumn4, "dataGridViewTextBoxColumn4");
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "COLLECT_DAY";
            dataGridViewCellStyle11.Format = "0 នាក់";
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle11;
            resources.ApplyResources(this.dataGridViewTextBoxColumn5, "dataGridViewTextBoxColumn5");
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "TOTAL_CUSTOMER";
            dataGridViewCellStyle12.Format = "0 នាក់";
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle12;
            resources.ApplyResources(this.dataGridViewTextBoxColumn6, "dataGridViewTextBoxColumn6");
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "NO_USAGE";
            dataGridViewCellStyle13.Format = "0 នាក់";
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle13;
            resources.ApplyResources(this.dataGridViewTextBoxColumn7, "dataGridViewTextBoxColumn7");
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "NO_USAGE";
            dataGridViewCellStyle14.Format = "0 នាក់";
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle14;
            resources.ApplyResources(this.dataGridViewTextBoxColumn8, "dataGridViewTextBoxColumn8");
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // calendarColumn1
            // 
            this.calendarColumn1.DataPropertyName = "BILLING_MONTH";
            dataGridViewCellStyle15.Format = "ខែ MM - ឆ្នាំ yyyy";
            this.calendarColumn1.DefaultCellStyle = dataGridViewCellStyle15;
            resources.ApplyResources(this.calendarColumn1, "calendarColumn1");
            this.calendarColumn1.Name = "calendarColumn1";
            this.calendarColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.calendarColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // calendarColumn2
            // 
            dataGridViewCellStyle16.Format = "ថ្ងៃទី dd - ខែ MM - ឆ្នាំ yyyy";
            this.calendarColumn2.DefaultCellStyle = dataGridViewCellStyle16;
            resources.ApplyResources(this.calendarColumn2, "calendarColumn2");
            this.calendarColumn2.Name = "calendarColumn2";
            this.calendarColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.calendarColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewTimeColumn1
            // 
            resources.ApplyResources(this.dataGridViewTimeColumn1, "dataGridViewTimeColumn1");
            this.dataGridViewTimeColumn1.Name = "dataGridViewTimeColumn1";
            this.dataGridViewTimeColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTimeColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // PageRunBill
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.pHeader);
            this.Name = "PageRunBill";
            this.pHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.searchControl.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBillingCycle)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel pHeader;
        private ExButton btnRUN_BILL;
        private ExButton btnCUSTOMER_NO_USAGE;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTimeColumn dataGridViewTimeColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private CalendarColumn calendarColumn1;
        private CalendarColumn calendarColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private ExButton btnMETER_UNREGISTER;
        private ExButton btnCHECK_USAGE;
        private DevExpress.XtraGrid.GridControl dgv;
        private DevExpress.XtraGrid.Views.Grid.GridView dgvBillingCycle;
        private DevExpress.XtraGrid.Columns.GridColumn colCYCLE_ID;
        private DevExpress.XtraGrid.Columns.GridColumn colCYCLE_NAME;
        private DevExpress.XtraGrid.Columns.GridColumn colBILLING_MONTH;
        private DevExpress.XtraGrid.Columns.GridColumn colSTART_DATE;
        private DevExpress.XtraGrid.Columns.GridColumn colEND_DATE;
        private DevExpress.XtraGrid.Columns.GridColumn colRowNo;
        private DevExpress.XtraGrid.Columns.GridColumn colTOTAL_CUSTOMER;
        private DevExpress.XtraGrid.Columns.GridColumn colBILLING_CUSTOMER;
        private DevExpress.XtraGrid.Columns.GridColumn colNO_USAGE;
        private DevExpress.XtraEditors.SearchControl searchControl;
    }
}
