﻿using System.ComponentModel;
using System.Windows.Forms;

namespace EPower
{
    partial class DialogUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDownloading = new System.Windows.Forms.ProgressBar();
            this.btnCANCEL = new System.Windows.Forms.Button();
            this.lblDOWNLOADING = new System.Windows.Forms.Label();
            this.pbAds = new System.Windows.Forms.PictureBox();
            this._lblDownloaded = new System.Windows.Forms.Label();
            this._lblrootname = new System.Windows.Forms.Label();
            this._lblFileSize = new System.Windows.Forms.Label();
            this._lblTransferRate = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAds)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this._lblTransferRate);
            this.content.Controls.Add(this._lblrootname);
            this.content.Controls.Add(this._lblFileSize);
            this.content.Controls.Add(this._lblDownloaded);
            this.content.Controls.Add(this.pbAds);
            this.content.Controls.Add(this.lblDOWNLOADING);
            this.content.Controls.Add(this.btnDownloading);
            this.content.Controls.Add(this.btnCANCEL);
            this.content.Size = new System.Drawing.Size(545, 370);
            this.content.Text = "\'CONTENT\'";
            this.content.Controls.SetChildIndex(this.btnCANCEL, 0);
            this.content.Controls.SetChildIndex(this.btnDownloading, 0);
            this.content.Controls.SetChildIndex(this.lblDOWNLOADING, 0);
            this.content.Controls.SetChildIndex(this.pbAds, 0);
            this.content.Controls.SetChildIndex(this._lblDownloaded, 0);
            this.content.Controls.SetChildIndex(this._lblFileSize, 0);
            this.content.Controls.SetChildIndex(this._lblrootname, 0);
            this.content.Controls.SetChildIndex(this._lblTransferRate, 0);
            // 
            // btnDownloading
            // 
            this.btnDownloading.Location = new System.Drawing.Point(13, 300);
            this.btnDownloading.MarqueeAnimationSpeed = 10;
            this.btnDownloading.Name = "btnDownloading";
            this.btnDownloading.Size = new System.Drawing.Size(522, 10);
            this.btnDownloading.Step = 2;
            this.btnDownloading.TabIndex = 1;
            // 
            // btnCANCEL
            // 
            this.btnCANCEL.Location = new System.Drawing.Point(467, 339);
            this.btnCANCEL.Name = "btnCANCEL";
            this.btnCANCEL.Size = new System.Drawing.Size(69, 26);
            this.btnCANCEL.TabIndex = 6;
            this.btnCANCEL.Text = "បោះបង់";
            this.btnCANCEL.UseVisualStyleBackColor = true;
            this.btnCANCEL.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblDOWNLOADING
            // 
            this.lblDOWNLOADING.AutoSize = true;
            this.lblDOWNLOADING.Location = new System.Drawing.Point(11, 12);
            this.lblDOWNLOADING.Name = "lblDOWNLOADING";
            this.lblDOWNLOADING.Size = new System.Drawing.Size(95, 19);
            this.lblDOWNLOADING.TabIndex = 7;
            this.lblDOWNLOADING.Text = "កំពុងទាញយក . . .";
            // 
            // pbAds
            // 
            this.pbAds.Location = new System.Drawing.Point(12, 38);
            this.pbAds.Name = "pbAds";
            this.pbAds.Size = new System.Drawing.Size(520, 234);
            this.pbAds.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbAds.TabIndex = 12;
            this.pbAds.TabStop = false;
            this.pbAds.LoadCompleted += new System.ComponentModel.AsyncCompletedEventHandler(this.pbAds_LoadCompleted);
            // 
            // _lblDownloaded
            // 
            this._lblDownloaded.ForeColor = System.Drawing.Color.Black;
            this._lblDownloaded.Location = new System.Drawing.Point(15, 312);
            this._lblDownloaded.Name = "_lblDownloaded";
            this._lblDownloaded.Size = new System.Drawing.Size(318, 26);
            this._lblDownloaded.TabIndex = 15;
            this._lblDownloaded.Text = "0.00M(0%)";
            this._lblDownloaded.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _lblrootname
            // 
            this._lblrootname.AutoSize = true;
            this._lblrootname.ForeColor = System.Drawing.Color.Black;
            this._lblrootname.Location = new System.Drawing.Point(15, 278);
            this._lblrootname.Name = "_lblrootname";
            this._lblrootname.Size = new System.Drawing.Size(59, 19);
            this._lblrootname.TabIndex = 24;
            this._lblrootname.Text = "7.1.1.1.zip";
            // 
            // _lblFileSize
            // 
            this._lblFileSize.ForeColor = System.Drawing.Color.Black;
            this._lblFileSize.Location = new System.Drawing.Point(374, 277);
            this._lblFileSize.Name = "_lblFileSize";
            this._lblFileSize.Size = new System.Drawing.Size(158, 21);
            this._lblFileSize.TabIndex = 23;
            this._lblFileSize.Text = "0.00MB";
            this._lblFileSize.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _lblTransferRate
            // 
            this._lblTransferRate.ForeColor = System.Drawing.Color.Black;
            this._lblTransferRate.Location = new System.Drawing.Point(369, 313);
            this._lblTransferRate.Name = "_lblTransferRate";
            this._lblTransferRate.Size = new System.Drawing.Size(163, 25);
            this._lblTransferRate.TabIndex = 25;
            this._lblTransferRate.Text = "0.00kb/s";
            this._lblTransferRate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DialogUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(545, 393);
            this.Name = "DialogUpdate";
            this.Text = "កំពុងទាញយក";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAds)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ProgressBar btnDownloading;
        private Button btnCANCEL;
        private Label lblDOWNLOADING;
        private PictureBox pbAds;
        private Label _lblDownloaded;
        private Label _lblrootname;
        private Label _lblFileSize;
        private Label _lblTransferRate;
    }
}