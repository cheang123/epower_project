﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Drawing;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using Login = SoftTech.Security.Logic.Login;

namespace EPower.Interface
{
    public partial class DialogMeter : ExDialog
    {
        GeneralProcess _flag = GeneralProcess.Insert;
        TBL_METER _objNew = new TBL_METER();
        public TBL_METER Meter
        {
            get { return _objNew; }
        }
        TBL_METER _objOld = new TBL_METER();

        #region Constructor
        public DialogMeter(GeneralProcess flag, TBL_METER objMeter)
        {
            InitializeComponent();

            this.txtMeterCode.MaxLength = Settings.Default.METER_DIGIT;

            btnAddMeterType.Enabled = Login.IsAuthorized(Permission.ADMIN_METERTYPE);

            _flag = flag;

            dataLookUp();
            objMeter._CopyTo(_objNew);
            objMeter._CopyTo(_objOld);

            if (flag == GeneralProcess.Insert)
            {
                this.Text = string.Concat(Resources.INSERT, this.Text);
                txtMeterCode.Text = _objNew.METER_CODE;
                cboMeterType.SelectedIndex = -1;
                cboStatus.SelectedValue = (int)MeterStatus.Stock;
                cboStatus.Enabled = false;
                lblREASON.Visible = false;
                label6.Visible = false;
                cboReason.Visible = false;
                panel1.Location = new Point(0, 177);
                btnCHANGE_LOG.Location = new Point(6, 184);
                btnOK.Location = new Point(208, 184);
                btnCLOSE.Location = new Point(287, 184);
                this.Size = new Size(369, 237);
            }
            else if (flag == GeneralProcess.Update)
            {
                this.Text = string.Concat(Resources.UPDATE, this.Text);
                read();
                lblREASON.Visible = true;
                label6.Visible = true;
                cboReason.Visible = true;
                panel1.Location = new Point(0, 225);
                btnCHANGE_LOG.Location = new Point(6, 232);
                btnOK.Location = new Point(208, 232);
                btnCLOSE.Location = new Point(287, 232);
                this.Size = new Size(369, 290);
            }
            else if (flag == GeneralProcess.Delete)
            {
                this.Text = string.Concat(Resources.DELETE, this.Text);
                read();
                UIHelper.SetEnabled(this, false);
            }
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;

        }
        #endregion

        #region Method
        private void read()
        {
            _objOld.REASON_ID = 0;
            txtMeterCode.Text = _objNew.METER_CODE;
            cboMeterType.SelectedValue = _objNew.METER_TYPE_ID;
            cboStatus.SelectedValue = _objNew.STATUS_ID;
            txtMULTIPLIER.Text = _objNew.MULTIPLIER.ToString();

        }

        private void write()
        {
            var mutiplier = DataHelper.ParseToInt(this.txtMULTIPLIER.Text);
            _objNew.METER_CODE = Method.FormatMeterCode(txtMeterCode.Text);
            _objNew.METER_TYPE_ID = (int)cboMeterType.SelectedValue;
            _objNew.STATUS_ID = (int)cboStatus.SelectedValue;
            _objNew.MULTIPLIER = mutiplier <= 0 ? 1 : mutiplier;
            _objNew.CREATE_BY = Login.CurrentLogin.LOGIN_NAME;
            _objNew.CREATE_ON = DBDataContext.Db.GetSystemDate();
            _objNew.IS_BLUETOOTH = checkBluetooth(_objNew.METER_CODE);

            if (_flag == GeneralProcess.Insert)
            {
                _objNew.REGISTER_CODE = "";
                _objNew.REASON_ID = 0;
            }
        }

        private void dataLookUp()
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                //Get meter type.
                bindMeterType();
                //Get status.
                UIHelper.SetDataSourceToComboBox(cboStatus,
                    from s in DBDataContext.Db.TLKP_METER_STATUS
                    select new
                    {
                        s.STATUS_ID,
                        s.STATUS_NAME
                    }, "STATUS_ID", "STATUS_NAME");
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
            this.Cursor = Cursors.Default;

            UIHelper.SetDataSourceToComboBox(this.cboReason, DBDataContext.Db.TLKP_REASONs.Where(x => x.IS_ACTIVE && x.TYPE_ID == (int)ReasonType.ChangeMetercode).Select(x => new { x.REASON_ID, x.REASON }), "");
        }

        private void bindMeterType()
        {
            UIHelper.SetDataSourceToComboBox(cboMeterType,
                from t in DBDataContext.Db.TBL_METER_TYPEs
                where t.IS_ACTIVE
                orderby t.METER_TYPE_NAME
                select new
                {
                    t.METER_TYPE_ID,
                    t.METER_TYPE_NAME
                }, "METER_TYPE_ID", "METER_TYPE_NAME");
        }

        private bool checkBluetooth(string meterCode)
        {
            var isBluetooth = false;
            System.Text.RegularExpressions.Regex regex = null;
            System.Text.RegularExpressions.Regex regex1 = null;
            regex = new System.Text.RegularExpressions.Regex("[0-0][0-0][0-0][2-2][2-2][1-9][0-7][0-9][0-9][0-9][0-9][0-9]");
            regex1 = new System.Text.RegularExpressions.Regex("[0-0][0-0][0-0][2-2][2-2][1-9][4-4][5-9][0-9][0-9][0-9][0-9]");
            isBluetooth = (regex.IsMatch(meterCode) && !regex1.IsMatch(meterCode));
            return isBluetooth;
        }

        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();


            if (txtMeterCode.Text.Trim() == string.Empty)
            {
                txtMeterCode.SetValidation(string.Format(Resources.REQUIRED, lblMETER_CODE.Text));
                val = true;
            }
            System.Text.RegularExpressions.Regex regex = null;

            regex = new System.Text.RegularExpressions.Regex("^([a-zA-Z0-9][-]?)*$");

            if (regex.IsMatch(txtMeterCode.Text))
            {
                val = false;
            }
            else
            {
                txtMeterCode.SetValidation(string.Format(Resources.NO_SPECIAL_CHARACTER_ALLOWED, lblMETER_CODE.Text));
                val = true;
            }
            if (cboMeterType.SelectedIndex == -1)
            {
                cboMeterType.SetValidation(string.Format(Resources.REQUIRED, lblMETER_TYPE.Text));
                val = true;
            }
            if (cboStatus.SelectedIndex == -1)
            {
                cboStatus.SetValidation(string.Format(Resources.REQUIRED, lblSTATUS.Text));
                val = true;
            }
            return val;
        }
        #endregion

        #region Operation
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }

            write();

            // validate meter status
            if (this._flag == GeneralProcess.Insert)
            {
                if (this._objNew.STATUS_ID == (int)MeterStatus.Used)
                {
                    cboStatus.SetValidation(Resources.MS_NEW_METER_STATUS);
                    return;
                }
            }
            else
            {
                // update code then degrade to mechanical meter
                if (this._objOld.METER_CODE != this._objNew.METER_CODE)
                {
                    this._objNew.REGISTER_CODE = "";
                    this._objNew.IS_DIGITAL = false;
                }
                // if change from InStock to Other
                if (this._objOld.STATUS_ID == (int)MeterStatus.Used
                    && this._objNew.STATUS_ID != (int)MeterStatus.Used)
                {
                    cboStatus.SetValidation(Resources.MS_METER_STATUS_IN_USE);
                    return;
                }
                // change from Other to Instock
                if (this._objOld.STATUS_ID != (int)MeterStatus.Used
                   && (int)this._objNew.STATUS_ID == (int)MeterStatus.Used)
                {
                    cboStatus.SetValidation(Resources.MS_CANNOT_CHANGE_STATUS_METER_IN_USE);
                    return;
                }

                //When update serial meter
                if (cboReason.SelectedIndex <= 0)
                {
                    cboReason.SetValidation(string.Format(Resources.REQUIRED, this.lblREASON.Text));
                    return;
                }
                _objNew.REASON_ID = (int)cboReason.SelectedValue;
            }

            txtMeterCode.ClearValidation();
            if (DBDataContext.Db.IsExits(_objNew, "METER_CODE"))
            {
                txtMeterCode.SetValidation(string.Format(Resources.MS_IS_EXISTS, lblMETER_CODE.Text));
                return;
            }

            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {


                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objNew);

                        DBDataContext.Db.TBL_STOCK_TRANs.InsertOnSubmit(new TBL_STOCK_TRAN()
                        {
                            STOCK_TRAN_TYPE_ID = (int)StockTranType.StockIn,
                            FROM_STOCK_TYPE_ID = (int)StockType.None,
                            TO_STOCK_TYPE_ID = (int)StockType.Stock,
                            CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                            CREATE_ON = DBDataContext.Db.GetSystemDate(),
                            ITEM_ID = _objNew.METER_TYPE_ID,
                            ITEM_TYPE_ID = (int)StockItemType.Meter,
                            REMARK = _objNew.METER_CODE,
                            QTY = 1,
                            UNIT_PRICE = 0
                        });
                        DBDataContext.Db.SubmitChanges();
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                        if (_objOld.STATUS_ID != _objNew.STATUS_ID)
                        {
                            DBDataContext.Db.TBL_STOCK_TRANs.InsertOnSubmit(new TBL_STOCK_TRAN()
                            {
                                STOCK_TRAN_TYPE_ID = (int)StockTranType.Adjust,
                                FROM_STOCK_TYPE_ID = _objOld.STATUS_ID,
                                TO_STOCK_TYPE_ID = _objNew.STATUS_ID,
                                CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                                CREATE_ON = DBDataContext.Db.GetSystemDate(),
                                ITEM_ID = _objOld.METER_TYPE_ID,
                                ITEM_TYPE_ID = (int)StockItemType.Meter,
                                REMARK = _objNew.METER_CODE,
                                QTY = 1,
                                UNIT_PRICE = 0
                            });
                            DBDataContext.Db.SubmitChanges();
                        }

                        // change current usage's multiplier
                        if (_objNew.MULTIPLIER != _objOld.MULTIPLIER)
                        {
                            this.changeMultiplierOfCurrentUsage();
                        }
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objNew);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void changeMultiplierOfCurrentUsage()
        {
            var objCustomerMeter = DBDataContext.Db.TBL_CUSTOMER_METERs.FirstOrDefault(x => x.METER_ID == _objNew.METER_ID && x.IS_ACTIVE);
            if (objCustomerMeter == null) return;

            // last power invoice
            var objLastPowerInvoice = (from i in DBDataContext.Db.TBL_INVOICEs
                                       orderby i.INVOICE_MONTH descending
                                       where i.CUSTOMER_ID == objCustomerMeter.CUSTOMER_ID && !i.IS_SERVICE_BILL
                                       select i).FirstOrDefault();

            // check to update multiplier in usage insert ready
            var q = new TBL_USAGE();
            if (objLastPowerInvoice != null)
            {
                q = DBDataContext.Db.TBL_USAGEs.FirstOrDefault(x => x.CUSTOMER_ID == objLastPowerInvoice.CUSTOMER_ID &&
                    x.USAGE_MONTH == objLastPowerInvoice.INVOICE_MONTH.AddMonths(1) && x.METER_ID == objCustomerMeter.METER_ID);
            }
            var nextBillingMonth = DateTime.Now;
            if (objLastPowerInvoice == null || q == null)
            {
                nextBillingMonth = Method.GetNextBillingMonth(DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == objCustomerMeter.CUSTOMER_ID).BILLING_CYCLE_ID);
            }
            else
            {
                nextBillingMonth = objLastPowerInvoice.INVOICE_MONTH.AddMonths(1);
            }

            // last usage that not jet issue invoice
            var objLastUsage = DBDataContext.Db.TBL_USAGEs.FirstOrDefault(row => row.USAGE_MONTH == nextBillingMonth && row.METER_ID == _objNew.METER_ID);
            if (objLastUsage == null) return;

            // commit change usage's multiplier
            objLastUsage.MULTIPLIER = _objNew.MULTIPLIER;
            DBDataContext.Db.SubmitChanges();
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }


        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        #endregion


        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }

        private void btnAddMeterType_AddItem(object sender, EventArgs e)
        {
            DialogMeterType dig = new DialogMeterType(GeneralProcess.Insert, new TBL_METER_TYPE());
            if (dig.ShowDialog() == DialogResult.OK)
            {
                bindMeterType();
                cboMeterType.SelectedValue = dig.MeterType.METER_TYPE_ID;
            }
        }


    }
}
