﻿using EPower.Base.Logic;
using EPower.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogCloseCustomer : ExDialog
    {
        List<TBL_CUSTOMER> _objCustomer = new List<TBL_CUSTOMER>();
        List<TBL_CUSTOMER> _objOldCustomer = new List<TBL_CUSTOMER>();
        List<TBL_CUS_STATUS_CHANGE> _objCusStatusChange = new List<TBL_CUS_STATUS_CHANGE>();
        List<TBL_CUSTOMER_METER> _objCustomerMeter = new List<TBL_CUSTOMER_METER>();
        List<TBL_METER> _objMeter = new List<TBL_METER>();
        List<int> lstId = new List<int>();
        TBL_USER_CASH_DRAWER _objUserCashDrawer = null;
        TBL_CASH_DRAWER _objCashDrawer = null;
        DataTable source = new DataTable();

        bool blnIsShowDialog = false;

        public DialogCloseCustomer(TBL_CUSTOMER objCustomer)
        {
            InitializeComponent();
            BindStatus();
            _objCustomer.Add(objCustomer);
            lstId.Add(objCustomer.CUSTOMER_ID);
            if (objCustomer.CUSTOMER_ID == objCustomer.INVOICE_CUSTOMER_ID)
            {
                _objCustomer = DBDataContext.Db.TBL_CUSTOMERs.Where(x => x.INVOICE_CUSTOMER_ID == objCustomer.CUSTOMER_ID && x.STATUS_ID != (int)CustomerStatus.Closed).ToList();
                lstId = _objCustomer.Select(x => x.CUSTOMER_ID).ToList();
            }
            _objOldCustomer.AddRange(_objCustomer);

            List<TBL_CUSTOMER_METER> objCusMeter = DBDataContext.Db.TBL_CUSTOMER_METERs.Where(cm => _objCustomer.Select(x => x.CUSTOMER_ID).Contains(cm.CUSTOMER_ID) && cm.IS_ACTIVE).ToList();
            _objCustomerMeter.AddRange(objCusMeter);

            List<TBL_METER> objMeter = DBDataContext.Db.TBL_METERs.Where(m => _objCustomerMeter.Select(x => x.METER_ID).Contains(m.METER_ID)).ToList();
            _objMeter.AddRange(objMeter);

            UIHelper.DataGridViewProperties(this.dgv);
            this._objUserCashDrawer = Login.CurrentCashDrawer;
            if (this._objUserCashDrawer != null)
            {
                this._objCashDrawer = DBDataContext.Db.TBL_CASH_DRAWERs.FirstOrDefault(row => row.CASH_DRAWER_ID == this._objUserCashDrawer.CASH_DRAWER_ID);
            }
            // if no cashdrawer is open then lock user edit.
            if (this._objCashDrawer != null)
            {
                this.txtCashDrawer.Text = this._objCashDrawer.CASH_DRAWER_NAME;
                this.blnIsShowDialog = true;
            }
            else
            {
                MsgBox.ShowInformation(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_CLOSE_CUSTOMER);
                UIHelper.SetEnabled(this, false);
                this.btnOK.Enabled = false;
                this.blnIsShowDialog = false;
            }
            read();
            UIHelper.SetDataSourceToComboBox(cboCurrency, DBDataContext.Db.TLKP_CURRENCies.Select(x => new { x.CURRENCY_ID, x.CURRENCY_NAME }));
        }

        public new DialogResult ShowDialog()
        {
            if (blnIsShowDialog)
            {
                return base.ShowDialog();
            }
            else
            {
                return DialogResult.No;
            }
        }

        private void BindStatus()
        {
            UIHelper.SetDataSourceToComboBox(cboStatus, DBDataContext.Db.TLKP_METER_STATUS.Where(x => x.STATUS_ID != (int)MeterStatus.Used));
            UIHelper.SetDataSourceToComboBox(cboBreakerStatus, DBDataContext.Db.TLKP_BRAKER_STATUS.Where(x => x.STATUS_ID != (int)MeterStatus.Used));
            cboStatus.SelectedValue = (int)MeterStatus.Stock;
            cboBreakerStatus.SelectedValue = (int)MeterStatus.Stock;
        }

        /// <summary>
        /// Reads object to display to the form.
        /// This form require tree objects to be not null
        /// this._objCustomer,
        /// this._objCustomerMeter, and 
        /// this._objMeter
        /// </summary>
        private void read()
        {
            // show customer information.
            TBL_CUSTOMER objCus = _objCustomer.FirstOrDefault();
            TBL_CUSTOMER_METER objCusMeter = _objCustomerMeter.FirstOrDefault();

            if (_objCustomer.Count > 1)
            {
                objCus = _objCustomer.Where(x => x.CUSTOMER_ID == x.INVOICE_CUSTOMER_ID).FirstOrDefault();
                objCusMeter = _objCustomerMeter.Where(x => x.CUSTOMER_ID == objCus.CUSTOMER_ID).FirstOrDefault();
            }

            this.txtCustomerName.Text = objCus.LAST_NAME_KH + " " + objCus.FIRST_NAME_KH;
            this.txtCustomerCode.Text = objCus.CUSTOMER_CODE;
            this.txtMeterCode.Text = this._objMeter.Where(x => x.METER_ID == objCusMeter.METER_ID).FirstOrDefault().METER_CODE;
            this.txtAreaName.Text = DBDataContext.Db.TBL_AREAs.FirstOrDefault(row => row.AREA_ID == objCus.AREA_ID).AREA_NAME;
            this.txtPole.Text = DBDataContext.Db.TBL_POLEs.FirstOrDefault(row => row.POLE_ID == objCusMeter.POLE_ID).POLE_CODE;
            this.txtBox.Text = DBDataContext.Db.TBL_BOXes.FirstOrDefault(row => row.BOX_ID == objCusMeter.BOX_ID).BOX_CODE;
            cboCurrency_SelectedIndexChanged(cboCurrency, EventArgs.Empty);
        }

        private void loadInvoice()
        {
            int nCurrencyId = (int)cboCurrency.SelectedValue;
            TLKP_CURRENCY objCurrency = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.CURRENCY_ID == nCurrencyId);
            // invoice.
            var query = from inv in DBDataContext.Db.TBL_INVOICEs
                        join c in DBDataContext.Db.TLKP_CURRENCies on inv.CURRENCY_ID equals c.CURRENCY_ID
                        where lstId.Contains(inv.CUSTOMER_ID) && c.CURRENCY_ID == nCurrencyId
                              && (inv.INVOICE_STATUS == (int)InvoiceStatus.Open || inv.INVOICE_STATUS == (int)InvoiceStatus.Pay)
                        select new
                        {
                            INVOICE_ID = inv.INVOICE_ID,
                            INVOICE_NO = inv.INVOICE_NO,
                            INVOICE_DATE = inv.INVOICE_DATE,
                            INVOICE_TITLE = inv.INVOICE_TITLE,
                            SETTLE_AMOUNT = inv.SETTLE_AMOUNT,
                            PAID_AMOUNT = inv.PAID_AMOUNT,
                            DUE_AMOUNT = inv.SETTLE_AMOUNT - inv.PAID_AMOUNT,
                            c.CURRENCY_SING
                        };

            this.source = query._ToDataTable();
            this.dgv.DataSource = this.source;
            decimal decTotalDue = 0;
            if (this.dgv.Rows.Count > 0)
            {
                decTotalDue = this.getTotalDue();
            }

            decimal decCusDepositAmount = 0;
            foreach (var i in lstId)
            {
                decCusDepositAmount += Method.GetCustomerDeposit(i, nCurrencyId);
            }

            this.txtTotalDue.Text = UIHelper.FormatCurrency(decTotalDue, nCurrencyId);
            this.txtRefundDeposit.Text = UIHelper.FormatCurrency(decCusDepositAmount - decTotalDue, nCurrencyId);
            this.txtDeposit.Text = UIHelper.FormatCurrency(decCusDepositAmount, nCurrencyId);
            this.txtPayDate.SetValue(DBDataContext.Db.GetSystemDate());

            txtSignDeposit.Text =
            txtSignRefundDeposit.Text =
            txtSignTotalDue.Text = objCurrency.CURRENCY_SING;
        }

        public decimal getTotalDue()
        {
            decimal tmp = 0.0m;
            foreach (DataGridViewRow row in this.dgv.Rows)
            {
                tmp += (decimal)row.Cells[DUE_AMOUNT.Name].Value;
            }
            return tmp;
        }
        public decimal getTotalPay()
        {
            return DataHelper.ParseToDecimal(this.txtDeposit.Text);
        }
        public decimal getTotalBalance()
        {
            return this.getTotalDue() - getTotalPay();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            decimal decRefundAmount = DataHelper.ParseToDecimal(txtRefundDeposit.Text.Trim());

            // if customer not yet clear all invoice(s) due
            // ask user to clear all invoice in payment form.
            if (DataHelper.ParseToDecimal(this.txtTotalDue.Text) > 0)
            {
                if (MsgBox.ShowQuestion(Resources.MSQ_CUSTOMER_DUE_AMOUNT, this.Text) != DialogResult.Yes)
                {
                    return;
                }
            }

            // if customer have deposit balance >0 
            // ask user to refund deposit in customer>deposit form.
            if (DataHelper.ParseToDecimal(this.txtDeposit.Text) > 0)
            {
                if (MsgBox.ShowQuestion(Resources.MSQ_CUSTOMER_HAVE_DEPSOIT_BALANCE, this.Text) != DialogResult.Yes)
                {
                    return;
                }
            }

            cboStatus.ClearValidation();
            if (cboStatus.SelectedIndex == -1)
            {
                cboStatus.SetValidation(string.Format(Resources.REQUIRED, lblMETER_STATUS.Text));
                return;
            }

            write();
            var meterStatus = (int)cboStatus.SelectedValue;
            var breakerStatus = (int)cboBreakerStatus.SelectedValue;
            Runner.RunNewThread(delegate ()
            {
                var tranOption = new TransactionOptions()
                {
                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted,
                    Timeout = TimeSpan.FromMinutes(10)
                };

                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.RequiresNew, tranOption))
                {
                    foreach (var id in lstId)
                    {
                        TBL_CHANGE_LOG objChangelog = DBDataContext.Db.Update(_objOldCustomer.Where(x => x.CUSTOMER_ID == id), _objCustomer.Where(x => x.CUSTOMER_ID == id));
                    }

                    foreach (var item in _objMeter)
                    {
                        item.STATUS_ID = meterStatus;
                    }
                    DBDataContext.Db.SubmitChanges();

                    // change breaker status 
                    var objBraker = DBDataContext.Db.TBL_CIRCUIT_BREAKERs.Where(x => _objCustomerMeter.Select(z => z.BREAKER_ID).Contains(x.BREAKER_ID));
                    foreach (var item in objBraker)
                    {
                        item.STATUS_ID = breakerStatus;
                    }
                    DBDataContext.Db.SubmitChanges();

                    // mark cusotmer meter is close
                    var objCusMeter = DBDataContext.Db.TBL_CUSTOMER_METERs.Where(x => lstId.Contains(x.CUSTOMER_ID) && x.IS_ACTIVE).ToList();
                    foreach (var item in objCusMeter)
                    {
                        item.IS_ACTIVE = false;
                    }

                    // customer status change
                    foreach (var obj in _objCusStatusChange)
                    {
                        DBDataContext.Db.Insert(obj);
                    }

                    var _objStockTran = new List<TBL_STOCK_TRAN>();
                    foreach (var item in _objMeter)
                    {
                        _objStockTran.Add(new TBL_STOCK_TRAN
                        {
                            STOCK_TRAN_TYPE_ID = (int)StockTranType.StockIn,
                            FROM_STOCK_TYPE_ID = (int)StockType.Used,
                            TO_STOCK_TYPE_ID = (int)item.STATUS_ID,
                            CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                            CREATE_ON = DBDataContext.Db.GetSystemDate(),
                            ITEM_ID = item.METER_TYPE_ID,
                            REMARK = item.METER_CODE,
                            ITEM_TYPE_ID = (int)StockItemType.Meter,
                            UNIT_PRICE = 0,
                            QTY = 1
                        });
                    }
                    foreach (var obj in _objStockTran)
                    {
                        DBDataContext.Db.Insert(obj);
                    }
                    DBDataContext.Db.SubmitChanges();

                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            });

            // Update B24 Customer 
            foreach (var id in lstId)
            {
                B24_Integration.UpdateCustomerRowDate(id, false);
            }
        }

        private void write()
        {
            foreach (var item in _objCustomer)
            {
                _objCusStatusChange.Add(new TBL_CUS_STATUS_CHANGE
                {
                    CUSTOMER_ID = item.CUSTOMER_ID,
                    CHNAGE_DATE = txtPayDate.Value,
                    CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                    NEW_STATUS_ID = (int)CustomerStatus.Closed,
                    OLD_STATUS_ID = item.STATUS_ID,
                    INVOICE_ID = 0
                });
                //update customer.
                item.STATUS_ID = (int)CustomerStatus.Closed;
            }
        }

        private void InputEnglish(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        private void InputKhmer(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void cboCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboCurrency.SelectedIndex == -1)
            {
                return;
            }
            loadInvoice();
        }
    }
}
