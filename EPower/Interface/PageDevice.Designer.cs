﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageDevice
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageDevice));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnREGISTER_DEVICE = new SoftTech.Component.ExButton();
            this.cboStatus = new System.Windows.Forms.ComboBox();
            this.btnADD = new SoftTech.Component.ExButton();
            this.btnEDIT = new SoftTech.Component.ExButton();
            this.txtQuickSearch = new SoftTech.Component.ExTextbox();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.DEVICE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DEVICE_TYPE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DEVICE_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DEVICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DEVICE_TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.STATUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.btnREGISTER_DEVICE);
            this.panel1.Controls.Add(this.cboStatus);
            this.panel1.Controls.Add(this.btnADD);
            this.panel1.Controls.Add(this.btnEDIT);
            this.panel1.Controls.Add(this.txtQuickSearch);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnREGISTER_DEVICE
            // 
            resources.ApplyResources(this.btnREGISTER_DEVICE, "btnREGISTER_DEVICE");
            this.btnREGISTER_DEVICE.Name = "btnREGISTER_DEVICE";
            this.btnREGISTER_DEVICE.UseVisualStyleBackColor = true;
            this.btnREGISTER_DEVICE.Click += new System.EventHandler(this.btnDeviceRegister_Click);
            // 
            // cboStatus
            // 
            this.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboStatus.FormattingEnabled = true;
            this.cboStatus.Items.AddRange(new object[] {
            resources.GetString("cboStatus.Items"),
            resources.GetString("cboStatus.Items1"),
            resources.GetString("cboStatus.Items2")});
            resources.ApplyResources(this.cboStatus, "cboStatus");
            this.cboStatus.Name = "cboStatus";
            this.cboStatus.SelectedIndexChanged += new System.EventHandler(this.txt_QuickSearch);
            // 
            // btnADD
            // 
            resources.ApplyResources(this.btnADD, "btnADD");
            this.btnADD.Name = "btnADD";
            this.btnADD.UseVisualStyleBackColor = true;
            this.btnADD.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnEDIT
            // 
            resources.ApplyResources(this.btnEDIT, "btnEDIT");
            this.btnEDIT.Name = "btnEDIT";
            this.btnEDIT.UseVisualStyleBackColor = true;
            this.btnEDIT.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // txtQuickSearch
            // 
            this.txtQuickSearch.BackColor = System.Drawing.Color.White;
            this.txtQuickSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtQuickSearch, "txtQuickSearch");
            this.txtQuickSearch.Name = "txtQuickSearch";
            this.txtQuickSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtQuickSearch.QuickSearch += new System.EventHandler(this.txt_QuickSearch);
            this.txtQuickSearch.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // dgv
            // 
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DEVICE_ID,
            this.DEVICE_TYPE_ID,
            this.DEVICE_CODE,
            this.DEVICE,
            this.DEVICE_TYPE,
            this.STATUS});
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellDoubleClick);
            // 
            // DEVICE_ID
            // 
            this.DEVICE_ID.DataPropertyName = "DEVICE_ID";
            resources.ApplyResources(this.DEVICE_ID, "DEVICE_ID");
            this.DEVICE_ID.Name = "DEVICE_ID";
            // 
            // DEVICE_TYPE_ID
            // 
            this.DEVICE_TYPE_ID.DataPropertyName = "DEVICE_TYPE_ID";
            resources.ApplyResources(this.DEVICE_TYPE_ID, "DEVICE_TYPE_ID");
            this.DEVICE_TYPE_ID.Name = "DEVICE_TYPE_ID";
            // 
            // DEVICE_CODE
            // 
            this.DEVICE_CODE.DataPropertyName = "DEVICE_CODE";
            resources.ApplyResources(this.DEVICE_CODE, "DEVICE_CODE");
            this.DEVICE_CODE.Name = "DEVICE_CODE";
            // 
            // DEVICE
            // 
            this.DEVICE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DEVICE.DataPropertyName = "DEVICE_OEM_INFO";
            resources.ApplyResources(this.DEVICE, "DEVICE");
            this.DEVICE.Name = "DEVICE";
            // 
            // DEVICE_TYPE
            // 
            this.DEVICE_TYPE.DataPropertyName = "DEVICE_TYPE_NAME";
            resources.ApplyResources(this.DEVICE_TYPE, "DEVICE_TYPE");
            this.DEVICE_TYPE.Name = "DEVICE_TYPE";
            // 
            // STATUS
            // 
            this.STATUS.DataPropertyName = "STATUS_NAME";
            resources.ApplyResources(this.STATUS, "STATUS");
            this.STATUS.Name = "STATUS";
            // 
            // PageDevice
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.panel1);
            this.Name = "PageDevice";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExTextbox txtQuickSearch;
        private ExButton btnADD;
        private ExButton btnEDIT;
        private DataGridView dgv;
        public ComboBox cboStatus;
        private ExButton btnREGISTER_DEVICE;
        private DataGridViewTextBoxColumn DEVICE_ID;
        private DataGridViewTextBoxColumn DEVICE_TYPE_ID;
        private DataGridViewTextBoxColumn DEVICE_CODE;
        private DataGridViewTextBoxColumn DEVICE;
        private DataGridViewTextBoxColumn DEVICE_TYPE;
        private DataGridViewTextBoxColumn STATUS;
    }
}
