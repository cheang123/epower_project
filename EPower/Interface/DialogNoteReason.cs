﻿using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using System;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogNoteReason : ExDialog
    {

        #region Constructor
        public DialogNoteReason(string rptName)
        {
            InitializeComponent();
            this._lblMsg.Text = string.Format(Resources.MS_CONFIRM_TO_SEND_REPORT_AGAIN, rptName);
            txtREASON.Focus();
        }
        #endregion

        #region Event
        private void btnOK_Click(object sender, EventArgs e)
        {
            this.ClearAllValidation();
            if (string.IsNullOrEmpty(txtREASON.Text.Trim()))
            {
                this.txtREASON.SetValidation(string.Format(Resources.REQUIRED, lblREASON.Text));
                return;
            }
            this.DialogResult = DialogResult.OK;
        }

        private void btnCLOSE_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
        #endregion
    }
}
