﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using Login = SoftTech.Security.Logic.Login;

namespace EPower.Interface
{
    public partial class DialogCollectUsage : ExDialog
    {
        bool _loading = false;
        bool _update = false;
        int _lastCollectorID = 0;
        int _lastAreaID = 0;
        DateTime _lastCollectMonth = new DateTime();
        List<int> _lstErrorRows = new List<int>();//Store error list when save

        public DialogCollectUsage()
        {
            InitializeComponent();

            // SET DATA TYPE TO DATA GRID
            this.dgv.Columns["START_USAGE"].ValueType = typeof(decimal);
            this.dgv.Columns["END_USAGE"].ValueType = typeof(decimal);
            this.dgv.Columns["START_USAGE"].DefaultCellStyle.Format = UIHelper._DefaultUsageFormat;
            this.dgv.Columns["END_USAGE"].DefaultCellStyle.Format = UIHelper._DefaultUsageFormat;

            this._loading = true;
            this.bindAreaAndCollector();
            this._loading = false;
        }

        private void bindAreaAndCollector()
        {
            // area.
            DataTable source = DBDataContext.Db.TBL_AREAs.Where(a => a.IS_ACTIVE).OrderBy(a => a.AREA_NAME)._ToDataTable();
            DataRow row = source.NewRow();
            row["AREA_ID"] = 0;
            row["AREA_NAME"] = Resources.ALL_AREA;
            source.Rows.InsertAt(row, 0);
            UIHelper.SetDataSourceToComboBox(this.cboArea, source);

            // collector.
            source = (from e in DBDataContext.Db.TBL_EMPLOYEEs
                      join p in DBDataContext.Db.TBL_EMPLOYEE_POSITIONs on e.EMPLOYEE_ID equals p.EMPLOYEE_ID
                      where p.POSITION_ID == (int)EmpPosition.Collector && e.IS_ACTIVE
                      select e)._ToDataTable();
            row = source.NewRow();
            row["EMPLOYEE_ID"] = 0;
            row["EMPLOYEE_NAME"] = Resources.SELECT_COLLECTOR;
            source.Rows.InsertAt(row, 0);
            UIHelper.SetDataSourceToComboBox(this.cboCollector, source);


            //Billing Cycle
            source = DBDataContext.Db.TBL_BILLING_CYCLEs.Where(b => b.IS_ACTIVE)._ToDataTable();
            row = source.NewRow();
            row["CYCLE_ID"] = 0;
            row["CYCLE_NAME"] = Resources.SELECT_BILLING_CYCLE;
            source.Rows.InsertAt(row, 0);
            UIHelper.SetDataSourceToComboBox(this.cboCycle, source);

            if (source.Rows.Count > 1)
            {
                this.cboCycle.SelectedIndex = 1;
                this.dtpMonth.Value = Method.GetNextBillingMonth((int)this.cboCycle.SelectedValue);
            }
        }

        private void LoadStartEndDate()
        {
            if (cboCycle.SelectedIndex != -1)
            {
                int intCycleId = int.Parse(cboCycle.SelectedValue.ToString());
                if (intCycleId > 0)
                {
                    TBL_BILLING_CYCLE objCycle = DBDataContext.Db.TBL_BILLING_CYCLEs.FirstOrDefault(b => b.CYCLE_ID == intCycleId);
                    DateTime datStart, datEnd;
                    DateTime datMonth = new DateTime(this.dtpMonth.Value.Year, this.dtpMonth.Value.Month, 1);

                    //if run end of month
                    if (objCycle.IS_END_MONTH)
                    {
                        datStart = new DateTime(dtpMonth.Value.Year, dtpMonth.Value.Month, 1);
                        datEnd = datStart.AddMonths(1).AddDays(-1);
                    }
                    else
                    {
                        datStart = new DateTime(dtpMonth.Value.Year, dtpMonth.Value.Month, objCycle.END_DAY).AddDays(1);
                        datEnd = new DateTime(datStart.AddMonths(1).Year, datStart.AddMonths(1).Month, objCycle.END_DAY);
                    }
                    dtpStartDate.Value = datStart;
                    dtpEndDate.Value = datEnd;
                    this.dgv.Columns["IS_METER_RENEW_CYCLE"].ReadOnly =
                        this.dgv.Columns["END_USAGE"].ReadOnly = DBDataContext.Db.TBL_RUN_BILLs.Count(r => r.CYCLE_ID == intCycleId && r.BILLING_MONTH == datMonth) > 0;
                }
            }
            bindData();
        }

        /// <summary>
        /// Bind to grid
        /// </summary>
        private void bindData()
        {
            this._loading = true;
            if (this.cboCollector.SelectedIndex < 1)
            {
                this.cboCollector.Focus();
                this._loading = false;
                return;
            }
            if (this.cboCycle.SelectedIndex < 1)
            {
                this.cboCycle.Focus();
                this._loading = false;
                return;
            }

            if (_update)
            {
                if (MsgBox.ShowQuestion(Resources.MSQ_SAVE_COLLECT_USAGE, Resources.SAVE) == DialogResult.Yes)
                {
                    // if data was not save correctly.
                    // rollback user change.
                    if (!this.saveData())
                    {
                        this.cboArea.SelectedValue = this._lastAreaID;
                        this.cboCollector.SelectedValue = this._lastCollectorID;
                        this.dtpMonth.Value = this._lastCollectMonth;
                        this._loading = false;
                        return;
                    }
                }
            }
            int collectorID = (int)this.cboCollector.SelectedValue;
            int areaID = (int)this.cboArea.SelectedValue;
            int intBillingCycleId = (int)this.cboCycle.SelectedValue;

            DateTime currentMonth = new DateTime(this.dtpMonth.Value.Year, this.dtpMonth.Value.Month, 1);
            DateTime lastMonth = new DateTime(currentMonth.AddMonths(-1).Year, currentMonth.AddMonths(-1).Month, 1);
            DateTime startDate = this.dtpStartDate.Value.Date;
            DateTime endDate = this.dtpEndDate.Value.Date;

            // binding source.
            DataTable dt = (from c in DBDataContext.Db.TBL_CUSTOMERs.Where(c => c.STATUS_ID != (int)CustomerStatus.Closed)
                            join m in DBDataContext.Db.TBL_CUSTOMER_METERs.Where(m => m.IS_ACTIVE) on c.CUSTOMER_ID equals m.CUSTOMER_ID
                            join p in DBDataContext.Db.TBL_POLEs on m.POLE_ID equals p.POLE_ID
                            join u in DBDataContext.Db.TBL_USAGEs.Where(row => row.USAGE_MONTH == currentMonth) on new { c.CUSTOMER_ID, m.METER_ID } equals new { u.CUSTOMER_ID, u.METER_ID } into tmpUsages
                            from ju in tmpUsages.DefaultIfEmpty()
                            where (areaID == 0 || p.AREA_ID == areaID)
                                  && (collectorID == 0 || p.COLLECTOR_ID == collectorID)
                                  && (intBillingCycleId == 0 || c.BILLING_CYCLE_ID == intBillingCycleId)
                            select new
                            {
                                // ju == null usage not yet have. 
                                USAGE_ID = ju == null ? 0 : ju.USAGE_ID,
                                CUSTOMER_ID = c.CUSTOMER_ID,
                                METER_ID = m.METER_ID,
                                CUSTOMER_CODE = c.CUSTOMER_CODE,
                                CUSTOMER_NAME = c.LAST_NAME_KH + " " + c.FIRST_NAME_KH,

                                // if not yet posted then start usage is endusage of last month.
                                START_USAGE = ju == null ? (from tmp in DBDataContext.Db.TBL_USAGEs
                                                            where tmp.USAGE_MONTH == lastMonth && tmp.CUSTOMER_ID == c.CUSTOMER_ID
                                                            orderby tmp.USAGE_ID descending
                                                            select (decimal?)tmp.END_USAGE).FirstOrDefault() ?? 0
                                                         : ju.START_USAGE,

                                // if not yet posted then end usage is 0.
                                END_USAGE = ju == null ? 0 : ju.END_USAGE,

                                IS_METER_RENEW_CYCLE = ju == null ? false : ju.IS_METER_RENEW_CYCLE,

                                // _DB flags whether the record is aready posted.
                                _DB = ju != null,

                                // _EDITED flags wheter user have edited record.
                                _EDITED = false
                            })._ToDataTable();

            this.dgv.Rows.Clear();
            foreach (DataRow row in dt.Rows)
            {
                this.dgv.Rows.Add(row.ItemArray);
            }
            // keep the last collector id.
            // it's enable to rollback when use change
            // the collector and save change is not complete.
            this._lastCollectorID = collectorID;
            this._lastAreaID = areaID;
            this._lastCollectMonth = currentMonth;

            // mark that user not yet edit data.
            this._update = false;

            // mark the binding load are complete
            this._loading = false;
        }
        private void showRecord()
        {
            string search = this.txtSearch.Text.ToLower();
            if (rdoShowAll.Checked)
            {
                foreach (DataGridViewRow row in this.dgv.Rows)
                {
                    row.Visible = (row.Cells["CUSTOMER_NAME"].Value.ToString() + row.Cells["CUSTOMER_CODE"].Value.ToString()).ToLower().Contains(search);
                }
            }
            else if (rdoShowInvalid.Checked)
            {
                foreach (DataGridViewRow row in this.dgv.Rows)
                {
                    row.Visible = (row.Cells["CUSTOMER_NAME"].Value.ToString() + row.Cells["CUSTOMER_CODE"].Value.ToString()).ToLower().Contains(search)
                                  && rowIsInvalid(row);
                }
            }
            else if (rdoShowNotComplete.Checked)
            {
                foreach (DataGridViewRow row in this.dgv.Rows)
                {
                    row.Visible = (row.Cells["CUSTOMER_NAME"].Value.ToString() + row.Cells["CUSTOMER_CODE"].Value.ToString()).ToLower().Contains(search)
                                  && rowIsNotCompleted(row);
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtCollector_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_loading)
            {
                bindData();
            }
        }

        private void txtArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_loading)
            {
                bindData();
            }
        }

        private void dgv_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != -1 && e.RowIndex != -1
                && (this.dgv.Columns[e.ColumnIndex].Name == "END_USAGE"))
            {
                DataGridViewRow row = this.dgv.Rows[e.RowIndex];
                row.Cells["_EDITED"].Value = true;
                _update = true;
            }
        }

        private void dgv_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MsgBox.ShowInformation(Resources.MS_INVALID_INPUT_USAGE);
            e.Cancel = false;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            // force datagridview update date.
            this.btnOK.Focus();

            // save data. 
            saveData();
        }

        private bool saveData()
        {

            if (this.invalid())
            {
                return false;
            };

            bool saveSuccess = false;

            Runner.Run(delegate ()
            {
                // do save data.
                try
                {
                    using (TransactionScope tran = new TransactionScope())
                    {
                        int collectorID = (int)this.cboCollector.SelectedValue;
                        DateTime endDate = this.dtpEndDate.Value.Date;
                        DateTime startDate = this.dtpStartDate.Value.Date;
                        DateTime usageMonth = new DateTime(this.dtpMonth.Value.Year, this.dtpMonth.Value.Month, 1);
                        DateTime now = DBDataContext.Db.GetSystemDate();

                        foreach (DataGridViewRow row in this.dgv.Rows)
                        {
                            if ((bool)row.Cells["_DB"].Value)
                            {
                                if ((bool)row.Cells["_EDITED"].Value)
                                {
                                    TBL_USAGE tmp = DBDataContext.Db.TBL_USAGEs.FirstOrDefault(u => u.USAGE_ID == (int)row.Cells["USAGE_ID"].Value);
                                    if (tmp != null)
                                    {
                                        TBL_USAGE objOld = new TBL_USAGE();
                                        tmp._CopyTo(objOld);
                                        TBL_USAGE objNew = new TBL_USAGE()
                                        {
                                            COLLECTOR_ID = collectorID,
                                            CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                                            CUSTOMER_ID = (int)row.Cells["CUSTOMER_ID"].Value,
                                            DEVICE_ID = objOld.DEVICE_ID,
                                            END_USAGE = DataHelper.ParseToDecimal(row.Cells["END_USAGE"].Value.ToString()),
                                            END_USE_DATE = endDate,
                                            IS_METER_RENEW_CYCLE = (bool)row.Cells["IS_METER_RENEW_CYCLE"].Value,
                                            METER_ID = (int)row.Cells["METER_ID"].Value,
                                            POSTING_DATE = now,
                                            START_USAGE = (decimal)row.Cells["START_USAGE"].Value,
                                            START_USE_DATE = startDate,
                                            USAGE_ID = objOld.USAGE_ID,
                                            USAGE_MONTH = usageMonth
                                        };
                                        DBDataContext.Db.Update(objOld, objNew);
                                    }
                                }
                            }
                            else
                            {
                                if ((bool)row.Cells["_EDITED"].Value)
                                {
                                    TBL_USAGE objNew = new TBL_USAGE()
                                    {
                                        COLLECTOR_ID = collectorID,
                                        CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                                        CUSTOMER_ID = (int)row.Cells["CUSTOMER_ID"].Value,
                                        DEVICE_ID = 0,
                                        END_USAGE = DataHelper.ParseToDecimal(row.Cells["END_USAGE"].Value.ToString()),
                                        END_USE_DATE = endDate,
                                        IS_METER_RENEW_CYCLE = (bool)row.Cells["IS_METER_RENEW_CYCLE"].Value,
                                        METER_ID = (int)row.Cells["METER_ID"].Value,
                                        POSTING_DATE = now,
                                        START_USAGE = (decimal)row.Cells["START_USAGE"].Value,
                                        START_USE_DATE = startDate,
                                        USAGE_ID = (int)row.Cells["USAGE_ID"].Value,
                                        USAGE_MONTH = usageMonth
                                    };
                                    DBDataContext.Db.Insert(objNew);
                                }
                            }
                        }
                        tran.Complete();
                    }
                    this._update = false;
                    this.bindData();
                    saveSuccess = true;
                }
                catch (Exception ex)
                {
                    MsgBox.ShowError(ex);
                    saveSuccess = false;
                }
            });

            return saveSuccess;
        }

        private bool invalid()
        {
            if (this.cboCollector.SelectedIndex <= 0)
            {
                MsgBox.ShowInformation(Resources.MS_SELECT_COLLECTOR);
                this.cboCollector.Focus();
                return true;
            }

            if (this.dtpEndDate.Value.Date < this.dtpStartDate.Value.Date)
            {
                MsgBox.ShowInformation(Resources.MS_SELECT_START_END_DATE);
                this.dtpStartDate.Focus();
                return true;
            }

            bool invalidInput = false;
            foreach (DataGridViewRow row in this.dgv.Rows)
            {
                if (rowIsInvalid(row))
                {
                    row.DefaultCellStyle.ForeColor = Color.Red;
                    _lstErrorRows.Add(row.Index);
                    invalidInput = true;
                }
            }

            if (invalidInput)
            {
                MsgBox.ShowInformation(string.Concat(Resources.MS_INVALID_INPUT_USAGE, Resources.MS_CORRECT_RED_CELL));
                return true;
            }
            return false;
        }
        bool rowIsInvalid(DataGridViewRow row)
        {
            return DataHelper.ParseToDecimal(row.Cells["START_USAGE"].Value.ToString()) <= DataHelper.ParseToDecimal(row.Cells["END_USAGE"].Value.ToString())
                == (bool)row.Cells["IS_METER_RENEW_CYCLE"].Value;
        }
        bool rowIsNotCompleted(DataGridViewRow row)
        {
            return DataHelper.ParseToDecimal(row.Cells["END_USAGE"].Value.ToString()) == 0;
        }
        private void dtpCollectMonth_ValueChanged(object sender, EventArgs e)
        {
            //set start end by billing cycle
            LoadStartEndDate();
        }

        private void cboBillingCycle_SelectedIndexChanged(object sender, EventArgs e)
        {
            // load start and end date.
            LoadStartEndDate();
        }

        private void rdoShowNotComplete_CheckedChanged(object sender, EventArgs e)
        {
            showRecord();
        }

        private void rdoShowInvalid_CheckedChanged(object sender, EventArgs e)
        {
            showRecord();
        }

        private void rdoShowAll_CheckedChanged(object sender, EventArgs e)
        {
            showRecord();
        }

        private void DialogCollectUsage_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_update)
            {
                DialogResult result = MsgBox.ShowDialog(Resources.MSQ_SAVE_COLLECT_USAGE, Resources.SAVE, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    if (!this.saveData())
                    {
                        e.Cancel = true;
                    }
                }
                else if (result == DialogResult.Cancel)
                {
                    e.Cancel = true;
                }
                else if (result == DialogResult.No)
                {
                    e.Cancel = false;
                }
            }
        }

        private void txtSearch_QuickSearch(object sender, EventArgs e)
        {
            this.showRecord();
        }

        private void txtSearch_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
    }
}
