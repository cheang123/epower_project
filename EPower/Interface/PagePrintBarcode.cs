﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PagePrintBarcode : Form
    {
        /// <summary>
        /// Determines the binding are being process on Status ComboBox and Type ComboBox.
        /// </summary>
        private bool _binding = false;
 
        private DataTable _ds =null;

        public PagePrintBarcode()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            //
            this.dgv.ReadOnly = false;
            foreach (DataGridViewColumn col in this.dgv.Columns)
            {
                col.ReadOnly = col.Name != this.IS_PRINT_.Name;
            } 
            bind();            
            BindData();
        }

         
        public void bind()
        {
            _binding = true;
            UIHelper.SetDataSourceToComboBox(cboArea, DBDataContext.Db.TBL_AREAs.Where(x => x.IS_ACTIVE), Resources.ALL_AREA);
            UIHelper.SetDataSourceToComboBox(cboTransformer,DBDataContext.Db.TBL_TRANSFORMERs.Where(row=>row.IS_ACTIVE),Resources.ALL_TRANSFORMER);
            _binding = false; 
        } 

        DataTable dt = new DataTable();
        public void BindData()
        { 
            // if statust and type is in binding mode 
            // then do not bind customer list
            // wait until status and customer type 
            // are completed bound.
            if (this._binding)
            { 
                return;
            }
            int areaId =  (int)cboArea.SelectedValue;
            int transfoId = (int)cboTransformer.SelectedValue; 

            var qry = (from c in DBDataContext.Db.TBL_CUSTOMERs
                  join m in DBDataContext.Db.TBL_CUSTOMER_METERs on c.CUSTOMER_ID equals m.CUSTOMER_ID
                  join a in DBDataContext.Db.TBL_AREAs on c.AREA_ID equals a.AREA_ID
                  join b in DBDataContext.Db.TBL_BOXes on m.BOX_ID equals b.BOX_ID
                  join p in DBDataContext.Db.TBL_POLEs on b.POLE_ID equals p.POLE_ID
                  join t in DBDataContext.Db.TBL_METERs on m.METER_ID equals t.METER_ID
                  join br in DBDataContext.Db.TBL_CUSTOMER_PRINT_BARCODEs on c.CUSTOMER_ID equals br.CUSTOMER_ID into print
                  from pr in print.DefaultIfEmpty()
                  where m.IS_ACTIVE
                        && c.STATUS_ID !=(int)CustomerStatus.Closed
                        && (areaId == 0 || a.AREA_ID == areaId)
                        && (transfoId == 0 || p.TRANSFORMER_ID == transfoId)
                        && (c.LAST_NAME_KH + " " + c.FIRST_NAME_KH + t.METER_CODE+a.AREA_CODE+p.POLE_CODE+b.BOX_CODE).ToLower().Contains(this.txtQuickSearch.Text.ToLower())                                        
                        && (!this.chkSHOW_CUSTOMER_TO_PRINT_BARCODE.Checked || pr!=null)
                  orderby 
                  a.AREA_CODE,
                  p.POLE_CODE,
                  b.BOX_CODE,
                  c.CUSTOMER_CODE
                  select new
                  { 
                      c.CUSTOMER_ID,
                      IS_PRINT_ = pr!=null,
                      c.CUSTOMER_CODE,
                      CUSTOMER_NAME = c.LAST_NAME_KH + " " + c.FIRST_NAME_KH,
                      AREA = a.AREA_NAME,
                      POLE = p.POLE_CODE,
                      BOX = b.BOX_CODE,
                      METER = t.METER_CODE
                  });
            this.dgv.DataSource = qry._ToDataTable();
            this._binding = false;  
        }
 
        private void txtCustomerType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        } 
        private void exTextbox1_QuickSearch(object sender, EventArgs e)
        {
            BindData();
        } 
        private void txtSearch_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void btnPrintBarcode_Click(object sender, EventArgs e)
        {
            Runner.Run(delegate()
            {
                CrystalReportHelper cr = new CrystalReportHelper("ReportPrintBarcode.rpt");
                cr.ViewReport("");
                cr.Dispose();
            });
        }
   
        private void txtQuickSearch_QuickSearch(object sender, EventArgs e)
        {
            BindData();
        }

        private void dgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && this.IS_PRINT_.Name == this.dgv.Columns[ e.ColumnIndex].Name && this.dgv.SelectedRows.Count > 0)
            {
                this.dgv[this.IS_PRINT_.Name, e.RowIndex].Value = !(bool)this.dgv[this.IS_PRINT_.Name, e.RowIndex].Value;

                bool print=(bool)this.dgv[this.IS_PRINT_.Name, e.RowIndex].Value;

                int customerId = (int)this.dgv.Rows[e.RowIndex].Cells[this.CUSTOMER_ID.Name].Value;
                var b = DBDataContext.Db.TBL_CUSTOMER_PRINT_BARCODEs.FirstOrDefault(row => row.CUSTOMER_ID == customerId);
                if (print)
                {
                    if (b == null)
                    {
                        DBDataContext.Db.TBL_CUSTOMER_PRINT_BARCODEs.InsertOnSubmit(new TBL_CUSTOMER_PRINT_BARCODE() { CUSTOMER_ID = customerId });
                        DBDataContext.Db.SubmitChanges();
                    }
                }
                else
                {
                    if (b != null)
                    {
                        DBDataContext.Db.TBL_CUSTOMER_PRINT_BARCODEs.DeleteOnSubmit(b);
                        DBDataContext.Db.SubmitChanges();
                    }     
                }
            }
        }

        
        private void chkViewPrintCustomer_CheckedChanged(object sender, EventArgs e)
        {
            this.BindData();
        }

        private void chkAll_CheckedChanged(object sender, EventArgs e)
        {
            Runner.Run(this.checkOrUncheckAll);
        }

        private void checkOrUncheckAll()
        {
            bool print = this.chkAll_.Checked;
            foreach (DataGridViewRow row in this.dgv.Rows)
            {
                row.Cells[this.IS_PRINT_.Name].Value = print;
                int customerId = (int)row.Cells[this.CUSTOMER_ID.Name].Value;
                var b = DBDataContext.Db.TBL_CUSTOMER_PRINT_BARCODEs.FirstOrDefault(c => c.CUSTOMER_ID == customerId);
                if (print)
                {
                    if (b == null)
                    {
                        DBDataContext.Db.TBL_CUSTOMER_PRINT_BARCODEs.InsertOnSubmit(new TBL_CUSTOMER_PRINT_BARCODE() { CUSTOMER_ID = customerId });
                        DBDataContext.Db.SubmitChanges();
                    }
                }
                else
                {
                    if (b != null)
                    {
                        DBDataContext.Db.TBL_CUSTOMER_PRINT_BARCODEs.DeleteOnSubmit(b);
                        DBDataContext.Db.SubmitChanges();
                    }
                }
            }
        }

        private void cboArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindData();
        }

        private void cboTransformer_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindData();
        } 
    }
}