﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Drawing;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogDepositAdd : ExDialog
    {
        #region Data
        private DepositAction _DepositAction = DepositAction.AddDeposit;
        private TBL_CUSTOMER _objCustomer = new TBL_CUSTOMER();
        private TBL_CUS_DEPOSIT _objCusDeposit = new TBL_CUS_DEPOSIT();
        private TBL_CUS_DEPOSIT _objOldCusDeposit = new TBL_CUS_DEPOSIT();
        int _intCurrencyID = 0;
        GeneralProcess _Flag = GeneralProcess.Insert;
        #endregion Data

        #region Constructor
        public DialogDepositAdd(TBL_CUSTOMER objCus, DepositAction depositAction, GeneralProcess Flag, TBL_CUS_DEPOSIT objCusDeposit)
        {
            InitializeComponent();
            // SET PERMISSION
            this.dtpDepositDate.Visible =
            this.lblDATE.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_DEPOSIT_DATE]);
            this.dtpDepositDate.Enabled = SoftTech.Security.Logic.Login.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_REGISTER_UPDATE_DEPOSIT_DATE);

            bindCurrency();
            this.chkPRINT_RECEIPT.Checked = Settings.Default.PRINT_DEPOSIT;
            this.chkIS_PAID.Checked = Settings.Default.IS_PAY_CHARGE;
            objCus._CopyTo(_objCustomer);
            _DepositAction = depositAction;
            _Flag = Flag;
            dtpDepositDate.Value = DBDataContext.Db.GetSystemDate();
            if (_Flag == GeneralProcess.Update)
            {
                objCusDeposit._CopyTo(_objCusDeposit);
                objCusDeposit._CopyTo(_objOldCusDeposit);
            }

            if (_DepositAction == DepositAction.AddDeposit && chkIS_PAID.Checked == false)
            {
                this.Text = Resources.DEPOSIT;
                this.chkIS_PAID.Checked = DataHelper.ParseToBoolean(Method.Utilities[Utility.DEPOSIT_IS_PAID_AUTO]);
            }
            else if (_DepositAction == DepositAction.AdjustDeposit)
            {
                this.Text = Resources.ADJUST_DEPOSIT;
            }
            else if (_DepositAction == DepositAction.RefundDeposit)
            {
                this.Text = Resources.REFUND_DEPOSIT;
                // this.dtpREQUEST_REFUND_DATE.Value = DateTime.Now.AddDays(-15);
                this.dtpREQUEST_REFUND_DATE.Visible = true;
                this.lblREQUEST_REFUND_DATE.Visible = true;
            }

            read();

        }
        #endregion Constructor

        #region Method

        private void bindCurrency()
        {
            UIHelper.SetDataSourceToComboBox(cboCurrency, DBDataContext.Db.TLKP_CURRENCies.Select(x => new { x.CURRENCY_ID, x.CURRENCY_NAME }));
        }

        private void read()
        {

            if (_DepositAction != DepositAction.AddDeposit)
            {
                chkIS_PAID.Checked = true;
                chkIS_PAID.Visible = false;
            }

            if (_DepositAction == DepositAction.AdjustDeposit)
            {
                chkPRINT_RECEIPT.Visible = false;
            }

            var objCus = (from c in DBDataContext.Db.TBL_CUSTOMERs
                          join a in DBDataContext.Db.TBL_AMPAREs on c.AMP_ID equals a.AMPARE_ID
                          join p in DBDataContext.Db.TBL_PHASEs on c.PHASE_ID equals p.PHASE_ID
                          join ct in DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs on c.CUSTOMER_CONNECTION_TYPE_ID equals ct.CUSTOMER_CONNECTION_TYPE_ID
                          join pr in DBDataContext.Db.TBL_PRICEs on c.PRICE_ID equals pr.PRICE_ID
                          where c.CUSTOMER_ID == _objCustomer.CUSTOMER_ID
                          select new
                          {
                              c.LAST_NAME_KH,
                              c.FIRST_NAME_KH,
                              a.AMPARE_NAME,
                              p.PHASE_NAME,
                              pr.PRICE_NAME,
                              ct.CUSTOMER_CONNECTION_TYPE_NAME,
                              pr.CURRENCY_ID
                          }).FirstOrDefault();


            if (objCus != null)
            {
                txtLastName.Text = objCus.LAST_NAME_KH;
                txtFirstName.Text = objCus.FIRST_NAME_KH;
                txtAmp.Text = objCus.AMPARE_NAME;
                txtPhase.Text = objCus.PHASE_NAME;
                txtCustomerType.Text = objCus.CUSTOMER_CONNECTION_TYPE_NAME;
                txtPrice.Text = objCus.PRICE_NAME;
                _intCurrencyID = objCus.CURRENCY_ID;

                loadBalanceDeposit();

                LoadCacutorPowerkVA();


                if (_objCustomer.POWER_KW > 0)
                {
                    if (_objCustomer.IS_MV == true)
                    {
                        if (_objCustomer.CUSTOMER_CONNECTION_TYPE_ID == 141)
                        {
                            cboCurrency.SelectedIndex = 0;
                        }
                        else
                        {
                            cboCurrency.SelectedIndex = 1;
                        }
                    }
                    else
                    {
                        TBL_CUSTOMER obj = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == _objCustomer.CUSTOMER_ID);
                        TBL_PRICE currency = DBDataContext.Db.TBL_PRICEs.FirstOrDefault(x => x.PRICE_ID == obj.PRICE_ID);
                        if (currency.CURRENCY_ID == 2)
                        {
                            cboCurrency.SelectedIndex = 1;
                        }
                        else
                        {
                            cboCurrency.SelectedIndex = 0;
                        }
                    }
                }
            }
        }

        private void loadBalanceDeposit()
        {
            if (cboCurrency.SelectedIndex == -1)
            {
                return;
            }

            int nCurrency = (int)cboCurrency.SelectedValue;
            decimal decAmountToDeposit = UIHelper.Round(Method.GetAmoutToDepsoit(_objCustomer.CUSTOMER_ID, nCurrency), nCurrency);


            //Display customer's currently deposit balance
            decimal decLastBalance = Method.GetCustomerDeposit(_objCustomer.CUSTOMER_ID, nCurrency);
            txtCurrentBalance.Text = UIHelper.FormatCurrency(decLastBalance, nCurrency);

            //if this customer already have Deposit that not yet paid
            //then display to update 
            if (_Flag == GeneralProcess.Update)
            {
                txtAdjustAmount.Text = UIHelper.FormatCurrency(_objCusDeposit.AMOUNT, nCurrency);
            }
            //add new deposit 
            else
            {
                if (decLastBalance <= decAmountToDeposit && _DepositAction == DepositAction.AddDeposit)
                {
                    txtAdjustAmount.Text = UIHelper.FormatCurrency(decAmountToDeposit - decLastBalance, nCurrency);
                }
                else
                {
                    txtAdjustAmount.Text = UIHelper.FormatCurrency(DataHelper.ParseToDecimal(txtAdjustAmount.Text), nCurrency);
                }
            }
            txtAdjustAmount_TextChanged(txtAdjustAmount, EventArgs.Empty);
        }

        private bool invalid()
        {
            bool blnReturn = false;
            this.ClearAllValidation();

            if (cboCurrency.SelectedIndex == -1)
            {
                cboCurrency.SetValidation(string.Format(Resources.REQUIRED, lblCURRENCY.Text));
                return true;
            }
            if (txtAdjustAmount.Text == string.Empty)
            {
                txtAdjustAmount.SetValidation(string.Format(Properties.Resources.REQUIRED, lblAMOUNT.Text));
                blnReturn = true;
            }
            if (UIHelper.Round(DataHelper.ParseToDecimal(txtLastBalance.Text), (int)cboCurrency.SelectedValue) < 0)
            {
                txtAdjustAmount.SetValidation(string.Format(Resources.REQUIRED, lblAMOUNT.Text));
                blnReturn = true;
            }

            if (_DepositAction == DepositAction.AdjustDeposit)
            {
                if (UIHelper.Round(DataHelper.ParseToDecimal(txtAdjustAmount.Text.Trim()), (int)cboCurrency.SelectedValue) == 0)
                {
                    txtAdjustAmount.SetValidation(string.Format(Resources.REQUIRED, lblAMOUNT.Text));
                    blnReturn = true;
                }
            }
            if (_DepositAction == DepositAction.RefundDeposit)
            {
                if (UIHelper.Round(DataHelper.ParseToDecimal(txtAdjustAmount.Text.Trim()), (int)cboCurrency.SelectedValue) <= 0)
                {
                    txtAdjustAmount.SetValidation(string.Format(Resources.REQUIRED_LOOKUP, lblAMOUNT.Text));
                    blnReturn = true;
                }
            }
            if (_DepositAction == DepositAction.AddDeposit)
            {
                if (UIHelper.Round(DataHelper.ParseToDecimal(txtAdjustAmount.Text.Trim()), (int)cboCurrency.SelectedValue) < 0)
                {
                    txtAdjustAmount.SetValidation(string.Format(Resources.REQUIRED_LOOKUP, lblAMOUNT.Text));
                    blnReturn = true;
                }
                if (UIHelper.Round(DataHelper.ParseToDecimal(txtAdjustAmount.Text.Trim()), (int)cboCurrency.SelectedValue) <= -1)
                {
                    txtAdjustAmount.SetValidation(string.Format(Resources.REQUIRED, lblAMOUNT.Text));
                    blnReturn = true;
                }
            }
            return blnReturn;
        }

        #endregion Method

        #region Event

        private void InputEnglish(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void InputKhmer(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void txtDecimalOnly(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (Login.CurrentCashDrawer == null && chkIS_PAID.Checked)
            {
                if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_RECEIVE_PAYMENT, string.Empty) == DialogResult.Yes)
                    new DialogOpenCashDrawer().ShowDialog();
                if (Login.CurrentCashDrawer == null)
                    return;
            }
            if (invalid())
            {
                return;
            }
            try
            {
                int currencyId = (int)cboCurrency.SelectedValue;
                decimal decAdjustAmount = DataHelper.ParseToDecimal(UIHelper.FormatCurrency(DataHelper.ParseToDecimal(txtAdjustAmount.Text), currencyId));
                Runner.RunNewThread(delegate ()
                {
                    using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                    {
                        DateTime datNow = dtpDepositDate.Value;
                        bool blnIsPaid = chkIS_PAID.Checked;
                        decimal balance = DataHelper.ParseToDecimal(UIHelper.FormatCurrency(DataHelper.ParseToDecimal(txtLastBalance.Text), currencyId));
                        if (_Flag == GeneralProcess.Insert)
                        {
                            //if the deposit is refund
                            //then add minus to adjust amount
                            if (_DepositAction == DepositAction.RefundDeposit)
                            {
                                decAdjustAmount = -decAdjustAmount;
                            } 
                            _objCusDeposit = Method.NewCustomerDeposit(_objCustomer.CUSTOMER_ID, decAdjustAmount, balance, currencyId, _DepositAction, txtNode.Text, datNow, blnIsPaid, dtpREQUEST_REFUND_DATE.Value);
                            _objCusDeposit.DEPOSIT_NO = Method.GetNextSequence(Sequence.Receipt, true);
                            DBDataContext.Db.Insert(_objCusDeposit);
                        }
                        else
                        {
                            _objCusDeposit = Method.NewCustomerDeposit(_objCustomer.CUSTOMER_ID, decAdjustAmount, balance, currencyId, _DepositAction, txtNode.Text, datNow, blnIsPaid, dtpREQUEST_REFUND_DATE.Value);
                            _objCusDeposit.CUS_DEPOSIT_ID = _objOldCusDeposit.CUS_DEPOSIT_ID;
                            _objCusDeposit.DEPOSIT_NO = _objOldCusDeposit?.DEPOSIT_NO ?? "";
                            DBDataContext.Db.Update(_objOldCusDeposit, _objCusDeposit);
                        }
                        tran.Complete();
                        this.DialogResult = DialogResult.OK;
                    }
                });
                if (this.chkPRINT_RECEIPT.Checked)
                {
                    printInvoice();
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void printInvoice()
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                if (_objCusDeposit.CUS_DEPOSIT_ID > 0)
                {
                    if (_DepositAction == DepositAction.AddDeposit)
                    {
                        //if already paid then print deposit receipt
                        if (_objCusDeposit.IS_PAID)
                        {
                            CrystalReportHelper ch = new CrystalReportHelper("ReportDepositReceipt.rpt");
                            ch.SetParameter("@CUS_DEPOSIT_ID", _objCusDeposit.CUS_DEPOSIT_ID);
                            ch.PrintReport(Settings.Default.PRINTER_RECIEPT);
                        }
                        //if not yet paid then print deposit invoice
                        else
                        {
                            CrystalReportHelper ch = new CrystalReportHelper("ReportDepositInvoice.rpt");
                            ch.SetParameter("@CUS_DEPOSIT_ID", _objCusDeposit.CUS_DEPOSIT_ID);
                            ch.PrintReport(Settings.Default.PRINTER_INVOICE);
                        }
                    }
                    else if (_DepositAction == DepositAction.RefundDeposit)
                    {
                        CrystalReportHelper ch = new CrystalReportHelper("ReportDepositRefund.rpt");
                        ch.SetParameter("@CUS_DEPOSIT_ID", _objCusDeposit.CUS_DEPOSIT_ID);
                        ch.PrintReport(Settings.Default.PRINTER_INVOICE);
                    }
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(Resources.MS_PRINTER_ERROR, Resources.WARNING);
            }
            Cursor.Current = Cursors.Default;
        }

        private void txtAdjustAmount_TextChanged(object sender, EventArgs e)
        {

            if (_DepositAction == DepositAction.RefundDeposit)
            {
                txtLastBalance.Text = UIHelper.FormatCurrency((DataHelper.ParseToDecimal(txtCurrentBalance.Text) + -DataHelper.ParseToDecimal(txtAdjustAmount.Text)), (int)cboCurrency.SelectedValue);
            }
            else
            {
                txtLastBalance.Text = UIHelper.FormatCurrency((DataHelper.ParseToDecimal(txtCurrentBalance.Text) + DataHelper.ParseToDecimal(txtAdjustAmount.Text)), (int)cboCurrency.SelectedValue);
            }
        }
        #endregion Event 

        private void chkPrintInvoice_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Default.PRINT_DEPOSIT = this.chkPRINT_RECEIPT.Checked;
            Settings.Default.Save();
        }

        private void chkIsPaid_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Default.IS_PAY_CHARGE = chkIS_PAID.Checked;
            Settings.Default.Save();
        }

        private void cboCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadBalanceDeposit();
            LoadCacutorPowerkVA();
        }

        private void dtpREQUEST_REFUND_DATE_Enter(object sender, EventArgs e)
        {
            InputEnglish(null, null);
        }

        private void dtpDepositDate_Enter(object sender, EventArgs e)
        {
            InputEnglish(null, null);
        }

        private void LoadCacutorPowerkVA()
        {
            if (_objCustomer.POWER_KW > 0)
            {
                TBL_CUSTOMER obj = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == _objCustomer.CUSTOMER_ID);
                TBL_PRICE currency = DBDataContext.Db.TBL_PRICEs.FirstOrDefault(x => x.PRICE_ID == obj.PRICE_ID);
                decimal amount = 0;
                amount = (Method.CalculatorPowerKW(_objCustomer.CUSTOMER_ID, _objCustomer.POWER_KW));
                if (currency.CURRENCY_ID == 2 && cboCurrency.SelectedIndex == 1)
                {
                    if (amount > 0 && _objCustomer.CUSTOMER_CONNECTION_TYPE_ID != 1)
                    {
                        txtAdjustAmount.Text = UIHelper.FormatCurrency(amount, currency.CURRENCY_ID);
                    }
                }

                if (currency.CURRENCY_ID == 1 && cboCurrency.SelectedIndex == 0)
                {
                    if (amount > 0 && _objCustomer.CUSTOMER_CONNECTION_TYPE_ID != 1)
                    {
                        txtAdjustAmount.Text = UIHelper.FormatCurrency(amount, currency.CURRENCY_ID);
                    }
                }
            }
        }

        private void picHelp_MouseEnter(object sender, EventArgs e)
        {
            ToolTip tp = new ToolTip
            {
                AutoPopDelay = 15000,
                ShowAlways = true,
                OwnerDraw = true,
            };
            if (_objCustomer.IS_MV == true)
            {
                if (_objCustomer.POWER_KW >= 275 && _objCustomer.POWER_KW <= 1000)
                {
                    tp.SetToolTip(picHelp, Resources.POWER_KVA_275_TO_1000);
                    tp.Draw += new DrawToolTipEventHandler(toolTip_Draw);
                }
                else if (_objCustomer.POWER_KW >= 1001 && _objCustomer.POWER_KW <= 3000)
                {
                    tp.SetToolTip(picHelp, Resources.POWER_KVA_1001_TO_3000);
                    tp.Draw += new DrawToolTipEventHandler(toolTip_Draw);
                }
                else if (_objCustomer.POWER_KW >= 3001 && _objCustomer.POWER_KW <= 10000)
                {
                    tp.SetToolTip(picHelp, Resources.POWER_KVA_3001_TO_10000);
                    tp.Draw += new DrawToolTipEventHandler(toolTip_Draw);
                }
            }
            else
            {
                if (_objCustomer.CUSTOMER_CONNECTION_TYPE_ID != 1)
                {
                    if (_objCustomer.POWER_KW >= 50 && _objCustomer.POWER_KW <= 275)
                    {
                        tp.SetToolTip(picHelp, Resources.POWER_KVA_50_TO_275);
                        tp.Draw += new DrawToolTipEventHandler(toolTip_Draw);
                    }
                }
            }
        }
        private void toolTip_Draw(object sender, DrawToolTipEventArgs e)
        {
            using (e.Graphics)
            {
                Font f = new Font("Khmer OS System", 8.0f);
                e.DrawBackground();
                e.DrawBorder();
                e.Graphics.DrawString(e.ToolTipText, f, Brushes.Black, new PointF(1, 1));
            }
        }
    }
}
