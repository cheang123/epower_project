﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogCollector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCollector));
            this.lblEMPLOYEE = new System.Windows.Forms.Label();
            this.txtEmpName = new System.Windows.Forms.TextBox();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.label9 = new System.Windows.Forms.Label();
            this.tabDEVICE = new System.Windows.Forms.TabPage();
            this.dgvDevice = new System.Windows.Forms.DataGridView();
            this.SELECT_DEVICE_ = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.DEVICE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DEVICE_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DEVICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPOSITION = new System.Windows.Forms.TabPage();
            this.dgvPosition = new System.Windows.Forms.DataGridView();
            this.SELECT_POSITION = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.POSITION_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.POSITION_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_USE_DEVICE = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tapComponent = new System.Windows.Forms.TabControl();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblPASSWORD = new System.Windows.Forms.Label();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabDEVICE.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDevice)).BeginInit();
            this.tabPOSITION.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPosition)).BeginInit();
            this.tapComponent.SuspendLayout();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.label2);
            this.content.Controls.Add(this.txtPassword);
            this.content.Controls.Add(this.lblPASSWORD);
            this.content.Controls.Add(this.tapComponent);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.txtEmpName);
            this.content.Controls.Add(this.lblEMPLOYEE);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblEMPLOYEE, 0);
            this.content.Controls.SetChildIndex(this.txtEmpName, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.tapComponent, 0);
            this.content.Controls.SetChildIndex(this.lblPASSWORD, 0);
            this.content.Controls.SetChildIndex(this.txtPassword, 0);
            this.content.Controls.SetChildIndex(this.label2, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            // 
            // lblEMPLOYEE
            // 
            resources.ApplyResources(this.lblEMPLOYEE, "lblEMPLOYEE");
            this.lblEMPLOYEE.Name = "lblEMPLOYEE";
            // 
            // txtEmpName
            // 
            resources.ApplyResources(this.txtEmpName, "txtEmpName");
            this.txtEmpName.Name = "txtEmpName";
            this.txtEmpName.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // tabDEVICE
            // 
            this.tabDEVICE.Controls.Add(this.dgvDevice);
            resources.ApplyResources(this.tabDEVICE, "tabDEVICE");
            this.tabDEVICE.Name = "tabDEVICE";
            this.tabDEVICE.UseVisualStyleBackColor = true;
            // 
            // dgvDevice
            // 
            this.dgvDevice.AllowUserToAddRows = false;
            this.dgvDevice.AllowUserToDeleteRows = false;
            this.dgvDevice.AllowUserToResizeRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvDevice.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvDevice.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgvDevice.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvDevice.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvDevice.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDevice.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SELECT_DEVICE_,
            this.DEVICE_ID,
            this.DEVICE_CODE,
            this.DEVICE});
            resources.ApplyResources(this.dgvDevice, "dgvDevice");
            this.dgvDevice.EnableHeadersVisualStyles = false;
            this.dgvDevice.GridColor = System.Drawing.Color.White;
            this.dgvDevice.MultiSelect = false;
            this.dgvDevice.Name = "dgvDevice";
            this.dgvDevice.RowHeadersVisible = false;
            this.dgvDevice.RowTemplate.Height = 25;
            this.dgvDevice.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDevice.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDevice_CellClick);
            // 
            // SELECT_DEVICE_
            // 
            this.SELECT_DEVICE_.DataPropertyName = "SELECT_DEVICE";
            this.SELECT_DEVICE_.FillWeight = 25F;
            resources.ApplyResources(this.SELECT_DEVICE_, "SELECT_DEVICE_");
            this.SELECT_DEVICE_.Name = "SELECT_DEVICE_";
            // 
            // DEVICE_ID
            // 
            this.DEVICE_ID.DataPropertyName = "DEVICE_ID";
            resources.ApplyResources(this.DEVICE_ID, "DEVICE_ID");
            this.DEVICE_ID.Name = "DEVICE_ID";
            this.DEVICE_ID.ReadOnly = true;
            // 
            // DEVICE_CODE
            // 
            this.DEVICE_CODE.DataPropertyName = "DEVICE_CODE";
            resources.ApplyResources(this.DEVICE_CODE, "DEVICE_CODE");
            this.DEVICE_CODE.Name = "DEVICE_CODE";
            this.DEVICE_CODE.ReadOnly = true;
            // 
            // DEVICE
            // 
            this.DEVICE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DEVICE.DataPropertyName = "DEVICE_OEM_INFO";
            resources.ApplyResources(this.DEVICE, "DEVICE");
            this.DEVICE.Name = "DEVICE";
            this.DEVICE.ReadOnly = true;
            // 
            // tabPOSITION
            // 
            this.tabPOSITION.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPOSITION.Controls.Add(this.dgvPosition);
            resources.ApplyResources(this.tabPOSITION, "tabPOSITION");
            this.tabPOSITION.Name = "tabPOSITION";
            // 
            // dgvPosition
            // 
            this.dgvPosition.AllowUserToAddRows = false;
            this.dgvPosition.AllowUserToDeleteRows = false;
            this.dgvPosition.AllowUserToResizeColumns = false;
            this.dgvPosition.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvPosition.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPosition.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgvPosition.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvPosition.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvPosition.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPosition.ColumnHeadersVisible = false;
            this.dgvPosition.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SELECT_POSITION,
            this.POSITION_ID,
            this.POSITION_NAME,
            this.IS_USE_DEVICE});
            resources.ApplyResources(this.dgvPosition, "dgvPosition");
            this.dgvPosition.EnableHeadersVisualStyles = false;
            this.dgvPosition.GridColor = System.Drawing.Color.White;
            this.dgvPosition.MultiSelect = false;
            this.dgvPosition.Name = "dgvPosition";
            this.dgvPosition.RowHeadersVisible = false;
            this.dgvPosition.RowTemplate.Height = 25;
            this.dgvPosition.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPosition.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPosition_CellClick);
            // 
            // SELECT_POSITION
            // 
            this.SELECT_POSITION.DataPropertyName = "SELECT_POSITION";
            this.SELECT_POSITION.FillWeight = 25F;
            resources.ApplyResources(this.SELECT_POSITION, "SELECT_POSITION");
            this.SELECT_POSITION.Name = "SELECT_POSITION";
            // 
            // POSITION_ID
            // 
            this.POSITION_ID.DataPropertyName = "POSITION_ID";
            resources.ApplyResources(this.POSITION_ID, "POSITION_ID");
            this.POSITION_ID.Name = "POSITION_ID";
            this.POSITION_ID.ReadOnly = true;
            // 
            // POSITION_NAME
            // 
            this.POSITION_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.POSITION_NAME.DataPropertyName = "POSITION_NAME";
            resources.ApplyResources(this.POSITION_NAME, "POSITION_NAME");
            this.POSITION_NAME.Name = "POSITION_NAME";
            this.POSITION_NAME.ReadOnly = true;
            // 
            // IS_USE_DEVICE
            // 
            this.IS_USE_DEVICE.DataPropertyName = "IS_USE_DEVICE";
            resources.ApplyResources(this.IS_USE_DEVICE, "IS_USE_DEVICE");
            this.IS_USE_DEVICE.Name = "IS_USE_DEVICE";
            this.IS_USE_DEVICE.ReadOnly = true;
            // 
            // tapComponent
            // 
            this.tapComponent.Controls.Add(this.tabPOSITION);
            this.tapComponent.Controls.Add(this.tabDEVICE);
            this.tapComponent.HotTrack = true;
            resources.ApplyResources(this.tapComponent, "tapComponent");
            this.tapComponent.Name = "tapComponent";
            this.tapComponent.SelectedIndex = 0;
            this.tapComponent.ControlAdded += new System.Windows.Forms.ControlEventHandler(this.tapComponent_ControlAdded);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Name = "label2";
            // 
            // txtPassword
            // 
            resources.ApplyResources(this.txtPassword, "txtPassword");
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.UseSystemPasswordChar = true;
            this.txtPassword.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            this.txtPassword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPassword_KeyPress);
            // 
            // lblPASSWORD
            // 
            resources.ApplyResources(this.lblPASSWORD, "lblPASSWORD");
            this.lblPASSWORD.Name = "lblPASSWORD";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // DialogCollector
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogCollector";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabDEVICE.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDevice)).EndInit();
            this.tabPOSITION.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPosition)).EndInit();
            this.tapComponent.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private TextBox txtEmpName;
        private Label lblEMPLOYEE;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label label9;
        private TabControl tapComponent;
        private TabPage tabPOSITION;
        private DataGridView dgvPosition;
        private TabPage tabDEVICE;
        private DataGridView dgvDevice;
        private DataGridViewCheckBoxColumn SELECT_POSITION;
        private DataGridViewTextBoxColumn POSITION_ID;
        private DataGridViewTextBoxColumn POSITION_NAME;
        private DataGridViewCheckBoxColumn IS_USE_DEVICE;
        private Label label2;
        private TextBox txtPassword;
        private Label lblPASSWORD;
        private ExButton btnCHANGE_LOG;
        private DataGridViewCheckBoxColumn SELECT_DEVICE_;
        private DataGridViewTextBoxColumn DEVICE_ID;
        private DataGridViewTextBoxColumn DEVICE_CODE;
        private DataGridViewTextBoxColumn DEVICE;
    }
}