﻿using System;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System.Linq;

namespace EPower.Interface
{
    public partial class DialogBoxTypeList : ExDialog
    {

        int _boxTypeId = 0;
        #region Constructor
        public DialogBoxTypeList(int boxTypeId)
        {
            InitializeComponent();
            _boxTypeId = boxTypeId;
            load();
        }

        #endregion

        #region Method

        private void load()
        {
            dgvBoxType.DataSource = from b in DBDataContext.Db.TBL_BOXes
                                    join p in DBDataContext.Db.TBL_POLEs on b.POLE_ID equals p.POLE_ID
                                    join a in DBDataContext.Db.TBL_AREAs on p.AREA_ID equals a.AREA_ID
                                    join s in DBDataContext.Db.TLKP_BOX_SATUS on b.STATUS_ID equals s.STATUS_ID
                                    where b.BOX_TYPE_ID==_boxTypeId 
                                        && (b.BOX_CODE+" "+p.POLE_CODE+" "+a.AREA_CODE+" "+s.STATUS_NAME).ToUpper().Contains(txtQuickSearch.Text.ToUpper())
                                    select new
                                    {
                                        b.BOX_CODE,
                                        p.POLE_CODE,
                                        a.AREA_CODE,
                                        s.STATUS_NAME
                                    }; 
            
        }

        #endregion 
        private void txtQuickSearch_QuickSearch(object sender, EventArgs e)
        {
            load();
        }

        private void ChangeEnglishKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
    }
}