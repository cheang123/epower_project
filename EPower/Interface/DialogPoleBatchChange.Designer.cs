﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogPoleBatchChange
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogPoleBatchChange));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnAddArea = new SoftTech.Component.ExAddItem();
            this.btnAddCollector = new SoftTech.Component.ExAddItem();
            this.btnAddBiller = new SoftTech.Component.ExAddItem();
            this.btnAddCutter = new SoftTech.Component.ExAddItem();
            this.btnAddTransfo = new SoftTech.Component.ExAddItem();
            this.lblSTART_USE_DATE = new System.Windows.Forms.Label();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.lblEND_USE_DATE = new System.Windows.Forms.Label();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.cboProvince = new System.Windows.Forms.ComboBox();
            this.lblPROVINCE = new System.Windows.Forms.Label();
            this.lblDISTRICT = new System.Windows.Forms.Label();
            this.lblCOMMUNE = new System.Windows.Forms.Label();
            this.lblVILLAGE = new System.Windows.Forms.Label();
            this.cboVillage = new System.Windows.Forms.ComboBox();
            this.cboCommune = new System.Windows.Forms.ComboBox();
            this.cboDistrict = new System.Windows.Forms.ComboBox();
            this.lblAREA = new System.Windows.Forms.Label();
            this.lblCOLLECTOR = new System.Windows.Forms.Label();
            this.lblBILLER = new System.Windows.Forms.Label();
            this.lblCUTTER = new System.Windows.Forms.Label();
            this.cboBiller = new System.Windows.Forms.ComboBox();
            this.cboAreaName = new System.Windows.Forms.ComboBox();
            this.cboCollector = new System.Windows.Forms.ComboBox();
            this.cboCutter = new System.Windows.Forms.ComboBox();
            this.lblTRANSFORMER = new System.Windows.Forms.Label();
            this.cboTransfo = new System.Windows.Forms.ComboBox();
            this.lblSTATUS = new System.Windows.Forms.Label();
            this.lblOWN = new System.Windows.Forms.CheckBox();
            this.lblPole_ = new System.Windows.Forms.Label();
            this.lblTOTAL_POLE_UPDATE = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.lblPole_);
            this.content.Controls.Add(this.lblTOTAL_POLE_UPDATE);
            this.content.Controls.Add(this.cboVillage);
            this.content.Controls.Add(this.cboCommune);
            this.content.Controls.Add(this.cboDistrict);
            this.content.Controls.Add(this.lblVILLAGE);
            this.content.Controls.Add(this.lblCOMMUNE);
            this.content.Controls.Add(this.lblDISTRICT);
            this.content.Controls.Add(this.cboProvince);
            this.content.Controls.Add(this.lblPROVINCE);
            this.content.Controls.Add(this.lblEND_USE_DATE);
            this.content.Controls.Add(this.dtpEndDate);
            this.content.Controls.Add(this.lblOWN);
            this.content.Controls.Add(this.lblSTATUS);
            this.content.Controls.Add(this.lblSTART_USE_DATE);
            this.content.Controls.Add(this.dtpStartDate);
            this.content.Controls.Add(this.btnAddTransfo);
            this.content.Controls.Add(this.cboTransfo);
            this.content.Controls.Add(this.lblTRANSFORMER);
            this.content.Controls.Add(this.btnAddCutter);
            this.content.Controls.Add(this.cboCutter);
            this.content.Controls.Add(this.btnAddBiller);
            this.content.Controls.Add(this.btnAddCollector);
            this.content.Controls.Add(this.cboCollector);
            this.content.Controls.Add(this.btnAddArea);
            this.content.Controls.Add(this.cboAreaName);
            this.content.Controls.Add(this.cboBiller);
            this.content.Controls.Add(this.lblCUTTER);
            this.content.Controls.Add(this.lblBILLER);
            this.content.Controls.Add(this.lblCOLLECTOR);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.lblAREA);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblAREA, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.lblCOLLECTOR, 0);
            this.content.Controls.SetChildIndex(this.lblBILLER, 0);
            this.content.Controls.SetChildIndex(this.lblCUTTER, 0);
            this.content.Controls.SetChildIndex(this.cboBiller, 0);
            this.content.Controls.SetChildIndex(this.cboAreaName, 0);
            this.content.Controls.SetChildIndex(this.btnAddArea, 0);
            this.content.Controls.SetChildIndex(this.cboCollector, 0);
            this.content.Controls.SetChildIndex(this.btnAddCollector, 0);
            this.content.Controls.SetChildIndex(this.btnAddBiller, 0);
            this.content.Controls.SetChildIndex(this.cboCutter, 0);
            this.content.Controls.SetChildIndex(this.btnAddCutter, 0);
            this.content.Controls.SetChildIndex(this.lblTRANSFORMER, 0);
            this.content.Controls.SetChildIndex(this.cboTransfo, 0);
            this.content.Controls.SetChildIndex(this.btnAddTransfo, 0);
            this.content.Controls.SetChildIndex(this.dtpStartDate, 0);
            this.content.Controls.SetChildIndex(this.lblSTART_USE_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblSTATUS, 0);
            this.content.Controls.SetChildIndex(this.lblOWN, 0);
            this.content.Controls.SetChildIndex(this.dtpEndDate, 0);
            this.content.Controls.SetChildIndex(this.lblEND_USE_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblPROVINCE, 0);
            this.content.Controls.SetChildIndex(this.cboProvince, 0);
            this.content.Controls.SetChildIndex(this.lblDISTRICT, 0);
            this.content.Controls.SetChildIndex(this.lblCOMMUNE, 0);
            this.content.Controls.SetChildIndex(this.lblVILLAGE, 0);
            this.content.Controls.SetChildIndex(this.cboDistrict, 0);
            this.content.Controls.SetChildIndex(this.cboCommune, 0);
            this.content.Controls.SetChildIndex(this.cboVillage, 0);
            this.content.Controls.SetChildIndex(this.lblTOTAL_POLE_UPDATE, 0);
            this.content.Controls.SetChildIndex(this.lblPole_, 0);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnAddArea
            // 
            this.btnAddArea.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnAddArea, "btnAddArea");
            this.btnAddArea.Name = "btnAddArea";
            this.btnAddArea.AddItem += new System.EventHandler(this.btnAddArea_AddItem);
            // 
            // btnAddCollector
            // 
            this.btnAddCollector.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnAddCollector, "btnAddCollector");
            this.btnAddCollector.Name = "btnAddCollector";
            this.btnAddCollector.AddItem += new System.EventHandler(this.btnAddCollector_AddItem);
            // 
            // btnAddBiller
            // 
            this.btnAddBiller.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnAddBiller, "btnAddBiller");
            this.btnAddBiller.Name = "btnAddBiller";
            this.btnAddBiller.AddItem += new System.EventHandler(this.btnAddBiller_AddItem);
            // 
            // btnAddCutter
            // 
            this.btnAddCutter.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnAddCutter, "btnAddCutter");
            this.btnAddCutter.Name = "btnAddCutter";
            this.btnAddCutter.AddItem += new System.EventHandler(this.btnAddCutter_AddItem);
            // 
            // btnAddTransfo
            // 
            this.btnAddTransfo.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnAddTransfo, "btnAddTransfo");
            this.btnAddTransfo.Name = "btnAddTransfo";
            this.btnAddTransfo.AddItem += new System.EventHandler(this.btnAddTransfo_AddItem);
            // 
            // lblSTART_USE_DATE
            // 
            resources.ApplyResources(this.lblSTART_USE_DATE, "lblSTART_USE_DATE");
            this.lblSTART_USE_DATE.Name = "lblSTART_USE_DATE";
            // 
            // dtpStartDate
            // 
            this.dtpStartDate.Checked = false;
            resources.ApplyResources(this.dtpStartDate, "dtpStartDate");
            this.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.ShowCheckBox = true;
            // 
            // lblEND_USE_DATE
            // 
            resources.ApplyResources(this.lblEND_USE_DATE, "lblEND_USE_DATE");
            this.lblEND_USE_DATE.Name = "lblEND_USE_DATE";
            // 
            // dtpEndDate
            // 
            this.dtpEndDate.Checked = false;
            resources.ApplyResources(this.dtpEndDate, "dtpEndDate");
            this.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.ShowCheckBox = true;
            this.dtpEndDate.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dtpEndDate_MouseDown);
            // 
            // cboProvince
            // 
            this.cboProvince.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboProvince, "cboProvince");
            this.cboProvince.FormattingEnabled = true;
            this.cboProvince.Name = "cboProvince";
            this.cboProvince.SelectedIndexChanged += new System.EventHandler(this.cboProvince_SelectedIndexChanged);
            // 
            // lblPROVINCE
            // 
            resources.ApplyResources(this.lblPROVINCE, "lblPROVINCE");
            this.lblPROVINCE.Name = "lblPROVINCE";
            // 
            // lblDISTRICT
            // 
            resources.ApplyResources(this.lblDISTRICT, "lblDISTRICT");
            this.lblDISTRICT.Name = "lblDISTRICT";
            // 
            // lblCOMMUNE
            // 
            resources.ApplyResources(this.lblCOMMUNE, "lblCOMMUNE");
            this.lblCOMMUNE.Name = "lblCOMMUNE";
            // 
            // lblVILLAGE
            // 
            resources.ApplyResources(this.lblVILLAGE, "lblVILLAGE");
            this.lblVILLAGE.Name = "lblVILLAGE";
            // 
            // cboVillage
            // 
            this.cboVillage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboVillage, "cboVillage");
            this.cboVillage.FormattingEnabled = true;
            this.cboVillage.Name = "cboVillage";
            // 
            // cboCommune
            // 
            this.cboCommune.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboCommune, "cboCommune");
            this.cboCommune.FormattingEnabled = true;
            this.cboCommune.Name = "cboCommune";
            this.cboCommune.SelectedIndexChanged += new System.EventHandler(this.cboCommune_SelectedIndexChanged);
            // 
            // cboDistrict
            // 
            this.cboDistrict.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboDistrict, "cboDistrict");
            this.cboDistrict.FormattingEnabled = true;
            this.cboDistrict.Name = "cboDistrict";
            this.cboDistrict.SelectedIndexChanged += new System.EventHandler(this.cboDistrict_SelectedIndexChanged);
            // 
            // lblAREA
            // 
            resources.ApplyResources(this.lblAREA, "lblAREA");
            this.lblAREA.Name = "lblAREA";
            // 
            // lblCOLLECTOR
            // 
            resources.ApplyResources(this.lblCOLLECTOR, "lblCOLLECTOR");
            this.lblCOLLECTOR.Name = "lblCOLLECTOR";
            // 
            // lblBILLER
            // 
            resources.ApplyResources(this.lblBILLER, "lblBILLER");
            this.lblBILLER.Name = "lblBILLER";
            // 
            // lblCUTTER
            // 
            resources.ApplyResources(this.lblCUTTER, "lblCUTTER");
            this.lblCUTTER.Name = "lblCUTTER";
            // 
            // cboBiller
            // 
            this.cboBiller.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBiller.FormattingEnabled = true;
            resources.ApplyResources(this.cboBiller, "cboBiller");
            this.cboBiller.Name = "cboBiller";
            // 
            // cboAreaName
            // 
            this.cboAreaName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAreaName.FormattingEnabled = true;
            resources.ApplyResources(this.cboAreaName, "cboAreaName");
            this.cboAreaName.Name = "cboAreaName";
            // 
            // cboCollector
            // 
            this.cboCollector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCollector.FormattingEnabled = true;
            resources.ApplyResources(this.cboCollector, "cboCollector");
            this.cboCollector.Name = "cboCollector";
            // 
            // cboCutter
            // 
            this.cboCutter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCutter.FormattingEnabled = true;
            resources.ApplyResources(this.cboCutter, "cboCutter");
            this.cboCutter.Name = "cboCutter";
            // 
            // lblTRANSFORMER
            // 
            resources.ApplyResources(this.lblTRANSFORMER, "lblTRANSFORMER");
            this.lblTRANSFORMER.Name = "lblTRANSFORMER";
            // 
            // cboTransfo
            // 
            this.cboTransfo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTransfo.FormattingEnabled = true;
            resources.ApplyResources(this.cboTransfo, "cboTransfo");
            this.cboTransfo.Name = "cboTransfo";
            // 
            // lblSTATUS
            // 
            resources.ApplyResources(this.lblSTATUS, "lblSTATUS");
            this.lblSTATUS.Name = "lblSTATUS";
            // 
            // lblOWN
            // 
            resources.ApplyResources(this.lblOWN, "lblOWN");
            this.lblOWN.Name = "lblOWN";
            this.lblOWN.UseVisualStyleBackColor = true;
            this.lblOWN.CheckedChanged += new System.EventHandler(this.lblOWN_CheckedChanged);
            // 
            // lblPole_
            // 
            resources.ApplyResources(this.lblPole_, "lblPole_");
            this.lblPole_.Name = "lblPole_";
            // 
            // lblTOTAL_POLE_UPDATE
            // 
            resources.ApplyResources(this.lblTOTAL_POLE_UPDATE, "lblTOTAL_POLE_UPDATE");
            this.lblTOTAL_POLE_UPDATE.Name = "lblTOTAL_POLE_UPDATE";
            // 
            // DialogPoleBatchChange
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogPoleBatchChange";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private ExAddItem btnAddArea;
        private ExAddItem btnAddCollector;
        private ExAddItem btnAddBiller;
        private ExAddItem btnAddCutter;
        private ExAddItem btnAddTransfo;
        private Label lblSTART_USE_DATE;
        private Label lblEND_USE_DATE;
        private Label lblPROVINCE;
        private Label lblDISTRICT;
        private Label lblCOMMUNE;
        private Label lblVILLAGE;
        private Label lblSTATUS;
        private Label lblTRANSFORMER;
        private Label lblCUTTER;
        private Label lblBILLER;
        private Label lblCOLLECTOR;
        private Label lblAREA;
        public DateTimePicker dtpStartDate;
        public DateTimePicker dtpEndDate;
        public ComboBox cboProvince;
        public ComboBox cboVillage;
        public ComboBox cboCommune;
        public ComboBox cboDistrict;
        public CheckBox lblOWN;
        public ComboBox cboTransfo;
        public ComboBox cboCutter;
        public ComboBox cboCollector;
        public ComboBox cboAreaName;
        public ComboBox cboBiller;
        private Label lblPole_;
        private Label lblTOTAL_POLE_UPDATE;
    }
}