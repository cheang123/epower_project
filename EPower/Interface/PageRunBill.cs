﻿using EPower.Base.Helper.DevExpressCustomize;
using EPower.Base.Logic;
using EPower.Interface.BankPayment;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageRunBill : Form
    {
        #region Data
        DateTime dtMonth = new DateTime();
        #endregion

        #region Constructor
        public PageRunBill()
        {
            InitializeComponent();
            dgvBillingCycle.SetDefaultGridview();
            searchControl.Properties.NullValuePrompt = Resources.SEARCH;
        }
        #endregion Constructor

        #region Method

        public void bind()
        {
            List<BillingCycleModel> dt = new List<BillingCycleModel>();
            Runner.RunNewThread(delegate ()
            {
                try
                {
                    using (TransactionScope tran = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions() { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                    {
                        foreach (TBL_BILLING_CYCLE bc in DBDataContext.Db.TBL_BILLING_CYCLEs.Where(b => b.IS_ACTIVE).OrderBy(x => x.CYCLE_NAME))
                        {
                            int MAX_DAY_TO_IGNORE_BILLING = DataHelper.ParseToInt(Method.Utilities[Utility.MAX_DAY_TO_IGNORE_BILLING]);

                            int totalCustomer = (from c in DBDataContext.Db.TBL_CUSTOMERs
                                                 join cm in DBDataContext.Db.TBL_CUSTOMER_METERs on c.CUSTOMER_ID equals cm.CUSTOMER_ID
                                                 where c.BILLING_CYCLE_ID == bc.CYCLE_ID
                                                         && cm.IS_ACTIVE
                                                         && c.IS_POST_PAID
                                                         && (c.STATUS_ID == (int)CustomerStatus.Active || c.STATUS_ID == (int)CustomerStatus.Blocked)
                                                 select c).Count();

                            //if have billing cycle to run
                            if (totalCustomer > 0)
                            {
                                DateTime datStart = UIHelper._DefaultDate;
                                DateTime datEnd = UIHelper._DefaultDate;
                                DateTime datRuningMonth = Method.GetNextBillingMonth(bc.CYCLE_ID, ref datStart, ref datEnd);

                                int billingCustomer = (from c in DBDataContext.Db.TBL_CUSTOMERs
                                                       join cm in DBDataContext.Db.TBL_CUSTOMER_METERs on c.CUSTOMER_ID equals cm.CUSTOMER_ID
                                                       where c.BILLING_CYCLE_ID == bc.CYCLE_ID
                                                           && cm.IS_ACTIVE
                                                           && c.IS_POST_PAID
                                                           && (c.STATUS_ID == (int)CustomerStatus.Active || c.STATUS_ID == (int)CustomerStatus.Blocked)
                                                           && (DBDataContext.Db.GetSystemDate() - c.ACTIVATE_DATE).TotalDays >= MAX_DAY_TO_IGNORE_BILLING
                                                       select c.CUSTOMER_ID).Count();

                                int intNoneUsageCustomer = Method.GetNoneUsageCustomer(bc.CYCLE_ID, datRuningMonth.Date);

                                dt.Add(new BillingCycleModel
                                {
                                    CYCLE_ID = bc.CYCLE_ID,
                                    CYCLE_NAME = bc.CYCLE_NAME,
                                    BILLING_MONTH = datRuningMonth.Date,
                                    START_DATE = datStart,
                                    END_DATE = datEnd,
                                    COLLECT_DAY = bc.COLLECT_DAY,
                                    TOTAL_CUSTOMER = totalCustomer,
                                    BILLING_CUSTOMER = billingCustomer,
                                    NO_USAGE = intNoneUsageCustomer
                                });
                            }
                        }
                        tran.Complete();
                    }
                }
                catch(Exception ex)
                {
                    MsgBox.LogError(ex);
                    throw new Exception(Base.Properties.Resources.YOU_CANNOT_VIEW_REPORT);
                }
                

            });
            dgv.DataSource = dt;
        }

        #endregion Method

        private void btnCheck_Click(object sender, EventArgs e)
        {
            if (dgvBillingCycle.SelectedRowsCount > 0)
            {
                List<BillingCycleModel> billings = new List<BillingCycleModel>();
                foreach (var i in dgvBillingCycle.GetSelectedRows())
                {
                    BillingCycleModel b = this.dgvBillingCycle.GetRow(i) as BillingCycleModel;
                    billings.Add(b);
                }
                (new DialogCustomerNoUsage(billings.Select(x => x.CYCLE_ID).ToList())).ShowDialog();
                bind();
            }
            //BillingCycleModel billingCycle = this.dgvBillingCycle.GetFocusedRow() as BillingCycleModel;
            //if (billingCycle != null)
            //{
            //    int intBillingCycle = billingCycle.CYCLE_ID;
            //    //DateTime datRunMonth = billingCycle.BILLING_MONTH;
            //    (new DialogCustomerNoUsage(intBillingCycle)).ShowDialog();
            //    bind();
            //}
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            if (dgvBillingCycle.SelectedRowsCount > 0)
            {
                List<BillingCycleModel> billings = new List<BillingCycleModel>();
                foreach (var i in dgvBillingCycle.GetSelectedRows())
                {
                    BillingCycleModel b = this.dgvBillingCycle.GetRow(i) as BillingCycleModel;
                    if (billings.Count == 0 || b.BILLING_MONTH == billings.FirstOrDefault().BILLING_MONTH)
                    {
                        billings.Add(b);
                    }
                }

                dtMonth = billings.FirstOrDefault().BILLING_MONTH;

                //// meter un register. 
                TBL_CONFIG conf = DBDataContext.Db.TBL_CONFIGs.FirstOrDefault();
                int intMeterUnregister = DBDataContext.Db.TBL_METERs.Where(row => row.REGISTER_CODE == "").Count();
                if (conf.IS_LOCK_METER && intMeterUnregister > 0)
                {
                    MsgBox.ShowInformation(string.Format(Resources.MS_CANNOT_RUN_BIL_BECAUSE_HAVE_METER_UNREGISTER, intMeterUnregister));
                    return;
                }
                // customer no usage.                
                int intNoneUsageCus = billings.Sum(x => x.NO_USAGE);
                if (intNoneUsageCus > 0)
                {
                    MsgBox.ShowInformation(string.Format(Resources.MS_CANNOT_RUN_BILL_BECAUSE_HAVE_CUSTOMER_NO_USAGE, intNoneUsageCus.ToString()));
                    return;
                }
                var now = DBDataContext.Db.GetSystemDate();
                DialogRunBill diag = new DialogRunBill();
                diag.cycleIds = billings.Select(x => x.CYCLE_ID).ToList();
                diag.txtCYCLE_NAME.Text = string.Join(",", billings.Select(x => x.CYCLE_NAME));
                diag.dtpRunMonth.Value = dtMonth;
                diag.dtpFrom.Value = billings.FirstOrDefault().START_DATE;
                diag.dtpTo.Value = billings.FirstOrDefault().END_DATE;
                if (diag.dtpRunMonth.Value.Year != now.Year || diag.dtpRunMonth.Value.Month != now.Month)
                {
                    diag.dtpInvoice_Date.CustomFormat = @" ";
                }

                if (diag.ShowDialog() == DialogResult.OK)
                {
                    bind();
                    if (DialogRemindSendData.IsRequiredReminder())
                    {
                        new DialogRemindSendData().ShowDialog();
                    }
                    PostingLogic.Instance.PostToPointerBg(DateTime.Now.Date, DateTime.Now.Date.AddDays(1).AddSeconds(-1));
                }
            }
            else
            {
                MsgBox.ShowInformation(string.Format(Resources.REQUIRED_SELECT_BEFORE_PROCESS, Resources.BILLING_CYCLE, Resources.RUN_BILL));
            }
        }

        private void btnViewUnregister_Click(object sender, EventArgs e)
        {
            DialogMeterUnregister diag = new DialogMeterUnregister();
            diag.ShowDialog();
        }

        private void btnSpecialUsage_Click(object sender, EventArgs e)
        {
            BillingCycleModel billingCycle = this.dgvBillingCycle.GetFocusedRow() as BillingCycleModel;
            if (billingCycle != null)
            {
                var diag = new DialogCustomerSpecialUsage();
                diag.cboCycle.SelectedValue = billingCycle.CYCLE_ID;
                diag.ShowDialog();
            }
        }
    }
    class BillingCycleModel
    {
        public int CYCLE_ID { get; set; }
        public string CYCLE_NAME { get; set; }
        public DateTime BILLING_MONTH { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
        public int COLLECT_DAY { get; set; }
        public int TOTAL_CUSTOMER { get; set; }
        public int BILLING_CUSTOMER { get; set; }
        public int NO_USAGE { get; set; }
    }
}