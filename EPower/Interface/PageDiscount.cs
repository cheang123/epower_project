﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PageDiscount : Form
    { 
        public TBL_DISCOUNT Discount
        {
            get
            {
                TBL_DISCOUNT obj = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    int discountId = (int)dgv.SelectedRows[0].Cells["DISCOUNT_ID"].Value;
                    try
                    {
                        obj = DBDataContext.Db.TBL_DISCOUNTs.FirstOrDefault(x => x.DISCOUNT_ID == discountId);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return obj;
            }
        }

        #region Constructor
        public PageDiscount()
        {
            InitializeComponent(); 
            UIHelper.DataGridViewProperties(dgv); 
            DataTable dt = DBDataContext.Db.TLKP_DISCOUNT_TYPEs._ToDataTable();
            DataRow newDiscounType = dt.NewRow();
            newDiscounType[0] = 0;
            newDiscounType[1] = Resources.ALL_TYPE;
            dt.Rows.InsertAt(newDiscounType, 0);
            UIHelper.SetDataSourceToComboBox(cboDiscountType, dt);            
            this.bindData();
        }
        #endregion

        #region Operation
        /// <summary>
        /// Load data from database.
        /// </summary>
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            bindData(); 
        }

        private void bindData()
        {
            try
            {
                if (cboDiscountType.SelectedIndex == -1)
                {
                    return;
                }
                int discountTypeId = (int)cboDiscountType.SelectedValue;

                dgv.DataSource = from d in DBDataContext.Db.TBL_DISCOUNTs
                                 join t in DBDataContext.Db.TLKP_DISCOUNT_TYPEs on d.DISCOUNT_TYPE_ID equals t.DISCOUNT_TYPE_ID
                                 where d.IS_ACTIVE && (discountTypeId==0 || d.DISCOUNT_TYPE_ID==discountTypeId)
                                 select new
                                 {
                                     DISCOUNT_ID = d.DISCOUNT_ID,
                                     DISCOUNT_NAME = d.DISCOUNT_NAME,
                                     DISCOUNT = d.DISCOUNT, 
                                     DISCOUNT_TYPE=t.DISCOUNT_TYPE,
                                     DISCOUNT_UNIT = (d.IS_PERCENTAGE ? " %" : (d.DISCOUNT_TYPE_ID==(int)DiscounType.DiscountUsage?" kWh" : DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(c=>c.IS_DEFAULT_CURRENCY).CURRENCY_NAME)),
                                     DESCRIPTION = d.DESCRIPTION
                                 };
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        } 
        /// <summary>
        /// Add new area.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNew_Click(object sender, EventArgs e)
        {
            DialogDiscount dig = new DialogDiscount(GeneralProcess.Insert, new TBL_DISCOUNT() { DISCOUNT_NAME = "",DISCOUNT_TYPE_ID=1});
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.Discount.DISCOUNT_ID);
            }
        }

        /// <summary>
        /// Edit area.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                DialogDiscount dig = new DialogDiscount(GeneralProcess.Update, Discount);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.Discount.DISCOUNT_ID);
                }
            }
        }

        /// <summary>
        /// Remove area.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if ( IsDeletable())
            {
                DialogDiscount dig = new DialogDiscount(GeneralProcess.Delete, Discount);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.Discount.DISCOUNT_ID - 1);
                }
            }
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        #endregion

        #region Method               
        private bool IsDeletable()
        {
            bool val = false;
            if (dgv.SelectedRows.Count>0)
            {
                val = true;
                if (DBDataContext.Db.TBL_CUSTOMER_DISCOUNTs.Where(x => x.DISCOUNT_ID == Discount.DISCOUNT_ID && x.IS_ACTIVE).Count() > 0)
                {
                    MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE,Resources.INFORMATION);
                    val = false;
                }
            }            
            return val;
        }
        #endregion

        private void btnCutomer_Click(object sender, EventArgs e)
        {
            if (this.Discount != null)
            {
                DialogDiscountCustomer diag = new DialogDiscountCustomer(this.Discount);
                diag.ShowDialog();
            }
        }

        private void cboDiscountType_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindData();
        }

        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }
    }
}
