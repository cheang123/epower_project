﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogCustomerType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCustomerType));
            this.lblCUSTOMER_TYPE = new System.Windows.Forms.Label();
            this.txtCustomerTypeName = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.label9 = new System.Windows.Forms.Label();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.lblDEPOSIT_USAGE = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDepositUsage = new System.Windows.Forms.TextBox();
            this.txtDailySupplyHour = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblDAILY_SUPPLY_HOUR = new System.Windows.Forms.Label();
            this.txtDailySupplySchedule = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lblDAILY_SUPPLY_SCHEDULE = new System.Windows.Forms.Label();
            this.lblINCOME_ACCOUNT = new System.Windows.Forms.Label();
            this.cboIncomeAccount = new System.Windows.Forms.ComboBox();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.lblNOTE = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.txtNote);
            this.content.Controls.Add(this.lblNOTE);
            this.content.Controls.Add(this.cboIncomeAccount);
            this.content.Controls.Add(this.lblINCOME_ACCOUNT);
            this.content.Controls.Add(this.txtDailySupplySchedule);
            this.content.Controls.Add(this.label6);
            this.content.Controls.Add(this.lblDAILY_SUPPLY_SCHEDULE);
            this.content.Controls.Add(this.txtDailySupplyHour);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.lblDAILY_SUPPLY_HOUR);
            this.content.Controls.Add(this.txtDepositUsage);
            this.content.Controls.Add(this.label2);
            this.content.Controls.Add(this.lblDEPOSIT_USAGE);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.txtCustomerTypeName);
            this.content.Controls.Add(this.lblCUSTOMER_TYPE);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_TYPE, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerTypeName, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.lblDEPOSIT_USAGE, 0);
            this.content.Controls.SetChildIndex(this.label2, 0);
            this.content.Controls.SetChildIndex(this.txtDepositUsage, 0);
            this.content.Controls.SetChildIndex(this.lblDAILY_SUPPLY_HOUR, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.txtDailySupplyHour, 0);
            this.content.Controls.SetChildIndex(this.lblDAILY_SUPPLY_SCHEDULE, 0);
            this.content.Controls.SetChildIndex(this.label6, 0);
            this.content.Controls.SetChildIndex(this.txtDailySupplySchedule, 0);
            this.content.Controls.SetChildIndex(this.lblINCOME_ACCOUNT, 0);
            this.content.Controls.SetChildIndex(this.cboIncomeAccount, 0);
            this.content.Controls.SetChildIndex(this.lblNOTE, 0);
            this.content.Controls.SetChildIndex(this.txtNote, 0);
            // 
            // lblCUSTOMER_TYPE
            // 
            resources.ApplyResources(this.lblCUSTOMER_TYPE, "lblCUSTOMER_TYPE");
            this.lblCUSTOMER_TYPE.Name = "lblCUSTOMER_TYPE";
            // 
            // txtCustomerTypeName
            // 
            resources.ApplyResources(this.txtCustomerTypeName, "txtCustomerTypeName");
            this.txtCustomerTypeName.Name = "txtCustomerTypeName";
            this.txtCustomerTypeName.Enter += new System.EventHandler(this.InputKhmer);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // lblDEPOSIT_USAGE
            // 
            resources.ApplyResources(this.lblDEPOSIT_USAGE, "lblDEPOSIT_USAGE");
            this.lblDEPOSIT_USAGE.Name = "lblDEPOSIT_USAGE";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Name = "label2";
            // 
            // txtDepositUsage
            // 
            resources.ApplyResources(this.txtDepositUsage, "txtDepositUsage");
            this.txtDepositUsage.Name = "txtDepositUsage";
            this.txtDepositUsage.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // txtDailySupplyHour
            // 
            resources.ApplyResources(this.txtDailySupplyHour, "txtDailySupplyHour");
            this.txtDailySupplyHour.Name = "txtDailySupplyHour";
            this.txtDailySupplyHour.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // lblDAILY_SUPPLY_HOUR
            // 
            resources.ApplyResources(this.lblDAILY_SUPPLY_HOUR, "lblDAILY_SUPPLY_HOUR");
            this.lblDAILY_SUPPLY_HOUR.Name = "lblDAILY_SUPPLY_HOUR";
            // 
            // txtDailySupplySchedule
            // 
            resources.ApplyResources(this.txtDailySupplySchedule, "txtDailySupplySchedule");
            this.txtDailySupplySchedule.Name = "txtDailySupplySchedule";
            this.txtDailySupplySchedule.Enter += new System.EventHandler(this.InputKhmer);
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Name = "label6";
            // 
            // lblDAILY_SUPPLY_SCHEDULE
            // 
            resources.ApplyResources(this.lblDAILY_SUPPLY_SCHEDULE, "lblDAILY_SUPPLY_SCHEDULE");
            this.lblDAILY_SUPPLY_SCHEDULE.Name = "lblDAILY_SUPPLY_SCHEDULE";
            // 
            // lblINCOME_ACCOUNT
            // 
            resources.ApplyResources(this.lblINCOME_ACCOUNT, "lblINCOME_ACCOUNT");
            this.lblINCOME_ACCOUNT.Name = "lblINCOME_ACCOUNT";
            // 
            // cboIncomeAccount
            // 
            this.cboIncomeAccount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboIncomeAccount.FormattingEnabled = true;
            resources.ApplyResources(this.cboIncomeAccount, "cboIncomeAccount");
            this.cboIncomeAccount.Name = "cboIncomeAccount";
            // 
            // txtNote
            // 
            resources.ApplyResources(this.txtNote, "txtNote");
            this.txtNote.Name = "txtNote";
            this.txtNote.Enter += new System.EventHandler(this.InputKhmer);
            // 
            // lblNOTE
            // 
            resources.ApplyResources(this.lblNOTE, "lblNOTE");
            this.lblNOTE.Name = "lblNOTE";
            // 
            // DialogCustomerType
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogCustomerType";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBox txtCustomerTypeName;
        private Label lblCUSTOMER_TYPE;
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label label9;
        private ExButton btnCHANGE_LOG;
        private TextBox txtDepositUsage;
        private Label label2;
        private Label lblDEPOSIT_USAGE;
        private TextBox txtDailySupplySchedule;
        private Label label6;
        private Label lblDAILY_SUPPLY_SCHEDULE;
        private TextBox txtDailySupplyHour;
        private Label label1;
        private Label lblDAILY_SUPPLY_HOUR;
        private Label lblINCOME_ACCOUNT;
        public ComboBox cboIncomeAccount;
        private TextBox txtNote;
        private Label lblNOTE;
    }
}