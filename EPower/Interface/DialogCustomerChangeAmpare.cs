﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogCustomerChangeAmpare : ExDialog
    {
        public TBL_CUSTOMER _objCustomer = new TBL_CUSTOMER();
        DateTime? dt;
        int? value;
        TBL_CUSTOMER_METER objCusMeter;

        public DialogCustomerChangeAmpare(TBL_CUSTOMER objCustomer)
        {
            InitializeComponent();
            //permission
            this.dtpCHANGE_DATE.Enabled = SoftTech.Security.Logic.Login.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_REGISTER_UPDATE_CHANGE_AMPARE_DATE);
            this._objCustomer = objCustomer;
            UIHelper.SetDataSourceToComboBox(this.cboOldAmpare, Lookup.GetPowerAmpare());
            UIHelper.SetDataSourceToComboBox(this.cboNewAmpare, Lookup.GetPowerAmpare());
            UIHelper.SetDataSourceToComboBox(this.cboReason, DBDataContext.Db.TLKP_REASONs.Where(x => x.IS_ACTIVE && x.TYPE_ID == (int)ReasonType.ChangeAmpere).Select(x => new { x.REASON_ID, x.REASON }), "");
            this.read();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void read()
        {
            this.cboOldAmpare.Enabled = false;
            this.txtCUSTOMER_CODE.Text = this._objCustomer.CUSTOMER_CODE;
            this.txtCUSTOMER_NAME.Text = this._objCustomer.LAST_NAME_KH + " " + this._objCustomer.FIRST_NAME_KH;
            this.cboOldAmpare.SelectedValue = this._objCustomer.AMP_ID;
            this.cboNewAmpare.SelectedIndex = 0;
            this.dtpCHANGE_DATE.Value = DBDataContext.Db.GetSystemDate();
        }

        private bool invalid()
        {
            bool result = false;
            this.ClearAllValidation();

            if (this.cboNewAmpare.SelectedIndex == -1)
            {
                cboNewAmpare.SetValidation(string.Format(Resources.REQUIRED_SELECT_VALUE, this.lblNEW_AMPARE.Text));
                result = true;
            }
            if (cboReason.SelectedIndex <= 0)
            {
                cboReason.SetValidation(string.Format(Resources.REQUIRED, this.lblREASON.Text));
                result = true;
            }
            if ((int)cboOldAmpare.SelectedValue == (int)cboNewAmpare.SelectedValue)
            {
                cboNewAmpare.SetValidation(string.Format(Resources.MSG_NEW_AMPARE_AND_OLD_AMPARE_IS_THE_SAME));
                result = true;
            }
            return result;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            TBL_CUSTOMER_METER _objCusMeter = DBDataContext.Db.TBL_CUSTOMER_METERs.FirstOrDefault(x => x.CUSTOMER_ID == _objCustomer.CUSTOMER_ID && x.IS_ACTIVE);
            
            if(_objCusMeter == null)
            {
                dt = null;
                value = null;
            }
            else
            {
                objCusMeter = DBDataContext.Db.TBL_CUSTOMER_METERs.FirstOrDefault(x => x.METER_ID == _objCusMeter.METER_ID);
            }

            // validate it first.
            if (invalid())
            {
                return;
            }
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    // log user change location
                    DBDataContext.Db.TBL_CUSTOMER_CHANGE_AMPAREs.InsertOnSubmit(new TBL_CUSTOMER_CHANGE_AMPARE
                    {
                        CREATE_BY = SoftTech.Security.Logic.Login.CurrentLogin.LOGIN_NAME,
                        CREATE_ON = dtpCHANGE_DATE.Value,
                        CUSTOMER_ID = _objCustomer.CUSTOMER_ID,
                        LOGIN_ID = SoftTech.Security.Logic.Login.CurrentLogin.LOGIN_ID,
                        NEW_AMPARE_ID = (int)cboNewAmpare.SelectedValue,
                        OLD_AMPARE_ID = _objCustomer.AMP_ID,
                        CHANGE_DATE = dtpCHANGE_DATE.Value,
                        NOTE = cboReason.Text,
                        REASON_ID = (int)cboReason.SelectedValue,
                        IS_ACTIVE = true,
                        USED_DATE = objCusMeter == null? dt : objCusMeter.USED_DATE,
                        METER_ID = objCusMeter == null? value : objCusMeter.METER_ID
                    });;

                    // Update Ampare Customer
                    TBL_CUSTOMER objCus = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == _objCustomer.CUSTOMER_ID);
                    objCus.AMP_ID = (int)cboNewAmpare.SelectedValue;
                    _objCustomer.AMP_ID = (int)cboNewAmpare.SelectedValue;

                    DBDataContext.Db.SubmitChanges();
                    tran.Complete();
                }

                // if succes than close form.
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void btnCHANGE_LOG_Click(object sender, EventArgs e)
        {
            new DialogCustomerChangeAmpareHistory(_objCustomer.CUSTOMER_ID).ShowDialog();
        }

        private void ChangeKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
    }
}
