﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogCashDrawer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCashDrawer));
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.tabLOGIN = new System.Windows.Forms.TabPage();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.chk = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.LOGIN_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LOGIN_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabCASH_DRAWER = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDefaultDayClose = new System.Windows.Forms.TextBox();
            this.txtDefaultFloat = new System.Windows.Forms.TextBox();
            this.txtCashDrawName = new System.Windows.Forms.TextBox();
            this.lblDefaultDayClose = new System.Windows.Forms.Label();
            this.chkIS_BANK = new System.Windows.Forms.CheckBox();
            this.lblIsBank = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblCURRENCY = new System.Windows.Forms.Label();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.lblDEFAULT_FLOATING_CASH = new System.Windows.Forms.Label();
            this.lblCASH_DRAWER = new System.Windows.Forms.Label();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabLOGIN.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.tabCASH_DRAWER.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.tabControl);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.tabControl, 0);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // tabLOGIN
            // 
            this.tabLOGIN.Controls.Add(this.dgv);
            resources.ApplyResources(this.tabLOGIN, "tabLOGIN");
            this.tabLOGIN.Name = "tabLOGIN";
            this.tabLOGIN.UseVisualStyleBackColor = true;
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.ColumnHeadersVisible = false;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.chk,
            this.LOGIN_ID,
            this.LOGIN_NAME});
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellContentClick);
            // 
            // chk
            // 
            this.chk.DataPropertyName = "chk";
            resources.ApplyResources(this.chk, "chk");
            this.chk.Name = "chk";
            this.chk.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // LOGIN_ID
            // 
            this.LOGIN_ID.DataPropertyName = "LOGIN_ID";
            resources.ApplyResources(this.LOGIN_ID, "LOGIN_ID");
            this.LOGIN_ID.Name = "LOGIN_ID";
            this.LOGIN_ID.ReadOnly = true;
            this.LOGIN_ID.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // LOGIN_NAME
            // 
            this.LOGIN_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.LOGIN_NAME.DataPropertyName = "LOGIN_NAME";
            resources.ApplyResources(this.LOGIN_NAME, "LOGIN_NAME");
            this.LOGIN_NAME.Name = "LOGIN_NAME";
            this.LOGIN_NAME.ReadOnly = true;
            this.LOGIN_NAME.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // tabCASH_DRAWER
            // 
            this.tabCASH_DRAWER.Controls.Add(this.label2);
            this.tabCASH_DRAWER.Controls.Add(this.txtDefaultDayClose);
            this.tabCASH_DRAWER.Controls.Add(this.txtDefaultFloat);
            this.tabCASH_DRAWER.Controls.Add(this.txtCashDrawName);
            this.tabCASH_DRAWER.Controls.Add(this.lblDefaultDayClose);
            this.tabCASH_DRAWER.Controls.Add(this.chkIS_BANK);
            this.tabCASH_DRAWER.Controls.Add(this.lblIsBank);
            this.tabCASH_DRAWER.Controls.Add(this.label8);
            this.tabCASH_DRAWER.Controls.Add(this.label7);
            this.tabCASH_DRAWER.Controls.Add(this.label9);
            this.tabCASH_DRAWER.Controls.Add(this.lblCURRENCY);
            this.tabCASH_DRAWER.Controls.Add(this.cboCurrency);
            this.tabCASH_DRAWER.Controls.Add(this.lblDEFAULT_FLOATING_CASH);
            this.tabCASH_DRAWER.Controls.Add(this.lblCASH_DRAWER);
            resources.ApplyResources(this.tabCASH_DRAWER, "tabCASH_DRAWER");
            this.tabCASH_DRAWER.Name = "tabCASH_DRAWER";
            this.tabCASH_DRAWER.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Name = "label2";
            // 
            // txtDefaultDayClose
            // 
            resources.ApplyResources(this.txtDefaultDayClose, "txtDefaultDayClose");
            this.txtDefaultDayClose.Name = "txtDefaultDayClose";
            // 
            // txtDefaultFloat
            // 
            resources.ApplyResources(this.txtDefaultFloat, "txtDefaultFloat");
            this.txtDefaultFloat.Name = "txtDefaultFloat";
            // 
            // txtCashDrawName
            // 
            resources.ApplyResources(this.txtCashDrawName, "txtCashDrawName");
            this.txtCashDrawName.Name = "txtCashDrawName";
            // 
            // lblDefaultDayClose
            // 
            resources.ApplyResources(this.lblDefaultDayClose, "lblDefaultDayClose");
            this.lblDefaultDayClose.Name = "lblDefaultDayClose";
            // 
            // chkIS_BANK
            // 
            resources.ApplyResources(this.chkIS_BANK, "chkIS_BANK");
            this.chkIS_BANK.Checked = true;
            this.chkIS_BANK.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIS_BANK.Name = "chkIS_BANK";
            this.chkIS_BANK.UseVisualStyleBackColor = true;
            // 
            // lblIsBank
            // 
            resources.ApplyResources(this.lblIsBank, "lblIsBank");
            this.lblIsBank.Name = "lblIsBank";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Name = "label8";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Name = "label7";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // lblCURRENCY
            // 
            resources.ApplyResources(this.lblCURRENCY, "lblCURRENCY");
            this.lblCURRENCY.Name = "lblCURRENCY";
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            this.cboCurrency.Items.AddRange(new object[] {
            resources.GetString("cboCurrency.Items"),
            resources.GetString("cboCurrency.Items1"),
            resources.GetString("cboCurrency.Items2"),
            resources.GetString("cboCurrency.Items3"),
            resources.GetString("cboCurrency.Items4")});
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            // 
            // lblDEFAULT_FLOATING_CASH
            // 
            resources.ApplyResources(this.lblDEFAULT_FLOATING_CASH, "lblDEFAULT_FLOATING_CASH");
            this.lblDEFAULT_FLOATING_CASH.Name = "lblDEFAULT_FLOATING_CASH";
            // 
            // lblCASH_DRAWER
            // 
            resources.ApplyResources(this.lblCASH_DRAWER, "lblCASH_DRAWER");
            this.lblCASH_DRAWER.Name = "lblCASH_DRAWER";
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabCASH_DRAWER);
            this.tabControl.Controls.Add(this.tabLOGIN);
            resources.ApplyResources(this.tabControl, "tabControl");
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            // 
            // DialogCashDrawer
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogCashDrawer";
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabLOGIN.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.tabCASH_DRAWER.ResumeLayout(false);
            this.tabCASH_DRAWER.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private ExButton btnCHANGE_LOG;
        private TabControl tabControl;
        private TabPage tabCASH_DRAWER;
        private Label label2;
        private TextBox txtDefaultDayClose;
        private TextBox txtDefaultFloat;
        private TextBox txtCashDrawName;
        private Label lblDefaultDayClose;
        private CheckBox chkIS_BANK;
        private Label lblIsBank;
        private Label label8;
        private Label label7;
        private Label label9;
        private Label lblCURRENCY;
        public ComboBox cboCurrency;
        private Label lblDEFAULT_FLOATING_CASH;
        private Label lblCASH_DRAWER;
        private TabPage tabLOGIN;
        private DataGridView dgv;
        private DataGridViewCheckBoxColumn chk;
        private DataGridViewTextBoxColumn LOGIN_ID;
        private DataGridViewTextBoxColumn LOGIN_NAME;
    }
}