﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageDeposit : Form
    {

        public TBL_DEPOSIT DEPOSIT
        {
            get
            {
                TBL_DEPOSIT objDeposit = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    try
                    {
                        int intDeposit = (int)dgv.SelectedRows[0].Cells[DEPOSIT_AMOUNT_ID.Name].Value;
                        objDeposit = DBDataContext.Db.TBL_DEPOSITs.FirstOrDefault(x => x.DEPOSIT_AMOUNT_ID == intDeposit);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objDeposit;
            }
        }

        #region Constructor
        public PageDeposit()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            this.Reload();
            UIHelper.SetDataSourceToComboBox(cboCurrency, DBDataContext.Db.TLKP_CURRENCies.Select(x => new { x.CURRENCY_ID, x.CURRENCY_NAME })._ToDataTable(), Resources.ALL_CURRENCY);
        }
        #endregion

        #region Operation
        /// <summary>
        /// Add new ampare.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNew_Click(object sender, EventArgs e)
        {
            DialogDeposit objDialog = new DialogDeposit(GeneralProcess.Insert, new TBL_DEPOSIT());
            if (objDialog.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, DEPOSIT_AMOUNT_ID.Name, objDialog.Deposit.DEPOSIT_AMOUNT_ID);
            }
        }

        /// <summary>
        /// Edit ampare
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                DialogDeposit dig = new DialogDeposit(GeneralProcess.Update, DEPOSIT);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, DEPOSIT_AMOUNT_ID.Name, dig.Deposit.DEPOSIT_AMOUNT_ID);
                }
            }
        }

        /// <summary>
        /// Remove ampare
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exButton1_Click(object sender, EventArgs e)
        {
            if (IsDeletable())
            {
                DialogDeposit dig = new DialogDeposit(GeneralProcess.Delete, DEPOSIT);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, DEPOSIT_AMOUNT_ID.Name, dig.Deposit.DEPOSIT_AMOUNT_ID - 1);
                }
            }
        }

        /// <summary>
        /// Load data from database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            try
            {
                Reload();
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        public void Reload()
        {
            int nCurrency = 0;
            if (cboCurrency.SelectedIndex != -1)
            {
                nCurrency = (int)cboCurrency.SelectedValue;
            }

            var q = (from d in DBDataContext.Db.TBL_DEPOSITs
                     join c in DBDataContext.Db.TLKP_CURRENCies on d.CURRENCY_ID equals c.CURRENCY_ID
                     join ct in DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs on d.CUSTOMER_TYPE_ID equals ct.CUSTOMER_CONNECTION_TYPE_ID
                     join g in DBDataContext.Db.TLKP_CUSTOMER_GROUPs on ct.NONLICENSE_CUSTOMER_GROUP_ID equals g.CUSTOMER_GROUP_ID
                     join a in DBDataContext.Db.TBL_AMPAREs on d.AMPARE_ID equals a.AMPARE_ID
                     join p in DBDataContext.Db.TBL_PHASEs on d.PHASE_ID equals p.PHASE_ID
                     where d.IS_ACTIVE && (ct.CUSTOMER_CONNECTION_TYPE_NAME + " " + a.AMPARE_NAME + " " + p.PHASE_NAME).ToLower().Contains(txtQuickSearch.Text.ToLower().Trim())
                     && (nCurrency == 0 || d.CURRENCY_ID == nCurrency)
                     select new
                     {
                         ct.CUSTOMER_CONNECTION_TYPE_ID,
                         CUSTOMER_TYPE_NAME = g.CUSTOMER_GROUP_ID == 1 || g.CUSTOMER_GROUP_ID == 2 || g.CUSTOMER_GROUP_ID == 3 ? g.CUSTOMER_GROUP_NAME + " (" + ct.CUSTOMER_CONNECTION_TYPE_NAME + ")" : ct.CUSTOMER_CONNECTION_TYPE_NAME,
                         p.PHASE_NAME,
                         a.AMPARE_NAME,
                         d.AMOUNT,
                         c.CURRENCY_SING,
                         d.DEPOSIT_AMOUNT_ID
                     }).ToArray();
            try
            {
                var dt = q
                    .OrderBy(x => x.CUSTOMER_CONNECTION_TYPE_ID)
                    .ThenBy(x => x.PHASE_NAME)
                    .ThenBy(x => Lookup.ParseAmpare(x.AMPARE_NAME))
                    .Select(x => x)
                    ._ToDataTable(); ;

                this.dgv.DataSource = dt;
            }
            catch (Exception ex)
            {

            }
        }
        private void cboCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            Reload();
        }

        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }
        #endregion

        #region Method
        private bool IsDeletable()
        {
            bool val = true;
            return val;
        }
        #endregion


    }
}