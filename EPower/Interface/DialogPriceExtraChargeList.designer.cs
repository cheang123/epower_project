﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogPriceExtraChargeList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogPriceExtraChargeList));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvPriceExtraCharge = new System.Windows.Forms.DataGridView();
            this.lblEXTRA_CHARGE = new System.Windows.Forms.Label();
            this.lblPriceName_ = new System.Windows.Forms.Label();
            this.btnREMOVE = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnADD = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnEDIT = new SoftTech.Component.ExLinkLabel(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.EXTRA_CHARGE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SERVICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NOTE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CHARGE_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPriceExtraCharge)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.lblPriceName_);
            this.content.Controls.Add(this.lblEXTRA_CHARGE);
            this.content.Controls.Add(this.dgvPriceExtraCharge);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.dgvPriceExtraCharge, 0);
            this.content.Controls.SetChildIndex(this.lblEXTRA_CHARGE, 0);
            this.content.Controls.SetChildIndex(this.lblPriceName_, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            // 
            // dgvPriceExtraCharge
            // 
            this.dgvPriceExtraCharge.AllowUserToAddRows = false;
            this.dgvPriceExtraCharge.AllowUserToDeleteRows = false;
            this.dgvPriceExtraCharge.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvPriceExtraCharge.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPriceExtraCharge.BackgroundColor = System.Drawing.Color.White;
            this.dgvPriceExtraCharge.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvPriceExtraCharge.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvPriceExtraCharge.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPriceExtraCharge.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EXTRA_CHARGE_ID,
            this.SERVICE,
            this.NOTE,
            this.PRICE,
            this.CHARGE_});
            this.dgvPriceExtraCharge.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgvPriceExtraCharge, "dgvPriceExtraCharge");
            this.dgvPriceExtraCharge.Name = "dgvPriceExtraCharge";
            this.dgvPriceExtraCharge.ReadOnly = true;
            this.dgvPriceExtraCharge.RowHeadersVisible = false;
            this.dgvPriceExtraCharge.RowTemplate.Height = 25;
            // 
            // lblEXTRA_CHARGE
            // 
            resources.ApplyResources(this.lblEXTRA_CHARGE, "lblEXTRA_CHARGE");
            this.lblEXTRA_CHARGE.Name = "lblEXTRA_CHARGE";
            // 
            // lblPriceName_
            // 
            resources.ApplyResources(this.lblPriceName_, "lblPriceName_");
            this.lblPriceName_.Name = "lblPriceName_";
            // 
            // btnREMOVE
            // 
            resources.ApplyResources(this.btnREMOVE, "btnREMOVE");
            this.btnREMOVE.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnREMOVE.Name = "btnREMOVE";
            this.btnREMOVE.TabStop = true;
            this.btnREMOVE.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkRemove_LinkClicked);
            // 
            // btnADD
            // 
            resources.ApplyResources(this.btnADD, "btnADD");
            this.btnADD.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnADD.Name = "btnADD";
            this.btnADD.TabStop = true;
            this.btnADD.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkAdd_LinkClicked);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnEDIT
            // 
            resources.ApplyResources(this.btnEDIT, "btnEDIT");
            this.btnEDIT.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnEDIT.Name = "btnEDIT";
            this.btnEDIT.TabStop = true;
            this.btnEDIT.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkEdit_LinkClicked);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnADD);
            this.panel1.Controls.Add(this.btnEDIT);
            this.panel1.Controls.Add(this.btnREMOVE);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // EXTRA_CHARGE_ID
            // 
            this.EXTRA_CHARGE_ID.DataPropertyName = "EXTRA_CHARGE_ID";
            resources.ApplyResources(this.EXTRA_CHARGE_ID, "EXTRA_CHARGE_ID");
            this.EXTRA_CHARGE_ID.Name = "EXTRA_CHARGE_ID";
            this.EXTRA_CHARGE_ID.ReadOnly = true;
            // 
            // SERVICE
            // 
            this.SERVICE.DataPropertyName = "INVOICE_ITEM_NAME";
            resources.ApplyResources(this.SERVICE, "SERVICE");
            this.SERVICE.Name = "SERVICE";
            this.SERVICE.ReadOnly = true;
            // 
            // NOTE
            // 
            this.NOTE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NOTE.DataPropertyName = "CHARGE_DESCRIPTION";
            resources.ApplyResources(this.NOTE, "NOTE");
            this.NOTE.Name = "NOTE";
            this.NOTE.ReadOnly = true;
            // 
            // PRICE
            // 
            this.PRICE.DataPropertyName = "CHARGE_VALUE";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "#,##0.####";
            dataGridViewCellStyle2.NullValue = null;
            this.PRICE.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.PRICE, "PRICE");
            this.PRICE.Name = "PRICE";
            this.PRICE.ReadOnly = true;
            // 
            // CHARGE_
            // 
            this.CHARGE_.DataPropertyName = "CHARGE";
            resources.ApplyResources(this.CHARGE_, "CHARGE_");
            this.CHARGE_.Name = "CHARGE_";
            this.CHARGE_.ReadOnly = true;
            // 
            // DialogPriceExtraChargeList
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogPriceExtraChargeList";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPriceExtraCharge)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Label lblPriceName_;
        private Label lblEXTRA_CHARGE;
        private DataGridView dgvPriceExtraCharge;
        private ExLinkLabel btnREMOVE;
        private ExLinkLabel btnADD;
        private ExButton btnCLOSE;
        private ExLinkLabel btnEDIT;
        private Panel panel1;
        private DataGridViewTextBoxColumn EXTRA_CHARGE_ID;
        private DataGridViewTextBoxColumn SERVICE;
        private DataGridViewTextBoxColumn NOTE;
        private DataGridViewTextBoxColumn PRICE;
        private DataGridViewTextBoxColumn CHARGE_;

    }
}