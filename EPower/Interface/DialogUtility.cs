﻿using EPower.Base.Logic;
using EPower.Logic;
using EPower.Logic.IRDevice;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing.Printing;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogUtility : ExDialog
    {
        #region Data
        IEnumerable<TBL_UTILITY> _ienuUsage;
        List<TBL_UTILITY> _ienuOldUsage = new List<TBL_UTILITY>();

        int _intPreTmpComPort = Settings.Default.PREPAID_COM_PORT;

        // obj sequence
        TBL_SEQUENCE_TYPE objSeqInvoice = DBDataContext.Db.TBL_SEQUENCE_TYPEs.FirstOrDefault(x => x.SEQUENCE_TYPE_ID == (int)SequenceType.Invoice);
        TBL_SEQUENCE_TYPE objSeqService = DBDataContext.Db.TBL_SEQUENCE_TYPEs.FirstOrDefault(x => x.SEQUENCE_TYPE_ID == (int)SequenceType.InvoiceService);
        TBL_SEQUENCE_TYPE objSeqPrepaid = DBDataContext.Db.TBL_SEQUENCE_TYPEs.FirstOrDefault(x => x.SEQUENCE_TYPE_ID == (int)SequenceType.Prepaid);
        #endregion

        #region Constructor
        public DialogUtility()
        {
            InitializeComponent();

            //cboPrepaidSequence.Visible = false;
            //label1.Visible = false;
            //lblPREPAID_SEQUENCE.Visible = false;

            cboInvoiceSequence.Enabled = false;
            cboServiceSequence.Enabled = false;

            _ienuUsage = from u in DBDataContext.Db.TBL_UTILITies
                         select u;
            //log record;
            foreach (TBL_UTILITY item in _ienuUsage)
            {
                TBL_UTILITY obj = new TBL_UTILITY();
                item._CopyTo(obj);
                _ienuOldUsage.Add(obj);
            }

            bind();

            read();

            this.lblDEVICE_BARCODE.Visible =
                this.btnTEST_3.Visible =
                this.cboMetroBaudRate.Visible =
               this.cboMetroCom.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_BARCODE_COLLECTION]);

            //this.lblPREPAID_SEQUENCE.Visible =
              //  this.label1.Visible =
                //this.cboPrepaidSequence.Visible = Settings.Default.IS_USE_PREPAID;
        }

        #endregion

        #region Operation
        /// <summary>
        /// Data transaction
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }

            write();

            try
            {

                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    //save to utility
                    TBL_CHANGE_LOG objChangeLog = null;
                    TBL_UTILITY objParent = _ienuUsage.FirstOrDefault();
                    foreach (TBL_UTILITY objNew in _ienuUsage)
                    {
                        TBL_UTILITY objOld = _ienuOldUsage.FirstOrDefault(x => x.UTILITY_ID == objNew.UTILITY_ID);
                        if (!objOld._Compare(objNew))
                        {
                            objOld.UTILITY_VALUE = objOld.DESCRIPTION + " - " + objOld.UTILITY_VALUE;
                        }
                        DBDataContext.Db.UpdateChild(objOld, objNew, objParent, ref objChangeLog);
                    }
                    Method.Utilities = null;

                    objSeqInvoice.SEQUENCE_ID = (int)cboInvoiceSequence.SelectedValue;
                    objSeqService.SEQUENCE_ID = (int)cboServiceSequence.SelectedValue;
                    //objSeqPrepaid.SEQUENCE_ID = (int)cboPrepaidSequence.SelectedValue;
                    DBDataContext.Db.SubmitChanges();
                    tran.Complete();

                    //save to config
                    Settings.Default.IR_COM_PORT = this.cboIRComPort.SelectedValue == null ? 0 : int.Parse(this.cboIRComPort.SelectedValue.ToString());
                    Settings.Default.IR_BOUND_RATE = this.cboIRBoundRate.SelectedValue == null ? 0 : int.Parse(this.cboIRBoundRate.Text);

                    Settings.Default.METROLOGIC_COM_PORT = this.cboMetroCom.SelectedValue == null ? 0 : int.Parse(this.cboMetroCom.SelectedValue.ToString());
                    Settings.Default.METROLOGIC_BAUDRATE = this.cboMetroBaudRate.SelectedValue == null ? 0 : int.Parse(this.cboMetroBaudRate.Text);

                    Settings.Default.PREPAID_COM_PORT = int.Parse(this.cboPrepaidComPort.SelectedValue.ToString());
                    Settings.Default.PRINTER_CASHDRAWER = this.cboPrinterCashDrawer.Text;
                    Settings.Default.PRINTER_INVOICE = this.cboPrinterInvoice.Text;
                    Settings.Default.PRINTER_RECIEPT = this.cboPrinterReciept.Text;
                    Settings.Default.Save();

                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp);
            }
        }


        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void txtKeyNumber(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputNumberOnly(sender, e);
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(_ienuUsage);
        }

        private void btnTestPrepaid_Click(object sender, EventArgs e)
        {
            Settings.Default.PREPAID_COM_PORT = int.Parse(this.cboPrepaidComPort.SelectedValue.ToString());
            int intOpen = ICHelper.TestPort();
            if (intOpen == 0)
            {
                MsgBox.ShowInformation(Resources.MS_IC_CARD_READER_CONNECTED);
            }
            else
            {
                MsgBox.ShowInformation(Resources.MS_CANNOT_OPEN_COM_PORT);
            }
        }

        private void btnTestIR_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            int intPortNumber = DataHelper.ParseToInt(cboIRComPort.SelectedValue.ToString());
            int intBoundRate = DataHelper.ParseToInt(cboIRBoundRate.SelectedValue.ToString());
            string strDeviceId = "", strError = "";
            int intResult = ThinPad.TestPort(ref strDeviceId, ref strError, intPortNumber, intBoundRate);
            if (intResult == 0)
            {
                MsgBox.ShowInformation(Resources.CONNECT_SUCCESS);
            }
            else
            {
                MsgBox.ShowInformation(strError);
            }
            Cursor.Current = Cursors.Default;
        }
        private void cboPrinterInvoice_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void chkDEPOSIT_IS_IGNORED_CheckedChanged(object sender, EventArgs e)
        {
            this.chkDEPOSIT_IS_PAID_AUTO.Enabled = !this.chkDEPOSIT_IS_IGNORED.Checked;
        }


        private void txtREF_DAYS_TO_EXECUTE_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void tabGENERAL_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && Control.ModifierKeys == Keys.Control)
            {
                lblREPORT_REF_SUBSIDY.Visible = lblDAY_TO_RUN_REF.Visible = txtDEFAULT_DAY_TO_RUN_REF.Visible = chkENABLE_ADJUST_BASED_PRICE_SUBSIDY.Visible = lblREPORT_REF_SUBSIDY.Visible == true ? false : true;
            }
            else if (e.Button == MouseButtons.Right && Control.ModifierKeys == Keys.Alt)
            {
                lblINVOICE.Visible = chkENABLE_ADJUST_OLD_INVOICE.Visible = chkENABLE_EDIT_PAYMENT_DATE.Visible = chkENABLE_EDIT_INVOICE_ADJUSTMENT.Visible = lblINVOICE.Visible == true ? false : true;
            }
        }
        #endregion

        #region Method

        private void bind()
        {
            DataTable dtComPort = new DataTable();
            dtComPort.Columns.Add("PORT", typeof(int));
            dtComPort.Columns.Add("PORT_NAME", typeof(string));

            DataTable dtBoundRate = new DataTable();
            dtBoundRate.Columns.Add("BOUND_RATE", typeof(int));

            int[] lstBoundRates = new int[4] { 9600, 38400, 57600, 115200 };

            //Assign value to com port
            for (int i = 1; i <= 4; i++)
            {
                DataRow drComPort = dtComPort.NewRow();
                drComPort["PORT"] = i;
                drComPort["PORT_NAME"] = "COM" + i.ToString();
                dtComPort.Rows.Add(drComPort);

                DataRow drBoundRate = dtBoundRate.NewRow();
                drBoundRate["BOUND_RATE"] = lstBoundRates[i - 1];
                dtBoundRate.Rows.Add(drBoundRate);
            }

            var dt = dtComPort.Copy();
            DataRow drUSB = dt.NewRow();
            drUSB["PORT"] = 0;
            drUSB["PORT_NAME"] = "USB";
            dt.Rows.InsertAt(drUSB, 0);



            UIHelper.SetDataSourceToComboBox(cboPrepaidComPort, dt, "PORT", "PORT_NAME");
            UIHelper.SetDataSourceToComboBox(cboIRComPort, dtComPort.Copy(), "PORT", "PORT_NAME");
            UIHelper.SetDataSourceToComboBox(cboMetroCom, dtComPort.Copy(), "PORT", "PORT_NAME");

            UIHelper.SetDataSourceToComboBox(cboIRBoundRate, dtBoundRate.Copy(), "BOUND_RATE", "BOUND_RATE");
            UIHelper.SetDataSourceToComboBox(cboMetroBaudRate, dtBoundRate.Copy(), "BOUND_RATE", "BOUND_RATE");

            UIHelper.SetDataSourceToComboBox(cboInvoiceSequence, DBDataContext.Db.TBL_SEQUENCEs.Where(x => x.IS_ACTIVE && x.SEQUENCE_ID > 0).Select(x => new { x.SEQUENCE_ID, x.SEQUENCE_NAME_KH }));
            UIHelper.SetDataSourceToComboBox(cboServiceSequence, DBDataContext.Db.TBL_SEQUENCEs.Where(x => x.IS_ACTIVE && x.SEQUENCE_ID > 0).Select(x => new { x.SEQUENCE_ID, x.SEQUENCE_NAME_KH }));
            //UIHelper.SetDataSourceToComboBox(cboPrepaidSequence, DBDataContext.Db.TBL_SEQUENCEs.Where(x => (x.IS_ACTIVE || x.SEQUENCE_ID == 2) && x.SEQUENCE_ID > 0).Select(x => new { x.SEQUENCE_ID, x.SEQUENCE_NAME_KH }));

            // PRINTER 
            this.cboPrinterInvoice.Items.Add("");
            this.cboPrinterReciept.Items.Add("");
            this.cboPrinterCashDrawer.Items.Add("");
            foreach (string printer in PrinterSettings.InstalledPrinters)
            {
                this.cboPrinterInvoice.Items.Add(printer);
                this.cboPrinterReciept.Items.Add(printer);
                this.cboPrinterCashDrawer.Items.Add(printer);
            }
        }

        /// <summary>
        /// Load data from object.
        /// </summary>
        private void read()
        {
            //Block and collect usage. 
            this.txtDEFAULT_CUT_OFF_DAY.Text = getUtilityValue(Utility.DEFAULT_CUT_OFF_DAY);
            this.txtINVOICE_DUE_DATE.Text = getUtilityValue(Utility.INVOICE_DUE_DATE);
            this.txtCUT_OFF_AMOUNT.Text = getUtilityValue(Utility.CUT_OFF_AMOUNT);
            this.txtMAX_DAY_TO_IGNORE_FIXED_AMOUNT.Text = getUtilityValue(Utility.MAX_DAY_TO_IGNORE_FIXED_AMOUNT);
            this.txtMAX_DAY_TO_IGNORE_BILLING.Text = getUtilityValue(Utility.MAX_DAY_TO_IGNORE_BILLING);

            // deposit.
            this.chkDEPOSIT_IS_IGNORED.Checked = DataHelper.ParseToBoolean(getUtilityValue(Utility.DEPOSIT_IS_IGNORED));
            this.chkDEPOSIT_IS_PAID_AUTO.Checked = DataHelper.ParseToBoolean(getUtilityValue(Utility.DEPOSIT_IS_PAID_AUTO));


            // COM PORT.
            this.cboIRComPort.SelectedValue = Settings.Default.IR_COM_PORT;
            this.cboIRBoundRate.SelectedValue = Settings.Default.IR_BOUND_RATE;
            this.cboMetroCom.SelectedValue = Settings.Default.METROLOGIC_COM_PORT;

            this.cboPrepaidComPort.SelectedValue = Settings.Default.PREPAID_COM_PORT;
            this.cboMetroBaudRate.SelectedValue = Settings.Default.METROLOGIC_BAUDRATE;

            this.txtAlarmQTY.Text = getUtilityValue(Utility.ALARM_QTY).ToString();

            // printer.
            this.cboPrinterInvoice.Text = Settings.Default.PRINTER_INVOICE;
            this.cboPrinterReciept.Text = Settings.Default.PRINTER_RECIEPT;
            this.cboPrinterCashDrawer.Text = Settings.Default.PRINTER_CASHDRAWER;

            // Ref Subsidy
            this.txtDEFAULT_DAY_TO_RUN_REF.Text = getUtilityValue(Utility.DEFAULT_DAY_TO_RUN_REF);
            this.chkENABLE_ADJUST_BASED_PRICE_SUBSIDY.Checked = DataHelper.ParseToBoolean(getUtilityValue(Utility.ENABLE_ADJUST_BASED_PRICE_SUBSIDY));

            // Invoice
            this.chkENABLE_ADJUST_OLD_INVOICE.Checked = DataHelper.ParseToBoolean(getUtilityValue(Utility.ENABLE_ADJUST_OLD_INVOICE));
            this.chkENABLE_EDIT_PAYMENT_DATE.Checked = DataHelper.ParseToBoolean(getUtilityValue(Utility.ENABLE_EDIT_PAYMENT_DATE));
            this.chkENABLE_EDIT_INVOICE_ADJUSTMENT.Checked = DataHelper.ParseToBoolean(getUtilityValue(Utility.ENABLE_EDIT_INVOICE_ADJUSTMENT_OVER3));

            // sequence
            this.cboInvoiceSequence.SelectedValue = objSeqInvoice.SEQUENCE_ID;
            this.cboServiceSequence.SelectedValue = objSeqService.SEQUENCE_ID;
            //this.cboPrepaidSequence.SelectedValue = objSeqPrepaid.SEQUENCE_ID;

            //E-Filling
            this.txtExportNumber.Text = getUtilityValue(Utility.E_FILLING_EXPORT_NUMBER);
            //prepayment
            //this.chkENABLE_PREPAYMENT_SERVICE.Checked = DataHelper.ParseToBoolean(getUtilityValue(Utility.ENABLE_PREPAYMENT_SERVICE));

            //Total customer revers bill
            this.txtTOTAL_CUSTOMER_REVERS.Text = getUtilityValue(Utility.MAXED_CUSTOMER_IN_CYCLE);

            //Backup brefore run bill and revers bill
            this.chkBACKUP_BEFORE_RUN_BILL.Checked = DataHelper.ParseToBoolean(getUtilityValue(Utility.BACKUP_BEFORE_RUN_BILL));
            this.chkBACKUP_BEFORE_REVERS_BILL.Checked = DataHelper.ParseToBoolean(getUtilityValue(Utility.BACKUP_BEFORE_REVERS_BILL));
        }


        private string getUtilityValue(Utility utility)
        {
            return _ienuUsage.FirstOrDefault(x => x.UTILITY_ID == (int)utility).UTILITY_VALUE;
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            this.write(Utility.CUT_OFF_AMOUNT, DataHelper.ParseToDecimal(this.txtCUT_OFF_AMOUNT.Text));
            this.write(Utility.INVOICE_DUE_DATE, DataHelper.ParseToInt(this.txtINVOICE_DUE_DATE.Text));
            this.write(Utility.DEPOSIT_IS_IGNORED, this.chkDEPOSIT_IS_IGNORED.Checked);
            this.write(Utility.DEPOSIT_IS_PAID_AUTO, this.chkDEPOSIT_IS_PAID_AUTO.Checked);
            this.write(Utility.ALARM_QTY, DataHelper.ParseToInt(this.txtAlarmQTY.Text));

            this.write(Utility.MAX_DAY_TO_IGNORE_FIXED_AMOUNT, DataHelper.ParseToInt(this.txtMAX_DAY_TO_IGNORE_FIXED_AMOUNT.Text));
            this.write(Utility.MAX_DAY_TO_IGNORE_BILLING, DataHelper.ParseToInt(this.txtMAX_DAY_TO_IGNORE_BILLING.Text));
            this.write(Utility.DEFAULT_DAY_TO_RUN_REF, DataHelper.ParseToInt(this.txtDEFAULT_DAY_TO_RUN_REF.Text));
            this.write(Utility.ENABLE_ADJUST_OLD_INVOICE, this.chkENABLE_ADJUST_OLD_INVOICE.Checked);
            this.write(Utility.ENABLE_EDIT_PAYMENT_DATE, this.chkENABLE_EDIT_PAYMENT_DATE.Checked);
            this.write(Utility.ENABLE_ADJUST_BASED_PRICE_SUBSIDY, this.chkENABLE_ADJUST_BASED_PRICE_SUBSIDY.Checked);
            this.write(Utility.ENABLE_EDIT_INVOICE_ADJUSTMENT_OVER3, this.chkENABLE_EDIT_INVOICE_ADJUSTMENT.Checked);
            this.write(Utility.E_FILLING_EXPORT_NUMBER, DataHelper.ParseToInt(this.txtExportNumber.Text));
            //this.write(Utility.ENABLE_PREPAYMENT_SERVICE, this.chkENABLE_PREPAYMENT_SERVICE.Checked);

            this.write(Utility.BACKUP_BEFORE_RUN_BILL, this.chkBACKUP_BEFORE_RUN_BILL.Checked);
            this.write(Utility.BACKUP_BEFORE_REVERS_BILL, this.chkBACKUP_BEFORE_REVERS_BILL.Checked);
            this.write(Utility.MAXED_CUSTOMER_IN_CYCLE, DataHelper.ParseToInt(this.txtTOTAL_CUSTOMER_REVERS.Text));
        }

        private void write(Utility u, object value)
        {
            _ienuUsage.FirstOrDefault(x => x.UTILITY_ID == (int)u)
                      .UTILITY_VALUE = value.ToString();
        }

        private TabPage selectedTab = null;

        private void setSelectTab(TabPage page)
        {
            if (selectedTab == null)
            {
                selectedTab = page;
            }
        }

        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();
            if (!DataHelper.IsInteger(this.txtINVOICE_DUE_DATE.Text))
            {
                setSelectTab(tabGENERAL);
                this.txtINVOICE_DUE_DATE.SetValidation(string.Format(Resources.REQUIRED_INPUT_NUMBER, this.lblINVOICE_DUE_DATE.Text));
                val = true;
            }
            if (!DataHelper.IsInteger(this.txtDEFAULT_CUT_OFF_DAY.Text))
            {
                setSelectTab(tabGENERAL);
                this.txtDEFAULT_CUT_OFF_DAY.SetValidation(string.Format(Resources.REQUIRED_INPUT_NUMBER, this.lblDEFAULT_CUT_OFF_DAY.Text));
                val = true;
            }
            if (!DataHelper.IsNumber(this.txtCUT_OFF_AMOUNT.Text))
            {
                setSelectTab(tabGENERAL);
                this.txtCUT_OFF_AMOUNT.SetValidation(string.Format(Resources.REQUIRED_INPUT_NUMBER, this.lblCUT_OFF_AMOUNT.Text));
                val = true;
            }
            if (!DataHelper.IsNumber(this.txtMAX_DAY_TO_IGNORE_FIXED_AMOUNT.Text))
            {
                setSelectTab(tabGENERAL);
                this.txtMAX_DAY_TO_IGNORE_FIXED_AMOUNT.SetValidation(string.Format(Resources.REQUIRED_INPUT_NUMBER, this.lblMAX_DAY_TO_IGNORE_FIXED_AMOUNT.Text));
                val = true;
            }
            if (!DataHelper.IsNumber(this.txtAlarmQTY.Text))
            {
                setSelectTab(tabDevice);
                this.txtAlarmQTY.SetValidation(string.Format(Resources.REQUIRED_INPUT_NUMBER, this.lblALARM_QTY.Text));
                val = true;
            }
            else if (DataHelper.ParseToInt(txtAlarmQTY.Text) <= 0)
            {
                setSelectTab(tabDevice);
                this.txtAlarmQTY.SetValidation(Resources.REQUEST_GRATER_THAN_ZERO);
                val = true;
            }
            if (!DataHelper.IsNumber(this.txtExportNumber.Text))
            {
                setSelectTab(tabSetting);
                this.txtExportNumber.SetValidation(string.Format(Properties.Resources.REQUIRED_INPUT_NUMBER, this.lblExportNumber.Text));
                val = true;
            }
            if (cboIRBoundRate.SelectedIndex == -1)
            {
                setSelectTab(tabDevice);
                this.cboIRBoundRate.SetValidation(string.Format(Resources.REQUIRED, lblDEVICE_COLLECT_USAGE.Text));
                val = true;
            }
            if (cboIRComPort.SelectedIndex == -1)
            {
                setSelectTab(tabDevice);
                this.cboIRComPort.SetValidation(string.Format(Resources.REQUIRED, lblDEVICE_COLLECT_USAGE.Text));
                val = true;
            }

            if (cboMetroBaudRate.Visible && cboMetroBaudRate.SelectedIndex == -1)
            {
                setSelectTab(tabDevice);
                this.cboMetroBaudRate.SetValidation(string.Format(Resources.REQUIRED, lblDEVICE_BARCODE.Text));
                val = true;
            }
            if (cboMetroCom.Visible && cboMetroCom.SelectedIndex == -1)
            {
                setSelectTab(tabDevice);
                this.cboMetroCom.SetValidation(string.Format(Resources.REQUIRED, lblDEVICE_BARCODE.Text));
                val = true;
            }

            if (cboPrepaidComPort.SelectedIndex == -1)
            {
                setSelectTab(tabDevice);
                this.cboPrepaidComPort.SetValidation(string.Format(Resources.REQUIRED, lblDEVICE_IC_CARD.Text));
                val = true;
            }
            if (!DataHelper.IsInteger(this.txtDEFAULT_DAY_TO_RUN_REF.Text))
            {
                setSelectTab(tabGENERAL);
                this.txtDEFAULT_DAY_TO_RUN_REF.SetValidation(string.Format(Resources.REQUIRED_INPUT_NUMBER, this.lblDAY_TO_RUN_REF.Text));
                val = true;
            }
            if (!DataHelper.IsNumber(this.txtTOTAL_CUSTOMER_REVERS.Text))
            {
                setSelectTab(tabSetting);
                this.txtTOTAL_CUSTOMER_REVERS.SetValidation(string.Format(Resources.REQUIRED_INPUT_NUMBER, this.lblTOTAL_INVOICE_REVERS.Text));
                val = true;
            }
            if (this.selectedTab != null)
            {
                this.tab.SelectedTab = this.selectedTab;
            }
            return val;
        }

        #endregion


    }
}