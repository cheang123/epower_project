﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogMeterType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogMeterType));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.lblVOLTAGE = new System.Windows.Forms.Label();
            this.lblPHASE = new System.Windows.Forms.Label();
            this.lblCONSTANT = new System.Windows.Forms.Label();
            this.txtMeterTypeCode = new System.Windows.Forms.TextBox();
            this.lblMETER_TYPE_CODE = new System.Windows.Forms.Label();
            this.txtMeterTypeName = new System.Windows.Forms.TextBox();
            this.lblMETER_TYPE_NAME = new System.Windows.Forms.Label();
            this.cboConstant = new System.Windows.Forms.ComboBox();
            this.cboVoltage = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.btnAddVoltage = new SoftTech.Component.ExAddItem();
            this.btnAddContant = new SoftTech.Component.ExAddItem();
            this.cboPhase = new System.Windows.Forms.ComboBox();
            this.cboAmpera = new System.Windows.Forms.ComboBox();
            this.lblAMPARE = new System.Windows.Forms.Label();
            this.btnAddPhase = new SoftTech.Component.ExAddItem();
            this.btnAddAmpare = new SoftTech.Component.ExAddItem();
            this.label14 = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.label14);
            this.content.Controls.Add(this.btnAddAmpare);
            this.content.Controls.Add(this.btnAddVoltage);
            this.content.Controls.Add(this.btnAddPhase);
            this.content.Controls.Add(this.btnAddContant);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.lblAMPARE);
            this.content.Controls.Add(this.label12);
            this.content.Controls.Add(this.label11);
            this.content.Controls.Add(this.label10);
            this.content.Controls.Add(this.label2);
            this.content.Controls.Add(this.label15);
            this.content.Controls.Add(this.cboAmpera);
            this.content.Controls.Add(this.cboVoltage);
            this.content.Controls.Add(this.cboPhase);
            this.content.Controls.Add(this.cboConstant);
            this.content.Controls.Add(this.lblVOLTAGE);
            this.content.Controls.Add(this.lblPHASE);
            this.content.Controls.Add(this.lblCONSTANT);
            this.content.Controls.Add(this.txtMeterTypeCode);
            this.content.Controls.Add(this.lblMETER_TYPE_CODE);
            this.content.Controls.Add(this.txtMeterTypeName);
            this.content.Controls.Add(this.lblMETER_TYPE_NAME);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_TYPE_NAME, 0);
            this.content.Controls.SetChildIndex(this.txtMeterTypeName, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_TYPE_CODE, 0);
            this.content.Controls.SetChildIndex(this.txtMeterTypeCode, 0);
            this.content.Controls.SetChildIndex(this.lblCONSTANT, 0);
            this.content.Controls.SetChildIndex(this.lblPHASE, 0);
            this.content.Controls.SetChildIndex(this.lblVOLTAGE, 0);
            this.content.Controls.SetChildIndex(this.cboConstant, 0);
            this.content.Controls.SetChildIndex(this.cboPhase, 0);
            this.content.Controls.SetChildIndex(this.cboVoltage, 0);
            this.content.Controls.SetChildIndex(this.cboAmpera, 0);
            this.content.Controls.SetChildIndex(this.label15, 0);
            this.content.Controls.SetChildIndex(this.label2, 0);
            this.content.Controls.SetChildIndex(this.label10, 0);
            this.content.Controls.SetChildIndex(this.label11, 0);
            this.content.Controls.SetChildIndex(this.label12, 0);
            this.content.Controls.SetChildIndex(this.lblAMPARE, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.btnAddContant, 0);
            this.content.Controls.SetChildIndex(this.btnAddPhase, 0);
            this.content.Controls.SetChildIndex(this.btnAddVoltage, 0);
            this.content.Controls.SetChildIndex(this.btnAddAmpare, 0);
            this.content.Controls.SetChildIndex(this.label14, 0);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lblVOLTAGE
            // 
            resources.ApplyResources(this.lblVOLTAGE, "lblVOLTAGE");
            this.lblVOLTAGE.Name = "lblVOLTAGE";
            // 
            // lblPHASE
            // 
            resources.ApplyResources(this.lblPHASE, "lblPHASE");
            this.lblPHASE.Name = "lblPHASE";
            // 
            // lblCONSTANT
            // 
            resources.ApplyResources(this.lblCONSTANT, "lblCONSTANT");
            this.lblCONSTANT.Name = "lblCONSTANT";
            // 
            // txtMeterTypeCode
            // 
            resources.ApplyResources(this.txtMeterTypeCode, "txtMeterTypeCode");
            this.txtMeterTypeCode.Name = "txtMeterTypeCode";
            this.txtMeterTypeCode.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // lblMETER_TYPE_CODE
            // 
            resources.ApplyResources(this.lblMETER_TYPE_CODE, "lblMETER_TYPE_CODE");
            this.lblMETER_TYPE_CODE.Name = "lblMETER_TYPE_CODE";
            // 
            // txtMeterTypeName
            // 
            resources.ApplyResources(this.txtMeterTypeName, "txtMeterTypeName");
            this.txtMeterTypeName.Name = "txtMeterTypeName";
            this.txtMeterTypeName.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // lblMETER_TYPE_NAME
            // 
            resources.ApplyResources(this.lblMETER_TYPE_NAME, "lblMETER_TYPE_NAME");
            this.lblMETER_TYPE_NAME.Name = "lblMETER_TYPE_NAME";
            // 
            // cboConstant
            // 
            this.cboConstant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboConstant.FormattingEnabled = true;
            resources.ApplyResources(this.cboConstant, "cboConstant");
            this.cboConstant.Name = "cboConstant";
            this.cboConstant.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // cboVoltage
            // 
            this.cboVoltage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboVoltage.FormattingEnabled = true;
            resources.ApplyResources(this.cboVoltage, "cboVoltage");
            this.cboVoltage.Name = "cboVoltage";
            this.cboVoltage.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Name = "label12";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Name = "label11";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Name = "label10";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Name = "label2";
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Name = "label15";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // btnAddVoltage
            // 
            this.btnAddVoltage.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnAddVoltage, "btnAddVoltage");
            this.btnAddVoltage.Name = "btnAddVoltage";
            this.btnAddVoltage.AddItem += new System.EventHandler(this.btnAddVoltage_AddItem);
            // 
            // btnAddContant
            // 
            this.btnAddContant.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnAddContant, "btnAddContant");
            this.btnAddContant.Name = "btnAddContant";
            this.btnAddContant.AddItem += new System.EventHandler(this.btnAddContant_AddItem);
            // 
            // cboPhase
            // 
            this.cboPhase.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPhase.FormattingEnabled = true;
            resources.ApplyResources(this.cboPhase, "cboPhase");
            this.cboPhase.Name = "cboPhase";
            this.cboPhase.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // cboAmpera
            // 
            this.cboAmpera.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAmpera.FormattingEnabled = true;
            resources.ApplyResources(this.cboAmpera, "cboAmpera");
            this.cboAmpera.Name = "cboAmpera";
            this.cboAmpera.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // lblAMPARE
            // 
            resources.ApplyResources(this.lblAMPARE, "lblAMPARE");
            this.lblAMPARE.Name = "lblAMPARE";
            // 
            // btnAddPhase
            // 
            this.btnAddPhase.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnAddPhase, "btnAddPhase");
            this.btnAddPhase.Name = "btnAddPhase";
            this.btnAddPhase.AddItem += new System.EventHandler(this.btnAddPhase_AddItem);
            // 
            // btnAddAmpare
            // 
            this.btnAddAmpare.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnAddAmpare, "btnAddAmpare");
            this.btnAddAmpare.Name = "btnAddAmpare";
            this.btnAddAmpare.AddItem += new System.EventHandler(this.btnAddAmpare_AddItem);
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Name = "label14";
            // 
            // DialogMeterType
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogMeterType";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label lblVOLTAGE;
        private Label lblPHASE;
        private Label lblCONSTANT;
        private TextBox txtMeterTypeCode;
        private Label lblMETER_TYPE_CODE;
        private TextBox txtMeterTypeName;
        private Label lblMETER_TYPE_NAME;
        private ComboBox cboVoltage;
        private ComboBox cboConstant;
        private Label label12;
        private Label label11;
        private Label label10;
        private Label label2;
        private Label label15;
        private ExButton btnCHANGE_LOG;
        private ExAddItem btnAddVoltage;
        private ExAddItem btnAddContant;
        private Label label14;
        private ExAddItem btnAddAmpare;
        private ExAddItem btnAddPhase;
        private Label lblAMPARE;
        private ComboBox cboAmpera;
        private ComboBox cboPhase;
    }
}