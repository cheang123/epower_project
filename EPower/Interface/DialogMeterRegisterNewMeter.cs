﻿using EPower.Base.Logic;
using EPower.CRMServiceReference;
using EPower.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogMeterRegisterNewMeter : ExDialog
    {
        DataTable dt = new DataTable();
        List<TBL_ACCOUNT_METER> lstMeters = new List<TBL_ACCOUNT_METER>();
        public DialogMeterRegisterNewMeter(List<TBL_ACCOUNT_METER> lst)
        {
            InitializeComponent();
            this.lstMeters = lst;
            UIHelper.SetDataSourceToComboBox(this.cboMeterType, DBDataContext.Db.TBL_METER_TYPEs.Where(x => x.IS_ACTIVE));
            this.dt = this.lstMeters.Select(x => new { METER_TYPE_NAME = "", x.METER_CODE, METER_ID = 0, METER_TYPE_ID = 0 })
                                    ._ToDataTable();
            this.dgv.DataSource = dt;
            this.bind();
        }

        private void bind()
        {
            this.lblCOUNT.Text = this.dgv.Rows.Count.ToString() + " កុងទ័រ";
        }

        #region Event 
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion Event 

        private void cboMeterType_SelectedIndexChanged(object sender, EventArgs e)
        {
            int meterTypeId = (int)this.cboMeterType.SelectedValue;
            foreach (DataGridViewRow row in this.dgv.SelectedRows)
            {
                row.Cells[this.METER_TYPE_ID.Name].Value = meterTypeId;
                row.Cells[this.METER_TYPE_NAME.Name].Value = this.cboMeterType.Text;
            }
            this.dgv.Focus();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            try
            {
                int meterNotSetType = this.dgv.SelectedRows.Cast<DataGridViewRow>().Count(x => (int)x.Cells[this.METER_TYPE_ID.Name].Value == 0);
                if (meterNotSetType > 0)
                {
                    MsgBox.ShowInformation(Resources.MS_PLEASE_CHANGE_METER_TYPE);
                    return;
                }

                List<TBL_METER> lstMeters = new List<TBL_METER>();
                // add new meter to local e-power system.
                Runner.Run(delegate ()
                {
                    foreach (DataGridViewRow row in this.dgv.SelectedRows)
                    {
                        Application.DoEvents();
                        string meterCode = Method.FormatMeterCode(row.Cells[this.METER_CODE.Name].Value.ToString());
                        var objMeter = DBDataContext.Db.TBL_METERs.FirstOrDefault(x => x.METER_CODE == meterCode);
                        if (objMeter == null)
                        {
                            objMeter = new TBL_METER()
                            {
                                METER_CODE = meterCode,
                                METER_TYPE_ID = (int)row.Cells[this.METER_TYPE_ID.Name].Value,
                                IS_DIGITAL = true,
                                CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                                CREATE_ON = DBDataContext.Db.GetSystemDate(),
                                MULTIPLIER = 1,
                                REGISTER_CODE = "",
                                STATUS_ID = 1
                            };
                            DBDataContext.Db.TBL_METERs.InsertOnSubmit(objMeter);
                            DBDataContext.Db.SubmitChanges();
                        }
                        lstMeters.Add(objMeter);
                    }
                });
                if (Runner.Instance.Error != null)
                {
                    return;
                }

                // process register new added meter to server
                if (CRMService.Intance.RegisterMeter(lstMeters))
                {
                    return;
                }

                // remove success meter from list
                Runner.Run(delegate ()
                {
                    foreach (DataGridViewRow row in this.dgv.SelectedRows)
                    {
                        Application.DoEvents();
                        if (DBDataContext.Db.TBL_METERs.Count(x => x.METER_CODE == Method.FormatMeterCode(row.Cells[this.METER_CODE.Name].Value.ToString())) > 0)
                        {
                            this.dgv.Rows.Remove(row);
                        }
                        this.bind();
                    }
                }, Resources.MS_FINALLIZING_PROGRESS);
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }
    }
}
