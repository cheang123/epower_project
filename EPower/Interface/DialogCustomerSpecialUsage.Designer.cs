﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogCustomerSpecialUsage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panelHeader = new System.Windows.Forms.Panel();
            this.chkMETER_TAMPER = new System.Windows.Forms.CheckBox();
            this.cboOperator = new System.Windows.Forms.ComboBox();
            this.btnReport = new SoftTech.Component.ExButton();
            this.dtpMonth = new System.Windows.Forms.DateTimePicker();
            this.txtUsageValue = new System.Windows.Forms.TextBox();
            this.chkUSAGE = new System.Windows.Forms.CheckBox();
            this.cboCycle = new System.Windows.Forms.ComboBox();
            this.chkIS_NEW_CYCLE = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.btnADJUST_USAGE_IV = new SoftTech.Component.ExButton();
            this.btnCANCEL = new SoftTech.Component.ExButton();
            this.CUSTOMER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.METER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMPARE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PHASE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AREA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BOX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.POLE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.START_USAGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.END_USAGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MULTIPLIER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_NEW_CYCLE = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.USAGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TAMPER_STATUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panelHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.btnCANCEL);
            this.content.Controls.Add(this.btnADJUST_USAGE_IV);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.dgv);
            this.content.Controls.Add(this.panelHeader);
            this.content.Size = new System.Drawing.Size(1072, 473);
            this.content.Text = "\'CONTENT\'";
            this.content.Controls.SetChildIndex(this.panelHeader, 0);
            this.content.Controls.SetChildIndex(this.dgv, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.btnADJUST_USAGE_IV, 0);
            this.content.Controls.SetChildIndex(this.btnCANCEL, 0);
            // 
            // panelHeader
            // 
            this.panelHeader.BackColor = System.Drawing.Color.Transparent;
            this.panelHeader.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panelHeader.Controls.Add(this.chkMETER_TAMPER);
            this.panelHeader.Controls.Add(this.cboOperator);
            this.panelHeader.Controls.Add(this.btnReport);
            this.panelHeader.Controls.Add(this.dtpMonth);
            this.panelHeader.Controls.Add(this.txtUsageValue);
            this.panelHeader.Controls.Add(this.chkUSAGE);
            this.panelHeader.Controls.Add(this.cboCycle);
            this.panelHeader.Controls.Add(this.chkIS_NEW_CYCLE);
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader.Location = new System.Drawing.Point(1, 0);
            this.panelHeader.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(1070, 34);
            this.panelHeader.TabIndex = 10;
            // 
            // chkMETER_TAMPER
            // 
            this.chkMETER_TAMPER.AutoSize = true;
            this.chkMETER_TAMPER.Location = new System.Drawing.Point(415, 6);
            this.chkMETER_TAMPER.Margin = new System.Windows.Forms.Padding(2);
            this.chkMETER_TAMPER.Name = "chkMETER_TAMPER";
            this.chkMETER_TAMPER.Size = new System.Drawing.Size(145, 23);
            this.chkMETER_TAMPER.TabIndex = 231;
            this.chkMETER_TAMPER.Text = "នាឡិកាស្ទង់3P មានបញ្ហា";
            this.chkMETER_TAMPER.UseVisualStyleBackColor = true;
            this.chkMETER_TAMPER.CheckedChanged += new System.EventHandler(this.chkMETER_TAMPER_CheckedChanged);
            // 
            // cboOperator
            // 
            this.cboOperator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboOperator.FormattingEnabled = true;
            this.cboOperator.Items.AddRange(new object[] {
            ">",
            "="});
            this.cboOperator.Location = new System.Drawing.Point(684, 3);
            this.cboOperator.Margin = new System.Windows.Forms.Padding(2);
            this.cboOperator.Name = "cboOperator";
            this.cboOperator.Size = new System.Drawing.Size(54, 27);
            this.cboOperator.TabIndex = 230;
            this.cboOperator.SelectedIndexChanged += new System.EventHandler(this.cboOperator_SelectedIndexChanged);
            // 
            // btnReport
            // 
            this.btnReport.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReport.Location = new System.Drawing.Point(967, 5);
            this.btnReport.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnReport.Name = "btnReport";
            this.btnReport.Size = new System.Drawing.Size(94, 23);
            this.btnReport.TabIndex = 229;
            this.btnReport.Text = "របាយការណ៍";
            this.btnReport.UseVisualStyleBackColor = true;
            this.btnReport.Click += new System.EventHandler(this.btnReport_Click);
            // 
            // dtpMonth
            // 
            this.dtpMonth.CustomFormat = "MM-yyyy";
            this.dtpMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMonth.Location = new System.Drawing.Point(147, 3);
            this.dtpMonth.Margin = new System.Windows.Forms.Padding(2);
            this.dtpMonth.Name = "dtpMonth";
            this.dtpMonth.Size = new System.Drawing.Size(128, 27);
            this.dtpMonth.TabIndex = 4;
            this.dtpMonth.ValueChanged += new System.EventHandler(this.dtpMonth_ValueChanged);
            // 
            // txtUsageValue
            // 
            this.txtUsageValue.Location = new System.Drawing.Point(742, 3);
            this.txtUsageValue.Margin = new System.Windows.Forms.Padding(2);
            this.txtUsageValue.Name = "txtUsageValue";
            this.txtUsageValue.Size = new System.Drawing.Size(55, 27);
            this.txtUsageValue.TabIndex = 3;
            this.txtUsageValue.Text = "500";
            this.txtUsageValue.TextChanged += new System.EventHandler(this.txtUsageValue_TextChanged);
            // 
            // chkUSAGE
            // 
            this.chkUSAGE.AutoSize = true;
            this.chkUSAGE.Checked = true;
            this.chkUSAGE.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkUSAGE.Location = new System.Drawing.Point(561, 5);
            this.chkUSAGE.Margin = new System.Windows.Forms.Padding(2);
            this.chkUSAGE.Name = "chkUSAGE";
            this.chkUSAGE.Size = new System.Drawing.Size(68, 23);
            this.chkUSAGE.TabIndex = 2;
            this.chkUSAGE.Text = "ថាមពល";
            this.chkUSAGE.UseVisualStyleBackColor = true;
            this.chkUSAGE.CheckedChanged += new System.EventHandler(this.chkShowGreaterThan_CheckedChanged);
            // 
            // cboCycle
            // 
            this.cboCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCycle.FormattingEnabled = true;
            this.cboCycle.Location = new System.Drawing.Point(4, 3);
            this.cboCycle.Margin = new System.Windows.Forms.Padding(2);
            this.cboCycle.Name = "cboCycle";
            this.cboCycle.Size = new System.Drawing.Size(139, 27);
            this.cboCycle.TabIndex = 1;
            this.cboCycle.SelectedIndexChanged += new System.EventHandler(this.cboCycle_SelectedIndexChanged);
            // 
            // chkIS_NEW_CYCLE
            // 
            this.chkIS_NEW_CYCLE.AutoSize = true;
            this.chkIS_NEW_CYCLE.Location = new System.Drawing.Point(279, 5);
            this.chkIS_NEW_CYCLE.Margin = new System.Windows.Forms.Padding(2);
            this.chkIS_NEW_CYCLE.Name = "chkIS_NEW_CYCLE";
            this.chkIS_NEW_CYCLE.Size = new System.Drawing.Size(136, 23);
            this.chkIS_NEW_CYCLE.TabIndex = 0;
            this.chkIS_NEW_CYCLE.Text = "នាឡិកាស្ទង់វិលដល់ជុំថ្មី";
            this.chkIS_NEW_CYCLE.UseVisualStyleBackColor = true;
            this.chkIS_NEW_CYCLE.CheckedChanged += new System.EventHandler(this.chkShowNewCycle_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Location = new System.Drawing.Point(2, 441);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1070, 1);
            this.panel1.TabIndex = 225;
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CUSTOMER_ID,
            this.CUSTOMER_CODE,
            this.CUSTOMER_NAME,
            this.METER_CODE,
            this.AMPARE,
            this.PHASE_ID,
            this.AREA,
            this.BOX,
            this.POLE,
            this.START_USAGE,
            this.END_USAGE,
            this.MULTIPLIER,
            this.IS_NEW_CYCLE,
            this.USAGE,
            this.TAMPER_STATUS});
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv.DefaultCellStyle = dataGridViewCellStyle12;
            this.dgv.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.Location = new System.Drawing.Point(1, 34);
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(1070, 407);
            this.dgv.TabIndex = 224;
            // 
            // btnADJUST_USAGE_IV
            // 
            this.btnADJUST_USAGE_IV.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnADJUST_USAGE_IV.Location = new System.Drawing.Point(899, 446);
            this.btnADJUST_USAGE_IV.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnADJUST_USAGE_IV.Name = "btnADJUST_USAGE_IV";
            this.btnADJUST_USAGE_IV.Size = new System.Drawing.Size(92, 23);
            this.btnADJUST_USAGE_IV.TabIndex = 226;
            this.btnADJUST_USAGE_IV.Text = "កែប្រែអំណាន";
            this.btnADJUST_USAGE_IV.UseVisualStyleBackColor = true;
            this.btnADJUST_USAGE_IV.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCANCEL
            // 
            this.btnCANCEL.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCANCEL.Location = new System.Drawing.Point(995, 446);
            this.btnCANCEL.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCANCEL.Name = "btnCANCEL";
            this.btnCANCEL.Size = new System.Drawing.Size(73, 23);
            this.btnCANCEL.TabIndex = 227;
            this.btnCANCEL.Text = "បិទ";
            this.btnCANCEL.UseVisualStyleBackColor = true;
            this.btnCANCEL.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // CUSTOMER_ID
            // 
            this.CUSTOMER_ID.DataPropertyName = "CUSTOMER_ID";
            this.CUSTOMER_ID.HeaderText = "CUSTOMER_ID";
            this.CUSTOMER_ID.Name = "CUSTOMER_ID";
            this.CUSTOMER_ID.ReadOnly = true;
            this.CUSTOMER_ID.Visible = false;
            // 
            // CUSTOMER_CODE
            // 
            this.CUSTOMER_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.CUSTOMER_CODE.DataPropertyName = "CUSTOMER_CODE";
            this.CUSTOMER_CODE.FillWeight = 21.29397F;
            this.CUSTOMER_CODE.HeaderText = "លេខអតិថិជន";
            this.CUSTOMER_CODE.Name = "CUSTOMER_CODE";
            this.CUSTOMER_CODE.ReadOnly = true;
            this.CUSTOMER_CODE.Width = 98;
            // 
            // CUSTOMER_NAME
            // 
            this.CUSTOMER_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.CUSTOMER_NAME.DataPropertyName = "CUSTOMER_NAME";
            this.CUSTOMER_NAME.FillWeight = 42.58793F;
            this.CUSTOMER_NAME.HeaderText = "ឈ្មោះអតិថិជន";
            this.CUSTOMER_NAME.Name = "CUSTOMER_NAME";
            this.CUSTOMER_NAME.ReadOnly = true;
            this.CUSTOMER_NAME.Width = 105;
            // 
            // METER_CODE
            // 
            this.METER_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.METER_CODE.DataPropertyName = "METER_CODE";
            this.METER_CODE.FillWeight = 42.58793F;
            this.METER_CODE.HeaderText = "លេខកុងទ័រ";
            this.METER_CODE.Name = "METER_CODE";
            this.METER_CODE.ReadOnly = true;
            this.METER_CODE.Width = 86;
            // 
            // AMPARE
            // 
            this.AMPARE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.AMPARE.DataPropertyName = "AMPARE_NAME";
            this.AMPARE.FillWeight = 42.58793F;
            this.AMPARE.HeaderText = "អាំងតង់ស៊ីតេ";
            this.AMPARE.Name = "AMPARE";
            this.AMPARE.ReadOnly = true;
            this.AMPARE.Width = 94;
            // 
            // PHASE_ID
            // 
            this.PHASE_ID.DataPropertyName = "PHASE_ID";
            this.PHASE_ID.HeaderText = "PHASE_ID";
            this.PHASE_ID.Name = "PHASE_ID";
            this.PHASE_ID.ReadOnly = true;
            this.PHASE_ID.Visible = false;
            // 
            // AREA
            // 
            this.AREA.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.AREA.DataPropertyName = "AREA_NAME";
            this.AREA.FillWeight = 42.58793F;
            this.AREA.HeaderText = "តំបន់";
            this.AREA.Name = "AREA";
            this.AREA.ReadOnly = true;
            this.AREA.Width = 58;
            // 
            // BOX
            // 
            this.BOX.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.BOX.DataPropertyName = "BOX_CODE";
            this.BOX.FillWeight = 42.58793F;
            this.BOX.HeaderText = "ប្រអប់";
            this.BOX.Name = "BOX";
            this.BOX.ReadOnly = true;
            this.BOX.Width = 62;
            // 
            // POLE
            // 
            this.POLE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.POLE.DataPropertyName = "POLE_CODE";
            this.POLE.HeaderText = "POLE_CODE";
            this.POLE.Name = "POLE";
            this.POLE.ReadOnly = true;
            this.POLE.Visible = false;
            this.POLE.Width = 99;
            // 
            // START_USAGE
            // 
            this.START_USAGE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.START_USAGE.DataPropertyName = "START_USAGE";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "N0";
            this.START_USAGE.DefaultCellStyle = dataGridViewCellStyle8;
            this.START_USAGE.FillWeight = 42.58793F;
            this.START_USAGE.HeaderText = "អំណានចាស់";
            this.START_USAGE.Name = "START_USAGE";
            this.START_USAGE.ReadOnly = true;
            this.START_USAGE.Width = 95;
            // 
            // END_USAGE
            // 
            this.END_USAGE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.END_USAGE.DataPropertyName = "END_USAGE";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.Format = "N0";
            this.END_USAGE.DefaultCellStyle = dataGridViewCellStyle9;
            this.END_USAGE.FillWeight = 42.58793F;
            this.END_USAGE.HeaderText = "អំណានថ្មី";
            this.END_USAGE.Name = "END_USAGE";
            this.END_USAGE.ReadOnly = true;
            this.END_USAGE.Width = 79;
            // 
            // MULTIPLIER
            // 
            this.MULTIPLIER.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.MULTIPLIER.DataPropertyName = "MULTIPLIER";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.MULTIPLIER.DefaultCellStyle = dataGridViewCellStyle10;
            this.MULTIPLIER.FillWeight = 240.8661F;
            this.MULTIPLIER.HeaderText = "មេគុណ";
            this.MULTIPLIER.Name = "MULTIPLIER";
            this.MULTIPLIER.ReadOnly = true;
            this.MULTIPLIER.Width = 71;
            // 
            // IS_NEW_CYCLE
            // 
            this.IS_NEW_CYCLE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.IS_NEW_CYCLE.DataPropertyName = "IS_METER_RENEW_CYCLE";
            this.IS_NEW_CYCLE.FillWeight = 42.58793F;
            this.IS_NEW_CYCLE.HeaderText = "កុងទ័រវិលដល់ជុំថ្មី";
            this.IS_NEW_CYCLE.Name = "IS_NEW_CYCLE";
            this.IS_NEW_CYCLE.ReadOnly = true;
            this.IS_NEW_CYCLE.Width = 95;
            // 
            // USAGE
            // 
            this.USAGE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.USAGE.DataPropertyName = "TOTAL_USAGE";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle11.Format = "N0";
            this.USAGE.DefaultCellStyle = dataGridViewCellStyle11;
            this.USAGE.FillWeight = 29.81156F;
            this.USAGE.HeaderText = "ថាមពលប្រើប្រាស់";
            this.USAGE.Name = "USAGE";
            this.USAGE.ReadOnly = true;
            this.USAGE.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.USAGE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.USAGE.Width = 99;
            // 
            // TAMPER_STATUS
            // 
            this.TAMPER_STATUS.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.TAMPER_STATUS.DataPropertyName = "TAMPER_STATUS";
            this.TAMPER_STATUS.HeaderText = "TAMPER_STATUS";
            this.TAMPER_STATUS.Name = "TAMPER_STATUS";
            this.TAMPER_STATUS.ReadOnly = true;
            this.TAMPER_STATUS.Width = 128;
            // 
            // DialogCustomerSpecialUsage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1072, 496);
            this.Name = "DialogCustomerSpecialUsage";
            this.Text = "អំណានត្រូវពិនិត្យ";
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panelHeader.ResumeLayout(false);
            this.panelHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panelHeader;
        private Panel panel1;
        private DataGridView dgv;
        private TextBox txtUsageValue;
        private CheckBox chkUSAGE;
        private CheckBox chkIS_NEW_CYCLE;
        private DateTimePicker dtpMonth;
        private ExButton btnCANCEL;
        private ExButton btnADJUST_USAGE_IV;
        private ExButton btnReport;
        public ComboBox cboCycle;
        public ComboBox cboOperator;
        private CheckBox chkMETER_TAMPER;
        private DataGridViewTextBoxColumn CUSTOMER_ID;
        private DataGridViewTextBoxColumn CUSTOMER_CODE;
        private DataGridViewTextBoxColumn CUSTOMER_NAME;
        private DataGridViewTextBoxColumn METER_CODE;
        private DataGridViewTextBoxColumn AMPARE;
        private DataGridViewTextBoxColumn PHASE_ID;
        private DataGridViewTextBoxColumn AREA;
        private DataGridViewTextBoxColumn BOX;
        private DataGridViewTextBoxColumn POLE;
        private DataGridViewTextBoxColumn START_USAGE;
        private DataGridViewTextBoxColumn END_USAGE;
        private DataGridViewTextBoxColumn MULTIPLIER;
        private DataGridViewCheckBoxColumn IS_NEW_CYCLE;
        private DataGridViewTextBoxColumn USAGE;
        private DataGridViewTextBoxColumn TAMPER_STATUS;
    }
}