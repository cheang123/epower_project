﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogAdjustment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogAdjustment));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.lblSTART_DATE = new System.Windows.Forms.Label();
            this.dtpStartUsage = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpEndUsage = new System.Windows.Forms.DateTimePicker();
            this.lblSTART_USAGE = new System.Windows.Forms.Label();
            this.lblEND_USAGE = new System.Windows.Forms.Label();
            this.txtStartUsage = new System.Windows.Forms.TextBox();
            this.txtEndUsage = new System.Windows.Forms.TextBox();
            this.lblMETER_CODE = new System.Windows.Forms.Label();
            this.txtTotalUsage = new System.Windows.Forms.TextBox();
            this.lblUSAGE = new System.Windows.Forms.Label();
            this.cboMeterCode = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.lblNOTE = new System.Windows.Forms.Label();
            this.txtTotalAmountNew = new System.Windows.Forms.TextBox();
            this.lblSETTLE_AMOUNT = new System.Windows.Forms.Label();
            this.lblMULTIPLIER = new System.Windows.Forms.Label();
            this.txtTotalAmount = new System.Windows.Forms.TextBox();
            this.lblAMOUNT_BEFOR_ADJUST = new System.Windows.Forms.Label();
            this.txtMultiplier = new System.Windows.Forms.TextBox();
            this.chkIS_NEW_CYCLE = new System.Windows.Forms.CheckBox();
            this.lblPRICE = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.lblSUBSIDY_TARIFF = new System.Windows.Forms.Label();
            this.txtBASED_PRICE = new System.Windows.Forms.TextBox();
            this.lblCURRENCY = new System.Windows.Forms.Label();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblStar_ = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.lblStar_);
            this.content.Controls.Add(this.label2);
            this.content.Controls.Add(this.cboCurrency);
            this.content.Controls.Add(this.lblCURRENCY);
            this.content.Controls.Add(this.txtBASED_PRICE);
            this.content.Controls.Add(this.lblSUBSIDY_TARIFF);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.chkIS_NEW_CYCLE);
            this.content.Controls.Add(this.lblAMOUNT_BEFOR_ADJUST);
            this.content.Controls.Add(this.txtTotalAmount);
            this.content.Controls.Add(this.txtMultiplier);
            this.content.Controls.Add(this.lblMULTIPLIER);
            this.content.Controls.Add(this.lblNOTE);
            this.content.Controls.Add(this.txtPrice);
            this.content.Controls.Add(this.lblPRICE);
            this.content.Controls.Add(this.txtTotalAmountNew);
            this.content.Controls.Add(this.lblSETTLE_AMOUNT);
            this.content.Controls.Add(this.cboMeterCode);
            this.content.Controls.Add(this.txtTotalUsage);
            this.content.Controls.Add(this.lblUSAGE);
            this.content.Controls.Add(this.lblMETER_CODE);
            this.content.Controls.Add(this.txtEndUsage);
            this.content.Controls.Add(this.txtStartUsage);
            this.content.Controls.Add(this.lblEND_USAGE);
            this.content.Controls.Add(this.lblSTART_USAGE);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.dtpEndUsage);
            this.content.Controls.Add(this.lblSTART_DATE);
            this.content.Controls.Add(this.dtpStartUsage);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.txtDescription);
            this.content.Controls.Add(this.label13);
            this.content.Controls.Add(this.label22);
            this.content.Controls.Add(this.label14);
            this.content.Controls.Add(this.label12);
            this.content.Controls.Add(this.label3);
            resources.ApplyResources(this.content, "content");
            this.content.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.content_MouseDoubleClick);
            this.content.Controls.SetChildIndex(this.label3, 0);
            this.content.Controls.SetChildIndex(this.label12, 0);
            this.content.Controls.SetChildIndex(this.label14, 0);
            this.content.Controls.SetChildIndex(this.label22, 0);
            this.content.Controls.SetChildIndex(this.label13, 0);
            this.content.Controls.SetChildIndex(this.txtDescription, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.dtpStartUsage, 0);
            this.content.Controls.SetChildIndex(this.lblSTART_DATE, 0);
            this.content.Controls.SetChildIndex(this.dtpEndUsage, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.lblSTART_USAGE, 0);
            this.content.Controls.SetChildIndex(this.lblEND_USAGE, 0);
            this.content.Controls.SetChildIndex(this.txtStartUsage, 0);
            this.content.Controls.SetChildIndex(this.txtEndUsage, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_CODE, 0);
            this.content.Controls.SetChildIndex(this.lblUSAGE, 0);
            this.content.Controls.SetChildIndex(this.txtTotalUsage, 0);
            this.content.Controls.SetChildIndex(this.cboMeterCode, 0);
            this.content.Controls.SetChildIndex(this.lblSETTLE_AMOUNT, 0);
            this.content.Controls.SetChildIndex(this.txtTotalAmountNew, 0);
            this.content.Controls.SetChildIndex(this.lblPRICE, 0);
            this.content.Controls.SetChildIndex(this.txtPrice, 0);
            this.content.Controls.SetChildIndex(this.lblNOTE, 0);
            this.content.Controls.SetChildIndex(this.lblMULTIPLIER, 0);
            this.content.Controls.SetChildIndex(this.txtMultiplier, 0);
            this.content.Controls.SetChildIndex(this.txtTotalAmount, 0);
            this.content.Controls.SetChildIndex(this.lblAMOUNT_BEFOR_ADJUST, 0);
            this.content.Controls.SetChildIndex(this.chkIS_NEW_CYCLE, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.lblSUBSIDY_TARIFF, 0);
            this.content.Controls.SetChildIndex(this.txtBASED_PRICE, 0);
            this.content.Controls.SetChildIndex(this.lblCURRENCY, 0);
            this.content.Controls.SetChildIndex(this.cboCurrency, 0);
            this.content.Controls.SetChildIndex(this.label2, 0);
            this.content.Controls.SetChildIndex(this.lblStar_, 0);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblSTART_DATE
            // 
            resources.ApplyResources(this.lblSTART_DATE, "lblSTART_DATE");
            this.lblSTART_DATE.Name = "lblSTART_DATE";
            // 
            // dtpStartUsage
            // 
            resources.ApplyResources(this.dtpStartUsage, "dtpStartUsage");
            this.dtpStartUsage.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartUsage.Name = "dtpStartUsage";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // dtpEndUsage
            // 
            resources.ApplyResources(this.dtpEndUsage, "dtpEndUsage");
            this.dtpEndUsage.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndUsage.Name = "dtpEndUsage";
            // 
            // lblSTART_USAGE
            // 
            resources.ApplyResources(this.lblSTART_USAGE, "lblSTART_USAGE");
            this.lblSTART_USAGE.Name = "lblSTART_USAGE";
            // 
            // lblEND_USAGE
            // 
            resources.ApplyResources(this.lblEND_USAGE, "lblEND_USAGE");
            this.lblEND_USAGE.Name = "lblEND_USAGE";
            // 
            // txtStartUsage
            // 
            resources.ApplyResources(this.txtStartUsage, "txtStartUsage");
            this.txtStartUsage.Name = "txtStartUsage";
            this.txtStartUsage.TextChanged += new System.EventHandler(this.txtOldUsage_TextChanged);
            // 
            // txtEndUsage
            // 
            resources.ApplyResources(this.txtEndUsage, "txtEndUsage");
            this.txtEndUsage.Name = "txtEndUsage";
            this.txtEndUsage.TextChanged += new System.EventHandler(this.txtNewUsage_TextChanged);
            this.txtEndUsage.Enter += new System.EventHandler(this.EnglishKey);
            // 
            // lblMETER_CODE
            // 
            resources.ApplyResources(this.lblMETER_CODE, "lblMETER_CODE");
            this.lblMETER_CODE.Name = "lblMETER_CODE";
            // 
            // txtTotalUsage
            // 
            resources.ApplyResources(this.txtTotalUsage, "txtTotalUsage");
            this.txtTotalUsage.Name = "txtTotalUsage";
            this.txtTotalUsage.ReadOnly = true;
            // 
            // lblUSAGE
            // 
            resources.ApplyResources(this.lblUSAGE, "lblUSAGE");
            this.lblUSAGE.Name = "lblUSAGE";
            // 
            // cboMeterCode
            // 
            this.cboMeterCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMeterCode.FormattingEnabled = true;
            resources.ApplyResources(this.cboMeterCode, "cboMeterCode");
            this.cboMeterCode.Name = "cboMeterCode";
            this.cboMeterCode.SelectedIndexChanged += new System.EventHandler(this.cboMeterCode_SelectedIndexChanged);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Name = "label3";
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Name = "label12";
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Name = "label13";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Name = "label14";
            // 
            // txtDescription
            // 
            resources.ApplyResources(this.txtDescription, "txtDescription");
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Enter += new System.EventHandler(this.txtDescription_Enter);
            // 
            // lblNOTE
            // 
            resources.ApplyResources(this.lblNOTE, "lblNOTE");
            this.lblNOTE.Name = "lblNOTE";
            // 
            // txtTotalAmountNew
            // 
            resources.ApplyResources(this.txtTotalAmountNew, "txtTotalAmountNew");
            this.txtTotalAmountNew.Name = "txtTotalAmountNew";
            this.txtTotalAmountNew.TextChanged += new System.EventHandler(this.txtTotalAmountNew_TextChanged);
            this.txtTotalAmountNew.Enter += new System.EventHandler(this.EnglishKey);
            this.txtTotalAmountNew.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTotalAmountNew_KeyPress);
            // 
            // lblSETTLE_AMOUNT
            // 
            resources.ApplyResources(this.lblSETTLE_AMOUNT, "lblSETTLE_AMOUNT");
            this.lblSETTLE_AMOUNT.Name = "lblSETTLE_AMOUNT";
            // 
            // lblMULTIPLIER
            // 
            resources.ApplyResources(this.lblMULTIPLIER, "lblMULTIPLIER");
            this.lblMULTIPLIER.Name = "lblMULTIPLIER";
            // 
            // txtTotalAmount
            // 
            resources.ApplyResources(this.txtTotalAmount, "txtTotalAmount");
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.ReadOnly = true;
            // 
            // lblAMOUNT_BEFOR_ADJUST
            // 
            resources.ApplyResources(this.lblAMOUNT_BEFOR_ADJUST, "lblAMOUNT_BEFOR_ADJUST");
            this.lblAMOUNT_BEFOR_ADJUST.Name = "lblAMOUNT_BEFOR_ADJUST";
            // 
            // txtMultiplier
            // 
            resources.ApplyResources(this.txtMultiplier, "txtMultiplier");
            this.txtMultiplier.Name = "txtMultiplier";
            // 
            // chkIS_NEW_CYCLE
            // 
            resources.ApplyResources(this.chkIS_NEW_CYCLE, "chkIS_NEW_CYCLE");
            this.chkIS_NEW_CYCLE.Name = "chkIS_NEW_CYCLE";
            this.chkIS_NEW_CYCLE.UseVisualStyleBackColor = true;
            this.chkIS_NEW_CYCLE.CheckedChanged += new System.EventHandler(this.chkMETER_IS_RENEW_CYCLE_CheckedChanged);
            // 
            // lblPRICE
            // 
            resources.ApplyResources(this.lblPRICE, "lblPRICE");
            this.lblPRICE.Name = "lblPRICE";
            // 
            // txtPrice
            // 
            resources.ApplyResources(this.txtPrice, "txtPrice");
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.TextChanged += new System.EventHandler(this.txtPrice_TextChanged);
            this.txtPrice.Enter += new System.EventHandler(this.EnglishKey);
            this.txtPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTotalAmountNew_KeyPress);
            // 
            // label22
            // 
            resources.ApplyResources(this.label22, "label22");
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Name = "label22";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnCHANGE_LOG_Click);
            // 
            // lblSUBSIDY_TARIFF
            // 
            resources.ApplyResources(this.lblSUBSIDY_TARIFF, "lblSUBSIDY_TARIFF");
            this.lblSUBSIDY_TARIFF.Name = "lblSUBSIDY_TARIFF";
            // 
            // txtBASED_PRICE
            // 
            resources.ApplyResources(this.txtBASED_PRICE, "txtBASED_PRICE");
            this.txtBASED_PRICE.Name = "txtBASED_PRICE";
            this.txtBASED_PRICE.Enter += new System.EventHandler(this.txtBASED_PRICE_Enter);
            // 
            // lblCURRENCY
            // 
            resources.ApplyResources(this.lblCURRENCY, "lblCURRENCY");
            this.lblCURRENCY.Name = "lblCURRENCY";
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // lblStar_
            // 
            resources.ApplyResources(this.lblStar_, "lblStar_");
            this.lblStar_.ForeColor = System.Drawing.Color.Red;
            this.lblStar_.Name = "lblStar_";
            // 
            // DialogAdjustment
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogAdjustment";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label label1;
        private DateTimePicker dtpEndUsage;
        private Label lblSTART_DATE;
        private DateTimePicker dtpStartUsage;
        private Label lblMETER_CODE;
        private Label lblEND_USAGE;
        private Label lblSTART_USAGE;
        private Label lblUSAGE;
        private Label label3;
        private Label label12;
        private Label label13;
        private Label label14;
        private TextBox txtDescription;
        private Label lblNOTE;
        private Label lblSETTLE_AMOUNT;
        private Label lblAMOUNT_BEFOR_ADJUST;
        private Label lblMULTIPLIER;
        private Label label22;
        private Label lblPRICE;
        private ExButton btnCHANGE_LOG;
        public TextBox txtEndUsage;
        public TextBox txtStartUsage;
        public TextBox txtTotalAmountNew;
        public TextBox txtMultiplier;
        public CheckBox chkIS_NEW_CYCLE;
        public TextBox txtPrice;
        public TextBox txtBASED_PRICE;
        public ComboBox cboCurrency;
        private TextBox txtTotalUsage;
        private TextBox txtTotalAmount;
        public Label lblSUBSIDY_TARIFF;
        public Label lblCURRENCY;
        public Label label2;
        public Label lblStar_;
        public ComboBox cboMeterCode;
    }
}
