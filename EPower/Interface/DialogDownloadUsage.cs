﻿using EPower.Logic;
using SoftTech;
using SoftTech.Component;
using System;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogDownloadUsage : ExDialog
    {
        public DialogDownloadUsage()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            int nCollect = 0;
            Runner.RunNewThread(delegate ()
            {
                nCollect = ServiceUsage.Intance.DownloadUsage();
            });

            txtHaveUsageCustomer.Text = nCollect.ToString("N0");
            txtUnknownMeter.Text = DBDataContext.Db.TBL_METER_UNKNOWNs.Count().ToString("N0");
            txtUnRegister.Text = DBDataContext.Db.TBL_METERs.Where(x => x.REGISTER_CODE == "").Count().ToString("N0");
        }


        private void btnCheckUnknown_Click(object sender, EventArgs e)
        {
            DialogMeterUnknown objDialog = new DialogMeterUnknown();
            objDialog.ShowDialog();
        }

        private void btnCheckUnregister_Click(object sender, EventArgs e)
        {
            DialogMeterUnregister objDialog = new DialogMeterUnregister();
            objDialog.ShowDialog();
        }

        private void btnViewNotCollectCustomer_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var cycles = DBDataContext.Db.TBL_BILLING_CYCLEs.Where(x => x.IS_ACTIVE);
            int cycleId = cycles.Count() > 1 ? 0 : cycles.FirstOrDefault().CYCLE_ID;
            var diag = new DialogCustomerNoUsage(cycleId);
            diag.ShowDialog();
        }


    }
}
