﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using Login = SoftTech.Security.Logic.Login;

namespace EPower.Interface
{
    public partial class DialogCustomerChangeBox : ExDialog
    {
        TBL_CUSTOMER _objCustomer = new TBL_CUSTOMER();
        public TBL_CUSTOMER Customer
        {
            get { return _objCustomer; }
            set { _objCustomer = value; }
        }

        TBL_CUSTOMER_METER _objCustomerMeterOld = new TBL_CUSTOMER_METER();
        TBL_CUSTOMER_METER _objCustomerMeterNew = new TBL_CUSTOMER_METER();
        TBL_BOX _objBoxOld = null;
        TBL_BOX _objBoxNew = null;
        TBL_POLE _objPoleOld = new TBL_POLE();
        TBL_POLE _objPoleNew = new TBL_POLE();

        public DialogCustomerChangeBox(TBL_CUSTOMER objCustomer)
        {
            InitializeComponent();
            //permission
            this.dtpCHANGE_DATE.Enabled = SoftTech.Security.Logic.Login.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_REGISTER_UPDATE_CHANGEBOX_DATE);

            // module "POSITION_IN_BOX_ENABLE"
            lblPOSITION_IN_BOX.Visible =
                txtOLDPositionInBox.Visible =
                lblPOSITION_IN_BOX_1.Visible =
                txtNEWPositionInBox.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.POSITION_IN_BOX_ENABLE]);

            this._objCustomer = objCustomer;
            this.read();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void read()
        {
            this.txtCUSTOMER_CODE.Text = this._objCustomer.CUSTOMER_CODE;
            this.txtCUSTOMER_NAME.Text = this._objCustomer.LAST_NAME_KH + " " + this._objCustomer.FIRST_NAME_KH;

            // read customer meter.
            this._objCustomerMeterOld = DBDataContext.Db.TBL_CUSTOMER_METERs.FirstOrDefault(r => r.CUSTOMER_ID == this._objCustomer.CUSTOMER_ID && r.IS_ACTIVE);
            if (this._objCustomerMeterOld != null)
            {
                this._objBoxOld = DBDataContext.Db.TBL_BOXes.FirstOrDefault(m => m.BOX_ID == this._objCustomerMeterOld.BOX_ID);
                this.txtOLD_BOX.Text = this._objBoxOld.BOX_CODE;
                this.txtOLD_BOX.AcceptSearch();
            }
        }

        private bool invalid()
        {
            bool result = false;
            this.ClearAllValidation();

            if (this._objBoxNew == null)
            {
                txtNEW_BOX.SetValidation(string.Format(Resources.REQUIRED, this.lblBOX_1.Text));
                result = true;
            }
            //if (txtNEWPositionInBox.Visible && !DataHelper.IsInteger(txtNEWPositionInBox.Text))
            //{
            //    txtNEWPositionInBox.SetValidation(string.Format(Properties.Resources.RequestInputNumber, this.lblNEWPositionInBox.Text));
            //    result = true;
            //}
            return result;
        }


        private void txtMeter_AdvanceSearch(object sender, EventArgs e)
        {
            if (txtOLD_BOX.Text.Trim() == "")
            {
                txtOLD_BOX.CancelSearch();
                return;
            }

            string strBox = txtOLD_BOX.Text;
            TBL_BOX tmp = DBDataContext.Db.TBL_BOXes.FirstOrDefault(b => b.BOX_CODE == strBox);

            if (tmp == null)
            {
                MsgBox.ShowInformation(Resources.MS_BOX_NOT_FOUND);
                this.txtOLD_BOX.CancelSearch();
                return;
            }

            if (tmp.STATUS_ID == (int)BoxStatus.Unavailable)
            {
                MsgBox.ShowInformation(Resources.MS_BOX_IS_UNAVAILABLE);
                this.txtOLD_BOX.CancelSearch();
                return;
            }

            _objBoxOld = new TBL_BOX();
            tmp._CopyTo(_objBoxOld);

            _objPoleOld = DBDataContext.Db.TBL_POLEs.FirstOrDefault(p => p.POLE_ID == _objBoxOld.POLE_ID);
            txtOLD_BOX_POLE.Text = _objPoleOld.POLE_CODE;
            txtOLD_BOX_COLLECTOR.Text = DBDataContext.Db.TBL_EMPLOYEEs.FirstOrDefault(emp => emp.EMPLOYEE_ID == _objPoleOld.COLLECTOR_ID).EMPLOYEE_NAME;
            txtOLD_BOX_BILLER.Text = DBDataContext.Db.TBL_EMPLOYEEs.FirstOrDefault(emp => emp.EMPLOYEE_ID == _objPoleOld.BILLER_ID).EMPLOYEE_NAME;
            txtOLD_BOX_CUTTER.Text = DBDataContext.Db.TBL_EMPLOYEEs.FirstOrDefault(emp => emp.EMPLOYEE_ID == _objPoleOld.CUTTER_ID).EMPLOYEE_NAME;
            txtOLDPositionInBox.Text = _objCustomerMeterOld.POSITION_IN_BOX.ToString();
        }

        private void dtpActivateDate_ValueChanged(object sender, EventArgs e)
        {

        }

        private void txtNEW_METER_AdvanceSearch(object sender, EventArgs e)
        {
            if (txtNEW_BOX.Text.Trim() == "")
            {
                txtNEW_BOX.CancelSearch();
                return;
            }

            string strBox = txtNEW_BOX.Text;
            TBL_BOX tmp = (from b in DBDataContext.Db.TBL_BOXes
                           where b.BOX_CODE.ToLower().Trim() == strBox.ToLower().Trim()
                           select b).FirstOrDefault();


            if (tmp == null)
            {
                //if user have permission to add new breaker
                if (Login.IsAuthorized(Permission.ADMIN_POLEANDBOX))
                {
                    //if user agree to add new breaker
                    if (MsgBox.ShowQuestion(string.Format(Resources.MSQ_CREATE_BOX, strBox), "") == DialogResult.Yes)
                    {
                        DialogBox diagBox = new DialogBox(GeneralProcess.Insert, new TBL_BOX() { BOX_CODE = strBox, STARTED_DATE = DBDataContext.Db.GetSystemDate(), END_DATE = UIHelper._DefaultDate });
                        diagBox.ShowDialog();
                        if (diagBox.DialogResult == DialogResult.OK)
                        {
                            tmp = diagBox.Box;
                        }
                        else
                        {
                            this.txtNEW_BOX.CancelSearch();
                            return;
                        }
                    }
                    else
                    {
                        this.txtNEW_BOX.CancelSearch();
                        return;
                    }
                }
                else
                {
                    MsgBox.ShowInformation(Resources.MS_BOX_NOT_FOUND);
                    this.txtNEW_BOX.CancelSearch();
                    return;
                }
            }



            if (tmp.STATUS_ID == (int)BoxStatus.Unavailable)
            {
                MsgBox.ShowInformation(Resources.MS_BOX_IS_UNAVAILABLE);
                this.txtNEW_BOX.CancelSearch();
                return;
            }

            _objBoxNew = new TBL_BOX();
            _objBoxOld = new TBL_BOX();
            tmp._CopyTo(_objBoxOld);
            tmp._CopyTo(_objBoxNew);

            _objPoleNew = DBDataContext.Db.TBL_POLEs.FirstOrDefault(p => p.POLE_ID == _objBoxNew.POLE_ID);

            if (_objCustomer.AREA_ID != _objPoleNew.AREA_ID)
            {
                if (MsgBox.ShowQuestion(Resources.MSQ_NEW_BOX_NOT_IN_AREA_OF_CUSTOMER, string.Empty) != DialogResult.Yes)
                {
                    this.txtNEW_BOX.CancelSearch();
                    return;
                }
            }
            // 
            txtNEW_BOX_POLE.Text = _objPoleNew.POLE_CODE;
            txtNEW_BOX_COLLECTOR.Text = DBDataContext.Db.TBL_EMPLOYEEs.FirstOrDefault(emp => emp.EMPLOYEE_ID == _objPoleNew.COLLECTOR_ID).EMPLOYEE_NAME;
            txtNEW_BOX_BILLER.Text = DBDataContext.Db.TBL_EMPLOYEEs.FirstOrDefault(emp => emp.EMPLOYEE_ID == _objPoleNew.BILLER_ID).EMPLOYEE_NAME;
            txtNEW_BOX_CUTTER.Text = DBDataContext.Db.TBL_EMPLOYEEs.FirstOrDefault(emp => emp.EMPLOYEE_ID == _objPoleNew.CUTTER_ID).EMPLOYEE_NAME;
            txtNEWPositionInBox.Text = "";
            if (txtNEWPositionInBox.Visible)
            {
                txtNEWPositionInBox.Focus();
            }
        }

        private void txtNEW_METER_CancelAdvanceSearch(object sender, EventArgs e)
        {
            txtNEW_BOX_POLE.Text = "";
            txtNEW_BOX_CUTTER.Text = "";
            txtNEW_BOX_COLLECTOR.Text = "";
            txtNEW_BOX_BILLER.Text = "";
            _objBoxNew = null;
            _objPoleNew = null;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            // validate it first.
            if (invalid())
            {
                return;
            }
            this.saveData();

        }

        private void saveData()
        {
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    // TO CHANGE BOX OF A CUSTOMER
                    // *********************************************
                    // 1. INSERT NEW CUSTOMER_METER
                    // 2. DELETE OLD CUSTOMER_METER
                    // 3. UPDATE NEW BOX STATUS 
                    // ********************************************* 

                    // create a blank change log.
                    TBL_CHANGE_LOG log = null;

                    // 1. DELETE OLD CUSTOMER METER
                    DBDataContext.Db.DeleteChild(this._objCustomerMeterOld, this._objCustomer, ref log);

                    // 2. INSERT NEW CUSTOMER METER
                    this._objCustomerMeterNew = new TBL_CUSTOMER_METER()
                    {
                        BOX_ID = this._objBoxNew.BOX_ID,
                        BREAKER_ID = this._objCustomerMeterOld.BREAKER_ID,
                        CABLE_SHIELD_ID = this._objCustomerMeterOld.CABLE_SHIELD_ID,
                        CUS_METER_ID = 0,
                        CUSTOMER_ID = this._objCustomer.CUSTOMER_ID,
                        IS_ACTIVE = true,
                        METER_ID = this._objCustomerMeterOld.METER_ID,
                        METER_SHIELD_ID = this._objCustomerMeterOld.METER_SHIELD_ID,
                        POLE_ID = this._objBoxNew.POLE_ID,
                        REMAIN_USAGE = this._objCustomerMeterOld.REMAIN_USAGE,
                        USED_DATE = this.dtpCHANGE_DATE.Value,
                        POSITION_IN_BOX = DataHelper.ParseToInt(txtNEWPositionInBox.Text)
                    };
                    DBDataContext.Db.InsertChild(this._objCustomerMeterNew, this._objCustomer, ref log);

                    // 3. UPDATE NEW METER STATUS
                    TBL_BOX tmpNew = new TBL_BOX();
                    _objBoxNew._CopyTo(tmpNew);
                    _objBoxNew.STATUS_ID = (int)MeterStatus.Used;
                    DBDataContext.Db.Update(tmpNew, _objBoxNew);

                    //4. UPDATE CUSTOMER AREA
                    TBL_CUSTOMER objOldCustomer = new TBL_CUSTOMER();
                    _objCustomer._CopyTo(objOldCustomer);
                    _objCustomer.AREA_ID = _objPoleNew.AREA_ID;
                    DBDataContext.Db.Update(objOldCustomer, _objCustomer);

                    // log user change location
                    DBDataContext.Db.TBL_CUSTOMER_CHANGE_BOXes.InsertOnSubmit(new TBL_CUSTOMER_CHANGE_BOX()
                    {
                        CREATE_BY = SoftTech.Security.Logic.Login.CurrentLogin.LOGIN_NAME,
                        CREATE_ON = DBDataContext.Db.GetSystemDate(),
                        CUSTOMER_ID = _objCustomer.CUSTOMER_ID,
                        LOGIN_ID = SoftTech.Security.Logic.Login.CurrentLogin.LOGIN_ID,
                        NEW_BOX_ID = _objCustomerMeterNew.BOX_ID,
                        OLD_BOX_ID = _objCustomerMeterOld.BOX_ID,
                        IS_ACTIVE = true,
                        CHANGE_DATE = dtpCHANGE_DATE.Value
                    });
                    DBDataContext.Db.SubmitChanges();

                    tran.Complete();
                }

                // if succes than close form.
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void InputEnglish(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        private void InputKhmer(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void txtNewPositionInBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputNumberOnly(sender, e);
            if (e.KeyChar == 13)
            {
                btnOK.Focus();
            }
        }

        private void lblViewBoxCustomers_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (this._objBoxNew == null) { return; }
            var diag = new DialogCustomerInBox(this._objBoxNew.BOX_ID);
            diag.ShowDialog();
        }

    }
}
