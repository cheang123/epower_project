﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using Login = SoftTech.Security.Logic.Login;

namespace EPower.Interface
{
    public partial class DialogPole : ExDialog
    {
        #region get default collector, biller, cutter
        int collectorId = (from em in DBDataContext.Db.TBL_EMPLOYEEs
                           join p in DBDataContext.Db.TBL_EMPLOYEE_POSITIONs on em.EMPLOYEE_ID equals p.EMPLOYEE_ID
                           where p.POSITION_ID == (int)EmpPosition.Collector
                           && em.IS_ACTIVE
                           orderby em.EMPLOYEE_NAME
                           select new { em.EMPLOYEE_ID }).FirstOrDefault().EMPLOYEE_ID;
        int billerId = (from em in DBDataContext.Db.TBL_EMPLOYEEs
                        join p in DBDataContext.Db.TBL_EMPLOYEE_POSITIONs on em.EMPLOYEE_ID equals p.EMPLOYEE_ID
                        where p.POSITION_ID == (int)EmpPosition.Biller
                        && em.IS_ACTIVE
                        orderby em.EMPLOYEE_NAME
                        select new { em.EMPLOYEE_ID }).FirstOrDefault().EMPLOYEE_ID;
        int cutterId = (from em in DBDataContext.Db.TBL_EMPLOYEEs
                        join p in DBDataContext.Db.TBL_EMPLOYEE_POSITIONs on em.EMPLOYEE_ID equals p.EMPLOYEE_ID
                        where p.POSITION_ID == (int)EmpPosition.Cutter
                        && em.IS_ACTIVE
                        orderby em.EMPLOYEE_NAME
                        select new { em.EMPLOYEE_ID }).FirstOrDefault().EMPLOYEE_ID;

        #endregion


        #region Data
        GeneralProcess _flag;
        bool _loading = false;
        TBL_POLE _objNew = new TBL_POLE();
        public TBL_POLE Pole
        {
            get { return _objNew; }
        }
        TBL_POLE _objOld = new TBL_POLE();
        #endregion Data

        #region Constructor
        public DialogPole(GeneralProcess flag, TBL_POLE objPole)
        {
            InitializeComponent();

            btnAddArea.Enabled = Login.IsAuthorized(Permission.ADMIN_AREA);
            btnAddCollector.Enabled =
            btnAddCutter.Enabled =
            btnAddBiller.Enabled = Login.IsAuthorized(Permission.ADMIN_EMPLOYEE);
            btnAddTransfo.Enabled = Login.IsAuthorized(Permission.ADMIN_TRANSFORMER);


            _flag = flag;
            objPole._CopyTo(_objNew);
            objPole._CopyTo(_objOld);
            if (flag == GeneralProcess.Insert)
            {
                objPole.IS_ACTIVE = true;
                this.Text = string.Concat(Resources.INSERT, this.Text);
            }
            else if (flag == GeneralProcess.Update)
            {
                this.Text = string.Concat(Resources.UPDATE, this.Text);
            }
            else if (flag == GeneralProcess.Delete)
            {
                this.Text = string.Concat(Resources.DELETE, this.Text);
                UIHelper.SetEnabled(this, false);
            }
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;

            read();
            this._loading = false;
        }


        #endregion

        #region Method

        void bind()
        {
            bindCutter();

            bindBiller();

            bindCollector();

            bindTransfo();

            bindArea();

            UIHelper.SetDataSourceToComboBox(cboProvince, DBDataContext.Db.TLKP_PROVINCEs);
        }

        private void bindArea()
        {
            UIHelper.SetDataSourceToComboBox(cboAreaName, Lookup.GetAreas());
        }

        private void bindTransfo()
        {
            UIHelper.SetDataSourceToComboBox(cboTransfo, Lookup.GetTransformers());
        }

        private void bindCollector()
        {
            UIHelper.SetDataSourceToComboBox(cboCollector, Lookup.GetEmployeeCollectors());
        }

        private void bindBiller()
        {
            UIHelper.SetDataSourceToComboBox(cboBiller, Lookup.GetEmployeeDistributers());
        }

        private void bindCutter()
        {
            UIHelper.SetDataSourceToComboBox(cboCutter, Lookup.GetEmployeeCutters());
        }



        /// <summary>
        /// Read data from object.
        /// </summary>
        private void read()
        {
            bind();

            txtPoleCode.Text = _objNew.POLE_CODE;
            cboAreaName.SelectedValue = _objNew.AREA_ID;
            cboCollector.SelectedValue = _objNew.COLLECTOR_ID == 0 ? collectorId : _objNew.COLLECTOR_ID;
            cboCutter.SelectedValue = _objNew.CUTTER_ID == 0 ? cutterId : _objNew.CUTTER_ID;
            cboBiller.SelectedValue = _objNew.BILLER_ID == 0 ? billerId : _objNew.BILLER_ID;
            cboTransfo.SelectedValue = _objNew.TRANSFORMER_ID;
            dtpStartDate.Value = _objNew.STARTED_DATE;
            dtpEndDate.SetValue(_objNew.END_DATE);
            lblOWN.Checked = _objNew.IS_PROPERTY;
            this.loadVillage(this._objNew.VILLAGE_CODE);
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            _objNew.POLE_CODE = txtPoleCode.Text.Trim();
            _objNew.AREA_ID = (int)cboAreaName.SelectedValue;
            _objNew.COLLECTOR_ID = int.Parse(cboCollector.SelectedValue.ToString());
            _objNew.BILLER_ID = int.Parse(cboBiller.SelectedValue.ToString());
            _objNew.CUTTER_ID = int.Parse(cboCutter.SelectedValue.ToString());
            _objNew.TRANSFORMER_ID = (int)cboTransfo.SelectedValue;
            _objNew.STARTED_DATE = dtpStartDate.Value;
            _objNew.END_DATE = dtpEndDate.Value;
            _objNew.IS_PROPERTY = lblOWN.Checked;
            _objNew.VILLAGE_CODE = cboVillage.SelectedValue.ToString();
        }

        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();
            if (txtPoleCode.Text.Trim().Length <= 0)
            {
                txtPoleCode.SetValidation(string.Format(Resources.REQUIRED, lblPOLE_CODE.Text));
                val = true;
            }
            if (cboAreaName.SelectedIndex == -1)
            {
                cboAreaName.SetValidation(string.Format(Resources.REQUIRED, lblAREA.Text));
                val = true;
            }
            if (cboTransfo.SelectedIndex == -1)
            {
                cboTransfo.SetValidation(string.Format(Resources.REQUIRED, lblTRANSFORMER.Text));
                val = true;
            }
            if (cboCollector.SelectedIndex == -1)
            {
                cboCollector.SelectedValue = collectorId;
                //cboCollector.SetValidation(string.Format(Resources.REQUIRED, lblCOLLECTOR.Text));
                //val = true;
            }
            if (cboBiller.SelectedIndex == -1)
            {
                cboBiller.SelectedValue = billerId;
                //cboBiller.SetValidation(string.Format(Resources.REQUIRED, lblBILLER.Text));
                //val = true;
            }
            if (cboCutter.SelectedIndex == -1)
            {
                cboCutter.SelectedValue = cutterId;
                //cboCutter.SetValidation(string.Format(Resources.REQUIRED, lblCUTTER.Text));
                //val = true;
            }
            if(cboProvince.SelectedIndex == -1)
            {
                cboProvince.SetValidation(string.Format(Resources.REQUIRED_SELECT_VALUE, lblPROVINCE.Text));
                val = true;
            }
            if (cboDistrict.SelectedIndex == -1)
            {
                cboDistrict.SetValidation(string.Format(Resources.REQUIRED_SELECT_VALUE, lblDISTRICT.Text));
                val = true;
            }
            if (cboCommune.SelectedIndex == -1)
            {
                cboCommune.SetValidation(string.Format(Resources.REQUIRED_SELECT_VALUE, lblCOMMUNE.Text));
                val = true;
            }
            if (cboVillage.SelectedIndex == -1)
            {
                cboVillage.SetValidation(string.Format(Resources.REQUIRED_SELECT_VALUE, lblVILLAGE.Text));
                val = true;
            }
            return val;
        }

        private void loadVillage(string villageCode)
        {
            this.cboProvince.SelectedIndex = -1;
            this.cboDistrict.SelectedIndex = -1;
            this.cboCommune.SelectedIndex = -1;
            this.cboVillage.SelectedIndex = -1;

            if (DBDataContext.Db.TLKP_VILLAGEs.FirstOrDefault(row => row.VILLAGE_CODE == villageCode) != null)
            {
                string communeCode = DBDataContext.Db.TLKP_VILLAGEs.FirstOrDefault(row => row.VILLAGE_CODE == villageCode).COMMUNE_CODE;
                string districCode = DBDataContext.Db.TLKP_COMMUNEs.FirstOrDefault(row => row.COMMUNE_CODE == communeCode).DISTRICT_CODE;
                string province = DBDataContext.Db.TLKP_DISTRICTs.FirstOrDefault(row => row.DISTRICT_CODE == districCode).PROVINCE_CODE;

                UIHelper.SetDataSourceToComboBox(this.cboDistrict, DBDataContext.Db.TLKP_DISTRICTs.Where(row => row.PROVINCE_CODE == province));
                UIHelper.SetDataSourceToComboBox(this.cboCommune, DBDataContext.Db.TLKP_COMMUNEs.Where(row => row.DISTRICT_CODE == districCode));
                UIHelper.SetDataSourceToComboBox(this.cboVillage, DBDataContext.Db.TLKP_VILLAGEs.Where(row => row.COMMUNE_CODE == communeCode));

                this.cboProvince.SelectedValue = province;
                this.cboDistrict.SelectedValue = districCode;
                this.cboCommune.SelectedValue = communeCode;
                this.cboVillage.SelectedValue = villageCode;
            }
        }

        #endregion


        /// <summary>
        /// Data transaction.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
            //If duplicat data.
            txtPoleCode.ClearValidation();
            if (DBDataContext.Db.IsExits(_objNew, "POLE_CODE"))
            {
                txtPoleCode.SetValidation(string.Format(Resources.MS_IS_EXISTS, lblPOLE_CODE.Text));
                return;
            }
            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objNew);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                        if (_objNew.AREA_ID != _objOld.AREA_ID)
                        {
                            var cusInThisPole = from m in DBDataContext.Db.TBL_CUSTOMER_METERs
                                                join b in DBDataContext.Db.TBL_BOXes on m.BOX_ID equals b.BOX_ID
                                                where b.POLE_ID == this._objNew.POLE_ID && m.IS_ACTIVE
                                                select m.CUSTOMER_ID;
                            foreach (var cid in cusInThisPole)
                            {
                                TBL_CUSTOMER objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == cid);
                                objCustomer.AREA_ID = this._objNew.AREA_ID;
                            }
                            DBDataContext.Db.SubmitChanges();
                        }
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objNew);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// Input number integer only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNumber(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputNumberOnly(sender, e);
        }

        /// <summary>
        /// Change current keyboard layout to khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }

        private void btnAddBiller_AddItem(object sender, EventArgs e)
        {
            int intLastCollectorID = 0, intLastCutterId = 0;
            if (cboCollector.SelectedIndex != -1)
            {
                intLastCollectorID = DataHelper.ParseToInt(cboCollector.SelectedValue.ToString());
            }
            if (cboCutter.SelectedIndex != -1)
            {
                intLastCutterId = DataHelper.ParseToInt(cboCutter.SelectedValue.ToString());
            }
            DialogCollector dig = new DialogCollector(new TBL_EMPLOYEE(), EmpPosition.Biller);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                bind();
                cboCutter.SelectedValue = intLastCutterId;
                cboCollector.SelectedValue = intLastCollectorID;
                cboBiller.SelectedValue = dig.Employee.EMPLOYEE_ID;
            }
        }

        private void btnAddCollector_AddItem(object sender, EventArgs e)
        {
            int intLastBillerID = 0, intLastCutterId = 0;
            if (cboBiller.SelectedIndex != -1)
            {
                intLastBillerID = DataHelper.ParseToInt(cboBiller.SelectedValue.ToString());
            }
            if (cboCutter.SelectedIndex != -1)
            {
                intLastCutterId = DataHelper.ParseToInt(cboCutter.SelectedValue.ToString());
            }
            DialogCollector dig = new DialogCollector(new TBL_EMPLOYEE(), EmpPosition.Collector);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                bind();
                cboBiller.SelectedValue = intLastBillerID;
                cboCutter.SelectedValue = intLastCutterId;
                cboCollector.SelectedValue = dig.Employee.EMPLOYEE_ID;
            }
        }

        private void btnAddCutter_AddItem(object sender, EventArgs e)
        {
            int intLastCollectorID = 0, intLastBillerId = 0;
            if (cboCollector.SelectedIndex != -1)
            {
                intLastCollectorID = DataHelper.ParseToInt(cboCollector.SelectedValue.ToString());
            }
            if (cboBiller.SelectedIndex != -1)
            {
                intLastBillerId = DataHelper.ParseToInt(cboBiller.SelectedValue.ToString());
            }
            DialogCollector dig = new DialogCollector(new TBL_EMPLOYEE(), EmpPosition.Cutter);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                bind();

                cboBiller.SelectedValue = intLastBillerId;
                cboCollector.SelectedValue = intLastCollectorID;
                cboCutter.SelectedValue = dig.Employee.EMPLOYEE_ID;
            }
        }

        private void btnAddArea_AddItem(object sender, EventArgs e)
        {
            DialogArea dig = new DialogArea(GeneralProcess.Insert, new TBL_AREA());
            if (dig.ShowDialog() == DialogResult.OK)
            {
                bindArea();
                cboAreaName.SelectedValue = dig.Area.AREA_ID;
            }
        }

        private void btnAddTransfo_AddItem(object sender, EventArgs e)
        {
            DialogTransformer diag = new DialogTransformer(GeneralProcess.Insert, new TBL_TRANSFORMER()
            {
                TRANSFORMER_CODE = string.Empty,
                START_DATE = DBDataContext.Db.GetSystemDate(),
                END_DATE = UIHelper._DefaultDate,
                IS_INUSED = true
            });
            if (diag.ShowDialog() == DialogResult.OK)
            {
                bindTransfo();
                cboTransfo.SelectedValue = diag.Transformer.TRANSFORMER_ID;
            }
        }

        private void dtpEndDate_MouseDown(object sender, MouseEventArgs e)
        {
            if (dtpEndDate.Checked)
            {
                if (dtpEndDate.Value.Date.Equals(UIHelper._DefaultDate))
                {
                    dtpEndDate.SetValue(DBDataContext.Db.GetSystemDate());
                }
                else
                {
                    dtpEndDate.CustomFormat = UIHelper._DefaultDateFormat;
                }
            }
            else
            {
                dtpEndDate.ClearValue();
            }
        }

        private void cboProvince_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!this._loading && cboProvince.SelectedIndex != -1)
            {
                string strProvinceCode = cboProvince.SelectedValue.ToString();
                // distict
                UIHelper.SetDataSourceToComboBox(cboDistrict, DBDataContext.Db.TLKP_DISTRICTs.Where(d => d.PROVINCE_CODE == strProvinceCode));
            }
        }

        private void cboDistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!this._loading && cboProvince.SelectedIndex != -1)
            {
                string strDisCode = cboDistrict.SelectedValue.ToString();
                // communte
                UIHelper.SetDataSourceToComboBox(cboCommune, DBDataContext.Db.TLKP_COMMUNEs.Where(c => c.DISTRICT_CODE == strDisCode));
            }
        }

        private void cboCommune_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!this._loading && cboCommune.SelectedIndex != -1)
            {
                string strComCode = cboCommune.SelectedValue.ToString();
                // village
                UIHelper.SetDataSourceToComboBox(cboVillage, DBDataContext.Db.TLKP_VILLAGEs.Where(v => v.COMMUNE_CODE == strComCode));
            }
        }
    }
}