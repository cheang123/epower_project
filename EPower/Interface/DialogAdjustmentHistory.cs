﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class DialogAdjustmentHistory : SoftTech.Component.ExDialog
    {
        TBL_INVOICE _objInvoice = new TBL_INVOICE();
        TBL_INVOICE_ADJUSTMENT _objAdjust = new TBL_INVOICE_ADJUSTMENT();
        public DialogAdjustmentHistory(TBL_INVOICE objInvoice)
        {
            InitializeComponent();
            objInvoice._CopyTo(_objInvoice);
            read();
        }
        private void read()
        {
            this.dgv.DataSource = from adj in DBDataContext.Db.TBL_INVOICE_ADJUSTMENTs
                                  join m in DBDataContext.Db.TBL_METERs on adj.METER_ID equals m.METER_ID into adjM
                                  from adj_m in adjM.DefaultIfEmpty()
                                  join inv in DBDataContext.Db.TBL_INVOICEs on adj.INVOICE_ID equals inv.INVOICE_ID
                                  join curr in DBDataContext.Db.TLKP_CURRENCies on inv.CURRENCY_ID equals curr.CURRENCY_ID
                                  where adj.INVOICE_ID == _objInvoice.INVOICE_ID
                                  select new
                                  {
                                      adj.ADJUST_INVOICE_ID,
                                      adj.INVOICE_ID,
                                      adj.CREATE_ON,
                                      METER_CODE = (adj_m.METER_CODE == null) ? "" : adj_m.METER_CODE,
                                      adj.BEFORE_ADJUST_USAGE,
                                      BEFORE_ADJUST_AMOUNT= adj.BEFORE_ADJUST_AMOUNT,
                                      adj.ADJUST_USAGE,
                                      ADJUST_AMOUNT=adj.ADJUST_AMOUNT,
                                      curr.CURRENCY_SING,
                                      adj.ADJUST_REASON,
                                      adj.CREATE_BY,
                                      curr.FORMAT
                                  };
        }
        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgv_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            var formatAmount = dgv.Rows[e.RowIndex].Cells[FORMAT.Name].Value?.ToString() ?? "";
            if (e.ColumnIndex != dgv.Columns[BEFORE_ADJUST_AMOUNT_I.Name].Index && e.ColumnIndex != dgv.Columns[ADJUST_AMOUNT_I.Name].Index)
            {
                return;
            }
            e.CellStyle.Format = formatAmount;
        }
    }
      

       
}
