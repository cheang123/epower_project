﻿using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogChangeConnectionType : ExDialog
    {
        TBL_INVOICE _objInv = new TBL_INVOICE();
        long invId;
        int conID, grpID, cusID;

        private bool _binding = false;
        public DialogChangeConnectionType(long _invId)
        {
            InitializeComponent();
            this.invId = _invId;
        }

        #region Method

        void bind()
        {
            var result = (from inv in DBDataContext.Db.TBL_INVOICEs
                          join c in DBDataContext.Db.TBL_CUSTOMERs on inv.CUSTOMER_ID equals c.CUSTOMER_ID
                          join ct in DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs on inv.CUSTOMER_CONNECTION_TYPE_ID equals ct.CUSTOMER_CONNECTION_TYPE_ID
                          join cg in DBDataContext.Db.TLKP_CUSTOMER_GROUPs on ct.NONLICENSE_CUSTOMER_GROUP_ID equals cg.CUSTOMER_GROUP_ID
                          where inv.INVOICE_ID == invId
                          select new
                          {
                              CUSTOMER_ID = c.CUSTOMER_ID,
                              INVOICE_NO = inv.INVOICE_NO,
                              CUSTOMER_NAME = c.LAST_NAME_KH + " " + c.FIRST_NAME_KH,
                              CONNECTION_ID = ct.CUSTOMER_CONNECTION_TYPE_ID,
                              CUSTOMER_GROUP_NAME = cg.CUSTOMER_GROUP_NAME,
                              GROUP_ID = cg.CUSTOMER_GROUP_ID,
                              CUSTOMER_CONNECTION_TYPE_NAME = ct.CUSTOMER_CONNECTION_TYPE_NAME
                          }).FirstOrDefault();

            txtCusName.Text = result.CUSTOMER_NAME;
            txtInvNo.Text = result.INVOICE_NO;
            txtOldCusConn.Text = result.CUSTOMER_CONNECTION_TYPE_NAME;
            txtOldCusGroup.Text = result.CUSTOMER_GROUP_NAME;
            conID = result.CONNECTION_ID;
            grpID = result.GROUP_ID;
            cusID = result.CUSTOMER_ID;
        }

        bool invalid()
        {
            bool blnReturn = false;
            if (cboNewCustomerGroup.SelectedIndex == -1)
            {
                cboNewCustomerGroup.SetValidation(string.Format(Properties.Resources.REQUIRED, " "));
                blnReturn = true;
            }
            if (cboNewConnectionType.SelectedIndex == -1)
            {
                cboNewConnectionType.SetValidation(string.Format(Properties.Resources.REQUIRED, ""));
                blnReturn = true;
            }
            if (cboNewConnectionType.Text == txtOldCusConn.Text && cboNewCustomerGroup.Text == txtOldCusGroup.Text)
            {
                cboNewConnectionType.SetValidation(string.Format(Resources.MSG_NEW_CONNECTION_TYPE_AND_OLD_CONNECTION_TYPE_IS_THE_SAME));
                blnReturn = true;
            }
            return blnReturn;
        }

        public void BindConnectionType()
        {
            _binding = true;
            UIHelper.SetDataSourceToComboBox(cboNewCustomerGroup, DBDataContext.Db.TLKP_CUSTOMER_GROUPs.OrderBy(x => x.DESCRIPTION));
            UIHelper.SetDataSourceToComboBox(cboNewConnectionType, DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs.Where(x => x.NONLICENSE_CUSTOMER_GROUP_ID == 4));
            _binding = false;

        }

        #endregion Method

        private void DialogChangeConnectionType_Load(object sender, EventArgs e)
        {
            bind();
            BindConnectionType();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCHANGE_LOG_Click(object sender, EventArgs e)
        {
            new DialogChangeConnectionTypeHistory(invId).ShowDialog();
        }

        private void cboNewCustomerGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            int groupId = (int)cboNewCustomerGroup.SelectedValue;
            UIHelper.SetDataSourceToComboBox(cboNewConnectionType,
                DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs.OrderBy(x => x.DESCRIPTION).Where(x => x.NONLICENSE_CUSTOMER_GROUP_ID == groupId));
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (invalid())
            {
                return;
            }
            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, DBDataContext.Db.TransactionOption()))
                {
                    DBDataContext.Db.TBL_CHANGE_INVOICE_CONNECTION_TYPEs.InsertOnSubmit(new TBL_CHANGE_INVOICE_CONNECTION_TYPE
                    {
                        CUSTOMER_ID = cusID,
                        INVOICE_ID = invId,
                        CUSTOMER_CONNECTION_TYPE_ID = (int)cboNewConnectionType.SelectedValue,
                        CUSTOMER_CONNECTION_TYPE_ID_OLD = conID,
                        CREATE_ON = DBDataContext.Db.GetSystemDate(),
                        CREATE_BY = SoftTech.Security.Logic.Login.CurrentLogin.LOGIN_NAME,
                        NOTE = txtNote.Text
                    });

                    TBL_INVOICE objInv = DBDataContext.Db.TBL_INVOICEs.FirstOrDefault(x => x.INVOICE_ID == invId);
                    objInv.CUSTOMER_CONNECTION_TYPE_ID = (int)cboNewConnectionType.SelectedValue;
                    objInv.ROW_DATE = DBDataContext.Db.GetSystemDate();

                    DBDataContext.Db.SubmitChanges();
                    tran.Complete();
                }
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }
    }
}
