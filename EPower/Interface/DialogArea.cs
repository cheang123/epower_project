﻿using System;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;

namespace EPower.Interface
{
    public partial class DialogArea : ExDialog
    {
        GeneralProcess _flag;
        TBL_AREA _objArea = new TBL_AREA();
        public TBL_AREA Area
        {
            get { return _objArea; }
        }
        TBL_AREA _oldObjArea = new TBL_AREA();

        #region Constructor
        public DialogArea(GeneralProcess flag, TBL_AREA objArea)
        {
            InitializeComponent();

            _flag = flag;
            objArea._CopyTo(_objArea);
            objArea._CopyTo(_oldObjArea);

            read();
        }
        
        #endregion

        #region Operation
        /// <summary>
        /// Change Keyboard to Khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        } 

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        /// <summary>
        /// Data transaction.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
             //If record is duplicate.
            txtAreaCode.ClearValidation();
            if (DBDataContext.Db.IsExits(_objArea, "AREA_CODE"))
            {
                txtAreaCode.SetValidation(string.Format( Resources.MS_IS_EXISTS,lblAREA_CODE.Text));
                return;
            }

            try
            {
                using (TransactionScope tran=new TransactionScope( TransactionScopeOption.Required,TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objArea);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_oldObjArea, _objArea);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objArea);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }                                               
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }            
        }
        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objArea);
        }

        private void DialogArea_Load(object sender, EventArgs e)
        {
            this.Text = _flag.GetText(this.Text);
            UIHelper.SetEnabled(this, !(_flag == GeneralProcess.Delete));
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;  
        }
        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
        #endregion

        #region Method
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();

            if (txtAreaCode.Text.Trim().Length <= 0)
            {
                txtAreaCode.SetValidation(string.Format(Resources.REQUIRED, lblAREA_CODE.Text));
                val = true;
            }

            if (txtAreaName.Text.Trim().Length <= 0)
            {
                txtAreaName.SetValidation(string.Format(Resources.REQUIRED, lblAREA_NAME.Text));
                val = true;
            }            
            return val;
        }

        /// <summary>
        /// Read data from object.
        /// </summary>
        private void read()
        {
            txtAreaCode.Text = _objArea.AREA_CODE;
            txtAreaName.Text = _objArea.AREA_NAME;
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            _objArea.AREA_CODE = txtAreaCode.Text.Trim();
            _objArea.AREA_NAME = txtAreaName.Text.Trim();
        }
        #endregion

       

       
    }
}