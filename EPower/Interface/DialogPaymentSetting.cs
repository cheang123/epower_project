﻿using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogPaymentSetting : ExDialog
    {
        List<TBL_PAYMENT_CONFIG> objPaymentConfig = new List<TBL_PAYMENT_CONFIG>();
        List<TBL_PAYMENT_CONFIG> objOldPaymentConfig = new List<TBL_PAYMENT_CONFIG>();
        internal static List<PaymentSettingConfigs> paymentConfig = null;

        public DialogPaymentSetting(string paymentMethod = "")
        {
            InitializeComponent();
            this.colACCOUNT_KHR.Visible = DBDataContext.Db.TLKP_CURRENCies.Any(x => x.CURRENCY_ID == (int)Currency.KHR);
            this.colACCOUNT_USD.Visible = DBDataContext.Db.TLKP_CURRENCies.Any(x => x.CURRENCY_ID == (int)Currency.USD);
            this.colACCOUNT_THB.Visible = DBDataContext.Db.TLKP_CURRENCies.Any(x => x.CURRENCY_ID == (int)Currency.THB);
            this.colACCOUNT_VND.Visible = DBDataContext.Db.TLKP_CURRENCies.Any(x => x.CURRENCY_ID == (int)Currency.VND);
            bind();
            searchControl.Properties.NullValuePrompt = Resources.SEARCH;
            searchControl.Text = paymentMethod;  
        }
        #region Operation

        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void dgvPaymentSetting_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            try
            {
                foreach (var newObj in objPaymentConfig)
                {
                    var oldObj = objOldPaymentConfig.FirstOrDefault(x => x.CONFIG_ID == newObj.CONFIG_ID);
                    if (oldObj != newObj)
                    {
                        DBDataContext.Db.Update(oldObj, newObj);
                    }
                }
                DBDataContext.Db.SubmitChanges();
            }
            catch
            {

            }
        }

        private void btnCLOSE_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            CommitPaymentConfig();
            this.DialogResult = DialogResult.OK;
        }
        #endregion

        #region Method
        private void bind()
        {
            try
            {
                var accounts = DBDataContext.Db.TBL_ACCOUNT_CHARTs.ToList(); //AccountLogic.Instance.List<AccountListModel>(new AccountSearchParam() { Natures = new List<AccountNatures> { AccountNatures.Asset }, ShowInAccountPage = false });

                repAccount.DataSource = accounts;

                paymentConfig = DBDataContext.Db.TBL_PAYMENT_CONFIGs.Where(x => x.IS_ACTIVE)
                                  .Select(pc => new PaymentSettingConfigs
                                  {
                                      CONFIG_ID = pc.CONFIG_ID,
                                      PAYMENT_TYPE = pc.PAYMENT_TYPE,
                                      ACCOUNT_ID_KHR = pc.ACCOUNT_ID_KHR,
                                      ACCOUNT_ID_USD = pc.ACCOUNT_ID_USD,
                                      ACCOUNT_ID_THB = pc.ACCOUNT_ID_THB,
                                      ACCOUNT_ID_VND = pc.ACCOUNT_ID_VND,
                                      CUT_OFF_DATE_PAYMENT = pc.CUT_OFF_DATE_PAYMENT
                                  }).ToList();

                //accUSD.DataSource = accounts;
                //accTHB.DataSource = accounts;
                //accVND.DataSource = accounts; 
                //var row = DBDataContext.Db.TBL_PAYMENT_CONFIGs.Where(x => x.IS_ACTIVE).ToList();
                //dgv.DataSource = row;
                dgv.DataSource = paymentConfig;
            }
            catch (Exception e)
            {
                MsgBox.ShowError(e);
            }
        }

        public static void CommitPaymentConfig()
        {
            TBL_PAYMENT_CONFIG objNew = new TBL_PAYMENT_CONFIG();
            foreach (var paymentConfig in paymentConfig)
            {
                var objOld = DBDataContext.Db.TBL_PAYMENT_CONFIGs.FirstOrDefault(x => x.CONFIG_ID == paymentConfig.CONFIG_ID);
                if (objOld != null)
                {
                    objOld._CopyTo(objNew);
                    objNew.ACCOUNT_ID_KHR = paymentConfig.ACCOUNT_ID_KHR;
                    objNew.ACCOUNT_ID_USD = paymentConfig.ACCOUNT_ID_USD;
                    objNew.CUT_OFF_DATE_PAYMENT = paymentConfig.CUT_OFF_DATE_PAYMENT;
                    DBDataContext.Db.Update(objOld, objNew);
                }
            }
            DBDataContext.Db.SubmitChanges();
        }

        #endregion

    }
    public class PaymentSettingConfigs
    {
        public int CONFIG_ID { get; set; }
        public string PAYMENT_TYPE { get; set; }
        public int ACCOUNT_ID_KHR { get; set; }
        public int ACCOUNT_ID_USD { get; set; }
        public int ACCOUNT_ID_THB { get; set; }
        public int ACCOUNT_ID_VND { get; set; }
        public bool IS_ACTIVE { get; set; }
        public DateTime? CUT_OFF_DATE_PAYMENT { get; set; }
    }

}
