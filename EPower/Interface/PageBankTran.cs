﻿using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageBankTran : Form
    {
        public PageBankTran()
        {
            InitializeComponent();
            dtpDate1.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            dtpDate2.Value = dtpDate1.Value.AddMonths(1).AddSeconds(-1);
            UIHelper.SetDataSourceToComboBox(cboCurrency, DBDataContext.Db.TLKP_CURRENCies.Select(x => new { x.CURRENCY_ID, x.CURRENCY_NAME })._ToDataTable(), Resources.ALL_CURRENCY);
            bind(); 
        }   

        private void bind()
        { 
            try
            {

                int mCurrencyId = 0;
                if (cboCurrency.SelectedIndex != -1)
                {
                    mCurrencyId = (int)cboCurrency.SelectedValue;
                }
                //var currency = StaticVariable.Currencies.FirstOrDefault(x => x.CURRENCY_ID == (int)cboCurrency.SelectedValue);
                //AMOUNT.DefaultCellStyle.Format = currency.FORMAT;

                var sources  = (from b in DBDataContext.Db.TBL_BANK_TRANs
                                 join c in DBDataContext.Db.TLKP_CURRENCies on b.CURRENCY_ID equals c.CURRENCY_ID
                                 join c1 in DBDataContext.Db.TLKP_CURRENCies on b.BALANCE_CURRENCY_ID equals c1.CURRENCY_ID
                                 where (b.REF_NO + b.NOTE).ToLower().Contains(txtSearch.Text.ToLower())
                                 && (b.BALANCE_CURRENCY_ID == mCurrencyId || mCurrencyId == 0)
                                 && b.TRAN_DATE >= dtpDate1.Value && b.TRAN_DATE <= dtpDate2.Value
                                        && b.IS_ACTIVE
                                 select new 
                                 {
                                     b.TRAN_ID,
                                     b.REF_NO,
                                     b.TRAN_DATE,
                                     b.CREATE_BY,
                                     Amount = b.AMOUNT,
                                     CURRENCY_SIGN = c.CURRENCY_SING,
                                     FORMAT =  c.FORMAT,
                                     BALANCE_CURRENCY_NAME = c1.CURRENCY_NAME,
                                     b.NOTE
                                 }).ToList().OrderByDescending(x=>x.TRAN_DATE).ToList();
                dgv.DataSource = sources; 
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }     
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (this.dgv.SelectedRows.Count > 0)
            {
                int id = (int) this.dgv.SelectedRows[0].Cells[0].Value;
                TBL_BANK_TRAN obj = DBDataContext.Db.TBL_BANK_TRANs.Single(row => row.TRAN_ID == id);
                DialogBankTran diag = new DialogBankTran(obj, GeneralProcess.Update);
                if (diag.ShowDialog() == DialogResult.OK)
                {
                    this.bind();
                    UIHelper.SelectRow(this.dgv,diag.Voltage.TRAN_ID);
                }
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            DialogBankTran diag = new DialogBankTran(new TBL_BANK_TRAN()
            { 
                TRAN_DATE= DBDataContext.Db.GetSystemDate(),
                CURRENCY_ID =1,
                EXCHANGE_RATE =1,
                BALANCE_CURRENCY_ID=1
            }, GeneralProcess.Insert);

            if (diag.ShowDialog() == DialogResult.OK)
            {
                bind();
                UIHelper.SelectRow(this.dgv,diag.Voltage.TRAN_ID);
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (this.dgv.SelectedRows.Count > 0)
            {
                int id = (int)this.dgv.SelectedRows[0].Cells[0].Value;
                if (isDeletable(id))
                {
                    TBL_BANK_TRAN obj = DBDataContext.Db.TBL_BANK_TRANs.Single(row => row.TRAN_ID == id);
                    DialogBankTran diag = new DialogBankTran(obj, GeneralProcess.Delete);
                    if (diag.ShowDialog() == DialogResult.OK)
                    {
                        this.bind();
                        UIHelper.SelectRow(this.dgv, diag.Voltage.TRAN_ID - 1);
                    }
                }
                else
                {
                    MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE); 
                }
            }
        }

        private void txtSearch_QuickSearch(object sender, EventArgs e)
        {
            bind();
        }

        private bool isDeletable(int id)
        {
            return true;
        }

        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }

        private void dgv_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.RowIndex == -1) 
            {
                return;
            }
            var format = dgv.Rows[e.RowIndex].Cells[FORMAT.Name].Value?.ToString()??"";
            if(e.ColumnIndex !=  dgv.Columns[AMOUNT.Name].Index)
            {
                return;
            }
            e.CellStyle.Format = format;
        }
    }
}
