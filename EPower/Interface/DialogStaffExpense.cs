﻿using EPower.Base.Helper;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using Login = SoftTech.Security.Logic.Login;

namespace EPower.Interface
{
    public partial class DialogStaffExpense : ExDialog
    {
        #region Data
        GeneralProcess _flag = GeneralProcess.Insert;
        TBL_STAFF_EXPENSE _objOld = new TBL_STAFF_EXPENSE();
        TBL_STAFF_EXPENSE _objNew = new TBL_STAFF_EXPENSE();
        public TBL_STAFF_EXPENSE StaffExpense
        {
            get
            {
                return _objNew;
            }
        }

        #endregion Data

        #region Constructor

        public DialogStaffExpense(GeneralProcess flag, TBL_STAFF_EXPENSE obj)
        {
            InitializeComponent();
            this._flag = flag;
            obj._CopyTo(this._objNew);
            obj._CopyTo(this._objOld);
            this.Text = flag.GetText(this.Text);
            UIHelper.SetDataSourceToComboBox(cboCurrency, Lookup.GetCurrencies());

            //Setup Account Payment
            new AccountChartPopulator().PopluateTree(cboAccountPayment.TreeView, AccountChartHelper.GetAccounts(AccountConfig.PAYMENT_ACCOUNTS));

            this.read();
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
            UIHelper.SetEnabled(this, _flag == GeneralProcess.Delete ? false : true);
        }

        #endregion Constructor

        #region Method

        public bool invalid()
        {
            bool val = false;

            this.ClearAllValidation();

            if (cboAccountPayment.TreeView.SelectedNode == null)
            {
                cboAccountPayment.SetValidation(string.Format(Resources.REQUIRED, lblPAYMENT_ACCOUNT.Text));
                val = true;
            }

            return val;
        }

        public void read()
        {
            dtpExpenseDate.Value = _objNew.EXPENSE_DATE;
            cboCurrency.SelectedValue = _objNew.CURRENCY_ID;
            txtTotal.Text = UIHelper.FormatCurrency(_objNew.TOTAL_AMOUNT, _objNew.CURRENCY_ID);
            cboAccountPayment.TreeView.SelectedNode = cboAccountPayment.TreeView.GetNodesIncludeAncestors().FirstOrDefault(x => (int)x.Tag == _objNew.PAYMENT_ACCOUNT_ID);
            cboAccountPayment.Text = cboAccountPayment.TreeView.SelectedNode != null ? cboAccountPayment.TreeView.SelectedNode.Text : "";
            readStaffDetail();
        }

        public void write()
        {
            _objNew.EXPENSE_DATE = dtpExpenseDate.Value;
            _objNew.CURRENCY_ID = (int)cboCurrency.SelectedValue;
            _objNew.TOTAL_AMOUNT = DataHelper.ParseToDecimal(txtTotal.Text);
            _objNew.CREATE_BY = Login.CurrentLogin.LOGIN_NAME;
            _objNew.CREATE_ON = DBDataContext.Db.GetSystemDate();
            _objNew.PAYMENT_ACCOUNT_ID = (int)cboAccountPayment.TreeView.SelectedNode.Tag;

            // not yet decide on interface
            _objNew.NOTE = "";
        }

        public void readStaffDetail()
        {
            var tmp = from p in DBDataContext.Db.TBL_POSITIONs
                      join b in DBDataContext.Db.TBL_LOOKUP_VALUEs on p.BUSINESS_DIVISION_ID equals b.VALUE_ID
                      join se in (
                            from sed in DBDataContext.Db.TBL_STAFF_EXPENSE_DETAILs
                            join se in DBDataContext.Db.TBL_STAFF_EXPENSEs on sed.EXPENSE_ID equals se.EXPENSE_ID
                            where se.IS_ACTIVE && se.EXPENSE_DATE == _objNew.EXPENSE_DATE
                            select new { sed.EXPENSE_DETAIL_ID, sed.POSITION_ID, sed.TOTAL_STAFF, sed.S6010, sed.S6020, sed.S6040, sed.S6050, sed.S6060, sed.S6070, sed.TOTAL }
                          ) on p.POSITION_ID equals se.POSITION_ID into joinSED
                      from sed in joinSED.DefaultIfEmpty()
                      orderby p.BUSINESS_DIVISION_ID, p.POSITION_NAME
                      select new
                      {
                          EXPENSE_DETAIL_ID = sed.EXPENSE_DETAIL_ID == null ? 0 : sed.EXPENSE_DETAIL_ID,
                          p.POSITION_ID,
                          DIVISION = b.VALUE_TEXT,
                          p.POSITION_NAME,
                          p.IS_PAYROLL,
                          p.IS_ACTIVE,
                          TOTAL_STAFF = _objNew.EXPENSE_ID == 0 ? DBDataContext.Db.TBL_EMPLOYEE_POSITIONs.Where(x => x.POSITION_ID == p.POSITION_ID).Count() : sed.TOTAL_STAFF == null ? 0 : sed.TOTAL_STAFF,
                          S6010 = sed.S6010 == null ? 0 : sed.S6010,
                          S6020 = sed.S6020 == null ? 0 : sed.S6020,
                          S6040 = sed.S6040 == null ? 0 : sed.S6040,
                          S6050 = sed.S6050 == null ? 0 : sed.S6050,
                          S6060 = sed.S6060 == null ? 0 : sed.S6060,
                          S6070 = sed.S6070 == null ? 0 : sed.S6070,
                          TOTAL = sed.TOTAL == null ? 0 : sed.TOTAL
                      };
            if (_flag == GeneralProcess.Insert)
            {
                tmp = tmp.Where(x => x.IS_PAYROLL && x.IS_ACTIVE);
            }
            else
            {
                tmp = tmp.Where(x => x.EXPENSE_DETAIL_ID > 0);
            }
            dgvStaffExpense.DataSource = tmp._ToDataTable();
            getTotal();
        }

        private void getTotal()
        {
            decimal total = 0;
            foreach (DataGridViewRow item in dgvStaffExpense.Rows)
            {
                total += (decimal)item.Cells[TOTAL.Name].Value;
            }
            txtTotal.Text = UIHelper.FormatCurrency(total, (int)cboCurrency.SelectedValue);
        }

        #endregion Method

        #region Event

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.invalid())
                {
                    return;
                }

                this.write();
                this.ClearValidation();
                if (DBDataContext.Db.TBL_STAFF_EXPENSEs.Where(x => x.EXPENSE_DATE == dtpExpenseDate.Value && x.IS_ACTIVE && x.EXPENSE_ID != _objNew.EXPENSE_ID).Count() > 0)
                {
                    dtpExpenseDate.SetValidation(string.Format(Resources.MS_IS_EXISTS, this.lblMONTH.Text));
                    return;
                }

                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    DateTime date = DBDataContext.Db.GetSystemDate();
                    if (this._flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(this._objNew);
                        DBDataContext.Db.SubmitChanges();

                        foreach (DataGridViewRow item in dgvStaffExpense.Rows)
                        {
                            DBDataContext.Db.TBL_STAFF_EXPENSE_DETAILs.InsertOnSubmit(new TBL_STAFF_EXPENSE_DETAIL()
                            {
                                EXPENSE_ID = _objNew.EXPENSE_ID,
                                POSITION_ID = (int)item.Cells[POSITION_ID.Name].Value,
                                TOTAL_STAFF = (decimal)item.Cells[TOTAL_STAFF.Name].Value,
                                S6010 = (decimal)item.Cells[BASE_SALARY.Name].Value,
                                S6020 = (decimal)item.Cells[OVER_TIME.Name].Value,
                                S6040 = (decimal)item.Cells[SOCAIL_SECURITY_EXPENSE.Name].Value,
                                S6050 = (decimal)item.Cells[BENEFIT_AND_BONUS.Name].Value,
                                S6060 = (decimal)item.Cells[FOOD_AND_OTHER_ALLOWANCE.Name].Value,
                                S6070 = (decimal)item.Cells[OTHER_BONUS.Name].Value,
                                TOTAL = (decimal)item.Cells[TOTAL.Name].Value,
                                IS_ACTIVE = true,
                                ROW_DATE = date
                            });
                        }
                    }
                    else if (this._flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(this._objOld, this._objNew);
                        foreach (DataGridViewRow item in dgvStaffExpense.Rows)
                        {
                            TBL_STAFF_EXPENSE_DETAIL objStaffExpenseDetail = DBDataContext.Db.TBL_STAFF_EXPENSE_DETAILs.FirstOrDefault(x => x.EXPENSE_DETAIL_ID == (int)item.Cells[EXPENSE_DETAIL_ID.Name].Value);
                            objStaffExpenseDetail.TOTAL_STAFF = (decimal)item.Cells[TOTAL_STAFF.Name].Value;
                            objStaffExpenseDetail.S6010 = (decimal)item.Cells[BASE_SALARY.Name].Value;
                            objStaffExpenseDetail.S6020 = (decimal)item.Cells[OVER_TIME.Name].Value;
                            objStaffExpenseDetail.S6040 = (decimal)item.Cells[SOCAIL_SECURITY_EXPENSE.Name].Value;
                            objStaffExpenseDetail.S6050 = (decimal)item.Cells[BENEFIT_AND_BONUS.Name].Value;
                            objStaffExpenseDetail.S6060 = (decimal)item.Cells[FOOD_AND_OTHER_ALLOWANCE.Name].Value;
                            objStaffExpenseDetail.S6070 = (decimal)item.Cells[OTHER_BONUS.Name].Value;
                            objStaffExpenseDetail.TOTAL = (decimal)item.Cells[TOTAL.Name].Value;
                            objStaffExpenseDetail.ROW_DATE = date;
                        }
                    }
                    else if (this._flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(this._objNew);
                    }

                    DBDataContext.Db.SubmitChanges();
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp);
            }
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }

        private void txtCAPACITY_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        #endregion Event

        private void dgvStaffExpense_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow dr = dgvStaffExpense.Rows[e.RowIndex];
            if (dr == null)
            {
                return;
            }

            decimal S6010 = (decimal)dr.Cells[BASE_SALARY.Name].Value;
            decimal S6020 = (decimal)dr.Cells[OVER_TIME.Name].Value;
            decimal S6040 = (decimal)dr.Cells[SOCAIL_SECURITY_EXPENSE.Name].Value;
            decimal S6050 = (decimal)dr.Cells[BENEFIT_AND_BONUS.Name].Value;
            decimal S6060 = (decimal)dr.Cells[FOOD_AND_OTHER_ALLOWANCE.Name].Value;
            decimal S6070 = (decimal)dr.Cells[OTHER_BONUS.Name].Value;
            dr.Cells[TOTAL.Name].Value = S6010 + S6020 + S6040 + S6050 + S6060 + S6070;
            getTotal();
        }

        private void dgvStaffExpense_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }

        private void dgvStaffExpense_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
    }
}
