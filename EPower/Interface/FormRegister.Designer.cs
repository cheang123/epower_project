﻿using System.ComponentModel;
using System.Windows.Forms;

namespace EPower.Interface
{
    partial class FormRegister
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnConnect = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.txtConnection = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtEncryptedConnection = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCustomers = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtMonths = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPrepaidCode = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtActivate = new System.Windows.Forms.DateTimePicker();
            this.txtProcessorCode = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupLicense = new System.Windows.Forms.GroupBox();
            this.groupIRReader = new System.Windows.Forms.GroupBox();
            this.txtIRCode = new System.Windows.Forms.TextBox();
            this.btnRegister = new System.Windows.Forms.Button();
            this.txtIRName = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupLicense.SuspendLayout();
            this.groupIRReader.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(11, 60);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(92, 23);
            this.btnConnect.TabIndex = 0;
            this.btnConnect.Text = "&Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // btnApply
            // 
            this.btnApply.Location = new System.Drawing.Point(15, 85);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(92, 23);
            this.btnApply.TabIndex = 1;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // txtConnection
            // 
            this.txtConnection.Location = new System.Drawing.Point(11, 37);
            this.txtConnection.Name = "txtConnection";
            this.txtConnection.Size = new System.Drawing.Size(550, 21);
            this.txtConnection.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Connection String";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(170, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Encrypted Connection String";
            // 
            // txtEncryptedConnection
            // 
            this.txtEncryptedConnection.BackColor = System.Drawing.Color.LightYellow;
            this.txtEncryptedConnection.Location = new System.Drawing.Point(12, 105);
            this.txtEncryptedConnection.Name = "txtEncryptedConnection";
            this.txtEncryptedConnection.Size = new System.Drawing.Size(550, 21);
            this.txtEncryptedConnection.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Lisence Info";
            // 
            // txtCustomers
            // 
            this.txtCustomers.Location = new System.Drawing.Point(12, 60);
            this.txtCustomers.Name = "txtCustomers";
            this.txtCustomers.Size = new System.Drawing.Size(95, 21);
            this.txtCustomers.TabIndex = 7;
            this.txtCustomers.Text = "150";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Customers";
            // 
            // txtMonths
            // 
            this.txtMonths.Location = new System.Drawing.Point(113, 60);
            this.txtMonths.Name = "txtMonths";
            this.txtMonths.Size = new System.Drawing.Size(95, 21);
            this.txtMonths.TabIndex = 9;
            this.txtMonths.Text = "6";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(110, 42);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Months";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(211, 42);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Prepaid Code";
            // 
            // txtPrepaidCode
            // 
            this.txtPrepaidCode.Location = new System.Drawing.Point(214, 60);
            this.txtPrepaidCode.Name = "txtPrepaidCode";
            this.txtPrepaidCode.Size = new System.Drawing.Size(95, 21);
            this.txtPrepaidCode.TabIndex = 11;
            this.txtPrepaidCode.Text = "8080";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(312, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Activate";
            // 
            // txtActivate
            // 
            this.txtActivate.CustomFormat = "yyyy-MM-dd";
            this.txtActivate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtActivate.Location = new System.Drawing.Point(315, 60);
            this.txtActivate.Name = "txtActivate";
            this.txtActivate.Size = new System.Drawing.Size(128, 21);
            this.txtActivate.TabIndex = 14;
            this.txtActivate.Value = new System.DateTime(2011, 1, 1, 0, 0, 0, 0);
            // 
            // txtProcessorCode
            // 
            this.txtProcessorCode.BackColor = System.Drawing.Color.LightYellow;
            this.txtProcessorCode.Location = new System.Drawing.Point(15, 129);
            this.txtProcessorCode.Name = "txtProcessorCode";
            this.txtProcessorCode.Size = new System.Drawing.Size(546, 21);
            this.txtProcessorCode.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 111);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(97, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "Processor Code";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnConnect);
            this.groupBox1.Controls.Add(this.txtConnection);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtEncryptedConnection);
            this.groupBox1.Location = new System.Drawing.Point(6, 1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(578, 139);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "SQL Connection";
            // 
            // groupLicense
            // 
            this.groupLicense.Controls.Add(this.label3);
            this.groupLicense.Controls.Add(this.txtCustomers);
            this.groupLicense.Controls.Add(this.label8);
            this.groupLicense.Controls.Add(this.txtProcessorCode);
            this.groupLicense.Controls.Add(this.label4);
            this.groupLicense.Controls.Add(this.txtMonths);
            this.groupLicense.Controls.Add(this.btnApply);
            this.groupLicense.Controls.Add(this.txtActivate);
            this.groupLicense.Controls.Add(this.label5);
            this.groupLicense.Controls.Add(this.label7);
            this.groupLicense.Controls.Add(this.txtPrepaidCode);
            this.groupLicense.Controls.Add(this.label6);
            this.groupLicense.Location = new System.Drawing.Point(6, 144);
            this.groupLicense.Name = "groupLicense";
            this.groupLicense.Size = new System.Drawing.Size(578, 170);
            this.groupLicense.TabIndex = 18;
            this.groupLicense.TabStop = false;
            this.groupLicense.Text = "License Info";
            // 
            // groupIRReader
            // 
            this.groupIRReader.Controls.Add(this.label10);
            this.groupIRReader.Controls.Add(this.label9);
            this.groupIRReader.Controls.Add(this.txtIRName);
            this.groupIRReader.Controls.Add(this.txtIRCode);
            this.groupIRReader.Controls.Add(this.btnRegister);
            this.groupIRReader.Location = new System.Drawing.Point(6, 320);
            this.groupIRReader.Name = "groupIRReader";
            this.groupIRReader.Size = new System.Drawing.Size(578, 113);
            this.groupIRReader.TabIndex = 19;
            this.groupIRReader.TabStop = false;
            this.groupIRReader.Text = "Register IR Reader";
            // 
            // txtIRCode
            // 
            this.txtIRCode.Location = new System.Drawing.Point(15, 39);
            this.txtIRCode.Name = "txtIRCode";
            this.txtIRCode.Size = new System.Drawing.Size(193, 21);
            this.txtIRCode.TabIndex = 7;
            // 
            // btnRegister
            // 
            this.btnRegister.Location = new System.Drawing.Point(15, 73);
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.Size = new System.Drawing.Size(92, 23);
            this.btnRegister.TabIndex = 1;
            this.btnRegister.Text = "&Register IR Reader";
            this.btnRegister.UseVisualStyleBackColor = true;
            this.btnRegister.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtIRName
            // 
            this.txtIRName.Location = new System.Drawing.Point(214, 39);
            this.txtIRName.Name = "txtIRName";
            this.txtIRName.Size = new System.Drawing.Size(347, 21);
            this.txtIRName.TabIndex = 8;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 20);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Code";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(211, 20);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(40, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "Name";
            // 
            // FormRegister
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(590, 445);
            this.Controls.Add(this.groupIRReader);
            this.Controls.Add(this.groupLicense);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FormRegister";
            this.Text = "FormRegister";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupLicense.ResumeLayout(false);
            this.groupLicense.PerformLayout();
            this.groupIRReader.ResumeLayout(false);
            this.groupIRReader.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Button btnConnect;
        private Button btnApply;
        private TextBox txtConnection;
        private Label label1;
        private Label label2;
        private TextBox txtEncryptedConnection;
        private Label label3;
        private TextBox txtCustomers;
        private Label label4;
        private TextBox txtMonths;
        private Label label5;
        private Label label6;
        private TextBox txtPrepaidCode;
        private Label label7;
        private DateTimePicker txtActivate;
        private TextBox txtProcessorCode;
        private Label label8;
        private GroupBox groupBox1;
        private GroupBox groupLicense;
        private GroupBox groupIRReader;
        private TextBox txtIRCode;
        private Button btnRegister;
        private Label label9;
        private TextBox txtIRName;
        private Label label10;
    }
}