﻿
namespace EPower.Interface
{
    partial class DialogImportInvoice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._btnBrowse = new System.Windows.Forms.Button();
            this.txtFILE_NAME = new System.Windows.Forms.TextBox();
            this.lblFILE_LOCATION = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkADD_NEW_INVOICE = new System.Windows.Forms.CheckBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.chkADD_NEW_INVOICE);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this._btnBrowse);
            this.content.Controls.Add(this.txtFILE_NAME);
            this.content.Controls.Add(this.lblFILE_LOCATION);
            this.content.Controls.Add(this.label16);
            this.content.Size = new System.Drawing.Size(445, 133);
            this.content.Text = "CONTENT";
            this.content.Controls.SetChildIndex(this.label16, 0);
            this.content.Controls.SetChildIndex(this.lblFILE_LOCATION, 0);
            this.content.Controls.SetChildIndex(this.txtFILE_NAME, 0);
            this.content.Controls.SetChildIndex(this._btnBrowse, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.chkADD_NEW_INVOICE, 0);
            // 
            // _btnBrowse
            // 
            this._btnBrowse.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this._btnBrowse.Location = new System.Drawing.Point(381, 18);
            this._btnBrowse.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this._btnBrowse.Name = "_btnBrowse";
            this._btnBrowse.Size = new System.Drawing.Size(49, 27);
            this._btnBrowse.TabIndex = 378;
            this._btnBrowse.Text = "...";
            this._btnBrowse.UseVisualStyleBackColor = true;
            this._btnBrowse.Click += new System.EventHandler(this._btnBrowse_Click);
            // 
            // txtFILE_NAME
            // 
            this.txtFILE_NAME.Location = new System.Drawing.Point(128, 18);
            this.txtFILE_NAME.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtFILE_NAME.Name = "txtFILE_NAME";
            this.txtFILE_NAME.ReadOnly = true;
            this.txtFILE_NAME.Size = new System.Drawing.Size(248, 27);
            this.txtFILE_NAME.TabIndex = 381;
            // 
            // lblFILE_LOCATION
            // 
            this.lblFILE_LOCATION.AutoSize = true;
            this.lblFILE_LOCATION.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblFILE_LOCATION.Location = new System.Drawing.Point(7, 21);
            this.lblFILE_LOCATION.Margin = new System.Windows.Forms.Padding(1);
            this.lblFILE_LOCATION.Name = "lblFILE_LOCATION";
            this.lblFILE_LOCATION.Size = new System.Drawing.Size(74, 19);
            this.lblFILE_LOCATION.TabIndex = 379;
            this.lblFILE_LOCATION.Text = "ទីតាំងឯកសារ";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label16.Location = new System.Drawing.Point(110, 22);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(15, 19);
            this.label16.TabIndex = 380;
            this.label16.Text = "៖";
            // 
            // btnCLOSE
            // 
            this.btnCLOSE.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnCLOSE.Location = new System.Drawing.Point(355, 99);
            this.btnCLOSE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.Size = new System.Drawing.Size(75, 23);
            this.btnCLOSE.TabIndex = 384;
            this.btnCLOSE.Text = "បិទ";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnCLOSE_Click);
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnOK.Location = new System.Drawing.Point(276, 99);
            this.btnOK.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 383;
            this.btnOK.Text = "យល់ព្រម";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Location = new System.Drawing.Point(2, 91);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(441, 1);
            this.panel1.TabIndex = 385;
            // 
            // chkADD_NEW_INVOICE
            // 
            this.chkADD_NEW_INVOICE.AutoSize = true;
            this.chkADD_NEW_INVOICE.Checked = true;
            this.chkADD_NEW_INVOICE.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkADD_NEW_INVOICE.Location = new System.Drawing.Point(12, 102);
            this.chkADD_NEW_INVOICE.Name = "chkADD_NEW_INVOICE";
            this.chkADD_NEW_INVOICE.Size = new System.Drawing.Size(92, 23);
            this.chkADD_NEW_INVOICE.TabIndex = 386;
            this.chkADD_NEW_INVOICE.Text = "បន្ថែមទម្រងថ្មី";
            this.chkADD_NEW_INVOICE.UseVisualStyleBackColor = true;
            // 
            // DialogImportInvoice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(445, 156);
            this.Name = "DialogImportInvoice";
            this.Text = "ទាញយក";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _btnBrowse;
        private System.Windows.Forms.TextBox txtFILE_NAME;
        private System.Windows.Forms.Label lblFILE_LOCATION;
        private System.Windows.Forms.Label label16;
        private SoftTech.Component.ExButton btnCLOSE;
        private SoftTech.Component.ExButton btnOK;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox chkADD_NEW_INVOICE;
    }
}