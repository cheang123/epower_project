﻿using EPower.Base.Logic;
using EPower.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageRunBillHistory : Form
    {
        #region Constructor
        public PageRunBillHistory()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgvBilling);
            bindBillingCycle();
            BindReport();
        }
        #endregion Constructor

        #region Method

        private void BindReport()
        {
            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("REPORT_ID", typeof(int));
            dtReport.Columns.Add("REPORT_NAME", typeof(string));
            dtReport.Rows.Add(0, Resources.REPORT_POWER);
            dtReport.Rows.Add(1, Resources.REPORT_SUMMARY);
            UIHelper.SetDataSourceToComboBox(cboReport, dtReport, "REPORT_ID", "REPORT_NAME");
            cboReport.SelectedIndex = 0;
        }
        void viewReport()
        {
            if (dgvBilling.SelectedRows.Count == 0)
            {
                return;
            }

            if (cboReport.SelectedIndex == 0)
            {
                if (new DialogReportUsageSummary().ShowDialog() != DialogResult.OK)
                {
                    return;
                }

                Runner.Run(delegate ()
                {
                    int intRunID = (int)this.dgvBilling.SelectedRows[0].Cells[RUN_ID.Name].Value;
                    TBL_RUN_BILL objRun = DBDataContext.Db.TBL_RUN_BILLs.FirstOrDefault(row => row.RUN_ID == intRunID);
                    TBL_BILLING_CYCLE objCycle = DBDataContext.Db.TBL_BILLING_CYCLEs.FirstOrDefault(row => row.CYCLE_ID == objRun.CYCLE_ID);
                    DateTime month = (DateTime)this.dgvBilling.SelectedRows[0].Cells[MONTH.Name].Value;
                    this.ch = ViewReportUsageSummary(objCycle.CYCLE_ID, month, objCycle.CYCLE_NAME);
                    this.ch.ViewReport(Resources.REPORT_POWER);
                });
            }
            else if (cboReport.SelectedIndex == 1)
            {
                Runner.Run(delegate ()
                {
                    int intRunID = (int)this.dgvBilling.SelectedRows[0].Cells[RUN_ID.Name].Value;
                    TBL_RUN_BILL objRun = DBDataContext.Db.TBL_RUN_BILLs.FirstOrDefault(row => row.RUN_ID == intRunID);
                    TBL_BILLING_CYCLE objCycle = DBDataContext.Db.TBL_BILLING_CYCLEs.FirstOrDefault(row => row.CYCLE_ID == objRun.CYCLE_ID);
                    DateTime month = (DateTime)this.dgvBilling.SelectedRows[0].Cells[MONTH.Name].Value;
                    this.ch = ViewReportBillingSummary(objRun.RUN_ID, month, objCycle.CYCLE_NAME);
                    this.ch.ViewReport(Resources.REPORT_SUMMARY);
                });
            }
        }
        private void bindBillingCycle()
        {
            DataTable dt = DBDataContext.Db.TBL_BILLING_CYCLEs.Where(r => r.IS_ACTIVE == true)._ToDataTable();
            DataRow row = dt.NewRow();
            dt.Rows.InsertAt(row, 0);
            row[0] = 0;
            row[1] = Resources.ALL_CYCLE;
            UIHelper.SetDataSourceToComboBox(this.cboBillingCycle, dt);
            this.cboBillingCycle.SelectedIndex = 0;
        }

        public void bind()
        {
            using (TransactionScope tran = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions() { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
            {
                if (cboBillingCycle.SelectedIndex == -1)
                {
                    return;
                }
                int intYear = dtpRunYear.Value.Year;
                int intBillingCycle = int.Parse(cboBillingCycle.SelectedValue.ToString());

                dgvBilling.DataSource = from r in DBDataContext.Db.TBL_RUN_BILLs
                                        join c in DBDataContext.Db.TBL_BILLING_CYCLEs on r.CYCLE_ID equals c.CYCLE_ID
                                        where r.IS_ACTIVE && r.BILLING_MONTH.Year == intYear && (r.CYCLE_ID == intBillingCycle || intBillingCycle == 0)
                                        orderby r.CREATE_ON descending
                                        select new
                                        {
                                            r.CYCLE_ID,
                                            r.RUN_ID,
                                            c.CYCLE_NAME,
                                            r.CREATE_BY,
                                            r.CREATE_ON,
                                            r.BILLING_MONTH,
                                            r.TOTAL_INVOICE,
                                            r.TOTAL_POWER
                                        };
                tran.Complete();
            }
        }
        #endregion Method

        #region Event
        private void cboBillingCycle_SelectedIndexChanged(object sender, EventArgs e)
        {
            bind();
        }

        private void dtpRunYear_ValueChanged(object sender, EventArgs e)
        {
            bind();
        }
        #endregion Event

        CrystalReportHelper ch = null;

        public CrystalReportHelper ViewReportUsageSummary(int intCycle, DateTime dtMonth, string strCycleName)
        {
            CrystalReportHelper c = new CrystalReportHelper("ReportUsageSummary.rpt");
            c.SetParameter("@CYCLE_ID", intCycle);
            c.SetParameter("@MONTH", dtMonth);
            c.SetParameter("@CYCLE_NAME", strCycleName);

            for (int i = 1; i <= 4; i++)
            {
                var p = DBDataContext.Db.TBL_PARAM_REPORT_USAGEs.FirstOrDefault(row => row.PARAM_ID == i);
                if (p != null)
                {
                    if (p.TO_USAGE == p.FROM_USAGE)
                    {
                        c.SetParameter("@TEXT" + i.ToString(), p.FROM_USAGE.ToString("0"));
                    }
                    else
                    {
                        c.SetParameter("@TEXT" + i.ToString(), string.Format("{0} - {1}", p.FROM_USAGE.ToString("0"), p.TO_USAGE.ToString("0")));
                    }
                }
            }
            return c;
        }

        public CrystalReportHelper ViewReportBillingSummary(int intRun, DateTime dtMonth, string strCycleName)
        {
            CrystalReportHelper c = new CrystalReportHelper("ReportBillingSummary.rpt");
            c.SetParameter("@RUN_ID", intRun);
            c.SetParameter("@MONTH", dtMonth);
            c.SetParameter("@CYCLE_NAME", strCycleName);
            return c;

        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            if (this.dgvBilling.SelectedRows.Count > 0)
            {
                if (new DialogReportUsageSummary().ShowDialog() != DialogResult.OK)
                {
                    return;
                }

                int intRunID = (int)this.dgvBilling.SelectedRows[0].Cells[RUN_ID.Name].Value;
                TBL_RUN_BILL objRun = DBDataContext.Db.TBL_RUN_BILLs.FirstOrDefault(row => row.RUN_ID == intRunID);
                TBL_BILLING_CYCLE objCycle = DBDataContext.Db.TBL_BILLING_CYCLEs.FirstOrDefault(row => row.CYCLE_ID == objRun.CYCLE_ID);
                DateTime month = (DateTime)this.dgvBilling.SelectedRows[0].Cells[MONTH.Name].Value;
                this.ch = ViewReportUsageSummary(objCycle.CYCLE_ID, month, objCycle.CYCLE_NAME);
                this.ch.ViewReport(Resources.REPORT);
            }
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            if (this.dgvBilling.SelectedRows.Count > 0)
            {
                int intRunID = (int)this.dgvBilling.SelectedRows[0].Cells[RUN_ID.Name].Value;
                TBL_RUN_BILL objRun = DBDataContext.Db.TBL_RUN_BILLs.FirstOrDefault(row => row.RUN_ID == intRunID);
                TBL_BILLING_CYCLE objCycle = DBDataContext.Db.TBL_BILLING_CYCLEs.FirstOrDefault(row => row.CYCLE_ID == objRun.CYCLE_ID);
                DateTime month = (DateTime)this.dgvBilling.SelectedRows[0].Cells[MONTH.Name].Value;
                this.ch = ViewReportBillingSummary(objRun.RUN_ID, month, objCycle.CYCLE_NAME);
                this.ch.ViewReport(Resources.REPORT);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (this.dgvBilling.SelectedRows.Count == 0)
                return;

            var diag = new DialogRunBillHistory((int)this.dgvBilling.SelectedRows[0].Cells[this.RUN_ID.Name].Value);
            if (diag.ShowDialog() == DialogResult.OK)
            {
                this.bind();
            }
        }

        private void btnReverseBill_Click(object sender, EventArgs e)
        {
            if (this.dgvBilling.SelectedRows.Count == 0)
            {
                return;
            }

            //check last runbill of this cycle
            var runId = (int)this.dgvBilling.SelectedRows[0].Cells[RUN_ID.Name].Value;
            var cycleId = (int)this.dgvBilling.SelectedRows[0].Cells[CYCLE_ID.Name].Value;
            var MaxbillingMonth = DBDataContext.Db.TBL_RUN_BILLs.Where(x => x.CYCLE_ID == cycleId);

            if (runId != MaxbillingMonth.Max(x => x.RUN_ID))
            {
                MsgBox.ShowInformation(String.Format(Resources.CANNOT_REVERSE_OLD_BILL), String.Format(Resources.CANNOT_REVERSE));
                return;
            }

            if (DBDataContext.Db.TBL_RUN_PREPAID_CREDITs.Where(x => x.RUN_ID == runId).Count() > 0)
            {
                MsgBox.ShowInformation(String.Format(Resources.CANNOT_REVERSE_BILLING_CYCLE_PREPAID), String.Format(Resources.CANNOT_REVERSE));
                return;
            }

            //limited reverse invoices in one cycle. 
            var allInvoices = DBDataContext.Db.TBL_INVOICEs.Where(x => x.RUN_ID == runId && x.CYCLE_ID == cycleId).Count();
            var maxedCustomerInCycle = int.Parse(Method.GetUtilityValue(Utility.MAXED_CUSTOMER_IN_CYCLE));

            if (allInvoices > maxedCustomerInCycle)
            {
                MsgBox.ShowInformation(String.Format(Resources.CAN_NOT_REVERSE_BILL_CONTACT_SUPPORT, maxedCustomerInCycle), String.Format(Resources.CANNOT_REVERSE));
                return;
            }
            //if (Logic.Login.CurrentCashDrawer == null)
            //{
            //    if (MsgBox.ShowQuestion(Properties.Resources.MSG_OPEN_CASH_DRAWER_BEFORE_REVERSE_BILL, string.Empty) == DialogResult.Yes)
            //    {
            //        new DialogOpenCashDrawer().ShowDialog();
            //        if (Logic.Login.CurrentCashDrawer == null)
            //        {
            //            return;
            //        }
            //    }
            //    else
            //    {
            //        return;
            //    }
            //}
            var message = MsgBox.ShowQuestion(string.Format(Resources.DO_YOU_WANT_TO_REVERES_BILL), Resources.WARNING);
            if (message == DialogResult.Yes)
            {
                if (new BillLogic().ReverseBill(runId, cycleId))
                {
                    MsgBox.ShowInformation(Resources.SUCCESS, Resources.MS_RUN_BILL_SUCCESS);
                    bind();
                }
            }
            else if (message != DialogResult.No)
            {
                return;
            }
        }

        private void pHeader_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            //if (e.Button == MouseButtons.Right)
            //{
            //    if (btnREVERSE_BILL.Visible)
            //    {
            //        btnREVERSE_BILL.Visible = false;
            //    }
            //    else
            //    {
            //        btnREVERSE_BILL.Visible = true;
            //    }
            //}
        }

        private void cboReport_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            //viewReport();
        }

        private void btnViewReport_Click_1(object sender, EventArgs e)
        {
            try
            {
                viewReport();
            }catch(Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_VIEW_REPORT, Base.Properties.Resources.WARNING);
            }
        }
    }
}
