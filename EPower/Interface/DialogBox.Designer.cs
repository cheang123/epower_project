﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogBox));
            this.lblBOX_CODE = new System.Windows.Forms.Label();
            this.txtCodeBox = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.label9 = new System.Windows.Forms.Label();
            this.lblPOLE = new System.Windows.Forms.Label();
            this.cboPole = new System.Windows.Forms.ComboBox();
            this.cboStatus = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblSTATUS = new System.Windows.Forms.Label();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAddPole = new SoftTech.Component.ExAddItem();
            this.lblBOX_TYPE = new System.Windows.Forms.Label();
            this.cboBoxType = new System.Windows.Forms.ComboBox();
            this.lblEND_USE_DATE = new System.Windows.Forms.Label();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.lblSTART_USE_DATE = new System.Windows.Forms.Label();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.btnBrowse_ = new SoftTech.Component.ExButton();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.btnBrowse_);
            this.content.Controls.Add(this.lblEND_USE_DATE);
            this.content.Controls.Add(this.dtpEndDate);
            this.content.Controls.Add(this.lblSTART_USE_DATE);
            this.content.Controls.Add(this.dtpStartDate);
            this.content.Controls.Add(this.btnAddPole);
            this.content.Controls.Add(this.label2);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.cboStatus);
            this.content.Controls.Add(this.label3);
            this.content.Controls.Add(this.lblSTATUS);
            this.content.Controls.Add(this.cboBoxType);
            this.content.Controls.Add(this.lblBOX_TYPE);
            this.content.Controls.Add(this.cboPole);
            this.content.Controls.Add(this.lblPOLE);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.txtCodeBox);
            this.content.Controls.Add(this.lblBOX_CODE);
            resources.ApplyResources(this.content, "content");
            this.content.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.content_MouseDoubleClick);
            this.content.Controls.SetChildIndex(this.lblBOX_CODE, 0);
            this.content.Controls.SetChildIndex(this.txtCodeBox, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.lblPOLE, 0);
            this.content.Controls.SetChildIndex(this.cboPole, 0);
            this.content.Controls.SetChildIndex(this.lblBOX_TYPE, 0);
            this.content.Controls.SetChildIndex(this.cboBoxType, 0);
            this.content.Controls.SetChildIndex(this.lblSTATUS, 0);
            this.content.Controls.SetChildIndex(this.label3, 0);
            this.content.Controls.SetChildIndex(this.cboStatus, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.label2, 0);
            this.content.Controls.SetChildIndex(this.btnAddPole, 0);
            this.content.Controls.SetChildIndex(this.dtpStartDate, 0);
            this.content.Controls.SetChildIndex(this.lblSTART_USE_DATE, 0);
            this.content.Controls.SetChildIndex(this.dtpEndDate, 0);
            this.content.Controls.SetChildIndex(this.lblEND_USE_DATE, 0);
            this.content.Controls.SetChildIndex(this.btnBrowse_, 0);
            // 
            // lblBOX_CODE
            // 
            resources.ApplyResources(this.lblBOX_CODE, "lblBOX_CODE");
            this.lblBOX_CODE.Name = "lblBOX_CODE";
            // 
            // txtCodeBox
            // 
            resources.ApplyResources(this.txtCodeBox, "txtCodeBox");
            this.txtCodeBox.Name = "txtCodeBox";
            this.txtCodeBox.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // lblPOLE
            // 
            resources.ApplyResources(this.lblPOLE, "lblPOLE");
            this.lblPOLE.Name = "lblPOLE";
            // 
            // cboPole
            // 
            this.cboPole.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPole.FormattingEnabled = true;
            resources.ApplyResources(this.cboPole, "cboPole");
            this.cboPole.Name = "cboPole";
            // 
            // cboStatus
            // 
            this.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboStatus.FormattingEnabled = true;
            resources.ApplyResources(this.cboStatus, "cboStatus");
            this.cboStatus.Name = "cboStatus";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Name = "label3";
            // 
            // lblSTATUS
            // 
            resources.ApplyResources(this.lblSTATUS, "lblSTATUS");
            this.lblSTATUS.Name = "lblSTATUS";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Name = "label2";
            // 
            // btnAddPole
            // 
            this.btnAddPole.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnAddPole, "btnAddPole");
            this.btnAddPole.Name = "btnAddPole";
            this.btnAddPole.AddItem += new System.EventHandler(this.btnAddPole_AddItem);
            // 
            // lblBOX_TYPE
            // 
            resources.ApplyResources(this.lblBOX_TYPE, "lblBOX_TYPE");
            this.lblBOX_TYPE.Name = "lblBOX_TYPE";
            // 
            // cboBoxType
            // 
            this.cboBoxType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBoxType.FormattingEnabled = true;
            resources.ApplyResources(this.cboBoxType, "cboBoxType");
            this.cboBoxType.Name = "cboBoxType";
            // 
            // lblEND_USE_DATE
            // 
            resources.ApplyResources(this.lblEND_USE_DATE, "lblEND_USE_DATE");
            this.lblEND_USE_DATE.Name = "lblEND_USE_DATE";
            // 
            // dtpEndDate
            // 
            this.dtpEndDate.Checked = false;
            resources.ApplyResources(this.dtpEndDate, "dtpEndDate");
            this.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.ShowCheckBox = true;
            this.dtpEndDate.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            this.dtpEndDate.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dtpEndDate_MouseDown);
            // 
            // lblSTART_USE_DATE
            // 
            resources.ApplyResources(this.lblSTART_USE_DATE, "lblSTART_USE_DATE");
            this.lblSTART_USE_DATE.Name = "lblSTART_USE_DATE";
            // 
            // dtpStartDate
            // 
            resources.ApplyResources(this.dtpStartDate, "dtpStartDate");
            this.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // btnBrowse_
            // 
            resources.ApplyResources(this.btnBrowse_, "btnBrowse_");
            this.btnBrowse_.Name = "btnBrowse_";
            this.btnBrowse_.UseVisualStyleBackColor = true;
            this.btnBrowse_.Click += new System.EventHandler(this.btnBrowse__Click);
            // 
            // DialogBox
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogBox";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBox txtCodeBox;
        private Label lblBOX_CODE;
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label label9;
        private Label lblPOLE;
        private ComboBox cboPole;
        private ComboBox cboStatus;
        private Label label3;
        private Label lblSTATUS;
        private ExButton btnCHANGE_LOG;
        private Label label2;
        private ExAddItem btnAddPole;
        private ComboBox cboBoxType;
        private Label lblBOX_TYPE;
        private Label lblEND_USE_DATE;
        private DateTimePicker dtpEndDate;
        private Label lblSTART_USE_DATE;
        private DateTimePicker dtpStartDate;
        private Label label1;
        private ExButton btnBrowse_;
    }
}