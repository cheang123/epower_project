﻿using Newtonsoft.Json;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EPower.Interface
{
    public partial class DialogMeterTamper : ExDialog
    {
        #region Private data
        TBL_USAGE_DEVICE_COLLECTION obj = new TBL_USAGE_DEVICE_COLLECTION();
        #endregion Private data

        #region Constructor
        public DialogMeterTamper(TBL_USAGE_DEVICE_COLLECTION _obj)
        {
            InitializeComponent();
            //Runner.RunNewThread(delegate()
            //{
            //    RegisterMeter.CheckUnregisterMeter();
            //}); 
            //this.bindMeterUnregister();
            obj = _obj;
            bind();
        }
        #endregion Constructor

        #region Method 
        void bind()
        {
            DBDataContext.Db = null;
            List<TMP_IR_USAGE> data = JsonConvert.DeserializeObject<List<TMP_IR_USAGE>>
                (Encoding.Unicode.GetString(obj.DATA.ToArray()), new JsonSerializerSettings()
                {
                    DateFormatString = "dd-MM-yyyy HH:mm:ss",
                    Formatting = Newtonsoft.Json.Formatting.Indented
                });
            var result = from s in data
                         join m in DBDataContext.Db.TBL_METERs on s.METER_CODE equals m.METER_CODE
                         join cm in DBDataContext.Db.TBL_CUSTOMER_METERs on m.METER_ID equals cm.METER_ID
                         join c in DBDataContext.Db.TBL_CUSTOMERs on cm.CUSTOMER_ID equals c.CUSTOMER_ID
                         join a in DBDataContext.Db.TBL_AREAs on c.AREA_ID equals a.AREA_ID
                         join b in DBDataContext.Db.TBL_BOXes on cm.BOX_ID equals b.BOX_ID
                         where s.TAMPER_STATUS != ""
                            && s.PHASE_ID == 3
                         select new
                         {
                             c.CUSTOMER_CODE,
                             FULL_NAME = c.LAST_NAME_KH + " " + c.FIRST_NAME_KH,
                             s.METER_CODE,
                             a.AREA_CODE,
                             b.BOX_CODE,
                             s.TAMPER_STATUS
                         };

            dgv.DataSource = result.ToList();

            this.lblCOUNT_.Text = result.Count().ToString();
        }

        #endregion Method

        #region Event

        private void DialogMeterUnregister_Load(object sender, EventArgs e)
        {
            //   bindMeterUnregister();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            CrystalReportHelper ch = ViewReportMeterUnregister();
            ch.ViewReport("");
        }

        public CrystalReportHelper ViewReportMeterUnregister()
        {
            return new CrystalReportHelper("ReportMeterUnregister.rpt");
        }
        #endregion Event



    }
}
