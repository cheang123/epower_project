﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class DialogAdjustUsageHistory : SoftTech.Component.ExDialog
    {
        TBL_USAGE _objUsage = new TBL_USAGE();
        SoftTech.TBL_USAGE_HISTORY _objAdjust = new SoftTech.TBL_USAGE_HISTORY();
        public DialogAdjustUsageHistory(TBL_USAGE objUsage)
        {
            InitializeComponent();
            objUsage._CopyTo(_objUsage);
            read();
        }
        private void read()
        {
            this.dgv.DataSource = from uh in DBDataContext.Db.TBL_USAGE_HISTORies
                                  join u in DBDataContext.Db.TBL_USAGEs on uh.USAGE_ID equals u.USAGE_ID
                                  where uh.USAGE_ID==_objUsage.USAGE_ID
                                  select new
                                  {
                                      ADJUST_USAGE_ID=uh.USAGE_HISTORY_ID,
                                      USAGE_ID=uh.USAGE_ID,
                                      CREATED_ON=uh.CREATED_ON,
                                      OLD_START_USAGE=uh.OLD_START_USAGE,
                                      OLD_END_USAGE=uh.OLD_END_USAGE,
                                      NEW_START_USAGE=uh.NEW_START_USAGE,
                                      NEW_END_USAGE=uh.NEW_END_USAGE,
                                      CREATE_BY=uh.CREATE_BY
                                  };
        }
        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
      

       
}
