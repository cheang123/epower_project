﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogAdjustmentAmount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogAdjustmentAmount));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.lblSTART_DATE = new System.Windows.Forms.Label();
            this.dtpStartUsage = new System.Windows.Forms.DateTimePicker();
            this.dtpEndUsage = new System.Windows.Forms.DateTimePicker();
            this.lblTOTAL_DUE_AMOUNT = new System.Windows.Forms.Label();
            this.txtTotalAmount = new System.Windows.Forms.TextBox();
            this.txtPayAmount = new System.Windows.Forms.TextBox();
            this.lblADJUST_AMOUNT = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.lblNOTE = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.label1 = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.label4);
            this.content.Controls.Add(this.label3);
            this.content.Controls.Add(this.txtDescription);
            this.content.Controls.Add(this.lblNOTE);
            this.content.Controls.Add(this.txtPayAmount);
            this.content.Controls.Add(this.lblADJUST_AMOUNT);
            this.content.Controls.Add(this.txtTotalAmount);
            this.content.Controls.Add(this.lblTOTAL_DUE_AMOUNT);
            this.content.Controls.Add(this.dtpEndUsage);
            this.content.Controls.Add(this.lblSTART_DATE);
            this.content.Controls.Add(this.dtpStartUsage);
            this.content.Controls.Add(this.panel1);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.dtpStartUsage, 0);
            this.content.Controls.SetChildIndex(this.lblSTART_DATE, 0);
            this.content.Controls.SetChildIndex(this.dtpEndUsage, 0);
            this.content.Controls.SetChildIndex(this.lblTOTAL_DUE_AMOUNT, 0);
            this.content.Controls.SetChildIndex(this.txtTotalAmount, 0);
            this.content.Controls.SetChildIndex(this.lblADJUST_AMOUNT, 0);
            this.content.Controls.SetChildIndex(this.txtPayAmount, 0);
            this.content.Controls.SetChildIndex(this.lblNOTE, 0);
            this.content.Controls.SetChildIndex(this.txtDescription, 0);
            this.content.Controls.SetChildIndex(this.label3, 0);
            this.content.Controls.SetChildIndex(this.label4, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblSTART_DATE
            // 
            resources.ApplyResources(this.lblSTART_DATE, "lblSTART_DATE");
            this.lblSTART_DATE.Name = "lblSTART_DATE";
            // 
            // dtpStartUsage
            // 
            resources.ApplyResources(this.dtpStartUsage, "dtpStartUsage");
            this.dtpStartUsage.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartUsage.Name = "dtpStartUsage";
            this.dtpStartUsage.Enter += new System.EventHandler(this.Enter_EnglishKey);
            // 
            // dtpEndUsage
            // 
            resources.ApplyResources(this.dtpEndUsage, "dtpEndUsage");
            this.dtpEndUsage.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndUsage.Name = "dtpEndUsage";
            this.dtpEndUsage.Enter += new System.EventHandler(this.Enter_EnglishKey);
            // 
            // lblTOTAL_DUE_AMOUNT
            // 
            resources.ApplyResources(this.lblTOTAL_DUE_AMOUNT, "lblTOTAL_DUE_AMOUNT");
            this.lblTOTAL_DUE_AMOUNT.Name = "lblTOTAL_DUE_AMOUNT";
            // 
            // txtTotalAmount
            // 
            resources.ApplyResources(this.txtTotalAmount, "txtTotalAmount");
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.ReadOnly = true;
            this.txtTotalAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumber);
            // 
            // txtPayAmount
            // 
            resources.ApplyResources(this.txtPayAmount, "txtPayAmount");
            this.txtPayAmount.Name = "txtPayAmount";
            this.txtPayAmount.Enter += new System.EventHandler(this.Enter_EnglishKey);
            this.txtPayAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumber);
            // 
            // lblADJUST_AMOUNT
            // 
            resources.ApplyResources(this.lblADJUST_AMOUNT, "lblADJUST_AMOUNT");
            this.lblADJUST_AMOUNT.Name = "lblADJUST_AMOUNT";
            // 
            // txtDescription
            // 
            resources.ApplyResources(this.txtDescription, "txtDescription");
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Enter += new System.EventHandler(this.Enter_KhmerKey);
            // 
            // lblNOTE
            // 
            resources.ApplyResources(this.lblNOTE, "lblNOTE");
            this.lblNOTE.Name = "lblNOTE";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Name = "label3";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Name = "label4";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnCHANGE_LOG_Click);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // DialogAdjustmentAmount
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogAdjustmentAmount";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private DateTimePicker dtpEndUsage;
        private Label lblSTART_DATE;
        private DateTimePicker dtpStartUsage;
        private TextBox txtTotalAmount;
        private Label lblTOTAL_DUE_AMOUNT;
        private TextBox txtPayAmount;
        private Label lblADJUST_AMOUNT;
        private TextBox txtDescription;
        private Label lblNOTE;
        private Label label4;
        private Label label3;
        private ExButton btnCHANGE_LOG;
        private Label label1;
    }
}
