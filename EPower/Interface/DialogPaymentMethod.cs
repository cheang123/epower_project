﻿using DevExpress.XtraEditors.Controls;
using EPower.Base.Helper;
using EPower.Base.Logic;
using EPower.Base.Properties;
using HB01.Domain.Enums;
using HB01.Domain.ListModels;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogPaymentMethod : ExDialog
    {
        #region Data
        GeneralProcess _flag = GeneralProcess.Insert;
        TBL_PAYMENT_METHOD _objOld = new TBL_PAYMENT_METHOD();
        TBL_PAYMENT_METHOD _objNew = new TBL_PAYMENT_METHOD();
        List<Base.Logic.Currency> currencies = new List<Base.Logic.Currency>();
        List<PaymentMethodAccountList> lst = new List<PaymentMethodAccountList>();

        #endregion Data

        #region Constructor

        public DialogPaymentMethod(TBL_PAYMENT_METHOD obj, GeneralProcess flag)
        {
            InitializeComponent();
            this._flag = flag;
            currencies = SettingLogic.Currencies;
            obj._CopyTo(this._objNew);
            obj._CopyTo(this._objOld);
            this.read();
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
            if (flag == GeneralProcess.Delete)
            {
                UIHelper.SetEnabled(this, false);
            }
            this.btnCLOSE.Click += btnClose_Click;
            this.btnOK.Click += btnOK_Click;
            this.btnCHANGE_LOG.Click += btnChangelog_Click;
        }

        #endregion Constructor

        #region Method

        public bool invalid()
        {
            bool val = false;
            this.ClearAllValidation();

            if (this.txtPaymentMethod.Text == "")
            {
                this.txtPaymentMethod.SetValidation(string.Format(Resources.REQUIRED, this.lblNAME.Text));
                this.txtPaymentMethod.Focus();
                val = true;
            }
            if (this.lst.Count > 0 && lst.Where(x => x.AccountId == 0).Any())
            {
                this.dgv.SetValidation(string.Format(Resources.REQUIRED_LOOKUP, Resources.ACCOUNT));
                val = true;
            }

            return val;
        }

        public void read()
        {
            this.txtPaymentMethod.Text = this._objNew.PAYMENT_METHOD_NAME;
            var k = new List<AccountNatures?> { AccountNatures.Asset };
            resAccount.DataSource = AccountChartHelper.GetAccounts(new AccountSearchParam { Natures = new List<AccountNatures> { AccountNatures.Asset } });

            foreach (LookUpColumnInfo item in resAccount.Columns)
            {
                item.Caption = ResourceHelper.Translate(item.Caption);
            }
            lst.Clear();
            if (currencies.Where(x => x.CURRENCY_ID == (int)Currency.KHR).Any())
            {
                lst.Add(new PaymentMethodAccountList { CurrencyId = (int)Currency.KHR, Currency = Resources.ACCOUNT_KHR, AccountId = _objOld.ACCOUNT_ID_KHR });
            }
            if (currencies.Where(x => x.CURRENCY_ID == (int)Currency.USD).Any())
            {
                lst.Add(new PaymentMethodAccountList { CurrencyId = (int)Currency.USD, Currency = Resources.ACCOUNT_USD, AccountId = _objOld.ACCOUNT_ID_USD });
            }
            if (currencies.Where(x => x.CURRENCY_ID == (int)Currency.THB).Any())
            {
                lst.Add(new PaymentMethodAccountList { CurrencyId = (int)Currency.THB, Currency = Resources.ACCOUNT_THB, AccountId = _objOld.ACCOUNT_ID_THB });
            }
            if (currencies.Where(x => x.CURRENCY_ID == (int)Currency.VND).Any())
            {
                lst.Add(new PaymentMethodAccountList { CurrencyId = (int)Currency.VND, Currency = Resources.ACCOUNT_VND, AccountId = _objOld.ACCOUNT_ID_VND });
            }
            if (lst.Count() > 2)
            {
                this.Size = new Size(this.Width, this.Height + ((lst.Count() - 2) * 25));
            }
            this.dgv.DataSource = lst;
        }

        public void write()
        {
            this._objNew.PAYMENT_METHOD_NAME = txtPaymentMethod.Text;
            this._objNew.ACCOUNT_ID_KHR = lst.Where(x => x.CurrencyId == (int)Currency.KHR).FirstOrDefault()?.AccountId ?? 0;
            this._objNew.ACCOUNT_ID_USD = lst.Where(x => x.CurrencyId == (int)Currency.USD).FirstOrDefault()?.AccountId ?? 0;
            this._objNew.ACCOUNT_ID_THB = lst.Where(x => x.CurrencyId == (int)Currency.THB).FirstOrDefault()?.AccountId ?? 0;
            this._objNew.ACCOUNT_ID_VND = lst.Where(x => x.CurrencyId == (int)Currency.VND).FirstOrDefault()?.AccountId ?? 0;
        }

        #endregion Method

        #region Event

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                btnOK.Focus();
                if (this.invalid())
                {
                    return;
                }

                this.write();
                txtPaymentMethod.ClearValidation();
                if (DBDataContext.Db.IsExits(this._objNew, "PAYMENT_METHOD_NAME"))
                {
                    txtPaymentMethod.SetValidation(string.Format(Resources.MS_IS_EXISTS, this.lblNAME.Text));
                    return;
                }

                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (this._flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(this._objNew);
                    }
                    else if (this._flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(this._objOld, this._objNew);
                    }
                    else if (this._flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(this._objNew);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp);
            }
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }

        #endregion Event
    }
    public class PaymentMethodAccountList
    {
        public int CurrencyId { get; set; }
        public string Currency { get; set; }
        public int AccountId { get; set; }
    }
}
