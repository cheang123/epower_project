﻿using System;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;

namespace EPower.Interface
{
    public partial class DialogCapacity: ExDialog
    {
        #region Data
        GeneralProcess _flag = GeneralProcess.Insert;
        TBL_CAPACITY _objOld=new TBL_CAPACITY();
        TBL_CAPACITY _objNew=new TBL_CAPACITY();
        public TBL_CAPACITY Capacity
        {
            get
            {
                return _objNew;
            }
        }

        #endregion Data

        #region Constructor

        public DialogCapacity( TBL_CAPACITY obj, GeneralProcess flag)
        {
            InitializeComponent();
            this._flag = flag;
            
            obj._CopyTo(this._objNew);
            obj._CopyTo(this._objOld);
            this.read(); 
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
            if (flag == GeneralProcess.Delete)
            {
                UIHelper.SetEnabled(this,false);
            }
        }

        #endregion Constructor

        #region Method

        public bool invalid()
        {
            bool result = false;
            
            this.ClearAllValidation();

            if (this.txtCapacity.Text == "")
            {
                this.txtCapacity.SetValidation(string.Format(Resources.REQUIRED, this.lblCAPACITY.Text));
                this.txtCapacity.Focus();
                return true;
            }
            else
            {
                if (!DataHelper.IsNumber(txtCapacity.Text))
                {
                    txtCapacity.SetValidation(Resources.REQUIRED_INPUT_NUMBER);
                    return true;
                }
            }


            return result;
        }

        public void read()
        {
            ///this.txtCapacity.Text =  this._objNew.CAPACITY.ToString("N2");
        }
 
        public void write()
        {
            ///this._objNew.CAPACITY = DataHelper.ParseToDecimal(this.txtCapacity.Text);
        }

        #endregion Method

        #region Event

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.invalid())
                {
                    return;
                }

                this.write();
                txtCapacity.ClearValidation();
                if (DBDataContext.Db.IsExits(this._objNew, "CAPACITY"))
                {
                    txtCapacity.SetValidation(string.Format(Resources.MS_IS_EXISTS, this.lblCAPACITY.Text));
                    return;
                }

                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (this._flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(this._objNew);
                    }
                    else if (this._flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(this._objOld, this._objNew);
                    }
                    else if (this._flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(this._objNew);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp);
            }                                   
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }

        private void txtCAPACITY_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        #endregion Event
    }
}
