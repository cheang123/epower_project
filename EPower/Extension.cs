﻿using System;
using System.Data.Linq;
using SoftTech;
using SoftTech.Component;

namespace EPower
{ 
    public static class Extension
    {
        public static T Refresh<T>(this T obj) where T : class
        { 
            if (obj != null)
            {
                try
                {
                    DBDataContext.Db.Refresh(RefreshMode.OverwriteCurrentValues, obj);
                }
                catch (Exception ex)
                {
                    MsgBox.LogError(ex);
                }
            }
            return obj;
        }
    }
}
