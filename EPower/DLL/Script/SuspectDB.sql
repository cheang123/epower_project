--http://support.powerdnn.com/kb/a398/how-to-repair-a-suspect-database-in-mssql.aspx

EXEC sp_resetstatus Epower;
ALTER DATABASE Epower SET EMERGENCY
DBCC checkdb(Epower)
ALTER DATABASE Epower SET SINGLE_USER WITH ROLLBACK IMMEDIATE
DBCC CheckDB (Epower, REPAIR_ALLOW_DATA_LOSS)
ALTER DATABASE Epower SET MULTI_USER