﻿using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using EPower.Properties;
using SoftTech.Helper;

namespace EPower.Logic
{
    class Metrologic
    {
        #region Enums
        public enum ConnectMode
        {
            RS232_or_IrDA = 1,
            IR_Cradle,
            Modem_TAPI,
            Modem_COM
        }
        public enum BaudRate
        {
            bps_115200 = 1,
            bps_57600,
            bps38400,
            bps_19200,
            bps_9600,
        }
        public enum SaveMode
        {
            Overwrite = 1,
            Append,
            Output_to_keyboard,
            Auto,
            Auto_append
        }
        #endregion

        #region Variables
        string _execData_Read = @"C:\Program Files\Honeywell\Optimus Software\Communication\Data_Read.exe";
        string _deviceID = string.Empty;
        string _pathBackup = string.Empty;
        bool _hasError = false; 
        string _message = string.Empty; 

        //param 1
        string _fileName = string.Empty;
        //param 2
        ConnectMode _connMode = ConnectMode.IR_Cradle;
        //param 3
        int _portNumber = 1;
        //param 4
        int _bps = 115200;
        //param 5
        SaveMode _svMode = SaveMode.Overwrite;
        //param 6 Add Return Character to each record
        bool _returnCharacter = false;
        //param 7 Add Line-Feed character to each record
        bool _lineFeed = false;
        //param 8 Show Message in case of error
        bool _showError = false;
        //param 9 View Received data
        bool _viewData = false;
        //param 10 always show this dialog box
        bool _showDialog = false;
        //param 11 Keep for received data automatic
        bool _keepOnline = false;
        //param 12 Polling time
        int _pollingTime = 2; 
        #endregion                                   
        
        #region Properties
        /// <summary>
        /// Data_Reader Application for Optimus S 5500
        /// </summary>
        public string ExecuteDataRead
        {
            get { return _execData_Read; }
            set { _execData_Read = value; }
        }

        /// <summary>
        /// Device Serial Number
        /// </summary>
        public string DeviceID
        {
            get { return _deviceID; }
            set { _deviceID = value; }
        }

        /// <summary>
        /// Parth Backup when received file.
        /// </summary>
        public string PathBackup
        {
            get { return _pathBackup; }
            set { _pathBackup = value; }
        }

        /// <summary>
        /// Get error when connect to data collector
        /// </summary>
        public bool HasError
        {
            get { return _hasError; }
        }

        /// <summary>
        /// Get message error.
        /// </summary>
        public string MessageError
        {
            get { return _message; }
        }

        /// <summary>
        /// File name save in local HDD.
        /// </summary>
        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }

        /// <summary>
        /// Connection mode to Data Collector
        /// </summary>
        internal ConnectMode ConnectionMode
        {
            get { return _connMode; }
            set { _connMode = value; }
        }

        /// <summary>
        /// COM Port Number
        /// </summary>
        public int PortNumber
        {
            get { return _portNumber; }
            set { _portNumber = value; }
        }

        /// <summary>
        /// Transmit speed.
        /// </summary>
        internal int BaudRateTransmit
        {
            get { return _bps; }
            set { _bps = value; }
        }

        /// <summary>
        /// Save file mode to local HDD.
        /// </summary>
        internal SaveMode SaveMethod
        {
            get { return _svMode; }
            set { _svMode = value; }
        }

        /// <summary>
        /// Add Return Character to each record
        /// </summary>
        public bool AddReturnCharacter
        {
            get { return _returnCharacter; }
            set { _returnCharacter = value; }
        }

        /// <summary>
        /// Add Line-Feed character to each record
        /// </summary>
        public bool AddLineFeed
        {
            get { return _lineFeed; }
            set { _lineFeed = value; }
        }

        /// <summary>
        /// Show Message in case of error
        /// </summary>
        public bool ShowError
        {
            get { return _showError; }
            set { _showError = value; }
        }

        /// <summary>
        /// View Received data
        /// </summary>
        public bool ViewData
        {
            get { return _viewData; }
            set { _viewData = value; }
        }

        /// <summary>
        /// always show this dialog box
        /// </summary>
        public bool ShowDialog
        {
            get { return _showDialog; }
            set { _showDialog = value; }
        }

        /// <summary>
        /// Keep for received data automatic
        /// </summary>
        public bool KeepOnline
        {
            get { return _keepOnline; }
            set { _keepOnline = value; }
        }

        /// <summary>
        /// Polling time
        /// </summary>
        public int PollingTime
        {
            get { return _pollingTime; }
            set { _pollingTime = value; }
        }
        #endregion 

        #region Constructor
        public Metrologic()
        {

        }

        public Metrologic(
            string fileName
            ,ConnectMode connMode
            ,int portNumber
            ,int bps
            ,SaveMode svMode
            ,bool returnChar
            ,bool addLineFeed
            ,bool ShowError
            ,bool viewData
            ,bool ShowDialog
            ,bool keepOnline
            ,int pollingTime)
        {
            _fileName = fileName;
            _connMode = connMode;
            _portNumber = portNumber;
            _bps = bps;
            _svMode = svMode;
            _returnCharacter = returnChar;
            _lineFeed = addLineFeed;
            _showError = ShowError;
            _viewData = viewData;
            _showDialog = ShowDialog;
            _keepOnline = keepOnline;
            _pollingTime = pollingTime;            
        }
	    #endregion

        #region Method
        public FileInfo ReceivedData()
        {
            //Clear tmp file.
            File.Create(_fileName).Close();
            Process psi = new Process();
            psi.StartInfo = new ProcessStartInfo(_execData_Read, GetArguments());
            psi.Start();
            psi.WaitForExit();
            ReceivedBackup();
            return new FileInfo(_fileName);
        }

        public void ReceivedBackup()
        {
            FileInfo fi = new FileInfo(   _fileName);
            if (!fi.Exists)
            {
                return;
            }             
            
            if (!Directory.Exists(_pathBackup))
            {
                _pathBackup = fi.Directory.FullName;
            }

            string fileBakcup = string.Concat("Backup",_deviceID
                                , "-", fi.Name.Replace(fi.Extension, string.Empty)
                                , "-", DateTime.Now.ToString("yyyyMMddhhmmss")
                                , fi.Extension);
            
            string fBakcup=Path.Combine(_pathBackup, fileBakcup);
            File.Copy(_fileName,fBakcup);
        }

        public DataTable ReceivedData(char splitColumn) 
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("METER_CODE"));
            dt.Columns.Add(new DataColumn("TOTAL_POWER",typeof(decimal)));
            dt.Columns.Add(new DataColumn("IS_DIGITAL",typeof(bool)));
            FileInfo fi = ReceivedData();
            int line=0;
            using (StreamReader sr=new StreamReader(fi.FullName))
            {
                while (!sr.EndOfStream)
                {
                    string read = sr.ReadLine(); 
                    line++;

                    string[] data = read.Split(splitColumn);

                    //data have problem
                    if (data.Length!=2)
                    {
                        _message = string.Format(Resources.METROLOGIC_DATA_ERROR, line);
                        return new DataTable();
                    }

                    DataRow dr = dt.NewRow();
                    dr[0] = data[0];
                    dr[1] = Math.Abs(DataHelper.ParseToDecimal(data[1]));  
                    dt.Rows.Add(dr);
                }
                sr.Close();
            } 

            return dt;
        }

        internal string GetArguments()
        {
            return string.Concat(
                _fileName,","
                ,(int)_connMode,","
                ,_portNumber,","
                ,(int)_bps,","
                ,(int)_svMode,","
                ,Convert.ToInt16(_returnCharacter),","
                ,Convert.ToInt16(_lineFeed),","
                ,Convert.ToInt16(_showDialog),","
                ,Convert.ToInt16(_viewData),","
                ,Convert.ToInt16(_showDialog),","
                ,Convert.ToInt16(_keepOnline),","
                ,_pollingTime);
        }
	    #endregion 
    }
}
