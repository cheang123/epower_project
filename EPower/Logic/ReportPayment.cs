﻿using EPower.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EPower.Logic
{
    public class ReportPaymentModel
    {
        public string CUSTOMER_CODE { set; get; }
        public string CUSTOMER_NAME { set; get; }
        public string PHONE_NUMBER { set; get; }
        public string CUSTOMER_TYPE { set; get; }
        public string ADDRESS_CUSTOMER { set; get; }
        public string METER_CODE { set; get; }
        public string AMPARE_NAME { set; get; }
        public string AREA_NAME { set; get; }
        public string POLE_NAME { set; get; }
        public string BOX_CODE { set; get; }
        public DateTime CREATE_ON { set; get; }
        public DateTime INVOICE_DATE { set; get; }
       
        public DateTime PAID_DATE { set; get; }

        public DateTime BANK_CUT_OFF_DATE { set; get; } 

        public DateTime EXACT_PAID_DATE { get { return PAID_DATE; } }
        public DateTime SHORT_PAID_DATE { get { return PAID_DATE.Date; } }

        public DateTime DUE_DATE { set; get; }
        public string REFERENCE { set; get; }
        public string DESCRIPTION { set; get; } 
        public string PAYMENT_METHOD { set; get; }
        public string PAY_AT_BANK { set; get; }
        public string RECEIVE_BY { set; get; }
        public decimal TOTAL_PAY { set; get; }
        public decimal TOTAL_VOID { set; get; }
        public decimal TOTAL_PAID { set; get; }
        public string STATUS { set; get; }
        public string NOTE { set; get; }
        public string CURRENCY { get; set; }

        public string PAYMENT_TYPE_GROUP
        {
            get
            {
                return (PAY_AT_BANK == "-") ? Resources.PAY_AT_COUNTER : Resources.PAY_AT_BANK;
            }
        }
    }
}
