﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EPower.Logic
{
    public static class JsonHelper
    {
        static JsonSerializerSettings _JSetting =  new JsonSerializerSettings()
        {
            DateFormatString = "dd-MM-yyyy HH:mm:ss",
                    Formatting = Formatting.Indented
        };

        public static string _ToJson<T>(this IEnumerable<T> obj )
        {
            return JsonConvert.SerializeObject(obj, _JSetting);
        }

        public static T _ToObject<T>(this string json)
        {
            return JsonConvert.DeserializeObject<T>(json, _JSetting);
        }
         

    }
}
