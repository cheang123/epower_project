﻿using EPower.Base.Logic;
using EPower.CRMServiceReference;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using Version = EPower.Update.Version;

namespace EPower.Logic
{
    public class CRMService
    {
        // singleton locker
        static CRMService instance;
        public static CRMService Intance
        {
            get
            {
                if (instance == null) instance = new CRMService();
                return instance;
            }
        }

        private DBDataContext db = new DBDataContext(Method.GetConnectionString(Settings.Default.CONNECTION));
        private ServiceClient client = null;
        public ServiceClient Client
        {
            get
            {
                if (client == null || client.State == CommunicationState.Faulted)
                {
                    string url = Method.Utilities[Utility.SERVICE_URL];
                    Uri address = new Uri(url);
                    var endpoint = new EndpointAddress(address.AbsoluteUri);
                    var binding = new WSHttpBinding()
                    {
                        CloseTimeout = TimeSpan.FromMinutes(1),
                        OpenTimeout = TimeSpan.FromMinutes(1),
                        ReceiveTimeout = TimeSpan.FromMinutes(5d),
                        SendTimeout = TimeSpan.FromMinutes(5d),
                        MaxBufferPoolSize = 2147483647,
                        MaxReceivedMessageSize = 2147483647,
                        BypassProxyOnLocal = false,
                        HostNameComparisonMode = HostNameComparisonMode.StrongWildcard,
                        ReaderQuotas = new XmlDictionaryReaderQuotas()
                        {
                            MaxArrayLength = 2147483647,
                            MaxBytesPerRead = 2147483647,
                            MaxStringContentLength = 2147483647,
                            MaxNameTableCharCount = 2147483647
                        },
                        AllowCookies = false
                    };
                    binding.Security.Mode = SecurityMode.Message;
                    binding.Security.Message.ClientCredentialType = MessageCredentialType.Windows;
                    client = new ServiceClient(binding, endpoint);
                    client.ClientCredentials.Windows.AllowedImpersonationLevel = TokenImpersonationLevel.Impersonation;
                    if (!url.Contains("localhost"))
                    {
                        client.ClientCredentials.Windows.ClientCredential = new NetworkCredential("wcf", "wcf");
                    }
                }
                return client;
            }
        }

        private string GetError()
        {
            string error = "";
            try
            {
                error = Client.GetLastError();
            }
            catch (Exception)
            {
            }
            return error;
        }

        public TBL_CLIENT_REQUEST AuthenticateRequest(ClientRequestType requestType)
        {
            TBL_CONFIG objConf = db.TBL_CONFIGs.FirstOrDefault();
            return Client.Authenticate(new TBL_CLIENT_REQUEST()
            {
                REQUEST_DATE = DateTime.Now,
                EPOWER_VERSION = Version.ClientVersion,
                ACCOUNT_NO = objConf.AREA_CODE,
                CONF_CPU = CheckSum.GetProcessorId(),
                CONF_ACTIVATE_DATE = objConf.ACTIVATE_DATE,
                CONF_AREA_CODE = objConf.AREA_CODE,
                CONF_IS_LOCK_METER = objConf.IS_LOCK_METER,
                CONF_PROCESSOR_ID = objConf.PROCESSOR_ID,
                CONF_TOTAL_CUSTOMER = objConf.TOTAL_CUTOMER,
                CONF_TOTAL_MONTH = objConf.TOTAL_MONTH,
                TOTAL_ACTIVE_CUSTOMER = db.TBL_CUSTOMERs.Count(x => x.STATUS_ID == (int)CustomerStatus.Active),
                REQUEST_TYPE_ID = (int)requestType
            }, requestType);
        }
        public bool RegisterMeter()
        {
            var meters = db.TBL_METERs.Where(x => x.REGISTER_CODE == "");
            return RegisterMeter(meters.ToList());
        }

        public bool RegisterMeter(List<TBL_METER> lstRegister)
        {

            var sb = new StringBuilder();
            foreach (var m in lstRegister)
            {
                sb.AppendLine(string.Format("{0},{1},{2},{3:yyyy-M-d}", m.METER_ID, m.METER_CODE, m.IS_DIGITAL ? 1 : 0, m.CREATE_ON));
            }

            var meters = sb.ToString();

            // authenticate.
            try
            {
                var objRequest = AuthenticateRequest(ClientRequestType.MeterRegister);
                // if server is not response or ristrick access
                // exit this method.
                if (objRequest == null)
                {
                    throw new MsgBox.MessageException(Resources.MS_METER_REGISTER_FAIL + "\n\r" + this.GetError());
                }
            }
            catch(Exception ex)
            {
                throw new MsgBox.MessageException(Resources.MS_METER_REGISTER_FAIL + "\n\r" + this.GetError());
            }
            var result = Client.RegisterMeters(meters);
            var lines = result.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            var q = lines.Select(line => new
            {
                METER_CODE = line.Split(',')[0],
                IS_DIGITAL = line.Split(',')[1] == "1" ? true : false,
                REGISTER_CODE = line.Split(',')[2]
            });
            var dt = q._ToDataTable();
            db.ExecuteCommand(@"
   IF OBJECT_ID('TMP_METER_REGISTER') IS NULL
   CREATE TABLE TMP_METER_REGISTER(
		METER_CODE NVARCHAR(50),
		IS_DIGITAL BIT,
		REGISTER_CODE NVARCHAR(100)
	)");
            db.ExecuteCommand("TRUNCATE TABLE TMP_METER_REGISTER");

            db.BulkCopy(dt, "TMP_METER_REGISTER");
            db.ExecuteCommand(@"
UPDATE TBL_METER
SET IS_DIGITAL=CASE WHEN t.REGISTER_CODE='' THEN m.IS_DIGITAL ELSE t.IS_DIGITAL END,
	REGISTER_CODE=t.REGISTER_CODE
FROM TBL_METER m
INNER JOIN TMP_METER_REGISTER t ON m.METER_CODE=t.METER_CODE;");

            return true;
        }

        public List<TBL_ACCOUNT_METER> GetNewMeters()
        {
            // authenticate.
            TBL_CLIENT_REQUEST objRequest = null;
            int chunksize = 10;
            int newMeter = 0;
            Runner.RunNewThread(delegate ()
            {
                // authenticate
                objRequest = AuthenticateRequest(ClientRequestType.MeterRegister);
                // get chunksize
                chunksize = Client.GetChunkSize();
                // get new meter
                newMeter = Client.GetNewMetersCount();

            }, Resources.MS_INITIALIZING_PROGRESS);
            if (Runner.Instance.Error != null)
            {
                return null;
            }

            // if server is not response or ristrick access
            // exit this method.
            if (objRequest == null)
            {
                MsgBox.ShowInformation(Resources.MS_METER_REGISTER_FAIL + "\n\r" + this.GetError());
                return new List<TBL_ACCOUNT_METER>();
            }

            int process = 0;
            List<TBL_ACCOUNT_METER> tmp = new List<TBL_ACCOUNT_METER>();
            while (process < newMeter)
            {
                Runner.RunNewThread(delegate ()
                {
                    var meters = Client.GetNewMeters();
                    tmp.AddRange(meters);
                    process += meters.Length;
                }, string.Format("ដំណើរការ : {0}/{1} ...", process, newMeter));
                if (Runner.Instance.Error != null)
                {
                    return null;
                }
            }
            return tmp;
        }

        public string GetNextVersion(string currentVersion)
        {
            try
            {
                var objRequest = AuthenticateRequest(ClientRequestType.UpdateVersion);
                return Client.GetNextVersion(currentVersion);
            }
            catch (Exception ex)
            {
                return Resources.CANNOT_CONECT_TO_SERVER; //ex.ToString();
                //return ex.ToString();
            }
        }

        public bool TrackMeter()
        {
            // regenerate CUSTOMER METER DATA.
            db.ExecuteQuery<int>(@"
INSERT INTO TBL_CUSTOMER_METER_VIEW
SELECT	cm.CUS_METER_ID, 
		c.CUSTOMER_ID,
		c.CUSTOMER_CODE,
		CUSTOMER_NAME=LAST_NAME_KH+' '+FIRST_NAME_KH, 
		AMPARE_NAME,
		PHASE_NAME,
		m.METER_ID,
		METER_CODE,
		USED_DATE,
		x.PRICE,
		IS_SEALED = CONVERT(BIT,0)
FROM TBL_CUSTOMER c
INNER JOIN TBL_CUSTOMER_METER cm ON cm.CUSTOMER_ID = c.CUSTOMER_ID
INNER JOIN TBL_AMPARE a ON a.AMPARE_ID = c.AMP_ID 
INNER JOIN TBL_PHASE p ON p.PHASE_ID = c.PHASE_ID
INNER JOIN TBL_METER m ON m.METER_ID = cm.METER_ID 
OUTER APPLY(SELECT TOP 1 PRICE  FROM TBL_PRICE_DETAIL WHERE PRICE_ID = c.PRICE_ID) x
WHERE CUS_METER_ID NOT IN (SELECT CUS_METER_ID FROM TBL_CUSTOMER_METER_VIEW);");

            // if no data to track to server
            var lst = db.TBL_CUSTOMER_METER_VIEWs.Where(x => !x.IS_COMPLETED).ToList();
            if (lst.Count == 0)
            {
                return false;
            }

            // authenticate.
            TBL_CLIENT_REQUEST objRequest = null;
            int chunksize = 10;


            // authenticate
            objRequest = AuthenticateRequest(ClientRequestType.TrackMeter);
            // get chunksize
            chunksize = Client.GetChunkSize();

            var process = 0;

            var tmp = new List<TBL_EPOWER_CUSTOMER>();
            foreach (var c in lst)
            {
                process++;
                tmp.Add(new TBL_EPOWER_CUSTOMER()
                {
                    CUS_METER_ID = c.CUS_METER_ID,
                    CUSTOMER_ID = c.CUS_METER_ID,
                    CUSTOMER_CODE = c.CUSTOMER_CODE,
                    CUSTOMER_NAME = c.CUSTOMER_NAME,
                    METER_ID = c.METER_ID,
                    METER_CODE = c.METER_CODE,
                    AMPARE_NAME = c.AMPARE_NAME,
                    PHASE_NAME = c.PHASE_NAME,
                    PRICE = c.PRICE,
                    USED_DATE = c.USED_DATE
                });
                if (process % chunksize == 0 || process >= lst.Count)
                {
                    var meters = Client.TrackMeter(tmp.ToArray());
                    if (meters != null)
                    {
                        foreach (var cv in tmp)
                        {
                            var objCustomerMeter = db.TBL_CUSTOMER_METER_VIEWs.FirstOrDefault(x => x.CUS_METER_ID == cv.CUS_METER_ID);
                            objCustomerMeter.IS_COMPLETED = true;
                        }
                        db.SubmitChanges();
                    }
                    tmp.Clear();
                }
            }
            return true;
        }

        public static Thread MeterTrackingThread = null;

        public static void StartTrackMeterInNewThread()
        {
            // track meter information.
            MeterTrackingThread = new Thread(delegate ()
            {
                while (true)
                {
                    // sleep 30 seconds to check connection
                    Thread.Sleep(30 * 1000);
                    try
                    {
                        if (TCPHelper.IsInternetConnected())
                        {
                            Logic.RegisterMeter.CheckUnregisterMeter(new DBDataContext(Method.GetConnectionString(Settings.Default.CONNECTION)));
                            Intance.RegisterMeter();
                            //Intance.TrackMeter();
                        }
                    }
                    catch (Exception ex)
                    {
                        MsgBox.LogError(ex);
                    }
                    // sleep 14 minutes to check connection
                    Thread.Sleep(30 * 60 * 1000);
                }
            });
            MeterTrackingThread.Start();
            Application.ApplicationExit += new EventHandler(Application_ApplicationExit);
        }


        static void Application_ApplicationExit(object sender, EventArgs e)
        {
            try
            {
                MeterTrackingThread.Abort();
            }
            catch (Exception) { }
        }

    }
}
