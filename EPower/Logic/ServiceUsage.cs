﻿using EPower.Base.Logic;
using EPower.EPowerServiceUsage;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.ServiceModel;
using System.Transactions;
using System.Windows.Forms;
using System.Xml;
using TBL_USAGE = EPower.EPowerServiceUsage.TBL_USAGE;

namespace EPower.Logic
{
    class ServiceUsage
    {
        static ServiceUsage instance;
        public static ServiceUsage Intance
        {
            get
            {
                if (instance == null) instance = new ServiceUsage();
                return instance;
            }
        }

        private DesktopServiceClient client = null;
        public DesktopServiceClient Client
        {
            get
            {
                if (client == null || client.State == CommunicationState.Faulted)
                {
                    string url = Method.Utilities[Utility.USAGE_SERVICE_URL];
                    Uri address = new Uri(url);
                    var endpoint = new EndpointAddress(address.AbsoluteUri);
                    var binding = new WSHttpBinding()
                    {
                        CloseTimeout = TimeSpan.FromMinutes(1),
                        OpenTimeout = TimeSpan.FromMinutes(1),
                        ReceiveTimeout = TimeSpan.FromMinutes(5d),
                        SendTimeout = TimeSpan.FromMinutes(5d),
                        MaxBufferPoolSize = 2147483647,
                        MaxReceivedMessageSize = 2147483647,
                        BypassProxyOnLocal = false,
                        HostNameComparisonMode = HostNameComparisonMode.StrongWildcard,
                        ReaderQuotas = new XmlDictionaryReaderQuotas()
                        {
                            MaxArrayLength = 2147483647,
                            MaxBytesPerRead = 2147483647,
                            MaxStringContentLength = 2147483647,
                            MaxNameTableCharCount = 2147483647
                        },
                        AllowCookies = false
                    };

                    client = new DesktopServiceClient(binding, endpoint);
                    binding.Security.Mode = SecurityMode.Message;
                    binding.Security.Message.ClientCredentialType = MessageCredentialType.Windows;
                    if (!url.Contains("localhost"))
                    {
                        binding.Security.Mode = SecurityMode.Message;
                        binding.Security.Message.ClientCredentialType = MessageCredentialType.Windows;
                        client.ClientCredentials.Windows.AllowedImpersonationLevel = TokenImpersonationLevel.Impersonation;
                        client.ClientCredentials.Windows.ClientCredential = new NetworkCredential("AMR", "@168$Service");
                    }
                }
                return client;
            }
        }

        private string _LicenseNo = DBDataContext.Db.TBL_CONFIGs.FirstOrDefault().AREA_CODE;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mCycles"></param>
        /// <param name="mAreas"></param>
        /// <returns>Total customers to upload usage</returns>
        public int UploadUsage(List<TBL_BILLING_CYCLE> mCycles, List<TBL_AREA> mAreas)
        {
            try
            {
                List<TBL_USAGE> usages = new List<TBL_USAGE>();

                var cycles = ";" + string.Join(";", mCycles.Select(x => x.CYCLE_ID.ToString()).ToArray()) + ";";
                var areas = ";" + string.Join(";", mAreas.Select(x => x.AREA_ID.ToString()).ToArray()) + ";";
                var qUsages = new IRDevice.IRDevice().GetMonthlyUsages(mCycles, mAreas);

                foreach (var row in qUsages)
                {
                    TBL_USAGE objNew = new TBL_USAGE();
                    objNew.CUSTOMER_CODE = row.CUSTOMER_CODE;
                    objNew.METER_CODE = row.METER_CODE;
                    objNew.FULL_NAME = row.FULL_NAME;
                    objNew.AREA_CODE = row.AREA_CODE;
                    objNew.POLE_CODE = row.POLE_CODE;
                    objNew.BOX_CODE = row.BOX_CODE;
                    objNew.IS_DIGITAL = row.IS_DIGITAL;
                    objNew.MULTIPLIER = row.MULTIPLIER;
                    objNew.START_USAGE = row.START_USAGE;
                    objNew.END_USAGE = row.END_USAGE;
                    objNew.IS_METER_RENEW_CYCLE = row.IS_METER_RENEW_CYCLE;
                    objNew.LAST_TOTAL_USAGE = row.LAST_TOTAL_USAGE;
                    objNew.DATE_COLLECT = UIHelper._DefaultDate;
                    objNew.IS_METER_REGISTER = row.IS_METER_REGISTER;
                    objNew.USAGE_HISTORY = row.USAGE_HISTORY;
                    objNew.IS_COLLECT_USAGE = false;
                    usages.Add(objNew);
                }


                var logId = Guid.NewGuid();
                var chunkSize = 128;
                for (int i = 0; i < usages.Count; i += chunkSize)
                {
                    var tmp = usages.Skip(i).Take(chunkSize);
                    Client.UploadUsage(_LicenseNo, logId, tmp.ToArray());

                    Runner.Instance.Text = string.Format(Resources.MS_VALUE_SEND_DATA_USAGE_TO_SERVER, i * 100.00 / usages.Count);
                }

                AuditTrial.Logger.Log(Resources.UPLOAD_USAGE, SoftTech.AuditTrialGroup.SendUsage);
                return usages.Count;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        public int DownloadUsage()
        {
            try
            {
                var logId = Guid.NewGuid();
                var downloaded = 0;

                while (true)
                {
                    var usages = Client.DownloadUsage(_LicenseNo, logId, downloaded);
                    var n = usages.Count();
                    if (n == 0 && downloaded == 0)
                    {
                        throw new MsgBox.MessageException(Resources.MS_DEVICE_NO_CUSTOMER_COLLECT_USAGE);
                    }

                    if (n == 0)
                    {
                        break;
                    }

                    downloaded += usages.Count();

                    Runner.Instance.Text = string.Format(Resources.MS_SAVING_DATA, downloaded);
                    Application.DoEvents();
                    DBDataContext.Db.ExecuteCommand("TRUNCATE TABLE TMP_IR_USAGE");
                    string str = Runner.Instance.Text;

                    using (var tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                    {
                        foreach (var row in usages)
                        {
                            TMP_IR_USAGE obj = new TMP_IR_USAGE();
                            obj.BOX_CODE = row.BOX_CODE;
                            obj.END_USAGE = row.END_USAGE;
                            obj.IS_DIGITAL = row.IS_DIGITAL;
                            obj.IS_METER_RENEW_CYCLE = row.IS_METER_RENEW_CYCLE;
                            obj.IS_READ_IR = row.IS_READ_IR;
                            obj.METER_CODE = row.METER_CODE;
                            obj.REGISTER_CODE = Method.GetMETER_REGCODE(row.METER_CODE, row.IS_DIGITAL);
                            obj.TAMPER_STATUS = "00";
                            DBDataContext.Db.TMP_IR_USAGEs.InsertOnSubmit(obj);
                        }
                        DBDataContext.Db.SubmitChanges();
                        new IRDevice.IRDevice().RunIRUsage(-1, -1);
                        tran.Complete();
                    }
                    DBDataContext.Db = null;
                }
                AuditTrial.Logger.Log(Resources.DOWNLOAD_USAGE, SoftTech.AuditTrialGroup.ReceiveUsage);
                return downloaded;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }
    }
}
