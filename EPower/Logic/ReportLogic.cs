﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EPower.Base.Helper;
using EPower.Base.Logic;
using EPower.Base.Models;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;

namespace EPower.Logic
{
    public class ReportLogic
    {
        public static ReportLogic Instance = new ReportLogic();
        private string ConnectionString = Method.GetConnectionString(Settings.Default.CONNECTION);
        private string loginName = SoftTech.Security.Logic.Login.CurrentLogin.LOGIN_NAME; 
        public DataTable ReportGroupByCustomerType(GroupCustomerType model)
        {
            var dt = new DataTable();
            using (var cnn = new SqlConnection(ConnectionString))
            {
                if (cnn.State == ConnectionState.Closed)
                {
                    cnn.Open();
                }

                using (var cmd = new SqlCommand("dbo.REPORT_SOLD_BY_CUSTOMER_TYPE", cnn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;
                    //cmd.Parameters.AddWithValue("@AREA_ID", model.AREA_ID);
                    cmd.Parameters.AddWithValue("@FromDate", model.FromDate);
                    cmd.Parameters.AddWithValue("@ToDate", model.ToDate);
                    //cmd.Parameters.AddWithValue("@BILLING_CYCLE_ID", model.CYCLE_ID);
                    //cmd.Parameters.AddWithValue("@CURRENCY_ID", model.CURRENCY_ID);
                    //cmd.Parameters.AddWithValue("@CUSTOMER_TYPE_ID", model.CUSTOMER_TYPE_ID);
                    //cmd.Parameters.AddWithValue("@CUSTOMER_CONNETION_TYPE_ID", model.CUSTOMER_CONNETION_TYPE_ID);
                    Runner.RunNewThread(() => dt.Load(cmd.ExecuteReader()));
                }
            }
            return dt;
        }

        public List<ReportInvoice> GetInvoiceToPrint(int printInvoiceId)
        {
            string ConnectionString = Method.GetConnectionString(Settings.Default.CONNECTION);
            var dt = new DataTable();
            using (var cnn = new SqlConnection(ConnectionString))
            {
                if (cnn.State == ConnectionState.Closed)
                {
                    cnn.Open();
                }
                using (var cmd = new SqlCommand("dbo.REPORT_INVOICE", cnn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;
                    cmd.Parameters.AddWithValue("@PRINT_INVOICE_ID", printInvoiceId);
                    Runner.RunNewThread(() => dt.Load(cmd.ExecuteReader()));
                }
            }
            var result = dt.ToDataListModel<ReportInvoice>();
            return result;
        }

        public List<ReportInvoiceHistory> GetHistories(int printInvoiceId)
        {
            string ConnectionString = Method.GetConnectionString(Settings.Default.CONNECTION);
            var dt = new DataTable();
            using (var cnn = new SqlConnection(ConnectionString))
            {
                if (cnn.State == ConnectionState.Closed)
                {
                    cnn.Open();
                }
                using (var cmd = new SqlCommand("dbo.REPORT_INVOICE_HISTORY_ALL", cnn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;
                    cmd.Parameters.AddWithValue("@PRINT_INVOICE_ID", printInvoiceId);
                    Runner.RunNewThread(() => dt.Load(cmd.ExecuteReader()));
                }
            }
            var result = dt.ToDataListModel<ReportInvoiceHistory>();
            return result;
        }
        public List<ReportInvoiceUsage> GetInvoiceUsage(int printInvoiceId)
        {
            string ConnectionString = Method.GetConnectionString(Settings.Default.CONNECTION);
            var dt = new DataTable();
            using (var cnn = new SqlConnection(ConnectionString))
            {
                if (cnn.State == ConnectionState.Closed)
                {
                    cnn.Open();
                }
                using (var cmd = new SqlCommand("dbo.REPORT_INVOICE_USAGE", cnn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;
                    cmd.Parameters.AddWithValue("@PRINT_INVOICE_ID", printInvoiceId);
                    Runner.RunNewThread(() => dt.Load(cmd.ExecuteReader()));
                }
            }
            var result = dt.ToDataListModel<ReportInvoiceUsage>();
            return result;
        }

        public ReportDocument GetReportNewDocument(string reportName, string printerName, int printInvoiceId)
        {
            var company = DBDataContext.Db.TBL_COMPANies.FirstOrDefault();
            var invoices = Logic.ReportLogic.Instance.GetInvoiceToPrint(printInvoiceId).AsEnumerable<ReportInvoice>();
            var histories = Logic.ReportLogic.Instance.GetHistories(printInvoiceId).AsEnumerable<ReportInvoiceHistory>();
            var usages = Logic.ReportLogic.Instance.GetInvoiceUsage(printInvoiceId).AsEnumerable<ReportInvoiceUsage>();

            var data = new Dictionary<string, IEnumerable>() { };
            data.Add("report_invoice", invoices);

            var subData = new Dictionary<string, Dictionary<string, IEnumerable>>();
            var sreportInvoiceUsage = new Dictionary<string, IEnumerable>();
            if (usages.Any())
            {
                sreportInvoiceUsage.Add("report_invoice_usage", usages);
                subData.Add("INVOICE_USAGE", sreportInvoiceUsage);
            }

            if (histories.Any())
            {
                var sreportHistory = new Dictionary<string, IEnumerable>();
                sreportHistory.Add("report_invoice_history", histories);
                subData.Add("REPORT_INVOICE_HISTORY", sreportHistory);
            }

            var param = new Dictionary<string, object>()
            {
                {"@COMPANY_NAME", company.COMPANY_NAME },
                {"@COMPANY_ADDRESS", company.ADDRESS },
                {"@LOGIN_NAME", loginName == null? "": loginName}
            };
            var report = ReportHelper.Instance.Load(reportName, data, subData, param);
            report.SummaryInfo.ReportTitle = reportName.Replace(".rpt","");
            return report;
        }

        public ReportDocument GetViewNewInvoice(string reportName, int printInvoiceId)
        {
            return GetReportNewDocument(reportName,"", printInvoiceId);
        }

        public ReportDocument GetPrintNewInvoice(string reportName, string printerName, int printInvoiceId)
        {
            var report = GetReportNewDocument(reportName, printerName, printInvoiceId);
            report.PrintOptions.PrinterName = printerName;
            report.PrintToPrinter(1, true, 1, 1000000);
            return report;
        }

        public ReportDocument GetReportOldDocument(string reportName, string printerName, int printInvoiceId)
        {
            ReportDocument report = new ReportDocument();
            var objCompany = DBDataContext.Db.TBL_COMPANies.FirstOrDefault();
            reportName = reportName.EndsWith(".rpt") ? reportName : reportName + ".rpt";
            var path = CrystalReportHelper.GetDefaultReportPath();
            var file = Path.Combine(path, reportName);
            report.Load(file);

            var builder = new SqlConnectionStringBuilder(Method.GetConnectionString(Settings.Default.CONNECTION));
            var HostName = builder.DataSource;
            var DBName = builder.InitialCatalog;
            var UserID = builder.UserID;
            var Password = builder.Password;

            // Create a Crystal ConnectionInfo object
            ConnectionInfo conRpt = new ConnectionInfo();
            conRpt.ServerName = HostName;
            conRpt.DatabaseName = DBName;
            conRpt.IntegratedSecurity = false;
            conRpt.UserID = UserID;
            conRpt.Password = Password;

            // Apply the connection information to the report tables
            Tables tblsRpt = report.Database.Tables;
            for (int i = 0; i < tblsRpt.Count; i++)
            {
                Table tblRpt = tblsRpt[i];
                TableLogOnInfo infoTbl = tblRpt.LogOnInfo;
                infoTbl.ConnectionInfo = conRpt;
                tblRpt.ApplyLogOnInfo(infoTbl);
            }
            report.SetParameterValue("@PRINT_INVOICE_ID", printInvoiceId);
            report.SetParameterValue("@COMPANY_NAME", objCompany.COMPANY_NAME);
            report.SetParameterValue("@COMPANY_ADDRESS", objCompany.ADDRESS);
            if (Login.CurrentLogin == null)
            {
                report.SetParameterValue("@LOGIN_NAME", "");
            }
            else
            {
                report.SetParameterValue("@LOGIN_NAME", Login.CurrentLogin.LOGIN_NAME);
            }
            report.SummaryInfo.ReportTitle = reportName;
            return report;
        }

        public ReportDocument GetViewOldInvoice(string reportName, int printInvoiceId)
        {
            return GetReportOldDocument(reportName, "", printInvoiceId);
        }

        public ReportDocument GetPrintOldInvoice(string reportName,string printerName, int printInvoiceId)
        {
            var report = GetReportOldDocument(reportName, printerName, printInvoiceId);
            report.PrintOptions.PrinterName = printerName;
            report.PrintToPrinter(1, true, 1, 1000000);
            return report;
        }

        public static Dictionary<List<ReportAgingModel>, DataTable> getAgingReportData(ReportAgingSearchParam model)
        {
            string ConnectionString = Method.GetConnectionString(Settings.Default.CONNECTION);
            var dt = new DataTable();
            using (var cnn = new SqlConnection(ConnectionString))
            {
                if (cnn.State == ConnectionState.Closed)
                {
                    cnn.Open();
                }
                using (var cmd = new SqlCommand("dbo.REPORT_AGING_NEW", cnn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;
                    cmd.Parameters.AddWithValue("@DATE", model.DATE.Date);
                    dt.Load(cmd.ExecuteReader());
                }
            }
            var result = dt.ToDataListModel<ReportAgingModel>();
            var orderedResult = result.OrderBy(x => x.AREA_NAME).ThenBy(x => x.BOX_CODE).ThenBy(x => x.POLE_CODE).ThenBy(x=>x.CUSTOMER_CODE).ToList();
            var id = 0;
            orderedResult.ForEach(x => 
            {
                x.ID = id += 1;
            });
             
            var dcResult = new System.Collections.Generic.Dictionary<List<ReportAgingModel>, DataTable>();
            
            dcResult.Add(orderedResult, dt);
            return dcResult;
        }

        public static Dictionary<List<ReportPaymentModel>, DataTable> getPaymentReportData(ReportPaymentSearchParam model)
        {
            string ConnectionString = Method.GetConnectionString(Settings.Default.CONNECTION);
            var dt = new DataTable();
            using (var cnn = new SqlConnection(ConnectionString))
            {
                if (cnn.State == ConnectionState.Closed)
                {
                    cnn.Open();
                }
                using (var cmd = new SqlCommand("dbo.REPORT_CUSTOMER_PAYMENT_DETAIL", cnn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;
                    cmd.Parameters.AddWithValue("@DATE_PAID_FROM", model.DATE_PAID_FROM);
                    cmd.Parameters.AddWithValue("@DATE_PAID_TO", model.DATE_PAID_TO);
                    dt.Load(cmd.ExecuteReader());
                }
            }
            var result = dt.ToDataListModel<ReportPaymentModel>();

            var allBank = result.Select(x => x.PAYMENT_METHOD).Distinct().ToList();
            var allBankConfig = DBDataContext.Db.TBL_PAYMENT_CONFIGs.ToList();
            result.ForEach(x =>
            {
                if (x.PAY_AT_BANK != "-")
                {
                    var bankpayment = allBankConfig.FirstOrDefault(p => p.PAYMENT_TYPE == x.PAYMENT_METHOD);
                    if (bankpayment != null)
                    {
                        if (bankpayment.CUT_OFF_DATE_PAYMENT != null)
                        {
                            var bcd = x.BANK_CUT_OFF_DATE;
                            var dbcd = bankpayment.CUT_OFF_DATE_PAYMENT.Value;
                            x.BANK_CUT_OFF_DATE = new DateTime(bcd.Year, bcd.Month, bcd.Day, dbcd.Hour, dbcd.Minute, dbcd.Second);
                            if (x.EXACT_PAID_DATE > x.BANK_CUT_OFF_DATE)
                            {
                                x.BANK_CUT_OFF_DATE = BankLogic.GetNextWorkingDay(x.BANK_CUT_OFF_DATE);
                            }
                        }
                    }
                }
            });
            var orderedResult = result.OrderBy(x => x.PAID_DATE).ToList();
            var id = 0;

            orderedResult.ForEach(x =>
            {
                x.ID = id += 1;
            });

            var dcResult = new System.Collections.Generic.Dictionary<List<ReportPaymentModel>, DataTable>();

            dcResult.Add(orderedResult, dt);
            return dcResult;
        }

    }
    public class ReportInvoice
    {
        public int PRINT_ORDER { get; set; }
        public long INVOICE_ID { get; set; }

        private string _invoice_id_string;
        public string INVOICE_ID_STRINGKEY { get { return INVOICE_ID.ToString(); } set { _invoice_id_string = value; } }
        public DateTime INVOICE_MONTH { get; set; }
        public string INVOICE_NO { get; set; }
        public string METER_CODE { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
        public DateTime INVOICE_DATE { get; set; }
        public decimal START_USAGE { get; set; }
        public decimal END_USAGE { get; set; }
        public long CUSTOMER_ID { get; set; }
        public decimal FORWARD_AMOUNT { get; set; }
        public decimal TOTAL_AMOUNT { get; set; }
        public decimal SETTLE_AMOUNT { get; set; }
        public decimal PAID_AMOUNT { get; set; }
        public decimal TOTAL_USAGE { get; set; }
        public int CURRENCY_ID { get; set; }
        public int CYCLE_ID { get; set; }
        public DateTime DUE_DATE { get; set; }
        public int INVOICE_STATUS { get; set; }
        public bool IS_SERVICE_BILL { get; set; }
        public int PRINT_COUNT { get; set; }
        public long RUN_ID { get; set; }
        public decimal DISCOUNT_USAGE { get; set; }
        public string DISCOUNT_USAGE_NAME { get; set; }
        public decimal DISCOUNT_AMOUNT { get; set; }
        public string DISCOUNT_AMOUNT_NAME { get; set; }
        public string INVOICE_TITLE { get; set; }
        public string LAST_NAME { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME_KH { get; set; }
        public string FIRST_NAME_KH { get; set; }
        public string ADDRESS { get; set; }
        public string HOUSE_NO { get; set; }
        public string STREET_NO { get; set; }
        public string PHONE_1 { get; set; }
        public string PHONE_2 { get; set; }
        public string COMPANY_NAME { get; set; }
        public string CUSTOMER_CODE { get; set; }
        public int AREA_ID { get; set; }
        public string AREA_NAME { get; set; }
        public string AREA_CODE { get; set; }
        public int BOX_ID { get; set; }
        public string BOX_CODE { get; set; }
        public int POLE_ID { get; set; }
        public string POLE_CODE { get; set; }
        public string AMPARE { get; set; }
        public decimal CURRENT_DUE { get; set; }
        public decimal ADJUST_AMOUNT { get; set; }
        public DateTime START_PAY_DATE { get; set; }
        public string CURRENCY_SING { get; set; }
        public int INVOICE_CUSTOMER_ID { get; set; }
        public long INVOICE_DETAIL_ID { get; set; }
        public long INVOICE_ID_DETAIL { get; set; }
        public decimal START_USAGE_DETAIL { get; set; }
        public decimal END_USAGE_DETAIL { get; set; }
        public decimal USAGE_DETAIL { get; set; }
        public decimal PRICE { get; set; }
        public int INVOICE_ITEM_ID { get; set; }
        public decimal AMOUNT { get; set; }
        public string CHARGE_DESCRIPTION { get; set; }
        public int REPORT_GROUP { get; set; }
        public int IS_DETAIL { get; set; }
        public int MULTIPLIER { get; set; }
        public int NO_USAGE_CUSTOMER { get; set; }
        public int NO_INVOICE_CUSTOMER { get; set; }
        public string ADJUST_NOTE { get; set; }
        public string STATISTIC { get; set; }
        public DateTime LAST_PAY_DATE { get; set; }
        public decimal LAST_PAY { get; set; }
    }
    public class ReportInvoiceUsage
    {
        public int INVOICE_USAGE_ID { get; set; }
        public int INVOICE_ID { get; set; }
        private string _invoice_id_string;
        public string INVOICE_ID_STRINGKEY { get { return INVOICE_ID.ToString(); } set { _invoice_id_string = value; } }
        public int CUSTOMER_ID { get; set; }
        public int METER_ID { get; set; }
        public string METER_CODE { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public decimal START_USAGE { get; set; }
        public decimal END_USAGE { get; set; }
        public int MULTIPLIER { get; set; }
        public decimal TOTAL_USAGE { get; set; }
    }
    public class ReportInvoiceHistory
    {
        public int CUSTOMER_ID { get; set; }
        public DateTime INVOICE_MONTH { get; set; }
        public decimal TOTAL_USAGE { get; set; }
    }
    public class BillPrintingListModel 
    {
        public string TemplateName { get; set; }
        public bool IsOld { get; set; }

    } 
}
