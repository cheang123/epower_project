﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;

namespace EPower.Logic.IRDevice
{
    class BlueStar:IRDevice
    {
        const string BlueStar_Info="BlueStar.dat"; 
        const string SysExecute = "E-Power.elf";
        const string PublishVersion = "V1.6 E-Power";
        public BlueStar()
            : base()
        {
             
 
        }

        public override int GetUsage(List<TBL_BILLING_CYCLE> cycle, List<TBL_AREA> area, ref string month,bool newDb=true)
        {
            return base.GetUsage(cycle, area, ref month);
        }
        public override string GetID()
        {
            string strCom = string.Concat("COM", this.COM); 
            bool result = false;
            using (BlueStarSerialPortPR510 objSpPR = new BlueStarSerialPortPR510())
            {
                result = objSpPR._Open(strCom,this.BaudRate.ToString());
                if (result)
                {
                    result = objSpPR._ReadSaveFile(BlueStar_Info, Application.StartupPath);
                }

                if (result)
                {
                    //Get Device ID
                    this.ProductID = File.ReadAllText(BlueStar_Info);
                } 
                objSpPR.DiscardOutBuffer(); 
                objSpPR.Dispose();
                objSpPR.Close();
            } 
            return this.ProductID; 
        }

        public override bool Test()
        {
            string strCom = string.Concat("COM", this.COM);
            bool result = false;
            using (BlueStarSerialPortPR510 objSpPR = new BlueStarSerialPortPR510())
            {
                result = objSpPR._Open(strCom, this.BaudRate.ToString());
                if (result)
                {
                    result = objSpPR._ReadSaveFile(BlueStar_Info, Application.StartupPath);
                }

                if (result)
                {
                    //Get Device ID
                    this.ProductID = File.ReadAllText(BlueStar_Info);
                }
                objSpPR.DiscardOutBuffer();
                objSpPR.Dispose();
                objSpPR.Close();
                
            }
            return result; 
        }

        public override List<TMP_IR_USAGE> GetFile(BackupType backup)
        {
            /*
             * No Extract data
             * too old version
             */ 
            var rows = base.GetFile(backup);
            bool bol = false;
            string strCom = string.Concat("COM", this.COM);
            string error = string.Empty;
            using (BlueStarSerialPortPR510 objSpPR = new BlueStarSerialPortPR510())
            {
                bol = objSpPR._Open(strCom, this.BaudRate.ToString());
                if (bol)
                {
                    FileInfo file = new FileInfo(Settings.Default.IR_FILE_DB_LOCAL);
                    bol = objSpPR._ReadSaveFile(file.Name, file.DirectoryName);
                }

                if (!bol)
                {
                    if (backup == BackupType.BeforeSFTD)
                    {
                        bol = true;
                    }
                    throw new Exception("File not found.");

                }

                if (bol)
                {
                    //backup file dbf After copy from device            
                    string fileBackup = string.Format("{0}{1}{2:yyyyMMddhhmmss}.dbf", backup.ToString(), this.ProductID, DBDataContext.Db.GetSystemDate());
                    File.Copy(Settings.Default.IR_FILE_DB_LOCAL, Path.Combine(this.PathBackup, fileBackup), true);
                }
                error = objSpPR.ErrorMessage;
                objSpPR.DiscardOutBuffer();
                objSpPR.Dispose();
                objSpPR.Close();

            }
            if (bol)
            {
                return rows;
            }
            else
            {
                throw new Exception(error);
            }
        }

        public override bool SendFile()
        {
            bool bol = false;  
            string strCom = string.Concat("COM", this.COM);
            string error=string.Empty;
            using (BlueStarSerialPortPR510 objSpPR = new BlueStarSerialPortPR510())
            {
                bol = objSpPR._Open(strCom, Settings.Default.IR_BOUND_RATE.ToString());

                Thread.Sleep(1200);
                if (bol)
                {
                    bol = objSpPR._WriteFile(Path.Combine(Application.StartupPath, this._TmpPath), this._DBInfo);
                    Thread.Sleep(3000);
                }

                if (bol)
                {
                    bol = objSpPR._WriteFile(Path.Combine(Application.StartupPath, this._TmpPath), this._DDSc);
                    Thread.Sleep(3000);
                }


                if (bol)
                {
                    bol = objSpPR._WriteFile(Path.Combine(Application.StartupPath, this._TmpPath), this._DDSb);
                }
                error = objSpPR.ErrorMessage;
                objSpPR.DiscardOutBuffer();
                objSpPR.Dispose();
                objSpPR.Close();
            } 
            if (bol)
            {
                return bol; 
            }
            else
            {
                throw new Exception(error);
            }
            
        }

        public override bool CheckVersion()
        {
            bool bol = false;
            string strCom = string.Concat("COM", this.COM);
            string error = string.Empty;
            using (BlueStarSerialPortPR510 objSpPR = new BlueStarSerialPortPR510())
            {
                
                bol = objSpPR._Open(strCom, this.BaudRate.ToString());
                if (bol)
                {
                    bol = false; 
                    ArrayList fileList = objSpPR._ListFile(); 
                    foreach (var item in fileList)//find file sys.dat
                    {
                        if (item.ToString().ToLower().Contains(FileSys))
                        {
                            bol = true;
                            break;
                        }
                    } 
                }
 
                error = objSpPR.ErrorMessage;
                objSpPR.DiscardOutBuffer();
                objSpPR.Dispose();
                objSpPR.Close();  
            }
            using(BlueStarSerialPortPR510 objSpPR = new BlueStarSerialPortPR510())
	        {
                FileInfo file = new FileInfo(Path.Combine(Application.StartupPath, FileSys));
                Thread.Sleep(1200);
                if (bol)
	            {
                    bol = objSpPR._Open(strCom, this.BaudRate.ToString()); 
	            }
                if (bol)
	            {
		             bol = objSpPR._ReadSaveFile(file.Name, file.DirectoryName); 
	            }
                error = objSpPR.ErrorMessage;
                objSpPR.DiscardOutBuffer();
                objSpPR.Dispose();
                objSpPR.Close(); 
		 
	        }   
            if (bol)
            {
                string buff = File.ReadAllText(FileSys);
                if (buff.Length < 14)
                {
                    return false;
                }
                buff = buff.Substring(2, PublishVersion.Length);

                if (buff.Contains(PublishVersion))
                {
                    this.ProductVersion = PublishVersion;
                    return true;      
                }
                bol = Update();
            }
             
            if (!bol)
            {
                throw new Exception(error); 
            } 
            return bol;
        }

        private bool Update()
        {
            bool bol = false;
            string strCom = string.Concat("COM", this.COM);
            string error = string.Empty;
            Runner.Instance.Text = string.Format(Resources.MS_UPDATING_APPLICATION_IN_DEVICE, string.Empty, string.Empty);
            if (!File.Exists(Path.Combine(_TmpPath, SysExecute)))
            {
                throw new Exception(string.Concat(Resources.MS_DEVICE_NEED_TO_UPDATE, "\n", Resources.MS_TAMPLATE_DEVICE_DATABASE_NOT_FOUND));
            }

            using (BlueStarSerialPortPR510 objSpPR = new BlueStarSerialPortPR510())
            {
                bol = objSpPR._Open(strCom, Settings.Default.IR_BOUND_RATE.ToString());

                Thread.Sleep(1200);
                if (bol)
                {
                    bol = objSpPR._WriteFile(Path.Combine(Application.StartupPath, this._TmpPath), SysExecute);
                    Thread.Sleep(3000);
                }  

                error = objSpPR.ErrorMessage;
                objSpPR.DiscardOutBuffer();
                objSpPR.Dispose();
                objSpPR.Close();
            }
            if (bol)
            {
                return bol;
            }
            else
            {
                throw new Exception(error);
            }
        }

    }
}
