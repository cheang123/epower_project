﻿using EPower.Base.Logic;
using EPower.Properties;
using Newtonsoft.Json;
using QRCoder;
using SoftTech;
using SoftTech.Component;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Logic.IRDevice
{
    class Android : IRDevice
    {
        AdbManager adb = new AdbManager();
        string[] models = { "HT380D", "JB_HT380D" };
        string sendDataUsages = "";
        List<TMP_IR_USAGE> receiveDataUsage;
        List<TMP_IR_RELAY_METER> relayData;
        List<TMP_IR_TAMPER_METER> tamperData;
        DeviceData _device;
        string PublishVersion = "2.8.3";

        public Bitmap RegisterQrCode(string registerCode)
        {
            var data = registerCode;
            var gen = new QRCodeGenerator();
            var qrData = gen.CreateQrCode(data, QRCodeGenerator.ECCLevel.Q);
            var qrCode = new QRCode(qrData);
            return qrCode.GetGraphic(4);
        }
        public List<TMP_IR_USAGE> ToIrUsage(string json)
        {
            var usages = json._ToObject<List<TMP_IR_USAGE>>();
            usages.ForEach(x => x.REGISTER_CODE = Method.GetMETER_REGCODE(x.METER_CODE, x.IS_DIGITAL));
            foreach (var u in usages)
            {
                //u.REGISTER_CODE = Method.GetMETER_REGCODE(u.METER_CODE, u.IS_DIGITAL);
                u.TAMPER_STATUS = u.TAMPER_STATUS == null ? "" : u.TAMPER_STATUS;
            }
            return usages;
        }

        public List<TMP_IR_RELAY_METER> toIrRelay(string json)
        {
            var data = json._ToObject<List<TMP_IR_RELAY_METER>>();
            var now = DBDataContext.Db.GetSystemDate();
            data.ForEach(x => x.CREATE_ON = now);
            return data;
        }

        public override bool CheckVersion()
        {
            var sdk = adb.implementCommandLine(adb.FULL_PATH, "shell getprop ro.build.version.sdk").Replace("\r", "").Replace("\n", "");
            var version = adb.implementCommandLine(adb.FULL_PATH, "shell dumpsys package kh.com.e_power.collectusage | grep versionName")
                 .Replace("\r", "")
                 .Replace("\n", "")
                 .Replace("versionName=", "").Trim();
            if (!version.Equals(PublishVersion))
            {
                return Update();
            }

            return true;
        }
        private bool Update()
        {
            //var path = Path.Combine(Application.StartupPath, @"Template\HT380D\app.apk");
            var hardware = adb.implementCommandLine(adb.FULL_PATH, "shell getprop ro.boot.hardware");
            hardware = hardware.Trim();
            /*
             * Old hardware (qcom)
             * Hardware        : Qualcomm Technologies, Inc MSM8916
             * Revision        : 0000
             * Serial          : 0000000000000000
             * Processor       : ARMv7 Processor rev 0 (v7l)
             * ===^^^===
             * New hardware (mt6755)
             * Processor       : AArch64 Processor rev 2 (aarch64)
             * processor       : 0
             * model name      : AArch64 Processor rev 2 (aarch64)
             * BogoMIPS        : 26.00
             * Features        : fp asimd evtstrm aes pmull sha1 sha2 crc32
             * CPU implementer : 0x41
             * CPU architecture: 8
             * CPU variant     : 0x0
             * CPU part        : 0xd03
             * CPU revision    : 2
             * Hardware        : MT6755M
             */
            var path = Path.Combine(Application.StartupPath, @"Template\HT380D\" + hardware + ".apk");

            var pakage = "kh.com.e_power.collectusage";
            adb.uninstall(pakage, "");
            var success = adb.install(path);
            if (success)
            {
                adb.StartApp(pakage);
                throw new Exception("តំឡើងកម្មវិធីជោគជ័យ សូមដំណើរការកម្មវិធីក្នុងឧបករណ៍ នឹងបញ្ជូនទិន្នន័យឡើងវិញ។");
            }
            return success;
        }
        public override int GetUsage(List<TBL_BILLING_CYCLE> mCycle, List<TBL_AREA> mArea, ref string month, bool newDb = true)
        {
            var usages = base.GetMonthlyUsages(mCycle, mArea);
            string licenseCode = DBDataContext.Db.TBL_CONFIGs.FirstOrDefault().AREA_CODE;
            sendDataUsages = "";
            string str = Runner.Instance.Text;
            int total = usages.Count();
            int id = 1;
            int num = 0;

            List<ROW_CONTEXT> customers = new List<ROW_CONTEXT>();

            foreach (var row in usages)
            {
                var cus = new ROW_CONTEXT();

                cus.usage.END_USAGE = row.END_USAGE;
                cus.usage.IS_METER_RENEW_CYCLE = row.IS_METER_RENEW_CYCLE;
                cus.usage.IS_DIGITAL = row.IS_DIGITAL;
                cus.usage.IS_READ_IR = row.IS_READ_IR;
                cus.usage.DATE_COLLECT = new DateTime(1900, 1, 1);
                cus.usage.IS_METER_REGISTER = row.IS_METER_REGISTER;
                cus.usage.IS_COLLECT_USAGE = false;
                cus.usage.IS_UPDATE = false;
                cus.usage.IS_BLUETOOTH = row.IS_BLUETOOTH;

                cus.customer.METER_CODE = row.METER_CODE;
                cus.customer.CUSTOMER_CODE = row.CUSTOMER_CODE;
                cus.customer.BOX_CODE = row.BOX_CODE;
                cus.customer.FULL_NAME = row.FULL_NAME;
                cus.customer.AREA_CODE = row.AREA_CODE;
                cus.customer.POLE_CODE = row.POLE_CODE;
                cus.customer.MULTIPLIER = row.MULTIPLIER;
                cus.customer.START_USAGE = row.START_USAGE;
                cus.customer.LAST_TOTAL_USAGE = row.LAST_TOTAL_USAGE;
                cus.customer.LICENSE_CODE = licenseCode;
                cus.customer.PHASE_ID = row.PHASE_ID;

                cus.usageHistory.USAGE_HISTORY = row.USAGE_HISTORY;

                //cus.tamperMeter.CUSTOMER_ID = row.CUSTOMER_ID;
                //cus.tamperMeter.METER_ID = row.METER_ID;
                //cus.tamperMeter.COLLECTOR_ID = cus.tamperMeter.DEVICE_ID = 0;
                cus.tamperMeter.CREATE_ON = DateTime.Parse("1999-01-01");
                cus.tamperMeter.TAMPER_STATUS = "";

                /*
                 * Gen id.
                 */
                cus.customer.ID = id;
                cus.usage.ID = id;
                cus.usageHistory.ID = id;
                cus.tamperMeter.ID = id;

                customers.Add(cus);

                //Commit Transaction every insert 100 record or end of record. 
                num++;
                if (num % 100 == 0 || num % total == 0)
                {
                    Runner.Instance.Text = string.Format(str + "\nCustomer {0} ({1:#.##}%)", num, (float)(num * 100 / total));
                    System.Windows.Forms.Application.DoEvents();
                }

                id++;
            }

            List<string> m = new List<string>();
            foreach (var c in mCycle)
            {
                m.Add(Method.GetNextBillingMonth(c.CYCLE_ID).Month.ToString("00"));
            }
            month = string.Join(",", m.Distinct().ToArray());


            JsonSerializerSettings js = new JsonSerializerSettings();
            js.DateFormatString = "yyyy-MMM-dd";
            sendDataUsages = string.Empty;
            sendDataUsages = JsonConvert.SerializeObject(customers, js);
            return total;
        }

        public override List<TMP_IR_USAGE> GetFile(BackupType backup)
        {
            try
            {
                receiveDataUsage = new List<TMP_IR_USAGE>();
                string respons = UtilsNetwork.GetSomethingFromAndroid();
                if (string.IsNullOrEmpty(respons))
                {
                    throw new Exception(Resources.IR_ANDROID_ENTER_COMMUNICATION_MODE);
                }
                if (respons == "No data found")
                {
                    return receiveDataUsage;
                }
                /*
                 * Backup
                 */
                var now = DBDataContext.Db.GetSystemDate();
                string fileBackup = Path.Combine(this.PathBackup, string.Format("{0}{1}{2:yyyyMMddhhmmssff}.txt", backup.ToString(), this.ProductID, now));
                File.WriteAllText(fileBackup, respons);

                receiveDataUsage = JsonConvert.DeserializeObject<List<TMP_IR_USAGE>>(respons);
                return receiveDataUsage;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        public override List<TMP_IR_RELAY_METER> GetRelayData(BackupType backup)
        {
            relayData = new List<TMP_IR_RELAY_METER>();
            try
            {
                string respons = UtilsNetwork.GetRelayFromIR();
                if (string.IsNullOrEmpty(respons))
                {
                    throw new Exception(Resources.IR_ANDROID_ENTER_COMMUNICATION_MODE);
                }
                /*
                 * Backup
                 */
                var now = DBDataContext.Db.GetSystemDate();
                string fileBackup = Path.Combine(this.PathBackup, string.Format("{0}{1}{2:yyyyMMddhhmmssff}.txt", backup.ToString(), this.ProductID, now));
                File.WriteAllText(fileBackup, respons);
                relayData = JsonConvert.DeserializeObject<List<TMP_IR_RELAY_METER>>(respons);

                return relayData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override List<TMP_IR_TAMPER_METER> GetTamperData(BackupType backup)
        {
            tamperData = new List<TMP_IR_TAMPER_METER>();
            try
            {
                string respons = UtilsNetwork.GetTamperFromIR();
                if (string.IsNullOrEmpty(respons))
                {
                    throw new Exception(Resources.IR_ANDROID_ENTER_COMMUNICATION_MODE);
                }
                /*
                 * Backup
                 */
                var now = DBDataContext.Db.GetSystemDate();
                string fileBackup = Path.Combine(this.PathBackup, string.Format("{0}{1}{2:yyyyMMddhhmmssff}.txt", backup.ToString(), this.ProductID, now));
                File.WriteAllText(fileBackup, respons);
                tamperData = JsonConvert.DeserializeObject<List<TMP_IR_TAMPER_METER>>(respons);

                return tamperData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void GetRelayMeter(List<IrMobile.Models.TBL_RELAY_METER> objs)
        {
            List<ROW_CONTEXT> relay = new List<ROW_CONTEXT>();
            string licenseCode = DBDataContext.Db.TBL_CONFIGs.FirstOrDefault().AREA_CODE;
            string str = Runner.Instance.Text;
            var id = 1;
            int num = 0;
            int total = objs.Count();
            var data = from r in objs
                       join m in DBDataContext.Db.TBL_METERs on r.METER_CODE equals m.METER_CODE
                       select new TBL_RELAY_METER
                       {
                           RELAY_METER_ID = r.RELAY_METER_ID,
                           METER_CODE = r.METER_CODE,
                           AREA_CODE = r.AREA_CODE,
                           POLE_CODE = r.POLE_CODE,
                           BOX_CODE = r.BOX_CODE,
                           CREATE_ON = r.CREATE_ON,
                           FULL_NAME = r.FULL_NAME,
                           METER_RELAY_STATUS = r.METER_RELAY_STATUS,
                           ORDER_RELAY_STATUS = r.ORDER_RELAY_STATUS,
                           IS_BLUETOOTH = m.IS_BLUETOOTH
                       };
            foreach (var row in data)
            {
                var cus = new ROW_CONTEXT();

                cus.relayMeter.RELAY_METER_ID = row.RELAY_METER_ID;
                cus.relayMeter.METER_CODE = row.METER_CODE;
                cus.relayMeter.AREA_CODE = row.AREA_CODE;
                cus.relayMeter.POLE_CODE = row.POLE_CODE;
                cus.relayMeter.BOX_CODE = row.BOX_CODE;
                cus.relayMeter.CREATE_ON = new DateTime(1900, 1, 1);
                cus.relayMeter.FULL_NAME = row.FULL_NAME;
                cus.relayMeter.METER_RELAY_STATUS = row.METER_RELAY_STATUS;
                cus.relayMeter.ORDER_RELAY_STATUS = row.ORDER_RELAY_STATUS;
                cus.relayMeter.IS_BLUETOOTH = row.IS_BLUETOOTH;
                cus.relayMeter.ID = id;

                relay.Add(cus);

                //Commit Transaction every insert 100 record or end of record. 
                num++;
                if (num % 100 == 0 || num % total == 0)
                {
                    Runner.Instance.Text = String.Format(str + "\nCustomer {0} ({1:#.##}%)", num, (float)(num * 100 / total));
                    Application.DoEvents();
                }

                id++;
            }
            JsonSerializerSettings js = new JsonSerializerSettings();
            js.DateFormatString = "yyyy-MMM-dd";
            try
            {
                sendDataUsages = string.Empty;
                sendDataUsages = JsonConvert.SerializeObject(relay, js);

            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        public void GetTamperMeter(List<TMP_IR_TAMPER_METER> obj)
        {
            List<ROW_CONTEXT> tamper = new List<ROW_CONTEXT>();
            string licenseCode = DBDataContext.Db.TBL_CONFIGs.FirstOrDefault().AREA_CODE;
            string str = Runner.Instance.Text;
            int num = 0;
            var id = 1;
            int total = obj.Count;
            foreach (var row in obj)
            {
                var cus = new ROW_CONTEXT();

                cus.tamperMeter.ID = id;
                cus.tamperMeter.METER_CODE = row.METER_CODE;
                cus.tamperMeter.FULL_NAME = row.FULL_NAME;
                cus.tamperMeter.AREA_CODE = row.AREA_CODE;
                cus.tamperMeter.POLE_CODE = row.POLE_CODE;
                cus.tamperMeter.BOX_CODE = row.BOX_CODE;
                cus.tamperMeter.CREATE_ON = DateTime.Parse("1999-01-01");
                cus.tamperMeter.TAMPER_STATUS = row.TAMPER_STATUS;
                cus.tamperMeter.IS_BLUETOOTH = row.IS_BLUETOOTH;

                tamper.Add(cus);

                num++;
                if (num % 100 == 0 || num % total == 0)
                {
                    Runner.Instance.Text = String.Format(str + "\nCustomer {0} ({1:#.##}%)", num, (float)(num * 100 / total));
                    Application.DoEvents();
                }

                id++;
            }
            JsonSerializerSettings js = new JsonSerializerSettings();
            js.DateFormatString = "yyyy-MMM-dd";
            try
            {
                sendDataUsages = string.Empty;
                sendDataUsages = JsonConvert.SerializeObject(tamper, js);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        public override bool SendFile()
        {
            try
            {
                string respons = UtilsNetwork.SendSomethingToAndroid(sendDataUsages);
                return respons != "";
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        public override bool SaveUsage(int intDeviceID, int intCollectorID)
        {
            bool blnReturn = false;
            try
            {

                TBL_DEVICE objDevice = DBDataContext.Db.TBL_DEVICEs.Where(x => x.DEVICE_ID == intDeviceID).FirstOrDefault();

                Runner.Instance.Text = string.Format(Resources.MS_SAVING_DATA, "");
                Application.DoEvents();
                DBDataContext.Db.ExecuteCommand("TRUNCATE TABLE TMP_IR_USAGE");
                string str = Runner.Instance.Text;
                var rows = receiveDataUsage;
                if (rows.Count == 0)
                {
                    throw new MsgBox.MessageException(Resources.MS_DEVICE_NO_CUSTOMER_COLLECT_USAGE);
                }

                using (var tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    int k = 0;

                    var n = rows.Count;

                    foreach (var row in rows)
                    {
                        row.REGISTER_CODE = Method.GetMETER_REGCODE(row.METER_CODE, row.IS_DIGITAL);
                        row.TAMPER_STATUS = string.IsNullOrEmpty(row.TAMPER_STATUS) ? "00" : row.TAMPER_STATUS;
                        DBDataContext.Db.TMP_IR_USAGEs.InsertOnSubmit(row);
                        k++;
                        if (k % 100 == 0 || k % n == 0)
                        {
                            DBDataContext.Db.SubmitChanges();
                            Runner.Instance.Text = string.Format(str + "\nCustomer {0} ({1:#.##}%)", k, (float)(k * 100 / n));
                            Application.DoEvents();
                        }
                    }

                    this.RunIRUsage(objDevice.DEVICE_ID, intCollectorID);
                    this.TotalCollectCustomer = rows.Count();
                    this.TotalTamperMeter = rows.Where(x => x.TAMPER_STATUS != "" && x.PHASE_ID == 3).Count();

                    tran.Complete();
                }
                DBDataContext.Db = null;
            }
            catch (Exception exp) { throw exp; }
            return blnReturn;
        }

        public bool SaveRelayMeter(int intDeviceID)
        {
            bool blnReturn = false;
            try
            {
                TBL_DEVICE objDevice = DBDataContext.Db.TBL_DEVICEs.Where(x => x.DEVICE_ID == intDeviceID).FirstOrDefault();
                Runner.Instance.Text = string.Format(Resources.MS_SAVING_DATA, "");
                Application.DoEvents();
                DBDataContext.Db.ExecuteCommand("TRUNCATE TABLE TMP_IR_RELAY_METER");
                string str = Runner.Instance.Text;
                var rows = relayData;
                if (rows.Count == 0)
                {
                    throw new MsgBox.MessageException(Resources.MS_DEVICE_NO_CUSTOMER_COLLECT_USAGE);
                }
                using (var tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    int k = 0;
                    var d = DBDataContext.Db.GetSystemDate();
                    var n = rows.Count;

                    foreach (var row in rows)
                    {
                        row.CREATE_ON = d;
                        DBDataContext.Db.TMP_IR_RELAY_METERs.InsertOnSubmit(row);
                        k++;
                        if (k % 100 == 0 || k % n == 0)
                        {
                            DBDataContext.Db.SubmitChanges();
                            Runner.Instance.Text = string.Format(str + "\nCustomer {0} ({1:#.##}%)", k, (float)(k * 100 / n));
                            Application.DoEvents();
                        }
                    }
                    this.RunIRRelayMeter(intDeviceID);
                    this.TotalCollectCustomer = rows.Count();
                    tran.Complete();
                }
                DBDataContext.Db = null;
            }
            catch (Exception exp) { throw exp; }
            return blnReturn;
        }

        public bool SaveTamperMeter(int intDeviceID, int intCollectorID)
        {
            bool blnReturn = false;
            try
            {
                TBL_DEVICE objDevice = DBDataContext.Db.TBL_DEVICEs.Where(x => x.DEVICE_ID == intDeviceID).FirstOrDefault();
                Runner.Instance.Text = string.Format(Resources.MS_SAVING_DATA, "");
                Application.DoEvents();
                DBDataContext.Db.ExecuteCommand("TRUNCATE TABLE TMP_IR_TAMPER_METER");
                string str = Runner.Instance.Text;
                var rows = tamperData;
                if (rows.Count == 0)
                {
                    throw new MsgBox.MessageException(Resources.MS_DEVICE_NO_CUSTOMER_COLLECT_USAGE);
                }
                using (var tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    int k = 0;
                    var d = DBDataContext.Db.GetSystemDate();
                    var n = rows.Count;

                    foreach (var row in rows)
                    {
                        row.CREATE_ON = d;
                        DBDataContext.Db.TMP_IR_TAMPER_METERs.InsertOnSubmit(row);
                        k++;
                        if (k % 100 == 0 || k % n == 0)
                        {
                            DBDataContext.Db.SubmitChanges();
                            Runner.Instance.Text = string.Format(str + "\nCustomer {0} ({1:#.##}%)", k, (float)(k * 100 / n));
                            Application.DoEvents();
                        }
                    }
                    this.RunIRTamperMeter(intDeviceID, intCollectorID);
                    //DBDataContext.Db.TMP_IR_TAMPER_METERs.InsertAllOnSubmit(rows);
                    this.TotalCollectCustomer = rows.Count();
                    tran.Complete();
                }
                DBDataContext.Db = null;
            }
            catch (Exception exp) { throw exp; }
            return blnReturn;
        }

        public override string GetID()
        {
            var devices = adb.devices();

            foreach (DeviceData d in devices)
            {
                if (models.Contains(d.Model))
                {
                    _device = d;
                    ProductID = _device.Serial;
                }
            }
            if (_device != null)
            {
                if (_device.State != DeviceState.Online)
                {
                    throw new Exception("Device state:" + _device.State.ToString());
                }
                // active soket server in android device.
                adb.forward();
                return _device.Serial;
            }
            else
            {
                return "";
            }
        }

        public override bool Test()
        {
            string id = GetID();
            return id != "";
        }

        public class ROW_CONTEXT
        {
            public IrMobile.Models.TBL_CUSTOMER customer;
            public TBL_USAGE usage;
            public IrMobile.Models.TBL_USAGE_HISTORY usageHistory;
            public TMP_IR_TAMPER_METER tamperMeter;
            public TBL_RELAY_METER relayMeter;

            public ROW_CONTEXT()
            {
                customer = new IrMobile.Models.TBL_CUSTOMER();
                usage = new TBL_USAGE();
                usageHistory = new IrMobile.Models.TBL_USAGE_HISTORY();
                tamperMeter = new TMP_IR_TAMPER_METER();
                relayMeter = new TBL_RELAY_METER();
            }
        }

        public class TBL_USAGE
        {
            public int ID { get; set; }
            public decimal END_USAGE { get; set; }
            public bool IS_METER_RENEW_CYCLE { get; set; }
            public bool IS_DIGITAL { get; set; }
            public bool IS_READ_IR { get; set; }
            public DateTime DATE_COLLECT { get; set; }
            public bool IS_METER_REGISTER { get; set; }
            public bool IS_COLLECT_USAGE { get; set; }
            public bool IS_UPDATE { get; set; }
            public bool IS_BLUETOOTH { get; set; }
        }

        public class TBL_RELAY_METER
        {
            public int ID { get; set; }
            public Guid RELAY_METER_ID { get; set; }
            public string METER_CODE { get; set; }
            public string BOX_CODE { get; set; }
            public string FULL_NAME { get; set; }
            public string AREA_CODE { get; set; }
            public string POLE_CODE { get; set; }
            public DateTime CREATE_ON { get; set; }
            public int ORDER_RELAY_STATUS { get; set; }
            public int METER_RELAY_STATUS { get; set; }
            public bool IS_BLUETOOTH { get; set; }
        }


        public class UtilsNetwork
        {
            private static string IP = "127.0.0.1";
            private static int PORT = 59900;
            private static string MAGIC_LETTER = "\n";

            public static bool IsIPv4(string value)
            {
                if (string.IsNullOrEmpty(value))
                    return false;
                var quads = value.Split('.');

                // if we do not have 4 quads, return false
                if (!(quads.Length == 4)) return false;

                // for each quad
                foreach (var quad in quads)
                {
                    int q;
                    // if parse fails 
                    // or length of parsed int != length of quad string (i.e.; '1' vs '001')
                    // or parsed int &lt; 0 // or parsed int &gt; 255
                    // return false
                    if (!int.TryParse(quad, out q)
                        || !q.ToString().Length.Equals(quad.Length)
                        || q < 0 || q > 255) { return false; }
                }
                return true;
            }

            public static bool PingHost(string nameOrAddress)
            {
                bool pingable = false;
                Ping pinger = new Ping();
                try
                {
                    PingReply reply = pinger.Send(nameOrAddress);
                    pingable = reply.Status == IPStatus.Success;
                }
                catch (PingException)
                {
                    // Discard PingExceptions and return false;
                }
                return pingable;
            }


            public static string GetRelayFromIR()
            {
                string result;
                NetworkStream stream = null;
                TcpClient client = null;
                try
                {
                    client = new TcpClient(IP, PORT);

                    //client = new TcpClient(server, port);
                    stream = client.GetStream();
                    stream.ReadTimeout = 15000;
                    stream.WriteTimeout = 15000;

                    var getDataRequestByte = Encoding.UTF8.GetBytes("get_a11" + MAGIC_LETTER);
                    stream.Write(getDataRequestByte, 0, getDataRequestByte.Length);

                    StringBuilder strReceived = new StringBuilder();
                    var dataRead = new byte[1024];
                    while (stream.Read(dataRead, 0, dataRead.Length) > 0)
                    {
                        strReceived.Append(Encoding.UTF8.GetString(dataRead));
                        dataRead = new byte[1024];
                    }


                    if (string.IsNullOrEmpty(strReceived.ToString()))
                    {
                        //result = "No data found";
                        throw new Exception("No data found");
                    }
                    else
                    {
                        result = strReceived.ToString();
                    }

                    var data = Encoding.UTF8.GetBytes("#end#");
                    stream.Write(data, 0, data.Length);
                    stream.Close();
                    client.Close();
                    return result;
                }
                catch (Exception e)
                {
                    if (stream != null) stream.Close();
                    if (client != null) client.Close();
                    return null;
                }
            }

            public static string GetTamperFromIR()
            {
                string result;
                NetworkStream stream = null;
                TcpClient client = null;
                try
                {
                    client = new TcpClient(IP, PORT);

                    //client = new TcpClient(server, port);
                    stream = client.GetStream();
                    stream.ReadTimeout = 15000;
                    stream.WriteTimeout = 15000;

                    var getDataRequestByte = Encoding.UTF8.GetBytes("get_tamper" + MAGIC_LETTER);
                    stream.Write(getDataRequestByte, 0, getDataRequestByte.Length);

                    StringBuilder strReceived = new StringBuilder();
                    var dataRead = new byte[1024];
                    while (stream.Read(dataRead, 0, dataRead.Length) > 0)
                    {
                        strReceived.Append(Encoding.UTF8.GetString(dataRead));
                        dataRead = new byte[1024];
                    }


                    if (string.IsNullOrEmpty(strReceived.ToString()))
                    {
                        result = "No data found";
                    }
                    else
                    {
                        result = strReceived.ToString();
                    }

                    var data = Encoding.UTF8.GetBytes("#end#");
                    stream.Write(data, 0, data.Length);
                    stream.Close();
                    client.Close();
                    return result;
                }
                catch (Exception e)
                {
                    if (stream != null) stream.Close();
                    if (client != null) client.Close();
                    return null;
                }
            }

            public static string GetSomethingFromAndroid()
            {
                string result;
                NetworkStream stream = null;
                TcpClient client = null;
                try
                {
                    client = new TcpClient(IP, PORT);

                    //client = new TcpClient(server, port);
                    stream = client.GetStream();
                    stream.ReadTimeout = 15000;
                    stream.WriteTimeout = 15000;

                    var getDataRequestByte = Encoding.UTF8.GetBytes("get_all" + MAGIC_LETTER);
                    stream.Write(getDataRequestByte, 0, getDataRequestByte.Length);

                    StringBuilder strReceived = new StringBuilder();
                    var dataRead = new byte[1024];
                    while (stream.Read(dataRead, 0, dataRead.Length) > 0)
                    {
                        strReceived.Append(Encoding.UTF8.GetString(dataRead));
                        dataRead = new byte[1024];
                    }


                    if (string.IsNullOrEmpty(strReceived.ToString()))
                    {
                        result = "No data found";
                    }
                    else
                    {
                        result = strReceived.ToString();
                    }

                    var data = Encoding.UTF8.GetBytes("#end#");
                    stream.Write(data, 0, data.Length);
                    stream.Close();
                    client.Close();
                    return result;
                }
                catch (Exception e)
                {
                    if (stream != null) stream.Close();
                    if (client != null) client.Close();
                    return null;
                }
            }

            public static string SendSomethingToAndroid(string sm2send)
            {
                NetworkStream stream = null;
                TcpClient client = null;
                try
                {
                    client = new TcpClient(IP, PORT);

                    //client = new TcpClient(server, port);
                    stream = client.GetStream();
                    stream.ReadTimeout = 15000;
                    stream.WriteTimeout = 15000;

                    var dataRequestByte = Encoding.UTF8.GetBytes("start_action" + MAGIC_LETTER);
                    stream.Write(dataRequestByte, 0, dataRequestByte.Length);

                    //stream.WriteByte(0x2);
                    Thread.Sleep(100);



                    var sm2beSent = Encoding.UTF8.GetBytes(sm2send);
                    //stream.Write(sm2beSent, 0, sm2beSent.Length);
                    //var line = Encoding.UTF8.GetBytes(MAGIC_LETTER);
                    //stream.Write(line, 0, line.Length);
                    var chunk = sm2beSent.Split(230400);
                    foreach (var item in chunk)
                    {
                        stream.Write(item.ToArray(), 0, item.Count());
                        var line = Encoding.UTF8.GetBytes(MAGIC_LETTER);
                        stream.Write(line, 0, line.Length);
                    }



                    var data = Encoding.UTF8.GetBytes("#end#" + MAGIC_LETTER);
                    stream.Write(data, 0, data.Length);
                    //stream.WriteByte(0x3);
                    stream.Close();
                    client.Close();
                    return "Data Sent Successfully: " + sm2send;
                }
                catch (Exception e)
                {
                    if (stream != null) stream.Close();
                    if (client != null) client.Close();
                    return "Something Went Wrong! " + e.Message;
                }
            }

            public static string SendToAndroid(string ip, int port, string message)
            {
                if (!IsIPv4(ip))
                {
                    return null;
                }
                NetworkStream stream = null;
                TcpClient client = null;
                try
                {
                    client = new TcpClient(ip, port);

                    //client = new TcpClient(server, port);
                    stream = client.GetStream();
                    stream.ReadTimeout = 15000;
                    stream.WriteTimeout = 15000;

                    var readData = new byte[1024];
                    stream.Read(readData, 0, readData.Length);

                    var firstMessage = Encoding.UTF8.GetString(readData);

                    if (firstMessage.StartsWith("sendingData"))
                    {
                        // recieve data here
                        var dataToSend = Encoding.UTF8.GetBytes("Get your data bow\n");
                        stream.Write(dataToSend, 0, dataToSend.Length);
                    }
                    else
                    {
                        var dataToSend = Encoding.UTF8.GetBytes(message + "\n");
                        stream.Write(dataToSend, 0, dataToSend.Length);
                        //var respnse = Encoding.UTF8.GetString(data);
                    }

                    var data = Encoding.UTF8.GetBytes("#end#");
                    stream.Write(data, 0, data.Length);
                    stream.Close();
                    client.Close();
                    return "";
                }
                catch (Exception e)
                {
                    if (stream != null) stream.Close();
                    if (client != null) client.Close();
                    return null;
                }
            }
        }

    }
}
