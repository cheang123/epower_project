﻿using EPower.Base.Logic;
using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;

namespace EPower.Logic.IRDevice
{
    public class AdbManager
    {
        // exe file name
        private const string CMD = "cmd.exe";
        private const string ADB = "adb.exe";
        public string FULL_PATH;
        // parameters
        private const string PARAM_RESTART = "";
        private const string PARAM_KILL = "kill-server";
        private const string PARAM_START = "start-server";
        private const string PARAM_FORWORD = "forward tcp:59900 tcp:59900";
        private const string PARAM_DEVICES_LIST = "devices -l";
        private const string PARAM_DEVICES = "devices";

        // 
        private const string MODEL = "HT380D";
        private const string PARAM_MODEL = "-s model:HT380D";
        private const string PATH_ON_ANDROID_PREFIX = "/sdcard/USBMbhFiles/";
        private const string PATH_AND_FILES = PATH_ON_ANDROID_PREFIX + "files/";
        private const string PARAM_PUSH = "push -p";

        private bool mHasFoundAdb = false;
        //private DeviceData mDevice;

        public AdbManager() : this(Method.GetUtilityValue(Utility.IR_ADB_PATH))
        {

            //    FULL_PATH = Method.GetUtilityValue(Utility.IR_ADB_PATH);
            //    //FULL_PATH = Path.Combine(@"C:\Users\Serey\AppData\Local\Android\sdk\platform-tools", ADB);
            //    //FULL_PATH = Path.Combine(@"C:\Users\SINSARATH\AppData\Local\Android\sdk\platform-tools", ADB);

            //    restart();

        }

        public AdbManager(string path)
        {
            FULL_PATH = path;
            restart();
        }

        public bool isMbhDeviceConnected()
        {
            //mDevice = null;
            //var devices = AdbClient.Instance.GetDevices();
            //foreach (DeviceData device in devices)
            //{
            //    if (device.Model.Contains("M757"))
            //    {
            //        mDevice = device;
            //    }
            //}
            //return mDevice != null;
            return true;
        }

        public string restart()
        {
            string[] restartCommands =  { FULL_PATH + " " + PARAM_KILL + " \r\n"
                                    ,FULL_PATH + " " + PARAM_START + " \r\n"};
            return implementMultipleCommandLines(CMD, restartCommands, 200);
        }

        public IList devices()
        {
            //return AdbClient.Instance.GetDevices();
            try
            {
                var r = implementCommandLine(FULL_PATH, PARAM_DEVICES_LIST);
                var data = r.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries);
                return data.Where(x => x != "List of devices attached").Select(d => DeviceData.CreateFromAdbData(d)).ToList();
            }
            catch (Exception exp)
            {
                try
                {
                    var r = implementCommandLine(FULL_PATH, PARAM_DEVICES);
                    var data = r.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries);
                    return data.Where(x => x != "List of devices attached").Select(d => DeviceData.CreateFromAdbData(d)).ToList();
                }
                catch (Exception)
                {
                    throw exp;
                }
            }

        }

        public void isRoot()
        {

        }

        public string forward()
        {
            return implementCommandLine(FULL_PATH, PARAM_FORWORD);
        }

        /// <summary>
        /// Push packages to the device and install them. Possible options are the following:
        /// install [options] package
        /// </summary>
        /// <param name="package"></param>
        /// <param name="option">l: Forward lock application.
        /// -r: Replace the existing application.
        /// -t: Allow test packages.
        /// -s: Install the application on the SD card.
        /// -d: Allow version code downgrade (debugging packages only).
        /// -g: Grant all runtime permissions.</param>
        public bool install(string package, string option = "-r")
        {
            var cmd = string.Format("install {0} \"{1}\"", option, package);
            var r = implementCommandLine(FULL_PATH, cmd).Replace("\r", "")
                 .Replace("\n", "");
            if (!r.Contains("Success"))
            {
                throw new Exception(r);
            }
            return true;
        }

        /// <summary>
        /// Remove this app package from the device. Add the -k option to keep the data and cache directories.
        /// </summary>
        /// <param name="package"></param>
        /// <param name="option">-k option to keep the data and cache directories.</param>
        public bool uninstall(string package, string option = "-k")
        {
            var cmd = string.Format("uninstall {0} {1}", option, package);
            var r = implementCommandLine(FULL_PATH, cmd).Replace("\r", "")
                  .Replace("\n", "");
            if (!r.Contains("Success"))
            {
                throw new Exception(r);
            }
            return true;
        }

        public void StartApp(string package)
        {
            var cmd = string.Format("shell am start -n {0}/{0}.MainActivity", package);
            var r = implementCommandLine(FULL_PATH, cmd).Replace("\r", "")
                  .Replace("\n", "");
        }

        //public bool pushFile(string path, string androidFilePath)
        //{
        //    if (mDevice == null)
        //    {
        //        if (!isMbhDeviceConnected())
        //        {
        //            return false;
        //        }
        //    }
        //    string command = PARAM_MODEL + " " + PARAM_PUSH + " " + @path + " " + PATH_AND_FILES + androidFilePath;

        //    string result = implementCommandLine(FULL_PATH, command);

        //    return result.Contains("100%");
        //}

        //private bool pullFile(string path, string androidFilePath)
        //{
        //    //if (mDevice == null)
        //    //{
        //    //    if (!isMbhDeviceConnected())
        //    //    {
        //    //        return false;
        //    //    }
        //    //}

        //    //IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 59900);
        //    //AdbSocket socket = new AdbSocket(endPoint);
        //    //using (SyncService service = new SyncService(socket, mDevice))
        //    //using (Stream stream = File.OpenWrite(path))
        //    //{
        //    //    service.Pull(androidFilePath, stream, null, CancellationToken.None);
        //    //}

        //    return true;
        //}

        public string implementCommandLine(string fileName, string param)
        {
            string output = "";
            string error = "";

            ProcessStartInfo startInfo = new ProcessStartInfo(fileName, param)
            {
                WindowStyle = ProcessWindowStyle.Hidden,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                CreateNoWindow = true
            };

            Process adbProc;
            adbProc = Process.Start(startInfo);
            using (StreamReader myOutput = adbProc.StandardOutput)
            {
                output = myOutput.ReadToEnd();
            }

            using (StreamReader myError = adbProc.StandardError)
            {
                error = myError.ReadToEnd();
                if (error != "")
                    return error;
            }

            return output;
        }

        public string implementMultipleCommandLines(string fileName, string[] commands, int sleepBetweenCommandsTime)
        {
            Process p = new Process();
            string output = "";

            ProcessStartInfo startInfo = new ProcessStartInfo()
            {
                FileName = fileName,
                WindowStyle = ProcessWindowStyle.Hidden,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardInput = true,
                CreateNoWindow = true
            };

            p.StartInfo = startInfo;
            p.Start();

            using (StreamWriter sw = p.StandardInput)
            {
                if (sw.BaseStream.CanWrite)
                {
                    foreach (string command in commands)
                    {
                        sw.WriteLine(command);
                        if (sleepBetweenCommandsTime > 100)
                            Thread.Sleep(200);
                    }
                }
            }

            using (StreamReader myOutput = p.StandardOutput)
            {
                output = myOutput.ReadToEnd();
            }

            return output;
        }
    }
    /// <summary>
    /// Represents a device that is connected to the Android Debug Bridge.
    /// </summary>
    public class DeviceData
    {
        /// <summary>
        /// A regular expression that can be used to parse the device information that is returned
        /// by the Android Debut Bridge.
        /// </summary>
        internal const string DeviceDataRegexString = @"^(?<serial>[a-zA-Z0-9_-]+(?:\s?[\.a-zA-Z0-9_-]+)?(?:\:\d{1,})?)\s+(?<state>device|offline|unknown|bootloader|recovery|download|unauthorized|host)(\s+usb:(?<usb>[^:]+))?(?:\s+product:(?<product>[^:]+)\s+model\:(?<model>[\S]+)\s+device\:(?<device>[\S]+))?(\s+features:(?<features>[^:]+))?$";
        //internal const string DeviceDataRegexString = @"^(?<serial>[a-zA-Z0-9_-]+(?:\s?[\.a-zA-Z0-9_-]+)?(?:\:\d{1,})?)\s+(?<state>device|offline|unknown|bootloader|recovery|download|unauthorized|host)(\s+usb:(?<usb>[^:]+))?(?:\s+product:(?<product>[^:]+))?(\s+model\:(?<model>[\S]+))?(\s+device\:(?<device>[\S]+))?(\s+features:(?<features>[^:]+))?$";


        /// <summary>
        /// A regular expression that can be used to parse the device information that is returned
        /// by the Android Debut Bridge.
        /// </summary>
        private static readonly Regex Regex = new Regex(DeviceDataRegexString, RegexOptions.Compiled | RegexOptions.IgnoreCase);

        /// <summary>
        /// Gets or sets the device serial number.
        /// </summary>
        public string Serial
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the device state.
        /// </summary>
        public DeviceState State
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the device model name.
        /// </summary>
        public string Model
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the device product name.
        /// </summary>
        public string Product
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the device name.
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the features available on the device.
        /// </summary>
        public string Features
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the USB port to which this device is connected. Usually available on Linux only.
        /// </summary>
        public string Usb
        {
            get;
            set;
        }

        /// <summary>
        /// Creates a new instance of the <see cref="DeviceData"/> class based on
        /// data retrieved from the Android Debug Bridge.
        /// </summary>
        /// <param name="data">
        /// The data retrieved from the Android Debug Bridge that represents a device.
        /// </param>
        /// <returns>
        /// A <see cref="DeviceData"/> object that represents the device.
        /// </returns>
        public static DeviceData CreateFromAdbData(string data)
        {
            //  data = "836f4f4d4a535646       device product:zenltedx model:SM_G9287C device:zenlte";
            //  data = "0123456789ABCDEF       device product:7 NOTE model:7 NOTE device:7_NOTE";
            Match m = Regex.Match(data);
            if (m.Success)
            {
                return new DeviceData()
                {
                    Serial = m.Groups["serial"].Value,
                    State = GetStateFromString(m.Groups["state"].Value),
                    Model = m.Groups["model"].Value,
                    Product = m.Groups["product"].Value,
                    Name = m.Groups["device"].Value,
                    Features = m.Groups["features"].Value,
                    Usb = m.Groups["usb"].Value
                };
            }
            else
            {
                throw new ArgumentException($"Invalid device list data '{data}'");
            }
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return this.Serial;
        }

        /// <summary>
        /// Get the device state from the string value
        /// </summary>
        /// <param name="state">The device state string</param>
        /// <returns></returns>
        internal static DeviceState GetStateFromString(string state)
        {
            // Default to the unknown state
            DeviceState value = DeviceState.Unknown;

            if (string.Equals(state, "device", StringComparison.OrdinalIgnoreCase))
            {
                // As a special case, the "device" state in ADB is translated to the
                // "Online" state in Managed.Adb
                value = DeviceState.Online;
            }
            else if (string.Equals(state, "no permissions", StringComparison.OrdinalIgnoreCase))
            {
                value = DeviceState.NoPermissions;
            }
            else
            {
                value = (DeviceState)Enum.Parse(typeof(DeviceState), state);
                //// Else, we try to match a value of the DeviceState enumeration.
                //if (!Enum.TryParse<DeviceState>(state, true, out value))
                //{
                //    value = DeviceState.Unknown;
                //}
            }

            return value;
        }
    }
    //
    // Summary:
    //     Defines the state of an Android device connected to the Android Debug Bridge.
    public enum DeviceState
    {
        //
        // Summary:
        //     The instance is not connected to adb or is not responding.
        Offline = 0,
        //
        // Summary:
        //     The device is in bootloader mode
        BootLoader = 1,
        //
        // Summary:
        //     The instance is now connected to the adb server. Note that this state does not
        //     imply that the Android system is fully booted and operational, since the instance
        //     connects to adb while the system is still booting. However, after boot-up, this
        //     is the normal operational state of an emulator/device instance.
        Online = 2,
        //
        // Summary:
        //     The device is the adb host.
        Host = 3,
        //
        // Summary:
        //     The device is in recovery mode.
        Recovery = 4,
        //
        // Summary:
        //     Insufficient permissions to communicate with the device.
        NoPermissions = 5,
        //
        // Summary:
        //     The device is in sideload mode.
        Sideload = 6,
        //
        // Summary:
        //     The device is connected to adb, but adb is not authorized for remote debugging
        //     of this device.
        Unauthorized = 7,
        //
        // Summary:
        //     The device state is unknown.
        Unknown = 8
    }
}
