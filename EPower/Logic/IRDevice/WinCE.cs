﻿using EPower.Base.Logic;
using EPower.IrMobile;
using EPower.IrMobile.Logics;
using EPower.IrMobile.Models;
using EPower.Properties;
using OpenNETCF.Desktop.Communication;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Transactions;
using System.Windows.Forms;
using System.Xml.Linq;
using TBL_RELAY_METER = EPower.IrMobile.Models.TBL_RELAY_METER;

namespace EPower.Logic.IRDevice
{
    public class Setting
    {
        public string ProductId { get; set; }
        public string ProductVersion { get; set; }
        public string IrPort { get; set; }
        public string ReadType { get; set; }
        public bool GPRS { get; set; }
        public int BatteryTimeout { get; set; }
        public int ACTimeout { get; set; }
    }
    class WinCE : IRDevice
    {
        const string ROOT_DIRECTORY = "Program Files\\epower.irmobile";
        string SD_DIRECTORY = "Storage\\epower.irmobile";
        string PublishVersion = "2.2";
        //limite date for collect usage per transfer data to Ir.
        const int REMAIN_DAY = 7;

        const string APPLICATION_NAME = "EPower.IrMobile.exe";
        const string COMMUNICATION_MANAGER = "EPower.Communication.exe";
        const string TASK_MANAGER = "EPower.TaskManager.exe";
        const string K_WIN32 = "k.win32.dll";

        string mFileName = "MobileDB.sdf";
        string mLogDb = "LogDb.csv";
        string mLastUsage = "LastUsage.xml";
        string mConfigFileName = "IrMobile.exe.Config";

        string mLocalFile = "";
        string mRemotePath = "";
        string mRemoteFile = "";
        string mConnectionString = "";


        RAPI rpp = null;
        public RAPI Connector
        {
            get
            {
                try
                {
                    if (rpp == null)
                    {
                        rpp = new RAPI();
                    }
                    if (!rpp.DevicePresent)
                    {
                        throw new MsgBox.MessageException(Resources.MS_PLEASE_CONNECT_DEVICE_TO_PC);
                    }
                    if (!rpp.Connected)
                    {
                        Thread thread = new Thread(new ThreadStart(rpp.Connect));
                        thread.Start();
                        int k = 0;
                        while (!rpp.Connected)
                        {
                            if (k++ == 5)
                            {
                                if (thread.IsAlive)
                                {
                                    thread.Abort();
                                }
                                throw new MsgBox.MessageException("Windows Mobile Device Center no connect.");
                            }
                            Thread.Sleep(120);
                        }

                        if (rpp.Connected)
                        {
                            if (rpp.DeviceFileExists(Path.Combine(ROOT_DIRECTORY, APPLICATION_NAME)))
                            {
                                RootDirectory();
                            }
                            else
                            {
                                // Find sd card
                                string dirName = CERegistry.LocalMachine.OpenSubKey(@"System\StorageManager\Profiles\SDMemory").GetValue("Folder").ToString();
                                SD_DIRECTORY = Path.Combine(dirName, "epower.irmobile");

                                if (rpp.DeviceFileExists(Path.Combine(SD_DIRECTORY, APPLICATION_NAME)))
                                {
                                    SdDirectory();
                                }
                                else
                                {
                                    throw new FileNotFoundException(Resources.MS_PLEASE_CONNECT_DEVICE_TO_PC);
                                }
                            }


                        }
                    }
                    //StartConnect();
                    return rpp;
                }
                catch (RankException exp) { throw exp; }
                catch (Exception exp) { throw exp; }
            }
        }

        public WinCE()
        {
            PublishVersion = Method.GetUtilityValue(Utility.WINCE_IR_VERSION);
            RootDirectory();
            mLocalFile = Path.Combine("MobileDB", mFileName);
            mConnectionString = string.Format("DataSource={0};Password={1};Autoshrink Threshold = 100;", mLocalFile, "underadmin");

            Cmd.ConnectionString = mConnectionString;
        }

        /// <summary>
        /// Default directory is [\Program Files\epower.irmobile]
        /// </summary>
        private void RootDirectory()
        {
            mRemotePath = ROOT_DIRECTORY;
            mRemoteFile = Path.Combine(mRemotePath, mFileName);
        }

        /// <summary>
        /// Default directory is [\Storage\Program Files\epoer.irmobile]
        /// </summary>
        private void SdDirectory()
        {
            mRemotePath = SD_DIRECTORY;
            mRemoteFile = Path.Combine(mRemotePath, mFileName);
        }

        #region Sql Query
        private string QueryInsert()
        {
            return @"INSERT TMP_MOBILE_USAGE (
	CUSTOMER_ID, 
	METER_ID,
    BILLING_CYCLE_ID,
	CUSTOMER_CODE, 
	METER_CODE, 
	FULL_NAME, 
	AREA_ID, 
	POLE_ID, 
	BOX_ID, 
	AREA_CODE, 
	POLE_CODE, 
	BOX_CODE, 
	IS_DIGITAL, 
	MULTIPLIER, 
	START_USAGE, 
	END_USAGE, 
	IS_METER_RENEW_CYCLE, 
	LAST_TOTAL_USAGE,
    USAGE_MONTH,
    START_DATE,
    END_DATE,
    DATE_COLLECT,
    IS_METER_REGISTER,
    IS_COLLECT_USAGE,
    IS_UPDATE,
    LICENSE_CODE,
    HISTORY_USAGE) 
VALUES (
	@CUSTOMER_ID, 
	@METER_ID,
    @BILLING_CYCLE_ID, 
	@CUSTOMER_CODE, 
	@METER_CODE, 
	@FULL_NAME, 
	@AREA_ID, 
	@POLE_ID, 
	@BOX_ID, 
	@AREA_CODE, 
	@POLE_CODE, 
	@BOX_CODE, 
	@IS_DIGITAL, 
	@MULTIPLIER, 
	@START_USAGE, 
	@END_USAGE, 
	@IS_METER_RENEW_CYCLE, 
	@LAST_TOTAL_USAGE,
    @USAGE_MONTH,
    @START_DATE,
    @END_DATE,
    @DATE_COLLECT,
    @IS_METER_REGISTER,
    @IS_COLLECT_USAGE,
    @IS_UPDATE,
    @LICENSE_CODE,
    @HISTORY_USAGE);";
        }

        private string QueryUpdate()
        {
            return @"UPDATE TMP_MOBILE_USAGE 
SET
	CUSTOMER_ID = @CUSTOMER_ID, 
	METER_ID = @METER_ID,
    BILLING_CYCLE_ID = @BILLING_CYCLE_ID,
	CUSTOMER_CODE = @CUSTOMER_CODE,
	FULL_NAME = @FULL_NAME,
	AREA_ID = @AREA_ID,
	POLE_ID = @POLE_ID, 
	BOX_ID = @BOX_ID,
	AREA_CODE = @AREA_CODE,
	POLE_CODE = @POLE_CODE, 
	BOX_CODE = @BOX_CODE,
	IS_DIGITAL = @IS_DIGITAL,
	MULTIPLIER = @MULTIPLIER, 
	START_USAGE = @START_USAGE,
	END_USAGE = @END_USAGE,
	IS_METER_RENEW_CYCLE = @IS_METER_RENEW_CYCLE,
	LAST_TOTAL_USAGE = @LAST_TOTAL_USAGE,
    USAGE_MONTH = @USAGE_MONTH,
    START_DATE = @START_DATE,
    END_DATE = @END_DATE,
    DATE_COLLECT = @DATE_COLLECT,
    IS_METER_REGISTER = @IS_METER_REGISTER,
    IS_COLLECT_USAGE = @IS_COLLECT_USAGE,
    IS_UPDATE = @IS_UPDATE,
    HISTORY_USAGE = @HISTORY_USAGE
WHERE LICENSE_CODE = @LICENSE_CODE AND METER_CODE = @METER_CODE;";
        }

        private string QueryInsertLog()
        {
            return "INSERT INTO LOG(CREATE_BY,CREATE_ON,REMAIN_DAY,LICENSE_NO,ENABLE_ALL_HISTORY_USAGE) VALUES(@CREATE_BY,@CREATE_ON,@REMAIN_DAY,@LICENSE_NO,@ENABLE_ALL_HISTORY_USAGE);";
        }

        private string[] QueryCreateTable()
        {
            return new string[]{@"CREATE TABLE TBL_CUSTOMER(
	ID INT PRIMARY KEY,	
	METER_CODE NVARCHAR(50) NOT NULL,
	CUSTOMER_CODE NVARCHAR(50) NOT NULL,
	BOX_CODE NVARCHAR(50) NOT NULL,
	FULL_NAME NVARCHAR(100) NOT NULL, 
	AREA_CODE NVARCHAR(50) NOT NULL,
	POLE_CODE NVARCHAR(50) NOT NULL,
	MULTIPLIER INT NOT NULL,
	START_USAGE DECIMAL(18, 4) NOT NULL,
	LAST_TOTAL_USAGE DECIMAL(18, 4) NOT NULL,
    LICENSE_CODE NVARCHAR(50) NOT NULL,
    PHASE_ID INT
);",

@"CREATE TABLE TBL_USAGE
(
	ID  INT PRIMARY KEY IDENTITY,	
	END_USAGE DECIMAL(18, 4) NOT NULL,
	IS_METER_RENEW_CYCLE BIT NOT NULL,
	IS_DIGITAL BIT NOT NULL,	
	IS_READ_IR BIT NOT NULL,
	DATE_COLLECT DATETIME NOT NULL,
	IS_METER_REGISTER BIT NOT NULL,
	IS_COLLECT_USAGE BIT NOT NULL,
	IS_UPDATE BIT NOT NULL
);",

@"CREATE TABLE TBL_USAGE_HISTORY
( 
	ID INT PRIMARY KEY,
	USAGE_HISTORY NVARCHAR(1000) NOT NULL
);",

@"CREATE TABLE TBL_INFO
(	
	ID  INT PRIMARY KEY IDENTITY,	
	CREATE_BY NVARCHAR(50) NOT NULL,
	CREATE_ON DATETIME NOT NULL,
	SAVE_ON DATETIME NOT NULL,
	LICENSE_NO NVARCHAR(50) NOT NULL,
	ENABLE_ALL_HISTORY_USAGE BIT NOT NULL
);",

@"CREATE TABLE TBL_RELAY_METER
(
	ID INT PRIMARY KEY IDENTITY,
	RELAY_METER_ID UNIQUEIDENTIFIER NOT NULL,
	METER_CODE NVARCHAR(50) NOT NULL,
	BOX_CODE NVARCHAR(50) NOT NULL,
	FULL_NAME NVARCHAR(100) NOT NULL,
	AREA_CODE NVARCHAR(50) NOT NULL,
	POLE_CODE NVARCHAR(50) NOT NULL,
	CREATE_ON DATETIME NOT NULL,
	ORDER_RELAY_STATUS INT NOT NULL,
	METER_RELAY_STATUS INT NOT NULL
);"
            };
        }


        private string[] QueryCreateIndex()
        {
            return new string[] {
                "CREATE INDEX IX_TBL_CUSTOMER_METER_CODE ON TBL_CUSTOMER (METER_CODE);",
                "CREATE INDEX IX_TBL_CUSTOMER_AREA_CODE ON TBL_CUSTOMER (AREA_CODE);"
            };
        }

        #endregion

        public override bool Test()
        {
            try
            {
                return Connector.Connected;
            }
            catch (Exception exp) { throw exp; }
        }

        public override string GetID()
        {
            try
            {
                string remoteFile = Path.Combine(mRemotePath, mConfigFileName);
                string localFile = Path.Combine("TEMP", mConfigFileName);
                if (!Directory.Exists("TEMP"))
                {
                    Directory.CreateDirectory("TEMP");
                }
                Connector.CopyFileFromDevice(localFile, remoteFile, true);
                //Read file config
                var doc = XDocument.Load(localFile);
                var setting = Serializer.Deserialize<Setting>(doc.ToString());
                this.ProductID = setting.ProductId;
                this.ProductVersion = setting.ProductVersion;
                return this.ProductID;
            }
            catch (RAPIException exp) { throw exp; }
            catch (InvalidOperationException exp)
            {
                if (exp.Message.Contains("There is an error in XML document (1, 2)."))
                {
                    Update();
                    return GetID();
                }
                throw exp;
            }
            catch (Exception exp) { throw exp; }
        }

        public override bool CheckVersion()
        {
            try
            {
                if (string.IsNullOrEmpty(this.ProductVersion))
                {
                    GetID();
                }

                if (!PublishVersion.Equals(this.ProductVersion))
                {
                    /*
                     * Mr.bollavy Ir old model (Barcode scanner + Infrared)
                     * Cannot update.
                     */
                    if (this.ProductID == "2510080152524575")
                    {
                        return true;
                    }
                    return Update();
                }
                else
                {
                    return true;
                }

            }
            catch (RAPIException exp) { throw exp; }
            catch (Exception exp) { throw exp; }
        }

        private bool Update()
        {
            Runner.Instance.Text = string.Format(Resources.MS_UPDATING_APPLICATION_IN_DEVICE, PublishVersion, "");
            try
            {

                var path = Application.StartupPath + "\\Template\\GF1100";
                var taskUpdate = path + "\\" + TASK_MANAGER;
                var kWin32Update = path + "\\" + K_WIN32;


                /*
                 * Fix error COM Port when update.
                 */
                if (File.Exists(Path.Combine(path, mConfigFileName)))
                {
                    File.Delete(Path.Combine(path, mConfigFileName));
                }

                //// End Application
                //string taskManager=mRemotePath+"\\"+TASK_MANAGER;
                //if (!Connector.DeviceFileExists(taskManager))
                //{
                //    Connector.CopyFileToDevice(taskUpdate, taskManager, true);
                //}
                //Connector.CreateProcess(taskManager, APPLICATION_NAME);
                //Thread.Sleep(1000); 

                foreach (var item in Directory.GetFiles(path).Where(x => x != taskUpdate || x != kWin32Update))
                {
                    var f = new FileInfo(item);
                    var fileUpdate = mRemotePath + "\\" + f.Name;
                    if (Connector.DeviceFileExists(fileUpdate) && Connector.GetDeviceFileAttributes(fileUpdate) != RAPI.RAPIFileAttributes.Normal)
                    {
                        Connector.SetDeviceFileAttributes(fileUpdate, RAPI.RAPIFileAttributes.Normal);
                    }
                    var size = f.Length;
                    Connector.CopyFileToDevice(f.FullName, fileUpdate, true);
                    if (size != Connector.GetDeviceFileSize(fileUpdate))
                    {
                        throw new Exception(string.Format("មិនអាច Update file {0} បានទេ!", f.FullName));
                    }
                }

                // Update k.win32.dll
                Connector.CopyFileToDevice(kWin32Update, "\\Windows\\" + K_WIN32, true);

                //// Start Application
                //Connector.CreateProcess(mRemotePath + "\\" + APPLICATION_NAME);
                //Thread.Sleep(10 * 1000);//10 second
                return true;
            }
            catch (RAPIException exp) { throw exp; }
            catch (Exception exp) { throw exp; }
        }

        public override int GetUsage(List<TBL_BILLING_CYCLE> mCycle, List<TBL_AREA> mArea, ref string month, bool newDb)
        {
            mLocalFile = Path.Combine("MobileDB", mFileName);
            mConnectionString = string.Format("DataSource={0};Password={1};Autoshrink Threshold = 100;", mLocalFile, "underadmin");

            Cmd.ConnectionString = mConnectionString;
            if (File.Exists(mLocalFile) && newDb)
            {
                File.Delete(mLocalFile);
            }
            //Db not exists...
            if (newDb == false && File.Exists(mLocalFile) == false)
            {
                newDb = true;
            }

            month = "";
            try
            {

                string licenseCode = DBDataContext.Db.TBL_CONFIGs.FirstOrDefault().AREA_CODE;
                if (newDb)
                {
                    //Create Database.
                    Cmd.CreateDatabase();

                    Cmd.CloseConnectionAfterExecute = true;
                    //Create Table
                    foreach (string t in QueryCreateTable())
                    {
                        Cmd.ExecuteNonQuery(t);
                    }

                    //Create Index
                    foreach (string index in QueryCreateIndex())
                    {
                        Cmd.ExecuteNonQuery(index);
                    }

                    var info = new TBL_INFO();
                    info.CREATE_BY = Login.CurrentLogin.LOGIN_NAME;
                    info.CREATE_ON = DBDataContext.Db.GetSystemDate();
                    info.SAVE_ON = UIHelper._DefaultDate;
                    info.LICENSE_NO = licenseCode;
                    info.ENABLE_ALL_HISTORY_USAGE = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_ALL_HISTORY_USAGE]);
                    Cmd.Insert(info, "ID");

                    Cmd.ExecuteNonQuery("CREATE TABLE TBL_DEVICE_INFO(DEVICE_INFO_ID INT NOT NULL,DEVICE_ID INT NOT NULL, DEVICE_CODE NVARCHAR(100) NOT NULL," +
                        " DEVICE_HARDWARE_ID NVARCHAR(500) NOT NULL, IS_USE BIT NOT NULL)");

                    Cmd.OnAfterExecute();
                    Cmd.CloseConnectionAfterExecute = true;
                }
                else
                {
                    Cmd.Begin();
                    Cmd.Parameters["@LICENSE_CODE"] = licenseCode;
                    Cmd.ExecuteNonQuery(@"DELETE TBL_USAGE WHERE EXISTS(SELECT ID FROM TBL_CUSTOMER WHERE LICENSE_CODE = @LICENSE_CODE);");
                    Cmd.Parameters["@LICENSE_CODE"] = licenseCode;
                    Cmd.ExecuteNonQuery(" DELETE TBL_USAGE_HISTORY WHERE EXISTS(SELECT ID FROM TBL_CUSTOMER WHERE LICENSE_CODE = @LICENSE_CODE);");
                    Cmd.Parameters["@LICENSE_CODE"] = licenseCode;
                    Cmd.ExecuteNonQuery("DELETE TBL_CUSTOMER WHERE LICENSE_CODE = @LICENSE_CODE;");
                    Cmd.ExecuteNonQuery("DELETE TBL_DEVICE_INFO");
                    Cmd.Commit();
                }

                int i = 1;
                foreach (var item in DBDataContext.Db.TBL_DEVICEs.Where(x => x.DRIVER == @"EPower.Logic.IRDevice.WinCE"))
                {
                    Cmd.Parameters["@DEVICE_INFO_ID"] = i;
                    Cmd.Parameters["@DEVICE_ID"] = item.DEVICE_ID;
                    Cmd.Parameters["@DEVICE_CODE"] = item.DEVICE_CODE;
                    Cmd.Parameters["@DEVICE_HARDWARE_ID"] = item.DEVICE_HARDWARE_ID;
                    Cmd.ExecuteNonQuery(@"INSERT INTO TBL_DEVICE_INFO VALUES(@DEVICE_INFO_ID,@DEVICE_ID,@DEVICE_CODE,@DEVICE_HARDWARE_ID,0)");
                }
                Cmd.Commit();


                var usages = base.GetMonthlyUsages(mCycle, mArea);

                string str = Runner.Instance.Text;
                int total = usages.Count();
                int num = 0;
                foreach (var row in usages)
                {
                    if (num % 100 == 0)
                    {
                        Cmd.Begin();
                    }

                    CustomerUsageLogic logic = new CustomerUsageLogic();
                    var cus = logic.NewCustomerUsage();
                    cus.USAGE.END_USAGE = row.END_USAGE;
                    cus.USAGE.IS_METER_RENEW_CYCLE = row.IS_METER_RENEW_CYCLE;
                    cus.USAGE.IS_DIGITAL = row.IS_DIGITAL;
                    cus.USAGE.IS_READ_IR = row.IS_READ_IR;
                    cus.USAGE.DATE_COLLECT = new DateTime(1900, 1, 1);
                    cus.USAGE.IS_METER_REGISTER = row.IS_METER_REGISTER;
                    cus.USAGE.IS_COLLECT_USAGE = false;
                    cus.USAGE.IS_UPDATE = false;

                    cus.CUSTOMER.METER_CODE = row.METER_CODE;
                    cus.CUSTOMER.CUSTOMER_CODE = row.CUSTOMER_CODE;
                    cus.CUSTOMER.BOX_CODE = row.BOX_CODE;
                    cus.CUSTOMER.FULL_NAME = row.FULL_NAME;
                    cus.CUSTOMER.AREA_CODE = row.AREA_CODE;
                    cus.CUSTOMER.POLE_CODE = row.POLE_CODE;
                    cus.CUSTOMER.MULTIPLIER = row.MULTIPLIER;
                    cus.CUSTOMER.START_USAGE = row.START_USAGE;
                    cus.CUSTOMER.LAST_TOTAL_USAGE = row.LAST_TOTAL_USAGE;
                    cus.CUSTOMER.LICENSE_CODE = licenseCode;
                    cus.CUSTOMER.PHASE_ID = row.PHASE_ID;

                    cus.USAGE_HISTORY.USAGE_HISTORY = row.USAGE_HISTORY;

                    logic.SaveNew(cus);

                    //Commit Transaction every insert 100 record or end of record. 
                    num++;
                    if (num % 100 == 0 || num % total == 0)
                    {
                        Cmd.Commit();
                        Runner.Instance.Text = String.Format(str + "\nCustomer {0} ({1:#.##}%)", num, (float)(num * 100 / total));
                        Application.DoEvents();
                    }
                }

                List<string> m = new List<string>();
                foreach (var c in mCycle)
                {
                    m.Add(Method.GetNextBillingMonth(c.CYCLE_ID).Month.ToString("00"));
                }
                month = string.Join(",", m.Distinct().ToArray());

                //Verify Database
                Runner.Instance.Text = Resources.MS_MOBILE_DB_IS_CORRUPT_NEED_TO_REPARE_BEFOR_SEND;
                Cmd.RepairDb();

                Cmd.OnAfterExecute();
                Cmd.CloseConnectionAfterExecute = true;

                return total;
            }
            catch (Exception exp)
            {
                Cmd.Rollback();

                throw exp;
            }
        }

        public override List<TMP_IR_USAGE> GetFile(BackupType backup)
        {
            try
            {
                if (!Connector.DeviceFileExists(mRemoteFile))
                {
                    if (backup == BackupType.BeforeSFTD)
                    {
                        return base.GetFile(backup);
                    }
                    throw new MsgBox.MessageException(Resources.MS_NO_DATABASE_FILE_IN_DEVICE);
                }
                var now = DBDataContext.Db.GetSystemDate();
                // End Application
                EndApplication();
                // Copy SDF database file
                long fileSize = Connector.GetDeviceFileSize(mRemoteFile);
                Connector.CopyFileFromDevice(mLocalFile, mRemoteFile, true);
                if (fileSize != new FileInfo(mLocalFile).Length)
                {
                    throw new MsgBox.MessageException(Resources.MS_COPY_DATABASE_FROM_DEVICE_FAIL);
                }
                string fileBackup = Path.Combine(this.PathBackup, string.Format("{0}{1}{2:yyyyMMddhhmmssff}.sdf", backup.ToString(), this.ProductID, now));
                File.Copy(mLocalFile, fileBackup, true);

                // Copy LogDb.csv file
                var remoteLogDb = Path.Combine(Connector.GetDeviceSystemFolderPath(SystemFolders.Personal), mLogDb);
                if (Connector.DeviceFileExists(remoteLogDb))
                {
                    var localLogDb = Path.Combine(this.PathBackup, string.Format("{0}{1}{2:yyyyMMddhhmmssff}.csv", backup.ToString(), this.ProductID, now));
                    Connector.CopyFileFromDevice(localLogDb, remoteLogDb, true);
                }
                /*
                 * Extract usages.
                 */
                Cmd.ConnectionString = string.Format("DataSource={0};Password={1};Autoshrink Threshold = 100;", fileBackup, "underadmin");
                Cmd.RepairDb();
                Cmd.ExecuteNonQuery(@"UPDATE TBL_CUSTOMER SET PHASE_ID = 1 WHERE PHASE_ID IS NULL");
                string licenseCode = DBDataContext.Db.TBL_CONFIGs.FirstOrDefault().AREA_CODE;
                Cmd.Parameters["@LICENSE_CODE"] = licenseCode;
                var rows = Cmd.ExecuteQuery<TMP_IR_USAGE>(@"SELECT * 
                                            FROM TBL_CUSTOMER c 
                                            INNER JOIN TBL_USAGE u ON u.ID = c.ID 
                                            WHERE u.IS_COLLECT_USAGE = 1 AND (LICENSE_CODE = '' OR LICENSE_CODE = @LICENSE_CODE );");

                return rows;
            }
            catch (RAPIException exp) { throw exp; }
            catch (Exception exp) { throw exp; }
        }

        private void StartApplication()
        {
            Connector.CreateProcess(Path.Combine(mRemotePath, APPLICATION_NAME));
            Thread.Sleep(1000);
        }

        private void EndApplication()
        {
            string path = Application.StartupPath + "\\Template\\GF1100";
            string taskUpdate = path + "\\" + TASK_MANAGER;
            string taskManager = mRemotePath + "\\" + TASK_MANAGER;
            if (!Connector.DeviceFileExists(taskManager))
            {
                Connector.CopyFileToDevice(taskUpdate, taskManager, true);
            }
            Connector.CreateProcess(taskManager, APPLICATION_NAME);
            Thread.Sleep(1000);
        }

        public override bool SendFile()
        {
            try
            {
                long sizeToSend = new FileInfo(mLocalFile).Length;
                Connector.CopyFileToDevice(mLocalFile, mRemoteFile, true);
                if (sizeToSend != Connector.GetDeviceFileSize(mRemoteFile))
                {
                    throw new MsgBox.MessageException(Resources.MS_COPY_DATABASE_FROM_DEVICE_FAIL);
                }

                // Clear LogDb.csv
                var remoteLogDb = Path.Combine(Connector.GetDeviceSystemFolderPath(SystemFolders.Personal), mLogDb);
                if (Connector.DeviceFileExists(remoteLogDb))
                {
                    Connector.DeleteDeviceFile(remoteLogDb);
                }
                // Clear LastUsage.xml
                var remoteLastUsage = Path.Combine(mRemotePath, mLastUsage);
                if (Connector.DeviceFileExists(remoteLastUsage))
                {
                    Connector.DeleteDeviceFile(remoteLastUsage);
                }

                // Startup Application
                StartApplication();

                return true;
            }
            catch (RAPIException exp) { throw exp; }
            catch (Exception exp) { throw exp; }
        }

        public override bool SaveUsage(int intDeviceID, int intCollectorID)
        {
            mLocalFile = Path.Combine("MobileDB", mFileName);
            mConnectionString = string.Format("DataSource={0};Password={1};Autoshrink Threshold = 100;", mLocalFile, "underadmin");

            Cmd.ConnectionString = mConnectionString;

            bool blnReturn = false;
            TBL_DEVICE objDevice = DBDataContext.Db.TBL_DEVICEs.Where(x => x.DEVICE_ID == intDeviceID).FirstOrDefault();

            try
            {

                string licenseCode = DBDataContext.Db.TBL_CONFIGs.FirstOrDefault().AREA_CODE;

                Cmd.RepairDb();
                //Check expire receive data from Ir.
                TBL_INFO info = Cmd.ExecuteQuery<TBL_INFO>(@"SELECT * FROM TBL_INFO").FirstOrDefault();
                if (info.IsSaveExpire)
                {
                    throw new MsgBox.MessageException(Resources.MS_DATA_COLLECT_USAGE_IN_DEVICE_OUT_OF_DATE);
                }
                Cmd.ExecuteNonQuery(@"UPDATE TBL_CUSTOMER SET PHASE_ID = 1 WHERE PHASE_ID IS NULL");
                Cmd.Parameters["@LICENSE_CODE"] = licenseCode;
                var rows = Cmd.ExecuteQuery<TMP_IR_USAGE>(@"SELECT *
                                            FROM TBL_CUSTOMER c 
                                            INNER JOIN TBL_USAGE u ON u.ID = c.ID 
                                            WHERE u.IS_COLLECT_USAGE = 1 AND (LICENSE_CODE = '' OR LICENSE_CODE = @LICENSE_CODE );");

                if (rows.Count == 0)
                {
                    throw new MsgBox.MessageException(Resources.MS_DEVICE_NO_CUSTOMER_COLLECT_USAGE);
                }

                Runner.Instance.Text = string.Format(Resources.MS_SAVING_DATA, "");
                Application.DoEvents();
                DBDataContext.Db.ExecuteCommand("TRUNCATE TABLE TMP_IR_USAGE");
                string str = Runner.Instance.Text;

                using (var tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    int k = 0;
                    var n = rows.Count;
                    foreach (var row in rows)
                    {
                        row.REGISTER_CODE = Method.GetMETER_REGCODE(row.METER_CODE, row.IS_DIGITAL);
                        row.TAMPER_STATUS = "00";
                        DBDataContext.Db.TMP_IR_USAGEs.InsertOnSubmit(row);
                        k++;
                        if (k % 100 == 0 || k % n == 0)
                        {
                            DBDataContext.Db.SubmitChanges();

                            Runner.Instance.Text = string.Format(str + "\nCustomer {0} ({1:#.##}%)", k, (float)(k * 100 / n));
                            Application.DoEvents();
                        }
                    }

                    this.RunIRUsage(objDevice.DEVICE_ID, intCollectorID);
                    this.TotalCollectCustomer = rows.Count();

                    tran.Complete();
                }

                DBDataContext.Db = null;

                //Update expire receive data from Ir.
                if (info.SAVE_ON == UIHelper._DefaultDate)
                {
                    info.SAVE_ON = DateTime.Now;
                    Cmd.Update<TBL_INFO>(info, "ID");
                    Cmd.RepairDb();
                    blnReturn = this.SendFile();
                }
                else
                {
                    blnReturn = true;
                }
            }
            catch (Exception exp) { throw exp; }
            return blnReturn;
        }

        public void SaveRelayMeter(int intDeviceID)
        {
            try
            {
                Cmd.RepairDb();

                var rows = Cmd.ExecuteQuery<TMP_IR_RELAY_METER>(@"SELECT * FROM TBL_RELAY_METER WHERE METER_RELAY_STATUS > 0");
                if (rows.Count == 0)
                {
                    throw new MsgBox.MessageException(Resources.MS_DEVICE_NO_CUSTOMER_COLLECT_USAGE);
                }

                Runner.Instance.Text = string.Format(Resources.MS_SAVING_DATA, "");
                Application.DoEvents();
                // Clean TMP_RELAY_METER
                DBDataContext.Db.ExecuteCommand("TRUNCATE TABLE TMP_IR_RELAY_METER");

                string str = Runner.Instance.Text;
                using (var tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    int k = 0;
                    var n = rows.Count;
                    foreach (var row in rows)
                    {
                        DBDataContext.Db.TMP_IR_RELAY_METERs.InsertOnSubmit(row);
                        k++;
                        if (k % 100 == 0 || k % n == 0)
                        {
                            DBDataContext.Db.SubmitChanges();

                            Runner.Instance.Text = string.Format(str + "\nCustomer {0} ({1:#.##}%)", k, (float)(k * 100 / n));
                            Application.DoEvents();
                        }
                    }

                    DBDataContext.Db.ExecuteCommand("EXEC RUN_ORDER_RELAY_METER @p0,@p1", intDeviceID, Login.CurrentLogin.LOGIN_NAME);
                    tran.Complete();

                }
                DBDataContext.Db = null;
            }
            catch (Exception exp)
            {
                throw exp;
            }

        }

        public void GetRelayMeter(List<TBL_RELAY_METER> objs)
        {
            try
            {
                //clean TBL_RELAY_METER
                Cmd.ExecuteNonQuery("DELETE TBL_RELAY_METER;");
                Cmd.ExecuteNonQuery("ALTER TABLE TBL_USAGE ALTER COLUMN ID IDENTITY(1,1);");

                //Cmd.Begin();
                //Insert data
                foreach (var item in objs)
                {
                    Cmd.Insert(item, "ID");
                }
                //Cmd.Commit();
                Cmd.RepairDb();
            }
            catch (Exception exp)
            {
                Cmd.Rollback();
                throw exp;
            }
            finally
            {
                Cmd.OnAfterExecute();
            }
        }

        public System.Data.DataTable SaveUsageFromSD(int intCollectorID)
        {
            mLocalFile = Path.Combine("MobileDB", mFileName);
            mConnectionString = string.Format("DataSource={0};Password={1};Autoshrink Threshold = 100;", mLocalFile, "underadmin");

            Cmd.ConnectionString = mConnectionString;

            bool blnReturn = false;
            // Get DeviceId
            System.Data.DataTable dt = new System.Data.DataTable();
            dt = Cmd.ExecuteDataTable("SELECT * FROM TBL_DEVICE_INFO WHERE IS_USE = 1");
            if (dt.Rows.Count == 0)
            {
                throw new Exception(Resources.MS_DEVICE_NO_CUSTOMER_COLLECT_USAGE);
            }
            TBL_DEVICE objDevice = DBDataContext.Db.TBL_DEVICEs.Where(x => x.DEVICE_ID == int.Parse(dt.Rows[0][1].ToString())).FirstOrDefault();

            try
            {

                string licenseCode = DBDataContext.Db.TBL_CONFIGs.FirstOrDefault().AREA_CODE;

                Cmd.RepairDb();
                //Check expire receive data from Ir.
                TBL_INFO info = Cmd.ExecuteQuery<TBL_INFO>(@"SELECT * FROM TBL_INFO").FirstOrDefault();
                if (info.IsSaveExpire)
                {
                    throw new MsgBox.MessageException(Resources.MS_DATA_COLLECT_USAGE_IN_DEVICE_OUT_OF_DATE);
                }
                Cmd.ExecuteNonQuery(@"UPDATE TBL_CUSTOMER SET PHASE_ID = 1 WHERE PHASE_ID IS NULL");
                Cmd.Parameters["@LICENSE_CODE"] = licenseCode;
                var rows = Cmd.ExecuteQuery<TMP_IR_USAGE>(@"SELECT * 
                                            FROM TBL_CUSTOMER c 
                                            INNER JOIN TBL_USAGE u ON u.ID = c.ID 
                                            WHERE u.IS_COLLECT_USAGE = 1 AND (LICENSE_CODE = '' OR LICENSE_CODE = @LICENSE_CODE );");
                if (rows.Count == 0)
                {
                    throw new MsgBox.MessageException(Resources.MS_DEVICE_NO_CUSTOMER_COLLECT_USAGE);
                }

                Runner.Instance.Text = string.Format(Resources.MS_SAVING_DATA, "");
                Application.DoEvents();
                DBDataContext.Db.ExecuteCommand("TRUNCATE TABLE TMP_IR_USAGE");
                string str = Runner.Instance.Text;

                using (var tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    int k = 0;
                    var n = rows.Count;
                    foreach (var row in rows)
                    {
                        row.REGISTER_CODE = Method.GetMETER_REGCODE(row.METER_CODE, row.IS_DIGITAL);
                        row.TAMPER_STATUS = "00";
                        DBDataContext.Db.TMP_IR_USAGEs.InsertOnSubmit(row);
                        k++;
                        if (k % 100 == 0 || k % n == 0)
                        {
                            DBDataContext.Db.SubmitChanges();

                            Runner.Instance.Text = string.Format(str + "\nCustomer {0} ({1:#.##}%)", k, (float)(k * 100 / n));
                            Application.DoEvents();
                        }
                    }

                    this.RunIRUsage(objDevice.DEVICE_ID, intCollectorID);
                    this.TotalCollectCustomer = rows.Count();

                    tran.Complete();
                }

                DBDataContext.Db = null;
            }
            catch (Exception exp) { throw exp; }
            //return blnReturn;
            return dt;
        }

        public List<TMP_IR_USAGE> BackUp(string FileBeforeCopy, BackupType backup)
        {
            string FileAfterCopy = "";
            if (File.Exists(FileBeforeCopy))
            {
                if (!Directory.Exists(Settings.Default.PATH_BACKUP + @"\IR"))
                {
                    Directory.CreateDirectory(Settings.Default.PATH_BACKUP + @"\IR");
                }
                FileAfterCopy = Settings.Default.PATH_BACKUP + @"\IR\" +
                    string.Format("{0}{1}{2:yyyyMMddhhmmssff}.sdf", backup.ToString(), this.ProductID, DBDataContext.Db.GetSystemDate());
                File.Copy(FileBeforeCopy, FileAfterCopy, true);
            }
            else return new List<TMP_IR_USAGE>();

            Cmd.ConnectionString = string.Format("DataSource={0};Password={1};Autoshrink Threshold = 100;", FileAfterCopy, "underadmin");
            Cmd.RepairDb();
            string licenseCode = DBDataContext.Db.TBL_CONFIGs.FirstOrDefault().AREA_CODE;
            Cmd.Parameters["@LICENSE_CODE"] = licenseCode;
            var rows = Cmd.ExecuteQuery<TMP_IR_USAGE>(@"SELECT * 
                                            FROM TBL_CUSTOMER c 
                                            INNER JOIN TBL_USAGE u ON u.ID = c.ID 
                                            WHERE u.IS_COLLECT_USAGE = 1 AND (LICENSE_CODE = '' OR LICENSE_CODE = @LICENSE_CODE );");

            return rows;
        }
    }
}
