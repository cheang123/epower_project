﻿using EPower.Base.Logic;
using EPower.HB02.Model;
using EPower.HB02.REST;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Windows.Forms;
using IsolationLevel = System.Data.IsolationLevel;
using TBL_RELAY_METER_IR = EPower.IrMobile.Models.TBL_RELAY_METER;

namespace EPower.Logic.IRDevice
{
    class IRDevice
    {
        public string ProductID { get; internal set; }
        public string ProductVersion { get; set; }
        public string DeviceOEM { get; set; }
        public int TotalCollectCustomer { get; set; }
        public int TotalTamperMeter { get; set; }
        public int TotalUnknown { get { return DBDataContext.Db.TBL_METER_UNKNOWNs.Count(); } }
        public int TotalUnregistered { get { return DBDataContext.Db.TBL_METERs.Where(x => x.REGISTER_CODE == "").Count(); } }
        public bool HasSerial { get; set; }
        public TBL_USAGE_DEVICE_COLLECTION UsageDeviceCollection { get; set; }

        //public string PathBackup
        //{
        //    get
        //    {
        //        string path = Path.Combine(Settings.Default.PATH_BACKUP, "IR");
        //        if (!Directory.Exists(path))
        //        {
        //            Directory.CreateDirectory(path);
        //        }
        //        return path;
        //    }
        //}

        public string PathBackup
        {
            get
            {
                string defaultPath = Path.Combine(Settings.Default.PATH_BACKUP);
                string path = Path.Combine(Settings.Default.PATH_BACKUP, "IR");
                if (Directory.Exists(defaultPath))
                {
                    Directory.CreateDirectory(path);
                }
                else
                {
                    path = Path.Combine(Settings.Default.PATH_TEMP, "IR");
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    else
                    {
                        DirectoryInfo di = new DirectoryInfo(path);
                        foreach (var files in di.GetFiles())
                        {
                            long totalSize = di.EnumerateFiles().Sum(file => file.Length);
                            if (totalSize > 200000000)
                            {
                                files.Delete();
                            }
                        }
                    }
                }
                return path;
            }
        }


        public int COM
        {
            get;
            set;
        }

        public int BaudRate
        {
            get;
            set;
        }
        /// <summary>
        /// Get type of device automatic when connected.
        /// </summary>
        /// <param name="objd">Type object of device</param>
        /// <returns>True if device connected</returns>
        public static IRDevice Connect()
        {
            IRDevice objtmp = new IRDevice();
            List<Type> types = new List<Type>() { typeof(WinCE), typeof(HandPOS), typeof(Android), typeof(ThinPad) };//, typeof(BlueStar)}; 
            var drivers = DBDataContext.Db.TBL_DEVICEs.Select(x => x.DRIVER).Distinct();

            // restructure the types to check by adding current type driver to beginning of the list
            foreach (var driver in drivers)
            {
                Type type = Type.GetType(driver);
                if (type != null)
                {
                    types.Remove(type);
                    types.Insert(0, type);
                }
            }

            bool connectSuccess = false;
            foreach (var type in types)
            {
                try
                {

                    objtmp = (IRDevice)Activator.CreateInstance(type);
                    connectSuccess = objtmp.Test();
                    if (connectSuccess)
                    {
                        break;
                    }
                }
                catch (FileNotFoundException ex)
                {
                    throw ex;
                }
                catch (Exception ex) { }
            }

            // connect to device to get deviceID 
            string deviceId = objtmp.GetID();
            if (deviceId == "")
            {
                
                throw new Exception(Resources.MS_PLEASE_CONNECT_DEVICE_TO_PC);
            }

            var objDevice = DBDataContext.Db.TBL_DEVICEs.FirstOrDefault(x => x.DEVICE_CODE == deviceId);
            // if driver store in db is not correct or not yet available 
            // update new driver to database.
            if (objDevice != null)
            {
                objtmp.DeviceOEM = objDevice.DEVICE_OEM_INFO;
                if (Type.GetType(objDevice.DRIVER) != objtmp.GetType())
                {
                    objDevice.DRIVER = objtmp.GetType().FullName;
                    DBDataContext.Db.SubmitChanges();
                }
            }
            return objtmp;
        }

        /// <summary>
        /// Validate device 
        /// 1. Device code is already register
        /// 2. Device code already assign to specific employees.
        /// </summary>
        /// <param name="objDevice"></param>
        /// <returns></returns>
        public static bool Validate(IRDevice objDevice)
        {
            if (objDevice.HasSerial)
            {


                string strDeviceCode = objDevice.ProductID;

                // if the this IR Reader have not been register
                string strEncryptReader = SoftTech.Security.Logic.CheckSum.GetIRReaderChecksum(strDeviceCode);
                if (DBDataContext.Db.TBL_DEVICEs.Count(d => d.DEVICE_HARDWARE_ID == strEncryptReader) == 0)
                {
                    throw new Exception(Resources.MS_DEVICE_NOT_REGISTER);
                }


                var deviceInfo = (from d in DBDataContext.Db.TBL_DEVICEs
                                  join ed in DBDataContext.Db.TBL_EMPLOYEE_DEVICEs on d.DEVICE_ID equals ed.DEVICE_ID
                                  where d.DEVICE_CODE == strDeviceCode
                                  select new { d.DEVICE_ID, d.DEVICE_OEM_INFO, ed.EMPLOYEE_ID }).FirstOrDefault();

                //if this device not yet assigned to any collector
                //then use the current user to be collector
                if (deviceInfo == null)
                {
                    throw new Exception(Resources.MS_DEVICE_NO_COLLECTOR);
                }
                return true;
            }
            return true;

        }

        public IRDevice()
        {
            this.COM = Settings.Default.IR_COM_PORT;
            this.BaudRate = Settings.Default.IR_BOUND_RATE;
            this.HasSerial = true;

            if (!Directory.Exists("Template"))
            {
                Directory.CreateDirectory("Template");
            }
            if (!File.Exists("Template/IRInfo.dat"))
            {
                File.Create("Template/IRInfo.dat").Close();
            }
        }

        public enum BackupType
        {
            AfterCPFD,//After copy file from device.
            BeforeSFTD//Before send file to device.
        }

        internal string _TmpPath = "Template";
        internal string _TmpDDSa = "DDSaTmp.dbf";
        internal string _TmpDDSb = "DDSbTmp.dbf";
        internal string _TmpDDSc = "DDScTmp.dbf";
        internal string _TmpHSPOSDDSb = "DDSbHSPOSTmp.dbf";
        internal string _TmpPRICE = "PRICETmp.dbf";
        internal string _TmpInfo = "DBInfoTmp.dbf";

        internal string _DDSa = "DDSa.dbf";
        internal string _DDSb = "DDSb.dbf";
        internal string _DDSc = "DDSc.dbf";
        internal string _DBInfo = "DBInfo.dbf";
        internal string _PRICE = "PRICE.dbf";

        internal const string FileSys = "sys.dat";
        internal string SysVer = "V1.5 E-Power";


        public virtual List<TMP_IR_USAGE> GetFile(BackupType backup)
        {
            return new List<TMP_IR_USAGE>();
        }

        public virtual List<TMP_IR_RELAY_METER> GetRelayData(BackupType backup)
        {
            return new List<TMP_IR_RELAY_METER>();
        }
        public virtual List<TMP_IR_TAMPER_METER> GetTamperData(BackupType backup)
        {
            return new List<TMP_IR_TAMPER_METER>();
        }

        public virtual bool SendFile()
        {
            return true;
        }
        public virtual string GetID()
        {
            return ProductID;
        }
        public virtual bool Test()
        {
            return true;
        }
        public virtual bool CheckVersion()
        {
            string buff = File.ReadAllText(FileSys);
            if (buff.Length < 14)
            {
                return false;
            }
            buff = buff.Substring(2, SysVer.Length);

            if (!buff.Contains(SysVer))
            {
                return false;
            }
            return true;
        }
        public virtual void Complete() { return; }
        public virtual bool SaveUsage(int intDeviceID, int intCollectorID)
        {
            bool blnReturn = false;

            this.TotalCollectCustomer = 0;
            try
            {
                //Insert new data to temp table 
                DataTable dtTmpUsage = this.LoadAndBackupUsage();

                if (dtTmpUsage.Rows.Count == 0)
                {
                    throw new Exception(Resources.MS_DEVICE_NO_CUSTOMER_COLLECT_USAGE);
                }

                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    Runner.Instance.Text = string.Format(Resources.MS_SAVING_DATA, "");
                    Application.DoEvents();

                    DateTime datCurrentDate = DBDataContext.Db.GetSystemDate();
                    string strSQL = string.Empty;

                    #region GET USAGE FROM DIGITAL METER RIGESTER
                    //Delete temp table with the current connected device
                    strSQL = "TRUNCATE TABLE TMP_IR_USAGE";
                    DBDataContext.Db.ExecuteCommand(strSQL);

                    //Insert all usage from IR Reader to TBL_TMP_USAGE with meter register
                    foreach (DataRow drTmpUage in dtTmpUsage.Rows)
                    {
                        TMP_IR_USAGE objTmp = new TMP_IR_USAGE();
                        string strMeterCode = drTmpUage["METER_CODE"].ToString();
                        objTmp.END_USAGE = UIHelper.Floor((decimal)drTmpUage["TOTAL_POWER"], 0);
                        objTmp.BOX_CODE = "";
                        objTmp.IS_DIGITAL = (bool)drTmpUage["IS_DIGITAL"];
                        objTmp.IS_READ_IR = (bool)drTmpUage["IS_READ_IR"];
                        objTmp.IS_METER_RENEW_CYCLE = false;
                        objTmp.TAMPER_STATUS = "00";

                        if (strMeterCode.Trim() != "" && objTmp.END_USAGE > -1)
                        {
                            objTmp.METER_CODE = Method.FormatMeterCode(strMeterCode);
                            objTmp.REGISTER_CODE = Method.GetMETER_REGCODE(objTmp.METER_CODE, objTmp.IS_DIGITAL);
                            DBDataContext.Db.TMP_IR_USAGEs.InsertOnSubmit(objTmp);

                        }
                    }
                    DBDataContext.Db.SubmitChanges();
                    #endregion


                    this.RunIRUsage(intDeviceID, intCollectorID);

                    this.TotalCollectCustomer = DBDataContext.Db.TMP_IR_USAGEs.Count();
                    tran.Complete();
                    blnReturn = true;
                }
                return blnReturn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RunIRUsage(int intDeviceID, int intCollectorID)
        {
            UsageDeviceCollection = new TBL_USAGE_DEVICE_COLLECTION();
            var json = DBDataContext.Db.TMP_IR_USAGEs._ToJson();
            UsageDeviceCollection.DATA = Encoding.Unicode.GetBytes(json);
            DBDataContext.Db.ExecuteCommand("EXEC RUN_IR_USAGE @p0,@p1,@p2", intDeviceID, intCollectorID, Login.CurrentLogin.LOGIN_NAME);
        }

        public void RunIRRelayMeter(int intDeviceID)
        {
            UsageDeviceCollection = new TBL_USAGE_DEVICE_COLLECTION();
            var json = DBDataContext.Db.TMP_IR_RELAY_METERs._ToJson();
            UsageDeviceCollection.DATA = Encoding.Unicode.GetBytes(json);
            DBDataContext.Db.ExecuteCommand("EXEC RUN_ORDER_RELAY_METER @p0,@p1", intDeviceID, Login.CurrentLogin.LOGIN_NAME);
        }

        public void RunIRTamperMeter(int intDeviceID, int intCollectorID)
        {
            UsageDeviceCollection = new TBL_USAGE_DEVICE_COLLECTION();
            var json = DBDataContext.Db.TMP_IR_TAMPER_METERs._ToJson();
            UsageDeviceCollection.DATA = Encoding.Unicode.GetBytes(json);
            DBDataContext.Db.ExecuteCommand("EXEC RUN_IR_TAMPER_METER @p0,@p1,@p2", intDeviceID, intCollectorID, Login.CurrentLogin.LOGIN_NAME);
        }

        public void LogUploadUsage(TBL_USAGE_DEVICE_COLLECTION obj)
        {
            obj.TRANSFER_TYPE_ID = (int)TransferType.UPLOAD_USAGE;
            obj.CREATE_BY = Login.CurrentLogin.LOGIN_NAME;
            obj.CREATE_ON = DBDataContext.Db.GetSystemDate();
            DBDataContext.Db.TBL_USAGE_DEVICE_COLLECTIONs.InsertOnSubmit(obj);
            DBDataContext.Db.SubmitChanges();
        }
        public void LogDownloadUsage(TBL_USAGE_DEVICE_COLLECTION obj)
        {
            obj.TRANSFER_TYPE_ID = (int)TransferType.DOWNLOD_USAGE;
            obj.CREATE_BY = Login.CurrentLogin.LOGIN_NAME;
            obj.CREATE_ON = DBDataContext.Db.GetSystemDate();
            DBDataContext.Db.TBL_USAGE_DEVICE_COLLECTIONs.InsertOnSubmit(obj);
            DBDataContext.Db.SubmitChanges();
        }

        public static bool SendOrderRelayToIR(List<TBL_RELAY_METER> rows, bool internet)
        {
            string deviceCode = "";
            bool isSend = false;
            var ir = (from e in rows
                      orderby e.AREA_CODE, e.POLE_CODE, e.BOX_CODE, e.METER_CODE
                      select new IrMobile.Models.TBL_RELAY_METER()
                      {
                          ID = 0,
                          RELAY_METER_ID = e.RELAY_METER_ID,
                          FULL_NAME = e.FULL_NAME,
                          METER_CODE = e.METER_CODE,
                          BOX_CODE = e.BOX_CODE,
                          POLE_CODE = e.POLE_CODE,
                          AREA_CODE = e.AREA_CODE,
                          CREATE_ON = e.CREATE_ON,
                          ORDER_RELAY_STATUS = e.ORDER_RELAY_STATUS,
                          METER_RELAY_STATUS = e.METER_RELAY_STATUS
                      }).ToList();

            if (ir.Count() == 0)
            {
                MsgBox.ShowInformation(Resources.MS_NO_CUSTOMER_TO_BLOCK_AND_UNBLOCK);
                return isSend;
            }
            if (!internet)
            {
                Runner.RunNewThread(() =>
                {
                    // Connect & check device identify 
                    var d = IRDevice.Connect();

                    if (!(d is WinCE) && !(d is Android))
                    {
                        throw new MsgBox.MessageException(Resources.MS_DEVICE_NO_CUSTOMER_BLOCK_AND_UNBLOCK);
                    }
                    IRDevice.Validate(d);
                    deviceCode = d.ProductID;
                    // Get data from GF1100
                    Runner.Instance.Text = string.Format(Resources.MS_BACKUP_DATA_IN_DEVICE, string.Empty, string.Empty);
                    d.GetFile(IRDevice.BackupType.BeforeSFTD);
                    // Get Order relay meter
                    Runner.Instance.Text = Resources.MS_PREPARING_DATA;
                    if (d is WinCE)
                    {
                        ((WinCE)d).GetRelayMeter(ir);
                    }
                    else if (d is Android)
                    {
                        ((Android)d).GetRelayMeter(ir);
                    }
                    // Send Data to GF1100
                    Runner.Instance.Text = string.Format(Resources.MS_SEND_FILE_TO_DEVICE, string.Empty, string.Empty);
                    isSend = d.SendFile();
                });
            }
            else
            {
                Runner.RunNewThread(() =>
                {
                    var id = 1;
                    DataSet ds = new DataSet();
                    Runner.Instance.Text = Resources.MS_PREPARING_DATA;
                    List<TBL_RELAY_METER_IR> relayMeter = new List<TBL_RELAY_METER_IR>();

                    foreach (var row in ir)
                    {
                        var relay = new TBL_RELAY_METER_IR();
                        relay.RELAY_METER_ID = row.RELAY_METER_ID;
                        relay.METER_CODE = row.METER_CODE;
                        relay.AREA_CODE = row.AREA_CODE;
                        relay.POLE_CODE = row.POLE_CODE;
                        relay.BOX_CODE = row.BOX_CODE;
                        relay.CREATE_ON = new DateTime(1900, 1, 1);
                        relay.FULL_NAME = row.FULL_NAME;
                        relay.METER_RELAY_STATUS = row.METER_RELAY_STATUS;
                        relay.ORDER_RELAY_STATUS = row.ORDER_RELAY_STATUS;
                        relay.ID = id;
                        relayMeter.Add(relay);

                        id++;
                    }

                    var customers = new List<IrMobile.Models.TBL_CUSTOMER>()._ToDataTable();
                    var data = relayMeter._ToDataTable();
                    data.TableName = "relayMeter";
                    customers.TableName = "customer";
                    ds.Tables.Add(data);
                    ds.Tables.Add(customers);

                    var uploadBody = new UploadBody()
                    {
                        destination_id = "*",
                        override_last = true,
                        data = ds
                    };
                    Runner.Instance.Text = "Uploading...";
                    HB02_API.Instance.Upload(uploadBody);
                    isSend = true;
                }, "Exporting...");
            }

            if (!isSend)
            {
                return isSend;
            }
            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    TBL_ORDER_RELAY_METER obj = new TBL_ORDER_RELAY_METER();
                    obj.CREATE_BY = Login.CurrentLogin.LOGIN_NAME;
                    obj.CREATE_ON = DBDataContext.Db.GetSystemDate();
                    obj.DEVICE_ID = DBDataContext.Db.TBL_DEVICEs.FirstOrDefault(x => x.DEVICE_CODE == deviceCode) == null ? -1 : DBDataContext.Db.TBL_DEVICEs.FirstOrDefault(x => x.DEVICE_CODE == deviceCode).DEVICE_ID;
                    DBDataContext.Db.TBL_ORDER_RELAY_METERs.InsertOnSubmit(obj);
                    DBDataContext.Db.SubmitChanges();

                    rows.ForEach(x => x.SEND_ORDER_RELAY_METER_ID = obj.ORDER_RELAY_METER_ID);
                    DBDataContext.Db.TBL_RELAY_METERs.InsertAllOnSubmit(rows);
                    DBDataContext.Db.SubmitChanges();
                    tran.Complete();
                }
                MsgBox.ShowInformation(Resources.UPLOAD_DATA_SUCCESS);
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp);
            }
            return isSend;
        }

        public static void ReceiveRelayByWired()
        {
            try
            {
                var success = false;
                Runner.RunNewThread(() =>
                {
                    // Connect & check device identify 
                    var d = Connect();

                    if (!(d is WinCE) && !(d is Android))
                    {
                        throw new MsgBox.MessageException(Resources.MS_DEVICE_NO_CUSTOMER_BLOCK_AND_UNBLOCK);
                    }
                    Validate(d);
                    var objDevice = DBDataContext.Db.TBL_DEVICEs.FirstOrDefault(x => x.DEVICE_CODE == d.ProductID);
                    // Get data from GF1100
                    Runner.Instance.Text = string.Format(Resources.MS_BACKUP_DATA_IN_DEVICE, objDevice.DEVICE_CODE, string.Empty);

                    if (d is WinCE)
                    {
                        d.GetFile(BackupType.AfterCPFD);
                        //Runner.Instance.Text = Resources.SUCCESS;
                        ((WinCE)d).SaveRelayMeter(objDevice.DEVICE_ID);
                    }
                    else if (d is Android)
                    {
                        d.GetRelayData(BackupType.AfterCPFD);
                        //Runner.Instance.Text = Resources.SUCCESS;
                        ((Android)d).SaveRelayMeter(objDevice.DEVICE_ID);
                    }
                    success = true;
                });
                if (success)
                {
                    MsgBox.ShowInformation(Resources.DOWNLOAD_DATA_SUCCESS);
                }
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp);
            }
        }

        public static void ReceiveRelayByInternet()
        {
            try
            {
                var success = false;
                Runner.RunNewThread(() =>
                {
                    var customer = 0;
                    var android = new Android();
                    var deviceName = "Online";
                    var deviceCode = deviceName;
                    var pendingDownload = HB02_API.Instance.PendingDownload();

                    using (var tran = new TransactionScope(TransactionScopeOption.Required, DBDataContext.Db.TransactionOption()))
                    {
                        DBDataContext.Db.ExecuteCommand("TRUNCATE TABLE TMP_IR_RELAY_METER");
                        foreach (var upload_id in pendingDownload)
                        {
                            Runner.Instance.Text = "Downloading...";
                            var json = HB02_API.Instance.Download(upload_id);
                            Runner.Instance.Text = "Importing...";
                            var relays = android.toIrRelay(json);
                            //foreach (var row in relays)
                            //{
                            DBDataContext.Db.TMP_IR_RELAY_METERs.InsertAllOnSubmit(relays);
                            //}
                            //DBDataContext.Db.BulkCopy(relays);
                            customer += relays.Count;
                        }
                        DBDataContext.Db.SubmitChanges();
                        android.RunIRRelayMeter(-1);
                        Runner.Instance.Text = "Validating...";
                        pendingDownload.ForEach(x => HB02_API.Instance.Success(x));
                        tran.Complete();
                    }

                    /*
                     * Log download usage transaction 
                     */
                    var usageDeviceCollection = android.UsageDeviceCollection;
                    usageDeviceCollection.AREA = "";
                    usageDeviceCollection.CYCLE = "";
                    usageDeviceCollection.MONTH = "";
                    usageDeviceCollection.DEVICE_CODE = deviceCode;
                    usageDeviceCollection.DEVICE_NAME = deviceName;
                    usageDeviceCollection.RECORD = customer;
                    usageDeviceCollection.TRANSMISSION_TYPE_ID = (int)TransmissionOption.Internet;
                    usageDeviceCollection.DATA_BACKUP = Encoding.Unicode.GetBytes(new List<TMP_IR_RELAY_METER>()._ToJson());
                    android.LogDownloadUsage(usageDeviceCollection);
                    success = true;
                }, "Downloading...");

                //Successfully save data to db
                if (success)
                {
                    MsgBox.ShowInformation(Resources.DOWNLOAD_DATA_SUCCESS);
                }
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp);
            }

        }


        /// <summary>
        /// Load to save in database
        /// Backup file to local storage
        /// </summary>
        /// <param name="strErrorMessage"></param>
        /// <returns></returns>
        private DataTable LoadAndBackupUsage()
        {
            DataTable dtReturn = new DataTable();
            dtReturn.Columns.Add("METER_CODE", typeof(string));
            dtReturn.Columns.Add("TOTAL_POWER", typeof(decimal));
            dtReturn.Columns.Add("CONSTANT", typeof(decimal));
            try
            {

                using (BinaryReader b = new BinaryReader(File.Open(Settings.Default.IR_FILE_DB_LOCAL, FileMode.Open, FileAccess.Read)))
                {
                    int header, recordNum, recordSize;
                    long seek;
                    byte[] dbfInfo = new byte[4];
                    byte[] data;
                    string meterCode, endUsage, constant;

                    b.BaseStream.Seek(8, SeekOrigin.Begin);
                    b.Read(dbfInfo, 0, dbfInfo.Length);
                    header = dbfInfo[0] + (dbfInfo[1] << 8);

                    b.BaseStream.Seek(4, SeekOrigin.Begin);
                    b.Read(dbfInfo, 0, dbfInfo.Length);
                    recordNum = dbfInfo[0] + (dbfInfo[1] << 8);

                    b.BaseStream.Seek(10, SeekOrigin.Begin);
                    b.Read(dbfInfo, 0, dbfInfo.Length);
                    recordSize = dbfInfo[0] + (dbfInfo[1] << 8);

                    data = new byte[recordSize];
                    header++;

                    recordNum = (int)(b.BaseStream.Length - header) / recordSize;

                    //UPDATE IS_DIGITAL TO DATABASE
                    using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                    {

                        for (int i = 0; i < recordNum; i++)
                        {
                            seek = header + i * recordSize;
                            b.BaseStream.Seek(seek, SeekOrigin.Begin);
                            b.Read(data, 0, recordSize);

                            Encoding s = Encoding.ASCII;
                            meterCode = new string(s.GetChars(data, 0, 12));
                            endUsage = new string(s.GetChars(data, 12, 9));
                            constant = new string(s.GetChars(data, 21, 6));
                            meterCode = RemoveNull(meterCode);
                            endUsage = RemoveNull(endUsage);
                            constant = RemoveNull(constant);

                            if (DataHelper.ParseToInt(constant) == -1)
                            {
                                continue;
                            }

                            TBL_METER objMeter = DBDataContext.Db.TBL_METERs.FirstOrDefault(x => x.METER_CODE.Equals(meterCode));
                            if (objMeter != null)
                            {
                                if ((objMeter.IS_DIGITAL == false) && DataHelper.ParseToInt(constant) > 1)
                                {
                                    objMeter.IS_DIGITAL = true;
                                }

                            }
                            dtReturn.Rows.Add(meterCode, DataHelper.ParseToDecimal(endUsage), DataHelper.ParseToDecimal(constant));
                        }
                        DBDataContext.Db.SubmitChanges();
                        tran.Complete();
                    }

                    var objIR = from dr in dtReturn.Select()
                                .Where(x => DataHelper.ParseToInt(x["CONSTANT"].ToString()) != (int)IRReaderEnergy.DefaultConstant
                                       && x["METER_CODE"].ToString().Trim() != string.Empty)
                                join m in DBDataContext.Db.TBL_METERs.ToList()
                                on dr["METER_CODE"].ToString() equals m.METER_CODE into temMeter
                                from m in temMeter.DefaultIfEmpty()
                                select new
                                {
                                    METER_CODE = dr["METER_CODE"].ToString(),
                                    TOTAL_POWER = DataHelper.ParseToDecimal(dr["TOTAL_POWER"].ToString()),
                                    CONSTANT = DataHelper.ParseToDecimal(dr["CONSTANT"].ToString()),
                                    IS_DIGITAL = m == null ? false : m.IS_DIGITAL
                                };
                    dtReturn = objIR._ToDataTable();
                    //dbConn.Close(); 
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
            return dtReturn;
        }

        private string RemoveNull(string str)
        {
            if (str.IndexOf('\0') == -1)
            {
                return str;
            }

            return str.Remove(str.IndexOf('\0'));
        }
        public virtual List<GET_MONTHLY_USAGE_RESULT> GetMonthlyUsages(List<TBL_BILLING_CYCLE> mCycles, List<TBL_AREA> mAreas)
        {
            DBDataContext.Db.CommandTimeout = (int)TimeSpan.FromMinutes(5).TotalMilliseconds;
            var cycles = ";" + string.Join(";", mCycles.Select(x => x.CYCLE_ID.ToString()).ToArray()) + ";";
            var areas = ";" + string.Join(";", mAreas.Select(x => x.AREA_ID.ToString()).ToArray()) + ";";
            var usages = DBDataContext.Db.ExecuteQuery<GET_MONTHLY_USAGE_RESULT>(@"EXEC dbo.GET_MONTHLY_USAGE @p0, @p1", areas, cycles).ToList();

            UsageDeviceCollection = new TBL_USAGE_DEVICE_COLLECTION();
            var json = usages._ToJson();
            UsageDeviceCollection.DATA = Encoding.Unicode.GetBytes(json);

            return usages;
        }
        public virtual int GetUsage(List<TBL_BILLING_CYCLE> mCycle, List<TBL_AREA> mArea, ref string month, bool newDb = true)
        {
            month = string.Empty;

            int totalCustomer = 0;
            if (File.Exists(Path.Combine(_TmpPath, _TmpDDSb))
                && File.Exists(Path.Combine(_TmpPath, _TmpDDSc))
                && File.Exists(Path.Combine(_TmpPath, _TmpInfo)))
            {
                File.Copy(Path.Combine(_TmpPath, _TmpDDSb), Path.Combine(_TmpPath, _DDSb), true);
                File.Copy(Path.Combine(_TmpPath, _TmpDDSc), Path.Combine(_TmpPath, _DDSc), true);
                File.Copy(Path.Combine(_TmpPath, _TmpInfo), Path.Combine(_TmpPath, _DBInfo), true);
            }
            else
            {
                throw new Exception(Resources.MS_TAMPLATE_DEVICE_DATABASE_NOT_FOUND);
            }

            var usages = this.GetMonthlyUsages(mCycle, mArea);

            //get data from local file
            OleDbConnection dbConn = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=Template\;Extended Properties=dBASE IV;");

            dbConn.Open();
            OleDbTransaction tran = dbConn.BeginTransaction(IsolationLevel.ReadCommitted);

            string strSQLVal = @"INSERT INTO DDSc
                                   ([METER_CODE]
                                   ,[END_USAGE]
                                   ,[LAST_TOTAL]
                                   ,[IS_DIGITAL])
                             VALUES
                                   (@METER_CODE
                                   ,@END_USAGE
                                   ,@LAST_TOTAL
                                   ,@IS_DIGITAL);";

            int k = 1;
            totalCustomer = usages.Count;

            foreach (var us in usages.OrderBy(x => x.METER_CODE))
            {
                Application.DoEvents();
                OleDbCommand cmdVal = new OleDbCommand(strSQLVal, dbConn, tran);
                cmdVal.Parameters.Add(new OleDbParameter("@METER_CODE", OleDbType.VarChar, 12) { Value = us.METER_CODE });
                cmdVal.Parameters.Add(new OleDbParameter("@END_USAGE", OleDbType.Single) { Value = DataHelper.ParseToSingle(us.START_USAGE.ToString()) });
                cmdVal.Parameters.Add(new OleDbParameter("@LAST_TOTAL", OleDbType.Single) { Value = DataHelper.ParseToSingle(us.LAST_TOTAL_USAGE.ToString()) });
                cmdVal.Parameters.Add(new OleDbParameter("@IS_DIGITAL", OleDbType.Integer) { Value = us.IS_DIGITAL });
                cmdVal.ExecuteNonQuery();
                Runner.Instance.Text = string.Format(Resources.MS_PREPARING_DATABASE, k++ / 2 * 100 / totalCustomer, "%");
            }

            strSQLVal = @"INSERT INTO DDSb 
                             VALUES
                                   (@METER_CODE
                                   ,@END_USAGE
                                   ,@CONSTANT);";

            foreach (var us in usages.OrderBy(x => x.METER_CODE))
            {
                Application.DoEvents();
                OleDbCommand cmdVal = new OleDbCommand(strSQLVal, dbConn, tran);
                cmdVal.Parameters.Add(new OleDbParameter("@METER_CODE", OleDbType.VarChar, 12) { Value = us.METER_CODE });
                cmdVal.Parameters.Add(new OleDbParameter("@END_USAGE", OleDbType.Single) { Value = 0 });
                cmdVal.Parameters.Add(new OleDbParameter("@CONSTANT", OleDbType.Single) { Value = -1 });
                cmdVal.ExecuteNonQuery();
                Runner.Instance.Text = string.Format(Resources.MS_PREPARING_DATABASE, k++ / 2 * 100 / totalCustomer, "%");
            }

            strSQLVal = @"INSERT INTO DBInfo 
                             VALUES
                                   (@NO
                                   ,@DB_NAME
                                   ,@RECORD);";
            OleDbCommand cmd = new OleDbCommand(strSQLVal, dbConn, tran);
            cmd.Parameters.Add(new OleDbParameter("@NO", OleDbType.Single) { Value = 1 });
            cmd.Parameters.Add(new OleDbParameter("@DB_NAME", OleDbType.VarChar, 12) { Value = _DDSb });
            cmd.Parameters.Add(new OleDbParameter("@RECORD", OleDbType.Single) { Value = totalCustomer });
            cmd.ExecuteNonQuery();

            cmd = new OleDbCommand(strSQLVal, dbConn, tran);
            cmd.Parameters.Add(new OleDbParameter("@NO", OleDbType.Single) { Value = 2 });
            cmd.Parameters.Add(new OleDbParameter("@DB_NAME", OleDbType.VarChar, 12) { Value = _DDSc });
            cmd.Parameters.Add(new OleDbParameter("@RECORD", OleDbType.Single) { Value = totalCustomer });
            cmd.ExecuteNonQuery();

            tran.Commit();
            dbConn.Close();

            month = month.TrimEnd(new char[] { ',', ' ' });
            return totalCustomer;
        }

        public class GET_MONTHLY_USAGE_RESULT
        {
            public string CUSTOMER_CODE { get; set; }
            public string METER_CODE { get; set; }
            public string FULL_NAME { get; set; }
            public string AREA_CODE { get; set; }
            public string POLE_CODE { get; set; }
            public string BOX_CODE { get; set; }
            public bool IS_DIGITAL { get; set; }
            public int MULTIPLIER { get; set; }
            public bool IS_METER_REGISTER { get; set; }
            public decimal START_USAGE { get; set; }
            public decimal END_USAGE { get; set; }
            public decimal LAST_TOTAL_USAGE { get; set; }
            public bool IS_METER_RENEW_CYCLE { get; set; }
            public bool IS_READ_IR { get; set; }
            public string USAGE_HISTORY { get; set; }
            public int PHASE_ID { get; set; }
            public bool IS_BLUETOOTH { get; set; }
        }

    }
}
