using System;
using System.Collections;
using System.IO;
using System.IO.Ports;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace EPower.Logic.IRDevice
{
    class BlueStarSerialPortPR510 : SerialPort
    {
        public string ErrorMessage { get; set; }


        /// <summary>
        /// open the port as per given port name and baud rate
        /// </summary>
        /// <param name="port"></param>
        /// <param name="baud"></param>
        /// <returns></returns>
        public bool _Open(string portName, string baud)
        {
            if (this.IsOpen == true)
                this.Close();

            this.BaudRate = Convert.ToInt32(baud);
            this.DataBits = 8;
            this.StopBits = StopBits.One;
            this.Parity = Parity.None;


            //this.DtrEnable = true;
            //this.RtsEnable = true;

            if (portName == null)
                this.PortName = "Com1";
            else
                this.PortName = portName;

            try
            {
                this.Open();
                return true;
            }
            catch (Exception)
            {
                throw new Exception("Could not open serial port."); 
            }

        }

        /// <summary>
        /// list the files *.* on HHU C:\
        /// </summary>
        /// <returns></returns>
        public ArrayList _ListFilesObsolete()
        {
            if (this.IsOpen == false)
                return null;     // port is not open

            ArrayList list = new ArrayList();
            byte[] buffer = new byte[1000]; // incoming buffer
            buffer.Initialize();
            int length = 0;                 // the valid data length (read from HHU) in buffer 

            // 1. Send request to HHU
            this.Write(new byte[] {0x01, 0x00, 0x00, 0x46, 0x00, 0x08, 0x00, 0x4C, 0x06, 0x43, 0x3A, 0x2A, 0x2E, 0x2A, 0x00, 0xB1, 0x34 } , 0, 17);

            this.ReadTimeout = 1500;
            Thread.Sleep(125);

            // 2. Read the ACK frame
            length = Read(buffer, 0, 9);

            if( buffer[4] != 'Y')   
                return null;        // not ACK frame, something wrong

            // 3.  Continue reading the rest of data
            // 3.1 read the data length
            Thread.Sleep(225);
            length += Read(buffer, length, 7);
            if (buffer[13] != 'D')
                return null;             // not ACK frame, something wrong

            // 3.2 Calculate remaining length
            int lengthToRead = (byte)buffer[14] + (byte)buffer[15] * 256;        // length byte Low and High
 
            // 3.3 Read remaining
            while(length < (lengthToRead + 7 + 9 + 2) )
            {
                Thread.Sleep(25);
                length += Read(buffer, length, 1000 - length);
            }

            // 4. Frame #2
            this.Write(new byte[] { 0x01, 0x00, 0x00, 0x59, 0x01, 0x00, 0x00, 0x09, 0x1D } , 0, 9);

            while (length < (522+135))
            {
                Thread.Sleep(25);
                length += Read(buffer, length, 1000 - length);
            }


            this.Close();

            return list;
        }

        /// <summary> Read file from HHU
        /// read the file List from HHU
        /// </summary>
        /// <returns></returns>
        public ArrayList _ListFile()
        {
            if (this.IsOpen == false)
                return null;     // port is not open

            ArrayList list = new ArrayList();

            try
            {
                // 1. Send request to HHU
                this.Write(new byte[] { 0x01, 0x00, 0x00, 0x46, 0x00, 0x08, 0x00, 0x4C, 0x06, 0x43, 0x3A, 0x2A, 0x2E, 0x2A, 0x00, 0xB1, 0x34 } , 0, 17);

                this.ReadTimeout = 1500;
                Thread.Sleep(125);

                // 2. Read the first ACK frame
                byte[] frameACK = this.ReadOneFrame();
                if (frameACK == null)       // time out error
                    throw new Exception();

                // 3. Read the First Data Frame
                byte[] frameData = this.ReadOneFrame();
                if (frameData == null)       // time out error
                    throw new Exception();


                //string str = Convert.ToString(.tostring(frameData, 15, 8);
                ArrayList list2 = GetFilesFromBuffer(frameData);


                // Loop here until the EndFrame is received
                byte fNumber = 1;   // frame number from PC
                while (true)
                {
                    // Send ACK Frame so HHU will continue
                    //this.Write(new byte[] { 0x01, 0x00, 0x00, 0x59, 0x01, 0x00, 0x00, 0x09, 0x1D } , 0, 9);
                    this.SendACKFrame(fNumber++);

                    Thread.Sleep(125);
                    byte[] frameData2 = this.ReadOneFrame();
                    if (frameData2 == null)       // time out error
                    { 
                        throw new Exception();
                    }
                    else if (this.IsEndFrame(frameData2))
                    {
                        // close and return;
                        this.SendACKFrame(fNumber++);
                        this.SendENDFrame(fNumber);  
                        return list2;
                    }
                    else
                    {
                        // this is good frame, add to the file list
                        ArrayList listMore = GetFilesFromBuffer(frameData2);
                        foreach (string s in listMore)
                            list2.Add(s);
                    }
                } 
            }
            catch (Exception)
            {
                // MessageBox.Show("Timeout error");
                return null;
            }
        }

        /// <summary> Delete file from HHU
        /// </summary>
        /// <returns></returns>
        public void _DeleteFile(string name)
        {
            name = name.TrimEnd(new char[] { '\0'});

            if (this.IsOpen == false)
                return;     // port is not open

            name = name.ToUpper();

            // Request Command
            // 0    1    2    3    4    5    6      7    8                    9    10   11   12   13   14   15   16
            // 0x01 0x00 0x00 0x46 0x00 0x08 0x00   0x44 0x06                 0x43 0x3A 0x53 0x2E 0x54 0x00 0x88 0xC5 
            // SOH  HHU    ID REQ  F#   Data Length D    File Name Length     C    :    s    .    t    \0   CRC L   H   
            byte[] head = new byte[] { 0x01, 0x00, 0x00, 0x46, 0x00, 0x08, 0x00, 0x44, 0x06, 0x43, 0x3A };    // head of request frame
            byte[] request = new byte[7 + 4 + 3 + name.Length];
            head.CopyTo(request, 0);

            // file name length
            request[8] = (byte)(name.Length + 3);   // C:FileName\0, 3 for C:\0
            // frame data length
            request[5] = (byte)(request[8] + 2);    // 0x52 and Filename Length 

            byte[] bFile = Encoding.GetBytes(name); // bytes of file name
            bFile.CopyTo(request, 11);              // file name starts from [11]

            // Calculate CRC bytes
            UInt16 crcCode = CRCcode(request);      //调用CRC校验方法获取校验码
            request[request.Length - 2] = Convert.ToByte(crcCode >> 8);//将校验码写入帧
            request[request.Length - 1] = Convert.ToByte(crcCode % 256);

            // 1. Send request to HHU
            this.Write(request, 0, request.Length);

            this.ReadTimeout = 1500;
            Thread.Sleep(125);

            // 2. Read the first ACK frame
            byte[] frameACK = this.ReadOneFrame();

        }

        /// <summary>
        /// read the file from HHU
        /// </summary>
        /// <returns></returns>
        public string _ReadFile(string name)
        { 
            if (this.IsOpen == false)
                return null;     // port is not open

            name = name.ToUpper();

            // Request Command
            // 0    1    2    3    4    5    6      7    8                    9    10   11   12   13   14   15   16
            // 0x01 0x00 0x00 0x46 0x00 0x08 0x00   0x52 0x06                 0x43 0x3A 0x53 0x2E 0x54 0x00 0x88 0xC5 
            // SOH  HHU    ID REQ  F#   Data Length R    File Name Length     C    :    s    .    t    \0   CRC L   H   
            byte[] head = new byte[] {0x01, 0x00, 0x00, 0x46, 0x00, 0x08, 0x00, 0x52, 0x06, 0x43, 0x3A};    // head of request frame
            byte[] request = new byte[7 + 4 + 3 + name.Length];
            head.CopyTo(request, 0);
            
            // file name length
            request[8] = (byte)(name.Length + 3);   // C:FileName\0, 3 for C:\0
            // frame data length
            request[5] = (byte)(request[8] + 2);    // 0x52 and Filename Length 

            byte[] bFile = Encoding.GetBytes(name); // bytes of file name
            bFile.CopyTo(request, 11);              // file name starts from [11]

            // Calculate CRC bytes
            UInt16 crcCode = CRCcode(request);      //调用CRC校验方法获取校验码
            request[request.Length-2] = Convert.ToByte(crcCode >> 8);//将校验码写入帧
            request[request.Length-1] = Convert.ToByte(crcCode % 256);

            // Request for Meter.info, the actual frame
            //0x01 0x00 0x00 0x46 0x00 0x0F 0x00 0x52 0x0D 0x43 0x3A 
            //0x4D 0x45 0x54 0x45 0x52 0x2E 0x49 0x4E 0x46 0x4F 0x00 0x21 0x4B

            // 1. Send request to HHU
            this.Write(request, 0, request.Length);

            this.ReadTimeout = 1500;
            Thread.Sleep(125);

            // 2. Read the first ACK frame
            byte[] frameACK = this.ReadOneFrame();
            if (frameACK == null) return "";

            // 3. Read the First Data Frame - File Attribute
            byte[] frameFileAttribute = this.ReadOneFrame();
            if (frameFileAttribute == null) return "";
            
            ////string str = Convert.ToString(.tostring(frameData, 15, 8);
            //ArrayList list2 = GetFilesFromBuffer(frameData);


            // Loop here until the EndFrame is received
            string strFile = string.Empty;  // string of the file， for now we use string, should use byte [] really 
            byte fNumber = 1;   // frame number from PC
            int retry = 3;
            while (true)
            {
                retry --;

                // Send ACK Frame so HHU will continue
                this.SendACKFrame(fNumber++);

                // Read one frame
                Thread.Sleep(125);
                byte[] frameData2 = this.ReadOneFrame();

                if (this.IsEndFrame(frameData2))
                {
                    // close and return;
                    this.SendACKFrame(fNumber++);
                    // this.Close();
                    return strFile;
                }
                else if (frameData2 == null)
                {
                    if (retry == 0)
                        return null; ; // 3 times wrong, so we give up.
                    continue;   // 
                }
                else
                {
                    retry = 3;  // we have 3 chances 
                    // not end frame, an OK Data frame
                    strFile += Encoding.GetString(frameData2, 7, frameData2[6] * 256 + frameData2[5]);
                }
            }

            //return null;
        }
        /// <summary>
        /// read the file from HHU
        /// </summary>
        /// <returns></returns>
        public bool _ReadSaveFile(string name, string path)//, FormCommunicating formComm)
        {
            if (this.IsOpen == false)
                return false;     // port is not open

            name = name.ToUpper();

            // Request Command
            // 0    1    2    3    4    5    6      7    8                    9    10   11   12   13   14   15   16
            // 0x01 0x00 0x00 0x46 0x00 0x08 0x00   0x52 0x06                 0x43 0x3A 0x53 0x2E 0x54 0x00 0x88 0xC5 
            // SOH  HHU    ID REQ  F#   Data Length R    File Name Length     C    :    s    .    t    \0   CRC L   H   
            byte[] head = new byte[] { 0x01, 0x00, 0x00, 0x46, 0x00, 0x08, 0x00, 0x52, 0x06, 0x43, 0x3A };    // head of request frame
            byte[] request = new byte[7 + 4 + 3 + name.Length];
            head.CopyTo(request, 0);

            // file name length
            request[8] = (byte)(name.Length + 3);   // C:FileName\0, 3 for C:\0
            // frame data length
            request[5] = (byte)(request[8] + 2);    // 0x52 and Filename Length 

            byte[] bFile = Encoding.GetBytes(name); // bytes of file name
            bFile.CopyTo(request, 11);              // file name starts from [11]

            // Calculate CRC bytes
            UInt16 crcCode = CRCcode(request);      //调用CRC校验方法获取校验码
            request[request.Length - 2] = Convert.ToByte(crcCode >> 8);//将校验码写入帧
            request[request.Length - 1] = Convert.ToByte(crcCode % 256);

            // Request for Meter.info, the actual frame
            //0x01 0x00 0x00 0x46 0x00 0x0F 0x00 0x52 0x0D 0x43 0x3A 
            //0x4D 0x45 0x54 0x45 0x52 0x2E 0x49 0x4E 0x46 0x4F 0x00 0x21 0x4B

            // 1. Send request to HHU
            this.Write(request, 0, request.Length);

            this.ReadTimeout = 1500;
            Thread.Sleep(150);

            // 2. Read the first ACK frame
            Thread.Sleep(150);
            byte[] frameACK = this.ReadOneFrame();
            if (frameACK == null) return false;

            // 3. Read the First Data Frame - File Attribute
            Thread.Sleep(150);
            byte[] frameFileAttribute = this.ReadOneFrame();
            if (frameFileAttribute == null) return false;

            Thread.Sleep(150);
            byte fNumber = 1;   // frame number from PC
            this.SendACKFrame(fNumber++); // ACK Frame to File Attribute Frame

            using (FileStream fs = File.Create(path + "\\" + name))
            // create the fils stream to be written
            {
                int retry = 3;
                while (true)
                {
                    retry--;

                    Application.DoEvents();
                    //if( formComm.UserCancelled == true)
                    //    return false;   // quit, because user has clicked Cancel

                    int offset = 0;

                    // Send ACK Frame so HHU will continue
                    //this.SendACKFrame(fNumber);       --> Moved up (File Attribute) and Down (After Data FRame)

                    // Read one frame
                    Thread.Sleep(175);
                    byte[] frameData2 = this.ReadOneFrame();

                    if (this.IsEndFrame(frameData2))
                    {
                        // close and return;
                        this.SendACKFrame(fNumber++);
                        fs.Flush();
                        fs.Close();
                        // this.Close();
                        // end frame
                        this.SendENDFrame(fNumber);
                        Thread.Sleep(225);
                        byte[] frameData3 = this.ReadOneFrame();
                        return true;    // ok
                    }
                    else if (frameData2 == null)
                    {
                        if (retry == 0)
                            return false; ; // 3 times wrong, so we give up.

                        // send NACK Frame
                        this.SendNACKFrame(fNumber);    // don't increase frame Number
                        continue;   // 
                    }
                    else // TODO should check if not ERROR Frame
                    {
                        retry = 3;  //s we have 3 chances 
                        // not end frame
                        // write to file
                        // sw.Write(char 

                        // Send ACK to ask for more frames
                        this.SendACKFrame(fNumber++); // ACK Frame to File Attribute Frame

                        fs.Write(frameData2, 7, frameData2[6] * 256 + frameData2[5]);
                        offset += frameData2.Length;

                        // strFile += Encoding.GetEncoding("GB2312").GetString(frameData2, 7, frameData2[6] * 256 + frameData2[5]);
                    }
                }
            }

           

            //return null;
        }

        /// <summary>
        /// parse the file names from the buffer
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        private ArrayList GetFilesFromBuffer(byte[] buffer)
        {
            ArrayList list = new ArrayList();
            try
            {
                int files = buffer.Length / 21;     // each file has 21 bytes, it will be rounded to number of files
                for (int f = 0; f < files; f++)
                {
                    // each file
                    StringBuilder sb = new StringBuilder(12);
                    byte[] ff = new byte[12];



                    for (int c = 0; c < 12; c++)    // 8.3 file name format
                    {
                        //0x01 0x07 0x52 0x02 0x44 0x7E 0x00 
                        //0xE1 0xCE 0xEE 0xEE 0xEE 0xEE 0xEE 0xEE 0x0C 0x4D 0x65 0x74 0x65 0x72 0x56 0x65 0x72 0x2E 0x64 0x61 0x74 

                        // 7 is the frame head
                        // 9 is the file head
                        // sb.Append(Convert.ToString(buffer[9 + 7 + c + 21 * f], 16).PadLeft(2, '0').PadRight(3, ' '));
                        sb.Append(Convert.ToChar(buffer[9 + 7 + c + 21 * f]));
                        
                        //// playing around with Chinese
                        //if (buffer[9 + 7 + c + 21 * f] < 0x80)
                        //{
                        //    // english
                        //    sb.Append(Convert.ToChar(buffer[9 + 7 + c + 21 * f]));
                        //    c++;
                        //}
                        //else
                        //{
                        //    sb.Append((char)(buffer[9 + 7 + c +1 + 21 * f] * 0x100 + buffer[9 + 7 + c + 21 * f]));
                        //    c+=2;
                        //}

                    }
                       
                    list.Add(sb.ToString().ToUpper());
                }

                return list;
            }
            catch (Exception)
            {
                
                return list;
            }
        }

        /// <summary>
        /// send one ack frame from PC to HHU
        /// </summary>
        /// <param name="frame"></param>
        private void SendACKFrame(byte frame)
        {
            byte [] ack = new byte[] { 0x01, 0x00, 0x00, 0x59, 0x01, 0x00, 0x00, 0x00, 0x00};
            ack[4] = frame; // frame #

            UInt16 crcCode = CRCcode(ack);      //调用CRC校验方法获取校验码
            ack[7] = Convert.ToByte(crcCode >> 8);//将校验码写入帧
            ack[8] = Convert.ToByte(crcCode % 256);

            this.Write( ack , 0, 9);
        }
        private void SendNACKFrame(byte frame)
        {
            byte[] ack = new byte[] { 0x01, 0x00, 0x00, 0x4E, 0x01, 0x00, 0x00, 0x00, 0x00 };
            ack[4] = frame; // frame #

            UInt16 crcCode = CRCcode(ack);      //调用CRC校验方法获取校验码
            ack[7] = Convert.ToByte(crcCode >> 8);//将校验码写入帧
            ack[8] = Convert.ToByte(crcCode % 256);

            this.Write(ack, 0, 9);
        }

        /// <summary>
        /// when write file to HHU. send one data frame
        /// </summary>
        /// <param name="frame"></param>
        /// <param name="data"></param>
        private void SendDataFrame(byte frame, byte[] data)
        {
            //                         0     1     2     3     4     Data Length
            byte[] head = new byte[] { 0x01, 0x00, 0x00, 0x44, 0x01, 0x00, 0x00};

            byte[] full = new byte[data.Length + head.Length + 2];      // CRC 2 bytes

            head.CopyTo(full, 0);
            data.CopyTo(full, head.Length);

            full[4] = frame;
            full[6] = (byte)(data.Length >> 8);     // data length Hi byte
            full[5] = (byte)(data.Length % 0x0100);     // data length Low byte

            UInt16 crcCode = CRCcode(full);
            full[full.Length - 2] = Convert.ToByte(crcCode >> 8);        // high byte
            full[full.Length - 1] = Convert.ToByte(crcCode % 0x0100);    // lower byte



            this.Write(full, 0, full.Length);
        }

        /// <summary>
        /// send one END frame from PC to HHU to finish write
        /// </summary>
        /// <param name="frame"></param>
        private void SendENDFrame(byte frame)
        {
            //                        0     1     2     3     4 
            byte[] ack = new byte[] { 0x01, 0x00, 0x00, 0x5A, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00 };

            ack[4] = frame; // frame #

            UInt16 crcCode = CRCcode(ack);      //调用CRC校验方法获取校验码
            ack[9] = Convert.ToByte(crcCode >> 8);//将校验码写入帧
            ack[10] = Convert.ToByte(crcCode % 256);

            this.Write(ack, 0, 11);

        }

        /// <summary>
        /// write the file to HHU
        /// </summary>
        /// <returns></returns>
        public bool _WriteFile(string path, string name)
        {
            path = path + "\\" + name;
            if (File.Exists(path) != true)
            {
                this.ErrorMessage = "File not found!";
                return false; 
            }

            // open this file
            FileStream fs = new FileStream(path, FileMode.Open);

            if (this.IsOpen == false)
            {
                this.ErrorMessage = "Port is not open!";
                return false;     // port is not open
            }

            //name = name.ToUpper();

            // name = "00999C.Bil";
            // Request Command
            // 0    1    2    3    4    5    6      7    8                    9    10   11   12   13   14   15   16
            // 0x01 0x00 0x00 0x46 0x00 0x08 0x00   0x52 0x06                 0x43 0x3A 0x53 0x2E 0x54 0x00 0x88 0xC5 
            // SOH  HHU    ID REQ  F#   Data Length R    File Name Length     C    :    s    .    t    \0   CRC L   H   
            byte[] head = new byte[] { 0x01, 0x00, 0x00, 0x46, 0x00, 0x08, 0x00, 0x57, 0x06, 0x43, 0x3A };    // head of request frame
            byte[] request = new byte[7 + 4 + 3 + name.Length + 22];
            head.CopyTo(request, 0);

            // file name length
            request[8] = (byte)(name.Length + 3);   // C:FileName\0, 3 for C:\0
            // frame data length
            request[5] = (byte)(request[8] + 2 + 22);    // 0x52 and Filename Length 

            // File Size: 4 bytes 
            // request[len-8][len-7][len-6][len-5]
            long ll = fs.Length & 0x0000FFFF;        // lower part
            request[request.Length - 7] = Convert.ToByte(ll >> 8);  // 0x00;
            request[request.Length - 8] = Convert.ToByte(ll % 256); // 0x07;
            ll = (Int16)(fs.Length >> 16);        // higher part
            request[request.Length - 5] = Convert.ToByte(ll >> 8);  // 0x00;
            request[request.Length - 6] = Convert.ToByte(ll % 256); // 0x07;

            // Serial No.
            request[request.Length - 4] = 0x03; // 0x07;
            request[request.Length - 3] = 0x2B; // 0x07;

            // File Attrubute, including Data and time etc
            byte[] fileAttribute = new byte[] { 0x20, 0x02, 0x5C, 0x9A, 0x37 };
            fileAttribute.CopyTo(request, request.Length - 13);

            byte[] bFile = Encoding.GetBytes(name); // bytes of file name
            bFile.CopyTo(request, 11);              // file name starts from [11]

            // Calculate CRC bytes
            UInt16 crcCode = CRCcode(request);      //调用CRC校验方法获取校验码
            request[request.Length - 2] = Convert.ToByte(crcCode >> 8);//将校验码写入帧
            request[request.Length - 1] = Convert.ToByte(crcCode % 256);

            // Request for Meter.info, the actual frame
            //0x01 0x00 0x00 0x46 0x00 0x0F 0x00 0x52 0x0D 0x43 0x3A 
            //0x4D 0x45 0x54 0x45 0x52 0x2E 0x49 0x4E 0x46 0x4F 0x00 0x21 0x4B

            // 1. Send request to HHU
            //byte[] request2 = new byte[] { 0x01, 0x00, 0x00, 0x46, 0x00, 0x25, 0x00, 0x57, 0x0D 
            //    , 0x43, 0x3A, 0x30, 0x30, 0x39, 0x43, 0x43, 0x43, 0x2E, 0x42, 0x69, 0x6C, 0x00, 0x30, 0x30, 0x39, 0x43, 0x43, 0x43, 0x20, 0x20 
            //    , 0x42, 0x69, 0x6C, 0x20, 0xAE, 0xB0, 0x97, 0x37, 0x00, 0x01, 0x00, 0x00, 0x03, 0x2B, 0xA2, 0x64
            //    };
            
            this.Write(request, 0, request.Length);
           
            // MPS Request to write 33.vsd to HHU
            //byte [] requestMPS = new byte[] {0x01, 0x00, 0x00, 0x46, 0x00, 0x21, 0x00, 0x57, 0x09, 0x43, 0x3A, 0x33, 0x33, 0x2E, 0x76, 0x73, 0x64, 0x00, 0x33, 0x33, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x76, 0x73, 0x64, 0x20, 0x02, 0x5C, 0x9A, 0x37, 0x00, 0x5C, 0x00, 0x00, 0x03, 0x2B, 0xD8, 0x49 };
            //this.Write(requestMPS, 0, request.Length);


            this.ReadTimeout = 1500;
            Thread.Sleep(225);

            // 2. Read the first ACK frame
            byte[] frameACK = this.ReadOneFrame();
            if (frameACK == null)
            {
                ErrorMessage = "Fail to transfer data!";
                return false;
            }

            //// 3. Read the First Data Frame - File Attribute
            //byte[] frameFileAttribute = this.ReadOneFrame();
            //if (frameFileAttribute == null) return "";

            ////string str = Convert.ToString(.tostring(frameData, 15, 8);
            //ArrayList list2 = GetFilesFromBuffer(frameData);


            // Loop here until the EndFrame is received
            string strFile = string.Empty;  // string of the file， for now we use string, should use byte [] really 
            byte fNumber = 1;   // frame number from PC
            while (true)
            {
                Int16 frameLength = 512;
                // data frames
                int frames = (int)((fs.Length+frameLength-1) / frameLength);       // one frame is 256 bytes

                for (int i = 0; i < frames; i++)
                {
                    byte[] data;

                    Thread.Sleep(125);
                    // each data frame
                    int seekLength = i * frameLength;
                    fs.Seek(seekLength, SeekOrigin.Begin);
                    int byteRemaining = (int)(fs.Length - seekLength);

                    int read = 0;
                    if( byteRemaining >= frameLength)
                    {
                        data = new byte[frameLength];
                        read = fs.Read(data, 0, frameLength);
                    }
                    else
                    {
                        data = new byte[byteRemaining]; // available less than 512 bytes, only apply for what is left 

                        read = fs.Read(data, 0, byteRemaining);
                    }
                    Thread.Sleep(125);
                    //// Send DATA Frame to HHU 
                    this.SendDataFrame((byte)(fNumber++), data);
                    // this.SendDataFrame((byte)fNumber + i, Encoding.Default.GetBytes("1234567"));

                    // Read ACK frame
                    Thread.Sleep(125);
                    byte[] frameData2 = this.ReadOneFrame();

                    // fNumber++;  // go to next frame - notnecessary because i++ 
                }
                fs.Close();

                // end frame
                this.SendENDFrame((byte)fNumber);
                Thread.Sleep(225);
                byte[] frameData3 = this.ReadOneFrame();


                return true;

              
                ////this.SendACKFrame(fNumber++);

                ////// Send DATA Frame to HHU 
                //this.SendDataFrame((byte)1, Encoding.Default.GetBytes("1234567"));

                //// Read ACK frame
                //Thread.Sleep(125);
                //byte[] frameData2 = this.ReadOneFrame();

                //this.SendENDFrame((byte)2);
                //Thread.Sleep(125);
                //byte[] frameData3 = this.ReadOneFrame();

                //return true;
                //if (this.IsEndFrame(frameData2))
                //{
                //    // close and return;
                //    this.SendACKFrame(fNumber++);
                //    // this.Close();
                //    return false;
                //}
                //else
                //{
                //    // not end frame
                //  //   strFile += Encoding.GetString(frameData2, 7, frameData2[6] * 256 + frameData2[5]);
                //}
            }

            //return null;
        }


        /// <summary>
        /// check if this frame is end frame from HHU
        /// </summary>
        /// <param name="frame"></param>
        /// <returns></returns>
        private bool IsEndFrame(byte[] frame)
        {
            // 0    1    2    3    4    5    6    7    8
            // 0x01 0x07 0x52 0x03 0x5A 0x00 0x00 0x23 0x42 		End of data
            // SOH  HHU ID    F#   TYPE LENGTH    CRC
            try
            {
                if (frame[4] == 0x5A)
                    return true;
            }
            catch (Exception)
            {
                return false;
            }
            
            return false;
        }

        #region            计算CRC校验码的方法
        /// <summary>
        /// 计算CRC校验码的方法
        /// </summary>
        /// <param name="arr">需要校验的数组</param>
        /// <returns>返回校验码</returns>
        public UInt16 CRCcode(byte[] arr)
        {
            int len = arr.Length - 2;
            UInt16 crcCode = 0;
            UInt16 i;
            while (len > 0)
            {
                byte a = arr[arr.Length - 2 - len];
                for (i = 0x80; i != 0; i >>= 1)
                {
                    if ((crcCode & 0x8000) != 0)
                    {
                        crcCode <<= 1;
                        crcCode ^= 0x1021;
                    }
                    else
                        crcCode <<= 1;

                    if ((a & i) != 0)
                        crcCode ^= 0x1021;
                }
                len--;
            }
            return crcCode;
        }
        #endregion

        /// <summary>
        /// read one frame from serial port and fill the buffer
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        private byte [] ReadOneFrame()
        {
            byte[] bufHead = new byte[7];   // shortest is 9 bytes, without CRC is 7

            // frame from HHU
            // 0   1   2   3       4   5   6   
            // 01  00  00  59      01  00  00  09  1D
            // SOH HHU ID  Y (ACK) F#  Length  CRC HL
            this.ReadTimeout = 1500;
            try
            {
                Thread.Sleep(10);   // short break;

                // 1. read the first 9 bytes, without CRC 2 bytes
                this.Read(bufHead, 0, 7);

                // 2.  Calculate remaining length, including 2 CRC bytes
                int lengthToRead = (byte)bufHead[5] + (byte)bufHead[6] * 256 + 2; // length byte Low and High + 2 CRC

                if (lengthToRead > 1024)
                    return null;    //something wrong

                // 3.0 apply for data buffer
                byte[] bufData = new byte[lengthToRead + 7]; 
                bufHead.CopyTo(bufData, 0);
                // bufData.CopyTo(

                // 3. Read the remaining
                int length = 0; // the total length read from HHU
                while (length < lengthToRead )
                {
                    Thread.Sleep(125);
                    //if(this.BytesToRead > 0)
                    length += Read(bufData, length + 7, lengthToRead - length); // 7 is the frame head, 7 bytes
                }

                // check CRC
                UInt16 crcCode = CRCcode(bufData);      //调用CRC校验方法获取校验码
                if( bufData[bufData.Length - 2] != Convert.ToByte(crcCode >> 8))
                    return null;        // not match
               
                if( bufData[bufData.Length - 1] != Convert.ToByte(crcCode % 256))
                    return null;


                return bufData;     // everything is OK
            }
            catch(Exception)
            {
                // MessageBox.Show(e.Message, "Communication Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;        // something was wrong
            }
        }
    }
}
