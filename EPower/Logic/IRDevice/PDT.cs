﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using EPower.Properties;
using OpenNETCF.Desktop.Communication;
using SoftTech;

namespace EPower.Logic.IRDevice
{
    /// <summary>
    /// Portable Device Terminal
    /// Platform   : Windows CE 5.0
    /// Communicate: Serial Port
    /// Models     : ...
    /// </summary>
    class PDT :IRDevice
    {  
        RAPI rpp; 

        const string PDT_Info = "PDT.DAT";
        public PDT()
            : base()
        {

            rpp = new RAPI(); 

            this.FileDeviceInfo = Path.Combine(Application.StartupPath, PDT_Info);
            this.RemoteFileDDSb = "Storage\\E-Power\\DDSb.dbf";
            this.RemoteFileDDSc = "Storage\\E-Power\\DDSc.dbf";
            this.RemoteFileDeviceInfo = "Storage\\E-Power\\PDT.DAT";
            this.RemoteFileDBInfo = "Storage\\E-Power\\DBInfo.dbf";
        }
        public string FileDeviceInfo
        {
            get;
            protected set;
        }

        public string RemoteFileDDSb
        {
            get;
            protected set;
        }

        public string RemoteFileDeviceInfo
        {
            get;
            protected set;
        }

        public string RemoteFileDDSc
        {
            get;
            protected set;
        }

        public string RemoteFileDBInfo
        {
            get;
            protected set;
        }

        public override int GetUsage(List<TBL_BILLING_CYCLE> cycle,List<TBL_AREA> area, ref string month,bool newDb)
        {
            return base.GetUsage(cycle,area, ref month);
        }

        public override bool GetFile(BackupType backup)
        {
            bool res = false;
            try
            {
                rpp.Connect(false, 5);
                Thread.Sleep(1200);

                if (rpp.Connected == false)
                {
                    rpp.Dispose(); rpp = new RAPI();
                    res = false; 
                }

                //get file from device
                if (!rpp.DeviceFileExists(RemoteFileDDSb) )
                {
                    if (backup == BackupType.BeforeSFTD)
                    {
                        return true;
                    }
                    throw new Exception("File not found.");
                } 

                rpp.CopyFileFromDevice(Settings.Default.IR_FILE_DB_LOCAL, RemoteFileDDSb, true);
                //backup file dbf After copy from device            
                string fileBackup = string.Format("{0}{1}{2:yyyyMMddhhmmss}.dbf", backup.ToString(), this.ProductID, DBDataContext.Db.GetSystemDate());
                File.Copy(Settings.Default.IR_FILE_DB_LOCAL, Path.Combine(this.PathBackup, fileBackup), true);
                rpp.Disconnect();
                res = true;
                return res;
            }
            catch (RAPIException ex)
            {
                if (ex.Win32Error == 3)
                {
                    throw new Exception(Resources.MS_DEVICE_DONT_HAVE_APPLICATION);
                }
                else if (ex.Win32Error == 32)
                {
                    throw new Exception(Resources.MS_CLOSE_APPLICATION_IN_DEVICE);
                }
                else
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            } 
        }

        public override string GetID()
        {  
            try
            {
                rpp.Connect(false, 5);

                Thread.Sleep(1200);

                if (rpp.Connected == false)
                {
                    rpp.Dispose();
                    rpp = new RAPI();
                    this.ProductID = string.Empty;
                    return this.ProductID;
                }
                rpp.CopyFileFromDevice(PDT_Info, RemoteFileDeviceInfo, true);
                this.ProductID = File.ReadAllText(PDT_Info); 
                rpp.Disconnect();
            }
            catch (RAPIException ex)
            { 
                if (ex.Win32Error == 3)
                {
                    throw new Exception(Resources.MS_DEVICE_DONT_HAVE_APPLICATION);
                }
                else if (ex.Win32Error == 32)
                {
                    throw new Exception(Resources.MS_CLOSE_APPLICATION_IN_DEVICE);
                }
                else
                {
                    throw ex;
                }

            }
            catch (Exception ex)
            { 
                throw ex;
            } 
            return this.ProductID;
        }

        public override bool SendFile()
        { 
            try
            {
                rpp.Connect(false, 5);

                Thread.Sleep(1200);

                if (rpp.Connected == false)
                {
                    rpp.Dispose(); rpp = new RAPI();
                    return false;
                }
                //send file DB check to device
                rpp.CopyFileToDevice(Path.Combine(this._TmpPath, this._DBInfo), this.RemoteFileDBInfo, true);
                rpp.CopyFileToDevice(Path.Combine(this._TmpPath, this._DDSc), this.RemoteFileDDSc, true);
                rpp.CopyFileToDevice(Path.Combine(this._TmpPath, this._DDSb), this.RemoteFileDDSb, true); 
                rpp.Disconnect();
                return true;

            }
            catch (RAPIException ex)
            {
                if (ex.Win32Error == 3)
                {
                    throw new Exception(Resources.MS_DEVICE_DONT_HAVE_APPLICATION);
                }
                else if (ex.Win32Error == 32)
                {
                    throw new Exception(Resources.MS_CLOSE_APPLICATION_IN_DEVICE);
                }
                else
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override bool Test()
        {

            rpp.Connect(false, 5);
            Thread.Sleep(1200);
            if (rpp.Connected == false)
            {
                rpp.Dispose();
                rpp = new RAPI();
                return false;
            }
            return true; 
        }

        public override bool CheckVersion()
        {
            return true;
        }
    }
}
