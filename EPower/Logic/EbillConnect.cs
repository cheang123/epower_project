﻿using EPower.Base.Logic;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows.Forms;

namespace EPower.Logic
{
    class EBillConnect
    {
        public bool Connect()
        {
            try
            {
                //"http://e-bill.e-power.com.kh:1115/api/processes/";
                var url = Method.GetUtilityValue(Utility.EBILL_SERVICE_URL) + "/processes/";
                var http = (HttpWebRequest)WebRequest.Create(url);
                var response = http.GetResponse();

                var stream = response.GetResponseStream();
                if (stream == null) return false;
                var sr = new StreamReader(stream);
                return sr.ReadToEnd().ToUpper() == "\"OK\"";
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        public System.Diagnostics.Process Process()
        {
            var path = Application.StartupPath + @"\EPower.EACApp\EACAppClient.exe";
            //var path = @"D:\E-Power\E-Bill\RestClient\bin\Debug\EACAppClient.exe.netz\EACAppClient.exe";
            var connectionString = Method.GetConnectionString(Properties.Settings.Default.CONNECTION); 
            var pro = new System.Diagnostics.Process()
            {
                StartInfo = new ProcessStartInfo(path)
                {
                    Arguments = connectionString,
                    WindowStyle = ProcessWindowStyle.Hidden,
                    //UseShellExecute = true,
                    //RedirectStandardOutput = true,
                    CreateNoWindow = true,
                }
            };
            return pro;
        }

        public void RunBackground() 
        {
            using (BackgroundWorker bg = new BackgroundWorker())
            {
                var pro = Process();
                /*
                 * Silent mode but not output result transaction.
                 */
                pro.StartInfo.UseShellExecute = true;
                pro.StartInfo.RedirectStandardOutput = false;
                bg.DoWork += delegate (object sender, DoWorkEventArgs e)
                {
                    try
                    {
                        pro.Start();
                    }
                    catch (Exception exp)
                    {
                        throw new Exception("EAC App error " + exp.Message, exp);
                    }
                };
                bg.RunWorkerCompleted += delegate (object sender, RunWorkerCompletedEventArgs e)
                {
                    if (e.Error!=null)
                    {
                        
                        MsgBox.ShowError(e.Error);
                    }
                };
                bg.RunWorkerAsync();
            } 
        }

        

        public static DateTime? GetLastSuccessUpload()
        {
            try
            {
                var lastSuccessUpload = DBDataContext.Db.TBL_EBL_PROCESSes.Where(x => x.IS_COMPLETED).OrderByDescending(x => x.TO_DATE).FirstOrDefault();
                if (lastSuccessUpload == null)
                {
                    return null;
                }
                else
                {
                    return lastSuccessUpload.TO_DATE;
                }
            }
            catch (Exception ex)
            {
                MsgBox.LogError(ex);
                return null;
            }
            
        }
        public static void CheckUploadRequired()
        {
            try
            {
                if (DataHelper.ParseToBoolean(Method.GetUtilityValue(Utility.EBILL_ENABLE)) == false)
                {
                    return;
                }
                var lastUpload = GetLastSuccessUpload() ?? new DateTime(2015, 1, 1);
                var missUpload = (DBDataContext.Db.GetSystemDate() - lastUpload).TotalDays;
                if (missUpload > 7)
                {
                    MsgBox.ShowInformation(Properties.Resources.PLEASE_SEND_DATA_TO_EAC_APP);
                }
            }
            catch (Exception ex)
            {
                MsgBox.LogError(ex); 
            }
            
        }
    }
}
