﻿
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace B24.Customer.Model
{
    public class Customer
    {
        public string address { get; set; }
        public string code { get; set; }
        public string created_by { get; set; }
        public string created_date { get; set; }
        public string email { get; set; }
        public object ext { get; set; }
        public string ext_data { get; set; }
        public string id { get; set; }
        public bool is_active { get; set; }
        public string name { get; set; }
        public string org_code { get; set; }
        public string phone { get; set; }
        public string supplier_id { get; set; }
        public object sync_code { get; set; }
        public string updated_by { get; set; }
        public string updated_date { get; set; }
    }

    public class Bill
    {
        public string bill_date { get; set; }
        public int bill_type { get; set; }
        public string code { get; set; }
        public string created_by { get; set; }
        public string created_date { get; set; }
        public string currency_code { get; set; }
        public string currency_id { get; set; }
        public string currency_sign { get; set; }
        public object customer_data { get; set; }
        public string customer_id { get; set; }
        public object customer_sync_code { get; set; }
        public string description { get; set; }
        public string due_date { get; set; }
        public object ext { get; set; }
        public string ext_data { get; set; }
        public string id { get; set; }
        public bool is_active { get; set; }
        public string org_code { get; set; }
        public decimal paid_amount { get; set; }
        public string supplier_id { get; set; }
        public object sync_code { get; set; }
        public string title { get; set; }
        public decimal total_amount { get; set; }
        public string updated_by { get; set; }
        public string updated_date { get; set; }
    }

    public class PaymentInfo
    {
        public string payment_token { get; set; }
        public string payment_url { get; set; }
    }

}
namespace B24.Customer
{
    using B24.Customer.Model;

    public class CustomerAPIClient
    {
        private RestClient client;

        public CustomerAPIClient(string url, string token)
        {
            this.client = new RestClient(url);
            client.AddDefaultHeader("token", token);
        }

        public Customer Who()
        {
            var request = new RestRequest("/who", Method.GET);
            var response = this.client.Execute<Customer>(request);
            return response.Data;
        }

        public List<Bill> UnpaidBill()
        {
            var request = new RestRequest("/unpaid-bill", Method.GET);
            var response = this.client.Execute<List<Bill>>(request);
            return response.Data;
        }

        public PaymentInfo Payment()
        {
            var request = new RestRequest("/payment", Method.GET);
            var response = this.client.Execute<PaymentInfo>(request);
            return response.Data;
        }

    }

    
}

