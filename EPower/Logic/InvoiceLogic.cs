﻿using SoftTech;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EPower.Logic
{
    class InvoiceLogic
    {
        public static List<TBL_INVOICE> getPaidInvoices(int runId = 0)
        {
            var result = from i in DBDataContext.Db.TBL_INVOICEs
                         where (runId == 0 || i.RUN_ID == runId) && i.PAID_AMOUNT > 0 && i.INVOICE_STATUS != (int)InvoiceStatus.Cancel
                         select i;

            return result.ToList();
        }
        public static IQueryable<TBL_INVOICE> getPaidInvoiceIQueryAble(int runId = 0)
        {
            var result = from i in DBDataContext.Db.TBL_INVOICEs
                         where (runId == 0 || i.RUN_ID == runId) && i.PAID_AMOUNT > 0 && i.INVOICE_STATUS != (int)InvoiceStatus.Cancel
                         select i; 
            return result;
        }
    }
}
