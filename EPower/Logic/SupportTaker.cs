﻿using System;
using System.Collections.Generic;
using System.Linq;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;

namespace EPower.Logic
{
    public   class SupportTaker
    {
        public enum Process
        {
            Contact,
            Login,
            RecivePayment,
            ViewInvoice,
            PrintInvoice,
            AddCustomer
        }

        internal Process _process;

        Dictionary<Process, string[]> Msg = new Dictionary<Process, string[]>();
          
        public SupportTaker(Process process)
        {
            _process = process;
            Msg[Process.Contact] = new string[]{Resources.MS_CONTACT_TO_SUPPORT_TO_CONTINUE_LICENSE};

            Msg[Process.Login] = new string[]
            {
                Resources.MS_PLEASE_CHECK_NETWORK_CONNECTION,
                Resources.MS_INVALID_USE_AND_PASSWORD,
                Resources.MS_SYSTEM_CANNOT_CONNECT_TO_SERVER
            };

            Msg[Process.RecivePayment] = new string[] { Resources.MS_NETWORK_PRINTER_ERROR, Resources.MS_PRINTER_ERROR };

            Msg[Process.ViewInvoice] = new string[]
            {
                Resources.MS_PRINTER_ERROR,
                Resources.MS_SYSTEM_CANNOT_FIND_INVOICE_TO_PRINT,
                Resources.MS_NETWORK_PRINTER_ERROR
            };
            Msg[Process.PrintInvoice] = new string[]
            { 
                Resources.MS_PRINTER_SERVICE_ERROR,
                Resources.MS_TEMPLATE_NOT_FOUND,
                Resources.NETWORK_FAIL, 
                Resources.BAD_FORMAT,
                Resources.MS_INVALID_BLOCK,
                Resources.MS_PAGE_FILE_ERROR,
                Resources.MS_INDEX_CORRUPT,
                 
            }; 
            Msg[Process.AddCustomer] = new string[]
            {
                Resources.MS_STORAGE_FAILURE,
                Resources.BAD_FORMAT,
                Resources.MS_INVALID_BLOCK,
                "PAGE_FILE_ERROR : Database page file is misconfigure, please contact administrator.",
                "INDEX_CORRUPT : Table indexes is corrupted, transaction will be aborted.", 
                "MSDTC_ERROR : MSDTC service is not available to complete your transaction."
            };
        }

        internal int[] GetRate()
        {
            TBL_CONFIG objConf=DBDataContext.Db.TBL_CONFIGs.FirstOrDefault();
            DateTime dtSupportExp = objConf.ACTIVATE_DATE.Date.AddMonths(objConf.TOTAL_MONTH);
            DateTime dtNow = DBDataContext.Db.GetSystemDate();

            double dbDaysExp = (dtNow.Date-dtSupportExp.Date).TotalDays;
            if (this._process == Process.AddCustomer)
            {
                int a = DBDataContext.Db.TBL_CONFIGs.FirstOrDefault().TOTAL_CUTOMER + new Random().Next(0, 100);
                int b = DBDataContext.Db.TBL_CUSTOMERs.Count(row => row.STATUS_ID == (int)CustomerStatus.Active); 
                if (b > a)
                {
                    return new int[] { 1 };
                }
            }

            if (dbDaysExp<-7)//int full support
            {
                //_process = Process.Contact;
                return new int[] { 0 };
            }
            else if (dbDaysExp>=-7 && dbDaysExp<=0)//alert contact supporter
            {
                //_process = Process.Contact;
                return new int[] { 0, 1, 1 ,1};
            }
            else if (dbDaysExp > 0 && dbDaysExp <=  7* 1) //support expire
            {
                return new int[] { 0, 0, 0, 1 };
            }
            else if (dbDaysExp >7* 1 && dbDaysExp <= 7* 3)
            {
                return new int[] { 0, 0, 1, 1 };
            }
            else if (dbDaysExp >7* 3 && dbDaysExp <= 7* 7)
            {
                return new int[] { 0, 1, 1, 1 };
            }
            else if (dbDaysExp > 7* 7)
            {
                if (_process== Process.Login)
                {
                    return new int[] { 0, 1, 1, 1 }; 
                }
                return new int[] { 1 };
            }
            return new int[]{0}; 
        }

        public bool Check()
        {
            int[] rate = GetRate();
            Random ran = new Random();
            int index= ran.Next(0, rate.Length-1);

            if (rate[index] == 0)
            {
                return false;
            }
            else
            {
                //ran=new Random();
                //index = ran.Next(0, Msg[_process].Length-1);
                var objConf  =  DBDataContext.Db.TBL_CONFIGs.FirstOrDefault();
                index =(objConf.TOTAL_CUTOMER+objConf.TOTAL_MONTH) % Msg[_process].Length;
                MsgBox.ShowWarning(Msg[_process][index], string.Empty);

                if (_process== Process.Contact)
                {
                    return false;
                }
                return true;
            }
        }  
    }
}
