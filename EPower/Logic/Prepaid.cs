﻿using System;
using System.Configuration;
using System.Linq;
using SoftTech;
using SoftTech.Helper;

namespace EPower.Logic
{
    class Prepaid
    {
        internal static void GetPortInfo(ref int intPortNo, ref int intBoundRate)
        {
            intPortNo = DataHelper.ParseToInt(ConfigurationManager.AppSettings["PREPAID_COM_PORT"]);
            intBoundRate = DataHelper.ParseToInt(ConfigurationManager.AppSettings["PREPAID_BOUND_RATE"]);
        }

        //36303030
        //36303037
        //36303038
        internal static string GetAreaCode()
        {
            string areaCode = string.Empty;
            TBL_CONFIG ac = DBDataContext.Db.TBL_CONFIGs.FirstOrDefault();
            if (ac!=null)
            {
                areaCode = ac.AREA_CODE;
            }
            //Default area code from 234L & 182L
            if (areaCode.Contains("36303037")||areaCode.Contains("36303038"))
            {
                areaCode = "36303030";
            }
            return areaCode;
        }

        internal static MeterType GetMeterType(TBL_METER objMeter)
        {         
            TBL_PHASE objPhase = (from mt in DBDataContext.Db.TBL_METER_TYPEs.Where(x => x.METER_TYPE_ID == objMeter.METER_TYPE_ID)
                                  join p in DBDataContext.Db.TBL_PHASEs on mt.METER_PHASE_ID equals p.PHASE_ID
                                  select p).FirstOrDefault();
            int intPhase = 0;
            if (!int.TryParse(objPhase.PHASE_NAME,out intPhase))
            {
                return MeterType.SinglePhase;
            }
            else
            {
                MeterType mt;
                if (intPhase<=1)
                {
                    mt= MeterType.SinglePhase;
                }
                else
                {
                    mt = MeterType.ThreePhase;
                }
                return mt;
            }            
        }

        internal static int GetAlarmQTY()
        {
            int intAlarmQty = 1;

            TBL_UTILITY objUtility=DBDataContext.Db.TBL_UTILITies.FirstOrDefault(x=>x.UTILITY_ID==(int)Utility.ALARM_QTY);

            if (objUtility!=null)
            {
                int.TryParse(objUtility.UTILITY_VALUE, out intAlarmQty);
            }

            return intAlarmQty;                         
        }

        internal static int GetNexPurchasingTimes(int intPurchasingTimes)
        {
            return intPurchasingTimes + 1;
        }

        internal static TBL_CUSTOMER GetCustomerInfo(string CardCode)
        {
            TBL_CUSTOMER objCustomer = (from c in DBDataContext.Db.TBL_CUSTOMERs
                                        join pc in DBDataContext.Db.TBL_PREPAID_CUSTOMERs on c.CUSTOMER_ID equals pc.CUSTOMER_ID
                                        where !c.IS_POST_PAID & pc.CARD_CODE.ToLower().Contains(CardCode.ToLower())
                                        select c).FirstOrDefault();

            return objCustomer;                                     
        }


        internal static int GetResetTimes()
        {
            return 3;
        }
    }

    class PreCustomer
    {
        public string AreaCode { get; set; }
        public string CustomerCode { get; set; }
        public MeterType MeterType { get; set; }
        public int AlarmQTY { get; set; }
        public DateTime ActivateDate { get; set; }
        public int Amparer { get; set; }
        public int Constant { get; set; }
    }
}
