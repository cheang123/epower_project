﻿using System;
using EPower.Properties;
using SoftTech.Component;

namespace EPower.Logic
{
    /// <summary>
    /// Meter type
    /// </summary>
    internal enum MeterType
    {
        SinglePhase = 1,
        ThreePhase = 3
    }

    /// <summary>
    /// Card Type
    /// </summary>
    internal enum CardType
    {
        NoCard=-2,
        BlankCard=-1,
        FormattedCard = 0,
        HavePowerCard = 1,
        NoPowerCard = 81,
        ResetCard = 2,
        PresetCard = 3,
        TestCard = 7,
        CutCard = 8
        
    }

    /// <summary>
    /// IC Card Base class
    /// </summary>
    internal class ICCard
    {
        string _strAreaCode = "36303030";//"68024100";//"36303030";//"68052005";//

        static int _intCardType = 0;
        const string _strFactoryCode = "RADARKing";//"RADARKingSKDD1";
        const string _strNonAreaCode = "????????";
        internal int ICCardType
        {
            get { return _intCardType; }
            set { _intCardType = value; }
        }

        #region Constrctor
        internal static ICCard Read( )
        {
            string strReturn;
            string strSound;
            int intRead = ICHelper.ReadCard(out strReturn);            
            if (intRead == 0)
            {
                ICHelper.BeepSound(out strSound);
                //get card type
                if (strReturn.Trim() == "")
                {
                    return new ICCard();                
                }                
                string strFactoryCode = strReturn.Substring(28, 14);

                string strReadAreaCode = strReturn.Substring(6, 8);
                if (!strFactoryCode.ToLower().StartsWith(_strFactoryCode.ToLower()))
                {
                    //no card
                    if (strReadAreaCode.ToLower().Contains(_strNonAreaCode.ToLower()))
                    {
                        return new ICCard() { ICCardType = (int)CardType.NoCard };
                    }
                    //New card
                    else
                    {
                        return new ICCard() { ICCardType = (int)CardType.BlankCard };
                    }                    
                }

                string strCardType = strReturn.Substring(26, 2);
                _intCardType = int.Parse(strCardType.Trim());

                
                switch (_intCardType)
                {
                    case (int)CardType.FormattedCard: return new ICCard();
                    case (int)CardType.HavePowerCard: return new PowerCard(strReturn);
                    case (int)CardType.NoPowerCard: return new PowerCard(strReturn);
                    case (int)CardType.PresetCard: return new PreReSetCard(strReturn);
                    case (int)CardType.ResetCard: return new PreReSetCard(strReturn);
                    case (int)CardType.TestCard: return new TestCutCard(strReturn);
                    case (int)CardType.CutCard: return new TestCutCard(strReturn);
                }
            }
            else
            {
                MsgBox.ShowInformation(Resources.MS_CANNOT_OPEN_COM_PORT);
                return new ICCard() {  ICCardType=(int)CardType.NoCard};
            }
            return null;
        }
        #endregion Constructor

        #region Method
        internal void SetAreaCode(string strAreaCode)
        {
            _strAreaCode = strAreaCode;
        }

        internal int FormatCard(out string strReturn ,string strAreaCode)
        {
            int intReturn = 1;
            string strData = FormatAreaCode(strAreaCode) + // the areacode should be the current area code
                                                          // becuause it will used when write spcial card
                             FormatCustomerCode("0") +
                             FormatMeterType(0) + //表类型
                             "9900" + // 最大屯积量
                             "00" + // 透支/报警量标志
                             "00" + // 透支/报警量
                             "005500" + // 最大负荷功率
                             "00" + // 报警方式
                             "01" + // 过载允许时间
                             FormatDate(DateTime.Now.Date) + // 开户日期
                             "00" +// 工号
                             "02" + //for 20A
                             "003200" +
                             "02" +
                             "0001" +
                             "2000" +
                             "0000";

            string strSent = ICHelper.GetSendFrame("01", strData);
            intReturn = ICHelper.SentCommand(strSent, out strReturn);
            return intReturn;
        }
        #endregion Method

        #region Protected Method

        /// <summary>
        /// Get Remainded power that can be used in the meter
        /// 1 decimal place
        /// </summary>
        /// <param name="strRemainPower"></param>
        /// <returns></returns>
        protected decimal GetReaminPower(string strRemainPower)
        {
            decimal decReturn = 0;
            string strSign = strRemainPower.Substring(0, 2);
            decReturn = decimal.Parse(strRemainPower.Substring(2))/10;
            //if the byte = 80 then the number is negative number
            if (strSign == "80")
            {
                decReturn = -decReturn;
            }
            return decReturn;
        }

        /// <summary>
        /// Get total power have been used in meter 
        /// 1 decimal place in the data
        /// </summary>
        /// <param name="strTotalUsedPower"></param>
        /// <returns></returns>
        protected decimal GetTotalUsedPower(string strTotalUsedPower)
        {
            decimal decReturn = 0;
           
            decReturn = decimal.Parse(strTotalUsedPower)/10;
            
            return decReturn;
        }

        /// <summary>
        /// Get last purchased Power
        /// 1 decimal place in the data
        /// </summary>
        /// <param name="strTotalUsedPower"></param>
        /// <returns></returns>
        protected decimal GetLastPurchasePower(string strLastPurchasePower)
        {
            decimal decReturn = 0;

            decReturn = decimal.Parse(strLastPurchasePower) / 10;
            
            return decReturn;
        }

        protected DateTime GetDate(string strDate)
        {
            int intYear = int.Parse(strDate.Substring(0,4));
            int intMonth = int.Parse(strDate.Substring(4,2));
            int intDay = int.Parse(strDate.Substring(6,2));
            return new DateTime(intYear,intMonth,intDay);
        }

        protected int GetMeterType(string strMeterType)
        {
            int intReturn = 0;
            if (strMeterType == "00" || strMeterType == "FF" || strMeterType == "90")
            {
                intReturn = (int)MeterType.SinglePhase;
            }
            else if (strMeterType == "01")
            {
                intReturn = (int)MeterType.ThreePhase;
            }
            return intReturn;
        }

        protected string FormatDate(DateTime datDate)
        {
            return datDate.ToString("yyyyMMdd");
        }

        protected string FormatCustomerCode(string strCustomerCode)
        {
            string strReturn = "";
            string strZero = new string('0', 12);
            double dblCustomerCode = double.Parse(strCustomerCode);
            strReturn = dblCustomerCode.ToString(strZero);
            return strReturn;
        }

        protected string FormatAreaCode(string strAreaCode)
        {
            //string strReturn = "";
            //string strZero = new string('0', 8);
            //double dblAreaCode = double.Parse(strAreaCode);
            //strReturn = dblAreaCode.ToString(strZero);
            return strAreaCode;
        }

        protected string FormatMeterType(int intMeterType)
        {
            string strReturn = "";
            if (intMeterType == (int)MeterType.SinglePhase)
            {
                strReturn = "FF";
            }
            else if (intMeterType == (int)MeterType.ThreePhase)
            {
                strReturn = "01";
            }
            return strReturn;
        }

        protected string FormatPurchasingTimes(int intPurchasingTime)
        {
            string strReturn = "";
            string strZero = new string('0', 4);
            strReturn = intPurchasingTime.ToString(strZero);
            return strReturn;
        }
        
        #endregion Protected Method
    }

    /// <summary>
    /// Power Card
    /// </summary>
    internal class PowerCard : ICCard
    {
        string _strFrame; //

        #region Private Data
        string _strAreaCode;//区号
        string _strCustomerCode;//用户号
        int _intMeterType;//表类型
        int _intPurchasingTimes = 0;
        decimal _decPurchasePower;//购电量
        decimal _decRemainPower;//剩余电量
        decimal _decTotalPower; //累计电量
        DateTime _datLastPurchaseDate = new DateTime();//购电日期
        int _intAlarmQty;//透支/报警量
        #endregion Private Data

        #region Properties
        internal string AreaCode
        {
            get { return _strAreaCode; }
            set { _strAreaCode = value; }
        }

        internal string CustomerCode
        {
            get { return _strCustomerCode; }
            set { _strCustomerCode = value; }
        }

        internal int MeterType
        {
            get { return _intMeterType; }
            set { _intMeterType = value; }
        }

        public int PurchasingTimes
        {
            get { return _intPurchasingTimes; }
            set { _intPurchasingTimes = value; }
        }

        internal decimal PurchasePower
        {
            get { return _decPurchasePower; }
            set { _decPurchasePower = value; }
        }

        internal decimal RemainPower
        {
            get { return _decRemainPower; }
            set { _decRemainPower = value; }
        }

        internal decimal TotalPower
        {
            get { return _decTotalPower; }
            set { _decTotalPower = value; }
        }

        internal DateTime LastPurchaseDate
        {
            get { return _datLastPurchaseDate; }
            set { _datLastPurchaseDate = value; }
        }
        internal int AlarmQty
        {
            get { return _intAlarmQty; }
            set { _intAlarmQty = value; }
        }

        #endregion Properties

        #region Constructor

        internal PowerCard()
            : base()
        {
        }

        internal PowerCard(string strFrame):base()
        {
             _strFrame = strFrame;
            _strAreaCode = strFrame.Substring(6, 8);
            _strCustomerCode = strFrame.Substring(14, 12);
            _intMeterType = GetMeterType(strFrame.Substring(42, 2));
            _intPurchasingTimes = int.Parse(strFrame.Substring(70,4));
            _decPurchasePower = decimal.Parse(strFrame.Substring(44,4));
            _decRemainPower =  GetReaminPower(strFrame.Substring(50,8));
            _decTotalPower = GetTotalUsedPower(strFrame.Substring(58,8));
            _datLastPurchaseDate = GetDate(strFrame.Substring(92, 8));
            _intAlarmQty = int.Parse(strFrame.Substring(82, 2));

            string strAlarmMode = strFrame.Substring(80,2);
            string strMethod = strFrame.Substring(78,2);
            string strMax = strFrame.Substring(74, 4);
        }

        #endregion Constructor

        #region Method

        /// <summary>
        /// Write Purchase power card for user
        /// </summary>
        /// <param name="strCusCode">Customer Code</param>
        /// <param name="intPurchaseTimes">Number of purchasing times</param>
        /// <param name="datPurchaseDate">Purchasing date</param>
        /// <param name="strReturnString">Returning string after write</param>
        /// <returns>return 0 mean write successfully;
        /// 1 mean fail;
        /// </returns>
        internal  int WritePurchaseCard(string strCusCode,int intPurchaseTimes, DateTime datPurchaseDate,decimal decPurchasePower, out string strReturnString)
        {
            //convert string to double first in order to format to 12 digit
            string strData = FormatCustomerCode(strCusCode) + intPurchaseTimes.ToString("0000") + decPurchasePower.ToString("0000") +
                   FormatDate(datPurchaseDate) + "00";

            strReturnString = "";
            string strCommand = "02";
            int intReturn = 1;
            //command before sent to device
            string strSend = ICHelper.GetSendFrame(strCommand, strData);
            //sent command to device
            intReturn = ICHelper.SentCommand(strSend, out strReturnString);
            return intReturn;
        }

        /// <summary>
        /// Write New Customer 
        /// </summary>
        /// <param name="objCard">Card Object info</param>        
        /// <returns>0 mean write succesfully</returns>                
        internal int WriteNewCustomer(string strAreaCode,string strCustomerCode,
            MeterType meterType,int intAlarmQty,DateTime datActiveDate,out string strReturn)
        {
            int intReturn = 1;
            //Format card befor write new customer.
            intReturn = new ICCard().FormatCard(out strReturn,strAreaCode);
            if (intReturn == 0)
            { 
                string strData = FormatAreaCode(strAreaCode) + //should be current area code
                                 FormatCustomerCode(strCustomerCode) +
                                 FormatMeterType((int)meterType) + //表类型
                                 "0000" + // 最大屯积量
                                 "00" + // 透支/报警量标志 alarm Method 1 mean cut power
                                 intAlarmQty.ToString("00") + // 透支/报警量 alarm quantity
                                 "023000" + // 最大负荷功率 maximun use of power , example 40A*220v = 8800w
                                 "01" + // 报警方式 triping
                                 "01" + // 过载允许时间 time after overload use of meter
                                 FormatDate(datActiveDate) + // 开户日期
                                 "00" + // 工号 
                                 "02" + //for 20A
                                 "003200" + //constant
                                 "02" +
                                 "0001" +
                                 "2000" +
                                 "0000";

                string strSent = ICHelper.GetSendFrame("01", strData);
                intReturn = ICHelper.SentCommand(strSent, out strReturn);
            }
            return intReturn;
        }

        internal int VoidLastPurchasing(out string strReturn)
        {
            int intReturn = 1;
            string strSent = ICHelper.GetSendFrame("1A", "") + "SLE4442";
            intReturn = ICHelper.SentCommand(strSent, out strReturn) ;
            return intReturn;
        }

        #endregion Method
    }

    /// <summary>
    /// Both preset and reset card
    /// </summary>
    internal class PreReSetCard : ICCard
    {
        #region Private Data
        string _strFrame;
        int _intRemaindTime; //剩余次数
        string _strCustomerCode1; //用户编号1
        decimal _decRemaindAmount1;//剩余量1
        string _strCustomerCode2;//用户编号2
        decimal _decRemaindAmount2;//剩余量2
        string _strCustomerCode3;//用户编号3
        decimal _decRemainAmount3;//剩余量3
        bool _blnIsBlank = false;

        #endregion Private Data

        #region Properties
        internal int RemaindTime
        {
            get { return _intRemaindTime; }
            set { _intRemaindTime = value; }
        }

        internal string CustomerCode1
        {
            get { return _strCustomerCode1; }
            set { _strCustomerCode1 = value; }
        }

        internal string CustomerCode2
        {
            get { return _strCustomerCode2; }
            set { _strCustomerCode2 = value; }
        }

        internal decimal RemaindAmount2
        {
            get { return _decRemaindAmount2; }
            set { _decRemaindAmount2 = value; }
        }

        internal string CustomerCode3
        {
            get { return _strCustomerCode3; }
            set { _strCustomerCode3 = value; }
        }

        internal decimal RemainAmount3
        {
            get { return _decRemainAmount3; }
            set { _decRemainAmount3 = value; }
        }
        #endregion Properties

        #region Constructor
        internal PreReSetCard()
            : base()
        {
        }

        internal PreReSetCard(string strFrame)
            : base()
        {
            _strFrame = strFrame;
            _intRemaindTime = int.Parse(strFrame.Substring(42, 2));
            //mean used at least 1 time
            if (_intRemaindTime >= 1)
            {
                _strCustomerCode1 = _strFrame.Substring(44, 12);
                if (double.Parse(_strCustomerCode1) == 0)
                {
                    _blnIsBlank = true;
                    return;
                }
                _decRemaindAmount1 = GetReaminPower(_strFrame.Substring(56, 8));
            }
            //mean used at least 2 time
            if (_intRemaindTime >= 2)
            {
                _strCustomerCode2 = _strFrame.Substring(64, 12);
                _decRemaindAmount2 = GetReaminPower(_strFrame.Substring(76, 8));
            }
            //mean used at least 3 time
            if (_intRemaindTime >= 3)
            {
                _strCustomerCode3 = _strFrame.Substring(84, 12);
                _decRemainAmount3 = GetReaminPower(_strFrame.Substring(96, 8));
            }
        }
        #endregion Constructor

        #region Method
        /// <summary>
        /// Write Reset card
        /// </summary>
        /// <param name="strCusCode">Customer Code</param>
        /// <param name="intPurchaseTimes">Number of purchasing times</param>
        /// <param name="datPurchaseDate">Purchasing date</param>
        /// <param name="strReturnString">Returning string after write</param>
        /// <returns>return 0 mean write successfully;
        /// 1 mean fail;
        /// </returns>
        internal int WriteResetCard(int intResetTimes, MeterType meterType,  out string strReturnString)
        {
            strReturnString = "";
            int intReturn = 1;

            string strMeterType = FormatMeterType((int)meterType);
            string strTimes = intResetTimes.ToString("00");
            //command before sent to device
            string strSend = ICHelper.GetSendFrame("03", strTimes + strMeterType) + "SLE4442";
            //sent command to device
            intReturn = ICHelper.SentCommand(strSend, out strReturnString);
            return intReturn;
        }
        #endregion Method
    }

    /// <summary>
    /// Test and power cut card
    /// </summary>
    internal class TestCutCard : ICCard
    {
        string _strFrame;
        string _strAreaCode;//区号
        string _strCustomerCode;//用户号
        int _intMeterType;//表类型
        decimal _decLastPurchasePower;//上次购电量
        decimal _decRemainPower;//剩余电量
        decimal _decTotalPower; //累计电量
        int _intBuyTimes;

       
        bool _blnIsBlank = false;


        #region Properties
        internal string AreaCode
        {
            get { return _strAreaCode; }
            set { _strAreaCode = value; }
        }

        internal string CustomerCode
        {
            get { return _strCustomerCode; }
            set { _strCustomerCode = value; }
        }

        internal int MeterType
        {
            get { return _intMeterType; }
            set { _intMeterType = value; }
        }

        internal decimal LastPurchasePower
        {
            get { return _decLastPurchasePower; }
            set { _decLastPurchasePower = value; }
        }

        internal decimal RemainPower
        {
            get { return _decRemainPower; }
            set { _decRemainPower = value; }
        }

        internal decimal TotalPower
        {
            get { return _decTotalPower; }
            set { _decTotalPower = value; }
        }

        internal int BuyTimes
        {
            get { return _intBuyTimes; }
            set { _intBuyTimes = value; }
        }
        #endregion Properties

        #region Cosstructor
        internal TestCutCard()
            : base()
        {
        }

        internal TestCutCard(string strFrame)
            : base()
        {
            _strFrame = strFrame;
            _strAreaCode = strFrame.Substring(42, 8);
            if (double.Parse(_strAreaCode) == 0)
            {
                _blnIsBlank = true;
                return;
            }
            _strCustomerCode = strFrame.Substring(50, 12);
            _intMeterType = GetMeterType(strFrame.Substring(62, 2));
            _decLastPurchasePower = GetLastPurchasePower(strFrame.Substring(80,6));
            _decRemainPower =  GetReaminPower(strFrame.Substring(64,8));
            _decTotalPower = GetTotalUsedPower(strFrame.Substring(72,8));
            _intBuyTimes = int.Parse( strFrame.Substring(118, 4));
            //string strAlramMode = 
            //string strAlarmMode 
        }
        #endregion Cosstructor


        #region Method

        /// <summary>
        /// Read Meter Information Card
        /// </summary>
        /// <param name="strCusCode">Customer Code</param>
        /// <param name="intPurchaseTimes">Number of purchasing times</param>
        /// <param name="datPurchaseDate">Purchasing date</param>
        /// <param name="strReturnString">Returning string after write</param>
        /// <returns>return 0 mean write successfully;
        /// 1 mean fail;
        /// </returns>
        internal int WriteReadCard(out string strReturnString)
        {
            strReturnString = "";
            int intReturn = 1;
            //command before sent to device
            string strSend = ICHelper.GetSendFrame("05", "") + "SLE4442";
            //sent command to device
            intReturn = ICHelper.SentCommand(strSend, out strReturnString);
            return intReturn;
        } 

        #endregion Method
    }
}
