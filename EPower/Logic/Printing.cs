﻿using EPower.Properties;
using SoftTech.Helper;

namespace EPower.Logic
{
    static class Printing
    {
        public static void DepositReceipt(int intCusDepositID)
        {
            SupportTaker objTC = new SupportTaker(SupportTaker.Process.PrintInvoice);
            if (objTC.Check())
            {
                return;
            }
            CrystalReportHelper ch = new CrystalReportHelper("ReportDepositReceipt.rpt");
            ch.SetParameter("@CUS_DEPOSIT_ID", intCusDepositID);
            ch.PrintReport(Settings.Default.PRINTER_RECIEPT);
        }
        public static void ReceiptPayment(int intPaymentID)
        {
            SupportTaker objTC = new SupportTaker(SupportTaker.Process.PrintInvoice);
            if (objTC.Check())
            {
                return;
            }
            CrystalReportHelper ch = new CrystalReportHelper("ReportReceiptPayment.rpt");
            ch.SetParameter("PAYMENT_ID", intPaymentID);
            ch.PrintReport(Settings.Default.PRINTER_RECIEPT);
        }
    }
}
