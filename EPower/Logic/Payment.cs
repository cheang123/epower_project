﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using EPower.Base.Helper;
using EPower.Base.Logic;
using EPower.Base.Models;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;

namespace EPower.Logic
{
    public class Payment
    {
        public static void AddPayment(TBL_PAYMENT objPayment)
        {
        }
        public static void AddPayment(TBL_PAYMENT objPayment, TBL_PAYMENT_DETAIL[] objPaymentDetails)
        {
        }
        public static bool CancelPayment(TBL_PAYMENT objPayment, string NOTE, bool isRemovePrepay = true)
        {
            return CancelPayment(objPayment, DBDataContext.Db, NOTE, isRemovePrepay);
        }
        public static bool CancelPayment(TBL_PAYMENT objPayment, DBDataContext db, string NOTE, bool isRemovePrepay = true)
        {
            if (objPayment.IS_VOID)
            {
                throw new Exception("Payment is already void!");
            } 
            DateTime datDate = db.GetSystemDate();
            //-- update payment
            TBL_PAYMENT objOLDPayment = DBDataContext.Db.TBL_PAYMENTs.FirstOrDefault(x => x.PAYMENT_ID == objPayment.PAYMENT_ID);
            objOLDPayment.IS_VOID = true;
            //1- Add cancel payment
            TBL_PAYMENT objPaymentCancel = new TBL_PAYMENT()
            {
                CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                CREATE_ON = datDate,
                CUSTOMER_ID = objOLDPayment.CUSTOMER_ID,
                DUE_AMOUNT = objOLDPayment.DUE_AMOUNT,
                IS_ACTIVE = true,
                PAY_AMOUNT = -1 * objOLDPayment.PAY_AMOUNT,
                PAY_DATE = objPayment.PAY_DATE,
                PAYMENT_NO = objOLDPayment.PAYMENT_NO,
                USER_CASH_DRAWER_ID = 0,
                CURRENCY_ID = objOLDPayment.CURRENCY_ID,
                PARENT_ID = objOLDPayment.PAYMENT_ID,
                IS_VOID = true,
                PAYMENT_ACCOUNT_ID = objOLDPayment.PAYMENT_ACCOUNT_ID,
                BANK_PAYMENT_DETAIL_ID = objOLDPayment.BANK_PAYMENT_DETAIL_ID,
                NOTE = NOTE,
                EXCHANGE_RATE = objOLDPayment.EXCHANGE_RATE,
                EXCHANGE_RATE_DATE = objOLDPayment.EXCHANGE_RATE_DATE,
                PAYMENT_METHOD_ID = objOLDPayment.PAYMENT_METHOD_ID
            };

            if (isRemovePrepay && objOLDPayment.USER_CASH_DRAWER_ID != 0)
            {
                objPaymentCancel.USER_CASH_DRAWER_ID = Login.CurrentCashDrawer.USER_CASH_DRAWER_ID;
            }
            db.InsertWithoutChangeLog(objPaymentCancel);
            //TBL_CHANGE_LOG objChangeLog = db.Insert(objPaymentCancel);
            //db.SubmitChanges();

            //get all data to local 
            var allpaymentDetials = db.TBL_PAYMENT_DETAILs.Where(pd => pd.PAYMENT_ID == objPayment.PAYMENT_ID);
            var allInvoiceIds = allpaymentDetials.Select(x => x.INVOICE_ID).ToList();
            var allInvoices = db.TBL_INVOICEs.Where(x => allInvoiceIds.Contains(x.INVOICE_ID)).ToList(); 
            var allCancalPaymentDetail = new List<TBL_PAYMENT_DETAIL>();

            foreach (TBL_PAYMENT_DETAIL pd in db.TBL_PAYMENT_DETAILs.Where(pd => pd.PAYMENT_ID == objPayment.PAYMENT_ID))
            {
                //2 - Add cancel payment detal
                TBL_PAYMENT_DETAIL objPayDetailCancel = new TBL_PAYMENT_DETAIL();
                objPayDetailCancel.PAYMENT_ID = objPaymentCancel.PAYMENT_ID;
                objPayDetailCancel.DUE_AMOUNT = pd.DUE_AMOUNT;
                objPayDetailCancel.INVOICE_ID = pd.INVOICE_ID;
                objPayDetailCancel.PAY_AMOUNT = -1 * pd.PAY_AMOUNT;
                allCancalPaymentDetail.Add(objPayDetailCancel);

                //db.InsertWithoutChangeLog(objPayDetailCancel);
                //db.InsertChild(objPayDetailCancel, objPayDetailCancel, ref objChangeLog);

                //3 - Update invoice after cancel payment
                TBL_INVOICE objInvoiceOld = allInvoices.FirstOrDefault(x => x.INVOICE_ID == pd.INVOICE_ID); //db.TBL_INVOICEs.Where(inv => inv.INVOICE_ID == pd.INVOICE_ID).FirstOrDefault();
                TBL_INVOICE objInvoice = new TBL_INVOICE();
                objInvoiceOld._CopyTo(objInvoice);

                objInvoice.PAID_AMOUNT -= pd.PAY_AMOUNT;
                objInvoice.INVOICE_STATUS = objInvoice.SETTLE_AMOUNT == objInvoice.PAID_AMOUNT ? (int)InvoiceStatus.Close : (int)InvoiceStatus.Open;
                objInvoice.ROW_DATE = datDate; 
                db.UpdateWithoutChangeLog(objInvoiceOld, objInvoice);
                //  db.Update(objInvoiceOld, objInvoice);
                //db.Insert(objPayDetailCancel);
                // db.UpdateChild(objInvoiceOld, objInvoice, objPaymentCancel, ref objChangeLog);
            }

            //submit payment detials. 
            db.TBL_PAYMENT_DETAILs.InsertAllOnSubmit(allCancalPaymentDetail);
            db.SubmitChanges();

            //check if that invoice paid with Bank payment
            if (objOLDPayment.BANK_PAYMENT_DETAIL_ID > 0)
            {
                TBL_BANK_PAYMENT objBank = db.TBL_BANK_PAYMENTs.FirstOrDefault(x => x.BANK_PAYMENT_ID == db.TBL_BANK_PAYMENT_DETAILs.
                            FirstOrDefault(a => a.BANK_PAYMENT_DETAIL_ID == objOLDPayment.BANK_PAYMENT_DETAIL_ID).BANK_PAYMENT_ID);
                // update bank service only
                if (objBank.BANK_PAYMENT_TYPE_ID != (int)BankPaymentTypes.SERVICE)
                {
                    return false;
                }

                //4 - Update Bank Payment Detail
                TBL_BANK_PAYMENT_DETAIL objBankPaymentDetail = db.TBL_BANK_PAYMENT_DETAILs.FirstOrDefault(x => x.BANK_PAYMENT_DETAIL_ID == objOLDPayment.BANK_PAYMENT_DETAIL_ID);
                objBankPaymentDetail.PAID_AMOUNT -= objOLDPayment.PAY_AMOUNT;

                foreach (var objDepositToCancel in db.TBL_CUS_DEPOSITs.Where(x => x.BANK_PAYMENT_DETAIL_ID == objOLDPayment.BANK_PAYMENT_DETAIL_ID))
                {
                    //cancelAmount += objDepositToCancel.AMOUNT;
                    objDepositToCancel.IS_PAID = false;
                    objBankPaymentDetail.PAID_AMOUNT -= objDepositToCancel.AMOUNT;
                    db.SubmitChanges();
                }
            }
            if (isRemovePrepay)
            {
                //update prepayment
                if (!new PrepaymentLogic().PaymentCancelRemovePrepay(objPayment, objPaymentCancel))
                {
                    return false;
                }
            }
            db.SubmitChanges();
            return true;
        }
        public static bool CancelBatchPayment(IQueryable<TBL_PAYMENT> Ipayments, DBDataContext db, bool isRemovePrepay = true)
        {
            //Load Local data
            //var allOldPaymentIds = payments.Select(x =>   x.PAYMENT_ID ).ToList();


            var IallOldPayments = (from p in DBDataContext.Db.TBL_PAYMENTs
                                   join id in Ipayments on p.PAYMENT_ID equals id.PAYMENT_ID
                                   select p);



            var IallPaymentDetails = from pd in db.TBL_PAYMENT_DETAILs
                                     join id in IallOldPayments on pd.PAYMENT_ID equals id.PAYMENT_ID
                                     select pd;

            //db.TBL_PAYMENT_DETAILs.Where(pd => allOldPayments.Select(x=>x.PAYMENT_ID).Contains(pd.PAYMENT_ID)).ToList();
            // var allInvoiceIds = allPaymentDetails.Select(x => x.INVOICE_ID);//.Distinct().ToList();

            var IallInvoices = from inv in db.TBL_INVOICEs
                               join pd in IallPaymentDetails on inv.INVOICE_ID equals pd.INVOICE_ID
                               select inv;

            var IallBankPayment = from b in db.TBL_BANK_PAYMENTs
                                  join d in db.TBL_BANK_PAYMENT_DETAILs on b.BANK_PAYMENT_ID equals d.BANK_PAYMENT_ID
                                  join p in IallOldPayments on d.BANK_PAYMENT_DETAIL_ID equals p.BANK_PAYMENT_DETAIL_ID
                                  select new { d.BANK_PAYMENT_DETAIL_ID, BankPayment = b };

            var IallOldBankPaymentDetails = from d in db.TBL_BANK_PAYMENT_DETAILs
                                            join p in IallOldPayments on d.BANK_PAYMENT_DETAIL_ID equals p.BANK_PAYMENT_DETAIL_ID
                                            select d;

            // .FirstOrDefault(x => x.BANK_PAYMENT_ID == db.TBL_BANK_PAYMENT_DETAILs.
            //FirstOrDefault(a => a.BANK_PAYMENT_DETAIL_ID == objOLDPayment.BANK_PAYMENT_DETAIL_ID).BANK_PAYMENT_ID);

            //db.TBL_INVOICEs.Where(inv => allInvoiceIds.Contains(inv.INVOICE_ID)).ToList();


            //to local 
            var allOldPayments = IallOldPayments.Distinct().ToList();
            var allPaymentDetails = IallPaymentDetails.Distinct().ToList();
            var allInvoices = IallInvoices.Distinct().ToList();

            //duplicated payment coz of Customer_meter ទឹកប្រាក់រួម create 2 invoice and paid in 1 payment. => reverse by invoice second time error 
            //payment already void.....
            var payments = Ipayments.ToList().Distinct().ToList();


            var CurrentLogin = Login.CurrentLogin;
            var CurrentCashDrawer = Login.CurrentCashDrawer;

            var allBankPayment = IallBankPayment.ToList();
            var allOldBankPaymentDetails = IallOldBankPaymentDetails.ToList();

            var allCusDeposit = (from c in db.TBL_CUS_DEPOSITs
                                 join p in IallOldPayments on c.BANK_PAYMENT_DETAIL_ID equals p.BANK_PAYMENT_DETAIL_ID
                                 select c).ToList();
            var i = 0;

            payments.ForEach(objPayment =>
            {
                i += 1;
                Console.WriteLine($"{i.ToString()}/{payments.Count.ToString()}");
                if (objPayment.IS_VOID)
                {
                    throw new Exception("Payment is already void!");
                }

                var objOLDPayment = allOldPayments.FirstOrDefault(x => x.PAYMENT_ID == objPayment.PAYMENT_ID);
                DateTime datDate = db.GetSystemDate();
                //Coz record is not reverse it just move to prepayment. there no cash drawer id for that record.
                // objOLDPayment.IS_VOID = true;

                //1- Add cancel payment
                TBL_PAYMENT objPaymentCancel = new TBL_PAYMENT()
                {
                    CREATE_BY = CurrentLogin.LOGIN_NAME,
                    CREATE_ON = datDate,
                    CUSTOMER_ID = objOLDPayment.CUSTOMER_ID,
                    DUE_AMOUNT = objOLDPayment.DUE_AMOUNT,
                    IS_ACTIVE = true,
                    PAY_AMOUNT = -1 * objOLDPayment.PAY_AMOUNT,
                    PAY_DATE = objPayment.PAY_DATE,
                    PAYMENT_NO = objOLDPayment.PAYMENT_NO,
                    USER_CASH_DRAWER_ID = 0,
                    CURRENCY_ID = objOLDPayment.CURRENCY_ID,
                    PARENT_ID = objOLDPayment.PAYMENT_ID,
                    IS_VOID = true,
                    PAYMENT_ACCOUNT_ID = objOLDPayment.PAYMENT_ACCOUNT_ID,
                    BANK_PAYMENT_DETAIL_ID = objOLDPayment.BANK_PAYMENT_DETAIL_ID,
                    NOTE = "",
                    EXCHANGE_RATE = objOLDPayment.EXCHANGE_RATE,
                    EXCHANGE_RATE_DATE = objOLDPayment.EXCHANGE_RATE_DATE,
                    PAYMENT_METHOD_ID = objOLDPayment.PAYMENT_METHOD_ID
                };


                //if paid from bank reversing record must have cash drawer same as orignal coz when paid from bank again 
                //it will have another cash drawer 
                //if(objOLDPayment.BANK_PAYMENT_DETAIL_ID > 0)
                //{
                //    objPaymentCancel.USER_CASH_DRAWER_ID = objOLDPayment.USER_CASH_DRAWER_ID;
                //}
                db.InsertWithoutChangeLog(objPaymentCancel);

                foreach (TBL_PAYMENT_DETAIL pd in allPaymentDetails.Where(x => x.PAYMENT_ID == objPayment.PAYMENT_ID))
                {
                    //2 - Add cancel payment detal
                    TBL_PAYMENT_DETAIL objPayDetailCancel = new TBL_PAYMENT_DETAIL();
                    objPayDetailCancel.PAYMENT_ID = objPaymentCancel.PAYMENT_ID;
                    objPayDetailCancel.DUE_AMOUNT = pd.DUE_AMOUNT;
                    objPayDetailCancel.INVOICE_ID = pd.INVOICE_ID;
                    objPayDetailCancel.PAY_AMOUNT = -1 * pd.PAY_AMOUNT;
                    db.InsertWithoutChangeLog(objPayDetailCancel);
                    //db.InsertChild(objPayDetailCancel, objPayDetailCancel, ref objChangeLog);

                    //3 - Update invoice after cancel payment
                    TBL_INVOICE objInvoiceOld = allInvoices.Where(inv => inv.INVOICE_ID == pd.INVOICE_ID).FirstOrDefault(); //db.TBL_INVOICEs.Where(inv => inv.INVOICE_ID == pd.INVOICE_ID).FirstOrDefault();
                    TBL_INVOICE objInvoice = new TBL_INVOICE();
                    objInvoiceOld._CopyTo(objInvoice);

                    objInvoice.PAID_AMOUNT -= pd.PAY_AMOUNT;
                    objInvoice.INVOICE_STATUS = objInvoice.SETTLE_AMOUNT == objInvoice.PAID_AMOUNT ? (int)InvoiceStatus.Close : (int)InvoiceStatus.Open;
                    objInvoice.ROW_DATE = datDate;

                    db.UpdateWithoutChangeLog(objInvoiceOld, objInvoice);
                    //  db.Update(objInvoiceOld, objInvoice);
                    //db.Insert(objPayDetailCancel);
                    // db.UpdateChild(objInvoiceOld, objInvoice, objPaymentCancel, ref objChangeLog);
                } 
                ////check if that invoice paid with Bank payment
                //if (objOLDPayment.BANK_PAYMENT_DETAIL_ID > 0)
                //{
                //    //TBL_BANK_PAYMENT objBank = db.TBL_BANK_PAYMENTs.FirstOrDefault(x => x.BANK_PAYMENT_ID == db.TBL_BANK_PAYMENT_DETAILs.
                //    //            FirstOrDefault(a => a.BANK_PAYMENT_DETAIL_ID == objOLDPayment.BANK_PAYMENT_DETAIL_ID).BANK_PAYMENT_ID);
                //    var objBank = allBankPayment.Where(x => x.BANK_PAYMENT_DETAIL_ID == objOLDPayment.BANK_PAYMENT_DETAIL_ID).Select(x=>x.BankPayment).FirstOrDefault();
                //    // update bank service only
                //    if (objBank.BANK_PAYMENT_TYPE_ID != (int)BankPaymentTypes.SERVICE)
                //    {
                //        return;
                //    }
                //    //4 - Update Bank Payment Detail
                //    TBL_BANK_PAYMENT_DETAIL objBankPaymentDetail = allOldBankPaymentDetails.FirstOrDefault(x => x.BANK_PAYMENT_DETAIL_ID == objOLDPayment.BANK_PAYMENT_DETAIL_ID); //db.TBL_BANK_PAYMENT_DETAILs.FirstOrDefault(x => x.BANK_PAYMENT_DETAIL_ID == objOLDPayment.BANK_PAYMENT_DETAIL_ID);
                //    objBankPaymentDetail.PAID_AMOUNT -= objOLDPayment.PAY_AMOUNT;

                //    foreach (var objDepositToCancel in allCusDeposit.Where(x => x.BANK_PAYMENT_DETAIL_ID == objOLDPayment.BANK_PAYMENT_DETAIL_ID))
                //    {
                //        //cancelAmount += objDepositToCancel.AMOUNT;
                //        objDepositToCancel.IS_PAID = false;
                //        objBankPaymentDetail.PAID_AMOUNT -= objDepositToCancel.AMOUNT;
                //        db.SubmitChanges();
                //    }
                //}
                if (isRemovePrepay && objPayment.USER_CASH_DRAWER_ID ==0 )
                {
                    //update prepayment
                    if (!new PrepaymentLogic().PaymentCancelRemovePrepay(objPayment, objPaymentCancel))
                    {
                        return;
                    }
                }
            });

            db.SubmitChanges();
            return true;
        }


        public static bool CancelPrepayment(int Prepayment_Id)
        {
            var logic = new PrepaymentLogic();
            DateTime datDate = DBDataContext.Db.GetSystemDate();
            var objToRevese = DBDataContext.Db.TBL_CUS_PREPAYMENTs.FirstOrDefault(x => x.CUS_PREPAYMENT_ID == Prepayment_Id);
            if (objToRevese == null)
            {
                return false;
            }
            var CusPrepay = logic.getLastestPrepayCus(objToRevese.CUSTOMER_ID, objToRevese.CURRENCY_ID);
            var objCusPrepayment = logic.RemoveCustomerPrepayment(datDate, objToRevese.CUSTOMER_ID, -objToRevese.AMOUNT, CusPrepay.BALANCE - objToRevese.AMOUNT, objToRevese.CURRENCY_ID, Method.GetNextSequence(Sequence.Receipt, true), "", PrepaymentAction.CancelPrepayment, objToRevese.CUS_PREPAYMENT_ID, objToRevese.USER_CASH_DRAWER_ID);
            //If settle transaction is not use cash drawer so the reverse transaction shall not effect to cash drawer as well......
            objCusPrepayment.USER_CASH_DRAWER_ID = Login.CurrentCashDrawer.USER_CASH_DRAWER_ID;
            DBDataContext.Db.TBL_CUS_PREPAYMENTs.InsertOnSubmit(objCusPrepayment);
            DBDataContext.Db.SubmitChanges();
            return true;
        }

        public static void CancelPayment(TBL_PAYMENT objPayment, DateTime payDate)
        {

        }

        public static List<TBL_PAYMENT> getPaymentByInv(List<TBL_INVOICE> invoices)
        {
            var result = from i in invoices
                         join pd in DBDataContext.Db.TBL_PAYMENT_DETAILs on i.INVOICE_ID equals pd.INVOICE_ID
                         join p in DBDataContext.Db.TBL_PAYMENTs on pd.PAYMENT_ID equals p.PAYMENT_ID
                         where p.IS_VOID == false
                         select p;
            return result.Distinct().ToList();
        }
        public static List<ReportPaymentModel> getPaymentReportData(ReportPaymentSearchParam model)
        {
            string ConnectionString = Method.GetConnectionString(Settings.Default.CONNECTION);
            var dt = new DataTable();
            using (var cnn = new SqlConnection(ConnectionString))
            {
                if (cnn.State == ConnectionState.Closed)
                {
                    cnn.Open();
                }
                using (var cmd = new SqlCommand("dbo.REPORT_CUSTOMER_PAYMENT_DETAIL", cnn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;
                    cmd.Parameters.AddWithValue("@DATE_PAID_FROM", model.DATE_PAID_FROM);
                    cmd.Parameters.AddWithValue("@DATE_PAID_TO", model.DATE_PAID_TO);
                    Runner.RunNewThread(() => dt.Load(cmd.ExecuteReader()));
                }
            }
            var result = dt.ToDataListModel<ReportPaymentModel>();

            var allBank = result.Select(x => x.PAYMENT_METHOD).Distinct().ToList();

            var allBankConfig = DBDataContext.Db.TBL_PAYMENT_CONFIGs.ToList();

            result.ForEach(x =>
            {
                if (x.PAY_AT_BANK != "-")
                {
                    var bankpayment = allBankConfig.FirstOrDefault(p => p.PAYMENT_TYPE == x.PAYMENT_METHOD);
                    if (bankpayment != null)
                    {
                        if (bankpayment.CUT_OFF_DATE_PAYMENT != null)
                        {
                            var bcd = x.BANK_CUT_OFF_DATE;
                            var dbcd = bankpayment.CUT_OFF_DATE_PAYMENT.Value;

                            x.BANK_CUT_OFF_DATE = new DateTime(bcd.Year, bcd.Month, bcd.Day, dbcd.Hour, dbcd.Minute, dbcd.Second);
                            if (x.EXACT_PAID_DATE > x.BANK_CUT_OFF_DATE)
                            {
                                x.BANK_CUT_OFF_DATE = GetNextDay(x.BANK_CUT_OFF_DATE);
                            }
                        }
                    }
                }
            });
            return result;
        }
        public static IQueryable<TBL_PAYMENT> getPaymentByInvIQuery(IQueryable<TBL_INVOICE> invoices)
        {
            var result = from i in invoices
                         join pd in DBDataContext.Db.TBL_PAYMENT_DETAILs on i.INVOICE_ID equals pd.INVOICE_ID
                         join p in DBDataContext.Db.TBL_PAYMENTs on pd.PAYMENT_ID equals p.PAYMENT_ID
                         where p.IS_VOID == false
                         select p;
            return result;
        }
        public static DateTime GetNextDay(DateTime datetime)
        {
            var holiday = DBDataContext.Db.TBL_BANK_HOLIDAYs.Where(x => !x.IS_HOLIDAY && x.DAY > datetime).OrderBy(x => x.DAY)?.FirstOrDefault();
            return holiday.DAY;
        }
    }

    public class InvoicePaymentAmount
    {
        public int PAYMENT_ID { get; set; }
        public decimal INVOICE_AMOUNT { get; set; }
    }
}
