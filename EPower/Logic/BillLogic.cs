﻿using EPower.Base.Logic;
using EPower.Properties;
using HB01.Logics.Security;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Logic
{
    public class BillLogic
    {
        //public static BillLogic Instance = new BillLogic();
        string flag = "";

        public bool Runbill(List<int> cycleIds, DateTime datRunMonth, DateTime datStart, DateTime datEnd, DateTime datDueDate, DateTime datStartPayDate, DateTime dtpInvoiceDate)
        {
            bool isComplete = false;
            bool backupOk = false;
            //backup before runbill
            flag = "Before Run Bill";
            Runner.RunNewThread(delegate ()
            {
                BackupDatabase();
            }, Resources.MS_RUN_BILL_BACKUP);
            //Runner.RunNewThread(BackupDatabase, Resources.MS_RUN_BILL_BACKUP);
            if (Runner.Instance.Error != null)
            {
                return isComplete;
            }
            // start running bill.
            Runner.RunNewThread(delegate
            {
                try
                {
                    foreach (int cycleId in cycleIds)
                    {
                        using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { Timeout = new TimeSpan(1, 0, 0) }))
                        {
                            if (DBDataContext.Db.TBL_RUN_BILLs.Where(x => x.CYCLE_ID == cycleId && x.BILLING_MONTH == datRunMonth && x.IS_ACTIVE).Count() > 0)
                            {
                                tran.Dispose();
                                return;
                            }

                            DBDataContext.Db.CommandTimeout = 3600;
                            DBDataContext.Db.ExecuteCommand(@"EXEC RUN_BILL @MONTH={0},@START_DATE={1},@END_DATE={2},@CYCLE_ID={3},@CREATE_BY={4},@DUE_DATE={5},@START_PAY_DATE={6},@INVOICE_DATE={7};",
                                                             datRunMonth,
                                                             datStart,
                                                             datEnd,
                                                             cycleId,
                                                             Login.CurrentLogin.LOGIN_NAME,
                                                             datDueDate,
                                                             datStartPayDate,
                                                             dtpInvoiceDate);
                            logRunBill(datRunMonth);
                            // run bill customer merge
                            DBDataContext.Db.ExecuteCommand(@"EXEC RUN_BILL_CUSTOMER_MERGE @MONTH={0},@CYCLE_ID={1},@CREATED_BY={2};",
                                                                        datRunMonth,
                                                                        cycleId,
                                                                        Login.CurrentLogin.LOGIN_NAME);
                            // run usage summary
                            DBDataContext.Db.ExecuteCommand(@"EXEC RUN_USAGE_SUMMARY @MONTH={0},@BILLING_CYCLE_ID={1};",
                                                                        datRunMonth, cycleId);

                            // RUN_BILL_CUSTOMER_COVID19_SUBSIDIZED
                            //if (DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_COVID19_SUBSIDIZED]))
                            //{
                            //    if (datRunMonth.Year == 2020 && (datRunMonth.Month >= 06 && datRunMonth.Month <= 10))
                            //    {
                            //        DBDataContext.Db.ExecuteCommand(@"EXEC RUN_BILL_CUSTOMER_COVID19_SUBSIDIZED @MONTH={0},@CYCLE_ID={1},@USER_CASH_DRAWER_ID={2};",
                            //                                            datRunMonth,
                            //                                            cycleId,
                            //                                            Logic.Login.CurrentCashDrawer.USER_CASH_DRAWER_ID);
                            //    }
                            //}
                            tran.Complete();
                            isComplete = true;
                        }
                    }
                }
                catch(Exception ex)
                {
                    MsgBox.LogError(ex);
                    throw new Exception(string.Format(Base.Properties.Resources.YOU_CANNOT_PROCESS, "", Base.Properties.Resources.RUN_BILL));
                }
            }, Resources.MS_RUN_BILL_IN_PROGRESS);

            if (isComplete)
            {
                foreach (int cycleId in cycleIds)
                {
                    using (TransactionScope tran = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions() { Timeout = new TimeSpan(1, 0, 0), IsolationLevel = IsolationLevel.ReadUncommitted }))
                    {
                        //Clear payment by Prepay
                        if (DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_PREPAYMENT_SERVICE]))
                        {
                            Runner.RunNewThread(() => { new PrepaymentLogic().ClearPaymentWithPrepay(cycleId); }, Resources.PREPAYMENT_APPLY_PAYMENT);
                            tran.Complete();
                        }
                    }
                }
            }
            DBDataContext.Db.Refresh();
            return isComplete;
        }

        public bool ReverseBill(int runid, int cycleid)
        {

            bool isComplete = false;
            var totalPayment = 0;
            var canceledPayment = 0;
            DateTime now = DBDataContext.Db.GetSystemDate();
            if (!isCanReverse(runid, cycleid))
            {
                return false;
            }
            flag = "Backup before reverse";
            Runner.RunNewThread(BackupDatabase, Resources.MS_RUN_BILL_BACKUP);
            if (Runner.Instance.Error != null)
            {
                return false;
            }

            Runner.RunNewThread(() =>
            {
                try
                {
                    using (var tran = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = IsolationLevel.ReadUncommitted, Timeout = TransactionManager.MaximumTimeout }))
                    {
                        //DBDataContext.Db.CommandTimeout = 3600;
                        TBL_REVERSE_BILL objReverse = new TBL_REVERSE_BILL()
                        {
                            RUN_ID = runid,
                            REVERSE_BY = Login.CurrentLogin.LOGIN_NAME,
                            REVERSE_DATE = now
                        };
                        DBDataContext.Db.Insert(objReverse);
                        //update TBL_RUN_BILL
                        TBL_RUN_BILL objRunBill = DBDataContext.Db.TBL_RUN_BILLs.FirstOrDefault(x => x.RUN_ID == runid);
                        objRunBill.IS_ACTIVE = false;

                        //clear invoices
                        // List<TBL_INVOICE> paidInvoices = InvoiceLogic.getPaidInvoices(runid);
                        var paidInvoices = InvoiceLogic.getPaidInvoiceIQueryAble(runid);
                        if (paidInvoices.Count() > 0)
                        {
                            // List<TBL_PAYMENT> payments = Payment.getPaymentByInv(paidInvoices);
                            IQueryable<TBL_PAYMENT> payments = Payment.getPaymentByInvIQuery(paidInvoices);

                            //move only payment not from prepayment .
                            var movePayment = payments.Where(x => x.USER_CASH_DRAWER_ID != 0).ToList();
                            //var moveToPrepayment  = payments.ToList();
                            Payment.CancelBatchPayment(payments, DBDataContext.Db, true);
                            new PrepaymentLogic().MovePaymentToPrepay(movePayment);
                        }

                        //update recurring service
                        var invoices = DBDataContext.Db.TBL_INVOICEs.Where(x => x.RUN_ID == runid);
                        var invRecurring = (from i in invoices
                                            join id in DBDataContext.Db.TBL_INVOICE_DETAILs on i.INVOICE_ID equals id.INVOICE_ID
                                            join it in DBDataContext.Db.TBL_INVOICE_ITEMs on id.INVOICE_ITEM_ID equals it.INVOICE_ITEM_ID
                                            where it.IS_RECURRING_SERVICE
                                            select i);

                        if (invRecurring.ToList().Count > 0)
                        {
                            var cusRecurring = (from i in invRecurring
                                                join cs in DBDataContext.Db.TBL_CUSTOMER_SERVICEs on i.CUSTOMER_ID equals cs.CUSTOMER_ID
                                                where cs.IS_ACTIVE
                                                select cs);

                            foreach (var obj in cusRecurring)
                            {
                                obj.LAST_BILLING_MONTH = obj.LAST_BILLING_MONTH.AddMonths(-1);
                                obj.NEXT_BILLING_MONTH = obj.NEXT_BILLING_MONTH.AddMonths(-1);
                                //obj.ACCUMULATED_AMOUNT -= invRecurring.FirstOrDefault(x => x.CUSTOMER_ID == obj.CUSTOMER_ID).TOTAL_AMOUNT;
                            }
                            //DBDataContext.Db.SubmitChanges();
                        }

                        string sql = string.Format(@"EXEC dbo.REVERSE_BILL @RUN_ID={0}, @REVERSE_BY=N'{1}',  @USER_CASH_DRAWER_ID={2}", runid, Login.CurrentLogin.LOGIN_NAME, 0);
                        DBDataContext.Db.ExecuteCommand(sql);
                        //foreach (var inv in invoices)
                        //{
                        //    inv.INVOICE_TITLE = inv.INVOICE_TITLE + "(លុប)";
                        //    inv.INVOICE_STATUS = (int)InvoiceStatus.Cancel;
                        //    inv.TOTAL_AMOUNT = inv.SETTLE_AMOUNT = 0;
                        //    inv.ROW_DATE = now;
                        //}
                        //invoices.ForEach(x => { x.INVOICE_TITLE = x.INVOICE_TITLE + "(លុប)"; x.INVOICE_STATUS = (int)InvoiceStatus.Cancel; x.TOTAL_AMOUNT = x.SETTLE_AMOUNT = 0; x.ROW_DATE = now; });
                        //foreach (var item in invoices)
                        //{
                        //    item.INVOICE_TITLE = item.INVOICE_TITLE + "(លុប)";
                        //    item.INVOICE_STATUS = (int)InvoiceStatus.Cancel;
                        //    item.TOTAL_AMOUNT = item.SETTLE_AMOUNT = 0;
                        //    item.ROW_DATE = now;
                        //}
                        DBDataContext.Db.SubmitChanges();
                        tran.Complete();
                        isComplete = true;
                        invoices = null;
                    }
                }
                catch(Exception ex)
                {
                    MsgBox.LogError(ex);
                    throw new Exception(string.Format(Base.Properties.Resources.YOU_CANNOT_PROCESS, "", Base.Properties.Resources.REVERSE_BILL));
                }
                
            }, Resources.MSG_REVERSE_BILL_IN_PROGRESS);
            DBDataContext.Db = null;
            return isComplete;
        }

        public void VoidInvoice(long invId)
        {
            var now = DBDataContext.Db.GetSystemDate();
            TBL_INVOICE invoice = DBDataContext.Db.TBL_INVOICEs.FirstOrDefault(x => x.INVOICE_ID == invId);

            var adjs = from a in DBDataContext.Db.TBL_INVOICE_ADJUSTMENTs
                       where a.INVOICE_ID == invId
                       group a by a.INVOICE_ITEM_ID into g
                       select new
                       {
                           INVOICE_ITEM_ID = g.Key,
                           ADJUST_AMOUNT = g.Sum(x => x.ADJUST_AMOUNT),
                           TAX_ADJUST_AMOUNT = g.Sum(x => x.TAX_ADJUST_AMOUNT)
                       };


            var invDetails = (from id in DBDataContext.Db.TBL_INVOICE_DETAILs
                              join a in adjs on id.INVOICE_ITEM_ID equals a.INVOICE_ITEM_ID into ad
                              from l in ad.DefaultIfEmpty()
                              where id.INVOICE_ID == invId && id.INVOICE_ITEM_ID != (int)InvoiceItem.Adjustment
                              orderby id.INVOICE_ID, id.INVOICE_DETAIL_ID descending
                              select new
                              {
                                  INVOICE_ID = id.INVOICE_ID,
                                  INVOICE_ITEM_ID = id.INVOICE_ITEM_ID,
                                  PRICE = id.PRICE,
                                  AMOUNT = (id.AMOUNT + ((decimal?)l.ADJUST_AMOUNT ?? 0)) * -1,
                                  TAX_ID = id.TAX_ID,
                                  TAX_AMOUNT = (id.TAX_AMOUNT + ((decimal?)l.TAX_ADJUST_AMOUNT ?? 0)) * -1,
                                  CHARGE_DESCRIPTION = id.REF_NO,
                                  EXCHANGE_RATE = id.EXCHANGE_RATE,
                                  EXCHANGE_RATE_DATE = id.EXCHANGE_RATE_DATE
                              }).ToList();

            foreach (var item in invDetails)
            {
                TBL_INVOICE_DETAIL obj = new TBL_INVOICE_DETAIL()
                {
                    INVOICE_ID = item.INVOICE_ID,
                    REF_NO = Method.GetNextSequence(Sequence.Reverse, true),
                    START_USAGE = 0,
                    END_USAGE = 0,
                    USAGE = 0,
                    PRICE = item.PRICE,
                    AMOUNT = item.AMOUNT,
                    TAX_AMOUNT = item.TAX_AMOUNT,
                    TAX_ID = item.TAX_ID,
                    INVOICE_ITEM_ID = item.INVOICE_ITEM_ID,
                    CHARGE_DESCRIPTION = item.CHARGE_DESCRIPTION,
                    TRAN_DATE = now,
                    EXCHANGE_RATE = item.EXCHANGE_RATE,
                    EXCHANGE_RATE_DATE = item.EXCHANGE_RATE_DATE
                };
                DBDataContext.Db.TBL_INVOICE_DETAILs.InsertOnSubmit(obj);
            }

            invoice.INVOICE_TITLE = invoice.INVOICE_TITLE + "(លុប)";
            invoice.INVOICE_STATUS = (int)InvoiceStatus.Cancel;
            invoice.TOTAL_AMOUNT = invoice.SETTLE_AMOUNT = 0;
            DBDataContext.Db.SubmitChanges();
        }




        // to prevent if any duplicated usage record
        public static void RemoveDuplicatedUsage()
        {
            string ConnectionString = Method.GetConnectionString(Settings.Default.CONNECTION);
            using (var cnn = new SqlConnection(ConnectionString))
            {
                if (cnn.State == System.Data.ConnectionState.Closed)
                {
                    cnn.Open();
                }
                var sqlText = @"WITH c AS (
                                SELECT CUSTOMER_ID, METER_ID, nrow = ROW_NUMBER() OVER(PARTITION BY CUSTOMER_ID, METER_ID, USAGE_MONTH ORDER BY USAGE_ID DESC), USAGE_ID
                                FROM dbo.TBL_USAGE
                                WHERE USAGE_MONTH >= '2021-11-01')
                                DELETE FROM dbo.TBL_USAGE WHERE USAGE_ID IN (SELECT c.USAGE_ID FROM c WHERE c.nrow > 1)";
                using (var cmd = new SqlCommand(sqlText, cnn))
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandTimeout = 0; 
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void reNewInvoice(TBL_INVOICE invoice, decimal price, long oldInvId)
        {
            TBL_INVOICE_USAGE invUsage;
            var now = DBDataContext.Db.GetSystemDate();
            var exchangeRate = new ExchangeRateLogic().findLastExchangeRate(now, invoice.CURRENCY_ID);
            //var oldInvUsage = DBDataContext.Db.TBL_INVOICE_USAGEs.FirstOrDefault(x => x.INVOICE_ID == invoice.INVOICE_ID);
            var lst = DBDataContext.Db.TBL_INVOICE_DETAILs.Where(x => x.INVOICE_ID == invoice.INVOICE_ID && x.INVOICE_ITEM_ID != (int)InvoiceItem.Adjustment && x.INVOICE_ITEM_ID != (int)InvoiceItem.Reverse).OrderByDescending(x => x.INVOICE_ITEM_ID).ToList();
            var amount = invoice.TOTAL_AMOUNT;

            //for calculate forward amount
            var i = DBDataContext.Db.TBL_INVOICEs.Where(x => x.CUSTOMER_ID == invoice.CUSTOMER_ID && x.CURRENCY_ID == invoice.CURRENCY_ID && x.IS_SERVICE_BILL == invoice.IS_SERVICE_BILL && x.INVOICE_STATUS != (int)InvoiceStatus.Cancel).OrderByDescending(x => x.INVOICE_ID).FirstOrDefault();
            //oldInvUsage._CopyTo(invUsage);
            //add invoice
            //GET FORWARD AMOUNT BY CURRENCY;
            var lastInvoice = DBDataContext.Db.TBL_INVOICEs.Where(x => !x.IS_SERVICE_BILL && x.CUSTOMER_ID == invoice.CUSTOMER_ID && x.CURRENCY_ID == invoice.CURRENCY_ID && x.INVOICE_STATUS != (int)InvoiceStatus.Cancel).OrderByDescending(x => x.INVOICE_ID).FirstOrDefault();
            var forwardAmount = 0m;
            if (lastInvoice != null)
            {
                forwardAmount = (lastInvoice.TOTAL_AMOUNT - lastInvoice.SETTLE_AMOUNT);
            }
            var amountWithForward = amount + forwardAmount;
            invoice.INVOICE_STATUS = (invoice.SETTLE_AMOUNT == 0) ? (int)InvoiceStatus.Close : invoice.INVOICE_STATUS = (int)InvoiceStatus.Open;
            invoice.INVOICE_DATE = now;
            invoice.PRICE = price;
            //invoice.EXCHANGE_RATE = exchangeRate.EXCHANGE_RATE;
            //invoice.EXCHANGE_RATE_DATE = exchangeRate.CREATE_ON;

            if (invoice.CURRENCY_ID == 1)
            {
                amountWithForward = DataHelper.ParseToDecimal(amountWithForward.ToString("N0"));
            }
            if (DataHelper.ParseToBoolean(Method.GetUtilityValue(Utility.KEEP_BILLING_REMAIN_AMOUNT)))
            {
                invoice.SETTLE_AMOUNT = UIHelper.Round(amountWithForward, invoice.CURRENCY_ID);
            }
            else
            {
                invoice.SETTLE_AMOUNT = amountWithForward;
            }
            invoice.TOTAL_AMOUNT = amountWithForward;
            invoice.FORWARD_AMOUNT = forwardAmount;
            DBDataContext.Db.TBL_INVOICEs.InsertOnSubmit(invoice);
            DBDataContext.Db.SubmitChanges();

            //add invoice detail
            foreach (var obj in lst)
            {
                TBL_INVOICE_DETAIL item = new TBL_INVOICE_DETAIL();
                obj._CopyTo(item);
                var itemPrice = DBDataContext.Db.TBL_INVOICE_ITEMs.FirstOrDefault(x => x.INVOICE_ITEM_ID == item.INVOICE_ITEM_ID);
                item.INVOICE_ID = invoice.INVOICE_ID;
                item.REF_NO = invoice.INVOICE_NO;
                item.TRAN_DATE = now;
                item.EXCHANGE_RATE = exchangeRate.EXCHANGE_RATE;
                item.EXCHANGE_RATE_DATE = exchangeRate.CREATE_ON;
                item.PRICE = item.INVOICE_ITEM_ID == 1 ? price : itemPrice.PRICE;
                item.AMOUNT = item.INVOICE_ITEM_ID == 1 ? amount : itemPrice.PRICE;
                if (invoice.CURRENCY_ID == 1)
                {
                    item.AMOUNT = DataHelper.ParseToDecimal(item.AMOUNT.ToString("N0"));
                }
                item.CHARGE_DESCRIPTION = item.INVOICE_ITEM_ID == 1 ? "" : itemPrice.INVOICE_ITEM_NAME;
                item.TAX_AMOUNT = 0;
                if (item.INVOICE_ITEM_ID == (int)InvoiceItem.Power)
                {
                    item.END_USAGE = invoice.TOTAL_USAGE;
                    item.USAGE = invoice.TOTAL_USAGE;
                }
                item.INVOICE_DETAIL_ID = 0;
                amount -= item.AMOUNT;
                DBDataContext.Db.TBL_INVOICE_DETAILs.InsertOnSubmit(item);
                DBDataContext.Db.SubmitChanges();
            }

            //add invoice usage
            List<TBL_INVOICE_USAGE> lstInvUsage = DBDataContext.Db.TBL_INVOICE_USAGEs.Where(x => x.INVOICE_ID == oldInvId).ToList();
            foreach (var obj in lstInvUsage)
            {
                invUsage = new TBL_INVOICE_USAGE();
                obj._CopyTo(invUsage);
                invUsage.INVOICE_ID = (int)invoice.INVOICE_ID;
                DBDataContext.Db.TBL_INVOICE_USAGEs.InsertOnSubmit(invUsage);
            }
            DBDataContext.Db.SubmitChanges();

            var balance = PrepaymentLogic.Instance.GetCustomerPrepayment(invoice.CUSTOMER_ID, invoice.CURRENCY_ID);
            if (balance > 0)
            {
                PrepaymentLogic.Instance.ClearPaymentWithPrepay(new List<TBL_INVOICE>() { invoice });
            }
        }

        private bool isCanReverse(int runid, int cycleid)
        {
            bool valid = true;

            if (runid != (DBDataContext.Db.TBL_RUN_BILLs.Where(x => x.CYCLE_ID == cycleid).Max(x => x.RUN_ID)))
            {
                MsgBox.ShowInformation(String.Format(Resources.CANNOT_REVERSE_OLD_BILL), String.Format(Resources.CANNOT_REVERSE));
                valid = false;
            }
            return valid;
        }

        private void logRunBill(DateTime dtpRunMonth)
        {
            // last run bill.
            TBL_RUN_BILL objRun = DBDataContext.Db.TBL_RUN_BILLs.OrderByDescending(row => row.RUN_ID).FirstOrDefault();
            string pathLog = Application.StartupPath + "/Log/";
            if (!Directory.Exists(pathLog))
            {
                Directory.CreateDirectory(pathLog);
            }

            // log billing summary
            string pathLogBillSummary = pathLog + dtpRunMonth.ToString("yyyy-MM") + "-bill-summary.log";
            if (File.Exists(pathLogBillSummary)) File.Delete(pathLogBillSummary);
            StringBuilder strBS = new StringBuilder();
            strBS.AppendLine(this.ToCSVHeader(typeof(TBL_RUN_BILL)));
            strBS.AppendLine(ToCSV(objRun));
            File.WriteAllText(pathLogBillSummary, strBS.ToString());

            // log AR before run
            string pathLogAgingBeforeRun = pathLog + dtpRunMonth.ToString("yyyy-MM") + "-aging-before.log";
            if (File.Exists(pathLogAgingBeforeRun)) File.Delete(pathLogAgingBeforeRun);
            StringBuilder strAB = new StringBuilder();
            strAB.AppendLine(ToCSVHeader(typeof(TBL_RUN_BILL_AGING)));
            foreach (var x in DBDataContext.Db.TBL_RUN_BILL_AGINGs.Where(row => row.RUN_ID == objRun.RUN_ID && row.IS_FEFORE))
            {
                strAB.AppendLine(ToCSV(x));
            }
            File.WriteAllText(pathLogAgingBeforeRun, strAB.ToString());

            // log AR after run
            string pathLogAgingAfterRun = pathLog + dtpRunMonth.ToString("yyyy-MM") + "-aging-after.log";
            if (File.Exists(pathLogAgingAfterRun)) File.Delete(pathLogAgingAfterRun);
            StringBuilder strAA = new StringBuilder();
            strAA.AppendLine(ToCSVHeader(typeof(TBL_RUN_BILL_AGING)));
            foreach (var x in DBDataContext.Db.TBL_RUN_BILL_AGINGs.Where(row => row.RUN_ID == objRun.RUN_ID && !row.IS_FEFORE))
            {
                strAA.AppendLine(ToCSV(x));
            }
            File.WriteAllText(pathLogAgingAfterRun, strAA.ToString());

            // log invoice
            string pathLogInvoice = pathLog + dtpRunMonth.ToString("yyyy-MM") + "-invoice.log";
            if (File.Exists(pathLogInvoice)) File.Delete(pathLogInvoice);
            StringBuilder strI = new StringBuilder();
            strI.AppendLine(ToCSVHeader(typeof(TBL_RUN_BILL_INVOICE)));
            foreach (var x in DBDataContext.Db.TBL_RUN_BILL_INVOICEs.Where(row => row.RUN_ID == objRun.RUN_ID))
            {
                strI.AppendLine(ToCSV(x));
            }
            File.WriteAllText(pathLogInvoice, strI.ToString());
        }

        private void BackupDatabase()
        {
            Application.DoEvents();
            string strBackupFileName = DBDataContext.Db.Connection.Database + " " + DBDataContext.Db.GetSystemDate().ToString("yyyyMMddHHmmss") + " " + flag + ".bak"; 
            SoftTech.Security.Logic.DBA.BackupToDisk(Settings.Default.PATH_BACKUP, strBackupFileName);

            if (PointerLogic.isConnectedPointer)
            {
                try
                {
                    string accountintFileName = DBDataContext.Db.Connection.Database  + "_Accounting"+ " " + DBDataContext.Db.GetSystemDate().ToString("yyyyMMddHHmmss") + " " + flag + ".bak";
                    var fileName = AppLogic.Instance.BackUpDatabase(Settings.Default.PATH_BACKUP, accountintFileName); 
                }
                catch (Exception ex)
                {
                    MsgBox.ShowWarning(ex.Message, Resources.BACKUP);
                }
            }
        }
        private string ToCSVHeader(Type type)
        {
            StringBuilder str = new StringBuilder();
            foreach (var pi in type.GetProperties())
            {
                str.Append(pi.Name);
                str.Append(',');
            }
            return str.ToString().Substring(0, str.Length - 1);

        }

        private string ToCSV<T>(T obj)
        {
            StringBuilder str = new StringBuilder();
            foreach (var pi in typeof(T).GetProperties())
            {
                str.Append(pi.GetValue(obj, null));
                str.Append(',');
            }
            return str.ToString().Substring(0, str.Length - 1);
        }
    }
}
