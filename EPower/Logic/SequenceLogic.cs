﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EPower.Logic
{
    class SequenceLogic
    {
        public static string StringFormat(string format, int currentValue)
        {
            format = format
                .Replace("{YYYY}", DateTime.Now.ToString("yyyy"))
                .Replace("{YY}", DateTime.Now.ToString("yy"))
                .Replace("{MM}", DateTime.Now.ToString("MM"))
                .Replace("{DD}", DateTime.Now.ToString("dd"))
                .Replace("N:", "0:d");
            return string.Format(format, (currentValue));
        }

        public static Dictionary<string, string> ParameterAndDscriptionList = new Dictionary<string, string>()
        {
            {"{N:6}", $"លទ្ឋផល៖​ {StringFormat("{0:d6}", 1)}, តំណាងអោយលេខកើនឡើង ហើយបំពេញអោយគ្រប់ ៦ខ្ទង់ដោយលេខ ០" },
            {"{YYYY}", $"លទ្ឋផល៖​ {DateTime.Now.ToString("yyyy")},​ តំណាងអោយឆ្នាំ យកគ្រប់៤ខ្ទង់" },
            {"{YY}",  $"លទ្ឋផល៖​ {DateTime.Now.ToString("yy")},​ តំណាងអោយឆ្នាំ យក២ខ្ទង់ចុងក្រោយ"},
            {"{MM}",  $"លទ្ឋផល៖​ {DateTime.Now.ToString("MM")},​ តំណាងអោយខែ"}
        };
    }
}
