﻿using System;
using System.Data;
using System.IO;
using System.Net;
using System.Windows.Forms;
using SoftTech;
using static System.Net.WebRequestMethods;

namespace EPower.Logic
{
    public class FTP
    {
        #region Data
        string _strFtpServer = "";
        string _strFtpUser = "";
        string _strFtpPassword = "";
        #endregion Data

        public void SetCredential(string strServer, string strUserName, string strPassword)
        {
            _strFtpServer = strServer;
            _strFtpUser = strUserName;
            _strFtpPassword = strPassword;
        }

        /// <summary>
        /// Method to upload the specified file to the specified FTP Server
        /// </summary>
        /// <param name="filename">file full name to be uploaded</param>
        public void Upload(string filename)
        {    
            FileInfo fUpload = new FileInfo(filename);             
            string uri = "ftp://" + _strFtpServer + "/" + fUpload.Name;
            FtpWebRequest reqFTP;

            // Create FtpWebRequest object from the Uri provided
            reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + _strFtpServer + "/" + fUpload.Name));

            // Provide the WebPermission Credintials
            reqFTP.Credentials = new NetworkCredential(_strFtpUser, _strFtpPassword);

            // By default KeepAlive is true, where the control connection is not closed
            // after a command is executed.
            reqFTP.KeepAlive = false;

            // Specify the command to be executed.
            reqFTP.Method = WebRequestMethods.Ftp.UploadFile;

            // Specify the data transfer type.
            reqFTP.UseBinary = true;

            // Notify the server about the size of the uploaded file
            reqFTP.ContentLength = fUpload.Length;

            // The buffer size is set to 2kb
            int buffLength = 2048;
            byte[] buff = new byte[buffLength];
            int contentLen;

            // Opens a file stream (System.IO.FileStream) to read the file to be uploaded
            FileStream fs = fUpload.OpenRead();

            try
            {
                // Stream to which the file to be upload is written
                Stream strm = reqFTP.GetRequestStream();

                // Read from the file stream 2kb at a time
                contentLen = fs.Read(buff, 0, buffLength);

                // Till Stream content ends
                while (contentLen != 0)
                {
                    // Write Content from the file stream to the FTP Upload Stream
                    strm.Write(buff, 0, contentLen);
                    contentLen = fs.Read(buff, 0, buffLength);
                }

                // Close the file stream and the Request Stream
                strm.Close();
                fs.Close();
                writeConfig(fUpload.FullName);
              //  UploadComplete(filename);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Upload Error");
            }
        }

        public void UploadComplete(string fileUpload)
        {
            string fileComplete = fileUpload.Replace(".zip", ".txt");            
            
            FileInfo fUpload = new FileInfo(fileComplete);            
            fUpload.Create();
            fUpload.OpenRead().Close();
            
            string uri = "ftp://" + _strFtpServer + "/" + fUpload.Name;
            FtpWebRequest reqFTP;

            // Create FtpWebRequest object from the Uri provided
            reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + _strFtpServer + "/" + fUpload.Name));

            // Provide the WebPermission Credintials
            reqFTP.Credentials = new NetworkCredential(_strFtpUser, _strFtpPassword);

            // By default KeepAlive is true, where the control connection is not closed
            // after a command is executed.
            reqFTP.KeepAlive = false;

            // Specify the command to be executed.
            reqFTP.Method = WebRequestMethods.Ftp.UploadFile;

            // Specify the data transfer type.
            reqFTP.UseBinary = true;

            // Notify the server about the size of the uploaded file
            reqFTP.ContentLength = fUpload.Length;

            // The buffer size is set to 2kb
            int buffLength = 2048;
            byte[] buff = new byte[buffLength];
            int contentLen;

            // Opens a file stream (System.IO.FileStream) to read the file to be uploaded
            FileStream fs = fUpload.OpenRead();

            try
            {
                // Stream to which the file to be upload is written
                Stream strm = reqFTP.GetRequestStream();

                // Read from the file stream 2kb at a time
                contentLen = fs.Read(buff, 0, buffLength);

                // Till Stream content ends
                while (contentLen != 0)
                {
                    // Write Content from the file stream to the FTP Upload Stream
                    strm.Write(buff, 0, contentLen);
                    contentLen = fs.Read(buff, 0, buffLength);
                }

                // Close the file stream and the Request Stream
                strm.Close();
                fs.Close();
            }
            catch (Exception)
            {
                
            }
        }

        public void Download(string filePath, string fileName)
        {
            FtpWebRequest reqFTP;
            try
            {
                //filePath = <<The full path where the file is to be created.>>, 
                //fileName = <<Name of the file to be created(Need not be the name of the file on FTP server).>>
                FileStream outputStream = new FileStream(filePath + "\\" + fileName, FileMode.Create);

                reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + _strFtpServer + "/" + fileName));
                reqFTP.Method = WebRequestMethods.Ftp.DownloadFile;
                reqFTP.UseBinary = true;
                reqFTP.Credentials = new NetworkCredential(_strFtpUser, _strFtpPassword);
                FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
                Stream ftpStream = response.GetResponseStream();
                long cl = response.ContentLength;
                int bufferSize = 2048;
                int readCount;
                byte[] buffer = new byte[bufferSize];

                readCount = ftpStream.Read(buffer, 0, bufferSize);
                while (readCount > 0)
                {
                    outputStream.Write(buffer, 0, readCount);
                    readCount = ftpStream.Read(buffer, 0, bufferSize);
                }

                ftpStream.Close();
                outputStream.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void GetFileList()
        {
            FtpWebRequest reqFTP;

            //filePath = <<The full path where the file is to be created.>>, 
            //fileName = <<Name of the file to be created(Need not be the name of the file on FTP server).>>

            reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + _strFtpServer));
            reqFTP.Method = WebRequestMethods.Ftp.ListDirectory;
            reqFTP.UsePassive = true;
            reqFTP.UseBinary = true;
            reqFTP.KeepAlive = true;
            reqFTP.Credentials = new NetworkCredential(_strFtpUser, _strFtpPassword);


        }

        
        public void writeConfig(string file)
        {
            DataTable dtConfigFile = new DataTable("ConfigFileUpload");
            FileInfo f=new FileInfo(file);
            if (!f.Exists)
            {
                return;
            } 
            f._AddToDataTable(dtConfigFile);
            dtConfigFile.WriteXml("ConfigFileUpload.xml", XmlWriteMode.WriteSchema);             
        }

        // check ftp status
        public bool checkFtpConnection()
        {
            var uri = new Uri(string.Format("ftp://{0}", _strFtpServer));
            try
            {
                FtpWebRequest reqFtp = (FtpWebRequest)WebRequest.Create(uri);
                reqFtp.Credentials = new NetworkCredential(_strFtpUser, _strFtpPassword);
                reqFtp.Method = Ftp.PrintWorkingDirectory;
                reqFtp.UsePassive = true;
                reqFtp.GetResponse();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        // upload multi files into FTP server
        public bool UploadFilesToFtp(string _ftpDir, DataTable files)
        {
            FtpWebRequest reqFTP;
            FileInfo fileInfo;
            FileStream fileStream;
            Stream uploadStream;
            int bufferLength = 2048;
            int contentLength;
            bool uploadSuccess = false;
            try
            {
                foreach (DataRow file in files.Rows)
                {
                    var uri = new Uri(string.Format("ftp://{0}/{1}/{2}", _strFtpServer, _ftpDir, Path.GetFileName(file["ATT_NAME"].ToString())));
                    reqFTP = (FtpWebRequest)WebRequest.Create(uri);
                    reqFTP.Credentials = new NetworkCredential(_strFtpUser, _strFtpPassword);
                    reqFTP.Method = Ftp.UploadFile;

                    // set up request properties
                    reqFTP.KeepAlive = false;
                    reqFTP.UseBinary = true;
                    reqFTP.UsePassive = true;

                    fileInfo = new FileInfo(Path.GetFullPath(file["FILE_NAME"].ToString()));
                    fileStream = fileInfo.OpenRead();

                    byte[] buffer = new byte[bufferLength];

                    uploadStream = reqFTP.GetRequestStream();
                    contentLength = fileStream.Read(buffer, 0, bufferLength);

                    while (contentLength != 0)
                    {
                        uploadStream.Write(buffer, 0, contentLength);
                        contentLength = fileStream.Read(buffer, 0, bufferLength);
                    }

                    uploadStream.Close();
                    fileStream.Close();
                    reqFTP = null;
                    uploadSuccess = true;
                }
            }
            catch (Exception)
            {
            }
            return uploadSuccess;
        }

        public bool FtpDirectoryExists(string _strFtpDir)
        {
            FtpWebRequest reqFtp;
            FtpWebResponse resFtp;
            string[] dir = _strFtpDir.Split('/');
            string uri = "";
            try
            {
                uri = string.Format("ftp://{0}", _strFtpServer);
                foreach (var subDir in dir)
                {
                    uri += "/" + subDir;
                    reqFtp = (FtpWebRequest)WebRequest.Create(uri);
                    reqFtp.Credentials = new NetworkCredential(_strFtpUser, _strFtpPassword);
                    reqFtp.Method = Ftp.PrintWorkingDirectory;
                    reqFtp.UsePassive = true;
                    resFtp = (FtpWebResponse)reqFtp.GetResponse();
                }
            }
            catch (Exception)
            {
                reqFtp = (FtpWebRequest)WebRequest.Create(uri);
                reqFtp.Credentials = new NetworkCredential(_strFtpUser, _strFtpPassword);
                reqFtp.Method = Ftp.MakeDirectory;

                reqFtp.UsePassive = true;
                reqFtp.UseBinary = true;
                resFtp = (FtpWebResponse)reqFtp.GetResponse();
            }
            return true;
        }

        public bool createFtpDirectory(string _strFtpDir)
        {
            bool complete = false;
            FtpWebRequest reqFtp;
            FtpWebResponse resFtp;
            string[] dir = _strFtpDir.Split('/');
            try
            {
                var uri = "ftp://" + _strFtpServer;
                foreach (var subDir in dir)
                {
                    uri += "/" + subDir;
                    reqFtp = (FtpWebRequest)WebRequest.Create(uri);
                    reqFtp.Credentials = new NetworkCredential(_strFtpUser, _strFtpPassword);
                    reqFtp.Method = Ftp.MakeDirectory;

                    reqFtp.UsePassive = true;
                    reqFtp.UseBinary = true;
                    resFtp = (FtpWebResponse)reqFtp.GetResponse();
                }
                complete = true;
            }
            catch (Exception ex)
            {
                complete = false;
            }
            return complete;
        }

        public bool DeleteFtpFile(string _strFtpFile)
        {
            FtpWebRequest reqFtp;
            try
            {
                string uri = string.Format("ftp://{0}/{1}", _strFtpServer, _strFtpFile);
                reqFtp = (FtpWebRequest)WebRequest.Create(uri);

                reqFtp.Credentials = new NetworkCredential(_strFtpUser, _strFtpPassword);
                reqFtp.Method = Ftp.DeleteFile;
                reqFtp.UsePassive = true;
                reqFtp.GetResponse();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
