﻿using DevExpress.Utils;
using Dynamic.Helpers;
using EPower.Base.Helper.DevExpressCustomize;
using EPower.Base.Logic;
using EPower.Base.Models;
using EPower.Base.Properties;
using HB01.Helpers;
using SoftTech.Component;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace EPower.Accounting.Interface
{
    public partial class JournalTransactionPage : HPage
    {
        public JournalTransactionPage()
        {
            InitializeComponent();
            ApplySetting();
            this.Load += journalTransactionPage_Load;
        }

        #region Method
        public void ApplySetting()
        {
            SoftTech.Helper.ResourceHelper.ApplyResource(this.tgv);
            this.AutoScaleMode = AutoScaleMode.Dpi;
            // setdefaultColumnSize();
            //tgv.SetBestFitColumns();
            //Print Permission  
            rdpDate.Value = ReportDatePicker.DateRanges.ThisFiscalYear;
            rdpDate.DateFilterTypes = ReportDatePicker.DateFilterType.Period;
            menuView.Cursor = Cursors.Hand;

            // Set Format Currency 
            SetFormatCurrency();
            tgv.OptionsView.ShowIndentAsRowStyle = false;
            tgv.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            tgv.OptionsView.ShowVertLines = false;
            tgv.OptionsView.ShowHorzLines = false;
            tgv.SetDefaultTreeList();
            //tgv.CustomDrawColumnHeader += Tgv_CustomDrawColumnHeader;

            //var currency = CurrencyLogic.Instance.Find(Current.CurrencyId);
            ApplyFormat("#,###.####");
            //_currencyFormat = currency.Format;

            searchControl.Properties.NullValuePrompt = Resources.SEARCH;
            tgv.OptionsPrint.PrintCheckBoxes = false;
            tgv.OptionsPrint.PrintTree = false;
            tgv.OptionsPrint.PrintTreeButtons = false;
            tgv.AppearancePrint.GroupFooter.Font = tgv.Appearance.GroupFooter.Font;
            tgv.AppearancePrint.HeaderPanel.Font = tgv.Appearance.HeaderPanel.Font;
            tgv.AppearancePrint.Row.Font = tgv.Appearance.Row.Font;
            tgv.BestFitColumns();

            //When user customize date time.
            rdpDate.DateFilterTypes = ReportDatePicker.DateFilterType.Period;
        }

        public void ApplyFormat(string currencyFormat)
        {
            colPOSTED_AMOUNT.Format.FormatType = FormatType.Numeric;
            colPOSTED_AMOUNT.Format.FormatString = currencyFormat;

            colAMOUNT.Format.FormatType = FormatType.Numeric;
            colAMOUNT.Format.FormatString = currencyFormat;

            colTRANSACTION_DATE.Format.FormatType = FormatType.DateTime;
            colTRANSACTION_DATE.Format.FormatString = "dd-MM-yyyy";
            colPOSTED_DATE.Format.FormatType = FormatType.DateTime;
            colPOSTED_DATE.Format.FormatString = "dd-MM-yyyy";
        }

        public void SetFormatCurrency()
        {
            //var currency = CurrencyLogic.Instance.Find(Current.Company.CurrencyId);
            //_currencyFormat = FormatDataHelper.CurrencyFormat = currency.Format;
        }

        public override void Reload()
        {
            if (this.IsLoading) return;
            bind();
        }

        private void bind()
        {
            try
            {
                var results = new List<JournalHistoryList>();
                HB01.Helpers.HProgress.RunDbTran(() =>
                {
                    results = JournalTranLogic.Instance.GetJournalHistoryLists(rdpDate.FromDate, rdpDate.ToDate);
                });
                tgv.DataSource = results;
                tgv.CollapseToLevel(1);
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp);
            }
        }

        #endregion

        #region Events
        private void rdpDate_ValueChanged(object sender, EventArgs e)
        {
            Reload();
        }

        private void btnPush_Click(object sender, EventArgs e)
        {
            if (PostingLogic.Instance.isPosting)
            {
                return;
            }

            if(tgv.AllNodesCount == 0)
            {
                MsgBox.ShowInformation(Resources.MSG_NO_DATA_TO_SEND);
                return;
            }

            var message = MsgBox.ShowQuestion(string.Format(Resources.MSG_DO_YOU_WANT_PUSH_DATA_TO_ACCOUTING, rdpDate.FromDate.ToShortDate(), rdpDate.ToDate.ToShortDate()), Resources.WARNING);
            if (message == DialogResult.Yes)
            {
                Runner.RunNewThread(delegate ()
                {
                    PostingLogic.Instance.PostToPointer(rdpDate.FromDate, rdpDate.ToDate);
                });
                if(Runner.Instance.Error != null)
                {
                    PostingLogic.Instance.isPosting = false;
                }
                Reload();
                if (JournalTranLogic.Instance.totalSucess + JournalTranLogic.Instance.totalfail != 0)
                {
                    MsgBox.ShowInformation(string.Format(Resources.MSG_POSTING_COMPLETE, JournalTranLogic.Instance.totalSucess, JournalTranLogic.Instance.totalfail), Resources.INFORMATION);
                }
                //else
                //{
                //    MsgBox.ShowInformation(Resources.MSG_NO_DATA_TO_SEND);
                //}
            }
        }

        private void journalTransactionPage_Load(object sender, EventArgs e)
        {
            tgv.CollapseToLevel(1);
        }

        #endregion
    }
}
