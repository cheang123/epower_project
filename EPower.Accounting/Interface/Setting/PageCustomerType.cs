﻿using EPower.Base.Helper;
using EPower.Base.Logic;
using EPower.Base.Properties;
using HB01.Domain.Enums;
using HB01.Domain.ListModels;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Accounting.Interface.Setting
{
    public partial class PageCustomerType : Form
    {
        List<Base.Logic.Currency> currencies = new List<Base.Logic.Currency>();
        public PageCustomerType()
        {
            InitializeComponent();
            Bind();
            searchControl.Properties.NullValuePrompt = Resources.SEARCH;
            this.dgvConnectionType.BestFitColumns();
            currencies = SettingLogic.Currencies;
            this.dgvConnectionType.Columns[colAR_KHR.FieldName].Visible = currencies.Any(x => x.CURRENCY_ID == (int)Currency.KHR);
            this.dgvConnectionType.Columns[colAR_USD.FieldName].Visible = currencies.Any(x => x.CURRENCY_ID == (int)Currency.USD);
            this.dgvConnectionType.Columns[colAR_THB.FieldName].Visible = currencies.Any(x => x.CURRENCY_ID == (int)Currency.THB);
            this.dgvConnectionType.Columns[colAR_VND.FieldName].Visible = currencies.Any(x => x.CURRENCY_ID == (int)Currency.VND);
        }
        #region Operation

        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        #endregion

        #region Method

        private void Bind()
        {
            try
            {
                var icAcc = AccountChartHelper.GetAccounts(new AccountSearchParam { Natures = new List<AccountNatures> { AccountNatures.Income } });
                var arAcc = AccountChartHelper.GetAccounts(new AccountSearchParam { Natures = new List<AccountNatures> { AccountNatures.Asset } });
                repIcAccount.DataSource = icAcc;
                repArAccount.DataSource = arAcc;
                repArKhr.DataSource = AccountChartHelper.GetAccounts(new AccountSearchParam { Natures = new List<AccountNatures> { AccountNatures.Asset }, CurrencyId = (int)Currency.KHR });
                repArUsd.DataSource = AccountChartHelper.GetAccounts(new AccountSearchParam { Natures = new List<AccountNatures> { AccountNatures.Asset }, CurrencyId = (int)Currency.USD });
                repArThb.DataSource = AccountChartHelper.GetAccounts(new AccountSearchParam { Natures = new List<AccountNatures> { AccountNatures.Asset }, CurrencyId = (int)Currency.THB });
                reqArVnd.DataSource = AccountChartHelper.GetAccounts(new AccountSearchParam { Natures = new List<AccountNatures> { AccountNatures.Asset }, CurrencyId = (int)Currency.VND });
                var q = SettingLogic.ConnectionTypes;
                dgv.DataSource = q.ToList();
            }
            catch (Exception e)
            {
                MsgBox.ShowError(e);
            }
        }

        #endregion

    }
}
