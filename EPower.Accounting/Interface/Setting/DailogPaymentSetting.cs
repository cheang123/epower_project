﻿using EPower.Base.Logic;
using HB01.Domain.Enums;
using HB01.Domain.ListModels;
using HB01.Logics;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Accounting.Interface.Setting
{
    public partial class DialogPaymentSetting : ExDialog
    {
        List<TBL_PAYMENT_CONFIG> objPaymentConfig = new List<TBL_PAYMENT_CONFIG>();
        List<TBL_PAYMENT_CONFIG> objOldPaymentConfig = new List<TBL_PAYMENT_CONFIG>();

        public DialogPaymentSetting(string paymentMethod = "")
        {
            InitializeComponent();
            this.colACCOUNT_KHR.Visible = DBDataContext.Db.TLKP_CURRENCies.Any(x => x.CURRENCY_ID == (int)Currency.KHR);
            this.colACCOUNT_USD.Visible = DBDataContext.Db.TLKP_CURRENCies.Any(x => x.CURRENCY_ID == (int)Currency.USD);
            this.colACCOUNT_THB.Visible = DBDataContext.Db.TLKP_CURRENCies.Any(x => x.CURRENCY_ID == (int)Currency.THB);
            this.colACCOUNT_VND.Visible = DBDataContext.Db.TLKP_CURRENCies.Any(x => x.CURRENCY_ID == (int)Currency.VND);
            bind();
            searchControl.Properties.NullValuePrompt = EPower.Base.Properties.Resources.SEARCH;
            searchControl.Text = paymentMethod;  
        }
        #region Operation

        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void dgvPaymentSetting_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            try
            {
                foreach (var newObj in objPaymentConfig)
                {
                    var oldObj = objOldPaymentConfig.FirstOrDefault(x => x.CONFIG_ID == newObj.CONFIG_ID);
                    if (oldObj != newObj)
                    {
                        DBDataContext.Db.Update(oldObj, newObj);
                    }
                }
                DBDataContext.Db.SubmitChanges();
            }
            catch
            {

            }
        }
        #endregion

        #region Method
        private void bind()
        {
            try
            {
                var accounts = DBDataContext.Db.TBL_ACCOUNT_CHARTs.ToList(); //AccountLogic.Instance.List<AccountListModel>(new AccountSearchParam() { Natures = new List<AccountNatures> { AccountNatures.Asset }, ShowInAccountPage = false });

                repAccount.DataSource = accounts;
                //accUSD.DataSource = accounts;
                //accTHB.DataSource = accounts;
                //accVND.DataSource = accounts; 
                var row = DBDataContext.Db.TBL_PAYMENT_CONFIGs.Where(x => x.IS_ACTIVE).ToList();
                dgv.DataSource = row;
                dgv.DataSource = SettingLogic.PaymentConfig;
            }
            catch (Exception e)
            {
                MsgBox.ShowError(e);
            }
        }
        #endregion

        private void btnCLOSE_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            SettingLogic.CommitPaymentConfig();
            this.DialogResult = DialogResult.OK;
        }
    }
}
