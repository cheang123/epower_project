﻿using EPower.Base.Logic;
using HB01.Domain.Models.Settings;
using HB01.Helpers;
using HB01.Interfaces;
using HB01.Logics;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace EPower.Accounting.Interface.Setting
{
    public partial class DialogSetting : ExDialog
    {
        public DialogSetting()
        {
            InitializeComponent();
            btnPRODUCTION_SETTING_Click(btnSETTING, EventArgs.Empty);
            Font tooltipFont = new Font("Khmer OS System,", 15.0f);
            new ToolTip().SetToolTip(btnCOMPANY, Base.Properties.Resources.COMPANY_INFO);
            new ToolTip().SetToolTip(btnCOMPANY_SETTING, Base.Properties.Resources.SETTING + Base.Properties.Resources.COMPANY_INFO);
            new ToolTip().SetToolTip(btnLogout, Base.Properties.Resources.LOGIN);

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void btnPRODUCTION_SETTING_Click(object sender, EventArgs e)
        {
            showPage(typeof(PageGeneralSetting), sender);
        }

        private void btnCURRENCY_Click(object sender, EventArgs e)
        {
            showPage(typeof(PageCurrency), sender);
        }

        private void btnCUSTOMER_TYPE_Click(object sender, EventArgs e)
        {
            showPage(typeof(PageCustomerType), sender);
        }

        private void btnINVOICE_ITEM_Click(object sender, EventArgs e)
        {
            showPage(typeof(PageInvoiceItem), sender);
        }

        private void btnPAYMENT_CONFIG_Click(object sender, EventArgs e)
        {
            showPage(typeof(DialogPaymentSetting), sender);
        }

        #region ShowPage
        private Dictionary<Type, Control> pages = new Dictionary<Type, Control>();
        private Control currentPage = null;
        private void showPage(Type type, object sender)
        {
            try
            {
                if (!this.pages.ContainsKey(type))
                {
                    this.pages[type] = (Control)Activator.CreateInstance(type);
                    this.pages[type].Size = this.main.Size;
                    if (this.pages[type] is Form)
                    {
                        Form frm = (Form)this.pages[type];
                        frm.TopLevel = false;
                        frm.Visible = true;
                        frm.FormBorderStyle = FormBorderStyle.None;
                    }
                    this.main.Controls.Add(this.pages[type]);
                    this.pages[type].Dock = DockStyle.Fill;
                    ResourceHelper.ApplyResource(this.pages[type]);
                }
                if (this.currentPage != this.pages[type])
                {
                    this.pages[type].Show();
                    if (this.currentPage != null)
                    {
                        this.currentPage.Hide();
                    }
                    this.currentPage = this.pages[type];
                }
                this.lblSETTING.Text = ((Control)sender).Text;
            }
            catch(Exception e)
            {
                throw e.InnerException;
            }
        }
        #endregion

        private void btnCOMPANY_Click(object sender, EventArgs e)
        {
            var company = new Company();
            Runner.RunNewThread(() =>
            //HProgress.RunNewThread(() =>
            {
                try
                {
                    company = CompanyLogic.Instance.Find(Current.CompanyId);

                }
                catch (Exception exp)
                {
                    HMsgBox.ShowWarning(exp.Message);
                }
            });
            var dig = new CompanySignupDialog(HB01.GeneralProcess.Edit, company);
            dig.IsLoading = false;
            dig.ShowDialog();
        }

        private void btnCOMPANY_SETTING_Click(object sender, EventArgs e)
        {
            var dig = new CompanySettingDialog(HB01.GeneralProcess.Edit, Current.Company);
            dig.IsLoading = false;
            dig.ShowDialog();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            var diag = new LoginDialog();
            diag.IsLoading = false;
            diag.ShowDialog();
        }

        private void btnSAVE_Click(object sender, EventArgs e)
        {
            Runner.RunNewThread(() =>
            {
                SettingLogic.CommitConnectionType();
                SettingLogic.CommitCurrencies();
                SettingLogic.CommitInvoiceItems();
                SettingLogic.CommitAccountConfig();
                SettingLogic.CommitJournalConfig();
            });            
            DialogResult = DialogResult.Yes;
        }

        private void DialogSetting_FormClosing(object sender, FormClosingEventArgs e)
        {
            SettingLogic.RenewSetting();
        }
    }
}