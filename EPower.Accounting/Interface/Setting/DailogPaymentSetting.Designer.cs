﻿namespace EPower.Accounting.Interface.Setting
{
    partial class DialogPaymentSetting
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogPaymentSetting));
            this.tBLPAYMENTCONFIGBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.dgv = new DevExpress.XtraGrid.GridControl();
            this.dgvPaymentSetting = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCONFIG_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAYMENT_TYPE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colACCOUNT_KHR = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repAccount = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colACCOUNT_USD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colACCOUNT_THB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colACCOUNT_VND = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCUT_OFF_DATE_PAYMENT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTimeEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.colIS_ACTIVE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repTotal = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.repMemo = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repQuantity = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.repTax = new DevExpress.XtraEditors.Repository.RepositoryItemTokenEdit();
            this.repRemove = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repTaxAmount = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.panel3 = new System.Windows.Forms.Panel();
            this.searchControl = new DevExpress.XtraEditors.SearchControl();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tBLPAYMENTCONFIGBindingSource)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPaymentSetting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repMemo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repRemove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTaxAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchControl.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.dgv);
            this.content.Controls.Add(this.panel3);
            this.content.Controls.Add(this.panel1);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.panel3, 0);
            this.content.Controls.SetChildIndex(this.dgv, 0);
            // 
            // tBLPAYMENTCONFIGBindingSource
            // 
            this.tBLPAYMENTCONFIGBindingSource.DataSource = typeof(SoftTech.TBL_PAYMENT_CONFIG);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "CUSTOMER_CONNECTION_TYPE_ID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn1, "dataGridViewTextBoxColumn1");
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "CUSTOMER_CONNECTION_TYPE_NAME";
            resources.ApplyResources(this.dataGridViewTextBoxColumn2, "dataGridViewTextBoxColumn2");
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.DataPropertyName = "IS_ACTIVE";
            resources.ApplyResources(this.dataGridViewCheckBoxColumn1, "dataGridViewCheckBoxColumn1");
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "INCOME_ACCOUNT_NAME";
            resources.ApplyResources(this.dataGridViewTextBoxColumn3, "dataGridViewTextBoxColumn3");
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "ACCOUNT_RECEIVABLE_NAME";
            resources.ApplyResources(this.dataGridViewTextBoxColumn4, "dataGridViewTextBoxColumn4");
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnCLOSE);
            this.panel1.Controls.Add(this.btnOK);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnCLOSE_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // dgv
            // 
            this.dgv.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.EmbeddedNavigator.Margin = ((System.Windows.Forms.Padding)(resources.GetObject("dgv.EmbeddedNavigator.Margin")));
            this.dgv.MainView = this.dgvPaymentSetting;
            this.dgv.Name = "dgv";
            this.dgv.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repTotal,
            this.repMemo,
            this.repAccount,
            this.repQuantity,
            this.repTax,
            this.repRemove,
            this.repTaxAmount,
            this.repositoryItemDateEdit1,
            this.repositoryItemTimeEdit1});
            this.dgv.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.dgvPaymentSetting});
            // 
            // dgvPaymentSetting
            // 
            this.dgvPaymentSetting.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCONFIG_ID,
            this.colPAYMENT_TYPE,
            this.colACCOUNT_KHR,
            this.colACCOUNT_USD,
            this.colACCOUNT_THB,
            this.colACCOUNT_VND,
            this.colCUT_OFF_DATE_PAYMENT,
            this.colIS_ACTIVE});
            this.dgvPaymentSetting.DetailHeight = 228;
            this.dgvPaymentSetting.GridControl = this.dgv;
            this.dgvPaymentSetting.Name = "dgvPaymentSetting";
            this.dgvPaymentSetting.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.dgvPaymentSetting.OptionsDetail.EnableMasterViewMode = false;
            this.dgvPaymentSetting.OptionsView.AnimationType = DevExpress.XtraGrid.Views.Base.GridAnimationType.AnimateAllContent;
            this.dgvPaymentSetting.OptionsView.ShowGroupPanel = false;
            this.dgvPaymentSetting.OptionsView.ShowIndicator = false;
            this.dgvPaymentSetting.OptionsView.WaitAnimationOptions = DevExpress.XtraEditors.WaitAnimationOptions.Panel;
            // 
            // colCONFIG_ID
            // 
            resources.ApplyResources(this.colCONFIG_ID, "colCONFIG_ID");
            this.colCONFIG_ID.FieldName = "CONFIG_ID";
            this.colCONFIG_ID.MinWidth = 15;
            this.colCONFIG_ID.Name = "colCONFIG_ID";
            // 
            // colPAYMENT_TYPE
            // 
            resources.ApplyResources(this.colPAYMENT_TYPE, "colPAYMENT_TYPE");
            this.colPAYMENT_TYPE.FieldName = "PAYMENT_TYPE";
            this.colPAYMENT_TYPE.MinWidth = 15;
            this.colPAYMENT_TYPE.Name = "colPAYMENT_TYPE";
            this.colPAYMENT_TYPE.OptionsColumn.ReadOnly = true;
            // 
            // colACCOUNT_KHR
            // 
            resources.ApplyResources(this.colACCOUNT_KHR, "colACCOUNT_KHR");
            this.colACCOUNT_KHR.ColumnEdit = this.repAccount;
            this.colACCOUNT_KHR.FieldName = "ACCOUNT_ID_KHR";
            this.colACCOUNT_KHR.ImageOptions.Alignment = ((System.Drawing.StringAlignment)(resources.GetObject("colACCOUNT_KHR.ImageOptions.Alignment")));
            this.colACCOUNT_KHR.MinWidth = 15;
            this.colACCOUNT_KHR.Name = "colACCOUNT_KHR";
            // 
            // repAccount
            // 
            resources.ApplyResources(this.repAccount, "repAccount");
            this.repAccount.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("repAccount.Buttons"))))});
            this.repAccount.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("repAccount.Columns"), resources.GetString("repAccount.Columns1"), ((int)(resources.GetObject("repAccount.Columns2"))), ((DevExpress.Utils.FormatType)(resources.GetObject("repAccount.Columns3"))), resources.GetString("repAccount.Columns4"), ((bool)(resources.GetObject("repAccount.Columns5"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("repAccount.Columns6"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("repAccount.Columns7"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("repAccount.Columns8")))),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("repAccount.Columns9"), resources.GetString("repAccount.Columns10"), ((int)(resources.GetObject("repAccount.Columns11"))), ((DevExpress.Utils.FormatType)(resources.GetObject("repAccount.Columns12"))), resources.GetString("repAccount.Columns13"), ((bool)(resources.GetObject("repAccount.Columns14"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("repAccount.Columns15"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("repAccount.Columns16"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("repAccount.Columns17"))))});
            this.repAccount.DisplayMember = "ACCOUNT_NAME";
            this.repAccount.Name = "repAccount";
            this.repAccount.PopupWidth = 375;
            this.repAccount.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.repAccount.ValueMember = "ACCOUNT_ID";
            // 
            // colACCOUNT_USD
            // 
            resources.ApplyResources(this.colACCOUNT_USD, "colACCOUNT_USD");
            this.colACCOUNT_USD.ColumnEdit = this.repAccount;
            this.colACCOUNT_USD.FieldName = "ACCOUNT_ID_USD";
            this.colACCOUNT_USD.ImageOptions.Alignment = ((System.Drawing.StringAlignment)(resources.GetObject("colACCOUNT_USD.ImageOptions.Alignment")));
            this.colACCOUNT_USD.MinWidth = 15;
            this.colACCOUNT_USD.Name = "colACCOUNT_USD";
            // 
            // colACCOUNT_THB
            // 
            resources.ApplyResources(this.colACCOUNT_THB, "colACCOUNT_THB");
            this.colACCOUNT_THB.ColumnEdit = this.repAccount;
            this.colACCOUNT_THB.FieldName = "ACCOUNT_ID_THB";
            this.colACCOUNT_THB.ImageOptions.Alignment = ((System.Drawing.StringAlignment)(resources.GetObject("colACCOUNT_THB.ImageOptions.Alignment")));
            this.colACCOUNT_THB.MinWidth = 15;
            this.colACCOUNT_THB.Name = "colACCOUNT_THB";
            // 
            // colACCOUNT_VND
            // 
            resources.ApplyResources(this.colACCOUNT_VND, "colACCOUNT_VND");
            this.colACCOUNT_VND.ColumnEdit = this.repAccount;
            this.colACCOUNT_VND.FieldName = "ACCOUNT_ID_VND";
            this.colACCOUNT_VND.MinWidth = 15;
            this.colACCOUNT_VND.Name = "colACCOUNT_VND";
            // 
            // colCUT_OFF_DATE_PAYMENT
            // 
            resources.ApplyResources(this.colCUT_OFF_DATE_PAYMENT, "colCUT_OFF_DATE_PAYMENT");
            this.colCUT_OFF_DATE_PAYMENT.ColumnEdit = this.repositoryItemTimeEdit1;
            this.colCUT_OFF_DATE_PAYMENT.FieldName = "CUT_OFF_DATE_PAYMENT";
            this.colCUT_OFF_DATE_PAYMENT.Name = "colCUT_OFF_DATE_PAYMENT";
            // 
            // repositoryItemTimeEdit1
            // 
            resources.ApplyResources(this.repositoryItemTimeEdit1, "repositoryItemTimeEdit1");
            this.repositoryItemTimeEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("repositoryItemTimeEdit1.Buttons"))))});
            this.repositoryItemTimeEdit1.Name = "repositoryItemTimeEdit1";
            // 
            // colIS_ACTIVE
            // 
            resources.ApplyResources(this.colIS_ACTIVE, "colIS_ACTIVE");
            this.colIS_ACTIVE.FieldName = "IS_ACTIVE";
            this.colIS_ACTIVE.MinWidth = 15;
            this.colIS_ACTIVE.Name = "colIS_ACTIVE";
            // 
            // repTotal
            // 
            resources.ApplyResources(this.repTotal, "repTotal");
            this.repTotal.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("repTotal.Buttons"))))});
            this.repTotal.Name = "repTotal";
            // 
            // repMemo
            // 
            resources.ApplyResources(this.repMemo, "repMemo");
            this.repMemo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("repMemo.Buttons"))))});
            this.repMemo.Name = "repMemo";
            this.repMemo.ShowIcon = false;
            // 
            // repQuantity
            // 
            resources.ApplyResources(this.repQuantity, "repQuantity");
            this.repQuantity.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("repQuantity.Buttons"))))});
            this.repQuantity.Name = "repQuantity";
            // 
            // repTax
            // 
            this.repTax.ClearCheckStatesOnLostFocus = DevExpress.Utils.DefaultBoolean.True;
            this.repTax.DropDownShowMode = DevExpress.XtraEditors.TokenEditDropDownShowMode.Regular;
            this.repTax.EditValueSeparatorChar = '\0';
            this.repTax.EditValueType = DevExpress.XtraEditors.TokenEditValueType.List;
            this.repTax.Name = "repTax";
            resources.ApplyResources(this.repTax, "repTax");
            this.repTax.Tag = "";
            // 
            // repRemove
            // 
            resources.ApplyResources(this.repRemove, "repRemove");
            this.repRemove.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom;
            this.repRemove.ImageOptions.ImageChecked = ((System.Drawing.Image)(resources.GetObject("repRemove.ImageOptions.ImageChecked")));
            this.repRemove.ImageOptions.ImageGrayed = ((System.Drawing.Image)(resources.GetObject("repRemove.ImageOptions.ImageGrayed")));
            this.repRemove.ImageOptions.ImageUnchecked = ((System.Drawing.Image)(resources.GetObject("repRemove.ImageOptions.ImageUnchecked")));
            this.repRemove.Name = "repRemove";
            // 
            // repTaxAmount
            // 
            resources.ApplyResources(this.repTaxAmount, "repTaxAmount");
            this.repTaxAmount.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("repTaxAmount.Buttons"))))});
            this.repTaxAmount.Name = "repTaxAmount";
            // 
            // repositoryItemDateEdit1
            // 
            resources.ApplyResources(this.repositoryItemDateEdit1, "repositoryItemDateEdit1");
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("repositoryItemDateEdit1.Buttons"))))});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("repositoryItemDateEdit1.CalendarTimeProperties.Buttons"))))});
            this.repositoryItemDateEdit1.DisplayFormat.FormatString = "t";
            this.repositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.EditFormat.FormatString = "t";
            this.repositoryItemDateEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.Controls.Add(this.searchControl);
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            // 
            // searchControl
            // 
            this.searchControl.Client = this.dgv;
            resources.ApplyResources(this.searchControl, "searchControl");
            this.searchControl.Name = "searchControl";
            this.searchControl.Properties.Appearance.Options.UseFont = true;
            this.searchControl.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Repository.ClearButton(),
            new DevExpress.XtraEditors.Repository.SearchButton()});
            this.searchControl.Properties.Client = this.dgv;
            this.searchControl.Properties.DropDownRows = 10;
            this.searchControl.Properties.Padding = new System.Windows.Forms.Padding(1);
            // 
            // DialogPaymentSetting
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Name = "DialogPaymentSetting";
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tBLPAYMENTCONFIGBindingSource)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPaymentSetting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repMemo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repRemove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTaxAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.searchControl.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.BindingSource tBLPAYMENTCONFIGBindingSource;
        private System.Windows.Forms.Panel panel1;
        private SoftTech.Component.ExButton btnCLOSE;
        private SoftTech.Component.ExButton btnOK;
        private DevExpress.XtraGrid.GridControl dgv;
        private DevExpress.XtraGrid.Views.Grid.GridView dgvPaymentSetting;
        private DevExpress.XtraGrid.Columns.GridColumn colCONFIG_ID;
        private DevExpress.XtraGrid.Columns.GridColumn colPAYMENT_TYPE;
        private DevExpress.XtraGrid.Columns.GridColumn colACCOUNT_KHR;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repAccount;
        private DevExpress.XtraGrid.Columns.GridColumn colACCOUNT_USD;
        private DevExpress.XtraGrid.Columns.GridColumn colACCOUNT_THB;
        private DevExpress.XtraGrid.Columns.GridColumn colACCOUNT_VND;
        private DevExpress.XtraGrid.Columns.GridColumn colCUT_OFF_DATE_PAYMENT;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colIS_ACTIVE;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repTotal;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repMemo;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repQuantity;
        private DevExpress.XtraEditors.Repository.RepositoryItemTokenEdit repTax;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repRemove;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repTaxAmount;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private System.Windows.Forms.Panel panel3;
        private DevExpress.XtraEditors.SearchControl searchControl;
    }
}
