﻿namespace EPower.Accounting.Interface.Setting
{
    partial class PageCustomerType
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageCustomerType));
            this.roundPanel1 = new Dynamic.Component.RoundPanel();
            this.dgv = new DevExpress.XtraGrid.GridControl();
            this.tLKPCUSTOMERCONNECTIONTYPEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dgvConnectionType = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCUSTOMER_CONNECTION_TYPE_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCUSTOMER_CONNECTION_TYPE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDESCRIPTION = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCURRENCY_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTARIFF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNONLICENSE_CUSTOMER_GROUP_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPRICE_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colINCOME_ACCOUNT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repIcAccount = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colAR_KHR = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repArAccount = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colAR_USD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAR_THB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAR_VND = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repArKhr = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.panel1 = new System.Windows.Forms.Panel();
            this.searchControl = new DevExpress.XtraEditors.SearchControl();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.repArUsd = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repArThb = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.reqArVnd = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.roundPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tLKPCUSTOMERCONNECTIONTYPEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConnectionType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repIcAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repArAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repArKhr)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchControl.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repArUsd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repArThb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reqArVnd)).BeginInit();
            this.SuspendLayout();
            // 
            // roundPanel1
            // 
            this.roundPanel1._BackColor = System.Drawing.Color.White;
            this.roundPanel1._Radius = 10;
            this.roundPanel1.BackColor = System.Drawing.Color.Transparent;
            this.roundPanel1.Controls.Add(this.dgv);
            this.roundPanel1.Controls.Add(this.panel1);
            resources.ApplyResources(this.roundPanel1, "roundPanel1");
            this.roundPanel1.Name = "roundPanel1";
            // 
            // dgv
            // 
            this.dgv.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dgv.DataSource = this.tLKPCUSTOMERCONNECTIONTYPEBindingSource;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.MainView = this.dgvConnectionType;
            this.dgv.Name = "dgv";
            this.dgv.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repArAccount,
            this.repIcAccount,
            this.repArKhr,
            this.repArUsd,
            this.repArThb,
            this.reqArVnd});
            this.dgv.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.dgvConnectionType});
            this.dgv.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // tLKPCUSTOMERCONNECTIONTYPEBindingSource
            // 
            this.tLKPCUSTOMERCONNECTIONTYPEBindingSource.DataSource = typeof(SoftTech.TLKP_CUSTOMER_CONNECTION_TYPE);
            // 
            // dgvConnectionType
            // 
            this.dgvConnectionType.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCUSTOMER_CONNECTION_TYPE_ID,
            this.colCUSTOMER_CONNECTION_TYPE,
            this.colDESCRIPTION,
            this.colCURRENCY_ID,
            this.colTARIFF,
            this.colNONLICENSE_CUSTOMER_GROUP_ID,
            this.colPRICE_ID,
            this.colINCOME_ACCOUNT,
            this.colAR_KHR,
            this.colAR_USD,
            this.colAR_THB,
            this.colAR_VND});
            this.dgvConnectionType.DetailHeight = 300;
            this.dgvConnectionType.GridControl = this.dgv;
            this.dgvConnectionType.Name = "dgvConnectionType";
            this.dgvConnectionType.OptionsDetail.EnableMasterViewMode = false;
            this.dgvConnectionType.OptionsView.AnimationType = DevExpress.XtraGrid.Views.Base.GridAnimationType.AnimateAllContent;
            this.dgvConnectionType.OptionsView.ShowGroupPanel = false;
            this.dgvConnectionType.OptionsView.ShowIndicator = false;
            this.dgvConnectionType.OptionsView.WaitAnimationOptions = DevExpress.XtraEditors.WaitAnimationOptions.Panel;
            // 
            // colCUSTOMER_CONNECTION_TYPE_ID
            // 
            resources.ApplyResources(this.colCUSTOMER_CONNECTION_TYPE_ID, "colCUSTOMER_CONNECTION_TYPE_ID");
            this.colCUSTOMER_CONNECTION_TYPE_ID.FieldName = "CUSTOMER_CONNECTION_TYPE_ID";
            this.colCUSTOMER_CONNECTION_TYPE_ID.Name = "colCUSTOMER_CONNECTION_TYPE_ID";
            // 
            // colCUSTOMER_CONNECTION_TYPE
            // 
            resources.ApplyResources(this.colCUSTOMER_CONNECTION_TYPE, "colCUSTOMER_CONNECTION_TYPE");
            this.colCUSTOMER_CONNECTION_TYPE.FieldName = "CUSTOMER_CONNECTION_TYPE_NAME";
            this.colCUSTOMER_CONNECTION_TYPE.Name = "colCUSTOMER_CONNECTION_TYPE";
            this.colCUSTOMER_CONNECTION_TYPE.OptionsColumn.ReadOnly = true;
            // 
            // colDESCRIPTION
            // 
            resources.ApplyResources(this.colDESCRIPTION, "colDESCRIPTION");
            this.colDESCRIPTION.FieldName = "DESCRIPTION";
            this.colDESCRIPTION.ImageOptions.Alignment = ((System.Drawing.StringAlignment)(resources.GetObject("colDESCRIPTION.ImageOptions.Alignment")));
            this.colDESCRIPTION.Name = "colDESCRIPTION";
            // 
            // colCURRENCY_ID
            // 
            resources.ApplyResources(this.colCURRENCY_ID, "colCURRENCY_ID");
            this.colCURRENCY_ID.FieldName = "CURRENCY_ID";
            this.colCURRENCY_ID.ImageOptions.Alignment = ((System.Drawing.StringAlignment)(resources.GetObject("colCURRENCY_ID.ImageOptions.Alignment")));
            this.colCURRENCY_ID.Name = "colCURRENCY_ID";
            // 
            // colTARIFF
            // 
            resources.ApplyResources(this.colTARIFF, "colTARIFF");
            this.colTARIFF.FieldName = "TARIFF";
            this.colTARIFF.ImageOptions.Alignment = ((System.Drawing.StringAlignment)(resources.GetObject("colTARIFF.ImageOptions.Alignment")));
            this.colTARIFF.Name = "colTARIFF";
            // 
            // colNONLICENSE_CUSTOMER_GROUP_ID
            // 
            resources.ApplyResources(this.colNONLICENSE_CUSTOMER_GROUP_ID, "colNONLICENSE_CUSTOMER_GROUP_ID");
            this.colNONLICENSE_CUSTOMER_GROUP_ID.FieldName = "NONLICENSE_CUSTOMER_GROUP_ID";
            this.colNONLICENSE_CUSTOMER_GROUP_ID.ImageOptions.Alignment = ((System.Drawing.StringAlignment)(resources.GetObject("colNONLICENSE_CUSTOMER_GROUP_ID.ImageOptions.Alignment")));
            this.colNONLICENSE_CUSTOMER_GROUP_ID.Name = "colNONLICENSE_CUSTOMER_GROUP_ID";
            // 
            // colPRICE_ID
            // 
            resources.ApplyResources(this.colPRICE_ID, "colPRICE_ID");
            this.colPRICE_ID.FieldName = "PRICE_ID";
            this.colPRICE_ID.Name = "colPRICE_ID";
            // 
            // colINCOME_ACCOUNT
            // 
            resources.ApplyResources(this.colINCOME_ACCOUNT, "colINCOME_ACCOUNT");
            this.colINCOME_ACCOUNT.ColumnEdit = this.repIcAccount;
            this.colINCOME_ACCOUNT.FieldName = "INCOME_ACCOUNT";
            this.colINCOME_ACCOUNT.Name = "colINCOME_ACCOUNT";
            // 
            // repIcAccount
            // 
            resources.ApplyResources(this.repIcAccount, "repIcAccount");
            this.repIcAccount.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("repIcAccount.Buttons"))))});
            this.repIcAccount.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("repIcAccount.Columns"), resources.GetString("repIcAccount.Columns1")),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("repIcAccount.Columns2"), resources.GetString("repIcAccount.Columns3"))});
            this.repIcAccount.DisplayMember = "AccountName";
            this.repIcAccount.Name = "repIcAccount";
            this.repIcAccount.PopupWidth = 500;
            this.repIcAccount.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.repIcAccount.ValueMember = "Id";
            // 
            // colAR_KHR
            // 
            resources.ApplyResources(this.colAR_KHR, "colAR_KHR");
            this.colAR_KHR.ColumnEdit = this.repArKhr;
            this.colAR_KHR.FieldName = "AR_KHR";
            this.colAR_KHR.Name = "colAR_KHR";
            // 
            // repArAccount
            // 
            resources.ApplyResources(this.repArAccount, "repArAccount");
            this.repArAccount.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("repArAccount.Buttons"))))});
            this.repArAccount.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("repArAccount.Columns"), resources.GetString("repArAccount.Columns1"), ((int)(resources.GetObject("repArAccount.Columns2"))), ((DevExpress.Utils.FormatType)(resources.GetObject("repArAccount.Columns3"))), resources.GetString("repArAccount.Columns4"), ((bool)(resources.GetObject("repArAccount.Columns5"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("repArAccount.Columns6"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("repArAccount.Columns7"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("repArAccount.Columns8")))),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("repArAccount.Columns9"), resources.GetString("repArAccount.Columns10"), ((int)(resources.GetObject("repArAccount.Columns11"))), ((DevExpress.Utils.FormatType)(resources.GetObject("repArAccount.Columns12"))), resources.GetString("repArAccount.Columns13"), ((bool)(resources.GetObject("repArAccount.Columns14"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("repArAccount.Columns15"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("repArAccount.Columns16"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("repArAccount.Columns17"))))});
            this.repArAccount.DisplayMember = "AccountName";
            this.repArAccount.Name = "repArAccount";
            this.repArAccount.PopupWidth = 500;
            this.repArAccount.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.repArAccount.ValueMember = "Id";
            // 
            // colAR_USD
            // 
            resources.ApplyResources(this.colAR_USD, "colAR_USD");
            this.colAR_USD.ColumnEdit = this.repArUsd;
            this.colAR_USD.FieldName = "AR_USD";
            this.colAR_USD.Name = "colAR_USD";
            // 
            // colAR_THB
            // 
            resources.ApplyResources(this.colAR_THB, "colAR_THB");
            this.colAR_THB.ColumnEdit = this.repArThb;
            this.colAR_THB.FieldName = "AR_THB";
            this.colAR_THB.Name = "colAR_THB";
            // 
            // colAR_VND
            // 
            resources.ApplyResources(this.colAR_VND, "colAR_VND");
            this.colAR_VND.ColumnEdit = this.reqArVnd;
            this.colAR_VND.FieldName = "AR_VND";
            this.colAR_VND.Name = "colAR_VND";
            // 
            // repArKhr
            // 
            resources.ApplyResources(this.repArKhr, "repArKhr");
            this.repArKhr.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("repArKhr.Buttons"))))});
            this.repArKhr.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("repArKhr.Columns"), resources.GetString("repArKhr.Columns1")),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("repArKhr.Columns2"), resources.GetString("repArKhr.Columns3"))});
            this.repArKhr.DisplayMember = "AccountName";
            this.repArKhr.Name = "repArKhr";
            this.repArKhr.PopupWidth = 500;
            this.repArKhr.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.repArKhr.ValueMember = "Id";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.searchControl);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // searchControl
            // 
            this.searchControl.Client = this.dgv;
            resources.ApplyResources(this.searchControl, "searchControl");
            this.searchControl.Name = "searchControl";
            this.searchControl.Properties.Appearance.Options.UseFont = true;
            this.searchControl.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Repository.ClearButton(),
            new DevExpress.XtraEditors.Repository.SearchButton()});
            this.searchControl.Properties.Client = this.dgv;
            this.searchControl.Properties.DropDownRows = 10;
            this.searchControl.Properties.Padding = new System.Windows.Forms.Padding(1);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "CUSTOMER_CONNECTION_TYPE_ID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn1, "dataGridViewTextBoxColumn1");
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "CUSTOMER_CONNECTION_TYPE_NAME";
            resources.ApplyResources(this.dataGridViewTextBoxColumn2, "dataGridViewTextBoxColumn2");
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.DataPropertyName = "IS_ACTIVE";
            resources.ApplyResources(this.dataGridViewCheckBoxColumn1, "dataGridViewCheckBoxColumn1");
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "INCOME_ACCOUNT_NAME";
            resources.ApplyResources(this.dataGridViewTextBoxColumn3, "dataGridViewTextBoxColumn3");
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "ACCOUNT_RECEIVABLE_NAME";
            resources.ApplyResources(this.dataGridViewTextBoxColumn4, "dataGridViewTextBoxColumn4");
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // repArUsd
            // 
            resources.ApplyResources(this.repArUsd, "repArUsd");
            this.repArUsd.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("repArUsd.Buttons"))))});
            this.repArUsd.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("repArUsd.Columns"), resources.GetString("repArUsd.Columns1")),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("repArUsd.Columns2"), resources.GetString("repArUsd.Columns3"))});
            this.repArUsd.DisplayMember = "AccountName";
            this.repArUsd.Name = "repArUsd";
            this.repArUsd.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.repArUsd.ValueMember = "Id";
            // 
            // repArThb
            // 
            resources.ApplyResources(this.repArThb, "repArThb");
            this.repArThb.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("repArThb.Buttons"))))});
            this.repArThb.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("repArThb.Columns"), resources.GetString("repArThb.Columns1")),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("repArThb.Columns2"), resources.GetString("repArThb.Columns3"))});
            this.repArThb.DisplayMember = "AccountName";
            this.repArThb.Name = "repArThb";
            this.repArThb.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.repArThb.ValueMember = "Id";
            // 
            // reqArVnd
            // 
            resources.ApplyResources(this.reqArVnd, "reqArVnd");
            this.reqArVnd.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("reqArVnd.Buttons"))))});
            this.reqArVnd.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("reqArVnd.Columns"), resources.GetString("reqArVnd.Columns1")),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("reqArVnd.Columns2"), resources.GetString("reqArVnd.Columns3"))});
            this.reqArVnd.DisplayMember = "AccountName";
            this.reqArVnd.Name = "reqArVnd";
            this.reqArVnd.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.reqArVnd.ValueMember = "Id";
            // 
            // PageCustomerType
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Controls.Add(this.roundPanel1);
            this.Name = "PageCustomerType";
            this.roundPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tLKPCUSTOMERCONNECTIONTYPEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConnectionType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repIcAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repArAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repArKhr)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.searchControl.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repArUsd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repArThb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reqArVnd)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Dynamic.Component.RoundPanel roundPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DevExpress.XtraGrid.GridControl dgv;
        private DevExpress.XtraGrid.Views.Grid.GridView dgvConnectionType;
        private DevExpress.XtraGrid.Columns.GridColumn colCUSTOMER_CONNECTION_TYPE_ID;
        private DevExpress.XtraGrid.Columns.GridColumn colCUSTOMER_CONNECTION_TYPE;
        private DevExpress.XtraGrid.Columns.GridColumn colDESCRIPTION;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repArAccount;
        private DevExpress.XtraGrid.Columns.GridColumn colCURRENCY_ID;
        private DevExpress.XtraGrid.Columns.GridColumn colTARIFF;
        private DevExpress.XtraGrid.Columns.GridColumn colNONLICENSE_CUSTOMER_GROUP_ID;
        private DevExpress.XtraGrid.Columns.GridColumn colPRICE_ID;
        private System.Windows.Forms.BindingSource tLKPCUSTOMERCONNECTIONTYPEBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colAR_KHR;
        private DevExpress.XtraGrid.Columns.GridColumn colINCOME_ACCOUNT;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repIcAccount;
        private DevExpress.XtraEditors.SearchControl searchControl;
        private DevExpress.XtraGrid.Columns.GridColumn colAR_USD;
        private DevExpress.XtraGrid.Columns.GridColumn colAR_THB;
        private DevExpress.XtraGrid.Columns.GridColumn colAR_VND;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repArKhr;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repArUsd;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repArThb;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit reqArVnd;
    }
}
