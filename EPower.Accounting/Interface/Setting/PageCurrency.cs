﻿using DevExpress.XtraGrid.Views.Base;
using EPower.Base.Logic;
using EPower.Base.Properties;
using HB01.Logics;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Accounting.Interface.Setting
{
    public partial class PageCurrency : Form
    {
        public PageCurrency()
        {
            InitializeComponent();
            searchControl.Properties.NullValuePrompt = Resources.SEARCH;
            this.dgvCurrenciesList.BestFitColumns();
            this.Load += PageCurrency_Load;
        }

        private void PageCurrency_Load(object sender, EventArgs e)
        {
            Bind();
        }

        #region Operation

        private void txt_QuickSearch(object sender, EventArgs e)
        {
            Bind();
        }

        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        public bool Valid()
        {
            if (!DBDataContext.Db.TLKP_CURRENCies.Any(x => x.EXTERNAL_CURRENCY_ID == 0)) return true;
            MsgBox.ShowWarning("សូមកំណត់គ្រប់រូបិយប័ណ្ណ ភ្ជាប់ជាមួយប្រព័ន្ធគណនេយ្យ!", Text);
            return false;
        }
        #endregion

        #region Method
        private void Bind()
        {
            try
            {
                dgv.DataSource = SettingLogic.Currencies;
                if (PointerLogic.isConnectedPointer)
                {
                    var currencies = CompanyLogic.Instance.AvailableCurrencies(Current.CompanyId).ToList();
                    repExternalCurrency.DataSource = currencies;
                }
                else
                {
                    colEXTERNAL_CURRENCY.OptionsColumn.AllowEdit = false;
                }                
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        #endregion
    }
}
