﻿using EPower.Base.Helper;
using EPower.Base.Logic;
using EPower.Base.Properties;
using HB01.Domain.Enums;
using HB01.Domain.ListModels;
using SoftTech;
using SoftTech.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Accounting.Interface.Setting
{
    public partial class PageInvoiceItem : Form
    {
        public PageInvoiceItem()
        {
            InitializeComponent();
            searchControl.Properties.NullValuePrompt = Resources.SEARCH;
            Bind();
            this.dgvInvoiceItems.BestFitColumns();
            this.dgvInvoiceItems.Columns[colAR_KHR.FieldName].Visible = SettingLogic.Currencies.Any(x => x.CURRENCY_ID == (int)Currency.KHR);
            this.dgvInvoiceItems.Columns[colAR_USD.FieldName].Visible = SettingLogic.Currencies.Any(x => x.CURRENCY_ID == (int)Currency.USD);
            this.dgvInvoiceItems.Columns[colAR_THB.FieldName].Visible = SettingLogic.Currencies.Any(x => x.CURRENCY_ID == (int)Currency.THB);
            this.dgvInvoiceItems.Columns[colAR_VND.FieldName].Visible = SettingLogic.Currencies.Any(x => x.CURRENCY_ID == (int)Currency.VND);
        }

        #region Method

        private void Bind()
        {
            try
            {
                repItemType.DataSource = DBDataContext.Db.TBL_INVOICE_ITEM_TYPEs.ToList();
                var icAcc = AccountChartHelper.GetAccounts(new AccountSearchParam { Natures = new List<AccountNatures> { AccountNatures.Income } });
                var arAcc = AccountChartHelper.GetAccounts(new AccountSearchParam { Natures = new List<AccountNatures> { AccountNatures.Asset } });
                repIcAccount.DataSource = icAcc;
                repArAccount.DataSource = arAcc;
                repCurrency.DataSource = SettingLogic.Currencies;
                dgv.DataSource = SettingLogic.InvoiceItems;
            }
            catch (Exception e)
            {
                MsgBox.ShowError(e);
            }
        }

        //public bool Valid()
        //{
        //    if (!DBDataContext.Db.TBL_INVOICE_ITEMs.Any(x => x.IS_ACTIVE && (x.INCOME_ACCOUNT_ID == 0 || x.AR_ACCOUNT == 0)))
        //        return true;
        //    MsgBox.ShowWarning("សូមកំណត់គ្រប់សេវាកម្ម ជាមួយប្រព័ន្ធគណនេយ្យដែលមិនទាន់បានបំពេញ!", Text);
        //    return false;
        //}
        #endregion
    }
}
