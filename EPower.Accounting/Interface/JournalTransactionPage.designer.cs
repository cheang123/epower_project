﻿
using HB01.Helpers;

namespace EPower.Accounting.Interface
{
    partial class JournalTransactionPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnPush = new HB01.Helpers.DevExpressCustomize.DButton();
            this.searchControl = new DevExpress.XtraEditors.SearchControl();
            this.tgv = new DevExpress.XtraTreeList.TreeList();
            this.colGroupName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colREF_NO = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colCUSTOMER_CODE = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colCUSTOMER_NAME = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colTRANSACTION_DATE = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colPOSTED_DATE = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colPOSTED_AMOUNT = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colAMOUNT = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this._colCurrencyCode = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repTranNo = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.rdpDate = new HB01.Helpers.ReportDatePicker();
            this.menuView = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.behaviorManager1 = new DevExpress.Utils.Behaviors.BehaviorManager(this.components);
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchControl.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTranNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.Controls.Add(this.btnPush);
            this.panel1.Controls.Add(this.searchControl);
            this.panel1.Controls.Add(this.rdpDate);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1386, 40);
            this.panel1.TabIndex = 10;
            // 
            // btnPush
            // 
            this.btnPush.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPush.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(92)))), ((int)(((byte)(115)))));
            this.btnPush.Appearance.Font = new System.Drawing.Font("Khmer Kep", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPush.Appearance.Options.UseBackColor = true;
            this.btnPush.Appearance.Options.UseFont = true;
            this.btnPush.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPush.HotKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.btnPush.Location = new System.Drawing.Point(1268, 4);
            this.btnPush.Margin = new System.Windows.Forms.Padding(2);
            this.btnPush.Name = "btnPush";
            this.btnPush.Size = new System.Drawing.Size(108, 32);
            this.btnPush.TabIndex = 36;
            this.btnPush.Text = "បញ្ជូនប្រតិបត្តិការ";
            this.btnPush.Click += new System.EventHandler(this.btnPush_Click);
            // 
            // searchControl
            // 
            this.searchControl.Client = this.tgv;
            this.searchControl.Cursor = System.Windows.Forms.Cursors.Hand;
            this.searchControl.EditValue = "";
            this.searchControl.Location = new System.Drawing.Point(3, 4);
            this.searchControl.Margin = new System.Windows.Forms.Padding(2);
            this.searchControl.Name = "searchControl";
            this.searchControl.Properties.Appearance.Font = new System.Drawing.Font("Khmer Kep", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchControl.Properties.Appearance.Options.UseFont = true;
            this.searchControl.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Repository.ClearButton(),
            new DevExpress.XtraEditors.Repository.SearchButton()});
            this.searchControl.Properties.Client = this.tgv;
            this.searchControl.Properties.DropDownRows = 10;
            this.searchControl.Properties.Padding = new System.Windows.Forms.Padding(1);
            this.searchControl.Size = new System.Drawing.Size(174, 32);
            this.searchControl.TabIndex = 35;
            // 
            // tgv
            // 
            this.tgv.AppearancePrint.BandPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tgv.AppearancePrint.BandPanel.Options.UseFont = true;
            this.tgv.AppearancePrint.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tgv.AppearancePrint.HeaderPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tgv.AppearancePrint.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tgv.AppearancePrint.HeaderPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tgv.AppearancePrint.HeaderPanel.Options.UseBackColor = true;
            this.tgv.AppearancePrint.HeaderPanel.Options.UseBorderColor = true;
            this.tgv.AppearancePrint.HeaderPanel.Options.UseFont = true;
            this.tgv.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colGroupName,
            this.colREF_NO,
            this.colCUSTOMER_CODE,
            this.colCUSTOMER_NAME,
            this.colTRANSACTION_DATE,
            this.colPOSTED_DATE,
            this.colPOSTED_AMOUNT,
            this.colAMOUNT,
            this._colCurrencyCode});
            this.tgv.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tgv.CustomizationFormBounds = new System.Drawing.Rectangle(1100, 92, 266, 373);
            this.tgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tgv.Font = new System.Drawing.Font("Khmer Kep", 9.75F);
            this.tgv.HorzScrollStep = 4;
            this.tgv.KeyFieldName = "RenderId";
            this.tgv.Location = new System.Drawing.Point(10, 40);
            this.tgv.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tgv.MinWidth = 28;
            this.tgv.Name = "tgv";
            this.tgv.OptionsBehavior.AutoPopulateColumns = false;
            this.tgv.OptionsBehavior.AutoScrollOnSorting = false;
            this.tgv.OptionsBehavior.Editable = false;
            this.tgv.OptionsBehavior.PopulateServiceColumns = true;
            this.tgv.OptionsCustomization.AllowSort = false;
            this.tgv.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.EntireBranch;
            this.tgv.OptionsPrint.PrintReportFooter = false;
            this.tgv.OptionsView.ShowIndicator = false;
            this.tgv.OptionsView.ShowTreeLines = DevExpress.Utils.DefaultBoolean.False;
            this.tgv.Padding = new System.Windows.Forms.Padding(1);
            this.tgv.ParentFieldName = "ParentId";
            this.tgv.PreviewFieldName = "Vendor";
            this.tgv.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repTranNo});
            this.tgv.Size = new System.Drawing.Size(1366, 633);
            this.tgv.TabIndex = 40;
            this.tgv.TreeLevelWidth = 25;
            // 
            // colGroupName
            // 
            this.colGroupName.Caption = "GroupName";
            this.colGroupName.FieldName = "GroupName";
            this.colGroupName.MinWidth = 28;
            this.colGroupName.Name = "colGroupName";
            this.colGroupName.Width = 106;
            // 
            // colREF_NO
            // 
            this.colREF_NO.Caption = "REF_NO";
            this.colREF_NO.FieldName = "RefNo";
            this.colREF_NO.MinWidth = 28;
            this.colREF_NO.Name = "colREF_NO";
            this.colREF_NO.Visible = true;
            this.colREF_NO.VisibleIndex = 0;
            this.colREF_NO.Width = 106;
            // 
            // colCUSTOMER_CODE
            // 
            this.colCUSTOMER_CODE.AppearanceCell.Options.UseTextOptions = true;
            this.colCUSTOMER_CODE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCUSTOMER_CODE.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCUSTOMER_CODE.AppearanceHeader.Options.UseTextOptions = true;
            this.colCUSTOMER_CODE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCUSTOMER_CODE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCUSTOMER_CODE.Caption = "CUSTOMER_CODE";
            this.colCUSTOMER_CODE.FieldName = "CustomerCode";
            this.colCUSTOMER_CODE.MinWidth = 28;
            this.colCUSTOMER_CODE.Name = "colCUSTOMER_CODE";
            this.colCUSTOMER_CODE.Visible = true;
            this.colCUSTOMER_CODE.VisibleIndex = 1;
            this.colCUSTOMER_CODE.Width = 106;
            // 
            // colCUSTOMER_NAME
            // 
            this.colCUSTOMER_NAME.AppearanceCell.Options.UseTextOptions = true;
            this.colCUSTOMER_NAME.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colCUSTOMER_NAME.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCUSTOMER_NAME.AppearanceHeader.Options.UseTextOptions = true;
            this.colCUSTOMER_NAME.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colCUSTOMER_NAME.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCUSTOMER_NAME.Caption = "CUSTOMER_NAME";
            this.colCUSTOMER_NAME.FieldName = "CustomerName";
            this.colCUSTOMER_NAME.MinWidth = 28;
            this.colCUSTOMER_NAME.Name = "colCUSTOMER_NAME";
            this.colCUSTOMER_NAME.Visible = true;
            this.colCUSTOMER_NAME.VisibleIndex = 2;
            this.colCUSTOMER_NAME.Width = 106;
            // 
            // colTRANSACTION_DATE
            // 
            this.colTRANSACTION_DATE.AppearanceCell.Options.UseTextOptions = true;
            this.colTRANSACTION_DATE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTRANSACTION_DATE.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTRANSACTION_DATE.AppearanceHeader.Options.UseTextOptions = true;
            this.colTRANSACTION_DATE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTRANSACTION_DATE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTRANSACTION_DATE.Caption = "TRANSACTION_DATE";
            this.colTRANSACTION_DATE.FieldName = "TranDate";
            this.colTRANSACTION_DATE.Name = "colTRANSACTION_DATE";
            this.colTRANSACTION_DATE.Visible = true;
            this.colTRANSACTION_DATE.VisibleIndex = 3;
            this.colTRANSACTION_DATE.Width = 106;
            // 
            // colPOSTED_DATE
            // 
            this.colPOSTED_DATE.AppearanceCell.Options.UseTextOptions = true;
            this.colPOSTED_DATE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPOSTED_DATE.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colPOSTED_DATE.AppearanceHeader.Options.UseTextOptions = true;
            this.colPOSTED_DATE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPOSTED_DATE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colPOSTED_DATE.Caption = "POSTED_DATE";
            this.colPOSTED_DATE.FieldName = "PostedDate";
            this.colPOSTED_DATE.Name = "colPOSTED_DATE";
            this.colPOSTED_DATE.Visible = true;
            this.colPOSTED_DATE.VisibleIndex = 4;
            // 
            // colPOSTED_AMOUNT
            // 
            this.colPOSTED_AMOUNT.AppearanceCell.Options.UseTextOptions = true;
            this.colPOSTED_AMOUNT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colPOSTED_AMOUNT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colPOSTED_AMOUNT.AppearanceHeader.Options.UseTextOptions = true;
            this.colPOSTED_AMOUNT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colPOSTED_AMOUNT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colPOSTED_AMOUNT.Caption = "POSTED_AMOUNT";
            this.colPOSTED_AMOUNT.FieldName = "PostedAmount";
            this.colPOSTED_AMOUNT.MinWidth = 28;
            this.colPOSTED_AMOUNT.Name = "colPOSTED_AMOUNT";
            this.colPOSTED_AMOUNT.Visible = true;
            this.colPOSTED_AMOUNT.VisibleIndex = 5;
            this.colPOSTED_AMOUNT.Width = 106;
            // 
            // colAMOUNT
            // 
            this.colAMOUNT.AppearanceCell.Options.UseTextOptions = true;
            this.colAMOUNT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colAMOUNT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colAMOUNT.AppearanceHeader.Options.UseTextOptions = true;
            this.colAMOUNT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colAMOUNT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colAMOUNT.Caption = "AMOUNT";
            this.colAMOUNT.FieldName = "Amount";
            this.colAMOUNT.MinWidth = 28;
            this.colAMOUNT.Name = "colAMOUNT";
            this.colAMOUNT.Visible = true;
            this.colAMOUNT.VisibleIndex = 6;
            this.colAMOUNT.Width = 106;
            // 
            // _colCurrencyCode
            // 
            this._colCurrencyCode.AppearanceCell.Options.UseTextOptions = true;
            this._colCurrencyCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colCurrencyCode.AppearanceHeader.Options.UseTextOptions = true;
            this._colCurrencyCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colCurrencyCode.Caption = "​";
            this._colCurrencyCode.CustomizationCaption = "រូបិយប័ណ្ណ";
            this._colCurrencyCode.FieldName = "CurrencyCode";
            this._colCurrencyCode.MinWidth = 28;
            this._colCurrencyCode.Name = "_colCurrencyCode";
            this._colCurrencyCode.Width = 106;
            // 
            // repTranNo
            // 
            this.repTranNo.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.repTranNo.Appearance.Options.UseForeColor = true;
            this.repTranNo.AppearanceDisabled.ForeColor = System.Drawing.Color.Blue;
            this.repTranNo.AppearanceDisabled.Options.UseForeColor = true;
            this.repTranNo.AppearanceFocused.BackColor = System.Drawing.Color.Blue;
            this.repTranNo.AppearanceFocused.FontStyleDelta = System.Drawing.FontStyle.Underline;
            this.repTranNo.AppearanceFocused.Options.UseBackColor = true;
            this.repTranNo.AppearanceFocused.Options.UseFont = true;
            this.repTranNo.AutoHeight = false;
            this.repTranNo.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repTranNo.LinkColor = System.Drawing.Color.Blue;
            this.repTranNo.Name = "repTranNo";
            // 
            // rdpDate
            // 
            this.rdpDate.AutoSave = false;
            this.rdpDate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.rdpDate.DateFilterTypes = HB01.Helpers.ReportDatePicker.DateFilterType.Period;
            this.rdpDate.DefaultValue = HB01.Helpers.ReportDatePicker.DateRanges.Today;
            this.rdpDate.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.rdpDate.FromDate = new System.DateTime(2022, 3, 24, 0, 0, 0, 0);
            this.rdpDate.LastCustomDays = 30;
            this.rdpDate.LastFinancialYear = true;
            this.rdpDate.LastMonth = true;
            this.rdpDate.LastQuarter = true;
            this.rdpDate.LastWeek = true;
            this.rdpDate.Location = new System.Drawing.Point(181, 4);
            this.rdpDate.Margin = new System.Windows.Forms.Padding(2);
            this.rdpDate.Name = "rdpDate";
            this.rdpDate.Size = new System.Drawing.Size(193, 32);
            this.rdpDate.TabIndex = 19;
            this.rdpDate.ThisFinancialYear = true;
            this.rdpDate.ThisMonth = true;
            this.rdpDate.ThisQuarter = true;
            this.rdpDate.ThisWeek = true;
            this.rdpDate.ToDate = new System.DateTime(2022, 3, 24, 23, 59, 59, 0);
            this.rdpDate.Today = true;
            this.rdpDate.Value = HB01.Helpers.ReportDatePicker.DateRanges.Today;
            this.rdpDate.Yesterday = true;
            this.rdpDate.ValueChanged += new System.EventHandler(this.rdpDate_ValueChanged);
            // 
            // menuView
            // 
            this.menuView.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.menuView.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuView.Name = "menuUser";
            this.menuView.Size = new System.Drawing.Size(61, 4);
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 40);
            this.flowLayoutPanel2.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(10, 633);
            this.flowLayoutPanel2.TabIndex = 35;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.flowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.flowLayoutPanel4.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(1376, 40);
            this.flowLayoutPanel4.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(10, 633);
            this.flowLayoutPanel4.TabIndex = 35;
            // 
            // JournalTransactionPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1386, 673);
            this.Controls.Add(this.tgv);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Name = "JournalTransactionPage";
            this.Text = "AgedPayableReportPage";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.searchControl.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTranNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private ReportDatePicker rdpDate;
        private System.Windows.Forms.ContextMenuStrip menuView;
        private DevExpress.XtraEditors.SearchControl searchControl;
        private DevExpress.Utils.Behaviors.BehaviorManager behaviorManager1;
        private DevExpress.XtraTreeList.TreeList tgv;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repTranNo;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colGroupName;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colREF_NO;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colCUSTOMER_CODE;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colCUSTOMER_NAME;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colTRANSACTION_DATE;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colPOSTED_AMOUNT;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colAMOUNT;
        private DevExpress.XtraTreeList.Columns.TreeListColumn _colCurrencyCode;
        private HB01.Helpers.DevExpressCustomize.DButton btnPush;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colPOSTED_DATE;
    }
}