﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Accounting.Interface
{
    partial class PageAccount
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageAccount));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnRESYNC_ACCOUNT = new SoftTech.Component.ExButton();
            this.searchControl = new DevExpress.XtraEditors.SearchControl();
            this.tgv = new DevExpress.XtraTreeList.TreeList();
            this.AccountId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.Account = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.ExtAccountId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.Ext_Account = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.btnREMOVE = new SoftTech.Component.ExButton();
            this.btnADD = new SoftTech.Component.ExButton();
            this.btnEDIT = new SoftTech.Component.ExButton();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchControl.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tgv)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.btnRESYNC_ACCOUNT);
            this.panel1.Controls.Add(this.searchControl);
            this.panel1.Controls.Add(this.btnREMOVE);
            this.panel1.Controls.Add(this.btnADD);
            this.panel1.Controls.Add(this.btnEDIT);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnRESYNC_ACCOUNT
            // 
            resources.ApplyResources(this.btnRESYNC_ACCOUNT, "btnRESYNC_ACCOUNT");
            this.btnRESYNC_ACCOUNT.Name = "btnRESYNC_ACCOUNT";
            this.btnRESYNC_ACCOUNT.UseVisualStyleBackColor = true;
            this.btnRESYNC_ACCOUNT.Click += new System.EventHandler(this.btnSyncAccount_Click);
            // 
            // searchControl
            // 
            this.searchControl.Client = this.tgv;
            resources.ApplyResources(this.searchControl, "searchControl");
            this.searchControl.Name = "searchControl";
            this.searchControl.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("searchControl.Properties.Appearance.Font")));
            this.searchControl.Properties.Appearance.Options.UseFont = true;
            this.searchControl.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Repository.ClearButton(),
            new DevExpress.XtraEditors.Repository.SearchButton()});
            this.searchControl.Properties.Client = this.tgv;
            this.searchControl.Properties.DropDownRows = 10;
            this.searchControl.Properties.Padding = new System.Windows.Forms.Padding(1);
            // 
            // tgv
            // 
            this.tgv.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.AccountId,
            this.Account,
            this.ExtAccountId,
            this.Ext_Account});
            this.tgv.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.tgv, "tgv");
            this.tgv.KeyFieldName = "RenderId";
            this.tgv.Name = "tgv";
            this.tgv.OptionsBehavior.Editable = false;
            this.tgv.OptionsView.ShowHorzLines = false;
            this.tgv.OptionsView.ShowIndicator = false;
            this.tgv.OptionsView.ShowTreeLines = DevExpress.Utils.DefaultBoolean.False;
            this.tgv.OptionsView.ShowVertLines = false;
            this.tgv.ParentFieldName = "ParentId";
            // 
            // AccountId
            // 
            resources.ApplyResources(this.AccountId, "AccountId");
            this.AccountId.FieldName = "AccountId";
            this.AccountId.Name = "AccountId";
            // 
            // Account
            // 
            resources.ApplyResources(this.Account, "Account");
            this.Account.FieldName = "Account";
            this.Account.Name = "Account";
            // 
            // ExtAccountId
            // 
            resources.ApplyResources(this.ExtAccountId, "ExtAccountId");
            this.ExtAccountId.FieldName = "ExtAccountId";
            this.ExtAccountId.Name = "ExtAccountId";
            // 
            // Ext_Account
            // 
            resources.ApplyResources(this.Ext_Account, "Ext_Account");
            this.Ext_Account.FieldName = "ExtAccount";
            this.Ext_Account.Name = "Ext_Account";
            // 
            // btnREMOVE
            // 
            resources.ApplyResources(this.btnREMOVE, "btnREMOVE");
            this.btnREMOVE.Name = "btnREMOVE";
            this.btnREMOVE.UseVisualStyleBackColor = true;
            this.btnREMOVE.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnADD
            // 
            resources.ApplyResources(this.btnADD, "btnADD");
            this.btnADD.Name = "btnADD";
            this.btnADD.UseVisualStyleBackColor = true;
            this.btnADD.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnEDIT
            // 
            resources.ApplyResources(this.btnEDIT, "btnEDIT");
            this.btnEDIT.Name = "btnEDIT";
            this.btnEDIT.UseVisualStyleBackColor = true;
            this.btnEDIT.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // PageAccount
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tgv);
            this.Controls.Add(this.panel1);
            this.Name = "PageAccount";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.searchControl.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnADD;
        private ExButton btnEDIT;
        private ExButton btnREMOVE;
        private DevExpress.XtraTreeList.TreeList tgv;
        private DevExpress.XtraTreeList.Columns.TreeListColumn AccountId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn Account;
        private DevExpress.XtraTreeList.Columns.TreeListColumn ExtAccountId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn Ext_Account;
        private DevExpress.XtraEditors.SearchControl searchControl;
        private ExButton btnRESYNC_ACCOUNT;
    }
}
