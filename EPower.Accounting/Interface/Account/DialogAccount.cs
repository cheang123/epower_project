﻿using EPower.Base.Logic;
using EPower.Base.Properties;
using HB01.Domain.ListModels;
using HB01.Helpers.DevExpressCustomize;
using HB01.Logics;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using CurrencyModel = HB01.Domain.Models.Settings.Currency;

namespace EPower.Accounting.Interface
{
    public partial class DialogAccount : ExDialog
    {
        #region Private data
        GeneralProcess _flag;
        TBL_ACCOUNT_CHART _objNew = new TBL_ACCOUNT_CHART();
        TBL_ACCOUNT_CHART _objOld = new TBL_ACCOUNT_CHART();
        public TBL_ACCOUNT_CHART AccountChart
        {
            get { return _objNew; }
        }

        #endregion

        #region Constructor
        public DialogAccount(GeneralProcess flag, TBL_ACCOUNT_CHART objFixAssetCategory)
        {
            InitializeComponent();
            _flag = flag;
            objFixAssetCategory._CopyTo(_objNew);
            objFixAssetCategory._CopyTo(_objOld);
            this.Text = flag.GetText(this.Text);
            read();
            this.btnCHANGE_LOG.Visible = false;// this._flag != GeneralProcess.Insert;
        }

        #endregion

        #region Events

        private void ChangeKeyboardKhmer(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        private void ChangeKeyboardEnglish(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        /// <summary>
        /// Data transaction.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
            //If record is duplicate.
            this.ClearAllValidation();

            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objNew);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objNew);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }

        #endregion

        #region Method

        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();
            //if (string.IsNullOrEmpty(this.cboParentAccount.Text.Trim()))
            //{
            //    cboParentAccount.SetValidation(string.Format(Resources.REQUIRED_SELECT_VALUE, lblPARENT_ACCOUNT.Text));
            //    val = true;
            //}
            if (txtCode.Text.Trim() == string.Empty)
            {
                txtCode.SetValidation(string.Format(Resources.REQUIRED, lblACCOUNT_CODE.Text));
                val = true;
            }
            if (txtName.Text.Trim() == string.Empty)
            {
                txtName.SetValidation(string.Format(Resources.REQUIRED, lblACCOUNT_NAME.Text));
                val = true;
            }
            if (PointerLogic.isConnectedPointer && string.IsNullOrEmpty(cboExtAccount.Text.Trim()))
            {
                cboExtAccount.SetValidation(string.Format(Resources.REQUIRED_SELECT_VALUE, lblEXT_ACCOUNT.Text));
                val = true;
            }
            return val;
        }

        private void read()
        {
            List<AccountListModel> accounts = new List<AccountListModel>();
            if (PointerLogic.isConnectedPointer)
            {
                this.cboExtAccount.Enabled = true;
                accounts = AccountLogic.Instance.List<AccountListModel>(new AccountSearchParam() { });
            }

            var accType = (from t in DBDataContext.Db.TBL_ACCOUNT_TYPEs
                           where t.IS_ACTIVE
                           select new AccountTypeListModel
                           {
                               Id = t.TYPE_ID,
                               Name = t.TYPE_NAME
                           }).ToList();
            var parentAcc = (from a in DBDataContext.Db.TBL_ACCOUNT_CHARTs
                             where a.IS_ACTIVE
                             select new AccountListModel
                             {
                                 Id = a.ACCOUNT_ID,
                                 AccountCode = a.ACCOUNT_CODE,
                                 AccountName = a.ACCOUNT_NAME
                             }).ToList();
            var currencies = (from c in SettingLogic.Currencies
                              select new CurrencyModel
                              {
                                  Id = c.CURRENCY_ID,
                                  Code = c.CURRENCY_CODE,
                                  Name = c.CURRENCY_NAME,
                                  Sign = c.CURRENCY_SIGN
                              }).ToList();
            cboParentAccount.SetDatasourceList(parentAcc, nameof(AccountListModel.AccountName));
            cboExtAccount.SetDatasourceList(accounts, nameof(AccountListModel.AccountName));
            cboAccountType.SetDatasourceList(accType, nameof(AccountTypeListModel.Name));
            cboCurrency.SetDatasource(currencies, nameof(CurrencyModel.Code));
            txtName.Text = _objNew.ACCOUNT_NAME;
            txtCode.Text = _objNew.ACCOUNT_CODE;
            cboParentAccount.EditValue = _objNew.PARENT_ID;
            cboAccountType.EditValue = _objNew.ACCOUNT_TYPE_ID;
            cboCurrency.EditValue = _objNew.CURRENCY_ID;
            cboExtAccount.EditValue = _objNew.EXT_ACCOUNT_ID;
        }

        private void write()
        {
            //_objNew.ACCOUNT_NAME = txtName.Text;
            //_objNew.ACCOUNT_CODE = txtCode.Text;
            _objNew.ACCOUNT_TYPE_ID = (int)cboAccountType.EditValue;
            _objNew.PARENT_ID = (int)cboParentAccount.EditValue;
            if (PointerLogic.isConnectedPointer)
            {
                _objNew.EXT_ACCOUNT_ID = (int)cboExtAccount.EditValue;
            }
        }
        #endregion

    }
}